// NOLINTBEGIN(bugprone-chained-comparison)

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <optional>
#include <nlohmann/json.hpp>

#include "zaber/motion/units.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/requests/test_request.private.h"
#include "zaber/motion/dto/microscopy/microscope_config.private.h"
#include "zaber/motion/dto/ascii/can_set_state_device_response.private.h"
#include "zaber/motion/dto/requests/pvt_point_request.private.h"

using namespace zaber::motion;

TEST_CASE( "Deserialization -- Test Request", "[dto][serialization]") {
    nlohmann::json obj;
    obj["returnError"] = true;
    obj["dataPing"] = "BINGO BANGO";
    obj["returnErrorWithData"] = true;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);

    requests::TestRequest dto;
    dto.populateFromByteArray(buffer);

    REQUIRE( dto.returnError );
    REQUIRE( dto.dataPing == "BINGO BANGO" );
    REQUIRE( dto.returnErrorWithData );
}

TEST_CASE( "Serialization -- Test Request", "[dto][serialization]") {
    requests::TestRequest dto;
    dto.returnError = true;
    dto.dataPing = "BANGO BINGO";
    dto.returnErrorWithData = true;

    nlohmann::json obj = nlohmann::json::from_bson(dto.toByteArray());

    REQUIRE( obj["returnError"] == true );
    REQUIRE( obj["dataPing"] == "BANGO BINGO" );
    REQUIRE( obj["returnErrorWithData"] == true );
}

TEST_CASE( "Deserialization -- Optional values", "[dto][serialization]") {
    nlohmann::json obj;
    obj["focusAxis"] = nullptr;
    obj["xAxis"] = nullptr;
    obj["yAxis"] = nullptr;
    obj["illuminator"] = 7;
    obj["filterChanger"] = nullptr;
    obj["objectiveChanger"] = nullptr;
    obj["autofocus"] = nullptr;
    obj["cameraTrigger"] = nullptr;

    microscopy::MicroscopeConfig dto;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    dto.populateFromByteArray(buffer);

    REQUIRE( dto.focusAxis == std::nullopt );
    REQUIRE( dto.xAxis == std::nullopt );
    REQUIRE( dto.xAxis == std::nullopt );
    REQUIRE( dto.illuminator == 7 );
    REQUIRE( dto.filterChanger == std::nullopt );
    REQUIRE( dto.objectiveChanger == std::nullopt );
    REQUIRE( dto.autofocus == std::nullopt );
    REQUIRE( dto.cameraTrigger == std::nullopt );
}

TEST_CASE( "Serialization -- Optional values", "[dto][serialization]") {
    microscopy::MicroscopeConfig dto;
    dto.illuminator = 7;

    nlohmann::json obj = nlohmann::json::from_bson(dto.toByteArray());

    REQUIRE( obj["focusAxis"].template get<std::optional<AxisAddress>>() == std::nullopt );
    REQUIRE( obj["illuminator"].template get<std::optional<int>>() == 7 );
}

TEST_CASE( "Serialization -- Empty vector", "[dto][serialization]") {
    ascii::CanSetStateDeviceResponse dto;
    nlohmann::json obj = nlohmann::json::from_bson(dto.toByteArray());

    REQUIRE( obj.contains("axisErrors") );
    std::vector vec = obj["axisErrors"].template get<std::vector<ascii::CanSetStateAxisResponse>>();
    REQUIRE( vec.empty() );
}

TEST_CASE( "Deserialization -- Vector values", "[dto][serialization]" ) {
    nlohmann::json obj;
    ascii::CanSetStateAxisResponse e1("some_error1", {});
    ascii::CanSetStateAxisResponse e2("some_error2", {});
    obj["error"] = "";
    obj["axisErrors"] = { e1, e2 };

    ascii::CanSetStateDeviceResponse dto;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    dto.populateFromByteArray(buffer);

    REQUIRE( dto.axisErrors.size() == 2 );
    REQUIRE( dto.axisErrors[0].error == "some_error1" );
    REQUIRE( dto.axisErrors[1].error == "some_error2" );
}

TEST_CASE( "Serialization -- Vector values", "[dto][serialization]" ) {
    ascii::CanSetStateDeviceResponse dto;
    ascii::CanSetStateAxisResponse e1("some_error1", {});
    ascii::CanSetStateAxisResponse e2("some_error2", {});
    dto.axisErrors = { e1, e2 };

    nlohmann::json obj = nlohmann::json::from_bson(dto.toByteArray());

    auto vec = obj["axisErrors"].template get<std::vector<ascii::CanSetStateAxisResponse>>();
    REQUIRE( vec.size() == 2 );
    REQUIRE( vec[0].error == "some_error1" );
    REQUIRE( vec[1].error == "some_error2" );
}

TEST_CASE( "Deserialization --  Nested optional DTO value", "[dto][serialization]") {
    nlohmann::json obj;
    obj["focusAxis"] = AxisAddress(13, 17);
    obj["xAxis"] = nullptr;
    obj["yAxis"] = nullptr;
    obj["illuminator"] = nullptr;
    obj["filterChanger"] = nullptr;
    obj["objectiveChanger"] = nullptr;
    obj["autofocus"] = nullptr;
    obj["cameraTrigger"] = nullptr;

    microscopy::MicroscopeConfig dto;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    dto.populateFromByteArray(buffer);

    REQUIRE( dto.focusAxis.has_value() );
    REQUIRE( dto.focusAxis.value().device == 13 );
    REQUIRE( dto.focusAxis.value().axis == 17 );
}

TEST_CASE( "Serialization --  Nested optional DTO value", "[dto][serialization]") {
    microscopy::MicroscopeConfig dto;
    dto.focusAxis = AxisAddress(13, 17);

    nlohmann::json obj = nlohmann::json::from_bson(dto.toByteArray());

    REQUIRE( obj["focusAxis"].template get<std::optional<AxisAddress>>().has_value() );
    REQUIRE( obj["focusAxis"].template get<std::optional<AxisAddress>>().value().device == 13 );
    REQUIRE( obj["focusAxis"].template get<std::optional<AxisAddress>>().value().axis == 17 );
}

TEST_CASE( "toString -- Test Request", "[dto][tostring]") {
    requests::TestRequest dto(true, "BINGO BANGO", true);

    const std::string expected = "TestRequest { "
        "returnError: 1, dataPing: BINGO BANGO, returnErrorWithData: 1 "
    "}";

    REQUIRE( expected == dto.toString() );
}

TEST_CASE( "toString -- Vector values", "[dto][tostring]") {
    ascii::CanSetStateDeviceResponse dto;
    ascii::CanSetStateAxisResponse e1("some_error1", {});
    ascii::CanSetStateAxisResponse e2("some_error2", {});
    dto.axisErrors = { e1, e2 };

    const std::string expected = "CanSetStateDeviceResponse { "
        "error: null, "
        "axisErrors: [ "
            "CanSetStateAxisResponse { error: some_error1, axisNumber: 0 }, "
            "CanSetStateAxisResponse { error: some_error2, axisNumber: 0 } ] "
        "}";

    REQUIRE( expected == dto.toString() );
}

TEST_CASE( "toString --  Nested optional DTO value", "[dto][tostring]") {
    microscopy::MicroscopeConfig dto;
    dto.focusAxis = AxisAddress(13, 17);

    const std::string expected = "MicroscopeConfig { "
        "focusAxis: AxisAddress { device: 13, axis: 17 }, xAxis: null, yAxis: null, "
        "illuminator: null, filterChanger: null, objectiveChanger: null, "
        "autofocus: null, cameraTrigger: null }";

    REQUIRE( expected == dto.toString() );
}

TEST_CASE( "toString --  Array of optional DTO value", "[dto][tostring]") {
    requests::PvtPointRequest dto;
    Measurement m1(1.5, Units::LENGTH_MILLIMETRES);
    std::optional<Measurement> m2 = std::nullopt;
    Measurement m3(3.0, Units::LENGTH_MILLIMETRES);
    dto.velocities = { m1, m2, m3 };

    const std::string expected = "PvtPointRequest { "
            "interfaceId: 0, device: 0, streamId: 0, pvt: 0, type: ABS, positions: [  ], "
            "velocities: [ "
                "Measurement { value: 1.5, unit: Length:millimetres }, "
                "null, "
                "Measurement { value: 3, unit: Length:millimetres } "
            "], "
            "time: Measurement { value: 0, unit: null } "
        "}";

    REQUIRE( expected == dto.toString() );
}

TEST_CASE( "== --  DTO Equal", "[dto][operator==]") {
    Measurement measurementA;
    Measurement measurementB;
    measurementA.setValue(1.5);
    measurementB.setValue(1.5);
    measurementA.setUnit(Units::LENGTH_MILLIMETRES);
    measurementB.setUnit(Units::LENGTH_MILLIMETRES);
    REQUIRE( measurementA == measurementA );
    REQUIRE( measurementA == measurementB );

    ascii::CanSetStateDeviceResponse deviceResponseA;
    ascii::CanSetStateDeviceResponse deviceResponseB;
    ascii::CanSetStateAxisResponse axisResponseA("some_error", 3);
    ascii::CanSetStateAxisResponse axisResponseB("some_error", 3);
    deviceResponseA.setError("some_error");
    deviceResponseB.setError("some_error");
    deviceResponseA.axisErrors.push_back(axisResponseA);
    deviceResponseB.axisErrors.push_back(axisResponseB);

    REQUIRE( axisResponseA == axisResponseB );
    REQUIRE( deviceResponseA == deviceResponseB );
}

TEST_CASE( "!= --  DTO Not Equal", "[dto][operator!=]") {
    Measurement measurementA;
    Measurement measurementB;
    measurementA.setValue(1);
    measurementB.setValue(1);
    measurementA.setUnit(Units::LENGTH_MILLIMETRES);
    measurementA.setUnit(Units::LENGTH_METRES);
    REQUIRE( measurementA != measurementB );

    ascii::CanSetStateDeviceResponse deviceResponseA;
    ascii::CanSetStateDeviceResponse deviceResponseB;
    ascii::CanSetStateAxisResponse axisResponseA("some_error", 3);
    deviceResponseA.setError("some_error");
    deviceResponseB.setError("some_error");
    deviceResponseA.axisErrors.push_back(axisResponseA);
    deviceResponseB.axisErrors.push_back(axisResponseA);
    deviceResponseB.axisErrors.push_back(axisResponseA);
    REQUIRE( deviceResponseA != deviceResponseB );
}

// NOLINTEND(bugprone-chained-comparison)
