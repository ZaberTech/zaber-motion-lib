#include "catch.hpp"

#include "utils.h"
#include "zaber/motion/gateway/event_handler.h"

namespace zaber { namespace motion {

TEST_CASE( "EventHandler -- handleEvent", "[event][handler]" ) {
    auto filter = [](const SdkEvent&) { return true; };
    auto mutator = [](const SdkEvent&) { return 0; };
    EventHandler<int> handler(nullptr, filter, mutator);
    SdkEvent dummy;

    SECTION( "does nothing when callback isn't set" ) {
        try {
            handler.handleEvent(dummy);
            REQUIRE( true ); // NOLINT
        } catch (...) {
            REQUIRE( false ); // NOLINT
        }
    }

    SECTION( "invokes callback" ) {
        int called = 0;
        handler.setCallback([&](int) { called++; });

        handler.handleEvent(dummy);

        REQUIRE( called == 1 ); // NOLINT
    }

    SECTION( "does not deadlock with nested callback set" ) {
        int called = 0;
        std::function<void(int)> callback = [&](int){ called++; };
        handler.setCallback([&](int) { handler.setCallback(callback); });

        handler.handleEvent(dummy);
        REQUIRE( called == 0 ); // NOLINT

        handler.handleEvent(dummy);
        REQUIRE( called == 1 ); // NOLINT
    }

    SECTION( "custom filter" ) {
        int called = 0;
        auto callback = [&](int) { called++; };
        EventHandler<int> handler1(callback, [](const SdkEvent&) { return false; }, mutator);
        handler1.handleEvent(dummy);
        REQUIRE( called == 0 ); // NOLINT

        EventHandler<int> handler2(callback, [](const SdkEvent&) { return true; }, mutator);
        handler2.handleEvent(dummy);
        REQUIRE( called == 1 ); // NOLINT
    }

    SECTION( "custom mutator" ) {
        int value = 0;
        auto callback = [&](int v) { value = v; };
        EventHandler<int> handler1(callback, [](const SdkEvent&) { return true; }, [](const SdkEvent&) { return 42; });

        handler1.handleEvent(dummy);
        REQUIRE( value == 42 ); // NOLINT
    }
}

}  // Namespace motion
}  // Namespace zaber
