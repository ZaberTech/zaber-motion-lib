#include "catch.hpp"

#include <future>
#include <chrono>
#include <thread>
#include <atomic>

#include "utils.h"
#include "zaber_motion_core_dummy.h"
#include "zaber/motion/binary/connection.h"
#include "zaber/motion/dto/requests/errors.h"
#include "zaber/motion/dto/requests/disconnected_event.h"
#include "zaber/motion/dto/requests/unknown_binary_response_event_wrapper.h"
#include "zaber/motion/dto/requests/binary_reply_only_event_wrapper.h"

using namespace std::chrono_literals;

#define FUTURE_WAIT_TIMEOUT 100ms // NOLINT

namespace zaber { namespace motion {

TEST_CASE( "binary::Connection callbacks", "[event][connection]" ) {
    binary::Connection conn { 0 };

    SECTION( "Unknown response callback" ) {
        std::promise<binary::UnknownResponseEvent> unknownPromise;
        std::future<binary::UnknownResponseEvent> unknownFuture = unknownPromise.get_future();

        conn.setUnknownResponseCallback([&](const binary::UnknownResponseEvent& event){
            unknownPromise.set_value(event);
        });

        binary::UnknownResponseEvent event(1, 2, 3);
        requests::UnknownBinaryResponseEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::UNKNOWN_BINARY_RESPONSE_STR, eventWrapper);

        bool result = unknownFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        binary::UnknownResponseEvent resultEvent = unknownFuture.get();
        REQUIRE( resultEvent.deviceAddress == 1 ); // NOLINT
        REQUIRE( resultEvent.command == 2 ); // NOLINT
        REQUIRE( resultEvent.data == 3 ); // NOLINT
    }

    SECTION( "Reply only callback" ) {
        std::promise<binary::ReplyOnlyEvent> replyOnlyPromise;
        std::future<binary::ReplyOnlyEvent> replyOnlyFuture = replyOnlyPromise.get_future();

        conn.setReplyOnlyCallback([&](const binary::ReplyOnlyEvent& event){
            replyOnlyPromise.set_value(event);
        });

        binary::ReplyOnlyEvent event(1, 2, 3);
        requests::BinaryReplyOnlyEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::BINARY_REPLY_ONLY_STR, eventWrapper);

        bool result = replyOnlyFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        binary::ReplyOnlyEvent resultEvent = replyOnlyFuture.get();
        REQUIRE( resultEvent.deviceAddress == 1 ); // NOLINT
        REQUIRE( resultEvent.command == 2 ); // NOLINT
        REQUIRE( resultEvent.data == 3 ); // NOLINT
    }

    SECTION( "Disconnected callback" ) {
        std::promise<std::shared_ptr<exceptions::MotionLibException>> exceptionPromise;
        std::future<std::shared_ptr<exceptions::MotionLibException>> exceptionFuture = exceptionPromise.get_future();

        conn.setDisconnectedCallback([&](const std::shared_ptr<exceptions::MotionLibException>& exception){
            exceptionPromise.set_value(exception);
        });

        requests::DisconnectedEvent eventWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);

        bool result = exceptionFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        std::shared_ptr<exceptions::MotionLibException> resultException = exceptionFuture.get();
        REQUIRE( resultException->getMessage() == "mock_disconnected" ); // NOLINT
    }

    SECTION( "Move assignment in callback" ) {
        std::promise<bool> promise;
        std::future<bool> future = promise.get_future();
        binary::Connection conn1;

        conn.setReplyOnlyCallback([&](const binary::ReplyOnlyEvent&){
            conn1 = std::move(conn);
            promise.set_value(true);
        });

        binary::ReplyOnlyEvent event;
        requests::BinaryReplyOnlyEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::BINARY_REPLY_ONLY_STR, eventWrapper);

        bool result = future.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
        REQUIRE( conn1.getInterfaceId() == 0 ); // NOLINT
    }

    SECTION( "Reasssigning callback from within callback" ) {
        std::promise<bool> promise1;
        std::future<bool> future1 = promise1.get_future();
        std::promise<bool> promise2;
        std::future<bool> future2 = promise2.get_future();

        auto otherCallback = [&](const binary::ReplyOnlyEvent&){
            promise2.set_value(true);
        };

        conn.setReplyOnlyCallback([&](const binary::ReplyOnlyEvent&){
            conn.setReplyOnlyCallback(otherCallback);
            promise1.set_value(true);
        });

        binary::ReplyOnlyEvent event;
        requests::BinaryReplyOnlyEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::BINARY_REPLY_ONLY_STR, eventWrapper);
        sendMockEvent(SdkEvent::BINARY_REPLY_ONLY_STR, eventWrapper);

        bool result1 = future1.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool result2 = future2.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;

        REQUIRE( result1 ); // NOLINT
        REQUIRE( result2 ); // NOLINT
    }

    SECTION( "All callbacks invoked once" ) {
        std::atomic<int> unknownCalled = 0, replyOnlyCalled = 0, disconnectedCalled = 0;

        std::promise<bool> unknownInvoked;
        std::future<bool> unknownInvokedFuture = unknownInvoked.get_future();
        conn.setUnknownResponseCallback([&](const binary::UnknownResponseEvent&){
            unknownCalled++;
            unknownInvoked.set_value(true);
        });

        std::promise<bool> replyOnlyInvoked;
        std::future<bool> replyOnlyInvokedFuture = replyOnlyInvoked.get_future();
        conn.setReplyOnlyCallback([&](const binary::ReplyOnlyEvent&){
            replyOnlyCalled++;
            replyOnlyInvoked.set_value(true);
        });

        std::promise<bool> disconnectedInvoked;
        std::future<bool> disconnectedInvokedFuture = disconnectedInvoked.get_future();
        conn.setDisconnectedCallback([&](const std::shared_ptr<exceptions::MotionLibException>&){
            disconnectedCalled++;
            disconnectedInvoked.set_value(true);
        });

        binary::UnknownResponseEvent unknownResponse;
        requests::UnknownBinaryResponseEventWrapper unknownResponseWrapper(0, unknownResponse);
        sendMockEvent(SdkEvent::UNKNOWN_BINARY_RESPONSE_STR, unknownResponseWrapper);

        binary::ReplyOnlyEvent replyOnlyEvent;
        requests::BinaryReplyOnlyEventWrapper replyOnlyEventWrapper(0, replyOnlyEvent);
        sendMockEvent(SdkEvent::BINARY_REPLY_ONLY_STR, replyOnlyEventWrapper);

        requests::DisconnectedEvent disconnectedWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, disconnectedWrapper);

        bool unknownResult = unknownInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool replyOnlyResult = replyOnlyInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool disconnectedResult = disconnectedInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;

        REQUIRE( unknownResult ); // NOLINT
        REQUIRE( unknownCalled == 1 ); // NOLINT
        REQUIRE( replyOnlyResult ); // NOLINT
        REQUIRE( replyOnlyCalled == 1 ); // NOLINT
        REQUIRE( disconnectedResult ); // NOLINT
        REQUIRE( disconnectedCalled == 1 ); // NOLINT
    }
}

} // namespace motion
} // namespace zaber
