#include "catch.hpp"

#include <string>
#include <memory>

#include "utils.h"
#include "zaber/motion/library_exception.h"
#include "zaber/motion/gateway/sdk_event.h"
#include "zaber/motion/gateway/serializer.h"
#include "zaber/motion/dto/requests/gateway_event.h"
#include "zaber/motion/dto/requests/unknown_response_event_wrapper.h"
#include "zaber/motion/dto/requests/unknown_binary_response_event_wrapper.h"
#include "zaber/motion/dto/requests/binary_reply_only_event_wrapper.h"
#include "zaber/motion/dto/requests/disconnected_event.h"
#include "zaber/motion/dto/requests/alert_event_wrapper.h"

namespace zaber { namespace motion {

TEST_CASE ("SdkEvent -- Constructor test", "[event][sdk_event]") {
    SECTION( "Unpack unknown response event" ) {
        ascii::UnknownResponseEvent event;
        requests::UnknownResponseEventWrapper wrapper(1, event);

        SdkEvent sdkEvent(serializeEvent(SdkEvent::UNKNOWN_RESPONSE_STR, wrapper));
        auto ptr = std::dynamic_pointer_cast<requests::UnknownResponseEventWrapper>(sdkEvent.getEventData());

        REQUIRE( ptr != nullptr ); // NOLINT
        REQUIRE( sdkEvent.getEventType() == SdkEvent::EventType::UNKNOWN_RESPONSE ); // NOLINT
        REQUIRE( sdkEvent.getInterfaceId() == 1 ); // NOLINT
    }

    SECTION( "Unpack binary unknown response event") {
        requests::UnknownBinaryResponseEventWrapper wrapper(1, binary::UnknownResponseEvent());
        SdkEvent sdkEvent(serializeEvent(SdkEvent::UNKNOWN_BINARY_RESPONSE_STR, wrapper));

        auto ptr = std::dynamic_pointer_cast<requests::UnknownBinaryResponseEventWrapper>(sdkEvent.getEventData());

        REQUIRE( ptr != nullptr ); // NOLINT
        REQUIRE( sdkEvent.getEventType() == SdkEvent::EventType::UNKNOWN_BINARY_RESPONSE ); // NOLINT
        REQUIRE( sdkEvent.getInterfaceId() == 1 ); // NOLINT
    }

    SECTION( "Unpack alert event") {
        requests::AlertEventWrapper wrapper(1, ascii::AlertEvent());
        SdkEvent sdkEvent(serializeEvent(SdkEvent::ALERT_STR, wrapper));

        auto ptr = std::dynamic_pointer_cast<requests::AlertEventWrapper>(sdkEvent.getEventData());

        REQUIRE( ptr != nullptr ); // NOLINT
        REQUIRE( sdkEvent.getEventType() == SdkEvent::EventType::ALERT ); // NOLINT
        REQUIRE( sdkEvent.getInterfaceId() == 1 ); // NOLINT
    }

    SECTION( "Unpack binary reply only event") {
        requests::BinaryReplyOnlyEventWrapper wrapper(1, binary::ReplyOnlyEvent());
        SdkEvent sdkEvent(serializeEvent(SdkEvent::BINARY_REPLY_ONLY_STR, wrapper));

        auto ptr = std::dynamic_pointer_cast<requests::BinaryReplyOnlyEventWrapper>(sdkEvent.getEventData());

        REQUIRE( ptr != nullptr ); // NOLINT
        REQUIRE( sdkEvent.getEventType() == SdkEvent::EventType::BINARY_REPLY_ONLY ); // NOLINT
        REQUIRE( sdkEvent.getInterfaceId() == 1 ); // NOLINT
    }

    SECTION( "Unpack disconnected event" ) {
        requests::DisconnectedEvent disconnected;
        disconnected.interfaceId = 1;
        SdkEvent sdkEvent(serializeEvent(SdkEvent::DISCONNECTED_STR, disconnected));
        auto ptr = std::dynamic_pointer_cast<requests::DisconnectedEvent>(sdkEvent.getEventData());

        REQUIRE( ptr != nullptr ); // NOLINT
        REQUIRE( sdkEvent.getEventType() == SdkEvent::EventType::DISCONNECTED ); // NOLINT
        REQUIRE( sdkEvent.getInterfaceId() == 1 ); // NOLINT
    }

    SECTION( "Throws on unknown event" ) {
        ascii::UnknownResponseEvent event;
        requests::UnknownResponseEventWrapper wrapper(1, event);

        try {
            SdkEvent sdkEvent(serializeEvent("blarg", wrapper));
            REQUIRE( false ); // NOLINT
        } catch (const LibraryIntegrationException& e) {
            REQUIRE( e.message() == "Unknown event" ); // NOLINT
        }
    }
}

}  // namespace motion
}  // namespace zaber
