#pragma once

#include <string>
#include <vector>

#include "zaber/motion/gateway/event_handler.h"
#include "zaber/motion/gateway/serializer.h"
#include "zaber/motion/gateway/sdk_event.h"
#include "zaber/motion/dto/serializable.h"
#include "zaber/motion/dto/requests/gateway_event.h"

inline std::vector<std::string> serializeEvent(std::string_view eventType, const zaber::motion::Serializable& wrapper) {
    zaber::motion::requests::GatewayEvent gatewayEvent = zaber::motion::requests::GatewayEvent(std::string(eventType));
    std::string serialized = zaber::motion::Serializer::serialize({ &gatewayEvent, &wrapper });
    return zaber::motion::Serializer::deserialize(serialized);
}

