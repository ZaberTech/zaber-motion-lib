#include "catch.hpp"

#include <future>
#include <chrono>
#include <thread>
#include <atomic>

#include "utils.h"
#include "zaber_motion_core_dummy.h"
#include "zaber/motion/ascii/connection.h"
#include "zaber/motion/dto/requests/errors.h"
#include "zaber/motion/dto/requests/disconnected_event.h"

using namespace std::chrono_literals;

// NOLINTBEGIN(readability-magic-numbers)

#define FUTURE_WAIT_TIMEOUT 100ms // NOLINT

namespace zaber { namespace motion {

TEST_CASE( "ascii::Connection callbacks", "[event][connection]" ) {
    ascii::Connection conn { 0 };

    SECTION( "Alert callback" ) {
        std::promise<ascii::AlertEvent> alertPromise;
        std::future<ascii::AlertEvent> alertFuture = alertPromise.get_future();

        conn.setAlertCallback([&](const ascii::AlertEvent& event){
            alertPromise.set_value(event);
        });

        ascii::AlertEvent event(3, 5, "holy moly", "the flag", "wow!");
        requests::AlertEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::ALERT_STR, eventWrapper);

        bool result = alertFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        ascii::AlertEvent resultEvent = alertFuture.get();
        REQUIRE( resultEvent.deviceAddress == 3 ); // NOLINT
        REQUIRE( resultEvent.axisNumber == 5 ); // NOLINT
        REQUIRE( resultEvent.status == "holy moly" ); // NOLINT
        REQUIRE( resultEvent.warningFlag == "the flag" ); // NOLINT
        REQUIRE( resultEvent.data == "wow!" ); // NOLINT
    }

    SECTION( "Unknown response callback" ) {
        std::promise<ascii::UnknownResponseEvent> unknownPromise;
        std::future<ascii::UnknownResponseEvent> unknownFuture = unknownPromise.get_future();

        conn.setUnknownResponseCallback([&](const ascii::UnknownResponseEvent& event){
            unknownPromise.set_value(event);
        });

        ascii::UnknownResponseEvent event(3, 5, "flag1", "the status", "flag2", "the data", ascii::MessageType::REPLY);
        requests::UnknownResponseEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);

        bool result = unknownFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        ascii::UnknownResponseEvent resultEvent = unknownFuture.get();
        REQUIRE( resultEvent.deviceAddress == 3 ); // NOLINT
        REQUIRE( resultEvent.axisNumber == 5 ); // NOLINT
        REQUIRE( resultEvent.replyFlag == "flag1" ); // NOLINT
        REQUIRE( resultEvent.status == "the status" ); // NOLINT
        REQUIRE( resultEvent.warningFlag == "flag2" ); // NOLINT
        REQUIRE( resultEvent.data == "the data" ); // NOLINT
        REQUIRE( resultEvent.messageType == ascii::MessageType::REPLY ); // NOLINT
    }

    SECTION( "Disconnected callback" ) {
        std::promise<std::shared_ptr<exceptions::MotionLibException>> disconnectedPromise;
        std::future<std::shared_ptr<exceptions::MotionLibException>> disconnectedFuture = disconnectedPromise.get_future();

        conn.setDisconnectedCallback([&](const std::shared_ptr<exceptions::MotionLibException>& exception){
            disconnectedPromise.set_value(exception);
        });

        requests::DisconnectedEvent eventWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);

        bool result = disconnectedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT

        std::shared_ptr<exceptions::MotionLibException> resultException = disconnectedFuture.get();
        REQUIRE( resultException->getMessage() == "mock_disconnected" ); // NOLINT
    }

    SECTION( "Move assignment in callback" ) {
        std::promise<bool> promise;
        std::future<bool> future = promise.get_future();
        ascii::Connection conn1;

        conn.setAlertCallback([&](const ascii::AlertEvent&){
            conn1 = std::move(conn);
            promise.set_value(true);
        });

        ascii::AlertEvent event;
        requests::AlertEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::ALERT_STR, eventWrapper);

        bool result = future.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
        REQUIRE( conn1.getInterfaceId() == 0 ); // NOLINT
    }

    SECTION( "Reasssigning callback from within callback" ) {
        std::promise<bool> promise1;
        std::future<bool> future1 = promise1.get_future();
        std::promise<bool> promise2;
        std::future<bool> future2 = promise2.get_future();

        auto otherCallback = [&](const ascii::AlertEvent&){
            promise2.set_value(true);
        };

        conn.setAlertCallback([&](const ascii::AlertEvent&){
            conn.setAlertCallback(otherCallback);
            promise1.set_value(true);
        });

        ascii::AlertEvent event;
        requests::AlertEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::ALERT_STR, eventWrapper);
        sendMockEvent(SdkEvent::ALERT_STR, eventWrapper);

        bool result1 = future1.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool result2 = future2.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;

        REQUIRE( result1 ); // NOLINT
        REQUIRE( result2 ); // NOLINT
    }

    SECTION( "All callbacks invoked once" ) {
        std::atomic<int> alertCalled = 0, unknownCalled = 0, disconnectedCalled = 0;

        std::promise<bool> alertInvoked;
        std::future<bool> alertInvokedFuture = alertInvoked.get_future();
        conn.setAlertCallback([&](const ascii::AlertEvent&){
            alertCalled++;
            alertInvoked.set_value(true);
        });

        std::promise<bool> unknownInvoked;
        std::future<bool> unknownInvokedFuture = unknownInvoked.get_future();
        conn.setUnknownResponseCallback([&](const ascii::UnknownResponseEvent&){
            unknownCalled++;
            unknownInvoked.set_value(true);
        });

        std::promise<bool> disconnectedInvoked;
        std::future<bool> disconnectedInvokedFuture = disconnectedInvoked.get_future();
        conn.setDisconnectedCallback([&](const std::shared_ptr<exceptions::MotionLibException>&){
            disconnectedCalled++;
            disconnectedInvoked.set_value(true);
        });

        ascii::AlertEvent alert;
        requests::AlertEventWrapper alertWrapper(0, alert);
        sendMockEvent(SdkEvent::ALERT_STR, alertWrapper);

        ascii::UnknownResponseEvent unknownResponse;
        requests::UnknownResponseEventWrapper unknownResponseWrapper(0, unknownResponse);
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, unknownResponseWrapper);

        requests::DisconnectedEvent disconnectedWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, disconnectedWrapper);

        bool alertResult = alertInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool unknownResult = unknownInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        bool disconnectedResult = disconnectedInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;

        REQUIRE( alertResult ); // NOLINT
        REQUIRE( alertCalled == 1 ); // NOLINT
        REQUIRE( unknownResult ); // NOLINT
        REQUIRE( unknownCalled == 1 ); // NOLINT
        REQUIRE( disconnectedResult ); // NOLINT
        REQUIRE( disconnectedCalled == 1 ); // NOLINT
    }
}

} // namespace motion
} // namespace zaber

// NOLINTEND(readability-magic-numbers)
