#include <future>
#include <chrono>
#include <atomic>
#include <thread>

#include "catch.hpp"

#include "utils.h"
#include "zaber_motion_core_dummy.h"
#include "zaber/motion/gateway/event_manager.h"
#include "zaber/motion/dto/requests/errors.h"
#include "zaber/motion/dto/requests/unknown_response_event_wrapper.h"
#include "zaber/motion/dto/requests/disconnected_event.h"
#include "zaber/motion/dto/requests/alert_event_wrapper.h"

using namespace std::chrono_literals;

#define FUTURE_WAIT_TIMEOUT 100ms // NOLINT

namespace zaber { namespace motion {

TEST_CASE( "EventManager and EventHandler Integration", "[event][manager]") {
    std::promise<bool> callbackInvoked;
    std::future<bool> callbackInvokedFuture = callbackInvoked.get_future();

    SECTION( "Invokes callback associated with interfaceId" ) {
        auto callback = [&callbackInvoked](const int&) { callbackInvoked.set_value(true); };
        auto filter = [](const SdkEvent& event) { return event.getInterfaceId() == 0; };
        auto mutator = [](const SdkEvent&) { return 0; };
        std::shared_ptr<EventHandler<int>> handler = EventManager::getSingleton().createEventHandler<int>(callback, filter, mutator);

        ascii::UnknownResponseEvent event;
        requests::UnknownResponseEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);

        bool result = callbackInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
    }

    SECTION( "Does not invoke callback if filter returns false" ) {
        auto callback = [&callbackInvoked](const int&) { callbackInvoked.set_value(true); };
        auto filter = [&](const SdkEvent& event) { return event.getInterfaceId() == 1; };
        auto mutator = [](const SdkEvent&) { return 0; };
        std::shared_ptr<EventHandler<int>> handler1 = EventManager::getSingleton().createEventHandler<int>(callback, filter, mutator);

        ascii::UnknownResponseEvent event;
        requests::UnknownResponseEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);

        eventWrapper.interfaceId = 1;
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);

        bool result = callbackInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
    }

    SECTION( "Does not call a destroyed event handler" ) {
        std::atomic<int> called = 0;
        {
            auto callback = [&](const int&) { called++; };
            auto filter = [](const SdkEvent&) { return true; };
            auto mutator = [](const SdkEvent&) { return 0; };
            std::shared_ptr<EventHandler<int>> handler1 = EventManager::getSingleton().createEventHandler<int>(callback, filter, mutator);
            // shared_ptr destructor decrements ref count
        }
        auto callback = [&callbackInvoked](const int&) { callbackInvoked.set_value(true); };
        auto filter = [](const SdkEvent&) { return true; };
        auto mutator = [](const SdkEvent&) { return 0; };
        std::shared_ptr<EventHandler<int>> handler2 = EventManager::getSingleton().createEventHandler<int>(callback, filter, mutator);

        ascii::UnknownResponseEvent event;
        requests::UnknownResponseEventWrapper eventWrapper(0, event);
        sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);

        bool result = callbackInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
        REQUIRE( called == 0 ); // NOLINT
    }

    SECTION( "Does not fail if user unsubscribes from events in callback" ) {
        auto filter = [](const SdkEvent& event) { return event.getInterfaceId() == 0; };
        auto mutator = [](const SdkEvent&) { return 0; };
        std::shared_ptr<EventHandler<int>> handler = EventManager::getSingleton().createEventHandler<int>(nullptr, filter, mutator);
        handler->setCallback([&](const int&) { handler->setCallback(nullptr); callbackInvoked.set_value(true); });

        requests::DisconnectedEvent eventWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);

        bool result = callbackInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
    }

    SECTION( "Allows for mutable function callbacks" ) {
        auto filter = [](const SdkEvent& event) { return event.getInterfaceId() == 0; };
        auto mutator = [](const SdkEvent&) { return 0; };
        std::shared_ptr<EventHandler<int>> handler = EventManager::getSingleton().createEventHandler<int>(nullptr, filter, mutator);

        int timesCalled = 0;
        handler->setCallback([&, counter = 0](const int&) mutable {
            timesCalled = ++counter;
            if (counter == 3) {
                callbackInvoked.set_value(true);
            }
        });

        requests::DisconnectedEvent eventWrapper(0, requests::Errors::REQUEST_TIMEOUT, "mock_disconnected");
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);
        sendMockEvent(SdkEvent::DISCONNECTED_STR, eventWrapper);

        bool result = callbackInvokedFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
        REQUIRE( result ); // NOLINT
    }
}

TEST_CASE( "Event Manager -- Allows callbacks to be set while handling events", "[event][manager]" ) {
    const int numEvents = 1000;

    // set up handler 1
    std::promise<bool> eventsHandled1;
    std::future<bool> eventsHandledFuture1 = eventsHandled1.get_future();
    int eventsHandledCount1_1 = 0;
    int eventsHandledCount1_2 = 0;

    auto callback1_1 = [&](int) {
        eventsHandledCount1_1++;
        if (eventsHandledCount1_1 + eventsHandledCount1_2 == numEvents) {
            eventsHandled1.set_value(true);
        }
    };
    auto callback1_2 = [&](int) {
        eventsHandledCount1_2++;
        if (eventsHandledCount1_1 + eventsHandledCount1_2 == numEvents) {
            eventsHandled1.set_value(true);
        }
    };

    auto filter1 = [](const SdkEvent& event) { return event.getEventType() == SdkEvent::EventType::ALERT; };
    auto mutator1 = [](const SdkEvent&) { return 0; };
    std::shared_ptr<EventHandler<int>> handler1 = EventManager::getSingleton().createEventHandler<int>(callback1_1, filter1, mutator1);

    // set up handler 2
    std::promise<bool> eventsHandled2;
    std::future<bool> eventsHandledFuture2 = eventsHandled2.get_future();

    int eventsHandledCount2_1 = 0;
    int eventsHandledCount2_2 = 0;
    auto callback2_1 = [&](int) {
        eventsHandledCount2_1++;
        if (eventsHandledCount2_1 + eventsHandledCount2_2 == numEvents) {
            eventsHandled2.set_value(true);
        }
    };
    auto callback2_2 = [&](int) {
        eventsHandledCount2_2++;
        if (eventsHandledCount2_1 + eventsHandledCount2_2 == numEvents) {
            eventsHandled2.set_value(true);
        }
    };

    auto filter2 = [](const SdkEvent& event) { return event.getEventType() == SdkEvent::EventType::UNKNOWN_RESPONSE; };
    auto mutator2 = [](const SdkEvent&) { return 0; };
    std::shared_ptr<EventHandler<int>> handler2 = EventManager::getSingleton().createEventHandler<int>(callback2_1, filter2, mutator2);

    std::atomic<bool> start = false;
    std::thread alertEventThread([=, &start]() {
        while (!start) { std::this_thread::yield(); }
        for (int i = 0; i < numEvents; i++) {
            ascii::AlertEvent event;
            requests::AlertEventWrapper eventWrapper(1, event);
            sendMockEvent(SdkEvent::ALERT_STR, eventWrapper);
        }
    });
    std::thread unknownResponseEventThread([=, &start]() {
        while (!start) { std::this_thread::yield(); }
        for (int i = 0; i < numEvents; i++) {
            ascii::UnknownResponseEvent event;
            requests::UnknownResponseEventWrapper eventWrapper(1, event);
            sendMockEvent(SdkEvent::UNKNOWN_RESPONSE_STR, eventWrapper);
        }
    });

    for (int i = 0; i < numEvents * 100; i++) {
        if (i == 0) {
            start = true;
        }

        handler1->setCallback(callback1_1);
        handler2->setCallback(callback2_1);
        handler1->setCallback(callback1_2);
        handler2->setCallback(callback2_2);
    }

    alertEventThread.join();
    unknownResponseEventThread.join();

    bool result1 = eventsHandledFuture1.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
    bool result2 = eventsHandledFuture2.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;

    REQUIRE( result1 ); // NOLINT
    REQUIRE( result2 ); // NOLINT
}

}  // Namespace motion
}  // Namespace zaber
