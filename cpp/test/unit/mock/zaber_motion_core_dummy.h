#pragma once

#include <cstdint>
#include <string>

#include "zaber/motion/dto/serializable.h"

typedef uint8_t GoUint8;
typedef int32_t GoInt32;
typedef int64_t GoInt64;


GoInt32 zml_call(void* request, GoInt64 tag, void* callback, GoUint8 async);
void zml_setEventHandler(int64_t tag, void* callback);
void sendMockEvent(std::string_view event_type, zaber::motion::Serializable& event);
