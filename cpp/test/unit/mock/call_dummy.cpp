#include <cstdint>
#include <string>
#include <vector>
#include <functional>

#include "zaber_motion_core_dummy.h"
#include "zaber/motion/gateway/serializer.h"
#include "zaber/motion/dto/serializable.h"
#include "zaber/motion/dto/requests/gateway_event.h"
#include "zaber/motion/dto/requests/gateway_response.h"
#include "zaber/motion/dto/requests/response_type.h"

std::function<void(void*,int64_t)> mockCallback; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

GoInt32 zml_call(void* request, GoInt64 tag, void* callback, GoUint8 async) {
    (void)request; (void)async;
    zaber::motion::requests::GatewayResponse gatewayResponse;
    gatewayResponse.response = zaber::motion::requests::ResponseType::OK;
    std::string buf = zaber::motion::Serializer::serialize({ &gatewayResponse });
    reinterpret_cast<void(*)(void*,int64_t)>(callback)(reinterpret_cast<void*>(buf.data()), static_cast<int64_t>(tag)); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
    return 0;
}

void zml_setEventHandler(int64_t tag, void* callback) {
    (void)tag;
    mockCallback = reinterpret_cast<void(*)(void*,int64_t)>(callback); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
}

void sendMockEvent(std::string_view event_type, zaber::motion::Serializable& event) {
    zaber::motion::requests::GatewayEvent gatewayEvent = zaber::motion::requests::GatewayEvent(std::string(event_type));
    std::string buf = zaber::motion::Serializer::serialize({ &gatewayEvent, &event });
    mockCallback(reinterpret_cast<void*>(buf.data()), 0); // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
}

