#include <future>
#include <chrono>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "zaber/motion/ascii/connection.h"
#include "zaber/motion/exceptions/request_timeout_exception.h"
#include "zaber/motion/exceptions/invalid_packet_exception.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/gateway/event_manager.h"
#include "zaber/motion/gateway/event_handler.h"
#include "zaber/motion/dto/requests/test_event.h"
#include "zaber/motion/dto/requests/test_request.h"
#include "zaber/motion/dto/requests/test_response.h"
#include "zaber/motion/dto/requests/test_response_long.h"

using namespace std::chrono_literals;

#define FUTURE_WAIT_TIMEOUT 100ms // NOLINT

namespace zaber { namespace motion {

TEST_CASE( "Synchronous Call Succeeds", "[integration]") {
    requests::TestRequest testRequest;
    testRequest.setReturnError(false);
    testRequest.setDataPing("Hello");

    requests::TestResponse syncResponse;
    callGateway("test/request", testRequest, &syncResponse);

    REQUIRE( syncResponse.getDataPong() == "Hello..." );  // NOLINT
}

TEST_CASE( "Synchronous Call Results in Error", "[integration][error_handling]") {
    requests::TestRequest testRequest;
    testRequest.setReturnError(true);
    testRequest.setDataPing("HelloSyncFail!");

    requests::TestResponse syncResponse;
    try {
        callGateway("test/request", testRequest, &syncResponse);
    } catch(const exceptions::RequestTimeoutException& err) {
        REQUIRE( err.getMessage() == "Device has not responded in given timeout" );  // NOLINT
    } catch( ... ) {
        // Fail the test if error type is incorrect
        REQUIRE( false );  // NOLINT
    }
}

TEST_CASE( "Synchronous Call Results in Error with Custom Data", "[integration][error_handling]") {
    requests::TestRequest testRequest;
    testRequest.setReturnErrorWithData(true);

    requests::TestResponse testResponse;
    try {
        callGateway("test/request", testRequest, &testResponse);
    } catch(const exceptions::InvalidPacketException& err) {
        std::string expectedPacket = "123";
        std::string expectedReason = "For test";
        std::string expectedMessage = "Cannot parse incoming packet: \"" + expectedPacket + "\" (" + expectedReason + ")";
        REQUIRE(err.getMessage() == expectedMessage);  // NOLINT
        REQUIRE(err.getDetails().getPacket() == expectedPacket);  // NOLINT
        REQUIRE(err.getDetails().getReason() == expectedReason);  // NOLINT
    } catch ( ... ) {
        // Fail the test if error type is incorrect
        REQUIRE( false );  // NOLINT
    }
}

TEST_CASE( "Synchronous Call Succeeds With Long Response", "[integration]") {
    requests::TestRequest testRequest;
    testRequest.setReturnError(false);
    testRequest.setDataPing("Hello");

    requests::TestResponseLong syncResponse;
    callGateway("test/request_long", testRequest, &syncResponse);

    for (int i = 0; i < static_cast<int>(syncResponse.getDataPong().size()); i++) {
        std::ostringstream out;
        out << "Hello..." << i;
        REQUIRE( syncResponse.getDataPong()[i] == out.str() );  // NOLINT
    }
}

TEST_CASE( "Event emission", "[integration]") {
    std::promise<requests::TestEvent> eventPromise;
    std::shared_future<requests::TestEvent> eventFuture = eventPromise.get_future();
    int called = 0;
    auto callback = [&](const requests::TestEvent& e){ called++; eventPromise.set_value(e); };
    auto filter = [](const SdkEvent&) { return true; };
    auto mutator = [](const SdkEvent& event) {
        return *std::static_pointer_cast<requests::TestEvent>(event.getEventData());
    };

    std::shared_ptr<EventHandler<requests::TestEvent>> eventHandler =
        EventManager::getSingleton().createEventHandler<requests::TestEvent>(callback, filter, mutator);

    callGateway("test/emit_event");

    bool result = eventFuture.wait_for(FUTURE_WAIT_TIMEOUT) == std::future_status::ready;
    REQUIRE( result );  // NOLINT
    const requests::TestEvent& testEvent = eventFuture.get();
    REQUIRE( testEvent.getData() == "testing event data" );  // NOLINT
    REQUIRE( called == 1 );  // NOLINT
}

}  // namespace motion
}  // namespace zaber
