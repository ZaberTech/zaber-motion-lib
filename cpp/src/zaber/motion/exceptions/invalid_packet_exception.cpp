﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_packet_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidPacketException::InvalidPacketException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    InvalidPacketExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

InvalidPacketException::InvalidPacketException(const std::string& message, const InvalidPacketExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for InvalidPacketException
 */
InvalidPacketExceptionData const & InvalidPacketException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this InvalidPacketException
 */
std::string InvalidPacketException::toString() {
    return std::string("InvalidPacketException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
