﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "io_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

IoFailedException::IoFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this IoFailedException
 */
std::string IoFailedException::toString() {
    return std::string("IoFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
