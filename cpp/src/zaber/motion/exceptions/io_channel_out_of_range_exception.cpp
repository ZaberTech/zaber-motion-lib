﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "io_channel_out_of_range_exception.h"

namespace zaber { namespace motion { namespace exceptions {

IoChannelOutOfRangeException::IoChannelOutOfRangeException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this IoChannelOutOfRangeException
 */
std::string IoChannelOutOfRangeException::toString() {
    return std::string("IoChannelOutOfRangeException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
