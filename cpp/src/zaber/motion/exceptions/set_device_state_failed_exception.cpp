﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "set_device_state_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

SetDeviceStateFailedException::SetDeviceStateFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    SetDeviceStateExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

SetDeviceStateFailedException::SetDeviceStateFailedException(const std::string& message, const SetDeviceStateExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for SetDeviceStateFailedException
 */
SetDeviceStateExceptionData const & SetDeviceStateFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this SetDeviceStateFailedException
 */
std::string SetDeviceStateFailedException::toString() {
    return std::string("SetDeviceStateFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
