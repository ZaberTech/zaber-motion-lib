﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "command_too_long_exception.h"

namespace zaber { namespace motion { namespace exceptions {

CommandTooLongException::CommandTooLongException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    CommandTooLongExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

CommandTooLongException::CommandTooLongException(const std::string& message, const CommandTooLongExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for CommandTooLongException
 */
CommandTooLongExceptionData const & CommandTooLongException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this CommandTooLongException
 */
std::string CommandTooLongException::toString() {
    return std::string("CommandTooLongException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
