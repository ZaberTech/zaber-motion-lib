﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "setting_not_found_exception.h"

namespace zaber { namespace motion { namespace exceptions {

SettingNotFoundException::SettingNotFoundException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this SettingNotFoundException
 */
std::string SettingNotFoundException::toString() {
    return std::string("SettingNotFoundException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
