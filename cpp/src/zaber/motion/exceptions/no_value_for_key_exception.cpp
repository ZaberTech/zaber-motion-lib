﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "no_value_for_key_exception.h"

namespace zaber { namespace motion { namespace exceptions {

NoValueForKeyException::NoValueForKeyException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this NoValueForKeyException
 */
std::string NoValueForKeyException::toString() {
    return std::string("NoValueForKeyException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
