﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_busy_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceBusyException::DeviceBusyException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this DeviceBusyException
 */
std::string DeviceBusyException::toString() {
    return std::string("DeviceBusyException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
