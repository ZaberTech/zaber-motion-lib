﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "set_peripheral_state_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

SetPeripheralStateFailedException::SetPeripheralStateFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    SetPeripheralStateExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

SetPeripheralStateFailedException::SetPeripheralStateFailedException(const std::string& message, const SetPeripheralStateExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for SetPeripheralStateFailedException
 */
SetPeripheralStateExceptionData const & SetPeripheralStateFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this SetPeripheralStateFailedException
 */
std::string SetPeripheralStateFailedException::toString() {
    return std::string("SetPeripheralStateFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
