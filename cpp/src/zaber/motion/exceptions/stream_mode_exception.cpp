﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_mode_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamModeException::StreamModeException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this StreamModeException
 */
std::string StreamModeException::toString() {
    return std::string("StreamModeException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
