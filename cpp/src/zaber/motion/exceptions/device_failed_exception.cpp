﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceFailedException::DeviceFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this DeviceFailedException
 */
std::string DeviceFailedException::toString() {
    return std::string("DeviceFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
