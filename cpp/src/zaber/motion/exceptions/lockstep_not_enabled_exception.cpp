﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "lockstep_not_enabled_exception.h"

namespace zaber { namespace motion { namespace exceptions {

LockstepNotEnabledException::LockstepNotEnabledException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this LockstepNotEnabledException
 */
std::string LockstepNotEnabledException::toString() {
    return std::string("LockstepNotEnabledException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
