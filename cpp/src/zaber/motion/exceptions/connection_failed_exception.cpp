﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "connection_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

ConnectionFailedException::ConnectionFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this ConnectionFailedException
 */
std::string ConnectionFailedException::toString() {
    return std::string("ConnectionFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
