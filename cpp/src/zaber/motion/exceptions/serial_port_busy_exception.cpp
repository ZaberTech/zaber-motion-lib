﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "serial_port_busy_exception.h"

namespace zaber { namespace motion { namespace exceptions {

SerialPortBusyException::SerialPortBusyException(const std::string& message): ConnectionFailedException(message) {
}

/**
 * Get a string representation of this SerialPortBusyException
 */
std::string SerialPortBusyException::toString() {
    return std::string("SerialPortBusyException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
