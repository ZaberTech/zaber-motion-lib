﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_response_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidResponseException::InvalidResponseException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    InvalidResponseExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

InvalidResponseException::InvalidResponseException(const std::string& message, const InvalidResponseExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for InvalidResponseException
 */
InvalidResponseExceptionData const & InvalidResponseException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this InvalidResponseException
 */
std::string InvalidResponseException::toString() {
    return std::string("InvalidResponseException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
