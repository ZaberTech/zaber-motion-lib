﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "movement_interrupted_exception.h"

namespace zaber { namespace motion { namespace exceptions {

MovementInterruptedException::MovementInterruptedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    MovementInterruptedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

MovementInterruptedException::MovementInterruptedException(const std::string& message, const MovementInterruptedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for MovementInterruptedException
 */
MovementInterruptedExceptionData const & MovementInterruptedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this MovementInterruptedException
 */
std::string MovementInterruptedException::toString() {
    return std::string("MovementInterruptedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
