﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_setup_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtSetupFailedException::PvtSetupFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this PvtSetupFailedException
 */
std::string PvtSetupFailedException::toString() {
    return std::string("PvtSetupFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
