﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "operation_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

OperationFailedException::OperationFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    OperationFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

OperationFailedException::OperationFailedException(const std::string& message, const OperationFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for OperationFailedException
 */
OperationFailedExceptionData const & OperationFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this OperationFailedException
 */
std::string OperationFailedException::toString() {
    return std::string("OperationFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
