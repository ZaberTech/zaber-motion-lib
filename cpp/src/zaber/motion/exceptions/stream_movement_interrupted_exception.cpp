﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_movement_interrupted_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamMovementInterruptedException::StreamMovementInterruptedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    StreamMovementInterruptedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

StreamMovementInterruptedException::StreamMovementInterruptedException(const std::string& message, const StreamMovementInterruptedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for StreamMovementInterruptedException
 */
StreamMovementInterruptedExceptionData const & StreamMovementInterruptedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this StreamMovementInterruptedException
 */
std::string StreamMovementInterruptedException::toString() {
    return std::string("StreamMovementInterruptedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
