#include "motion_lib_exception.h"
#include <stdexcept>

namespace zaber { namespace motion { namespace exceptions {

MotionLibException::MotionLibException(const std::string& message): std::runtime_error(message) {
}

/**
 * Description of the error that has occured
**/
std::string MotionLibException::getMessage() const {
    return this->what();
}

std::string MotionLibException::toString() {
    return std::string("MotionLibException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
