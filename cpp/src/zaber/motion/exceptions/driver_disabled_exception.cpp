﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "driver_disabled_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DriverDisabledException::DriverDisabledException(const std::string& message, const std::string &customData): CommandFailedException(message, customData) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
}

DriverDisabledException::DriverDisabledException(const std::string& message, const CommandFailedExceptionData &&customData): CommandFailedException(message, std::move(customData)) {
}

/**
 * Get a string representation of this DriverDisabledException
 */
std::string DriverDisabledException::toString() {
    return std::string("DriverDisabledException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
