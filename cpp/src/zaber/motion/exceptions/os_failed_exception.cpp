﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "os_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

OsFailedException::OsFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this OsFailedException
 */
std::string OsFailedException::toString() {
    return std::string("OsFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
