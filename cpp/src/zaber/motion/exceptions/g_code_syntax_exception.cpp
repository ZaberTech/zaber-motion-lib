﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "g_code_syntax_exception.h"

namespace zaber { namespace motion { namespace exceptions {

GCodeSyntaxException::GCodeSyntaxException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    GCodeSyntaxExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

GCodeSyntaxException::GCodeSyntaxException(const std::string& message, const GCodeSyntaxExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for GCodeSyntaxException
 */
GCodeSyntaxExceptionData const & GCodeSyntaxException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this GCodeSyntaxException
 */
std::string GCodeSyntaxException::toString() {
    return std::string("GCodeSyntaxException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
