﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "bad_data_exception.h"

namespace zaber { namespace motion { namespace exceptions {

BadDataException::BadDataException(const std::string& message, const std::string &customData): CommandFailedException(message, customData) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
}

BadDataException::BadDataException(const std::string& message, const CommandFailedExceptionData &&customData): CommandFailedException(message, std::move(customData)) {
}

/**
 * Get a string representation of this BadDataException
 */
std::string BadDataException::toString() {
    return std::string("BadDataException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
