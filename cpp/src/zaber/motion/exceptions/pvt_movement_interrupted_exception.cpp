﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_movement_interrupted_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtMovementInterruptedException::PvtMovementInterruptedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    PvtMovementInterruptedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

PvtMovementInterruptedException::PvtMovementInterruptedException(const std::string& message, const PvtMovementInterruptedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for PvtMovementInterruptedException
 */
PvtMovementInterruptedExceptionData const & PvtMovementInterruptedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this PvtMovementInterruptedException
 */
std::string PvtMovementInterruptedException::toString() {
    return std::string("PvtMovementInterruptedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
