﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_request_data_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidRequestDataException::InvalidRequestDataException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InvalidRequestDataException
 */
std::string InvalidRequestDataException::toString() {
    return std::string("InvalidRequestDataException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
