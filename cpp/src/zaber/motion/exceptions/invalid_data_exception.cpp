﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_data_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidDataException::InvalidDataException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InvalidDataException
 */
std::string InvalidDataException::toString() {
    return std::string("InvalidDataException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
