﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "internal_error_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InternalErrorException::InternalErrorException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InternalErrorException
 */
std::string InternalErrorException::toString() {
    return std::string("InternalErrorException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
