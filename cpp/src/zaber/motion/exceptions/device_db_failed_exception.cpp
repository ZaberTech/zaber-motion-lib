﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_db_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceDbFailedException::DeviceDbFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    DeviceDbFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

DeviceDbFailedException::DeviceDbFailedException(const std::string& message, const DeviceDbFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for DeviceDbFailedException
 */
DeviceDbFailedExceptionData const & DeviceDbFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this DeviceDbFailedException
 */
std::string DeviceDbFailedException::toString() {
    return std::string("DeviceDbFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
