﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_operation_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidOperationException::InvalidOperationException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InvalidOperationException
 */
std::string InvalidOperationException::toString() {
    return std::string("InvalidOperationException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
