﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_argument_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidArgumentException::InvalidArgumentException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InvalidArgumentException
 */
std::string InvalidArgumentException::toString() {
    return std::string("InvalidArgumentException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
