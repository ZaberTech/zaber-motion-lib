﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "conversion_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

ConversionFailedException::ConversionFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this ConversionFailedException
 */
std::string ConversionFailedException::toString() {
    return std::string("ConversionFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
