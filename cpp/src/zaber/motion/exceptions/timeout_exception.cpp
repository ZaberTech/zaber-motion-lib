﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "timeout_exception.h"

namespace zaber { namespace motion { namespace exceptions {

TimeoutException::TimeoutException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this TimeoutException
 */
std::string TimeoutException::toString() {
    return std::string("TimeoutException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
