﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_setup_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamSetupFailedException::StreamSetupFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this StreamSetupFailedException
 */
std::string StreamSetupFailedException::toString() {
    return std::string("StreamSetupFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
