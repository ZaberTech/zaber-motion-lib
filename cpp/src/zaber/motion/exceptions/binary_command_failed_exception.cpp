﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "binary_command_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

BinaryCommandFailedException::BinaryCommandFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    BinaryCommandFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

BinaryCommandFailedException::BinaryCommandFailedException(const std::string& message, const BinaryCommandFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for BinaryCommandFailedException
 */
BinaryCommandFailedExceptionData const & BinaryCommandFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this BinaryCommandFailedException
 */
std::string BinaryCommandFailedException::toString() {
    return std::string("BinaryCommandFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
