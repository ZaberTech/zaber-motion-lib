﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_discontinuity_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamDiscontinuityException::StreamDiscontinuityException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this StreamDiscontinuityException
 */
std::string StreamDiscontinuityException::toString() {
    return std::string("StreamDiscontinuityException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
