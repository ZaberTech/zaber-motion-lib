﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_mode_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtModeException::PvtModeException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this PvtModeException
 */
std::string PvtModeException::toString() {
    return std::string("PvtModeException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
