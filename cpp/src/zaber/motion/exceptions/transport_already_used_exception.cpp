﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "transport_already_used_exception.h"

namespace zaber { namespace motion { namespace exceptions {

TransportAlreadyUsedException::TransportAlreadyUsedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this TransportAlreadyUsedException
 */
std::string TransportAlreadyUsedException::toString() {
    return std::string("TransportAlreadyUsedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
