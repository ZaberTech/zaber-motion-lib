﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_address_conflict_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceAddressConflictException::DeviceAddressConflictException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    DeviceAddressConflictExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

DeviceAddressConflictException::DeviceAddressConflictException(const std::string& message, const DeviceAddressConflictExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for DeviceAddressConflictException
 */
DeviceAddressConflictExceptionData const & DeviceAddressConflictException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this DeviceAddressConflictException
 */
std::string DeviceAddressConflictException::toString() {
    return std::string("DeviceAddressConflictException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
