﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "invalid_park_state_exception.h"

namespace zaber { namespace motion { namespace exceptions {

InvalidParkStateException::InvalidParkStateException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this InvalidParkStateException
 */
std::string InvalidParkStateException::toString() {
    return std::string("InvalidParkStateException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
