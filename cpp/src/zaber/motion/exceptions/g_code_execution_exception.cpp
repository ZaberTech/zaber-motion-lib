﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "g_code_execution_exception.h"

namespace zaber { namespace motion { namespace exceptions {

GCodeExecutionException::GCodeExecutionException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    GCodeExecutionExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

GCodeExecutionException::GCodeExecutionException(const std::string& message, const GCodeExecutionExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for GCodeExecutionException
 */
GCodeExecutionExceptionData const & GCodeExecutionException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this GCodeExecutionException
 */
std::string GCodeExecutionException::toString() {
    return std::string("GCodeExecutionException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
