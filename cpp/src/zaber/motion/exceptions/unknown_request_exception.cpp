﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "unknown_request_exception.h"

namespace zaber { namespace motion { namespace exceptions {

UnknownRequestException::UnknownRequestException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this UnknownRequestException
 */
std::string UnknownRequestException::toString() {
    return std::string("UnknownRequestException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
