﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_execution_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamExecutionException::StreamExecutionException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    StreamExecutionExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

StreamExecutionException::StreamExecutionException(const std::string& message, const StreamExecutionExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for StreamExecutionException
 */
StreamExecutionExceptionData const & StreamExecutionException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this StreamExecutionException
 */
std::string StreamExecutionException::toString() {
    return std::string("StreamExecutionException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
