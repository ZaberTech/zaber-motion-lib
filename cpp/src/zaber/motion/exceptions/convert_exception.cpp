﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/exceptions/convert_exception.h"
#include "zaber/motion/exceptions/motion_lib_exception.h"

#include "zaber/motion/exceptions/bad_command_exception.h"
#include "zaber/motion/exceptions/bad_data_exception.h"
#include "zaber/motion/exceptions/binary_command_failed_exception.h"
#include "zaber/motion/exceptions/command_failed_exception.h"
#include "zaber/motion/exceptions/command_preempted_exception.h"
#include "zaber/motion/exceptions/command_too_long_exception.h"
#include "zaber/motion/exceptions/connection_closed_exception.h"
#include "zaber/motion/exceptions/connection_failed_exception.h"
#include "zaber/motion/exceptions/conversion_failed_exception.h"
#include "zaber/motion/exceptions/device_address_conflict_exception.h"
#include "zaber/motion/exceptions/device_busy_exception.h"
#include "zaber/motion/exceptions/device_db_failed_exception.h"
#include "zaber/motion/exceptions/device_detection_failed_exception.h"
#include "zaber/motion/exceptions/device_failed_exception.h"
#include "zaber/motion/exceptions/device_not_identified_exception.h"
#include "zaber/motion/exceptions/driver_disabled_exception.h"
#include "zaber/motion/exceptions/g_code_execution_exception.h"
#include "zaber/motion/exceptions/g_code_syntax_exception.h"
#include "zaber/motion/exceptions/incompatible_shared_library_exception.h"
#include "zaber/motion/exceptions/internal_error_exception.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/exceptions/invalid_data_exception.h"
#include "zaber/motion/exceptions/invalid_operation_exception.h"
#include "zaber/motion/exceptions/invalid_packet_exception.h"
#include "zaber/motion/exceptions/invalid_park_state_exception.h"
#include "zaber/motion/exceptions/invalid_request_data_exception.h"
#include "zaber/motion/exceptions/invalid_response_exception.h"
#include "zaber/motion/exceptions/io_channel_out_of_range_exception.h"
#include "zaber/motion/exceptions/io_failed_exception.h"
#include "zaber/motion/exceptions/lockstep_enabled_exception.h"
#include "zaber/motion/exceptions/lockstep_not_enabled_exception.h"
#include "zaber/motion/exceptions/movement_failed_exception.h"
#include "zaber/motion/exceptions/movement_interrupted_exception.h"
#include "zaber/motion/exceptions/no_device_found_exception.h"
#include "zaber/motion/exceptions/no_value_for_key_exception.h"
#include "zaber/motion/exceptions/not_supported_exception.h"
#include "zaber/motion/exceptions/operation_failed_exception.h"
#include "zaber/motion/exceptions/os_failed_exception.h"
#include "zaber/motion/exceptions/out_of_request_ids_exception.h"
#include "zaber/motion/exceptions/pvt_discontinuity_exception.h"
#include "zaber/motion/exceptions/pvt_execution_exception.h"
#include "zaber/motion/exceptions/pvt_mode_exception.h"
#include "zaber/motion/exceptions/pvt_movement_failed_exception.h"
#include "zaber/motion/exceptions/pvt_movement_interrupted_exception.h"
#include "zaber/motion/exceptions/pvt_setup_failed_exception.h"
#include "zaber/motion/exceptions/remote_mode_exception.h"
#include "zaber/motion/exceptions/request_timeout_exception.h"
#include "zaber/motion/exceptions/serial_port_busy_exception.h"
#include "zaber/motion/exceptions/set_device_state_failed_exception.h"
#include "zaber/motion/exceptions/set_peripheral_state_failed_exception.h"
#include "zaber/motion/exceptions/setting_not_found_exception.h"
#include "zaber/motion/exceptions/stream_discontinuity_exception.h"
#include "zaber/motion/exceptions/stream_execution_exception.h"
#include "zaber/motion/exceptions/stream_mode_exception.h"
#include "zaber/motion/exceptions/stream_movement_failed_exception.h"
#include "zaber/motion/exceptions/stream_movement_interrupted_exception.h"
#include "zaber/motion/exceptions/stream_setup_failed_exception.h"
#include "zaber/motion/exceptions/timeout_exception.h"
#include "zaber/motion/exceptions/transport_already_used_exception.h"
#include "zaber/motion/exceptions/unknown_request_exception.h"
#include <string>

namespace zaber { namespace motion { namespace exceptions {

void convertException(requests::Errors error, const std::string &message)
{
    switch (error) {
    case requests::Errors::COMMAND_PREEMPTED:
        throw exceptions::CommandPreemptedException(message);
    case requests::Errors::CONNECTION_CLOSED:
        throw exceptions::ConnectionClosedException(message);
    case requests::Errors::CONNECTION_FAILED:
        throw exceptions::ConnectionFailedException(message);
    case requests::Errors::CONVERSION_FAILED:
        throw exceptions::ConversionFailedException(message);
    case requests::Errors::DEVICE_BUSY:
        throw exceptions::DeviceBusyException(message);
    case requests::Errors::DEVICE_DETECTION_FAILED:
        throw exceptions::DeviceDetectionFailedException(message);
    case requests::Errors::DEVICE_FAILED:
        throw exceptions::DeviceFailedException(message);
    case requests::Errors::DEVICE_NOT_IDENTIFIED:
        throw exceptions::DeviceNotIdentifiedException(message);
    case requests::Errors::INCOMPATIBLE_SHARED_LIBRARY:
        throw exceptions::IncompatibleSharedLibraryException(message);
    case requests::Errors::INTERNAL_ERROR:
        throw exceptions::InternalErrorException(message);
    case requests::Errors::INVALID_ARGUMENT:
        throw exceptions::InvalidArgumentException(message);
    case requests::Errors::INVALID_DATA:
        throw exceptions::InvalidDataException(message);
    case requests::Errors::INVALID_OPERATION:
        throw exceptions::InvalidOperationException(message);
    case requests::Errors::INVALID_PARK_STATE:
        throw exceptions::InvalidParkStateException(message);
    case requests::Errors::INVALID_REQUEST_DATA:
        throw exceptions::InvalidRequestDataException(message);
    case requests::Errors::IO_CHANNEL_OUT_OF_RANGE:
        throw exceptions::IoChannelOutOfRangeException(message);
    case requests::Errors::IO_FAILED:
        throw exceptions::IoFailedException(message);
    case requests::Errors::LOCKSTEP_ENABLED:
        throw exceptions::LockstepEnabledException(message);
    case requests::Errors::LOCKSTEP_NOT_ENABLED:
        throw exceptions::LockstepNotEnabledException(message);
    case requests::Errors::NO_DEVICE_FOUND:
        throw exceptions::NoDeviceFoundException(message);
    case requests::Errors::NO_VALUE_FOR_KEY:
        throw exceptions::NoValueForKeyException(message);
    case requests::Errors::NOT_SUPPORTED:
        throw exceptions::NotSupportedException(message);
    case requests::Errors::OS_FAILED:
        throw exceptions::OsFailedException(message);
    case requests::Errors::OUT_OF_REQUEST_IDS:
        throw exceptions::OutOfRequestIdsException(message);
    case requests::Errors::PVT_DISCONTINUITY:
        throw exceptions::PvtDiscontinuityException(message);
    case requests::Errors::PVT_MODE:
        throw exceptions::PvtModeException(message);
    case requests::Errors::PVT_SETUP_FAILED:
        throw exceptions::PvtSetupFailedException(message);
    case requests::Errors::REQUEST_TIMEOUT:
        throw exceptions::RequestTimeoutException(message);
    case requests::Errors::SERIAL_PORT_BUSY:
        throw exceptions::SerialPortBusyException(message);
    case requests::Errors::SETTING_NOT_FOUND:
        throw exceptions::SettingNotFoundException(message);
    case requests::Errors::STREAM_DISCONTINUITY:
        throw exceptions::StreamDiscontinuityException(message);
    case requests::Errors::STREAM_MODE:
        throw exceptions::StreamModeException(message);
    case requests::Errors::STREAM_SETUP_FAILED:
        throw exceptions::StreamSetupFailedException(message);
    case requests::Errors::TIMEOUT:
        throw exceptions::TimeoutException(message);
    case requests::Errors::TRANSPORT_ALREADY_USED:
        throw exceptions::TransportAlreadyUsedException(message);
    case requests::Errors::UNKNOWN_REQUEST:
        throw exceptions::UnknownRequestException(message);
    default :
        throw exceptions::MotionLibException(message);
    }
}

void convertException(requests::Errors error, const std::string &message, const std::string &customData) {
    switch (error) {
    case requests::Errors::BAD_COMMAND :
        throw exceptions::BadCommandException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::BAD_DATA :
        throw exceptions::BadDataException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::BINARY_COMMAND_FAILED :
        throw exceptions::BinaryCommandFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::COMMAND_FAILED :
        throw exceptions::CommandFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::COMMAND_TOO_LONG :
        throw exceptions::CommandTooLongException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::DEVICE_ADDRESS_CONFLICT :
        throw exceptions::DeviceAddressConflictException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::DEVICE_DB_FAILED :
        throw exceptions::DeviceDbFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::DRIVER_DISABLED :
        throw exceptions::DriverDisabledException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::G_CODE_EXECUTION :
        throw exceptions::GCodeExecutionException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::G_CODE_SYNTAX :
        throw exceptions::GCodeSyntaxException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::INVALID_PACKET :
        throw exceptions::InvalidPacketException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::INVALID_RESPONSE :
        throw exceptions::InvalidResponseException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::MOVEMENT_FAILED :
        throw exceptions::MovementFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::MOVEMENT_INTERRUPTED :
        throw exceptions::MovementInterruptedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::OPERATION_FAILED :
        throw exceptions::OperationFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::PVT_EXECUTION :
        throw exceptions::PvtExecutionException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::PVT_MOVEMENT_FAILED :
        throw exceptions::PvtMovementFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::PVT_MOVEMENT_INTERRUPTED :
        throw exceptions::PvtMovementInterruptedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::REMOTE_MODE :
        throw exceptions::RemoteModeException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::SET_DEVICE_STATE_FAILED :
        throw exceptions::SetDeviceStateFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::SET_PERIPHERAL_STATE_FAILED :
        throw exceptions::SetPeripheralStateFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::STREAM_EXECUTION :
        throw exceptions::StreamExecutionException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::STREAM_MOVEMENT_FAILED :
        throw exceptions::StreamMovementFailedException(message, customData); // NOLINT(cert-err60-cpp)
    case requests::Errors::STREAM_MOVEMENT_INTERRUPTED :
        throw exceptions::StreamMovementInterruptedException(message, customData); // NOLINT(cert-err60-cpp)
    default:
        throw exceptions::MotionLibException(message);
    }
}

std::shared_ptr<exceptions::MotionLibException> convertExceptionSP(requests::Errors error, const std::string &message)
{
    std::shared_ptr<exceptions::MotionLibException> exceptionSP;

    switch (error) {
    case requests::Errors::COMMAND_PREEMPTED :
        exceptionSP = std::make_shared<exceptions::CommandPreemptedException>(message);
        break;
    case requests::Errors::CONNECTION_CLOSED :
        exceptionSP = std::make_shared<exceptions::ConnectionClosedException>(message);
        break;
    case requests::Errors::CONNECTION_FAILED :
        exceptionSP = std::make_shared<exceptions::ConnectionFailedException>(message);
        break;
    case requests::Errors::CONVERSION_FAILED :
        exceptionSP = std::make_shared<exceptions::ConversionFailedException>(message);
        break;
    case requests::Errors::DEVICE_BUSY :
        exceptionSP = std::make_shared<exceptions::DeviceBusyException>(message);
        break;
    case requests::Errors::DEVICE_DETECTION_FAILED :
        exceptionSP = std::make_shared<exceptions::DeviceDetectionFailedException>(message);
        break;
    case requests::Errors::DEVICE_FAILED :
        exceptionSP = std::make_shared<exceptions::DeviceFailedException>(message);
        break;
    case requests::Errors::DEVICE_NOT_IDENTIFIED :
        exceptionSP = std::make_shared<exceptions::DeviceNotIdentifiedException>(message);
        break;
    case requests::Errors::INCOMPATIBLE_SHARED_LIBRARY :
        exceptionSP = std::make_shared<exceptions::IncompatibleSharedLibraryException>(message);
        break;
    case requests::Errors::INTERNAL_ERROR :
        exceptionSP = std::make_shared<exceptions::InternalErrorException>(message);
        break;
    case requests::Errors::INVALID_ARGUMENT :
        exceptionSP = std::make_shared<exceptions::InvalidArgumentException>(message);
        break;
    case requests::Errors::INVALID_DATA :
        exceptionSP = std::make_shared<exceptions::InvalidDataException>(message);
        break;
    case requests::Errors::INVALID_OPERATION :
        exceptionSP = std::make_shared<exceptions::InvalidOperationException>(message);
        break;
    case requests::Errors::INVALID_PARK_STATE :
        exceptionSP = std::make_shared<exceptions::InvalidParkStateException>(message);
        break;
    case requests::Errors::INVALID_REQUEST_DATA :
        exceptionSP = std::make_shared<exceptions::InvalidRequestDataException>(message);
        break;
    case requests::Errors::IO_CHANNEL_OUT_OF_RANGE :
        exceptionSP = std::make_shared<exceptions::IoChannelOutOfRangeException>(message);
        break;
    case requests::Errors::IO_FAILED :
        exceptionSP = std::make_shared<exceptions::IoFailedException>(message);
        break;
    case requests::Errors::LOCKSTEP_ENABLED :
        exceptionSP = std::make_shared<exceptions::LockstepEnabledException>(message);
        break;
    case requests::Errors::LOCKSTEP_NOT_ENABLED :
        exceptionSP = std::make_shared<exceptions::LockstepNotEnabledException>(message);
        break;
    case requests::Errors::NO_DEVICE_FOUND :
        exceptionSP = std::make_shared<exceptions::NoDeviceFoundException>(message);
        break;
    case requests::Errors::NO_VALUE_FOR_KEY :
        exceptionSP = std::make_shared<exceptions::NoValueForKeyException>(message);
        break;
    case requests::Errors::NOT_SUPPORTED :
        exceptionSP = std::make_shared<exceptions::NotSupportedException>(message);
        break;
    case requests::Errors::OS_FAILED :
        exceptionSP = std::make_shared<exceptions::OsFailedException>(message);
        break;
    case requests::Errors::OUT_OF_REQUEST_IDS :
        exceptionSP = std::make_shared<exceptions::OutOfRequestIdsException>(message);
        break;
    case requests::Errors::PVT_DISCONTINUITY :
        exceptionSP = std::make_shared<exceptions::PvtDiscontinuityException>(message);
        break;
    case requests::Errors::PVT_MODE :
        exceptionSP = std::make_shared<exceptions::PvtModeException>(message);
        break;
    case requests::Errors::PVT_SETUP_FAILED :
        exceptionSP = std::make_shared<exceptions::PvtSetupFailedException>(message);
        break;
    case requests::Errors::REQUEST_TIMEOUT :
        exceptionSP = std::make_shared<exceptions::RequestTimeoutException>(message);
        break;
    case requests::Errors::SERIAL_PORT_BUSY :
        exceptionSP = std::make_shared<exceptions::SerialPortBusyException>(message);
        break;
    case requests::Errors::SETTING_NOT_FOUND :
        exceptionSP = std::make_shared<exceptions::SettingNotFoundException>(message);
        break;
    case requests::Errors::STREAM_DISCONTINUITY :
        exceptionSP = std::make_shared<exceptions::StreamDiscontinuityException>(message);
        break;
    case requests::Errors::STREAM_MODE :
        exceptionSP = std::make_shared<exceptions::StreamModeException>(message);
        break;
    case requests::Errors::STREAM_SETUP_FAILED :
        exceptionSP = std::make_shared<exceptions::StreamSetupFailedException>(message);
        break;
    case requests::Errors::TIMEOUT :
        exceptionSP = std::make_shared<exceptions::TimeoutException>(message);
        break;
    case requests::Errors::TRANSPORT_ALREADY_USED :
        exceptionSP = std::make_shared<exceptions::TransportAlreadyUsedException>(message);
        break;
    case requests::Errors::UNKNOWN_REQUEST :
        exceptionSP = std::make_shared<exceptions::UnknownRequestException>(message);
        break;
    default :
        exceptionSP = std::make_shared<exceptions::MotionLibException>(message);
        break;
    }

    return exceptionSP;
}

std::shared_ptr<exceptions::MotionLibException> convertExceptionSP(requests::Errors error, const std::string &message, const std::string &customData) {
    std::shared_ptr<exceptions::MotionLibException> exceptionSP;

    switch (error) {
    case requests::Errors::BAD_COMMAND :
        exceptionSP = std::make_shared<exceptions::BadCommandException>(message, customData);
        break;
    case requests::Errors::BAD_DATA :
        exceptionSP = std::make_shared<exceptions::BadDataException>(message, customData);
        break;
    case requests::Errors::BINARY_COMMAND_FAILED :
        exceptionSP = std::make_shared<exceptions::BinaryCommandFailedException>(message, customData);
        break;
    case requests::Errors::COMMAND_FAILED :
        exceptionSP = std::make_shared<exceptions::CommandFailedException>(message, customData);
        break;
    case requests::Errors::COMMAND_TOO_LONG :
        exceptionSP = std::make_shared<exceptions::CommandTooLongException>(message, customData);
        break;
    case requests::Errors::DEVICE_ADDRESS_CONFLICT :
        exceptionSP = std::make_shared<exceptions::DeviceAddressConflictException>(message, customData);
        break;
    case requests::Errors::DEVICE_DB_FAILED :
        exceptionSP = std::make_shared<exceptions::DeviceDbFailedException>(message, customData);
        break;
    case requests::Errors::DRIVER_DISABLED :
        exceptionSP = std::make_shared<exceptions::DriverDisabledException>(message, customData);
        break;
    case requests::Errors::G_CODE_EXECUTION :
        exceptionSP = std::make_shared<exceptions::GCodeExecutionException>(message, customData);
        break;
    case requests::Errors::G_CODE_SYNTAX :
        exceptionSP = std::make_shared<exceptions::GCodeSyntaxException>(message, customData);
        break;
    case requests::Errors::INVALID_PACKET :
        exceptionSP = std::make_shared<exceptions::InvalidPacketException>(message, customData);
        break;
    case requests::Errors::INVALID_RESPONSE :
        exceptionSP = std::make_shared<exceptions::InvalidResponseException>(message, customData);
        break;
    case requests::Errors::MOVEMENT_FAILED :
        exceptionSP = std::make_shared<exceptions::MovementFailedException>(message, customData);
        break;
    case requests::Errors::MOVEMENT_INTERRUPTED :
        exceptionSP = std::make_shared<exceptions::MovementInterruptedException>(message, customData);
        break;
    case requests::Errors::OPERATION_FAILED :
        exceptionSP = std::make_shared<exceptions::OperationFailedException>(message, customData);
        break;
    case requests::Errors::PVT_EXECUTION :
        exceptionSP = std::make_shared<exceptions::PvtExecutionException>(message, customData);
        break;
    case requests::Errors::PVT_MOVEMENT_FAILED :
        exceptionSP = std::make_shared<exceptions::PvtMovementFailedException>(message, customData);
        break;
    case requests::Errors::PVT_MOVEMENT_INTERRUPTED :
        exceptionSP = std::make_shared<exceptions::PvtMovementInterruptedException>(message, customData);
        break;
    case requests::Errors::REMOTE_MODE :
        exceptionSP = std::make_shared<exceptions::RemoteModeException>(message, customData);
        break;
    case requests::Errors::SET_DEVICE_STATE_FAILED :
        exceptionSP = std::make_shared<exceptions::SetDeviceStateFailedException>(message, customData);
        break;
    case requests::Errors::SET_PERIPHERAL_STATE_FAILED :
        exceptionSP = std::make_shared<exceptions::SetPeripheralStateFailedException>(message, customData);
        break;
    case requests::Errors::STREAM_EXECUTION :
        exceptionSP = std::make_shared<exceptions::StreamExecutionException>(message, customData);
        break;
    case requests::Errors::STREAM_MOVEMENT_FAILED :
        exceptionSP = std::make_shared<exceptions::StreamMovementFailedException>(message, customData);
        break;
    case requests::Errors::STREAM_MOVEMENT_INTERRUPTED :
        exceptionSP = std::make_shared<exceptions::StreamMovementInterruptedException>(message, customData);
        break;
    default:
        exceptionSP = std::make_shared<exceptions::MotionLibException>(message);
    }

    return exceptionSP;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
