﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_movement_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtMovementFailedException::PvtMovementFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    PvtMovementFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

PvtMovementFailedException::PvtMovementFailedException(const std::string& message, const PvtMovementFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for PvtMovementFailedException
 */
PvtMovementFailedExceptionData const & PvtMovementFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this PvtMovementFailedException
 */
std::string PvtMovementFailedException::toString() {
    return std::string("PvtMovementFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
