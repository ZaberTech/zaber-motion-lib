﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "stream_movement_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

StreamMovementFailedException::StreamMovementFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    StreamMovementFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

StreamMovementFailedException::StreamMovementFailedException(const std::string& message, const StreamMovementFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for StreamMovementFailedException
 */
StreamMovementFailedExceptionData const & StreamMovementFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this StreamMovementFailedException
 */
std::string StreamMovementFailedException::toString() {
    return std::string("StreamMovementFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
