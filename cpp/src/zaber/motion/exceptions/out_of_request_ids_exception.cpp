﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "out_of_request_ids_exception.h"

namespace zaber { namespace motion { namespace exceptions {

OutOfRequestIdsException::OutOfRequestIdsException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this OutOfRequestIdsException
 */
std::string OutOfRequestIdsException::toString() {
    return std::string("OutOfRequestIdsException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
