﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "not_supported_exception.h"

namespace zaber { namespace motion { namespace exceptions {

NotSupportedException::NotSupportedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this NotSupportedException
 */
std::string NotSupportedException::toString() {
    return std::string("NotSupportedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
