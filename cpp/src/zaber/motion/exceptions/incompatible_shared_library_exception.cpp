﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "incompatible_shared_library_exception.h"

namespace zaber { namespace motion { namespace exceptions {

IncompatibleSharedLibraryException::IncompatibleSharedLibraryException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this IncompatibleSharedLibraryException
 */
std::string IncompatibleSharedLibraryException::toString() {
    return std::string("IncompatibleSharedLibraryException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
