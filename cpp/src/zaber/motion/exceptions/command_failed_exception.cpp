﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "command_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

CommandFailedException::CommandFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    CommandFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

CommandFailedException::CommandFailedException(const std::string& message, const CommandFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for CommandFailedException
 */
CommandFailedExceptionData const & CommandFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this CommandFailedException
 */
std::string CommandFailedException::toString() {
    return std::string("CommandFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
