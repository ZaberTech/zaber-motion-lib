﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "movement_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

MovementFailedException::MovementFailedException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    MovementFailedExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

MovementFailedException::MovementFailedException(const std::string& message, const MovementFailedExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for MovementFailedException
 */
MovementFailedExceptionData const & MovementFailedException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this MovementFailedException
 */
std::string MovementFailedException::toString() {
    return std::string("MovementFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
