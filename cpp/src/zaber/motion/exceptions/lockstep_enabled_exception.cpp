﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "lockstep_enabled_exception.h"

namespace zaber { namespace motion { namespace exceptions {

LockstepEnabledException::LockstepEnabledException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this LockstepEnabledException
 */
std::string LockstepEnabledException::toString() {
    return std::string("LockstepEnabledException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
