﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "request_timeout_exception.h"

namespace zaber { namespace motion { namespace exceptions {

RequestTimeoutException::RequestTimeoutException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this RequestTimeoutException
 */
std::string RequestTimeoutException::toString() {
    return std::string("RequestTimeoutException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
