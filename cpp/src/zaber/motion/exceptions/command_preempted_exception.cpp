﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "command_preempted_exception.h"

namespace zaber { namespace motion { namespace exceptions {

CommandPreemptedException::CommandPreemptedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this CommandPreemptedException
 */
std::string CommandPreemptedException::toString() {
    return std::string("CommandPreemptedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
