﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "no_device_found_exception.h"

namespace zaber { namespace motion { namespace exceptions {

NoDeviceFoundException::NoDeviceFoundException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this NoDeviceFoundException
 */
std::string NoDeviceFoundException::toString() {
    return std::string("NoDeviceFoundException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
