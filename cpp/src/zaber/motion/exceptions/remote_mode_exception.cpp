﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "remote_mode_exception.h"

namespace zaber { namespace motion { namespace exceptions {

RemoteModeException::RemoteModeException(const std::string& message, const std::string &customData): CommandFailedException(message, customData) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
}

RemoteModeException::RemoteModeException(const std::string& message, const CommandFailedExceptionData &&customData): CommandFailedException(message, std::move(customData)) {
}

/**
 * Get a string representation of this RemoteModeException
 */
std::string RemoteModeException::toString() {
    return std::string("RemoteModeException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
