﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_discontinuity_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtDiscontinuityException::PvtDiscontinuityException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this PvtDiscontinuityException
 */
std::string PvtDiscontinuityException::toString() {
    return std::string("PvtDiscontinuityException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
