﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_detection_failed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceDetectionFailedException::DeviceDetectionFailedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this DeviceDetectionFailedException
 */
std::string DeviceDetectionFailedException::toString() {
    return std::string("DeviceDetectionFailedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
