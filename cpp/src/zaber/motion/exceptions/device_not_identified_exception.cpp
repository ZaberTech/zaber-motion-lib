﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "device_not_identified_exception.h"

namespace zaber { namespace motion { namespace exceptions {

DeviceNotIdentifiedException::DeviceNotIdentifiedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this DeviceNotIdentifiedException
 */
std::string DeviceNotIdentifiedException::toString() {
    return std::string("DeviceNotIdentifiedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
