﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "pvt_execution_exception.h"

namespace zaber { namespace motion { namespace exceptions {

PvtExecutionException::PvtExecutionException(const std::string& message, const std::string &customData): MotionLibException(message) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
    PvtExecutionExceptionData exceptionData;
    static_cast<Serializable&>(exceptionData).populateFromByteArray(customData);
    this->details = exceptionData;
}

PvtExecutionException::PvtExecutionException(const std::string& message, const PvtExecutionExceptionData &&customData): MotionLibException(message), details(customData) {
}

/**
 * Additional data for PvtExecutionException
 */
PvtExecutionExceptionData const & PvtExecutionException::getDetails() const {
    return this->details;
}

/**
 * Get a string representation of this PvtExecutionException
 */
std::string PvtExecutionException::toString() {
    return std::string("PvtExecutionException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
