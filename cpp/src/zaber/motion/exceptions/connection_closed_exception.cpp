﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "connection_closed_exception.h"

namespace zaber { namespace motion { namespace exceptions {

ConnectionClosedException::ConnectionClosedException(const std::string& message): MotionLibException(message) {
}

/**
 * Get a string representation of this ConnectionClosedException
 */
std::string ConnectionClosedException::toString() {
    return std::string("ConnectionClosedException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
