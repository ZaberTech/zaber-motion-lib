﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "bad_command_exception.h"

namespace zaber { namespace motion { namespace exceptions {

BadCommandException::BadCommandException(const std::string& message, const std::string &customData): CommandFailedException(message, customData) { // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
}

BadCommandException::BadCommandException(const std::string& message, const CommandFailedExceptionData &&customData): CommandFailedException(message, std::move(customData)) {
}

/**
 * Get a string representation of this BadCommandException
 */
std::string BadCommandException::toString() {
    return std::string("BadCommandException: ").append(this->what());
}

}  // namespace exceptions
}  // namespace motion
}  // namespace zaber
