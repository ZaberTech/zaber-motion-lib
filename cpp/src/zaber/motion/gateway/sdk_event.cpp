#include "zaber/motion/gateway/sdk_event.h"

#include <functional>

#include "zaber/motion/dto/requests/gateway_event.h"
#include "zaber/motion/library_exception.h"
#include "zaber/motion/dto/serializable.h"
#include "zaber/motion/dto/requests/test_event.h"
#include "zaber/motion/dto/requests/unknown_response_event_wrapper.h"
#include "zaber/motion/dto/requests/unknown_binary_response_event_wrapper.h"
#include "zaber/motion/dto/requests/alert_event_wrapper.h"
#include "zaber/motion/dto/requests/binary_reply_only_event_wrapper.h"
#include "zaber/motion/dto/requests/disconnected_event.h"

namespace zaber { namespace motion {

SdkEvent::SdkEvent(const std::vector<std::string>& responseBuffers) {
    requests::GatewayEvent event;
    static_cast<Serializable&>(event).populateFromByteArray(responseBuffers[0]);

    std::string eventName = event.getEvent();
    unpackEventData(eventName, responseBuffers[1]);
}

void SdkEvent::unpackEventData(const std::string& eventName, const std::string& data) {
    if (eventName == SdkEvent::TEST_STR) {
        _eventData = std::make_shared<requests::TestEvent>();
        _eventData->populateFromByteArray(data);
        _eventType = EventType::TEST;
    } else if (eventName == SdkEvent::UNKNOWN_RESPONSE_STR) {
        initEventData<requests::UnknownResponseEventWrapper>(data);
        _eventType = EventType::UNKNOWN_RESPONSE;
    } else if (eventName == SdkEvent::UNKNOWN_BINARY_RESPONSE_STR) {
        initEventData<requests::UnknownBinaryResponseEventWrapper>(data);
        _eventType = EventType::UNKNOWN_BINARY_RESPONSE;
    } else if (eventName == SdkEvent::ALERT_STR) {
        initEventData<requests::AlertEventWrapper>(data);
        _eventType = EventType::ALERT;
    } else if (eventName == SdkEvent::BINARY_REPLY_ONLY_STR) {
        initEventData<requests::BinaryReplyOnlyEventWrapper>(data);
        _eventType = EventType::BINARY_REPLY_ONLY;
    } else if (eventName == SdkEvent::DISCONNECTED_STR ) {
        initEventData<requests::DisconnectedEvent>(data);
        _eventType = EventType::DISCONNECTED;
    } else {
        throw LibraryIntegrationException("Unknown event");
    }
}

}  // namespace motion
}  // namespace zaber
