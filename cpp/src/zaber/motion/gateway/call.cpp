#include "zaber/motion/gateway/call.h"

#include <cstdint>
#include <string>
#include <vector>
#include <future>

#include "zaber_motion_core.h"
#include "zaber/motion/exceptions/convert_exception.h"
#include "zaber/motion/gateway/serializer.h"
#include "zaber/motion/library_exception.h"

constexpr uint8_t GOTRUE = 1U;

namespace zaber { namespace motion {

///////////////////////////////////////////////////////////
// AsyncContext

class AsyncContext {
public:
    AsyncContext() : _response_pointer(nullptr) { }
    explicit AsyncContext(Serializable* responsePtr) : _response_pointer(responsePtr) { }
    ~AsyncContext() = default;

    AsyncContext(const AsyncContext&) = delete;
    AsyncContext(const AsyncContext&&) = delete;
    AsyncContext& operator=(const AsyncContext&) = delete;
    AsyncContext& operator=(const AsyncContext&&) = delete;

    void copyBuffer(void* responseData) {
        uint32_t msgLen = *reinterpret_cast<uint32_t*>(responseData);  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast) because callback uses void*
        this->_response_buffer = std::string(reinterpret_cast<char*>(responseData), msgLen);  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast) because callback uses void*
    }

    void completePromise() {
        this->_promise.set_value();
    }

    std::shared_future<void> getFuture() {
        return this->_promise.get_future();
    }

    void deserializeAndParse() {
        std::vector<std::string> responses = Serializer::deserialize(this->_response_buffer);
        parseResponse(this->_response_pointer, responses);
    }

    static void parseResponse(Serializable* responsePtr, const std::vector<std::string>& responses) {
        requests::GatewayResponse response;
        static_cast<Serializable&>(response).populateFromByteArray(responses[0]);

        if (response.getResponse() != requests::ResponseType::OK) {
            if (responses.size() > 1) {
                exceptions::convertException(response.getErrorType(), response.getErrorMessage(), responses[1]);
            }
            exceptions::convertException(response.getErrorType(), response.getErrorMessage());
        }
        if (responsePtr != nullptr && responses.size() == 1) {
            throw LibraryIntegrationException("No response from library!");
        }
        if (responsePtr == nullptr && responses.size() > 1) {
            throw LibraryIntegrationException("Expected no response but got response!");
        }
        if (responsePtr != nullptr) {
            responsePtr->populateFromByteArray(responses[1]);
        }
    }

private:
    std::promise<void> _promise;
    std::string _response_buffer;
    Serializable* _response_pointer;
};


///////////////////////////////////////////////////////////
// Internal


void callback(void* responseData, int64_t tag) {
    auto* asyncContextPtr = reinterpret_cast<AsyncContext*>(tag);  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast, performance-no-int-to-ptr) because callback uses void*
    asyncContextPtr->copyBuffer(responseData);
    asyncContextPtr->completePromise();
}

void callGatewayBase(const std::string& request, Serializable* requestData, AsyncContext* asyncContext) {
    zaber::motion::requests::GatewayRequest gatewayRequest;
    gatewayRequest.setRequest(request);

    std::vector<const Serializable*> messages;
    messages.push_back(&gatewayRequest);
    if (requestData != nullptr) {
        messages.push_back(requestData);
    }

    std::string requestBuffer = Serializer::serialize(messages);

    int result = zml_call(reinterpret_cast<void*>(const_cast<char*>(requestBuffer.data())), reinterpret_cast<int64_t>(asyncContext), reinterpret_cast<void*>(callback), GOTRUE);  // NOLINT
    if (result != 0) {
        throw LibraryIntegrationException("Could not complete library call");
    }

    asyncContext->getFuture().wait();
    asyncContext->deserializeAndParse();
}

///////////////////////////////////////////////////////////
// Gateway Interface

void callGateway(const std::string& request, Serializable& requestData, Serializable* response) {
    AsyncContext asyncContext(response);
    callGatewayBase(request, &requestData, &asyncContext);
}

void callGateway(const std::string& request, Serializable* response) {
    AsyncContext asyncContext(response);
    callGatewayBase(request, nullptr, &asyncContext);
}

void callGateway(const std::string& request, Serializable& requestData) {
    AsyncContext asyncContext;
    callGatewayBase(request, &requestData, &asyncContext);
}

void callGateway(const std::string& request) {
    AsyncContext asyncContext;
    callGatewayBase(request, nullptr, &asyncContext);
}

}  // namespace motion
}  // namespace zaber
