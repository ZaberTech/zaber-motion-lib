#include "zaber/motion/gateway/serializer.h"
#include <algorithm>
#include <cstdint>
#include <cstring>
#include <string>

constexpr uint32_t SIZE_TYPE_SIZE = sizeof(uint32_t);

namespace zaber {
namespace motion {

std::string Serializer::serialize(const std::vector<const Serializable*>& messages) {
    uint32_t totalLength = SIZE_TYPE_SIZE;
    std::vector<std::string> serializedMessages;

    for (const Serializable* msgPtr: messages) {
        serializedMessages.push_back(msgPtr->toByteArray());
        totalLength += static_cast<uint32_t>(serializedMessages.back().size()) + SIZE_TYPE_SIZE;
    }

    std::string stringBuffer;
    stringBuffer.reserve(totalLength);

    stringBuffer.append(reinterpret_cast<char *>(&totalLength), SIZE_TYPE_SIZE );  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast): We want the binary data of the integer

    for (const std::string& serializedMessage : serializedMessages) {
        auto msgSize = static_cast<uint32_t>(serializedMessage.size());
        stringBuffer.append(reinterpret_cast<char *>(&msgSize), SIZE_TYPE_SIZE );  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast): We want the binary data of the integer
        stringBuffer.append(serializedMessage);
    }
    return stringBuffer;
}


std::vector<std::string> Serializer::deserialize(const std::string& responseBuffer) {
    uint32_t offset = SIZE_TYPE_SIZE;
    std::vector<std::string> responses;

    while (offset < responseBuffer.length()) {
        uint32_t msgSize; // NOLINT(cppcoreguidelines-init-variables)
        std::memcpy(&msgSize, responseBuffer.data() + offset, SIZE_TYPE_SIZE);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic): We want the binary data of the integer

        offset += SIZE_TYPE_SIZE;

        std::string rawResponse (responseBuffer, offset, msgSize);
        responses.push_back(rawResponse);
        offset += msgSize;
    }

    return responses;
}

}  // namespace motion
}  // namespace zaber
