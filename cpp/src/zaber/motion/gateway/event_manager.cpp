#include "zaber/motion/gateway/event_manager.h"

#include <vector>
#include <algorithm>
#include <optional>
#include <iostream>

#include "zaber_motion_core.h"
#include "zaber/motion/gateway/serializer.h"
#include "zaber/motion/dto/serializable.h"

namespace zaber { namespace motion {

EventManager::EventCallbackRegistrar EventManager::_registrar;

EventManager::EventCallbackRegistrar::EventCallbackRegistrar() noexcept {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    zml_setEventHandler(static_cast<int64_t>(0), reinterpret_cast<void*>(&EventManager::handleEvent));
}

EventManager::EventManager() {
    _eventThread = std::thread(&EventManager::handleEventQueue, this);
}

EventManager::~EventManager() {
    {
        std::lock_guard<std::mutex> lock(_eventQueueMutex);
        _handleEventQueue = false;
    }
    _workerThreadCondition.notify_one();
    _eventThread.join();
}

EventManager& EventManager::getSingleton() {
    static EventManager singleton;
    return singleton;
}

void EventManager::processEvent(const std::string& responseBuffer) {
    std::vector<std::string> responses = Serializer::deserialize(responseBuffer);
    SdkEvent sdkEvent(responses);
    {
        std::lock_guard<std::mutex> lock(_eventQueueMutex);
        _eventQueue.push(sdkEvent);
    }
    _workerThreadCondition.notify_one();
}

void EventManager::handleEvent(void* responseData, int64_t /*unused*/) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast) because callback uses void*
    auto msgLen = *reinterpret_cast<uint32_t *>(responseData);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast) because callback uses void*
    std::string responseBuffer (reinterpret_cast<char *>(responseData), msgLen);
    EventManager::getSingleton().processEvent(responseBuffer);
}

// worker thread event loop
void EventManager::handleEventQueue() {
    while (true) {
        SdkEvent event;
        {
            std::unique_lock<std::mutex> lock(_eventQueueMutex);
            _workerThreadCondition.wait(lock, [this] { return !_eventQueue.empty() || !_handleEventQueue; });

            if (!_handleEventQueue) {
                break;
            }

            event = std::move(_eventQueue.front());
            _eventQueue.pop();
        }

        tryAddHandlers();
        tryRemoveHandlers();

        for (const std::weak_ptr<EventHandlerBase>& handler : _eventHandlers) {
            if (auto sharedHandler = handler.lock()) {
                try {
                    sharedHandler->handleEvent(event);
                } catch (const std::exception&) {
                    std::cerr << "Zaber Motion Error: Unhandled exception in user callback" << std::endl;
                    std::terminate();
                }
            }
        }
    }
}

void EventManager::tryAddHandlers() {
    std::lock_guard<std::mutex> lock(_newHandlersMutex);
    _eventHandlers.insert(_eventHandlers.end(), std::move_iterator(_newHandlers.begin()), std::move_iterator(_newHandlers.end()));
    _newHandlers.clear();
}

void EventManager::tryRemoveHandlers() {
    _eventHandlers.erase(
        std::remove_if( _eventHandlers.begin(), _eventHandlers.end(),
        std::mem_fn(&std::weak_ptr<EventHandlerBase>::expired)),
        _eventHandlers.end()
    );
}

}  // namespace motion
}  // namespace zaber

