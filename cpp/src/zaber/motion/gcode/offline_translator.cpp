﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "offline_translator.h"
#include "zaber/motion/dto/gcode/device_definition.h"
#include "zaber/motion/dto/gcode/translate_result.h"
#include "zaber/motion/dto/gcode/translator_config.h"
#include "zaber/motion/ascii/connection.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Device = zaber::motion::ascii::Device;

namespace zaber { namespace motion { namespace gcode {

OfflineTranslator::OfflineTranslator(int translatorId): _translatorId(translatorId) {
}

/**
 * Sets up translator from provided device definition and configuration.
 * @param definition Definition of device and its peripherals.
 * The definition must match a device that later performs the commands.
 * @param config Configuration of the translator.
 * @return New instance of translator.
 */
OfflineTranslator OfflineTranslator::setup(const DeviceDefinition& definition, const std::optional<TranslatorConfig>& config) {

    ::zaber::motion::requests::TranslatorCreateRequest request;
    request.setDefinition(definition);
    request.setConfig(config);
    ::zaber::motion::requests::TranslatorCreateResponse response;
    ::zaber::motion::callGateway("gcode/create", request, &response);

    return {response.getTranslatorId()};
}

/**
 * Sets up an offline translator from provided device, axes, and configuration.
 * @param device Device that later performs the command streaming.
 * @param axes Axis numbers that are later used to setup the stream.
 * For a lockstep group specify only the first axis of the group.
 * @param config Configuration of the translator.
 * @return New instance of translator.
 */
OfflineTranslator OfflineTranslator::setupFromDevice(const Device& device, const std::vector<int>& axes, const std::optional<TranslatorConfig>& config) {

    ::zaber::motion::requests::TranslatorCreateFromDeviceRequest request;
    request.setInterfaceId(device.getConnection().getInterfaceId());
    request.setDevice(device.getDeviceAddress());
    request.setAxes(std::vector<int>(axes));
    request.setConfig(config);
    ::zaber::motion::requests::TranslatorCreateResponse response;
    ::zaber::motion::callGateway("gcode/create_from_device", request, &response);

    return {response.getTranslatorId()};
}

/**
 * Translates a single block (line) of G-code.
 * @param block Block (line) of G-code.
 * @return Result of translation containing the stream commands.
 */
TranslateResult OfflineTranslator::translate(const std::string& block) {

    ::zaber::motion::requests::TranslatorTranslateRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setBlock(block);
    TranslateResult response;
    ::zaber::motion::callGateway("gcode/translate", request, &response);

    return response;
}

/**
 * Flushes the remaining stream commands waiting in optimization buffer.
 * The flush is also performed by M2 and M30 codes.
 * @return The remaining stream commands.
 */
std::vector<std::string> OfflineTranslator::flush() {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::requests::TranslatorFlushResponse response;
    ::zaber::motion::callGateway("gcode/flush", request, &response);

    return response.getCommands();
}

/**
 * Sets the speed at which the device moves when traversing (G0).
 * @param traverseRate The traverse rate.
 * @param unit Units of the traverse rate.
 */
void OfflineTranslator::setTraverseRate(double traverseRate, Units unit) {

    ::zaber::motion::requests::TranslatorSetTraverseRateRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setTraverseRate(traverseRate);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_traverse_rate", request);
}

/**
 * Sets position of translator's axis.
 * Use this method to set position after performing movement outside of the translator.
 * This method does not cause any movement.
 * @param axis Letter of the axis.
 * @param position The position.
 * @param unit Units of position.
 */
void OfflineTranslator::setAxisPosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_position", request);
}

/**
 * Gets position of translator's axis.
 * This method does not query device but returns value from translator's state.
 * @param axis Letter of the axis.
 * @param unit Units of position.
 * @return Position of translator's axis.
 */
double OfflineTranslator::getAxisPosition(const std::string& axis, Units unit) {

    ::zaber::motion::requests::TranslatorGetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("gcode/get_axis_position", request, &response);

    return response.getValue();
}

/**
 * Sets the home position of translator's axis.
 * This position is used by G28.
 * @param axis Letter of the axis.
 * @param position The home position.
 * @param unit Units of position.
 */
void OfflineTranslator::setAxisHomePosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_home", request);
}

/**
 * Sets the secondary home position of translator's axis.
 * This position is used by G30.
 * @param axis Letter of the axis.
 * @param position The home position.
 * @param unit Units of position.
 */
void OfflineTranslator::setAxisSecondaryHomePosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_secondary_home", request);
}

/**
 * Gets offset of an axis in a given coordinate system.
 * @param coordinateSystem Coordinate system (e.g. G54).
 * @param axis Letter of the axis.
 * @param unit Units of position.
 * @return Offset in translator units of the axis.
 */
double OfflineTranslator::getAxisCoordinateSystemOffset(const std::string& coordinateSystem, const std::string& axis, Units unit) {

    ::zaber::motion::requests::TranslatorGetAxisOffsetRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setCoordinateSystem(coordinateSystem);
    request.setAxis(axis);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("gcode/get_axis_offset", request, &response);

    return response.getValue();
}

/**
 * Resets internal state after device rejected generated command.
 * Axis positions become uninitialized.
 */
void OfflineTranslator::resetAfterStreamError() {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::callGateway("gcode/reset_after_stream_error", request);
}

/**
 * Allows to scale feed rate of the translated code by a coefficient.
 * @param coefficient Coefficient of the original feed rate.
 */
void OfflineTranslator::setFeedRateOverride(double coefficient) {

    ::zaber::motion::requests::TranslatorSetFeedRateOverrideRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setCoefficient(coefficient);
    ::zaber::motion::callGateway("gcode/set_feed_rate_override", request);
}

/**
 * Releases native resources of a translator.
 * @param translatorId The ID of the translator.
 */
void OfflineTranslator::free(int translatorId) {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(translatorId);
    ::zaber::motion::callGateway("gcode/free", request);
}

/**
 * Gets current coordinate system (e.g. G54).
 * @return Current coordinate system.
 */
std::string OfflineTranslator::getCurrentCoordinateSystem() const {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("gcode/get_current_coordinate_system", request, &response);

    return response.getValue();
}

/**
 * The ID of the translator that serves to identify native resources.
 */
int OfflineTranslator::getTranslatorId() const {
    return this->_translatorId;
}

/**
 * Current coordinate system.
 */
std::string OfflineTranslator::getCoordinateSystem() const {
    return this->getCurrentCoordinateSystem();
}


OfflineTranslator& OfflineTranslator::operator=(OfflineTranslator&& other) noexcept {
    if (this != &other) {
        if (this->_translatorId >= 0) {
            OfflineTranslator::free(this->_translatorId);
            this->_translatorId = -1;
        }

        std::swap(_translatorId, other._translatorId);
    }
    return *this;
}

OfflineTranslator::OfflineTranslator(OfflineTranslator&& other) noexcept: _translatorId(-1) {
    *this = std::move(other);
}

OfflineTranslator::~OfflineTranslator() {
    if (this->_translatorId >= 0) {
        OfflineTranslator::free(this->_translatorId);
    }
}

}  // namespace gcode
}  // namespace motion
}  // namespace zaber
