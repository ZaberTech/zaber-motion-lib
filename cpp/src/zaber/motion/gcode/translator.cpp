﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/gcode/translator.h"

#include "zaber/motion/dto/gcode/translate_result.h"
#include "zaber/motion/dto/gcode/translator_config.h"
#include "zaber/motion/ascii/connection.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/ascii/stream.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Stream = zaber::motion::ascii::Stream;

namespace zaber { namespace motion { namespace gcode {

Translator::Translator(int translatorId): _translatorId(translatorId) {
}

/**
 * Sets up the translator on top of a provided stream.
 * @param stream The stream to setup the translator on.
 * The stream must be already setup in a live or a store mode.
 * @param config Configuration of the translator.
 * @return New instance of translator.
 */
Translator Translator::setup(const Stream& stream, const std::optional<TranslatorConfig>& config) {

    ::zaber::motion::requests::TranslatorCreateLiveRequest request;
    request.setDevice(stream.getDevice().getDeviceAddress());
    request.setInterfaceId(stream.getDevice().getConnection().getInterfaceId());
    request.setStreamId(stream.getStreamId());
    request.setConfig(config);
    ::zaber::motion::requests::TranslatorCreateResponse response;
    ::zaber::motion::callGateway("gcode/create_live", request, &response);

    return {response.getTranslatorId()};
}

/**
 * Translates a single block (line) of G-code.
 * The commands are queued in the underlying stream to ensure smooth continues movement.
 * Returning of this method indicates that the commands are queued (not necessarily executed).
 * @param block Block (line) of G-code.
 * @return Result of translation containing the commands sent to the device.
 */
TranslateResult Translator::translate(const std::string& block) {

    ::zaber::motion::requests::TranslatorTranslateRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setBlock(block);
    TranslateResult response;
    ::zaber::motion::callGateway("gcode/translate_live", request, &response);

    return response;
}

/**
 * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
 * The flush is also performed by M2 and M30 codes.
 * @param waitUntilIdle Determines whether to wait for the stream to finish all the movements.
 * @return The remaining stream commands.
 */
std::vector<std::string> Translator::flush(bool waitUntilIdle) {

    ::zaber::motion::requests::TranslatorFlushLiveRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::requests::TranslatorFlushResponse response;
    ::zaber::motion::callGateway("gcode/flush_live", request, &response);

    return response.getCommands();
}

/**
 * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
 * The flush is also performed by M2 and M30 codes.
 * @param options A struct of type FlushOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether to wait for the stream to finish all the movements.
 * @return The remaining stream commands.
 */
std::vector<std::string> Translator::flush(const Translator::FlushOptions& options) {
    return Translator::flush(options.waitUntilIdle);
}

/**
 * Resets position of the translator from the underlying stream.
 * Call this method after performing a movement outside of translator.
 */
void Translator::resetPosition() {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::callGateway("gcode/reset_position_from_stream", request);
}

/**
 * Sets the speed at which the device moves when traversing (G0).
 * @param traverseRate The traverse rate.
 * @param unit Units of the traverse rate.
 */
void Translator::setTraverseRate(double traverseRate, Units unit) {

    ::zaber::motion::requests::TranslatorSetTraverseRateRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setTraverseRate(traverseRate);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_traverse_rate", request);
}

/**
 * Sets position of translator's axis.
 * Use this method to set position after performing movement outside of the translator.
 * This method does not cause any movement.
 * @param axis Letter of the axis.
 * @param position The position.
 * @param unit Units of position.
 */
void Translator::setAxisPosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_position", request);
}

/**
 * Gets position of translator's axis.
 * This method does not query device but returns value from translator's state.
 * @param axis Letter of the axis.
 * @param unit Units of position.
 * @return Position of translator's axis.
 */
double Translator::getAxisPosition(const std::string& axis, Units unit) {

    ::zaber::motion::requests::TranslatorGetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("gcode/get_axis_position", request, &response);

    return response.getValue();
}

/**
 * Sets the home position of translator's axis.
 * This position is used by G28.
 * @param axis Letter of the axis.
 * @param position The home position.
 * @param unit Units of position.
 */
void Translator::setAxisHomePosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_home", request);
}

/**
 * Sets the secondary home position of translator's axis.
 * This position is used by G30.
 * @param axis Letter of the axis.
 * @param position The home position.
 * @param unit Units of position.
 */
void Translator::setAxisSecondaryHomePosition(const std::string& axis, double position, Units unit) {

    ::zaber::motion::requests::TranslatorSetAxisPositionRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setAxis(axis);
    request.setPosition(position);
    request.setUnit(unit);
    ::zaber::motion::callGateway("gcode/set_axis_secondary_home", request);
}

/**
 * Gets offset of an axis in a given coordinate system.
 * @param coordinateSystem Coordinate system (e.g. G54).
 * @param axis Letter of the axis.
 * @param unit Units of position.
 * @return Offset in translator units of the axis.
 */
double Translator::getAxisCoordinateSystemOffset(const std::string& coordinateSystem, const std::string& axis, Units unit) {

    ::zaber::motion::requests::TranslatorGetAxisOffsetRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setCoordinateSystem(coordinateSystem);
    request.setAxis(axis);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("gcode/get_axis_offset", request, &response);

    return response.getValue();
}

/**
 * Resets internal state after device rejected generated command.
 * Axis positions become uninitialized.
 */
void Translator::resetAfterStreamError() {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::callGateway("gcode/reset_after_stream_error", request);
}

/**
 * Allows to scale feed rate of the translated code by a coefficient.
 * @param coefficient Coefficient of the original feed rate.
 */
void Translator::setFeedRateOverride(double coefficient) {

    ::zaber::motion::requests::TranslatorSetFeedRateOverrideRequest request;
    request.setTranslatorId(this->getTranslatorId());
    request.setCoefficient(coefficient);
    ::zaber::motion::callGateway("gcode/set_feed_rate_override", request);
}

/**
 * Releases native resources of a translator.
 * @param translatorId The ID of the translator.
 */
void Translator::free(int translatorId) {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(translatorId);
    ::zaber::motion::callGateway("gcode/free", request);
}

/**
 * Gets current coordinate system (e.g. G54).
 * @return Current coordinate system.
 */
std::string Translator::getCurrentCoordinateSystem() const {

    ::zaber::motion::requests::TranslatorEmptyRequest request;
    request.setTranslatorId(this->getTranslatorId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("gcode/get_current_coordinate_system", request, &response);

    return response.getValue();
}

/**
 * The ID of the translator that serves to identify native resources.
 */
int Translator::getTranslatorId() const {
    return this->_translatorId;
}

/**
 * Current coordinate system.
 */
std::string Translator::getCoordinateSystem() const {
    return this->getCurrentCoordinateSystem();
}


Translator& Translator::operator=(Translator&& other) noexcept {
    if (this != &other) {
        if (this->_translatorId >= 0) {
            Translator::free(this->_translatorId);
            this->_translatorId = -1;
        }

        std::swap(_translatorId, other._translatorId);
    }
    return *this;
}

Translator::Translator(Translator&& other) noexcept: _translatorId(-1) {
    *this = std::move(other);
}

Translator::~Translator() {
    if (this->_translatorId >= 0) {
        Translator::free(this->_translatorId);
    }
}

}  // namespace gcode
}  // namespace motion
}  // namespace zaber
