// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/error_code.h"

namespace zaber {
namespace motion {
namespace binary {

std::string ErrorCode_toString(ErrorCode value) {
    switch (value) {
        case ErrorCode::CANNOT_HOME: return "CANNOT_HOME";
        case ErrorCode::DEVICE_NUMBER_INVALID: return "DEVICE_NUMBER_INVALID";
        case ErrorCode::ADDRESS_INVALID: return "ADDRESS_INVALID";
        case ErrorCode::VOLTAGE_LOW: return "VOLTAGE_LOW";
        case ErrorCode::VOLTAGE_HIGH: return "VOLTAGE_HIGH";
        case ErrorCode::STORED_POSITION_INVALID: return "STORED_POSITION_INVALID";
        case ErrorCode::ABSOLUTE_POSITION_INVALID: return "ABSOLUTE_POSITION_INVALID";
        case ErrorCode::RELATIVE_POSITION_INVALID: return "RELATIVE_POSITION_INVALID";
        case ErrorCode::VELOCITY_INVALID: return "VELOCITY_INVALID";
        case ErrorCode::AXIS_INVALID: return "AXIS_INVALID";
        case ErrorCode::AXIS_DEVICE_NUMBER_INVALID: return "AXIS_DEVICE_NUMBER_INVALID";
        case ErrorCode::INVERSION_INVALID: return "INVERSION_INVALID";
        case ErrorCode::VELOCITY_PROFILE_INVALID: return "VELOCITY_PROFILE_INVALID";
        case ErrorCode::VELOCITY_SCALE_INVALID: return "VELOCITY_SCALE_INVALID";
        case ErrorCode::LOAD_EVENT_INVALID: return "LOAD_EVENT_INVALID";
        case ErrorCode::RETURN_EVENT_INVALID: return "RETURN_EVENT_INVALID";
        case ErrorCode::JOYSTICK_CALIBRATION_MODE_INVALID: return "JOYSTICK_CALIBRATION_MODE_INVALID";
        case ErrorCode::PERIPHERAL_ID_INVALID: return "PERIPHERAL_ID_INVALID";
        case ErrorCode::RESOLUTION_INVALID: return "RESOLUTION_INVALID";
        case ErrorCode::RUN_CURRENT_INVALID: return "RUN_CURRENT_INVALID";
        case ErrorCode::HOLD_CURRENT_INVALID: return "HOLD_CURRENT_INVALID";
        case ErrorCode::MODE_INVALID: return "MODE_INVALID";
        case ErrorCode::HOME_SPEED_INVALID: return "HOME_SPEED_INVALID";
        case ErrorCode::SPEED_INVALID: return "SPEED_INVALID";
        case ErrorCode::ACCELERATION_INVALID: return "ACCELERATION_INVALID";
        case ErrorCode::MAXIMUM_POSITION_INVALID: return "MAXIMUM_POSITION_INVALID";
        case ErrorCode::CURRENT_POSITION_INVALID: return "CURRENT_POSITION_INVALID";
        case ErrorCode::MAXIMUM_RELATIVE_MOVE_INVALID: return "MAXIMUM_RELATIVE_MOVE_INVALID";
        case ErrorCode::OFFSET_INVALID: return "OFFSET_INVALID";
        case ErrorCode::ALIAS_INVALID: return "ALIAS_INVALID";
        case ErrorCode::LOCK_STATE_INVALID: return "LOCK_STATE_INVALID";
        case ErrorCode::DEVICE_ID_UNKNOWN: return "DEVICE_ID_UNKNOWN";
        case ErrorCode::SETTING_INVALID: return "SETTING_INVALID";
        case ErrorCode::COMMAND_INVALID: return "COMMAND_INVALID";
        case ErrorCode::PARK_STATE_INVALID: return "PARK_STATE_INVALID";
        case ErrorCode::TEMPERATURE_HIGH: return "TEMPERATURE_HIGH";
        case ErrorCode::DIGITAL_INPUT_PIN_INVALID: return "DIGITAL_INPUT_PIN_INVALID";
        case ErrorCode::DIGITAL_OUTPUT_PIN_INVALID: return "DIGITAL_OUTPUT_PIN_INVALID";
        case ErrorCode::DIGITAL_OUTPUT_MASK_INVALID: return "DIGITAL_OUTPUT_MASK_INVALID";
        case ErrorCode::ANALOG_INPUT_PIN_INVALID: return "ANALOG_INPUT_PIN_INVALID";
        case ErrorCode::MOVE_INDEX_NUMBER_INVALID: return "MOVE_INDEX_NUMBER_INVALID";
        case ErrorCode::INDEX_DISTANCE_INVALID: return "INDEX_DISTANCE_INVALID";
        case ErrorCode::CYCLE_DISTANCE_INVALID: return "CYCLE_DISTANCE_INVALID";
        case ErrorCode::FILTER_HOLDER_ID_INVALID: return "FILTER_HOLDER_ID_INVALID";
        case ErrorCode::ABSOLUTE_FORCE_INVALID: return "ABSOLUTE_FORCE_INVALID";
        case ErrorCode::AUTO_REPLY_DISABLED_MODE_INVALID: return "AUTO_REPLY_DISABLED_MODE_INVALID";
        case ErrorCode::MESSAGE_ID_MODE_INVALID: return "MESSAGE_ID_MODE_INVALID";
        case ErrorCode::HOME_STATUS_INVALID: return "HOME_STATUS_INVALID";
        case ErrorCode::HOME_SENSOR_TYPE_INVALID: return "HOME_SENSOR_TYPE_INVALID";
        case ErrorCode::AUTO_HOME_DISABLED_MODE_INVALID: return "AUTO_HOME_DISABLED_MODE_INVALID";
        case ErrorCode::MINIMUM_POSITION_INVALID: return "MINIMUM_POSITION_INVALID";
        case ErrorCode::KNOB_DISABLED_MODE_INVALID: return "KNOB_DISABLED_MODE_INVALID";
        case ErrorCode::KNOB_DIRECTION_INVALID: return "KNOB_DIRECTION_INVALID";
        case ErrorCode::KNOB_MOVEMENT_MODE_INVALID: return "KNOB_MOVEMENT_MODE_INVALID";
        case ErrorCode::KNOB_JOG_SIZE_INVALID: return "KNOB_JOG_SIZE_INVALID";
        case ErrorCode::KNOB_VELOCITY_SCALE_INVALID: return "KNOB_VELOCITY_SCALE_INVALID";
        case ErrorCode::KNOB_VELOCITY_PROFILE_INVALID: return "KNOB_VELOCITY_PROFILE_INVALID";
        case ErrorCode::ACCELERATION_ONLY_INVALID: return "ACCELERATION_ONLY_INVALID";
        case ErrorCode::DECELERATION_ONLY_INVALID: return "DECELERATION_ONLY_INVALID";
        case ErrorCode::MOVE_TRACKING_MODE_INVALID: return "MOVE_TRACKING_MODE_INVALID";
        case ErrorCode::MANUAL_MOVE_TRACKING_DISABLED_MODE_INVALID: return "MANUAL_MOVE_TRACKING_DISABLED_MODE_INVALID";
        case ErrorCode::MOVE_TRACKING_PERIOD_INVALID: return "MOVE_TRACKING_PERIOD_INVALID";
        case ErrorCode::CLOSED_LOOP_MODE_INVALID: return "CLOSED_LOOP_MODE_INVALID";
        case ErrorCode::SLIP_TRACKING_PERIOD_INVALID: return "SLIP_TRACKING_PERIOD_INVALID";
        case ErrorCode::STALL_TIMEOUT_INVALID: return "STALL_TIMEOUT_INVALID";
        case ErrorCode::DEVICE_DIRECTION_INVALID: return "DEVICE_DIRECTION_INVALID";
        case ErrorCode::BAUD_RATE_INVALID: return "BAUD_RATE_INVALID";
        case ErrorCode::PROTOCOL_INVALID: return "PROTOCOL_INVALID";
        case ErrorCode::BAUD_RATE_OR_PROTOCOL_INVALID: return "BAUD_RATE_OR_PROTOCOL_INVALID";
        case ErrorCode::BUSY: return "BUSY";
        case ErrorCode::SYSTEM_ERROR: return "SYSTEM_ERROR";
        case ErrorCode::STORAGE_FULL: return "STORAGE_FULL";
        case ErrorCode::REGISTER_ADDRESS_INVALID: return "REGISTER_ADDRESS_INVALID";
        case ErrorCode::REGISTER_VALUE_INVALID: return "REGISTER_VALUE_INVALID";
        case ErrorCode::SAVE_POSITION_INVALID: return "SAVE_POSITION_INVALID";
        case ErrorCode::SAVE_POSITION_NOT_HOMED: return "SAVE_POSITION_NOT_HOMED";
        case ErrorCode::RETURN_POSITION_INVALID: return "RETURN_POSITION_INVALID";
        case ErrorCode::MOVE_POSITION_INVALID: return "MOVE_POSITION_INVALID";
        case ErrorCode::MOVE_POSITION_NOT_HOMED: return "MOVE_POSITION_NOT_HOMED";
        case ErrorCode::RELATIVE_POSITION_LIMITED: return "RELATIVE_POSITION_LIMITED";
        case ErrorCode::SETTINGS_LOCKED: return "SETTINGS_LOCKED";
        case ErrorCode::BIT_1_INVALID: return "BIT_1_INVALID";
        case ErrorCode::BIT_2_INVALID: return "BIT_2_INVALID";
        case ErrorCode::DISABLE_AUTO_HOME_INVALID: return "DISABLE_AUTO_HOME_INVALID";
        case ErrorCode::BIT_10_INVALID: return "BIT_10_INVALID";
        case ErrorCode::BIT_11_INVALID: return "BIT_11_INVALID";
        case ErrorCode::HOME_SWITCH_INVALID: return "HOME_SWITCH_INVALID";
        case ErrorCode::BIT_13_INVALID: return "BIT_13_INVALID";
        case ErrorCode::BIT_14_INVALID: return "BIT_14_INVALID";
        case ErrorCode::BIT_15_INVALID: return "BIT_15_INVALID";
        case ErrorCode::DEVICE_PARKED: return "DEVICE_PARKED";
        case ErrorCode::DRIVER_DISABLED: return "DRIVER_DISABLED";
    }
    return "<Invalid value>";
}

} // namespace binary
} // namespace motion
} // namespace zaber
