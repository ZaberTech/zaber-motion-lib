// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/command_code.h"

namespace zaber {
namespace motion {
namespace binary {

std::string CommandCode_toString(CommandCode value) {
    switch (value) {
        case CommandCode::RESET: return "RESET";
        case CommandCode::HOME: return "HOME";
        case CommandCode::RENUMBER: return "RENUMBER";
        case CommandCode::STORE_CURRENT_POSITION: return "STORE_CURRENT_POSITION";
        case CommandCode::RETURN_STORED_POSITION: return "RETURN_STORED_POSITION";
        case CommandCode::MOVE_TO_STORED_POSITION: return "MOVE_TO_STORED_POSITION";
        case CommandCode::MOVE_ABSOLUTE: return "MOVE_ABSOLUTE";
        case CommandCode::MOVE_RELATIVE: return "MOVE_RELATIVE";
        case CommandCode::MOVE_AT_CONSTANT_SPEED: return "MOVE_AT_CONSTANT_SPEED";
        case CommandCode::STOP: return "STOP";
        case CommandCode::SET_ACTIVE_AXIS: return "SET_ACTIVE_AXIS";
        case CommandCode::SET_AXIS_DEVICE_NUMBER: return "SET_AXIS_DEVICE_NUMBER";
        case CommandCode::SET_AXIS_INVERSION: return "SET_AXIS_INVERSION";
        case CommandCode::SET_AXIS_VELOCITY_PROFILE: return "SET_AXIS_VELOCITY_PROFILE";
        case CommandCode::SET_AXIS_VELOCITY_SCALE: return "SET_AXIS_VELOCITY_SCALE";
        case CommandCode::LOAD_EVENT_INSTRUCTION: return "LOAD_EVENT_INSTRUCTION";
        case CommandCode::RETURN_EVENT_INSTRUCTION: return "RETURN_EVENT_INSTRUCTION";
        case CommandCode::SET_CALIBRATION_MODE: return "SET_CALIBRATION_MODE";
        case CommandCode::READ_OR_WRITE_MEMORY: return "READ_OR_WRITE_MEMORY";
        case CommandCode::RESTORE_SETTINGS: return "RESTORE_SETTINGS";
        case CommandCode::SET_MICROSTEP_RESOLUTION: return "SET_MICROSTEP_RESOLUTION";
        case CommandCode::SET_RUNNING_CURRENT: return "SET_RUNNING_CURRENT";
        case CommandCode::SET_HOLD_CURRENT: return "SET_HOLD_CURRENT";
        case CommandCode::SET_DEVICE_MODE: return "SET_DEVICE_MODE";
        case CommandCode::SET_HOME_SPEED: return "SET_HOME_SPEED";
        case CommandCode::SET_TARGET_SPEED: return "SET_TARGET_SPEED";
        case CommandCode::SET_ACCELERATION: return "SET_ACCELERATION";
        case CommandCode::SET_MAXIMUM_POSITION: return "SET_MAXIMUM_POSITION";
        case CommandCode::SET_CURRENT_POSITION: return "SET_CURRENT_POSITION";
        case CommandCode::SET_MAXIMUM_RELATIVE_MOVE: return "SET_MAXIMUM_RELATIVE_MOVE";
        case CommandCode::SET_HOME_OFFSET: return "SET_HOME_OFFSET";
        case CommandCode::SET_ALIAS_NUMBER: return "SET_ALIAS_NUMBER";
        case CommandCode::SET_LOCK_STATE: return "SET_LOCK_STATE";
        case CommandCode::RETURN_DEVICE_ID: return "RETURN_DEVICE_ID";
        case CommandCode::RETURN_FIRMWARE_VERSION: return "RETURN_FIRMWARE_VERSION";
        case CommandCode::RETURN_POWER_SUPPLY_VOLTAGE: return "RETURN_POWER_SUPPLY_VOLTAGE";
        case CommandCode::RETURN_SETTING: return "RETURN_SETTING";
        case CommandCode::RETURN_STATUS: return "RETURN_STATUS";
        case CommandCode::ECHO_DATA: return "ECHO_DATA";
        case CommandCode::RETURN_FIRMWARE_BUILD: return "RETURN_FIRMWARE_BUILD";
        case CommandCode::RETURN_CURRENT_POSITION: return "RETURN_CURRENT_POSITION";
        case CommandCode::RETURN_SERIAL_NUMBER: return "RETURN_SERIAL_NUMBER";
        case CommandCode::SET_PARK_STATE: return "SET_PARK_STATE";
        case CommandCode::SET_PERIPHERAL_ID: return "SET_PERIPHERAL_ID";
        case CommandCode::RETURN_DIGITAL_INPUT_COUNT: return "RETURN_DIGITAL_INPUT_COUNT";
        case CommandCode::READ_DIGITAL_INPUT: return "READ_DIGITAL_INPUT";
        case CommandCode::READ_ALL_DIGITAL_INPUTS: return "READ_ALL_DIGITAL_INPUTS";
        case CommandCode::RETURN_DIGITAL_OUTPUT_COUNT: return "RETURN_DIGITAL_OUTPUT_COUNT";
        case CommandCode::READ_DIGITAL_OUTPUT: return "READ_DIGITAL_OUTPUT";
        case CommandCode::READ_ALL_DIGITAL_OUTPUTS: return "READ_ALL_DIGITAL_OUTPUTS";
        case CommandCode::WRITE_DIGITAL_OUTPUT: return "WRITE_DIGITAL_OUTPUT";
        case CommandCode::WRITE_ALL_DIGITAL_OUTPUTS: return "WRITE_ALL_DIGITAL_OUTPUTS";
        case CommandCode::RETURN_ANALOG_INPUT_COUNT: return "RETURN_ANALOG_INPUT_COUNT";
        case CommandCode::READ_ANALOG_INPUT: return "READ_ANALOG_INPUT";
        case CommandCode::RETURN_ANALOG_OUTPUT_COUNT: return "RETURN_ANALOG_OUTPUT_COUNT";
        case CommandCode::MOVE_INDEX: return "MOVE_INDEX";
        case CommandCode::SET_INDEX_DISTANCE: return "SET_INDEX_DISTANCE";
        case CommandCode::SET_CYCLE_DISTANCE: return "SET_CYCLE_DISTANCE";
        case CommandCode::SET_FILTER_HOLDER_ID: return "SET_FILTER_HOLDER_ID";
        case CommandCode::RETURN_ENCODER_COUNT: return "RETURN_ENCODER_COUNT";
        case CommandCode::RETURN_CALIBRATED_ENCODER_COUNT: return "RETURN_CALIBRATED_ENCODER_COUNT";
        case CommandCode::RETURN_CALIBRATION_TYPE: return "RETURN_CALIBRATION_TYPE";
        case CommandCode::RETURN_CALIBRATION_ERROR: return "RETURN_CALIBRATION_ERROR";
        case CommandCode::RETURN_PERIPHERAL_SERIAL_NUMBER: return "RETURN_PERIPHERAL_SERIAL_NUMBER";
        case CommandCode::FORCE_ABSOLUTE: return "FORCE_ABSOLUTE";
        case CommandCode::FORCE_OFF: return "FORCE_OFF";
        case CommandCode::RETURN_ENCODER_POSITION: return "RETURN_ENCODER_POSITION";
        case CommandCode::RETURN_PERIPHERAL_ID_PENDING: return "RETURN_PERIPHERAL_ID_PENDING";
        case CommandCode::RETURN_PERIPHERAL_SERIAL_PENDING: return "RETURN_PERIPHERAL_SERIAL_PENDING";
        case CommandCode::ACTIVATE: return "ACTIVATE";
        case CommandCode::SET_AUTO_REPLY_DISABLED_MODE: return "SET_AUTO_REPLY_DISABLED_MODE";
        case CommandCode::SET_MESSAGE_ID_MODE: return "SET_MESSAGE_ID_MODE";
        case CommandCode::SET_HOME_STATUS: return "SET_HOME_STATUS";
        case CommandCode::SET_HOME_SENSOR_TYPE: return "SET_HOME_SENSOR_TYPE";
        case CommandCode::SET_AUTO_HOME_DISABLED_MODE: return "SET_AUTO_HOME_DISABLED_MODE";
        case CommandCode::SET_MINIMUM_POSITION: return "SET_MINIMUM_POSITION";
        case CommandCode::SET_KNOB_DISABLED_MODE: return "SET_KNOB_DISABLED_MODE";
        case CommandCode::SET_KNOB_DIRECTION: return "SET_KNOB_DIRECTION";
        case CommandCode::SET_KNOB_MOVEMENT_MODE: return "SET_KNOB_MOVEMENT_MODE";
        case CommandCode::SET_KNOB_JOG_SIZE: return "SET_KNOB_JOG_SIZE";
        case CommandCode::SET_KNOB_VELOCITY_SCALE: return "SET_KNOB_VELOCITY_SCALE";
        case CommandCode::SET_KNOB_VELOCITY_PROFILE: return "SET_KNOB_VELOCITY_PROFILE";
        case CommandCode::SET_ACCELERATION_ONLY: return "SET_ACCELERATION_ONLY";
        case CommandCode::SET_DECELERATION_ONLY: return "SET_DECELERATION_ONLY";
        case CommandCode::SET_MOVE_TRACKING_MODE: return "SET_MOVE_TRACKING_MODE";
        case CommandCode::SET_MANUAL_MOVE_TRACKING_DISABLED_MODE: return "SET_MANUAL_MOVE_TRACKING_DISABLED_MODE";
        case CommandCode::SET_MOVE_TRACKING_PERIOD: return "SET_MOVE_TRACKING_PERIOD";
        case CommandCode::SET_CLOSED_LOOP_MODE: return "SET_CLOSED_LOOP_MODE";
        case CommandCode::SET_SLIP_TRACKING_PERIOD: return "SET_SLIP_TRACKING_PERIOD";
        case CommandCode::SET_STALL_TIMEOUT: return "SET_STALL_TIMEOUT";
        case CommandCode::SET_DEVICE_DIRECTION: return "SET_DEVICE_DIRECTION";
        case CommandCode::SET_BAUD_RATE: return "SET_BAUD_RATE";
        case CommandCode::SET_PROTOCOL: return "SET_PROTOCOL";
        case CommandCode::CONVERT_TO_ASCII: return "CONVERT_TO_ASCII";
    }
    return "<Invalid value>";
}

} // namespace binary
} // namespace motion
} // namespace zaber
