// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/device_identity.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace binary {

DeviceIdentity::DeviceIdentity() { }

DeviceIdentity::DeviceIdentity(
    int p_deviceId,
    unsigned int p_serialNumber,
    std::string p_name,
    FirmwareVersion p_firmwareVersion,
    bool p_isPeripheral,
    int p_peripheralId,
    std::string p_peripheralName,
    DeviceType p_deviceType
) :
    deviceId(p_deviceId),
    serialNumber(p_serialNumber),
    name(std::move(p_name)),
    firmwareVersion(std::move(p_firmwareVersion)),
    isPeripheral(p_isPeripheral),
    peripheralId(p_peripheralId),
    peripheralName(std::move(p_peripheralName)),
    deviceType(std::move(p_deviceType))
{ }

bool DeviceIdentity::operator==(const DeviceIdentity& other) const {
    return std::tie(
    deviceId,
    serialNumber,
    name,
    firmwareVersion,
    isPeripheral,
    peripheralId,
    peripheralName,
    deviceType
    ) == std::tie(
    other.deviceId,
    other.serialNumber,
    other.name,
    other.firmwareVersion,
    other.isPeripheral,
    other.peripheralId,
    other.peripheralName,
    other.deviceType
    );
}

int DeviceIdentity::getDeviceId() const {
    return deviceId;
}
void DeviceIdentity::setDeviceId(int p_deviceId) {
    deviceId = p_deviceId;
}

unsigned int DeviceIdentity::getSerialNumber() const {
    return serialNumber;
}
void DeviceIdentity::setSerialNumber(unsigned int p_serialNumber) {
    serialNumber = p_serialNumber;
}

std::string const& DeviceIdentity::getName() const {
    return name;
}
void DeviceIdentity::setName(std::string p_name) {
    name = std::move(p_name);
}

FirmwareVersion const& DeviceIdentity::getFirmwareVersion() const {
    return firmwareVersion;
}
void DeviceIdentity::setFirmwareVersion(FirmwareVersion p_firmwareVersion) {
    firmwareVersion = std::move(p_firmwareVersion);
}

bool DeviceIdentity::getIsPeripheral() const {
    return isPeripheral;
}
void DeviceIdentity::setIsPeripheral(bool p_isPeripheral) {
    isPeripheral = p_isPeripheral;
}

int DeviceIdentity::getPeripheralId() const {
    return peripheralId;
}
void DeviceIdentity::setPeripheralId(int p_peripheralId) {
    peripheralId = p_peripheralId;
}

std::string const& DeviceIdentity::getPeripheralName() const {
    return peripheralName;
}
void DeviceIdentity::setPeripheralName(std::string p_peripheralName) {
    peripheralName = std::move(p_peripheralName);
}

DeviceType const& DeviceIdentity::getDeviceType() const {
    return deviceType;
}
void DeviceIdentity::setDeviceType(DeviceType p_deviceType) {
    deviceType = std::move(p_deviceType);
}

std::string DeviceIdentity::toString() const {
    std::stringstream ss;
    ss << "DeviceIdentity { ";
    ss << "deviceId: ";
    ss << this->deviceId;
    ss << ", ";
    ss << "serialNumber: ";
    ss << this->serialNumber;
    ss << ", ";
    ss << "name: ";
    ss << this->name;
    ss << ", ";
    ss << "firmwareVersion: ";
    ss << this->firmwareVersion.toString();
    ss << ", ";
    ss << "isPeripheral: ";
    ss << this->isPeripheral;
    ss << ", ";
    ss << "peripheralId: ";
    ss << this->peripheralId;
    ss << ", ";
    ss << "peripheralName: ";
    ss << this->peripheralName;
    ss << ", ";
    ss << "deviceType: ";
    ss << DeviceType_toString(this->deviceType);
    ss << " }";
    return ss.str();
}

void DeviceIdentity::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceIdentity>();
}

std::string DeviceIdentity::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace binary
} // namespace motion
} // namespace zaber
