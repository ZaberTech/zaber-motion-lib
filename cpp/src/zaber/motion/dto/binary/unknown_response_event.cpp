// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/unknown_response_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace binary {

UnknownResponseEvent::UnknownResponseEvent() { }

UnknownResponseEvent::UnknownResponseEvent(
    int p_deviceAddress,
    int p_command,
    int p_data
) :
    deviceAddress(p_deviceAddress),
    command(p_command),
    data(p_data)
{ }

bool UnknownResponseEvent::operator==(const UnknownResponseEvent& other) const {
    return std::tie(
    deviceAddress,
    command,
    data
    ) == std::tie(
    other.deviceAddress,
    other.command,
    other.data
    );
}

int UnknownResponseEvent::getDeviceAddress() const {
    return deviceAddress;
}
void UnknownResponseEvent::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int UnknownResponseEvent::getCommand() const {
    return command;
}
void UnknownResponseEvent::setCommand(int p_command) {
    command = p_command;
}

int UnknownResponseEvent::getData() const {
    return data;
}
void UnknownResponseEvent::setData(int p_data) {
    data = p_data;
}

std::string UnknownResponseEvent::toString() const {
    std::stringstream ss;
    ss << "UnknownResponseEvent { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << " }";
    return ss.str();
}

void UnknownResponseEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnknownResponseEvent>();
}

std::string UnknownResponseEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace binary
} // namespace motion
} // namespace zaber
