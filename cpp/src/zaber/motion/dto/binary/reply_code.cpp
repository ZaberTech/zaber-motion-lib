// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/reply_code.h"

#pragma push_macro("ERROR")
#undef ERROR

namespace zaber {
namespace motion {
namespace binary {

std::string ReplyCode_toString(ReplyCode value) {
    switch (value) {
        case ReplyCode::MOVE_TRACKING: return "MOVE_TRACKING";
        case ReplyCode::LIMIT_ACTIVE: return "LIMIT_ACTIVE";
        case ReplyCode::MANUAL_MOVE_TRACKING: return "MANUAL_MOVE_TRACKING";
        case ReplyCode::MANUAL_MOVE: return "MANUAL_MOVE";
        case ReplyCode::SLIP_TRACKING: return "SLIP_TRACKING";
        case ReplyCode::UNEXPECTED_POSITION: return "UNEXPECTED_POSITION";
        case ReplyCode::ERROR: return "ERROR";
    }
    return "<Invalid value>";
}

} // namespace binary
} // namespace motion
} // namespace zaber

#pragma pop_macro("ERROR")
