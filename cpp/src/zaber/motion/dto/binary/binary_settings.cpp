// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/binary_settings.h"

namespace zaber {
namespace motion {
namespace binary {

std::string BinarySettings_toString(BinarySettings value) {
    switch (value) {
        case BinarySettings::ACCELERATION: return "ACCELERATION";
        case BinarySettings::ACCELERATION_ONLY: return "ACCELERATION_ONLY";
        case BinarySettings::ACTIVE_AXIS: return "ACTIVE_AXIS";
        case BinarySettings::ALIAS_NUMBER: return "ALIAS_NUMBER";
        case BinarySettings::ANALOG_INPUT_COUNT: return "ANALOG_INPUT_COUNT";
        case BinarySettings::ANALOG_OUTPUT_COUNT: return "ANALOG_OUTPUT_COUNT";
        case BinarySettings::AUTO_HOME_DISABLED_MODE: return "AUTO_HOME_DISABLED_MODE";
        case BinarySettings::AUTO_REPLY_DISABLED_MODE: return "AUTO_REPLY_DISABLED_MODE";
        case BinarySettings::AXIS_DEVICE_NUMBER: return "AXIS_DEVICE_NUMBER";
        case BinarySettings::AXIS_INVERSION: return "AXIS_INVERSION";
        case BinarySettings::AXIS_VELOCITY_PROFILE: return "AXIS_VELOCITY_PROFILE";
        case BinarySettings::AXIS_VELOCITY_SCALE: return "AXIS_VELOCITY_SCALE";
        case BinarySettings::BAUD_RATE: return "BAUD_RATE";
        case BinarySettings::CALIBRATED_ENCODER_COUNT: return "CALIBRATED_ENCODER_COUNT";
        case BinarySettings::CALIBRATION_ERROR: return "CALIBRATION_ERROR";
        case BinarySettings::CALIBRATION_TYPE: return "CALIBRATION_TYPE";
        case BinarySettings::CLOSED_LOOP_MODE: return "CLOSED_LOOP_MODE";
        case BinarySettings::CURRENT_POSITION: return "CURRENT_POSITION";
        case BinarySettings::CYCLE_DISTANCE: return "CYCLE_DISTANCE";
        case BinarySettings::DECELERATION_ONLY: return "DECELERATION_ONLY";
        case BinarySettings::DEVICE_DIRECTION: return "DEVICE_DIRECTION";
        case BinarySettings::DEVICE_ID: return "DEVICE_ID";
        case BinarySettings::DEVICE_MODE: return "DEVICE_MODE";
        case BinarySettings::DIGITAL_INPUT_COUNT: return "DIGITAL_INPUT_COUNT";
        case BinarySettings::DIGITAL_OUTPUT_COUNT: return "DIGITAL_OUTPUT_COUNT";
        case BinarySettings::ENCODER_COUNT: return "ENCODER_COUNT";
        case BinarySettings::ENCODER_POSITION: return "ENCODER_POSITION";
        case BinarySettings::FILTER_HOLDER_ID: return "FILTER_HOLDER_ID";
        case BinarySettings::FIRMWARE_BUILD: return "FIRMWARE_BUILD";
        case BinarySettings::FIRMWARE_VERSION: return "FIRMWARE_VERSION";
        case BinarySettings::HOLD_CURRENT: return "HOLD_CURRENT";
        case BinarySettings::HOME_OFFSET: return "HOME_OFFSET";
        case BinarySettings::HOME_SENSOR_TYPE: return "HOME_SENSOR_TYPE";
        case BinarySettings::HOME_SPEED: return "HOME_SPEED";
        case BinarySettings::HOME_STATUS: return "HOME_STATUS";
        case BinarySettings::INDEX_DISTANCE: return "INDEX_DISTANCE";
        case BinarySettings::JOYSTICK_CALIBRATION_MODE: return "JOYSTICK_CALIBRATION_MODE";
        case BinarySettings::KNOB_DIRECTION: return "KNOB_DIRECTION";
        case BinarySettings::KNOB_DISABLED_MODE: return "KNOB_DISABLED_MODE";
        case BinarySettings::KNOB_JOG_SIZE: return "KNOB_JOG_SIZE";
        case BinarySettings::KNOB_MOVEMENT_MODE: return "KNOB_MOVEMENT_MODE";
        case BinarySettings::KNOB_VELOCITY_PROFILE: return "KNOB_VELOCITY_PROFILE";
        case BinarySettings::KNOB_VELOCITY_SCALE: return "KNOB_VELOCITY_SCALE";
        case BinarySettings::LOCK_STATE: return "LOCK_STATE";
        case BinarySettings::MANUAL_MOVE_TRACKING_DISABLED_MODE: return "MANUAL_MOVE_TRACKING_DISABLED_MODE";
        case BinarySettings::MAXIMUM_POSITION: return "MAXIMUM_POSITION";
        case BinarySettings::MAXIMUM_RELATIVE_MOVE: return "MAXIMUM_RELATIVE_MOVE";
        case BinarySettings::MESSAGE_ID_MODE: return "MESSAGE_ID_MODE";
        case BinarySettings::MICROSTEP_RESOLUTION: return "MICROSTEP_RESOLUTION";
        case BinarySettings::MINIMUM_POSITION: return "MINIMUM_POSITION";
        case BinarySettings::MOVE_TRACKING_MODE: return "MOVE_TRACKING_MODE";
        case BinarySettings::MOVE_TRACKING_PERIOD: return "MOVE_TRACKING_PERIOD";
        case BinarySettings::PARK_STATE: return "PARK_STATE";
        case BinarySettings::PERIPHERAL_ID: return "PERIPHERAL_ID";
        case BinarySettings::PERIPHERAL_ID_PENDING: return "PERIPHERAL_ID_PENDING";
        case BinarySettings::PERIPHERAL_SERIAL_NUMBER: return "PERIPHERAL_SERIAL_NUMBER";
        case BinarySettings::PERIPHERAL_SERIAL_PENDING: return "PERIPHERAL_SERIAL_PENDING";
        case BinarySettings::POWER_SUPPLY_VOLTAGE: return "POWER_SUPPLY_VOLTAGE";
        case BinarySettings::PROTOCOL: return "PROTOCOL";
        case BinarySettings::RUNNING_CURRENT: return "RUNNING_CURRENT";
        case BinarySettings::SERIAL_NUMBER: return "SERIAL_NUMBER";
        case BinarySettings::SLIP_TRACKING_PERIOD: return "SLIP_TRACKING_PERIOD";
        case BinarySettings::STALL_TIMEOUT: return "STALL_TIMEOUT";
        case BinarySettings::STATUS: return "STATUS";
        case BinarySettings::TARGET_SPEED: return "TARGET_SPEED";
    }
    return "<Invalid value>";
}

} // namespace binary
} // namespace motion
} // namespace zaber
