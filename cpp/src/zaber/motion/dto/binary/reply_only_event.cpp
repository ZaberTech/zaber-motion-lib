// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/reply_only_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace binary {

ReplyOnlyEvent::ReplyOnlyEvent() { }

ReplyOnlyEvent::ReplyOnlyEvent(
    int p_deviceAddress,
    int p_command,
    int p_data
) :
    deviceAddress(p_deviceAddress),
    command(p_command),
    data(p_data)
{ }

bool ReplyOnlyEvent::operator==(const ReplyOnlyEvent& other) const {
    return std::tie(
    deviceAddress,
    command,
    data
    ) == std::tie(
    other.deviceAddress,
    other.command,
    other.data
    );
}

int ReplyOnlyEvent::getDeviceAddress() const {
    return deviceAddress;
}
void ReplyOnlyEvent::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int ReplyOnlyEvent::getCommand() const {
    return command;
}
void ReplyOnlyEvent::setCommand(int p_command) {
    command = p_command;
}

int ReplyOnlyEvent::getData() const {
    return data;
}
void ReplyOnlyEvent::setData(int p_data) {
    data = p_data;
}

std::string ReplyOnlyEvent::toString() const {
    std::stringstream ss;
    ss << "ReplyOnlyEvent { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << " }";
    return ss.str();
}

void ReplyOnlyEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ReplyOnlyEvent>();
}

std::string ReplyOnlyEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace binary
} // namespace motion
} // namespace zaber
