// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/binary/reply_only_event.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace binary {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ReplyOnlyEvent, deviceAddress, command, data)

} // namespace binary
} // namespace motion
} // namespace zaber
