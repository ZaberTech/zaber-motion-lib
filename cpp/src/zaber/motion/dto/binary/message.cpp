// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/message.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace binary {

Message::Message() { }

Message::Message(
    int p_deviceAddress,
    int p_command,
    int p_data
) :
    deviceAddress(p_deviceAddress),
    command(p_command),
    data(p_data)
{ }

bool Message::operator==(const Message& other) const {
    return std::tie(
    deviceAddress,
    command,
    data
    ) == std::tie(
    other.deviceAddress,
    other.command,
    other.data
    );
}

int Message::getDeviceAddress() const {
    return deviceAddress;
}
void Message::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int Message::getCommand() const {
    return command;
}
void Message::setCommand(int p_command) {
    command = p_command;
}

int Message::getData() const {
    return data;
}
void Message::setData(int p_data) {
    data = p_data;
}

std::string Message::toString() const {
    std::stringstream ss;
    ss << "Message { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << " }";
    return ss.str();
}

void Message::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<Message>();
}

std::string Message::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace binary
} // namespace motion
} // namespace zaber
