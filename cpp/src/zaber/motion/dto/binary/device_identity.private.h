// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/binary/device_identity.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/firmware_version.private.h"

namespace zaber {
namespace motion {
namespace binary {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceIdentity, deviceId, serialNumber, name, firmwareVersion, isPeripheral, peripheralId, peripheralName, deviceType)

} // namespace binary
} // namespace motion
} // namespace zaber
