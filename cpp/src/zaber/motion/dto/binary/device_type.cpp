// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/binary/device_type.h"

namespace zaber {
namespace motion {
namespace binary {

std::string DeviceType_toString(DeviceType value) {
    switch (value) {
        case DeviceType::UNKNOWN: return "UNKNOWN";
        case DeviceType::LINEAR: return "LINEAR";
        case DeviceType::ROTARY: return "ROTARY";
    }
    return "<Invalid value>";
}

} // namespace binary
} // namespace motion
} // namespace zaber
