// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/log_output_mode.h"

namespace zaber {
namespace motion {

std::string LogOutputMode_toString(LogOutputMode value) {
    switch (value) {
        case LogOutputMode::OFF: return "OFF";
        case LogOutputMode::STDOUT: return "STDOUT";
        case LogOutputMode::STDERR: return "STDERR";
        case LogOutputMode::FILE: return "FILE";
    }
    return "<Invalid value>";
}

} // namespace motion
} // namespace zaber
