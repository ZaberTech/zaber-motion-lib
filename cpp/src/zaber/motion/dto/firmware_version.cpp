// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/firmware_version.private.h"

#include <sstream>

#pragma push_macro("major")
#undef major
#pragma push_macro("minor")
#undef minor

namespace zaber {
namespace motion {

FirmwareVersion::FirmwareVersion() { }

FirmwareVersion::FirmwareVersion(
    int p_major,
    int p_minor,
    int p_build
) :
    major(p_major),
    minor(p_minor),
    build(p_build)
{ }

bool FirmwareVersion::operator==(const FirmwareVersion& other) const {
    return std::tie(
    major,
    minor,
    build
    ) == std::tie(
    other.major,
    other.minor,
    other.build
    );
}

int FirmwareVersion::getMajor() const {
    return major;
}
void FirmwareVersion::setMajor(int p_major) {
    major = p_major;
}

int FirmwareVersion::getMinor() const {
    return minor;
}
void FirmwareVersion::setMinor(int p_minor) {
    minor = p_minor;
}

int FirmwareVersion::getBuild() const {
    return build;
}
void FirmwareVersion::setBuild(int p_build) {
    build = p_build;
}

std::string FirmwareVersion::toString() const {
    std::stringstream ss;
    ss << "FirmwareVersion { ";
    ss << "major: ";
    ss << this->major;
    ss << ", ";
    ss << "minor: ";
    ss << this->minor;
    ss << ", ";
    ss << "build: ";
    ss << this->build;
    ss << " }";
    return ss.str();
}

void FirmwareVersion::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<FirmwareVersion>();
}

std::string FirmwareVersion::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace motion
} // namespace zaber

#pragma pop_macro("major")
#pragma pop_macro("minor")
