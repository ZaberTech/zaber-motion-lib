// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/device_db_source_type.h"

namespace zaber {
namespace motion {

std::string DeviceDbSourceType_toString(DeviceDbSourceType value) {
    switch (value) {
        case DeviceDbSourceType::WEB_SERVICE: return "WEB_SERVICE";
        case DeviceDbSourceType::FILE: return "FILE";
    }
    return "<Invalid value>";
}

} // namespace motion
} // namespace zaber
