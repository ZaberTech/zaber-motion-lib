// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/product/process_controller_mode.h"

namespace zaber {
namespace motion {
namespace product {

std::string ProcessControllerMode_toString(ProcessControllerMode value) {
    switch (value) {
        case ProcessControllerMode::MANUAL: return "MANUAL";
        case ProcessControllerMode::PID: return "PID";
        case ProcessControllerMode::PID_HEATER: return "PID_HEATER";
        case ProcessControllerMode::ON_OFF: return "ON_OFF";
    }
    return "<Invalid value>";
}

} // namespace product
} // namespace motion
} // namespace zaber
