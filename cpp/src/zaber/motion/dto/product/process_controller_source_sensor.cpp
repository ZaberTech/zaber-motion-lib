// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/product/process_controller_source_sensor.h"

namespace zaber {
namespace motion {
namespace product {

std::string ProcessControllerSourceSensor_toString(ProcessControllerSourceSensor value) {
    switch (value) {
        case ProcessControllerSourceSensor::THERMISTOR: return "THERMISTOR";
        case ProcessControllerSourceSensor::ANALOG_INPUT: return "ANALOG_INPUT";
    }
    return "<Invalid value>";
}

} // namespace product
} // namespace motion
} // namespace zaber
