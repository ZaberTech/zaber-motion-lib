// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/product/process_controller_source.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace product {

ProcessControllerSource::ProcessControllerSource() { }

ProcessControllerSource::ProcessControllerSource(
    ProcessControllerSourceSensor p_sensor,
    int p_port
) :
    sensor(std::move(p_sensor)),
    port(p_port)
{ }

bool ProcessControllerSource::operator==(const ProcessControllerSource& other) const {
    return std::tie(
    sensor,
    port
    ) == std::tie(
    other.sensor,
    other.port
    );
}

ProcessControllerSourceSensor const& ProcessControllerSource::getSensor() const {
    return sensor;
}
void ProcessControllerSource::setSensor(ProcessControllerSourceSensor p_sensor) {
    sensor = std::move(p_sensor);
}

int ProcessControllerSource::getPort() const {
    return port;
}
void ProcessControllerSource::setPort(int p_port) {
    port = p_port;
}

std::string ProcessControllerSource::toString() const {
    std::stringstream ss;
    ss << "ProcessControllerSource { ";
    ss << "sensor: ";
    ss << ProcessControllerSourceSensor_toString(this->sensor);
    ss << ", ";
    ss << "port: ";
    ss << this->port;
    ss << " }";
    return ss.str();
}

void ProcessControllerSource::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ProcessControllerSource>();
}

std::string ProcessControllerSource::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace product
} // namespace motion
} // namespace zaber
