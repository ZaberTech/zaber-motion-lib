// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/product/process_controller_source.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace product {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ProcessControllerSource, sensor, port)

} // namespace product
} // namespace motion
} // namespace zaber
