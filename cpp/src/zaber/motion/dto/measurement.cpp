// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/measurement.private.h"

#include <sstream>

namespace zaber {
namespace motion {

Measurement::Measurement() { }

Measurement::Measurement(
    double p_value,
    std::optional<Units> p_unit
) :
    value(p_value),
    unit(p_unit)
{ }

Measurement::Measurement(
    double p_value
) :
    value(p_value)
{ }

bool Measurement::operator==(const Measurement& other) const {
    return std::tie(
    value,
    unit
    ) == std::tie(
    other.value,
    other.unit
    );
}

double Measurement::getValue() const {
    return value;
}
void Measurement::setValue(double p_value) {
    value = p_value;
}

std::optional<Units> Measurement::getUnit() const {
    return unit;
}
void Measurement::setUnit(std::optional<Units> p_unit) {
    unit = p_unit;
}

std::string Measurement::toString() const {
    std::stringstream ss;
    ss << "Measurement { ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    if (this->unit.has_value()) {
        ss << getUnitLongName(this->unit.value());
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void Measurement::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<Measurement>();
}

std::string Measurement::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace motion
} // namespace zaber
