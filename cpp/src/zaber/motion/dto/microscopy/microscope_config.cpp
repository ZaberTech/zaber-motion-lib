// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/microscopy/microscope_config.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace microscopy {

MicroscopeConfig::MicroscopeConfig() { }

MicroscopeConfig::MicroscopeConfig(
    std::optional<AxisAddress> p_focusAxis,
    std::optional<AxisAddress> p_xAxis,
    std::optional<AxisAddress> p_yAxis,
    std::optional<int> p_illuminator,
    std::optional<int> p_filterChanger,
    std::optional<int> p_objectiveChanger,
    std::optional<int> p_autofocus,
    std::optional<ChannelAddress> p_cameraTrigger
) :
    focusAxis(std::move(p_focusAxis)),
    xAxis(std::move(p_xAxis)),
    yAxis(std::move(p_yAxis)),
    illuminator(p_illuminator),
    filterChanger(p_filterChanger),
    objectiveChanger(p_objectiveChanger),
    autofocus(p_autofocus),
    cameraTrigger(std::move(p_cameraTrigger))
{ }

bool MicroscopeConfig::operator==(const MicroscopeConfig& other) const {
    return std::tie(
    focusAxis,
    xAxis,
    yAxis,
    illuminator,
    filterChanger,
    objectiveChanger,
    autofocus,
    cameraTrigger
    ) == std::tie(
    other.focusAxis,
    other.xAxis,
    other.yAxis,
    other.illuminator,
    other.filterChanger,
    other.objectiveChanger,
    other.autofocus,
    other.cameraTrigger
    );
}

std::optional<AxisAddress> const& MicroscopeConfig::getFocusAxis() const {
    return focusAxis;
}
void MicroscopeConfig::setFocusAxis(std::optional<AxisAddress> p_focusAxis) {
    focusAxis = std::move(p_focusAxis);
}

std::optional<AxisAddress> const& MicroscopeConfig::getXAxis() const {
    return xAxis;
}
void MicroscopeConfig::setXAxis(std::optional<AxisAddress> p_xAxis) {
    xAxis = std::move(p_xAxis);
}

std::optional<AxisAddress> const& MicroscopeConfig::getYAxis() const {
    return yAxis;
}
void MicroscopeConfig::setYAxis(std::optional<AxisAddress> p_yAxis) {
    yAxis = std::move(p_yAxis);
}

std::optional<int> MicroscopeConfig::getIlluminator() const {
    return illuminator;
}
void MicroscopeConfig::setIlluminator(std::optional<int> p_illuminator) {
    illuminator = p_illuminator;
}

std::optional<int> MicroscopeConfig::getFilterChanger() const {
    return filterChanger;
}
void MicroscopeConfig::setFilterChanger(std::optional<int> p_filterChanger) {
    filterChanger = p_filterChanger;
}

std::optional<int> MicroscopeConfig::getObjectiveChanger() const {
    return objectiveChanger;
}
void MicroscopeConfig::setObjectiveChanger(std::optional<int> p_objectiveChanger) {
    objectiveChanger = p_objectiveChanger;
}

std::optional<int> MicroscopeConfig::getAutofocus() const {
    return autofocus;
}
void MicroscopeConfig::setAutofocus(std::optional<int> p_autofocus) {
    autofocus = p_autofocus;
}

std::optional<ChannelAddress> const& MicroscopeConfig::getCameraTrigger() const {
    return cameraTrigger;
}
void MicroscopeConfig::setCameraTrigger(std::optional<ChannelAddress> p_cameraTrigger) {
    cameraTrigger = std::move(p_cameraTrigger);
}

std::string MicroscopeConfig::toString() const {
    std::stringstream ss;
    ss << "MicroscopeConfig { ";
    ss << "focusAxis: ";
    if (this->focusAxis.has_value()) {
        ss << this->focusAxis.value().toString();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "xAxis: ";
    if (this->xAxis.has_value()) {
        ss << this->xAxis.value().toString();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "yAxis: ";
    if (this->yAxis.has_value()) {
        ss << this->yAxis.value().toString();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "illuminator: ";
    if (this->illuminator.has_value()) {
        ss << this->illuminator.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "filterChanger: ";
    if (this->filterChanger.has_value()) {
        ss << this->filterChanger.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "objectiveChanger: ";
    if (this->objectiveChanger.has_value()) {
        ss << this->objectiveChanger.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "autofocus: ";
    if (this->autofocus.has_value()) {
        ss << this->autofocus.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "cameraTrigger: ";
    if (this->cameraTrigger.has_value()) {
        ss << this->cameraTrigger.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void MicroscopeConfig::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeConfig>();
}

std::string MicroscopeConfig::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace microscopy
} // namespace motion
} // namespace zaber
