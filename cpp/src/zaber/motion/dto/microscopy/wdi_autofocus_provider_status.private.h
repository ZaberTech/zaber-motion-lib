// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/microscopy/wdi_autofocus_provider_status.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace microscopy {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(WdiAutofocusProviderStatus, inRange, laserOn)

} // namespace microscopy
} // namespace motion
} // namespace zaber
