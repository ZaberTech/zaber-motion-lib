// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/microscopy/microscope_config.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/axis_address.private.h"
#include "zaber/motion/dto/channel_address.private.h"

namespace zaber {
namespace motion {
namespace microscopy {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(MicroscopeConfig, focusAxis, xAxis, yAxis, illuminator, filterChanger, objectiveChanger, autofocus, cameraTrigger)

} // namespace microscopy
} // namespace motion
} // namespace zaber
