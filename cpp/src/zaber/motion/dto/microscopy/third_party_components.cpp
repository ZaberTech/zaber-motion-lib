// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/microscopy/third_party_components.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace microscopy {

ThirdPartyComponents::ThirdPartyComponents() { }

ThirdPartyComponents::ThirdPartyComponents(
    std::optional<int> p_autofocus
) :
    autofocus(p_autofocus)
{ }

bool ThirdPartyComponents::operator==(const ThirdPartyComponents& other) const {
    return std::tie(
    autofocus
    ) == std::tie(
    other.autofocus
    );
}

std::optional<int> ThirdPartyComponents::getAutofocus() const {
    return autofocus;
}
void ThirdPartyComponents::setAutofocus(std::optional<int> p_autofocus) {
    autofocus = p_autofocus;
}

std::string ThirdPartyComponents::toString() const {
    std::stringstream ss;
    ss << "ThirdPartyComponents { ";
    ss << "autofocus: ";
    if (this->autofocus.has_value()) {
        ss << this->autofocus.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void ThirdPartyComponents::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ThirdPartyComponents>();
}

std::string ThirdPartyComponents::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace microscopy
} // namespace motion
} // namespace zaber
