// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/microscopy/autofocus_status.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace microscopy {

AutofocusStatus::AutofocusStatus() { }

AutofocusStatus::AutofocusStatus(
    bool p_inFocus,
    bool p_inRange
) :
    inFocus(p_inFocus),
    inRange(p_inRange)
{ }

bool AutofocusStatus::operator==(const AutofocusStatus& other) const {
    return std::tie(
    inFocus,
    inRange
    ) == std::tie(
    other.inFocus,
    other.inRange
    );
}

bool AutofocusStatus::getInFocus() const {
    return inFocus;
}
void AutofocusStatus::setInFocus(bool p_inFocus) {
    inFocus = p_inFocus;
}

bool AutofocusStatus::getInRange() const {
    return inRange;
}
void AutofocusStatus::setInRange(bool p_inRange) {
    inRange = p_inRange;
}

std::string AutofocusStatus::toString() const {
    std::stringstream ss;
    ss << "AutofocusStatus { ";
    ss << "inFocus: ";
    ss << this->inFocus;
    ss << ", ";
    ss << "inRange: ";
    ss << this->inRange;
    ss << " }";
    return ss.str();
}

void AutofocusStatus::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusStatus>();
}

std::string AutofocusStatus::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace microscopy
} // namespace motion
} // namespace zaber
