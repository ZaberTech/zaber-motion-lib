// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/microscopy/wdi_autofocus_provider_status.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace microscopy {

WdiAutofocusProviderStatus::WdiAutofocusProviderStatus() { }

WdiAutofocusProviderStatus::WdiAutofocusProviderStatus(
    bool p_inRange,
    bool p_laserOn
) :
    inRange(p_inRange),
    laserOn(p_laserOn)
{ }

bool WdiAutofocusProviderStatus::operator==(const WdiAutofocusProviderStatus& other) const {
    return std::tie(
    inRange,
    laserOn
    ) == std::tie(
    other.inRange,
    other.laserOn
    );
}

bool WdiAutofocusProviderStatus::getInRange() const {
    return inRange;
}
void WdiAutofocusProviderStatus::setInRange(bool p_inRange) {
    inRange = p_inRange;
}

bool WdiAutofocusProviderStatus::getLaserOn() const {
    return laserOn;
}
void WdiAutofocusProviderStatus::setLaserOn(bool p_laserOn) {
    laserOn = p_laserOn;
}

std::string WdiAutofocusProviderStatus::toString() const {
    std::stringstream ss;
    ss << "WdiAutofocusProviderStatus { ";
    ss << "inRange: ";
    ss << this->inRange;
    ss << ", ";
    ss << "laserOn: ";
    ss << this->laserOn;
    ss << " }";
    return ss.str();
}

void WdiAutofocusProviderStatus::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<WdiAutofocusProviderStatus>();
}

std::string WdiAutofocusProviderStatus::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace microscopy
} // namespace motion
} // namespace zaber
