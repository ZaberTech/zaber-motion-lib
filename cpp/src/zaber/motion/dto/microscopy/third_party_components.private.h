// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/microscopy/third_party_components.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace microscopy {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ThirdPartyComponents, autofocus)

} // namespace microscopy
} // namespace motion
} // namespace zaber
