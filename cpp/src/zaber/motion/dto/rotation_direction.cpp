// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/rotation_direction.h"

namespace zaber {
namespace motion {

std::string RotationDirection_toString(RotationDirection value) {
    switch (value) {
        case RotationDirection::CLOCKWISE: return "CLOCKWISE";
        case RotationDirection::COUNTERCLOCKWISE: return "COUNTERCLOCKWISE";
    }
    return "<Invalid value>";
}

} // namespace motion
} // namespace zaber
