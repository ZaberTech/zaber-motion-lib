// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/axis_address.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AxisAddress, device, axis)

} // namespace motion
} // namespace zaber
