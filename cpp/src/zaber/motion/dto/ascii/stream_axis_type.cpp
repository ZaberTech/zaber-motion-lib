// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/stream_axis_type.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string StreamAxisType_toString(StreamAxisType value) {
    switch (value) {
        case StreamAxisType::PHYSICAL: return "PHYSICAL";
        case StreamAxisType::LOCKSTEP: return "LOCKSTEP";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
