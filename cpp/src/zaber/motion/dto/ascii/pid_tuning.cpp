// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/pid_tuning.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

PidTuning::PidTuning() { }

PidTuning::PidTuning(
    std::string p_type,
    int p_version,
    double p_p,
    double p_i,
    double p_d,
    double p_fc
) :
    type(std::move(p_type)),
    version(p_version),
    p(p_p),
    i(p_i),
    d(p_d),
    fc(p_fc)
{ }

bool PidTuning::operator==(const PidTuning& other) const {
    return std::tie(
    type,
    version,
    p,
    i,
    d,
    fc
    ) == std::tie(
    other.type,
    other.version,
    other.p,
    other.i,
    other.d,
    other.fc
    );
}

std::string const& PidTuning::getType() const {
    return type;
}
void PidTuning::setType(std::string p_type) {
    type = std::move(p_type);
}

int PidTuning::getVersion() const {
    return version;
}
void PidTuning::setVersion(int p_version) {
    version = p_version;
}

double PidTuning::getP() const {
    return p;
}
void PidTuning::setP(double p_p) {
    p = p_p;
}

double PidTuning::getI() const {
    return i;
}
void PidTuning::setI(double p_i) {
    i = p_i;
}

double PidTuning::getD() const {
    return d;
}
void PidTuning::setD(double p_d) {
    d = p_d;
}

double PidTuning::getFc() const {
    return fc;
}
void PidTuning::setFc(double p_fc) {
    fc = p_fc;
}

std::string PidTuning::toString() const {
    std::stringstream ss;
    ss << "PidTuning { ";
    ss << "type: ";
    ss << this->type;
    ss << ", ";
    ss << "version: ";
    ss << this->version;
    ss << ", ";
    ss << "p: ";
    ss << this->p;
    ss << ", ";
    ss << "i: ";
    ss << this->i;
    ss << ", ";
    ss << "d: ";
    ss << this->d;
    ss << ", ";
    ss << "fc: ";
    ss << this->fc;
    ss << " }";
    return ss.str();
}

void PidTuning::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PidTuning>();
}

std::string PidTuning::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
