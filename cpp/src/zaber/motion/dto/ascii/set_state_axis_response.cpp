// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/set_state_axis_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

SetStateAxisResponse::SetStateAxisResponse() { }

SetStateAxisResponse::SetStateAxisResponse(
    std::vector<std::string> p_warnings,
    int p_axisNumber
) :
    warnings(std::move(p_warnings)),
    axisNumber(p_axisNumber)
{ }

bool SetStateAxisResponse::operator==(const SetStateAxisResponse& other) const {
    return std::tie(
    warnings,
    axisNumber
    ) == std::tie(
    other.warnings,
    other.axisNumber
    );
}

std::vector<std::string> const& SetStateAxisResponse::getWarnings() const {
    return warnings;
}
void SetStateAxisResponse::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

int SetStateAxisResponse::getAxisNumber() const {
    return axisNumber;
}
void SetStateAxisResponse::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::string SetStateAxisResponse::toString() const {
    std::stringstream ss;
    ss << "SetStateAxisResponse { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << " }";
    return ss.str();
}

void SetStateAxisResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetStateAxisResponse>();
}

std::string SetStateAxisResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
