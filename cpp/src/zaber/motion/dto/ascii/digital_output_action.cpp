// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/digital_output_action.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string DigitalOutputAction_toString(DigitalOutputAction value) {
    switch (value) {
        case DigitalOutputAction::OFF: return "OFF";
        case DigitalOutputAction::ON: return "ON";
        case DigitalOutputAction::TOGGLE: return "TOGGLE";
        case DigitalOutputAction::KEEP: return "KEEP";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
