// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/io_port_type.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string IoPortType_toString(IoPortType value) {
    switch (value) {
        case IoPortType::NONE: return "NONE";
        case IoPortType::ANALOG_INPUT: return "ANALOG_INPUT";
        case IoPortType::ANALOG_OUTPUT: return "ANALOG_OUTPUT";
        case IoPortType::DIGITAL_INPUT: return "DIGITAL_INPUT";
        case IoPortType::DIGITAL_OUTPUT: return "DIGITAL_OUTPUT";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
