// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/axis_type.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string AxisType_toString(AxisType value) {
    switch (value) {
        case AxisType::UNKNOWN: return "UNKNOWN";
        case AxisType::LINEAR: return "LINEAR";
        case AxisType::ROTARY: return "ROTARY";
        case AxisType::PROCESS: return "PROCESS";
        case AxisType::LAMP: return "LAMP";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
