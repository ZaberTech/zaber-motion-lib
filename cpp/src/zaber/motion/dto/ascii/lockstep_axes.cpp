// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/lockstep_axes.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

LockstepAxes::LockstepAxes() { }

LockstepAxes::LockstepAxes(
    int p_axis1,
    int p_axis2,
    int p_axis3,
    int p_axis4
) :
    axis1(p_axis1),
    axis2(p_axis2),
    axis3(p_axis3),
    axis4(p_axis4)
{ }

bool LockstepAxes::operator==(const LockstepAxes& other) const {
    return std::tie(
    axis1,
    axis2,
    axis3,
    axis4
    ) == std::tie(
    other.axis1,
    other.axis2,
    other.axis3,
    other.axis4
    );
}

int LockstepAxes::getAxis1() const {
    return axis1;
}
void LockstepAxes::setAxis1(int p_axis1) {
    axis1 = p_axis1;
}

int LockstepAxes::getAxis2() const {
    return axis2;
}
void LockstepAxes::setAxis2(int p_axis2) {
    axis2 = p_axis2;
}

int LockstepAxes::getAxis3() const {
    return axis3;
}
void LockstepAxes::setAxis3(int p_axis3) {
    axis3 = p_axis3;
}

int LockstepAxes::getAxis4() const {
    return axis4;
}
void LockstepAxes::setAxis4(int p_axis4) {
    axis4 = p_axis4;
}

std::string LockstepAxes::toString() const {
    std::stringstream ss;
    ss << "LockstepAxes { ";
    ss << "axis1: ";
    ss << this->axis1;
    ss << ", ";
    ss << "axis2: ";
    ss << this->axis2;
    ss << ", ";
    ss << "axis3: ";
    ss << this->axis3;
    ss << ", ";
    ss << "axis4: ";
    ss << this->axis4;
    ss << " }";
    return ss.str();
}

void LockstepAxes::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepAxes>();
}

std::string LockstepAxes::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
