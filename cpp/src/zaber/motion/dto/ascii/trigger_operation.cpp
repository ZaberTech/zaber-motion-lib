// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/trigger_operation.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string TriggerOperation_toString(TriggerOperation value) {
    switch (value) {
        case TriggerOperation::SET_TO: return "SET_TO";
        case TriggerOperation::INCREMENT_BY: return "INCREMENT_BY";
        case TriggerOperation::DECREMENT_BY: return "DECREMENT_BY";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
