// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/simple_tuning_param_definition.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SimpleTuningParamDefinition, name, minLabel, maxLabel, dataType, defaultValue)

} // namespace ascii
} // namespace motion
} // namespace zaber
