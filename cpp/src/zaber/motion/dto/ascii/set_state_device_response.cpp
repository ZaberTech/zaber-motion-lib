// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/set_state_device_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

SetStateDeviceResponse::SetStateDeviceResponse() { }

SetStateDeviceResponse::SetStateDeviceResponse(
    std::vector<std::string> p_warnings,
    std::vector<SetStateAxisResponse> p_axisResponses
) :
    warnings(std::move(p_warnings)),
    axisResponses(std::move(p_axisResponses))
{ }

bool SetStateDeviceResponse::operator==(const SetStateDeviceResponse& other) const {
    return std::tie(
    warnings,
    axisResponses
    ) == std::tie(
    other.warnings,
    other.axisResponses
    );
}

std::vector<std::string> const& SetStateDeviceResponse::getWarnings() const {
    return warnings;
}
void SetStateDeviceResponse::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::vector<SetStateAxisResponse> const& SetStateDeviceResponse::getAxisResponses() const {
    return axisResponses;
}
void SetStateDeviceResponse::setAxisResponses(std::vector<SetStateAxisResponse> p_axisResponses) {
    axisResponses = std::move(p_axisResponses);
}

std::string SetStateDeviceResponse::toString() const {
    std::stringstream ss;
    ss << "SetStateDeviceResponse { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axisResponses: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axisResponses.size(); i++) {
        ss << this->axisResponses[i].toString();
        if (i < this->axisResponses.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void SetStateDeviceResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetStateDeviceResponse>();
}

std::string SetStateDeviceResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
