// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/conversion_factor.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

ConversionFactor::ConversionFactor() { }

ConversionFactor::ConversionFactor(
    std::string p_setting,
    double p_value,
    Units p_unit
) :
    setting(std::move(p_setting)),
    value(p_value),
    unit(p_unit)
{ }

bool ConversionFactor::operator==(const ConversionFactor& other) const {
    return std::tie(
    setting,
    value,
    unit
    ) == std::tie(
    other.setting,
    other.value,
    other.unit
    );
}

std::string const& ConversionFactor::getSetting() const {
    return setting;
}
void ConversionFactor::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

double ConversionFactor::getValue() const {
    return value;
}
void ConversionFactor::setValue(double p_value) {
    value = p_value;
}

Units ConversionFactor::getUnit() const {
    return unit;
}
void ConversionFactor::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string ConversionFactor::toString() const {
    std::stringstream ss;
    ss << "ConversionFactor { ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void ConversionFactor::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ConversionFactor>();
}

std::string ConversionFactor::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
