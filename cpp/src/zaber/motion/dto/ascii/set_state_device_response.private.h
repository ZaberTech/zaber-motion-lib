// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/set_state_device_response.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/set_state_axis_response.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetStateDeviceResponse, warnings, axisResponses)

} // namespace ascii
} // namespace motion
} // namespace zaber
