// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/get_axis_setting_result.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

GetAxisSettingResult::GetAxisSettingResult() { }

GetAxisSettingResult::GetAxisSettingResult(
    std::string p_setting,
    double p_value,
    Units p_unit
) :
    setting(std::move(p_setting)),
    value(p_value),
    unit(p_unit)
{ }

bool GetAxisSettingResult::operator==(const GetAxisSettingResult& other) const {
    return std::tie(
    setting,
    value,
    unit
    ) == std::tie(
    other.setting,
    other.value,
    other.unit
    );
}

std::string const& GetAxisSettingResult::getSetting() const {
    return setting;
}
void GetAxisSettingResult::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

double GetAxisSettingResult::getValue() const {
    return value;
}
void GetAxisSettingResult::setValue(double p_value) {
    value = p_value;
}

Units GetAxisSettingResult::getUnit() const {
    return unit;
}
void GetAxisSettingResult::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string GetAxisSettingResult::toString() const {
    std::stringstream ss;
    ss << "GetAxisSettingResult { ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void GetAxisSettingResult::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetAxisSettingResult>();
}

std::string GetAxisSettingResult::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
