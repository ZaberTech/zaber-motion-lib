// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/unknown_response_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

UnknownResponseEvent::UnknownResponseEvent() { }

UnknownResponseEvent::UnknownResponseEvent(
    int p_deviceAddress,
    int p_axisNumber,
    std::string p_replyFlag,
    std::string p_status,
    std::string p_warningFlag,
    std::string p_data,
    MessageType p_messageType
) :
    deviceAddress(p_deviceAddress),
    axisNumber(p_axisNumber),
    replyFlag(std::move(p_replyFlag)),
    status(std::move(p_status)),
    warningFlag(std::move(p_warningFlag)),
    data(std::move(p_data)),
    messageType(std::move(p_messageType))
{ }

bool UnknownResponseEvent::operator==(const UnknownResponseEvent& other) const {
    return std::tie(
    deviceAddress,
    axisNumber,
    replyFlag,
    status,
    warningFlag,
    data,
    messageType
    ) == std::tie(
    other.deviceAddress,
    other.axisNumber,
    other.replyFlag,
    other.status,
    other.warningFlag,
    other.data,
    other.messageType
    );
}

int UnknownResponseEvent::getDeviceAddress() const {
    return deviceAddress;
}
void UnknownResponseEvent::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int UnknownResponseEvent::getAxisNumber() const {
    return axisNumber;
}
void UnknownResponseEvent::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::string const& UnknownResponseEvent::getReplyFlag() const {
    return replyFlag;
}
void UnknownResponseEvent::setReplyFlag(std::string p_replyFlag) {
    replyFlag = std::move(p_replyFlag);
}

std::string const& UnknownResponseEvent::getStatus() const {
    return status;
}
void UnknownResponseEvent::setStatus(std::string p_status) {
    status = std::move(p_status);
}

std::string const& UnknownResponseEvent::getWarningFlag() const {
    return warningFlag;
}
void UnknownResponseEvent::setWarningFlag(std::string p_warningFlag) {
    warningFlag = std::move(p_warningFlag);
}

std::string const& UnknownResponseEvent::getData() const {
    return data;
}
void UnknownResponseEvent::setData(std::string p_data) {
    data = std::move(p_data);
}

MessageType const& UnknownResponseEvent::getMessageType() const {
    return messageType;
}
void UnknownResponseEvent::setMessageType(MessageType p_messageType) {
    messageType = std::move(p_messageType);
}

std::string UnknownResponseEvent::toString() const {
    std::stringstream ss;
    ss << "UnknownResponseEvent { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "replyFlag: ";
    ss << this->replyFlag;
    ss << ", ";
    ss << "status: ";
    ss << this->status;
    ss << ", ";
    ss << "warningFlag: ";
    ss << this->warningFlag;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << ", ";
    ss << "messageType: ";
    ss << MessageType_toString(this->messageType);
    ss << " }";
    return ss.str();
}

void UnknownResponseEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnknownResponseEvent>();
}

std::string UnknownResponseEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
