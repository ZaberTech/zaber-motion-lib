// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

Response::Response() { }

Response::Response(
    int p_deviceAddress,
    int p_axisNumber,
    std::string p_replyFlag,
    std::string p_status,
    std::string p_warningFlag,
    std::string p_data,
    MessageType p_messageType
) :
    deviceAddress(p_deviceAddress),
    axisNumber(p_axisNumber),
    replyFlag(std::move(p_replyFlag)),
    status(std::move(p_status)),
    warningFlag(std::move(p_warningFlag)),
    data(std::move(p_data)),
    messageType(std::move(p_messageType))
{ }

bool Response::operator==(const Response& other) const {
    return std::tie(
    deviceAddress,
    axisNumber,
    replyFlag,
    status,
    warningFlag,
    data,
    messageType
    ) == std::tie(
    other.deviceAddress,
    other.axisNumber,
    other.replyFlag,
    other.status,
    other.warningFlag,
    other.data,
    other.messageType
    );
}

int Response::getDeviceAddress() const {
    return deviceAddress;
}
void Response::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int Response::getAxisNumber() const {
    return axisNumber;
}
void Response::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::string const& Response::getReplyFlag() const {
    return replyFlag;
}
void Response::setReplyFlag(std::string p_replyFlag) {
    replyFlag = std::move(p_replyFlag);
}

std::string const& Response::getStatus() const {
    return status;
}
void Response::setStatus(std::string p_status) {
    status = std::move(p_status);
}

std::string const& Response::getWarningFlag() const {
    return warningFlag;
}
void Response::setWarningFlag(std::string p_warningFlag) {
    warningFlag = std::move(p_warningFlag);
}

std::string const& Response::getData() const {
    return data;
}
void Response::setData(std::string p_data) {
    data = std::move(p_data);
}

MessageType const& Response::getMessageType() const {
    return messageType;
}
void Response::setMessageType(MessageType p_messageType) {
    messageType = std::move(p_messageType);
}

std::string Response::toString() const {
    std::stringstream ss;
    ss << "Response { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "replyFlag: ";
    ss << this->replyFlag;
    ss << ", ";
    ss << "status: ";
    ss << this->status;
    ss << ", ";
    ss << "warningFlag: ";
    ss << this->warningFlag;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << ", ";
    ss << "messageType: ";
    ss << MessageType_toString(this->messageType);
    ss << " }";
    return ss.str();
}

void Response::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<Response>();
}

std::string Response::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
