// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/get_setting.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GetSetting, setting, axes, unit)

} // namespace ascii
} // namespace motion
} // namespace zaber
