// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/simple_tuning_param_definition.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

SimpleTuningParamDefinition::SimpleTuningParamDefinition() { }

SimpleTuningParamDefinition::SimpleTuningParamDefinition(
    std::string p_name,
    std::string p_minLabel,
    std::string p_maxLabel,
    std::string p_dataType,
    std::optional<double> p_defaultValue
) :
    name(std::move(p_name)),
    minLabel(std::move(p_minLabel)),
    maxLabel(std::move(p_maxLabel)),
    dataType(std::move(p_dataType)),
    defaultValue(p_defaultValue)
{ }

SimpleTuningParamDefinition::SimpleTuningParamDefinition(
    std::string p_name,
    std::string p_minLabel,
    std::string p_maxLabel,
    std::string p_dataType
) :
    name(std::move(p_name)),
    minLabel(std::move(p_minLabel)),
    maxLabel(std::move(p_maxLabel)),
    dataType(std::move(p_dataType))
{ }

bool SimpleTuningParamDefinition::operator==(const SimpleTuningParamDefinition& other) const {
    return std::tie(
    name,
    minLabel,
    maxLabel,
    dataType,
    defaultValue
    ) == std::tie(
    other.name,
    other.minLabel,
    other.maxLabel,
    other.dataType,
    other.defaultValue
    );
}

std::string const& SimpleTuningParamDefinition::getName() const {
    return name;
}
void SimpleTuningParamDefinition::setName(std::string p_name) {
    name = std::move(p_name);
}

std::string const& SimpleTuningParamDefinition::getMinLabel() const {
    return minLabel;
}
void SimpleTuningParamDefinition::setMinLabel(std::string p_minLabel) {
    minLabel = std::move(p_minLabel);
}

std::string const& SimpleTuningParamDefinition::getMaxLabel() const {
    return maxLabel;
}
void SimpleTuningParamDefinition::setMaxLabel(std::string p_maxLabel) {
    maxLabel = std::move(p_maxLabel);
}

std::string const& SimpleTuningParamDefinition::getDataType() const {
    return dataType;
}
void SimpleTuningParamDefinition::setDataType(std::string p_dataType) {
    dataType = std::move(p_dataType);
}

std::optional<double> SimpleTuningParamDefinition::getDefaultValue() const {
    return defaultValue;
}
void SimpleTuningParamDefinition::setDefaultValue(std::optional<double> p_defaultValue) {
    defaultValue = p_defaultValue;
}

std::string SimpleTuningParamDefinition::toString() const {
    std::stringstream ss;
    ss << "SimpleTuningParamDefinition { ";
    ss << "name: ";
    ss << this->name;
    ss << ", ";
    ss << "minLabel: ";
    ss << this->minLabel;
    ss << ", ";
    ss << "maxLabel: ";
    ss << this->maxLabel;
    ss << ", ";
    ss << "dataType: ";
    ss << this->dataType;
    ss << ", ";
    ss << "defaultValue: ";
    if (this->defaultValue.has_value()) {
        ss << this->defaultValue.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void SimpleTuningParamDefinition::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SimpleTuningParamDefinition>();
}

std::string SimpleTuningParamDefinition::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
