// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/get_setting_result.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

GetSettingResult::GetSettingResult() { }

GetSettingResult::GetSettingResult(
    std::string p_setting,
    std::vector<double> p_values,
    Units p_unit
) :
    setting(std::move(p_setting)),
    values(std::move(p_values)),
    unit(p_unit)
{ }

bool GetSettingResult::operator==(const GetSettingResult& other) const {
    return std::tie(
    setting,
    values,
    unit
    ) == std::tie(
    other.setting,
    other.values,
    other.unit
    );
}

std::string const& GetSettingResult::getSetting() const {
    return setting;
}
void GetSettingResult::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::vector<double> const& GetSettingResult::getValues() const {
    return values;
}
void GetSettingResult::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

Units GetSettingResult::getUnit() const {
    return unit;
}
void GetSettingResult::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string GetSettingResult::toString() const {
    std::stringstream ss;
    ss << "GetSettingResult { ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void GetSettingResult::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetSettingResult>();
}

std::string GetSettingResult::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
