// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/trigger_condition.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string TriggerCondition_toString(TriggerCondition value) {
    switch (value) {
        case TriggerCondition::EQ: return "EQ";
        case TriggerCondition::NE: return "NE";
        case TriggerCondition::GT: return "GT";
        case TriggerCondition::GE: return "GE";
        case TriggerCondition::LT: return "LT";
        case TriggerCondition::LE: return "LE";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
