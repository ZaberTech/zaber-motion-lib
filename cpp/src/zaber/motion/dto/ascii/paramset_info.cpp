// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/paramset_info.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

ParamsetInfo::ParamsetInfo() { }

ParamsetInfo::ParamsetInfo(
    std::string p_type,
    int p_version,
    std::vector<ServoTuningParam> p_params
) :
    type(std::move(p_type)),
    version(p_version),
    params(std::move(p_params))
{ }

bool ParamsetInfo::operator==(const ParamsetInfo& other) const {
    return std::tie(
    type,
    version,
    params
    ) == std::tie(
    other.type,
    other.version,
    other.params
    );
}

std::string const& ParamsetInfo::getType() const {
    return type;
}
void ParamsetInfo::setType(std::string p_type) {
    type = std::move(p_type);
}

int ParamsetInfo::getVersion() const {
    return version;
}
void ParamsetInfo::setVersion(int p_version) {
    version = p_version;
}

std::vector<ServoTuningParam> const& ParamsetInfo::getParams() const {
    return params;
}
void ParamsetInfo::setParams(std::vector<ServoTuningParam> p_params) {
    params = std::move(p_params);
}

std::string ParamsetInfo::toString() const {
    std::stringstream ss;
    ss << "ParamsetInfo { ";
    ss << "type: ";
    ss << this->type;
    ss << ", ";
    ss << "version: ";
    ss << this->version;
    ss << ", ";
    ss << "params: ";
    ss << "[ ";
    for (size_t i = 0; i < this->params.size(); i++) {
        ss << this->params[i].toString();
        if (i < this->params.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void ParamsetInfo::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ParamsetInfo>();
}

std::string ParamsetInfo::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
