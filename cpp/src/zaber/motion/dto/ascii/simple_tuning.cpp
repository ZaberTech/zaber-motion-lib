// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/simple_tuning.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

SimpleTuning::SimpleTuning() { }

SimpleTuning::SimpleTuning(
    bool p_isUsed,
    std::optional<double> p_carriageMass,
    double p_loadMass,
    std::vector<ServoTuningParam> p_tuningParams
) :
    isUsed(p_isUsed),
    carriageMass(p_carriageMass),
    loadMass(p_loadMass),
    tuningParams(std::move(p_tuningParams))
{ }

SimpleTuning::SimpleTuning(
    bool p_isUsed,
    double p_loadMass,
    std::vector<ServoTuningParam> p_tuningParams
) :
    isUsed(p_isUsed),
    loadMass(p_loadMass),
    tuningParams(std::move(p_tuningParams))
{ }

bool SimpleTuning::operator==(const SimpleTuning& other) const {
    return std::tie(
    isUsed,
    carriageMass,
    loadMass,
    tuningParams
    ) == std::tie(
    other.isUsed,
    other.carriageMass,
    other.loadMass,
    other.tuningParams
    );
}

bool SimpleTuning::getIsUsed() const {
    return isUsed;
}
void SimpleTuning::setIsUsed(bool p_isUsed) {
    isUsed = p_isUsed;
}

std::optional<double> SimpleTuning::getCarriageMass() const {
    return carriageMass;
}
void SimpleTuning::setCarriageMass(std::optional<double> p_carriageMass) {
    carriageMass = p_carriageMass;
}

double SimpleTuning::getLoadMass() const {
    return loadMass;
}
void SimpleTuning::setLoadMass(double p_loadMass) {
    loadMass = p_loadMass;
}

std::vector<ServoTuningParam> const& SimpleTuning::getTuningParams() const {
    return tuningParams;
}
void SimpleTuning::setTuningParams(std::vector<ServoTuningParam> p_tuningParams) {
    tuningParams = std::move(p_tuningParams);
}

std::string SimpleTuning::toString() const {
    std::stringstream ss;
    ss << "SimpleTuning { ";
    ss << "isUsed: ";
    ss << this->isUsed;
    ss << ", ";
    ss << "carriageMass: ";
    if (this->carriageMass.has_value()) {
        ss << this->carriageMass.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "loadMass: ";
    ss << this->loadMass;
    ss << ", ";
    ss << "tuningParams: ";
    ss << "[ ";
    for (size_t i = 0; i < this->tuningParams.size(); i++) {
        ss << this->tuningParams[i].toString();
        if (i < this->tuningParams.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void SimpleTuning::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SimpleTuning>();
}

std::string SimpleTuning::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
