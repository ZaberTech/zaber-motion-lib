// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/can_set_state_device_response.private.h"

#include <sstream>

#pragma push_macro("error")
#undef error

namespace zaber {
namespace motion {
namespace ascii {

CanSetStateDeviceResponse::CanSetStateDeviceResponse() { }

CanSetStateDeviceResponse::CanSetStateDeviceResponse(
    std::optional<std::string> p_error,
    std::vector<CanSetStateAxisResponse> p_axisErrors
) :
    error(std::move(p_error)),
    axisErrors(std::move(p_axisErrors))
{ }

CanSetStateDeviceResponse::CanSetStateDeviceResponse(
    std::vector<CanSetStateAxisResponse> p_axisErrors
) :
    axisErrors(std::move(p_axisErrors))
{ }

bool CanSetStateDeviceResponse::operator==(const CanSetStateDeviceResponse& other) const {
    return std::tie(
    error,
    axisErrors
    ) == std::tie(
    other.error,
    other.axisErrors
    );
}

std::optional<std::string> const& CanSetStateDeviceResponse::getError() const {
    return error;
}
void CanSetStateDeviceResponse::setError(std::optional<std::string> p_error) {
    error = std::move(p_error);
}

std::vector<CanSetStateAxisResponse> const& CanSetStateDeviceResponse::getAxisErrors() const {
    return axisErrors;
}
void CanSetStateDeviceResponse::setAxisErrors(std::vector<CanSetStateAxisResponse> p_axisErrors) {
    axisErrors = std::move(p_axisErrors);
}

std::string CanSetStateDeviceResponse::toString() const {
    std::stringstream ss;
    ss << "CanSetStateDeviceResponse { ";
    ss << "error: ";
    if (this->error.has_value()) {
        ss << this->error.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "axisErrors: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axisErrors.size(); i++) {
        ss << this->axisErrors[i].toString();
        if (i < this->axisErrors.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void CanSetStateDeviceResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CanSetStateDeviceResponse>();
}

std::string CanSetStateDeviceResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber

#pragma pop_macro("error")
