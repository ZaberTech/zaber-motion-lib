// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/trigger_enabled_state.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

TriggerEnabledState::TriggerEnabledState() { }

TriggerEnabledState::TriggerEnabledState(
    bool p_enabled,
    int p_firesRemaining
) :
    enabled(p_enabled),
    firesRemaining(p_firesRemaining)
{ }

bool TriggerEnabledState::operator==(const TriggerEnabledState& other) const {
    return std::tie(
    enabled,
    firesRemaining
    ) == std::tie(
    other.enabled,
    other.firesRemaining
    );
}

bool TriggerEnabledState::getEnabled() const {
    return enabled;
}
void TriggerEnabledState::setEnabled(bool p_enabled) {
    enabled = p_enabled;
}

int TriggerEnabledState::getFiresRemaining() const {
    return firesRemaining;
}
void TriggerEnabledState::setFiresRemaining(int p_firesRemaining) {
    firesRemaining = p_firesRemaining;
}

std::string TriggerEnabledState::toString() const {
    std::stringstream ss;
    ss << "TriggerEnabledState { ";
    ss << "enabled: ";
    ss << this->enabled;
    ss << ", ";
    ss << "firesRemaining: ";
    ss << this->firesRemaining;
    ss << " }";
    return ss.str();
}

void TriggerEnabledState::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerEnabledState>();
}

std::string TriggerEnabledState::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
