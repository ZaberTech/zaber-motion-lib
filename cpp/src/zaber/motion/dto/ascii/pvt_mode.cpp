// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/pvt_mode.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string PvtMode_toString(PvtMode value) {
    switch (value) {
        case PvtMode::DISABLED: return "DISABLED";
        case PvtMode::STORE: return "STORE";
        case PvtMode::LIVE: return "LIVE";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
