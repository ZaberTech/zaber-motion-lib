// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/alert_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

AlertEvent::AlertEvent() { }

AlertEvent::AlertEvent(
    int p_deviceAddress,
    int p_axisNumber,
    std::string p_status,
    std::string p_warningFlag,
    std::string p_data
) :
    deviceAddress(p_deviceAddress),
    axisNumber(p_axisNumber),
    status(std::move(p_status)),
    warningFlag(std::move(p_warningFlag)),
    data(std::move(p_data))
{ }

bool AlertEvent::operator==(const AlertEvent& other) const {
    return std::tie(
    deviceAddress,
    axisNumber,
    status,
    warningFlag,
    data
    ) == std::tie(
    other.deviceAddress,
    other.axisNumber,
    other.status,
    other.warningFlag,
    other.data
    );
}

int AlertEvent::getDeviceAddress() const {
    return deviceAddress;
}
void AlertEvent::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int AlertEvent::getAxisNumber() const {
    return axisNumber;
}
void AlertEvent::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::string const& AlertEvent::getStatus() const {
    return status;
}
void AlertEvent::setStatus(std::string p_status) {
    status = std::move(p_status);
}

std::string const& AlertEvent::getWarningFlag() const {
    return warningFlag;
}
void AlertEvent::setWarningFlag(std::string p_warningFlag) {
    warningFlag = std::move(p_warningFlag);
}

std::string const& AlertEvent::getData() const {
    return data;
}
void AlertEvent::setData(std::string p_data) {
    data = std::move(p_data);
}

std::string AlertEvent::toString() const {
    std::stringstream ss;
    ss << "AlertEvent { ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "status: ";
    ss << this->status;
    ss << ", ";
    ss << "warningFlag: ";
    ss << this->warningFlag;
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << " }";
    return ss.str();
}

void AlertEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AlertEvent>();
}

std::string AlertEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
