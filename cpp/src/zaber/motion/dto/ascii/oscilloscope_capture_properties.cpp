// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/oscilloscope_capture_properties.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

OscilloscopeCaptureProperties::OscilloscopeCaptureProperties() { }

OscilloscopeCaptureProperties::OscilloscopeCaptureProperties(
    OscilloscopeDataSource p_dataSource,
    std::string p_setting,
    int p_axisNumber,
    IoPortType p_ioType,
    int p_ioChannel
) :
    dataSource(std::move(p_dataSource)),
    setting(std::move(p_setting)),
    axisNumber(p_axisNumber),
    ioType(std::move(p_ioType)),
    ioChannel(p_ioChannel)
{ }

bool OscilloscopeCaptureProperties::operator==(const OscilloscopeCaptureProperties& other) const {
    return std::tie(
    dataSource,
    setting,
    axisNumber,
    ioType,
    ioChannel
    ) == std::tie(
    other.dataSource,
    other.setting,
    other.axisNumber,
    other.ioType,
    other.ioChannel
    );
}

OscilloscopeDataSource const& OscilloscopeCaptureProperties::getDataSource() const {
    return dataSource;
}
void OscilloscopeCaptureProperties::setDataSource(OscilloscopeDataSource p_dataSource) {
    dataSource = std::move(p_dataSource);
}

std::string const& OscilloscopeCaptureProperties::getSetting() const {
    return setting;
}
void OscilloscopeCaptureProperties::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

int OscilloscopeCaptureProperties::getAxisNumber() const {
    return axisNumber;
}
void OscilloscopeCaptureProperties::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

IoPortType const& OscilloscopeCaptureProperties::getIoType() const {
    return ioType;
}
void OscilloscopeCaptureProperties::setIoType(IoPortType p_ioType) {
    ioType = std::move(p_ioType);
}

int OscilloscopeCaptureProperties::getIoChannel() const {
    return ioChannel;
}
void OscilloscopeCaptureProperties::setIoChannel(int p_ioChannel) {
    ioChannel = p_ioChannel;
}

std::string OscilloscopeCaptureProperties::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeCaptureProperties { ";
    ss << "dataSource: ";
    ss << OscilloscopeDataSource_toString(this->dataSource);
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "ioType: ";
    ss << IoPortType_toString(this->ioType);
    ss << ", ";
    ss << "ioChannel: ";
    ss << this->ioChannel;
    ss << " }";
    return ss.str();
}

void OscilloscopeCaptureProperties::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeCaptureProperties>();
}

std::string OscilloscopeCaptureProperties::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
