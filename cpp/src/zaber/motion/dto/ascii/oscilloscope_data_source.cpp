// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/oscilloscope_data_source.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string OscilloscopeDataSource_toString(OscilloscopeDataSource value) {
    switch (value) {
        case OscilloscopeDataSource::SETTING: return "SETTING";
        case OscilloscopeDataSource::IO: return "IO";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
