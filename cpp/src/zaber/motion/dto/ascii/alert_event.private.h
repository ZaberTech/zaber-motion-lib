// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/alert_event.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AlertEvent, deviceAddress, axisNumber, status, warningFlag, data)

} // namespace ascii
} // namespace motion
} // namespace zaber
