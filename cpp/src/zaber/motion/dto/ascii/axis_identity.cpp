// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/axis_identity.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

AxisIdentity::AxisIdentity() { }

AxisIdentity::AxisIdentity(
    int p_peripheralId,
    std::string p_peripheralName,
    unsigned int p_peripheralSerialNumber,
    bool p_isPeripheral,
    AxisType p_axisType,
    bool p_isModified
) :
    peripheralId(p_peripheralId),
    peripheralName(std::move(p_peripheralName)),
    peripheralSerialNumber(p_peripheralSerialNumber),
    isPeripheral(p_isPeripheral),
    axisType(std::move(p_axisType)),
    isModified(p_isModified)
{ }

bool AxisIdentity::operator==(const AxisIdentity& other) const {
    return std::tie(
    peripheralId,
    peripheralName,
    peripheralSerialNumber,
    isPeripheral,
    axisType,
    isModified
    ) == std::tie(
    other.peripheralId,
    other.peripheralName,
    other.peripheralSerialNumber,
    other.isPeripheral,
    other.axisType,
    other.isModified
    );
}

int AxisIdentity::getPeripheralId() const {
    return peripheralId;
}
void AxisIdentity::setPeripheralId(int p_peripheralId) {
    peripheralId = p_peripheralId;
}

std::string const& AxisIdentity::getPeripheralName() const {
    return peripheralName;
}
void AxisIdentity::setPeripheralName(std::string p_peripheralName) {
    peripheralName = std::move(p_peripheralName);
}

unsigned int AxisIdentity::getPeripheralSerialNumber() const {
    return peripheralSerialNumber;
}
void AxisIdentity::setPeripheralSerialNumber(unsigned int p_peripheralSerialNumber) {
    peripheralSerialNumber = p_peripheralSerialNumber;
}

bool AxisIdentity::getIsPeripheral() const {
    return isPeripheral;
}
void AxisIdentity::setIsPeripheral(bool p_isPeripheral) {
    isPeripheral = p_isPeripheral;
}

AxisType const& AxisIdentity::getAxisType() const {
    return axisType;
}
void AxisIdentity::setAxisType(AxisType p_axisType) {
    axisType = std::move(p_axisType);
}

bool AxisIdentity::getIsModified() const {
    return isModified;
}
void AxisIdentity::setIsModified(bool p_isModified) {
    isModified = p_isModified;
}

std::string AxisIdentity::toString() const {
    std::stringstream ss;
    ss << "AxisIdentity { ";
    ss << "peripheralId: ";
    ss << this->peripheralId;
    ss << ", ";
    ss << "peripheralName: ";
    ss << this->peripheralName;
    ss << ", ";
    ss << "peripheralSerialNumber: ";
    ss << this->peripheralSerialNumber;
    ss << ", ";
    ss << "isPeripheral: ";
    ss << this->isPeripheral;
    ss << ", ";
    ss << "axisType: ";
    ss << AxisType_toString(this->axisType);
    ss << ", ";
    ss << "isModified: ";
    ss << this->isModified;
    ss << " }";
    return ss.str();
}

void AxisIdentity::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisIdentity>();
}

std::string AxisIdentity::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
