// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/unknown_response_event.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(UnknownResponseEvent, deviceAddress, axisNumber, replyFlag, status, warningFlag, data, messageType)

} // namespace ascii
} // namespace motion
} // namespace zaber
