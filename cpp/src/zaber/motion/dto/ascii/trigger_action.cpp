// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/trigger_action.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string TriggerAction_toString(TriggerAction value) {
    switch (value) {
        case TriggerAction::ALL: return "ALL";
        case TriggerAction::A: return "A";
        case TriggerAction::B: return "B";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
