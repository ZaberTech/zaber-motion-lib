// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/get_setting.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

GetSetting::GetSetting() { }

GetSetting::GetSetting(
    std::string p_setting,
    std::vector<int> p_axes,
    std::optional<Units> p_unit
) :
    setting(std::move(p_setting)),
    axes(std::move(p_axes)),
    unit(p_unit)
{ }

GetSetting::GetSetting(
    std::string p_setting
) :
    setting(std::move(p_setting))
{ }

bool GetSetting::operator==(const GetSetting& other) const {
    return std::tie(
    setting,
    axes,
    unit
    ) == std::tie(
    other.setting,
    other.axes,
    other.unit
    );
}

std::string const& GetSetting::getSetting() const {
    return setting;
}
void GetSetting::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::vector<int> const& GetSetting::getAxes() const {
    return axes;
}
void GetSetting::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::optional<Units> GetSetting::getUnit() const {
    return unit;
}
void GetSetting::setUnit(std::optional<Units> p_unit) {
    unit = p_unit;
}

std::string GetSetting::toString() const {
    std::stringstream ss;
    ss << "GetSetting { ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "unit: ";
    if (this->unit.has_value()) {
        ss << getUnitLongName(this->unit.value());
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void GetSetting::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetSetting>();
}

std::string GetSetting::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
