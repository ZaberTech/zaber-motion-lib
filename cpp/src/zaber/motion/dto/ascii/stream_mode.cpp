// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/stream_mode.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string StreamMode_toString(StreamMode value) {
    switch (value) {
        case StreamMode::DISABLED: return "DISABLED";
        case StreamMode::STORE: return "STORE";
        case StreamMode::STORE_ARBITRARY_AXES: return "STORE_ARBITRARY_AXES";
        case StreamMode::LIVE: return "LIVE";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
