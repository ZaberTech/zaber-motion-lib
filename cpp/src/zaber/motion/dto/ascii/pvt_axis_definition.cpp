// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/pvt_axis_definition.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

PvtAxisDefinition::PvtAxisDefinition() { }

PvtAxisDefinition::PvtAxisDefinition(
    int p_axisNumber,
    std::optional<PvtAxisType> p_axisType
) :
    axisNumber(p_axisNumber),
    axisType(std::move(p_axisType))
{ }

PvtAxisDefinition::PvtAxisDefinition(
    int p_axisNumber
) :
    axisNumber(p_axisNumber)
{ }

bool PvtAxisDefinition::operator==(const PvtAxisDefinition& other) const {
    return std::tie(
    axisNumber,
    axisType
    ) == std::tie(
    other.axisNumber,
    other.axisType
    );
}

int PvtAxisDefinition::getAxisNumber() const {
    return axisNumber;
}
void PvtAxisDefinition::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::optional<PvtAxisType> const& PvtAxisDefinition::getAxisType() const {
    return axisType;
}
void PvtAxisDefinition::setAxisType(std::optional<PvtAxisType> p_axisType) {
    axisType = std::move(p_axisType);
}

std::string PvtAxisDefinition::toString() const {
    std::stringstream ss;
    ss << "PvtAxisDefinition { ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "axisType: ";
    if (this->axisType.has_value()) {
        ss << PvtAxisType_toString(this->axisType.value());
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void PvtAxisDefinition::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PvtAxisDefinition>();
}

std::string PvtAxisDefinition::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
