// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/can_set_state_axis_response.private.h"

#include <sstream>

#pragma push_macro("error")
#undef error

namespace zaber {
namespace motion {
namespace ascii {

CanSetStateAxisResponse::CanSetStateAxisResponse() { }

CanSetStateAxisResponse::CanSetStateAxisResponse(
    std::optional<std::string> p_error,
    int p_axisNumber
) :
    error(std::move(p_error)),
    axisNumber(p_axisNumber)
{ }

CanSetStateAxisResponse::CanSetStateAxisResponse(
    int p_axisNumber
) :
    axisNumber(p_axisNumber)
{ }

bool CanSetStateAxisResponse::operator==(const CanSetStateAxisResponse& other) const {
    return std::tie(
    error,
    axisNumber
    ) == std::tie(
    other.error,
    other.axisNumber
    );
}

std::optional<std::string> const& CanSetStateAxisResponse::getError() const {
    return error;
}
void CanSetStateAxisResponse::setError(std::optional<std::string> p_error) {
    error = std::move(p_error);
}

int CanSetStateAxisResponse::getAxisNumber() const {
    return axisNumber;
}
void CanSetStateAxisResponse::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::string CanSetStateAxisResponse::toString() const {
    std::stringstream ss;
    ss << "CanSetStateAxisResponse { ";
    ss << "error: ";
    if (this->error.has_value()) {
        ss << this->error.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << " }";
    return ss.str();
}

void CanSetStateAxisResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CanSetStateAxisResponse>();
}

std::string CanSetStateAxisResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber

#pragma pop_macro("error")
