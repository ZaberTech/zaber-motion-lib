// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/device_identity.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/firmware_version.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceIdentity, deviceId, serialNumber, name, axisCount, firmwareVersion, isModified, isIntegrated)

} // namespace ascii
} // namespace motion
} // namespace zaber
