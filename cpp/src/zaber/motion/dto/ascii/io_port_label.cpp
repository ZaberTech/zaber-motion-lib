// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/io_port_label.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

IoPortLabel::IoPortLabel() { }

IoPortLabel::IoPortLabel(
    IoPortType p_portType,
    int p_channelNumber,
    std::string p_label
) :
    portType(std::move(p_portType)),
    channelNumber(p_channelNumber),
    label(std::move(p_label))
{ }

bool IoPortLabel::operator==(const IoPortLabel& other) const {
    return std::tie(
    portType,
    channelNumber,
    label
    ) == std::tie(
    other.portType,
    other.channelNumber,
    other.label
    );
}

IoPortType const& IoPortLabel::getPortType() const {
    return portType;
}
void IoPortLabel::setPortType(IoPortType p_portType) {
    portType = std::move(p_portType);
}

int IoPortLabel::getChannelNumber() const {
    return channelNumber;
}
void IoPortLabel::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string const& IoPortLabel::getLabel() const {
    return label;
}
void IoPortLabel::setLabel(std::string p_label) {
    label = std::move(p_label);
}

std::string IoPortLabel::toString() const {
    std::stringstream ss;
    ss << "IoPortLabel { ";
    ss << "portType: ";
    ss << IoPortType_toString(this->portType);
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "label: ";
    ss << this->label;
    ss << " }";
    return ss.str();
}

void IoPortLabel::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<IoPortLabel>();
}

std::string IoPortLabel::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
