// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/message_type.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string MessageType_toString(MessageType value) {
    switch (value) {
        case MessageType::REPLY: return "REPLY";
        case MessageType::INFO: return "INFO";
        case MessageType::ALERT: return "ALERT";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
