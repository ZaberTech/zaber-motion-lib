// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/stream_axis_definition.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

StreamAxisDefinition::StreamAxisDefinition() { }

StreamAxisDefinition::StreamAxisDefinition(
    int p_axisNumber,
    std::optional<StreamAxisType> p_axisType
) :
    axisNumber(p_axisNumber),
    axisType(std::move(p_axisType))
{ }

StreamAxisDefinition::StreamAxisDefinition(
    int p_axisNumber
) :
    axisNumber(p_axisNumber)
{ }

bool StreamAxisDefinition::operator==(const StreamAxisDefinition& other) const {
    return std::tie(
    axisNumber,
    axisType
    ) == std::tie(
    other.axisNumber,
    other.axisType
    );
}

int StreamAxisDefinition::getAxisNumber() const {
    return axisNumber;
}
void StreamAxisDefinition::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::optional<StreamAxisType> const& StreamAxisDefinition::getAxisType() const {
    return axisType;
}
void StreamAxisDefinition::setAxisType(std::optional<StreamAxisType> p_axisType) {
    axisType = std::move(p_axisType);
}

std::string StreamAxisDefinition::toString() const {
    std::stringstream ss;
    ss << "StreamAxisDefinition { ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "axisType: ";
    if (this->axisType.has_value()) {
        ss << StreamAxisType_toString(this->axisType.value());
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void StreamAxisDefinition::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamAxisDefinition>();
}

std::string StreamAxisDefinition::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
