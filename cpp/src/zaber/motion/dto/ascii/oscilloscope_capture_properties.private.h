// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/oscilloscope_capture_properties.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(OscilloscopeCaptureProperties, dataSource, setting, axisNumber, ioType, ioChannel)

} // namespace ascii
} // namespace motion
} // namespace zaber
