// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/trigger_state.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

TriggerState::TriggerState() { }

TriggerState::TriggerState(
    std::string p_condition,
    std::vector<std::string> p_actions,
    bool p_enabled,
    int p_firesTotal,
    int p_firesRemaining
) :
    condition(std::move(p_condition)),
    actions(std::move(p_actions)),
    enabled(p_enabled),
    firesTotal(p_firesTotal),
    firesRemaining(p_firesRemaining)
{ }

bool TriggerState::operator==(const TriggerState& other) const {
    return std::tie(
    condition,
    actions,
    enabled,
    firesTotal,
    firesRemaining
    ) == std::tie(
    other.condition,
    other.actions,
    other.enabled,
    other.firesTotal,
    other.firesRemaining
    );
}

std::string const& TriggerState::getCondition() const {
    return condition;
}
void TriggerState::setCondition(std::string p_condition) {
    condition = std::move(p_condition);
}

std::vector<std::string> const& TriggerState::getActions() const {
    return actions;
}
void TriggerState::setActions(std::vector<std::string> p_actions) {
    actions = std::move(p_actions);
}

bool TriggerState::getEnabled() const {
    return enabled;
}
void TriggerState::setEnabled(bool p_enabled) {
    enabled = p_enabled;
}

int TriggerState::getFiresTotal() const {
    return firesTotal;
}
void TriggerState::setFiresTotal(int p_firesTotal) {
    firesTotal = p_firesTotal;
}

int TriggerState::getFiresRemaining() const {
    return firesRemaining;
}
void TriggerState::setFiresRemaining(int p_firesRemaining) {
    firesRemaining = p_firesRemaining;
}

std::string TriggerState::toString() const {
    std::stringstream ss;
    ss << "TriggerState { ";
    ss << "condition: ";
    ss << this->condition;
    ss << ", ";
    ss << "actions: ";
    ss << "[ ";
    for (size_t i = 0; i < this->actions.size(); i++) {
        ss << this->actions[i];
        if (i < this->actions.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "enabled: ";
    ss << this->enabled;
    ss << ", ";
    ss << "firesTotal: ";
    ss << this->firesTotal;
    ss << ", ";
    ss << "firesRemaining: ";
    ss << this->firesRemaining;
    ss << " }";
    return ss.str();
}

void TriggerState::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerState>();
}

std::string TriggerState::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
