// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/servo_tuning_paramset.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string ServoTuningParamset_toString(ServoTuningParamset value) {
    switch (value) {
        case ServoTuningParamset::LIVE: return "LIVE";
        case ServoTuningParamset::P_1: return "P_1";
        case ServoTuningParamset::P_2: return "P_2";
        case ServoTuningParamset::P_3: return "P_3";
        case ServoTuningParamset::P_4: return "P_4";
        case ServoTuningParamset::P_5: return "P_5";
        case ServoTuningParamset::P_6: return "P_6";
        case ServoTuningParamset::P_7: return "P_7";
        case ServoTuningParamset::P_8: return "P_8";
        case ServoTuningParamset::P_9: return "P_9";
        case ServoTuningParamset::STAGING: return "STAGING";
        case ServoTuningParamset::DEFAULT: return "DEFAULT";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
