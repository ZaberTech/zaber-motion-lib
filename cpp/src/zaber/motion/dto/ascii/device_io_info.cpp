// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/device_io_info.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

DeviceIOInfo::DeviceIOInfo() { }

DeviceIOInfo::DeviceIOInfo(
    int p_numberAnalogOutputs,
    int p_numberAnalogInputs,
    int p_numberDigitalOutputs,
    int p_numberDigitalInputs
) :
    numberAnalogOutputs(p_numberAnalogOutputs),
    numberAnalogInputs(p_numberAnalogInputs),
    numberDigitalOutputs(p_numberDigitalOutputs),
    numberDigitalInputs(p_numberDigitalInputs)
{ }

bool DeviceIOInfo::operator==(const DeviceIOInfo& other) const {
    return std::tie(
    numberAnalogOutputs,
    numberAnalogInputs,
    numberDigitalOutputs,
    numberDigitalInputs
    ) == std::tie(
    other.numberAnalogOutputs,
    other.numberAnalogInputs,
    other.numberDigitalOutputs,
    other.numberDigitalInputs
    );
}

int DeviceIOInfo::getNumberAnalogOutputs() const {
    return numberAnalogOutputs;
}
void DeviceIOInfo::setNumberAnalogOutputs(int p_numberAnalogOutputs) {
    numberAnalogOutputs = p_numberAnalogOutputs;
}

int DeviceIOInfo::getNumberAnalogInputs() const {
    return numberAnalogInputs;
}
void DeviceIOInfo::setNumberAnalogInputs(int p_numberAnalogInputs) {
    numberAnalogInputs = p_numberAnalogInputs;
}

int DeviceIOInfo::getNumberDigitalOutputs() const {
    return numberDigitalOutputs;
}
void DeviceIOInfo::setNumberDigitalOutputs(int p_numberDigitalOutputs) {
    numberDigitalOutputs = p_numberDigitalOutputs;
}

int DeviceIOInfo::getNumberDigitalInputs() const {
    return numberDigitalInputs;
}
void DeviceIOInfo::setNumberDigitalInputs(int p_numberDigitalInputs) {
    numberDigitalInputs = p_numberDigitalInputs;
}

std::string DeviceIOInfo::toString() const {
    std::stringstream ss;
    ss << "DeviceIOInfo { ";
    ss << "numberAnalogOutputs: ";
    ss << this->numberAnalogOutputs;
    ss << ", ";
    ss << "numberAnalogInputs: ";
    ss << this->numberAnalogInputs;
    ss << ", ";
    ss << "numberDigitalOutputs: ";
    ss << this->numberDigitalOutputs;
    ss << ", ";
    ss << "numberDigitalInputs: ";
    ss << this->numberDigitalInputs;
    ss << " }";
    return ss.str();
}

void DeviceIOInfo::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceIOInfo>();
}

std::string DeviceIOInfo::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
