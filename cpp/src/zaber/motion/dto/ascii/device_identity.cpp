// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/device_identity.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

DeviceIdentity::DeviceIdentity() { }

DeviceIdentity::DeviceIdentity(
    int p_deviceId,
    unsigned int p_serialNumber,
    std::string p_name,
    int p_axisCount,
    FirmwareVersion p_firmwareVersion,
    bool p_isModified,
    bool p_isIntegrated
) :
    deviceId(p_deviceId),
    serialNumber(p_serialNumber),
    name(std::move(p_name)),
    axisCount(p_axisCount),
    firmwareVersion(std::move(p_firmwareVersion)),
    isModified(p_isModified),
    isIntegrated(p_isIntegrated)
{ }

bool DeviceIdentity::operator==(const DeviceIdentity& other) const {
    return std::tie(
    deviceId,
    serialNumber,
    name,
    axisCount,
    firmwareVersion,
    isModified,
    isIntegrated
    ) == std::tie(
    other.deviceId,
    other.serialNumber,
    other.name,
    other.axisCount,
    other.firmwareVersion,
    other.isModified,
    other.isIntegrated
    );
}

int DeviceIdentity::getDeviceId() const {
    return deviceId;
}
void DeviceIdentity::setDeviceId(int p_deviceId) {
    deviceId = p_deviceId;
}

unsigned int DeviceIdentity::getSerialNumber() const {
    return serialNumber;
}
void DeviceIdentity::setSerialNumber(unsigned int p_serialNumber) {
    serialNumber = p_serialNumber;
}

std::string const& DeviceIdentity::getName() const {
    return name;
}
void DeviceIdentity::setName(std::string p_name) {
    name = std::move(p_name);
}

int DeviceIdentity::getAxisCount() const {
    return axisCount;
}
void DeviceIdentity::setAxisCount(int p_axisCount) {
    axisCount = p_axisCount;
}

FirmwareVersion const& DeviceIdentity::getFirmwareVersion() const {
    return firmwareVersion;
}
void DeviceIdentity::setFirmwareVersion(FirmwareVersion p_firmwareVersion) {
    firmwareVersion = std::move(p_firmwareVersion);
}

bool DeviceIdentity::getIsModified() const {
    return isModified;
}
void DeviceIdentity::setIsModified(bool p_isModified) {
    isModified = p_isModified;
}

bool DeviceIdentity::getIsIntegrated() const {
    return isIntegrated;
}
void DeviceIdentity::setIsIntegrated(bool p_isIntegrated) {
    isIntegrated = p_isIntegrated;
}

std::string DeviceIdentity::toString() const {
    std::stringstream ss;
    ss << "DeviceIdentity { ";
    ss << "deviceId: ";
    ss << this->deviceId;
    ss << ", ";
    ss << "serialNumber: ";
    ss << this->serialNumber;
    ss << ", ";
    ss << "name: ";
    ss << this->name;
    ss << ", ";
    ss << "axisCount: ";
    ss << this->axisCount;
    ss << ", ";
    ss << "firmwareVersion: ";
    ss << this->firmwareVersion.toString();
    ss << ", ";
    ss << "isModified: ";
    ss << this->isModified;
    ss << ", ";
    ss << "isIntegrated: ";
    ss << this->isIntegrated;
    ss << " }";
    return ss.str();
}

void DeviceIdentity::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceIdentity>();
}

std::string DeviceIdentity::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
