// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/servo_tuning_param.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

ServoTuningParam::ServoTuningParam() { }

ServoTuningParam::ServoTuningParam(
    std::string p_name,
    double p_value
) :
    name(std::move(p_name)),
    value(p_value)
{ }

bool ServoTuningParam::operator==(const ServoTuningParam& other) const {
    return std::tie(
    name,
    value
    ) == std::tie(
    other.name,
    other.value
    );
}

std::string const& ServoTuningParam::getName() const {
    return name;
}
void ServoTuningParam::setName(std::string p_name) {
    name = std::move(p_name);
}

double ServoTuningParam::getValue() const {
    return value;
}
void ServoTuningParam::setValue(double p_value) {
    value = p_value;
}

std::string ServoTuningParam::toString() const {
    std::stringstream ss;
    ss << "ServoTuningParam { ";
    ss << "name: ";
    ss << this->name;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void ServoTuningParam::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ServoTuningParam>();
}

std::string ServoTuningParam::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
