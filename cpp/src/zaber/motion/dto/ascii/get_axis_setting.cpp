// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/get_axis_setting.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace ascii {

GetAxisSetting::GetAxisSetting() { }

GetAxisSetting::GetAxisSetting(
    std::string p_setting,
    std::optional<Units> p_unit
) :
    setting(std::move(p_setting)),
    unit(p_unit)
{ }

GetAxisSetting::GetAxisSetting(
    std::string p_setting
) :
    setting(std::move(p_setting))
{ }

bool GetAxisSetting::operator==(const GetAxisSetting& other) const {
    return std::tie(
    setting,
    unit
    ) == std::tie(
    other.setting,
    other.unit
    );
}

std::string const& GetAxisSetting::getSetting() const {
    return setting;
}
void GetAxisSetting::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::optional<Units> GetAxisSetting::getUnit() const {
    return unit;
}
void GetAxisSetting::setUnit(std::optional<Units> p_unit) {
    unit = p_unit;
}

std::string GetAxisSetting::toString() const {
    std::stringstream ss;
    ss << "GetAxisSetting { ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "unit: ";
    if (this->unit.has_value()) {
        ss << getUnitLongName(this->unit.value());
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void GetAxisSetting::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetAxisSetting>();
}

std::string GetAxisSetting::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace ascii
} // namespace motion
} // namespace zaber
