// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/ascii/pvt_axis_type.h"

namespace zaber {
namespace motion {
namespace ascii {

std::string PvtAxisType_toString(PvtAxisType value) {
    switch (value) {
        case PvtAxisType::PHYSICAL: return "PHYSICAL";
        case PvtAxisType::LOCKSTEP: return "LOCKSTEP";
    }
    return "<Invalid value>";
}

} // namespace ascii
} // namespace motion
} // namespace zaber
