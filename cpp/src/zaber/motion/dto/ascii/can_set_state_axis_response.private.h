// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/can_set_state_axis_response.h"

#include "zaber/motion/utils/serialization_utils.private.h"

#pragma push_macro("error")
#undef error

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(CanSetStateAxisResponse, error, axisNumber)

} // namespace ascii
} // namespace motion
} // namespace zaber

#pragma pop_macro("error")
