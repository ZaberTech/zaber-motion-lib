// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/ascii/device_io_info.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace ascii {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceIOInfo, numberAnalogOutputs, numberAnalogInputs, numberDigitalOutputs, numberDigitalInputs)

} // namespace ascii
} // namespace motion
} // namespace zaber
