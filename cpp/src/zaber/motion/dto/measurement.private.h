// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/measurement.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Measurement, value, unit)

} // namespace motion
} // namespace zaber
