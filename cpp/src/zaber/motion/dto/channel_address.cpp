// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/channel_address.private.h"

#include <sstream>

namespace zaber {
namespace motion {

ChannelAddress::ChannelAddress() { }

ChannelAddress::ChannelAddress(
    int p_device,
    int p_channel
) :
    device(p_device),
    channel(p_channel)
{ }

bool ChannelAddress::operator==(const ChannelAddress& other) const {
    return std::tie(
    device,
    channel
    ) == std::tie(
    other.device,
    other.channel
    );
}

int ChannelAddress::getDevice() const {
    return device;
}
void ChannelAddress::setDevice(int p_device) {
    device = p_device;
}

int ChannelAddress::getChannel() const {
    return channel;
}
void ChannelAddress::setChannel(int p_channel) {
    channel = p_channel;
}

std::string ChannelAddress::toString() const {
    std::stringstream ss;
    ss << "ChannelAddress { ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channel: ";
    ss << this->channel;
    ss << " }";
    return ss.str();
}

void ChannelAddress::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ChannelAddress>();
}

std::string ChannelAddress::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace motion
} // namespace zaber
