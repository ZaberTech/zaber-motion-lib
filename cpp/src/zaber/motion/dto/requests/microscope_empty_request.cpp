// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/microscope_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

MicroscopeEmptyRequest::MicroscopeEmptyRequest() { }

MicroscopeEmptyRequest::MicroscopeEmptyRequest(
    int p_interfaceId,
    microscopy::MicroscopeConfig p_config
) :
    interfaceId(p_interfaceId),
    config(std::move(p_config))
{ }

bool MicroscopeEmptyRequest::operator==(const MicroscopeEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    config
    ) == std::tie(
    other.interfaceId,
    other.config
    );
}

int MicroscopeEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void MicroscopeEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

microscopy::MicroscopeConfig const& MicroscopeEmptyRequest::getConfig() const {
    return config;
}
void MicroscopeEmptyRequest::setConfig(microscopy::MicroscopeConfig p_config) {
    config = std::move(p_config);
}

std::string MicroscopeEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "MicroscopeEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "config: ";
    ss << this->config.toString();
    ss << " }";
    return ss.str();
}

void MicroscopeEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeEmptyRequest>();
}

std::string MicroscopeEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
