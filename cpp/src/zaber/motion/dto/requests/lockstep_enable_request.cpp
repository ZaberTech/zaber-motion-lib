// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_enable_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepEnableRequest::LockstepEnableRequest() { }

LockstepEnableRequest::LockstepEnableRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    std::vector<int> p_axes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    axes(std::move(p_axes))
{ }

bool LockstepEnableRequest::operator==(const LockstepEnableRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    axes
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.axes
    );
}

int LockstepEnableRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepEnableRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepEnableRequest::getDevice() const {
    return device;
}
void LockstepEnableRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepEnableRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepEnableRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

std::vector<int> const& LockstepEnableRequest::getAxes() const {
    return axes;
}
void LockstepEnableRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::string LockstepEnableRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepEnableRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void LockstepEnableRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepEnableRequest>();
}

std::string LockstepEnableRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
