// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_get_axes_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGetAxesResponse::StreamGetAxesResponse() { }

StreamGetAxesResponse::StreamGetAxesResponse(
    std::vector<ascii::StreamAxisDefinition> p_axes,
    std::vector<ascii::PvtAxisDefinition> p_pvtAxes
) :
    axes(std::move(p_axes)),
    pvtAxes(std::move(p_pvtAxes))
{ }

bool StreamGetAxesResponse::operator==(const StreamGetAxesResponse& other) const {
    return std::tie(
    axes,
    pvtAxes
    ) == std::tie(
    other.axes,
    other.pvtAxes
    );
}

std::vector<ascii::StreamAxisDefinition> const& StreamGetAxesResponse::getAxes() const {
    return axes;
}
void StreamGetAxesResponse::setAxes(std::vector<ascii::StreamAxisDefinition> p_axes) {
    axes = std::move(p_axes);
}

std::vector<ascii::PvtAxisDefinition> const& StreamGetAxesResponse::getPvtAxes() const {
    return pvtAxes;
}
void StreamGetAxesResponse::setPvtAxes(std::vector<ascii::PvtAxisDefinition> p_pvtAxes) {
    pvtAxes = std::move(p_pvtAxes);
}

std::string StreamGetAxesResponse::toString() const {
    std::stringstream ss;
    ss << "StreamGetAxesResponse { ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i].toString();
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "pvtAxes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->pvtAxes.size(); i++) {
        ss << this->pvtAxes[i].toString();
        if (i < this->pvtAxes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamGetAxesResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGetAxesResponse>();
}

std::string StreamGetAxesResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
