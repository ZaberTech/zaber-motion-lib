// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/load_paramset.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LoadParamset::LoadParamset() { }

LoadParamset::LoadParamset(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_toParamset,
    ascii::ServoTuningParamset p_fromParamset
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    toParamset(std::move(p_toParamset)),
    fromParamset(std::move(p_fromParamset))
{ }

bool LoadParamset::operator==(const LoadParamset& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    toParamset,
    fromParamset
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.toParamset,
    other.fromParamset
    );
}

int LoadParamset::getInterfaceId() const {
    return interfaceId;
}
void LoadParamset::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LoadParamset::getDevice() const {
    return device;
}
void LoadParamset::setDevice(int p_device) {
    device = p_device;
}

int LoadParamset::getAxis() const {
    return axis;
}
void LoadParamset::setAxis(int p_axis) {
    axis = p_axis;
}

ascii::ServoTuningParamset const& LoadParamset::getToParamset() const {
    return toParamset;
}
void LoadParamset::setToParamset(ascii::ServoTuningParamset p_toParamset) {
    toParamset = std::move(p_toParamset);
}

ascii::ServoTuningParamset const& LoadParamset::getFromParamset() const {
    return fromParamset;
}
void LoadParamset::setFromParamset(ascii::ServoTuningParamset p_fromParamset) {
    fromParamset = std::move(p_fromParamset);
}

std::string LoadParamset::toString() const {
    std::stringstream ss;
    ss << "LoadParamset { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "toParamset: ";
    ss << ServoTuningParamset_toString(this->toParamset);
    ss << ", ";
    ss << "fromParamset: ";
    ss << ServoTuningParamset_toString(this->fromParamset);
    ss << " }";
    return ss.str();
}

void LoadParamset::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LoadParamset>();
}

std::string LoadParamset::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
