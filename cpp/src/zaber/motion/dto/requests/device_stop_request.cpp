// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_stop_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceStopRequest::DeviceStopRequest() { }

DeviceStopRequest::DeviceStopRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool DeviceStopRequest::operator==(const DeviceStopRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.waitUntilIdle
    );
}

int DeviceStopRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceStopRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceStopRequest::getDevice() const {
    return device;
}
void DeviceStopRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceStopRequest::getAxis() const {
    return axis;
}
void DeviceStopRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceStopRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void DeviceStopRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string DeviceStopRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceStopRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void DeviceStopRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceStopRequest>();
}

std::string DeviceStopRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
