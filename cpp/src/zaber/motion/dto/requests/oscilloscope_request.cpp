// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeRequest::OscilloscopeRequest() { }

OscilloscopeRequest::OscilloscopeRequest(
    int p_interfaceId,
    int p_device
) :
    interfaceId(p_interfaceId),
    device(p_device)
{ }

bool OscilloscopeRequest::operator==(const OscilloscopeRequest& other) const {
    return std::tie(
    interfaceId,
    device
    ) == std::tie(
    other.interfaceId,
    other.device
    );
}

int OscilloscopeRequest::getInterfaceId() const {
    return interfaceId;
}
void OscilloscopeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int OscilloscopeRequest::getDevice() const {
    return device;
}
void OscilloscopeRequest::setDevice(int p_device) {
    device = p_device;
}

std::string OscilloscopeRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << " }";
    return ss.str();
}

void OscilloscopeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeRequest>();
}

std::string OscilloscopeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
