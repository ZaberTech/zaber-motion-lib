// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_disable_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepDisableRequest::LockstepDisableRequest() { }

LockstepDisableRequest::LockstepDisableRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool LockstepDisableRequest::operator==(const LockstepDisableRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.waitUntilIdle
    );
}

int LockstepDisableRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepDisableRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepDisableRequest::getDevice() const {
    return device;
}
void LockstepDisableRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepDisableRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepDisableRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

bool LockstepDisableRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void LockstepDisableRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string LockstepDisableRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepDisableRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void LockstepDisableRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepDisableRequest>();
}

std::string LockstepDisableRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
