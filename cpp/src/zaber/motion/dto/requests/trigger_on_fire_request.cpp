// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_on_fire_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerOnFireRequest::TriggerOnFireRequest() { }

TriggerOnFireRequest::TriggerOnFireRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    ascii::TriggerAction p_action,
    int p_axis,
    std::string p_command
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    action(std::move(p_action)),
    axis(p_axis),
    command(std::move(p_command))
{ }

bool TriggerOnFireRequest::operator==(const TriggerOnFireRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    action,
    axis,
    command
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.action,
    other.axis,
    other.command
    );
}

int TriggerOnFireRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerOnFireRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerOnFireRequest::getDevice() const {
    return device;
}
void TriggerOnFireRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerOnFireRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerOnFireRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

ascii::TriggerAction const& TriggerOnFireRequest::getAction() const {
    return action;
}
void TriggerOnFireRequest::setAction(ascii::TriggerAction p_action) {
    action = std::move(p_action);
}

int TriggerOnFireRequest::getAxis() const {
    return axis;
}
void TriggerOnFireRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& TriggerOnFireRequest::getCommand() const {
    return command;
}
void TriggerOnFireRequest::setCommand(std::string p_command) {
    command = std::move(p_command);
}

std::string TriggerOnFireRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerOnFireRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "action: ";
    ss << TriggerAction_toString(this->action);
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << " }";
    return ss.str();
}

void TriggerOnFireRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerOnFireRequest>();
}

std::string TriggerOnFireRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
