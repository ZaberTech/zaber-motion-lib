// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_all_analog_io_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetAllAnalogIOResponse::DeviceGetAllAnalogIOResponse() { }

DeviceGetAllAnalogIOResponse::DeviceGetAllAnalogIOResponse(
    std::vector<double> p_values
) :
    values(std::move(p_values))
{ }

bool DeviceGetAllAnalogIOResponse::operator==(const DeviceGetAllAnalogIOResponse& other) const {
    return std::tie(
    values
    ) == std::tie(
    other.values
    );
}

std::vector<double> const& DeviceGetAllAnalogIOResponse::getValues() const {
    return values;
}
void DeviceGetAllAnalogIOResponse::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::string DeviceGetAllAnalogIOResponse::toString() const {
    std::stringstream ss;
    ss << "DeviceGetAllAnalogIOResponse { ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceGetAllAnalogIOResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetAllAnalogIOResponse>();
}

std::string DeviceGetAllAnalogIOResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
