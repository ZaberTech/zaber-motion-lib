// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/get_all_io_port_labels_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GetAllIoPortLabelsResponse::GetAllIoPortLabelsResponse() { }

GetAllIoPortLabelsResponse::GetAllIoPortLabelsResponse(
    std::vector<ascii::IoPortLabel> p_labels
) :
    labels(std::move(p_labels))
{ }

bool GetAllIoPortLabelsResponse::operator==(const GetAllIoPortLabelsResponse& other) const {
    return std::tie(
    labels
    ) == std::tie(
    other.labels
    );
}

std::vector<ascii::IoPortLabel> const& GetAllIoPortLabelsResponse::getLabels() const {
    return labels;
}
void GetAllIoPortLabelsResponse::setLabels(std::vector<ascii::IoPortLabel> p_labels) {
    labels = std::move(p_labels);
}

std::string GetAllIoPortLabelsResponse::toString() const {
    std::stringstream ss;
    ss << "GetAllIoPortLabelsResponse { ";
    ss << "labels: ";
    ss << "[ ";
    for (size_t i = 0; i < this->labels.size(); i++) {
        ss << this->labels[i].toString();
        if (i < this->labels.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void GetAllIoPortLabelsResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetAllIoPortLabelsResponse>();
}

std::string GetAllIoPortLabelsResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
