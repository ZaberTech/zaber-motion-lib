// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/microscope_find_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/microscopy/third_party_components.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(MicroscopeFindRequest, interfaceId, thirdParty)

} // namespace requests
} // namespace motion
} // namespace zaber
