// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/objective_changer_change_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ObjectiveChangerChangeRequest, interfaceId, turretAddress, focusAddress, focusAxis, objective, focusOffset)

} // namespace requests
} // namespace motion
} // namespace zaber
