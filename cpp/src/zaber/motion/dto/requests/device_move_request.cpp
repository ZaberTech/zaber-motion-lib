// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_move_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceMoveRequest::DeviceMoveRequest() { }

DeviceMoveRequest::DeviceMoveRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_waitUntilIdle,
    AxisMoveType p_type,
    double p_arg,
    int p_argInt,
    Units p_unit,
    double p_velocity,
    Units p_velocityUnit,
    double p_acceleration,
    Units p_accelerationUnit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    waitUntilIdle(p_waitUntilIdle),
    type(std::move(p_type)),
    arg(p_arg),
    argInt(p_argInt),
    unit(p_unit),
    velocity(p_velocity),
    velocityUnit(p_velocityUnit),
    acceleration(p_acceleration),
    accelerationUnit(p_accelerationUnit)
{ }

bool DeviceMoveRequest::operator==(const DeviceMoveRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    waitUntilIdle,
    type,
    arg,
    argInt,
    unit,
    velocity,
    velocityUnit,
    acceleration,
    accelerationUnit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.waitUntilIdle,
    other.type,
    other.arg,
    other.argInt,
    other.unit,
    other.velocity,
    other.velocityUnit,
    other.acceleration,
    other.accelerationUnit
    );
}

int DeviceMoveRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceMoveRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceMoveRequest::getDevice() const {
    return device;
}
void DeviceMoveRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceMoveRequest::getAxis() const {
    return axis;
}
void DeviceMoveRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceMoveRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void DeviceMoveRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

AxisMoveType const& DeviceMoveRequest::getType() const {
    return type;
}
void DeviceMoveRequest::setType(AxisMoveType p_type) {
    type = std::move(p_type);
}

double DeviceMoveRequest::getArg() const {
    return arg;
}
void DeviceMoveRequest::setArg(double p_arg) {
    arg = p_arg;
}

int DeviceMoveRequest::getArgInt() const {
    return argInt;
}
void DeviceMoveRequest::setArgInt(int p_argInt) {
    argInt = p_argInt;
}

Units DeviceMoveRequest::getUnit() const {
    return unit;
}
void DeviceMoveRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

double DeviceMoveRequest::getVelocity() const {
    return velocity;
}
void DeviceMoveRequest::setVelocity(double p_velocity) {
    velocity = p_velocity;
}

Units DeviceMoveRequest::getVelocityUnit() const {
    return velocityUnit;
}
void DeviceMoveRequest::setVelocityUnit(Units p_velocityUnit) {
    velocityUnit = p_velocityUnit;
}

double DeviceMoveRequest::getAcceleration() const {
    return acceleration;
}
void DeviceMoveRequest::setAcceleration(double p_acceleration) {
    acceleration = p_acceleration;
}

Units DeviceMoveRequest::getAccelerationUnit() const {
    return accelerationUnit;
}
void DeviceMoveRequest::setAccelerationUnit(Units p_accelerationUnit) {
    accelerationUnit = p_accelerationUnit;
}

std::string DeviceMoveRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceMoveRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << ", ";
    ss << "type: ";
    ss << AxisMoveType_toString(this->type);
    ss << ", ";
    ss << "arg: ";
    ss << this->arg;
    ss << ", ";
    ss << "argInt: ";
    ss << this->argInt;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "velocity: ";
    ss << this->velocity;
    ss << ", ";
    ss << "velocityUnit: ";
    ss << getUnitLongName(this->velocityUnit);
    ss << ", ";
    ss << "acceleration: ";
    ss << this->acceleration;
    ss << ", ";
    ss << "accelerationUnit: ";
    ss << getUnitLongName(this->accelerationUnit);
    ss << " }";
    return ss.str();
}

void DeviceMoveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceMoveRequest>();
}

std::string DeviceMoveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
