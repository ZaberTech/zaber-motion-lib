// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axis_to_string_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AxisToStringRequest::AxisToStringRequest() { }

AxisToStringRequest::AxisToStringRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_typeOverride
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    typeOverride(std::move(p_typeOverride))
{ }

bool AxisToStringRequest::operator==(const AxisToStringRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    typeOverride
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.typeOverride
    );
}

int AxisToStringRequest::getInterfaceId() const {
    return interfaceId;
}
void AxisToStringRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int AxisToStringRequest::getDevice() const {
    return device;
}
void AxisToStringRequest::setDevice(int p_device) {
    device = p_device;
}

int AxisToStringRequest::getAxis() const {
    return axis;
}
void AxisToStringRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& AxisToStringRequest::getTypeOverride() const {
    return typeOverride;
}
void AxisToStringRequest::setTypeOverride(std::string p_typeOverride) {
    typeOverride = std::move(p_typeOverride);
}

std::string AxisToStringRequest::toString() const {
    std::stringstream ss;
    ss << "AxisToStringRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "typeOverride: ";
    ss << this->typeOverride;
    ss << " }";
    return ss.str();
}

void AxisToStringRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisToStringRequest>();
}

std::string AxisToStringRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
