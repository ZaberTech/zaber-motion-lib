// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_add_io_channel_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeAddIoChannelRequest::OscilloscopeAddIoChannelRequest() { }

OscilloscopeAddIoChannelRequest::OscilloscopeAddIoChannelRequest(
    int p_interfaceId,
    int p_device,
    ascii::IoPortType p_ioType,
    int p_ioChannel
) :
    interfaceId(p_interfaceId),
    device(p_device),
    ioType(std::move(p_ioType)),
    ioChannel(p_ioChannel)
{ }

bool OscilloscopeAddIoChannelRequest::operator==(const OscilloscopeAddIoChannelRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    ioType,
    ioChannel
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.ioType,
    other.ioChannel
    );
}

int OscilloscopeAddIoChannelRequest::getInterfaceId() const {
    return interfaceId;
}
void OscilloscopeAddIoChannelRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int OscilloscopeAddIoChannelRequest::getDevice() const {
    return device;
}
void OscilloscopeAddIoChannelRequest::setDevice(int p_device) {
    device = p_device;
}

ascii::IoPortType const& OscilloscopeAddIoChannelRequest::getIoType() const {
    return ioType;
}
void OscilloscopeAddIoChannelRequest::setIoType(ascii::IoPortType p_ioType) {
    ioType = std::move(p_ioType);
}

int OscilloscopeAddIoChannelRequest::getIoChannel() const {
    return ioChannel;
}
void OscilloscopeAddIoChannelRequest::setIoChannel(int p_ioChannel) {
    ioChannel = p_ioChannel;
}

std::string OscilloscopeAddIoChannelRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeAddIoChannelRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "ioType: ";
    ss << IoPortType_toString(this->ioType);
    ss << ", ";
    ss << "ioChannel: ";
    ss << this->ioChannel;
    ss << " }";
    return ss.str();
}

void OscilloscopeAddIoChannelRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeAddIoChannelRequest>();
}

std::string OscilloscopeAddIoChannelRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
