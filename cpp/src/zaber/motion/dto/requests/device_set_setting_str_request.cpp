// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_setting_str_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetSettingStrRequest::DeviceSetSettingStrRequest() { }

DeviceSetSettingStrRequest::DeviceSetSettingStrRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_setting,
    std::string p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    setting(std::move(p_setting)),
    value(std::move(p_value))
{ }

bool DeviceSetSettingStrRequest::operator==(const DeviceSetSettingStrRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    setting,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.setting,
    other.value
    );
}

int DeviceSetSettingStrRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetSettingStrRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetSettingStrRequest::getDevice() const {
    return device;
}
void DeviceSetSettingStrRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetSettingStrRequest::getAxis() const {
    return axis;
}
void DeviceSetSettingStrRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetSettingStrRequest::getSetting() const {
    return setting;
}
void DeviceSetSettingStrRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::string const& DeviceSetSettingStrRequest::getValue() const {
    return value;
}
void DeviceSetSettingStrRequest::setValue(std::string p_value) {
    value = std::move(p_value);
}

std::string DeviceSetSettingStrRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetSettingStrRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void DeviceSetSettingStrRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetSettingStrRequest>();
}

std::string DeviceSetSettingStrRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
