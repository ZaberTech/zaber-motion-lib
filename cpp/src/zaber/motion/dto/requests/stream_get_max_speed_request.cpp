// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_get_max_speed_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGetMaxSpeedRequest::StreamGetMaxSpeedRequest() { }

StreamGetMaxSpeedRequest::StreamGetMaxSpeedRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    unit(p_unit)
{ }

bool StreamGetMaxSpeedRequest::operator==(const StreamGetMaxSpeedRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.unit
    );
}

int StreamGetMaxSpeedRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamGetMaxSpeedRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamGetMaxSpeedRequest::getDevice() const {
    return device;
}
void StreamGetMaxSpeedRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamGetMaxSpeedRequest::getStreamId() const {
    return streamId;
}
void StreamGetMaxSpeedRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamGetMaxSpeedRequest::getPvt() const {
    return pvt;
}
void StreamGetMaxSpeedRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

Units StreamGetMaxSpeedRequest::getUnit() const {
    return unit;
}
void StreamGetMaxSpeedRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamGetMaxSpeedRequest::toString() const {
    std::stringstream ss;
    ss << "StreamGetMaxSpeedRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamGetMaxSpeedRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGetMaxSpeedRequest>();
}

std::string StreamGetMaxSpeedRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
