// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unit_get_enum_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnitGetEnumResponse::UnitGetEnumResponse() { }

UnitGetEnumResponse::UnitGetEnumResponse(
    Units p_unit
) :
    unit(p_unit)
{ }

bool UnitGetEnumResponse::operator==(const UnitGetEnumResponse& other) const {
    return std::tie(
    unit
    ) == std::tie(
    other.unit
    );
}

Units UnitGetEnumResponse::getUnit() const {
    return unit;
}
void UnitGetEnumResponse::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string UnitGetEnumResponse::toString() const {
    std::stringstream ss;
    ss << "UnitGetEnumResponse { ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void UnitGetEnumResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnitGetEnumResponse>();
}

std::string UnitGetEnumResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
