// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_create_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorCreateRequest::TranslatorCreateRequest() { }

TranslatorCreateRequest::TranslatorCreateRequest(
    gcode::DeviceDefinition p_definition,
    std::optional<gcode::TranslatorConfig> p_config
) :
    definition(std::move(p_definition)),
    config(std::move(p_config))
{ }

TranslatorCreateRequest::TranslatorCreateRequest(
    gcode::DeviceDefinition p_definition
) :
    definition(std::move(p_definition))
{ }

bool TranslatorCreateRequest::operator==(const TranslatorCreateRequest& other) const {
    return std::tie(
    definition,
    config
    ) == std::tie(
    other.definition,
    other.config
    );
}

gcode::DeviceDefinition const& TranslatorCreateRequest::getDefinition() const {
    return definition;
}
void TranslatorCreateRequest::setDefinition(gcode::DeviceDefinition p_definition) {
    definition = std::move(p_definition);
}

std::optional<gcode::TranslatorConfig> const& TranslatorCreateRequest::getConfig() const {
    return config;
}
void TranslatorCreateRequest::setConfig(std::optional<gcode::TranslatorConfig> p_config) {
    config = std::move(p_config);
}

std::string TranslatorCreateRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorCreateRequest { ";
    ss << "definition: ";
    ss << this->definition.toString();
    ss << ", ";
    ss << "config: ";
    if (this->config.has_value()) {
        ss << this->config.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void TranslatorCreateRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorCreateRequest>();
}

std::string TranslatorCreateRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
