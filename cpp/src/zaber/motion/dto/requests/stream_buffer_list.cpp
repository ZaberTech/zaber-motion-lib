// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_buffer_list.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamBufferList::StreamBufferList() { }

StreamBufferList::StreamBufferList(
    int p_interfaceId,
    int p_device,
    bool p_pvt
) :
    interfaceId(p_interfaceId),
    device(p_device),
    pvt(p_pvt)
{ }

bool StreamBufferList::operator==(const StreamBufferList& other) const {
    return std::tie(
    interfaceId,
    device,
    pvt
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.pvt
    );
}

int StreamBufferList::getInterfaceId() const {
    return interfaceId;
}
void StreamBufferList::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamBufferList::getDevice() const {
    return device;
}
void StreamBufferList::setDevice(int p_device) {
    device = p_device;
}

bool StreamBufferList::getPvt() const {
    return pvt;
}
void StreamBufferList::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::string StreamBufferList::toString() const {
    std::stringstream ss;
    ss << "StreamBufferList { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << " }";
    return ss.str();
}

void StreamBufferList::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamBufferList>();
}

std::string StreamBufferList::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
