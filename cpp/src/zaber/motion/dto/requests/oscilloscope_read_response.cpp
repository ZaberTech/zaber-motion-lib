// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_read_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeReadResponse::OscilloscopeReadResponse() { }

OscilloscopeReadResponse::OscilloscopeReadResponse(
    std::vector<int> p_dataIds
) :
    dataIds(std::move(p_dataIds))
{ }

bool OscilloscopeReadResponse::operator==(const OscilloscopeReadResponse& other) const {
    return std::tie(
    dataIds
    ) == std::tie(
    other.dataIds
    );
}

std::vector<int> const& OscilloscopeReadResponse::getDataIds() const {
    return dataIds;
}
void OscilloscopeReadResponse::setDataIds(std::vector<int> p_dataIds) {
    dataIds = std::move(p_dataIds);
}

std::string OscilloscopeReadResponse::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeReadResponse { ";
    ss << "dataIds: ";
    ss << "[ ";
    for (size_t i = 0; i < this->dataIds.size(); i++) {
        ss << this->dataIds[i];
        if (i < this->dataIds.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void OscilloscopeReadResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeReadResponse>();
}

std::string OscilloscopeReadResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
