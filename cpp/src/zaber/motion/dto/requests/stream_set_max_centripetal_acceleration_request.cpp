// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_max_centripetal_acceleration_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetMaxCentripetalAccelerationRequest::StreamSetMaxCentripetalAccelerationRequest() { }

StreamSetMaxCentripetalAccelerationRequest::StreamSetMaxCentripetalAccelerationRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    double p_maxCentripetalAcceleration,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    maxCentripetalAcceleration(p_maxCentripetalAcceleration),
    unit(p_unit)
{ }

bool StreamSetMaxCentripetalAccelerationRequest::operator==(const StreamSetMaxCentripetalAccelerationRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    maxCentripetalAcceleration,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.maxCentripetalAcceleration,
    other.unit
    );
}

int StreamSetMaxCentripetalAccelerationRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetMaxCentripetalAccelerationRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetMaxCentripetalAccelerationRequest::getDevice() const {
    return device;
}
void StreamSetMaxCentripetalAccelerationRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetMaxCentripetalAccelerationRequest::getStreamId() const {
    return streamId;
}
void StreamSetMaxCentripetalAccelerationRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetMaxCentripetalAccelerationRequest::getPvt() const {
    return pvt;
}
void StreamSetMaxCentripetalAccelerationRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

double StreamSetMaxCentripetalAccelerationRequest::getMaxCentripetalAcceleration() const {
    return maxCentripetalAcceleration;
}
void StreamSetMaxCentripetalAccelerationRequest::setMaxCentripetalAcceleration(double p_maxCentripetalAcceleration) {
    maxCentripetalAcceleration = p_maxCentripetalAcceleration;
}

Units StreamSetMaxCentripetalAccelerationRequest::getUnit() const {
    return unit;
}
void StreamSetMaxCentripetalAccelerationRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetMaxCentripetalAccelerationRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetMaxCentripetalAccelerationRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "maxCentripetalAcceleration: ";
    ss << this->maxCentripetalAcceleration;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetMaxCentripetalAccelerationRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetMaxCentripetalAccelerationRequest>();
}

std::string StreamSetMaxCentripetalAccelerationRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
