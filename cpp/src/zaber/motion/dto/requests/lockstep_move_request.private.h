// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/lockstep_move_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(LockstepMoveRequest, interfaceId, device, lockstepGroupId, waitUntilIdle, type, arg, unit, velocity, velocityUnit, acceleration, accelerationUnit)

} // namespace requests
} // namespace motion
} // namespace zaber
