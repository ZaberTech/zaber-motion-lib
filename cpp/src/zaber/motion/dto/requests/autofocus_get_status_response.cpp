// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/autofocus_get_status_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AutofocusGetStatusResponse::AutofocusGetStatusResponse() { }

AutofocusGetStatusResponse::AutofocusGetStatusResponse(
    microscopy::AutofocusStatus p_status
) :
    status(std::move(p_status))
{ }

bool AutofocusGetStatusResponse::operator==(const AutofocusGetStatusResponse& other) const {
    return std::tie(
    status
    ) == std::tie(
    other.status
    );
}

microscopy::AutofocusStatus const& AutofocusGetStatusResponse::getStatus() const {
    return status;
}
void AutofocusGetStatusResponse::setStatus(microscopy::AutofocusStatus p_status) {
    status = std::move(p_status);
}

std::string AutofocusGetStatusResponse::toString() const {
    std::stringstream ss;
    ss << "AutofocusGetStatusResponse { ";
    ss << "status: ";
    ss << this->status.toString();
    ss << " }";
    return ss.str();
}

void AutofocusGetStatusResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusGetStatusResponse>();
}

std::string AutofocusGetStatusResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
