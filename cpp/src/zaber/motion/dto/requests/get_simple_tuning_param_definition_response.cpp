// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/get_simple_tuning_param_definition_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GetSimpleTuningParamDefinitionResponse::GetSimpleTuningParamDefinitionResponse() { }

GetSimpleTuningParamDefinitionResponse::GetSimpleTuningParamDefinitionResponse(
    std::vector<ascii::SimpleTuningParamDefinition> p_params
) :
    params(std::move(p_params))
{ }

bool GetSimpleTuningParamDefinitionResponse::operator==(const GetSimpleTuningParamDefinitionResponse& other) const {
    return std::tie(
    params
    ) == std::tie(
    other.params
    );
}

std::vector<ascii::SimpleTuningParamDefinition> const& GetSimpleTuningParamDefinitionResponse::getParams() const {
    return params;
}
void GetSimpleTuningParamDefinitionResponse::setParams(std::vector<ascii::SimpleTuningParamDefinition> p_params) {
    params = std::move(p_params);
}

std::string GetSimpleTuningParamDefinitionResponse::toString() const {
    std::stringstream ss;
    ss << "GetSimpleTuningParamDefinitionResponse { ";
    ss << "params: ";
    ss << "[ ";
    for (size_t i = 0; i < this->params.size(); i++) {
        ss << this->params[i].toString();
        if (i < this->params.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void GetSimpleTuningParamDefinitionResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetSimpleTuningParamDefinitionResponse>();
}

std::string GetSimpleTuningParamDefinitionResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
