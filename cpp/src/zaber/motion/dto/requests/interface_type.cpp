// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/interface_type.h"

namespace zaber {
namespace motion {
namespace requests {

std::string InterfaceType_toString(InterfaceType value) {
    switch (value) {
        case InterfaceType::SERIAL_PORT: return "SERIAL_PORT";
        case InterfaceType::TCP: return "TCP";
        case InterfaceType::CUSTOM: return "CUSTOM";
        case InterfaceType::IOT: return "IOT";
        case InterfaceType::NETWORK_SHARE: return "NETWORK_SHARE";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber
