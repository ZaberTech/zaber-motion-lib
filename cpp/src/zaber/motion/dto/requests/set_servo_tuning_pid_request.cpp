// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_servo_tuning_pid_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetServoTuningPIDRequest::SetServoTuningPIDRequest() { }

SetServoTuningPIDRequest::SetServoTuningPIDRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_paramset,
    double p_p,
    double p_i,
    double p_d,
    double p_fc
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    paramset(std::move(p_paramset)),
    p(p_p),
    i(p_i),
    d(p_d),
    fc(p_fc)
{ }

bool SetServoTuningPIDRequest::operator==(const SetServoTuningPIDRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    paramset,
    p,
    i,
    d,
    fc
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.paramset,
    other.p,
    other.i,
    other.d,
    other.fc
    );
}

int SetServoTuningPIDRequest::getInterfaceId() const {
    return interfaceId;
}
void SetServoTuningPIDRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetServoTuningPIDRequest::getDevice() const {
    return device;
}
void SetServoTuningPIDRequest::setDevice(int p_device) {
    device = p_device;
}

int SetServoTuningPIDRequest::getAxis() const {
    return axis;
}
void SetServoTuningPIDRequest::setAxis(int p_axis) {
    axis = p_axis;
}

ascii::ServoTuningParamset const& SetServoTuningPIDRequest::getParamset() const {
    return paramset;
}
void SetServoTuningPIDRequest::setParamset(ascii::ServoTuningParamset p_paramset) {
    paramset = std::move(p_paramset);
}

double SetServoTuningPIDRequest::getP() const {
    return p;
}
void SetServoTuningPIDRequest::setP(double p_p) {
    p = p_p;
}

double SetServoTuningPIDRequest::getI() const {
    return i;
}
void SetServoTuningPIDRequest::setI(double p_i) {
    i = p_i;
}

double SetServoTuningPIDRequest::getD() const {
    return d;
}
void SetServoTuningPIDRequest::setD(double p_d) {
    d = p_d;
}

double SetServoTuningPIDRequest::getFc() const {
    return fc;
}
void SetServoTuningPIDRequest::setFc(double p_fc) {
    fc = p_fc;
}

std::string SetServoTuningPIDRequest::toString() const {
    std::stringstream ss;
    ss << "SetServoTuningPIDRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "paramset: ";
    ss << ServoTuningParamset_toString(this->paramset);
    ss << ", ";
    ss << "p: ";
    ss << this->p;
    ss << ", ";
    ss << "i: ";
    ss << this->i;
    ss << ", ";
    ss << "d: ";
    ss << this->d;
    ss << ", ";
    ss << "fc: ";
    ss << this->fc;
    ss << " }";
    return ss.str();
}

void SetServoTuningPIDRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetServoTuningPIDRequest>();
}

std::string SetServoTuningPIDRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
