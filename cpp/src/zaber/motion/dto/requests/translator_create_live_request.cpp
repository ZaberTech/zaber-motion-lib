// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_create_live_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorCreateLiveRequest::TranslatorCreateLiveRequest() { }

TranslatorCreateLiveRequest::TranslatorCreateLiveRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    std::optional<gcode::TranslatorConfig> p_config
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    config(std::move(p_config))
{ }

TranslatorCreateLiveRequest::TranslatorCreateLiveRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId)
{ }

bool TranslatorCreateLiveRequest::operator==(const TranslatorCreateLiveRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    config
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.config
    );
}

int TranslatorCreateLiveRequest::getInterfaceId() const {
    return interfaceId;
}
void TranslatorCreateLiveRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TranslatorCreateLiveRequest::getDevice() const {
    return device;
}
void TranslatorCreateLiveRequest::setDevice(int p_device) {
    device = p_device;
}

int TranslatorCreateLiveRequest::getStreamId() const {
    return streamId;
}
void TranslatorCreateLiveRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

std::optional<gcode::TranslatorConfig> const& TranslatorCreateLiveRequest::getConfig() const {
    return config;
}
void TranslatorCreateLiveRequest::setConfig(std::optional<gcode::TranslatorConfig> p_config) {
    config = std::move(p_config);
}

std::string TranslatorCreateLiveRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorCreateLiveRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "config: ";
    if (this->config.has_value()) {
        ss << this->config.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void TranslatorCreateLiveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorCreateLiveRequest>();
}

std::string TranslatorCreateLiveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
