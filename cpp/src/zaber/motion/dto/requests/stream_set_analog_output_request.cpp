// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_analog_output_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAnalogOutputRequest::StreamSetAnalogOutputRequest() { }

StreamSetAnalogOutputRequest::StreamSetAnalogOutputRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    double p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    value(p_value)
{ }

bool StreamSetAnalogOutputRequest::operator==(const StreamSetAnalogOutputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.value
    );
}

int StreamSetAnalogOutputRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAnalogOutputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAnalogOutputRequest::getDevice() const {
    return device;
}
void StreamSetAnalogOutputRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAnalogOutputRequest::getStreamId() const {
    return streamId;
}
void StreamSetAnalogOutputRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAnalogOutputRequest::getPvt() const {
    return pvt;
}
void StreamSetAnalogOutputRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetAnalogOutputRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamSetAnalogOutputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double StreamSetAnalogOutputRequest::getValue() const {
    return value;
}
void StreamSetAnalogOutputRequest::setValue(double p_value) {
    value = p_value;
}

std::string StreamSetAnalogOutputRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAnalogOutputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void StreamSetAnalogOutputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAnalogOutputRequest>();
}

std::string StreamSetAnalogOutputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
