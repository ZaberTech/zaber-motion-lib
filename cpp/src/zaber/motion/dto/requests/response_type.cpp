// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/response_type.h"

#pragma push_macro("ERROR")
#undef ERROR

namespace zaber {
namespace motion {
namespace requests {

std::string ResponseType_toString(ResponseType value) {
    switch (value) {
        case ResponseType::OK: return "OK";
        case ResponseType::ERROR: return "ERROR";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber

#pragma pop_macro("ERROR")
