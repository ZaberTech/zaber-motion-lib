// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/microscope_config_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

MicroscopeConfigResponse::MicroscopeConfigResponse() { }

MicroscopeConfigResponse::MicroscopeConfigResponse(
    microscopy::MicroscopeConfig p_config
) :
    config(std::move(p_config))
{ }

bool MicroscopeConfigResponse::operator==(const MicroscopeConfigResponse& other) const {
    return std::tie(
    config
    ) == std::tie(
    other.config
    );
}

microscopy::MicroscopeConfig const& MicroscopeConfigResponse::getConfig() const {
    return config;
}
void MicroscopeConfigResponse::setConfig(microscopy::MicroscopeConfig p_config) {
    config = std::move(p_config);
}

std::string MicroscopeConfigResponse::toString() const {
    std::stringstream ss;
    ss << "MicroscopeConfigResponse { ";
    ss << "config: ";
    ss << this->config.toString();
    ss << " }";
    return ss.str();
}

void MicroscopeConfigResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeConfigResponse>();
}

std::string MicroscopeConfigResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
