// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_get_axis_offset_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorGetAxisOffsetRequest::TranslatorGetAxisOffsetRequest() { }

TranslatorGetAxisOffsetRequest::TranslatorGetAxisOffsetRequest(
    int p_translatorId,
    std::string p_coordinateSystem,
    std::string p_axis,
    Units p_unit
) :
    translatorId(p_translatorId),
    coordinateSystem(std::move(p_coordinateSystem)),
    axis(std::move(p_axis)),
    unit(p_unit)
{ }

bool TranslatorGetAxisOffsetRequest::operator==(const TranslatorGetAxisOffsetRequest& other) const {
    return std::tie(
    translatorId,
    coordinateSystem,
    axis,
    unit
    ) == std::tie(
    other.translatorId,
    other.coordinateSystem,
    other.axis,
    other.unit
    );
}

int TranslatorGetAxisOffsetRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorGetAxisOffsetRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string const& TranslatorGetAxisOffsetRequest::getCoordinateSystem() const {
    return coordinateSystem;
}
void TranslatorGetAxisOffsetRequest::setCoordinateSystem(std::string p_coordinateSystem) {
    coordinateSystem = std::move(p_coordinateSystem);
}

std::string const& TranslatorGetAxisOffsetRequest::getAxis() const {
    return axis;
}
void TranslatorGetAxisOffsetRequest::setAxis(std::string p_axis) {
    axis = std::move(p_axis);
}

Units TranslatorGetAxisOffsetRequest::getUnit() const {
    return unit;
}
void TranslatorGetAxisOffsetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TranslatorGetAxisOffsetRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorGetAxisOffsetRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "coordinateSystem: ";
    ss << this->coordinateSystem;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TranslatorGetAxisOffsetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorGetAxisOffsetRequest>();
}

std::string TranslatorGetAxisOffsetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
