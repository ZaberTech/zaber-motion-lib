// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_generic_with_units_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryGenericWithUnitsRequest::BinaryGenericWithUnitsRequest() { }

BinaryGenericWithUnitsRequest::BinaryGenericWithUnitsRequest(
    int p_interfaceId,
    int p_device,
    binary::CommandCode p_command,
    double p_data,
    Units p_fromUnit,
    Units p_toUnit,
    double p_timeout
) :
    interfaceId(p_interfaceId),
    device(p_device),
    command(std::move(p_command)),
    data(p_data),
    fromUnit(p_fromUnit),
    toUnit(p_toUnit),
    timeout(p_timeout)
{ }

bool BinaryGenericWithUnitsRequest::operator==(const BinaryGenericWithUnitsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    command,
    data,
    fromUnit,
    toUnit,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.command,
    other.data,
    other.fromUnit,
    other.toUnit,
    other.timeout
    );
}

int BinaryGenericWithUnitsRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryGenericWithUnitsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryGenericWithUnitsRequest::getDevice() const {
    return device;
}
void BinaryGenericWithUnitsRequest::setDevice(int p_device) {
    device = p_device;
}

binary::CommandCode const& BinaryGenericWithUnitsRequest::getCommand() const {
    return command;
}
void BinaryGenericWithUnitsRequest::setCommand(binary::CommandCode p_command) {
    command = std::move(p_command);
}

double BinaryGenericWithUnitsRequest::getData() const {
    return data;
}
void BinaryGenericWithUnitsRequest::setData(double p_data) {
    data = p_data;
}

Units BinaryGenericWithUnitsRequest::getFromUnit() const {
    return fromUnit;
}
void BinaryGenericWithUnitsRequest::setFromUnit(Units p_fromUnit) {
    fromUnit = p_fromUnit;
}

Units BinaryGenericWithUnitsRequest::getToUnit() const {
    return toUnit;
}
void BinaryGenericWithUnitsRequest::setToUnit(Units p_toUnit) {
    toUnit = p_toUnit;
}

double BinaryGenericWithUnitsRequest::getTimeout() const {
    return timeout;
}
void BinaryGenericWithUnitsRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

std::string BinaryGenericWithUnitsRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryGenericWithUnitsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "command: ";
    ss << CommandCode_toString(this->command);
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << ", ";
    ss << "fromUnit: ";
    ss << getUnitLongName(this->fromUnit);
    ss << ", ";
    ss << "toUnit: ";
    ss << getUnitLongName(this->toUnit);
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void BinaryGenericWithUnitsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryGenericWithUnitsRequest>();
}

std::string BinaryGenericWithUnitsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
