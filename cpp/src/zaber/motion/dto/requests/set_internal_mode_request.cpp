// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_internal_mode_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetInternalModeRequest::SetInternalModeRequest() { }

SetInternalModeRequest::SetInternalModeRequest(
    bool p_mode
) :
    mode(p_mode)
{ }

bool SetInternalModeRequest::operator==(const SetInternalModeRequest& other) const {
    return std::tie(
    mode
    ) == std::tie(
    other.mode
    );
}

bool SetInternalModeRequest::getMode() const {
    return mode;
}
void SetInternalModeRequest::setMode(bool p_mode) {
    mode = p_mode;
}

std::string SetInternalModeRequest::toString() const {
    std::stringstream ss;
    ss << "SetInternalModeRequest { ";
    ss << "mode: ";
    ss << this->mode;
    ss << " }";
    return ss.str();
}

void SetInternalModeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetInternalModeRequest>();
}

std::string SetInternalModeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
