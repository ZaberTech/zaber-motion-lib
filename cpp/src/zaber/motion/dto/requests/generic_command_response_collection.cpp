// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/generic_command_response_collection.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GenericCommandResponseCollection::GenericCommandResponseCollection() { }

GenericCommandResponseCollection::GenericCommandResponseCollection(
    std::vector<ascii::Response> p_responses
) :
    responses(std::move(p_responses))
{ }

bool GenericCommandResponseCollection::operator==(const GenericCommandResponseCollection& other) const {
    return std::tie(
    responses
    ) == std::tie(
    other.responses
    );
}

std::vector<ascii::Response> const& GenericCommandResponseCollection::getResponses() const {
    return responses;
}
void GenericCommandResponseCollection::setResponses(std::vector<ascii::Response> p_responses) {
    responses = std::move(p_responses);
}

std::string GenericCommandResponseCollection::toString() const {
    std::stringstream ss;
    ss << "GenericCommandResponseCollection { ";
    ss << "responses: ";
    ss << "[ ";
    for (size_t i = 0; i < this->responses.size(); i++) {
        ss << this->responses[i].toString();
        if (i < this->responses.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void GenericCommandResponseCollection::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GenericCommandResponseCollection>();
}

std::string GenericCommandResponseCollection::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
