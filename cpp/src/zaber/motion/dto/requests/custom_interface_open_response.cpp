// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/custom_interface_open_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CustomInterfaceOpenResponse::CustomInterfaceOpenResponse() { }

CustomInterfaceOpenResponse::CustomInterfaceOpenResponse(
    int p_transportId
) :
    transportId(p_transportId)
{ }

bool CustomInterfaceOpenResponse::operator==(const CustomInterfaceOpenResponse& other) const {
    return std::tie(
    transportId
    ) == std::tie(
    other.transportId
    );
}

int CustomInterfaceOpenResponse::getTransportId() const {
    return transportId;
}
void CustomInterfaceOpenResponse::setTransportId(int p_transportId) {
    transportId = p_transportId;
}

std::string CustomInterfaceOpenResponse::toString() const {
    std::stringstream ss;
    ss << "CustomInterfaceOpenResponse { ";
    ss << "transportId: ";
    ss << this->transportId;
    ss << " }";
    return ss.str();
}

void CustomInterfaceOpenResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CustomInterfaceOpenResponse>();
}

std::string CustomInterfaceOpenResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
