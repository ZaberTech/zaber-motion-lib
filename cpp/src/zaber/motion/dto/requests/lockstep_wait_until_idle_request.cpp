// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_wait_until_idle_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepWaitUntilIdleRequest::LockstepWaitUntilIdleRequest() { }

LockstepWaitUntilIdleRequest::LockstepWaitUntilIdleRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    bool p_throwErrorOnFault
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    throwErrorOnFault(p_throwErrorOnFault)
{ }

bool LockstepWaitUntilIdleRequest::operator==(const LockstepWaitUntilIdleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    throwErrorOnFault
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.throwErrorOnFault
    );
}

int LockstepWaitUntilIdleRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepWaitUntilIdleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepWaitUntilIdleRequest::getDevice() const {
    return device;
}
void LockstepWaitUntilIdleRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepWaitUntilIdleRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepWaitUntilIdleRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

bool LockstepWaitUntilIdleRequest::getThrowErrorOnFault() const {
    return throwErrorOnFault;
}
void LockstepWaitUntilIdleRequest::setThrowErrorOnFault(bool p_throwErrorOnFault) {
    throwErrorOnFault = p_throwErrorOnFault;
}

std::string LockstepWaitUntilIdleRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepWaitUntilIdleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "throwErrorOnFault: ";
    ss << this->throwErrorOnFault;
    ss << " }";
    return ss.str();
}

void LockstepWaitUntilIdleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepWaitUntilIdleRequest>();
}

std::string LockstepWaitUntilIdleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
