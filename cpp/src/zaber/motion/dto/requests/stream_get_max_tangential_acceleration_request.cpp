// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_get_max_tangential_acceleration_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGetMaxTangentialAccelerationRequest::StreamGetMaxTangentialAccelerationRequest() { }

StreamGetMaxTangentialAccelerationRequest::StreamGetMaxTangentialAccelerationRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    unit(p_unit)
{ }

bool StreamGetMaxTangentialAccelerationRequest::operator==(const StreamGetMaxTangentialAccelerationRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.unit
    );
}

int StreamGetMaxTangentialAccelerationRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamGetMaxTangentialAccelerationRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamGetMaxTangentialAccelerationRequest::getDevice() const {
    return device;
}
void StreamGetMaxTangentialAccelerationRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamGetMaxTangentialAccelerationRequest::getStreamId() const {
    return streamId;
}
void StreamGetMaxTangentialAccelerationRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamGetMaxTangentialAccelerationRequest::getPvt() const {
    return pvt;
}
void StreamGetMaxTangentialAccelerationRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

Units StreamGetMaxTangentialAccelerationRequest::getUnit() const {
    return unit;
}
void StreamGetMaxTangentialAccelerationRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamGetMaxTangentialAccelerationRequest::toString() const {
    std::stringstream ss;
    ss << "StreamGetMaxTangentialAccelerationRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamGetMaxTangentialAccelerationRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGetMaxTangentialAccelerationRequest>();
}

std::string StreamGetMaxTangentialAccelerationRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
