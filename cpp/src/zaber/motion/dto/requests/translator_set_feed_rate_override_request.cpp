// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_set_feed_rate_override_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorSetFeedRateOverrideRequest::TranslatorSetFeedRateOverrideRequest() { }

TranslatorSetFeedRateOverrideRequest::TranslatorSetFeedRateOverrideRequest(
    int p_translatorId,
    double p_coefficient
) :
    translatorId(p_translatorId),
    coefficient(p_coefficient)
{ }

bool TranslatorSetFeedRateOverrideRequest::operator==(const TranslatorSetFeedRateOverrideRequest& other) const {
    return std::tie(
    translatorId,
    coefficient
    ) == std::tie(
    other.translatorId,
    other.coefficient
    );
}

int TranslatorSetFeedRateOverrideRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorSetFeedRateOverrideRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

double TranslatorSetFeedRateOverrideRequest::getCoefficient() const {
    return coefficient;
}
void TranslatorSetFeedRateOverrideRequest::setCoefficient(double p_coefficient) {
    coefficient = p_coefficient;
}

std::string TranslatorSetFeedRateOverrideRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorSetFeedRateOverrideRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "coefficient: ";
    ss << this->coefficient;
    ss << " }";
    return ss.str();
}

void TranslatorSetFeedRateOverrideRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorSetFeedRateOverrideRequest>();
}

std::string TranslatorSetFeedRateOverrideRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
