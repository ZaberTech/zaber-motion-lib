// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_buffer_get_content_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamBufferGetContentResponse::StreamBufferGetContentResponse() { }

StreamBufferGetContentResponse::StreamBufferGetContentResponse(
    std::vector<std::string> p_bufferLines
) :
    bufferLines(std::move(p_bufferLines))
{ }

bool StreamBufferGetContentResponse::operator==(const StreamBufferGetContentResponse& other) const {
    return std::tie(
    bufferLines
    ) == std::tie(
    other.bufferLines
    );
}

std::vector<std::string> const& StreamBufferGetContentResponse::getBufferLines() const {
    return bufferLines;
}
void StreamBufferGetContentResponse::setBufferLines(std::vector<std::string> p_bufferLines) {
    bufferLines = std::move(p_bufferLines);
}

std::string StreamBufferGetContentResponse::toString() const {
    std::stringstream ss;
    ss << "StreamBufferGetContentResponse { ";
    ss << "bufferLines: ";
    ss << "[ ";
    for (size_t i = 0; i < this->bufferLines.size(); i++) {
        ss << this->bufferLines[i];
        if (i < this->bufferLines.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamBufferGetContentResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamBufferGetContentResponse>();
}

std::string StreamBufferGetContentResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
