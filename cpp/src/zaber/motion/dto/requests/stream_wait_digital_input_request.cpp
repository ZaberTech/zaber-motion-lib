// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_wait_digital_input_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamWaitDigitalInputRequest::StreamWaitDigitalInputRequest() { }

StreamWaitDigitalInputRequest::StreamWaitDigitalInputRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    bool p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    value(p_value)
{ }

bool StreamWaitDigitalInputRequest::operator==(const StreamWaitDigitalInputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.value
    );
}

int StreamWaitDigitalInputRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamWaitDigitalInputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamWaitDigitalInputRequest::getDevice() const {
    return device;
}
void StreamWaitDigitalInputRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamWaitDigitalInputRequest::getStreamId() const {
    return streamId;
}
void StreamWaitDigitalInputRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamWaitDigitalInputRequest::getPvt() const {
    return pvt;
}
void StreamWaitDigitalInputRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamWaitDigitalInputRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamWaitDigitalInputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

bool StreamWaitDigitalInputRequest::getValue() const {
    return value;
}
void StreamWaitDigitalInputRequest::setValue(bool p_value) {
    value = p_value;
}

std::string StreamWaitDigitalInputRequest::toString() const {
    std::stringstream ss;
    ss << "StreamWaitDigitalInputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void StreamWaitDigitalInputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamWaitDigitalInputRequest>();
}

std::string StreamWaitDigitalInputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
