// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_home_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceHomeRequest::DeviceHomeRequest() { }

DeviceHomeRequest::DeviceHomeRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool DeviceHomeRequest::operator==(const DeviceHomeRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.waitUntilIdle
    );
}

int DeviceHomeRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceHomeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceHomeRequest::getDevice() const {
    return device;
}
void DeviceHomeRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceHomeRequest::getAxis() const {
    return axis;
}
void DeviceHomeRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceHomeRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void DeviceHomeRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string DeviceHomeRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceHomeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void DeviceHomeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceHomeRequest>();
}

std::string DeviceHomeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
