// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_generic_command_batch_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGenericCommandBatchRequest::StreamGenericCommandBatchRequest() { }

StreamGenericCommandBatchRequest::StreamGenericCommandBatchRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<std::string> p_batch
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    batch(std::move(p_batch))
{ }

bool StreamGenericCommandBatchRequest::operator==(const StreamGenericCommandBatchRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    batch
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.batch
    );
}

int StreamGenericCommandBatchRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamGenericCommandBatchRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamGenericCommandBatchRequest::getDevice() const {
    return device;
}
void StreamGenericCommandBatchRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamGenericCommandBatchRequest::getStreamId() const {
    return streamId;
}
void StreamGenericCommandBatchRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamGenericCommandBatchRequest::getPvt() const {
    return pvt;
}
void StreamGenericCommandBatchRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<std::string> const& StreamGenericCommandBatchRequest::getBatch() const {
    return batch;
}
void StreamGenericCommandBatchRequest::setBatch(std::vector<std::string> p_batch) {
    batch = std::move(p_batch);
}

std::string StreamGenericCommandBatchRequest::toString() const {
    std::stringstream ss;
    ss << "StreamGenericCommandBatchRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "batch: ";
    ss << "[ ";
    for (size_t i = 0; i < this->batch.size(); i++) {
        ss << this->batch[i];
        if (i < this->batch.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamGenericCommandBatchRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGenericCommandBatchRequest>();
}

std::string StreamGenericCommandBatchRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
