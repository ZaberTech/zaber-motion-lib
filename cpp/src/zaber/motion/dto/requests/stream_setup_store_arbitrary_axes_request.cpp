// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_setup_store_arbitrary_axes_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetupStoreArbitraryAxesRequest::StreamSetupStoreArbitraryAxesRequest() { }

StreamSetupStoreArbitraryAxesRequest::StreamSetupStoreArbitraryAxesRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_streamBuffer,
    int p_pvtBuffer,
    int p_axesCount
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    streamBuffer(p_streamBuffer),
    pvtBuffer(p_pvtBuffer),
    axesCount(p_axesCount)
{ }

bool StreamSetupStoreArbitraryAxesRequest::operator==(const StreamSetupStoreArbitraryAxesRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    streamBuffer,
    pvtBuffer,
    axesCount
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.streamBuffer,
    other.pvtBuffer,
    other.axesCount
    );
}

int StreamSetupStoreArbitraryAxesRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetupStoreArbitraryAxesRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetupStoreArbitraryAxesRequest::getDevice() const {
    return device;
}
void StreamSetupStoreArbitraryAxesRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetupStoreArbitraryAxesRequest::getStreamId() const {
    return streamId;
}
void StreamSetupStoreArbitraryAxesRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetupStoreArbitraryAxesRequest::getPvt() const {
    return pvt;
}
void StreamSetupStoreArbitraryAxesRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetupStoreArbitraryAxesRequest::getStreamBuffer() const {
    return streamBuffer;
}
void StreamSetupStoreArbitraryAxesRequest::setStreamBuffer(int p_streamBuffer) {
    streamBuffer = p_streamBuffer;
}

int StreamSetupStoreArbitraryAxesRequest::getPvtBuffer() const {
    return pvtBuffer;
}
void StreamSetupStoreArbitraryAxesRequest::setPvtBuffer(int p_pvtBuffer) {
    pvtBuffer = p_pvtBuffer;
}

int StreamSetupStoreArbitraryAxesRequest::getAxesCount() const {
    return axesCount;
}
void StreamSetupStoreArbitraryAxesRequest::setAxesCount(int p_axesCount) {
    axesCount = p_axesCount;
}

std::string StreamSetupStoreArbitraryAxesRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetupStoreArbitraryAxesRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "streamBuffer: ";
    ss << this->streamBuffer;
    ss << ", ";
    ss << "pvtBuffer: ";
    ss << this->pvtBuffer;
    ss << ", ";
    ss << "axesCount: ";
    ss << this->axesCount;
    ss << " }";
    return ss.str();
}

void StreamSetupStoreArbitraryAxesRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetupStoreArbitraryAxesRequest>();
}

std::string StreamSetupStoreArbitraryAxesRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
