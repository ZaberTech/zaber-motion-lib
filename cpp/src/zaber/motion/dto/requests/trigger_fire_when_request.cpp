// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_fire_when_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerFireWhenRequest::TriggerFireWhenRequest() { }

TriggerFireWhenRequest::TriggerFireWhenRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    std::string p_condition
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    condition(std::move(p_condition))
{ }

bool TriggerFireWhenRequest::operator==(const TriggerFireWhenRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    condition
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.condition
    );
}

int TriggerFireWhenRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerFireWhenRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerFireWhenRequest::getDevice() const {
    return device;
}
void TriggerFireWhenRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerFireWhenRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerFireWhenRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

std::string const& TriggerFireWhenRequest::getCondition() const {
    return condition;
}
void TriggerFireWhenRequest::setCondition(std::string p_condition) {
    condition = std::move(p_condition);
}

std::string TriggerFireWhenRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerFireWhenRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "condition: ";
    ss << this->condition;
    ss << " }";
    return ss.str();
}

void TriggerFireWhenRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerFireWhenRequest>();
}

std::string TriggerFireWhenRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
