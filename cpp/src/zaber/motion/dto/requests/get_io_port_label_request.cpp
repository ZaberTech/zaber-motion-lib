// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/get_io_port_label_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GetIoPortLabelRequest::GetIoPortLabelRequest() { }

GetIoPortLabelRequest::GetIoPortLabelRequest(
    int p_interfaceId,
    int p_device,
    ascii::IoPortType p_portType,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    portType(std::move(p_portType)),
    channelNumber(p_channelNumber)
{ }

bool GetIoPortLabelRequest::operator==(const GetIoPortLabelRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    portType,
    channelNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.portType,
    other.channelNumber
    );
}

int GetIoPortLabelRequest::getInterfaceId() const {
    return interfaceId;
}
void GetIoPortLabelRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int GetIoPortLabelRequest::getDevice() const {
    return device;
}
void GetIoPortLabelRequest::setDevice(int p_device) {
    device = p_device;
}

ascii::IoPortType const& GetIoPortLabelRequest::getPortType() const {
    return portType;
}
void GetIoPortLabelRequest::setPortType(ascii::IoPortType p_portType) {
    portType = std::move(p_portType);
}

int GetIoPortLabelRequest::getChannelNumber() const {
    return channelNumber;
}
void GetIoPortLabelRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string GetIoPortLabelRequest::toString() const {
    std::stringstream ss;
    ss << "GetIoPortLabelRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "portType: ";
    ss << IoPortType_toString(this->portType);
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << " }";
    return ss.str();
}

void GetIoPortLabelRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetIoPortLabelRequest>();
}

std::string GetIoPortLabelRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
