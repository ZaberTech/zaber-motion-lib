// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/int_response.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(IntResponse, value)

} // namespace requests
} // namespace motion
} // namespace zaber
