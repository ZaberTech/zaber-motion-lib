// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/microscope_init_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

MicroscopeInitRequest::MicroscopeInitRequest() { }

MicroscopeInitRequest::MicroscopeInitRequest(
    int p_interfaceId,
    microscopy::MicroscopeConfig p_config,
    bool p_force
) :
    interfaceId(p_interfaceId),
    config(std::move(p_config)),
    force(p_force)
{ }

bool MicroscopeInitRequest::operator==(const MicroscopeInitRequest& other) const {
    return std::tie(
    interfaceId,
    config,
    force
    ) == std::tie(
    other.interfaceId,
    other.config,
    other.force
    );
}

int MicroscopeInitRequest::getInterfaceId() const {
    return interfaceId;
}
void MicroscopeInitRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

microscopy::MicroscopeConfig const& MicroscopeInitRequest::getConfig() const {
    return config;
}
void MicroscopeInitRequest::setConfig(microscopy::MicroscopeConfig p_config) {
    config = std::move(p_config);
}

bool MicroscopeInitRequest::getForce() const {
    return force;
}
void MicroscopeInitRequest::setForce(bool p_force) {
    force = p_force;
}

std::string MicroscopeInitRequest::toString() const {
    std::stringstream ss;
    ss << "MicroscopeInitRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "config: ";
    ss << this->config.toString();
    ss << ", ";
    ss << "force: ";
    ss << this->force;
    ss << " }";
    return ss.str();
}

void MicroscopeInitRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeInitRequest>();
}

std::string MicroscopeInitRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
