// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/gateway_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GatewayResponse::GatewayResponse() { }

GatewayResponse::GatewayResponse(
    ResponseType p_response,
    Errors p_errorType,
    std::string p_errorMessage
) :
    response(std::move(p_response)),
    errorType(std::move(p_errorType)),
    errorMessage(std::move(p_errorMessage))
{ }

bool GatewayResponse::operator==(const GatewayResponse& other) const {
    return std::tie(
    response,
    errorType,
    errorMessage
    ) == std::tie(
    other.response,
    other.errorType,
    other.errorMessage
    );
}

ResponseType const& GatewayResponse::getResponse() const {
    return response;
}
void GatewayResponse::setResponse(ResponseType p_response) {
    response = std::move(p_response);
}

Errors const& GatewayResponse::getErrorType() const {
    return errorType;
}
void GatewayResponse::setErrorType(Errors p_errorType) {
    errorType = std::move(p_errorType);
}

std::string const& GatewayResponse::getErrorMessage() const {
    return errorMessage;
}
void GatewayResponse::setErrorMessage(std::string p_errorMessage) {
    errorMessage = std::move(p_errorMessage);
}

std::string GatewayResponse::toString() const {
    std::stringstream ss;
    ss << "GatewayResponse { ";
    ss << "response: ";
    ss << ResponseType_toString(this->response);
    ss << ", ";
    ss << "errorType: ";
    ss << Errors_toString(this->errorType);
    ss << ", ";
    ss << "errorMessage: ";
    ss << this->errorMessage;
    ss << " }";
    return ss.str();
}

void GatewayResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GatewayResponse>();
}

std::string GatewayResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
