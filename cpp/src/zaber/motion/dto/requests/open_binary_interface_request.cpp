// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/open_binary_interface_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OpenBinaryInterfaceRequest::OpenBinaryInterfaceRequest() { }

OpenBinaryInterfaceRequest::OpenBinaryInterfaceRequest(
    InterfaceType p_interfaceType,
    std::string p_portName,
    int p_baudRate,
    std::string p_hostName,
    int p_port,
    bool p_useMessageIds
) :
    interfaceType(std::move(p_interfaceType)),
    portName(std::move(p_portName)),
    baudRate(p_baudRate),
    hostName(std::move(p_hostName)),
    port(p_port),
    useMessageIds(p_useMessageIds)
{ }

bool OpenBinaryInterfaceRequest::operator==(const OpenBinaryInterfaceRequest& other) const {
    return std::tie(
    interfaceType,
    portName,
    baudRate,
    hostName,
    port,
    useMessageIds
    ) == std::tie(
    other.interfaceType,
    other.portName,
    other.baudRate,
    other.hostName,
    other.port,
    other.useMessageIds
    );
}

InterfaceType const& OpenBinaryInterfaceRequest::getInterfaceType() const {
    return interfaceType;
}
void OpenBinaryInterfaceRequest::setInterfaceType(InterfaceType p_interfaceType) {
    interfaceType = std::move(p_interfaceType);
}

std::string const& OpenBinaryInterfaceRequest::getPortName() const {
    return portName;
}
void OpenBinaryInterfaceRequest::setPortName(std::string p_portName) {
    portName = std::move(p_portName);
}

int OpenBinaryInterfaceRequest::getBaudRate() const {
    return baudRate;
}
void OpenBinaryInterfaceRequest::setBaudRate(int p_baudRate) {
    baudRate = p_baudRate;
}

std::string const& OpenBinaryInterfaceRequest::getHostName() const {
    return hostName;
}
void OpenBinaryInterfaceRequest::setHostName(std::string p_hostName) {
    hostName = std::move(p_hostName);
}

int OpenBinaryInterfaceRequest::getPort() const {
    return port;
}
void OpenBinaryInterfaceRequest::setPort(int p_port) {
    port = p_port;
}

bool OpenBinaryInterfaceRequest::getUseMessageIds() const {
    return useMessageIds;
}
void OpenBinaryInterfaceRequest::setUseMessageIds(bool p_useMessageIds) {
    useMessageIds = p_useMessageIds;
}

std::string OpenBinaryInterfaceRequest::toString() const {
    std::stringstream ss;
    ss << "OpenBinaryInterfaceRequest { ";
    ss << "interfaceType: ";
    ss << InterfaceType_toString(this->interfaceType);
    ss << ", ";
    ss << "portName: ";
    ss << this->portName;
    ss << ", ";
    ss << "baudRate: ";
    ss << this->baudRate;
    ss << ", ";
    ss << "hostName: ";
    ss << this->hostName;
    ss << ", ";
    ss << "port: ";
    ss << this->port;
    ss << ", ";
    ss << "useMessageIds: ";
    ss << this->useMessageIds;
    ss << " }";
    return ss.str();
}

void OpenBinaryInterfaceRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OpenBinaryInterfaceRequest>();
}

std::string OpenBinaryInterfaceRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
