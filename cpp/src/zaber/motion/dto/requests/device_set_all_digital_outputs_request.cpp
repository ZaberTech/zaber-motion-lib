// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_all_digital_outputs_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAllDigitalOutputsRequest::DeviceSetAllDigitalOutputsRequest() { }

DeviceSetAllDigitalOutputsRequest::DeviceSetAllDigitalOutputsRequest(
    int p_interfaceId,
    int p_device,
    std::vector<ascii::DigitalOutputAction> p_values
) :
    interfaceId(p_interfaceId),
    device(p_device),
    values(std::move(p_values))
{ }

bool DeviceSetAllDigitalOutputsRequest::operator==(const DeviceSetAllDigitalOutputsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    values
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.values
    );
}

int DeviceSetAllDigitalOutputsRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAllDigitalOutputsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAllDigitalOutputsRequest::getDevice() const {
    return device;
}
void DeviceSetAllDigitalOutputsRequest::setDevice(int p_device) {
    device = p_device;
}

std::vector<ascii::DigitalOutputAction> const& DeviceSetAllDigitalOutputsRequest::getValues() const {
    return values;
}
void DeviceSetAllDigitalOutputsRequest::setValues(std::vector<ascii::DigitalOutputAction> p_values) {
    values = std::move(p_values);
}

std::string DeviceSetAllDigitalOutputsRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAllDigitalOutputsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << DigitalOutputAction_toString(this->values[i]);
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceSetAllDigitalOutputsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAllDigitalOutputsRequest>();
}

std::string DeviceSetAllDigitalOutputsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
