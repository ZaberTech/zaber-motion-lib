// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_buffer_erase_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamBufferEraseRequest::StreamBufferEraseRequest() { }

StreamBufferEraseRequest::StreamBufferEraseRequest(
    int p_interfaceId,
    int p_device,
    int p_bufferId,
    bool p_pvt
) :
    interfaceId(p_interfaceId),
    device(p_device),
    bufferId(p_bufferId),
    pvt(p_pvt)
{ }

bool StreamBufferEraseRequest::operator==(const StreamBufferEraseRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    bufferId,
    pvt
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.bufferId,
    other.pvt
    );
}

int StreamBufferEraseRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamBufferEraseRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamBufferEraseRequest::getDevice() const {
    return device;
}
void StreamBufferEraseRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamBufferEraseRequest::getBufferId() const {
    return bufferId;
}
void StreamBufferEraseRequest::setBufferId(int p_bufferId) {
    bufferId = p_bufferId;
}

bool StreamBufferEraseRequest::getPvt() const {
    return pvt;
}
void StreamBufferEraseRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::string StreamBufferEraseRequest::toString() const {
    std::stringstream ss;
    ss << "StreamBufferEraseRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "bufferId: ";
    ss << this->bufferId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << " }";
    return ss.str();
}

void StreamBufferEraseRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamBufferEraseRequest>();
}

std::string StreamBufferEraseRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
