// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/translator_create_live_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/gcode/translator_config.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TranslatorCreateLiveRequest, interfaceId, device, streamId, config)

} // namespace requests
} // namespace motion
} // namespace zaber
