// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_call_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamCallRequest::StreamCallRequest() { }

StreamCallRequest::StreamCallRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_streamBuffer,
    int p_pvtBuffer
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    streamBuffer(p_streamBuffer),
    pvtBuffer(p_pvtBuffer)
{ }

bool StreamCallRequest::operator==(const StreamCallRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    streamBuffer,
    pvtBuffer
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.streamBuffer,
    other.pvtBuffer
    );
}

int StreamCallRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamCallRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamCallRequest::getDevice() const {
    return device;
}
void StreamCallRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamCallRequest::getStreamId() const {
    return streamId;
}
void StreamCallRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamCallRequest::getPvt() const {
    return pvt;
}
void StreamCallRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamCallRequest::getStreamBuffer() const {
    return streamBuffer;
}
void StreamCallRequest::setStreamBuffer(int p_streamBuffer) {
    streamBuffer = p_streamBuffer;
}

int StreamCallRequest::getPvtBuffer() const {
    return pvtBuffer;
}
void StreamCallRequest::setPvtBuffer(int p_pvtBuffer) {
    pvtBuffer = p_pvtBuffer;
}

std::string StreamCallRequest::toString() const {
    std::stringstream ss;
    ss << "StreamCallRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "streamBuffer: ";
    ss << this->streamBuffer;
    ss << ", ";
    ss << "pvtBuffer: ";
    ss << this->pvtBuffer;
    ss << " }";
    return ss.str();
}

void StreamCallRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamCallRequest>();
}

std::string StreamCallRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
