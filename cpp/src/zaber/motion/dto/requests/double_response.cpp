// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/double_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DoubleResponse::DoubleResponse() { }

DoubleResponse::DoubleResponse(
    double p_value
) :
    value(p_value)
{ }

bool DoubleResponse::operator==(const DoubleResponse& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

double DoubleResponse::getValue() const {
    return value;
}
void DoubleResponse::setValue(double p_value) {
    value = p_value;
}

std::string DoubleResponse::toString() const {
    std::stringstream ss;
    ss << "DoubleResponse { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void DoubleResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DoubleResponse>();
}

std::string DoubleResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
