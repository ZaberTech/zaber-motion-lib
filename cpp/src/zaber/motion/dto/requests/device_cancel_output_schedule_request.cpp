// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_cancel_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceCancelOutputScheduleRequest::DeviceCancelOutputScheduleRequest() { }

DeviceCancelOutputScheduleRequest::DeviceCancelOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    bool p_analog,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    analog(p_analog),
    channelNumber(p_channelNumber)
{ }

bool DeviceCancelOutputScheduleRequest::operator==(const DeviceCancelOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    analog,
    channelNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.analog,
    other.channelNumber
    );
}

int DeviceCancelOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceCancelOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceCancelOutputScheduleRequest::getDevice() const {
    return device;
}
void DeviceCancelOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

bool DeviceCancelOutputScheduleRequest::getAnalog() const {
    return analog;
}
void DeviceCancelOutputScheduleRequest::setAnalog(bool p_analog) {
    analog = p_analog;
}

int DeviceCancelOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceCancelOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string DeviceCancelOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceCancelOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "analog: ";
    ss << this->analog;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << " }";
    return ss.str();
}

void DeviceCancelOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceCancelOutputScheduleRequest>();
}

std::string DeviceCancelOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
