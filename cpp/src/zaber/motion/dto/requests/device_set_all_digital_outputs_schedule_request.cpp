// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_all_digital_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAllDigitalOutputsScheduleRequest::DeviceSetAllDigitalOutputsScheduleRequest() { }

DeviceSetAllDigitalOutputsScheduleRequest::DeviceSetAllDigitalOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    std::vector<ascii::DigitalOutputAction> p_values,
    std::vector<ascii::DigitalOutputAction> p_futureValues,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    values(std::move(p_values)),
    futureValues(std::move(p_futureValues)),
    delay(p_delay),
    unit(p_unit)
{ }

bool DeviceSetAllDigitalOutputsScheduleRequest::operator==(const DeviceSetAllDigitalOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    values,
    futureValues,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.values,
    other.futureValues,
    other.delay,
    other.unit
    );
}

int DeviceSetAllDigitalOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAllDigitalOutputsScheduleRequest::getDevice() const {
    return device;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

std::vector<ascii::DigitalOutputAction> const& DeviceSetAllDigitalOutputsScheduleRequest::getValues() const {
    return values;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setValues(std::vector<ascii::DigitalOutputAction> p_values) {
    values = std::move(p_values);
}

std::vector<ascii::DigitalOutputAction> const& DeviceSetAllDigitalOutputsScheduleRequest::getFutureValues() const {
    return futureValues;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setFutureValues(std::vector<ascii::DigitalOutputAction> p_futureValues) {
    futureValues = std::move(p_futureValues);
}

double DeviceSetAllDigitalOutputsScheduleRequest::getDelay() const {
    return delay;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units DeviceSetAllDigitalOutputsScheduleRequest::getUnit() const {
    return unit;
}
void DeviceSetAllDigitalOutputsScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetAllDigitalOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAllDigitalOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << DigitalOutputAction_toString(this->values[i]);
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "futureValues: ";
    ss << "[ ";
    for (size_t i = 0; i < this->futureValues.size(); i++) {
        ss << DigitalOutputAction_toString(this->futureValues[i]);
        if (i < this->futureValues.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetAllDigitalOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAllDigitalOutputsScheduleRequest>();
}

std::string DeviceSetAllDigitalOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
