// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_wait_until_idle_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamWaitUntilIdleRequest::StreamWaitUntilIdleRequest() { }

StreamWaitUntilIdleRequest::StreamWaitUntilIdleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    bool p_throwErrorOnFault
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    throwErrorOnFault(p_throwErrorOnFault)
{ }

bool StreamWaitUntilIdleRequest::operator==(const StreamWaitUntilIdleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    throwErrorOnFault
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.throwErrorOnFault
    );
}

int StreamWaitUntilIdleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamWaitUntilIdleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamWaitUntilIdleRequest::getDevice() const {
    return device;
}
void StreamWaitUntilIdleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamWaitUntilIdleRequest::getStreamId() const {
    return streamId;
}
void StreamWaitUntilIdleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamWaitUntilIdleRequest::getPvt() const {
    return pvt;
}
void StreamWaitUntilIdleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

bool StreamWaitUntilIdleRequest::getThrowErrorOnFault() const {
    return throwErrorOnFault;
}
void StreamWaitUntilIdleRequest::setThrowErrorOnFault(bool p_throwErrorOnFault) {
    throwErrorOnFault = p_throwErrorOnFault;
}

std::string StreamWaitUntilIdleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamWaitUntilIdleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "throwErrorOnFault: ";
    ss << this->throwErrorOnFault;
    ss << " }";
    return ss.str();
}

void StreamWaitUntilIdleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamWaitUntilIdleRequest>();
}

std::string StreamWaitUntilIdleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
