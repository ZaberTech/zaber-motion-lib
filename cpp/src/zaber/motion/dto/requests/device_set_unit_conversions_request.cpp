// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_unit_conversions_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetUnitConversionsRequest::DeviceSetUnitConversionsRequest() { }

DeviceSetUnitConversionsRequest::DeviceSetUnitConversionsRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key,
    std::vector<ascii::ConversionFactor> p_conversions
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key)),
    conversions(std::move(p_conversions))
{ }

bool DeviceSetUnitConversionsRequest::operator==(const DeviceSetUnitConversionsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key,
    conversions
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key,
    other.conversions
    );
}

int DeviceSetUnitConversionsRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetUnitConversionsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetUnitConversionsRequest::getDevice() const {
    return device;
}
void DeviceSetUnitConversionsRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetUnitConversionsRequest::getAxis() const {
    return axis;
}
void DeviceSetUnitConversionsRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetUnitConversionsRequest::getKey() const {
    return key;
}
void DeviceSetUnitConversionsRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

std::vector<ascii::ConversionFactor> const& DeviceSetUnitConversionsRequest::getConversions() const {
    return conversions;
}
void DeviceSetUnitConversionsRequest::setConversions(std::vector<ascii::ConversionFactor> p_conversions) {
    conversions = std::move(p_conversions);
}

std::string DeviceSetUnitConversionsRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetUnitConversionsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << ", ";
    ss << "conversions: ";
    ss << "[ ";
    for (size_t i = 0; i < this->conversions.size(); i++) {
        ss << this->conversions[i].toString();
        if (i < this->conversions.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceSetUnitConversionsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetUnitConversionsRequest>();
}

std::string DeviceSetUnitConversionsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
