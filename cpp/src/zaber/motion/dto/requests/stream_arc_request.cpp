// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_arc_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamArcRequest::StreamArcRequest() { }

StreamArcRequest::StreamArcRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    StreamSegmentType p_type,
    RotationDirection p_rotationDirection,
    Measurement p_centerX,
    Measurement p_centerY,
    Measurement p_endX,
    Measurement p_endY,
    std::vector<int> p_targetAxesIndices,
    std::vector<Measurement> p_endpoint
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    type(std::move(p_type)),
    rotationDirection(std::move(p_rotationDirection)),
    centerX(std::move(p_centerX)),
    centerY(std::move(p_centerY)),
    endX(std::move(p_endX)),
    endY(std::move(p_endY)),
    targetAxesIndices(std::move(p_targetAxesIndices)),
    endpoint(std::move(p_endpoint))
{ }

bool StreamArcRequest::operator==(const StreamArcRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    type,
    rotationDirection,
    centerX,
    centerY,
    endX,
    endY,
    targetAxesIndices,
    endpoint
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.type,
    other.rotationDirection,
    other.centerX,
    other.centerY,
    other.endX,
    other.endY,
    other.targetAxesIndices,
    other.endpoint
    );
}

int StreamArcRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamArcRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamArcRequest::getDevice() const {
    return device;
}
void StreamArcRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamArcRequest::getStreamId() const {
    return streamId;
}
void StreamArcRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamArcRequest::getPvt() const {
    return pvt;
}
void StreamArcRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

StreamSegmentType const& StreamArcRequest::getType() const {
    return type;
}
void StreamArcRequest::setType(StreamSegmentType p_type) {
    type = std::move(p_type);
}

RotationDirection const& StreamArcRequest::getRotationDirection() const {
    return rotationDirection;
}
void StreamArcRequest::setRotationDirection(RotationDirection p_rotationDirection) {
    rotationDirection = std::move(p_rotationDirection);
}

Measurement const& StreamArcRequest::getCenterX() const {
    return centerX;
}
void StreamArcRequest::setCenterX(Measurement p_centerX) {
    centerX = std::move(p_centerX);
}

Measurement const& StreamArcRequest::getCenterY() const {
    return centerY;
}
void StreamArcRequest::setCenterY(Measurement p_centerY) {
    centerY = std::move(p_centerY);
}

Measurement const& StreamArcRequest::getEndX() const {
    return endX;
}
void StreamArcRequest::setEndX(Measurement p_endX) {
    endX = std::move(p_endX);
}

Measurement const& StreamArcRequest::getEndY() const {
    return endY;
}
void StreamArcRequest::setEndY(Measurement p_endY) {
    endY = std::move(p_endY);
}

std::vector<int> const& StreamArcRequest::getTargetAxesIndices() const {
    return targetAxesIndices;
}
void StreamArcRequest::setTargetAxesIndices(std::vector<int> p_targetAxesIndices) {
    targetAxesIndices = std::move(p_targetAxesIndices);
}

std::vector<Measurement> const& StreamArcRequest::getEndpoint() const {
    return endpoint;
}
void StreamArcRequest::setEndpoint(std::vector<Measurement> p_endpoint) {
    endpoint = std::move(p_endpoint);
}

std::string StreamArcRequest::toString() const {
    std::stringstream ss;
    ss << "StreamArcRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "type: ";
    ss << StreamSegmentType_toString(this->type);
    ss << ", ";
    ss << "rotationDirection: ";
    ss << RotationDirection_toString(this->rotationDirection);
    ss << ", ";
    ss << "centerX: ";
    ss << this->centerX.toString();
    ss << ", ";
    ss << "centerY: ";
    ss << this->centerY.toString();
    ss << ", ";
    ss << "endX: ";
    ss << this->endX.toString();
    ss << ", ";
    ss << "endY: ";
    ss << this->endY.toString();
    ss << ", ";
    ss << "targetAxesIndices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->targetAxesIndices.size(); i++) {
        ss << this->targetAxesIndices[i];
        if (i < this->targetAxesIndices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "endpoint: ";
    ss << "[ ";
    for (size_t i = 0; i < this->endpoint.size(); i++) {
        ss << this->endpoint[i].toString();
        if (i < this->endpoint.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamArcRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamArcRequest>();
}

std::string StreamArcRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
