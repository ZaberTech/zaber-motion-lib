// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_detect_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceDetectRequest::BinaryDeviceDetectRequest() { }

BinaryDeviceDetectRequest::BinaryDeviceDetectRequest(
    int p_interfaceId,
    bool p_identifyDevices
) :
    interfaceId(p_interfaceId),
    identifyDevices(p_identifyDevices)
{ }

bool BinaryDeviceDetectRequest::operator==(const BinaryDeviceDetectRequest& other) const {
    return std::tie(
    interfaceId,
    identifyDevices
    ) == std::tie(
    other.interfaceId,
    other.identifyDevices
    );
}

int BinaryDeviceDetectRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceDetectRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

bool BinaryDeviceDetectRequest::getIdentifyDevices() const {
    return identifyDevices;
}
void BinaryDeviceDetectRequest::setIdentifyDevices(bool p_identifyDevices) {
    identifyDevices = p_identifyDevices;
}

std::string BinaryDeviceDetectRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceDetectRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "identifyDevices: ";
    ss << this->identifyDevices;
    ss << " }";
    return ss.str();
}

void BinaryDeviceDetectRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceDetectRequest>();
}

std::string BinaryDeviceDetectRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
