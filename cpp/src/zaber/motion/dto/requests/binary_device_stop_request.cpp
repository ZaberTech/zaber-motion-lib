// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_stop_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceStopRequest::BinaryDeviceStopRequest() { }

BinaryDeviceStopRequest::BinaryDeviceStopRequest(
    int p_interfaceId,
    int p_device,
    double p_timeout,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    timeout(p_timeout),
    unit(p_unit)
{ }

bool BinaryDeviceStopRequest::operator==(const BinaryDeviceStopRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    timeout,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.timeout,
    other.unit
    );
}

int BinaryDeviceStopRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceStopRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryDeviceStopRequest::getDevice() const {
    return device;
}
void BinaryDeviceStopRequest::setDevice(int p_device) {
    device = p_device;
}

double BinaryDeviceStopRequest::getTimeout() const {
    return timeout;
}
void BinaryDeviceStopRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

Units BinaryDeviceStopRequest::getUnit() const {
    return unit;
}
void BinaryDeviceStopRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string BinaryDeviceStopRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceStopRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void BinaryDeviceStopRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceStopRequest>();
}

std::string BinaryDeviceStopRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
