// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_process_controller_source.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetProcessControllerSource::SetProcessControllerSource() { }

SetProcessControllerSource::SetProcessControllerSource(
    int p_interfaceId,
    int p_device,
    int p_axis,
    product::ProcessControllerSource p_source
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    source(std::move(p_source))
{ }

bool SetProcessControllerSource::operator==(const SetProcessControllerSource& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    source
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.source
    );
}

int SetProcessControllerSource::getInterfaceId() const {
    return interfaceId;
}
void SetProcessControllerSource::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetProcessControllerSource::getDevice() const {
    return device;
}
void SetProcessControllerSource::setDevice(int p_device) {
    device = p_device;
}

int SetProcessControllerSource::getAxis() const {
    return axis;
}
void SetProcessControllerSource::setAxis(int p_axis) {
    axis = p_axis;
}

product::ProcessControllerSource const& SetProcessControllerSource::getSource() const {
    return source;
}
void SetProcessControllerSource::setSource(product::ProcessControllerSource p_source) {
    source = std::move(p_source);
}

std::string SetProcessControllerSource::toString() const {
    std::stringstream ss;
    ss << "SetProcessControllerSource { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "source: ";
    ss << this->source.toString();
    ss << " }";
    return ss.str();
}

void SetProcessControllerSource::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetProcessControllerSource>();
}

std::string SetProcessControllerSource::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
