// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_states.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerStates::TriggerStates() { }

TriggerStates::TriggerStates(
    std::vector<ascii::TriggerState> p_states
) :
    states(std::move(p_states))
{ }

bool TriggerStates::operator==(const TriggerStates& other) const {
    return std::tie(
    states
    ) == std::tie(
    other.states
    );
}

std::vector<ascii::TriggerState> const& TriggerStates::getStates() const {
    return states;
}
void TriggerStates::setStates(std::vector<ascii::TriggerState> p_states) {
    states = std::move(p_states);
}

std::string TriggerStates::toString() const {
    std::stringstream ss;
    ss << "TriggerStates { ";
    ss << "states: ";
    ss << "[ ";
    for (size_t i = 0; i < this->states.size(); i++) {
        ss << this->states[i].toString();
        if (i < this->states.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TriggerStates::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerStates>();
}

std::string TriggerStates::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
