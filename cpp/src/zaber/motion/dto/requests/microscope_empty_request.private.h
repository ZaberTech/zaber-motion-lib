// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/microscope_empty_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/microscopy/microscope_config.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(MicroscopeEmptyRequest, interfaceId, config)

} // namespace requests
} // namespace motion
} // namespace zaber
