// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_analog_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAnalogOutputScheduleRequest::DeviceSetAnalogOutputScheduleRequest() { }

DeviceSetAnalogOutputScheduleRequest::DeviceSetAnalogOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    double p_value,
    double p_futureValue,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    value(p_value),
    futureValue(p_futureValue),
    delay(p_delay),
    unit(p_unit)
{ }

bool DeviceSetAnalogOutputScheduleRequest::operator==(const DeviceSetAnalogOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    value,
    futureValue,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.value,
    other.futureValue,
    other.delay,
    other.unit
    );
}

int DeviceSetAnalogOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAnalogOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAnalogOutputScheduleRequest::getDevice() const {
    return device;
}
void DeviceSetAnalogOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetAnalogOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceSetAnalogOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double DeviceSetAnalogOutputScheduleRequest::getValue() const {
    return value;
}
void DeviceSetAnalogOutputScheduleRequest::setValue(double p_value) {
    value = p_value;
}

double DeviceSetAnalogOutputScheduleRequest::getFutureValue() const {
    return futureValue;
}
void DeviceSetAnalogOutputScheduleRequest::setFutureValue(double p_futureValue) {
    futureValue = p_futureValue;
}

double DeviceSetAnalogOutputScheduleRequest::getDelay() const {
    return delay;
}
void DeviceSetAnalogOutputScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units DeviceSetAnalogOutputScheduleRequest::getUnit() const {
    return unit;
}
void DeviceSetAnalogOutputScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetAnalogOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAnalogOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "futureValue: ";
    ss << this->futureValue;
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetAnalogOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAnalogOutputScheduleRequest>();
}

std::string DeviceSetAnalogOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
