// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerEmptyRequest::TriggerEmptyRequest() { }

TriggerEmptyRequest::TriggerEmptyRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber)
{ }

bool TriggerEmptyRequest::operator==(const TriggerEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber
    );
}

int TriggerEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerEmptyRequest::getDevice() const {
    return device;
}
void TriggerEmptyRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerEmptyRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerEmptyRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

std::string TriggerEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << " }";
    return ss.str();
}

void TriggerEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerEmptyRequest>();
}

std::string TriggerEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
