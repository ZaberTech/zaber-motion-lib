// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_digital_io_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetDigitalIORequest::DeviceGetDigitalIORequest() { }

DeviceGetDigitalIORequest::DeviceGetDigitalIORequest(
    int p_interfaceId,
    int p_device,
    std::string p_channelType,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelType(std::move(p_channelType)),
    channelNumber(p_channelNumber)
{ }

bool DeviceGetDigitalIORequest::operator==(const DeviceGetDigitalIORequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelType,
    channelNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelType,
    other.channelNumber
    );
}

int DeviceGetDigitalIORequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetDigitalIORequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetDigitalIORequest::getDevice() const {
    return device;
}
void DeviceGetDigitalIORequest::setDevice(int p_device) {
    device = p_device;
}

std::string const& DeviceGetDigitalIORequest::getChannelType() const {
    return channelType;
}
void DeviceGetDigitalIORequest::setChannelType(std::string p_channelType) {
    channelType = std::move(p_channelType);
}

int DeviceGetDigitalIORequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceGetDigitalIORequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string DeviceGetDigitalIORequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetDigitalIORequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelType: ";
    ss << this->channelType;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << " }";
    return ss.str();
}

void DeviceGetDigitalIORequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetDigitalIORequest>();
}

std::string DeviceGetDigitalIORequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
