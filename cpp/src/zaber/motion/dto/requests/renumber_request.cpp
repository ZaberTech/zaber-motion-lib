// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/renumber_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

RenumberRequest::RenumberRequest() { }

RenumberRequest::RenumberRequest(
    int p_interfaceId,
    int p_device,
    int p_address
) :
    interfaceId(p_interfaceId),
    device(p_device),
    address(p_address)
{ }

bool RenumberRequest::operator==(const RenumberRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    address
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.address
    );
}

int RenumberRequest::getInterfaceId() const {
    return interfaceId;
}
void RenumberRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int RenumberRequest::getDevice() const {
    return device;
}
void RenumberRequest::setDevice(int p_device) {
    device = p_device;
}

int RenumberRequest::getAddress() const {
    return address;
}
void RenumberRequest::setAddress(int p_address) {
    address = p_address;
}

std::string RenumberRequest::toString() const {
    std::stringstream ss;
    ss << "RenumberRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "address: ";
    ss << this->address;
    ss << " }";
    return ss.str();
}

void RenumberRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<RenumberRequest>();
}

std::string RenumberRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
