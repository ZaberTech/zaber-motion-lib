// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_wait_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamWaitRequest::StreamWaitRequest() { }

StreamWaitRequest::StreamWaitRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    double p_time,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    time(p_time),
    unit(p_unit)
{ }

bool StreamWaitRequest::operator==(const StreamWaitRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    time,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.time,
    other.unit
    );
}

int StreamWaitRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamWaitRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamWaitRequest::getDevice() const {
    return device;
}
void StreamWaitRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamWaitRequest::getStreamId() const {
    return streamId;
}
void StreamWaitRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamWaitRequest::getPvt() const {
    return pvt;
}
void StreamWaitRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

double StreamWaitRequest::getTime() const {
    return time;
}
void StreamWaitRequest::setTime(double p_time) {
    time = p_time;
}

Units StreamWaitRequest::getUnit() const {
    return unit;
}
void StreamWaitRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamWaitRequest::toString() const {
    std::stringstream ss;
    ss << "StreamWaitRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "time: ";
    ss << this->time;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamWaitRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamWaitRequest>();
}

std::string StreamWaitRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
