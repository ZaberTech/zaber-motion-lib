// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/disconnected_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DisconnectedEvent::DisconnectedEvent() { }

DisconnectedEvent::DisconnectedEvent(
    int p_interfaceId,
    Errors p_errorType,
    std::string p_errorMessage
) :
    interfaceId(p_interfaceId),
    errorType(std::move(p_errorType)),
    errorMessage(std::move(p_errorMessage))
{ }

bool DisconnectedEvent::operator==(const DisconnectedEvent& other) const {
    return std::tie(
    interfaceId,
    errorType,
    errorMessage
    ) == std::tie(
    other.interfaceId,
    other.errorType,
    other.errorMessage
    );
}

int DisconnectedEvent::getInterfaceId() const {
    return interfaceId;
}
void DisconnectedEvent::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

Errors const& DisconnectedEvent::getErrorType() const {
    return errorType;
}
void DisconnectedEvent::setErrorType(Errors p_errorType) {
    errorType = std::move(p_errorType);
}

std::string const& DisconnectedEvent::getErrorMessage() const {
    return errorMessage;
}
void DisconnectedEvent::setErrorMessage(std::string p_errorMessage) {
    errorMessage = std::move(p_errorMessage);
}

std::string DisconnectedEvent::toString() const {
    std::stringstream ss;
    ss << "DisconnectedEvent { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "errorType: ";
    ss << Errors_toString(this->errorType);
    ss << ", ";
    ss << "errorMessage: ";
    ss << this->errorMessage;
    ss << " }";
    return ss.str();
}

void DisconnectedEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DisconnectedEvent>();
}

std::string DisconnectedEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
