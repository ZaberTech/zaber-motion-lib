// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/objective_changer_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ObjectiveChangerRequest::ObjectiveChangerRequest() { }

ObjectiveChangerRequest::ObjectiveChangerRequest(
    int p_interfaceId,
    int p_turretAddress,
    int p_focusAddress,
    int p_focusAxis
) :
    interfaceId(p_interfaceId),
    turretAddress(p_turretAddress),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis)
{ }

bool ObjectiveChangerRequest::operator==(const ObjectiveChangerRequest& other) const {
    return std::tie(
    interfaceId,
    turretAddress,
    focusAddress,
    focusAxis
    ) == std::tie(
    other.interfaceId,
    other.turretAddress,
    other.focusAddress,
    other.focusAxis
    );
}

int ObjectiveChangerRequest::getInterfaceId() const {
    return interfaceId;
}
void ObjectiveChangerRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ObjectiveChangerRequest::getTurretAddress() const {
    return turretAddress;
}
void ObjectiveChangerRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

int ObjectiveChangerRequest::getFocusAddress() const {
    return focusAddress;
}
void ObjectiveChangerRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int ObjectiveChangerRequest::getFocusAxis() const {
    return focusAxis;
}
void ObjectiveChangerRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

std::string ObjectiveChangerRequest::toString() const {
    std::stringstream ss;
    ss << "ObjectiveChangerRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << " }";
    return ss.str();
}

void ObjectiveChangerRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ObjectiveChangerRequest>();
}

std::string ObjectiveChangerRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
