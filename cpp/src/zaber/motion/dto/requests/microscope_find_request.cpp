// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/microscope_find_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

MicroscopeFindRequest::MicroscopeFindRequest() { }

MicroscopeFindRequest::MicroscopeFindRequest(
    int p_interfaceId,
    std::optional<microscopy::ThirdPartyComponents> p_thirdParty
) :
    interfaceId(p_interfaceId),
    thirdParty(std::move(p_thirdParty))
{ }

MicroscopeFindRequest::MicroscopeFindRequest(
    int p_interfaceId
) :
    interfaceId(p_interfaceId)
{ }

bool MicroscopeFindRequest::operator==(const MicroscopeFindRequest& other) const {
    return std::tie(
    interfaceId,
    thirdParty
    ) == std::tie(
    other.interfaceId,
    other.thirdParty
    );
}

int MicroscopeFindRequest::getInterfaceId() const {
    return interfaceId;
}
void MicroscopeFindRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

std::optional<microscopy::ThirdPartyComponents> const& MicroscopeFindRequest::getThirdParty() const {
    return thirdParty;
}
void MicroscopeFindRequest::setThirdParty(std::optional<microscopy::ThirdPartyComponents> p_thirdParty) {
    thirdParty = std::move(p_thirdParty);
}

std::string MicroscopeFindRequest::toString() const {
    std::stringstream ss;
    ss << "MicroscopeFindRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "thirdParty: ";
    if (this->thirdParty.has_value()) {
        ss << this->thirdParty.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void MicroscopeFindRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeFindRequest>();
}

std::string MicroscopeFindRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
