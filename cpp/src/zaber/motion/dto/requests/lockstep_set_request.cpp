// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_set_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepSetRequest::LockstepSetRequest() { }

LockstepSetRequest::LockstepSetRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    double p_value,
    Units p_unit,
    int p_axisIndex
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    value(p_value),
    unit(p_unit),
    axisIndex(p_axisIndex)
{ }

bool LockstepSetRequest::operator==(const LockstepSetRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    value,
    unit,
    axisIndex
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.value,
    other.unit,
    other.axisIndex
    );
}

int LockstepSetRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepSetRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepSetRequest::getDevice() const {
    return device;
}
void LockstepSetRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepSetRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepSetRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

double LockstepSetRequest::getValue() const {
    return value;
}
void LockstepSetRequest::setValue(double p_value) {
    value = p_value;
}

Units LockstepSetRequest::getUnit() const {
    return unit;
}
void LockstepSetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

int LockstepSetRequest::getAxisIndex() const {
    return axisIndex;
}
void LockstepSetRequest::setAxisIndex(int p_axisIndex) {
    axisIndex = p_axisIndex;
}

std::string LockstepSetRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepSetRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "axisIndex: ";
    ss << this->axisIndex;
    ss << " }";
    return ss.str();
}

void LockstepSetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepSetRequest>();
}

std::string LockstepSetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
