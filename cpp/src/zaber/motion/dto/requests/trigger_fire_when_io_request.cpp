// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_fire_when_io_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerFireWhenIoRequest::TriggerFireWhenIoRequest() { }

TriggerFireWhenIoRequest::TriggerFireWhenIoRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    ascii::IoPortType p_portType,
    int p_channel,
    ascii::TriggerCondition p_triggerCondition,
    double p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    portType(std::move(p_portType)),
    channel(p_channel),
    triggerCondition(std::move(p_triggerCondition)),
    value(p_value)
{ }

bool TriggerFireWhenIoRequest::operator==(const TriggerFireWhenIoRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    portType,
    channel,
    triggerCondition,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.portType,
    other.channel,
    other.triggerCondition,
    other.value
    );
}

int TriggerFireWhenIoRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerFireWhenIoRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerFireWhenIoRequest::getDevice() const {
    return device;
}
void TriggerFireWhenIoRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerFireWhenIoRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerFireWhenIoRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

ascii::IoPortType const& TriggerFireWhenIoRequest::getPortType() const {
    return portType;
}
void TriggerFireWhenIoRequest::setPortType(ascii::IoPortType p_portType) {
    portType = std::move(p_portType);
}

int TriggerFireWhenIoRequest::getChannel() const {
    return channel;
}
void TriggerFireWhenIoRequest::setChannel(int p_channel) {
    channel = p_channel;
}

ascii::TriggerCondition const& TriggerFireWhenIoRequest::getTriggerCondition() const {
    return triggerCondition;
}
void TriggerFireWhenIoRequest::setTriggerCondition(ascii::TriggerCondition p_triggerCondition) {
    triggerCondition = std::move(p_triggerCondition);
}

double TriggerFireWhenIoRequest::getValue() const {
    return value;
}
void TriggerFireWhenIoRequest::setValue(double p_value) {
    value = p_value;
}

std::string TriggerFireWhenIoRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerFireWhenIoRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "portType: ";
    ss << IoPortType_toString(this->portType);
    ss << ", ";
    ss << "channel: ";
    ss << this->channel;
    ss << ", ";
    ss << "triggerCondition: ";
    ss << TriggerCondition_toString(this->triggerCondition);
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void TriggerFireWhenIoRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerFireWhenIoRequest>();
}

std::string TriggerFireWhenIoRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
