// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_data_identifier.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeDataIdentifier::OscilloscopeDataIdentifier() { }

OscilloscopeDataIdentifier::OscilloscopeDataIdentifier(
    int p_dataId
) :
    dataId(p_dataId)
{ }

bool OscilloscopeDataIdentifier::operator==(const OscilloscopeDataIdentifier& other) const {
    return std::tie(
    dataId
    ) == std::tie(
    other.dataId
    );
}

int OscilloscopeDataIdentifier::getDataId() const {
    return dataId;
}
void OscilloscopeDataIdentifier::setDataId(int p_dataId) {
    dataId = p_dataId;
}

std::string OscilloscopeDataIdentifier::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeDataIdentifier { ";
    ss << "dataId: ";
    ss << this->dataId;
    ss << " }";
    return ss.str();
}

void OscilloscopeDataIdentifier::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeDataIdentifier>();
}

std::string OscilloscopeDataIdentifier::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
