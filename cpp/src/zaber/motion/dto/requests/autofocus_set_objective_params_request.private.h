// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/autofocus_set_objective_params_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/named_parameter.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AutofocusSetObjectiveParamsRequest, providerId, interfaceId, focusAddress, focusAxis, turretAddress, objective, parameters)

} // namespace requests
} // namespace motion
} // namespace zaber
