// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_device_db_source_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetDeviceDbSourceRequest::SetDeviceDbSourceRequest() { }

SetDeviceDbSourceRequest::SetDeviceDbSourceRequest(
    DeviceDbSourceType p_sourceType,
    std::optional<std::string> p_urlOrFilePath
) :
    sourceType(std::move(p_sourceType)),
    urlOrFilePath(std::move(p_urlOrFilePath))
{ }

SetDeviceDbSourceRequest::SetDeviceDbSourceRequest(
    DeviceDbSourceType p_sourceType
) :
    sourceType(std::move(p_sourceType))
{ }

bool SetDeviceDbSourceRequest::operator==(const SetDeviceDbSourceRequest& other) const {
    return std::tie(
    sourceType,
    urlOrFilePath
    ) == std::tie(
    other.sourceType,
    other.urlOrFilePath
    );
}

DeviceDbSourceType const& SetDeviceDbSourceRequest::getSourceType() const {
    return sourceType;
}
void SetDeviceDbSourceRequest::setSourceType(DeviceDbSourceType p_sourceType) {
    sourceType = std::move(p_sourceType);
}

std::optional<std::string> const& SetDeviceDbSourceRequest::getUrlOrFilePath() const {
    return urlOrFilePath;
}
void SetDeviceDbSourceRequest::setUrlOrFilePath(std::optional<std::string> p_urlOrFilePath) {
    urlOrFilePath = std::move(p_urlOrFilePath);
}

std::string SetDeviceDbSourceRequest::toString() const {
    std::stringstream ss;
    ss << "SetDeviceDbSourceRequest { ";
    ss << "sourceType: ";
    ss << DeviceDbSourceType_toString(this->sourceType);
    ss << ", ";
    ss << "urlOrFilePath: ";
    if (this->urlOrFilePath.has_value()) {
        ss << this->urlOrFilePath.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void SetDeviceDbSourceRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetDeviceDbSourceRequest>();
}

std::string SetDeviceDbSourceRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
