// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_cancel_all_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceCancelAllOutputsScheduleRequest::DeviceCancelAllOutputsScheduleRequest() { }

DeviceCancelAllOutputsScheduleRequest::DeviceCancelAllOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    bool p_analog,
    std::vector<bool> p_channels
) :
    interfaceId(p_interfaceId),
    device(p_device),
    analog(p_analog),
    channels(std::move(p_channels))
{ }

bool DeviceCancelAllOutputsScheduleRequest::operator==(const DeviceCancelAllOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    analog,
    channels
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.analog,
    other.channels
    );
}

int DeviceCancelAllOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceCancelAllOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceCancelAllOutputsScheduleRequest::getDevice() const {
    return device;
}
void DeviceCancelAllOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

bool DeviceCancelAllOutputsScheduleRequest::getAnalog() const {
    return analog;
}
void DeviceCancelAllOutputsScheduleRequest::setAnalog(bool p_analog) {
    analog = p_analog;
}

std::vector<bool> const& DeviceCancelAllOutputsScheduleRequest::getChannels() const {
    return channels;
}
void DeviceCancelAllOutputsScheduleRequest::setChannels(std::vector<bool> p_channels) {
    channels = std::move(p_channels);
}

std::string DeviceCancelAllOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceCancelAllOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "analog: ";
    ss << this->analog;
    ss << ", ";
    ss << "channels: ";
    ss << "[ ";
    for (size_t i = 0; i < this->channels.size(); i++) {
        ss << this->channels[i];
        if (i < this->channels.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceCancelAllOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceCancelAllOutputsScheduleRequest>();
}

std::string DeviceCancelAllOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
