// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_set_all_digital_outputs_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamSetAllDigitalOutputsRequest, interfaceId, device, streamId, pvt, values)

} // namespace requests
} // namespace motion
} // namespace zaber
