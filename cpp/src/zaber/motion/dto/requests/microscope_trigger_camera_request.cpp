// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/microscope_trigger_camera_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

MicroscopeTriggerCameraRequest::MicroscopeTriggerCameraRequest() { }

MicroscopeTriggerCameraRequest::MicroscopeTriggerCameraRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    double p_delay,
    Units p_unit,
    bool p_wait
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    delay(p_delay),
    unit(p_unit),
    wait(p_wait)
{ }

bool MicroscopeTriggerCameraRequest::operator==(const MicroscopeTriggerCameraRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    delay,
    unit,
    wait
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.delay,
    other.unit,
    other.wait
    );
}

int MicroscopeTriggerCameraRequest::getInterfaceId() const {
    return interfaceId;
}
void MicroscopeTriggerCameraRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int MicroscopeTriggerCameraRequest::getDevice() const {
    return device;
}
void MicroscopeTriggerCameraRequest::setDevice(int p_device) {
    device = p_device;
}

int MicroscopeTriggerCameraRequest::getChannelNumber() const {
    return channelNumber;
}
void MicroscopeTriggerCameraRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double MicroscopeTriggerCameraRequest::getDelay() const {
    return delay;
}
void MicroscopeTriggerCameraRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units MicroscopeTriggerCameraRequest::getUnit() const {
    return unit;
}
void MicroscopeTriggerCameraRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

bool MicroscopeTriggerCameraRequest::getWait() const {
    return wait;
}
void MicroscopeTriggerCameraRequest::setWait(bool p_wait) {
    wait = p_wait;
}

std::string MicroscopeTriggerCameraRequest::toString() const {
    std::stringstream ss;
    ss << "MicroscopeTriggerCameraRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "wait: ";
    ss << this->wait;
    ss << " }";
    return ss.str();
}

void MicroscopeTriggerCameraRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MicroscopeTriggerCameraRequest>();
}

std::string MicroscopeTriggerCameraRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
