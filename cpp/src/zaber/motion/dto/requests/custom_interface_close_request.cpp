// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/custom_interface_close_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CustomInterfaceCloseRequest::CustomInterfaceCloseRequest() { }

CustomInterfaceCloseRequest::CustomInterfaceCloseRequest(
    int p_transportId,
    std::string p_errorMessage
) :
    transportId(p_transportId),
    errorMessage(std::move(p_errorMessage))
{ }

bool CustomInterfaceCloseRequest::operator==(const CustomInterfaceCloseRequest& other) const {
    return std::tie(
    transportId,
    errorMessage
    ) == std::tie(
    other.transportId,
    other.errorMessage
    );
}

int CustomInterfaceCloseRequest::getTransportId() const {
    return transportId;
}
void CustomInterfaceCloseRequest::setTransportId(int p_transportId) {
    transportId = p_transportId;
}

std::string const& CustomInterfaceCloseRequest::getErrorMessage() const {
    return errorMessage;
}
void CustomInterfaceCloseRequest::setErrorMessage(std::string p_errorMessage) {
    errorMessage = std::move(p_errorMessage);
}

std::string CustomInterfaceCloseRequest::toString() const {
    std::stringstream ss;
    ss << "CustomInterfaceCloseRequest { ";
    ss << "transportId: ";
    ss << this->transportId;
    ss << ", ";
    ss << "errorMessage: ";
    ss << this->errorMessage;
    ss << " }";
    return ss.str();
}

void CustomInterfaceCloseRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CustomInterfaceCloseRequest>();
}

std::string CustomInterfaceCloseRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
