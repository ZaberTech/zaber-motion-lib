// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_servo_tuning_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetServoTuningRequest::SetServoTuningRequest() { }

SetServoTuningRequest::SetServoTuningRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_paramset,
    std::vector<ascii::ServoTuningParam> p_tuningParams,
    bool p_setUnspecifiedToDefault
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    paramset(std::move(p_paramset)),
    tuningParams(std::move(p_tuningParams)),
    setUnspecifiedToDefault(p_setUnspecifiedToDefault)
{ }

bool SetServoTuningRequest::operator==(const SetServoTuningRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    paramset,
    tuningParams,
    setUnspecifiedToDefault
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.paramset,
    other.tuningParams,
    other.setUnspecifiedToDefault
    );
}

int SetServoTuningRequest::getInterfaceId() const {
    return interfaceId;
}
void SetServoTuningRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetServoTuningRequest::getDevice() const {
    return device;
}
void SetServoTuningRequest::setDevice(int p_device) {
    device = p_device;
}

int SetServoTuningRequest::getAxis() const {
    return axis;
}
void SetServoTuningRequest::setAxis(int p_axis) {
    axis = p_axis;
}

ascii::ServoTuningParamset const& SetServoTuningRequest::getParamset() const {
    return paramset;
}
void SetServoTuningRequest::setParamset(ascii::ServoTuningParamset p_paramset) {
    paramset = std::move(p_paramset);
}

std::vector<ascii::ServoTuningParam> const& SetServoTuningRequest::getTuningParams() const {
    return tuningParams;
}
void SetServoTuningRequest::setTuningParams(std::vector<ascii::ServoTuningParam> p_tuningParams) {
    tuningParams = std::move(p_tuningParams);
}

bool SetServoTuningRequest::getSetUnspecifiedToDefault() const {
    return setUnspecifiedToDefault;
}
void SetServoTuningRequest::setSetUnspecifiedToDefault(bool p_setUnspecifiedToDefault) {
    setUnspecifiedToDefault = p_setUnspecifiedToDefault;
}

std::string SetServoTuningRequest::toString() const {
    std::stringstream ss;
    ss << "SetServoTuningRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "paramset: ";
    ss << ServoTuningParamset_toString(this->paramset);
    ss << ", ";
    ss << "tuningParams: ";
    ss << "[ ";
    for (size_t i = 0; i < this->tuningParams.size(); i++) {
        ss << this->tuningParams[i].toString();
        if (i < this->tuningParams.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "setUnspecifiedToDefault: ";
    ss << this->setUnspecifiedToDefault;
    ss << " }";
    return ss.str();
}

void SetServoTuningRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetServoTuningRequest>();
}

std::string SetServoTuningRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
