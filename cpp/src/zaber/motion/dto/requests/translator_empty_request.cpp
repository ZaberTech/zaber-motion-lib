// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorEmptyRequest::TranslatorEmptyRequest() { }

TranslatorEmptyRequest::TranslatorEmptyRequest(
    int p_translatorId
) :
    translatorId(p_translatorId)
{ }

bool TranslatorEmptyRequest::operator==(const TranslatorEmptyRequest& other) const {
    return std::tie(
    translatorId
    ) == std::tie(
    other.translatorId
    );
}

int TranslatorEmptyRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorEmptyRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string TranslatorEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorEmptyRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << " }";
    return ss.str();
}

void TranslatorEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorEmptyRequest>();
}

std::string TranslatorEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
