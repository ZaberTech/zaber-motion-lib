// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_reply_only_event_wrapper.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryReplyOnlyEventWrapper::BinaryReplyOnlyEventWrapper() { }

BinaryReplyOnlyEventWrapper::BinaryReplyOnlyEventWrapper(
    int p_interfaceId,
    binary::ReplyOnlyEvent p_reply
) :
    interfaceId(p_interfaceId),
    reply(std::move(p_reply))
{ }

bool BinaryReplyOnlyEventWrapper::operator==(const BinaryReplyOnlyEventWrapper& other) const {
    return std::tie(
    interfaceId,
    reply
    ) == std::tie(
    other.interfaceId,
    other.reply
    );
}

int BinaryReplyOnlyEventWrapper::getInterfaceId() const {
    return interfaceId;
}
void BinaryReplyOnlyEventWrapper::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

binary::ReplyOnlyEvent const& BinaryReplyOnlyEventWrapper::getReply() const {
    return reply;
}
void BinaryReplyOnlyEventWrapper::setReply(binary::ReplyOnlyEvent p_reply) {
    reply = std::move(p_reply);
}

std::string BinaryReplyOnlyEventWrapper::toString() const {
    std::stringstream ss;
    ss << "BinaryReplyOnlyEventWrapper { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "reply: ";
    ss << this->reply.toString();
    ss << " }";
    return ss.str();
}

void BinaryReplyOnlyEventWrapper::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryReplyOnlyEventWrapper>();
}

std::string BinaryReplyOnlyEventWrapper::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
