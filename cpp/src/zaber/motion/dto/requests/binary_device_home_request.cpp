// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_home_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceHomeRequest::BinaryDeviceHomeRequest() { }

BinaryDeviceHomeRequest::BinaryDeviceHomeRequest(
    int p_interfaceId,
    int p_device,
    double p_timeout,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    timeout(p_timeout),
    unit(p_unit)
{ }

bool BinaryDeviceHomeRequest::operator==(const BinaryDeviceHomeRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    timeout,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.timeout,
    other.unit
    );
}

int BinaryDeviceHomeRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceHomeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryDeviceHomeRequest::getDevice() const {
    return device;
}
void BinaryDeviceHomeRequest::setDevice(int p_device) {
    device = p_device;
}

double BinaryDeviceHomeRequest::getTimeout() const {
    return timeout;
}
void BinaryDeviceHomeRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

Units BinaryDeviceHomeRequest::getUnit() const {
    return unit;
}
void BinaryDeviceHomeRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string BinaryDeviceHomeRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceHomeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void BinaryDeviceHomeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceHomeRequest>();
}

std::string BinaryDeviceHomeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
