// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_wait_until_idle_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceWaitUntilIdleRequest::DeviceWaitUntilIdleRequest() { }

DeviceWaitUntilIdleRequest::DeviceWaitUntilIdleRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_throwErrorOnFault
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    throwErrorOnFault(p_throwErrorOnFault)
{ }

bool DeviceWaitUntilIdleRequest::operator==(const DeviceWaitUntilIdleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    throwErrorOnFault
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.throwErrorOnFault
    );
}

int DeviceWaitUntilIdleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceWaitUntilIdleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceWaitUntilIdleRequest::getDevice() const {
    return device;
}
void DeviceWaitUntilIdleRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceWaitUntilIdleRequest::getAxis() const {
    return axis;
}
void DeviceWaitUntilIdleRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceWaitUntilIdleRequest::getThrowErrorOnFault() const {
    return throwErrorOnFault;
}
void DeviceWaitUntilIdleRequest::setThrowErrorOnFault(bool p_throwErrorOnFault) {
    throwErrorOnFault = p_throwErrorOnFault;
}

std::string DeviceWaitUntilIdleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceWaitUntilIdleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "throwErrorOnFault: ";
    ss << this->throwErrorOnFault;
    ss << " }";
    return ss.str();
}

void DeviceWaitUntilIdleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceWaitUntilIdleRequest>();
}

std::string DeviceWaitUntilIdleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
