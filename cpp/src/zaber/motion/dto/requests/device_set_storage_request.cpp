// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_storage_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetStorageRequest::DeviceSetStorageRequest() { }

DeviceSetStorageRequest::DeviceSetStorageRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key,
    std::string p_value,
    bool p_encode
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key)),
    value(std::move(p_value)),
    encode(p_encode)
{ }

bool DeviceSetStorageRequest::operator==(const DeviceSetStorageRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key,
    value,
    encode
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key,
    other.value,
    other.encode
    );
}

int DeviceSetStorageRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetStorageRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetStorageRequest::getDevice() const {
    return device;
}
void DeviceSetStorageRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetStorageRequest::getAxis() const {
    return axis;
}
void DeviceSetStorageRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetStorageRequest::getKey() const {
    return key;
}
void DeviceSetStorageRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

std::string const& DeviceSetStorageRequest::getValue() const {
    return value;
}
void DeviceSetStorageRequest::setValue(std::string p_value) {
    value = std::move(p_value);
}

bool DeviceSetStorageRequest::getEncode() const {
    return encode;
}
void DeviceSetStorageRequest::setEncode(bool p_encode) {
    encode = p_encode;
}

std::string DeviceSetStorageRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetStorageRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "encode: ";
    ss << this->encode;
    ss << " }";
    return ss.str();
}

void DeviceSetStorageRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetStorageRequest>();
}

std::string DeviceSetStorageRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
