// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/servo_tuning_paramset_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ServoTuningParamsetResponse::ServoTuningParamsetResponse() { }

ServoTuningParamsetResponse::ServoTuningParamsetResponse(
    ascii::ServoTuningParamset p_paramset
) :
    paramset(std::move(p_paramset))
{ }

bool ServoTuningParamsetResponse::operator==(const ServoTuningParamsetResponse& other) const {
    return std::tie(
    paramset
    ) == std::tie(
    other.paramset
    );
}

ascii::ServoTuningParamset const& ServoTuningParamsetResponse::getParamset() const {
    return paramset;
}
void ServoTuningParamsetResponse::setParamset(ascii::ServoTuningParamset p_paramset) {
    paramset = std::move(p_paramset);
}

std::string ServoTuningParamsetResponse::toString() const {
    std::stringstream ss;
    ss << "ServoTuningParamsetResponse { ";
    ss << "paramset: ";
    ss << ServoTuningParamset_toString(this->paramset);
    ss << " }";
    return ss.str();
}

void ServoTuningParamsetResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ServoTuningParamsetResponse>();
}

std::string ServoTuningParamsetResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
