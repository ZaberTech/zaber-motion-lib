// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_enable_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerEnableRequest::TriggerEnableRequest() { }

TriggerEnableRequest::TriggerEnableRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    int p_count
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    count(p_count)
{ }

bool TriggerEnableRequest::operator==(const TriggerEnableRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    count
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.count
    );
}

int TriggerEnableRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerEnableRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerEnableRequest::getDevice() const {
    return device;
}
void TriggerEnableRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerEnableRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerEnableRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

int TriggerEnableRequest::getCount() const {
    return count;
}
void TriggerEnableRequest::setCount(int p_count) {
    count = p_count;
}

std::string TriggerEnableRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerEnableRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "count: ";
    ss << this->count;
    ss << " }";
    return ss.str();
}

void TriggerEnableRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerEnableRequest>();
}

std::string TriggerEnableRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
