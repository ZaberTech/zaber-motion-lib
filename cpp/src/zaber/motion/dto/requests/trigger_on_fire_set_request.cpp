// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_on_fire_set_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerOnFireSetRequest::TriggerOnFireSetRequest() { }

TriggerOnFireSetRequest::TriggerOnFireSetRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    ascii::TriggerAction p_action,
    int p_axis,
    std::string p_setting,
    ascii::TriggerOperation p_operation,
    double p_value,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    action(std::move(p_action)),
    axis(p_axis),
    setting(std::move(p_setting)),
    operation(std::move(p_operation)),
    value(p_value),
    unit(p_unit)
{ }

bool TriggerOnFireSetRequest::operator==(const TriggerOnFireSetRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    action,
    axis,
    setting,
    operation,
    value,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.action,
    other.axis,
    other.setting,
    other.operation,
    other.value,
    other.unit
    );
}

int TriggerOnFireSetRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerOnFireSetRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerOnFireSetRequest::getDevice() const {
    return device;
}
void TriggerOnFireSetRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerOnFireSetRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerOnFireSetRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

ascii::TriggerAction const& TriggerOnFireSetRequest::getAction() const {
    return action;
}
void TriggerOnFireSetRequest::setAction(ascii::TriggerAction p_action) {
    action = std::move(p_action);
}

int TriggerOnFireSetRequest::getAxis() const {
    return axis;
}
void TriggerOnFireSetRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& TriggerOnFireSetRequest::getSetting() const {
    return setting;
}
void TriggerOnFireSetRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

ascii::TriggerOperation const& TriggerOnFireSetRequest::getOperation() const {
    return operation;
}
void TriggerOnFireSetRequest::setOperation(ascii::TriggerOperation p_operation) {
    operation = std::move(p_operation);
}

double TriggerOnFireSetRequest::getValue() const {
    return value;
}
void TriggerOnFireSetRequest::setValue(double p_value) {
    value = p_value;
}

Units TriggerOnFireSetRequest::getUnit() const {
    return unit;
}
void TriggerOnFireSetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TriggerOnFireSetRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerOnFireSetRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "action: ";
    ss << TriggerAction_toString(this->action);
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "operation: ";
    ss << TriggerOperation_toString(this->operation);
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TriggerOnFireSetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerOnFireSetRequest>();
}

std::string TriggerOnFireSetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
