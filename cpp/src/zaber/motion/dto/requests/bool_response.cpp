// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/bool_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BoolResponse::BoolResponse() { }

BoolResponse::BoolResponse(
    bool p_value
) :
    value(p_value)
{ }

bool BoolResponse::operator==(const BoolResponse& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

bool BoolResponse::getValue() const {
    return value;
}
void BoolResponse::setValue(bool p_value) {
    value = p_value;
}

std::string BoolResponse::toString() const {
    std::stringstream ss;
    ss << "BoolResponse { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void BoolResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BoolResponse>();
}

std::string BoolResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
