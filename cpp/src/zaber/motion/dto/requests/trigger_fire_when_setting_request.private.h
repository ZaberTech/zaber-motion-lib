// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/trigger_fire_when_setting_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TriggerFireWhenSettingRequest, interfaceId, device, triggerNumber, axis, setting, triggerCondition, value, unit)

} // namespace requests
} // namespace motion
} // namespace zaber
