// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_setup_store_composite_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/stream_axis_definition.private.h"
#include "zaber/motion/dto/ascii/pvt_axis_definition.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamSetupStoreCompositeRequest, interfaceId, device, streamId, pvt, streamBuffer, pvtBuffer, axes, pvtAxes)

} // namespace requests
} // namespace motion
} // namespace zaber
