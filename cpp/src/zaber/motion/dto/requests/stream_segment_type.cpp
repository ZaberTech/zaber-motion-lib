// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_segment_type.h"

namespace zaber {
namespace motion {
namespace requests {

std::string StreamSegmentType_toString(StreamSegmentType value) {
    switch (value) {
        case StreamSegmentType::ABS: return "ABS";
        case StreamSegmentType::REL: return "REL";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber
