// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_move_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepMoveRequest::LockstepMoveRequest() { }

LockstepMoveRequest::LockstepMoveRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    bool p_waitUntilIdle,
    AxisMoveType p_type,
    double p_arg,
    Units p_unit,
    double p_velocity,
    Units p_velocityUnit,
    double p_acceleration,
    Units p_accelerationUnit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    waitUntilIdle(p_waitUntilIdle),
    type(std::move(p_type)),
    arg(p_arg),
    unit(p_unit),
    velocity(p_velocity),
    velocityUnit(p_velocityUnit),
    acceleration(p_acceleration),
    accelerationUnit(p_accelerationUnit)
{ }

bool LockstepMoveRequest::operator==(const LockstepMoveRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    waitUntilIdle,
    type,
    arg,
    unit,
    velocity,
    velocityUnit,
    acceleration,
    accelerationUnit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.waitUntilIdle,
    other.type,
    other.arg,
    other.unit,
    other.velocity,
    other.velocityUnit,
    other.acceleration,
    other.accelerationUnit
    );
}

int LockstepMoveRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepMoveRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepMoveRequest::getDevice() const {
    return device;
}
void LockstepMoveRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepMoveRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepMoveRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

bool LockstepMoveRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void LockstepMoveRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

AxisMoveType const& LockstepMoveRequest::getType() const {
    return type;
}
void LockstepMoveRequest::setType(AxisMoveType p_type) {
    type = std::move(p_type);
}

double LockstepMoveRequest::getArg() const {
    return arg;
}
void LockstepMoveRequest::setArg(double p_arg) {
    arg = p_arg;
}

Units LockstepMoveRequest::getUnit() const {
    return unit;
}
void LockstepMoveRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

double LockstepMoveRequest::getVelocity() const {
    return velocity;
}
void LockstepMoveRequest::setVelocity(double p_velocity) {
    velocity = p_velocity;
}

Units LockstepMoveRequest::getVelocityUnit() const {
    return velocityUnit;
}
void LockstepMoveRequest::setVelocityUnit(Units p_velocityUnit) {
    velocityUnit = p_velocityUnit;
}

double LockstepMoveRequest::getAcceleration() const {
    return acceleration;
}
void LockstepMoveRequest::setAcceleration(double p_acceleration) {
    acceleration = p_acceleration;
}

Units LockstepMoveRequest::getAccelerationUnit() const {
    return accelerationUnit;
}
void LockstepMoveRequest::setAccelerationUnit(Units p_accelerationUnit) {
    accelerationUnit = p_accelerationUnit;
}

std::string LockstepMoveRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepMoveRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << ", ";
    ss << "type: ";
    ss << AxisMoveType_toString(this->type);
    ss << ", ";
    ss << "arg: ";
    ss << this->arg;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "velocity: ";
    ss << this->velocity;
    ss << ", ";
    ss << "velocityUnit: ";
    ss << getUnitLongName(this->velocityUnit);
    ss << ", ";
    ss << "acceleration: ";
    ss << this->acceleration;
    ss << ", ";
    ss << "accelerationUnit: ";
    ss << getUnitLongName(this->accelerationUnit);
    ss << " }";
    return ss.str();
}

void LockstepMoveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepMoveRequest>();
}

std::string LockstepMoveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
