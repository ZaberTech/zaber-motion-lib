// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_setup_store_arbitrary_axes_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamSetupStoreArbitraryAxesRequest, interfaceId, device, streamId, pvt, streamBuffer, pvtBuffer, axesCount)

} // namespace requests
} // namespace motion
} // namespace zaber
