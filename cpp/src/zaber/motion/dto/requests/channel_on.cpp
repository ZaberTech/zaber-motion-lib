// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/channel_on.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ChannelOn::ChannelOn() { }

ChannelOn::ChannelOn(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_on
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    on(p_on)
{ }

bool ChannelOn::operator==(const ChannelOn& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    on
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.on
    );
}

int ChannelOn::getInterfaceId() const {
    return interfaceId;
}
void ChannelOn::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ChannelOn::getDevice() const {
    return device;
}
void ChannelOn::setDevice(int p_device) {
    device = p_device;
}

int ChannelOn::getAxis() const {
    return axis;
}
void ChannelOn::setAxis(int p_axis) {
    axis = p_axis;
}

bool ChannelOn::getOn() const {
    return on;
}
void ChannelOn::setOn(bool p_on) {
    on = p_on;
}

std::string ChannelOn::toString() const {
    std::stringstream ss;
    ss << "ChannelOn { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "on: ";
    ss << this->on;
    ss << " }";
    return ss.str();
}

void ChannelOn::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ChannelOn>();
}

std::string ChannelOn::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
