// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetSettingRequest::DeviceSetSettingRequest() { }

DeviceSetSettingRequest::DeviceSetSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_setting,
    double p_value,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    setting(std::move(p_setting)),
    value(p_value),
    unit(p_unit)
{ }

bool DeviceSetSettingRequest::operator==(const DeviceSetSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    setting,
    value,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.setting,
    other.value,
    other.unit
    );
}

int DeviceSetSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetSettingRequest::getDevice() const {
    return device;
}
void DeviceSetSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetSettingRequest::getAxis() const {
    return axis;
}
void DeviceSetSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetSettingRequest::getSetting() const {
    return setting;
}
void DeviceSetSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

double DeviceSetSettingRequest::getValue() const {
    return value;
}
void DeviceSetSettingRequest::setValue(double p_value) {
    value = p_value;
}

Units DeviceSetSettingRequest::getUnit() const {
    return unit;
}
void DeviceSetSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetSettingRequest>();
}

std::string DeviceSetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
