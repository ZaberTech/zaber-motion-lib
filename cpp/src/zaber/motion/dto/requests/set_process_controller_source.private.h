// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/set_process_controller_source.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/product/process_controller_source.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetProcessControllerSource, interfaceId, device, axis, source)

} // namespace requests
} // namespace motion
} // namespace zaber
