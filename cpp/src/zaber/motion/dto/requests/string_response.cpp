// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/string_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StringResponse::StringResponse() { }

StringResponse::StringResponse(
    std::string p_value
) :
    value(std::move(p_value))
{ }

bool StringResponse::operator==(const StringResponse& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

std::string const& StringResponse::getValue() const {
    return value;
}
void StringResponse::setValue(std::string p_value) {
    value = std::move(p_value);
}

std::string StringResponse::toString() const {
    std::stringstream ss;
    ss << "StringResponse { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void StringResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StringResponse>();
}

std::string StringResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
