// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/test_response_long.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TestResponseLong::TestResponseLong() { }

TestResponseLong::TestResponseLong(
    std::vector<std::string> p_dataPong
) :
    dataPong(std::move(p_dataPong))
{ }

bool TestResponseLong::operator==(const TestResponseLong& other) const {
    return std::tie(
    dataPong
    ) == std::tie(
    other.dataPong
    );
}

std::vector<std::string> const& TestResponseLong::getDataPong() const {
    return dataPong;
}
void TestResponseLong::setDataPong(std::vector<std::string> p_dataPong) {
    dataPong = std::move(p_dataPong);
}

std::string TestResponseLong::toString() const {
    std::stringstream ss;
    ss << "TestResponseLong { ";
    ss << "dataPong: ";
    ss << "[ ";
    for (size_t i = 0; i < this->dataPong.size(); i++) {
        ss << this->dataPong[i];
        if (i < this->dataPong.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TestResponseLong::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TestResponseLong>();
}

std::string TestResponseLong::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
