// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/driver_enable_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DriverEnableRequest::DriverEnableRequest() { }

DriverEnableRequest::DriverEnableRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    double p_timeout
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    timeout(p_timeout)
{ }

bool DriverEnableRequest::operator==(const DriverEnableRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.timeout
    );
}

int DriverEnableRequest::getInterfaceId() const {
    return interfaceId;
}
void DriverEnableRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DriverEnableRequest::getDevice() const {
    return device;
}
void DriverEnableRequest::setDevice(int p_device) {
    device = p_device;
}

int DriverEnableRequest::getAxis() const {
    return axis;
}
void DriverEnableRequest::setAxis(int p_axis) {
    axis = p_axis;
}

double DriverEnableRequest::getTimeout() const {
    return timeout;
}
void DriverEnableRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

std::string DriverEnableRequest::toString() const {
    std::stringstream ss;
    ss << "DriverEnableRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void DriverEnableRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DriverEnableRequest>();
}

std::string DriverEnableRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
