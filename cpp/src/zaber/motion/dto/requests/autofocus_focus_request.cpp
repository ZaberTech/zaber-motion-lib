// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/autofocus_focus_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AutofocusFocusRequest::AutofocusFocusRequest() { }

AutofocusFocusRequest::AutofocusFocusRequest(
    int p_providerId,
    int p_interfaceId,
    int p_focusAddress,
    int p_focusAxis,
    int p_turretAddress,
    bool p_scan,
    bool p_once,
    int p_timeout
) :
    providerId(p_providerId),
    interfaceId(p_interfaceId),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    turretAddress(p_turretAddress),
    scan(p_scan),
    once(p_once),
    timeout(p_timeout)
{ }

bool AutofocusFocusRequest::operator==(const AutofocusFocusRequest& other) const {
    return std::tie(
    providerId,
    interfaceId,
    focusAddress,
    focusAxis,
    turretAddress,
    scan,
    once,
    timeout
    ) == std::tie(
    other.providerId,
    other.interfaceId,
    other.focusAddress,
    other.focusAxis,
    other.turretAddress,
    other.scan,
    other.once,
    other.timeout
    );
}

int AutofocusFocusRequest::getProviderId() const {
    return providerId;
}
void AutofocusFocusRequest::setProviderId(int p_providerId) {
    providerId = p_providerId;
}

int AutofocusFocusRequest::getInterfaceId() const {
    return interfaceId;
}
void AutofocusFocusRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int AutofocusFocusRequest::getFocusAddress() const {
    return focusAddress;
}
void AutofocusFocusRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int AutofocusFocusRequest::getFocusAxis() const {
    return focusAxis;
}
void AutofocusFocusRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

int AutofocusFocusRequest::getTurretAddress() const {
    return turretAddress;
}
void AutofocusFocusRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

bool AutofocusFocusRequest::getScan() const {
    return scan;
}
void AutofocusFocusRequest::setScan(bool p_scan) {
    scan = p_scan;
}

bool AutofocusFocusRequest::getOnce() const {
    return once;
}
void AutofocusFocusRequest::setOnce(bool p_once) {
    once = p_once;
}

int AutofocusFocusRequest::getTimeout() const {
    return timeout;
}
void AutofocusFocusRequest::setTimeout(int p_timeout) {
    timeout = p_timeout;
}

std::string AutofocusFocusRequest::toString() const {
    std::stringstream ss;
    ss << "AutofocusFocusRequest { ";
    ss << "providerId: ";
    ss << this->providerId;
    ss << ", ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "scan: ";
    ss << this->scan;
    ss << ", ";
    ss << "once: ";
    ss << this->once;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void AutofocusFocusRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusFocusRequest>();
}

std::string AutofocusFocusRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
