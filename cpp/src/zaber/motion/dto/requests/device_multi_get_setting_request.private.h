// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/device_multi_get_setting_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/get_setting.private.h"
#include "zaber/motion/dto/ascii/get_axis_setting.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceMultiGetSettingRequest, interfaceId, device, axis, settings, axisSettings)

} // namespace requests
} // namespace motion
} // namespace zaber
