// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_home_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepHomeRequest::LockstepHomeRequest() { }

LockstepHomeRequest::LockstepHomeRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool LockstepHomeRequest::operator==(const LockstepHomeRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.waitUntilIdle
    );
}

int LockstepHomeRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepHomeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepHomeRequest::getDevice() const {
    return device;
}
void LockstepHomeRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepHomeRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepHomeRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

bool LockstepHomeRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void LockstepHomeRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string LockstepHomeRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepHomeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void LockstepHomeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepHomeRequest>();
}

std::string LockstepHomeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
