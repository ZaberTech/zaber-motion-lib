// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/test_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TestResponse::TestResponse() { }

TestResponse::TestResponse(
    std::string p_dataPong
) :
    dataPong(std::move(p_dataPong))
{ }

bool TestResponse::operator==(const TestResponse& other) const {
    return std::tie(
    dataPong
    ) == std::tie(
    other.dataPong
    );
}

std::string const& TestResponse::getDataPong() const {
    return dataPong;
}
void TestResponse::setDataPong(std::string p_dataPong) {
    dataPong = std::move(p_dataPong);
}

std::string TestResponse::toString() const {
    std::stringstream ss;
    ss << "TestResponse { ";
    ss << "dataPong: ";
    ss << this->dataPong;
    ss << " }";
    return ss.str();
}

void TestResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TestResponse>();
}

std::string TestResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
