// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_all_digital_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAllDigitalOutputsScheduleRequest::StreamSetAllDigitalOutputsScheduleRequest() { }

StreamSetAllDigitalOutputsScheduleRequest::StreamSetAllDigitalOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<ascii::DigitalOutputAction> p_values,
    std::vector<ascii::DigitalOutputAction> p_futureValues,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    values(std::move(p_values)),
    futureValues(std::move(p_futureValues)),
    delay(p_delay),
    unit(p_unit)
{ }

bool StreamSetAllDigitalOutputsScheduleRequest::operator==(const StreamSetAllDigitalOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    values,
    futureValues,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.values,
    other.futureValues,
    other.delay,
    other.unit
    );
}

int StreamSetAllDigitalOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAllDigitalOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAllDigitalOutputsScheduleRequest::getDevice() const {
    return device;
}
void StreamSetAllDigitalOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAllDigitalOutputsScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamSetAllDigitalOutputsScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAllDigitalOutputsScheduleRequest::getPvt() const {
    return pvt;
}
void StreamSetAllDigitalOutputsScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<ascii::DigitalOutputAction> const& StreamSetAllDigitalOutputsScheduleRequest::getValues() const {
    return values;
}
void StreamSetAllDigitalOutputsScheduleRequest::setValues(std::vector<ascii::DigitalOutputAction> p_values) {
    values = std::move(p_values);
}

std::vector<ascii::DigitalOutputAction> const& StreamSetAllDigitalOutputsScheduleRequest::getFutureValues() const {
    return futureValues;
}
void StreamSetAllDigitalOutputsScheduleRequest::setFutureValues(std::vector<ascii::DigitalOutputAction> p_futureValues) {
    futureValues = std::move(p_futureValues);
}

double StreamSetAllDigitalOutputsScheduleRequest::getDelay() const {
    return delay;
}
void StreamSetAllDigitalOutputsScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units StreamSetAllDigitalOutputsScheduleRequest::getUnit() const {
    return unit;
}
void StreamSetAllDigitalOutputsScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetAllDigitalOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAllDigitalOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << DigitalOutputAction_toString(this->values[i]);
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "futureValues: ";
    ss << "[ ";
    for (size_t i = 0; i < this->futureValues.size(); i++) {
        ss << DigitalOutputAction_toString(this->futureValues[i]);
        if (i < this->futureValues.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetAllDigitalOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAllDigitalOutputsScheduleRequest>();
}

std::string StreamSetAllDigitalOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
