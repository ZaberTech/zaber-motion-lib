// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/trigger_on_fire_set_to_setting_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TriggerOnFireSetToSettingRequest, interfaceId, device, triggerNumber, action, axis, setting, operation, fromAxis, fromSetting)

} // namespace requests
} // namespace motion
} // namespace zaber
