// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axes_get_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AxesGetSettingRequest::AxesGetSettingRequest() { }

AxesGetSettingRequest::AxesGetSettingRequest(
    std::vector<int> p_interfaces,
    std::vector<int> p_devices,
    std::vector<int> p_axes,
    std::vector<Units> p_unit,
    std::string p_setting
) :
    interfaces(std::move(p_interfaces)),
    devices(std::move(p_devices)),
    axes(std::move(p_axes)),
    unit(std::move(p_unit)),
    setting(std::move(p_setting))
{ }

bool AxesGetSettingRequest::operator==(const AxesGetSettingRequest& other) const {
    return std::tie(
    interfaces,
    devices,
    axes,
    unit,
    setting
    ) == std::tie(
    other.interfaces,
    other.devices,
    other.axes,
    other.unit,
    other.setting
    );
}

std::vector<int> const& AxesGetSettingRequest::getInterfaces() const {
    return interfaces;
}
void AxesGetSettingRequest::setInterfaces(std::vector<int> p_interfaces) {
    interfaces = std::move(p_interfaces);
}

std::vector<int> const& AxesGetSettingRequest::getDevices() const {
    return devices;
}
void AxesGetSettingRequest::setDevices(std::vector<int> p_devices) {
    devices = std::move(p_devices);
}

std::vector<int> const& AxesGetSettingRequest::getAxes() const {
    return axes;
}
void AxesGetSettingRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::vector<Units> const& AxesGetSettingRequest::getUnit() const {
    return unit;
}
void AxesGetSettingRequest::setUnit(std::vector<Units> p_unit) {
    unit = std::move(p_unit);
}

std::string const& AxesGetSettingRequest::getSetting() const {
    return setting;
}
void AxesGetSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::string AxesGetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "AxesGetSettingRequest { ";
    ss << "interfaces: ";
    ss << "[ ";
    for (size_t i = 0; i < this->interfaces.size(); i++) {
        ss << this->interfaces[i];
        if (i < this->interfaces.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "devices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->devices.size(); i++) {
        ss << this->devices[i];
        if (i < this->devices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "unit: ";
    ss << "[ ";
    for (size_t i = 0; i < this->unit.size(); i++) {
        ss << getUnitLongName(this->unit[i]);
        if (i < this->unit.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << " }";
    return ss.str();
}

void AxesGetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxesGetSettingRequest>();
}

std::string AxesGetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
