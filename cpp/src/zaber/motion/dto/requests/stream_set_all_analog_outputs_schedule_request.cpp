// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_all_analog_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAllAnalogOutputsScheduleRequest::StreamSetAllAnalogOutputsScheduleRequest() { }

StreamSetAllAnalogOutputsScheduleRequest::StreamSetAllAnalogOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<double> p_values,
    std::vector<double> p_futureValues,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    values(std::move(p_values)),
    futureValues(std::move(p_futureValues)),
    delay(p_delay),
    unit(p_unit)
{ }

bool StreamSetAllAnalogOutputsScheduleRequest::operator==(const StreamSetAllAnalogOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    values,
    futureValues,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.values,
    other.futureValues,
    other.delay,
    other.unit
    );
}

int StreamSetAllAnalogOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAllAnalogOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAllAnalogOutputsScheduleRequest::getDevice() const {
    return device;
}
void StreamSetAllAnalogOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAllAnalogOutputsScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamSetAllAnalogOutputsScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAllAnalogOutputsScheduleRequest::getPvt() const {
    return pvt;
}
void StreamSetAllAnalogOutputsScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<double> const& StreamSetAllAnalogOutputsScheduleRequest::getValues() const {
    return values;
}
void StreamSetAllAnalogOutputsScheduleRequest::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::vector<double> const& StreamSetAllAnalogOutputsScheduleRequest::getFutureValues() const {
    return futureValues;
}
void StreamSetAllAnalogOutputsScheduleRequest::setFutureValues(std::vector<double> p_futureValues) {
    futureValues = std::move(p_futureValues);
}

double StreamSetAllAnalogOutputsScheduleRequest::getDelay() const {
    return delay;
}
void StreamSetAllAnalogOutputsScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units StreamSetAllAnalogOutputsScheduleRequest::getUnit() const {
    return unit;
}
void StreamSetAllAnalogOutputsScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetAllAnalogOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAllAnalogOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "futureValues: ";
    ss << "[ ";
    for (size_t i = 0; i < this->futureValues.size(); i++) {
        ss << this->futureValues[i];
        if (i < this->futureValues.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetAllAnalogOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAllAnalogOutputsScheduleRequest>();
}

std::string StreamSetAllAnalogOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
