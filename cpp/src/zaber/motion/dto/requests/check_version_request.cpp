// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/check_version_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CheckVersionRequest::CheckVersionRequest() { }

CheckVersionRequest::CheckVersionRequest(
    std::string p_version,
    std::string p_host
) :
    version(std::move(p_version)),
    host(std::move(p_host))
{ }

bool CheckVersionRequest::operator==(const CheckVersionRequest& other) const {
    return std::tie(
    version,
    host
    ) == std::tie(
    other.version,
    other.host
    );
}

std::string const& CheckVersionRequest::getVersion() const {
    return version;
}
void CheckVersionRequest::setVersion(std::string p_version) {
    version = std::move(p_version);
}

std::string const& CheckVersionRequest::getHost() const {
    return host;
}
void CheckVersionRequest::setHost(std::string p_host) {
    host = std::move(p_host);
}

std::string CheckVersionRequest::toString() const {
    std::stringstream ss;
    ss << "CheckVersionRequest { ";
    ss << "version: ";
    ss << this->version;
    ss << ", ";
    ss << "host: ";
    ss << this->host;
    ss << " }";
    return ss.str();
}

void CheckVersionRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CheckVersionRequest>();
}

std::string CheckVersionRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
