// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_digital_output_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetDigitalOutputRequest::DeviceSetDigitalOutputRequest() { }

DeviceSetDigitalOutputRequest::DeviceSetDigitalOutputRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    ascii::DigitalOutputAction p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    value(std::move(p_value))
{ }

bool DeviceSetDigitalOutputRequest::operator==(const DeviceSetDigitalOutputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.value
    );
}

int DeviceSetDigitalOutputRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetDigitalOutputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetDigitalOutputRequest::getDevice() const {
    return device;
}
void DeviceSetDigitalOutputRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetDigitalOutputRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceSetDigitalOutputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

ascii::DigitalOutputAction const& DeviceSetDigitalOutputRequest::getValue() const {
    return value;
}
void DeviceSetDigitalOutputRequest::setValue(ascii::DigitalOutputAction p_value) {
    value = std::move(p_value);
}

std::string DeviceSetDigitalOutputRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetDigitalOutputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << DigitalOutputAction_toString(this->value);
    ss << " }";
    return ss.str();
}

void DeviceSetDigitalOutputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetDigitalOutputRequest>();
}

std::string DeviceSetDigitalOutputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
