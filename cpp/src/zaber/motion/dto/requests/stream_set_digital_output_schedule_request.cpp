// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_digital_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetDigitalOutputScheduleRequest::StreamSetDigitalOutputScheduleRequest() { }

StreamSetDigitalOutputScheduleRequest::StreamSetDigitalOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    ascii::DigitalOutputAction p_value,
    ascii::DigitalOutputAction p_futureValue,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    value(std::move(p_value)),
    futureValue(std::move(p_futureValue)),
    delay(p_delay),
    unit(p_unit)
{ }

bool StreamSetDigitalOutputScheduleRequest::operator==(const StreamSetDigitalOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    value,
    futureValue,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.value,
    other.futureValue,
    other.delay,
    other.unit
    );
}

int StreamSetDigitalOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetDigitalOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetDigitalOutputScheduleRequest::getDevice() const {
    return device;
}
void StreamSetDigitalOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetDigitalOutputScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamSetDigitalOutputScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetDigitalOutputScheduleRequest::getPvt() const {
    return pvt;
}
void StreamSetDigitalOutputScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetDigitalOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamSetDigitalOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

ascii::DigitalOutputAction const& StreamSetDigitalOutputScheduleRequest::getValue() const {
    return value;
}
void StreamSetDigitalOutputScheduleRequest::setValue(ascii::DigitalOutputAction p_value) {
    value = std::move(p_value);
}

ascii::DigitalOutputAction const& StreamSetDigitalOutputScheduleRequest::getFutureValue() const {
    return futureValue;
}
void StreamSetDigitalOutputScheduleRequest::setFutureValue(ascii::DigitalOutputAction p_futureValue) {
    futureValue = std::move(p_futureValue);
}

double StreamSetDigitalOutputScheduleRequest::getDelay() const {
    return delay;
}
void StreamSetDigitalOutputScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units StreamSetDigitalOutputScheduleRequest::getUnit() const {
    return unit;
}
void StreamSetDigitalOutputScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetDigitalOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetDigitalOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << DigitalOutputAction_toString(this->value);
    ss << ", ";
    ss << "futureValue: ";
    ss << DigitalOutputAction_toString(this->futureValue);
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetDigitalOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetDigitalOutputScheduleRequest>();
}

std::string StreamSetDigitalOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
