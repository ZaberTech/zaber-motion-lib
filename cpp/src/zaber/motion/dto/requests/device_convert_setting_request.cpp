// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_convert_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceConvertSettingRequest::DeviceConvertSettingRequest() { }

DeviceConvertSettingRequest::DeviceConvertSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_setting,
    Units p_unit,
    double p_value,
    bool p_fromNative
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    setting(std::move(p_setting)),
    unit(p_unit),
    value(p_value),
    fromNative(p_fromNative)
{ }

bool DeviceConvertSettingRequest::operator==(const DeviceConvertSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    setting,
    unit,
    value,
    fromNative
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.setting,
    other.unit,
    other.value,
    other.fromNative
    );
}

int DeviceConvertSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceConvertSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceConvertSettingRequest::getDevice() const {
    return device;
}
void DeviceConvertSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceConvertSettingRequest::getAxis() const {
    return axis;
}
void DeviceConvertSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceConvertSettingRequest::getSetting() const {
    return setting;
}
void DeviceConvertSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

Units DeviceConvertSettingRequest::getUnit() const {
    return unit;
}
void DeviceConvertSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

double DeviceConvertSettingRequest::getValue() const {
    return value;
}
void DeviceConvertSettingRequest::setValue(double p_value) {
    value = p_value;
}

bool DeviceConvertSettingRequest::getFromNative() const {
    return fromNative;
}
void DeviceConvertSettingRequest::setFromNative(bool p_fromNative) {
    fromNative = p_fromNative;
}

std::string DeviceConvertSettingRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceConvertSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "fromNative: ";
    ss << this->fromNative;
    ss << " }";
    return ss.str();
}

void DeviceConvertSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceConvertSettingRequest>();
}

std::string DeviceConvertSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
