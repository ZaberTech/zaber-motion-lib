// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/wdi_get_status_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

WdiGetStatusResponse::WdiGetStatusResponse() { }

WdiGetStatusResponse::WdiGetStatusResponse(
    microscopy::WdiAutofocusProviderStatus p_status
) :
    status(std::move(p_status))
{ }

bool WdiGetStatusResponse::operator==(const WdiGetStatusResponse& other) const {
    return std::tie(
    status
    ) == std::tie(
    other.status
    );
}

microscopy::WdiAutofocusProviderStatus const& WdiGetStatusResponse::getStatus() const {
    return status;
}
void WdiGetStatusResponse::setStatus(microscopy::WdiAutofocusProviderStatus p_status) {
    status = std::move(p_status);
}

std::string WdiGetStatusResponse::toString() const {
    std::stringstream ss;
    ss << "WdiGetStatusResponse { ";
    ss << "status: ";
    ss << this->status.toString();
    ss << " }";
    return ss.str();
}

void WdiGetStatusResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<WdiGetStatusResponse>();
}

std::string WdiGetStatusResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
