// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_analog_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAnalogOutputScheduleRequest::StreamSetAnalogOutputScheduleRequest() { }

StreamSetAnalogOutputScheduleRequest::StreamSetAnalogOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    double p_value,
    double p_futureValue,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    value(p_value),
    futureValue(p_futureValue),
    delay(p_delay),
    unit(p_unit)
{ }

bool StreamSetAnalogOutputScheduleRequest::operator==(const StreamSetAnalogOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    value,
    futureValue,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.value,
    other.futureValue,
    other.delay,
    other.unit
    );
}

int StreamSetAnalogOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAnalogOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAnalogOutputScheduleRequest::getDevice() const {
    return device;
}
void StreamSetAnalogOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAnalogOutputScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamSetAnalogOutputScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAnalogOutputScheduleRequest::getPvt() const {
    return pvt;
}
void StreamSetAnalogOutputScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetAnalogOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamSetAnalogOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double StreamSetAnalogOutputScheduleRequest::getValue() const {
    return value;
}
void StreamSetAnalogOutputScheduleRequest::setValue(double p_value) {
    value = p_value;
}

double StreamSetAnalogOutputScheduleRequest::getFutureValue() const {
    return futureValue;
}
void StreamSetAnalogOutputScheduleRequest::setFutureValue(double p_futureValue) {
    futureValue = p_futureValue;
}

double StreamSetAnalogOutputScheduleRequest::getDelay() const {
    return delay;
}
void StreamSetAnalogOutputScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units StreamSetAnalogOutputScheduleRequest::getUnit() const {
    return unit;
}
void StreamSetAnalogOutputScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetAnalogOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAnalogOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "futureValue: ";
    ss << this->futureValue;
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetAnalogOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAnalogOutputScheduleRequest>();
}

std::string StreamSetAnalogOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
