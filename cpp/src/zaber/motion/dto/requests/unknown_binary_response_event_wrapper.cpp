// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unknown_binary_response_event_wrapper.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnknownBinaryResponseEventWrapper::UnknownBinaryResponseEventWrapper() { }

UnknownBinaryResponseEventWrapper::UnknownBinaryResponseEventWrapper(
    int p_interfaceId,
    binary::UnknownResponseEvent p_unknownResponse
) :
    interfaceId(p_interfaceId),
    unknownResponse(std::move(p_unknownResponse))
{ }

bool UnknownBinaryResponseEventWrapper::operator==(const UnknownBinaryResponseEventWrapper& other) const {
    return std::tie(
    interfaceId,
    unknownResponse
    ) == std::tie(
    other.interfaceId,
    other.unknownResponse
    );
}

int UnknownBinaryResponseEventWrapper::getInterfaceId() const {
    return interfaceId;
}
void UnknownBinaryResponseEventWrapper::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

binary::UnknownResponseEvent const& UnknownBinaryResponseEventWrapper::getUnknownResponse() const {
    return unknownResponse;
}
void UnknownBinaryResponseEventWrapper::setUnknownResponse(binary::UnknownResponseEvent p_unknownResponse) {
    unknownResponse = std::move(p_unknownResponse);
}

std::string UnknownBinaryResponseEventWrapper::toString() const {
    std::stringstream ss;
    ss << "UnknownBinaryResponseEventWrapper { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "unknownResponse: ";
    ss << this->unknownResponse.toString();
    ss << " }";
    return ss.str();
}

void UnknownBinaryResponseEventWrapper::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnknownBinaryResponseEventWrapper>();
}

std::string UnknownBinaryResponseEventWrapper::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
