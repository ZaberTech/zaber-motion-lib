// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/interface_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

InterfaceEmptyRequest::InterfaceEmptyRequest() { }

InterfaceEmptyRequest::InterfaceEmptyRequest(
    int p_interfaceId
) :
    interfaceId(p_interfaceId)
{ }

bool InterfaceEmptyRequest::operator==(const InterfaceEmptyRequest& other) const {
    return std::tie(
    interfaceId
    ) == std::tie(
    other.interfaceId
    );
}

int InterfaceEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void InterfaceEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

std::string InterfaceEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "InterfaceEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << " }";
    return ss.str();
}

void InterfaceEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<InterfaceEmptyRequest>();
}

std::string InterfaceEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
