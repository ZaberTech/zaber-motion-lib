// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_line_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamLineRequest::StreamLineRequest() { }

StreamLineRequest::StreamLineRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    StreamSegmentType p_type,
    std::vector<Measurement> p_endpoint,
    std::vector<int> p_targetAxesIndices
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    type(std::move(p_type)),
    endpoint(std::move(p_endpoint)),
    targetAxesIndices(std::move(p_targetAxesIndices))
{ }

bool StreamLineRequest::operator==(const StreamLineRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    type,
    endpoint,
    targetAxesIndices
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.type,
    other.endpoint,
    other.targetAxesIndices
    );
}

int StreamLineRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamLineRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamLineRequest::getDevice() const {
    return device;
}
void StreamLineRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamLineRequest::getStreamId() const {
    return streamId;
}
void StreamLineRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamLineRequest::getPvt() const {
    return pvt;
}
void StreamLineRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

StreamSegmentType const& StreamLineRequest::getType() const {
    return type;
}
void StreamLineRequest::setType(StreamSegmentType p_type) {
    type = std::move(p_type);
}

std::vector<Measurement> const& StreamLineRequest::getEndpoint() const {
    return endpoint;
}
void StreamLineRequest::setEndpoint(std::vector<Measurement> p_endpoint) {
    endpoint = std::move(p_endpoint);
}

std::vector<int> const& StreamLineRequest::getTargetAxesIndices() const {
    return targetAxesIndices;
}
void StreamLineRequest::setTargetAxesIndices(std::vector<int> p_targetAxesIndices) {
    targetAxesIndices = std::move(p_targetAxesIndices);
}

std::string StreamLineRequest::toString() const {
    std::stringstream ss;
    ss << "StreamLineRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "type: ";
    ss << StreamSegmentType_toString(this->type);
    ss << ", ";
    ss << "endpoint: ";
    ss << "[ ";
    for (size_t i = 0; i < this->endpoint.size(); i++) {
        ss << this->endpoint[i].toString();
        if (i < this->endpoint.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "targetAxesIndices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->targetAxesIndices.size(); i++) {
        ss << this->targetAxesIndices[i];
        if (i < this->targetAxesIndices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamLineRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamLineRequest>();
}

std::string StreamLineRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
