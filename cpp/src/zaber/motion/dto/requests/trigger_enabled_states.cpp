// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_enabled_states.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerEnabledStates::TriggerEnabledStates() { }

TriggerEnabledStates::TriggerEnabledStates(
    std::vector<ascii::TriggerEnabledState> p_states
) :
    states(std::move(p_states))
{ }

bool TriggerEnabledStates::operator==(const TriggerEnabledStates& other) const {
    return std::tie(
    states
    ) == std::tie(
    other.states
    );
}

std::vector<ascii::TriggerEnabledState> const& TriggerEnabledStates::getStates() const {
    return states;
}
void TriggerEnabledStates::setStates(std::vector<ascii::TriggerEnabledState> p_states) {
    states = std::move(p_states);
}

std::string TriggerEnabledStates::toString() const {
    std::stringstream ss;
    ss << "TriggerEnabledStates { ";
    ss << "states: ";
    ss << "[ ";
    for (size_t i = 0; i < this->states.size(); i++) {
        ss << this->states[i].toString();
        if (i < this->states.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TriggerEnabledStates::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerEnabledStates>();
}

std::string TriggerEnabledStates::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
