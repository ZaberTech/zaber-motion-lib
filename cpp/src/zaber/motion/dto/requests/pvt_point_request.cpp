// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/pvt_point_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

PvtPointRequest::PvtPointRequest() { }

PvtPointRequest::PvtPointRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    StreamSegmentType p_type,
    std::vector<Measurement> p_positions,
    std::vector<std::optional<Measurement>> p_velocities,
    Measurement p_time
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    type(std::move(p_type)),
    positions(std::move(p_positions)),
    velocities(std::move(p_velocities)),
    time(std::move(p_time))
{ }

bool PvtPointRequest::operator==(const PvtPointRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    type,
    positions,
    velocities,
    time
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.type,
    other.positions,
    other.velocities,
    other.time
    );
}

int PvtPointRequest::getInterfaceId() const {
    return interfaceId;
}
void PvtPointRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int PvtPointRequest::getDevice() const {
    return device;
}
void PvtPointRequest::setDevice(int p_device) {
    device = p_device;
}

int PvtPointRequest::getStreamId() const {
    return streamId;
}
void PvtPointRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool PvtPointRequest::getPvt() const {
    return pvt;
}
void PvtPointRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

StreamSegmentType const& PvtPointRequest::getType() const {
    return type;
}
void PvtPointRequest::setType(StreamSegmentType p_type) {
    type = std::move(p_type);
}

std::vector<Measurement> const& PvtPointRequest::getPositions() const {
    return positions;
}
void PvtPointRequest::setPositions(std::vector<Measurement> p_positions) {
    positions = std::move(p_positions);
}

std::vector<std::optional<Measurement>> const& PvtPointRequest::getVelocities() const {
    return velocities;
}
void PvtPointRequest::setVelocities(std::vector<std::optional<Measurement>> p_velocities) {
    velocities = std::move(p_velocities);
}

Measurement const& PvtPointRequest::getTime() const {
    return time;
}
void PvtPointRequest::setTime(Measurement p_time) {
    time = std::move(p_time);
}

std::string PvtPointRequest::toString() const {
    std::stringstream ss;
    ss << "PvtPointRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "type: ";
    ss << StreamSegmentType_toString(this->type);
    ss << ", ";
    ss << "positions: ";
    ss << "[ ";
    for (size_t i = 0; i < this->positions.size(); i++) {
        ss << this->positions[i].toString();
        if (i < this->positions.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "velocities: ";
    ss << "[ ";
    for (size_t i = 0; i < this->velocities.size(); i++) {
        if (this->velocities[i].has_value()) {
            ss << this->velocities[i].value().toString();
        } else {
            ss << "null";
        }
        if (i < this->velocities.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "time: ";
    ss << this->time.toString();
    ss << " }";
    return ss.str();
}

void PvtPointRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PvtPointRequest>();
}

std::string PvtPointRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
