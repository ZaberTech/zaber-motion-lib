// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unit_get_enum_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnitGetEnumRequest::UnitGetEnumRequest() { }

UnitGetEnumRequest::UnitGetEnumRequest(
    std::string p_symbol
) :
    symbol(std::move(p_symbol))
{ }

bool UnitGetEnumRequest::operator==(const UnitGetEnumRequest& other) const {
    return std::tie(
    symbol
    ) == std::tie(
    other.symbol
    );
}

std::string const& UnitGetEnumRequest::getSymbol() const {
    return symbol;
}
void UnitGetEnumRequest::setSymbol(std::string p_symbol) {
    symbol = std::move(p_symbol);
}

std::string UnitGetEnumRequest::toString() const {
    std::stringstream ss;
    ss << "UnitGetEnumRequest { ";
    ss << "symbol: ";
    ss << this->symbol;
    ss << " }";
    return ss.str();
}

void UnitGetEnumRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnitGetEnumRequest>();
}

std::string UnitGetEnumRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
