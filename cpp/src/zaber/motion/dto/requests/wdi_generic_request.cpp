// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/wdi_generic_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

WdiGenericRequest::WdiGenericRequest() { }

WdiGenericRequest::WdiGenericRequest(
    int p_interfaceId,
    int p_registerId,
    int p_size,
    int p_count,
    int p_offset,
    std::string p_registerBank,
    std::vector<int> p_data
) :
    interfaceId(p_interfaceId),
    registerId(p_registerId),
    size(p_size),
    count(p_count),
    offset(p_offset),
    registerBank(std::move(p_registerBank)),
    data(std::move(p_data))
{ }

bool WdiGenericRequest::operator==(const WdiGenericRequest& other) const {
    return std::tie(
    interfaceId,
    registerId,
    size,
    count,
    offset,
    registerBank,
    data
    ) == std::tie(
    other.interfaceId,
    other.registerId,
    other.size,
    other.count,
    other.offset,
    other.registerBank,
    other.data
    );
}

int WdiGenericRequest::getInterfaceId() const {
    return interfaceId;
}
void WdiGenericRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int WdiGenericRequest::getRegisterId() const {
    return registerId;
}
void WdiGenericRequest::setRegisterId(int p_registerId) {
    registerId = p_registerId;
}

int WdiGenericRequest::getSize() const {
    return size;
}
void WdiGenericRequest::setSize(int p_size) {
    size = p_size;
}

int WdiGenericRequest::getCount() const {
    return count;
}
void WdiGenericRequest::setCount(int p_count) {
    count = p_count;
}

int WdiGenericRequest::getOffset() const {
    return offset;
}
void WdiGenericRequest::setOffset(int p_offset) {
    offset = p_offset;
}

std::string const& WdiGenericRequest::getRegisterBank() const {
    return registerBank;
}
void WdiGenericRequest::setRegisterBank(std::string p_registerBank) {
    registerBank = std::move(p_registerBank);
}

std::vector<int> const& WdiGenericRequest::getData() const {
    return data;
}
void WdiGenericRequest::setData(std::vector<int> p_data) {
    data = std::move(p_data);
}

std::string WdiGenericRequest::toString() const {
    std::stringstream ss;
    ss << "WdiGenericRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "registerId: ";
    ss << this->registerId;
    ss << ", ";
    ss << "size: ";
    ss << this->size;
    ss << ", ";
    ss << "count: ";
    ss << this->count;
    ss << ", ";
    ss << "offset: ";
    ss << this->offset;
    ss << ", ";
    ss << "registerBank: ";
    ss << this->registerBank;
    ss << ", ";
    ss << "data: ";
    ss << "[ ";
    for (size_t i = 0; i < this->data.size(); i++) {
        ss << this->data[i];
        if (i < this->data.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void WdiGenericRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<WdiGenericRequest>();
}

std::string WdiGenericRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
