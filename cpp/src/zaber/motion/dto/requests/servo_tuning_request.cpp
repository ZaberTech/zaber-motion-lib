// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/servo_tuning_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ServoTuningRequest::ServoTuningRequest() { }

ServoTuningRequest::ServoTuningRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_paramset
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    paramset(std::move(p_paramset))
{ }

bool ServoTuningRequest::operator==(const ServoTuningRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    paramset
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.paramset
    );
}

int ServoTuningRequest::getInterfaceId() const {
    return interfaceId;
}
void ServoTuningRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ServoTuningRequest::getDevice() const {
    return device;
}
void ServoTuningRequest::setDevice(int p_device) {
    device = p_device;
}

int ServoTuningRequest::getAxis() const {
    return axis;
}
void ServoTuningRequest::setAxis(int p_axis) {
    axis = p_axis;
}

ascii::ServoTuningParamset const& ServoTuningRequest::getParamset() const {
    return paramset;
}
void ServoTuningRequest::setParamset(ascii::ServoTuningParamset p_paramset) {
    paramset = std::move(p_paramset);
}

std::string ServoTuningRequest::toString() const {
    std::stringstream ss;
    ss << "ServoTuningRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "paramset: ";
    ss << ServoTuningParamset_toString(this->paramset);
    ss << " }";
    return ss.str();
}

void ServoTuningRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ServoTuningRequest>();
}

std::string ServoTuningRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
