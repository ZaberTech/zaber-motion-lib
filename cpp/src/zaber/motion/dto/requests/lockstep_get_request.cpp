// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_get_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepGetRequest::LockstepGetRequest() { }

LockstepGetRequest::LockstepGetRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    unit(p_unit)
{ }

bool LockstepGetRequest::operator==(const LockstepGetRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.unit
    );
}

int LockstepGetRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepGetRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepGetRequest::getDevice() const {
    return device;
}
void LockstepGetRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepGetRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepGetRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

Units LockstepGetRequest::getUnit() const {
    return unit;
}
void LockstepGetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string LockstepGetRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepGetRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void LockstepGetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepGetRequest>();
}

std::string LockstepGetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
