// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_storage_bool_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetStorageBoolRequest::DeviceSetStorageBoolRequest() { }

DeviceSetStorageBoolRequest::DeviceSetStorageBoolRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key,
    bool p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key)),
    value(p_value)
{ }

bool DeviceSetStorageBoolRequest::operator==(const DeviceSetStorageBoolRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key,
    other.value
    );
}

int DeviceSetStorageBoolRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetStorageBoolRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetStorageBoolRequest::getDevice() const {
    return device;
}
void DeviceSetStorageBoolRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetStorageBoolRequest::getAxis() const {
    return axis;
}
void DeviceSetStorageBoolRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetStorageBoolRequest::getKey() const {
    return key;
}
void DeviceSetStorageBoolRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

bool DeviceSetStorageBoolRequest::getValue() const {
    return value;
}
void DeviceSetStorageBoolRequest::setValue(bool p_value) {
    value = p_value;
}

std::string DeviceSetStorageBoolRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetStorageBoolRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void DeviceSetStorageBoolRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetStorageBoolRequest>();
}

std::string DeviceSetStorageBoolRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
