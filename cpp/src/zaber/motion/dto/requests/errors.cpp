// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/errors.h"

namespace zaber {
namespace motion {
namespace requests {

std::string Errors_toString(Errors value) {
    switch (value) {
        case Errors::BAD_COMMAND: return "BAD_COMMAND";
        case Errors::BAD_DATA: return "BAD_DATA";
        case Errors::BINARY_COMMAND_FAILED: return "BINARY_COMMAND_FAILED";
        case Errors::COMMAND_FAILED: return "COMMAND_FAILED";
        case Errors::COMMAND_PREEMPTED: return "COMMAND_PREEMPTED";
        case Errors::COMMAND_TOO_LONG: return "COMMAND_TOO_LONG";
        case Errors::CONNECTION_CLOSED: return "CONNECTION_CLOSED";
        case Errors::CONNECTION_FAILED: return "CONNECTION_FAILED";
        case Errors::CONVERSION_FAILED: return "CONVERSION_FAILED";
        case Errors::DEVICE_ADDRESS_CONFLICT: return "DEVICE_ADDRESS_CONFLICT";
        case Errors::DEVICE_BUSY: return "DEVICE_BUSY";
        case Errors::DEVICE_DB_FAILED: return "DEVICE_DB_FAILED";
        case Errors::DEVICE_DETECTION_FAILED: return "DEVICE_DETECTION_FAILED";
        case Errors::DEVICE_FAILED: return "DEVICE_FAILED";
        case Errors::DEVICE_NOT_IDENTIFIED: return "DEVICE_NOT_IDENTIFIED";
        case Errors::DRIVER_DISABLED: return "DRIVER_DISABLED";
        case Errors::G_CODE_EXECUTION: return "G_CODE_EXECUTION";
        case Errors::G_CODE_SYNTAX: return "G_CODE_SYNTAX";
        case Errors::INCOMPATIBLE_SHARED_LIBRARY: return "INCOMPATIBLE_SHARED_LIBRARY";
        case Errors::INTERNAL_ERROR: return "INTERNAL_ERROR";
        case Errors::INVALID_ARGUMENT: return "INVALID_ARGUMENT";
        case Errors::INVALID_DATA: return "INVALID_DATA";
        case Errors::INVALID_OPERATION: return "INVALID_OPERATION";
        case Errors::INVALID_PACKET: return "INVALID_PACKET";
        case Errors::INVALID_PARK_STATE: return "INVALID_PARK_STATE";
        case Errors::INVALID_REQUEST_DATA: return "INVALID_REQUEST_DATA";
        case Errors::INVALID_RESPONSE: return "INVALID_RESPONSE";
        case Errors::IO_CHANNEL_OUT_OF_RANGE: return "IO_CHANNEL_OUT_OF_RANGE";
        case Errors::IO_FAILED: return "IO_FAILED";
        case Errors::LOCKSTEP_ENABLED: return "LOCKSTEP_ENABLED";
        case Errors::LOCKSTEP_NOT_ENABLED: return "LOCKSTEP_NOT_ENABLED";
        case Errors::MOVEMENT_FAILED: return "MOVEMENT_FAILED";
        case Errors::MOVEMENT_INTERRUPTED: return "MOVEMENT_INTERRUPTED";
        case Errors::NO_DEVICE_FOUND: return "NO_DEVICE_FOUND";
        case Errors::NO_VALUE_FOR_KEY: return "NO_VALUE_FOR_KEY";
        case Errors::NOT_SUPPORTED: return "NOT_SUPPORTED";
        case Errors::OPERATION_FAILED: return "OPERATION_FAILED";
        case Errors::OS_FAILED: return "OS_FAILED";
        case Errors::OUT_OF_REQUEST_IDS: return "OUT_OF_REQUEST_IDS";
        case Errors::PVT_DISCONTINUITY: return "PVT_DISCONTINUITY";
        case Errors::PVT_EXECUTION: return "PVT_EXECUTION";
        case Errors::PVT_MODE: return "PVT_MODE";
        case Errors::PVT_MOVEMENT_FAILED: return "PVT_MOVEMENT_FAILED";
        case Errors::PVT_MOVEMENT_INTERRUPTED: return "PVT_MOVEMENT_INTERRUPTED";
        case Errors::PVT_SETUP_FAILED: return "PVT_SETUP_FAILED";
        case Errors::REMOTE_MODE: return "REMOTE_MODE";
        case Errors::REQUEST_TIMEOUT: return "REQUEST_TIMEOUT";
        case Errors::SERIAL_PORT_BUSY: return "SERIAL_PORT_BUSY";
        case Errors::SET_DEVICE_STATE_FAILED: return "SET_DEVICE_STATE_FAILED";
        case Errors::SET_PERIPHERAL_STATE_FAILED: return "SET_PERIPHERAL_STATE_FAILED";
        case Errors::SETTING_NOT_FOUND: return "SETTING_NOT_FOUND";
        case Errors::STREAM_DISCONTINUITY: return "STREAM_DISCONTINUITY";
        case Errors::STREAM_EXECUTION: return "STREAM_EXECUTION";
        case Errors::STREAM_MODE: return "STREAM_MODE";
        case Errors::STREAM_MOVEMENT_FAILED: return "STREAM_MOVEMENT_FAILED";
        case Errors::STREAM_MOVEMENT_INTERRUPTED: return "STREAM_MOVEMENT_INTERRUPTED";
        case Errors::STREAM_SETUP_FAILED: return "STREAM_SETUP_FAILED";
        case Errors::TIMEOUT: return "TIMEOUT";
        case Errors::TRANSPORT_ALREADY_USED: return "TRANSPORT_ALREADY_USED";
        case Errors::UNKNOWN_REQUEST: return "UNKNOWN_REQUEST";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber
