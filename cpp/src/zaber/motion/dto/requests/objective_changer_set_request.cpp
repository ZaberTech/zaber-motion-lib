// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/objective_changer_set_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ObjectiveChangerSetRequest::ObjectiveChangerSetRequest() { }

ObjectiveChangerSetRequest::ObjectiveChangerSetRequest(
    int p_interfaceId,
    int p_turretAddress,
    int p_focusAddress,
    int p_focusAxis,
    double p_value,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    turretAddress(p_turretAddress),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    value(p_value),
    unit(p_unit)
{ }

bool ObjectiveChangerSetRequest::operator==(const ObjectiveChangerSetRequest& other) const {
    return std::tie(
    interfaceId,
    turretAddress,
    focusAddress,
    focusAxis,
    value,
    unit
    ) == std::tie(
    other.interfaceId,
    other.turretAddress,
    other.focusAddress,
    other.focusAxis,
    other.value,
    other.unit
    );
}

int ObjectiveChangerSetRequest::getInterfaceId() const {
    return interfaceId;
}
void ObjectiveChangerSetRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ObjectiveChangerSetRequest::getTurretAddress() const {
    return turretAddress;
}
void ObjectiveChangerSetRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

int ObjectiveChangerSetRequest::getFocusAddress() const {
    return focusAddress;
}
void ObjectiveChangerSetRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int ObjectiveChangerSetRequest::getFocusAxis() const {
    return focusAxis;
}
void ObjectiveChangerSetRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

double ObjectiveChangerSetRequest::getValue() const {
    return value;
}
void ObjectiveChangerSetRequest::setValue(double p_value) {
    value = p_value;
}

Units ObjectiveChangerSetRequest::getUnit() const {
    return unit;
}
void ObjectiveChangerSetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string ObjectiveChangerSetRequest::toString() const {
    std::stringstream ss;
    ss << "ObjectiveChangerSetRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void ObjectiveChangerSetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ObjectiveChangerSetRequest>();
}

std::string ObjectiveChangerSetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
