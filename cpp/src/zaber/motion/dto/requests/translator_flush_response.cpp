// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_flush_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorFlushResponse::TranslatorFlushResponse() { }

TranslatorFlushResponse::TranslatorFlushResponse(
    std::vector<std::string> p_commands
) :
    commands(std::move(p_commands))
{ }

bool TranslatorFlushResponse::operator==(const TranslatorFlushResponse& other) const {
    return std::tie(
    commands
    ) == std::tie(
    other.commands
    );
}

std::vector<std::string> const& TranslatorFlushResponse::getCommands() const {
    return commands;
}
void TranslatorFlushResponse::setCommands(std::vector<std::string> p_commands) {
    commands = std::move(p_commands);
}

std::string TranslatorFlushResponse::toString() const {
    std::stringstream ss;
    ss << "TranslatorFlushResponse { ";
    ss << "commands: ";
    ss << "[ ";
    for (size_t i = 0; i < this->commands.size(); i++) {
        ss << this->commands[i];
        if (i < this->commands.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TranslatorFlushResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorFlushResponse>();
}

std::string TranslatorFlushResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
