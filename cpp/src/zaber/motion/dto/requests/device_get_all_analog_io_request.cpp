// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_all_analog_io_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetAllAnalogIORequest::DeviceGetAllAnalogIORequest() { }

DeviceGetAllAnalogIORequest::DeviceGetAllAnalogIORequest(
    int p_interfaceId,
    int p_device,
    std::string p_channelType
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelType(std::move(p_channelType))
{ }

bool DeviceGetAllAnalogIORequest::operator==(const DeviceGetAllAnalogIORequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelType
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelType
    );
}

int DeviceGetAllAnalogIORequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetAllAnalogIORequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetAllAnalogIORequest::getDevice() const {
    return device;
}
void DeviceGetAllAnalogIORequest::setDevice(int p_device) {
    device = p_device;
}

std::string const& DeviceGetAllAnalogIORequest::getChannelType() const {
    return channelType;
}
void DeviceGetAllAnalogIORequest::setChannelType(std::string p_channelType) {
    channelType = std::move(p_channelType);
}

std::string DeviceGetAllAnalogIORequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetAllAnalogIORequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelType: ";
    ss << this->channelType;
    ss << " }";
    return ss.str();
}

void DeviceGetAllAnalogIORequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetAllAnalogIORequest>();
}

std::string DeviceGetAllAnalogIORequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
