// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_move_sin_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceMoveSinRequest::DeviceMoveSinRequest() { }

DeviceMoveSinRequest::DeviceMoveSinRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    double p_amplitude,
    Units p_amplitudeUnits,
    double p_period,
    Units p_periodUnits,
    double p_count,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    amplitude(p_amplitude),
    amplitudeUnits(p_amplitudeUnits),
    period(p_period),
    periodUnits(p_periodUnits),
    count(p_count),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool DeviceMoveSinRequest::operator==(const DeviceMoveSinRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    amplitude,
    amplitudeUnits,
    period,
    periodUnits,
    count,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.amplitude,
    other.amplitudeUnits,
    other.period,
    other.periodUnits,
    other.count,
    other.waitUntilIdle
    );
}

int DeviceMoveSinRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceMoveSinRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceMoveSinRequest::getDevice() const {
    return device;
}
void DeviceMoveSinRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceMoveSinRequest::getAxis() const {
    return axis;
}
void DeviceMoveSinRequest::setAxis(int p_axis) {
    axis = p_axis;
}

double DeviceMoveSinRequest::getAmplitude() const {
    return amplitude;
}
void DeviceMoveSinRequest::setAmplitude(double p_amplitude) {
    amplitude = p_amplitude;
}

Units DeviceMoveSinRequest::getAmplitudeUnits() const {
    return amplitudeUnits;
}
void DeviceMoveSinRequest::setAmplitudeUnits(Units p_amplitudeUnits) {
    amplitudeUnits = p_amplitudeUnits;
}

double DeviceMoveSinRequest::getPeriod() const {
    return period;
}
void DeviceMoveSinRequest::setPeriod(double p_period) {
    period = p_period;
}

Units DeviceMoveSinRequest::getPeriodUnits() const {
    return periodUnits;
}
void DeviceMoveSinRequest::setPeriodUnits(Units p_periodUnits) {
    periodUnits = p_periodUnits;
}

double DeviceMoveSinRequest::getCount() const {
    return count;
}
void DeviceMoveSinRequest::setCount(double p_count) {
    count = p_count;
}

bool DeviceMoveSinRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void DeviceMoveSinRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string DeviceMoveSinRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceMoveSinRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "amplitude: ";
    ss << this->amplitude;
    ss << ", ";
    ss << "amplitudeUnits: ";
    ss << getUnitLongName(this->amplitudeUnits);
    ss << ", ";
    ss << "period: ";
    ss << this->period;
    ss << ", ";
    ss << "periodUnits: ";
    ss << getUnitLongName(this->periodUnits);
    ss << ", ";
    ss << "count: ";
    ss << this->count;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void DeviceMoveSinRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceMoveSinRequest>();
}

std::string DeviceMoveSinRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
