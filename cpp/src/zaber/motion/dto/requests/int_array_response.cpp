// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/int_array_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

IntArrayResponse::IntArrayResponse() { }

IntArrayResponse::IntArrayResponse(
    std::vector<int> p_values
) :
    values(std::move(p_values))
{ }

bool IntArrayResponse::operator==(const IntArrayResponse& other) const {
    return std::tie(
    values
    ) == std::tie(
    other.values
    );
}

std::vector<int> const& IntArrayResponse::getValues() const {
    return values;
}
void IntArrayResponse::setValues(std::vector<int> p_values) {
    values = std::move(p_values);
}

std::string IntArrayResponse::toString() const {
    std::stringstream ss;
    ss << "IntArrayResponse { ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void IntArrayResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<IntArrayResponse>();
}

std::string IntArrayResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
