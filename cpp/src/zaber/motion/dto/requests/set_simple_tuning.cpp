// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_simple_tuning.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetSimpleTuning::SetSimpleTuning() { }

SetSimpleTuning::SetSimpleTuning(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_paramset,
    std::optional<double> p_carriageMass,
    double p_loadMass,
    std::vector<ascii::ServoTuningParam> p_tuningParams
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    paramset(std::move(p_paramset)),
    carriageMass(p_carriageMass),
    loadMass(p_loadMass),
    tuningParams(std::move(p_tuningParams))
{ }

SetSimpleTuning::SetSimpleTuning(
    int p_interfaceId,
    int p_device,
    int p_axis,
    ascii::ServoTuningParamset p_paramset,
    double p_loadMass,
    std::vector<ascii::ServoTuningParam> p_tuningParams
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    paramset(std::move(p_paramset)),
    loadMass(p_loadMass),
    tuningParams(std::move(p_tuningParams))
{ }

bool SetSimpleTuning::operator==(const SetSimpleTuning& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    paramset,
    carriageMass,
    loadMass,
    tuningParams
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.paramset,
    other.carriageMass,
    other.loadMass,
    other.tuningParams
    );
}

int SetSimpleTuning::getInterfaceId() const {
    return interfaceId;
}
void SetSimpleTuning::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetSimpleTuning::getDevice() const {
    return device;
}
void SetSimpleTuning::setDevice(int p_device) {
    device = p_device;
}

int SetSimpleTuning::getAxis() const {
    return axis;
}
void SetSimpleTuning::setAxis(int p_axis) {
    axis = p_axis;
}

ascii::ServoTuningParamset const& SetSimpleTuning::getParamset() const {
    return paramset;
}
void SetSimpleTuning::setParamset(ascii::ServoTuningParamset p_paramset) {
    paramset = std::move(p_paramset);
}

std::optional<double> SetSimpleTuning::getCarriageMass() const {
    return carriageMass;
}
void SetSimpleTuning::setCarriageMass(std::optional<double> p_carriageMass) {
    carriageMass = p_carriageMass;
}

double SetSimpleTuning::getLoadMass() const {
    return loadMass;
}
void SetSimpleTuning::setLoadMass(double p_loadMass) {
    loadMass = p_loadMass;
}

std::vector<ascii::ServoTuningParam> const& SetSimpleTuning::getTuningParams() const {
    return tuningParams;
}
void SetSimpleTuning::setTuningParams(std::vector<ascii::ServoTuningParam> p_tuningParams) {
    tuningParams = std::move(p_tuningParams);
}

std::string SetSimpleTuning::toString() const {
    std::stringstream ss;
    ss << "SetSimpleTuning { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "paramset: ";
    ss << ServoTuningParamset_toString(this->paramset);
    ss << ", ";
    ss << "carriageMass: ";
    if (this->carriageMass.has_value()) {
        ss << this->carriageMass.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "loadMass: ";
    ss << this->loadMass;
    ss << ", ";
    ss << "tuningParams: ";
    ss << "[ ";
    for (size_t i = 0; i < this->tuningParams.size(); i++) {
        ss << this->tuningParams[i].toString();
        if (i < this->tuningParams.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void SetSimpleTuning::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetSimpleTuning>();
}

std::string SetSimpleTuning::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
