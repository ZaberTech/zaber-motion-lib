// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/open_interface_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(OpenInterfaceRequest, interfaceType, portName, baudRate, hostName, port, transport, rejectRoutedConnection, cloudId, connectionName, realm, token, api)

} // namespace requests
} // namespace motion
} // namespace zaber
