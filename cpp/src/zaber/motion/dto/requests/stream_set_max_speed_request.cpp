// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_max_speed_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetMaxSpeedRequest::StreamSetMaxSpeedRequest() { }

StreamSetMaxSpeedRequest::StreamSetMaxSpeedRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    double p_maxSpeed,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    maxSpeed(p_maxSpeed),
    unit(p_unit)
{ }

bool StreamSetMaxSpeedRequest::operator==(const StreamSetMaxSpeedRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    maxSpeed,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.maxSpeed,
    other.unit
    );
}

int StreamSetMaxSpeedRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetMaxSpeedRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetMaxSpeedRequest::getDevice() const {
    return device;
}
void StreamSetMaxSpeedRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetMaxSpeedRequest::getStreamId() const {
    return streamId;
}
void StreamSetMaxSpeedRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetMaxSpeedRequest::getPvt() const {
    return pvt;
}
void StreamSetMaxSpeedRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

double StreamSetMaxSpeedRequest::getMaxSpeed() const {
    return maxSpeed;
}
void StreamSetMaxSpeedRequest::setMaxSpeed(double p_maxSpeed) {
    maxSpeed = p_maxSpeed;
}

Units StreamSetMaxSpeedRequest::getUnit() const {
    return unit;
}
void StreamSetMaxSpeedRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetMaxSpeedRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetMaxSpeedRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "maxSpeed: ";
    ss << this->maxSpeed;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetMaxSpeedRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetMaxSpeedRequest>();
}

std::string StreamSetMaxSpeedRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
