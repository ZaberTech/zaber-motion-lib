// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_on_fire_set_to_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerOnFireSetToSettingRequest::TriggerOnFireSetToSettingRequest() { }

TriggerOnFireSetToSettingRequest::TriggerOnFireSetToSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    ascii::TriggerAction p_action,
    int p_axis,
    std::string p_setting,
    ascii::TriggerOperation p_operation,
    int p_fromAxis,
    std::string p_fromSetting
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    action(std::move(p_action)),
    axis(p_axis),
    setting(std::move(p_setting)),
    operation(std::move(p_operation)),
    fromAxis(p_fromAxis),
    fromSetting(std::move(p_fromSetting))
{ }

bool TriggerOnFireSetToSettingRequest::operator==(const TriggerOnFireSetToSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    action,
    axis,
    setting,
    operation,
    fromAxis,
    fromSetting
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.action,
    other.axis,
    other.setting,
    other.operation,
    other.fromAxis,
    other.fromSetting
    );
}

int TriggerOnFireSetToSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerOnFireSetToSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerOnFireSetToSettingRequest::getDevice() const {
    return device;
}
void TriggerOnFireSetToSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerOnFireSetToSettingRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerOnFireSetToSettingRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

ascii::TriggerAction const& TriggerOnFireSetToSettingRequest::getAction() const {
    return action;
}
void TriggerOnFireSetToSettingRequest::setAction(ascii::TriggerAction p_action) {
    action = std::move(p_action);
}

int TriggerOnFireSetToSettingRequest::getAxis() const {
    return axis;
}
void TriggerOnFireSetToSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& TriggerOnFireSetToSettingRequest::getSetting() const {
    return setting;
}
void TriggerOnFireSetToSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

ascii::TriggerOperation const& TriggerOnFireSetToSettingRequest::getOperation() const {
    return operation;
}
void TriggerOnFireSetToSettingRequest::setOperation(ascii::TriggerOperation p_operation) {
    operation = std::move(p_operation);
}

int TriggerOnFireSetToSettingRequest::getFromAxis() const {
    return fromAxis;
}
void TriggerOnFireSetToSettingRequest::setFromAxis(int p_fromAxis) {
    fromAxis = p_fromAxis;
}

std::string const& TriggerOnFireSetToSettingRequest::getFromSetting() const {
    return fromSetting;
}
void TriggerOnFireSetToSettingRequest::setFromSetting(std::string p_fromSetting) {
    fromSetting = std::move(p_fromSetting);
}

std::string TriggerOnFireSetToSettingRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerOnFireSetToSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "action: ";
    ss << TriggerAction_toString(this->action);
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "operation: ";
    ss << TriggerOperation_toString(this->operation);
    ss << ", ";
    ss << "fromAxis: ";
    ss << this->fromAxis;
    ss << ", ";
    ss << "fromSetting: ";
    ss << this->fromSetting;
    ss << " }";
    return ss.str();
}

void TriggerOnFireSetToSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerOnFireSetToSettingRequest>();
}

std::string TriggerOnFireSetToSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
