// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/autofocus_get_objective_params_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AutofocusGetObjectiveParamsResponse::AutofocusGetObjectiveParamsResponse() { }

AutofocusGetObjectiveParamsResponse::AutofocusGetObjectiveParamsResponse(
    std::vector<NamedParameter> p_parameters
) :
    parameters(std::move(p_parameters))
{ }

bool AutofocusGetObjectiveParamsResponse::operator==(const AutofocusGetObjectiveParamsResponse& other) const {
    return std::tie(
    parameters
    ) == std::tie(
    other.parameters
    );
}

std::vector<NamedParameter> const& AutofocusGetObjectiveParamsResponse::getParameters() const {
    return parameters;
}
void AutofocusGetObjectiveParamsResponse::setParameters(std::vector<NamedParameter> p_parameters) {
    parameters = std::move(p_parameters);
}

std::string AutofocusGetObjectiveParamsResponse::toString() const {
    std::stringstream ss;
    ss << "AutofocusGetObjectiveParamsResponse { ";
    ss << "parameters: ";
    ss << "[ ";
    for (size_t i = 0; i < this->parameters.size(); i++) {
        ss << this->parameters[i].toString();
        if (i < this->parameters.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void AutofocusGetObjectiveParamsResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusGetObjectiveParamsResponse>();
}

std::string AutofocusGetObjectiveParamsResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
