// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_all_analog_outputs_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAllAnalogOutputsRequest::StreamSetAllAnalogOutputsRequest() { }

StreamSetAllAnalogOutputsRequest::StreamSetAllAnalogOutputsRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<double> p_values
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    values(std::move(p_values))
{ }

bool StreamSetAllAnalogOutputsRequest::operator==(const StreamSetAllAnalogOutputsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    values
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.values
    );
}

int StreamSetAllAnalogOutputsRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAllAnalogOutputsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAllAnalogOutputsRequest::getDevice() const {
    return device;
}
void StreamSetAllAnalogOutputsRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAllAnalogOutputsRequest::getStreamId() const {
    return streamId;
}
void StreamSetAllAnalogOutputsRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAllAnalogOutputsRequest::getPvt() const {
    return pvt;
}
void StreamSetAllAnalogOutputsRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<double> const& StreamSetAllAnalogOutputsRequest::getValues() const {
    return values;
}
void StreamSetAllAnalogOutputsRequest::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::string StreamSetAllAnalogOutputsRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAllAnalogOutputsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetAllAnalogOutputsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAllAnalogOutputsRequest>();
}

std::string StreamSetAllAnalogOutputsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
