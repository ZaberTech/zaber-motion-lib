// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_data_get_samples_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeDataGetSamplesResponse::OscilloscopeDataGetSamplesResponse() { }

OscilloscopeDataGetSamplesResponse::OscilloscopeDataGetSamplesResponse(
    std::vector<double> p_data
) :
    data(std::move(p_data))
{ }

bool OscilloscopeDataGetSamplesResponse::operator==(const OscilloscopeDataGetSamplesResponse& other) const {
    return std::tie(
    data
    ) == std::tie(
    other.data
    );
}

std::vector<double> const& OscilloscopeDataGetSamplesResponse::getData() const {
    return data;
}
void OscilloscopeDataGetSamplesResponse::setData(std::vector<double> p_data) {
    data = std::move(p_data);
}

std::string OscilloscopeDataGetSamplesResponse::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeDataGetSamplesResponse { ";
    ss << "data: ";
    ss << "[ ";
    for (size_t i = 0; i < this->data.size(); i++) {
        ss << this->data[i];
        if (i < this->data.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void OscilloscopeDataGetSamplesResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeDataGetSamplesResponse>();
}

std::string OscilloscopeDataGetSamplesResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
