// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axes_move_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AxesMoveRequest::AxesMoveRequest() { }

AxesMoveRequest::AxesMoveRequest(
    std::vector<int> p_interfaces,
    std::vector<int> p_devices,
    std::vector<int> p_axes,
    std::vector<Measurement> p_position
) :
    interfaces(std::move(p_interfaces)),
    devices(std::move(p_devices)),
    axes(std::move(p_axes)),
    position(std::move(p_position))
{ }

bool AxesMoveRequest::operator==(const AxesMoveRequest& other) const {
    return std::tie(
    interfaces,
    devices,
    axes,
    position
    ) == std::tie(
    other.interfaces,
    other.devices,
    other.axes,
    other.position
    );
}

std::vector<int> const& AxesMoveRequest::getInterfaces() const {
    return interfaces;
}
void AxesMoveRequest::setInterfaces(std::vector<int> p_interfaces) {
    interfaces = std::move(p_interfaces);
}

std::vector<int> const& AxesMoveRequest::getDevices() const {
    return devices;
}
void AxesMoveRequest::setDevices(std::vector<int> p_devices) {
    devices = std::move(p_devices);
}

std::vector<int> const& AxesMoveRequest::getAxes() const {
    return axes;
}
void AxesMoveRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::vector<Measurement> const& AxesMoveRequest::getPosition() const {
    return position;
}
void AxesMoveRequest::setPosition(std::vector<Measurement> p_position) {
    position = std::move(p_position);
}

std::string AxesMoveRequest::toString() const {
    std::stringstream ss;
    ss << "AxesMoveRequest { ";
    ss << "interfaces: ";
    ss << "[ ";
    for (size_t i = 0; i < this->interfaces.size(); i++) {
        ss << this->interfaces[i];
        if (i < this->interfaces.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "devices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->devices.size(); i++) {
        ss << this->devices[i];
        if (i < this->devices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "position: ";
    ss << "[ ";
    for (size_t i = 0; i < this->position.size(); i++) {
        ss << this->position[i].toString();
        if (i < this->position.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void AxesMoveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxesMoveRequest>();
}

std::string AxesMoveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
