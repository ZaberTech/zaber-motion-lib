// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/process_on.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ProcessOn::ProcessOn() { }

ProcessOn::ProcessOn(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_on,
    double p_duration,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    on(p_on),
    duration(p_duration),
    unit(p_unit)
{ }

bool ProcessOn::operator==(const ProcessOn& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    on,
    duration,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.on,
    other.duration,
    other.unit
    );
}

int ProcessOn::getInterfaceId() const {
    return interfaceId;
}
void ProcessOn::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ProcessOn::getDevice() const {
    return device;
}
void ProcessOn::setDevice(int p_device) {
    device = p_device;
}

int ProcessOn::getAxis() const {
    return axis;
}
void ProcessOn::setAxis(int p_axis) {
    axis = p_axis;
}

bool ProcessOn::getOn() const {
    return on;
}
void ProcessOn::setOn(bool p_on) {
    on = p_on;
}

double ProcessOn::getDuration() const {
    return duration;
}
void ProcessOn::setDuration(double p_duration) {
    duration = p_duration;
}

Units ProcessOn::getUnit() const {
    return unit;
}
void ProcessOn::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string ProcessOn::toString() const {
    std::stringstream ss;
    ss << "ProcessOn { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "on: ";
    ss << this->on;
    ss << ", ";
    ss << "duration: ";
    ss << this->duration;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void ProcessOn::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ProcessOn>();
}

std::string ProcessOn::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
