// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/pvt_point_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PvtPointRequest, interfaceId, device, streamId, pvt, type, positions, velocities, time)

} // namespace requests
} // namespace motion
} // namespace zaber
