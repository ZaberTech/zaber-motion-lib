// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/autofocus_get_status_response.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/microscopy/autofocus_status.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AutofocusGetStatusResponse, status)

} // namespace requests
} // namespace motion
} // namespace zaber
