// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_state_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetStateRequest::SetStateRequest() { }

SetStateRequest::SetStateRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_state,
    bool p_deviceOnly
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    state(std::move(p_state)),
    deviceOnly(p_deviceOnly)
{ }

bool SetStateRequest::operator==(const SetStateRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    state,
    deviceOnly
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.state,
    other.deviceOnly
    );
}

int SetStateRequest::getInterfaceId() const {
    return interfaceId;
}
void SetStateRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetStateRequest::getDevice() const {
    return device;
}
void SetStateRequest::setDevice(int p_device) {
    device = p_device;
}

int SetStateRequest::getAxis() const {
    return axis;
}
void SetStateRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& SetStateRequest::getState() const {
    return state;
}
void SetStateRequest::setState(std::string p_state) {
    state = std::move(p_state);
}

bool SetStateRequest::getDeviceOnly() const {
    return deviceOnly;
}
void SetStateRequest::setDeviceOnly(bool p_deviceOnly) {
    deviceOnly = p_deviceOnly;
}

std::string SetStateRequest::toString() const {
    std::stringstream ss;
    ss << "SetStateRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "state: ";
    ss << this->state;
    ss << ", ";
    ss << "deviceOnly: ";
    ss << this->deviceOnly;
    ss << " }";
    return ss.str();
}

void SetStateRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetStateRequest>();
}

std::string SetStateRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
