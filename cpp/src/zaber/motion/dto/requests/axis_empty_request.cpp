// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axis_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AxisEmptyRequest::AxisEmptyRequest() { }

AxisEmptyRequest::AxisEmptyRequest(
    int p_interfaceId,
    int p_device,
    int p_axis
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis)
{ }

bool AxisEmptyRequest::operator==(const AxisEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis
    );
}

int AxisEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void AxisEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int AxisEmptyRequest::getDevice() const {
    return device;
}
void AxisEmptyRequest::setDevice(int p_device) {
    device = p_device;
}

int AxisEmptyRequest::getAxis() const {
    return axis;
}
void AxisEmptyRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string AxisEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "AxisEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << " }";
    return ss.str();
}

void AxisEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisEmptyRequest>();
}

std::string AxisEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
