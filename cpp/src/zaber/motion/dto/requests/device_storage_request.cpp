// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_storage_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceStorageRequest::DeviceStorageRequest() { }

DeviceStorageRequest::DeviceStorageRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key))
{ }

bool DeviceStorageRequest::operator==(const DeviceStorageRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key
    );
}

int DeviceStorageRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceStorageRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceStorageRequest::getDevice() const {
    return device;
}
void DeviceStorageRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceStorageRequest::getAxis() const {
    return axis;
}
void DeviceStorageRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceStorageRequest::getKey() const {
    return key;
}
void DeviceStorageRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

std::string DeviceStorageRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceStorageRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << " }";
    return ss.str();
}

void DeviceStorageRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceStorageRequest>();
}

std::string DeviceStorageRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
