// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_fire_at_interval_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerFireAtIntervalRequest::TriggerFireAtIntervalRequest() { }

TriggerFireAtIntervalRequest::TriggerFireAtIntervalRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    double p_interval,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    interval(p_interval),
    unit(p_unit)
{ }

bool TriggerFireAtIntervalRequest::operator==(const TriggerFireAtIntervalRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    interval,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.interval,
    other.unit
    );
}

int TriggerFireAtIntervalRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerFireAtIntervalRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerFireAtIntervalRequest::getDevice() const {
    return device;
}
void TriggerFireAtIntervalRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerFireAtIntervalRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerFireAtIntervalRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

double TriggerFireAtIntervalRequest::getInterval() const {
    return interval;
}
void TriggerFireAtIntervalRequest::setInterval(double p_interval) {
    interval = p_interval;
}

Units TriggerFireAtIntervalRequest::getUnit() const {
    return unit;
}
void TriggerFireAtIntervalRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TriggerFireAtIntervalRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerFireAtIntervalRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "interval: ";
    ss << this->interval;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TriggerFireAtIntervalRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerFireAtIntervalRequest>();
}

std::string TriggerFireAtIntervalRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
