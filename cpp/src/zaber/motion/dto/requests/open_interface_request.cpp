// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/open_interface_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OpenInterfaceRequest::OpenInterfaceRequest() { }

OpenInterfaceRequest::OpenInterfaceRequest(
    InterfaceType p_interfaceType,
    std::string p_portName,
    int p_baudRate,
    std::string p_hostName,
    int p_port,
    int p_transport,
    bool p_rejectRoutedConnection,
    std::string p_cloudId,
    std::optional<std::string> p_connectionName,
    std::optional<std::string> p_realm,
    std::string p_token,
    std::string p_api
) :
    interfaceType(std::move(p_interfaceType)),
    portName(std::move(p_portName)),
    baudRate(p_baudRate),
    hostName(std::move(p_hostName)),
    port(p_port),
    transport(p_transport),
    rejectRoutedConnection(p_rejectRoutedConnection),
    cloudId(std::move(p_cloudId)),
    connectionName(std::move(p_connectionName)),
    realm(std::move(p_realm)),
    token(std::move(p_token)),
    api(std::move(p_api))
{ }

OpenInterfaceRequest::OpenInterfaceRequest(
    InterfaceType p_interfaceType,
    std::string p_portName,
    int p_baudRate,
    std::string p_hostName,
    int p_port,
    int p_transport,
    bool p_rejectRoutedConnection,
    std::string p_cloudId,
    std::string p_token,
    std::string p_api
) :
    interfaceType(std::move(p_interfaceType)),
    portName(std::move(p_portName)),
    baudRate(p_baudRate),
    hostName(std::move(p_hostName)),
    port(p_port),
    transport(p_transport),
    rejectRoutedConnection(p_rejectRoutedConnection),
    cloudId(std::move(p_cloudId)),
    token(std::move(p_token)),
    api(std::move(p_api))
{ }

bool OpenInterfaceRequest::operator==(const OpenInterfaceRequest& other) const {
    return std::tie(
    interfaceType,
    portName,
    baudRate,
    hostName,
    port,
    transport,
    rejectRoutedConnection,
    cloudId,
    connectionName,
    realm,
    token,
    api
    ) == std::tie(
    other.interfaceType,
    other.portName,
    other.baudRate,
    other.hostName,
    other.port,
    other.transport,
    other.rejectRoutedConnection,
    other.cloudId,
    other.connectionName,
    other.realm,
    other.token,
    other.api
    );
}

InterfaceType const& OpenInterfaceRequest::getInterfaceType() const {
    return interfaceType;
}
void OpenInterfaceRequest::setInterfaceType(InterfaceType p_interfaceType) {
    interfaceType = std::move(p_interfaceType);
}

std::string const& OpenInterfaceRequest::getPortName() const {
    return portName;
}
void OpenInterfaceRequest::setPortName(std::string p_portName) {
    portName = std::move(p_portName);
}

int OpenInterfaceRequest::getBaudRate() const {
    return baudRate;
}
void OpenInterfaceRequest::setBaudRate(int p_baudRate) {
    baudRate = p_baudRate;
}

std::string const& OpenInterfaceRequest::getHostName() const {
    return hostName;
}
void OpenInterfaceRequest::setHostName(std::string p_hostName) {
    hostName = std::move(p_hostName);
}

int OpenInterfaceRequest::getPort() const {
    return port;
}
void OpenInterfaceRequest::setPort(int p_port) {
    port = p_port;
}

int OpenInterfaceRequest::getTransport() const {
    return transport;
}
void OpenInterfaceRequest::setTransport(int p_transport) {
    transport = p_transport;
}

bool OpenInterfaceRequest::getRejectRoutedConnection() const {
    return rejectRoutedConnection;
}
void OpenInterfaceRequest::setRejectRoutedConnection(bool p_rejectRoutedConnection) {
    rejectRoutedConnection = p_rejectRoutedConnection;
}

std::string const& OpenInterfaceRequest::getCloudId() const {
    return cloudId;
}
void OpenInterfaceRequest::setCloudId(std::string p_cloudId) {
    cloudId = std::move(p_cloudId);
}

std::optional<std::string> const& OpenInterfaceRequest::getConnectionName() const {
    return connectionName;
}
void OpenInterfaceRequest::setConnectionName(std::optional<std::string> p_connectionName) {
    connectionName = std::move(p_connectionName);
}

std::optional<std::string> const& OpenInterfaceRequest::getRealm() const {
    return realm;
}
void OpenInterfaceRequest::setRealm(std::optional<std::string> p_realm) {
    realm = std::move(p_realm);
}

std::string const& OpenInterfaceRequest::getToken() const {
    return token;
}
void OpenInterfaceRequest::setToken(std::string p_token) {
    token = std::move(p_token);
}

std::string const& OpenInterfaceRequest::getApi() const {
    return api;
}
void OpenInterfaceRequest::setApi(std::string p_api) {
    api = std::move(p_api);
}

std::string OpenInterfaceRequest::toString() const {
    std::stringstream ss;
    ss << "OpenInterfaceRequest { ";
    ss << "interfaceType: ";
    ss << InterfaceType_toString(this->interfaceType);
    ss << ", ";
    ss << "portName: ";
    ss << this->portName;
    ss << ", ";
    ss << "baudRate: ";
    ss << this->baudRate;
    ss << ", ";
    ss << "hostName: ";
    ss << this->hostName;
    ss << ", ";
    ss << "port: ";
    ss << this->port;
    ss << ", ";
    ss << "transport: ";
    ss << this->transport;
    ss << ", ";
    ss << "rejectRoutedConnection: ";
    ss << this->rejectRoutedConnection;
    ss << ", ";
    ss << "cloudId: ";
    ss << this->cloudId;
    ss << ", ";
    ss << "connectionName: ";
    if (this->connectionName.has_value()) {
        ss << this->connectionName.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "realm: ";
    if (this->realm.has_value()) {
        ss << this->realm.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "token: ";
    ss << this->token;
    ss << ", ";
    ss << "api: ";
    ss << this->api;
    ss << " }";
    return ss.str();
}

void OpenInterfaceRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OpenInterfaceRequest>();
}

std::string OpenInterfaceRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
