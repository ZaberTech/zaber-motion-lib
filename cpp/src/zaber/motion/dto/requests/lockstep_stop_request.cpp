// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_stop_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepStopRequest::LockstepStopRequest() { }

LockstepStopRequest::LockstepStopRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool LockstepStopRequest::operator==(const LockstepStopRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.waitUntilIdle
    );
}

int LockstepStopRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepStopRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepStopRequest::getDevice() const {
    return device;
}
void LockstepStopRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepStopRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepStopRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

bool LockstepStopRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void LockstepStopRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string LockstepStopRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepStopRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void LockstepStopRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepStopRequest>();
}

std::string LockstepStopRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
