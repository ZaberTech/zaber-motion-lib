// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_start_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeStartRequest::OscilloscopeStartRequest() { }

OscilloscopeStartRequest::OscilloscopeStartRequest(
    int p_interfaceId,
    int p_device,
    int p_captureLength
) :
    interfaceId(p_interfaceId),
    device(p_device),
    captureLength(p_captureLength)
{ }

bool OscilloscopeStartRequest::operator==(const OscilloscopeStartRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    captureLength
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.captureLength
    );
}

int OscilloscopeStartRequest::getInterfaceId() const {
    return interfaceId;
}
void OscilloscopeStartRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int OscilloscopeStartRequest::getDevice() const {
    return device;
}
void OscilloscopeStartRequest::setDevice(int p_device) {
    device = p_device;
}

int OscilloscopeStartRequest::getCaptureLength() const {
    return captureLength;
}
void OscilloscopeStartRequest::setCaptureLength(int p_captureLength) {
    captureLength = p_captureLength;
}

std::string OscilloscopeStartRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeStartRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "captureLength: ";
    ss << this->captureLength;
    ss << " }";
    return ss.str();
}

void OscilloscopeStartRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeStartRequest>();
}

std::string OscilloscopeStartRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
