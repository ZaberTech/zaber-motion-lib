// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_io_port_label_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetIoPortLabelRequest::SetIoPortLabelRequest() { }

SetIoPortLabelRequest::SetIoPortLabelRequest(
    int p_interfaceId,
    int p_device,
    ascii::IoPortType p_portType,
    int p_channelNumber,
    std::optional<std::string> p_label
) :
    interfaceId(p_interfaceId),
    device(p_device),
    portType(std::move(p_portType)),
    channelNumber(p_channelNumber),
    label(std::move(p_label))
{ }

SetIoPortLabelRequest::SetIoPortLabelRequest(
    int p_interfaceId,
    int p_device,
    ascii::IoPortType p_portType,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    portType(std::move(p_portType)),
    channelNumber(p_channelNumber)
{ }

bool SetIoPortLabelRequest::operator==(const SetIoPortLabelRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    portType,
    channelNumber,
    label
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.portType,
    other.channelNumber,
    other.label
    );
}

int SetIoPortLabelRequest::getInterfaceId() const {
    return interfaceId;
}
void SetIoPortLabelRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetIoPortLabelRequest::getDevice() const {
    return device;
}
void SetIoPortLabelRequest::setDevice(int p_device) {
    device = p_device;
}

ascii::IoPortType const& SetIoPortLabelRequest::getPortType() const {
    return portType;
}
void SetIoPortLabelRequest::setPortType(ascii::IoPortType p_portType) {
    portType = std::move(p_portType);
}

int SetIoPortLabelRequest::getChannelNumber() const {
    return channelNumber;
}
void SetIoPortLabelRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::optional<std::string> const& SetIoPortLabelRequest::getLabel() const {
    return label;
}
void SetIoPortLabelRequest::setLabel(std::optional<std::string> p_label) {
    label = std::move(p_label);
}

std::string SetIoPortLabelRequest::toString() const {
    std::stringstream ss;
    ss << "SetIoPortLabelRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "portType: ";
    ss << IoPortType_toString(this->portType);
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "label: ";
    if (this->label.has_value()) {
        ss << this->label.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void SetIoPortLabelRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetIoPortLabelRequest>();
}

std::string SetIoPortLabelRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
