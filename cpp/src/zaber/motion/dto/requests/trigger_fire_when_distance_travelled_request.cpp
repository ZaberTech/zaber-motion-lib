// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_fire_when_distance_travelled_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerFireWhenDistanceTravelledRequest::TriggerFireWhenDistanceTravelledRequest() { }

TriggerFireWhenDistanceTravelledRequest::TriggerFireWhenDistanceTravelledRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    int p_axis,
    double p_distance,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    axis(p_axis),
    distance(p_distance),
    unit(p_unit)
{ }

bool TriggerFireWhenDistanceTravelledRequest::operator==(const TriggerFireWhenDistanceTravelledRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    axis,
    distance,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.axis,
    other.distance,
    other.unit
    );
}

int TriggerFireWhenDistanceTravelledRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerFireWhenDistanceTravelledRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerFireWhenDistanceTravelledRequest::getDevice() const {
    return device;
}
void TriggerFireWhenDistanceTravelledRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerFireWhenDistanceTravelledRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerFireWhenDistanceTravelledRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

int TriggerFireWhenDistanceTravelledRequest::getAxis() const {
    return axis;
}
void TriggerFireWhenDistanceTravelledRequest::setAxis(int p_axis) {
    axis = p_axis;
}

double TriggerFireWhenDistanceTravelledRequest::getDistance() const {
    return distance;
}
void TriggerFireWhenDistanceTravelledRequest::setDistance(double p_distance) {
    distance = p_distance;
}

Units TriggerFireWhenDistanceTravelledRequest::getUnit() const {
    return unit;
}
void TriggerFireWhenDistanceTravelledRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TriggerFireWhenDistanceTravelledRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerFireWhenDistanceTravelledRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "distance: ";
    ss << this->distance;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TriggerFireWhenDistanceTravelledRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerFireWhenDistanceTravelledRequest>();
}

std::string TriggerFireWhenDistanceTravelledRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
