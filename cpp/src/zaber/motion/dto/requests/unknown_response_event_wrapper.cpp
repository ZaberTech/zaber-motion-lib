// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unknown_response_event_wrapper.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnknownResponseEventWrapper::UnknownResponseEventWrapper() { }

UnknownResponseEventWrapper::UnknownResponseEventWrapper(
    int p_interfaceId,
    ascii::UnknownResponseEvent p_unknownResponse
) :
    interfaceId(p_interfaceId),
    unknownResponse(std::move(p_unknownResponse))
{ }

bool UnknownResponseEventWrapper::operator==(const UnknownResponseEventWrapper& other) const {
    return std::tie(
    interfaceId,
    unknownResponse
    ) == std::tie(
    other.interfaceId,
    other.unknownResponse
    );
}

int UnknownResponseEventWrapper::getInterfaceId() const {
    return interfaceId;
}
void UnknownResponseEventWrapper::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

ascii::UnknownResponseEvent const& UnknownResponseEventWrapper::getUnknownResponse() const {
    return unknownResponse;
}
void UnknownResponseEventWrapper::setUnknownResponse(ascii::UnknownResponseEvent p_unknownResponse) {
    unknownResponse = std::move(p_unknownResponse);
}

std::string UnknownResponseEventWrapper::toString() const {
    std::stringstream ss;
    ss << "UnknownResponseEventWrapper { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "unknownResponse: ";
    ss << this->unknownResponse.toString();
    ss << " }";
    return ss.str();
}

void UnknownResponseEventWrapper::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnknownResponseEventWrapper>();
}

std::string UnknownResponseEventWrapper::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
