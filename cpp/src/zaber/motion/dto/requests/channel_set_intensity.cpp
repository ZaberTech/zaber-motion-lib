// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/channel_set_intensity.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ChannelSetIntensity::ChannelSetIntensity() { }

ChannelSetIntensity::ChannelSetIntensity(
    int p_interfaceId,
    int p_device,
    int p_axis,
    double p_intensity
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    intensity(p_intensity)
{ }

bool ChannelSetIntensity::operator==(const ChannelSetIntensity& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    intensity
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.intensity
    );
}

int ChannelSetIntensity::getInterfaceId() const {
    return interfaceId;
}
void ChannelSetIntensity::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ChannelSetIntensity::getDevice() const {
    return device;
}
void ChannelSetIntensity::setDevice(int p_device) {
    device = p_device;
}

int ChannelSetIntensity::getAxis() const {
    return axis;
}
void ChannelSetIntensity::setAxis(int p_axis) {
    axis = p_axis;
}

double ChannelSetIntensity::getIntensity() const {
    return intensity;
}
void ChannelSetIntensity::setIntensity(double p_intensity) {
    intensity = p_intensity;
}

std::string ChannelSetIntensity::toString() const {
    std::stringstream ss;
    ss << "ChannelSetIntensity { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "intensity: ";
    ss << this->intensity;
    ss << " }";
    return ss.str();
}

void ChannelSetIntensity::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ChannelSetIntensity>();
}

std::string ChannelSetIntensity::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
