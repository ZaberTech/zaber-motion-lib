// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_move_sin_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepMoveSinRequest::LockstepMoveSinRequest() { }

LockstepMoveSinRequest::LockstepMoveSinRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId,
    double p_amplitude,
    Units p_amplitudeUnits,
    double p_period,
    Units p_periodUnits,
    double p_count,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId),
    amplitude(p_amplitude),
    amplitudeUnits(p_amplitudeUnits),
    period(p_period),
    periodUnits(p_periodUnits),
    count(p_count),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool LockstepMoveSinRequest::operator==(const LockstepMoveSinRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId,
    amplitude,
    amplitudeUnits,
    period,
    periodUnits,
    count,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId,
    other.amplitude,
    other.amplitudeUnits,
    other.period,
    other.periodUnits,
    other.count,
    other.waitUntilIdle
    );
}

int LockstepMoveSinRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepMoveSinRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepMoveSinRequest::getDevice() const {
    return device;
}
void LockstepMoveSinRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepMoveSinRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepMoveSinRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

double LockstepMoveSinRequest::getAmplitude() const {
    return amplitude;
}
void LockstepMoveSinRequest::setAmplitude(double p_amplitude) {
    amplitude = p_amplitude;
}

Units LockstepMoveSinRequest::getAmplitudeUnits() const {
    return amplitudeUnits;
}
void LockstepMoveSinRequest::setAmplitudeUnits(Units p_amplitudeUnits) {
    amplitudeUnits = p_amplitudeUnits;
}

double LockstepMoveSinRequest::getPeriod() const {
    return period;
}
void LockstepMoveSinRequest::setPeriod(double p_period) {
    period = p_period;
}

Units LockstepMoveSinRequest::getPeriodUnits() const {
    return periodUnits;
}
void LockstepMoveSinRequest::setPeriodUnits(Units p_periodUnits) {
    periodUnits = p_periodUnits;
}

double LockstepMoveSinRequest::getCount() const {
    return count;
}
void LockstepMoveSinRequest::setCount(double p_count) {
    count = p_count;
}

bool LockstepMoveSinRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void LockstepMoveSinRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string LockstepMoveSinRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepMoveSinRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << ", ";
    ss << "amplitude: ";
    ss << this->amplitude;
    ss << ", ";
    ss << "amplitudeUnits: ";
    ss << getUnitLongName(this->amplitudeUnits);
    ss << ", ";
    ss << "period: ";
    ss << this->period;
    ss << ", ";
    ss << "periodUnits: ";
    ss << getUnitLongName(this->periodUnits);
    ss << ", ";
    ss << "count: ";
    ss << this->count;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void LockstepMoveSinRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepMoveSinRequest>();
}

std::string LockstepMoveSinRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
