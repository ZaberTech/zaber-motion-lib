// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/toggle_device_db_store_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ToggleDeviceDbStoreRequest::ToggleDeviceDbStoreRequest() { }

ToggleDeviceDbStoreRequest::ToggleDeviceDbStoreRequest(
    bool p_toggleOn,
    std::optional<std::string> p_storeLocation
) :
    toggleOn(p_toggleOn),
    storeLocation(std::move(p_storeLocation))
{ }

ToggleDeviceDbStoreRequest::ToggleDeviceDbStoreRequest(
    bool p_toggleOn
) :
    toggleOn(p_toggleOn)
{ }

bool ToggleDeviceDbStoreRequest::operator==(const ToggleDeviceDbStoreRequest& other) const {
    return std::tie(
    toggleOn,
    storeLocation
    ) == std::tie(
    other.toggleOn,
    other.storeLocation
    );
}

bool ToggleDeviceDbStoreRequest::getToggleOn() const {
    return toggleOn;
}
void ToggleDeviceDbStoreRequest::setToggleOn(bool p_toggleOn) {
    toggleOn = p_toggleOn;
}

std::optional<std::string> const& ToggleDeviceDbStoreRequest::getStoreLocation() const {
    return storeLocation;
}
void ToggleDeviceDbStoreRequest::setStoreLocation(std::optional<std::string> p_storeLocation) {
    storeLocation = std::move(p_storeLocation);
}

std::string ToggleDeviceDbStoreRequest::toString() const {
    std::stringstream ss;
    ss << "ToggleDeviceDbStoreRequest { ";
    ss << "toggleOn: ";
    ss << this->toggleOn;
    ss << ", ";
    ss << "storeLocation: ";
    if (this->storeLocation.has_value()) {
        ss << this->storeLocation.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void ToggleDeviceDbStoreRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ToggleDeviceDbStoreRequest>();
}

std::string ToggleDeviceDbStoreRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
