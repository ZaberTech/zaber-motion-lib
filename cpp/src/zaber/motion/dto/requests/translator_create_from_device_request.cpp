// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_create_from_device_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorCreateFromDeviceRequest::TranslatorCreateFromDeviceRequest() { }

TranslatorCreateFromDeviceRequest::TranslatorCreateFromDeviceRequest(
    int p_interfaceId,
    int p_device,
    std::vector<int> p_axes,
    std::optional<gcode::TranslatorConfig> p_config
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axes(std::move(p_axes)),
    config(std::move(p_config))
{ }

TranslatorCreateFromDeviceRequest::TranslatorCreateFromDeviceRequest(
    int p_interfaceId,
    int p_device,
    std::vector<int> p_axes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axes(std::move(p_axes))
{ }

bool TranslatorCreateFromDeviceRequest::operator==(const TranslatorCreateFromDeviceRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axes,
    config
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axes,
    other.config
    );
}

int TranslatorCreateFromDeviceRequest::getInterfaceId() const {
    return interfaceId;
}
void TranslatorCreateFromDeviceRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TranslatorCreateFromDeviceRequest::getDevice() const {
    return device;
}
void TranslatorCreateFromDeviceRequest::setDevice(int p_device) {
    device = p_device;
}

std::vector<int> const& TranslatorCreateFromDeviceRequest::getAxes() const {
    return axes;
}
void TranslatorCreateFromDeviceRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::optional<gcode::TranslatorConfig> const& TranslatorCreateFromDeviceRequest::getConfig() const {
    return config;
}
void TranslatorCreateFromDeviceRequest::setConfig(std::optional<gcode::TranslatorConfig> p_config) {
    config = std::move(p_config);
}

std::string TranslatorCreateFromDeviceRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorCreateFromDeviceRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "config: ";
    if (this->config.has_value()) {
        ss << this->config.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void TranslatorCreateFromDeviceRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorCreateFromDeviceRequest>();
}

std::string TranslatorCreateFromDeviceRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
