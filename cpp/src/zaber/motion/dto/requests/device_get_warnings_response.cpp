// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_warnings_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetWarningsResponse::DeviceGetWarningsResponse() { }

DeviceGetWarningsResponse::DeviceGetWarningsResponse(
    std::vector<std::string> p_flags
) :
    flags(std::move(p_flags))
{ }

bool DeviceGetWarningsResponse::operator==(const DeviceGetWarningsResponse& other) const {
    return std::tie(
    flags
    ) == std::tie(
    other.flags
    );
}

std::vector<std::string> const& DeviceGetWarningsResponse::getFlags() const {
    return flags;
}
void DeviceGetWarningsResponse::setFlags(std::vector<std::string> p_flags) {
    flags = std::move(p_flags);
}

std::string DeviceGetWarningsResponse::toString() const {
    std::stringstream ss;
    ss << "DeviceGetWarningsResponse { ";
    ss << "flags: ";
    ss << "[ ";
    for (size_t i = 0; i < this->flags.size(); i++) {
        ss << this->flags[i];
        if (i < this->flags.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceGetWarningsResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetWarningsResponse>();
}

std::string DeviceGetWarningsResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
