// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_digital_output_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetDigitalOutputRequest::StreamSetDigitalOutputRequest() { }

StreamSetDigitalOutputRequest::StreamSetDigitalOutputRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    ascii::DigitalOutputAction p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    value(std::move(p_value))
{ }

bool StreamSetDigitalOutputRequest::operator==(const StreamSetDigitalOutputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.value
    );
}

int StreamSetDigitalOutputRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetDigitalOutputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetDigitalOutputRequest::getDevice() const {
    return device;
}
void StreamSetDigitalOutputRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetDigitalOutputRequest::getStreamId() const {
    return streamId;
}
void StreamSetDigitalOutputRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetDigitalOutputRequest::getPvt() const {
    return pvt;
}
void StreamSetDigitalOutputRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetDigitalOutputRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamSetDigitalOutputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

ascii::DigitalOutputAction const& StreamSetDigitalOutputRequest::getValue() const {
    return value;
}
void StreamSetDigitalOutputRequest::setValue(ascii::DigitalOutputAction p_value) {
    value = std::move(p_value);
}

std::string StreamSetDigitalOutputRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetDigitalOutputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << DigitalOutputAction_toString(this->value);
    ss << " }";
    return ss.str();
}

void StreamSetDigitalOutputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetDigitalOutputRequest>();
}

std::string StreamSetDigitalOutputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
