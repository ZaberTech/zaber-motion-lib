// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_digital_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetDigitalOutputScheduleRequest::DeviceSetDigitalOutputScheduleRequest() { }

DeviceSetDigitalOutputScheduleRequest::DeviceSetDigitalOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    ascii::DigitalOutputAction p_value,
    ascii::DigitalOutputAction p_futureValue,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    value(std::move(p_value)),
    futureValue(std::move(p_futureValue)),
    delay(p_delay),
    unit(p_unit)
{ }

bool DeviceSetDigitalOutputScheduleRequest::operator==(const DeviceSetDigitalOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    value,
    futureValue,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.value,
    other.futureValue,
    other.delay,
    other.unit
    );
}

int DeviceSetDigitalOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetDigitalOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetDigitalOutputScheduleRequest::getDevice() const {
    return device;
}
void DeviceSetDigitalOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetDigitalOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceSetDigitalOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

ascii::DigitalOutputAction const& DeviceSetDigitalOutputScheduleRequest::getValue() const {
    return value;
}
void DeviceSetDigitalOutputScheduleRequest::setValue(ascii::DigitalOutputAction p_value) {
    value = std::move(p_value);
}

ascii::DigitalOutputAction const& DeviceSetDigitalOutputScheduleRequest::getFutureValue() const {
    return futureValue;
}
void DeviceSetDigitalOutputScheduleRequest::setFutureValue(ascii::DigitalOutputAction p_futureValue) {
    futureValue = std::move(p_futureValue);
}

double DeviceSetDigitalOutputScheduleRequest::getDelay() const {
    return delay;
}
void DeviceSetDigitalOutputScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units DeviceSetDigitalOutputScheduleRequest::getUnit() const {
    return unit;
}
void DeviceSetDigitalOutputScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetDigitalOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetDigitalOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << DigitalOutputAction_toString(this->value);
    ss << ", ";
    ss << "futureValue: ";
    ss << DigitalOutputAction_toString(this->futureValue);
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetDigitalOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetDigitalOutputScheduleRequest>();
}

std::string DeviceSetDigitalOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
