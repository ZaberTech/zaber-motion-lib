// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_get_axis_numbers_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepGetAxisNumbersResponse::LockstepGetAxisNumbersResponse() { }

LockstepGetAxisNumbersResponse::LockstepGetAxisNumbersResponse(
    std::vector<int> p_axes
) :
    axes(std::move(p_axes))
{ }

bool LockstepGetAxisNumbersResponse::operator==(const LockstepGetAxisNumbersResponse& other) const {
    return std::tie(
    axes
    ) == std::tie(
    other.axes
    );
}

std::vector<int> const& LockstepGetAxisNumbersResponse::getAxes() const {
    return axes;
}
void LockstepGetAxisNumbersResponse::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::string LockstepGetAxisNumbersResponse::toString() const {
    std::stringstream ss;
    ss << "LockstepGetAxisNumbersResponse { ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void LockstepGetAxisNumbersResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepGetAxisNumbersResponse>();
}

std::string LockstepGetAxisNumbersResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
