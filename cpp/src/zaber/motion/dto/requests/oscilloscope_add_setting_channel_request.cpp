// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_add_setting_channel_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeAddSettingChannelRequest::OscilloscopeAddSettingChannelRequest() { }

OscilloscopeAddSettingChannelRequest::OscilloscopeAddSettingChannelRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_setting
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    setting(std::move(p_setting))
{ }

bool OscilloscopeAddSettingChannelRequest::operator==(const OscilloscopeAddSettingChannelRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    setting
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.setting
    );
}

int OscilloscopeAddSettingChannelRequest::getInterfaceId() const {
    return interfaceId;
}
void OscilloscopeAddSettingChannelRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int OscilloscopeAddSettingChannelRequest::getDevice() const {
    return device;
}
void OscilloscopeAddSettingChannelRequest::setDevice(int p_device) {
    device = p_device;
}

int OscilloscopeAddSettingChannelRequest::getAxis() const {
    return axis;
}
void OscilloscopeAddSettingChannelRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& OscilloscopeAddSettingChannelRequest::getSetting() const {
    return setting;
}
void OscilloscopeAddSettingChannelRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

std::string OscilloscopeAddSettingChannelRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeAddSettingChannelRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << " }";
    return ss.str();
}

void OscilloscopeAddSettingChannelRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeAddSettingChannelRequest>();
}

std::string OscilloscopeAddSettingChannelRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
