// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_setup_live_composite_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetupLiveCompositeRequest::StreamSetupLiveCompositeRequest() { }

StreamSetupLiveCompositeRequest::StreamSetupLiveCompositeRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<ascii::StreamAxisDefinition> p_axes,
    std::vector<ascii::PvtAxisDefinition> p_pvtAxes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    axes(std::move(p_axes)),
    pvtAxes(std::move(p_pvtAxes))
{ }

bool StreamSetupLiveCompositeRequest::operator==(const StreamSetupLiveCompositeRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    axes,
    pvtAxes
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.axes,
    other.pvtAxes
    );
}

int StreamSetupLiveCompositeRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetupLiveCompositeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetupLiveCompositeRequest::getDevice() const {
    return device;
}
void StreamSetupLiveCompositeRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetupLiveCompositeRequest::getStreamId() const {
    return streamId;
}
void StreamSetupLiveCompositeRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetupLiveCompositeRequest::getPvt() const {
    return pvt;
}
void StreamSetupLiveCompositeRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<ascii::StreamAxisDefinition> const& StreamSetupLiveCompositeRequest::getAxes() const {
    return axes;
}
void StreamSetupLiveCompositeRequest::setAxes(std::vector<ascii::StreamAxisDefinition> p_axes) {
    axes = std::move(p_axes);
}

std::vector<ascii::PvtAxisDefinition> const& StreamSetupLiveCompositeRequest::getPvtAxes() const {
    return pvtAxes;
}
void StreamSetupLiveCompositeRequest::setPvtAxes(std::vector<ascii::PvtAxisDefinition> p_pvtAxes) {
    pvtAxes = std::move(p_pvtAxes);
}

std::string StreamSetupLiveCompositeRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetupLiveCompositeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i].toString();
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "pvtAxes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->pvtAxes.size(); i++) {
        ss << this->pvtAxes[i].toString();
        if (i < this->pvtAxes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetupLiveCompositeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetupLiveCompositeRequest>();
}

std::string StreamSetupLiveCompositeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
