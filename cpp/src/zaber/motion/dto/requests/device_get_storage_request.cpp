// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_storage_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetStorageRequest::DeviceGetStorageRequest() { }

DeviceGetStorageRequest::DeviceGetStorageRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key,
    bool p_decode
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key)),
    decode(p_decode)
{ }

bool DeviceGetStorageRequest::operator==(const DeviceGetStorageRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key,
    decode
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key,
    other.decode
    );
}

int DeviceGetStorageRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetStorageRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetStorageRequest::getDevice() const {
    return device;
}
void DeviceGetStorageRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceGetStorageRequest::getAxis() const {
    return axis;
}
void DeviceGetStorageRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceGetStorageRequest::getKey() const {
    return key;
}
void DeviceGetStorageRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

bool DeviceGetStorageRequest::getDecode() const {
    return decode;
}
void DeviceGetStorageRequest::setDecode(bool p_decode) {
    decode = p_decode;
}

std::string DeviceGetStorageRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetStorageRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << ", ";
    ss << "decode: ";
    ss << this->decode;
    ss << " }";
    return ss.str();
}

void DeviceGetStorageRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetStorageRequest>();
}

std::string DeviceGetStorageRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
