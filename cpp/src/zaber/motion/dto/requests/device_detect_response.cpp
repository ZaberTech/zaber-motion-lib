// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_detect_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceDetectResponse::DeviceDetectResponse() { }

DeviceDetectResponse::DeviceDetectResponse(
    std::vector<int> p_devices
) :
    devices(std::move(p_devices))
{ }

bool DeviceDetectResponse::operator==(const DeviceDetectResponse& other) const {
    return std::tie(
    devices
    ) == std::tie(
    other.devices
    );
}

std::vector<int> const& DeviceDetectResponse::getDevices() const {
    return devices;
}
void DeviceDetectResponse::setDevices(std::vector<int> p_devices) {
    devices = std::move(p_devices);
}

std::string DeviceDetectResponse::toString() const {
    std::stringstream ss;
    ss << "DeviceDetectResponse { ";
    ss << "devices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->devices.size(); i++) {
        ss << this->devices[i];
        if (i < this->devices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceDetectResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceDetectResponse>();
}

std::string DeviceDetectResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
