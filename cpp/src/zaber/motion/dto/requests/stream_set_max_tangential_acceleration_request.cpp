// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_max_tangential_acceleration_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetMaxTangentialAccelerationRequest::StreamSetMaxTangentialAccelerationRequest() { }

StreamSetMaxTangentialAccelerationRequest::StreamSetMaxTangentialAccelerationRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    double p_maxTangentialAcceleration,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    maxTangentialAcceleration(p_maxTangentialAcceleration),
    unit(p_unit)
{ }

bool StreamSetMaxTangentialAccelerationRequest::operator==(const StreamSetMaxTangentialAccelerationRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    maxTangentialAcceleration,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.maxTangentialAcceleration,
    other.unit
    );
}

int StreamSetMaxTangentialAccelerationRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetMaxTangentialAccelerationRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetMaxTangentialAccelerationRequest::getDevice() const {
    return device;
}
void StreamSetMaxTangentialAccelerationRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetMaxTangentialAccelerationRequest::getStreamId() const {
    return streamId;
}
void StreamSetMaxTangentialAccelerationRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetMaxTangentialAccelerationRequest::getPvt() const {
    return pvt;
}
void StreamSetMaxTangentialAccelerationRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

double StreamSetMaxTangentialAccelerationRequest::getMaxTangentialAcceleration() const {
    return maxTangentialAcceleration;
}
void StreamSetMaxTangentialAccelerationRequest::setMaxTangentialAcceleration(double p_maxTangentialAcceleration) {
    maxTangentialAcceleration = p_maxTangentialAcceleration;
}

Units StreamSetMaxTangentialAccelerationRequest::getUnit() const {
    return unit;
}
void StreamSetMaxTangentialAccelerationRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamSetMaxTangentialAccelerationRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetMaxTangentialAccelerationRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "maxTangentialAcceleration: ";
    ss << this->maxTangentialAcceleration;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamSetMaxTangentialAccelerationRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetMaxTangentialAccelerationRequest>();
}

std::string StreamSetMaxTangentialAccelerationRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
