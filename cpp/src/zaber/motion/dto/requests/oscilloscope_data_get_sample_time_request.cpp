// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_data_get_sample_time_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeDataGetSampleTimeRequest::OscilloscopeDataGetSampleTimeRequest() { }

OscilloscopeDataGetSampleTimeRequest::OscilloscopeDataGetSampleTimeRequest(
    int p_dataId,
    Units p_unit,
    int p_index
) :
    dataId(p_dataId),
    unit(p_unit),
    index(p_index)
{ }

bool OscilloscopeDataGetSampleTimeRequest::operator==(const OscilloscopeDataGetSampleTimeRequest& other) const {
    return std::tie(
    dataId,
    unit,
    index
    ) == std::tie(
    other.dataId,
    other.unit,
    other.index
    );
}

int OscilloscopeDataGetSampleTimeRequest::getDataId() const {
    return dataId;
}
void OscilloscopeDataGetSampleTimeRequest::setDataId(int p_dataId) {
    dataId = p_dataId;
}

Units OscilloscopeDataGetSampleTimeRequest::getUnit() const {
    return unit;
}
void OscilloscopeDataGetSampleTimeRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

int OscilloscopeDataGetSampleTimeRequest::getIndex() const {
    return index;
}
void OscilloscopeDataGetSampleTimeRequest::setIndex(int p_index) {
    index = p_index;
}

std::string OscilloscopeDataGetSampleTimeRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeDataGetSampleTimeRequest { ";
    ss << "dataId: ";
    ss << this->dataId;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << ", ";
    ss << "index: ";
    ss << this->index;
    ss << " }";
    return ss.str();
}

void OscilloscopeDataGetSampleTimeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeDataGetSampleTimeRequest>();
}

std::string OscilloscopeDataGetSampleTimeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
