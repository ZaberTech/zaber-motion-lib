// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/find_device_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

FindDeviceRequest::FindDeviceRequest() { }

FindDeviceRequest::FindDeviceRequest(
    int p_interfaceId,
    int p_deviceAddress
) :
    interfaceId(p_interfaceId),
    deviceAddress(p_deviceAddress)
{ }

bool FindDeviceRequest::operator==(const FindDeviceRequest& other) const {
    return std::tie(
    interfaceId,
    deviceAddress
    ) == std::tie(
    other.interfaceId,
    other.deviceAddress
    );
}

int FindDeviceRequest::getInterfaceId() const {
    return interfaceId;
}
void FindDeviceRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int FindDeviceRequest::getDeviceAddress() const {
    return deviceAddress;
}
void FindDeviceRequest::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

std::string FindDeviceRequest::toString() const {
    std::stringstream ss;
    ss << "FindDeviceRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << " }";
    return ss.str();
}

void FindDeviceRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<FindDeviceRequest>();
}

std::string FindDeviceRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
