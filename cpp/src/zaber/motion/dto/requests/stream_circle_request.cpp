// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_circle_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamCircleRequest::StreamCircleRequest() { }

StreamCircleRequest::StreamCircleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    StreamSegmentType p_type,
    RotationDirection p_rotationDirection,
    Measurement p_centerX,
    Measurement p_centerY,
    std::vector<int> p_targetAxesIndices
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    type(std::move(p_type)),
    rotationDirection(std::move(p_rotationDirection)),
    centerX(std::move(p_centerX)),
    centerY(std::move(p_centerY)),
    targetAxesIndices(std::move(p_targetAxesIndices))
{ }

bool StreamCircleRequest::operator==(const StreamCircleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    type,
    rotationDirection,
    centerX,
    centerY,
    targetAxesIndices
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.type,
    other.rotationDirection,
    other.centerX,
    other.centerY,
    other.targetAxesIndices
    );
}

int StreamCircleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamCircleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamCircleRequest::getDevice() const {
    return device;
}
void StreamCircleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamCircleRequest::getStreamId() const {
    return streamId;
}
void StreamCircleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamCircleRequest::getPvt() const {
    return pvt;
}
void StreamCircleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

StreamSegmentType const& StreamCircleRequest::getType() const {
    return type;
}
void StreamCircleRequest::setType(StreamSegmentType p_type) {
    type = std::move(p_type);
}

RotationDirection const& StreamCircleRequest::getRotationDirection() const {
    return rotationDirection;
}
void StreamCircleRequest::setRotationDirection(RotationDirection p_rotationDirection) {
    rotationDirection = std::move(p_rotationDirection);
}

Measurement const& StreamCircleRequest::getCenterX() const {
    return centerX;
}
void StreamCircleRequest::setCenterX(Measurement p_centerX) {
    centerX = std::move(p_centerX);
}

Measurement const& StreamCircleRequest::getCenterY() const {
    return centerY;
}
void StreamCircleRequest::setCenterY(Measurement p_centerY) {
    centerY = std::move(p_centerY);
}

std::vector<int> const& StreamCircleRequest::getTargetAxesIndices() const {
    return targetAxesIndices;
}
void StreamCircleRequest::setTargetAxesIndices(std::vector<int> p_targetAxesIndices) {
    targetAxesIndices = std::move(p_targetAxesIndices);
}

std::string StreamCircleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamCircleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "type: ";
    ss << StreamSegmentType_toString(this->type);
    ss << ", ";
    ss << "rotationDirection: ";
    ss << RotationDirection_toString(this->rotationDirection);
    ss << ", ";
    ss << "centerX: ";
    ss << this->centerX.toString();
    ss << ", ";
    ss << "centerY: ";
    ss << this->centerY.toString();
    ss << ", ";
    ss << "targetAxesIndices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->targetAxesIndices.size(); i++) {
        ss << this->targetAxesIndices[i];
        if (i < this->targetAxesIndices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamCircleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamCircleRequest>();
}

std::string StreamCircleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
