// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

std::string EmptyRequest::toString() const {
    std::stringstream ss;
    ss << "EmptyRequest { ";
    ss << " }";
    return ss.str();
}

void EmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<EmptyRequest>();
}

std::string EmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
