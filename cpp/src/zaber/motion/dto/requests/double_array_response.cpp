// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/double_array_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DoubleArrayResponse::DoubleArrayResponse() { }

DoubleArrayResponse::DoubleArrayResponse(
    std::vector<double> p_values
) :
    values(std::move(p_values))
{ }

bool DoubleArrayResponse::operator==(const DoubleArrayResponse& other) const {
    return std::tie(
    values
    ) == std::tie(
    other.values
    );
}

std::vector<double> const& DoubleArrayResponse::getValues() const {
    return values;
}
void DoubleArrayResponse::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::string DoubleArrayResponse::toString() const {
    std::stringstream ss;
    ss << "DoubleArrayResponse { ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DoubleArrayResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DoubleArrayResponse>();
}

std::string DoubleArrayResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
