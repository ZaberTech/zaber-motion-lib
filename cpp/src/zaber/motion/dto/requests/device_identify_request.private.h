// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/device_identify_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/firmware_version.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceIdentifyRequest, interfaceId, device, assumeVersion)

} // namespace requests
} // namespace motion
} // namespace zaber
