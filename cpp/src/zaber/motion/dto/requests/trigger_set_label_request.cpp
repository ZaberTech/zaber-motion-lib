// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_set_label_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerSetLabelRequest::TriggerSetLabelRequest() { }

TriggerSetLabelRequest::TriggerSetLabelRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    std::optional<std::string> p_label
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    label(std::move(p_label))
{ }

TriggerSetLabelRequest::TriggerSetLabelRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber)
{ }

bool TriggerSetLabelRequest::operator==(const TriggerSetLabelRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    label
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.label
    );
}

int TriggerSetLabelRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerSetLabelRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerSetLabelRequest::getDevice() const {
    return device;
}
void TriggerSetLabelRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerSetLabelRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerSetLabelRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

std::optional<std::string> const& TriggerSetLabelRequest::getLabel() const {
    return label;
}
void TriggerSetLabelRequest::setLabel(std::optional<std::string> p_label) {
    label = std::move(p_label);
}

std::string TriggerSetLabelRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerSetLabelRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "label: ";
    if (this->label.has_value()) {
        ss << this->label.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void TriggerSetLabelRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerSetLabelRequest>();
}

std::string TriggerSetLabelRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
