// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_generic_command_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGenericCommandRequest::StreamGenericCommandRequest() { }

StreamGenericCommandRequest::StreamGenericCommandRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::string p_command
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    command(std::move(p_command))
{ }

bool StreamGenericCommandRequest::operator==(const StreamGenericCommandRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    command
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.command
    );
}

int StreamGenericCommandRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamGenericCommandRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamGenericCommandRequest::getDevice() const {
    return device;
}
void StreamGenericCommandRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamGenericCommandRequest::getStreamId() const {
    return streamId;
}
void StreamGenericCommandRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamGenericCommandRequest::getPvt() const {
    return pvt;
}
void StreamGenericCommandRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::string const& StreamGenericCommandRequest::getCommand() const {
    return command;
}
void StreamGenericCommandRequest::setCommand(std::string p_command) {
    command = std::move(p_command);
}

std::string StreamGenericCommandRequest::toString() const {
    std::stringstream ss;
    ss << "StreamGenericCommandRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << " }";
    return ss.str();
}

void StreamGenericCommandRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGenericCommandRequest>();
}

std::string StreamGenericCommandRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
