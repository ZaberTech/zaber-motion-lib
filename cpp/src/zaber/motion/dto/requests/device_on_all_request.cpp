// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_on_all_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceOnAllRequest::DeviceOnAllRequest() { }

DeviceOnAllRequest::DeviceOnAllRequest(
    int p_interfaceId,
    bool p_waitUntilIdle
) :
    interfaceId(p_interfaceId),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool DeviceOnAllRequest::operator==(const DeviceOnAllRequest& other) const {
    return std::tie(
    interfaceId,
    waitUntilIdle
    ) == std::tie(
    other.interfaceId,
    other.waitUntilIdle
    );
}

int DeviceOnAllRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceOnAllRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

bool DeviceOnAllRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void DeviceOnAllRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string DeviceOnAllRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceOnAllRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void DeviceOnAllRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceOnAllRequest>();
}

std::string DeviceOnAllRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
