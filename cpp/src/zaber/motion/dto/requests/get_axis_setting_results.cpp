// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/get_axis_setting_results.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GetAxisSettingResults::GetAxisSettingResults() { }

GetAxisSettingResults::GetAxisSettingResults(
    std::vector<ascii::GetAxisSettingResult> p_results
) :
    results(std::move(p_results))
{ }

bool GetAxisSettingResults::operator==(const GetAxisSettingResults& other) const {
    return std::tie(
    results
    ) == std::tie(
    other.results
    );
}

std::vector<ascii::GetAxisSettingResult> const& GetAxisSettingResults::getResults() const {
    return results;
}
void GetAxisSettingResults::setResults(std::vector<ascii::GetAxisSettingResult> p_results) {
    results = std::move(p_results);
}

std::string GetAxisSettingResults::toString() const {
    std::stringstream ss;
    ss << "GetAxisSettingResults { ";
    ss << "results: ";
    ss << "[ ";
    for (size_t i = 0; i < this->results.size(); i++) {
        ss << this->results[i].toString();
        if (i < this->results.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void GetAxisSettingResults::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetAxisSettingResults>();
}

std::string GetAxisSettingResults::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
