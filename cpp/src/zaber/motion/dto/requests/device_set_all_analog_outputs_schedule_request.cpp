// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_all_analog_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAllAnalogOutputsScheduleRequest::DeviceSetAllAnalogOutputsScheduleRequest() { }

DeviceSetAllAnalogOutputsScheduleRequest::DeviceSetAllAnalogOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    std::vector<double> p_values,
    std::vector<double> p_futureValues,
    double p_delay,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    values(std::move(p_values)),
    futureValues(std::move(p_futureValues)),
    delay(p_delay),
    unit(p_unit)
{ }

bool DeviceSetAllAnalogOutputsScheduleRequest::operator==(const DeviceSetAllAnalogOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    values,
    futureValues,
    delay,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.values,
    other.futureValues,
    other.delay,
    other.unit
    );
}

int DeviceSetAllAnalogOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAllAnalogOutputsScheduleRequest::getDevice() const {
    return device;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

std::vector<double> const& DeviceSetAllAnalogOutputsScheduleRequest::getValues() const {
    return values;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::vector<double> const& DeviceSetAllAnalogOutputsScheduleRequest::getFutureValues() const {
    return futureValues;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setFutureValues(std::vector<double> p_futureValues) {
    futureValues = std::move(p_futureValues);
}

double DeviceSetAllAnalogOutputsScheduleRequest::getDelay() const {
    return delay;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setDelay(double p_delay) {
    delay = p_delay;
}

Units DeviceSetAllAnalogOutputsScheduleRequest::getUnit() const {
    return unit;
}
void DeviceSetAllAnalogOutputsScheduleRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetAllAnalogOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAllAnalogOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "futureValues: ";
    ss << "[ ";
    for (size_t i = 0; i < this->futureValues.size(); i++) {
        ss << this->futureValues[i];
        if (i < this->futureValues.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "delay: ";
    ss << this->delay;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetAllAnalogOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAllAnalogOutputsScheduleRequest>();
}

std::string DeviceSetAllAnalogOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
