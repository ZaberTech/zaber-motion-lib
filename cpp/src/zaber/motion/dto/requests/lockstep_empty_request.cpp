// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/lockstep_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

LockstepEmptyRequest::LockstepEmptyRequest() { }

LockstepEmptyRequest::LockstepEmptyRequest(
    int p_interfaceId,
    int p_device,
    int p_lockstepGroupId
) :
    interfaceId(p_interfaceId),
    device(p_device),
    lockstepGroupId(p_lockstepGroupId)
{ }

bool LockstepEmptyRequest::operator==(const LockstepEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    lockstepGroupId
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.lockstepGroupId
    );
}

int LockstepEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void LockstepEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int LockstepEmptyRequest::getDevice() const {
    return device;
}
void LockstepEmptyRequest::setDevice(int p_device) {
    device = p_device;
}

int LockstepEmptyRequest::getLockstepGroupId() const {
    return lockstepGroupId;
}
void LockstepEmptyRequest::setLockstepGroupId(int p_lockstepGroupId) {
    lockstepGroupId = p_lockstepGroupId;
}

std::string LockstepEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "LockstepEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "lockstepGroupId: ";
    ss << this->lockstepGroupId;
    ss << " }";
    return ss.str();
}

void LockstepEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<LockstepEmptyRequest>();
}

std::string LockstepEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
