// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_all_analog_outputs_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAllAnalogOutputsRequest::DeviceSetAllAnalogOutputsRequest() { }

DeviceSetAllAnalogOutputsRequest::DeviceSetAllAnalogOutputsRequest(
    int p_interfaceId,
    int p_device,
    std::vector<double> p_values
) :
    interfaceId(p_interfaceId),
    device(p_device),
    values(std::move(p_values))
{ }

bool DeviceSetAllAnalogOutputsRequest::operator==(const DeviceSetAllAnalogOutputsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    values
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.values
    );
}

int DeviceSetAllAnalogOutputsRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAllAnalogOutputsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAllAnalogOutputsRequest::getDevice() const {
    return device;
}
void DeviceSetAllAnalogOutputsRequest::setDevice(int p_device) {
    device = p_device;
}

std::vector<double> const& DeviceSetAllAnalogOutputsRequest::getValues() const {
    return values;
}
void DeviceSetAllAnalogOutputsRequest::setValues(std::vector<double> p_values) {
    values = std::move(p_values);
}

std::string DeviceSetAllAnalogOutputsRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAllAnalogOutputsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceSetAllAnalogOutputsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAllAnalogOutputsRequest>();
}

std::string DeviceSetAllAnalogOutputsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
