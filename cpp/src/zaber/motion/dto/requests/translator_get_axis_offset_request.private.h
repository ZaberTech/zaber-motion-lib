// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/translator_get_axis_offset_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TranslatorGetAxisOffsetRequest, translatorId, coordinateSystem, axis, unit)

} // namespace requests
} // namespace motion
} // namespace zaber
