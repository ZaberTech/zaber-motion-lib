// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_create_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorCreateResponse::TranslatorCreateResponse() { }

TranslatorCreateResponse::TranslatorCreateResponse(
    int p_translatorId
) :
    translatorId(p_translatorId)
{ }

bool TranslatorCreateResponse::operator==(const TranslatorCreateResponse& other) const {
    return std::tie(
    translatorId
    ) == std::tie(
    other.translatorId
    );
}

int TranslatorCreateResponse::getTranslatorId() const {
    return translatorId;
}
void TranslatorCreateResponse::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string TranslatorCreateResponse::toString() const {
    std::stringstream ss;
    ss << "TranslatorCreateResponse { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << " }";
    return ss.str();
}

void TranslatorCreateResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorCreateResponse>();
}

std::string TranslatorCreateResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
