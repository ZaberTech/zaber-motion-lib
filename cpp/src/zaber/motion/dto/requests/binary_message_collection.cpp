// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_message_collection.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryMessageCollection::BinaryMessageCollection() { }

BinaryMessageCollection::BinaryMessageCollection(
    std::vector<binary::Message> p_messages
) :
    messages(std::move(p_messages))
{ }

bool BinaryMessageCollection::operator==(const BinaryMessageCollection& other) const {
    return std::tie(
    messages
    ) == std::tie(
    other.messages
    );
}

std::vector<binary::Message> const& BinaryMessageCollection::getMessages() const {
    return messages;
}
void BinaryMessageCollection::setMessages(std::vector<binary::Message> p_messages) {
    messages = std::move(p_messages);
}

std::string BinaryMessageCollection::toString() const {
    std::stringstream ss;
    ss << "BinaryMessageCollection { ";
    ss << "messages: ";
    ss << "[ ";
    for (size_t i = 0; i < this->messages.size(); i++) {
        ss << this->messages[i].toString();
        if (i < this->messages.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void BinaryMessageCollection::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryMessageCollection>();
}

std::string BinaryMessageCollection::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
