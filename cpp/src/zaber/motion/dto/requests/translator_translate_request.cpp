// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_translate_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorTranslateRequest::TranslatorTranslateRequest() { }

TranslatorTranslateRequest::TranslatorTranslateRequest(
    int p_translatorId,
    std::string p_block
) :
    translatorId(p_translatorId),
    block(std::move(p_block))
{ }

bool TranslatorTranslateRequest::operator==(const TranslatorTranslateRequest& other) const {
    return std::tie(
    translatorId,
    block
    ) == std::tie(
    other.translatorId,
    other.block
    );
}

int TranslatorTranslateRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorTranslateRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string const& TranslatorTranslateRequest::getBlock() const {
    return block;
}
void TranslatorTranslateRequest::setBlock(std::string p_block) {
    block = std::move(p_block);
}

std::string TranslatorTranslateRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorTranslateRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "block: ";
    ss << this->block;
    ss << " }";
    return ss.str();
}

void TranslatorTranslateRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorTranslateRequest>();
}

std::string TranslatorTranslateRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
