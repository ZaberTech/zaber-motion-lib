// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/binary_generic_with_units_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(BinaryGenericWithUnitsRequest, interfaceId, device, command, data, fromUnit, toUnit, timeout)

} // namespace requests
} // namespace motion
} // namespace zaber
