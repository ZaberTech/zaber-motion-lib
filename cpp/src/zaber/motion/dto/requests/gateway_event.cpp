// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/gateway_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GatewayEvent::GatewayEvent() { }

GatewayEvent::GatewayEvent(
    std::string p_event
) :
    event(std::move(p_event))
{ }

bool GatewayEvent::operator==(const GatewayEvent& other) const {
    return std::tie(
    event
    ) == std::tie(
    other.event
    );
}

std::string const& GatewayEvent::getEvent() const {
    return event;
}
void GatewayEvent::setEvent(std::string p_event) {
    event = std::move(p_event);
}

std::string GatewayEvent::toString() const {
    std::stringstream ss;
    ss << "GatewayEvent { ";
    ss << "event: ";
    ss << this->event;
    ss << " }";
    return ss.str();
}

void GatewayEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GatewayEvent>();
}

std::string GatewayEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
