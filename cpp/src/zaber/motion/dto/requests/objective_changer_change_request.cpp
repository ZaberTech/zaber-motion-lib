// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/objective_changer_change_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ObjectiveChangerChangeRequest::ObjectiveChangerChangeRequest() { }

ObjectiveChangerChangeRequest::ObjectiveChangerChangeRequest(
    int p_interfaceId,
    int p_turretAddress,
    int p_focusAddress,
    int p_focusAxis,
    int p_objective,
    std::optional<Measurement> p_focusOffset
) :
    interfaceId(p_interfaceId),
    turretAddress(p_turretAddress),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    objective(p_objective),
    focusOffset(std::move(p_focusOffset))
{ }

ObjectiveChangerChangeRequest::ObjectiveChangerChangeRequest(
    int p_interfaceId,
    int p_turretAddress,
    int p_focusAddress,
    int p_focusAxis,
    int p_objective
) :
    interfaceId(p_interfaceId),
    turretAddress(p_turretAddress),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    objective(p_objective)
{ }

bool ObjectiveChangerChangeRequest::operator==(const ObjectiveChangerChangeRequest& other) const {
    return std::tie(
    interfaceId,
    turretAddress,
    focusAddress,
    focusAxis,
    objective,
    focusOffset
    ) == std::tie(
    other.interfaceId,
    other.turretAddress,
    other.focusAddress,
    other.focusAxis,
    other.objective,
    other.focusOffset
    );
}

int ObjectiveChangerChangeRequest::getInterfaceId() const {
    return interfaceId;
}
void ObjectiveChangerChangeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int ObjectiveChangerChangeRequest::getTurretAddress() const {
    return turretAddress;
}
void ObjectiveChangerChangeRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

int ObjectiveChangerChangeRequest::getFocusAddress() const {
    return focusAddress;
}
void ObjectiveChangerChangeRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int ObjectiveChangerChangeRequest::getFocusAxis() const {
    return focusAxis;
}
void ObjectiveChangerChangeRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

int ObjectiveChangerChangeRequest::getObjective() const {
    return objective;
}
void ObjectiveChangerChangeRequest::setObjective(int p_objective) {
    objective = p_objective;
}

std::optional<Measurement> const& ObjectiveChangerChangeRequest::getFocusOffset() const {
    return focusOffset;
}
void ObjectiveChangerChangeRequest::setFocusOffset(std::optional<Measurement> p_focusOffset) {
    focusOffset = std::move(p_focusOffset);
}

std::string ObjectiveChangerChangeRequest::toString() const {
    std::stringstream ss;
    ss << "ObjectiveChangerChangeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "objective: ";
    ss << this->objective;
    ss << ", ";
    ss << "focusOffset: ";
    if (this->focusOffset.has_value()) {
        ss << this->focusOffset.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void ObjectiveChangerChangeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ObjectiveChangerChangeRequest>();
}

std::string ObjectiveChangerChangeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
