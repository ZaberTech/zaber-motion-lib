// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_type.h"

namespace zaber {
namespace motion {
namespace requests {

std::string DeviceType_toString(DeviceType value) {
    switch (value) {
        case DeviceType::ANY: return "ANY";
        case DeviceType::PROCESS_CONTROLLER: return "PROCESS_CONTROLLER";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber
