// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_get_axis_position_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorGetAxisPositionRequest::TranslatorGetAxisPositionRequest() { }

TranslatorGetAxisPositionRequest::TranslatorGetAxisPositionRequest(
    int p_translatorId,
    std::string p_axis,
    Units p_unit
) :
    translatorId(p_translatorId),
    axis(std::move(p_axis)),
    unit(p_unit)
{ }

bool TranslatorGetAxisPositionRequest::operator==(const TranslatorGetAxisPositionRequest& other) const {
    return std::tie(
    translatorId,
    axis,
    unit
    ) == std::tie(
    other.translatorId,
    other.axis,
    other.unit
    );
}

int TranslatorGetAxisPositionRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorGetAxisPositionRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string const& TranslatorGetAxisPositionRequest::getAxis() const {
    return axis;
}
void TranslatorGetAxisPositionRequest::setAxis(std::string p_axis) {
    axis = std::move(p_axis);
}

Units TranslatorGetAxisPositionRequest::getUnit() const {
    return unit;
}
void TranslatorGetAxisPositionRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TranslatorGetAxisPositionRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorGetAxisPositionRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TranslatorGetAxisPositionRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorGetAxisPositionRequest>();
}

std::string TranslatorGetAxisPositionRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
