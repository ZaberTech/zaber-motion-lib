// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_mode_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamModeResponse::StreamModeResponse() { }

StreamModeResponse::StreamModeResponse(
    ascii::StreamMode p_streamMode,
    ascii::PvtMode p_pvtMode
) :
    streamMode(std::move(p_streamMode)),
    pvtMode(std::move(p_pvtMode))
{ }

bool StreamModeResponse::operator==(const StreamModeResponse& other) const {
    return std::tie(
    streamMode,
    pvtMode
    ) == std::tie(
    other.streamMode,
    other.pvtMode
    );
}

ascii::StreamMode const& StreamModeResponse::getStreamMode() const {
    return streamMode;
}
void StreamModeResponse::setStreamMode(ascii::StreamMode p_streamMode) {
    streamMode = std::move(p_streamMode);
}

ascii::PvtMode const& StreamModeResponse::getPvtMode() const {
    return pvtMode;
}
void StreamModeResponse::setPvtMode(ascii::PvtMode p_pvtMode) {
    pvtMode = std::move(p_pvtMode);
}

std::string StreamModeResponse::toString() const {
    std::stringstream ss;
    ss << "StreamModeResponse { ";
    ss << "streamMode: ";
    ss << StreamMode_toString(this->streamMode);
    ss << ", ";
    ss << "pvtMode: ";
    ss << PvtMode_toString(this->pvtMode);
    ss << " }";
    return ss.str();
}

void StreamModeResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamModeResponse>();
}

std::string StreamModeResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
