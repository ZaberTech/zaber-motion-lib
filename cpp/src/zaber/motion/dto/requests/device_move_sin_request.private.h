// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/device_move_sin_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceMoveSinRequest, interfaceId, device, axis, amplitude, amplitudeUnits, period, periodUnits, count, waitUntilIdle)

} // namespace requests
} // namespace motion
} // namespace zaber
