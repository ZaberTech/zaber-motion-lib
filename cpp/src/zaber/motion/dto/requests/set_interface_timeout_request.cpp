// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_interface_timeout_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetInterfaceTimeoutRequest::SetInterfaceTimeoutRequest() { }

SetInterfaceTimeoutRequest::SetInterfaceTimeoutRequest(
    int p_interfaceId,
    int p_timeout
) :
    interfaceId(p_interfaceId),
    timeout(p_timeout)
{ }

bool SetInterfaceTimeoutRequest::operator==(const SetInterfaceTimeoutRequest& other) const {
    return std::tie(
    interfaceId,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.timeout
    );
}

int SetInterfaceTimeoutRequest::getInterfaceId() const {
    return interfaceId;
}
void SetInterfaceTimeoutRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int SetInterfaceTimeoutRequest::getTimeout() const {
    return timeout;
}
void SetInterfaceTimeoutRequest::setTimeout(int p_timeout) {
    timeout = p_timeout;
}

std::string SetInterfaceTimeoutRequest::toString() const {
    std::stringstream ss;
    ss << "SetInterfaceTimeoutRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void SetInterfaceTimeoutRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetInterfaceTimeoutRequest>();
}

std::string SetInterfaceTimeoutRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
