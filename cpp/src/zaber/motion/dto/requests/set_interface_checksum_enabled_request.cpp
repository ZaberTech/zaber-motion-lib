// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_interface_checksum_enabled_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetInterfaceChecksumEnabledRequest::SetInterfaceChecksumEnabledRequest() { }

SetInterfaceChecksumEnabledRequest::SetInterfaceChecksumEnabledRequest(
    int p_interfaceId,
    bool p_isEnabled
) :
    interfaceId(p_interfaceId),
    isEnabled(p_isEnabled)
{ }

bool SetInterfaceChecksumEnabledRequest::operator==(const SetInterfaceChecksumEnabledRequest& other) const {
    return std::tie(
    interfaceId,
    isEnabled
    ) == std::tie(
    other.interfaceId,
    other.isEnabled
    );
}

int SetInterfaceChecksumEnabledRequest::getInterfaceId() const {
    return interfaceId;
}
void SetInterfaceChecksumEnabledRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

bool SetInterfaceChecksumEnabledRequest::getIsEnabled() const {
    return isEnabled;
}
void SetInterfaceChecksumEnabledRequest::setIsEnabled(bool p_isEnabled) {
    isEnabled = p_isEnabled;
}

std::string SetInterfaceChecksumEnabledRequest::toString() const {
    std::stringstream ss;
    ss << "SetInterfaceChecksumEnabledRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "isEnabled: ";
    ss << this->isEnabled;
    ss << " }";
    return ss.str();
}

void SetInterfaceChecksumEnabledRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetInterfaceChecksumEnabledRequest>();
}

std::string SetInterfaceChecksumEnabledRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
