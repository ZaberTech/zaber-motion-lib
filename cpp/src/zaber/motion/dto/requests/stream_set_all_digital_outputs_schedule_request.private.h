// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_set_all_digital_outputs_schedule_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamSetAllDigitalOutputsScheduleRequest, interfaceId, device, streamId, pvt, values, futureValues, delay, unit)

} // namespace requests
} // namespace motion
} // namespace zaber
