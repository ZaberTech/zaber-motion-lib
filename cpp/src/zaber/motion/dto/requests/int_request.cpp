// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/int_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

IntRequest::IntRequest() { }

IntRequest::IntRequest(
    int p_value
) :
    value(p_value)
{ }

bool IntRequest::operator==(const IntRequest& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

int IntRequest::getValue() const {
    return value;
}
void IntRequest::setValue(int p_value) {
    value = p_value;
}

std::string IntRequest::toString() const {
    std::stringstream ss;
    ss << "IntRequest { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void IntRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<IntRequest>();
}

std::string IntRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
