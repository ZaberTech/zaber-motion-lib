// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/prepare_command_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PrepareCommandRequest, interfaceId, device, axis, commandTemplate, parameters)

} // namespace requests
} // namespace motion
} // namespace zaber
