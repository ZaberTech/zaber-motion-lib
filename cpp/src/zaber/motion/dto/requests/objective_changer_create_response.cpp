// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/objective_changer_create_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ObjectiveChangerCreateResponse::ObjectiveChangerCreateResponse() { }

ObjectiveChangerCreateResponse::ObjectiveChangerCreateResponse(
    int p_turret,
    int p_focusAddress,
    int p_focusAxis
) :
    turret(p_turret),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis)
{ }

bool ObjectiveChangerCreateResponse::operator==(const ObjectiveChangerCreateResponse& other) const {
    return std::tie(
    turret,
    focusAddress,
    focusAxis
    ) == std::tie(
    other.turret,
    other.focusAddress,
    other.focusAxis
    );
}

int ObjectiveChangerCreateResponse::getTurret() const {
    return turret;
}
void ObjectiveChangerCreateResponse::setTurret(int p_turret) {
    turret = p_turret;
}

int ObjectiveChangerCreateResponse::getFocusAddress() const {
    return focusAddress;
}
void ObjectiveChangerCreateResponse::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int ObjectiveChangerCreateResponse::getFocusAxis() const {
    return focusAxis;
}
void ObjectiveChangerCreateResponse::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

std::string ObjectiveChangerCreateResponse::toString() const {
    std::stringstream ss;
    ss << "ObjectiveChangerCreateResponse { ";
    ss << "turret: ";
    ss << this->turret;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << " }";
    return ss.str();
}

void ObjectiveChangerCreateResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ObjectiveChangerCreateResponse>();
}

std::string ObjectiveChangerCreateResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
