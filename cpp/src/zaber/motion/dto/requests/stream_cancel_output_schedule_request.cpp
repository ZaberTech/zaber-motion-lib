// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_cancel_output_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamCancelOutputScheduleRequest::StreamCancelOutputScheduleRequest() { }

StreamCancelOutputScheduleRequest::StreamCancelOutputScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    bool p_analog,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    analog(p_analog),
    channelNumber(p_channelNumber)
{ }

bool StreamCancelOutputScheduleRequest::operator==(const StreamCancelOutputScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    analog,
    channelNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.analog,
    other.channelNumber
    );
}

int StreamCancelOutputScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamCancelOutputScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamCancelOutputScheduleRequest::getDevice() const {
    return device;
}
void StreamCancelOutputScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamCancelOutputScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamCancelOutputScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamCancelOutputScheduleRequest::getPvt() const {
    return pvt;
}
void StreamCancelOutputScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

bool StreamCancelOutputScheduleRequest::getAnalog() const {
    return analog;
}
void StreamCancelOutputScheduleRequest::setAnalog(bool p_analog) {
    analog = p_analog;
}

int StreamCancelOutputScheduleRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamCancelOutputScheduleRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string StreamCancelOutputScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamCancelOutputScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "analog: ";
    ss << this->analog;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << " }";
    return ss.str();
}

void StreamCancelOutputScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamCancelOutputScheduleRequest>();
}

std::string StreamCancelOutputScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
