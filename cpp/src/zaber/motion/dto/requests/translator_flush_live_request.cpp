// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_flush_live_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorFlushLiveRequest::TranslatorFlushLiveRequest() { }

TranslatorFlushLiveRequest::TranslatorFlushLiveRequest(
    int p_translatorId,
    bool p_waitUntilIdle
) :
    translatorId(p_translatorId),
    waitUntilIdle(p_waitUntilIdle)
{ }

bool TranslatorFlushLiveRequest::operator==(const TranslatorFlushLiveRequest& other) const {
    return std::tie(
    translatorId,
    waitUntilIdle
    ) == std::tie(
    other.translatorId,
    other.waitUntilIdle
    );
}

int TranslatorFlushLiveRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorFlushLiveRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

bool TranslatorFlushLiveRequest::getWaitUntilIdle() const {
    return waitUntilIdle;
}
void TranslatorFlushLiveRequest::setWaitUntilIdle(bool p_waitUntilIdle) {
    waitUntilIdle = p_waitUntilIdle;
}

std::string TranslatorFlushLiveRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorFlushLiveRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "waitUntilIdle: ";
    ss << this->waitUntilIdle;
    ss << " }";
    return ss.str();
}

void TranslatorFlushLiveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorFlushLiveRequest>();
}

std::string TranslatorFlushLiveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
