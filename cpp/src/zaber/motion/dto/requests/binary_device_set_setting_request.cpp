// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_set_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceSetSettingRequest::BinaryDeviceSetSettingRequest() { }

BinaryDeviceSetSettingRequest::BinaryDeviceSetSettingRequest(
    int p_interfaceId,
    int p_device,
    binary::BinarySettings p_setting,
    double p_value,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    setting(std::move(p_setting)),
    value(p_value),
    unit(p_unit)
{ }

bool BinaryDeviceSetSettingRequest::operator==(const BinaryDeviceSetSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    setting,
    value,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.setting,
    other.value,
    other.unit
    );
}

int BinaryDeviceSetSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceSetSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryDeviceSetSettingRequest::getDevice() const {
    return device;
}
void BinaryDeviceSetSettingRequest::setDevice(int p_device) {
    device = p_device;
}

binary::BinarySettings const& BinaryDeviceSetSettingRequest::getSetting() const {
    return setting;
}
void BinaryDeviceSetSettingRequest::setSetting(binary::BinarySettings p_setting) {
    setting = std::move(p_setting);
}

double BinaryDeviceSetSettingRequest::getValue() const {
    return value;
}
void BinaryDeviceSetSettingRequest::setValue(double p_value) {
    value = p_value;
}

Units BinaryDeviceSetSettingRequest::getUnit() const {
    return unit;
}
void BinaryDeviceSetSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string BinaryDeviceSetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceSetSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "setting: ";
    ss << BinarySettings_toString(this->setting);
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void BinaryDeviceSetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceSetSettingRequest>();
}

std::string BinaryDeviceSetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
