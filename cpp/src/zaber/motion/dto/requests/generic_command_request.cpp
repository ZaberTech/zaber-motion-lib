// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/generic_command_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GenericCommandRequest::GenericCommandRequest() { }

GenericCommandRequest::GenericCommandRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_command,
    bool p_checkErrors,
    int p_timeout
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    command(std::move(p_command)),
    checkErrors(p_checkErrors),
    timeout(p_timeout)
{ }

bool GenericCommandRequest::operator==(const GenericCommandRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    command,
    checkErrors,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.command,
    other.checkErrors,
    other.timeout
    );
}

int GenericCommandRequest::getInterfaceId() const {
    return interfaceId;
}
void GenericCommandRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int GenericCommandRequest::getDevice() const {
    return device;
}
void GenericCommandRequest::setDevice(int p_device) {
    device = p_device;
}

int GenericCommandRequest::getAxis() const {
    return axis;
}
void GenericCommandRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& GenericCommandRequest::getCommand() const {
    return command;
}
void GenericCommandRequest::setCommand(std::string p_command) {
    command = std::move(p_command);
}

bool GenericCommandRequest::getCheckErrors() const {
    return checkErrors;
}
void GenericCommandRequest::setCheckErrors(bool p_checkErrors) {
    checkErrors = p_checkErrors;
}

int GenericCommandRequest::getTimeout() const {
    return timeout;
}
void GenericCommandRequest::setTimeout(int p_timeout) {
    timeout = p_timeout;
}

std::string GenericCommandRequest::toString() const {
    std::stringstream ss;
    ss << "GenericCommandRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "command: ";
    ss << this->command;
    ss << ", ";
    ss << "checkErrors: ";
    ss << this->checkErrors;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void GenericCommandRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GenericCommandRequest>();
}

std::string GenericCommandRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
