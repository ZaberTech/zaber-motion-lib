// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/oscilloscope_add_io_channel_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(OscilloscopeAddIoChannelRequest, interfaceId, device, ioType, ioChannel)

} // namespace requests
} // namespace motion
} // namespace zaber
