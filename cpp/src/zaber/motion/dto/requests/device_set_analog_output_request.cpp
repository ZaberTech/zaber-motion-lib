// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_analog_output_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetAnalogOutputRequest::DeviceSetAnalogOutputRequest() { }

DeviceSetAnalogOutputRequest::DeviceSetAnalogOutputRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    double p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    value(p_value)
{ }

bool DeviceSetAnalogOutputRequest::operator==(const DeviceSetAnalogOutputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.value
    );
}

int DeviceSetAnalogOutputRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetAnalogOutputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetAnalogOutputRequest::getDevice() const {
    return device;
}
void DeviceSetAnalogOutputRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetAnalogOutputRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceSetAnalogOutputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double DeviceSetAnalogOutputRequest::getValue() const {
    return value;
}
void DeviceSetAnalogOutputRequest::setValue(double p_value) {
    value = p_value;
}

std::string DeviceSetAnalogOutputRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetAnalogOutputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void DeviceSetAnalogOutputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetAnalogOutputRequest>();
}

std::string DeviceSetAnalogOutputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
