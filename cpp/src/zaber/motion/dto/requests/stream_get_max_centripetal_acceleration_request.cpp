// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_get_max_centripetal_acceleration_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamGetMaxCentripetalAccelerationRequest::StreamGetMaxCentripetalAccelerationRequest() { }

StreamGetMaxCentripetalAccelerationRequest::StreamGetMaxCentripetalAccelerationRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    unit(p_unit)
{ }

bool StreamGetMaxCentripetalAccelerationRequest::operator==(const StreamGetMaxCentripetalAccelerationRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.unit
    );
}

int StreamGetMaxCentripetalAccelerationRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamGetMaxCentripetalAccelerationRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamGetMaxCentripetalAccelerationRequest::getDevice() const {
    return device;
}
void StreamGetMaxCentripetalAccelerationRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamGetMaxCentripetalAccelerationRequest::getStreamId() const {
    return streamId;
}
void StreamGetMaxCentripetalAccelerationRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamGetMaxCentripetalAccelerationRequest::getPvt() const {
    return pvt;
}
void StreamGetMaxCentripetalAccelerationRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

Units StreamGetMaxCentripetalAccelerationRequest::getUnit() const {
    return unit;
}
void StreamGetMaxCentripetalAccelerationRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string StreamGetMaxCentripetalAccelerationRequest::toString() const {
    std::stringstream ss;
    ss << "StreamGetMaxCentripetalAccelerationRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void StreamGetMaxCentripetalAccelerationRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamGetMaxCentripetalAccelerationRequest>();
}

std::string StreamGetMaxCentripetalAccelerationRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
