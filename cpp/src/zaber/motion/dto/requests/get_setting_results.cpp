// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/get_setting_results.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GetSettingResults::GetSettingResults() { }

GetSettingResults::GetSettingResults(
    std::vector<ascii::GetSettingResult> p_results
) :
    results(std::move(p_results))
{ }

bool GetSettingResults::operator==(const GetSettingResults& other) const {
    return std::tie(
    results
    ) == std::tie(
    other.results
    );
}

std::vector<ascii::GetSettingResult> const& GetSettingResults::getResults() const {
    return results;
}
void GetSettingResults::setResults(std::vector<ascii::GetSettingResult> p_results) {
    results = std::move(p_results);
}

std::string GetSettingResults::toString() const {
    std::stringstream ss;
    ss << "GetSettingResults { ";
    ss << "results: ";
    ss << "[ ";
    for (size_t i = 0; i < this->results.size(); i++) {
        ss << this->results[i].toString();
        if (i < this->results.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void GetSettingResults::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GetSettingResults>();
}

std::string GetSettingResults::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
