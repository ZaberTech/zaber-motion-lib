// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_all_digital_io_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetAllDigitalIOResponse::DeviceGetAllDigitalIOResponse() { }

DeviceGetAllDigitalIOResponse::DeviceGetAllDigitalIOResponse(
    std::vector<bool> p_values
) :
    values(std::move(p_values))
{ }

bool DeviceGetAllDigitalIOResponse::operator==(const DeviceGetAllDigitalIOResponse& other) const {
    return std::tie(
    values
    ) == std::tie(
    other.values
    );
}

std::vector<bool> const& DeviceGetAllDigitalIOResponse::getValues() const {
    return values;
}
void DeviceGetAllDigitalIOResponse::setValues(std::vector<bool> p_values) {
    values = std::move(p_values);
}

std::string DeviceGetAllDigitalIOResponse::toString() const {
    std::stringstream ss;
    ss << "DeviceGetAllDigitalIOResponse { ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceGetAllDigitalIOResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetAllDigitalIOResponse>();
}

std::string DeviceGetAllDigitalIOResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
