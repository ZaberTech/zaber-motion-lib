// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/test_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TestRequest::TestRequest() { }

TestRequest::TestRequest(
    bool p_returnError,
    std::string p_dataPing,
    bool p_returnErrorWithData
) :
    returnError(p_returnError),
    dataPing(std::move(p_dataPing)),
    returnErrorWithData(p_returnErrorWithData)
{ }

bool TestRequest::operator==(const TestRequest& other) const {
    return std::tie(
    returnError,
    dataPing,
    returnErrorWithData
    ) == std::tie(
    other.returnError,
    other.dataPing,
    other.returnErrorWithData
    );
}

bool TestRequest::getReturnError() const {
    return returnError;
}
void TestRequest::setReturnError(bool p_returnError) {
    returnError = p_returnError;
}

std::string const& TestRequest::getDataPing() const {
    return dataPing;
}
void TestRequest::setDataPing(std::string p_dataPing) {
    dataPing = std::move(p_dataPing);
}

bool TestRequest::getReturnErrorWithData() const {
    return returnErrorWithData;
}
void TestRequest::setReturnErrorWithData(bool p_returnErrorWithData) {
    returnErrorWithData = p_returnErrorWithData;
}

std::string TestRequest::toString() const {
    std::stringstream ss;
    ss << "TestRequest { ";
    ss << "returnError: ";
    ss << this->returnError;
    ss << ", ";
    ss << "dataPing: ";
    ss << this->dataPing;
    ss << ", ";
    ss << "returnErrorWithData: ";
    ss << this->returnErrorWithData;
    ss << " }";
    return ss.str();
}

void TestRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TestRequest>();
}

std::string TestRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
