// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unit_get_symbol_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnitGetSymbolResponse::UnitGetSymbolResponse() { }

UnitGetSymbolResponse::UnitGetSymbolResponse(
    std::string p_symbol
) :
    symbol(std::move(p_symbol))
{ }

bool UnitGetSymbolResponse::operator==(const UnitGetSymbolResponse& other) const {
    return std::tie(
    symbol
    ) == std::tie(
    other.symbol
    );
}

std::string const& UnitGetSymbolResponse::getSymbol() const {
    return symbol;
}
void UnitGetSymbolResponse::setSymbol(std::string p_symbol) {
    symbol = std::move(p_symbol);
}

std::string UnitGetSymbolResponse::toString() const {
    std::stringstream ss;
    ss << "UnitGetSymbolResponse { ";
    ss << "symbol: ";
    ss << this->symbol;
    ss << " }";
    return ss.str();
}

void UnitGetSymbolResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnitGetSymbolResponse>();
}

std::string UnitGetSymbolResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
