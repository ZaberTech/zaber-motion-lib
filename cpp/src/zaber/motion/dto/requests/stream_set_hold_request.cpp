// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_hold_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetHoldRequest::StreamSetHoldRequest() { }

StreamSetHoldRequest::StreamSetHoldRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    bool p_hold
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    hold(p_hold)
{ }

bool StreamSetHoldRequest::operator==(const StreamSetHoldRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    hold
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.hold
    );
}

int StreamSetHoldRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetHoldRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetHoldRequest::getDevice() const {
    return device;
}
void StreamSetHoldRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetHoldRequest::getStreamId() const {
    return streamId;
}
void StreamSetHoldRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetHoldRequest::getPvt() const {
    return pvt;
}
void StreamSetHoldRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

bool StreamSetHoldRequest::getHold() const {
    return hold;
}
void StreamSetHoldRequest::setHold(bool p_hold) {
    hold = p_hold;
}

std::string StreamSetHoldRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetHoldRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "hold: ";
    ss << this->hold;
    ss << " }";
    return ss.str();
}

void StreamSetHoldRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetHoldRequest>();
}

std::string StreamSetHoldRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
