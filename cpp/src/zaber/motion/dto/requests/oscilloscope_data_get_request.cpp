// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/oscilloscope_data_get_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OscilloscopeDataGetRequest::OscilloscopeDataGetRequest() { }

OscilloscopeDataGetRequest::OscilloscopeDataGetRequest(
    int p_dataId,
    Units p_unit
) :
    dataId(p_dataId),
    unit(p_unit)
{ }

bool OscilloscopeDataGetRequest::operator==(const OscilloscopeDataGetRequest& other) const {
    return std::tie(
    dataId,
    unit
    ) == std::tie(
    other.dataId,
    other.unit
    );
}

int OscilloscopeDataGetRequest::getDataId() const {
    return dataId;
}
void OscilloscopeDataGetRequest::setDataId(int p_dataId) {
    dataId = p_dataId;
}

Units OscilloscopeDataGetRequest::getUnit() const {
    return unit;
}
void OscilloscopeDataGetRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string OscilloscopeDataGetRequest::toString() const {
    std::stringstream ss;
    ss << "OscilloscopeDataGetRequest { ";
    ss << "dataId: ";
    ss << this->dataId;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void OscilloscopeDataGetRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OscilloscopeDataGetRequest>();
}

std::string OscilloscopeDataGetRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
