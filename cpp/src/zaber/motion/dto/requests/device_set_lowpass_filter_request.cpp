// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_lowpass_filter_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetLowpassFilterRequest::DeviceSetLowpassFilterRequest() { }

DeviceSetLowpassFilterRequest::DeviceSetLowpassFilterRequest(
    int p_interfaceId,
    int p_device,
    int p_channelNumber,
    double p_cutoffFrequency,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelNumber(p_channelNumber),
    cutoffFrequency(p_cutoffFrequency),
    unit(p_unit)
{ }

bool DeviceSetLowpassFilterRequest::operator==(const DeviceSetLowpassFilterRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelNumber,
    cutoffFrequency,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelNumber,
    other.cutoffFrequency,
    other.unit
    );
}

int DeviceSetLowpassFilterRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetLowpassFilterRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetLowpassFilterRequest::getDevice() const {
    return device;
}
void DeviceSetLowpassFilterRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetLowpassFilterRequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceSetLowpassFilterRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

double DeviceSetLowpassFilterRequest::getCutoffFrequency() const {
    return cutoffFrequency;
}
void DeviceSetLowpassFilterRequest::setCutoffFrequency(double p_cutoffFrequency) {
    cutoffFrequency = p_cutoffFrequency;
}

Units DeviceSetLowpassFilterRequest::getUnit() const {
    return unit;
}
void DeviceSetLowpassFilterRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceSetLowpassFilterRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetLowpassFilterRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "cutoffFrequency: ";
    ss << this->cutoffFrequency;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceSetLowpassFilterRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetLowpassFilterRequest>();
}

std::string DeviceSetLowpassFilterRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
