// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/empty_autofocus_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

EmptyAutofocusRequest::EmptyAutofocusRequest() { }

EmptyAutofocusRequest::EmptyAutofocusRequest(
    int p_providerId,
    int p_interfaceId,
    int p_focusAddress,
    int p_focusAxis,
    int p_turretAddress
) :
    providerId(p_providerId),
    interfaceId(p_interfaceId),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    turretAddress(p_turretAddress)
{ }

bool EmptyAutofocusRequest::operator==(const EmptyAutofocusRequest& other) const {
    return std::tie(
    providerId,
    interfaceId,
    focusAddress,
    focusAxis,
    turretAddress
    ) == std::tie(
    other.providerId,
    other.interfaceId,
    other.focusAddress,
    other.focusAxis,
    other.turretAddress
    );
}

int EmptyAutofocusRequest::getProviderId() const {
    return providerId;
}
void EmptyAutofocusRequest::setProviderId(int p_providerId) {
    providerId = p_providerId;
}

int EmptyAutofocusRequest::getInterfaceId() const {
    return interfaceId;
}
void EmptyAutofocusRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int EmptyAutofocusRequest::getFocusAddress() const {
    return focusAddress;
}
void EmptyAutofocusRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int EmptyAutofocusRequest::getFocusAxis() const {
    return focusAxis;
}
void EmptyAutofocusRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

int EmptyAutofocusRequest::getTurretAddress() const {
    return turretAddress;
}
void EmptyAutofocusRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

std::string EmptyAutofocusRequest::toString() const {
    std::stringstream ss;
    ss << "EmptyAutofocusRequest { ";
    ss << "providerId: ";
    ss << this->providerId;
    ss << ", ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << " }";
    return ss.str();
}

void EmptyAutofocusRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<EmptyAutofocusRequest>();
}

std::string EmptyAutofocusRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
