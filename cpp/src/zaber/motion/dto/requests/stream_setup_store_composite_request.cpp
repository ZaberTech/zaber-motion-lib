// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_setup_store_composite_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetupStoreCompositeRequest::StreamSetupStoreCompositeRequest() { }

StreamSetupStoreCompositeRequest::StreamSetupStoreCompositeRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_streamBuffer,
    int p_pvtBuffer,
    std::vector<ascii::StreamAxisDefinition> p_axes,
    std::vector<ascii::PvtAxisDefinition> p_pvtAxes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    streamBuffer(p_streamBuffer),
    pvtBuffer(p_pvtBuffer),
    axes(std::move(p_axes)),
    pvtAxes(std::move(p_pvtAxes))
{ }

bool StreamSetupStoreCompositeRequest::operator==(const StreamSetupStoreCompositeRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    streamBuffer,
    pvtBuffer,
    axes,
    pvtAxes
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.streamBuffer,
    other.pvtBuffer,
    other.axes,
    other.pvtAxes
    );
}

int StreamSetupStoreCompositeRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetupStoreCompositeRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetupStoreCompositeRequest::getDevice() const {
    return device;
}
void StreamSetupStoreCompositeRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetupStoreCompositeRequest::getStreamId() const {
    return streamId;
}
void StreamSetupStoreCompositeRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetupStoreCompositeRequest::getPvt() const {
    return pvt;
}
void StreamSetupStoreCompositeRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetupStoreCompositeRequest::getStreamBuffer() const {
    return streamBuffer;
}
void StreamSetupStoreCompositeRequest::setStreamBuffer(int p_streamBuffer) {
    streamBuffer = p_streamBuffer;
}

int StreamSetupStoreCompositeRequest::getPvtBuffer() const {
    return pvtBuffer;
}
void StreamSetupStoreCompositeRequest::setPvtBuffer(int p_pvtBuffer) {
    pvtBuffer = p_pvtBuffer;
}

std::vector<ascii::StreamAxisDefinition> const& StreamSetupStoreCompositeRequest::getAxes() const {
    return axes;
}
void StreamSetupStoreCompositeRequest::setAxes(std::vector<ascii::StreamAxisDefinition> p_axes) {
    axes = std::move(p_axes);
}

std::vector<ascii::PvtAxisDefinition> const& StreamSetupStoreCompositeRequest::getPvtAxes() const {
    return pvtAxes;
}
void StreamSetupStoreCompositeRequest::setPvtAxes(std::vector<ascii::PvtAxisDefinition> p_pvtAxes) {
    pvtAxes = std::move(p_pvtAxes);
}

std::string StreamSetupStoreCompositeRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetupStoreCompositeRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "streamBuffer: ";
    ss << this->streamBuffer;
    ss << ", ";
    ss << "pvtBuffer: ";
    ss << this->pvtBuffer;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i].toString();
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "pvtAxes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->pvtAxes.size(); i++) {
        ss << this->pvtAxes[i].toString();
        if (i < this->pvtAxes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetupStoreCompositeRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetupStoreCompositeRequest>();
}

std::string StreamSetupStoreCompositeRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
