// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/set_servo_tuning_pid_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetServoTuningPIDRequest, interfaceId, device, axis, paramset, p, i, d, fc)

} // namespace requests
} // namespace motion
} // namespace zaber
