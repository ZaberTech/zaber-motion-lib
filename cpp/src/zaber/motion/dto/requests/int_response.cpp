// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/int_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

IntResponse::IntResponse() { }

IntResponse::IntResponse(
    int p_value
) :
    value(p_value)
{ }

bool IntResponse::operator==(const IntResponse& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

int IntResponse::getValue() const {
    return value;
}
void IntResponse::setValue(int p_value) {
    value = p_value;
}

std::string IntResponse::toString() const {
    std::stringstream ss;
    ss << "IntResponse { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void IntResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<IntResponse>();
}

std::string IntResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
