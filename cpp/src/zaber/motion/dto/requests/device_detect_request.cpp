// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_detect_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceDetectRequest::DeviceDetectRequest() { }

DeviceDetectRequest::DeviceDetectRequest(
    int p_interfaceId,
    bool p_identifyDevices,
    DeviceType p_type
) :
    interfaceId(p_interfaceId),
    identifyDevices(p_identifyDevices),
    type(std::move(p_type))
{ }

bool DeviceDetectRequest::operator==(const DeviceDetectRequest& other) const {
    return std::tie(
    interfaceId,
    identifyDevices,
    type
    ) == std::tie(
    other.interfaceId,
    other.identifyDevices,
    other.type
    );
}

int DeviceDetectRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceDetectRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

bool DeviceDetectRequest::getIdentifyDevices() const {
    return identifyDevices;
}
void DeviceDetectRequest::setIdentifyDevices(bool p_identifyDevices) {
    identifyDevices = p_identifyDevices;
}

DeviceType const& DeviceDetectRequest::getType() const {
    return type;
}
void DeviceDetectRequest::setType(DeviceType p_type) {
    type = std::move(p_type);
}

std::string DeviceDetectRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceDetectRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "identifyDevices: ";
    ss << this->identifyDevices;
    ss << ", ";
    ss << "type: ";
    ss << DeviceType_toString(this->type);
    ss << " }";
    return ss.str();
}

void DeviceDetectRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceDetectRequest>();
}

std::string DeviceDetectRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
