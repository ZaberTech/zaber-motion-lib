// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_multi_get_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceMultiGetSettingRequest::DeviceMultiGetSettingRequest() { }

DeviceMultiGetSettingRequest::DeviceMultiGetSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::vector<ascii::GetSetting> p_settings,
    std::vector<ascii::GetAxisSetting> p_axisSettings
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    settings(std::move(p_settings)),
    axisSettings(std::move(p_axisSettings))
{ }

bool DeviceMultiGetSettingRequest::operator==(const DeviceMultiGetSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    settings,
    axisSettings
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.settings,
    other.axisSettings
    );
}

int DeviceMultiGetSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceMultiGetSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceMultiGetSettingRequest::getDevice() const {
    return device;
}
void DeviceMultiGetSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceMultiGetSettingRequest::getAxis() const {
    return axis;
}
void DeviceMultiGetSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::vector<ascii::GetSetting> const& DeviceMultiGetSettingRequest::getSettings() const {
    return settings;
}
void DeviceMultiGetSettingRequest::setSettings(std::vector<ascii::GetSetting> p_settings) {
    settings = std::move(p_settings);
}

std::vector<ascii::GetAxisSetting> const& DeviceMultiGetSettingRequest::getAxisSettings() const {
    return axisSettings;
}
void DeviceMultiGetSettingRequest::setAxisSettings(std::vector<ascii::GetAxisSetting> p_axisSettings) {
    axisSettings = std::move(p_axisSettings);
}

std::string DeviceMultiGetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceMultiGetSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "settings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->settings.size(); i++) {
        ss << this->settings[i].toString();
        if (i < this->settings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axisSettings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axisSettings.size(); i++) {
        ss << this->axisSettings[i].toString();
        if (i < this->axisSettings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceMultiGetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceMultiGetSettingRequest>();
}

std::string DeviceMultiGetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
