// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/binary_reply_only_event_wrapper.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/binary/reply_only_event.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(BinaryReplyOnlyEventWrapper, interfaceId, reply)

} // namespace requests
} // namespace motion
} // namespace zaber
