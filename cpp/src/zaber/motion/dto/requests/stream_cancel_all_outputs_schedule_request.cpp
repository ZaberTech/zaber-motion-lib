// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_cancel_all_outputs_schedule_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamCancelAllOutputsScheduleRequest::StreamCancelAllOutputsScheduleRequest() { }

StreamCancelAllOutputsScheduleRequest::StreamCancelAllOutputsScheduleRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    bool p_analog,
    std::vector<bool> p_channels
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    analog(p_analog),
    channels(std::move(p_channels))
{ }

bool StreamCancelAllOutputsScheduleRequest::operator==(const StreamCancelAllOutputsScheduleRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    analog,
    channels
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.analog,
    other.channels
    );
}

int StreamCancelAllOutputsScheduleRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamCancelAllOutputsScheduleRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamCancelAllOutputsScheduleRequest::getDevice() const {
    return device;
}
void StreamCancelAllOutputsScheduleRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamCancelAllOutputsScheduleRequest::getStreamId() const {
    return streamId;
}
void StreamCancelAllOutputsScheduleRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamCancelAllOutputsScheduleRequest::getPvt() const {
    return pvt;
}
void StreamCancelAllOutputsScheduleRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

bool StreamCancelAllOutputsScheduleRequest::getAnalog() const {
    return analog;
}
void StreamCancelAllOutputsScheduleRequest::setAnalog(bool p_analog) {
    analog = p_analog;
}

std::vector<bool> const& StreamCancelAllOutputsScheduleRequest::getChannels() const {
    return channels;
}
void StreamCancelAllOutputsScheduleRequest::setChannels(std::vector<bool> p_channels) {
    channels = std::move(p_channels);
}

std::string StreamCancelAllOutputsScheduleRequest::toString() const {
    std::stringstream ss;
    ss << "StreamCancelAllOutputsScheduleRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "analog: ";
    ss << this->analog;
    ss << ", ";
    ss << "channels: ";
    ss << "[ ";
    for (size_t i = 0; i < this->channels.size(); i++) {
        ss << this->channels[i];
        if (i < this->channels.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamCancelAllOutputsScheduleRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamCancelAllOutputsScheduleRequest>();
}

std::string StreamCancelAllOutputsScheduleRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
