// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_detect_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceDetectResponse::BinaryDeviceDetectResponse() { }

BinaryDeviceDetectResponse::BinaryDeviceDetectResponse(
    std::vector<int> p_devices
) :
    devices(std::move(p_devices))
{ }

bool BinaryDeviceDetectResponse::operator==(const BinaryDeviceDetectResponse& other) const {
    return std::tie(
    devices
    ) == std::tie(
    other.devices
    );
}

std::vector<int> const& BinaryDeviceDetectResponse::getDevices() const {
    return devices;
}
void BinaryDeviceDetectResponse::setDevices(std::vector<int> p_devices) {
    devices = std::move(p_devices);
}

std::string BinaryDeviceDetectResponse::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceDetectResponse { ";
    ss << "devices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->devices.size(); i++) {
        ss << this->devices[i];
        if (i < this->devices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void BinaryDeviceDetectResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceDetectResponse>();
}

std::string BinaryDeviceDetectResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
