// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_identify_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceIdentifyRequest::DeviceIdentifyRequest() { }

DeviceIdentifyRequest::DeviceIdentifyRequest(
    int p_interfaceId,
    int p_device,
    std::optional<FirmwareVersion> p_assumeVersion
) :
    interfaceId(p_interfaceId),
    device(p_device),
    assumeVersion(std::move(p_assumeVersion))
{ }

DeviceIdentifyRequest::DeviceIdentifyRequest(
    int p_interfaceId,
    int p_device
) :
    interfaceId(p_interfaceId),
    device(p_device)
{ }

bool DeviceIdentifyRequest::operator==(const DeviceIdentifyRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    assumeVersion
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.assumeVersion
    );
}

int DeviceIdentifyRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceIdentifyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceIdentifyRequest::getDevice() const {
    return device;
}
void DeviceIdentifyRequest::setDevice(int p_device) {
    device = p_device;
}

std::optional<FirmwareVersion> const& DeviceIdentifyRequest::getAssumeVersion() const {
    return assumeVersion;
}
void DeviceIdentifyRequest::setAssumeVersion(std::optional<FirmwareVersion> p_assumeVersion) {
    assumeVersion = std::move(p_assumeVersion);
}

std::string DeviceIdentifyRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceIdentifyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "assumeVersion: ";
    if (this->assumeVersion.has_value()) {
        ss << this->assumeVersion.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void DeviceIdentifyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceIdentifyRequest>();
}

std::string DeviceIdentifyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
