// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_get_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceGetSettingRequest::BinaryDeviceGetSettingRequest() { }

BinaryDeviceGetSettingRequest::BinaryDeviceGetSettingRequest(
    int p_interfaceId,
    int p_device,
    binary::BinarySettings p_setting,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    setting(std::move(p_setting)),
    unit(p_unit)
{ }

bool BinaryDeviceGetSettingRequest::operator==(const BinaryDeviceGetSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    setting,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.setting,
    other.unit
    );
}

int BinaryDeviceGetSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceGetSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryDeviceGetSettingRequest::getDevice() const {
    return device;
}
void BinaryDeviceGetSettingRequest::setDevice(int p_device) {
    device = p_device;
}

binary::BinarySettings const& BinaryDeviceGetSettingRequest::getSetting() const {
    return setting;
}
void BinaryDeviceGetSettingRequest::setSetting(binary::BinarySettings p_setting) {
    setting = std::move(p_setting);
}

Units BinaryDeviceGetSettingRequest::getUnit() const {
    return unit;
}
void BinaryDeviceGetSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string BinaryDeviceGetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceGetSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "setting: ";
    ss << BinarySettings_toString(this->setting);
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void BinaryDeviceGetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceGetSettingRequest>();
}

std::string BinaryDeviceGetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
