// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetSettingRequest::DeviceGetSettingRequest() { }

DeviceGetSettingRequest::DeviceGetSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_setting,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    setting(std::move(p_setting)),
    unit(p_unit)
{ }

bool DeviceGetSettingRequest::operator==(const DeviceGetSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    setting,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.setting,
    other.unit
    );
}

int DeviceGetSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetSettingRequest::getDevice() const {
    return device;
}
void DeviceGetSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceGetSettingRequest::getAxis() const {
    return axis;
}
void DeviceGetSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceGetSettingRequest::getSetting() const {
    return setting;
}
void DeviceGetSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

Units DeviceGetSettingRequest::getUnit() const {
    return unit;
}
void DeviceGetSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string DeviceGetSettingRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void DeviceGetSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetSettingRequest>();
}

std::string DeviceGetSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
