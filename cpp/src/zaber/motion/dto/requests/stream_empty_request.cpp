// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamEmptyRequest::StreamEmptyRequest() { }

StreamEmptyRequest::StreamEmptyRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt)
{ }

bool StreamEmptyRequest::operator==(const StreamEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt
    );
}

int StreamEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamEmptyRequest::getDevice() const {
    return device;
}
void StreamEmptyRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamEmptyRequest::getStreamId() const {
    return streamId;
}
void StreamEmptyRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamEmptyRequest::getPvt() const {
    return pvt;
}
void StreamEmptyRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::string StreamEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "StreamEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << " }";
    return ss.str();
}

void StreamEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamEmptyRequest>();
}

std::string StreamEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
