// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/unknown_response_event_wrapper.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/unknown_response_event.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(UnknownResponseEventWrapper, interfaceId, unknownResponse)

} // namespace requests
} // namespace motion
} // namespace zaber
