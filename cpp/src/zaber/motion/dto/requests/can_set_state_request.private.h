// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/can_set_state_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/firmware_version.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(CanSetStateRequest, interfaceId, device, axis, state, firmwareVersion)

} // namespace requests
} // namespace motion
} // namespace zaber
