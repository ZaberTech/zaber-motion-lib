// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/empty_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

void to_json(nlohmann::json& j, const EmptyRequest&) {
    j = nlohmann::json::object();
}

void from_json(const nlohmann::json&, const EmptyRequest&) {
}

} // namespace requests
} // namespace motion
} // namespace zaber
