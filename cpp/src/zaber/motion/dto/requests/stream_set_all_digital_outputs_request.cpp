// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_set_all_digital_outputs_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetAllDigitalOutputsRequest::StreamSetAllDigitalOutputsRequest() { }

StreamSetAllDigitalOutputsRequest::StreamSetAllDigitalOutputsRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<ascii::DigitalOutputAction> p_values
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    values(std::move(p_values))
{ }

bool StreamSetAllDigitalOutputsRequest::operator==(const StreamSetAllDigitalOutputsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    values
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.values
    );
}

int StreamSetAllDigitalOutputsRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetAllDigitalOutputsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetAllDigitalOutputsRequest::getDevice() const {
    return device;
}
void StreamSetAllDigitalOutputsRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetAllDigitalOutputsRequest::getStreamId() const {
    return streamId;
}
void StreamSetAllDigitalOutputsRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetAllDigitalOutputsRequest::getPvt() const {
    return pvt;
}
void StreamSetAllDigitalOutputsRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<ascii::DigitalOutputAction> const& StreamSetAllDigitalOutputsRequest::getValues() const {
    return values;
}
void StreamSetAllDigitalOutputsRequest::setValues(std::vector<ascii::DigitalOutputAction> p_values) {
    values = std::move(p_values);
}

std::string StreamSetAllDigitalOutputsRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetAllDigitalOutputsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << DigitalOutputAction_toString(this->values[i]);
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetAllDigitalOutputsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetAllDigitalOutputsRequest>();
}

std::string StreamSetAllDigitalOutputsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
