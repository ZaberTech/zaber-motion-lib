// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_restore_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceRestoreRequest::DeviceRestoreRequest() { }

DeviceRestoreRequest::DeviceRestoreRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_hard
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    hard(p_hard)
{ }

bool DeviceRestoreRequest::operator==(const DeviceRestoreRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    hard
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.hard
    );
}

int DeviceRestoreRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceRestoreRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceRestoreRequest::getDevice() const {
    return device;
}
void DeviceRestoreRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceRestoreRequest::getAxis() const {
    return axis;
}
void DeviceRestoreRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceRestoreRequest::getHard() const {
    return hard;
}
void DeviceRestoreRequest::setHard(bool p_hard) {
    hard = p_hard;
}

std::string DeviceRestoreRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceRestoreRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "hard: ";
    ss << this->hard;
    ss << " }";
    return ss.str();
}

void DeviceRestoreRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceRestoreRequest>();
}

std::string DeviceRestoreRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
