// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/gateway_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GatewayRequest::GatewayRequest() { }

GatewayRequest::GatewayRequest(
    std::string p_request
) :
    request(std::move(p_request))
{ }

bool GatewayRequest::operator==(const GatewayRequest& other) const {
    return std::tie(
    request
    ) == std::tie(
    other.request
    );
}

std::string const& GatewayRequest::getRequest() const {
    return request;
}
void GatewayRequest::setRequest(std::string p_request) {
    request = std::move(p_request);
}

std::string GatewayRequest::toString() const {
    std::stringstream ss;
    ss << "GatewayRequest { ";
    ss << "request: ";
    ss << this->request;
    ss << " }";
    return ss.str();
}

void GatewayRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GatewayRequest>();
}

std::string GatewayRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
