// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/string_array_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StringArrayResponse::StringArrayResponse() { }

StringArrayResponse::StringArrayResponse(
    std::vector<std::string> p_values
) :
    values(std::move(p_values))
{ }

bool StringArrayResponse::operator==(const StringArrayResponse& other) const {
    return std::tie(
    values
    ) == std::tie(
    other.values
    );
}

std::vector<std::string> const& StringArrayResponse::getValues() const {
    return values;
}
void StringArrayResponse::setValues(std::vector<std::string> p_values) {
    values = std::move(p_values);
}

std::string StringArrayResponse::toString() const {
    std::stringstream ss;
    ss << "StringArrayResponse { ";
    ss << "values: ";
    ss << "[ ";
    for (size_t i = 0; i < this->values.size(); i++) {
        ss << this->values[i];
        if (i < this->values.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StringArrayResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StringArrayResponse>();
}

std::string StringArrayResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
