// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/forget_devices_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ForgetDevicesRequest::ForgetDevicesRequest() { }

ForgetDevicesRequest::ForgetDevicesRequest(
    int p_interfaceId,
    std::vector<int> p_exceptDevices
) :
    interfaceId(p_interfaceId),
    exceptDevices(std::move(p_exceptDevices))
{ }

bool ForgetDevicesRequest::operator==(const ForgetDevicesRequest& other) const {
    return std::tie(
    interfaceId,
    exceptDevices
    ) == std::tie(
    other.interfaceId,
    other.exceptDevices
    );
}

int ForgetDevicesRequest::getInterfaceId() const {
    return interfaceId;
}
void ForgetDevicesRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

std::vector<int> const& ForgetDevicesRequest::getExceptDevices() const {
    return exceptDevices;
}
void ForgetDevicesRequest::setExceptDevices(std::vector<int> p_exceptDevices) {
    exceptDevices = std::move(p_exceptDevices);
}

std::string ForgetDevicesRequest::toString() const {
    std::stringstream ss;
    ss << "ForgetDevicesRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "exceptDevices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->exceptDevices.size(); i++) {
        ss << this->exceptDevices[i];
        if (i < this->exceptDevices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void ForgetDevicesRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ForgetDevicesRequest>();
}

std::string ForgetDevicesRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
