// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_arc_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamArcRequest, interfaceId, device, streamId, pvt, type, rotationDirection, centerX, centerY, endX, endY, targetAxesIndices, endpoint)

} // namespace requests
} // namespace motion
} // namespace zaber
