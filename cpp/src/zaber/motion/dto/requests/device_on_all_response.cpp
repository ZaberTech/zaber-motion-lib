// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_on_all_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceOnAllResponse::DeviceOnAllResponse() { }

DeviceOnAllResponse::DeviceOnAllResponse(
    std::vector<int> p_deviceAddresses
) :
    deviceAddresses(std::move(p_deviceAddresses))
{ }

bool DeviceOnAllResponse::operator==(const DeviceOnAllResponse& other) const {
    return std::tie(
    deviceAddresses
    ) == std::tie(
    other.deviceAddresses
    );
}

std::vector<int> const& DeviceOnAllResponse::getDeviceAddresses() const {
    return deviceAddresses;
}
void DeviceOnAllResponse::setDeviceAddresses(std::vector<int> p_deviceAddresses) {
    deviceAddresses = std::move(p_deviceAddresses);
}

std::string DeviceOnAllResponse::toString() const {
    std::stringstream ss;
    ss << "DeviceOnAllResponse { ";
    ss << "deviceAddresses: ";
    ss << "[ ";
    for (size_t i = 0; i < this->deviceAddresses.size(); i++) {
        ss << this->deviceAddresses[i];
        if (i < this->deviceAddresses.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceOnAllResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceOnAllResponse>();
}

std::string DeviceOnAllResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
