// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/open_interface_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

OpenInterfaceResponse::OpenInterfaceResponse() { }

OpenInterfaceResponse::OpenInterfaceResponse(
    int p_interfaceId
) :
    interfaceId(p_interfaceId)
{ }

bool OpenInterfaceResponse::operator==(const OpenInterfaceResponse& other) const {
    return std::tie(
    interfaceId
    ) == std::tie(
    other.interfaceId
    );
}

int OpenInterfaceResponse::getInterfaceId() const {
    return interfaceId;
}
void OpenInterfaceResponse::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

std::string OpenInterfaceResponse::toString() const {
    std::stringstream ss;
    ss << "OpenInterfaceResponse { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << " }";
    return ss.str();
}

void OpenInterfaceResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OpenInterfaceResponse>();
}

std::string OpenInterfaceResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
