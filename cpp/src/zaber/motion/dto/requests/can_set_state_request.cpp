// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/can_set_state_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CanSetStateRequest::CanSetStateRequest() { }

CanSetStateRequest::CanSetStateRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_state,
    std::optional<FirmwareVersion> p_firmwareVersion
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    state(std::move(p_state)),
    firmwareVersion(std::move(p_firmwareVersion))
{ }

CanSetStateRequest::CanSetStateRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_state
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    state(std::move(p_state))
{ }

bool CanSetStateRequest::operator==(const CanSetStateRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    state,
    firmwareVersion
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.state,
    other.firmwareVersion
    );
}

int CanSetStateRequest::getInterfaceId() const {
    return interfaceId;
}
void CanSetStateRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int CanSetStateRequest::getDevice() const {
    return device;
}
void CanSetStateRequest::setDevice(int p_device) {
    device = p_device;
}

int CanSetStateRequest::getAxis() const {
    return axis;
}
void CanSetStateRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& CanSetStateRequest::getState() const {
    return state;
}
void CanSetStateRequest::setState(std::string p_state) {
    state = std::move(p_state);
}

std::optional<FirmwareVersion> const& CanSetStateRequest::getFirmwareVersion() const {
    return firmwareVersion;
}
void CanSetStateRequest::setFirmwareVersion(std::optional<FirmwareVersion> p_firmwareVersion) {
    firmwareVersion = std::move(p_firmwareVersion);
}

std::string CanSetStateRequest::toString() const {
    std::stringstream ss;
    ss << "CanSetStateRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "state: ";
    ss << this->state;
    ss << ", ";
    ss << "firmwareVersion: ";
    if (this->firmwareVersion.has_value()) {
        ss << this->firmwareVersion.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void CanSetStateRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CanSetStateRequest>();
}

std::string CanSetStateRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
