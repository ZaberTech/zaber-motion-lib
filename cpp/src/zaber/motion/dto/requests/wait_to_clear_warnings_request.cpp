// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/wait_to_clear_warnings_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

WaitToClearWarningsRequest::WaitToClearWarningsRequest() { }

WaitToClearWarningsRequest::WaitToClearWarningsRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    double p_timeout,
    std::vector<std::string> p_warningFlags
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    timeout(p_timeout),
    warningFlags(std::move(p_warningFlags))
{ }

bool WaitToClearWarningsRequest::operator==(const WaitToClearWarningsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    timeout,
    warningFlags
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.timeout,
    other.warningFlags
    );
}

int WaitToClearWarningsRequest::getInterfaceId() const {
    return interfaceId;
}
void WaitToClearWarningsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int WaitToClearWarningsRequest::getDevice() const {
    return device;
}
void WaitToClearWarningsRequest::setDevice(int p_device) {
    device = p_device;
}

int WaitToClearWarningsRequest::getAxis() const {
    return axis;
}
void WaitToClearWarningsRequest::setAxis(int p_axis) {
    axis = p_axis;
}

double WaitToClearWarningsRequest::getTimeout() const {
    return timeout;
}
void WaitToClearWarningsRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

std::vector<std::string> const& WaitToClearWarningsRequest::getWarningFlags() const {
    return warningFlags;
}
void WaitToClearWarningsRequest::setWarningFlags(std::vector<std::string> p_warningFlags) {
    warningFlags = std::move(p_warningFlags);
}

std::string WaitToClearWarningsRequest::toString() const {
    std::stringstream ss;
    ss << "WaitToClearWarningsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << ", ";
    ss << "warningFlags: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warningFlags.size(); i++) {
        ss << this->warningFlags[i];
        if (i < this->warningFlags.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void WaitToClearWarningsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<WaitToClearWarningsRequest>();
}

std::string WaitToClearWarningsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
