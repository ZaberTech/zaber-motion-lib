// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/wait_to_clear_warnings_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(WaitToClearWarningsRequest, interfaceId, device, axis, timeout, warningFlags)

} // namespace requests
} // namespace motion
} // namespace zaber
