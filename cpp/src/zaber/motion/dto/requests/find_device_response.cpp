// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/find_device_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

FindDeviceResponse::FindDeviceResponse() { }

FindDeviceResponse::FindDeviceResponse(
    int p_address
) :
    address(p_address)
{ }

bool FindDeviceResponse::operator==(const FindDeviceResponse& other) const {
    return std::tie(
    address
    ) == std::tie(
    other.address
    );
}

int FindDeviceResponse::getAddress() const {
    return address;
}
void FindDeviceResponse::setAddress(int p_address) {
    address = p_address;
}

std::string FindDeviceResponse::toString() const {
    std::stringstream ss;
    ss << "FindDeviceResponse { ";
    ss << "address: ";
    ss << this->address;
    ss << " }";
    return ss.str();
}

void FindDeviceResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<FindDeviceResponse>();
}

std::string FindDeviceResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
