// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/generic_binary_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

GenericBinaryRequest::GenericBinaryRequest() { }

GenericBinaryRequest::GenericBinaryRequest(
    int p_interfaceId,
    int p_device,
    binary::CommandCode p_command,
    int p_data,
    bool p_checkErrors,
    double p_timeout
) :
    interfaceId(p_interfaceId),
    device(p_device),
    command(std::move(p_command)),
    data(p_data),
    checkErrors(p_checkErrors),
    timeout(p_timeout)
{ }

bool GenericBinaryRequest::operator==(const GenericBinaryRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    command,
    data,
    checkErrors,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.command,
    other.data,
    other.checkErrors,
    other.timeout
    );
}

int GenericBinaryRequest::getInterfaceId() const {
    return interfaceId;
}
void GenericBinaryRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int GenericBinaryRequest::getDevice() const {
    return device;
}
void GenericBinaryRequest::setDevice(int p_device) {
    device = p_device;
}

binary::CommandCode const& GenericBinaryRequest::getCommand() const {
    return command;
}
void GenericBinaryRequest::setCommand(binary::CommandCode p_command) {
    command = std::move(p_command);
}

int GenericBinaryRequest::getData() const {
    return data;
}
void GenericBinaryRequest::setData(int p_data) {
    data = p_data;
}

bool GenericBinaryRequest::getCheckErrors() const {
    return checkErrors;
}
void GenericBinaryRequest::setCheckErrors(bool p_checkErrors) {
    checkErrors = p_checkErrors;
}

double GenericBinaryRequest::getTimeout() const {
    return timeout;
}
void GenericBinaryRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

std::string GenericBinaryRequest::toString() const {
    std::stringstream ss;
    ss << "GenericBinaryRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "command: ";
    ss << CommandCode_toString(this->command);
    ss << ", ";
    ss << "data: ";
    ss << this->data;
    ss << ", ";
    ss << "checkErrors: ";
    ss << this->checkErrors;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void GenericBinaryRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GenericBinaryRequest>();
}

std::string GenericBinaryRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
