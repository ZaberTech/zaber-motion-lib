// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/binary_device_move_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

BinaryDeviceMoveRequest::BinaryDeviceMoveRequest() { }

BinaryDeviceMoveRequest::BinaryDeviceMoveRequest(
    int p_interfaceId,
    int p_device,
    double p_timeout,
    AxisMoveType p_type,
    double p_arg,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    timeout(p_timeout),
    type(std::move(p_type)),
    arg(p_arg),
    unit(p_unit)
{ }

bool BinaryDeviceMoveRequest::operator==(const BinaryDeviceMoveRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    timeout,
    type,
    arg,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.timeout,
    other.type,
    other.arg,
    other.unit
    );
}

int BinaryDeviceMoveRequest::getInterfaceId() const {
    return interfaceId;
}
void BinaryDeviceMoveRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int BinaryDeviceMoveRequest::getDevice() const {
    return device;
}
void BinaryDeviceMoveRequest::setDevice(int p_device) {
    device = p_device;
}

double BinaryDeviceMoveRequest::getTimeout() const {
    return timeout;
}
void BinaryDeviceMoveRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

AxisMoveType const& BinaryDeviceMoveRequest::getType() const {
    return type;
}
void BinaryDeviceMoveRequest::setType(AxisMoveType p_type) {
    type = std::move(p_type);
}

double BinaryDeviceMoveRequest::getArg() const {
    return arg;
}
void BinaryDeviceMoveRequest::setArg(double p_arg) {
    arg = p_arg;
}

Units BinaryDeviceMoveRequest::getUnit() const {
    return unit;
}
void BinaryDeviceMoveRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string BinaryDeviceMoveRequest::toString() const {
    std::stringstream ss;
    ss << "BinaryDeviceMoveRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << ", ";
    ss << "type: ";
    ss << AxisMoveType_toString(this->type);
    ss << ", ";
    ss << "arg: ";
    ss << this->arg;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void BinaryDeviceMoveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryDeviceMoveRequest>();
}

std::string BinaryDeviceMoveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
