// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/prepare_command_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

PrepareCommandRequest::PrepareCommandRequest() { }

PrepareCommandRequest::PrepareCommandRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_commandTemplate,
    std::vector<Measurement> p_parameters
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    commandTemplate(std::move(p_commandTemplate)),
    parameters(std::move(p_parameters))
{ }

bool PrepareCommandRequest::operator==(const PrepareCommandRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    commandTemplate,
    parameters
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.commandTemplate,
    other.parameters
    );
}

int PrepareCommandRequest::getInterfaceId() const {
    return interfaceId;
}
void PrepareCommandRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int PrepareCommandRequest::getDevice() const {
    return device;
}
void PrepareCommandRequest::setDevice(int p_device) {
    device = p_device;
}

int PrepareCommandRequest::getAxis() const {
    return axis;
}
void PrepareCommandRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& PrepareCommandRequest::getCommandTemplate() const {
    return commandTemplate;
}
void PrepareCommandRequest::setCommandTemplate(std::string p_commandTemplate) {
    commandTemplate = std::move(p_commandTemplate);
}

std::vector<Measurement> const& PrepareCommandRequest::getParameters() const {
    return parameters;
}
void PrepareCommandRequest::setParameters(std::vector<Measurement> p_parameters) {
    parameters = std::move(p_parameters);
}

std::string PrepareCommandRequest::toString() const {
    std::stringstream ss;
    ss << "PrepareCommandRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "commandTemplate: ";
    ss << this->commandTemplate;
    ss << ", ";
    ss << "parameters: ";
    ss << "[ ";
    for (size_t i = 0; i < this->parameters.size(); i++) {
        ss << this->parameters[i].toString();
        if (i < this->parameters.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void PrepareCommandRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PrepareCommandRequest>();
}

std::string PrepareCommandRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
