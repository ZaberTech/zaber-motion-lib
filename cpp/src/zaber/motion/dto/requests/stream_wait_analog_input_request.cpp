// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_wait_analog_input_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamWaitAnalogInputRequest::StreamWaitAnalogInputRequest() { }

StreamWaitAnalogInputRequest::StreamWaitAnalogInputRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_channelNumber,
    std::string p_condition,
    double p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    channelNumber(p_channelNumber),
    condition(std::move(p_condition)),
    value(p_value)
{ }

bool StreamWaitAnalogInputRequest::operator==(const StreamWaitAnalogInputRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    channelNumber,
    condition,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.channelNumber,
    other.condition,
    other.value
    );
}

int StreamWaitAnalogInputRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamWaitAnalogInputRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamWaitAnalogInputRequest::getDevice() const {
    return device;
}
void StreamWaitAnalogInputRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamWaitAnalogInputRequest::getStreamId() const {
    return streamId;
}
void StreamWaitAnalogInputRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamWaitAnalogInputRequest::getPvt() const {
    return pvt;
}
void StreamWaitAnalogInputRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamWaitAnalogInputRequest::getChannelNumber() const {
    return channelNumber;
}
void StreamWaitAnalogInputRequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string const& StreamWaitAnalogInputRequest::getCondition() const {
    return condition;
}
void StreamWaitAnalogInputRequest::setCondition(std::string p_condition) {
    condition = std::move(p_condition);
}

double StreamWaitAnalogInputRequest::getValue() const {
    return value;
}
void StreamWaitAnalogInputRequest::setValue(double p_value) {
    value = p_value;
}

std::string StreamWaitAnalogInputRequest::toString() const {
    std::stringstream ss;
    ss << "StreamWaitAnalogInputRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << ", ";
    ss << "condition: ";
    ss << this->condition;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void StreamWaitAnalogInputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamWaitAnalogInputRequest>();
}

std::string StreamWaitAnalogInputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
