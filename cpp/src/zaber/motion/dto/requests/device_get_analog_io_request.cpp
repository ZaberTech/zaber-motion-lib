// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_analog_io_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetAnalogIORequest::DeviceGetAnalogIORequest() { }

DeviceGetAnalogIORequest::DeviceGetAnalogIORequest(
    int p_interfaceId,
    int p_device,
    std::string p_channelType,
    int p_channelNumber
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelType(std::move(p_channelType)),
    channelNumber(p_channelNumber)
{ }

bool DeviceGetAnalogIORequest::operator==(const DeviceGetAnalogIORequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelType,
    channelNumber
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelType,
    other.channelNumber
    );
}

int DeviceGetAnalogIORequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetAnalogIORequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetAnalogIORequest::getDevice() const {
    return device;
}
void DeviceGetAnalogIORequest::setDevice(int p_device) {
    device = p_device;
}

std::string const& DeviceGetAnalogIORequest::getChannelType() const {
    return channelType;
}
void DeviceGetAnalogIORequest::setChannelType(std::string p_channelType) {
    channelType = std::move(p_channelType);
}

int DeviceGetAnalogIORequest::getChannelNumber() const {
    return channelNumber;
}
void DeviceGetAnalogIORequest::setChannelNumber(int p_channelNumber) {
    channelNumber = p_channelNumber;
}

std::string DeviceGetAnalogIORequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetAnalogIORequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelType: ";
    ss << this->channelType;
    ss << ", ";
    ss << "channelNumber: ";
    ss << this->channelNumber;
    ss << " }";
    return ss.str();
}

void DeviceGetAnalogIORequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetAnalogIORequest>();
}

std::string DeviceGetAnalogIORequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
