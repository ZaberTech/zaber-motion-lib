// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/custom_interface_read_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CustomInterfaceReadRequest::CustomInterfaceReadRequest() { }

CustomInterfaceReadRequest::CustomInterfaceReadRequest(
    int p_transportId
) :
    transportId(p_transportId)
{ }

bool CustomInterfaceReadRequest::operator==(const CustomInterfaceReadRequest& other) const {
    return std::tie(
    transportId
    ) == std::tie(
    other.transportId
    );
}

int CustomInterfaceReadRequest::getTransportId() const {
    return transportId;
}
void CustomInterfaceReadRequest::setTransportId(int p_transportId) {
    transportId = p_transportId;
}

std::string CustomInterfaceReadRequest::toString() const {
    std::stringstream ss;
    ss << "CustomInterfaceReadRequest { ";
    ss << "transportId: ";
    ss << this->transportId;
    ss << " }";
    return ss.str();
}

void CustomInterfaceReadRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CustomInterfaceReadRequest>();
}

std::string CustomInterfaceReadRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
