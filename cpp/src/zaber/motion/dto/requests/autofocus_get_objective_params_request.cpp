// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/autofocus_get_objective_params_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AutofocusGetObjectiveParamsRequest::AutofocusGetObjectiveParamsRequest() { }

AutofocusGetObjectiveParamsRequest::AutofocusGetObjectiveParamsRequest(
    int p_providerId,
    int p_interfaceId,
    int p_focusAddress,
    int p_focusAxis,
    int p_turretAddress,
    int p_objective
) :
    providerId(p_providerId),
    interfaceId(p_interfaceId),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    turretAddress(p_turretAddress),
    objective(p_objective)
{ }

bool AutofocusGetObjectiveParamsRequest::operator==(const AutofocusGetObjectiveParamsRequest& other) const {
    return std::tie(
    providerId,
    interfaceId,
    focusAddress,
    focusAxis,
    turretAddress,
    objective
    ) == std::tie(
    other.providerId,
    other.interfaceId,
    other.focusAddress,
    other.focusAxis,
    other.turretAddress,
    other.objective
    );
}

int AutofocusGetObjectiveParamsRequest::getProviderId() const {
    return providerId;
}
void AutofocusGetObjectiveParamsRequest::setProviderId(int p_providerId) {
    providerId = p_providerId;
}

int AutofocusGetObjectiveParamsRequest::getInterfaceId() const {
    return interfaceId;
}
void AutofocusGetObjectiveParamsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int AutofocusGetObjectiveParamsRequest::getFocusAddress() const {
    return focusAddress;
}
void AutofocusGetObjectiveParamsRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int AutofocusGetObjectiveParamsRequest::getFocusAxis() const {
    return focusAxis;
}
void AutofocusGetObjectiveParamsRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

int AutofocusGetObjectiveParamsRequest::getTurretAddress() const {
    return turretAddress;
}
void AutofocusGetObjectiveParamsRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

int AutofocusGetObjectiveParamsRequest::getObjective() const {
    return objective;
}
void AutofocusGetObjectiveParamsRequest::setObjective(int p_objective) {
    objective = p_objective;
}

std::string AutofocusGetObjectiveParamsRequest::toString() const {
    std::stringstream ss;
    ss << "AutofocusGetObjectiveParamsRequest { ";
    ss << "providerId: ";
    ss << this->providerId;
    ss << ", ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "objective: ";
    ss << this->objective;
    ss << " }";
    return ss.str();
}

void AutofocusGetObjectiveParamsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusGetObjectiveParamsRequest>();
}

std::string AutofocusGetObjectiveParamsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
