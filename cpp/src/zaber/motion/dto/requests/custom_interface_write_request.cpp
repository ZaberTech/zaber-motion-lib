// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/custom_interface_write_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

CustomInterfaceWriteRequest::CustomInterfaceWriteRequest() { }

CustomInterfaceWriteRequest::CustomInterfaceWriteRequest(
    int p_transportId,
    std::string p_message
) :
    transportId(p_transportId),
    message(std::move(p_message))
{ }

bool CustomInterfaceWriteRequest::operator==(const CustomInterfaceWriteRequest& other) const {
    return std::tie(
    transportId,
    message
    ) == std::tie(
    other.transportId,
    other.message
    );
}

int CustomInterfaceWriteRequest::getTransportId() const {
    return transportId;
}
void CustomInterfaceWriteRequest::setTransportId(int p_transportId) {
    transportId = p_transportId;
}

std::string const& CustomInterfaceWriteRequest::getMessage() const {
    return message;
}
void CustomInterfaceWriteRequest::setMessage(std::string p_message) {
    message = std::move(p_message);
}

std::string CustomInterfaceWriteRequest::toString() const {
    std::stringstream ss;
    ss << "CustomInterfaceWriteRequest { ";
    ss << "transportId: ";
    ss << this->transportId;
    ss << ", ";
    ss << "message: ";
    ss << this->message;
    ss << " }";
    return ss.str();
}

void CustomInterfaceWriteRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CustomInterfaceWriteRequest>();
}

std::string CustomInterfaceWriteRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
