// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/generic_command_response_collection.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/response.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GenericCommandResponseCollection, responses)

} // namespace requests
} // namespace motion
} // namespace zaber
