// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_all_digital_io_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetAllDigitalIORequest::DeviceGetAllDigitalIORequest() { }

DeviceGetAllDigitalIORequest::DeviceGetAllDigitalIORequest(
    int p_interfaceId,
    int p_device,
    std::string p_channelType
) :
    interfaceId(p_interfaceId),
    device(p_device),
    channelType(std::move(p_channelType))
{ }

bool DeviceGetAllDigitalIORequest::operator==(const DeviceGetAllDigitalIORequest& other) const {
    return std::tie(
    interfaceId,
    device,
    channelType
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.channelType
    );
}

int DeviceGetAllDigitalIORequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetAllDigitalIORequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetAllDigitalIORequest::getDevice() const {
    return device;
}
void DeviceGetAllDigitalIORequest::setDevice(int p_device) {
    device = p_device;
}

std::string const& DeviceGetAllDigitalIORequest::getChannelType() const {
    return channelType;
}
void DeviceGetAllDigitalIORequest::setChannelType(std::string p_channelType) {
    channelType = std::move(p_channelType);
}

std::string DeviceGetAllDigitalIORequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetAllDigitalIORequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "channelType: ";
    ss << this->channelType;
    ss << " }";
    return ss.str();
}

void DeviceGetAllDigitalIORequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetAllDigitalIORequest>();
}

std::string DeviceGetAllDigitalIORequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
