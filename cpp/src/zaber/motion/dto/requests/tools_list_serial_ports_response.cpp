// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/tools_list_serial_ports_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ToolsListSerialPortsResponse::ToolsListSerialPortsResponse() { }

ToolsListSerialPortsResponse::ToolsListSerialPortsResponse(
    std::vector<std::string> p_ports
) :
    ports(std::move(p_ports))
{ }

bool ToolsListSerialPortsResponse::operator==(const ToolsListSerialPortsResponse& other) const {
    return std::tie(
    ports
    ) == std::tie(
    other.ports
    );
}

std::vector<std::string> const& ToolsListSerialPortsResponse::getPorts() const {
    return ports;
}
void ToolsListSerialPortsResponse::setPorts(std::vector<std::string> p_ports) {
    ports = std::move(p_ports);
}

std::string ToolsListSerialPortsResponse::toString() const {
    std::stringstream ss;
    ss << "ToolsListSerialPortsResponse { ";
    ss << "ports: ";
    ss << "[ ";
    for (size_t i = 0; i < this->ports.size(); i++) {
        ss << this->ports[i];
        if (i < this->ports.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void ToolsListSerialPortsResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ToolsListSerialPortsResponse>();
}

std::string ToolsListSerialPortsResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
