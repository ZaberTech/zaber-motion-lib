// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/get_setting_results.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/get_setting_result.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GetSettingResults, results)

} // namespace requests
} // namespace motion
} // namespace zaber
