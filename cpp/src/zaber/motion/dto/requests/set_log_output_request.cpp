// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/set_log_output_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

SetLogOutputRequest::SetLogOutputRequest() { }

SetLogOutputRequest::SetLogOutputRequest(
    LogOutputMode p_mode,
    std::optional<std::string> p_filePath
) :
    mode(std::move(p_mode)),
    filePath(std::move(p_filePath))
{ }

SetLogOutputRequest::SetLogOutputRequest(
    LogOutputMode p_mode
) :
    mode(std::move(p_mode))
{ }

bool SetLogOutputRequest::operator==(const SetLogOutputRequest& other) const {
    return std::tie(
    mode,
    filePath
    ) == std::tie(
    other.mode,
    other.filePath
    );
}

LogOutputMode const& SetLogOutputRequest::getMode() const {
    return mode;
}
void SetLogOutputRequest::setMode(LogOutputMode p_mode) {
    mode = std::move(p_mode);
}

std::optional<std::string> const& SetLogOutputRequest::getFilePath() const {
    return filePath;
}
void SetLogOutputRequest::setFilePath(std::optional<std::string> p_filePath) {
    filePath = std::move(p_filePath);
}

std::string SetLogOutputRequest::toString() const {
    std::stringstream ss;
    ss << "SetLogOutputRequest { ";
    ss << "mode: ";
    ss << LogOutputMode_toString(this->mode);
    ss << ", ";
    ss << "filePath: ";
    if (this->filePath.has_value()) {
        ss << this->filePath.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void SetLogOutputRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetLogOutputRequest>();
}

std::string SetLogOutputRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
