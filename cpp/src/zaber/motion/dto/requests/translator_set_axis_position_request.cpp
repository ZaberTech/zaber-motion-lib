// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_set_axis_position_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorSetAxisPositionRequest::TranslatorSetAxisPositionRequest() { }

TranslatorSetAxisPositionRequest::TranslatorSetAxisPositionRequest(
    int p_translatorId,
    std::string p_axis,
    double p_position,
    Units p_unit
) :
    translatorId(p_translatorId),
    axis(std::move(p_axis)),
    position(p_position),
    unit(p_unit)
{ }

bool TranslatorSetAxisPositionRequest::operator==(const TranslatorSetAxisPositionRequest& other) const {
    return std::tie(
    translatorId,
    axis,
    position,
    unit
    ) == std::tie(
    other.translatorId,
    other.axis,
    other.position,
    other.unit
    );
}

int TranslatorSetAxisPositionRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorSetAxisPositionRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

std::string const& TranslatorSetAxisPositionRequest::getAxis() const {
    return axis;
}
void TranslatorSetAxisPositionRequest::setAxis(std::string p_axis) {
    axis = std::move(p_axis);
}

double TranslatorSetAxisPositionRequest::getPosition() const {
    return position;
}
void TranslatorSetAxisPositionRequest::setPosition(double p_position) {
    position = p_position;
}

Units TranslatorSetAxisPositionRequest::getUnit() const {
    return unit;
}
void TranslatorSetAxisPositionRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TranslatorSetAxisPositionRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorSetAxisPositionRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "position: ";
    ss << this->position;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TranslatorSetAxisPositionRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorSetAxisPositionRequest>();
}

std::string TranslatorSetAxisPositionRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
