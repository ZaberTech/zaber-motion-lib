// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_set_storage_number_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceSetStorageNumberRequest::DeviceSetStorageNumberRequest() { }

DeviceSetStorageNumberRequest::DeviceSetStorageNumberRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::string p_key,
    double p_value
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    key(std::move(p_key)),
    value(p_value)
{ }

bool DeviceSetStorageNumberRequest::operator==(const DeviceSetStorageNumberRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    key,
    value
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.key,
    other.value
    );
}

int DeviceSetStorageNumberRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceSetStorageNumberRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceSetStorageNumberRequest::getDevice() const {
    return device;
}
void DeviceSetStorageNumberRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceSetStorageNumberRequest::getAxis() const {
    return axis;
}
void DeviceSetStorageNumberRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& DeviceSetStorageNumberRequest::getKey() const {
    return key;
}
void DeviceSetStorageNumberRequest::setKey(std::string p_key) {
    key = std::move(p_key);
}

double DeviceSetStorageNumberRequest::getValue() const {
    return value;
}
void DeviceSetStorageNumberRequest::setValue(double p_value) {
    value = p_value;
}

std::string DeviceSetStorageNumberRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceSetStorageNumberRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "key: ";
    ss << this->key;
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void DeviceSetStorageNumberRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceSetStorageNumberRequest>();
}

std::string DeviceSetStorageNumberRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
