// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/autofocus_set_objective_params_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AutofocusSetObjectiveParamsRequest::AutofocusSetObjectiveParamsRequest() { }

AutofocusSetObjectiveParamsRequest::AutofocusSetObjectiveParamsRequest(
    int p_providerId,
    int p_interfaceId,
    int p_focusAddress,
    int p_focusAxis,
    int p_turretAddress,
    int p_objective,
    std::vector<NamedParameter> p_parameters
) :
    providerId(p_providerId),
    interfaceId(p_interfaceId),
    focusAddress(p_focusAddress),
    focusAxis(p_focusAxis),
    turretAddress(p_turretAddress),
    objective(p_objective),
    parameters(std::move(p_parameters))
{ }

bool AutofocusSetObjectiveParamsRequest::operator==(const AutofocusSetObjectiveParamsRequest& other) const {
    return std::tie(
    providerId,
    interfaceId,
    focusAddress,
    focusAxis,
    turretAddress,
    objective,
    parameters
    ) == std::tie(
    other.providerId,
    other.interfaceId,
    other.focusAddress,
    other.focusAxis,
    other.turretAddress,
    other.objective,
    other.parameters
    );
}

int AutofocusSetObjectiveParamsRequest::getProviderId() const {
    return providerId;
}
void AutofocusSetObjectiveParamsRequest::setProviderId(int p_providerId) {
    providerId = p_providerId;
}

int AutofocusSetObjectiveParamsRequest::getInterfaceId() const {
    return interfaceId;
}
void AutofocusSetObjectiveParamsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int AutofocusSetObjectiveParamsRequest::getFocusAddress() const {
    return focusAddress;
}
void AutofocusSetObjectiveParamsRequest::setFocusAddress(int p_focusAddress) {
    focusAddress = p_focusAddress;
}

int AutofocusSetObjectiveParamsRequest::getFocusAxis() const {
    return focusAxis;
}
void AutofocusSetObjectiveParamsRequest::setFocusAxis(int p_focusAxis) {
    focusAxis = p_focusAxis;
}

int AutofocusSetObjectiveParamsRequest::getTurretAddress() const {
    return turretAddress;
}
void AutofocusSetObjectiveParamsRequest::setTurretAddress(int p_turretAddress) {
    turretAddress = p_turretAddress;
}

int AutofocusSetObjectiveParamsRequest::getObjective() const {
    return objective;
}
void AutofocusSetObjectiveParamsRequest::setObjective(int p_objective) {
    objective = p_objective;
}

std::vector<NamedParameter> const& AutofocusSetObjectiveParamsRequest::getParameters() const {
    return parameters;
}
void AutofocusSetObjectiveParamsRequest::setParameters(std::vector<NamedParameter> p_parameters) {
    parameters = std::move(p_parameters);
}

std::string AutofocusSetObjectiveParamsRequest::toString() const {
    std::stringstream ss;
    ss << "AutofocusSetObjectiveParamsRequest { ";
    ss << "providerId: ";
    ss << this->providerId;
    ss << ", ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "focusAddress: ";
    ss << this->focusAddress;
    ss << ", ";
    ss << "focusAxis: ";
    ss << this->focusAxis;
    ss << ", ";
    ss << "turretAddress: ";
    ss << this->turretAddress;
    ss << ", ";
    ss << "objective: ";
    ss << this->objective;
    ss << ", ";
    ss << "parameters: ";
    ss << "[ ";
    for (size_t i = 0; i < this->parameters.size(); i++) {
        ss << this->parameters[i].toString();
        if (i < this->parameters.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void AutofocusSetObjectiveParamsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AutofocusSetObjectiveParamsRequest>();
}

std::string AutofocusSetObjectiveParamsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
