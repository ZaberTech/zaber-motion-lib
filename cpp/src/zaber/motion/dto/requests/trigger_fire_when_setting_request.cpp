// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_fire_when_setting_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerFireWhenSettingRequest::TriggerFireWhenSettingRequest() { }

TriggerFireWhenSettingRequest::TriggerFireWhenSettingRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    int p_axis,
    std::string p_setting,
    ascii::TriggerCondition p_triggerCondition,
    double p_value,
    Units p_unit
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    axis(p_axis),
    setting(std::move(p_setting)),
    triggerCondition(std::move(p_triggerCondition)),
    value(p_value),
    unit(p_unit)
{ }

bool TriggerFireWhenSettingRequest::operator==(const TriggerFireWhenSettingRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    axis,
    setting,
    triggerCondition,
    value,
    unit
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.axis,
    other.setting,
    other.triggerCondition,
    other.value,
    other.unit
    );
}

int TriggerFireWhenSettingRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerFireWhenSettingRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerFireWhenSettingRequest::getDevice() const {
    return device;
}
void TriggerFireWhenSettingRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerFireWhenSettingRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerFireWhenSettingRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

int TriggerFireWhenSettingRequest::getAxis() const {
    return axis;
}
void TriggerFireWhenSettingRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::string const& TriggerFireWhenSettingRequest::getSetting() const {
    return setting;
}
void TriggerFireWhenSettingRequest::setSetting(std::string p_setting) {
    setting = std::move(p_setting);
}

ascii::TriggerCondition const& TriggerFireWhenSettingRequest::getTriggerCondition() const {
    return triggerCondition;
}
void TriggerFireWhenSettingRequest::setTriggerCondition(ascii::TriggerCondition p_triggerCondition) {
    triggerCondition = std::move(p_triggerCondition);
}

double TriggerFireWhenSettingRequest::getValue() const {
    return value;
}
void TriggerFireWhenSettingRequest::setValue(double p_value) {
    value = p_value;
}

Units TriggerFireWhenSettingRequest::getUnit() const {
    return unit;
}
void TriggerFireWhenSettingRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TriggerFireWhenSettingRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerFireWhenSettingRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "setting: ";
    ss << this->setting;
    ss << ", ";
    ss << "triggerCondition: ";
    ss << TriggerCondition_toString(this->triggerCondition);
    ss << ", ";
    ss << "value: ";
    ss << this->value;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TriggerFireWhenSettingRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerFireWhenSettingRequest>();
}

std::string TriggerFireWhenSettingRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
