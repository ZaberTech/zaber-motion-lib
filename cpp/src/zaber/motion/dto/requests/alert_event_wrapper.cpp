// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/alert_event_wrapper.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AlertEventWrapper::AlertEventWrapper() { }

AlertEventWrapper::AlertEventWrapper(
    int p_interfaceId,
    ascii::AlertEvent p_alert
) :
    interfaceId(p_interfaceId),
    alert(std::move(p_alert))
{ }

bool AlertEventWrapper::operator==(const AlertEventWrapper& other) const {
    return std::tie(
    interfaceId,
    alert
    ) == std::tie(
    other.interfaceId,
    other.alert
    );
}

int AlertEventWrapper::getInterfaceId() const {
    return interfaceId;
}
void AlertEventWrapper::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

ascii::AlertEvent const& AlertEventWrapper::getAlert() const {
    return alert;
}
void AlertEventWrapper::setAlert(ascii::AlertEvent p_alert) {
    alert = std::move(p_alert);
}

std::string AlertEventWrapper::toString() const {
    std::stringstream ss;
    ss << "AlertEventWrapper { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "alert: ";
    ss << this->alert.toString();
    ss << " }";
    return ss.str();
}

void AlertEventWrapper::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AlertEventWrapper>();
}

std::string AlertEventWrapper::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
