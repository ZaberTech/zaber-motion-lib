// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/trigger_on_fire_set_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/units.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TriggerOnFireSetRequest, interfaceId, device, triggerNumber, action, axis, setting, operation, value, unit)

} // namespace requests
} // namespace motion
} // namespace zaber
