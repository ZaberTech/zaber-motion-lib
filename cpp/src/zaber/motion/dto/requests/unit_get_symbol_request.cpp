// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/unit_get_symbol_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

UnitGetSymbolRequest::UnitGetSymbolRequest() { }

UnitGetSymbolRequest::UnitGetSymbolRequest(
    Units p_unit
) :
    unit(p_unit)
{ }

bool UnitGetSymbolRequest::operator==(const UnitGetSymbolRequest& other) const {
    return std::tie(
    unit
    ) == std::tie(
    other.unit
    );
}

Units UnitGetSymbolRequest::getUnit() const {
    return unit;
}
void UnitGetSymbolRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string UnitGetSymbolRequest::toString() const {
    std::stringstream ss;
    ss << "UnitGetSymbolRequest { ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void UnitGetSymbolRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<UnitGetSymbolRequest>();
}

std::string UnitGetSymbolRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
