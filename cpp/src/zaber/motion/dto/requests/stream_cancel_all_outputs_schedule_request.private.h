// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/stream_cancel_all_outputs_schedule_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamCancelAllOutputsScheduleRequest, interfaceId, device, streamId, pvt, analog, channels)

} // namespace requests
} // namespace motion
} // namespace zaber
