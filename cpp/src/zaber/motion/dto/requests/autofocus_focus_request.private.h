// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/autofocus_focus_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AutofocusFocusRequest, providerId, interfaceId, focusAddress, focusAxis, turretAddress, scan, once, timeout)

} // namespace requests
} // namespace motion
} // namespace zaber
