// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_buffer_get_content_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamBufferGetContentRequest::StreamBufferGetContentRequest() { }

StreamBufferGetContentRequest::StreamBufferGetContentRequest(
    int p_interfaceId,
    int p_device,
    int p_bufferId,
    bool p_pvt
) :
    interfaceId(p_interfaceId),
    device(p_device),
    bufferId(p_bufferId),
    pvt(p_pvt)
{ }

bool StreamBufferGetContentRequest::operator==(const StreamBufferGetContentRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    bufferId,
    pvt
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.bufferId,
    other.pvt
    );
}

int StreamBufferGetContentRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamBufferGetContentRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamBufferGetContentRequest::getDevice() const {
    return device;
}
void StreamBufferGetContentRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamBufferGetContentRequest::getBufferId() const {
    return bufferId;
}
void StreamBufferGetContentRequest::setBufferId(int p_bufferId) {
    bufferId = p_bufferId;
}

bool StreamBufferGetContentRequest::getPvt() const {
    return pvt;
}
void StreamBufferGetContentRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::string StreamBufferGetContentRequest::toString() const {
    std::stringstream ss;
    ss << "StreamBufferGetContentRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "bufferId: ";
    ss << this->bufferId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << " }";
    return ss.str();
}

void StreamBufferGetContentRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamBufferGetContentRequest>();
}

std::string StreamBufferGetContentRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
