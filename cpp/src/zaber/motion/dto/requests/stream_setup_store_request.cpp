// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_setup_store_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetupStoreRequest::StreamSetupStoreRequest() { }

StreamSetupStoreRequest::StreamSetupStoreRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    int p_streamBuffer,
    int p_pvtBuffer,
    std::vector<int> p_axes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    streamBuffer(p_streamBuffer),
    pvtBuffer(p_pvtBuffer),
    axes(std::move(p_axes))
{ }

bool StreamSetupStoreRequest::operator==(const StreamSetupStoreRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    streamBuffer,
    pvtBuffer,
    axes
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.streamBuffer,
    other.pvtBuffer,
    other.axes
    );
}

int StreamSetupStoreRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetupStoreRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetupStoreRequest::getDevice() const {
    return device;
}
void StreamSetupStoreRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetupStoreRequest::getStreamId() const {
    return streamId;
}
void StreamSetupStoreRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetupStoreRequest::getPvt() const {
    return pvt;
}
void StreamSetupStoreRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

int StreamSetupStoreRequest::getStreamBuffer() const {
    return streamBuffer;
}
void StreamSetupStoreRequest::setStreamBuffer(int p_streamBuffer) {
    streamBuffer = p_streamBuffer;
}

int StreamSetupStoreRequest::getPvtBuffer() const {
    return pvtBuffer;
}
void StreamSetupStoreRequest::setPvtBuffer(int p_pvtBuffer) {
    pvtBuffer = p_pvtBuffer;
}

std::vector<int> const& StreamSetupStoreRequest::getAxes() const {
    return axes;
}
void StreamSetupStoreRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::string StreamSetupStoreRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetupStoreRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "streamBuffer: ";
    ss << this->streamBuffer;
    ss << ", ";
    ss << "pvtBuffer: ";
    ss << this->pvtBuffer;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetupStoreRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetupStoreRequest>();
}

std::string StreamSetupStoreRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
