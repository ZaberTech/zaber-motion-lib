// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/trigger_clear_action_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TriggerClearActionRequest::TriggerClearActionRequest() { }

TriggerClearActionRequest::TriggerClearActionRequest(
    int p_interfaceId,
    int p_device,
    int p_triggerNumber,
    ascii::TriggerAction p_action
) :
    interfaceId(p_interfaceId),
    device(p_device),
    triggerNumber(p_triggerNumber),
    action(std::move(p_action))
{ }

bool TriggerClearActionRequest::operator==(const TriggerClearActionRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    triggerNumber,
    action
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.triggerNumber,
    other.action
    );
}

int TriggerClearActionRequest::getInterfaceId() const {
    return interfaceId;
}
void TriggerClearActionRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int TriggerClearActionRequest::getDevice() const {
    return device;
}
void TriggerClearActionRequest::setDevice(int p_device) {
    device = p_device;
}

int TriggerClearActionRequest::getTriggerNumber() const {
    return triggerNumber;
}
void TriggerClearActionRequest::setTriggerNumber(int p_triggerNumber) {
    triggerNumber = p_triggerNumber;
}

ascii::TriggerAction const& TriggerClearActionRequest::getAction() const {
    return action;
}
void TriggerClearActionRequest::setAction(ascii::TriggerAction p_action) {
    action = std::move(p_action);
}

std::string TriggerClearActionRequest::toString() const {
    std::stringstream ss;
    ss << "TriggerClearActionRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "triggerNumber: ";
    ss << this->triggerNumber;
    ss << ", ";
    ss << "action: ";
    ss << TriggerAction_toString(this->action);
    ss << " }";
    return ss.str();
}

void TriggerClearActionRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TriggerClearActionRequest>();
}

std::string TriggerClearActionRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
