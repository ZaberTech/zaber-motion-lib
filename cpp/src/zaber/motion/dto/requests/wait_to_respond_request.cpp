// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/wait_to_respond_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

WaitToRespondRequest::WaitToRespondRequest() { }

WaitToRespondRequest::WaitToRespondRequest(
    int p_interfaceId,
    int p_device,
    double p_timeout
) :
    interfaceId(p_interfaceId),
    device(p_device),
    timeout(p_timeout)
{ }

bool WaitToRespondRequest::operator==(const WaitToRespondRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    timeout
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.timeout
    );
}

int WaitToRespondRequest::getInterfaceId() const {
    return interfaceId;
}
void WaitToRespondRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int WaitToRespondRequest::getDevice() const {
    return device;
}
void WaitToRespondRequest::setDevice(int p_device) {
    device = p_device;
}

double WaitToRespondRequest::getTimeout() const {
    return timeout;
}
void WaitToRespondRequest::setTimeout(double p_timeout) {
    timeout = p_timeout;
}

std::string WaitToRespondRequest::toString() const {
    std::stringstream ss;
    ss << "WaitToRespondRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "timeout: ";
    ss << this->timeout;
    ss << " }";
    return ss.str();
}

void WaitToRespondRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<WaitToRespondRequest>();
}

std::string WaitToRespondRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
