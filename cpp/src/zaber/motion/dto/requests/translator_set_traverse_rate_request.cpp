// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/translator_set_traverse_rate_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TranslatorSetTraverseRateRequest::TranslatorSetTraverseRateRequest() { }

TranslatorSetTraverseRateRequest::TranslatorSetTraverseRateRequest(
    int p_translatorId,
    double p_traverseRate,
    Units p_unit
) :
    translatorId(p_translatorId),
    traverseRate(p_traverseRate),
    unit(p_unit)
{ }

bool TranslatorSetTraverseRateRequest::operator==(const TranslatorSetTraverseRateRequest& other) const {
    return std::tie(
    translatorId,
    traverseRate,
    unit
    ) == std::tie(
    other.translatorId,
    other.traverseRate,
    other.unit
    );
}

int TranslatorSetTraverseRateRequest::getTranslatorId() const {
    return translatorId;
}
void TranslatorSetTraverseRateRequest::setTranslatorId(int p_translatorId) {
    translatorId = p_translatorId;
}

double TranslatorSetTraverseRateRequest::getTraverseRate() const {
    return traverseRate;
}
void TranslatorSetTraverseRateRequest::setTraverseRate(double p_traverseRate) {
    traverseRate = p_traverseRate;
}

Units TranslatorSetTraverseRateRequest::getUnit() const {
    return unit;
}
void TranslatorSetTraverseRateRequest::setUnit(Units p_unit) {
    unit = p_unit;
}

std::string TranslatorSetTraverseRateRequest::toString() const {
    std::stringstream ss;
    ss << "TranslatorSetTraverseRateRequest { ";
    ss << "translatorId: ";
    ss << this->translatorId;
    ss << ", ";
    ss << "traverseRate: ";
    ss << this->traverseRate;
    ss << ", ";
    ss << "unit: ";
    ss << getUnitLongName(this->unit);
    ss << " }";
    return ss.str();
}

void TranslatorSetTraverseRateRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorSetTraverseRateRequest>();
}

std::string TranslatorSetTraverseRateRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
