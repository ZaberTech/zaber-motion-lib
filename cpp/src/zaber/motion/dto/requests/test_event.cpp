// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/test_event.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

TestEvent::TestEvent() { }

TestEvent::TestEvent(
    std::string p_data
) :
    data(std::move(p_data))
{ }

bool TestEvent::operator==(const TestEvent& other) const {
    return std::tie(
    data
    ) == std::tie(
    other.data
    );
}

std::string const& TestEvent::getData() const {
    return data;
}
void TestEvent::setData(std::string p_data) {
    data = std::move(p_data);
}

std::string TestEvent::toString() const {
    std::stringstream ss;
    ss << "TestEvent { ";
    ss << "data: ";
    ss << this->data;
    ss << " }";
    return ss.str();
}

void TestEvent::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TestEvent>();
}

std::string TestEvent::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
