// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_get_warnings_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceGetWarningsRequest::DeviceGetWarningsRequest() { }

DeviceGetWarningsRequest::DeviceGetWarningsRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    bool p_clear
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    clear(p_clear)
{ }

bool DeviceGetWarningsRequest::operator==(const DeviceGetWarningsRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    clear
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.clear
    );
}

int DeviceGetWarningsRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceGetWarningsRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceGetWarningsRequest::getDevice() const {
    return device;
}
void DeviceGetWarningsRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceGetWarningsRequest::getAxis() const {
    return axis;
}
void DeviceGetWarningsRequest::setAxis(int p_axis) {
    axis = p_axis;
}

bool DeviceGetWarningsRequest::getClear() const {
    return clear;
}
void DeviceGetWarningsRequest::setClear(bool p_clear) {
    clear = p_clear;
}

std::string DeviceGetWarningsRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceGetWarningsRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "clear: ";
    ss << this->clear;
    ss << " }";
    return ss.str();
}

void DeviceGetWarningsRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceGetWarningsRequest>();
}

std::string DeviceGetWarningsRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
