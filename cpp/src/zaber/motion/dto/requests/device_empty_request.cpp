// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceEmptyRequest::DeviceEmptyRequest() { }

DeviceEmptyRequest::DeviceEmptyRequest(
    int p_interfaceId,
    int p_device
) :
    interfaceId(p_interfaceId),
    device(p_device)
{ }

bool DeviceEmptyRequest::operator==(const DeviceEmptyRequest& other) const {
    return std::tie(
    interfaceId,
    device
    ) == std::tie(
    other.interfaceId,
    other.device
    );
}

int DeviceEmptyRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceEmptyRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceEmptyRequest::getDevice() const {
    return device;
}
void DeviceEmptyRequest::setDevice(int p_device) {
    device = p_device;
}

std::string DeviceEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceEmptyRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << " }";
    return ss.str();
}

void DeviceEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceEmptyRequest>();
}

std::string DeviceEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
