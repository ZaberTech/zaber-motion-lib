// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/stream_setup_live_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

StreamSetupLiveRequest::StreamSetupLiveRequest() { }

StreamSetupLiveRequest::StreamSetupLiveRequest(
    int p_interfaceId,
    int p_device,
    int p_streamId,
    bool p_pvt,
    std::vector<int> p_axes
) :
    interfaceId(p_interfaceId),
    device(p_device),
    streamId(p_streamId),
    pvt(p_pvt),
    axes(std::move(p_axes))
{ }

bool StreamSetupLiveRequest::operator==(const StreamSetupLiveRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    streamId,
    pvt,
    axes
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.streamId,
    other.pvt,
    other.axes
    );
}

int StreamSetupLiveRequest::getInterfaceId() const {
    return interfaceId;
}
void StreamSetupLiveRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int StreamSetupLiveRequest::getDevice() const {
    return device;
}
void StreamSetupLiveRequest::setDevice(int p_device) {
    device = p_device;
}

int StreamSetupLiveRequest::getStreamId() const {
    return streamId;
}
void StreamSetupLiveRequest::setStreamId(int p_streamId) {
    streamId = p_streamId;
}

bool StreamSetupLiveRequest::getPvt() const {
    return pvt;
}
void StreamSetupLiveRequest::setPvt(bool p_pvt) {
    pvt = p_pvt;
}

std::vector<int> const& StreamSetupLiveRequest::getAxes() const {
    return axes;
}
void StreamSetupLiveRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::string StreamSetupLiveRequest::toString() const {
    std::stringstream ss;
    ss << "StreamSetupLiveRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "streamId: ";
    ss << this->streamId;
    ss << ", ";
    ss << "pvt: ";
    ss << this->pvt;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void StreamSetupLiveRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamSetupLiveRequest>();
}

std::string StreamSetupLiveRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
