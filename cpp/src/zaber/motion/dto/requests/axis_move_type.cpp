// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axis_move_type.h"

namespace zaber {
namespace motion {
namespace requests {

std::string AxisMoveType_toString(AxisMoveType value) {
    switch (value) {
        case AxisMoveType::ABS: return "ABS";
        case AxisMoveType::REL: return "REL";
        case AxisMoveType::VEL: return "VEL";
        case AxisMoveType::MAX: return "MAX";
        case AxisMoveType::MIN: return "MIN";
        case AxisMoveType::INDEX: return "INDEX";
    }
    return "<Invalid value>";
}

} // namespace requests
} // namespace motion
} // namespace zaber
