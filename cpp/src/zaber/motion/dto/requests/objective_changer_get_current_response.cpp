// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/objective_changer_get_current_response.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

ObjectiveChangerGetCurrentResponse::ObjectiveChangerGetCurrentResponse() { }

ObjectiveChangerGetCurrentResponse::ObjectiveChangerGetCurrentResponse(
    int p_value
) :
    value(p_value)
{ }

bool ObjectiveChangerGetCurrentResponse::operator==(const ObjectiveChangerGetCurrentResponse& other) const {
    return std::tie(
    value
    ) == std::tie(
    other.value
    );
}

int ObjectiveChangerGetCurrentResponse::getValue() const {
    return value;
}
void ObjectiveChangerGetCurrentResponse::setValue(int p_value) {
    value = p_value;
}

std::string ObjectiveChangerGetCurrentResponse::toString() const {
    std::stringstream ss;
    ss << "ObjectiveChangerGetCurrentResponse { ";
    ss << "value: ";
    ss << this->value;
    ss << " }";
    return ss.str();
}

void ObjectiveChangerGetCurrentResponse::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<ObjectiveChangerGetCurrentResponse>();
}

std::string ObjectiveChangerGetCurrentResponse::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
