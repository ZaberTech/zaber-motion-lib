// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/set_servo_tuning_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/ascii/servo_tuning_param.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetServoTuningRequest, interfaceId, device, axis, paramset, tuningParams, setUnspecifiedToDefault)

} // namespace requests
} // namespace motion
} // namespace zaber
