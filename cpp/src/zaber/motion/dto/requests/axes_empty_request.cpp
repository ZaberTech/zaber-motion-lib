// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/axes_empty_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

AxesEmptyRequest::AxesEmptyRequest() { }

AxesEmptyRequest::AxesEmptyRequest(
    std::vector<int> p_interfaces,
    std::vector<int> p_devices,
    std::vector<int> p_axes
) :
    interfaces(std::move(p_interfaces)),
    devices(std::move(p_devices)),
    axes(std::move(p_axes))
{ }

bool AxesEmptyRequest::operator==(const AxesEmptyRequest& other) const {
    return std::tie(
    interfaces,
    devices,
    axes
    ) == std::tie(
    other.interfaces,
    other.devices,
    other.axes
    );
}

std::vector<int> const& AxesEmptyRequest::getInterfaces() const {
    return interfaces;
}
void AxesEmptyRequest::setInterfaces(std::vector<int> p_interfaces) {
    interfaces = std::move(p_interfaces);
}

std::vector<int> const& AxesEmptyRequest::getDevices() const {
    return devices;
}
void AxesEmptyRequest::setDevices(std::vector<int> p_devices) {
    devices = std::move(p_devices);
}

std::vector<int> const& AxesEmptyRequest::getAxes() const {
    return axes;
}
void AxesEmptyRequest::setAxes(std::vector<int> p_axes) {
    axes = std::move(p_axes);
}

std::string AxesEmptyRequest::toString() const {
    std::stringstream ss;
    ss << "AxesEmptyRequest { ";
    ss << "interfaces: ";
    ss << "[ ";
    for (size_t i = 0; i < this->interfaces.size(); i++) {
        ss << this->interfaces[i];
        if (i < this->interfaces.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "devices: ";
    ss << "[ ";
    for (size_t i = 0; i < this->devices.size(); i++) {
        ss << this->devices[i];
        if (i < this->devices.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i];
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void AxesEmptyRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxesEmptyRequest>();
}

std::string AxesEmptyRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
