// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/requests/open_binary_interface_request.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace requests {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(OpenBinaryInterfaceRequest, interfaceType, portName, baudRate, hostName, port, useMessageIds)

} // namespace requests
} // namespace motion
} // namespace zaber
