// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/requests/device_storage_list_keys_request.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace requests {

DeviceStorageListKeysRequest::DeviceStorageListKeysRequest() { }

DeviceStorageListKeysRequest::DeviceStorageListKeysRequest(
    int p_interfaceId,
    int p_device,
    int p_axis,
    std::optional<std::string> p_prefix
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis),
    prefix(std::move(p_prefix))
{ }

DeviceStorageListKeysRequest::DeviceStorageListKeysRequest(
    int p_interfaceId,
    int p_device,
    int p_axis
) :
    interfaceId(p_interfaceId),
    device(p_device),
    axis(p_axis)
{ }

bool DeviceStorageListKeysRequest::operator==(const DeviceStorageListKeysRequest& other) const {
    return std::tie(
    interfaceId,
    device,
    axis,
    prefix
    ) == std::tie(
    other.interfaceId,
    other.device,
    other.axis,
    other.prefix
    );
}

int DeviceStorageListKeysRequest::getInterfaceId() const {
    return interfaceId;
}
void DeviceStorageListKeysRequest::setInterfaceId(int p_interfaceId) {
    interfaceId = p_interfaceId;
}

int DeviceStorageListKeysRequest::getDevice() const {
    return device;
}
void DeviceStorageListKeysRequest::setDevice(int p_device) {
    device = p_device;
}

int DeviceStorageListKeysRequest::getAxis() const {
    return axis;
}
void DeviceStorageListKeysRequest::setAxis(int p_axis) {
    axis = p_axis;
}

std::optional<std::string> const& DeviceStorageListKeysRequest::getPrefix() const {
    return prefix;
}
void DeviceStorageListKeysRequest::setPrefix(std::optional<std::string> p_prefix) {
    prefix = std::move(p_prefix);
}

std::string DeviceStorageListKeysRequest::toString() const {
    std::stringstream ss;
    ss << "DeviceStorageListKeysRequest { ";
    ss << "interfaceId: ";
    ss << this->interfaceId;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << ", ";
    ss << "prefix: ";
    if (this->prefix.has_value()) {
        ss << this->prefix.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void DeviceStorageListKeysRequest::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceStorageListKeysRequest>();
}

std::string DeviceStorageListKeysRequest::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace requests
} // namespace motion
} // namespace zaber
