// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/axis_address.private.h"

#include <sstream>

namespace zaber {
namespace motion {

AxisAddress::AxisAddress() { }

AxisAddress::AxisAddress(
    int p_device,
    int p_axis
) :
    device(p_device),
    axis(p_axis)
{ }

bool AxisAddress::operator==(const AxisAddress& other) const {
    return std::tie(
    device,
    axis
    ) == std::tie(
    other.device,
    other.axis
    );
}

int AxisAddress::getDevice() const {
    return device;
}
void AxisAddress::setDevice(int p_device) {
    device = p_device;
}

int AxisAddress::getAxis() const {
    return axis;
}
void AxisAddress::setAxis(int p_axis) {
    axis = p_axis;
}

std::string AxisAddress::toString() const {
    std::stringstream ss;
    ss << "AxisAddress { ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << " }";
    return ss.str();
}

void AxisAddress::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisAddress>();
}

std::string AxisAddress::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace motion
} // namespace zaber
