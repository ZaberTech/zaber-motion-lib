// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/stream_movement_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

StreamMovementFailedExceptionData::StreamMovementFailedExceptionData() { }

StreamMovementFailedExceptionData::StreamMovementFailedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason))
{ }

bool StreamMovementFailedExceptionData::operator==(const StreamMovementFailedExceptionData& other) const {
    return std::tie(
    warnings,
    reason
    ) == std::tie(
    other.warnings,
    other.reason
    );
}

std::vector<std::string> const& StreamMovementFailedExceptionData::getWarnings() const {
    return warnings;
}
void StreamMovementFailedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& StreamMovementFailedExceptionData::getReason() const {
    return reason;
}
void StreamMovementFailedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::string StreamMovementFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "StreamMovementFailedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << " }";
    return ss.str();
}

void StreamMovementFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamMovementFailedExceptionData>();
}

std::string StreamMovementFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
