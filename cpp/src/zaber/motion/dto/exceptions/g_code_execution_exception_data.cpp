// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/g_code_execution_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

GCodeExecutionExceptionData::GCodeExecutionExceptionData() { }

GCodeExecutionExceptionData::GCodeExecutionExceptionData(
    int p_fromBlock,
    int p_toBlock
) :
    fromBlock(p_fromBlock),
    toBlock(p_toBlock)
{ }

bool GCodeExecutionExceptionData::operator==(const GCodeExecutionExceptionData& other) const {
    return std::tie(
    fromBlock,
    toBlock
    ) == std::tie(
    other.fromBlock,
    other.toBlock
    );
}

int GCodeExecutionExceptionData::getFromBlock() const {
    return fromBlock;
}
void GCodeExecutionExceptionData::setFromBlock(int p_fromBlock) {
    fromBlock = p_fromBlock;
}

int GCodeExecutionExceptionData::getToBlock() const {
    return toBlock;
}
void GCodeExecutionExceptionData::setToBlock(int p_toBlock) {
    toBlock = p_toBlock;
}

std::string GCodeExecutionExceptionData::toString() const {
    std::stringstream ss;
    ss << "GCodeExecutionExceptionData { ";
    ss << "fromBlock: ";
    ss << this->fromBlock;
    ss << ", ";
    ss << "toBlock: ";
    ss << this->toBlock;
    ss << " }";
    return ss.str();
}

void GCodeExecutionExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GCodeExecutionExceptionData>();
}

std::string GCodeExecutionExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
