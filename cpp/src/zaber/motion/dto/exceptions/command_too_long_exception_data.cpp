// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/command_too_long_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

CommandTooLongExceptionData::CommandTooLongExceptionData() { }

CommandTooLongExceptionData::CommandTooLongExceptionData(
    std::string p_fit,
    std::string p_remainder,
    int p_packetSize,
    int p_packetsMax
) :
    fit(std::move(p_fit)),
    remainder(std::move(p_remainder)),
    packetSize(p_packetSize),
    packetsMax(p_packetsMax)
{ }

bool CommandTooLongExceptionData::operator==(const CommandTooLongExceptionData& other) const {
    return std::tie(
    fit,
    remainder,
    packetSize,
    packetsMax
    ) == std::tie(
    other.fit,
    other.remainder,
    other.packetSize,
    other.packetsMax
    );
}

std::string const& CommandTooLongExceptionData::getFit() const {
    return fit;
}
void CommandTooLongExceptionData::setFit(std::string p_fit) {
    fit = std::move(p_fit);
}

std::string const& CommandTooLongExceptionData::getRemainder() const {
    return remainder;
}
void CommandTooLongExceptionData::setRemainder(std::string p_remainder) {
    remainder = std::move(p_remainder);
}

int CommandTooLongExceptionData::getPacketSize() const {
    return packetSize;
}
void CommandTooLongExceptionData::setPacketSize(int p_packetSize) {
    packetSize = p_packetSize;
}

int CommandTooLongExceptionData::getPacketsMax() const {
    return packetsMax;
}
void CommandTooLongExceptionData::setPacketsMax(int p_packetsMax) {
    packetsMax = p_packetsMax;
}

std::string CommandTooLongExceptionData::toString() const {
    std::stringstream ss;
    ss << "CommandTooLongExceptionData { ";
    ss << "fit: ";
    ss << this->fit;
    ss << ", ";
    ss << "remainder: ";
    ss << this->remainder;
    ss << ", ";
    ss << "packetSize: ";
    ss << this->packetSize;
    ss << ", ";
    ss << "packetsMax: ";
    ss << this->packetsMax;
    ss << " }";
    return ss.str();
}

void CommandTooLongExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CommandTooLongExceptionData>();
}

std::string CommandTooLongExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
