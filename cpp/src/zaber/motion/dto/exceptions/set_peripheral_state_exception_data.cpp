// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/set_peripheral_state_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

SetPeripheralStateExceptionData::SetPeripheralStateExceptionData() { }

SetPeripheralStateExceptionData::SetPeripheralStateExceptionData(
    int p_axisNumber,
    std::vector<std::string> p_settings,
    std::string p_servoTuning,
    std::vector<std::string> p_storedPositions,
    std::vector<std::string> p_storage
) :
    axisNumber(p_axisNumber),
    settings(std::move(p_settings)),
    servoTuning(std::move(p_servoTuning)),
    storedPositions(std::move(p_storedPositions)),
    storage(std::move(p_storage))
{ }

bool SetPeripheralStateExceptionData::operator==(const SetPeripheralStateExceptionData& other) const {
    return std::tie(
    axisNumber,
    settings,
    servoTuning,
    storedPositions,
    storage
    ) == std::tie(
    other.axisNumber,
    other.settings,
    other.servoTuning,
    other.storedPositions,
    other.storage
    );
}

int SetPeripheralStateExceptionData::getAxisNumber() const {
    return axisNumber;
}
void SetPeripheralStateExceptionData::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

std::vector<std::string> const& SetPeripheralStateExceptionData::getSettings() const {
    return settings;
}
void SetPeripheralStateExceptionData::setSettings(std::vector<std::string> p_settings) {
    settings = std::move(p_settings);
}

std::string const& SetPeripheralStateExceptionData::getServoTuning() const {
    return servoTuning;
}
void SetPeripheralStateExceptionData::setServoTuning(std::string p_servoTuning) {
    servoTuning = std::move(p_servoTuning);
}

std::vector<std::string> const& SetPeripheralStateExceptionData::getStoredPositions() const {
    return storedPositions;
}
void SetPeripheralStateExceptionData::setStoredPositions(std::vector<std::string> p_storedPositions) {
    storedPositions = std::move(p_storedPositions);
}

std::vector<std::string> const& SetPeripheralStateExceptionData::getStorage() const {
    return storage;
}
void SetPeripheralStateExceptionData::setStorage(std::vector<std::string> p_storage) {
    storage = std::move(p_storage);
}

std::string SetPeripheralStateExceptionData::toString() const {
    std::stringstream ss;
    ss << "SetPeripheralStateExceptionData { ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "settings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->settings.size(); i++) {
        ss << this->settings[i];
        if (i < this->settings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "servoTuning: ";
    ss << this->servoTuning;
    ss << ", ";
    ss << "storedPositions: ";
    ss << "[ ";
    for (size_t i = 0; i < this->storedPositions.size(); i++) {
        ss << this->storedPositions[i];
        if (i < this->storedPositions.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "storage: ";
    ss << "[ ";
    for (size_t i = 0; i < this->storage.size(); i++) {
        ss << this->storage[i];
        if (i < this->storage.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void SetPeripheralStateExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetPeripheralStateExceptionData>();
}

std::string SetPeripheralStateExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
