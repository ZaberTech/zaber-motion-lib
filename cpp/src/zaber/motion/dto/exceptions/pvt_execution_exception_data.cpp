// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/pvt_execution_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

PvtExecutionExceptionData::PvtExecutionExceptionData() { }

PvtExecutionExceptionData::PvtExecutionExceptionData(
    std::string p_errorFlag,
    std::string p_reason,
    std::vector<InvalidPvtPoint> p_invalidPoints
) :
    errorFlag(std::move(p_errorFlag)),
    reason(std::move(p_reason)),
    invalidPoints(std::move(p_invalidPoints))
{ }

bool PvtExecutionExceptionData::operator==(const PvtExecutionExceptionData& other) const {
    return std::tie(
    errorFlag,
    reason,
    invalidPoints
    ) == std::tie(
    other.errorFlag,
    other.reason,
    other.invalidPoints
    );
}

std::string const& PvtExecutionExceptionData::getErrorFlag() const {
    return errorFlag;
}
void PvtExecutionExceptionData::setErrorFlag(std::string p_errorFlag) {
    errorFlag = std::move(p_errorFlag);
}

std::string const& PvtExecutionExceptionData::getReason() const {
    return reason;
}
void PvtExecutionExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::vector<InvalidPvtPoint> const& PvtExecutionExceptionData::getInvalidPoints() const {
    return invalidPoints;
}
void PvtExecutionExceptionData::setInvalidPoints(std::vector<InvalidPvtPoint> p_invalidPoints) {
    invalidPoints = std::move(p_invalidPoints);
}

std::string PvtExecutionExceptionData::toString() const {
    std::stringstream ss;
    ss << "PvtExecutionExceptionData { ";
    ss << "errorFlag: ";
    ss << this->errorFlag;
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << ", ";
    ss << "invalidPoints: ";
    ss << "[ ";
    for (size_t i = 0; i < this->invalidPoints.size(); i++) {
        ss << this->invalidPoints[i].toString();
        if (i < this->invalidPoints.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void PvtExecutionExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PvtExecutionExceptionData>();
}

std::string PvtExecutionExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
