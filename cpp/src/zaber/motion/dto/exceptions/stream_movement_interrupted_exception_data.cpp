// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/stream_movement_interrupted_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

StreamMovementInterruptedExceptionData::StreamMovementInterruptedExceptionData() { }

StreamMovementInterruptedExceptionData::StreamMovementInterruptedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason))
{ }

bool StreamMovementInterruptedExceptionData::operator==(const StreamMovementInterruptedExceptionData& other) const {
    return std::tie(
    warnings,
    reason
    ) == std::tie(
    other.warnings,
    other.reason
    );
}

std::vector<std::string> const& StreamMovementInterruptedExceptionData::getWarnings() const {
    return warnings;
}
void StreamMovementInterruptedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& StreamMovementInterruptedExceptionData::getReason() const {
    return reason;
}
void StreamMovementInterruptedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::string StreamMovementInterruptedExceptionData::toString() const {
    std::stringstream ss;
    ss << "StreamMovementInterruptedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << " }";
    return ss.str();
}

void StreamMovementInterruptedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamMovementInterruptedExceptionData>();
}

std::string StreamMovementInterruptedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
