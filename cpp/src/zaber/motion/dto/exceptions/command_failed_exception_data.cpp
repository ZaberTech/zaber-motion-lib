// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/command_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

CommandFailedExceptionData::CommandFailedExceptionData() { }

CommandFailedExceptionData::CommandFailedExceptionData(
    std::string p_command,
    std::string p_responseData,
    std::string p_replyFlag,
    std::string p_status,
    std::string p_warningFlag,
    int p_deviceAddress,
    int p_axisNumber,
    int p_id
) :
    command(std::move(p_command)),
    responseData(std::move(p_responseData)),
    replyFlag(std::move(p_replyFlag)),
    status(std::move(p_status)),
    warningFlag(std::move(p_warningFlag)),
    deviceAddress(p_deviceAddress),
    axisNumber(p_axisNumber),
    id(p_id)
{ }

bool CommandFailedExceptionData::operator==(const CommandFailedExceptionData& other) const {
    return std::tie(
    command,
    responseData,
    replyFlag,
    status,
    warningFlag,
    deviceAddress,
    axisNumber,
    id
    ) == std::tie(
    other.command,
    other.responseData,
    other.replyFlag,
    other.status,
    other.warningFlag,
    other.deviceAddress,
    other.axisNumber,
    other.id
    );
}

std::string const& CommandFailedExceptionData::getCommand() const {
    return command;
}
void CommandFailedExceptionData::setCommand(std::string p_command) {
    command = std::move(p_command);
}

std::string const& CommandFailedExceptionData::getResponseData() const {
    return responseData;
}
void CommandFailedExceptionData::setResponseData(std::string p_responseData) {
    responseData = std::move(p_responseData);
}

std::string const& CommandFailedExceptionData::getReplyFlag() const {
    return replyFlag;
}
void CommandFailedExceptionData::setReplyFlag(std::string p_replyFlag) {
    replyFlag = std::move(p_replyFlag);
}

std::string const& CommandFailedExceptionData::getStatus() const {
    return status;
}
void CommandFailedExceptionData::setStatus(std::string p_status) {
    status = std::move(p_status);
}

std::string const& CommandFailedExceptionData::getWarningFlag() const {
    return warningFlag;
}
void CommandFailedExceptionData::setWarningFlag(std::string p_warningFlag) {
    warningFlag = std::move(p_warningFlag);
}

int CommandFailedExceptionData::getDeviceAddress() const {
    return deviceAddress;
}
void CommandFailedExceptionData::setDeviceAddress(int p_deviceAddress) {
    deviceAddress = p_deviceAddress;
}

int CommandFailedExceptionData::getAxisNumber() const {
    return axisNumber;
}
void CommandFailedExceptionData::setAxisNumber(int p_axisNumber) {
    axisNumber = p_axisNumber;
}

int CommandFailedExceptionData::getId() const {
    return id;
}
void CommandFailedExceptionData::setId(int p_id) {
    id = p_id;
}

std::string CommandFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "CommandFailedExceptionData { ";
    ss << "command: ";
    ss << this->command;
    ss << ", ";
    ss << "responseData: ";
    ss << this->responseData;
    ss << ", ";
    ss << "replyFlag: ";
    ss << this->replyFlag;
    ss << ", ";
    ss << "status: ";
    ss << this->status;
    ss << ", ";
    ss << "warningFlag: ";
    ss << this->warningFlag;
    ss << ", ";
    ss << "deviceAddress: ";
    ss << this->deviceAddress;
    ss << ", ";
    ss << "axisNumber: ";
    ss << this->axisNumber;
    ss << ", ";
    ss << "id: ";
    ss << this->id;
    ss << " }";
    return ss.str();
}

void CommandFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<CommandFailedExceptionData>();
}

std::string CommandFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
