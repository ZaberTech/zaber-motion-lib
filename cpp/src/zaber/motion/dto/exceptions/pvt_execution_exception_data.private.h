// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/exceptions/pvt_execution_exception_data.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/exceptions/invalid_pvt_point.private.h"

namespace zaber {
namespace motion {
namespace exceptions {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PvtExecutionExceptionData, errorFlag, reason, invalidPoints)

} // namespace exceptions
} // namespace motion
} // namespace zaber
