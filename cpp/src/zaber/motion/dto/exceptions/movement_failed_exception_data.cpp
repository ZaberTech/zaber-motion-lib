// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/movement_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

MovementFailedExceptionData::MovementFailedExceptionData() { }

MovementFailedExceptionData::MovementFailedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason,
    int p_device,
    int p_axis
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason)),
    device(p_device),
    axis(p_axis)
{ }

bool MovementFailedExceptionData::operator==(const MovementFailedExceptionData& other) const {
    return std::tie(
    warnings,
    reason,
    device,
    axis
    ) == std::tie(
    other.warnings,
    other.reason,
    other.device,
    other.axis
    );
}

std::vector<std::string> const& MovementFailedExceptionData::getWarnings() const {
    return warnings;
}
void MovementFailedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& MovementFailedExceptionData::getReason() const {
    return reason;
}
void MovementFailedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

int MovementFailedExceptionData::getDevice() const {
    return device;
}
void MovementFailedExceptionData::setDevice(int p_device) {
    device = p_device;
}

int MovementFailedExceptionData::getAxis() const {
    return axis;
}
void MovementFailedExceptionData::setAxis(int p_axis) {
    axis = p_axis;
}

std::string MovementFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "MovementFailedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << " }";
    return ss.str();
}

void MovementFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MovementFailedExceptionData>();
}

std::string MovementFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
