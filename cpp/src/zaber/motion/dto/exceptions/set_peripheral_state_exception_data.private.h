// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/exceptions/set_peripheral_state_exception_data.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace exceptions {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetPeripheralStateExceptionData, axisNumber, settings, servoTuning, storedPositions, storage)

} // namespace exceptions
} // namespace motion
} // namespace zaber
