// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/set_device_state_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

SetDeviceStateExceptionData::SetDeviceStateExceptionData() { }

SetDeviceStateExceptionData::SetDeviceStateExceptionData(
    std::vector<std::string> p_settings,
    std::vector<std::string> p_streamBuffers,
    std::vector<std::string> p_pvtBuffers,
    std::vector<std::string> p_triggers,
    std::string p_servoTuning,
    std::vector<std::string> p_storedPositions,
    std::vector<std::string> p_storage,
    std::vector<SetPeripheralStateExceptionData> p_peripherals
) :
    settings(std::move(p_settings)),
    streamBuffers(std::move(p_streamBuffers)),
    pvtBuffers(std::move(p_pvtBuffers)),
    triggers(std::move(p_triggers)),
    servoTuning(std::move(p_servoTuning)),
    storedPositions(std::move(p_storedPositions)),
    storage(std::move(p_storage)),
    peripherals(std::move(p_peripherals))
{ }

bool SetDeviceStateExceptionData::operator==(const SetDeviceStateExceptionData& other) const {
    return std::tie(
    settings,
    streamBuffers,
    pvtBuffers,
    triggers,
    servoTuning,
    storedPositions,
    storage,
    peripherals
    ) == std::tie(
    other.settings,
    other.streamBuffers,
    other.pvtBuffers,
    other.triggers,
    other.servoTuning,
    other.storedPositions,
    other.storage,
    other.peripherals
    );
}

std::vector<std::string> const& SetDeviceStateExceptionData::getSettings() const {
    return settings;
}
void SetDeviceStateExceptionData::setSettings(std::vector<std::string> p_settings) {
    settings = std::move(p_settings);
}

std::vector<std::string> const& SetDeviceStateExceptionData::getStreamBuffers() const {
    return streamBuffers;
}
void SetDeviceStateExceptionData::setStreamBuffers(std::vector<std::string> p_streamBuffers) {
    streamBuffers = std::move(p_streamBuffers);
}

std::vector<std::string> const& SetDeviceStateExceptionData::getPvtBuffers() const {
    return pvtBuffers;
}
void SetDeviceStateExceptionData::setPvtBuffers(std::vector<std::string> p_pvtBuffers) {
    pvtBuffers = std::move(p_pvtBuffers);
}

std::vector<std::string> const& SetDeviceStateExceptionData::getTriggers() const {
    return triggers;
}
void SetDeviceStateExceptionData::setTriggers(std::vector<std::string> p_triggers) {
    triggers = std::move(p_triggers);
}

std::string const& SetDeviceStateExceptionData::getServoTuning() const {
    return servoTuning;
}
void SetDeviceStateExceptionData::setServoTuning(std::string p_servoTuning) {
    servoTuning = std::move(p_servoTuning);
}

std::vector<std::string> const& SetDeviceStateExceptionData::getStoredPositions() const {
    return storedPositions;
}
void SetDeviceStateExceptionData::setStoredPositions(std::vector<std::string> p_storedPositions) {
    storedPositions = std::move(p_storedPositions);
}

std::vector<std::string> const& SetDeviceStateExceptionData::getStorage() const {
    return storage;
}
void SetDeviceStateExceptionData::setStorage(std::vector<std::string> p_storage) {
    storage = std::move(p_storage);
}

std::vector<SetPeripheralStateExceptionData> const& SetDeviceStateExceptionData::getPeripherals() const {
    return peripherals;
}
void SetDeviceStateExceptionData::setPeripherals(std::vector<SetPeripheralStateExceptionData> p_peripherals) {
    peripherals = std::move(p_peripherals);
}

std::string SetDeviceStateExceptionData::toString() const {
    std::stringstream ss;
    ss << "SetDeviceStateExceptionData { ";
    ss << "settings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->settings.size(); i++) {
        ss << this->settings[i];
        if (i < this->settings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "streamBuffers: ";
    ss << "[ ";
    for (size_t i = 0; i < this->streamBuffers.size(); i++) {
        ss << this->streamBuffers[i];
        if (i < this->streamBuffers.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "pvtBuffers: ";
    ss << "[ ";
    for (size_t i = 0; i < this->pvtBuffers.size(); i++) {
        ss << this->pvtBuffers[i];
        if (i < this->pvtBuffers.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "triggers: ";
    ss << "[ ";
    for (size_t i = 0; i < this->triggers.size(); i++) {
        ss << this->triggers[i];
        if (i < this->triggers.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "servoTuning: ";
    ss << this->servoTuning;
    ss << ", ";
    ss << "storedPositions: ";
    ss << "[ ";
    for (size_t i = 0; i < this->storedPositions.size(); i++) {
        ss << this->storedPositions[i];
        if (i < this->storedPositions.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "storage: ";
    ss << "[ ";
    for (size_t i = 0; i < this->storage.size(); i++) {
        ss << this->storage[i];
        if (i < this->storage.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "peripherals: ";
    ss << "[ ";
    for (size_t i = 0; i < this->peripherals.size(); i++) {
        ss << this->peripherals[i].toString();
        if (i < this->peripherals.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void SetDeviceStateExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<SetDeviceStateExceptionData>();
}

std::string SetDeviceStateExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
