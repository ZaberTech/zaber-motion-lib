// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/exceptions/set_device_state_exception_data.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/exceptions/set_peripheral_state_exception_data.private.h"

namespace zaber {
namespace motion {
namespace exceptions {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(SetDeviceStateExceptionData, settings, streamBuffers, pvtBuffers, triggers, servoTuning, storedPositions, storage, peripherals)

} // namespace exceptions
} // namespace motion
} // namespace zaber
