// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/binary_command_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

BinaryCommandFailedExceptionData::BinaryCommandFailedExceptionData() { }

BinaryCommandFailedExceptionData::BinaryCommandFailedExceptionData(
    int p_responseData
) :
    responseData(p_responseData)
{ }

bool BinaryCommandFailedExceptionData::operator==(const BinaryCommandFailedExceptionData& other) const {
    return std::tie(
    responseData
    ) == std::tie(
    other.responseData
    );
}

int BinaryCommandFailedExceptionData::getResponseData() const {
    return responseData;
}
void BinaryCommandFailedExceptionData::setResponseData(int p_responseData) {
    responseData = p_responseData;
}

std::string BinaryCommandFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "BinaryCommandFailedExceptionData { ";
    ss << "responseData: ";
    ss << this->responseData;
    ss << " }";
    return ss.str();
}

void BinaryCommandFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<BinaryCommandFailedExceptionData>();
}

std::string BinaryCommandFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
