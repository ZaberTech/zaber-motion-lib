// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/invalid_response_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

InvalidResponseExceptionData::InvalidResponseExceptionData() { }

InvalidResponseExceptionData::InvalidResponseExceptionData(
    std::string p_response
) :
    response(std::move(p_response))
{ }

bool InvalidResponseExceptionData::operator==(const InvalidResponseExceptionData& other) const {
    return std::tie(
    response
    ) == std::tie(
    other.response
    );
}

std::string const& InvalidResponseExceptionData::getResponse() const {
    return response;
}
void InvalidResponseExceptionData::setResponse(std::string p_response) {
    response = std::move(p_response);
}

std::string InvalidResponseExceptionData::toString() const {
    std::stringstream ss;
    ss << "InvalidResponseExceptionData { ";
    ss << "response: ";
    ss << this->response;
    ss << " }";
    return ss.str();
}

void InvalidResponseExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<InvalidResponseExceptionData>();
}

std::string InvalidResponseExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
