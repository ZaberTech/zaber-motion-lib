// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/g_code_syntax_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

GCodeSyntaxExceptionData::GCodeSyntaxExceptionData() { }

GCodeSyntaxExceptionData::GCodeSyntaxExceptionData(
    int p_fromBlock,
    int p_toBlock
) :
    fromBlock(p_fromBlock),
    toBlock(p_toBlock)
{ }

bool GCodeSyntaxExceptionData::operator==(const GCodeSyntaxExceptionData& other) const {
    return std::tie(
    fromBlock,
    toBlock
    ) == std::tie(
    other.fromBlock,
    other.toBlock
    );
}

int GCodeSyntaxExceptionData::getFromBlock() const {
    return fromBlock;
}
void GCodeSyntaxExceptionData::setFromBlock(int p_fromBlock) {
    fromBlock = p_fromBlock;
}

int GCodeSyntaxExceptionData::getToBlock() const {
    return toBlock;
}
void GCodeSyntaxExceptionData::setToBlock(int p_toBlock) {
    toBlock = p_toBlock;
}

std::string GCodeSyntaxExceptionData::toString() const {
    std::stringstream ss;
    ss << "GCodeSyntaxExceptionData { ";
    ss << "fromBlock: ";
    ss << this->fromBlock;
    ss << ", ";
    ss << "toBlock: ";
    ss << this->toBlock;
    ss << " }";
    return ss.str();
}

void GCodeSyntaxExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<GCodeSyntaxExceptionData>();
}

std::string GCodeSyntaxExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
