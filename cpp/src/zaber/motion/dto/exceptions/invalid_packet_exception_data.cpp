// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/invalid_packet_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

InvalidPacketExceptionData::InvalidPacketExceptionData() { }

InvalidPacketExceptionData::InvalidPacketExceptionData(
    std::string p_packet,
    std::string p_reason
) :
    packet(std::move(p_packet)),
    reason(std::move(p_reason))
{ }

bool InvalidPacketExceptionData::operator==(const InvalidPacketExceptionData& other) const {
    return std::tie(
    packet,
    reason
    ) == std::tie(
    other.packet,
    other.reason
    );
}

std::string const& InvalidPacketExceptionData::getPacket() const {
    return packet;
}
void InvalidPacketExceptionData::setPacket(std::string p_packet) {
    packet = std::move(p_packet);
}

std::string const& InvalidPacketExceptionData::getReason() const {
    return reason;
}
void InvalidPacketExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::string InvalidPacketExceptionData::toString() const {
    std::stringstream ss;
    ss << "InvalidPacketExceptionData { ";
    ss << "packet: ";
    ss << this->packet;
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << " }";
    return ss.str();
}

void InvalidPacketExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<InvalidPacketExceptionData>();
}

std::string InvalidPacketExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
