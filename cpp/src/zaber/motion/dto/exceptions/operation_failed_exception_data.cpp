// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/operation_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

OperationFailedExceptionData::OperationFailedExceptionData() { }

OperationFailedExceptionData::OperationFailedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason,
    int p_device,
    int p_axis
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason)),
    device(p_device),
    axis(p_axis)
{ }

bool OperationFailedExceptionData::operator==(const OperationFailedExceptionData& other) const {
    return std::tie(
    warnings,
    reason,
    device,
    axis
    ) == std::tie(
    other.warnings,
    other.reason,
    other.device,
    other.axis
    );
}

std::vector<std::string> const& OperationFailedExceptionData::getWarnings() const {
    return warnings;
}
void OperationFailedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& OperationFailedExceptionData::getReason() const {
    return reason;
}
void OperationFailedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

int OperationFailedExceptionData::getDevice() const {
    return device;
}
void OperationFailedExceptionData::setDevice(int p_device) {
    device = p_device;
}

int OperationFailedExceptionData::getAxis() const {
    return axis;
}
void OperationFailedExceptionData::setAxis(int p_axis) {
    axis = p_axis;
}

std::string OperationFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "OperationFailedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << " }";
    return ss.str();
}

void OperationFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<OperationFailedExceptionData>();
}

std::string OperationFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
