// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/device_address_conflict_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

DeviceAddressConflictExceptionData::DeviceAddressConflictExceptionData() { }

DeviceAddressConflictExceptionData::DeviceAddressConflictExceptionData(
    std::vector<int> p_deviceAddresses
) :
    deviceAddresses(std::move(p_deviceAddresses))
{ }

bool DeviceAddressConflictExceptionData::operator==(const DeviceAddressConflictExceptionData& other) const {
    return std::tie(
    deviceAddresses
    ) == std::tie(
    other.deviceAddresses
    );
}

std::vector<int> const& DeviceAddressConflictExceptionData::getDeviceAddresses() const {
    return deviceAddresses;
}
void DeviceAddressConflictExceptionData::setDeviceAddresses(std::vector<int> p_deviceAddresses) {
    deviceAddresses = std::move(p_deviceAddresses);
}

std::string DeviceAddressConflictExceptionData::toString() const {
    std::stringstream ss;
    ss << "DeviceAddressConflictExceptionData { ";
    ss << "deviceAddresses: ";
    ss << "[ ";
    for (size_t i = 0; i < this->deviceAddresses.size(); i++) {
        ss << this->deviceAddresses[i];
        if (i < this->deviceAddresses.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void DeviceAddressConflictExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceAddressConflictExceptionData>();
}

std::string DeviceAddressConflictExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
