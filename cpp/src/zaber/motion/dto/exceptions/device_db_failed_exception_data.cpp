// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/device_db_failed_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

DeviceDbFailedExceptionData::DeviceDbFailedExceptionData() { }

DeviceDbFailedExceptionData::DeviceDbFailedExceptionData(
    std::string p_code
) :
    code(std::move(p_code))
{ }

bool DeviceDbFailedExceptionData::operator==(const DeviceDbFailedExceptionData& other) const {
    return std::tie(
    code
    ) == std::tie(
    other.code
    );
}

std::string const& DeviceDbFailedExceptionData::getCode() const {
    return code;
}
void DeviceDbFailedExceptionData::setCode(std::string p_code) {
    code = std::move(p_code);
}

std::string DeviceDbFailedExceptionData::toString() const {
    std::stringstream ss;
    ss << "DeviceDbFailedExceptionData { ";
    ss << "code: ";
    ss << this->code;
    ss << " }";
    return ss.str();
}

void DeviceDbFailedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceDbFailedExceptionData>();
}

std::string DeviceDbFailedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
