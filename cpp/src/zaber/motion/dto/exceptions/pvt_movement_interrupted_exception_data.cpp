// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/pvt_movement_interrupted_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

PvtMovementInterruptedExceptionData::PvtMovementInterruptedExceptionData() { }

PvtMovementInterruptedExceptionData::PvtMovementInterruptedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason))
{ }

bool PvtMovementInterruptedExceptionData::operator==(const PvtMovementInterruptedExceptionData& other) const {
    return std::tie(
    warnings,
    reason
    ) == std::tie(
    other.warnings,
    other.reason
    );
}

std::vector<std::string> const& PvtMovementInterruptedExceptionData::getWarnings() const {
    return warnings;
}
void PvtMovementInterruptedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& PvtMovementInterruptedExceptionData::getReason() const {
    return reason;
}
void PvtMovementInterruptedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::string PvtMovementInterruptedExceptionData::toString() const {
    std::stringstream ss;
    ss << "PvtMovementInterruptedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << " }";
    return ss.str();
}

void PvtMovementInterruptedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<PvtMovementInterruptedExceptionData>();
}

std::string PvtMovementInterruptedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
