// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/exceptions/stream_execution_exception_data.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace exceptions {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StreamExecutionExceptionData, errorFlag, reason)

} // namespace exceptions
} // namespace motion
} // namespace zaber
