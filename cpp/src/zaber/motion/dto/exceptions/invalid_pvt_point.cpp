// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/invalid_pvt_point.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

InvalidPvtPoint::InvalidPvtPoint() { }

InvalidPvtPoint::InvalidPvtPoint(
    int p_index,
    std::string p_point
) :
    index(p_index),
    point(std::move(p_point))
{ }

bool InvalidPvtPoint::operator==(const InvalidPvtPoint& other) const {
    return std::tie(
    index,
    point
    ) == std::tie(
    other.index,
    other.point
    );
}

int InvalidPvtPoint::getIndex() const {
    return index;
}
void InvalidPvtPoint::setIndex(int p_index) {
    index = p_index;
}

std::string const& InvalidPvtPoint::getPoint() const {
    return point;
}
void InvalidPvtPoint::setPoint(std::string p_point) {
    point = std::move(p_point);
}

std::string InvalidPvtPoint::toString() const {
    std::stringstream ss;
    ss << "InvalidPvtPoint { ";
    ss << "index: ";
    ss << this->index;
    ss << ", ";
    ss << "point: ";
    ss << this->point;
    ss << " }";
    return ss.str();
}

void InvalidPvtPoint::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<InvalidPvtPoint>();
}

std::string InvalidPvtPoint::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
