// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/exceptions/operation_failed_exception_data.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace exceptions {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(OperationFailedExceptionData, warnings, reason, device, axis)

} // namespace exceptions
} // namespace motion
} // namespace zaber
