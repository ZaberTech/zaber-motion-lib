// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/movement_interrupted_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

MovementInterruptedExceptionData::MovementInterruptedExceptionData() { }

MovementInterruptedExceptionData::MovementInterruptedExceptionData(
    std::vector<std::string> p_warnings,
    std::string p_reason,
    int p_device,
    int p_axis
) :
    warnings(std::move(p_warnings)),
    reason(std::move(p_reason)),
    device(p_device),
    axis(p_axis)
{ }

bool MovementInterruptedExceptionData::operator==(const MovementInterruptedExceptionData& other) const {
    return std::tie(
    warnings,
    reason,
    device,
    axis
    ) == std::tie(
    other.warnings,
    other.reason,
    other.device,
    other.axis
    );
}

std::vector<std::string> const& MovementInterruptedExceptionData::getWarnings() const {
    return warnings;
}
void MovementInterruptedExceptionData::setWarnings(std::vector<std::string> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string const& MovementInterruptedExceptionData::getReason() const {
    return reason;
}
void MovementInterruptedExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

int MovementInterruptedExceptionData::getDevice() const {
    return device;
}
void MovementInterruptedExceptionData::setDevice(int p_device) {
    device = p_device;
}

int MovementInterruptedExceptionData::getAxis() const {
    return axis;
}
void MovementInterruptedExceptionData::setAxis(int p_axis) {
    axis = p_axis;
}

std::string MovementInterruptedExceptionData::toString() const {
    std::stringstream ss;
    ss << "MovementInterruptedExceptionData { ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i];
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << ", ";
    ss << "device: ";
    ss << this->device;
    ss << ", ";
    ss << "axis: ";
    ss << this->axis;
    ss << " }";
    return ss.str();
}

void MovementInterruptedExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<MovementInterruptedExceptionData>();
}

std::string MovementInterruptedExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
