// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/exceptions/stream_execution_exception_data.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace exceptions {

StreamExecutionExceptionData::StreamExecutionExceptionData() { }

StreamExecutionExceptionData::StreamExecutionExceptionData(
    std::string p_errorFlag,
    std::string p_reason
) :
    errorFlag(std::move(p_errorFlag)),
    reason(std::move(p_reason))
{ }

bool StreamExecutionExceptionData::operator==(const StreamExecutionExceptionData& other) const {
    return std::tie(
    errorFlag,
    reason
    ) == std::tie(
    other.errorFlag,
    other.reason
    );
}

std::string const& StreamExecutionExceptionData::getErrorFlag() const {
    return errorFlag;
}
void StreamExecutionExceptionData::setErrorFlag(std::string p_errorFlag) {
    errorFlag = std::move(p_errorFlag);
}

std::string const& StreamExecutionExceptionData::getReason() const {
    return reason;
}
void StreamExecutionExceptionData::setReason(std::string p_reason) {
    reason = std::move(p_reason);
}

std::string StreamExecutionExceptionData::toString() const {
    std::stringstream ss;
    ss << "StreamExecutionExceptionData { ";
    ss << "errorFlag: ";
    ss << this->errorFlag;
    ss << ", ";
    ss << "reason: ";
    ss << this->reason;
    ss << " }";
    return ss.str();
}

void StreamExecutionExceptionData::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<StreamExecutionExceptionData>();
}

std::string StreamExecutionExceptionData::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace exceptions
} // namespace motion
} // namespace zaber
