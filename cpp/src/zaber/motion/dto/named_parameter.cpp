// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/named_parameter.private.h"

#include <sstream>

namespace zaber {
namespace motion {

NamedParameter::NamedParameter() { }

NamedParameter::NamedParameter(
    std::string p_name,
    std::optional<double> p_value
) :
    name(std::move(p_name)),
    value(p_value)
{ }

NamedParameter::NamedParameter(
    std::string p_name
) :
    name(std::move(p_name))
{ }

bool NamedParameter::operator==(const NamedParameter& other) const {
    return std::tie(
    name,
    value
    ) == std::tie(
    other.name,
    other.value
    );
}

std::string const& NamedParameter::getName() const {
    return name;
}
void NamedParameter::setName(std::string p_name) {
    name = std::move(p_name);
}

std::optional<double> NamedParameter::getValue() const {
    return value;
}
void NamedParameter::setValue(std::optional<double> p_value) {
    value = p_value;
}

std::string NamedParameter::toString() const {
    std::stringstream ss;
    ss << "NamedParameter { ";
    ss << "name: ";
    ss << this->name;
    ss << ", ";
    ss << "value: ";
    if (this->value.has_value()) {
        ss << this->value.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void NamedParameter::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<NamedParameter>();
}

std::string NamedParameter::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace motion
} // namespace zaber
