// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/gcode/translate_result.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/gcode/translate_message.private.h"

namespace zaber {
namespace motion {
namespace gcode {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TranslateResult, commands, warnings)

} // namespace gcode
} // namespace motion
} // namespace zaber
