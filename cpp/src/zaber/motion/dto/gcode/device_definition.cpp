// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/device_definition.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

DeviceDefinition::DeviceDefinition() { }

DeviceDefinition::DeviceDefinition(
    int p_deviceId,
    std::vector<AxisDefinition> p_axes,
    Measurement p_maxSpeed
) :
    deviceId(p_deviceId),
    axes(std::move(p_axes)),
    maxSpeed(std::move(p_maxSpeed))
{ }

bool DeviceDefinition::operator==(const DeviceDefinition& other) const {
    return std::tie(
    deviceId,
    axes,
    maxSpeed
    ) == std::tie(
    other.deviceId,
    other.axes,
    other.maxSpeed
    );
}

int DeviceDefinition::getDeviceId() const {
    return deviceId;
}
void DeviceDefinition::setDeviceId(int p_deviceId) {
    deviceId = p_deviceId;
}

std::vector<AxisDefinition> const& DeviceDefinition::getAxes() const {
    return axes;
}
void DeviceDefinition::setAxes(std::vector<AxisDefinition> p_axes) {
    axes = std::move(p_axes);
}

Measurement const& DeviceDefinition::getMaxSpeed() const {
    return maxSpeed;
}
void DeviceDefinition::setMaxSpeed(Measurement p_maxSpeed) {
    maxSpeed = std::move(p_maxSpeed);
}

std::string DeviceDefinition::toString() const {
    std::stringstream ss;
    ss << "DeviceDefinition { ";
    ss << "deviceId: ";
    ss << this->deviceId;
    ss << ", ";
    ss << "axes: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axes.size(); i++) {
        ss << this->axes[i].toString();
        if (i < this->axes.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "maxSpeed: ";
    ss << this->maxSpeed.toString();
    ss << " }";
    return ss.str();
}

void DeviceDefinition::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<DeviceDefinition>();
}

std::string DeviceDefinition::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
