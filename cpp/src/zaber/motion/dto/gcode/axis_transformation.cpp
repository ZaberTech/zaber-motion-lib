// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/axis_transformation.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

AxisTransformation::AxisTransformation() { }

AxisTransformation::AxisTransformation(
    std::string p_axisLetter,
    std::optional<double> p_scaling,
    std::optional<Measurement> p_translation
) :
    axisLetter(std::move(p_axisLetter)),
    scaling(p_scaling),
    translation(std::move(p_translation))
{ }

AxisTransformation::AxisTransformation(
    std::string p_axisLetter
) :
    axisLetter(std::move(p_axisLetter))
{ }

bool AxisTransformation::operator==(const AxisTransformation& other) const {
    return std::tie(
    axisLetter,
    scaling,
    translation
    ) == std::tie(
    other.axisLetter,
    other.scaling,
    other.translation
    );
}

std::string const& AxisTransformation::getAxisLetter() const {
    return axisLetter;
}
void AxisTransformation::setAxisLetter(std::string p_axisLetter) {
    axisLetter = std::move(p_axisLetter);
}

std::optional<double> AxisTransformation::getScaling() const {
    return scaling;
}
void AxisTransformation::setScaling(std::optional<double> p_scaling) {
    scaling = p_scaling;
}

std::optional<Measurement> const& AxisTransformation::getTranslation() const {
    return translation;
}
void AxisTransformation::setTranslation(std::optional<Measurement> p_translation) {
    translation = std::move(p_translation);
}

std::string AxisTransformation::toString() const {
    std::stringstream ss;
    ss << "AxisTransformation { ";
    ss << "axisLetter: ";
    ss << this->axisLetter;
    ss << ", ";
    ss << "scaling: ";
    if (this->scaling.has_value()) {
        ss << this->scaling.value();
    } else {
        ss << "null";
    }
    ss << ", ";
    ss << "translation: ";
    if (this->translation.has_value()) {
        ss << this->translation.value().toString();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void AxisTransformation::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisTransformation>();
}

std::string AxisTransformation::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
