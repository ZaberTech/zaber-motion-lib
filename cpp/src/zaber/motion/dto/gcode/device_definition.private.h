// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/gcode/device_definition.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/gcode/axis_definition.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace gcode {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeviceDefinition, deviceId, axes, maxSpeed)

} // namespace gcode
} // namespace motion
} // namespace zaber
