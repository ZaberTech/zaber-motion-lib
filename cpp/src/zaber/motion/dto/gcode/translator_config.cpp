// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/translator_config.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

TranslatorConfig::TranslatorConfig() { }

TranslatorConfig::TranslatorConfig(
    std::vector<AxisMapping> p_axisMappings,
    std::vector<AxisTransformation> p_axisTransformations
) :
    axisMappings(std::move(p_axisMappings)),
    axisTransformations(std::move(p_axisTransformations))
{ }

bool TranslatorConfig::operator==(const TranslatorConfig& other) const {
    return std::tie(
    axisMappings,
    axisTransformations
    ) == std::tie(
    other.axisMappings,
    other.axisTransformations
    );
}

std::vector<AxisMapping> const& TranslatorConfig::getAxisMappings() const {
    return axisMappings;
}
void TranslatorConfig::setAxisMappings(std::vector<AxisMapping> p_axisMappings) {
    axisMappings = std::move(p_axisMappings);
}

std::vector<AxisTransformation> const& TranslatorConfig::getAxisTransformations() const {
    return axisTransformations;
}
void TranslatorConfig::setAxisTransformations(std::vector<AxisTransformation> p_axisTransformations) {
    axisTransformations = std::move(p_axisTransformations);
}

std::string TranslatorConfig::toString() const {
    std::stringstream ss;
    ss << "TranslatorConfig { ";
    ss << "axisMappings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axisMappings.size(); i++) {
        ss << this->axisMappings[i].toString();
        if (i < this->axisMappings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "axisTransformations: ";
    ss << "[ ";
    for (size_t i = 0; i < this->axisTransformations.size(); i++) {
        ss << this->axisTransformations[i].toString();
        if (i < this->axisTransformations.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TranslatorConfig::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslatorConfig>();
}

std::string TranslatorConfig::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
