// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/gcode/axis_transformation.h"

#include "zaber/motion/utils/serialization_utils.private.h"
#include "zaber/motion/dto/measurement.private.h"

namespace zaber {
namespace motion {
namespace gcode {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AxisTransformation, axisLetter, scaling, translation)

} // namespace gcode
} // namespace motion
} // namespace zaber
