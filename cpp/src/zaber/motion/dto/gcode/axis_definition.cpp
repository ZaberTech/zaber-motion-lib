// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/axis_definition.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

AxisDefinition::AxisDefinition() { }

AxisDefinition::AxisDefinition(
    int p_peripheralId,
    std::optional<int> p_microstepResolution
) :
    peripheralId(p_peripheralId),
    microstepResolution(p_microstepResolution)
{ }

AxisDefinition::AxisDefinition(
    int p_peripheralId
) :
    peripheralId(p_peripheralId)
{ }

bool AxisDefinition::operator==(const AxisDefinition& other) const {
    return std::tie(
    peripheralId,
    microstepResolution
    ) == std::tie(
    other.peripheralId,
    other.microstepResolution
    );
}

int AxisDefinition::getPeripheralId() const {
    return peripheralId;
}
void AxisDefinition::setPeripheralId(int p_peripheralId) {
    peripheralId = p_peripheralId;
}

std::optional<int> AxisDefinition::getMicrostepResolution() const {
    return microstepResolution;
}
void AxisDefinition::setMicrostepResolution(std::optional<int> p_microstepResolution) {
    microstepResolution = p_microstepResolution;
}

std::string AxisDefinition::toString() const {
    std::stringstream ss;
    ss << "AxisDefinition { ";
    ss << "peripheralId: ";
    ss << this->peripheralId;
    ss << ", ";
    ss << "microstepResolution: ";
    if (this->microstepResolution.has_value()) {
        ss << this->microstepResolution.value();
    } else {
        ss << "null";
    }
    ss << " }";
    return ss.str();
}

void AxisDefinition::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisDefinition>();
}

std::string AxisDefinition::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
