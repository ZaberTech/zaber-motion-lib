// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/axis_mapping.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

AxisMapping::AxisMapping() { }

AxisMapping::AxisMapping(
    std::string p_axisLetter,
    int p_axisIndex
) :
    axisLetter(std::move(p_axisLetter)),
    axisIndex(p_axisIndex)
{ }

bool AxisMapping::operator==(const AxisMapping& other) const {
    return std::tie(
    axisLetter,
    axisIndex
    ) == std::tie(
    other.axisLetter,
    other.axisIndex
    );
}

std::string const& AxisMapping::getAxisLetter() const {
    return axisLetter;
}
void AxisMapping::setAxisLetter(std::string p_axisLetter) {
    axisLetter = std::move(p_axisLetter);
}

int AxisMapping::getAxisIndex() const {
    return axisIndex;
}
void AxisMapping::setAxisIndex(int p_axisIndex) {
    axisIndex = p_axisIndex;
}

std::string AxisMapping::toString() const {
    std::stringstream ss;
    ss << "AxisMapping { ";
    ss << "axisLetter: ";
    ss << this->axisLetter;
    ss << ", ";
    ss << "axisIndex: ";
    ss << this->axisIndex;
    ss << " }";
    return ss.str();
}

void AxisMapping::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<AxisMapping>();
}

std::string AxisMapping::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
