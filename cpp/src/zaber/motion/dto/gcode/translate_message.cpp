// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/translate_message.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

TranslateMessage::TranslateMessage() { }

TranslateMessage::TranslateMessage(
    std::string p_message,
    int p_fromBlock,
    int p_toBlock
) :
    message(std::move(p_message)),
    fromBlock(p_fromBlock),
    toBlock(p_toBlock)
{ }

bool TranslateMessage::operator==(const TranslateMessage& other) const {
    return std::tie(
    message,
    fromBlock,
    toBlock
    ) == std::tie(
    other.message,
    other.fromBlock,
    other.toBlock
    );
}

std::string const& TranslateMessage::getMessage() const {
    return message;
}
void TranslateMessage::setMessage(std::string p_message) {
    message = std::move(p_message);
}

int TranslateMessage::getFromBlock() const {
    return fromBlock;
}
void TranslateMessage::setFromBlock(int p_fromBlock) {
    fromBlock = p_fromBlock;
}

int TranslateMessage::getToBlock() const {
    return toBlock;
}
void TranslateMessage::setToBlock(int p_toBlock) {
    toBlock = p_toBlock;
}

std::string TranslateMessage::toString() const {
    std::stringstream ss;
    ss << "TranslateMessage { ";
    ss << "message: ";
    ss << this->message;
    ss << ", ";
    ss << "fromBlock: ";
    ss << this->fromBlock;
    ss << ", ";
    ss << "toBlock: ";
    ss << this->toBlock;
    ss << " }";
    return ss.str();
}

void TranslateMessage::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslateMessage>();
}

std::string TranslateMessage::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
