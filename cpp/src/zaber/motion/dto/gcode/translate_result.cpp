// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#include "zaber/motion/dto/gcode/translate_result.private.h"

#include <sstream>

namespace zaber {
namespace motion {
namespace gcode {

TranslateResult::TranslateResult() { }

TranslateResult::TranslateResult(
    std::vector<std::string> p_commands,
    std::vector<TranslateMessage> p_warnings
) :
    commands(std::move(p_commands)),
    warnings(std::move(p_warnings))
{ }

bool TranslateResult::operator==(const TranslateResult& other) const {
    return std::tie(
    commands,
    warnings
    ) == std::tie(
    other.commands,
    other.warnings
    );
}

std::vector<std::string> const& TranslateResult::getCommands() const {
    return commands;
}
void TranslateResult::setCommands(std::vector<std::string> p_commands) {
    commands = std::move(p_commands);
}

std::vector<TranslateMessage> const& TranslateResult::getWarnings() const {
    return warnings;
}
void TranslateResult::setWarnings(std::vector<TranslateMessage> p_warnings) {
    warnings = std::move(p_warnings);
}

std::string TranslateResult::toString() const {
    std::stringstream ss;
    ss << "TranslateResult { ";
    ss << "commands: ";
    ss << "[ ";
    for (size_t i = 0; i < this->commands.size(); i++) {
        ss << this->commands[i];
        if (i < this->commands.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << ", ";
    ss << "warnings: ";
    ss << "[ ";
    for (size_t i = 0; i < this->warnings.size(); i++) {
        ss << this->warnings[i].toString();
        if (i < this->warnings.size() - 1) {
            ss << ", ";
        }
    }
    ss << " ]";
    ss << " }";
    return ss.str();
}

void TranslateResult::populateFromByteArray(const std::string& buffer) {
    nlohmann::json obj = nlohmann::json::from_bson(buffer);
    *this = obj.template get<TranslateResult>();
}

std::string TranslateResult::toByteArray() const {
    nlohmann::json obj = *this;
    std::string buffer;
    nlohmann::json::to_bson(obj, buffer);
    return buffer;
}

} // namespace gcode
} // namespace motion
} // namespace zaber
