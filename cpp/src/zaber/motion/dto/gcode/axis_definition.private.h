// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/gcode/axis_definition.h"

#include "zaber/motion/utils/serialization_utils.private.h"

namespace zaber {
namespace motion {
namespace gcode {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(AxisDefinition, peripheralId, microstepResolution)

} // namespace gcode
} // namespace motion
} // namespace zaber
