// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma once

#include "zaber/motion/dto/firmware_version.h"

#include "zaber/motion/utils/serialization_utils.private.h"

#pragma push_macro("major")
#undef major
#pragma push_macro("minor")
#undef minor

namespace zaber {
namespace motion {

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(FirmwareVersion, major, minor, build)

} // namespace motion
} // namespace zaber

#pragma pop_macro("major")
#pragma pop_macro("minor")
