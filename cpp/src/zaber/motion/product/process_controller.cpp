﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/utils/functional_utils.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/product/process.h"
#include "zaber/motion/product/process_controller.h"
#include "zaber/motion/ascii/storage.h"
#include "zaber/motion/ascii/warnings.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace product {

/**
 * Creates instance of `ProcessController` of the given device.
 * If the device is identified, this constructor will ensure it is a process controller.
 */
ProcessController::ProcessController(Device device): _device(std::move(device)) {
    this->verifyIsProcessController();
}

/**
 * Detects the process controllers on the connection.
 * @param connection The connection to detect process controllers on.
 * @param identify If the Process Controllers should be identified upon detection.
 * @return A list of all `ProcessController`s on the connection.
 */
std::vector<ProcessController> ProcessController::detect(const BaseConnection& connection, bool identify) {

    ::zaber::motion::requests::DeviceDetectRequest request;
    request.setType(::zaber::motion::requests::DeviceType::PROCESS_CONTROLLER);
    request.setInterfaceId(connection.getInterfaceId());
    request.setIdentifyDevices(identify);
    ::zaber::motion::requests::DeviceDetectResponse response;
    ::zaber::motion::callGateway("device/detect", request, &response);

    return zml_util::map_vec<int,ProcessController>(response.getDevices(), [connection](const int& adr) {
        return ProcessController(Device(connection, adr));
    });
}

/**
 * Gets an Process class instance which allows you to control a particular voltage source.
 * Axes are numbered from 1.
 * @param processNumber Number of process to control.
 * @return Process instance.
 */
Process ProcessController::getProcess(int processNumber) {
    if (processNumber <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; processes are numbered from 1.");
    }

    return {*this, processNumber};
}

/**
 * Checks if this is a process controller or some other type of device and throws an error if it is not.
 */
void ProcessController::verifyIsProcessController() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::callGateway("process_controller/verify", request);
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string ProcessController::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * The base device of this process controller.
 */
Device ProcessController::getDevice() const {
    return this->_device;
}

}  // namespace product
}  // namespace motion
}  // namespace zaber
