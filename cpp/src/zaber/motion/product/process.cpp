﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/product/process.h"
#include "zaber/motion/dto/ascii/response.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"

using Axis = zaber::motion::ascii::Axis;
using AxisSettings = zaber::motion::ascii::AxisSettings;
using AxisStorage = zaber::motion::ascii::AxisStorage;
using Warnings = zaber::motion::ascii::Warnings;
using Response = zaber::motion::ascii::Response;
using SetStateAxisResponse = zaber::motion::ascii::SetStateAxisResponse;

namespace zaber { namespace motion { namespace product {

Process::Process(ProcessController controller, int processNumber): _controller(std::move(controller)), _processNumber(processNumber) {
}

/**
 * Sets the enabled state of the driver.
 * @param enabled If true (default) enables drive. If false disables.
 */
void Process::enable(bool enabled) {

    ::zaber::motion::requests::ProcessOn request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setOn(enabled);
    ::zaber::motion::callGateway("process-controller/enable", request);
}

/**
 * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
 * @param duration How long to leave the process on.
 * @param unit Units of time.
 */
void Process::on(double duration, Units unit) {

    ::zaber::motion::requests::ProcessOn request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setOn(true);
    request.setDuration(duration);
    request.setUnit(unit);
    ::zaber::motion::callGateway("process-controller/on", request);
}

/**
 * Turns this process off.
 */
void Process::off() {

    ::zaber::motion::requests::ProcessOn request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setOn(false);
    ::zaber::motion::callGateway("process-controller/on", request);
}

/**
 * Sets the control mode of this process.
 * @param mode Mode to set this process to.
 */
void Process::setMode(ProcessControllerMode mode) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setSetting("process.control.mode");
    request.setValue(static_cast<double>(mode));
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Gets the control mode of this process.
 * @return Control mode.
 */
ProcessControllerMode Process::getMode() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setSetting("process.control.mode");
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return static_cast<ProcessControllerMode>(response.getValue());
}

/**
 * Gets the source used to control this process.
 * @return The source providing feedback for this process.
 */
ProcessControllerSource Process::getSource() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    ProcessControllerSource response;
    ::zaber::motion::callGateway("process_controller/get_source", request, &response);

    return response;
}

/**
 * Sets the source used to control this process.
 * @param source Sets the source that should provide feedback for this process.
 */
void Process::setSource(const ProcessControllerSource& source) {

    ::zaber::motion::requests::SetProcessControllerSource request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setSource(source);
    ::zaber::motion::callGateway("process_controller/set_source", request);
}

/**
 * Gets the current value of the source used to control this process.
 * @return The current value of this process's controlling source.
 */
Measurement Process::getInput() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    Measurement response;
    ::zaber::motion::callGateway("process_controller/get_input", request, &response);

    return response;
}

/**
 * Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
 */
void Process::bridge() {

    ::zaber::motion::requests::ProcessOn request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setOn(true);
    ::zaber::motion::callGateway("process_controller/bridge", request);
}

/**
 * Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
 * This method is only callable on axis 1 and 3.
 */
void Process::unbridge() {

    ::zaber::motion::requests::ProcessOn request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setOn(false);
    ::zaber::motion::callGateway("process_controller/bridge", request);
}

/**
 * Detects if the given process is in bridging mode.
 * @return Whether this process is bridged with its neighbor.
 */
bool Process::isBridge() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("process_controller/is_bridge", request, &response);

    return response.getValue();
}

/**
 * Sends a generic ASCII command to this process' underlying axis.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Process::genericCommand(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    Response response;
    ::zaber::motion::callGateway("interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic ASCII command to this process' underlying axis.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Process::genericCommand(const std::string& command, const Process::GenericCommandOptions& options) {
    return Process::genericCommand(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this process and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when a device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Process::genericCommandMultiResponse(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    ::zaber::motion::requests::GenericCommandResponseCollection response;
    ::zaber::motion::callGateway("interface/generic_command_multi_response", request, &response);

    return response.getResponses();
}

/**
 * Sends a generic ASCII command to this process and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when a device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Process::genericCommandMultiResponse(const std::string& command, const Process::GenericCommandMultiResponseOptions& options) {
    return Process::genericCommandMultiResponse(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this process without expecting a response and without adding a message ID
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 */
void Process::genericCommandNoResponse(const std::string& command) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setCommand(command);
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Returns a serialization of the current process state that can be saved and reapplied.
 * @return A serialization of the current state of the process.
 */
std::string Process::getState() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_state", request, &response);

    return response.getValue();
}

/**
 * Applies a saved state to this process.
 * @param state The state object to apply to this process.
 * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
 */
SetStateAxisResponse Process::setState(const std::string& state) {

    ::zaber::motion::requests::SetStateRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setState(state);
    SetStateAxisResponse response;
    ::zaber::motion::callGateway("device/set_axis_state", request, &response);

    return response;
}

/**
 * Checks if a state can be applied to this process.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param firmwareVersion The firmware version of the device to apply the state to.
 * Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this process.
 */
std::optional<std::string> Process::canSetState(const std::string& state, const std::optional<FirmwareVersion>& firmwareVersion) {

    ::zaber::motion::requests::CanSetStateRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setState(state);
    request.setFirmwareVersion(firmwareVersion);
    ::zaber::motion::requests::CanSetStateAxisResponse response;
    ::zaber::motion::callGateway("device/can_set_axis_state", request, &response);

    return response.getError();
}

/**
 * Checks if a state can be applied to this process.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param options A struct of type CanSetStateOptions. It has the following members:
 * * `firmwareVersion`: The firmware version of the device to apply the state to.
 *   Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this process.
 */
std::optional<std::string> Process::canSetState(const std::string& state, const Process::CanSetStateOptions& options) {
    return Process::canSetState(state, options.firmwareVersion);
}

/**
 * Returns a string that represents the process.
 * @return A string that represents the process.
 */
std::string Process::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getController().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getController().getDevice().getDeviceAddress());
    request.setAxis(this->getProcessNumber());
    request.setTypeOverride("Process");
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/axis_to_string", request, &response);

    return response.getValue();
}

/**
 * Controller for this process.
 */
ProcessController Process::getController() const {
    return this->_controller;
}

/**
 * The process number identifies the process on the controller.
 */
int Process::getProcessNumber() const {
    return this->_processNumber;
}

Axis Process::getAxis() const {
    return {this->_controller.getDevice(), this->_processNumber};
}

/**
 * Settings and properties of this process.
 */
AxisSettings Process::getSettings() const {
    return {this->getAxis()};
}

/**
 * Key-value storage of this process.
 */
AxisStorage Process::getStorage() const {
    return {this->getAxis()};
}

/**
 * Warnings and faults of this process.
 */
Warnings Process::getWarnings() const {
    return {this->_controller.getDevice(), this->_processNumber};
}

}  // namespace product
}  // namespace motion
}  // namespace zaber
