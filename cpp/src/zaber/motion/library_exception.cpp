#include "library_exception.h"
#include <stdexcept>

namespace zaber { namespace motion {

LibraryIntegrationException::LibraryIntegrationException(const std::string& message): std::runtime_error(message) {
}

std::string LibraryIntegrationException::message() const {
    return this->what();
}

}  // namespace motion
}  // namespace zaber