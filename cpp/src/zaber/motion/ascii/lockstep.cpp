﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/lockstep.h"
#include "zaber/motion/dto/ascii/lockstep_axes.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Lockstep::Lockstep(Device device, int lockstepGroupId): _device(std::move(device)), _lockstepGroupId(lockstepGroupId) {
}

/**
 * Activate the lockstep group on the axes specified.
 * @param axes The numbers of axes in the lockstep group.
 */
void Lockstep::enable(std::initializer_list<int> axes) {

    ::zaber::motion::requests::LockstepEnableRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setAxes(std::vector<int>(axes));
    ::zaber::motion::callGateway("device/lockstep_enable", request);
}

/**
 * Disable the lockstep group.
 */
void Lockstep::disable() {

    ::zaber::motion::requests::LockstepDisableRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    ::zaber::motion::callGateway("device/lockstep_disable", request);
}

/**
 * Stops ongoing lockstep group movement. Decelerates until zero speed.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::stop(bool waitUntilIdle) {

    ::zaber::motion::requests::LockstepStopRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/lockstep_stop", request);
}

/**
 * Stops ongoing lockstep group movement. Decelerates until zero speed.
 * @param options A struct of type StopOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::stop(const Lockstep::StopOptions& options) {
    Lockstep::stop(options.waitUntilIdle);
}

/**
 * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::home(bool waitUntilIdle) {

    ::zaber::motion::requests::LockstepHomeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/lockstep_home", request);
}

/**
 * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
 * @param options A struct of type HomeOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::home(const Lockstep::HomeOptions& options) {
    Lockstep::home(options.waitUntilIdle);
}

/**
 * Move the first axis of the lockstep group to an absolute position.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param position Absolute position.
 * @param unit Units of position.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Lockstep::moveAbsolute(double position, Units unit, bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::LockstepMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setType(::zaber::motion::requests::AxisMoveType::ABS);
    request.setArg(position);
    request.setUnit(unit);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/lockstep_move", request);
}

/**
 * Move the first axis of the lockstep group to an absolute position.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param position Absolute position.
 * @param unit Units of position.
 * @param options A struct of type MoveAbsoluteOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Lockstep::moveAbsolute(double position, Units unit, const Lockstep::MoveAbsoluteOptions& options) {
    Lockstep::moveAbsolute(position, unit, options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Move the first axis of the lockstep group to a position relative to its current position.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param position Relative position.
 * @param unit Units of position.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Lockstep::moveRelative(double position, Units unit, bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::LockstepMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setType(::zaber::motion::requests::AxisMoveType::REL);
    request.setArg(position);
    request.setUnit(unit);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/lockstep_move", request);
}

/**
 * Move the first axis of the lockstep group to a position relative to its current position.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param position Relative position.
 * @param unit Units of position.
 * @param options A struct of type MoveRelativeOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Lockstep::moveRelative(double position, Units unit, const Lockstep::MoveRelativeOptions& options) {
    Lockstep::moveRelative(position, unit, options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Moves the first axis of the lockstep group at the specified speed.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param velocity Movement velocity.
 * @param unit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Lockstep::moveVelocity(double velocity, Units unit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::LockstepMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setType(::zaber::motion::requests::AxisMoveType::VEL);
    request.setArg(velocity);
    request.setUnit(unit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/lockstep_move", request);
}

/**
 * Moves the first axis of the lockstep group at the specified speed.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param velocity Movement velocity.
 * @param unit Units of velocity.
 * @param options A struct of type MoveVelocityOptions. It has the following members:
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Lockstep::moveVelocity(double velocity, Units unit, const Lockstep::MoveVelocityOptions& options) {
    Lockstep::moveVelocity(velocity, unit, options.acceleration, options.accelerationUnit);
}

/**
 * Moves the first axis of the lockstep group in a sinusoidal trajectory.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
 * @param amplitudeUnits Units of position.
 * @param period Period of the sinusoidal motion in milliseconds.
 * @param periodUnits Units of time.
 * @param count Number of sinusoidal cycles to complete.
 * Must be a multiple of 0.5
 * If count is not specified or set to 0, the axis will move indefinitely.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::moveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count, bool waitUntilIdle) {

    ::zaber::motion::requests::LockstepMoveSinRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setAmplitude(amplitude);
    request.setAmplitudeUnits(amplitudeUnits);
    request.setPeriod(period);
    request.setPeriodUnits(periodUnits);
    request.setCount(count);
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/lockstep_move_sin", request);
}

/**
 * Moves the first axis of the lockstep group in a sinusoidal trajectory.
 * The other axes in the lockstep group maintain their offsets throughout movement.
 * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
 * @param amplitudeUnits Units of position.
 * @param period Period of the sinusoidal motion in milliseconds.
 * @param periodUnits Units of time.
 * @param options A struct of type MoveSinOptions. It has the following members:
 * * `count`: Number of sinusoidal cycles to complete.
 *   Must be a multiple of 0.5
 *   If count is not specified or set to 0, the axis will move indefinitely.
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Lockstep::moveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, const Lockstep::MoveSinOptions& options) {
    Lockstep::moveSin(amplitude, amplitudeUnits, period, periodUnits, options.count, options.waitUntilIdle);
}

/**
 * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
 * If the sinusoidal motion was started with an integer-plus-half cycle count,
 * the motion ends at the half-way point of the sinusoidal trajectory.
 * @param waitUntilIdle Determines whether function should return after the movement is finished.
 */
void Lockstep::moveSinStop(bool waitUntilIdle) {

    ::zaber::motion::requests::LockstepStopRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/lockstep_move_sin_stop", request);
}

/**
 * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
 * If the sinusoidal motion was started with an integer-plus-half cycle count,
 * the motion ends at the half-way point of the sinusoidal trajectory.
 * @param options A struct of type MoveSinStopOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished.
 */
void Lockstep::moveSinStop(const Lockstep::MoveSinStopOptions& options) {
    Lockstep::moveSinStop(options.waitUntilIdle);
}

/**
 * Moves the axes to the maximum valid position.
 * The axes in the lockstep group maintain their offsets throughout movement.
 * Respects lim.max for all axes in the group.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Lockstep::moveMax(bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::LockstepMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setType(::zaber::motion::requests::AxisMoveType::MAX);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/lockstep_move", request);
}

/**
 * Moves the axes to the maximum valid position.
 * The axes in the lockstep group maintain their offsets throughout movement.
 * Respects lim.max for all axes in the group.
 * @param options A struct of type MoveMaxOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Lockstep::moveMax(const Lockstep::MoveMaxOptions& options) {
    Lockstep::moveMax(options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Moves the axes to the minimum valid position.
 * The axes in the lockstep group maintain their offsets throughout movement.
 * Respects lim.min for all axes in the group.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Lockstep::moveMin(bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::LockstepMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setType(::zaber::motion::requests::AxisMoveType::MIN);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/lockstep_move", request);
}

/**
 * Moves the axes to the minimum valid position.
 * The axes in the lockstep group maintain their offsets throughout movement.
 * Respects lim.min for all axes in the group.
 * @param options A struct of type MoveMinOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Lockstep::moveMin(const Lockstep::MoveMinOptions& options) {
    Lockstep::moveMin(options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Waits until the lockstep group stops moving.
 * @param throwErrorOnFault Determines whether to throw error when fault is observed.
 */
void Lockstep::waitUntilIdle(bool throwErrorOnFault) {

    ::zaber::motion::requests::LockstepWaitUntilIdleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setThrowErrorOnFault(throwErrorOnFault);
    ::zaber::motion::callGateway("device/lockstep_wait_until_idle", request);
}

/**
 * Waits until the lockstep group stops moving.
 * @param options A struct of type WaitUntilIdleOptions. It has the following members:
 * * `throwErrorOnFault`: Determines whether to throw error when fault is observed.
 */
void Lockstep::waitUntilIdle(const Lockstep::WaitUntilIdleOptions& options) {
    Lockstep::waitUntilIdle(options.throwErrorOnFault);
}

/**
 * Returns bool indicating whether the lockstep group is executing a motion command.
 * @return True if the axes are currently executing a motion command.
 */
bool Lockstep::isBusy() {

    ::zaber::motion::requests::LockstepEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/lockstep_is_busy", request, &response);

    return response.getValue();
}

/**
 * Deprecated: Use GetAxisNumbers instead.
 *
 * Gets the axes of the lockstep group.
 * @return LockstepAxes instance which contains the axes numbers of the lockstep group.
 */
LockstepAxes Lockstep::getAxes() {

    ::zaber::motion::requests::LockstepEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    LockstepAxes response;
    ::zaber::motion::callGateway("device/lockstep_get_axes", request, &response);

    return response;
}

/**
 * Gets the axis numbers of the lockstep group.
 * @return Axis numbers in order specified when enabling lockstep.
 */
std::vector<int> Lockstep::getAxisNumbers() {

    ::zaber::motion::requests::LockstepEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    ::zaber::motion::requests::LockstepGetAxisNumbersResponse response;
    ::zaber::motion::callGateway("device/lockstep_get_axis_numbers", request, &response);

    return response.getAxes();
}

/**
 * Gets the initial offsets of secondary axes of an enabled lockstep group.
 * @param unit Units of position.
 * Uses primary axis unit conversion.
 * @return Initial offset for each axis of the lockstep group.
 */
std::vector<double> Lockstep::getOffsets(Units unit) {

    ::zaber::motion::requests::LockstepGetRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleArrayResponse response;
    ::zaber::motion::callGateway("device/lockstep_get_offsets", request, &response);

    return response.getValues();
}

/**
 * Gets the twists of secondary axes of an enabled lockstep group.
 * @param unit Units of position.
 * Uses primary axis unit conversion.
 * @return Difference between the initial offset and the actual offset for each axis of the lockstep group.
 */
std::vector<double> Lockstep::getTwists(Units unit) {

    ::zaber::motion::requests::LockstepGetRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleArrayResponse response;
    ::zaber::motion::callGateway("device/lockstep_get_twists", request, &response);

    return response.getValues();
}

/**
 * Returns current position of the primary axis.
 * @param unit Units of the position.
 * @return Primary axis position.
 */
double Lockstep::getPosition(Units unit) {

    ::zaber::motion::requests::LockstepGetRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/lockstep_get_pos", request, &response);

    return response.getValue();
}

/**
 * Sets lockstep twist tolerance.
 * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
 * @param tolerance Twist tolerance.
 * @param unit Units of the tolerance.
 * Uses primary axis unit conversion when setting to all axes,
 * otherwise uses specified secondary axis unit conversion.
 * @param axisIndex Optional index of a secondary axis to set the tolerance for.
 * If left empty or set to 0, the tolerance is set to all the secondary axes.
 */
void Lockstep::setTolerance(double tolerance, Units unit, int axisIndex) {

    ::zaber::motion::requests::LockstepSetRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    request.setValue(tolerance);
    request.setUnit(unit);
    request.setAxisIndex(axisIndex);
    ::zaber::motion::callGateway("device/lockstep_set_tolerance", request);
}

/**
 * Sets lockstep twist tolerance.
 * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
 * @param tolerance Twist tolerance.
 * @param unit Units of the tolerance.
 * Uses primary axis unit conversion when setting to all axes,
 * otherwise uses specified secondary axis unit conversion.
 * @param options A struct of type SetToleranceOptions. It has the following members:
 * * `axisIndex`: Optional index of a secondary axis to set the tolerance for.
 *   If left empty or set to 0, the tolerance is set to all the secondary axes.
 */
void Lockstep::setTolerance(double tolerance, Units unit, const Lockstep::SetToleranceOptions& options) {
    Lockstep::setTolerance(tolerance, unit, options.axisIndex);
}

/**
 * Checks if the lockstep group is currently enabled on the device.
 * @return True if a lockstep group with this ID is enabled on the device.
 */
bool Lockstep::isEnabled() {

    ::zaber::motion::requests::LockstepEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/lockstep_is_enabled", request, &response);

    return response.getValue();
}

/**
 * Returns a string which represents the enabled lockstep group.
 * @return String which represents the enabled lockstep group.
 */
std::string Lockstep::toString() const {

    ::zaber::motion::requests::LockstepEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setLockstepGroupId(this->getLockstepGroupId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/lockstep_to_string", request, &response);

    return response.getValue();
}

/**
 * Device that controls this lockstep group.
 */
Device Lockstep::getDevice() const {
    return this->_device;
}

/**
 * The number that identifies the lockstep group on the device.
 */
int Lockstep::getLockstepGroupId() const {
    return this->_lockstepGroupId;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
