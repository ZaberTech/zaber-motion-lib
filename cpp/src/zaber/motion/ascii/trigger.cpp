﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/ascii/trigger.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Trigger::Trigger(Device device, int triggerNumber): _device(std::move(device)), _triggerNumber(triggerNumber) {
}

/**
 * Enables the trigger.
 * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
 * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
 * @param count Number of times the trigger will fire before disabling itself.
 * If count is not specified, or 0, the trigger will fire indefinitely.
 */
void Trigger::enable(int count) {
    if (count < 0) {
        throw exceptions::InvalidArgumentException("Invalid value; count must be 0 or positive.");
    }

    ::zaber::motion::requests::TriggerEnableRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setCount(count);
    ::zaber::motion::callGateway("trigger/enable", request);
}

/**
 * Disables the trigger.
 * Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
 */
void Trigger::disable() {

    ::zaber::motion::requests::TriggerEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    ::zaber::motion::callGateway("trigger/disable", request);
}

/**
 * Gets the state of the trigger.
 * @return Complete state of the trigger.
 */
TriggerState Trigger::getState() {

    ::zaber::motion::requests::TriggerEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    TriggerState response;
    ::zaber::motion::callGateway("trigger/get_state", request, &response);

    return response;
}

/**
 * Gets the enabled state of the trigger.
 * @return Whether the trigger is enabled and the number of times it will fire.
 */
TriggerEnabledState Trigger::getEnabledState() {

    ::zaber::motion::requests::TriggerEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    TriggerEnabledState response;
    ::zaber::motion::callGateway("trigger/get_enabled_state", request, &response);

    return response;
}

/**
 * Set a generic trigger condition.
 * @param condition The condition to set for this trigger.
 */
void Trigger::fireWhen(const std::string& condition) {

    ::zaber::motion::requests::TriggerFireWhenRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setCondition(condition);
    ::zaber::motion::callGateway("trigger/fire_when", request);
}

/**
 * Set a trigger condition for when an encoder position has changed by a specific distance.
 * @param axis The axis to monitor for this condition.
 * May be set to 0 on single-axis devices only.
 * @param distance The measured encoder distance between trigger fires.
 * @param unit Units of dist.
 */
void Trigger::fireWhenEncoderDistanceTravelled(int axis, double distance, Units unit) {
    if (distance <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; encoder distance must be a positive value.");
    }

    ::zaber::motion::requests::TriggerFireWhenDistanceTravelledRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAxis(axis);
    request.setDistance(distance);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/fire_when_encoder_distance_travelled", request);
}

/**
 * Set a trigger condition for when an axis position has changed by a specific distance.
 * @param axis The axis to monitor for this condition.
 * May be set to 0 on single-axis devices only.
 * @param distance The measured distance between trigger fires.
 * @param unit Units of dist.
 */
void Trigger::fireWhenDistanceTravelled(int axis, double distance, Units unit) {
    if (distance <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; distance must be a positive value.");
    }

    ::zaber::motion::requests::TriggerFireWhenDistanceTravelledRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAxis(axis);
    request.setDistance(distance);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/fire_when_distance_travelled", request);
}

/**
 * Set a trigger condition based on an IO channel value.
 * @param portType The type of IO channel to monitor.
 * @param channel The IO channel to monitor.
 * @param triggerCondition Comparison operator.
 * @param value Comparison value.
 */
void Trigger::fireWhenIo(IoPortType portType, int channel, TriggerCondition triggerCondition, double value) {
    if (channel <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; channel must be a positive value.");
    }

    ::zaber::motion::requests::TriggerFireWhenIoRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setPortType(portType);
    request.setChannel(channel);
    request.setTriggerCondition(triggerCondition);
    request.setValue(value);
    ::zaber::motion::callGateway("trigger/fire_when_io", request);
}

/**
 * Set a trigger condition based on a setting value.
 * @param axis The axis to monitor for this condition.
 * Set to 0 for device-scope settings.
 * @param setting The setting to monitor.
 * @param triggerCondition Comparison operator.
 * @param value Comparison value.
 * @param unit Units of value.
 */
void Trigger::fireWhenSetting(int axis, const std::string& setting, TriggerCondition triggerCondition, double value, Units unit) {

    ::zaber::motion::requests::TriggerFireWhenSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAxis(axis);
    request.setSetting(setting);
    request.setTriggerCondition(triggerCondition);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/fire_when_setting", request);
}

/**
 * Set a trigger condition based on an absolute setting value.
 * @param axis The axis to monitor for this condition.
 * Set to 0 for device-scope settings.
 * @param setting The setting to monitor.
 * @param triggerCondition Comparison operator.
 * @param value Comparison value.
 * @param unit Units of value.
 */
void Trigger::fireWhenAbsoluteSetting(int axis, const std::string& setting, TriggerCondition triggerCondition, double value, Units unit) {

    ::zaber::motion::requests::TriggerFireWhenSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAxis(axis);
    request.setSetting(setting);
    request.setTriggerCondition(triggerCondition);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/fire_when_setting_absolute", request);
}

/**
 * Set a trigger condition based on a time interval.
 * @param interval The time interval between trigger fires.
 * @param unit Units of time.
 */
void Trigger::fireAtInterval(double interval, Units unit) {
    if (interval <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; interval must be a positive value.");
    }

    ::zaber::motion::requests::TriggerFireAtIntervalRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setInterval(interval);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/fire_at_interval", request);
}

/**
 * Set a command to be a trigger action.
 * @param action The action number to assign the command to.
 * @param axis The axis to on which to run this command.
 * Set to 0 for device-scope settings or to run command on all axes.
 * @param command The command to run when the action is triggered.
 */
void Trigger::onFire(TriggerAction action, int axis, const std::string& command) {

    ::zaber::motion::requests::TriggerOnFireRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAction(action);
    request.setAxis(axis);
    request.setCommand(command);
    ::zaber::motion::callGateway("trigger/on_fire", request);
}

/**
 * Set a trigger action to update a setting.
 * @param action The action number to assign the command to.
 * @param axis The axis on which to change the setting.
 * Set to 0 to change the setting for the device.
 * @param setting The name of the setting to change.
 * @param operation The operation to apply to the setting.
 * @param value Operation value.
 * @param unit Units of value.
 */
void Trigger::onFireSet(TriggerAction action, int axis, const std::string& setting, TriggerOperation operation, double value, Units unit) {

    ::zaber::motion::requests::TriggerOnFireSetRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAction(action);
    request.setAxis(axis);
    request.setSetting(setting);
    request.setOperation(operation);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("trigger/on_fire_set", request);
}

/**
 * Set a trigger action to update a setting with the value of another setting.
 * @param action The action number to assign the command to.
 * @param axis The axis on which to change the setting.
 * Set to 0 to change the setting for the device.
 * @param setting The name of the setting to change.
 * Must have either integer or boolean type.
 * @param operation The operation to apply to the setting.
 * @param fromAxis The axis from which to read the setting.
 * Set to 0 to read the setting from the device.
 * @param fromSetting The name of the setting to read.
 * Must have either integer or boolean type.
 */
void Trigger::onFireSetToSetting(TriggerAction action, int axis, const std::string& setting, TriggerOperation operation, int fromAxis, const std::string& fromSetting) {

    ::zaber::motion::requests::TriggerOnFireSetToSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAction(action);
    request.setAxis(axis);
    request.setSetting(setting);
    request.setOperation(operation);
    request.setFromAxis(fromAxis);
    request.setFromSetting(fromSetting);
    ::zaber::motion::callGateway("trigger/on_fire_set_to_setting", request);
}

/**
 * Clear a trigger action.
 * @param action The action number to clear.
 * The default option is to clear all actions.
 */
void Trigger::clearAction(TriggerAction action) {

    ::zaber::motion::requests::TriggerClearActionRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setAction(action);
    ::zaber::motion::callGateway("trigger/clear_action", request);
}

/**
 * Returns the label for the trigger.
 * @return The label for the trigger.
 */
std::string Trigger::getLabel() {

    ::zaber::motion::requests::TriggerEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("trigger/get_label", request, &response);

    return response.getValue();
}

/**
 * Sets the label for the trigger.
 * @param label The label to set for this trigger.
 * If no value or an empty string is provided, this label is deleted.
 */
void Trigger::setLabel(const std::optional<std::string>& label) {

    ::zaber::motion::requests::TriggerSetLabelRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setTriggerNumber(this->getTriggerNumber());
    request.setLabel(label);
    ::zaber::motion::callGateway("trigger/set_label", request);
}

/**
 * Device that this trigger belongs to.
 */
Device Trigger::getDevice() const {
    return this->_device;
}

/**
 * Number of this trigger.
 */
int Trigger::getTriggerNumber() const {
    return this->_triggerNumber;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
