﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/pvt_sequence.h"
#include "zaber/motion/ascii/pvt_buffer.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

PvtSequence::PvtSequence(Device device, int pvtId): _device(std::move(device)), _pvtId(pvtId) {
}

PvtSequence::PvtSequence(): _device(), _pvtId(-1) {
}

/**
 * Setup the PVT sequence to control the specified axes and to queue actions on the device.
 * Allows use of lockstep axes in a PVT sequence.
 * @param pvtAxes Definition of the PVT sequence axes.
 */
void PvtSequence::setupLiveComposite(std::initializer_list<PvtAxisDefinition> pvtAxes) {

    ::zaber::motion::requests::StreamSetupLiveCompositeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setPvtAxes(std::vector<PvtAxisDefinition>(pvtAxes));
    ::zaber::motion::callGateway("device/stream_setup_live_composite", request);
}

/**
 * Setup the PVT sequence to control the specified axes and to queue actions on the device.
 * @param axes Numbers of physical axes to setup the PVT sequence on.
 */
void PvtSequence::setupLive(std::initializer_list<int> axes) {

    ::zaber::motion::requests::StreamSetupLiveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setAxes(std::vector<int>(axes));
    ::zaber::motion::callGateway("device/stream_setup_live", request);
}

/**
 * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
 * Allows use of lockstep axes in a PVT sequence.
 * @param pvtBuffer The PVT buffer to queue actions in.
 * @param pvtAxes Definition of the PVT sequence axes.
 */
void PvtSequence::setupStoreComposite(const PvtBuffer& pvtBuffer, std::initializer_list<PvtAxisDefinition> pvtAxes) {

    ::zaber::motion::requests::StreamSetupStoreCompositeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setPvtBuffer(pvtBuffer.getBufferId());
    request.setPvtAxes(std::vector<PvtAxisDefinition>(pvtAxes));
    ::zaber::motion::callGateway("device/stream_setup_store_composite", request);
}

/**
 * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
 * @param pvtBuffer The PVT buffer to queue actions in.
 * @param axes Numbers of physical axes to setup the PVT sequence on.
 */
void PvtSequence::setupStore(const PvtBuffer& pvtBuffer, std::initializer_list<int> axes) {

    ::zaber::motion::requests::StreamSetupStoreRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setPvtBuffer(pvtBuffer.getBufferId());
    request.setAxes(std::vector<int>(axes));
    ::zaber::motion::callGateway("device/stream_setup_store", request);
}

/**
 * Append the actions in a PVT buffer to the sequence's queue.
 * @param pvtBuffer The PVT buffer to call.
 */
void PvtSequence::call(const PvtBuffer& pvtBuffer) {

    ::zaber::motion::requests::StreamCallRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setPvtBuffer(pvtBuffer.getBufferId());
    ::zaber::motion::callGateway("device/stream_call", request);
}

/**
 * Queues a point with absolute coordinates in the PVT sequence.
 * If some or all velocities are not provided, the sequence calculates the velocities
 * from surrounding points using finite difference.
 * The last point of the sequence must have defined velocity (likely zero).
 * @param positions Positions for the axes to move through, relative to their home positions.
 * @param velocities The axes velocities at the given point.
 * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
 * @param time The duration between the previous point in the sequence and this one.
 */
void PvtSequence::point(const std::vector<Measurement>& positions, const std::vector<std::optional<Measurement>>& velocities, const Measurement& time) {

    ::zaber::motion::requests::PvtPointRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setPositions(std::vector<Measurement>(positions));
    request.setVelocities(std::vector<std::optional<Measurement>>(velocities));
    request.setTime(time);
    ::zaber::motion::callGateway("device/stream_point", request);
}

/**
 * Queues a point with coordinates relative to the previous point in the PVT sequence.
 * If some or all velocities are not provided, the sequence calculates the velocities
 * from surrounding points using finite difference.
 * The last point of the sequence must have defined velocity (likely zero).
 * @param positions Positions for the axes to move through, relative to the previous point.
 * @param velocities The axes velocities at the given point.
 * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
 * @param time The duration between the previous point in the sequence and this one.
 */
void PvtSequence::pointRelative(const std::vector<Measurement>& positions, const std::vector<std::optional<Measurement>>& velocities, const Measurement& time) {

    ::zaber::motion::requests::PvtPointRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setPositions(std::vector<Measurement>(positions));
    request.setVelocities(std::vector<std::optional<Measurement>>(velocities));
    request.setTime(time);
    ::zaber::motion::callGateway("device/stream_point", request);
}

/**
 * Waits until the live PVT sequence executes all queued actions.
 * @param throwErrorOnFault Determines whether to throw error when fault is observed.
 */
void PvtSequence::waitUntilIdle(bool throwErrorOnFault) {

    ::zaber::motion::requests::StreamWaitUntilIdleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setThrowErrorOnFault(throwErrorOnFault);
    ::zaber::motion::callGateway("device/stream_wait_until_idle", request);
}

/**
 * Waits until the live PVT sequence executes all queued actions.
 * @param options A struct of type WaitUntilIdleOptions. It has the following members:
 * * `throwErrorOnFault`: Determines whether to throw error when fault is observed.
 */
void PvtSequence::waitUntilIdle(const PvtSequence::WaitUntilIdleOptions& options) {
    PvtSequence::waitUntilIdle(options.throwErrorOnFault);
}

/**
 * Cork the front of the PVT sequences's action queue, blocking execution.
 * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
 * Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
 * You can only cork an idle live PVT sequence.
 */
void PvtSequence::cork() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_cork", request);
}

/**
 * Uncork the front of the queue, unblocking command execution.
 * You can only uncork an idle live PVT sequence that is corked.
 */
void PvtSequence::uncork() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_uncork", request);
}

/**
 * Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
 * @return True if the PVT sequence is executing a queued action.
 */
bool PvtSequence::isBusy() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/stream_is_busy", request, &response);

    return response.getValue();
}

/**
 * Returns a string which represents the PVT sequence.
 * @return String which represents the PVT sequence.
 */
std::string PvtSequence::toString() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/stream_to_string", request, &response);

    return response.getValue();
}

/**
 * Disables the PVT sequence.
 * If the PVT sequence is not setup, this command does nothing.
 * Once disabled, the PVT sequence will no longer accept PVT commands.
 * The PVT sequence will process the rest of the commands in the queue until it is empty.
 */
void PvtSequence::disable() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_disable", request);
}

/**
 * Sends a generic ASCII command to the PVT sequence.
 * Keeps resending the command while the device rejects with AGAIN reason.
 * @param command Command and its parameters.
 */
void PvtSequence::genericCommand(const std::string& command) {

    ::zaber::motion::requests::StreamGenericCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setCommand(command);
    ::zaber::motion::callGateway("device/stream_generic_command", request);
}

/**
 * Sends a batch of generic ASCII commands to the PVT sequence.
 * Keeps resending command while the device rejects with AGAIN reason.
 * The batch is atomic in terms of thread safety.
 * @param batch Array of commands.
 */
void PvtSequence::genericCommandBatch(const std::vector<std::string>& batch) {

    ::zaber::motion::requests::StreamGenericCommandBatchRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setBatch(std::vector<std::string>(batch));
    ::zaber::motion::callGateway("device/stream_generic_command_batch", request);
}

/**
 * Queries the PVT sequence status from the device
 * and returns boolean indicating whether the PVT sequence is disabled.
 * Useful to determine if execution was interrupted by other movements.
 * @return True if the PVT sequence is disabled.
 */
bool PvtSequence::checkDisabled() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/stream_check_disabled", request, &response);

    return response.getValue();
}

/**
 * Makes the PVT sequence throw PvtDiscontinuityException when it encounters discontinuities (ND warning flag).
 */
void PvtSequence::treatDiscontinuitiesAsError() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_treat_discontinuities", request);
}

/**
 * Prevents PvtDiscontinuityException as a result of expected discontinuity when resuming the sequence.
 */
void PvtSequence::ignoreCurrentDiscontinuity() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_ignore_discontinuity", request);
}

/**
 * Gets the axes of the PVT sequence.
 * @return An array of axis numbers of the axes the PVT sequence is set up to control.
 */
std::vector<PvtAxisDefinition> PvtSequence::retrieveAxes() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::requests::StreamGetAxesResponse response;
    ::zaber::motion::callGateway("device/stream_get_axes", request, &response);

    return response.getPvtAxes();
}

/**
 * Get the mode of the PVT sequence.
 * @return Mode of the PVT sequence.
 */
PvtMode PvtSequence::retrieveMode() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    ::zaber::motion::requests::StreamModeResponse response;
    ::zaber::motion::callGateway("device/stream_get_mode", request, &response);

    return response.getPvtMode();
}

/**
 * Deprecated: Use PvtSequence.Io.SetDigitalOutput instead.
 *
 * Sets value for the specified digital output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform on the channel.
 */
void PvtSequence::setDigitalOutput(int channelNumber, DigitalOutputAction value) {

    ::zaber::motion::requests::StreamSetDigitalOutputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_digital_output", request);
}

/**
 * Deprecated: Use PvtSequence.Io.SetAllDigitalOutputs instead.
 *
 * Sets values for all digital output channels.
 * @param values The type of action to perform on the channel.
 */
void PvtSequence::setAllDigitalOutputs(const std::vector<DigitalOutputAction>& values) {

    ::zaber::motion::requests::StreamSetAllDigitalOutputsRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setValues(std::vector<DigitalOutputAction>(values));
    ::zaber::motion::callGateway("device/stream_set_all_digital_outputs", request);
}

/**
 * Deprecated: Use PvtSequence.Io.SetAnalogOutput instead.
 *
 * Sets value for the specified analog output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to.
 */
void PvtSequence::setAnalogOutput(int channelNumber, double value) {

    ::zaber::motion::requests::StreamSetAnalogOutputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_analog_output", request);
}

/**
 * Deprecated: Use PvtSequence.Io.SetAllAnalogOutputs instead.
 *
 * Sets values for all analog output channels.
 * @param values Voltage values to set the output channels to.
 */
void PvtSequence::setAllAnalogOutputs(const std::vector<double>& values) {

    ::zaber::motion::requests::StreamSetAllAnalogOutputsRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getPvtId());
    request.setPvt(true);
    request.setValues(std::vector<double>(values));
    ::zaber::motion::callGateway("device/stream_set_all_analog_outputs", request);
}

/**
 * Device that controls this PVT sequence.
 */
Device PvtSequence::getDevice() const {
    return this->_device;
}

/**
 * The number that identifies the PVT sequence on the device.
 */
int PvtSequence::getPvtId() const {
    return this->_pvtId;
}

/**
 * Current mode of the PVT sequence.
 */
PvtMode PvtSequence::getMode() const {
    return this->retrieveMode();
}

/**
 * An array of axes definitions the PVT sequence is set up to control.
 */
std::vector<PvtAxisDefinition> PvtSequence::getAxes() const {
    return this->retrieveAxes();
}

/**
 * Gets an object that provides access to I/O for this sequence.
 */
PvtIo PvtSequence::getIo() const {
    return {this->_device, this->getPvtId()};
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber
