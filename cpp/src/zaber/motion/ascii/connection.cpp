﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

// NOLINTBEGIN(bugprone-exception-escape)

#include "zaber/motion/ascii/connection.h"
#include "zaber/motion/ascii/transport.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/gateway/event_manager.h"
#include "zaber/motion/dto/requests/interface_type.h"
#include "zaber/motion/dto/requests/open_interface_request.h"
#include "zaber/motion/dto/requests/open_interface_response.h"
#include "zaber/motion/dto/requests/interface_empty_request.h"
#include "zaber/motion/dto/requests/generic_command_request.h"
#include "zaber/motion/utils/functional_utils.h"
#include "zaber/motion/exceptions/convert_exception.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"


namespace zaber { namespace motion { namespace ascii {

Connection::Connection(int interfaceId): BaseConnection(interfaceId) { }

/**
 * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
 * Zaber Launcher allows sharing of the port between multiple applications,
 * If port sharing is not desirable, use the `direct` parameter.
 * @param portName Name of the port to open.
 * @param baudRate Optional baud rate (defaults to 115200).
 * @param direct If true will connect to the serial port directly,
 * failing if the connection is already opened by a message router instance.
 * @return An object representing the port.
 */
Connection Connection::openSerialPort(const std::string& portName, int baudRate, bool direct) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::SERIAL_PORT);
    request.setPortName(portName);
    request.setBaudRate(baudRate);
    request.setRejectRoutedConnection(direct);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
 * Zaber Launcher allows sharing of the port between multiple applications,
 * If port sharing is not desirable, use the `direct` parameter.
 * @param portName Name of the port to open.
 * @param options A struct of type OpenSerialPortOptions. It has the following members:
 * * `baudRate`: Optional baud rate (defaults to 115200).
 * * `direct`: If true will connect to the serial port directly,
 *   failing if the connection is already opened by a message router instance.
 * @return An object representing the port.
 */
Connection Connection::openSerialPort(const std::string& portName, const Connection::OpenSerialPortOptions& options) {
    return Connection::openSerialPort(portName, options.baudRate, options.direct);
}

/**
 * Opens a TCP connection.
 * @param hostName Hostname or IP address.
 * @param port Optional port number (defaults to 55550).
 * @return An object representing the connection.
 */
Connection Connection::openTcp(const std::string& hostName, int port) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::TCP);
    request.setHostName(hostName);
    request.setPort(port);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a connection using a custom transport.
 * @param transport The custom connection transport.
 * @return An object representing the connection.
 */
Connection Connection::openCustom(const Transport& transport) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::CUSTOM);
    request.setTransport(transport.getTransportId());
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a secured connection to a cloud connected device chain.
 * Use this method to connect to devices on your account.
 * @param cloudId The cloud ID to connect to.
 * @param token The token to authenticate with. By default the connection will be unauthenticated.
 * @param connectionName The name of the connection to open.
 * Can be left empty to default to the only connection present.
 * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
 * @param realm The realm to connect to.
 * Can be left empty for the default account realm.
 * @param api The URL of the API to receive connection info from.
 * @return An object representing the connection.
 */
Connection Connection::openIot(const std::string& cloudId, const std::string& token, const std::optional<std::string>& connectionName, const std::optional<std::string>& realm, const std::string& api) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::IOT);
    request.setCloudId(cloudId);
    request.setToken(token);
    request.setConnectionName(connectionName);
    request.setRealm(realm);
    request.setApi(api);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a secured connection to a cloud connected device chain.
 * Use this method to connect to devices on your account.
 * @param cloudId The cloud ID to connect to.
 * @param options A struct of type OpenIotOptions. It has the following members:
 * * `token`: The token to authenticate with. By default the connection will be unauthenticated.
 * * `connectionName`: The name of the connection to open.
 *   Can be left empty to default to the only connection present.
 *   Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
 * * `realm`: The realm to connect to.
 *   Can be left empty for the default account realm.
 * * `api`: The URL of the API to receive connection info from.
 * @return An object representing the connection.
 */
Connection Connection::openIot(const std::string& cloudId, const Connection::OpenIotOptions& options) {
    return Connection::openIot(cloudId, options.token, options.connectionName, options.realm, options.api);
}

/**
 * Opens a connection to Zaber Launcher in your Local Area Network.
 * The connection is not secured.
 * @param hostName Hostname or IP address.
 * @param port Port number.
 * @param connectionName The name of the connection to open.
 * Can be left empty to default to the only connection present.
 * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
 * @return An object representing the connection.
 */
Connection Connection::openNetworkShare(const std::string& hostName, int port, const std::optional<std::string>& connectionName) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::NETWORK_SHARE);
    request.setHostName(hostName);
    request.setPort(port);
    request.setConnectionName(connectionName);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Close the connection.
 */
void Connection::close() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::callGateway("interface/close", request);
}

Connection& Connection::operator=(Connection&& other) noexcept {
    if (this != &other) {
        if (_interfaceId >= 0) {
            close();
            _interfaceId = -1;
        }

        resetEventHandlers();
        std::swap(_interfaceId, other._interfaceId);
        std::swap(_unknownResponseHandler, other._unknownResponseHandler);
        std::swap(_alertEventHandler, other._alertEventHandler);
        std::swap(_disconnectedHandler, other._disconnectedHandler);
    }
    return *this;
}

Connection::Connection(Connection&& other) noexcept: BaseConnection() {
    *this = std::move(other);
}

Connection::~Connection() {
    if (_interfaceId >= 0) {
        close();
    }
    resetEventHandlers();
}

void Connection::setUnknownResponseCallback(std::function<void(const UnknownResponseEvent&)> callback) {
    if (_unknownResponseHandler) {
        _unknownResponseHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::UNKNOWN_RESPONSE;
        };
        static auto mutator = [](const SdkEvent& event) {
            return std::static_pointer_cast<requests::UnknownResponseEventWrapper>(event.getEventData())->getUnknownResponse();
        };
        _unknownResponseHandler = EventManager::getSingleton().createEventHandler<UnknownResponseEvent>(callback, filter, mutator);
    }
}

void Connection::setAlertCallback(std::function<void(const AlertEvent&)> callback) {
    if (_alertEventHandler) {
        _alertEventHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::ALERT;
        };
        static auto mutator = [](const SdkEvent& event) {
            return std::static_pointer_cast<requests::AlertEventWrapper>(event.getEventData())->getAlert();
        };
        _alertEventHandler = EventManager::getSingleton().createEventHandler<AlertEvent>(callback, filter, mutator);
    }
}

void Connection::setDisconnectedCallback(std::function<void(const std::shared_ptr<exceptions::MotionLibException>&)> callback) {
    if (_disconnectedHandler) {
        _disconnectedHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::DISCONNECTED;
        };
        static auto mutator = [](const SdkEvent& event) {
            std::shared_ptr<requests::DisconnectedEvent> e = std::static_pointer_cast<requests::DisconnectedEvent>(event.getEventData());
            return exceptions::convertExceptionSP(e->getErrorType(), e->getErrorMessage());
        };
        _disconnectedHandler = EventManager::getSingleton()
            .createEventHandler<std::shared_ptr<exceptions::MotionLibException>>(callback, filter, mutator);
    }
}

void Connection::resetEventHandlers() {
    if (_unknownResponseHandler) {
        _unknownResponseHandler->setCallback(nullptr);
        _unknownResponseHandler = nullptr;
    }
    if (_alertEventHandler) {
        _alertEventHandler->setCallback(nullptr);
        _alertEventHandler = nullptr;
    }
    if (_disconnectedHandler) {
        _disconnectedHandler->setCallback(nullptr);
        _disconnectedHandler = nullptr;
    }
}

/**
 * Creates an instance of BaseConnection.
 */
BaseConnection::BaseConnection(int interfaceId): _interfaceId(interfaceId) {
}

/**
 * Sends a generic ASCII command to this connection.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param device Optional device address to send the command to.
 * @param axis Optional axis number to send the command to.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response BaseConnection::genericCommand(const std::string& command, int device, int axis, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand(command);
    request.setDevice(device);
    request.setAxis(axis);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    Response response;
    ::zaber::motion::callGateway("interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic ASCII command to this connection.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `device`: Optional device address to send the command to.
 * * `axis`: Optional axis number to send the command to.
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response BaseConnection::genericCommand(const std::string& command, const BaseConnection::GenericCommandOptions& options) {
    return BaseConnection::genericCommand(command, options.device, options.axis, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param device Optional device address to send the command to.
 * Specifying -1 omits the number completely.
 * @param axis Optional axis number to send the command to.
 * Specifying -1 omits the number completely.
 */
void BaseConnection::genericCommandNoResponse(const std::string& command, int device, int axis) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand(command);
    request.setDevice(device);
    request.setAxis(axis);
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandNoResponseOptions. It has the following members:
 * * `device`: Optional device address to send the command to.
 *   Specifying -1 omits the number completely.
 * * `axis`: Optional axis number to send the command to.
 *   Specifying -1 omits the number completely.
 */
void BaseConnection::genericCommandNoResponse(const std::string& command, const BaseConnection::GenericCommandNoResponseOptions& options) {
    BaseConnection::genericCommandNoResponse(command, options.device, options.axis);
}

/**
 * Sends a generic ASCII command to this connection and expect multiple responses,
 * either from one device or from many devices.
 * Responses are returned in order of arrival.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param device Optional device address to send the command to.
 * @param axis Optional axis number to send the command to.
 * @param checkErrors Controls whether to throw an exception when a device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> BaseConnection::genericCommandMultiResponse(const std::string& command, int device, int axis, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand(command);
    request.setDevice(device);
    request.setAxis(axis);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    ::zaber::motion::requests::GenericCommandResponseCollection response;
    ::zaber::motion::callGateway("interface/generic_command_multi_response", request, &response);

    return response.getResponses();
}

/**
 * Sends a generic ASCII command to this connection and expect multiple responses,
 * either from one device or from many devices.
 * Responses are returned in order of arrival.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `device`: Optional device address to send the command to.
 * * `axis`: Optional axis number to send the command to.
 * * `checkErrors`: Controls whether to throw an exception when a device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> BaseConnection::genericCommandMultiResponse(const std::string& command, const BaseConnection::GenericCommandMultiResponseOptions& options) {
    return BaseConnection::genericCommandMultiResponse(command, options.device, options.axis, options.checkErrors, options.timeout);
}

/**
 * Enables alerts for all devices on the connection.
 * This will change the "comm.alert" setting to 1 on all supported devices.
 */
void BaseConnection::enableAlerts() {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand("set comm.alert 1");
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Disables alerts for all devices on the connection.
 * This will change the "comm.alert" setting to 0 on all supported devices.
 */
void BaseConnection::disableAlerts() {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand("set comm.alert 0");
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Resets ASCII protocol message IDs. Only for testing purposes.
 */
void BaseConnection::resetIds() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::callGateway("interface/reset_ids", request);
}

/**
 * Gets a Device class instance which allows you to control a particular device on this connection.
 * Devices are numbered from 1.
 * @param deviceAddress Address of device intended to control. Address is configured for each device.
 * @return Device instance.
 */
Device BaseConnection::getDevice(int deviceAddress) {
    if (deviceAddress <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; physical devices are numbered from 1.");
    }

    return { *this, deviceAddress };
}

/**
 * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
 * @param firstAddress This is the address that the device closest to the computer is given.
 * Remaining devices are numbered consecutively.
 * @return Total number of devices that responded to the renumber.
 */
int BaseConnection::renumberDevices(int firstAddress) {
    if (firstAddress <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; device addresses are numbered from 1.");
    }

    ::zaber::motion::requests::RenumberRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setAddress(firstAddress);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/renumber_all", request, &response);

    return response.getValue();
}

/**
 * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
 * @param options A struct of type RenumberDevicesOptions. It has the following members:
 * * `firstAddress`: This is the address that the device closest to the computer is given.
 *   Remaining devices are numbered consecutively.
 * @return Total number of devices that responded to the renumber.
 */
int BaseConnection::renumberDevices(const BaseConnection::RenumberDevicesOptions& options) {
    return BaseConnection::renumberDevices(options.firstAddress);
}

/**
 * Attempts to detect any devices present on this connection.
 * @param identifyDevices Determines whether device identification should be performed as well.
 * @return Array of detected devices.
 */
std::vector<Device> BaseConnection::detectDevices(bool identifyDevices) {

    ::zaber::motion::requests::DeviceDetectRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setIdentifyDevices(identifyDevices);
    ::zaber::motion::requests::DeviceDetectResponse response;
    ::zaber::motion::callGateway("device/detect", request, &response);

    return zml_util::map_vec<int,Device>(response.getDevices(), [this](const int& i) { return Device(*this, i); });
}

/**
 * Attempts to detect any devices present on this connection.
 * @param options A struct of type DetectDevicesOptions. It has the following members:
 * * `identifyDevices`: Determines whether device identification should be performed as well.
 * @return Array of detected devices.
 */
std::vector<Device> BaseConnection::detectDevices(const BaseConnection::DetectDevicesOptions& options) {
    return BaseConnection::detectDevices(options.identifyDevices);
}

/**
 * Forgets all the information associated with devices on the connection.
 * Useful when devices are removed from the chain indefinitely.
 * @param exceptDevices Addresses of devices that should not be forgotten.
 */
void BaseConnection::forgetDevices(const std::vector<int>& exceptDevices) {

    ::zaber::motion::requests::ForgetDevicesRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setExceptDevices(std::vector<int>(exceptDevices));
    ::zaber::motion::callGateway("device/forget", request);
}

/**
 * Stops all of the devices on this connection.
 * @param waitUntilIdle Determines whether the function should return immediately
 * or wait until the devices are stopped.
 * @return The addresses of the devices that were stopped by this command.
 */
std::vector<int> BaseConnection::stopAll(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceOnAllRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::requests::DeviceOnAllResponse response;
    ::zaber::motion::callGateway("device/stop_all", request, &response);

    return response.getDeviceAddresses();
}

/**
 * Stops all of the devices on this connection.
 * @param options A struct of type StopAllOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether the function should return immediately
 *   or wait until the devices are stopped.
 * @return The addresses of the devices that were stopped by this command.
 */
std::vector<int> BaseConnection::stopAll(const BaseConnection::StopAllOptions& options) {
    return BaseConnection::stopAll(options.waitUntilIdle);
}

/**
 * Homes all of the devices on this connection.
 * @param waitUntilIdle Determines whether the function should return immediately
 * or wait until the devices are homed.
 * @return The addresses of the devices that were homed by this command.
 */
std::vector<int> BaseConnection::homeAll(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceOnAllRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::requests::DeviceOnAllResponse response;
    ::zaber::motion::callGateway("device/home_all", request, &response);

    return response.getDeviceAddresses();
}

/**
 * Homes all of the devices on this connection.
 * @param options A struct of type HomeAllOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether the function should return immediately
 *   or wait until the devices are homed.
 * @return The addresses of the devices that were homed by this command.
 */
std::vector<int> BaseConnection::homeAll(const BaseConnection::HomeAllOptions& options) {
    return BaseConnection::homeAll(options.waitUntilIdle);
}

/**
 * Returns a string that represents the connection.
 * @return A string that represents the connection.
 */
std::string BaseConnection::toString() const {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("interface/to_string", request, &response);

    return response.getValue();
}

/**
 * Returns default request timeout.
 * @return Default request timeout.
 */
int BaseConnection::retrieveTimeout() const {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("interface/get_timeout", request, &response);

    return response.getValue();
}

/**
 * Sets default request timeout.
 * @param timeout Default request timeout.
 */
void BaseConnection::changeTimeout(int timeout) const {

    ::zaber::motion::requests::SetInterfaceTimeoutRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setTimeout(timeout);
    ::zaber::motion::callGateway("interface/set_timeout", request);
}

/**
 * Returns checksum enabled.
 * @return Checksum enabled.
 */
bool BaseConnection::retrieveChecksumEnabled() const {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("interface/get_checksum_enabled", request, &response);

    return response.getValue();
}

/**
 * Sets checksum enabled.
 * @param isEnabled Checksum enabled.
 */
void BaseConnection::changeChecksumEnabled(bool isEnabled) const {

    ::zaber::motion::requests::SetInterfaceChecksumEnabledRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setIsEnabled(isEnabled);
    ::zaber::motion::callGateway("interface/set_checksum_enabled", request);
}

/**
 * The interface ID identifies this Connection instance with the underlying library.
 */
int BaseConnection::getInterfaceId() const {
    return this->_interfaceId;
}

/**
 * The default timeout, in milliseconds, for a device to respond to a request.
 * Setting the timeout to a too low value may cause request timeout exceptions.
 * The initial value is 1000 (one second).
 */
int BaseConnection::getDefaultRequestTimeout() const {
    return this->retrieveTimeout();
}
/**
 * The default timeout, in milliseconds, for a device to respond to a request.
 * Setting the timeout to a too low value may cause request timeout exceptions.
 * The initial value is 1000 (one second).
 */
void BaseConnection::setDefaultRequestTimeout(int value) {
    this->changeTimeout(value);
}

/**
 * Controls whether outgoing messages contain checksum.
 */
bool BaseConnection::getChecksumEnabled() const {
    return this->retrieveChecksumEnabled();
}
/**
 * Controls whether outgoing messages contain checksum.
 */
void BaseConnection::setChecksumEnabled(bool value) {
    this->changeChecksumEnabled(value);
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber

// NOLINTEND(bugprone-exception-escape)
