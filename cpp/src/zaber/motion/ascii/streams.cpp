﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/ascii/streams.h"


namespace zaber { namespace motion { namespace ascii {

Streams::Streams(Device device): _device(std::move(device)) {
}

/**
 * Gets a Stream class instance which allows you to control a particular stream on the device.
 * @param streamId The ID of the stream to control. Stream IDs start at one.
 * @return Stream instance.
 */
Stream Streams::getStream(int streamId) {
    if (streamId <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; streams are numbered from 1.");
    }

    return {this->_device, streamId};
}

/**
 * Gets a StreamBuffer class instance which is a handle for a stream buffer on the device.
 * @param streamBufferId The ID of the stream buffer to control. Stream buffer IDs start at one.
 * @return StreamBuffer instance.
 */
StreamBuffer Streams::getBuffer(int streamBufferId) {
    if (streamBufferId <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; stream buffers are numbered from 1.");
    }

    return {this->_device, streamBufferId};
}

/**
 * Get a list of buffer IDs that are currently in use.
 * @return List of buffer IDs.
 */
std::vector<int> Streams::listBufferIds() {

    ::zaber::motion::requests::StreamBufferList request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setPvt(false);
    ::zaber::motion::requests::IntArrayResponse response;
    ::zaber::motion::callGateway("device/stream_buffer_list", request, &response);

    return response.getValues();
}

/**
 * Device that these streams belong to.
 */
Device Streams::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
