﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/oscilloscope_data.h"
#include "zaber/motion/dto/ascii/oscilloscope_capture_properties.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

OscilloscopeData::OscilloscopeData(int dataId): _dataId(dataId) {
}

/**
 * Get the sample interval that this data was recorded with.
 * @param unit Unit of measure to represent the timebase in.
 * @return The timebase setting at the time the data was recorded.
 */
double OscilloscopeData::getTimebase(Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetRequest request;
    request.setDataId(this->getDataId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_timebase", request, &response);

    return response.getValue();
}

/**
 * Get the sampling frequency that this data was recorded with.
 * @param unit Unit of measure to represent the frequency in.
 * @return The frequency (inverse of the timebase setting) at the time the data was recorded.
 */
double OscilloscopeData::getFrequency(Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetRequest request;
    request.setDataId(this->getDataId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_frequency", request, &response);

    return response.getValue();
}

/**
 * Get the user-specified time period between receipt of the start command and the first data point.
 * Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.
 * @param unit Unit of measure to represent the delay in.
 * @return The delay setting at the time the data was recorded.
 */
double OscilloscopeData::getDelay(Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetRequest request;
    request.setDataId(this->getDataId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_delay", request, &response);

    return response.getValue();
}

/**
 * Calculate the time a sample was recorded, relative to when the recording was triggered.
 * @param index 0-based index of the sample to calculate the time of.
 * @param unit Unit of measure to represent the calculated time in.
 * @return The calculated time offset of the data sample at the given index.
 */
double OscilloscopeData::getSampleTime(int index, Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetSampleTimeRequest request;
    request.setDataId(this->getDataId());
    request.setIndex(index);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_sample_time", request, &response);

    return response.getValue();
}

/**
 * Calculate the time for all samples, relative to when the recording was triggered.
 * @param unit Unit of measure to represent the calculated time in.
 * @return The calculated time offsets of all data samples.
 */
std::vector<double> OscilloscopeData::getSampleTimes(Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetSampleTimeRequest request;
    request.setDataId(this->getDataId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleArrayResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_sample_times", request, &response);

    return response.getValues();
}

/**
 * Get the recorded data as an array of doubles, with optional unit conversion.
 * Note that not all quantities can be unit converted.
 * For example, digital I/O channels and pure numbers such as device mode settings have no units.
 * @param unit Unit of measure to convert the data to.
 * @return The recorded data for one oscilloscope channel, converted to the units specified.
 */
std::vector<double> OscilloscopeData::getData(Units unit) {

    ::zaber::motion::requests::OscilloscopeDataGetRequest request;
    request.setDataId(this->getDataId());
    request.setUnit(unit);
    ::zaber::motion::requests::OscilloscopeDataGetSamplesResponse response;
    ::zaber::motion::callGateway("oscilloscopedata/get_samples", request, &response);

    return response.getData();
}

/**
 * Releases native resources of an oscilloscope data buffer.
 * @param dataId The ID of the data buffer to delete.
 */
void OscilloscopeData::free(int dataId) {

    ::zaber::motion::requests::OscilloscopeDataIdentifier request;
    request.setDataId(dataId);
    ::zaber::motion::callGateway("oscilloscopedata/free", request);
}

/**
 * Returns recording properties.
 * @return Capture properties.
 */
OscilloscopeCaptureProperties OscilloscopeData::retrieveProperties() const {

    ::zaber::motion::requests::OscilloscopeDataIdentifier request;
    request.setDataId(this->getDataId());
    OscilloscopeCaptureProperties response;
    ::zaber::motion::callGateway("oscilloscopedata/get_properties", request, &response);

    return response;
}

/**
 * Unique ID for this block of recorded data.
 */
int OscilloscopeData::getDataId() const {
    return this->_dataId;
}

/**
 * Indicates whether the data came from a setting or an I/O pin.
 */
OscilloscopeDataSource OscilloscopeData::getDataSource() const {
    return this->retrieveProperties().getDataSource();
}

/**
 * The name of the recorded setting.
 */
std::string OscilloscopeData::getSetting() const {
    return this->retrieveProperties().getSetting();
}

/**
 * The number of the axis the data was recorded from, or 0 for the controller.
 */
int OscilloscopeData::getAxisNumber() const {
    return this->retrieveProperties().getAxisNumber();
}

/**
 * Which kind of I/O port data was recorded from.
 */
IoPortType OscilloscopeData::getIoType() const {
    return this->retrieveProperties().getIoType();
}

/**
 * Which I/O pin within the port was recorded.
 */
int OscilloscopeData::getIoChannel() const {
    return this->retrieveProperties().getIoChannel();
}


OscilloscopeData& OscilloscopeData::operator=(OscilloscopeData&& other) noexcept {
    if (this != &other) {
        if (this->_dataId >= 0) {
            OscilloscopeData::free(this->_dataId);
            this->_dataId = -1;
        }

        std::swap(_dataId, other._dataId);
    }
    return *this;
}

OscilloscopeData::OscilloscopeData(OscilloscopeData&& other) noexcept: _dataId(-1) {
    *this = std::move(other);
}

OscilloscopeData::~OscilloscopeData() {
    if (this->_dataId >= 0) {
        OscilloscopeData::free(this->_dataId);
    }
}
}  // namespace ascii
}  // namespace motion
}  // namespace zaber
