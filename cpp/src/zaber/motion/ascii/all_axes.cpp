﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/all_axes.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

namespace zaber { namespace motion { namespace ascii {

AllAxes::AllAxes(Device device): _device(std::move(device)) {
}

/**
 * Homes all axes. Axes return to their homing positions.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void AllAxes::home(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceHomeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/home", request);
}

/**
 * Homes all axes. Axes return to their homing positions.
 * @param options A struct of type HomeOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void AllAxes::home(const AllAxes::HomeOptions& options) {
    AllAxes::home(options.waitUntilIdle);
}

/**
 * Stops ongoing axes movement. Decelerates until zero speed.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void AllAxes::stop(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceStopRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/stop", request);
}

/**
 * Stops ongoing axes movement. Decelerates until zero speed.
 * @param options A struct of type StopOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void AllAxes::stop(const AllAxes::StopOptions& options) {
    AllAxes::stop(options.waitUntilIdle);
}

/**
 * Waits until all axes of device stop moving.
 * @param throwErrorOnFault Determines whether to throw error when fault is observed.
 */
void AllAxes::waitUntilIdle(bool throwErrorOnFault) {

    ::zaber::motion::requests::DeviceWaitUntilIdleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    request.setThrowErrorOnFault(throwErrorOnFault);
    ::zaber::motion::callGateway("device/wait_until_idle", request);
}

/**
 * Waits until all axes of device stop moving.
 * @param options A struct of type WaitUntilIdleOptions. It has the following members:
 * * `throwErrorOnFault`: Determines whether to throw error when fault is observed.
 */
void AllAxes::waitUntilIdle(const AllAxes::WaitUntilIdleOptions& options) {
    AllAxes::waitUntilIdle(options.throwErrorOnFault);
}

/**
 * Parks the device in anticipation of turning the power off.
 * It can later be powered on, unparked, and moved without first having to home it.
 */
void AllAxes::park() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::callGateway("device/park", request);
}

/**
 * Unparks the device. The device will now be able to move.
 */
void AllAxes::unpark() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::callGateway("device/unpark", request);
}

/**
 * Returns bool indicating whether any axis is executing a motion command.
 * @return True if any axis is currently executing a motion command.
 */
bool AllAxes::isBusy() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/is_busy", request, &response);

    return response.getValue();
}

/**
 * Returns bool indicating whether all axes have position reference and were homed.
 * @return True if all axes have position reference and were homed.
 */
bool AllAxes::isHomed() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/is_homed", request, &response);

    return response.getValue();
}

/**
 * Disables all axes drivers, which prevents current from being sent to the motor or load.
 */
void AllAxes::driverDisable() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::callGateway("device/driver_disable", request);
}

/**
 * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
 * If the driver is already enabled, the driver remains enabled.
 * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
 */
void AllAxes::driverEnable(double timeout) {

    ::zaber::motion::requests::DriverEnableRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    request.setTimeout(timeout);
    ::zaber::motion::callGateway("device/driver_enable", request);
}

/**
 * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
 * If the driver is already enabled, the driver remains enabled.
 * @param options A struct of type DriverEnableOptions. It has the following members:
 * * `timeout`: Timeout in seconds. Specify 0 to attempt to enable the driver once.
 */
void AllAxes::driverEnable(const AllAxes::DriverEnableOptions& options) {
    AllAxes::driverEnable(options.timeout);
}

/**
 * Returns a string that represents the axes.
 * @return A string that represents the axes.
 */
std::string AllAxes::toString() const {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(0);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/all_axes_to_string", request, &response);

    return response.getValue();
}

/**
 * Device that controls this axis.
 */
Device AllAxes::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
