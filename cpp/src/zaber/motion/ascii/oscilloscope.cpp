﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/oscilloscope.h"
#include "zaber/motion/utils/functional_utils.h"
#include "zaber/motion/ascii/oscilloscope_data.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Oscilloscope::Oscilloscope(Device device): _device(std::move(device)) {
}

/**
 * Select a setting to be recorded.
 * @param axis The 1-based index of the axis to record the value from.
 * @param setting The name of a setting to record.
 */
void Oscilloscope::addChannel(int axis, const std::string& setting) {

    ::zaber::motion::requests::OscilloscopeAddSettingChannelRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(axis);
    request.setSetting(setting);
    ::zaber::motion::callGateway("oscilloscope/add_setting_channel", request);
}

/**
 * Select an I/O pin to be recorded.
 * Requires at least Firmware 7.33.
 * @param ioType The I/O port type to read data from.
 * @param ioChannel The 1-based index of the I/O pin to read from.
 */
void Oscilloscope::addIoChannel(IoPortType ioType, int ioChannel) {

    ::zaber::motion::requests::OscilloscopeAddIoChannelRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setIoType(ioType);
    request.setIoChannel(ioChannel);
    ::zaber::motion::callGateway("oscilloscope/add_io_channel", request);
}

/**
 * Clear the list of channels to record.
 */
void Oscilloscope::clear() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::callGateway("oscilloscope/clear_channels", request);
}

/**
 * Get the current sampling interval.
 * @param unit Unit of measure to represent the timebase in.
 * @return The current sampling interval in the selected time units.
 */
double Oscilloscope::getTimebase(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.timebase");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Set the sampling interval.
 * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
 * @param unit Unit of measure the timebase is represented in.
 */
void Oscilloscope::setTimebase(double interval, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.timebase");
    request.setValue(interval);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Get the current sampling frequency.
 * The values is calculated as the inverse of the current sampling interval.
 * @param unit Unit of measure to represent the frequency in.
 * @return The inverse of current sampling interval in the selected units.
 */
double Oscilloscope::getFrequency(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.timebase");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("oscilloscope/get_frequency", request, &response);

    return response.getValue();
}

/**
 * Set the sampling frequency (inverse of the sampling interval).
 * The value is quantized to the next closest value supported by the firmware.
 * @param frequency Sample frequency for the next oscilloscope recording.
 * @param unit Unit of measure the frequency is represented in.
 */
void Oscilloscope::setFrequency(double frequency, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.timebase");
    request.setValue(frequency);
    request.setUnit(unit);
    ::zaber::motion::callGateway("oscilloscope/set_frequency", request);
}

/**
 * Get the delay before oscilloscope recording starts.
 * @param unit Unit of measure to represent the delay in.
 * @return The current start delay in the selected time units.
 */
double Oscilloscope::getDelay(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.delay");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Set the sampling start delay.
 * @param interval Delay time between triggering a recording and the first data point being recorded.
 * @param unit Unit of measure the delay is represented in.
 */
void Oscilloscope::setDelay(double interval, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.delay");
    request.setValue(interval);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Get the maximum number of channels that can be recorded.
 * @return The maximum number of channels that can be added to an Oscilloscope recording.
 */
int Oscilloscope::getMaxChannels() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.numchannels");
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("oscilloscope/get_setting", request, &response);

    return response.getValue();
}

/**
 * Get the maximum number of samples that can be recorded per Oscilloscope channel.
 * @return The maximum number of samples that can be recorded per Oscilloscope channel.
 */
int Oscilloscope::getMaxBufferSize() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.channel.size.max");
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("oscilloscope/get_setting", request, &response);

    return response.getValue();
}

/**
 * Get the number of samples that can be recorded per channel given the current number of channels added.
 * @return Number of samples that will be recorded per channel with the current channels. Zero if none have been added.
 */
int Oscilloscope::getBufferSize() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("scope.channel.size");
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("oscilloscope/get_setting", request, &response);

    return response.getValue();
}

/**
 * Trigger data recording.
 * @param captureLength Optional number of samples to record per channel.
 * If left empty, the device records samples until the buffer fills.
 * Requires at least Firmware 7.29.
 */
void Oscilloscope::start(int captureLength) {

    ::zaber::motion::requests::OscilloscopeStartRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setCaptureLength(captureLength);
    ::zaber::motion::callGateway("oscilloscope/start", request);
}

/**
 * End data recording if currently in progress.
 */
void Oscilloscope::stop() {

    ::zaber::motion::requests::OscilloscopeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::callGateway("oscilloscope/stop", request);
}

/**
 * Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
 * @return Array of recorded channel data arrays, in the order added.
 */
std::vector<OscilloscopeData> Oscilloscope::read() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::OscilloscopeReadResponse response;
    ::zaber::motion::callGateway("oscilloscope/read", request, &response);

    return zml_util::map_vec<int,OscilloscopeData>(response.getDataIds(), [](const int& id) { return OscilloscopeData(id); });
}

/**
 * Device that this Oscilloscope measures.
 */
Device Oscilloscope::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
