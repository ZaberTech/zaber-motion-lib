﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/pvt_buffer.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

PvtBuffer::PvtBuffer(Device device, int bufferId): _device(std::move(device)), _bufferId(bufferId) {
}

/**
 * Gets the buffer contents as an array of strings.
 * @return A string array containing all the PVT commands stored in the buffer.
 */
std::vector<std::string> PvtBuffer::getContent() {

    ::zaber::motion::requests::StreamBufferGetContentRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setBufferId(this->getBufferId());
    request.setPvt(true);
    ::zaber::motion::requests::StreamBufferGetContentResponse response;
    ::zaber::motion::callGateway("device/stream_buffer_get_content", request, &response);

    return response.getBufferLines();
}

/**
 * Erases the contents of the buffer.
 * This method fails if there is a PVT sequence writing to the buffer.
 */
void PvtBuffer::erase() {

    ::zaber::motion::requests::StreamBufferEraseRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setBufferId(this->getBufferId());
    request.setPvt(true);
    ::zaber::motion::callGateway("device/stream_buffer_erase", request);
}

/**
 * The Device this buffer exists on.
 */
Device PvtBuffer::getDevice() const {
    return this->_device;
}

/**
 * The number identifying the buffer on the device.
 */
int PvtBuffer::getBufferId() const {
    return this->_bufferId;
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber
