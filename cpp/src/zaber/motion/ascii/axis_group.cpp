﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/axis_group.h"
#include "zaber/motion/utils/functional_utils.h"
#include "zaber/motion/dto/ascii/conversion_factor.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"



namespace zaber { namespace motion { namespace ascii {

/**
 * Initializes the group with the axes to be controlled.
 */
AxisGroup::AxisGroup(std::vector<Axis> axes): _axes(std::move(axes)) {
}

AxisGroup::AxisGroup(): _axes() {
}

/**
 * Homes the axes.
 */
void AxisGroup::home() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::callGateway("axes/home", request);
}

/**
 * Stops the axes.
 */
void AxisGroup::stop() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::callGateway("axes/stop", request);
}

/**
 * Moves the axes to absolute position.
 * @param position Position.
 */
void AxisGroup::moveAbsolute(std::initializer_list<Measurement> position) {

    ::zaber::motion::requests::AxesMoveRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    request.setPosition(std::vector<Measurement>(position));
    ::zaber::motion::callGateway("axes/move_absolute", request);
}

/**
 * Move axes to position relative to the current position.
 * @param position Position.
 */
void AxisGroup::moveRelative(std::initializer_list<Measurement> position) {

    ::zaber::motion::requests::AxesMoveRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    request.setPosition(std::vector<Measurement>(position));
    ::zaber::motion::callGateway("axes/move_relative", request);
}

/**
 * Moves axes to the minimum position as specified by limit.min.
 */
void AxisGroup::moveMin() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::callGateway("axes/move_min", request);
}

/**
 * Moves axes to the maximum position as specified by limit.max.
 */
void AxisGroup::moveMax() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::callGateway("axes/move_max", request);
}

/**
 * Waits until all the axes stop moving.
 */
void AxisGroup::waitUntilIdle() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::callGateway("axes/wait_until_idle", request);
}

/**
 * Returns bool indicating whether any of the axes is executing a motion command.
 * @return True if any of the axes is currently executing a motion command. False otherwise.
 */
bool AxisGroup::isBusy() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("axes/is_busy", request, &response);

    return response.getValue();
}

/**
 * Returns bool indicating whether all the axes are homed.
 * @return True if all the axes are homed. False otherwise.
 */
bool AxisGroup::isHomed() {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("axes/is_homed", request, &response);

    return response.getValue();
}

/**
 * Returns current axes position.
 * The positions are requested sequentially.
 * The result position may not be accurate if the axes are moving.
 * @param unit Units of position. You can specify units once or for each axis separately.
 * @return Axes position.
 */
std::vector<double> AxisGroup::getPosition(std::initializer_list<Units> unit) {

    ::zaber::motion::requests::AxesGetSettingRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    request.setSetting("pos");
    request.setUnit(std::vector<Units>(unit));
    ::zaber::motion::requests::DoubleArrayResponse response;
    ::zaber::motion::callGateway("axes/get_setting", request, &response);

    return response.getValues();
}

/**
 * Returns a string that represents the axes.
 * @return A string that represents the axes.
 */
std::string AxisGroup::toString() const {

    ::zaber::motion::requests::AxesEmptyRequest request;
    request.setInterfaces(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); }));
    request.setDevices(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); }));
    request.setAxes(zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); }));
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("axes/to_string", request, &response);

    return response.getValue();
}

/**
 * Axes of the group.
 */
std::vector<Axis> AxisGroup::getAxes() const {
    return this->_axes;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
