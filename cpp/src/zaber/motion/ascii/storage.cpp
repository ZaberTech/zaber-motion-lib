﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/storage.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"



namespace zaber { namespace motion { namespace ascii {

AxisStorage::AxisStorage(Axis axis): _axis(std::move(axis)) {
}

/**
 * Sets the axis value stored at the provided key.
 * @param key Key to set the value at.
 * @param value Value to set.
 * @param encode Whether the stored value should be base64 encoded before being stored.
 * This makes the string unreadable to humans using the ASCII protocol,
 * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
 */
void AxisStorage::setString(const std::string& key, const std::string& value, bool encode) {

    ::zaber::motion::requests::DeviceSetStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    request.setValue(value);
    request.setEncode(encode);
    ::zaber::motion::callGateway("device/set_storage", request);
}

/**
 * Sets the axis value stored at the provided key.
 * @param key Key to set the value at.
 * @param value Value to set.
 * @param options A struct of type SetStringOptions. It has the following members:
 * * `encode`: Whether the stored value should be base64 encoded before being stored.
 *   This makes the string unreadable to humans using the ASCII protocol,
 *   however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
 */
void AxisStorage::setString(const std::string& key, const std::string& value, const AxisStorage::SetStringOptions& options) {
    AxisStorage::setString(key, value, options.encode);
}

/**
 * Gets the axis value stored with the provided key.
 * @param key Key to read the value of.
 * @param decode Whether the stored value should be decoded.
 * Only use this when reading values set by storage.set with "encode" true.
 * @return Stored value.
 */
std::string AxisStorage::getString(const std::string& key, bool decode) {

    ::zaber::motion::requests::DeviceGetStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    request.setDecode(decode);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_storage", request, &response);

    return response.getValue();
}

/**
 * Gets the axis value stored with the provided key.
 * @param key Key to read the value of.
 * @param options A struct of type GetStringOptions. It has the following members:
 * * `decode`: Whether the stored value should be decoded.
 *   Only use this when reading values set by storage.set with "encode" true.
 * @return Stored value.
 */
std::string AxisStorage::getString(const std::string& key, const AxisStorage::GetStringOptions& options) {
    return AxisStorage::getString(key, options.decode);
}

/**
 * Sets the value at the provided key to the provided number.
 * @param key Key to set the value at.
 * @param value Value to set.
 */
void AxisStorage::setNumber(const std::string& key, double value) {

    ::zaber::motion::requests::DeviceSetStorageNumberRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_storage_number", request);
}

/**
 * Gets the value at the provided key interpreted as a number.
 * @param key Key to get the value at.
 * @return Stored value.
 */
double AxisStorage::getNumber(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_storage_number", request, &response);

    return response.getValue();
}

/**
 * Sets the value at the provided key to the provided boolean.
 * @param key Key to set the value at.
 * @param value Value to set.
 */
void AxisStorage::setBool(const std::string& key, bool value) {

    ::zaber::motion::requests::DeviceSetStorageBoolRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_storage_bool", request);
}

/**
 * Gets the value at the provided key interpreted as a boolean.
 * @param key Key to get the value at.
 * @return Stored value.
 */
bool AxisStorage::getBool(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/get_storage_bool", request, &response);

    return response.getValue();
}

/**
 * Erases the axis value stored at the provided key.
 * @param key Key to erase.
 * @return A boolean indicating if the key existed.
 */
bool AxisStorage::eraseKey(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/erase_storage", request, &response);

    return response.getValue();
}

/**
 * Lists the axis storage keys matching a given prefix.
 * Omit the prefix to list all the keys.
 * @param prefix Optional key prefix.
 * @return Storage keys matching the given prefix.
 */
std::vector<std::string> AxisStorage::listKeys(const std::optional<std::string>& prefix) {

    ::zaber::motion::requests::DeviceStorageListKeysRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setPrefix(prefix);
    ::zaber::motion::requests::StringArrayResponse response;
    ::zaber::motion::callGateway("device/storage_list_keys", request, &response);

    return response.getValues();
}

/**
 * Lists the axis storage keys matching a given prefix.
 * Omit the prefix to list all the keys.
 * @param options A struct of type ListKeysOptions. It has the following members:
 * * `prefix`: Optional key prefix.
 * @return Storage keys matching the given prefix.
 */
std::vector<std::string> AxisStorage::listKeys(const AxisStorage::ListKeysOptions& options) {
    return AxisStorage::listKeys(options.prefix);
}

/**
 * Determines whether a given key exists in axis storage.
 * @param key Key which existence to determine.
 * @return True indicating that the key exists, false otherwise.
 */
bool AxisStorage::keyExists(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/storage_key_exists", request, &response);

    return response.getValue();
}

Axis AxisStorage::getAxis() const {
    return this->_axis;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber

namespace zaber { namespace motion { namespace ascii {
  
DeviceStorage::DeviceStorage(Device device): _device(std::move(device)) {
}

/**
 * Sets the device value stored at the provided key.
 * @param key Key to set the value at.
 * @param value Value to set.
 * @param encode Whether the stored value should be base64 encoded before being stored.
 * This makes the string unreadable to humans using the ASCII protocol,
 * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
 */
void DeviceStorage::setString(const std::string& key, const std::string& value, bool encode) {

    ::zaber::motion::requests::DeviceSetStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    request.setValue(value);
    request.setEncode(encode);
    ::zaber::motion::callGateway("device/set_storage", request);
}

/**
 * Sets the device value stored at the provided key.
 * @param key Key to set the value at.
 * @param value Value to set.
 * @param options A struct of type SetStringOptions. It has the following members:
 * * `encode`: Whether the stored value should be base64 encoded before being stored.
 *   This makes the string unreadable to humans using the ASCII protocol,
 *   however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
 */
void DeviceStorage::setString(const std::string& key, const std::string& value, const DeviceStorage::SetStringOptions& options) {
    DeviceStorage::setString(key, value, options.encode);
}

/**
 * Gets the device value stored with the provided key.
 * @param key Key to read the value of.
 * @param decode Whether the stored value should be decoded.
 * Only use this when reading values set by storage.set with "encode" true.
 * @return Stored value.
 */
std::string DeviceStorage::getString(const std::string& key, bool decode) {

    ::zaber::motion::requests::DeviceGetStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    request.setDecode(decode);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_storage", request, &response);

    return response.getValue();
}

/**
 * Gets the device value stored with the provided key.
 * @param key Key to read the value of.
 * @param options A struct of type GetStringOptions. It has the following members:
 * * `decode`: Whether the stored value should be decoded.
 *   Only use this when reading values set by storage.set with "encode" true.
 * @return Stored value.
 */
std::string DeviceStorage::getString(const std::string& key, const DeviceStorage::GetStringOptions& options) {
    return DeviceStorage::getString(key, options.decode);
}

/**
 * Sets the value at the provided key to the provided number.
 * @param key Key to set the value at.
 * @param value Value to set.
 */
void DeviceStorage::setNumber(const std::string& key, double value) {

    ::zaber::motion::requests::DeviceSetStorageNumberRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_storage_number", request);
}

/**
 * Gets the value at the provided key interpreted as a number.
 * @param key Key to get the value at.
 * @return Stored value.
 */
double DeviceStorage::getNumber(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_storage_number", request, &response);

    return response.getValue();
}

/**
 * Sets the value at the provided key to the provided boolean.
 * @param key Key to set the value at.
 * @param value Value to set.
 */
void DeviceStorage::setBool(const std::string& key, bool value) {

    ::zaber::motion::requests::DeviceSetStorageBoolRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_storage_bool", request);
}

/**
 * Gets the value at the provided key interpreted as a boolean.
 * @param key Key to get the value at.
 * @return Stored value.
 */
bool DeviceStorage::getBool(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/get_storage_bool", request, &response);

    return response.getValue();
}

/**
 * Erases the device value stored at the provided key.
 * @param key Key to erase.
 * @return A boolean indicating if the key existed.
 */
bool DeviceStorage::eraseKey(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/erase_storage", request, &response);

    return response.getValue();
}

/**
 * Lists the device storage keys matching a given prefix.
 * Omit the prefix to list all the keys.
 * @param prefix Optional key prefix.
 * @return Storage keys matching the given prefix.
 */
std::vector<std::string> DeviceStorage::listKeys(const std::optional<std::string>& prefix) {

    ::zaber::motion::requests::DeviceStorageListKeysRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setPrefix(prefix);
    ::zaber::motion::requests::StringArrayResponse response;
    ::zaber::motion::callGateway("device/storage_list_keys", request, &response);

    return response.getValues();
}

/**
 * Lists the device storage keys matching a given prefix.
 * Omit the prefix to list all the keys.
 * @param options A struct of type ListKeysOptions. It has the following members:
 * * `prefix`: Optional key prefix.
 * @return Storage keys matching the given prefix.
 */
std::vector<std::string> DeviceStorage::listKeys(const DeviceStorage::ListKeysOptions& options) {
    return DeviceStorage::listKeys(options.prefix);
}

/**
 * Determines whether a given key exists in device storage.
 * @param key Key which existence to determine.
 * @return True indicating that the key exists, false otherwise.
 */
bool DeviceStorage::keyExists(const std::string& key) {

    ::zaber::motion::requests::DeviceStorageRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setKey(key);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/storage_key_exists", request, &response);

    return response.getValue();
}

Device DeviceStorage::getDevice() const {
    return this->_device;
}

  }  // namespace ascii
  }  // namespace motion
  }  // namespace zaber
