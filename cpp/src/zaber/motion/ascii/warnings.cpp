﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/warnings.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"



namespace zaber { namespace motion { namespace ascii {

Warnings::Warnings(Device device, int axisNumber): _device(std::move(device)), _axisNumber(axisNumber) {
}

/**
 * Returns current warnings and faults on axis or device.
 * @return Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.
 */
std::unordered_set<std::string> Warnings::getFlags() {

    ::zaber::motion::requests::DeviceGetWarningsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setClear(false);
    ::zaber::motion::requests::DeviceGetWarningsResponse response;
    ::zaber::motion::callGateway("device/get_warnings", request, &response);

    return std::unordered_set<std::string>(response.flags.begin(), response.flags.end());
}

/**
 * Clears (acknowledges) current warnings and faults on axis or device and returns them.
 * @return Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.
 */
std::unordered_set<std::string> Warnings::clearFlags() {

    ::zaber::motion::requests::DeviceGetWarningsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setClear(true);
    ::zaber::motion::requests::DeviceGetWarningsResponse response;
    ::zaber::motion::callGateway("device/get_warnings", request, &response);

    return std::unordered_set<std::string>(response.flags.begin(), response.flags.end());
}

/**
 * Waits for the specified flags to clear.
 * Use for warnings flags that clear on their own.
 * Does not clear clearable warnings flags.
 * Throws TimeoutException if the flags don't clear in the specified time.
 * @param timeout For how long to wait in milliseconds for the flags to clear.
 * @param warningFlags The specific warning flags for which to wait to clear.
 */
void Warnings::waitToClear(double timeout, std::initializer_list<std::string> warningFlags) {

    ::zaber::motion::requests::WaitToClearWarningsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setTimeout(timeout);
    request.setWarningFlags(std::vector<std::string>(warningFlags));
    ::zaber::motion::callGateway("device/wait_to_clear_warnings", request);
}

Device Warnings::getDevice() const {
    return this->_device;
}

int Warnings::getAxisNumber() const {
    return this->_axisNumber;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
