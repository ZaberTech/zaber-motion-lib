﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/ascii/device_io.h"
#include "zaber/motion/dto/ascii/device_io_info.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <algorithm>


namespace zaber { namespace motion { namespace ascii {

DeviceIO::DeviceIO(Device device): _device(std::move(device)) {
}

/**
 * Returns the number of I/O channels the device has.
 * @return An object containing the number of I/O channels the device has.
 */
DeviceIOInfo DeviceIO::getChannelsInfo() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    DeviceIOInfo response;
    ::zaber::motion::callGateway("device/get_io_info", request, &response);

    return response;
}

/**
 * Sets the label of the specified channel.
 * @param portType The type of channel to set the label of.
 * @param channelNumber Channel number starting at 1.
 * @param label The label to set for the specified channel.
 * If no value or an empty string is provided, this label is deleted.
 */
void DeviceIO::setLabel(IoPortType portType, int channelNumber, const std::optional<std::string>& label) {

    ::zaber::motion::requests::SetIoPortLabelRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setPortType(portType);
    request.setChannelNumber(channelNumber);
    request.setLabel(label);
    ::zaber::motion::callGateway("device/set_io_label", request);
}

/**
 * Returns the label of the specified channel.
 * @param portType The type of channel to get the label of.
 * @param channelNumber Channel number starting at 1.
 * @return The label of the specified channel.
 */
std::string DeviceIO::getLabel(IoPortType portType, int channelNumber) {

    ::zaber::motion::requests::GetIoPortLabelRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setPortType(portType);
    request.setChannelNumber(channelNumber);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_io_label", request, &response);

    return response.getValue();
}

/**
 * Returns every label assigned to an IO port on this device.
 * @return The labels set for this device's IO.
 */
std::vector<IoPortLabel> DeviceIO::getAllLabels() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    ::zaber::motion::requests::GetAllIoPortLabelsResponse response;
    ::zaber::motion::callGateway("device/get_all_io_labels", request, &response);

    return response.getLabels();
}

/**
 * Returns the current value of the specified digital input channel.
 * @param channelNumber Channel number starting at 1.
 * @return True if voltage is present on the input channel and false otherwise.
 */
bool DeviceIO::getDigitalInput(int channelNumber) {

    ::zaber::motion::requests::DeviceGetDigitalIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("di");
    request.setChannelNumber(channelNumber);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/get_digital_io", request, &response);

    return response.getValue();
}

/**
 * Returns the current value of the specified digital output channel.
 * @param channelNumber Channel number starting at 1.
 * @return True if the output channel is conducting and false otherwise.
 */
bool DeviceIO::getDigitalOutput(int channelNumber) {

    ::zaber::motion::requests::DeviceGetDigitalIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("do");
    request.setChannelNumber(channelNumber);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/get_digital_io", request, &response);

    return response.getValue();
}

/**
 * Returns the current value of the specified analog input channel.
 * @param channelNumber Channel number starting at 1.
 * @return  A measurement of the voltage present on the input channel.
 */
double DeviceIO::getAnalogInput(int channelNumber) {

    ::zaber::motion::requests::DeviceGetAnalogIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("ai");
    request.setChannelNumber(channelNumber);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_analog_io", request, &response);

    return response.getValue();
}

/**
 * Returns the current values of the specified analog output channel.
 * @param channelNumber Channel number starting at 1.
 * @return A measurement of voltage that the output channel is conducting.
 */
double DeviceIO::getAnalogOutput(int channelNumber) {

    ::zaber::motion::requests::DeviceGetAnalogIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("ao");
    request.setChannelNumber(channelNumber);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_analog_io", request, &response);

    return response.getValue();
}

/**
 * Returns the current values of all digital input channels.
 * @return True if voltage is present on the input channel and false otherwise.
 */
std::vector<bool> DeviceIO::getAllDigitalInputs() {

    ::zaber::motion::requests::DeviceGetAllDigitalIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("di");
    ::zaber::motion::requests::DeviceGetAllDigitalIOResponse response;
    ::zaber::motion::callGateway("device/get_all_digital_io", request, &response);

    return response.getValues();
}

/**
 * Returns the current values of all digital output channels.
 * @return True if the output channel is conducting and false otherwise.
 */
std::vector<bool> DeviceIO::getAllDigitalOutputs() {

    ::zaber::motion::requests::DeviceGetAllDigitalIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("do");
    ::zaber::motion::requests::DeviceGetAllDigitalIOResponse response;
    ::zaber::motion::callGateway("device/get_all_digital_io", request, &response);

    return response.getValues();
}

/**
 * Returns the current values of all analog input channels.
 * @return Measurements of the voltages present on the input channels.
 */
std::vector<double> DeviceIO::getAllAnalogInputs() {

    ::zaber::motion::requests::DeviceGetAllAnalogIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("ai");
    ::zaber::motion::requests::DeviceGetAllAnalogIOResponse response;
    ::zaber::motion::callGateway("device/get_all_analog_io", request, &response);

    return response.getValues();
}

/**
 * Returns the current values of all analog output channels.
 * @return Measurements of voltage that the output channels are conducting.
 */
std::vector<double> DeviceIO::getAllAnalogOutputs() {

    ::zaber::motion::requests::DeviceGetAllAnalogIORequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelType("ao");
    ::zaber::motion::requests::DeviceGetAllAnalogIOResponse response;
    ::zaber::motion::callGateway("device/get_all_analog_io", request, &response);

    return response.getValues();
}

/**
 * Sets value for the specified digital output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform on the channel.
 */
void DeviceIO::setDigitalOutput(int channelNumber, DigitalOutputAction value) {

    ::zaber::motion::requests::DeviceSetDigitalOutputRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_digital_output", request);
}

/**
 * Sets values for all digital output channels.
 * @param values The type of action to perform on the channel.
 */
void DeviceIO::setAllDigitalOutputs(const std::vector<DigitalOutputAction>& values) {

    ::zaber::motion::requests::DeviceSetAllDigitalOutputsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setValues(std::vector<DigitalOutputAction>(values));
    ::zaber::motion::callGateway("device/set_all_digital_outputs", request);
}

/**
 * Sets current and future value for the specified digital output channel.
 * Requires at least Firmware 7.37.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform immediately on the channel.
 * @param futureValue The type of action to perform in the future on the channel.
 * @param delay Delay between setting current value and setting future value.
 * @param unit Units of time.
 */
void DeviceIO::setDigitalOutputSchedule(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::DeviceSetDigitalOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    request.setFutureValue(futureValue);
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_digital_output_schedule", request);
}

/**
 * Sets current and future values for all digital output channels.
 * Requires at least Firmware 7.37.
 * @param values The type of actions to perform immediately on output channels.
 * @param futureValues The type of actions to perform in the future on output channels.
 * @param delay Delay between setting current values and setting future values.
 * @param unit Units of time.
 */
void DeviceIO::setAllDigitalOutputsSchedule(const std::vector<DigitalOutputAction>& values, const std::vector<DigitalOutputAction>& futureValues, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::DeviceSetAllDigitalOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setValues(std::vector<DigitalOutputAction>(values));
    request.setFutureValues(std::vector<DigitalOutputAction>(futureValues));
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_all_digital_outputs_schedule", request);
}

/**
 * Sets value for the specified analog output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to.
 */
void DeviceIO::setAnalogOutput(int channelNumber, double value) {

    ::zaber::motion::requests::DeviceSetAnalogOutputRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_analog_output", request);
}

/**
 * Sets values for all analog output channels.
 * @param values Voltage values to set the output channels to.
 */
void DeviceIO::setAllAnalogOutputs(const std::vector<double>& values) {

    ::zaber::motion::requests::DeviceSetAllAnalogOutputsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setValues(std::vector<double>(values));
    ::zaber::motion::callGateway("device/set_all_analog_outputs", request);
}

/**
 * Sets current and future value for the specified analog output channel.
 * Requires at least Firmware 7.38.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to immediately.
 * @param futureValue Value to set the output channel voltage to in the future.
 * @param delay Delay between setting current value and setting future value.
 * @param unit Units of time.
 */
void DeviceIO::setAnalogOutputSchedule(int channelNumber, double value, double futureValue, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::DeviceSetAnalogOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    request.setFutureValue(futureValue);
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_analog_output_schedule", request);
}

/**
 * Sets current and future values for all analog output channels.
 * Requires at least Firmware 7.38.
 * @param values Voltage values to set the output channels to immediately.
 * @param futureValues Voltage values to set the output channels to in the future.
 * @param delay Delay between setting current values and setting future values.
 * @param unit Units of time.
 */
void DeviceIO::setAllAnalogOutputsSchedule(const std::vector<double>& values, const std::vector<double>& futureValues, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::DeviceSetAllAnalogOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setValues(std::vector<double>(values));
    request.setFutureValues(std::vector<double>(futureValues));
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_all_analog_outputs_schedule", request);
}

/**
 * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
 * Set the frequency to 0 to disable the filter.
 * @param channelNumber Channel number starting at 1.
 * @param cutoffFrequency Cutoff frequency of the low-pass filter.
 * @param unit Units of frequency.
 */
void DeviceIO::setAnalogInputLowpassFilter(int channelNumber, double cutoffFrequency, Units unit) {

    ::zaber::motion::requests::DeviceSetLowpassFilterRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setChannelNumber(channelNumber);
    request.setCutoffFrequency(cutoffFrequency);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_lowpass_filter", request);
}

/**
 * Cancels a scheduled digital output action.
 * Requires at least Firmware 7.37.
 * @param channelNumber Channel number starting at 1.
 */
void DeviceIO::cancelDigitalOutputSchedule(int channelNumber) {

    ::zaber::motion::requests::DeviceCancelOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(false);
    request.setChannelNumber(channelNumber);
    ::zaber::motion::callGateway("device/cancel_output_schedule", request);
}

/**
 * Cancel all scheduled digital output actions.
 * Requires at least Firmware 7.37.
 * @param channels Optionally specify which channels to cancel.
 * Array length must be empty or equal to the number of channels on device.
 * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
 */
void DeviceIO::cancelAllDigitalOutputsSchedule(const std::vector<bool>& channels) {

    ::zaber::motion::requests::DeviceCancelAllOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(false);
    request.setChannels(std::vector<bool>(channels));
    ::zaber::motion::callGateway("device/cancel_all_outputs_schedule", request);
}

/**
 * Cancels a scheduled analog output value.
 * Requires at least Firmware 7.38.
 * @param channelNumber Channel number starting at 1.
 */
void DeviceIO::cancelAnalogOutputSchedule(int channelNumber) {

    ::zaber::motion::requests::DeviceCancelOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(true);
    request.setChannelNumber(channelNumber);
    ::zaber::motion::callGateway("device/cancel_output_schedule", request);
}

/**
 * Cancel all scheduled analog output actions.
 * Requires at least Firmware 7.38.
 * @param channels Optionally specify which channels to cancel.
 * Array length must be empty or equal to the number of channels on device.
 * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
 */
void DeviceIO::cancelAllAnalogOutputsSchedule(const std::vector<bool>& channels) {

    ::zaber::motion::requests::DeviceCancelAllOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(true);
    request.setChannels(std::vector<bool>(channels));
    ::zaber::motion::callGateway("device/cancel_all_outputs_schedule", request);
}

Device DeviceIO::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
