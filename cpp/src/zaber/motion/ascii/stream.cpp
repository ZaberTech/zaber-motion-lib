﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/stream.h"
#include "zaber/motion/ascii/stream_buffer.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Stream::Stream(Device device, int streamId): _device(std::move(device)), _streamId(streamId) {
}

Stream::Stream(): _device(), _streamId(-1) {
}

/**
 * Setup the stream to control the specified axes and to queue actions on the device.
 * Allows use of lockstep axes in a stream.
 * @param axes Definition of the stream axes.
 */
void Stream::setupLiveComposite(std::initializer_list<StreamAxisDefinition> axes) {

    ::zaber::motion::requests::StreamSetupLiveCompositeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setAxes(std::vector<StreamAxisDefinition>(axes));
    ::zaber::motion::callGateway("device/stream_setup_live_composite", request);
}

/**
 * Setup the stream to control the specified axes and to queue actions on the device.
 * @param axes Numbers of physical axes to setup the stream on.
 */
void Stream::setupLive(std::initializer_list<int> axes) {

    ::zaber::motion::requests::StreamSetupLiveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setAxes(std::vector<int>(axes));
    ::zaber::motion::callGateway("device/stream_setup_live", request);
}

/**
 * Setup the stream to control the specified axes and queue actions into a stream buffer.
 * Allows use of lockstep axes in a stream.
 * @param streamBuffer The stream buffer to queue actions in.
 * @param axes Definition of the stream axes.
 */
void Stream::setupStoreComposite(const StreamBuffer& streamBuffer, std::initializer_list<StreamAxisDefinition> axes) {

    ::zaber::motion::requests::StreamSetupStoreCompositeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setStreamBuffer(streamBuffer.getBufferId());
    request.setAxes(std::vector<StreamAxisDefinition>(axes));
    ::zaber::motion::callGateway("device/stream_setup_store_composite", request);
}

/**
 * Setup the stream to control the specified axes and queue actions into a stream buffer.
 * @param streamBuffer The stream buffer to queue actions in.
 * @param axes Numbers of physical axes to setup the stream on.
 */
void Stream::setupStore(const StreamBuffer& streamBuffer, std::initializer_list<int> axes) {

    ::zaber::motion::requests::StreamSetupStoreRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setStreamBuffer(streamBuffer.getBufferId());
    request.setAxes(std::vector<int>(axes));
    ::zaber::motion::callGateway("device/stream_setup_store", request);
}

/**
 * Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
 * Afterwards, you may call the resulting stream buffer on arbitrary axes.
 * This mode does not allow for unit conversions.
 * @param streamBuffer The stream buffer to queue actions in.
 * @param axesCount The number of axes in the stream.
 */
void Stream::setupStoreArbitraryAxes(const StreamBuffer& streamBuffer, int axesCount) {

    ::zaber::motion::requests::StreamSetupStoreArbitraryAxesRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setStreamBuffer(streamBuffer.getBufferId());
    request.setAxesCount(axesCount);
    ::zaber::motion::callGateway("device/stream_setup_store_arbitrary_axes", request);
}

/**
 * Append the actions in a stream buffer to the queue.
 * @param streamBuffer The stream buffer to call.
 */
void Stream::call(const StreamBuffer& streamBuffer) {

    ::zaber::motion::requests::StreamCallRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setStreamBuffer(streamBuffer.getBufferId());
    ::zaber::motion::callGateway("device/stream_call", request);
}

/**
 * Queue an absolute line movement in the stream.
 * @param endpoint Positions for the axes to move to, relative to their home positions.
 */
void Stream::lineAbsolute(std::initializer_list<Measurement> endpoint) {

    ::zaber::motion::requests::StreamLineRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_line", request);
}

/**
 * Queue a relative line movement in the stream.
 * @param endpoint Positions for the axes to move to, relative to their positions before movement.
 */
void Stream::lineRelative(std::initializer_list<Measurement> endpoint) {

    ::zaber::motion::requests::StreamLineRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_line", request);
}

/**
 * Queue an absolute line movement in the stream, targeting a subset of the stream axes.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param endpoint Positions for the axes to move to, relative to their home positions.
 */
void Stream::lineAbsoluteOn(const std::vector<int>& targetAxesIndices, const std::vector<Measurement>& endpoint) {

    ::zaber::motion::requests::StreamLineRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_line", request);
}

/**
 * Queue a relative line movement in the stream, targeting a subset of the stream axes.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param endpoint Positions for the axes to move to, relative to their positions before movement.
 */
void Stream::lineRelativeOn(const std::vector<int>& targetAxesIndices, const std::vector<Measurement>& endpoint) {

    ::zaber::motion::requests::StreamLineRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_line", request);
}

/**
 * Queue an absolute arc movement on the first two axes of the stream.
 * Absolute meaning that the home positions of the axes is treated as the origin.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
 * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
 * @param endX The first dimension of the end position of the arc.
 * @param endY The second dimension of the end position of the arc.
 */
void Stream::arcAbsolute(RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    ::zaber::motion::callGateway("device/stream_arc", request);
}

/**
 * Queue a relative arc movement on the first two axes of the stream.
 * Relative meaning that the current position of the axes is treated as the origin.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
 * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
 * @param endX The first dimension of the end position of the arc.
 * @param endY The second dimension of the end position of the arc.
 */
void Stream::arcRelative(RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    ::zaber::motion::callGateway("device/stream_arc", request);
}

/**
 * Queue an absolute arc movement in the stream.
 * The movement will only target the specified subset of axes in the stream.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
 * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
 * @param endX The first dimension of the end position of the arc.
 * @param endY The second dimension of the end position of the arc.
 */
void Stream::arcAbsoluteOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    ::zaber::motion::callGateway("device/stream_arc", request);
}

/**
 * Queue a relative arc movement in the stream.
 * The movement will only target the specified subset of axes in the stream.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
 * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
 * @param endX The first dimension of the end position of the arc.
 * @param endY The second dimension of the end position of the arc.
 */
void Stream::arcRelativeOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    ::zaber::motion::callGateway("device/stream_arc", request);
}

/**
 * Queue an absolute helix movement in the stream.
 * Requires at least Firmware 7.28.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * The first two axes refer to the helix's arc component,
 * while the rest refers to the helix's line component.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
 * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
 * @param endX The first dimension of the end position of the helix's arc component.
 * @param endY The second dimension of the end position of the helix's arc component.
 * @param endpoint Positions for the helix's line component axes, relative to their home positions.
 */
void Stream::helixAbsoluteOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY, std::initializer_list<Measurement> endpoint) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_helix", request);
}

/**
 * Queue a relative helix movement in the stream.
 * Requires at least Firmware 7.28.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * The first two axes refer to the helix's arc component,
 * while the rest refers to the helix's line component.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
 * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
 * @param endX The first dimension of the end position of the helix's arc component.
 * @param endY The second dimension of the end position of the helix's arc component.
 * @param endpoint Positions for the helix's line component axes, relative to their positions before movement.
 */
void Stream::helixRelativeOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY, const Measurement& endX, const Measurement& endY, std::initializer_list<Measurement> endpoint) {

    ::zaber::motion::requests::StreamArcRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    request.setEndX(endX);
    request.setEndY(endY);
    request.setEndpoint(std::vector<Measurement>(endpoint));
    ::zaber::motion::callGateway("device/stream_helix", request);
}

/**
 * Queue an absolute circle movement on the first two axes of the stream.
 * Absolute meaning that the home positions of the axes are treated as the origin.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle.
 * @param centerY The second dimension of the position of the center of the circle.
 */
void Stream::circleAbsolute(RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY) {

    ::zaber::motion::requests::StreamCircleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    ::zaber::motion::callGateway("device/stream_circle", request);
}

/**
 * Queue a relative circle movement on the first two axes of the stream.
 * Relative meaning that the current position of the axes is treated as the origin.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle.
 * @param centerY The second dimension of the position of the center of the circle.
 */
void Stream::circleRelative(RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY) {

    ::zaber::motion::requests::StreamCircleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    ::zaber::motion::callGateway("device/stream_circle", request);
}

/**
 * Queue an absolute circle movement in the stream.
 * The movement will only target the specified subset of axes in the stream.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle.
 * @param centerY The second dimension of the position of the center of the circle.
 */
void Stream::circleAbsoluteOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY) {

    ::zaber::motion::requests::StreamCircleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::ABS);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    ::zaber::motion::callGateway("device/stream_circle", request);
}

/**
 * Queue a relative circle movement in the stream.
 * The movement will only target the specified subset of axes in the stream.
 * Requires at least Firmware 7.11.
 * @param targetAxesIndices Indices of the axes in the stream the movement targets.
 * Refers to the axes provided during the stream setup or further execution.
 * Indices are zero-based.
 * @param rotationDirection The direction of the rotation.
 * @param centerX The first dimension of the position of the center of the circle.
 * @param centerY The second dimension of the position of the center of the circle.
 */
void Stream::circleRelativeOn(const std::vector<int>& targetAxesIndices, RotationDirection rotationDirection, const Measurement& centerX, const Measurement& centerY) {

    ::zaber::motion::requests::StreamCircleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setType(::zaber::motion::requests::StreamSegmentType::REL);
    request.setTargetAxesIndices(std::vector<int>(targetAxesIndices));
    request.setRotationDirection(rotationDirection);
    request.setCenterX(centerX);
    request.setCenterY(centerY);
    ::zaber::motion::callGateway("device/stream_circle", request);
}

/**
 * Wait a specified time.
 * @param time Amount of time to wait.
 * @param unit Units of time.
 */
void Stream::wait(double time, Units unit) {

    ::zaber::motion::requests::StreamWaitRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setTime(time);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_wait", request);
}

/**
 * Waits until the live stream executes all queued actions.
 * @param throwErrorOnFault Determines whether to throw error when fault is observed.
 */
void Stream::waitUntilIdle(bool throwErrorOnFault) {

    ::zaber::motion::requests::StreamWaitUntilIdleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setThrowErrorOnFault(throwErrorOnFault);
    ::zaber::motion::callGateway("device/stream_wait_until_idle", request);
}

/**
 * Waits until the live stream executes all queued actions.
 * @param options A struct of type WaitUntilIdleOptions. It has the following members:
 * * `throwErrorOnFault`: Determines whether to throw error when fault is observed.
 */
void Stream::waitUntilIdle(const Stream::WaitUntilIdleOptions& options) {
    Stream::waitUntilIdle(options.throwErrorOnFault);
}

/**
 * Cork the front of the stream's action queue, blocking execution.
 * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
 * Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
 * You can only cork an idle live stream.
 */
void Stream::cork() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::callGateway("device/stream_cork", request);
}

/**
 * Uncork the front of the queue, unblocking command execution.
 * You can only uncork an idle live stream that is corked.
 */
void Stream::uncork() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::callGateway("device/stream_uncork", request);
}

/**
 * Pauses or resumes execution of the stream in live mode.
 * The hold only takes effect during execution of motion segments.
 * @param hold True to pause execution, false to resume.
 */
void Stream::setHold(bool hold) {

    ::zaber::motion::requests::StreamSetHoldRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setHold(hold);
    ::zaber::motion::callGateway("device/stream_set_hold", request);
}

/**
 * Returns a boolean value indicating whether the live stream is executing a queued action.
 * @return True if the stream is executing a queued action.
 */
bool Stream::isBusy() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/stream_is_busy", request, &response);

    return response.getValue();
}

/**
 * Gets the maximum speed of the live stream.
 * Converts the units using the first axis of the stream.
 * @param unit Units of velocity.
 * @return The maximum speed of the stream.
 */
double Stream::getMaxSpeed(Units unit) {

    ::zaber::motion::requests::StreamGetMaxSpeedRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/stream_get_max_speed", request, &response);

    return response.getValue();
}

/**
 * Sets the maximum speed of the live stream.
 * Converts the units using the first axis of the stream.
 * @param maxSpeed Maximum speed at which any stream action is executed.
 * @param unit Units of velocity.
 */
void Stream::setMaxSpeed(double maxSpeed, Units unit) {

    ::zaber::motion::requests::StreamSetMaxSpeedRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setMaxSpeed(maxSpeed);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_max_speed", request);
}

/**
 * Gets the maximum tangential acceleration of the live stream.
 * Converts the units using the first axis of the stream.
 * @param unit Units of acceleration.
 * @return The maximum tangential acceleration of the live stream.
 */
double Stream::getMaxTangentialAcceleration(Units unit) {

    ::zaber::motion::requests::StreamGetMaxTangentialAccelerationRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/stream_get_max_tangential_acceleration", request, &response);

    return response.getValue();
}

/**
 * Sets the maximum tangential acceleration of the live stream.
 * Converts the units using the first axis of the stream.
 * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
 * @param unit Units of acceleration.
 */
void Stream::setMaxTangentialAcceleration(double maxTangentialAcceleration, Units unit) {

    ::zaber::motion::requests::StreamSetMaxTangentialAccelerationRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setMaxTangentialAcceleration(maxTangentialAcceleration);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_max_tangential_acceleration", request);
}

/**
 * Gets the maximum centripetal acceleration of the live stream.
 * Converts the units using the first axis of the stream.
 * @param unit Units of acceleration.
 * @return The maximum centripetal acceleration of the live stream.
 */
double Stream::getMaxCentripetalAcceleration(Units unit) {

    ::zaber::motion::requests::StreamGetMaxCentripetalAccelerationRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/stream_get_max_centripetal_acceleration", request, &response);

    return response.getValue();
}

/**
 * Sets the maximum centripetal acceleration of the live stream.
 * Converts the units using the first axis of the stream.
 * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
 * @param unit Units of acceleration.
 */
void Stream::setMaxCentripetalAcceleration(double maxCentripetalAcceleration, Units unit) {

    ::zaber::motion::requests::StreamSetMaxCentripetalAccelerationRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setMaxCentripetalAcceleration(maxCentripetalAcceleration);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_max_centripetal_acceleration", request);
}

/**
 * Returns a string which represents the stream.
 * @return String which represents the stream.
 */
std::string Stream::toString() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/stream_to_string", request, &response);

    return response.getValue();
}

/**
 * Disables the stream.
 * If the stream is not setup, this command does nothing.
 * Once disabled, the stream will no longer accept stream commands.
 * The stream will process the rest of the commands in the queue until it is empty.
 */
void Stream::disable() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::callGateway("device/stream_disable", request);
}

/**
 * Sends a generic ASCII command to the stream.
 * Keeps resending the command while the device rejects with AGAIN reason.
 * @param command Command and its parameters.
 */
void Stream::genericCommand(const std::string& command) {

    ::zaber::motion::requests::StreamGenericCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setCommand(command);
    ::zaber::motion::callGateway("device/stream_generic_command", request);
}

/**
 * Sends a batch of generic ASCII commands to the stream.
 * Keeps resending command while the device rejects with AGAIN reason.
 * The batch is atomic in terms of thread safety.
 * @param batch Array of commands.
 */
void Stream::genericCommandBatch(const std::vector<std::string>& batch) {

    ::zaber::motion::requests::StreamGenericCommandBatchRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setBatch(std::vector<std::string>(batch));
    ::zaber::motion::callGateway("device/stream_generic_command_batch", request);
}

/**
 * Queries the stream status from the device
 * and returns boolean indicating whether the stream is disabled.
 * Useful to determine if streaming was interrupted by other movements.
 * @return True if the stream is disabled.
 */
bool Stream::checkDisabled() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/stream_check_disabled", request, &response);

    return response.getValue();
}

/**
 * Makes the stream throw StreamDiscontinuityException when it encounters discontinuities (ND warning flag).
 */
void Stream::treatDiscontinuitiesAsError() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::callGateway("device/stream_treat_discontinuities", request);
}

/**
 * Prevents StreamDiscontinuityException as a result of expected discontinuity when resuming streaming.
 */
void Stream::ignoreCurrentDiscontinuity() {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::callGateway("device/stream_ignore_discontinuity", request);
}

/**
 * Gets the axes of the stream.
 * @return An array of axis numbers of the axes the stream is set up to control.
 */
std::vector<StreamAxisDefinition> Stream::retrieveAxes() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::requests::StreamGetAxesResponse response;
    ::zaber::motion::callGateway("device/stream_get_axes", request, &response);

    return response.getAxes();
}

/**
 * Get the mode of the stream.
 * @return Mode of the stream.
 */
StreamMode Stream::retrieveMode() const {

    ::zaber::motion::requests::StreamEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    ::zaber::motion::requests::StreamModeResponse response;
    ::zaber::motion::callGateway("device/stream_get_mode", request, &response);

    return response.getStreamMode();
}

/**
 * Deprecated: Use Stream.Io.WaitDigitalInput instead.
 *
 * Wait for a digital input channel to reach a given value.
 * @param channelNumber The number of the digital input channel.
 * Channel numbers are numbered from one.
 * @param value The value that the stream should wait for.
 */
void Stream::waitDigitalInput(int channelNumber, bool value) {

    ::zaber::motion::requests::StreamWaitDigitalInputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_wait_digital_input", request);
}

/**
 * Deprecated: Use Stream.Io.WaitAnalogInput instead.
 *
 * Wait for the value of a analog input channel to reach a condition concerning a given value.
 * @param channelNumber The number of the analog input channel.
 * Channel numbers are numbered from one.
 * @param condition A condition (e.g. <, <=, ==, !=).
 * @param value The value that the condition concerns, in Volts.
 */
void Stream::waitAnalogInput(int channelNumber, const std::string& condition, double value) {

    ::zaber::motion::requests::StreamWaitAnalogInputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setChannelNumber(channelNumber);
    request.setCondition(condition);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_wait_analog_input", request);
}

/**
 * Deprecated: Use Stream.Io.SetDigitalOutput instead.
 *
 * Sets value for the specified digital output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform on the channel.
 */
void Stream::setDigitalOutput(int channelNumber, DigitalOutputAction value) {

    ::zaber::motion::requests::StreamSetDigitalOutputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_digital_output", request);
}

/**
 * Deprecated: Use Stream.Io.SetAllDigitalOutputs instead.
 *
 * Sets values for all digital output channels.
 * @param values The type of action to perform on the channel.
 */
void Stream::setAllDigitalOutputs(const std::vector<DigitalOutputAction>& values) {

    ::zaber::motion::requests::StreamSetAllDigitalOutputsRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setValues(std::vector<DigitalOutputAction>(values));
    ::zaber::motion::callGateway("device/stream_set_all_digital_outputs", request);
}

/**
 * Deprecated: Use Stream.Io.setAnalogOutput instead.
 *
 * Sets value for the specified analog output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to.
 */
void Stream::setAnalogOutput(int channelNumber, double value) {

    ::zaber::motion::requests::StreamSetAnalogOutputRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_analog_output", request);
}

/**
 * Deprecated: Use Stream.Io.setAllAnalogOutputs instead.
 *
 * Sets values for all analog output channels.
 * @param values Voltage values to set the output channels to.
 */
void Stream::setAllAnalogOutputs(const std::vector<double>& values) {

    ::zaber::motion::requests::StreamSetAllAnalogOutputsRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setValues(std::vector<double>(values));
    ::zaber::motion::callGateway("device/stream_set_all_analog_outputs", request);
}

/**
 * Device that controls this stream.
 */
Device Stream::getDevice() const {
    return this->_device;
}

/**
 * The number that identifies the stream on the device.
 */
int Stream::getStreamId() const {
    return this->_streamId;
}

/**
 * Current mode of the stream.
 */
StreamMode Stream::getMode() const {
    return this->retrieveMode();
}

/**
 * An array of axes definitions the stream is set up to control.
 */
std::vector<StreamAxisDefinition> Stream::getAxes() const {
    return this->retrieveAxes();
}

/**
 * Gets an object that provides access to I/O for this stream.
 */
StreamIo Stream::getIo() const {
    return {this->_device, this->_streamId};
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber
