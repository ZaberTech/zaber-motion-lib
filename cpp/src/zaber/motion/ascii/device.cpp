﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/device.h"
#include "zaber/motion/ascii/all_axes.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/dto/ascii/device_identity.h"
#include "zaber/motion/ascii/device_io.h"
#include "zaber/motion/ascii/device_settings.h"
#include "zaber/motion/ascii/lockstep.h"
#include "zaber/motion/ascii/oscilloscope.h"
#include "zaber/motion/ascii/pvt.h"
#include "zaber/motion/dto/ascii/response.h"
#include "zaber/motion/ascii/storage.h"
#include "zaber/motion/ascii/warnings.h"
#include "zaber/motion/ascii/triggers.h"
#include "zaber/motion/ascii/streams.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/dto/firmware_version.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Device::Device(BaseConnection connection, int deviceAddress): _connection(std::move(connection)), _deviceAddress(deviceAddress) {
}

Device::Device(): _connection(), _deviceAddress(-1) {
}

/**
 * Queries the device and the database, gathering information about the product.
 * Without this information features such as unit conversions will not work.
 * Usually, called automatically by detect devices method.
 * @param assumeVersion The identification assumes the specified firmware version
 * instead of the version queried from the device.
 * Providing this argument can lead to unexpected compatibility issues.
 * @return Device identification data.
 */
DeviceIdentity Device::identify(const std::optional<FirmwareVersion>& assumeVersion) {

    ::zaber::motion::requests::DeviceIdentifyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setAssumeVersion(assumeVersion);
    DeviceIdentity response;
    ::zaber::motion::callGateway("device/identify", request, &response);

    return response;
}

/**
 * Queries the device and the database, gathering information about the product.
 * Without this information features such as unit conversions will not work.
 * Usually, called automatically by detect devices method.
 * @param options A struct of type IdentifyOptions. It has the following members:
 * * `assumeVersion`: The identification assumes the specified firmware version
 *   instead of the version queried from the device.
 *   Providing this argument can lead to unexpected compatibility issues.
 * @return Device identification data.
 */
DeviceIdentity Device::identify(const Device::IdentifyOptions& options) {
    return Device::identify(options.assumeVersion);
}

/**
 * Sends a generic ASCII command to this device.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param axis Optional axis number to send the command to.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Device::genericCommand(const std::string& command, int axis, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setAxis(axis);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    Response response;
    ::zaber::motion::callGateway("interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic ASCII command to this device.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `axis`: Optional axis number to send the command to.
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Device::genericCommand(const std::string& command, const Device::GenericCommandOptions& options) {
    return Device::genericCommand(command, options.axis, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this device and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param axis Optional axis number to send the command to.
 * @param checkErrors Controls whether to throw an exception when a device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Device::genericCommandMultiResponse(const std::string& command, int axis, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setAxis(axis);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    ::zaber::motion::requests::GenericCommandResponseCollection response;
    ::zaber::motion::callGateway("interface/generic_command_multi_response", request, &response);

    return response.getResponses();
}

/**
 * Sends a generic ASCII command to this device and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `axis`: Optional axis number to send the command to.
 * * `checkErrors`: Controls whether to throw an exception when a device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Device::genericCommandMultiResponse(const std::string& command, const Device::GenericCommandMultiResponseOptions& options) {
    return Device::genericCommandMultiResponse(command, options.axis, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param axis Optional axis number to send the command to.
 * Specifying -1 omits the number completely.
 */
void Device::genericCommandNoResponse(const std::string& command, int axis) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setAxis(axis);
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandNoResponseOptions. It has the following members:
 * * `axis`: Optional axis number to send the command to.
 *   Specifying -1 omits the number completely.
 */
void Device::genericCommandNoResponse(const std::string& command, const Device::GenericCommandNoResponseOptions& options) {
    Device::genericCommandNoResponse(command, options.axis);
}

/**
 * Gets an Axis class instance which allows you to control a particular axis on this device.
 * Axes are numbered from 1.
 * @param axisNumber Number of axis intended to control.
 * @return Axis instance.
 */
Axis Device::getAxis(int axisNumber) {
    if (axisNumber <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; physical axes are numbered from 1.");
    }

    return {*this, axisNumber};
}

/**
 * Gets a Lockstep class instance which allows you to control a particular lockstep group on the device.
 * Requires at least Firmware 6.15 or 7.11.
 * @param lockstepGroupId The ID of the lockstep group to control. Lockstep group IDs start at one.
 * @return Lockstep instance.
 */
Lockstep Device::getLockstep(int lockstepGroupId) {
    if (lockstepGroupId <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; lockstep groups are numbered from 1.");
    }

    return {*this, lockstepGroupId};
}

/**
 * Formats parameters into a command and performs unit conversions.
 * Parameters in the command template are denoted by a question mark.
 * Command returned is only valid for this device.
 * Unit conversion is not supported for commands where axes can be remapped, such as stream and PVT commands.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
 * @param parameters Variable number of command parameters.
 * @return Command with converted parameters.
 */
std::string Device::prepareCommand(const std::string& commandTemplate, std::initializer_list<Measurement> parameters) {

    ::zaber::motion::requests::PrepareCommandRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommandTemplate(commandTemplate);
    request.setParameters(std::vector<Measurement>(parameters));
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/prepare_command", request, &response);

    return response.getValue();
}

/**
 * Sets the user-assigned device label.
 * The label is stored on the controller and recognized by other software.
 * @param label Label to set.
 */
void Device::setLabel(const std::string& label) {

    ::zaber::motion::requests::DeviceSetStorageRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setValue(label);
    ::zaber::motion::callGateway("device/set_label", request);
}

/**
 * Gets the device name.
 * @return The label.
 */
std::string Device::retrieveLabel() const {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_label", request, &response);

    return response.getValue();
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string Device::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * Returns a serialization of the current device state that can be saved and reapplied.
 * @return A serialization of the current state of the device.
 */
std::string Device::getState() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_state", request, &response);

    return response.getValue();
}

/**
 * Applies a saved state to this device.
 * @param state The state object to apply to this device.
 * @param deviceOnly If true, only device scope settings and features will be set.
 * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
 */
SetStateDeviceResponse Device::setState(const std::string& state, bool deviceOnly) {

    ::zaber::motion::requests::SetStateRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setState(state);
    request.setDeviceOnly(deviceOnly);
    SetStateDeviceResponse response;
    ::zaber::motion::callGateway("device/set_device_state", request, &response);

    return response;
}

/**
 * Applies a saved state to this device.
 * @param state The state object to apply to this device.
 * @param options A struct of type SetStateOptions. It has the following members:
 * * `deviceOnly`: If true, only device scope settings and features will be set.
 * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
 */
SetStateDeviceResponse Device::setState(const std::string& state, const Device::SetStateOptions& options) {
    return Device::setState(state, options.deviceOnly);
}

/**
 * Checks if a state can be applied to this device.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param firmwareVersion The firmware version of the device to apply the state to.
 * Use this to ensure the state will still be compatible after an update.
 * @return An object listing errors that come up when trying to set the state.
 */
CanSetStateDeviceResponse Device::canSetState(const std::string& state, const std::optional<FirmwareVersion>& firmwareVersion) {

    ::zaber::motion::requests::CanSetStateRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setState(state);
    request.setFirmwareVersion(firmwareVersion);
    CanSetStateDeviceResponse response;
    ::zaber::motion::callGateway("device/can_set_state", request, &response);

    return response;
}

/**
 * Checks if a state can be applied to this device.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param options A struct of type CanSetStateOptions. It has the following members:
 * * `firmwareVersion`: The firmware version of the device to apply the state to.
 *   Use this to ensure the state will still be compatible after an update.
 * @return An object listing errors that come up when trying to set the state.
 */
CanSetStateDeviceResponse Device::canSetState(const std::string& state, const Device::CanSetStateOptions& options) {
    return Device::canSetState(state, options.firmwareVersion);
}

/**
 * Waits for the device to start responding to messages.
 * Useful to call after resetting the device.
 * Throws RequestTimeoutException upon timeout.
 * @param timeout For how long to wait in milliseconds for the device to start responding.
 */
void Device::waitToRespond(double timeout) {

    ::zaber::motion::requests::WaitToRespondRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setTimeout(timeout);
    ::zaber::motion::callGateway("device/wait_to_respond", request);
}

/**
 * Changes the address of this device.
 * After the address is successfully changed, the existing device class instance no longer represents the device.
 * Instead, use the new device instance returned by this method.
 * @param address The new address to assign to the device.
 * @return New device instance with the new address.
 */
Device Device::renumber(int address) {
    if (address < 1 || address > 99) {
        throw exceptions::InvalidArgumentException("Invalid value; device addresses are numbered from 1 to 99.");
    }

    ::zaber::motion::requests::RenumberRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setAddress(address);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/renumber", request, &response);

    return {this->_connection, response.getValue()};
}

/**
 * Restores most of the settings to their default values.
 * Deletes all triggers, stream and PVT buffers, servo tunings.
 * Deletes all zaber storage keys.
 * Disables locksteps, unparks axes.
 * Preserves storage, communication settings, peripherals (unless hard is specified).
 * The device needs to be identified again after the restore.
 * @param hard If true, completely erases device's memory. The device also resets.
 */
void Device::restore(bool hard) {

    ::zaber::motion::requests::DeviceRestoreRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setHard(hard);
    ::zaber::motion::callGateway("device/restore", request);
}

/**
 * Returns identity.
 * @return Device identity.
 */
DeviceIdentity Device::retrieveIdentity() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    DeviceIdentity response;
    ::zaber::motion::callGateway("device/get_identity", request, &response);

    return response;
}

/**
 * Returns whether or not the device have been identified.
 * @return True if the device has already been identified. False otherwise.
 */
bool Device::retrieveIsIdentified() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/get_is_identified", request, &response);

    return response.getValue();
}

/**
 * Connection of this device.
 */
BaseConnection Device::getConnection() const {
    return this->_connection;
}

/**
 * The device address uniquely identifies the device on the connection.
 * It can be configured or automatically assigned by the renumber command.
 */
int Device::getDeviceAddress() const {
    return this->_deviceAddress;
}

/**
 * Settings and properties of this device.
 */
DeviceSettings Device::getSettings() const {
    return {*this};
}

/**
 * Key-value storage of this device.
 */
DeviceStorage Device::getStorage() const {
    return {*this};
}

/**
 * I/O channels of this device.
 */
DeviceIO Device::getIO() const {
    return {*this};
}

/**
 * Virtual axis which allows you to target all axes of this device.
 */
AllAxes Device::getAllAxes() const {
    return {*this};
}

/**
 * Warnings and faults of this device and all its axes.
 */
Warnings Device::getWarnings() const {
    return {*this, 0};
}

/**
 * Identity of the device.
 */
DeviceIdentity Device::getIdentity() const {
    return this->retrieveIdentity();
}

/**
 * Indicates whether or not the device has been identified.
 */
bool Device::getIsIdentified() const {
    return this->retrieveIsIdentified();
}

/**
 * Oscilloscope recording helper for this device.
 * Requires at least Firmware 7.00.
 */
Oscilloscope Device::getOscilloscope() const {
    return {*this};
}

/**
 * Unique ID of the device hardware.
 */
int Device::getDeviceId() const {
    return this->getIdentity().getDeviceId();
}

/**
 * Serial number of the device.
 */
unsigned int Device::getSerialNumber() const {
    return this->getIdentity().getSerialNumber();
}

/**
 * Name of the product.
 */
std::string Device::getName() const {
    return this->getIdentity().getName();
}

/**
 * Number of axes this device has.
 */
int Device::getAxisCount() const {
    return this->getIdentity().getAxisCount();
}

/**
 * Version of the firmware.
 */
FirmwareVersion Device::getFirmwareVersion() const {
    return this->getIdentity().getFirmwareVersion();
}

/**
 * The device is an integrated product.
 */
bool Device::getIsIntegrated() const {
    return this->getIdentity().getIsIntegrated();
}

/**
 * User-assigned label of the device.
 */
std::string Device::getLabel() const {
    return this->retrieveLabel();
}

/**
 * Triggers for this device.
 * Requires at least Firmware 7.06.
 */
Triggers Device::getTriggers() const {
    return {*this};
}

/**
 * Gets an object that provides access to Streams on this device.
 * Requires at least Firmware 7.05.
 */
Streams Device::getStreams() const {
    return {*this};
}

/**
 * Gets an object that provides access to PVT functions of this device.
 * Note that as of ZML v5.0.0, this returns a Pvt object and NOT a PvtSequence object.
 * The PvtSequence can now be obtained from the Pvt object.
 * Requires at least Firmware 7.33.
 */
Pvt Device::getPvt() const {
    return {*this};
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
