﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/stream_buffer.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

StreamBuffer::StreamBuffer(Device device, int bufferId): _device(std::move(device)), _bufferId(bufferId) {
}

/**
 * Get the buffer contents as an array of strings.
 * @return A string array containing all the stream commands stored in the buffer.
 */
std::vector<std::string> StreamBuffer::getContent() {

    ::zaber::motion::requests::StreamBufferGetContentRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setBufferId(this->getBufferId());
    ::zaber::motion::requests::StreamBufferGetContentResponse response;
    ::zaber::motion::callGateway("device/stream_buffer_get_content", request, &response);

    return response.getBufferLines();
}

/**
 * Erase the contents of the buffer.
 * This method fails if there is a stream writing to the buffer.
 */
void StreamBuffer::erase() {

    ::zaber::motion::requests::StreamBufferEraseRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setBufferId(this->getBufferId());
    ::zaber::motion::callGateway("device/stream_buffer_erase", request);
}

/**
 * The Device this buffer exists on.
 */
Device StreamBuffer::getDevice() const {
    return this->_device;
}

/**
 * The number identifying the buffer on the device.
 */
int StreamBuffer::getBufferId() const {
    return this->_bufferId;
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber
