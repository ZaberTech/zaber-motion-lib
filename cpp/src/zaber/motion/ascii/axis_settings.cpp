﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/axis_settings.h"
#include "zaber/motion/dto/ascii/conversion_factor.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

AxisSettings::AxisSettings(Axis axis): _axis(std::move(axis)) {
}

/**
 * Returns any axis setting or property.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param unit Units of setting.
 * @return Setting value.
 */
double AxisSettings::get(const std::string& setting, Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Sets any axis setting.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param value Value of the setting.
 * @param unit Units of setting.
 */
void AxisSettings::set(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Returns any axis setting or property as a string.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @return Setting value.
 */
std::string AxisSettings::getString(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_setting_str", request, &response);

    return response.getValue();
}

/**
 * Sets any axis setting as a string.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param value Value of the setting.
 */
void AxisSettings::setString(const std::string& setting, const std::string& value) {

    ::zaber::motion::requests::DeviceSetSettingStrRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_setting_str", request);
}

/**
 * Convert arbitrary setting value to Zaber native units.
 * @param setting Name of the setting.
 * @param value Value of the setting in units specified by following argument.
 * @param unit Units of the value.
 * @return Setting value.
 */
double AxisSettings::convertToNativeUnits(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceConvertSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/convert_setting", request, &response);

    return response.getValue();
}

/**
 * Convert arbitrary setting value from Zaber native units.
 * @param setting Name of the setting.
 * @param value Value of the setting in Zaber native units.
 * @param unit Units to convert value to.
 * @return Setting value.
 */
double AxisSettings::convertFromNativeUnits(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceConvertSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setFromNative(true);
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/convert_setting", request, &response);

    return response.getValue();
}

/**
 * Returns the default value of a setting.
 * @param setting Name of the setting.
 * @param unit Units of setting.
 * @return Default setting value.
 */
double AxisSettings::getDefault(const std::string& setting, Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting_default", request, &response);

    return response.getValue();
}

/**
 * Returns the default value of a setting as a string.
 * @param setting Name of the setting.
 * @return Default setting value.
 */
std::string AxisSettings::getDefaultString(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_setting_default_str", request, &response);

    return response.getValue();
}

/**
 * Indicates if given setting can be converted from and to native units.
 * @param setting Name of the setting.
 * @return True if unit conversion can be performed.
 */
bool AxisSettings::canConvertNativeUnits(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setSetting(setting);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/can_convert_setting", request, &response);

    return response.getValue();
}

/**
 * Overrides default unit conversions.
 * Conversion factors are specified by setting names representing underlying dimensions.
 * Requires at least Firmware 7.30.
 * @param conversions Factors of all conversions to override.
 */
void AxisSettings::setCustomUnitConversions(const std::vector<ConversionFactor>& conversions) {

    ::zaber::motion::requests::DeviceSetUnitConversionsRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setConversions(std::vector<ConversionFactor>(conversions));
    ::zaber::motion::callGateway("device/set_unit_conversions", request);
}

/**
 * Gets many setting values in as few requests as possible.
 * @param axisSettings The settings to read.
 * @return The setting values read.
 */
std::vector<GetAxisSettingResult> AxisSettings::getMany(std::initializer_list<GetAxisSetting> axisSettings) {

    ::zaber::motion::requests::DeviceMultiGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setAxisSettings(std::vector<GetAxisSetting>(axisSettings));
    ::zaber::motion::requests::GetAxisSettingResults response;
    ::zaber::motion::callGateway("device/get_many_settings", request, &response);

    return response.getResults();
}

/**
 * Gets many setting values in the same tick, ensuring their values are synchronized.
 * Requires at least Firmware 7.35.
 * @param axisSettings The settings to read.
 * @return The setting values read.
 */
std::vector<GetAxisSettingResult> AxisSettings::getSynchronized(std::initializer_list<GetAxisSetting> axisSettings) {

    ::zaber::motion::requests::DeviceMultiGetSettingRequest request;
    request.setInterfaceId(this->_axis.getDevice().getConnection().getInterfaceId());
    request.setDevice(this->_axis.getDevice().getDeviceAddress());
    request.setAxis(this->_axis.getAxisNumber());
    request.setAxisSettings(std::vector<GetAxisSetting>(axisSettings));
    ::zaber::motion::requests::GetAxisSettingResults response;
    ::zaber::motion::callGateway("device/get_sync_settings", request, &response);

    return response.getResults();
}

Axis AxisSettings::getAxis() const {
    return this->_axis;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
