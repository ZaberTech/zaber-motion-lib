﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include "zaber/motion/ascii/pvt.h"
#include "zaber/motion/ascii/pvt_buffer.h"
#include "zaber/motion/ascii/pvt_sequence.h"

#include "zaber/motion/exceptions/invalid_argument_exception.h"

namespace zaber { namespace motion { namespace ascii {

Pvt::Pvt(Device device): _device(std::move(device)) {
}

/**
 * Gets a PvtSequence class instance which allows you to control a particular PVT sequence on the device.
 * @param pvtId The ID of the PVT sequence to control. The IDs start at 1.
 * @return PvtSequence instance.
 */
PvtSequence Pvt::getSequence(int pvtId) {
    if (pvtId <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; PVT sequences are numbered from 1.");
    }

    return {this->_device, pvtId};
}

/**
 * Gets a PvtBuffer class instance which is a handle for a PVT buffer on the device.
 * @param pvtBufferId The ID of the PVT buffer to control. PVT buffer IDs start at one.
 * @return PvtBuffer instance.
 */
PvtBuffer Pvt::getBuffer(int pvtBufferId) {
    if (pvtBufferId <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; PVT buffers are numbered from 1.");
    }

    return {this->_device, pvtBufferId};
}

/**
 * Get a list of buffer IDs that are currently in use.
 * @return List of buffer IDs.
 */
std::vector<int> Pvt::listBufferIds() {

    ::zaber::motion::requests::StreamBufferList request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setPvt(true);
    ::zaber::motion::requests::IntArrayResponse response;
    ::zaber::motion::callGateway("device/stream_buffer_list", request, &response);

    return response.getValues();
}

/**
 * Device that this PVT belongs to.
 */
Device Pvt::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
