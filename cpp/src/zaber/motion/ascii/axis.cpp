﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/dto/ascii/axis_identity.h"
#include "zaber/motion/ascii/axis_settings.h"
#include "zaber/motion/dto/ascii/axis_type.h"
#include "zaber/motion/dto/ascii/response.h"
#include "zaber/motion/ascii/storage.h"
#include "zaber/motion/ascii/warnings.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Axis::Axis(Device device, int axisNumber): _device(std::move(device)), _axisNumber(axisNumber) {
}

Axis::Axis(): _device(), _axisNumber(-1) {
}

/**
 * Homes axis. Axis returns to its homing position.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Axis::home(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceHomeRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/home", request);
}

/**
 * Homes axis. Axis returns to its homing position.
 * @param options A struct of type HomeOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Axis::home(const Axis::HomeOptions& options) {
    Axis::home(options.waitUntilIdle);
}

/**
 * Stops ongoing axis movement. Decelerates until zero speed.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Axis::stop(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceStopRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/stop", request);
}

/**
 * Stops ongoing axis movement. Decelerates until zero speed.
 * @param options A struct of type StopOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Axis::stop(const Axis::StopOptions& options) {
    Axis::stop(options.waitUntilIdle);
}

/**
 * Parks the axis in anticipation of turning the power off.
 * It can later be powered on, unparked, and moved without first having to home it.
 */
void Axis::park() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::callGateway("device/park", request);
}

/**
 * Unparks axis. Axis will now be able to move.
 */
void Axis::unpark() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::callGateway("device/unpark", request);
}

/**
 * Returns bool indicating whether the axis is parked or not.
 * @return True if the axis is currently parked. False otherwise.
 */
bool Axis::isParked() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/is_parked", request, &response);

    return response.getValue();
}

/**
 * Waits until axis stops moving.
 * @param throwErrorOnFault Determines whether to throw error when fault is observed.
 */
void Axis::waitUntilIdle(bool throwErrorOnFault) {

    ::zaber::motion::requests::DeviceWaitUntilIdleRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setThrowErrorOnFault(throwErrorOnFault);
    ::zaber::motion::callGateway("device/wait_until_idle", request);
}

/**
 * Waits until axis stops moving.
 * @param options A struct of type WaitUntilIdleOptions. It has the following members:
 * * `throwErrorOnFault`: Determines whether to throw error when fault is observed.
 */
void Axis::waitUntilIdle(const Axis::WaitUntilIdleOptions& options) {
    Axis::waitUntilIdle(options.throwErrorOnFault);
}

/**
 * Returns bool indicating whether the axis is executing a motion command.
 * @return True if the axis is currently executing a motion command.
 */
bool Axis::isBusy() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/is_busy", request, &response);

    return response.getValue();
}

/**
 * Returns bool indicating whether the axis has position reference and was homed.
 * @return True if the axis has position reference and was homed.
 */
bool Axis::isHomed() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/is_homed", request, &response);

    return response.getValue();
}

/**
 * Move axis to absolute position.
 * @param position Absolute position.
 * @param unit Units of position.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveAbsolute(double position, Units unit, bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::ABS);
    request.setArg(position);
    request.setUnit(unit);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Move axis to absolute position.
 * @param position Absolute position.
 * @param unit Units of position.
 * @param options A struct of type MoveAbsoluteOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveAbsolute(double position, Units unit, const Axis::MoveAbsoluteOptions& options) {
    Axis::moveAbsolute(position, unit, options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Moves the axis to the maximum position as specified by limit.max.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveMax(bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::MAX);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Moves the axis to the maximum position as specified by limit.max.
 * @param options A struct of type MoveMaxOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveMax(const Axis::MoveMaxOptions& options) {
    Axis::moveMax(options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Moves the axis to the minimum position as specified by limit.min.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveMin(bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::MIN);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Moves the axis to the minimum position as specified by limit.min.
 * @param options A struct of type MoveMinOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveMin(const Axis::MoveMinOptions& options) {
    Axis::moveMin(options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Move axis to position relative to current position.
 * @param position Relative position.
 * @param unit Units of position.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveRelative(double position, Units unit, bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::REL);
    request.setArg(position);
    request.setUnit(unit);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Move axis to position relative to current position.
 * @param position Relative position.
 * @param unit Units of position.
 * @param options A struct of type MoveRelativeOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveRelative(double position, Units unit, const Axis::MoveRelativeOptions& options) {
    Axis::moveRelative(position, unit, options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Begins to move axis at specified speed.
 * @param velocity Movement velocity.
 * @param unit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveVelocity(double velocity, Units unit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::VEL);
    request.setArg(velocity);
    request.setUnit(unit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Begins to move axis at specified speed.
 * @param velocity Movement velocity.
 * @param unit Units of velocity.
 * @param options A struct of type MoveVelocityOptions. It has the following members:
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveVelocity(double velocity, Units unit, const Axis::MoveVelocityOptions& options) {
    Axis::moveVelocity(velocity, unit, options.acceleration, options.accelerationUnit);
}

/**
 * Returns current axis position.
 * @param unit Units of position.
 * @return Axis position.
 */
double Axis::getPosition(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setSetting("pos");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Gets number of index positions of the axis.
 * @return Number of index positions.
 */
int Axis::getNumberOfIndexPositions() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_count", request, &response);

    return response.getValue();
}

/**
 * Returns current axis index position.
 * @return Index position starting from 1 or 0 if the position is not an index position.
 */
int Axis::getIndexPosition() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_position", request, &response);

    return response.getValue();
}

/**
 * Moves the axis to index position.
 * @param index Index position. Index positions are numbered from 1.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 * @param velocity Movement velocity.
 * Default value of 0 indicates that the maxspeed setting is used instead.
 * Requires at least Firmware 7.25.
 * @param velocityUnit Units of velocity.
 * @param acceleration Movement acceleration.
 * Default value of 0 indicates that the accel setting is used instead.
 * Requires at least Firmware 7.25.
 * @param accelerationUnit Units of acceleration.
 */
void Axis::moveIndex(int index, bool waitUntilIdle, double velocity, Units velocityUnit, double acceleration, Units accelerationUnit) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setType(::zaber::motion::requests::AxisMoveType::INDEX);
    request.setArgInt(index);
    request.setWaitUntilIdle(waitUntilIdle);
    request.setVelocity(velocity);
    request.setVelocityUnit(velocityUnit);
    request.setAcceleration(acceleration);
    request.setAccelerationUnit(accelerationUnit);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Moves the axis to index position.
 * @param index Index position. Index positions are numbered from 1.
 * @param options A struct of type MoveIndexOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 * * `velocity`: Movement velocity.
 *   Default value of 0 indicates that the maxspeed setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `velocityUnit`: Units of velocity.
 * * `acceleration`: Movement acceleration.
 *   Default value of 0 indicates that the accel setting is used instead.
 *   Requires at least Firmware 7.25.
 * * `accelerationUnit`: Units of acceleration.
 */
void Axis::moveIndex(int index, const Axis::MoveIndexOptions& options) {
    Axis::moveIndex(index, options.waitUntilIdle, options.velocity, options.velocityUnit, options.acceleration, options.accelerationUnit);
}

/**
 * Sends a generic ASCII command to this axis.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Axis::genericCommand(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    Response response;
    ::zaber::motion::callGateway("interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic ASCII command to this axis.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response Axis::genericCommand(const std::string& command, const Axis::GenericCommandOptions& options) {
    return Axis::genericCommand(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this axis and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when a device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Axis::genericCommandMultiResponse(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    ::zaber::motion::requests::GenericCommandResponseCollection response;
    ::zaber::motion::callGateway("interface/generic_command_multi_response", request, &response);

    return response.getResponses();
}

/**
 * Sends a generic ASCII command to this axis and expect multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when a device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> Axis::genericCommandMultiResponse(const std::string& command, const Axis::GenericCommandMultiResponseOptions& options) {
    return Axis::genericCommandMultiResponse(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 */
void Axis::genericCommandNoResponse(const std::string& command) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setCommand(command);
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Formats parameters into a command and performs unit conversions.
 * Parameters in the command template are denoted by a question mark.
 * Command returned is only valid for this axis and this device.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
 * @param parameters Variable number of command parameters.
 * @return Command with converted parameters.
 */
std::string Axis::prepareCommand(const std::string& commandTemplate, std::initializer_list<Measurement> parameters) {

    ::zaber::motion::requests::PrepareCommandRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setCommandTemplate(commandTemplate);
    request.setParameters(std::vector<Measurement>(parameters));
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/prepare_command", request, &response);

    return response.getValue();
}

/**
 * Sets the user-assigned peripheral label.
 * The label is stored on the controller and recognized by other software.
 * @param label Label to set.
 */
void Axis::setLabel(const std::string& label) {

    ::zaber::motion::requests::DeviceSetStorageRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setValue(label);
    ::zaber::motion::callGateway("device/set_label", request);
}

/**
 * Gets the peripheral name.
 * @return The label.
 */
std::string Axis::retrieveLabel() const {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_label", request, &response);

    return response.getValue();
}

/**
 * Returns a string that represents the axis.
 * @return A string that represents the axis.
 */
std::string Axis::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/axis_to_string", request, &response);

    return response.getValue();
}

/**
 * Returns a serialization of the current axis state that can be saved and reapplied.
 * @return A serialization of the current state of the axis.
 */
std::string Axis::getState() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_state", request, &response);

    return response.getValue();
}

/**
 * Applies a saved state to this axis.
 * @param state The state object to apply to this axis.
 * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
 */
SetStateAxisResponse Axis::setState(const std::string& state) {

    ::zaber::motion::requests::SetStateRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setState(state);
    SetStateAxisResponse response;
    ::zaber::motion::callGateway("device/set_axis_state", request, &response);

    return response;
}

/**
 * Checks if a state can be applied to this axis.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param firmwareVersion The firmware version of the device to apply the state to.
 * Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this axis.
 */
std::optional<std::string> Axis::canSetState(const std::string& state, const std::optional<FirmwareVersion>& firmwareVersion) {

    ::zaber::motion::requests::CanSetStateRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setState(state);
    request.setFirmwareVersion(firmwareVersion);
    ::zaber::motion::requests::CanSetStateAxisResponse response;
    ::zaber::motion::callGateway("device/can_set_axis_state", request, &response);

    return response.getError();
}

/**
 * Checks if a state can be applied to this axis.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param options A struct of type CanSetStateOptions. It has the following members:
 * * `firmwareVersion`: The firmware version of the device to apply the state to.
 *   Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this axis.
 */
std::optional<std::string> Axis::canSetState(const std::string& state, const Axis::CanSetStateOptions& options) {
    return Axis::canSetState(state, options.firmwareVersion);
}

/**
 * Returns identity.
 * @return Axis identity.
 */
AxisIdentity Axis::retrieveIdentity() const {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    AxisIdentity response;
    ::zaber::motion::callGateway("device/get_axis_identity", request, &response);

    return response;
}

/**
 * Disables the driver, which prevents current from being sent to the motor or load.
 * If the driver is already disabled, the driver remains disabled.
 */
void Axis::driverDisable() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::callGateway("device/driver_disable", request);
}

/**
 * Attempts to enable the driver repeatedly for the specified timeout.
 * If the driver is already enabled, the driver remains enabled.
 * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
 */
void Axis::driverEnable(double timeout) {

    ::zaber::motion::requests::DriverEnableRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setTimeout(timeout);
    ::zaber::motion::callGateway("device/driver_enable", request);
}

/**
 * Attempts to enable the driver repeatedly for the specified timeout.
 * If the driver is already enabled, the driver remains enabled.
 * @param options A struct of type DriverEnableOptions. It has the following members:
 * * `timeout`: Timeout in seconds. Specify 0 to attempt to enable the driver once.
 */
void Axis::driverEnable(const Axis::DriverEnableOptions& options) {
    Axis::driverEnable(options.timeout);
}

/**
 * Activates a peripheral on this axis.
 * Removes all identity information for the device.
 * Run the identify method on the device after activating to refresh the information.
 */
void Axis::activate() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::callGateway("device/activate", request);
}

/**
 * Restores all axis settings to their default values.
 * Deletes all zaber axis storage keys.
 * Disables lockstep if the axis is part of one. Unparks the axis.
 * Preserves storage.
 * The device needs to be identified again after the restore.
 */
void Axis::restore() {

    ::zaber::motion::requests::DeviceRestoreRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    ::zaber::motion::callGateway("device/restore", request);
}

/**
 * Moves the axis in a sinusoidal trajectory.
 * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
 * @param amplitudeUnits Units of position.
 * @param period Period of the sinusoidal motion in milliseconds.
 * @param periodUnits Units of time.
 * @param count Number of sinusoidal cycles to complete.
 * Must be a multiple of 0.5
 * If count is not specified or set to 0, the axis will move indefinitely.
 * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
 */
void Axis::moveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count, bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceMoveSinRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setAmplitude(amplitude);
    request.setAmplitudeUnits(amplitudeUnits);
    request.setPeriod(period);
    request.setPeriodUnits(periodUnits);
    request.setCount(count);
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/move_sin", request);
}

/**
 * Moves the axis in a sinusoidal trajectory.
 * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
 * @param amplitudeUnits Units of position.
 * @param period Period of the sinusoidal motion in milliseconds.
 * @param periodUnits Units of time.
 * @param options A struct of type MoveSinOptions. It has the following members:
 * * `count`: Number of sinusoidal cycles to complete.
 *   Must be a multiple of 0.5
 *   If count is not specified or set to 0, the axis will move indefinitely.
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished or just started.
 */
void Axis::moveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, const Axis::MoveSinOptions& options) {
    Axis::moveSin(amplitude, amplitudeUnits, period, periodUnits, options.count, options.waitUntilIdle);
}

/**
 * Stops the axis at the end of the sinusoidal trajectory.
 * If the sinusoidal motion was started with an integer-plus-half cycle count,
 * the motion ends at the half-way point of the sinusoidal trajectory.
 * @param waitUntilIdle Determines whether function should return after the movement is finished.
 */
void Axis::moveSinStop(bool waitUntilIdle) {

    ::zaber::motion::requests::DeviceStopRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(this->getAxisNumber());
    request.setWaitUntilIdle(waitUntilIdle);
    ::zaber::motion::callGateway("device/move_sin_stop", request);
}

/**
 * Stops the axis at the end of the sinusoidal trajectory.
 * If the sinusoidal motion was started with an integer-plus-half cycle count,
 * the motion ends at the half-way point of the sinusoidal trajectory.
 * @param options A struct of type MoveSinStopOptions. It has the following members:
 * * `waitUntilIdle`: Determines whether function should return after the movement is finished.
 */
void Axis::moveSinStop(const Axis::MoveSinStopOptions& options) {
    Axis::moveSinStop(options.waitUntilIdle);
}

/**
 * Device that controls this axis.
 */
Device Axis::getDevice() const {
    return this->_device;
}

/**
 * The axis number identifies the axis on the device.
 * The first axis has the number one.
 */
int Axis::getAxisNumber() const {
    return this->_axisNumber;
}

/**
 * Settings and properties of this axis.
 */
AxisSettings Axis::getSettings() const {
    return {*this};
}

/**
 * Key-value storage of this axis.
 * Requires at least Firmware 7.30.
 */
AxisStorage Axis::getStorage() const {
    return {*this};
}

/**
 * Warnings and faults of this axis.
 */
Warnings Axis::getWarnings() const {
    return {this->_device, this->_axisNumber};
}

/**
 * Identity of the axis.
 */
AxisIdentity Axis::getIdentity() const {
    return this->retrieveIdentity();
}

/**
 * Unique ID of the peripheral hardware.
 */
int Axis::getPeripheralId() const {
    return this->getIdentity().getPeripheralId();
}

/**
 * Name of the peripheral.
 */
std::string Axis::getPeripheralName() const {
    return this->getIdentity().getPeripheralName();
}

/**
 * Serial number of the peripheral, or 0 when not applicable.
 */
unsigned int Axis::getPeripheralSerialNumber() const {
    return this->getIdentity().getPeripheralSerialNumber();
}

/**
 * Indicates whether the axis is a peripheral or part of an integrated device.
 */
bool Axis::getIsPeripheral() const {
    return this->getIdentity().getIsPeripheral();
}

/**
 * Determines the type of an axis and units it accepts.
 */
AxisType Axis::getAxisType() const {
    return this->getIdentity().getAxisType();
}

/**
 * User-assigned label of the peripheral.
 */
std::string Axis::getLabel() const {
    return this->retrieveLabel();
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
