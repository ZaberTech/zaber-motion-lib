﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/servo_tuner.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

/**
 * Creates instance of ServoTuner for the given axis.
 */
ServoTuner::ServoTuner(Axis axis): _axis(std::move(axis)) {
}

/**
 * Get the paramset that this device uses by default when it starts up.
 * @return The paramset used when the device restarts.
 */
ServoTuningParamset ServoTuner::getStartupParamset() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    ::zaber::motion::requests::ServoTuningParamsetResponse response;
    ::zaber::motion::callGateway("servotuning/get_startup_set", request, &response);

    return response.getParamset();
}

/**
 * Set the paramset that this device uses by default when it starts up.
 * @param paramset The paramset to use at startup.
 */
void ServoTuner::setStartupParamset(ServoTuningParamset paramset) {

    ::zaber::motion::requests::ServoTuningRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    ::zaber::motion::callGateway("servotuning/set_startup_set", request);
}

/**
 * Load the values from one paramset into another.
 * @param toParamset The paramset to load into.
 * @param fromParamset The paramset to load from.
 */
void ServoTuner::loadParamset(ServoTuningParamset toParamset, ServoTuningParamset fromParamset) {

    ::zaber::motion::requests::LoadParamset request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setToParamset(toParamset);
    request.setFromParamset(fromParamset);
    ::zaber::motion::callGateway("servotuning/load_paramset", request);
}

/**
 * Get the full set of tuning parameters used by the firmware driving this axis.
 * @param paramset The paramset to get tuning for.
 * @return The raw representation of the current tuning.
 */
ParamsetInfo ServoTuner::getTuning(ServoTuningParamset paramset) {

    ::zaber::motion::requests::ServoTuningRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    ParamsetInfo response;
    ::zaber::motion::callGateway("servotuning/get_raw", request, &response);

    return response;
}

/**
 * Set individual tuning parameters.
 * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
 * @param paramset The paramset to set tuning of.
 * @param tuningParams The params to set.
 * @param setUnspecifiedToDefault If true, any tuning parameters not included in TuningParams
 * are reset to their default values.
 */
void ServoTuner::setTuning(ServoTuningParamset paramset, const std::vector<ServoTuningParam>& tuningParams, bool setUnspecifiedToDefault) {

    ::zaber::motion::requests::SetServoTuningRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    request.setTuningParams(std::vector<ServoTuningParam>(tuningParams));
    request.setSetUnspecifiedToDefault(setUnspecifiedToDefault);
    ::zaber::motion::callGateway("servotuning/set_raw", request);
}

/**
 * Sets the tuning of a paramset using the PID method.
 * @param paramset The paramset to get tuning for.
 * @param p The proportional gain. Must be in units of N/m.
 * @param i The integral gain. Must be in units of N/m⋅s.
 * @param d The derivative gain. Must be in units of N⋅s/m.
 * @param fc The cutoff frequency. Must be in units of Hz.
 * @return The PID representation of the current tuning after your changes have been applied.
 */
PidTuning ServoTuner::setPidTuning(ServoTuningParamset paramset, double p, double i, double d, double fc) {

    ::zaber::motion::requests::SetServoTuningPIDRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    request.setP(p);
    request.setI(i);
    request.setD(d);
    request.setFc(fc);
    PidTuning response;
    ::zaber::motion::callGateway("servotuning/set_pid", request, &response);

    return response;
}

/**
 * Gets the PID representation of this paramset's servo tuning.
 * @param paramset The paramset to get tuning for.
 * @return The PID representation of the current tuning.
 */
PidTuning ServoTuner::getPidTuning(ServoTuningParamset paramset) {

    ::zaber::motion::requests::ServoTuningRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    PidTuning response;
    ::zaber::motion::callGateway("servotuning/get_pid", request, &response);

    return response;
}

/**
 * Gets the parameters that are required to tune this device.
 * @return The tuning parameters.
 */
std::vector<SimpleTuningParamDefinition> ServoTuner::getSimpleTuningParamDefinitions() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    ::zaber::motion::requests::GetSimpleTuningParamDefinitionResponse response;
    ::zaber::motion::callGateway("servotuning/get_simple_params_definition", request, &response);

    return response.getParams();
}

/**
 * Set the tuning of this device using the simple input method.
 * @param paramset The paramset to set tuning for.
 * @param tuningParams The params used to tune this device.
 * To get what parameters are expected, call GetSimpleTuningParamList.
 * All values must be between 0 and 1.
 * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
 * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
 */
void ServoTuner::setSimpleTuning(ServoTuningParamset paramset, const std::vector<ServoTuningParam>& tuningParams, double loadMass, const std::optional<double>& carriageMass) {

    ::zaber::motion::requests::SetSimpleTuning request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    request.setTuningParams(std::vector<ServoTuningParam>(tuningParams));
    request.setLoadMass(loadMass);
    request.setCarriageMass(carriageMass);
    ::zaber::motion::callGateway("servotuning/set_simple_tuning", request);
}

/**
 * Set the tuning of this device using the simple input method.
 * @param paramset The paramset to set tuning for.
 * @param tuningParams The params used to tune this device.
 * To get what parameters are expected, call GetSimpleTuningParamList.
 * All values must be between 0 and 1.
 * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
 * @param options A struct of type SetSimpleTuningOptions. It has the following members:
 * * `carriageMass`: The mass of the carriage in kg. If this value is not set the default carriage mass is used.
 */
void ServoTuner::setSimpleTuning(ServoTuningParamset paramset, const std::vector<ServoTuningParam>& tuningParams, double loadMass, const ServoTuner::SetSimpleTuningOptions& options) {
    ServoTuner::setSimpleTuning(paramset, tuningParams, loadMass, options.carriageMass);
}

/**
 * Get the simple tuning parameters for this device.
 * @param paramset The paramset to get tuning for.
 * @return The simple tuning parameters.
 */
SimpleTuning ServoTuner::getSimpleTuning(ServoTuningParamset paramset) {

    ::zaber::motion::requests::ServoTuningRequest request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    SimpleTuning response;
    ::zaber::motion::callGateway("servotuning/get_simple_tuning", request, &response);

    return response;
}

/**
 * Deprecated: Use GetSimpleTuning instead.
 *
 * Checks if the provided simple tuning is being stored by this paramset.
 * @param paramset The paramset to set tuning for.
 * @param tuningParams The params used to tune this device.
 * To get what parameters are expected, call GetSimpleTuningParamList.
 * All values must be between 0 and 1.
 * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
 * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
 * @return True if the provided simple tuning is currently stored in this paramset.
 */
bool ServoTuner::isUsingSimpleTuning(ServoTuningParamset paramset, const std::vector<ServoTuningParam>& tuningParams, double loadMass, const std::optional<double>& carriageMass) {

    ::zaber::motion::requests::SetSimpleTuning request;
    request.setInterfaceId(this->getAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getAxis().getAxisNumber());
    request.setParamset(paramset);
    request.setTuningParams(std::vector<ServoTuningParam>(tuningParams));
    request.setLoadMass(loadMass);
    request.setCarriageMass(carriageMass);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("servotuning/is_using_simple_tuning", request, &response);

    return response.getValue();
}

/**
 * Deprecated: Use GetSimpleTuning instead.
 *
 * Checks if the provided simple tuning is being stored by this paramset.
 * @param paramset The paramset to set tuning for.
 * @param tuningParams The params used to tune this device.
 * To get what parameters are expected, call GetSimpleTuningParamList.
 * All values must be between 0 and 1.
 * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
 * @param options A struct of type IsUsingSimpleTuningOptions. It has the following members:
 * * `carriageMass`: The mass of the carriage in kg. If this value is not set the default carriage mass is used.
 * @return True if the provided simple tuning is currently stored in this paramset.
 */
bool ServoTuner::isUsingSimpleTuning(ServoTuningParamset paramset, const std::vector<ServoTuningParam>& tuningParams, double loadMass, const ServoTuner::IsUsingSimpleTuningOptions& options) {
    return ServoTuner::isUsingSimpleTuning(paramset, tuningParams, loadMass, options.carriageMass);
}

/**
 * The axis that will be tuned.
 */
Axis ServoTuner::getAxis() const {
    return this->_axis;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
