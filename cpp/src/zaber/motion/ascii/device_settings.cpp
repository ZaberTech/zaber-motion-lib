﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/device_settings.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

DeviceSettings::DeviceSettings(Device device): _device(std::move(device)) {
}

/**
 * Returns any device setting or property.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param unit Units of setting.
 * @return Setting value.
 */
double DeviceSettings::get(const std::string& setting, Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Sets any device setting.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param value Value of the setting.
 * @param unit Units of setting.
 */
void DeviceSettings::set(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Returns any device setting or property as a string.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @return Setting value.
 */
std::string DeviceSettings::getString(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_setting_str", request, &response);

    return response.getValue();
}

/**
 * Sets any device setting as a string.
 * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 * @param setting Name of the setting.
 * @param value Value of the setting.
 */
void DeviceSettings::setString(const std::string& setting, const std::string& value) {

    ::zaber::motion::requests::DeviceSetSettingStrRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setValue(value);
    ::zaber::motion::callGateway("device/set_setting_str", request);
}

/**
 * Convert arbitrary setting value to Zaber native units.
 * @param setting Name of the setting.
 * @param value Value of the setting in units specified by following argument.
 * @param unit Units of the value.
 * @return Setting value.
 */
double DeviceSettings::convertToNativeUnits(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceConvertSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/convert_setting", request, &response);

    return response.getValue();
}

/**
 * Convert arbitrary setting value from Zaber native units.
 * @param setting Name of the setting.
 * @param value Value of the setting in Zaber native units.
 * @param unit Units to convert value to.
 * @return Setting value.
 */
double DeviceSettings::convertFromNativeUnits(const std::string& setting, double value, Units unit) {

    ::zaber::motion::requests::DeviceConvertSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setFromNative(true);
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/convert_setting", request, &response);

    return response.getValue();
}

/**
 * Returns the default value of a setting.
 * @param setting Name of the setting.
 * @param unit Units of setting.
 * @return Default setting value.
 */
double DeviceSettings::getDefault(const std::string& setting, Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting_default", request, &response);

    return response.getValue();
}

/**
 * Returns the default value of a setting as a string.
 * @param setting Name of the setting.
 * @return Default setting value.
 */
std::string DeviceSettings::getDefaultString(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_setting_default_str", request, &response);

    return response.getValue();
}

/**
 * Indicates if given setting can be converted from and to native units.
 * @param setting Name of the setting.
 * @return True if unit conversion can be performed.
 */
bool DeviceSettings::canConvertNativeUnits(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("device/can_convert_setting", request, &response);

    return response.getValue();
}

/**
 * Gets the value of an axis scope setting for each axis on the device.
 * Values may be NaN where the setting is not applicable.
 * @param setting Name of the setting.
 * @return The setting values on each axis.
 */
std::vector<double> DeviceSettings::getFromAllAxes(const std::string& setting) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    ::zaber::motion::requests::DoubleArrayResponse response;
    ::zaber::motion::callGateway("device/get_setting_from_all_axes", request, &response);

    return response.getValues();
}

/**
 * Gets many setting values in as few device requests as possible.
 * @param settings The settings to read.
 * @return The setting values read.
 */
std::vector<GetSettingResult> DeviceSettings::getMany(std::initializer_list<GetSetting> settings) {

    ::zaber::motion::requests::DeviceMultiGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSettings(std::vector<GetSetting>(settings));
    ::zaber::motion::requests::GetSettingResults response;
    ::zaber::motion::callGateway("device/get_many_settings", request, &response);

    return response.getResults();
}

/**
 * Gets many setting values in the same tick, ensuring their values are synchronized.
 * Requires at least Firmware 7.35.
 * @param settings The settings to read.
 * @return The setting values read.
 */
std::vector<GetSettingResult> DeviceSettings::getSynchronized(std::initializer_list<GetSetting> settings) {

    ::zaber::motion::requests::DeviceMultiGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSettings(std::vector<GetSetting>(settings));
    ::zaber::motion::requests::GetSettingResults response;
    ::zaber::motion::callGateway("device/get_sync_settings", request, &response);

    return response.getResults();
}

Device DeviceSettings::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
