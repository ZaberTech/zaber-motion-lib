﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/ascii/pvt_io.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber {
namespace motion {
namespace ascii {

PvtIo::PvtIo(Device device, int streamId): _device(std::move(device)), _streamId(streamId) {
}

/**
 * Sets value for the specified digital output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform on the channel.
 */
void PvtIo::setDigitalOutput(int channelNumber, DigitalOutputAction value) {

    ::zaber::motion::requests::StreamSetDigitalOutputRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_digital_output", request);
}

/**
 * Sets values for all digital output channels.
 * @param values The type of action to perform on the channel.
 */
void PvtIo::setAllDigitalOutputs(const std::vector<DigitalOutputAction>& values) {

    ::zaber::motion::requests::StreamSetAllDigitalOutputsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setValues(std::vector<DigitalOutputAction>(values));
    ::zaber::motion::callGateway("device/stream_set_all_digital_outputs", request);
}

/**
 * Sets value for the specified analog output channel.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to.
 */
void PvtIo::setAnalogOutput(int channelNumber, double value) {

    ::zaber::motion::requests::StreamSetAnalogOutputRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    ::zaber::motion::callGateway("device/stream_set_analog_output", request);
}

/**
 * Sets values for all analog output channels.
 * @param values Voltage values to set the output channels to.
 */
void PvtIo::setAllAnalogOutputs(const std::vector<double>& values) {

    ::zaber::motion::requests::StreamSetAllAnalogOutputsRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setValues(std::vector<double>(values));
    ::zaber::motion::callGateway("device/stream_set_all_analog_outputs", request);
}

/**
 * Sets current and future value for the specified digital output channel.
 * Requires at least Firmware 7.37.
 * @param channelNumber Channel number starting at 1.
 * @param value The type of action to perform immediately on the channel.
 * @param futureValue The type of action to perform in the future on the channel.
 * @param delay Delay between setting current value and setting future value.
 * @param unit Units of time.
 */
void PvtIo::setDigitalOutputSchedule(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::StreamSetDigitalOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    request.setFutureValue(futureValue);
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_digital_output_schedule", request);
}

/**
 * Sets current and future values for all digital output channels.
 * Requires at least Firmware 7.37.
 * @param values The type of actions to perform immediately on output channels.
 * @param futureValues The type of actions to perform in the future on output channels.
 * @param delay Delay between setting current values and setting future values.
 * @param unit Units of time.
 */
void PvtIo::setAllDigitalOutputsSchedule(const std::vector<DigitalOutputAction>& values, const std::vector<DigitalOutputAction>& futureValues, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::StreamSetAllDigitalOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setValues(std::vector<DigitalOutputAction>(values));
    request.setFutureValues(std::vector<DigitalOutputAction>(futureValues));
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_all_digital_outputs_schedule", request);
}

/**
 * Sets current and future value for the specified analog output channel.
 * Requires at least Firmware 7.38.
 * @param channelNumber Channel number starting at 1.
 * @param value Value to set the output channel voltage to immediately.
 * @param futureValue Value to set the output channel voltage to in the future.
 * @param delay Delay between setting current value and setting future value.
 * @param unit Units of time.
 */
void PvtIo::setAnalogOutputSchedule(int channelNumber, double value, double futureValue, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::StreamSetAnalogOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    request.setValue(value);
    request.setFutureValue(futureValue);
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_analog_output_schedule", request);
}

/**
 * Sets current and future values for all analog output channels.
 * Requires at least Firmware 7.38.
 * @param values Voltage values to set the output channels to immediately.
 * @param futureValues Voltage values to set the output channels to in the future.
 * @param delay Delay between setting current values and setting future values.
 * @param unit Units of time.
 */
void PvtIo::setAllAnalogOutputsSchedule(const std::vector<double>& values, const std::vector<double>& futureValues, double delay, Units unit) {
    if (delay <= 0) {
        throw exceptions::InvalidArgumentException("Delay must be a positive value.");
    }

    ::zaber::motion::requests::StreamSetAllAnalogOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setValues(std::vector<double>(values));
    request.setFutureValues(std::vector<double>(futureValues));
    request.setDelay(delay);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/stream_set_all_analog_outputs_schedule", request);
}

/**
 * Cancels a scheduled digital output action.
 * Requires at least Firmware 7.37.
 * @param channelNumber Channel number starting at 1.
 */
void PvtIo::cancelDigitalOutputSchedule(int channelNumber) {

    ::zaber::motion::requests::StreamCancelOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(false);
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    ::zaber::motion::callGateway("device/stream_cancel_output_schedule", request);
}

/**
 * Cancel all scheduled digital output actions.
 * Requires at least Firmware 7.37.
 * @param channels Optionally specify which channels to cancel.
 * Array length must be empty or equal to the number of channels on device.
 * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
 */
void PvtIo::cancelAllDigitalOutputsSchedule(const std::vector<bool>& channels) {

    ::zaber::motion::requests::StreamCancelAllOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(false);
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannels(std::vector<bool>(channels));
    ::zaber::motion::callGateway("device/stream_cancel_all_outputs_schedule", request);
}

/**
 * Cancels a scheduled analog output value.
 * Requires at least Firmware 7.38.
 * @param channelNumber Channel number starting at 1.
 */
void PvtIo::cancelAnalogOutputSchedule(int channelNumber) {

    ::zaber::motion::requests::StreamCancelOutputScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(true);
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannelNumber(channelNumber);
    ::zaber::motion::callGateway("device/stream_cancel_output_schedule", request);
}

/**
 * Cancel all scheduled analog output actions.
 * Requires at least Firmware 7.38.
 * @param channels Optionally specify which channels to cancel.
 * Array length must be empty or equal to the number of channels on device.
 * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
 */
void PvtIo::cancelAllAnalogOutputsSchedule(const std::vector<bool>& channels) {

    ::zaber::motion::requests::StreamCancelAllOutputsScheduleRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setAnalog(true);
    request.setStreamId(this->getStreamId());
    request.setPvt(true);
    request.setChannels(std::vector<bool>(channels));
    ::zaber::motion::callGateway("device/stream_cancel_all_outputs_schedule", request);
}

Device PvtIo::getDevice() const {
    return this->_device;
}

int PvtIo::getStreamId() const {
    return this->_streamId;
}


}  // namespace ascii
}  // namespace motion
}  // namespace zaber
