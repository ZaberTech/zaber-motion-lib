﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/ascii/triggers.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


namespace zaber { namespace motion { namespace ascii {

Triggers::Triggers(Device device): _device(std::move(device)) {
}

/**
 * Get the number of triggers for this device.
 * @return Number of triggers for this device.
 */
int Triggers::getNumberOfTriggers() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("trigger.numtriggers");
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("triggers/get_setting", request, &response);

    return response.getValue();
}

/**
 * Get the number of actions for each trigger for this device.
 * @return Number of actions for each trigger for this device.
 */
int Triggers::getNumberOfActions() {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setSetting("trigger.numactions");
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("triggers/get_setting", request, &response);

    return response.getValue();
}

/**
 * Get a specific trigger for this device.
 * @param triggerNumber The number of the trigger to control. Trigger numbers start at 1.
 * @return Trigger instance.
 */
Trigger Triggers::getTrigger(int triggerNumber) {
    if (triggerNumber <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; triggers are numbered from 1.");
    }

    return {this->_device, triggerNumber};
}

/**
 * Get the state for every trigger for this device.
 * @return Complete state for every trigger.
 */
std::vector<TriggerState> Triggers::getTriggerStates() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::TriggerStates response;
    ::zaber::motion::callGateway("triggers/get_trigger_states", request, &response);

    return response.getStates();
}

/**
 * Gets the enabled state for every trigger for this device.
 * @return Whether triggers are enabled and the number of times they will fire.
 */
std::vector<TriggerEnabledState> Triggers::getEnabledStates() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::TriggerEnabledStates response;
    ::zaber::motion::callGateway("triggers/get_enabled_states", request, &response);

    return response.getStates();
}

/**
 * Gets the labels for every trigger for this device.
 * @return The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.
 */
std::vector<std::string> Triggers::getAllLabels() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::StringArrayResponse response;
    ::zaber::motion::callGateway("triggers/get_all_labels", request, &response);

    return response.getValues();
}

/**
 * Device that these triggers belong to.
 */
Device Triggers::getDevice() const {
    return this->_device;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber
