﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/unit_table.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <cstdint>


namespace zaber { namespace motion {

/**
 * Gets the standard symbol associated with a given unit.
 * @param unit Unit of measure.
 * @return Symbols corresponding to the given unit. Throws NoValueForKey if no symbol is defined.
 */
std::string UnitTable::getSymbol(Units unit) {

    ::zaber::motion::requests::UnitGetSymbolRequest request;
    request.setUnit(unit);
    ::zaber::motion::requests::UnitGetSymbolResponse response;
    ::zaber::motion::callGateway("units/get_symbol", request, &response);

    return response.getSymbol();
}

/**
 * Gets the unit enum value associated with a standard symbol.
 * Note not all units can be retrieved this way.
 * @param symbol Symbol to look up.
 * @return The unit enum value with the given symbols. Throws NoValueForKey if the symbol is not supported for lookup.
 */
Units UnitTable::getUnit(const std::string& symbol) {

    ::zaber::motion::requests::UnitGetEnumRequest request;
    request.setSymbol(symbol);
    ::zaber::motion::requests::UnitGetEnumResponse response;
    ::zaber::motion::callGateway("units/get_enum", request, &response);

    return response.getUnit();
}

}  // namespace motion
}  // namespace zaber
