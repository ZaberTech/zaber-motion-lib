﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "camera_trigger.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `CameraTrigger` based on the given device and digital output channel.
 */
CameraTrigger::CameraTrigger(Device device, int channel): _device(std::move(device)), _channel(channel) {
}

CameraTrigger::CameraTrigger(): _device(), _channel(0) {
}

/**
 * Triggers the camera.
 * Schedules trigger pulse on the digital output channel.
 * By default, the method waits until the trigger pulse is finished.
 * @param pulseWidth The time duration of the trigger pulse.
 * Depending on the camera setting, the argument can be use to specify exposure.
 * @param unit Units of time.
 * @param wait If false, the method does not wait until the trigger pulse is finished.
 */
void CameraTrigger::trigger(double pulseWidth, Units unit, bool wait) {

    ::zaber::motion::requests::MicroscopeTriggerCameraRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setChannelNumber(this->getChannel());
    request.setDelay(pulseWidth);
    request.setUnit(unit);
    request.setWait(wait);
    ::zaber::motion::callGateway("microscope/trigger_camera", request);
}

/**
 * Triggers the camera.
 * Schedules trigger pulse on the digital output channel.
 * By default, the method waits until the trigger pulse is finished.
 * @param pulseWidth The time duration of the trigger pulse.
 * Depending on the camera setting, the argument can be use to specify exposure.
 * @param unit Units of time.
 * @param options A struct of type TriggerOptions. It has the following members:
 * * `wait`: If false, the method does not wait until the trigger pulse is finished.
 */
void CameraTrigger::trigger(double pulseWidth, Units unit, const CameraTrigger::TriggerOptions& options) {
    CameraTrigger::trigger(pulseWidth, unit, options.wait);
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string CameraTrigger::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * The device whose digital output triggers the camera.
 */
Device CameraTrigger::getDevice() const {
    return this->_device;
}

/**
 * The digital output channel that triggers the camera.
 */
int CameraTrigger::getChannel() const {
    return this->_channel;
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
