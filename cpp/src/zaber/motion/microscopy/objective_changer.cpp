﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "objective_changer.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `ObjectiveChanger` based on the given device.
 * If the device is identified, this constructor will ensure it is an objective changer.
 */
ObjectiveChanger::ObjectiveChanger(Device turret, Axis focusAxis): _turret(std::move(turret)), _focusAxis(std::move(focusAxis)) {
    this->verifyIsChanger();
}

ObjectiveChanger::ObjectiveChanger(): _turret(), _focusAxis() {
    this->verifyIsChanger();
}

/**
 * Deprecated: Use microscope's `Find` method instead or instantiate manually.
 *
 * Finds an objective changer on a connection.
 * In case of conflict, specify the optional device addresses.
 * Devices on the connection must be identified.
 * @param connection Connection on which to detect the objective changer.
 * @param turretAddress Optional device address of the turret device (X-MOR).
 * @param focusAddress Optional device address of the focus device (X-LDA).
 * @return New instance of objective changer.
 */
ObjectiveChanger ObjectiveChanger::find(const BaseConnection& connection, int turretAddress, int focusAddress) {

    ::zaber::motion::requests::ObjectiveChangerRequest request;
    request.setInterfaceId(connection.getInterfaceId());
    request.setTurretAddress(turretAddress);
    request.setFocusAddress(focusAddress);
    ::zaber::motion::requests::ObjectiveChangerCreateResponse response;
    ::zaber::motion::callGateway("objective_changer/detect", request, &response);

    return {{connection, response.getTurret()}, {{connection, response.getFocusAddress()}, response.getFocusAxis()}};
}

/**
 * Deprecated: Use microscope's `Find` method instead or instantiate manually.
 *
 * Finds an objective changer on a connection.
 * In case of conflict, specify the optional device addresses.
 * Devices on the connection must be identified.
 * @param connection Connection on which to detect the objective changer.
 * @param options A struct of type FindOptions. It has the following members:
 * * `turretAddress`: Optional device address of the turret device (X-MOR).
 * * `focusAddress`: Optional device address of the focus device (X-LDA).
 * @return New instance of objective changer.
 */
ObjectiveChanger ObjectiveChanger::find(const BaseConnection& connection, const ObjectiveChanger::FindOptions& options) {
    return ObjectiveChanger::find(connection, options.turretAddress, options.focusAddress);
}

/**
 * Changes the objective.
 * Runs a sequence of movements switching from the current objective to the new one.
 * The focus stage moves to the focus datum after the objective change.
 * @param objective Objective number starting from 1.
 * @param focusOffset Optional offset from the focus datum.
 */
void ObjectiveChanger::change(int objective, const std::optional<Measurement>& focusOffset) {

    ::zaber::motion::requests::ObjectiveChangerChangeRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setTurretAddress(this->getTurret().getDeviceAddress());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setObjective(objective);
    request.setFocusOffset(focusOffset);
    ::zaber::motion::callGateway("objective_changer/change", request);
}

/**
 * Changes the objective.
 * Runs a sequence of movements switching from the current objective to the new one.
 * The focus stage moves to the focus datum after the objective change.
 * @param objective Objective number starting from 1.
 * @param options A struct of type ChangeOptions. It has the following members:
 * * `focusOffset`: Optional offset from the focus datum.
 */
void ObjectiveChanger::change(int objective, const ObjectiveChanger::ChangeOptions& options) {
    ObjectiveChanger::change(objective, options.focusOffset);
}

/**
 * Moves the focus stage out of the turret releasing the current objective.
 */
void ObjectiveChanger::release() {

    ::zaber::motion::requests::ObjectiveChangerRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setTurretAddress(this->getTurret().getDeviceAddress());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    ::zaber::motion::callGateway("objective_changer/release", request);
}

/**
 * Returns current objective number starting from 1.
 * The value of 0 indicates that the position is either unknown or between two objectives.
 * @return Current objective number starting from 1 or 0 if not applicable.
 */
int ObjectiveChanger::getCurrentObjective() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setDevice(this->getTurret().getDeviceAddress());
    request.setAxis(1);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_position", request, &response);

    return response.getValue();
}

/**
 * Gets number of objectives that the turret can accommodate.
 * @return Number of positions.
 */
int ObjectiveChanger::getNumberOfObjectives() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setDevice(this->getTurret().getDeviceAddress());
    request.setAxis(1);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_count", request, &response);

    return response.getValue();
}

/**
 * Gets the focus datum.
 * The focus datum is the position that the focus stage moves to after an objective change.
 * It is backed by the limit.home.offset setting.
 * @param unit Units of datum.
 * @return The datum.
 */
double ObjectiveChanger::getFocusDatum(Units unit) {

    ::zaber::motion::requests::ObjectiveChangerSetRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setTurretAddress(this->getTurret().getDeviceAddress());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("objective_changer/get_datum", request, &response);

    return response.getValue();
}

/**
 * Sets the focus datum.
 * The focus datum is the position that the focus stage moves to after an objective change.
 * It is backed by the limit.home.offset setting.
 * @param datum Value of datum.
 * @param unit Units of datum.
 */
void ObjectiveChanger::setFocusDatum(double datum, Units unit) {

    ::zaber::motion::requests::ObjectiveChangerSetRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setTurretAddress(this->getTurret().getDeviceAddress());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setValue(datum);
    request.setUnit(unit);
    ::zaber::motion::callGateway("objective_changer/set_datum", request);
}

/**
 * Checks if this is a objective changer and throws an error if it is not.
 */
void ObjectiveChanger::verifyIsChanger() const {

    ::zaber::motion::requests::ObjectiveChangerRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setTurretAddress(this->getTurret().getDeviceAddress());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    ::zaber::motion::callGateway("objective_changer/verify", request);
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string ObjectiveChanger::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getTurret().getConnection().getInterfaceId());
    request.setDevice(this->getTurret().getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * Device address of the turret.
 */
Device ObjectiveChanger::getTurret() const {
    return this->_turret;
}

/**
 * The focus axis.
 */
Axis ObjectiveChanger::getFocusAxis() const {
    return this->_focusAxis;
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
