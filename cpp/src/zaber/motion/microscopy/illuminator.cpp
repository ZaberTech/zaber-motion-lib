﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "illuminator.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/ascii/device_io.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"
#include "zaber/motion/microscopy/illuminator_channel.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Device = zaber::motion::ascii::Device;
using DeviceIO = zaber::motion::ascii::DeviceIO;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `Illuminator` based on the given device.
 * If the device is identified, this constructor will ensure it is an illuminator.
 */
Illuminator::Illuminator(Device device): _device(std::move(device)) {
    this->verifyIsIlluminator();
}

Illuminator::Illuminator(): _device() {
    this->verifyIsIlluminator();
}

/**
 * Gets an IlluminatorChannel class instance that allows control of a particular channel.
 * Channels are numbered from 1.
 * @param channelNumber Number of channel to control.
 * @return Illuminator channel instance.
 */
IlluminatorChannel Illuminator::getChannel(int channelNumber) {
    if (channelNumber <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; channels are numbered from 1.");
    }

    return {*this, channelNumber};
}

/**
 * Checks if this is an illuminator or some other type of device and throws an error if it is not.
 */
void Illuminator::verifyIsIlluminator() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::callGateway("illuminator/verify", request);
}

/**
 * Finds an illuminator on a connection.
 * In case of conflict, specify the optional device address.
 * @param connection Connection on which to detect the illuminator.
 * @param deviceAddress Optional device address of the illuminator.
 * @return New instance of illuminator.
 */
Illuminator Illuminator::find(const BaseConnection& connection, int deviceAddress) {

    ::zaber::motion::requests::FindDeviceRequest request;
    request.setInterfaceId(connection.getInterfaceId());
    request.setDeviceAddress(deviceAddress);
    ::zaber::motion::requests::FindDeviceResponse response;
    ::zaber::motion::callGateway("illuminator/detect", request, &response);

    return {{connection, response.getAddress()}};
}

/**
 * Finds an illuminator on a connection.
 * In case of conflict, specify the optional device address.
 * @param connection Connection on which to detect the illuminator.
 * @param options A struct of type FindOptions. It has the following members:
 * * `deviceAddress`: Optional device address of the illuminator.
 * @return New instance of illuminator.
 */
Illuminator Illuminator::find(const BaseConnection& connection, const Illuminator::FindOptions& options) {
    return Illuminator::find(connection, options.deviceAddress);
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string Illuminator::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * The base device of this illuminator.
 */
Device Illuminator::getDevice() const {
    return this->_device;
}

/**
 * I/O channels of this device.
 */
DeviceIO Illuminator::getIO() const {
    return {this->_device};
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
