﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "microscope.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using AxisGroup = zaber::motion::ascii::AxisGroup;
using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `Microscope` from the given config.
 * Parts are instantiated depending on device addresses in the config.
 */
Microscope::Microscope(BaseConnection connection, MicroscopeConfig config): _connection(std::move(connection)), _config(std::move(config)), _illuminator(
        _config.getIlluminator().value_or(0) > 0 ?
        std::optional<Illuminator>(Device{_connection, _config.getIlluminator().value()}) : std::nullopt), _focusAxis(
        _config.getFocusAxis() && _config.getFocusAxis()->getDevice() > 0 ?
        std::optional<Axis>({Device{_connection, _config.getFocusAxis()->getDevice()}, _config.getFocusAxis()->getAxis()}) : std::nullopt), _xAxis(
        _config.getXAxis() && _config.getXAxis()->getDevice() > 0 ?
        std::optional<Axis>({Device{_connection, _config.getXAxis()->getDevice()}, _config.getXAxis()->getAxis()}) : std::nullopt), _yAxis(
        _config.getYAxis() && _config.getYAxis()->getDevice() > 0 ?
        std::optional<Axis>({Device{_connection, _config.getYAxis()->getDevice()}, _config.getYAxis()->getAxis()}) : std::nullopt), _plate(
        getXAxis() && getYAxis() ?
        std::optional<AxisGroup>({{getXAxis().value(), getYAxis().value()}}) : std::nullopt), _objectiveChanger(
        _config.getObjectiveChanger().value_or(0) > 0 && getFocusAxis() ?
        std::optional<ObjectiveChanger>({Device{_connection, _config.getObjectiveChanger().value()}, getFocusAxis().value()}) : std::nullopt), _filterChanger(
        _config.getFilterChanger().value_or(0) > 0 ?
        std::optional<FilterChanger>(Device{_connection, _config.getFilterChanger().value()}) : std::nullopt), _autofocus(
        this->_config.getAutofocus().value_or(0) > 0 && getFocusAxis() ?
        std::optional<Autofocus>({
            this->_config.getAutofocus().value(),
            getFocusAxis().value(),
            _objectiveChanger ? std::optional<Device>(_objectiveChanger->getTurret()) : std::nullopt
        }) : std::nullopt), _cameraTrigger(
        _config.getCameraTrigger() && _config.getCameraTrigger()->getDevice() > 0 ?
        std::optional<CameraTrigger>({Device{_connection, _config.getCameraTrigger()->getDevice()}, _config.getCameraTrigger()->getChannel()}) : std::nullopt) {
}

Microscope::Microscope(): _connection(), _config(), _illuminator(), _focusAxis(), _xAxis(), _yAxis(), _plate(), _objectiveChanger(), _filterChanger(), _autofocus(), _cameraTrigger(
        _config.getCameraTrigger() && _config.getCameraTrigger()->getDevice() > 0 ?
        std::optional<CameraTrigger>({Device{_connection, _config.getCameraTrigger()->getDevice()}, _config.getCameraTrigger()->getChannel()}) : std::nullopt) {
}

/**
 * Finds a microscope on a connection.
 * @param connection Connection on which to detect the microscope.
 * @param thirdPartyComponents Third party components of the microscope that cannot be found on the connection.
 * @return New instance of microscope.
 */
Microscope Microscope::find(const BaseConnection& connection, const std::optional<ThirdPartyComponents>& thirdPartyComponents) {

    ::zaber::motion::requests::MicroscopeFindRequest request;
    request.setInterfaceId(connection.getInterfaceId());
    request.setThirdParty(thirdPartyComponents);
    ::zaber::motion::requests::MicroscopeConfigResponse response;
    ::zaber::motion::callGateway("microscope/detect", request, &response);

    return {connection, response.getConfig()};
}

/**
 * Initializes the microscope.
 * Homes all axes, filter changer, and objective changer if they require it.
 * @param force Forces all devices to home even when not required.
 */
void Microscope::initialize(bool force) {

    ::zaber::motion::requests::MicroscopeInitRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setConfig(this->_config);
    request.setForce(force);
    ::zaber::motion::callGateway("microscope/initialize", request);
}

/**
 * Initializes the microscope.
 * Homes all axes, filter changer, and objective changer if they require it.
 * @param options A struct of type InitializeOptions. It has the following members:
 * * `force`: Forces all devices to home even when not required.
 */
void Microscope::initialize(const Microscope::InitializeOptions& options) {
    Microscope::initialize(options.force);
}

/**
 * Checks whether the microscope is initialized.
 * @return True, when the microscope is initialized. False, otherwise.
 */
bool Microscope::isInitialized() {

    ::zaber::motion::requests::MicroscopeEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setConfig(this->_config);
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("microscope/is_initialized", request, &response);

    return response.getValue();
}

/**
 * Returns a string that represents the microscope.
 * @return A string that represents the microscope.
 */
std::string Microscope::toString() const {

    ::zaber::motion::requests::MicroscopeEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setConfig(this->_config);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("microscope/to_string", request, &response);

    return response.getValue();
}

/**
 * Connection of the microscope.
 */
BaseConnection Microscope::getConnection() const {
    return this->_connection;
}

MicroscopeConfig Microscope::getConfig() const {
    return this->_config;
}

/**
 * The illuminator.
 */
std::optional<Illuminator> Microscope::getIlluminator() const {
    return this->_illuminator;
}

/**
 * The focus axis.
 */
std::optional<Axis> Microscope::getFocusAxis() const {
    return this->_focusAxis;
}

/**
 * The X axis.
 */
std::optional<Axis> Microscope::getXAxis() const {
    return this->_xAxis;
}

/**
 * The Y axis.
 */
std::optional<Axis> Microscope::getYAxis() const {
    return this->_yAxis;
}

/**
 * Axis group consisting of X and Y axes representing the plate of the microscope.
 */
std::optional<AxisGroup> Microscope::getPlate() const {
    return this->_plate;
}

/**
 * The objective changer.
 */
std::optional<ObjectiveChanger> Microscope::getObjectiveChanger() const {
    return this->_objectiveChanger;
}

/**
 * The filter changer.
 */
std::optional<FilterChanger> Microscope::getFilterChanger() const {
    return this->_filterChanger;
}

/**
 * The autofocus feature.
 */
std::optional<Autofocus> Microscope::getAutofocus() const {
    return this->_autofocus;
}

/**
 * The camera trigger.
 */
std::optional<CameraTrigger> Microscope::getCameraTrigger() const {
    return this->_cameraTrigger;
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
