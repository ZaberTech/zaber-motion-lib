﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "illuminator_channel.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/ascii/axis_settings.h"
#include "zaber/motion/ascii/storage.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/dto/ascii/response.h"
#include "zaber/motion/ascii/warnings.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using AxisSettings = zaber::motion::ascii::AxisSettings;
using AxisStorage = zaber::motion::ascii::AxisStorage;
using Warnings = zaber::motion::ascii::Warnings;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;
using Device = zaber::motion::ascii::Device;
using Response = zaber::motion::ascii::Response;
using SetStateAxisResponse = zaber::motion::ascii::SetStateAxisResponse;

namespace zaber { namespace motion { namespace microscopy {

IlluminatorChannel::IlluminatorChannel(Illuminator illuminator, int channelNumber): _illuminator(std::move(illuminator)), _channelNumber(channelNumber) {
}

IlluminatorChannel::IlluminatorChannel(): _illuminator(), _channelNumber(0) {
}

/**
 * Turns this channel on.
 */
void IlluminatorChannel::on() {

    ::zaber::motion::requests::ChannelOn request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setOn(true);
    ::zaber::motion::callGateway("illuminator/on", request);
}

/**
 * Turns this channel off.
 */
void IlluminatorChannel::off() {

    ::zaber::motion::requests::ChannelOn request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setOn(false);
    ::zaber::motion::callGateway("illuminator/on", request);
}

/**
 * Turns this channel on or off.
 * @param on True to turn channel on, false to turn it off.
 */
void IlluminatorChannel::setOn(bool on) {

    ::zaber::motion::requests::ChannelOn request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setOn(on);
    ::zaber::motion::callGateway("illuminator/on", request);
}

/**
 * Checks if this channel is on.
 * @return True if channel is on, false otherwise.
 */
bool IlluminatorChannel::isOn() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("illuminator/is_on", request, &response);

    return response.getValue();
}

/**
 * Sets channel intensity as a fraction of the maximum flux.
 * @param intensity Fraction of intensity to set (between 0 and 1).
 */
void IlluminatorChannel::setIntensity(double intensity) {

    ::zaber::motion::requests::ChannelSetIntensity request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setIntensity(intensity);
    ::zaber::motion::callGateway("illuminator/set_intensity", request);
}

/**
 * Gets the current intensity of this channel.
 * @return Current intensity as fraction of maximum flux.
 */
double IlluminatorChannel::getIntensity() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("illuminator/get_intensity", request, &response);

    return response.getValue();
}

/**
 * Sends a generic ASCII command to this channel.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response IlluminatorChannel::genericCommand(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    Response response;
    ::zaber::motion::callGateway("interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic ASCII command to this channel.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return A response to the command.
 */
Response IlluminatorChannel::genericCommand(const std::string& command, const IlluminatorChannel::GenericCommandOptions& options) {
    return IlluminatorChannel::genericCommand(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this channel and expects multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param checkErrors Controls whether to throw an exception when a device rejects the command.
 * @param timeout The timeout, in milliseconds, for a device to respond to the command.
 * Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> IlluminatorChannel::genericCommandMultiResponse(const std::string& command, bool checkErrors, int timeout) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setCommand(command);
    request.setCheckErrors(checkErrors);
    request.setTimeout(timeout);
    ::zaber::motion::requests::GenericCommandResponseCollection response;
    ::zaber::motion::callGateway("interface/generic_command_multi_response", request, &response);

    return response.getResponses();
}

/**
 * Sends a generic ASCII command to this channel and expects multiple responses.
 * Responses are returned in order of arrival.
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `checkErrors`: Controls whether to throw an exception when a device rejects the command.
 * * `timeout`: The timeout, in milliseconds, for a device to respond to the command.
 *   Overrides the connection default request timeout.
 * @return All responses to the command.
 */
std::vector<Response> IlluminatorChannel::genericCommandMultiResponse(const std::string& command, const IlluminatorChannel::GenericCommandMultiResponseOptions& options) {
    return IlluminatorChannel::genericCommandMultiResponse(command, options.checkErrors, options.timeout);
}

/**
 * Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
 * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
 * @param command Command and its parameters.
 */
void IlluminatorChannel::genericCommandNoResponse(const std::string& command) {

    ::zaber::motion::requests::GenericCommandRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setCommand(command);
    ::zaber::motion::callGateway("interface/generic_command_no_response", request);
}

/**
 * Returns a serialization of the current channel state that can be saved and reapplied.
 * @return A serialization of the current state of the channel.
 */
std::string IlluminatorChannel::getState() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/get_state", request, &response);

    return response.getValue();
}

/**
 * Applies a saved state to this channel.
 * @param state The state object to apply to this channel.
 * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
 */
SetStateAxisResponse IlluminatorChannel::setState(const std::string& state) {

    ::zaber::motion::requests::SetStateRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setState(state);
    SetStateAxisResponse response;
    ::zaber::motion::callGateway("device/set_axis_state", request, &response);

    return response;
}

/**
 * Checks if a state can be applied to this channel.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param firmwareVersion The firmware version of the device to apply the state to.
 * Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this channel.
 */
std::optional<std::string> IlluminatorChannel::canSetState(const std::string& state, const std::optional<FirmwareVersion>& firmwareVersion) {

    ::zaber::motion::requests::CanSetStateRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setState(state);
    request.setFirmwareVersion(firmwareVersion);
    ::zaber::motion::requests::CanSetStateAxisResponse response;
    ::zaber::motion::callGateway("device/can_set_axis_state", request, &response);

    return response.getError();
}

/**
 * Checks if a state can be applied to this channel.
 * This only covers exceptions that can be determined statically such as mismatches of ID or version,
 * the process of applying the state can still fail when running.
 * @param state The state object to check against.
 * @param options A struct of type CanSetStateOptions. It has the following members:
 * * `firmwareVersion`: The firmware version of the device to apply the state to.
 *   Use this to ensure the state will still be compatible after an update.
 * @return An explanation of why this state cannot be set to this channel.
 */
std::optional<std::string> IlluminatorChannel::canSetState(const std::string& state, const IlluminatorChannel::CanSetStateOptions& options) {
    return IlluminatorChannel::canSetState(state, options.firmwareVersion);
}

/**
 * Returns a string that represents the channel.
 * @return A string that represents the channel.
 */
std::string IlluminatorChannel::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getIlluminator().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getIlluminator().getDevice().getDeviceAddress());
    request.setAxis(this->getChannelNumber());
    request.setTypeOverride("Channel");
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/axis_to_string", request, &response);

    return response.getValue();
}

/**
 * Illuminator of this channel.
 */
Illuminator IlluminatorChannel::getIlluminator() const {
    return this->_illuminator;
}

/**
 * The channel number identifies the channel on the illuminator.
 */
int IlluminatorChannel::getChannelNumber() const {
    return this->_channelNumber;
}

Axis IlluminatorChannel::getAxis() const {
    return {this->_illuminator.getDevice(), this->_channelNumber};
}

/**
 * Settings and properties of this channel.
 */
AxisSettings IlluminatorChannel::getSettings() const {
    return {this->getAxis()};
}

/**
 * Key-value storage of this channel.
 */
AxisStorage IlluminatorChannel::getStorage() const {
    return {this->getAxis()};
}

/**
 * Warnings and faults of this channel.
 */
Warnings IlluminatorChannel::getWarnings() const {
    return {this->_illuminator.getDevice(), this->_channelNumber};
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
