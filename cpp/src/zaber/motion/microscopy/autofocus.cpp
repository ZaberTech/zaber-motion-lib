﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "autofocus.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `Autofocus` based on the given provider id.
 */
Autofocus::Autofocus(int providerId, Axis focusAxis, std::optional<Device> objectiveTurret): _providerId(providerId), _focusAxis(std::move(focusAxis)), _objectiveTurret(std::move(objectiveTurret)) {
}

Autofocus::Autofocus(): _providerId(), _focusAxis(), _objectiveTurret() {
}

/**
 * Sets the current focus to be target for the autofocus control loop.
 */
void Autofocus::setFocusZero() {

    ::zaber::motion::requests::EmptyAutofocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::callGateway("autofocus/set_zero", request);
}

/**
 * Returns the status of the autofocus.
 * @return The status of the autofocus.
 */
AutofocusStatus Autofocus::getStatus() {

    ::zaber::motion::requests::EmptyAutofocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::requests::AutofocusGetStatusResponse response;
    ::zaber::motion::callGateway("autofocus/get_status", request, &response);

    return response.getStatus();
}

/**
 * Moves the device until it's in focus.
 * @param scan If true, the autofocus will approach from the limit moving until it's in range.
 * @param timeout Sets autofocus timeout duration in milliseconds.
 */
void Autofocus::focusOnce(bool scan, int timeout) {

    ::zaber::motion::requests::AutofocusFocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    request.setOnce(true);
    request.setScan(scan);
    request.setTimeout(timeout);
    ::zaber::motion::callGateway("autofocus/focus_once", request);
}

/**
 * Moves the focus axis continuously maintaining focus.
 * Starts the autofocus control loop.
 * Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
 */
void Autofocus::startFocusLoop() {

    ::zaber::motion::requests::AutofocusFocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::callGateway("autofocus/start_focus_loop", request);
}

/**
 * Stops autofocus control loop.
 * If the focus axis already stopped moving because of an error, an exception will be thrown.
 */
void Autofocus::stopFocusLoop() {

    ::zaber::motion::requests::EmptyAutofocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::callGateway("autofocus/stop_focus_loop", request);
}

/**
 * Gets the lower motion limit for the autofocus control loop.
 * Gets motion.tracking.limit.min setting of the focus axis.
 * @param unit The units of the limit.
 * @return Limit value.
 */
double Autofocus::getLimitMin(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getFocusAxis().getAxisNumber());
    request.setSetting("motion.tracking.limit.min");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Gets the upper motion limit for the autofocus control loop.
 * Gets motion.tracking.limit.max setting of the focus axis.
 * @param unit The units of the limit.
 * @return Limit value.
 */
double Autofocus::getLimitMax(Units unit) {

    ::zaber::motion::requests::DeviceGetSettingRequest request;
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getFocusAxis().getAxisNumber());
    request.setSetting("motion.tracking.limit.max");
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Sets the lower motion limit for the autofocus control loop.
 * Use the limits to prevent the focus axis from crashing into the sample.
 * Changes motion.tracking.limit.min setting of the focus axis.
 * @param limit The lower limit of the focus axis.
 * @param unit The units of the limit.
 */
void Autofocus::setLimitMin(double limit, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getFocusAxis().getAxisNumber());
    request.setSetting("motion.tracking.limit.min");
    request.setValue(limit);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Sets the upper motion limit for the autofocus control loop.
 * Use the limits to prevent the focus axis from crashing into the sample.
 * Changes motion.tracking.limit.max setting of the focus axis.
 * @param limit The upper limit of the focus axis.
 * @param unit The units of the limit.
 */
void Autofocus::setLimitMax(double limit, Units unit) {

    ::zaber::motion::requests::DeviceSetSettingRequest request;
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setAxis(this->getFocusAxis().getAxisNumber());
    request.setSetting("motion.tracking.limit.max");
    request.setValue(limit);
    request.setUnit(unit);
    ::zaber::motion::callGateway("device/set_setting", request);
}

/**
 * Typically, the control loop parameters and objective are kept synchronized by the library.
 * If the parameters or current objective changes outside of the library, call this method to synchronize them.
 */
void Autofocus::synchronizeParameters() {

    ::zaber::motion::requests::EmptyAutofocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::callGateway("autofocus/sync_params", request);
}

/**
 * Sets the parameters for the autofocus objective.
 * Note that the method temporarily switches current objective to set the parameters.
 * @param objective The objective (numbered from 1) to set the parameters for.
 * If your microscope has only one objective, use value of 1.
 * @param parameters The parameters for the autofocus objective.
 */
void Autofocus::setObjectiveParameters(int objective, const std::vector<NamedParameter>& parameters) {

    ::zaber::motion::requests::AutofocusSetObjectiveParamsRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    request.setObjective(objective);
    request.setParameters(std::vector<NamedParameter>(parameters));
    ::zaber::motion::callGateway("autofocus/set_objective_params", request);
}

/**
 * Returns the parameters for the autofocus objective.
 * @param objective The objective (numbered from 1) to get the parameters for.
 * If your microscope has only one objective, use value of 1.
 * Note that the method temporarily switches current objective to get the parameters.
 * @return The parameters for the autofocus objective.
 */
std::vector<NamedParameter> Autofocus::getObjectiveParameters(int objective) {

    ::zaber::motion::requests::AutofocusGetObjectiveParamsRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    request.setObjective(objective);
    ::zaber::motion::requests::AutofocusGetObjectiveParamsResponse response;
    ::zaber::motion::callGateway("autofocus/get_objective_params", request, &response);

    return response.getParameters();
}

/**
 * Returns a string that represents the autofocus.
 * @return A string that represents the autofocus.
 */
std::string Autofocus::toString() const {

    ::zaber::motion::requests::EmptyAutofocusRequest request;
    request.setProviderId(this->getProviderId());
    request.setInterfaceId(this->getFocusAxis().getDevice().getConnection().getInterfaceId());
    request.setFocusAddress(this->getFocusAxis().getDevice().getDeviceAddress());
    request.setFocusAxis(this->getFocusAxis().getAxisNumber());
    request.setTurretAddress(this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0);
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("autofocus/to_string", request, &response);

    return response.getValue();
}

/**
 * The identification of external device providing the capability.
 */
int Autofocus::getProviderId() const {
    return this->_providerId;
}

/**
 * The focus axis.
 */
Axis Autofocus::getFocusAxis() const {
    return this->_focusAxis;
}

/**
 * The objective turret device if the microscope has one.
 */
std::optional<Device> Autofocus::getObjectiveTurret() const {
    return this->_objectiveTurret;
}


}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
