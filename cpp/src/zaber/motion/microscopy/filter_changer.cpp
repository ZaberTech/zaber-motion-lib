﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "filter_changer.h"
#include "zaber/motion/ascii/axis.h"
#include "zaber/motion/ascii/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"


using Axis = zaber::motion::ascii::Axis;
using Device = zaber::motion::ascii::Device;
using BaseConnection = zaber::motion::ascii::BaseConnection;
using Connection = zaber::motion::ascii::Connection;

namespace zaber { namespace motion { namespace microscopy {

/**
 * Creates instance of `FilterChanger` based on the given device.
 */
FilterChanger::FilterChanger(Device device): _device(std::move(device)) {
}

FilterChanger::FilterChanger(): _device() {
}

/**
 * Gets number of filters of the changer.
 * @return Number of positions.
 */
int FilterChanger::getNumberOfFilters() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(1);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_count", request, &response);

    return response.getValue();
}

/**
 * Returns the current filter number starting from 1.
 * The value of 0 indicates that the position is either unknown or between two filters.
 * @return Filter number starting from 1 or 0 if the position cannot be determined.
 */
int FilterChanger::getCurrentFilter() {

    ::zaber::motion::requests::AxisEmptyRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(1);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("device/get_index_position", request, &response);

    return response.getValue();
}

/**
 * Changes to the specified filter.
 * @param filter Filter number starting from 1.
 */
void FilterChanger::change(int filter) {

    ::zaber::motion::requests::DeviceMoveRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    request.setAxis(1);
    request.setType(::zaber::motion::requests::AxisMoveType::INDEX);
    request.setWaitUntilIdle(true);
    request.setArgInt(filter);
    ::zaber::motion::callGateway("device/move", request);
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string FilterChanger::toString() const {

    ::zaber::motion::requests::AxisToStringRequest request;
    request.setInterfaceId(this->getDevice().getConnection().getInterfaceId());
    request.setDevice(this->getDevice().getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * The base device of this turret.
 */
Device FilterChanger::getDevice() const {
    return this->_device;
}



}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
