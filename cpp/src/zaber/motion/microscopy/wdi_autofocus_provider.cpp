﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "wdi_autofocus_provider.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

namespace zaber { namespace motion { namespace microscopy {

WdiAutofocusProvider::WdiAutofocusProvider(int providerId): _providerId(providerId) {
}

/**
 * Opens a TCP connection to WDI autofocus.
 * @param hostName Hostname or IP address.
 * @param port Optional port number (defaults to 27).
 * @return An object representing the autofocus connection.
 */
WdiAutofocusProvider WdiAutofocusProvider::openTcp(const std::string& hostName, int port) {

    ::zaber::motion::requests::OpenInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::TCP);
    request.setHostName(hostName);
    request.setPort(port);
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("wdi/open", request, &response);

    return {response.getValue()};
}

/**
 * Close the connection.
 */
void WdiAutofocusProvider::close() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getProviderId());
    ::zaber::motion::callGateway("wdi/close", request);
}

/**
 * Generic read operation.
 * @param registerId Register address to read from.
 * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
 * @param count Number of values to read (defaults to 1).
 * @param offset Offset within the register (defaults to 0).
 * @param registerBank Register bank letter (defaults to U for user bank).
 * @return Array of integers read from the device.
 */
std::vector<int> WdiAutofocusProvider::genericRead(int registerId, int size, int count, int offset, const std::string& registerBank) {

    ::zaber::motion::requests::WdiGenericRequest request;
    request.setInterfaceId(this->getProviderId());
    request.setRegisterId(registerId);
    request.setSize(size);
    request.setCount(count);
    request.setOffset(offset);
    request.setRegisterBank(registerBank);
    ::zaber::motion::requests::IntArrayResponse response;
    ::zaber::motion::callGateway("wdi/read", request, &response);

    return response.getValues();
}

/**
 * Generic read operation.
 * @param registerId Register address to read from.
 * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
 * @param options A struct of type GenericReadOptions. It has the following members:
 * * `count`: Number of values to read (defaults to 1).
 * * `offset`: Offset within the register (defaults to 0).
 * * `registerBank`: Register bank letter (defaults to U for user bank).
 * @return Array of integers read from the device.
 */
std::vector<int> WdiAutofocusProvider::genericRead(int registerId, int size, const WdiAutofocusProvider::GenericReadOptions& options) {
    return WdiAutofocusProvider::genericRead(registerId, size, options.count, options.offset, options.registerBank);
}

/**
 * Generic write operation.
 * @param registerId Register address to read from.
 * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
 * @param data Array of values to write to the register. Empty array is allowed.
 * @param offset Offset within the register (defaults to 0).
 * @param registerBank Register bank letter (defaults to U for user bank).
 */
void WdiAutofocusProvider::genericWrite(int registerId, int size, const std::vector<int>& data, int offset, const std::string& registerBank) {

    ::zaber::motion::requests::WdiGenericRequest request;
    request.setInterfaceId(this->getProviderId());
    request.setRegisterId(registerId);
    request.setSize(size);
    request.setData(std::vector<int>(data));
    request.setOffset(offset);
    request.setRegisterBank(registerBank);
    ::zaber::motion::callGateway("wdi/write", request);
}

/**
 * Generic write operation.
 * @param registerId Register address to read from.
 * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
 * @param data Array of values to write to the register. Empty array is allowed.
 * @param options A struct of type GenericWriteOptions. It has the following members:
 * * `offset`: Offset within the register (defaults to 0).
 * * `registerBank`: Register bank letter (defaults to U for user bank).
 */
void WdiAutofocusProvider::genericWrite(int registerId, int size, const std::vector<int>& data, const WdiAutofocusProvider::GenericWriteOptions& options) {
    WdiAutofocusProvider::genericWrite(registerId, size, data, options.offset, options.registerBank);
}

/**
 * Enables the laser.
 */
void WdiAutofocusProvider::enableLaser() {

    ::zaber::motion::requests::WdiGenericRequest request;
    request.setInterfaceId(this->getProviderId());
    request.setRegisterId(1);
    ::zaber::motion::callGateway("wdi/write", request);
}

/**
 * Disables the laser.
 */
void WdiAutofocusProvider::disableLaser() {

    ::zaber::motion::requests::WdiGenericRequest request;
    request.setInterfaceId(this->getProviderId());
    request.setRegisterId(2);
    ::zaber::motion::callGateway("wdi/write", request);
}

/**
 * Gets the status of the autofocus.
 * @return The status of the autofocus.
 */
WdiAutofocusProviderStatus WdiAutofocusProvider::getStatus() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getProviderId());
    ::zaber::motion::requests::WdiGetStatusResponse response;
    ::zaber::motion::callGateway("wdi/get_status", request, &response);

    return response.getStatus();
}

/**
 * Returns a string that represents the autofocus connection.
 * @return A string that represents the connection.
 */
std::string WdiAutofocusProvider::toString() const {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getProviderId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("wdi/to_string", request, &response);

    return response.getValue();
}

/**
 * The ID identifies the autofocus with the underlying library.
 */
int WdiAutofocusProvider::getProviderId() const {
    return this->_providerId;
}


WdiAutofocusProvider& WdiAutofocusProvider::operator=(WdiAutofocusProvider&& other) noexcept {
  if (this != &other) {
      if (this->_providerId >= 0) {
          this->close();
          this->_providerId = -1;
      }

      std::swap(_providerId, other._providerId);
  }
  return *this;
}

WdiAutofocusProvider::WdiAutofocusProvider (WdiAutofocusProvider&& other) noexcept: _providerId(-1) {
  *this = std::move(other);
}

WdiAutofocusProvider::~WdiAutofocusProvider() {
  if (this->_providerId >= 0) {
      this->close();
  }
}

}  // namespace microscopy
}  // namespace motion
}  // namespace zaber
