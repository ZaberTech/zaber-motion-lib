// ===== THIS FILE IS GENERATED FROM ZABER DEVICE DB ===== //
// ================= DO NOT EDIT DIRECTLY ================ //

#include "units.private.h"

#include <array>
#include <string>

namespace zaber { namespace motion {

    constexpr unsigned int unitLongNameLUTLength = 95;
    constexpr std::array<const char*, unitLongNameLUTLength> unitLongNameLUT = {{
        // Native Device Units
        "",
    
        // Dimension: Length, unit: metres
        "Length:metres",
    
        // Dimension: Length, unit: centimetres
        "Length:centimetres",
    
        // Dimension: Length, unit: millimetres
        "Length:millimetres",
    
        // Dimension: Length, unit: micrometres
        "Length:micrometres",
    
        // Dimension: Length, unit: nanometres
        "Length:nanometres",
    
        // Dimension: Length, unit: inches
        "Length:inches",
    
        // Dimension: Velocity, unit: metres per second
        "Velocity:metres per second",
    
        // Dimension: Velocity, unit: centimetres per second
        "Velocity:centimetres per second",
    
        // Dimension: Velocity, unit: millimetres per second
        "Velocity:millimetres per second",
    
        // Dimension: Velocity, unit: micrometres per second
        "Velocity:micrometres per second",
    
        // Dimension: Velocity, unit: nanometres per second
        "Velocity:nanometres per second",
    
        // Dimension: Velocity, unit: inches per second
        "Velocity:inches per second",
    
        // Dimension: Acceleration, unit: metres per second squared
        "Acceleration:metres per second squared",
    
        // Dimension: Acceleration, unit: centimetres per second squared
        "Acceleration:centimetres per second squared",
    
        // Dimension: Acceleration, unit: millimetres per second squared
        "Acceleration:millimetres per second squared",
    
        // Dimension: Acceleration, unit: micrometres per second squared
        "Acceleration:micrometres per second squared",
    
        // Dimension: Acceleration, unit: nanometres per second squared
        "Acceleration:nanometres per second squared",
    
        // Dimension: Acceleration, unit: inches per second squared
        "Acceleration:inches per second squared",
    
        // Dimension: Angle, unit: degrees
        "Angle:degrees",
    
        // Dimension: Angle, unit: radians
        "Angle:radians",
    
        // Dimension: Angular Velocity, unit: degrees per second
        "Angular Velocity:degrees per second",
    
        // Dimension: Angular Velocity, unit: radians per second
        "Angular Velocity:radians per second",
    
        // Dimension: Angular Acceleration, unit: degrees per second squared
        "Angular Acceleration:degrees per second squared",
    
        // Dimension: Angular Acceleration, unit: radians per second squared
        "Angular Acceleration:radians per second squared",
    
        // Dimension: AC Electric Current, unit: amperes peak
        "AC Electric Current:amperes peak",
    
        // Dimension: AC Electric Current, unit: amperes RMS
        "AC Electric Current:amperes RMS",
    
        // Dimension: Percent, unit: percent
        "Percent:percent",
    
        // Dimension: DC Electric Current, unit: amperes
        "DC Electric Current:amperes",
    
        // Dimension: Force, unit: newtons
        "Force:newtons",
    
        // Dimension: Force, unit: millinewtons
        "Force:millinewtons",
    
        // Dimension: Force, unit: pounds-force
        "Force:pounds-force",
    
        // Dimension: Force, unit: kilonewtons
        "Force:kilonewtons",
    
        // Dimension: Time, unit: seconds
        "Time:seconds",
    
        // Dimension: Time, unit: milliseconds
        "Time:milliseconds",
    
        // Dimension: Time, unit: microseconds
        "Time:microseconds",
    
        // Dimension: Torque, unit: newton metres
        "Torque:newton metres",
    
        // Dimension: Torque, unit: newton centimetres
        "Torque:newton centimetres",
    
        // Dimension: Torque, unit: pound-force-feet
        "Torque:pound-force-feet",
    
        // Dimension: Torque, unit: ounce-force-inches
        "Torque:ounce-force-inches",
    
        // Dimension: Inertia, unit: grams
        "Inertia:grams",
    
        // Dimension: Inertia, unit: kilograms
        "Inertia:kilograms",
    
        // Dimension: Inertia, unit: milligrams
        "Inertia:milligrams",
    
        // Dimension: Inertia, unit: pounds
        "Inertia:pounds",
    
        // Dimension: Inertia, unit: ounces
        "Inertia:ounces",
    
        // Dimension: Rotational Inertia, unit: gram-square metre
        "Rotational Inertia:gram-square metre",
    
        // Dimension: Rotational Inertia, unit: kilogram-square metre
        "Rotational Inertia:kilogram-square metre",
    
        // Dimension: Rotational Inertia, unit: pound-square-feet
        "Rotational Inertia:pound-square-feet",
    
        // Dimension: Force Constant, unit: newtons per amp
        "Force Constant:newtons per amp",
    
        // Dimension: Force Constant, unit: millinewtons per amp
        "Force Constant:millinewtons per amp",
    
        // Dimension: Force Constant, unit: kilonewtons per amp
        "Force Constant:kilonewtons per amp",
    
        // Dimension: Force Constant, unit: pounds-force per amp
        "Force Constant:pounds-force per amp",
    
        // Dimension: Torque Constant, unit: newton metres per amp
        "Torque Constant:newton metres per amp",
    
        // Dimension: Torque Constant, unit: millinewton metres per amp
        "Torque Constant:millinewton metres per amp",
    
        // Dimension: Torque Constant, unit: kilonewton metres per amp
        "Torque Constant:kilonewton metres per amp",
    
        // Dimension: Torque Constant, unit: pound-force-feet per amp
        "Torque Constant:pound-force-feet per amp",
    
        // Dimension: Voltage, unit: volts
        "Voltage:volts",
    
        // Dimension: Voltage, unit: millivolts
        "Voltage:millivolts",
    
        // Dimension: Voltage, unit: microvolts
        "Voltage:microvolts",
    
        // Dimension: Current Controller Proportional Gain, unit: volts per amp
        "Current Controller Proportional Gain:volts per amp",
    
        // Dimension: Current Controller Proportional Gain, unit: millivolts per amp
        "Current Controller Proportional Gain:millivolts per amp",
    
        // Dimension: Current Controller Proportional Gain, unit: microvolts per amp
        "Current Controller Proportional Gain:microvolts per amp",
    
        // Dimension: Current Controller Integral Gain, unit: volts per amp per second
        "Current Controller Integral Gain:volts per amp per second",
    
        // Dimension: Current Controller Integral Gain, unit: millivolts per amp per second
        "Current Controller Integral Gain:millivolts per amp per second",
    
        // Dimension: Current Controller Integral Gain, unit: microvolts per amp per second
        "Current Controller Integral Gain:microvolts per amp per second",
    
        // Dimension: Current Controller Derivative Gain, unit: volts second per amp
        "Current Controller Derivative Gain:volts second per amp",
    
        // Dimension: Current Controller Derivative Gain, unit: millivolts second per amp
        "Current Controller Derivative Gain:millivolts second per amp",
    
        // Dimension: Current Controller Derivative Gain, unit: microvolts second per amp
        "Current Controller Derivative Gain:microvolts second per amp",
    
        // Dimension: Resistance, unit: kiloohms
        "Resistance:kiloohms",
    
        // Dimension: Resistance, unit: ohms
        "Resistance:ohms",
    
        // Dimension: Resistance, unit: milliohms
        "Resistance:milliohms",
    
        // Dimension: Resistance, unit: microohms
        "Resistance:microohms",
    
        // Dimension: Resistance, unit: nanoohms
        "Resistance:nanoohms",
    
        // Dimension: Inductance, unit: henries
        "Inductance:henries",
    
        // Dimension: Inductance, unit: millihenries
        "Inductance:millihenries",
    
        // Dimension: Inductance, unit: microhenries
        "Inductance:microhenries",
    
        // Dimension: Inductance, unit: nanohenries
        "Inductance:nanohenries",
    
        // Dimension: Voltage Constant, unit: volt seconds per radian
        "Voltage Constant:volt seconds per radian",
    
        // Dimension: Voltage Constant, unit: millivolt seconds per radian
        "Voltage Constant:millivolt seconds per radian",
    
        // Dimension: Voltage Constant, unit: microvolt seconds per radian
        "Voltage Constant:microvolt seconds per radian",
    
        // Dimension: Absolute Temperature, unit: degrees Celsius
        "Absolute Temperature:degrees Celsius",
    
        // Dimension: Absolute Temperature, unit: kelvins
        "Absolute Temperature:kelvins",
    
        // Dimension: Absolute Temperature, unit: degrees Fahrenheit
        "Absolute Temperature:degrees Fahrenheit",
    
        // Dimension: Absolute Temperature, unit: degrees Rankine
        "Absolute Temperature:degrees Rankine",
    
        // Dimension: Relative Temperature, unit: degrees Celsius
        "Relative Temperature:degrees Celsius",
    
        // Dimension: Relative Temperature, unit: kelvins
        "Relative Temperature:kelvins",
    
        // Dimension: Relative Temperature, unit: degrees Fahrenheit
        "Relative Temperature:degrees Fahrenheit",
    
        // Dimension: Relative Temperature, unit: degrees Rankine
        "Relative Temperature:degrees Rankine",
    
        // Dimension: Frequency, unit: gigahertz
        "Frequency:gigahertz",
    
        // Dimension: Frequency, unit: megahertz
        "Frequency:megahertz",
    
        // Dimension: Frequency, unit: kilohertz
        "Frequency:kilohertz",
    
        // Dimension: Frequency, unit: hertz
        "Frequency:hertz",
    
        // Dimension: Frequency, unit: millihertz
        "Frequency:millihertz",
    
        // Dimension: Frequency, unit: microhertz
        "Frequency:microhertz",
    
        // Dimension: Frequency, unit: nanohertz
        "Frequency:nanohertz",
        }};


    std::string getUnitLongName(Units unit) {
        return unitLongNameLUT.at(static_cast<unsigned int>(unit));
    }

    Units getLongNameUnit(std::string name) {
        for (unsigned int i = 0; i < unitLongNameLUTLength; i++) {
            const char* unitName = unitLongNameLUT.at(i);
            if (name == unitName) {
                return static_cast<Units>(i);
            }
        }
        return Units::NATIVE;
    }

}  // namespace motion
}  // namespace zaber
