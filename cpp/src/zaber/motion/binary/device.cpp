﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/binary/device.h"
#include "zaber/motion/dto/binary/binary_settings.h"
#include "zaber/motion/dto/binary/command_code.h"
#include "zaber/motion/dto/binary/device_identity.h"
#include "zaber/motion/binary/device_settings.h"
#include "zaber/motion/dto/binary/device_type.h"
#include "zaber/motion/dto/binary/message.h"
#include "zaber/motion/dto/firmware_version.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <cstdint>


namespace zaber { namespace motion { namespace binary {

Device::Device(BaseConnection connection, int deviceAddress): _connection(std::move(connection)), _deviceAddress(deviceAddress) {
}

Device::Device(): _connection(), _deviceAddress(-1) {
}

/**
 * Sends a generic Binary command to this device.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @return A response to the command.
 */
Message Device::genericCommand(CommandCode command, int data, double timeout, bool checkErrors) {

    ::zaber::motion::requests::GenericBinaryRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setData(data);
    request.setTimeout(timeout);
    request.setCheckErrors(checkErrors);
    Message response;
    ::zaber::motion::callGateway("binary/interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic Binary command to this device.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * @return A response to the command.
 */
Message Device::genericCommand(CommandCode command, int data, const Device::GenericCommandOptions& options) {
    return Device::genericCommand(command, data, options.timeout, options.checkErrors);
}

/**
 * Sends a generic Binary command to this device without expecting a response.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 */
void Device::genericCommandNoResponse(CommandCode command, int data) {

    ::zaber::motion::requests::GenericBinaryRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setData(data);
    ::zaber::motion::callGateway("binary/interface/generic_command_no_response", request);
}

/**
 * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
 * @param command Command to send.
 * @param data Data argument to the command. Defaults to zero.
 * @param fromUnit Unit to convert sent data from.
 * @param toUnit Unit to convert retrieved data to.
 * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * @return Data that has been converted to the provided unit.
 */
double Device::genericCommandWithUnits(CommandCode command, double data, Units fromUnit, Units toUnit, double timeout) {

    ::zaber::motion::requests::BinaryGenericWithUnitsRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setCommand(command);
    request.setData(data);
    request.setFromUnit(fromUnit);
    request.setToUnit(toUnit);
    request.setTimeout(timeout);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/generic_command_with_units", request, &response);

    return response.getValue();
}

/**
 * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
 * @param command Command to send.
 * @param data Data argument to the command. Defaults to zero.
 * @param fromUnit Unit to convert sent data from.
 * @param toUnit Unit to convert retrieved data to.
 * @param options A struct of type GenericCommandWithUnitsOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * @return Data that has been converted to the provided unit.
 */
double Device::genericCommandWithUnits(CommandCode command, double data, Units fromUnit, Units toUnit, const Device::GenericCommandWithUnitsOptions& options) {
    return Device::genericCommandWithUnits(command, data, fromUnit, toUnit, options.timeout);
}

/**
 * Homes device. Device returns to its homing position.
 * @param unit Unit to convert returned position to.
 * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::home(Units unit, double timeout) {

    ::zaber::motion::requests::BinaryDeviceHomeRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setUnit(unit);
    request.setTimeout(timeout);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/home", request, &response);

    return response.getValue();
}

/**
 * Homes device. Device returns to its homing position.
 * @param options A struct of type HomeOptions. It has the following members:
 * * `unit`: Unit to convert returned position to.
 * * `timeout`: Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::home(const Device::HomeOptions& options) {
    return Device::home(options.unit, options.timeout);
}

/**
 * Stops ongoing device movement. Decelerates until zero speed.
 * @param unit Unit to convert returned position to.
 * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::stop(Units unit, double timeout) {

    ::zaber::motion::requests::BinaryDeviceStopRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setUnit(unit);
    request.setTimeout(timeout);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/stop", request, &response);

    return response.getValue();
}

/**
 * Stops ongoing device movement. Decelerates until zero speed.
 * @param options A struct of type StopOptions. It has the following members:
 * * `unit`: Unit to convert returned position to.
 * * `timeout`: Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::stop(const Device::StopOptions& options) {
    return Device::stop(options.unit, options.timeout);
}

/**
 * Move device to absolute position.
 * @param position Absolute position.
 * @param unit Unit for the provided position as well as position returned by the device.
 * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::moveAbsolute(double position, Units unit, double timeout) {

    ::zaber::motion::requests::BinaryDeviceMoveRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setType(::zaber::motion::requests::AxisMoveType::ABS);
    request.setArg(position);
    request.setUnit(unit);
    request.setTimeout(timeout);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/move", request, &response);

    return response.getValue();
}

/**
 * Move device to absolute position.
 * @param position Absolute position.
 * @param unit Unit for the provided position as well as position returned by the device.
 * @param options A struct of type MoveAbsoluteOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::moveAbsolute(double position, Units unit, const Device::MoveAbsoluteOptions& options) {
    return Device::moveAbsolute(position, unit, options.timeout);
}

/**
 * Move device to position relative to current position.
 * @param position Relative position.
 * @param unit Unit for the provided position as well as position returned by the device.
 * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::moveRelative(double position, Units unit, double timeout) {

    ::zaber::motion::requests::BinaryDeviceMoveRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setType(::zaber::motion::requests::AxisMoveType::REL);
    request.setArg(position);
    request.setUnit(unit);
    request.setTimeout(timeout);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/move", request, &response);

    return response.getValue();
}

/**
 * Move device to position relative to current position.
 * @param position Relative position.
 * @param unit Unit for the provided position as well as position returned by the device.
 * @param options A struct of type MoveRelativeOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for response from the device chain (defaults to 60s).
 * @return Current position that has been converted to the provided unit.
 */
double Device::moveRelative(double position, Units unit, const Device::MoveRelativeOptions& options) {
    return Device::moveRelative(position, unit, options.timeout);
}

/**
 * Begins to move device at specified speed.
 * @param velocity Movement velocity.
 * @param unit Unit to convert returned velocity to.
 * @return Device velocity that has been converted to the provided unit.
 */
double Device::moveVelocity(double velocity, Units unit) {

    ::zaber::motion::requests::BinaryDeviceMoveRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setType(::zaber::motion::requests::AxisMoveType::VEL);
    request.setArg(velocity);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/move", request, &response);

    return response.getValue();
}

/**
 * Waits until device stops moving.
 */
void Device::waitUntilIdle() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::callGateway("binary/device/wait_until_idle", request);
}

/**
 * Check whether the device is moving.
 * @return True if the device is currently executing a motion command.
 */
bool Device::isBusy() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("binary/device/is_busy", request, &response);

    return response.getValue();
}

/**
 * Queries the device and the database, gathering information about the product.
 * Without this information features such as unit conversions will not work.
 * Usually, called automatically by detect devices method.
 * @param assumeVersion The identification assumes the specified firmware version
 * instead of the version queried from the device.
 * Providing this argument can lead to unexpected compatibility issues.
 * @return Device identification data.
 */
DeviceIdentity Device::identify(const std::optional<FirmwareVersion>& assumeVersion) {

    ::zaber::motion::requests::DeviceIdentifyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setAssumeVersion(assumeVersion);
    DeviceIdentity response;
    ::zaber::motion::callGateway("binary/device/identify", request, &response);

    return response;
}

/**
 * Queries the device and the database, gathering information about the product.
 * Without this information features such as unit conversions will not work.
 * Usually, called automatically by detect devices method.
 * @param options A struct of type IdentifyOptions. It has the following members:
 * * `assumeVersion`: The identification assumes the specified firmware version
 *   instead of the version queried from the device.
 *   Providing this argument can lead to unexpected compatibility issues.
 * @return Device identification data.
 */
DeviceIdentity Device::identify(const Device::IdentifyOptions& options) {
    return Device::identify(options.assumeVersion);
}

/**
 * Parks the axis.
 * Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
 * It can later be unparked and moved without first having to home it.
 * Requires at least Firmware 6.06.
 */
void Device::park() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::callGateway("binary/device/park", request);
}

/**
 * Unparks axis. Axis will now be able to move.
 * Requires at least Firmware 6.06.
 */
void Device::unpark() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::callGateway("binary/device/unpark", request);
}

/**
 * Returns bool indicating whether the axis is parked or not.
 * Requires at least Firmware 6.06.
 * @return True if the axis is currently parked. False otherwise.
 */
bool Device::isParked() {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("binary/device/is_parked", request, &response);

    return response.getValue();
}

/**
 * Returns current device position.
 * @param unit Units of position.
 * @return Axis position.
 */
double Device::getPosition(Units unit) {

    ::zaber::motion::requests::BinaryDeviceGetSettingRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    request.setSetting(BinarySettings::CURRENT_POSITION);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Returns a string that represents the device.
 * @return A string that represents the device.
 */
std::string Device::toString() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("binary/device/device_to_string", request, &response);

    return response.getValue();
}

/**
 * Returns identity.
 * @return Device identity.
 */
DeviceIdentity Device::retrieveIdentity() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    DeviceIdentity response;
    ::zaber::motion::callGateway("binary/device/get_identity", request, &response);

    return response;
}

/**
 * Returns whether or not the device have been identified.
 * @return True if the device has already been identified. False otherwise.
 */
bool Device::retrieveIsIdentified() const {

    ::zaber::motion::requests::DeviceEmptyRequest request;
    request.setInterfaceId(this->getConnection().getInterfaceId());
    request.setDevice(this->getDeviceAddress());
    ::zaber::motion::requests::BoolResponse response;
    ::zaber::motion::callGateway("binary/device/get_is_identified", request, &response);

    return response.getValue();
}

/**
 * Connection of this device.
 */
BaseConnection Device::getConnection() const {
    return this->_connection;
}

/**
 * Settings and properties of this axis.
 */
DeviceSettings Device::getSettings() const {
    return {*this};
}

/**
 * The device address uniquely identifies the device on the connection.
 * It can be configured or automatically assigned by the renumber command.
 */
int Device::getDeviceAddress() const {
    return this->_deviceAddress;
}

/**
 * Identity of the device.
 */
DeviceIdentity Device::getIdentity() const {
    return this->retrieveIdentity();
}

/**
 * Indicates whether or not the device has been identified.
 */
bool Device::getIsIdentified() const {
    return this->retrieveIsIdentified();
}

/**
 * Unique ID of the device hardware.
 */
int Device::getDeviceId() const {
    return this->getIdentity().getDeviceId();
}

/**
 * Serial number of the device.
 * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
 */
unsigned int Device::getSerialNumber() const {
    return this->getIdentity().getSerialNumber();
}

/**
 * Name of the product.
 */
std::string Device::getName() const {
    return this->getIdentity().getName();
}

/**
 * Version of the firmware.
 */
FirmwareVersion Device::getFirmwareVersion() const {
    return this->getIdentity().getFirmwareVersion();
}

/**
 * Indicates whether the device is a peripheral or part of an integrated device.
 */
bool Device::getIsPeripheral() const {
    return this->getIdentity().getIsPeripheral();
}

/**
 * Unique ID of the peripheral hardware.
 */
int Device::getPeripheralId() const {
    return this->getIdentity().getPeripheralId();
}

/**
 * Name of the peripheral hardware.
 */
std::string Device::getPeripheralName() const {
    return this->getIdentity().getPeripheralName();
}

/**
 * Determines the type of an device and units it accepts.
 */
DeviceType Device::getDeviceType() const {
    return this->getIdentity().getDeviceType();
}

}  // namespace binary
}  // namespace motion
}  // namespace zaber
