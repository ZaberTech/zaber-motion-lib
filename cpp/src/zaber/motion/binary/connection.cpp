﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

// NOLINTBEGIN(bugprone-exception-escape)

#include <cstdint>

#include "zaber/motion/binary/connection.h"
#include "zaber/motion/binary/device.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/gateway/event_manager.h"
#include "zaber/motion/utils/functional_utils.h"
#include "zaber/motion/dto/binary/command_code.h"
#include "zaber/motion/dto/binary/message.h"
#include "zaber/motion/exceptions/convert_exception.h"
#include "zaber/motion/exceptions/invalid_argument_exception.h"


namespace zaber { namespace motion { namespace binary {

Connection::Connection(int interfaceId): BaseConnection(interfaceId) { }

/**
 * Opens a serial port.
 * @param portName Name of the port to open.
 * @param baudRate Optional baud rate (defaults to 9600).
 * @param useMessageIds Enable use of message IDs (defaults to disabled).
 * All your devices must be pre-configured to match.
 * @return An object representing the port.
 */
Connection Connection::openSerialPort(const std::string& portName, int baudRate, bool useMessageIds) {

    ::zaber::motion::requests::OpenBinaryInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::SERIAL_PORT);
    request.setPortName(portName);
    request.setBaudRate(baudRate);
    request.setUseMessageIds(useMessageIds);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("binary/interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a serial port.
 * @param portName Name of the port to open.
 * @param options A struct of type OpenSerialPortOptions. It has the following members:
 * * `baudRate`: Optional baud rate (defaults to 9600).
 * * `useMessageIds`: Enable use of message IDs (defaults to disabled).
 *   All your devices must be pre-configured to match.
 * @return An object representing the port.
 */
Connection Connection::openSerialPort(const std::string& portName, const Connection::OpenSerialPortOptions& options) {
    return Connection::openSerialPort(portName, options.baudRate, options.useMessageIds);
}

/**
 * Opens a TCP connection.
 * @param hostName Hostname or IP address.
 * @param port Port number.
 * @param useMessageIds Enable use of message IDs (defaults to disabled).
 * All your devices must be pre-configured to match.
 * @return An object representing the connection.
 */
Connection Connection::openTcp(const std::string& hostName, int port, bool useMessageIds) {

    ::zaber::motion::requests::OpenBinaryInterfaceRequest request;
    request.setInterfaceType(::zaber::motion::requests::InterfaceType::TCP);
    request.setHostName(hostName);
    request.setPort(port);
    request.setUseMessageIds(useMessageIds);
    ::zaber::motion::requests::OpenInterfaceResponse response;
    ::zaber::motion::callGateway("binary/interface/open", request, &response);

    return {response.getInterfaceId()};
}

/**
 * Opens a TCP connection.
 * @param hostName Hostname or IP address.
 * @param port Port number.
 * @param options A struct of type OpenTcpOptions. It has the following members:
 * * `useMessageIds`: Enable use of message IDs (defaults to disabled).
 *   All your devices must be pre-configured to match.
 * @return An object representing the connection.
 */
Connection Connection::openTcp(const std::string& hostName, int port, const Connection::OpenTcpOptions& options) {
    return Connection::openTcp(hostName, port, options.useMessageIds);
}

/**
 * Close the connection.
 */
void Connection::close() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::callGateway("interface/close", request);
}

Connection& Connection::operator=(Connection&& other) noexcept {
    if (this != &other) {
        if (_interfaceId >= 0) {
            close();
            _interfaceId = -1;
        }

        resetEventHandlers();
        std::swap(_interfaceId, other._interfaceId);
        std::swap(_unknownResponseHandler, other._unknownResponseHandler);
        std::swap(_replyOnlyEventHandler, other._replyOnlyEventHandler);
        std::swap(_disconnectedHandler, other._disconnectedHandler);
    }
    return *this;
}

Connection::Connection(Connection&& other) noexcept: BaseConnection() {
    *this = std::move(other);
}

Connection::~Connection() {
    if (_interfaceId >= 0) {
        close();
    }
    resetEventHandlers();
}

void Connection::setUnknownResponseCallback(std::function<void(const UnknownResponseEvent&)> callback) {
    if (_unknownResponseHandler) {
        _unknownResponseHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::UNKNOWN_BINARY_RESPONSE;
        };
        static auto mutator = [](const SdkEvent& event) {
            return std::static_pointer_cast<requests::UnknownBinaryResponseEventWrapper>(event.getEventData())->getUnknownResponse();
        };
        _unknownResponseHandler = EventManager::getSingleton().createEventHandler<UnknownResponseEvent>(callback, filter, mutator);
    }
}

void Connection::setReplyOnlyCallback(std::function<void(const ReplyOnlyEvent&)> callback) {
    if (_replyOnlyEventHandler) {
        _replyOnlyEventHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::BINARY_REPLY_ONLY;
        };
        static auto mutator = [](const SdkEvent& event) {
            return std::static_pointer_cast<requests::BinaryReplyOnlyEventWrapper>(event.getEventData())->getReply();
        };
        _replyOnlyEventHandler = EventManager::getSingleton().createEventHandler<ReplyOnlyEvent>(callback, filter, mutator);
    }
}

void Connection::setDisconnectedCallback(std::function<void(const std::shared_ptr<exceptions::MotionLibException>&)> callback) {
    if (_disconnectedHandler) {
        _disconnectedHandler->setCallback(std::move(callback));
    } else {
        int interfaceId = _interfaceId;
        auto filter = [=](const SdkEvent& event) {
            return event.getInterfaceId() == interfaceId && event.getEventType() == SdkEvent::EventType::DISCONNECTED;
        };
        static auto mutator = [](const SdkEvent& event) {
            std::shared_ptr<requests::DisconnectedEvent> e = std::static_pointer_cast<requests::DisconnectedEvent>(event.getEventData());
            return exceptions::convertExceptionSP(e->getErrorType(), e->getErrorMessage());
        };
        _disconnectedHandler = EventManager::getSingleton()
            .createEventHandler<std::shared_ptr<exceptions::MotionLibException>>(callback, filter, mutator);
    }
}

void Connection::resetEventHandlers() {
    if (_unknownResponseHandler) {
        _unknownResponseHandler->setCallback(nullptr);
        _unknownResponseHandler = nullptr;
    }
    if (_replyOnlyEventHandler) {
        _replyOnlyEventHandler->setCallback(nullptr);
        _replyOnlyEventHandler = nullptr;
    }
    if (_disconnectedHandler) {
        _disconnectedHandler->setCallback(nullptr);
        _disconnectedHandler = nullptr;
    }
}

/**
 * Creates an instance of BaseConnection.
 */
BaseConnection::BaseConnection(int interfaceId): _interfaceId(interfaceId) {
}

/**
 * Sends a generic Binary command to this connection.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param device Device address to send the command to. Use zero for broadcast.
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * @param checkErrors Controls whether to throw an exception when the device rejects the command.
 * @return A response to the command.
 */
Message BaseConnection::genericCommand(int device, CommandCode command, int data, double timeout, bool checkErrors) {

    ::zaber::motion::requests::GenericBinaryRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setDevice(device);
    request.setCommand(command);
    request.setData(data);
    request.setTimeout(timeout);
    request.setCheckErrors(checkErrors);
    Message response;
    ::zaber::motion::callGateway("binary/interface/generic_command", request, &response);

    return response;
}

/**
 * Sends a generic Binary command to this connection.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param device Device address to send the command to. Use zero for broadcast.
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param options A struct of type GenericCommandOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
 * * `checkErrors`: Controls whether to throw an exception when the device rejects the command.
 * @return A response to the command.
 */
Message BaseConnection::genericCommand(int device, CommandCode command, int data, const BaseConnection::GenericCommandOptions& options) {
    return BaseConnection::genericCommand(device, command, data, options.timeout, options.checkErrors);
}

/**
 * Sends a generic Binary command to this connection without expecting a response.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param device Device address to send the command to. Use zero for broadcast.
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 */
void BaseConnection::genericCommandNoResponse(int device, CommandCode command, int data) {

    ::zaber::motion::requests::GenericBinaryRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setDevice(device);
    request.setCommand(command);
    request.setData(data);
    ::zaber::motion::callGateway("binary/interface/generic_command_no_response", request);
}

/**
 * Sends a generic Binary command to this connection and expects responses from one or more devices.
 * Responses are returned in order of arrival.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param timeout Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
 * @param checkErrors Controls whether to throw an exception when any device rejects the command.
 * @return All responses to the command.
 */
std::vector<Message> BaseConnection::genericCommandMultiResponse(CommandCode command, int data, double timeout, bool checkErrors) {

    ::zaber::motion::requests::GenericBinaryRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setCommand(command);
    request.setData(data);
    request.setTimeout(timeout);
    request.setCheckErrors(checkErrors);
    ::zaber::motion::requests::BinaryMessageCollection response;
    ::zaber::motion::callGateway("binary/interface/generic_command_multi_response", request, &response);

    return response.getMessages();
}

/**
 * Sends a generic Binary command to this connection and expects responses from one or more devices.
 * Responses are returned in order of arrival.
 * For more information please refer to the
 * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
 * @param command Command to send.
 * @param data Optional data argument to the command. Defaults to zero.
 * @param options A struct of type GenericCommandMultiResponseOptions. It has the following members:
 * * `timeout`: Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
 * * `checkErrors`: Controls whether to throw an exception when any device rejects the command.
 * @return All responses to the command.
 */
std::vector<Message> BaseConnection::genericCommandMultiResponse(CommandCode command, int data, const BaseConnection::GenericCommandMultiResponseOptions& options) {
    return BaseConnection::genericCommandMultiResponse(command, data, options.timeout, options.checkErrors);
}

/**
 * Renumbers devices present on this connection. After renumbering, you must identify devices again.
 * @return Total number of devices that responded to the renumber.
 */
int BaseConnection::renumberDevices() {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::requests::IntResponse response;
    ::zaber::motion::callGateway("binary/device/renumber", request, &response);

    return response.getValue();
}

/**
 * Attempts to detect any devices present on this connection.
 * @param identifyDevices Determines whether device identification should be performed as well.
 * @return Array of detected devices.
 */
std::vector<Device> BaseConnection::detectDevices(bool identifyDevices) {

    ::zaber::motion::requests::BinaryDeviceDetectRequest request;
    request.setInterfaceId(this->getInterfaceId());
    request.setIdentifyDevices(identifyDevices);
    ::zaber::motion::requests::BinaryDeviceDetectResponse response;
    ::zaber::motion::callGateway("binary/device/detect", request, &response);

    return zml_util::map_vec<int,Device>(response.getDevices(), [this](const int& i) { return Device(*this, i); });
}

/**
 * Attempts to detect any devices present on this connection.
 * @param options A struct of type DetectDevicesOptions. It has the following members:
 * * `identifyDevices`: Determines whether device identification should be performed as well.
 * @return Array of detected devices.
 */
std::vector<Device> BaseConnection::detectDevices(const BaseConnection::DetectDevicesOptions& options) {
    return BaseConnection::detectDevices(options.identifyDevices);
}

/**
 * Gets a Device class instance which allows you to control a particular device on this connection.
 * Devices are numbered from 1.
 * @param deviceAddress Address of device intended to control. Address is configured for each device.
 * @return Device instance.
 */
Device BaseConnection::getDevice(int deviceAddress) {
    if (deviceAddress <= 0) {
        throw exceptions::InvalidArgumentException("Invalid value; physical devices are numbered from 1.");
    }

    return { *this, deviceAddress };
}

/**
 * Returns a string that represents the connection.
 * @return A string that represents the connection.
 */
std::string BaseConnection::toString() const {

    ::zaber::motion::requests::InterfaceEmptyRequest request;
    request.setInterfaceId(this->getInterfaceId());
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("interface/to_string", request, &response);

    return response.getValue();
}

/**
 * The interface ID identifies thisConnection instance with the underlying library.
 */
int BaseConnection::getInterfaceId() const {
    return this->_interfaceId;
}


}  // namespace binary
}  // namespace motion
}  // namespace zaber

// NOLINTEND(bugprone-exception-escape)
