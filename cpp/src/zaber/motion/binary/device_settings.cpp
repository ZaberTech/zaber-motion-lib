﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/dto/binary/binary_settings.h"
#include "zaber/motion/binary/device_settings.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <cstdint>


namespace zaber { namespace motion { namespace binary {

DeviceSettings::DeviceSettings(Device device): _device(std::move(device)) {
}

/**
 * Returns any device setting or property.
 * @param setting Setting to get.
 * @param unit Units of setting.
 * @return Setting value.
 */
double DeviceSettings::get(BinarySettings setting, Units unit) {

    ::zaber::motion::requests::BinaryDeviceGetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setUnit(unit);
    ::zaber::motion::requests::DoubleResponse response;
    ::zaber::motion::callGateway("binary/device/get_setting", request, &response);

    return response.getValue();
}

/**
 * Sets any device setting.
 * @param setting Setting to set.
 * @param value Value of the setting.
 * @param unit Units of setting.
 */
void DeviceSettings::set(BinarySettings setting, double value, Units unit) {

    ::zaber::motion::requests::BinaryDeviceSetSettingRequest request;
    request.setInterfaceId(this->_device.getConnection().getInterfaceId());
    request.setDevice(this->_device.getDeviceAddress());
    request.setSetting(setting);
    request.setValue(value);
    request.setUnit(unit);
    ::zaber::motion::callGateway("binary/device/set_setting", request);
}

Device DeviceSettings::getDevice() const {
    return this->_device;
}

}  // namespace binary
}  // namespace motion
}  // namespace zaber
