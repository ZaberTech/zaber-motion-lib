#pragma once

#define ZML_VERSION_MAJOR 7
#define ZML_VERSION_MINOR 5
#define ZML_VERSION_PATCH 0
#define ZML_VERSION_STR "7.5.0"
