﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/tools.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <algorithm>


namespace zaber { namespace motion {

/**
 * Lists all serial ports on the computer.
 * @return Array of serial port names.
 */
std::vector<std::string> Tools::listSerialPorts() {

    ::zaber::motion::requests::EmptyRequest request;
    ::zaber::motion::requests::ToolsListSerialPortsResponse response;
    ::zaber::motion::callGateway("tools/list_serial_ports", request, &response);

    return response.getPorts();
}

/**
 * Returns path of message router named pipe on Windows
 * or file path of unix domain socket on UNIX.
 * @return Path of message router's named pipe or unix domain socket.
 */
std::string Tools::getMessageRouterPipePath() {

    ::zaber::motion::requests::EmptyRequest request;
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("tools/get_message_router_pipe", request, &response);

    return response.getValue();
}

/**
 * Returns the path for communicating with a local device database service.
 * This will be a named pipe on Windows and the file path of a unix domain socket on UNIX.
 * @return Path of database service's named pipe or unix domain socket.
 */
std::string Tools::getDbServicePipePath() {

    ::zaber::motion::requests::EmptyRequest request;
    ::zaber::motion::requests::StringResponse response;
    ::zaber::motion::callGateway("tools/get_db_service_pipe", request, &response);

    return response.getValue();
}

}  // namespace motion
}  // namespace zaber
