﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //


#include "zaber/motion/library.h"
#include "zaber/motion/gateway/call.h"
#include "zaber/motion/dto/requests/includes.h"

#include <cstdint>


namespace zaber { namespace motion {

/**
 * Sets library logging output.
 * @param mode Logging output mode.
 * @param filePath Path of the file to open.
 */
void Library::setLogOutput(const LogOutputMode& mode, const std::optional<std::string>& filePath) {

    ::zaber::motion::requests::SetLogOutputRequest request;
    request.setMode(mode);
    request.setFilePath(filePath);
    ::zaber::motion::callGateway("logging/set_output", request);
}

/**
 * Sets source of Device DB data. Allows selection of a web service or a local file.
 * @param sourceType Source type.
 * @param urlOrFilePath URL of the web service or path to the local file.
 * Leave empty for the default URL of Zaber web service.
 */
void Library::setDeviceDbSource(const DeviceDbSourceType& sourceType, const std::optional<std::string>& urlOrFilePath) {

    ::zaber::motion::requests::SetDeviceDbSourceRequest request;
    request.setSourceType(sourceType);
    request.setUrlOrFilePath(urlOrFilePath);
    ::zaber::motion::callGateway("device_db/set_source", request);
}

/**
 * Enables Device DB store.
 * The store uses filesystem to save information obtained from the Device DB.
 * The stored data are later used instead of the Device DB.
 * @param storeLocation Specifies relative or absolute path of the folder used by the store.
 * If left empty defaults to a folder in user home directory.
 * Must be accessible by the process.
 */
void Library::enableDeviceDbStore(const std::optional<std::string>& storeLocation) {

    ::zaber::motion::requests::ToggleDeviceDbStoreRequest request;
    request.setToggleOn(true);
    request.setStoreLocation(storeLocation);
    ::zaber::motion::callGateway("device_db/toggle_store", request);
}

/**
 * Disables Device DB store.
 */
void Library::disableDeviceDbStore() {

    ::zaber::motion::requests::ToggleDeviceDbStoreRequest request;
    ::zaber::motion::callGateway("device_db/toggle_store", request);
}

/**
 * Disables certain customer checks (like FF flag).
 * @param mode Whether to turn internal mode on or off.
 */
void Library::setInternalMode(bool mode) {

    ::zaber::motion::requests::SetInternalModeRequest request;
    request.setMode(mode);
    ::zaber::motion::callGateway("library/set_internal_mode", request);
}

/**
 * Sets the period between polling for IDLE during movements.
 * Caution: Setting the period too low may cause performance issues.
 * @param period Period in milliseconds.
 * Negative value restores the default period.
 */
void Library::setIdlePollingPeriod(int period) {

    ::zaber::motion::requests::IntRequest request;
    request.setValue(period);
    ::zaber::motion::callGateway("library/set_idle_polling_period", request);
}

/**
 * Throws an error if the version of the loaded shared library does not match the caller's version.
 */
void Library::checkVersion() {

    ::zaber::motion::requests::CheckVersionRequest request;
    request.setHost("cpp");
    request.setVersion("7.5.0");
    ::zaber::motion::callGateway("library/check_version", request);
}

}  // namespace motion
}  // namespace zaber
