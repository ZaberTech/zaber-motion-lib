#pragma once

#include <cstdint>

#if _WIN32
#define ZML_DLL_IMPORT __declspec(dllimport)
#else
#define ZML_DLL_IMPORT
#endif

typedef uint8_t GoUint8;
typedef int32_t GoInt32;
typedef int64_t GoInt64;

extern "C" {
    extern ZML_DLL_IMPORT GoInt32 zml_call(void* request, GoInt64 tag, void* callback, GoUint8 async);
    extern ZML_DLL_IMPORT void zml_setEventHandler(int64_t tag, void* callback);
}
