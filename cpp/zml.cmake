file( GLOB_RECURSE zml_files src/zaber/motion/**.cpp )
list(FILTER zml_files EXCLUDE REGEX "src/zaber/motion/gateway/.*")
file( GLOB_RECURSE zml_gateway_files src/zaber/motion/gateway/**.cpp )

set( ZML_SOURCE_FILES ${zml_files} )
set( ZML_GATEWAY_SOURCE_FILES ${zml_gateway_files} )
