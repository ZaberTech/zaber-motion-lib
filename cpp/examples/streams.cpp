#include "zaber/motion/ascii.h"
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

namespace zaber { namespace motion { namespace ascii {

int main() {
    Library::setLogOutput(LogOutputMode::STDOUT);
	Library::setDeviceDbSource(DeviceDbSourceType::WEB_SERVICE, "https://api.zaber.io/device-db/master");

    std::cout << "Starting connection to device" << std::endl;
    Connection conn = Connection::openTcp("localhost", TCP_PORT_DEVICE_ONLY);

    std::cout << "Detecting devices" << std::endl;
	auto detected = conn.detectDevices();
    Device& device = detected[0];

	auto allAxes = device.getAllAxes();
	allAxes.home();

	double numStreams = device.getSettings().get("stream.numstreams");
	std::cout << "Number of streams possible: " << numStreams << std::endl;

	Stream stream = device.getStreams().getStream(1);

	StreamBuffer streamBuffer = device.getStreams().getBuffer(1);
	streamBuffer.erase();

	stream.setupStore(streamBuffer, 1, 2);

	stream.lineAbsolute(
		Measurement(29.0047, Units::LENGTH_MILLIMETRES),
		Measurement(40.49, Units::LENGTH_MILLIMETRES)
	);
	stream.lineAbsolute(
		Measurement(0),
		Measurement(50.5, Units::LENGTH_MILLIMETRES)
	);

	double pathInCm[5][2] = { {0.00, 3.00}, {2.25, 7.10}, {5.35, 0.15}, {1.45, 10.20}, {9.00, 9.00} };
	for (double* point : pathInCm) {
		stream.lineAbsolute(
			Measurement(point[0], Units::LENGTH_CENTIMETRES),
			Measurement(point[1], Units::LENGTH_CENTIMETRES)
		);
	}

	std::vector<std::string> content = streamBuffer.getContent();
	for (auto line : content) {
		std::cout << line << std::endl;
	}

	stream.disable();

	stream.setupLive(1, 2);

	stream.call(streamBuffer);

	Measurement circleCenterAbs[] = {
		Measurement(2, Units::LENGTH_CENTIMETRES),
		Measurement(4, Units::LENGTH_CENTIMETRES),
	};
	stream.circleAbsolute(RotationDirection::CW, circleCenterAbs[0], circleCenterAbs[1]);

	Measurement circleCenterRel[] = {
		Measurement(-2, Units::LENGTH_CENTIMETRES),
		Measurement(0, Units::LENGTH_CENTIMETRES),
	};
	stream.circleRelative(RotationDirection::CCW, circleCenterRel[0], circleCenterRel[1]);

	Measurement arcCircleCenterRel[] = {
		Measurement(-2, Units::LENGTH_CENTIMETRES),
		Measurement(0, Units::LENGTH_CENTIMETRES),
	};
	Measurement arcEndRel[] = {
		Measurement(-2, Units::LENGTH_CENTIMETRES),
		Measurement(0, Units::LENGTH_CENTIMETRES),
	};
	stream.arcRelative(
		RotationDirection::CCW,
		arcCircleCenterRel[0], arcCircleCenterRel[1],
		arcEndRel[0], arcEndRel[1]
	);

	Measurement arcCircleCenterAbs[] = {
		Measurement(2, Units::LENGTH_CENTIMETRES),
		Measurement(4, Units::LENGTH_CENTIMETRES),
	};
	Measurement arcEndAbs[] = {
		Measurement(4, Units::LENGTH_CENTIMETRES),
		Measurement(4, Units::LENGTH_CENTIMETRES),
	};
	stream.arcAbsolute(
		RotationDirection::CW,
		arcCircleCenterAbs[0], arcCircleCenterAbs[1],
		arcEndAbs[0], arcEndAbs[1]
	);

	stream.lineAbsoluteOn({ 1 }, { Measurement(1) });

	stream.setMaxCentripetalAcceleration(5, Units::ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
	stream.setMaxTangentialAcceleration(5, Units::ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
	stream.setMaxSpeed(0.5, Units::VELOCITY_MILLIMETRES_PER_SECOND);

	stream.wait(2, Units::TIME_SECONDS);

	stream.setDigitalOutput(1, DigitalOutputAction::ON);
	stream.waitDigitalInput(1, true);

	stream.setDigitalOutput(1, DigitalOutputAction::TOGGLE);
	stream.setDigitalOutput(1, DigitalOutputAction::TOGGLE);

	stream.setAnalogOutput(1, 0.42);
	stream.waitAnalogInput(1, ">=", 0.50);

	stream.waitUntilIdle();

	std::cout << stream.toString() << std::endl;
	std::cout << stream.getAxes().size() << std::endl;
	std::cout << stream.getMaxSpeed(Units::VELOCITY_CENTIMETRES_PER_SECOND) << std::endl;
	std::cout << stream.getMaxTangentialAcceleration(Units::ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED) << std::endl;
	std::cout << stream.getMaxCentripetalAcceleration(Units::ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED) << std::endl;

	stream.cork();
	stream.uncork();

	if (stream.isBusy()) {
		stream.waitUntilIdle();
	}

	stream.disable();

    conn.close();
    return 0;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber

int main() { return zaber::motion::ascii::main(); }
