#include "zaber/motion/ascii.h"
#include "zaber/motion/dto/measurement.h"
#include "zaber/motion/units.h"

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

namespace zaber { namespace motion { namespace ascii {

int main() {
    Library::setLogOutput(LogOutputMode::STDOUT);

    std::cout << "Starting connection to device" << std::endl;
    Connection conn = Connection::openSerialPort("/dev/ttyUSB0");

    conn.setAlertCallback([](const AlertEvent& alert){
        std::cout << "Alert " << alert.toString() << std::endl;
    });

    conn.setDisconnectedCallback([](const std::shared_ptr<exceptions::MotionLibException>& e){
        std::cout << "Disconnected " << e->what() << std::endl;
    });

    std::cout << "Detecting devices" << std::endl;
    Device device = conn.detectDevices()[0];

    std::cout << "Homing device" << std::endl;
    device.getAllAxes().home();

    Axis axis = device.getAxis(1);

    std::string cmd = axis.prepareCommand("move sin ? ? ?",
                            Measurement(10, Units::LENGTH_MILLIMETRES),  // NOLINT: Magic numbers are okay here
                            Measurement(1, Units::TIME_SECONDS),         // NOLINT: Magic numbers are okay here
                            Measurement(3)                               // NOLINT: Magic numbers are okay here
                        );
    axis.genericCommand(cmd);
    axis.waitUntilIdle();

    std::cout << "Test begins!" << std::endl;
    axis.moveAbsolute(1, Units::LENGTH_CENTIMETRES);
    axis.moveRelative(-1, Units::LENGTH_MILLIMETRES);

    axis.moveVelocity(3, Units::VELOCITY_MILLIMETRES_PER_SECOND);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    axis.stop();

    double position = axis.getPosition(Units::LENGTH_MILLIMETRES);
    std::cout << "Axis is at " << position << "mm" << std::endl;

    std::cout << "Test Ends" << std::endl;
    conn.close();
    return 0;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber

int main() { return zaber::motion::ascii::main(); }
