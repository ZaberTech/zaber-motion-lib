#include "zaber/motion/ascii.h"
#include "zaber/motion/microscopy.h"
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using namespace zaber::motion::microscopy;
using namespace zaber::motion::ascii;
using namespace zaber::motion;

int main() {
    Library::setLogOutput(LogOutputMode::STDOUT);

    Connection conn = Connection::openSerialPort("COM1");

    conn.detectDevices();

    auto changer = ObjectiveChanger::find(conn);

    for (int i = 0; i < 5; i++) {
        changer.change((i % 4) + 1);
    }

    return 0;
}
