#include "zaber/motion/ascii.h"
#include <chrono>
#include <iostream>
#include <mutex>
#include <numeric>
#include <thread>
#include <vector>

namespace zaber { namespace motion { namespace ascii {

constexpr int THREAD_COUNT = 12;
constexpr double CYCLE_COUNT = 10000;

constexpr int TCP_PORT = 11234;

std::vector<double> g_averages;  // NOLINT(fuchsia-statically-constructed-objects)
std::mutex g_averages_mutex;  // NOLINT(fuchsia-statically-constructed-objects)

void runThread() {
    double avg = 0.0;
    Connection conn = Connection::openTcp("127.0.0.1", TCP_PORT);

    for (int i=0; i <= CYCLE_COUNT; ++i) {
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        conn.genericCommand("");
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

        avg += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    }

    conn.close();
    avg /= CYCLE_COUNT;

    std::lock_guard<std::mutex> lock(g_averages_mutex);
    g_averages.push_back(avg);
}

void runTest(const int& threadCount) {
    std::vector<std::thread> threads(threadCount);
    threads.reserve(threadCount);

    g_averages.clear();
    g_averages.reserve(threadCount);

    for (int i=0; i < threadCount; ++i) {
        threads[i] = std::thread(runThread);
    }
    for (int i=0; i < threadCount; ++i) {
        threads[i].join();
    }

    double average = std::accumulate(g_averages.begin(), g_averages.end(), 0.0) / g_averages.size();
    std::cout << threadCount << " " << average << std::endl;
}

int main() {
    for (int i=1; i <= THREAD_COUNT; ++i) {
        runTest(i);
    }

    return 0;
}

}  // namespace ascii
}  // namespace motion
}  // namespace zaber

int main() { return zaber::motion::ascii::main(); }
