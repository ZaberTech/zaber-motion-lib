set( EXAMPLE_TARGET_NAME streams_example )
add_executable( ${EXAMPLE_TARGET_NAME} streams.cpp )

target_include_directories( ${EXAMPLE_TARGET_NAME}
  PUBLIC
    $<INSTALL_INTERFACE:include>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/../src
)
target_include_directories( ${EXAMPLE_TARGET_NAME}
  SYSTEM PRIVATE
    ${CMAKE_SOURCE_DIR}/../build
)
target_include_directories( ${EXAMPLE_TARGET_NAME} PUBLIC ${NLOHMANN_INCLUDE_DIR} )

if( MSVC )
  target_compile_options( ${EXAMPLE_TARGET_NAME} PRIVATE $<$<CONFIG:DEBUG>:/${MSVC_RUNTIME_FLAG}d> $<$<CONFIG:RELEASE>:/${MSVC_RUNTIME_FLAG}> /W4 /WX /MP )
else()
  target_compile_options( ${EXAMPLE_TARGET_NAME} PRIVATE -Wall -Wextra -pedantic -Werror )
endif()

file( GLOB DYNAMIC_LIB_FILES ../../build/zaber-motion-core* )
file( COPY ${DYNAMIC_LIB_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

# Link the tests with the zml
target_link_libraries( ${EXAMPLE_TARGET_NAME} zml )
