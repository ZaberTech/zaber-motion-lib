#include "zaber/motion/ascii.h"
#include "zaber/motion/gcode.h"
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using namespace zaber::motion::gcode;
using namespace zaber::motion::ascii;
using namespace zaber::motion;

int main() {
  Library::setLogOutput(LogOutputMode::STDOUT);
  Library::setDeviceDbSource(DeviceDbSourceType::WEB_SERVICE, "https://api.zaber.io/device-db/master");

  Connection conn = Connection::openTcp("localhost", 11321);

  auto detected = conn.detectDevices();
  Device& device = detected[0];

  auto allAxes = device.getAllAxes();
  allAxes.home();

  Stream stream = device.getStreams().getStream(1);

  stream.setupLive(1, 2);

  {
    std::vector<AxisMapping> mappings({ AxisMapping("Y", 0), AxisMapping("X", 1) });
    TranslatorConfig config;
    config.setAxisMappings(mappings);

    Translator translator = Translator::setup(stream, config);

    translator.translate("G28 Y10");
    translator.translate("G0 X10 Y20");
    translator.flush();

    auto y = translator.getAxisPosition("Y", Units::LENGTH_MILLIMETRES);
    translator.setAxisPosition("Y", y * 2, Units::LENGTH_MILLIMETRES);

    translator.setTraverseRate(3, Units::VELOCITY_MILLIMETRES_PER_SECOND);

    translator.translate("G0 X10 Y20");
    translator.flush();
  }

  stream.disable();

  conn.close();
  return 0;
}
