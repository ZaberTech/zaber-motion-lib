#include "zaber/motion/binary.h"
#include <chrono>
#include <cstdio>
#include <iostream>
#include <thread>

namespace zaber { namespace motion { namespace binary {

constexpr int MOVE_TIME = 2000;
constexpr int MOVE_POS = 2;
constexpr int MOVE_SPEED = 1;

int main() {
    std::cout << "Starting connection to device" << std::endl;
    Connection conn = Connection::openSerialPort("/dev/ttyUSB0");

    std::cout << "Detecting devices" << std::endl;
    Device device = conn.detectDevices()[0];
    std::cout << "Device " << device.getDeviceAddress() << " has device ID " << device.getIdentity().getDeviceId() << std::endl;

    double pos = device.home(Units::LENGTH_CENTIMETRES);
    std::cout << "Position after home: " << pos << " cm" << std::endl;

    pos = device.moveAbsolute(MOVE_POS, Units::LENGTH_CENTIMETRES);
    std::cout << "Position after move absolute: " << pos << " cm" << std::endl;

    pos = device.moveRelative(MOVE_POS, Units::LENGTH_MILLIMETRES);
    std::cout << "Position after move relative: " << pos << " mm" << std::endl;

    double velocity = device.moveVelocity(MOVE_SPEED, Units::VELOCITY_MILLIMETRES_PER_SECOND);
    std::cout << "Starting move velocity with speed: " << velocity << " mm/s" << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(MOVE_TIME));
    pos = device.stop(Units::LENGTH_CENTIMETRES);
    std::cout << "Position after stop: " << pos << " cm" << std::endl;

    std::cout << "Final position in microsteps: " << device.getPosition() << std::endl;

    conn.close();
    return 0;
}

}  // namespace binary
}  // namespace motion
}  // namespace zaber

int main() { return zaber::motion::binary::main(); }
