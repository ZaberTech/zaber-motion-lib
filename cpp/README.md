# Zaber Motion Library

Zaber Motion Library is a multi-platform library used to operate Zaber devices.

External documentation: [https://software.zaber.com/motion-library/](https://software.zaber.com/motion-library/)
