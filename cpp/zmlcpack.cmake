### Cpack variables and configuration for the Zaber Motion Library ###

if( CMAKE_SYSTEM_NAME STREQUAL "Windows" )
  set( CPACK_GENERATOR "NSIS")
  set( CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
endif()
if( CMAKE_SYSTEM_NAME STREQUAL "Darwin" )
  set( CPACK_GENERATOR "productbuild;STGZ")
  set( CPACK_PACKAGING_INSTALL_PREFIX "/usr/local")
endif()
if( CMAKE_SYSTEM_NAME STREQUAL "Linux" )
  set( CPACK_GENERATOR "DEB;STGZ")
endif()

set(CPACK_PACKAGE_VENDOR "Zaber Technologies Inc.")
set(CPACK_PACKAGE_CONTACT "contact@zaber.com")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE.txt" )

set(CPACK_PACKAGE_NAME "ZaberMotionCppInstaller")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Installs the C++ version of the Zaber Motion Library")
set(CPACK_PACKAGE_DESCRIPTION "This program installs the C++ version of the Zaber Motion Library. Please view the online documentation for installation and usage instructions.")

if( DEFINED ZML_VERSION )
  set(CPACK_PACKAGE_VERSION "${ZML_VERSION}")
else()
  set(CPACK_PACKAGE_VERSION "DEV")
endif()

set(CPACK_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}_${ZML_ARCH}")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "Zaber Motion Library")
if ( CMAKE_SYSTEM_NAME STREQUAL "Windows" AND ZML_ARCH STREQUAL "arm64" )
  set ( CPACK_NSIS_INSTALL_ROOT "C:\\Program Files (Arm)" )
endif()

set(CPACK_NSIS_MUI_ICON "${CMAKE_SOURCE_DIR}/../dev-portal/docs/assets/images/favicon-old.ico")
set(CPACK_NSIS_MUI_UNIICON "${CMAKE_SOURCE_DIR}/../dev-portal/docs/assets/images/favicon-old.ico")

# Configure component handling options
set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)

set( CPACK_COMPONENT_ZMLLIB_REQUIRED                    ON )
set( CPACK_COMPONENT_ZMLLIB_DISPLAY_NAME                "Zaber motion C++ wrapper library" )
set( CPACK_COMPONENT_ZMLLIB_DESCRIPTION                 "This is shared object for the Zaber Motion C++ wrapper library. It is required for Zaber Motion C++ support.")

set( CPACK_COMPONENT_ZMLHEADERS_REQUIRED                ON )
set( CPACK_COMPONENT_ZMLHEADERS_DEPENDS                 zmllib )
set( CPACK_COMPONENT_ZMLHEADERS_DISPLAY_NAME            "Zaber motion C++ wrapper library headers" )
set( CPACK_COMPONENT_ZMLHEADERS_DESCRIPTION             "These are the headers for the Zaber Motion C++ wrapper library. It is required for Zaber Motion C++ support.")

set( CPACK_COMPONENT_ZMLCGOLIB_REQUIRED                 ON )
set( CPACK_COMPONENT_ZMLCGOLIB_DEPENDS                  zmllib )
set( CPACK_COMPONENT_ZMLCGOLIB_DISPLAY_NAME             "Zaber motion core library" )
set( CPACK_COMPONENT_ZMLCGOLIB_DESCRIPTION              "This is shared object for the Zaber Motion Library. Required in conjunction with the wrapper library for Zaber Motion C++ support.")

set( CPACK_COMPONENT_ZMLCGOLIBHEADERS_REQUIRED          ON )
set( CPACK_COMPONENT_ZMLCGOLIBHEADERS_DEPENDS           zmllib )
set( CPACK_COMPONENT_ZMLCGOLIBHEADERS_DISPLAY_NAME      "Zaber Motion core library headers" )
set( CPACK_COMPONENT_ZMLCGOLIBHEADERS_DESCRIPTION       "These are the headers for the Zaber Motion Library. These are a dependency for the wrapper library headers.")
