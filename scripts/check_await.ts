
import path from 'path';
import fs from 'fs/promises';

import { glob } from 'glob';

/**
 * This is somehow primitive check of await usage in C# code.
 */
export const checkAwaitsCsharp = async () => {
  console.log('Checking awaits...');
  const problems: string[] = [];

  const files = await glob(path.join(__dirname, '..', 'csharp', 'Zaber.Motion', '**', '*.cs'), { windowsPathsNoEscape: true });
  for (const file of files) {
    const content = await fs.readFile(file, 'utf8');

    // check for missing .ConfigureAwait(false)
    content.replace(/\s+await\s+([^;]*);/g, (match, call) => {
      if (typeof call !== 'string' || !call.includes('.ConfigureAwait(false)')) {
        problems.push(`Missing ".ConfigureAwait(false)": ${call}`);
      }
      return '';
    });

    // check for nested await which poses problem for this check
    content.replace(/\s+await\s+[^;]*(await\s+[^;]*);/g, (match, call) => {
      problems.push(`Nested await: ${call}`);
      return '';
    });
  }

  if (problems.length) {
    console.log('Found following issues:');
    problems.forEach(problem => console.log(problem));
    throw new Error(`Code is not clean. Found ${problems.length} issues.`);
  }

  console.log('Code clean.');
};
