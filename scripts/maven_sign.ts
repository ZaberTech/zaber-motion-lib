/* eslint-disable camelcase */
import fs from 'fs';
import { promisify } from 'util';

import copyCb from 'copy';
import { rimraf } from 'rimraf';

import { exec } from './tools';

const copy = promisify(copyCb);

const HOME = process.env.HOME;
const MVN_SETTINGS = `${HOME}/.m2/settings.xml`;

async function throwIfExists(file: string) {
  try {
    await fs.promises.access(file);
    throw new Error(`File ${file} is already populated. The script won't overwrite your files.`);
  } catch (err) {
    if ((err as { code?: string }).code !== 'ENOENT') {
      throw err;
    }
  }
}

export async function prepare_maven_sign() {
  await throwIfExists(MVN_SETTINGS);
  await fs.promises.mkdir(`${HOME}/.m2`, { recursive: true });
  await fs.promises.writeFile(MVN_SETTINGS, `
<settings>
  <servers>
      <server>
          <id>ossrh</id>
          <username>${process.env.MAVEN_USER}</username>
          <password>${process.env.MAVEN_PASSWORD}</password>
      </server>
  </servers>
</settings>
  `.trim(), 'utf-8');

  await throwIfExists(`${HOME}/.gnupg/pubring.kbx`);
  await exec(`git clone --depth 1 --branch master ${process.env.GIT_SERVER}/software-internal/software-keys.git keys`);
  await copy('keys/keys/libs/zml-maven/**/*', `${HOME}/.gnupg`);
  await exec(`chmod 700 ${HOME}/.gnupg`);
  await rimraf('keys');
}
