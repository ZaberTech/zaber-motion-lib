/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import got from 'got';

import { execOut } from './tools';

const baseUrl = 'https://gitlab.izaber.com/api/v4';
const project = 'software-public%2Fzaber-motion-lib';
const headers = {
  'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
};

async function getLastTag() {
  const { stdout } = await execOut('git tag');
  const tags = stdout.split('\n').map(tag => tag.trim()).filter(tag => tag);
  const lastTag = tags.splice(-1)[0];
  return lastTag;
}

const DATE_REGEX = /^Date:/;

async function getTagData(tag: string) {
  const { stdout } = await execOut(`git show "${tag}"`);
  const lines = stdout.split('\n');

  const dateLine = lines.find(line => line.match(DATE_REGEX));
  if (!dateLine) {
    throw new Error('Date line not found');
  }
  const date = new Date(dateLine.replace(DATE_REGEX, '').trim());

  return { tag, date };
}

async function releaseNotes() {
  const lastTag = await getLastTag();

  const tagData = await getTagData(lastTag);
  console.log(tagData);

  const searchParams = new URLSearchParams([['updated_after', tagData.date.toString()]]);

  const response = await got(
    `${baseUrl}/projects/${project}/merge_requests?${searchParams.toString()}`,
    { headers, responseType: 'json' }
  );
  const body = response.body as any[]; // eslint-disable-line @typescript-eslint/no-explicit-any
  console.log(`MRs: ${body.length}`);
  console.log();

  for (const mr of body) {
    console.log('---------------');
    console.log('URL:', mr.web_url);
    console.log('Branch:', mr.source_branch);
    console.log('State:', mr.state);
    console.log();

    console.log(`* **${mr.title}**`);
    console.log('    *', mr.description);
    console.log();
  }
}

releaseNotes().catch(console.log);
