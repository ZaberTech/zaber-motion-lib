import childProcess from 'child_process';

import { ensureArray } from '@zaber/toolbox';
import replaceInFileImport, { ReplaceInFileConfig } from 'replace-in-file';

export const exec = (command: string, options?: childProcess.ExecOptions) => new Promise<void>((resolve, reject) => {
  const child = childProcess.exec(command, options, err => err ? reject(err) : resolve());
  child.stdout?.pipe(process.stdout);
  child.stderr?.pipe(process.stderr);
});

export const execOut = (command: string, options?: childProcess.ExecOptions) =>
  new Promise<{ stdout: string; stderr: string }>((resolve, reject) =>
    childProcess.exec(command, { ...options, encoding: 'utf-8' },
      (err, stdout, stderr) => err ? reject(err) : resolve({ stdout, stderr }))
  );

export const execOutNoErr = (command: string, options?: childProcess.ExecOptions) =>
  new Promise<{ stdout: string; stderr: string; err: childProcess.ExecException | null }>(resolve =>
    childProcess.exec(command, { ...options, encoding: 'utf-8' },
      (err, stdout, stderr) => resolve({ err, stdout, stderr }))
  );

export function compareVersions(a: { major: number; minor: number }, b: { major: number; minor: number }) {
  if (a.major !== b.major) {
    return a.major - b.major;
  }
  return a.minor - b.minor;
}

/** Fixed replace-in-file that accepts windows path separator. */
export async function replaceInFile(options: ReplaceInFileConfig) {
  const fixedOptions: typeof options = {
    ...options,
    files: ensureArray(options.files).map(file => file.replace(/\\/g, '/')),
  };
  await replaceInFileImport(fixedOptions);
}

export async function isCommandAvailable(command: string): Promise<boolean> {
  try {
    await exec(`command -v ${command}`);
    return true;
  } catch {
    return false;
  }
}
