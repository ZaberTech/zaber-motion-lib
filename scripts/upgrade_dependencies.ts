/* eslint-disable camelcase */
import path from 'path';

import { series } from 'gulp';
import { notNil } from '@zaber/toolbox';

import { exec, execOut, replaceInFile } from './tools';


async function upgrade_py() {
  await exec('cd py && pdm update');
}

async function upgrade_js() {
  await exec('cd js && npm install && npm update');
}

function parseGitTags(output: string) {
  const lines = output.split('\n');
  const tags = lines.map(line => {
    const match = line.match(/refs\/tags\/([^^]*)/);
    return match?.[1];
  }).filter(notNil);
  const versions = tags.map(tag => {
    const match = tag.match(/^v(\d+)\.(\d+)\.(\d+)$/);
    return match && {
      tag,
      version: [+match[1], +match[2], +match[3]] as const,
    };
  }).filter(notNil);

  const sortedVersions = versions.sort(
    (a, b) => (a.version[0] - b.version[0]) || (a.version[1] - b.version[1]) || (a.version[2] - b.version[2])
  );
  return sortedVersions;
}

async function upgrade_cpp() {
  const { stdout } = await execOut('git ls-remote --tags https://github.com/ReactiveX/RxCpp');
  const versions = parseGitTags(stdout);
  const lastStable = versions.slice(-1)[0];
  await replaceInFile({
    files: path.join(__dirname, '..', 'cpp', 'thirdparty', 'rxcpp', 'CMakeLists.txt'),
    from: /GIT_TAG .*/g,
    to: `GIT_TAG ${lastStable.tag}`,
  });
}

async function upgrade_cs() {
  console.log('Nothing to update');
}

async function upgrade_java() {
  await exec('cd java && mvn versions:use-latest-releases');
}

export const upgrade_all_dependencies = series(upgrade_py, upgrade_js, upgrade_cpp, upgrade_cs, upgrade_java);
