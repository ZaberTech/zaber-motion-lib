declare module '@zaber/tools' {
  function getLatestDatabaseFile(): Promise<string>;
  function generateGoBinLicense(outputFile: string, projectDir: string, forkReplacements: Record<string, string>): Promise<void>;
}

declare module 'yargs' {
  export const argv: Record<string, string | undefined>;
}
