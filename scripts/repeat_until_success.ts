
import { exec } from './tools';

async function main() {
  const args = process.argv.slice(2);
  let attemptsLeft = +args.shift()!;

  while (attemptsLeft-- > 0) {
    try {
      await exec(args.join(' '));
      break;
    } catch (err) {
      if (attemptsLeft === 0) {
        throw err;
      }
    }
  }
}

main().catch(err => {
  console.log(err);
  process.exit(1);
});
