const ZML_ICON = 'https://i.imgur.com/K9CsLps.png';

export const generateCard = (version: string, changeLog: string) => ({
  cardsV2: [
    {
      cardId: `zaber-motion-library-release-${version}`,
      card: {
        header: {
          title: 'Zaber Motion Library Release',
          subtitle: `${version}`,
          imageUrl: ZML_ICON,
        },
        sections: [
          ...changeLog ? [{
            header: 'Changelog',
            collapsible: true,
            widgets: [
              {
                textParagraph: {
                  text: changeLog
                }
              }
            ]
          }] : [],
          {
            widgets: [
              {
                buttonList: {
                  buttons: [
                    {
                      text: 'Download / Update',
                      onClick: {
                        openLink: {
                          url: 'https://software.zaber.com/motion-library/docs/tutorials'
                        }
                      },
                      icon: {
                        materialIcon: {
                          name: 'logout'
                        }
                      }
                    },
                  ]
                }
              }
            ]
          }
        ]
      }
    }
  ]
});
