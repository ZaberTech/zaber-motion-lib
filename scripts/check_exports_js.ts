import path from 'path';
import fs from 'fs/promises';

/** Checks that exports in package.json match directories in src. */
export const checkPackageExports = async () => {
  const srcPath = path.resolve(path.join(__dirname, '..', 'js', 'src'));
  const srcEntries = await fs.readdir(srcPath, { withFileTypes: true });
  const namespaces = srcEntries
    .filter(entry => entry.isDirectory() && !entry.name.match(/^(gateway|requests)$/))
    .map(entry => entry.name);

  const packagePath = path.resolve(path.join(__dirname, '..', 'js', 'package.json'));
  const packageJson = JSON.parse(await fs.readFile(packagePath, 'utf-8'));
  const { exports } = packageJson;

  for (const namespace of namespaces) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (!exports[`./${namespace}`]) {
      throw new Error(`Namespace ${namespace} is missing in package.json`);
    }
  }
};
