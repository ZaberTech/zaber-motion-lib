import pytest
import asyncio
from concurrent.futures import Future
from reactivex import operators as rxop

from zaber_motion.call import call, call_sync, call_async
from zaber_motion.events import filter_events
from zaber_motion.dto import requests as dto
from zaber_motion import RequestTimeoutException, InvalidPacketException, IncompatibleSharedLibraryException
from zaber_motion import Library

def test_request_response() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
    )

    response = call("test/request", request, dto.TestResponse.from_binary)

    assert response.data_pong == 'Hello...'

@pytest.mark.asyncio
async def test_request_response_async() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
    )

    response = await call_async("test/request", request, dto.TestResponse.from_binary)

    assert response.data_pong == 'Hello...'

@pytest.mark.asyncio
async def test_request_response_async_cancel() -> None:
    """
    Tests that cancellation does not cause an issue for async call
    """
    async def task_func() -> None:
        request = dto.TestRequest(
            data_ping = "Hello",
        )

        current_task = asyncio.current_task()
        assert current_task is not None
        current_task.cancel()
        await call_async("test/request", request, dto.TestResponse.from_binary)

    try:
        task = asyncio.create_task(task_func())
    except AttributeError:
        pytest.skip('Unsupported Python version')

    try:
        await task
    except asyncio.CancelledError:
        pass

def test_request_response_sync() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
    )

    response = call_sync("test/request", request, dto.TestResponse.from_binary)

    assert response.data_pong == 'Hello...'

def test_request_error() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
        return_error = True,
    )

    try:
        call("test/request", request, dto.TestResponse.from_binary)
        assert False, "No error thrown"
    except RequestTimeoutException as e:
        assert e.message == "Device has not responded in given timeout"
        assert str(e) == "RequestTimeoutException: Device has not responded in given timeout"

def test_request_error_with_data() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
        return_error_with_data = True,
    )

    try:
        call("test/request", request, dto.TestResponse.from_binary)
        assert False, "No error thrown"
    except InvalidPacketException as e:
        expected_packet = '123'
        expected_reason = 'For test'
        expected_message = 'Cannot parse incoming packet: "{}" ({})'.format(expected_packet, expected_reason)
        assert e.message == expected_message
        assert str(e) == 'InvalidPacketException: ' + expected_message
        assert e.details.packet == expected_packet
        assert e.details.reason == expected_reason

def test_request_no_response() -> None:
    response = call("test/request_no_response")
    assert response == None


def test_request_long_response() -> None:
    request = dto.TestRequest(
        data_ping = "Hello",
    )

    response = call("test/request_long", request, dto.TestResponseLong.from_binary)

    for i in range(len(response.data_pong)):
        assert response.data_pong[i] == 'Hello...{}'.format(i)


def test_event() -> None:
    promise: Future[dto.TestEvent] = Future()
    with filter_events("test/event", dto.TestEvent).pipe(rxop.take(1)).subscribe(lambda e: promise.set_result(e)):
        call("test/emit_event")

        event: dto.TestEvent = promise.result()
        assert event.data == "testing event data"
