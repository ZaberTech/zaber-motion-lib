import pytest
import decimal

from zaber_motion.dto import FirmwareVersion, Measurement
from zaber_motion.dto.ascii import ConversionFactor
from zaber_motion.dto.gcode import DeviceDefinition, AxisDefinition, TranslateResult
from zaber_motion import Units

def test_serializes_basic_structure() -> None:
    version = FirmwareVersion(1, 2, 3)

    expected_dict = {
        'major': 1,
        'minor': 2,
        'build': 3,
    }
    assert version.to_dict() == expected_dict
    assert FirmwareVersion.from_dict(expected_dict) == version


def test_serializes_units() -> None:
    assert Measurement(1, 'mm').to_dict() == {
        'value': 1,
        'unit': 'Length:millimetres',
    }
    assert Measurement(1, Units.LENGTH_METRES).to_dict() == {
        'value': 1,
        'unit': 'Length:metres',
    }
    assert Measurement(2).to_dict() == {
        'value': 2,
        'unit': None,
    }

    assert Measurement.from_dict({
        'value': 1,
        'unit': 'Length:metres',
    }) == Measurement(1, Units.LENGTH_METRES)

    assert Measurement.from_dict({
        'value': 2,
        'unit': '',
    }) == Measurement(2, Units.NATIVE)


def test_serializes_complex_structures() -> None:
    device = DeviceDefinition(
        device_id=1,
        axes=[
            AxisDefinition(
                peripheral_id=1,
                microstep_resolution=2,
            ),
            AxisDefinition(
                peripheral_id=3,
            ),
        ],
        max_speed=Measurement(4, Units.VELOCITY_MILLIMETRES_PER_SECOND),
    )
    device.validate()

    expected_dict = {
        'deviceId': 1,
        'axes': [
            {
                'peripheralId': 1,
                'microstepResolution': 2,
            },
            {
                'peripheralId': 3,
                'microstepResolution': None,
            },
        ],
        'maxSpeed': {
            'value': 4,
            'unit': 'Velocity:millimetres per second',
        },
    }

    assert device.to_dict() == expected_dict
    assert DeviceDefinition.from_dict(expected_dict) == device

def test_accepts_various_numbers() -> None:
    device = DeviceDefinition(
        device_id=15.0, # type: ignore
        axes=[
            AxisDefinition(
                peripheral_id=decimal.Decimal("123"), # type: ignore
                microstep_resolution=32.0, # type: ignore
            ),
        ],
        max_speed=Measurement(decimal.Decimal("4.123")), # type: ignore
    )
    device.validate()

    expected_dict = {
        'deviceId': 15,
        'axes': [
            {
                'peripheralId': 123,
                'microstepResolution': 32,
            },
        ],
        'maxSpeed': {
            'value': 4.123,
            'unit': None,
        },
    }

    result_dict = device.to_dict()
    assert result_dict == expected_dict
    assert isinstance(result_dict['deviceId'], int)
    assert isinstance(result_dict['axes'][0]['peripheralId'], int)
    assert isinstance(result_dict['axes'][0]['microstepResolution'], int)
    assert isinstance(result_dict['maxSpeed']['value'], float)

def test_accepts_none_for_empty_array() -> None:
    device = DeviceDefinition(
        device_id=15,
        axes=None, # type: ignore
        max_speed=Measurement(4),
    )
    device.validate()

    expected_dict = {
        'deviceId': 15,
        'axes': [],
        'maxSpeed': {
            'value': 4,
            'unit': None,
        },
    }

    assert device.to_dict() == expected_dict

def test_tolerates_any_iterable_for_array() -> None:
    device = DeviceDefinition(
        device_id=15,
        axes=(AxisDefinition(123), AxisDefinition(456)), # type: ignore
        max_speed=Measurement(4),
    )
    device.validate()

    expected_dict = [
        {
            'peripheralId': 123,
            'microstepResolution': None,
        },
        {
            'peripheralId': 456,
            'microstepResolution': None,
        },
    ]

    assert device.to_dict()['axes'] == expected_dict

def test_tolerates_none_for_string() -> None:
    factor = ConversionFactor(
        setting=None, # type: ignore
        value=0,
        unit=Units.NATIVE,
    )
    factor.validate()
    assert factor.to_dict() == {
        'setting': '',
        'value': 0,
        'unit': '',
    }

    result = TranslateResult(
        commands=[None, None], # type: ignore
        warnings=[],
    )
    result.validate()
    assert result.to_dict() == {
        'commands': ['', ''],
        'warnings': [],
    }
