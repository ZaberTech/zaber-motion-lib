import pytest

from zaber_motion.dto import FirmwareVersion, Measurement
from zaber_motion.dto.gcode import DeviceDefinition, AxisDefinition, TranslateResult
from zaber_motion.dto.ascii import IoPortLabel, IoPortType, ConversionFactor
from zaber_motion import Units

def test_checks_that_value_is_integer() -> None:
    try:
        version = FirmwareVersion(1.4, 2, 3)  # type: ignore
        version.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Major" of "FirmwareVersion" is not integer value.'

def test_checks_that_value_is_number() -> None:
    try:
        version = FirmwareVersion(None, 2, 3)  # type: ignore
        version.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Major" of "FirmwareVersion" is None.'

    try:
        version = FirmwareVersion("1", 2, 3)  # type: ignore
        version.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Major" of "FirmwareVersion" is not a number.'

def test_value_is_enum() -> None:
    try:
        label = IoPortLabel("di", 1, "")  # type: ignore
        label.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "PortType" of "IoPortLabel" is not an instance of "IoPortType".'

    try:
        label = IoPortLabel(None, 1, "")  # type: ignore
        label.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "PortType" of "IoPortLabel" is None.'

def test_value_is_string() -> None:
    try:
        label = IoPortLabel(IoPortType.DIGITAL_INPUT, 1, 12356)  # type: ignore
        label.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Label" of "IoPortLabel" is not a string.'

def test_checks_nested_instance_correct_type() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[],
            max_speed=FirmwareVersion(1, 2, 3),  # type: ignore
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "MaxSpeed" of "DeviceDefinition" is not an instance of "Measurement".'

def test_checks_nested_instance_none() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[],
            max_speed=None,  # type: ignore
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "MaxSpeed" of "DeviceDefinition" is None.'

def test_checks_nested_sanitization() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[],
            max_speed=Measurement("12"),  # type: ignore
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Value" of "Measurement" is not a number.'

def test_checks_array_elements_list() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=FirmwareVersion(1, 2, 3), # type: ignore
            max_speed=Measurement(4),
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "Axes" of "DeviceDefinition" is not iterable.'

def test_checks_array_elements_none() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[None], # type: ignore
            max_speed=Measurement(4),
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Item 0 in property "Axes" of "DeviceDefinition" is None.'

def test_checks_array_elements_type() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[FirmwareVersion(1, 2, 3)],  # type: ignore
            max_speed=Measurement(4),
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Item 0 in property "Axes" of "DeviceDefinition" is not an instance of "AxisDefinition".'

def test_checks_array_elements_sanitization() -> None:
    try:
        device = DeviceDefinition(
            device_id=15,
            axes=[AxisDefinition(peripheral_id=1, microstep_resolution=2.5)],   # type: ignore
            max_speed=Measurement(4),
        )
        device.validate()
        assert False
    except ValueError as e:
        assert str(e) == 'Property "MicrostepResolution" of "AxisDefinition" is not integer value.'
