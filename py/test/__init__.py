from os import path
import sys

sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), "..")))
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), "../../bindings_linux")))
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), "../../bindings_windows")))
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), "../../bindings_darwin")))
