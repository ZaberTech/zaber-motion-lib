from zaber_motion.microscopy import WdiAutofocusProvider, Microscope, ThirdPartyComponents
from zaber_motion.ascii import Connection

with WdiAutofocusProvider.open_tcp("169.254.64.162") as autofocus:
    with Connection.open_serial_port("COM5") as connection:
        connection.detect_devices()
        microscope = Microscope.find(connection, ThirdPartyComponents(autofocus.provider_id))
        print("Microscope found")

        while True:
            input("Press enter to focus")
            microscope.autofocus.focus_once()
            print("Position", microscope.focus_axis.get_position())
