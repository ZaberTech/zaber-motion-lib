# Zaber Motion Library

Zaber Motion Library is a multi-platform library used to operate Zaber devices.

Official documentation: [https://software.zaber.com/motion-library/](https://software.zaber.com/motion-library/)

The library must be installed using the built distributions (wheels).
The source distribution does not contain the necessary shared library built from the Go source code.
Please contact Zaber Technologies support if this causes you any inconvenience.
