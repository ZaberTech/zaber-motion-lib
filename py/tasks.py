from invoke import task
from shutil import rmtree, copy, copytree
from glob import glob
import os
import packaging.tags
from wheel.bdist_wheel import get_platform
import sys
import platform

PYTHON_VERSION = f"{sys.version_info.major}.{sys.version_info.minor}"
PACKAGE_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.realpath(os.path.join(PACKAGE_DIR, '..'))

def copy_bson():
    BSON_PACKAGE = ".venv/Lib/site-packages/zaber_bson" if sys.platform == "win32" else f".venv/lib/python{PYTHON_VERSION}/site-packages/zaber_bson"
    copytree(BSON_PACKAGE, "zaber_bson")
    license = glob(BSON_PACKAGE + "-*.dist-info/**/LICENSE", recursive=True)
    if len(license) != 1:
        raise FileNotFoundError("License file not found")
    copy(license[0], "zaber_bson/LICENSE")


def get_binding_name() -> str:
    """ Mirrors the logic in zaber_motion/gateway.py to determine the binding name. """

    is64bit = sys.maxsize > 2**32
    os_system = platform.system().lower()
    os_machine = platform.machine().lower()

    if os_system == "darwin":
        arch = "uni"
    else:
        if os_machine.startswith("aarch64") or os_machine.startswith("arm"):
            arch = "arm64" if is64bit else "arm"
        else:
            arch = "amd64" if is64bit else "386"

    if os_system == "linux":
        ext = ".so"
    elif os_system == "darwin":
        ext = ".dylib"
    elif os_system == "windows":
        ext = ".dll"
    else:
        raise ImportError(f"Unsupported operating system {os_system}")

    return f"zaber-motion-core-{os_system}-{arch}{ext}"


def build_wheel(c, platform_tag, binding_name):
    binding = os.path.join(ROOT_DIR, 'build', binding_name)
    if not os.path.exists(binding):
        raise FileNotFoundError(f'Binding {binding} not found.')

    # cleanup any remnants of previous wheel builds
    rmtree('build', ignore_errors=True)
    rmtree('zaber_motion_bindings', ignore_errors=True)
    os.mkdir('zaber_motion_bindings')

    copy(binding, os.path.join('zaber_motion_bindings', binding_name))

    # Platform tag must be overridden wheel because we seemingly have no external modules
    cmd = f'python -m build --wheel "--config-setting=--build-option=--plat-name {platform_tag}"'
    c.run(cmd, hide=True, env={'SETUP_SKIP_ZML_BINDINGS': '1'})


@task
def build(c):
    platform_tags = list(packaging.tags.platform_tags())
    platform_tag = packaging.tags._normalize_string(get_platform(None))  # pylint: disable=protected-access

    if platform_tag.startswith('macosx_'):
        # We provide universal binaries, 10.4 is the lowest version that supports it.
        platform_tag = 'macosx_10_4_universal2'
    elif platform_tag.startswith('linux_'):
        # Generic linux tag with GLIBC 2.27 and higher (ldd --version).
        platform_tag = platform_tag.replace('linux', 'manylinux_2_27')

    if platform_tag not in platform_tags:
        raise ValueError(f'Platform tag {platform_tag} not in platform tags. We need to change the tags.')

    clean(c)

    copy_bson()

    # source dist must be built first, before we copy any of the bindings
    c.run('python -m build --sdist', hide=True, env={'SETUP_SKIP_ZML_BINDINGS': '1'})

    if platform_tag == 'win_amd64':
        build_wheel(c, 'win32', get_binding_name().replace('amd64', '386'))

    build_wheel(c, platform_tag, get_binding_name())


@task
def clean(c):
    rmtree('build', ignore_errors=True)
    rmtree('dist', ignore_errors=True)
    rmtree('zaber_motion_bindings', ignore_errors=True)
    rmtree('zaber_bson', ignore_errors=True)
    rmtree('zaber_motion.egg-info', ignore_errors=True)


@task
def test(c):
    c.run('pytest test --capture=no')


@task
def lint(c):
    print('Typechecking...')
    c.run("pyright --warnings --stats zaber_motion test")
    c.run("mypy --strict --implicit-reexport -p zaber_motion")
    c.run("mypy --strict test")
    print('Linting...')
    c.run("pycodestyle zaber_motion")
    c.run("pylint zaber_motion")


@task
def publish(c):
    upload = 'twine upload --repository-url https://test.pypi.org/legacy/ dist/* -u $PYPI_USER -p $PYPI_TEST_TOKEN'
    c.run(upload)


@task
def publish_prod(c):
    upload = 'twine upload dist/* -u $PYPI_USER -p $PYPI_TOKEN'
    c.run(upload)
