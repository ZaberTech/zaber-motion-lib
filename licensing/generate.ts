import fs from 'fs';
import path from 'path';

const forkReplacements = {
  'github.com/zabertech/go-serial': 'github.com/bugst/go-serial',
  'github.com/zabertech/go-ethereum': 'github.com/ethereum/go-ethereum',
};

const destinations = [
  path.join(__dirname, '..', 'js', 'LICENSE'),
  path.join(__dirname, '..', 'py', 'zaber_motion', 'LICENSE.txt'),
  path.join(__dirname, '..', 'py', 'zaber_motion_bindings', 'LICENSE.txt'),
  path.join(__dirname, '..', 'csharp', 'Zaber.Motion', 'LICENSE.txt'),
  path.join(__dirname, '..', 'java', 'src', 'main', 'resources', 'LICENSE.txt'),
  path.join(__dirname, '..', 'matlab', 'LICENSE.txt'),
  path.join(__dirname, '..', 'cpp', 'LICENSE.txt'),
];

export async function collectLicenses() {
  const licensePath = path.join(__dirname, 'LICENSE');
  const { generateGoBinLicense } = await import('@zaber/tools');
  await generateGoBinLicense(path.join(__dirname, '..'), licensePath, forkReplacements);

  for (const destination of destinations) {
    const destinationDir = path.dirname(destination);
    await fs.promises.mkdir(destinationDir, { recursive: true });
    await fs.promises.copyFile(licensePath, destination);
  }
}
