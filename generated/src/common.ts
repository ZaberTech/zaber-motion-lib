import path from 'path';

import { renderFile } from 'ejs';
import { ensureDir, writeFile } from 'fs-extra';
import { Database, OPEN_READONLY } from 'sqlite3';

export const ROOT = '..';
export const GENERATED_DEFINITIONS = 'templates/src/dto/definitions/generated';

export async function generate(srcDir: string, src: string, dst: string, data: ejs.Data) {
  const templateFile = path.join(srcDir, src);
  const compiled = await renderFile(templateFile, data, {});
  const destFile = path.join(ROOT, dst);
  await ensureDir(path.dirname(destFile));
  await writeFile(destFile, compiled as string);
}

export async function withDeviceDatabase<TReturn>(callback: (db: Database) => Promise<TReturn>): Promise<TReturn> {
  const { getLatestDatabaseFile } = await import('@zaber/tools');
  const file = await getLatestDatabaseFile();
  const db = new Database(file, OPEN_READONLY);
  try {
    return await callback(db);
  } finally {
    db.close();
  }
}
