import _ from 'lodash';
import sql from 'sqlite3';

import { generate, withDeviceDatabase } from '../common';

const ignoredSettings = new Set(['encoder.counth']);
const alternativeDescriptions = {
  'peripheralid': 'Peripheral ID (Firmware 6 and lower)',
  'peripheral.id': 'Peripheral ID (Firmware 7 and higher)',
  'deviceid': 'Device ID (Firmware 6 and lower)',
  'device.id': 'Device ID (Firmware 7 and higher)',
} as Record<string, string>;

async function genAsciiSettings(db: sql.Database) {
  const settings = await new Promise((resolve, reject) =>
    db.all(
      'SELECT DISTINCT ASCIIName FROM Data_Settings WHERE ASCIIName IS NOT NULL ORDER BY ASCIIName;',
      [], (err, rows: { ASCIIName: string }[]) => {
        if (err) {
          return reject(err);
        }

        const data = rows.filter(row =>
          !ignoredSettings.has(row.ASCIIName) && !row.ASCIIName.startsWith('_')
        ).map(row => {
          let description = _.startCase(row.ASCIIName).replace('.', ' ');
          if (row.ASCIIName in alternativeDescriptions) {
            description = alternativeDescriptions[row.ASCIIName];
          }
          return {
            name: _.upperFirst(_.camelCase(row.ASCIIName).replace('.', ' ')),
            description,
            value: row.ASCIIName,
          };
        });

        resolve(data);
      }
    )
  );

  const type = {
    name: 'SettingConstants',
    description: ['Named constants for all Zaber ASCII protocol settings.',
      'For more information please refer to the',
      '[ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).'],
    namespace: 'ascii',
    values: settings,
  };

  await generate(__dirname, 'string_constant_def.ejs', 'templates/src/templates/definitions/generated/ascii_settings.ts', { type });
}

export async function genStringConstants() {
  await withDeviceDatabase(async db => genAsciiSettings(db));
}
