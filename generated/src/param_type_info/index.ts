
import sql from 'sqlite3';
import _ from 'lodash';

import { generate, withDeviceDatabase } from '../common';

function getParamTypes(db: sql.Database) {
  return new Promise((resolve, reject) =>
    db.all('SELECT * FROM Data_ParamTypes ORDER BY Name;', [],
      (err, rows: { Name: string; DecimalPlaces: number }[]) => {
        if (err) {
          return reject(err);
        }

        const data = rows.map(row => ({
          name: row.Name,
          decimalPlaces: row.DecimalPlaces
        }));

        resolve(data);
      })
  );
}

export async function genParamTypeDecimalPlaces() {
  await withDeviceDatabase(async db => {
    const paramTypes = await getParamTypes(db);
    await generate(__dirname, 'param_type_info.go.ejs', 'internal/generated/param_type_info.go', {
      paramTypes,
      _,
    });
  });
}
