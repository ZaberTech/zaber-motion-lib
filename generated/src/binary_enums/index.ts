import path from 'path';

import _ from 'lodash';
import sql from 'sqlite3';

import { GENERATED_DEFINITIONS, generate, withDeviceDatabase } from '../common';

function dedupBinarySettings(rows: { BinaryReturnCommand?: number; BinarySetCommand?: number; BinaryName: string }[]) {
  const uniqueRows = _.map(_.groupBy(rows, 'BinaryName'), settingsRow => {
    if (settingsRow.length === 1) {
      return settingsRow[0];
    }
    const setCommands = _.uniq(settingsRow.map(row => row.BinarySetCommand).filter(r => r));
    const returnCommands = _.uniq(settingsRow.map(row => row.BinaryReturnCommand).filter(r => r));
    if (setCommands.length > 1) {
      throw new Error(`Conflicting BinarySetCommand ${setCommands.join()} for ${settingsRow[0].BinaryName}`);
    }
    if (returnCommands.length > 1) {
      throw new Error(`Conflicting BinaryReturnCommand ${returnCommands.join()} for ${settingsRow[0].BinaryName}`);
    }
    return {
      ...settingsRow[0],
      BinaryReturnCommand: returnCommands[0],
      BinarySetCommand: setCommands[0],
    };
  });
  return uniqueRows;
}

async function genBinaryCommandEnum(db: sql.Database) {
  // Base binary commands.
  const commands = await new Promise<{ name: string; value: number }[]>((resolve, reject) =>
    db.all('SELECT DISTINCT Command, Name FROM Data_BinaryCommands;', [],
      (err, rows: { Command: number; Name: string }[]) => {
        if (err) {
          return reject(err);
        }

        const data = rows.map(row => ({
          name: _.startCase(row.Name).replace(/[\s-_]/g, ''),
          value: row.Command,
        }));

        resolve(data);
      })
  );

  // Add set commands from settings table as additional binary commands.
  const setCommands = await new Promise<{ name: string; value: number }[]>((resolve, reject) =>
    db.all(`
SELECT DISTINCT BinaryReturnCommand, BinarySetCommand, BinaryName
FROM Data_Settings
WHERE BinaryReturnCommand NOT NULL OR BinarySetCommand NOT NULL;
`, [], (err, rows: { BinaryReturnCommand?: number; BinarySetCommand?: number; BinaryName: string }[]) => {
      if (err) {
        return reject(err);
      }
      const uniqueRows = dedupBinarySettings(rows);

      const data = uniqueRows.reduce((result, row) => {
        const cleanName = _.startCase(row.BinaryName).replace(/[\s-_]/g, '');

        if (row.BinaryReturnCommand) {
          result.push({
            name: `Return${cleanName}`,
            value: row.BinaryReturnCommand,
          });
        }

        if (row.BinarySetCommand) {
          result.push({
            name: `Set${cleanName}`,
            value: row.BinarySetCommand,
          });
        }

        return result;
      }, [] as { name: string; value: number }[]);

      resolve(data);
    })
  );

  let allCommands = commands.concat(setCommands);
  allCommands = _.sortBy(_.uniqBy(allCommands, v => v.name.toLowerCase()), 'value');

  const type = {
    name: 'CommandCode',
    description: ['Named constants for all Zaber Binary protocol commands.'],
    values: allCommands,
  };

  await generate(__dirname, 'enum_def.ejs', path.join(GENERATED_DEFINITIONS, 'binary_commands.ts'), { type, _ });
}

async function genBinaryErrorEnum(db: sql.Database) {
  const errors = await new Promise((resolve, reject) =>
    db.all('SELECT Name, Code FROM Data_BinaryErrors ORDER BY Code;', [],
      (err, rows: { Name: string; Code: number }[]) => {
        if (err) {
          return reject(err);
        }

        const data = rows.map(row => ({
          name: _.startCase(row.Name).replace(/[\s-_]/g, ''),
          value: row.Code,
        }));

        resolve(data);
      })
  );

  const type = {
    name: 'ErrorCode',
    description: ['Named constants for all Zaber Binary protocol error codes.'],
    values: errors,
  };

  await generate(__dirname, 'enum_def.ejs', path.join(GENERATED_DEFINITIONS, 'binary_errors.ts'), { type, _ });
}

function getBinaryReplyData(db: sql.Database) {
  return new Promise((resolve, reject) =>
    db.all('SELECT DISTINCT Reply, Name FROM Data_BinaryReplies ORDER BY Reply;', [],
      (err, rows: { Reply: number; Name: string }[]) => {
        if (err) {
          return reject(err);
        }

        const replies = rows.map(row => ({
          name: _.startCase(row.Name).replace(/[\s-_]/g, ''),
          value: row.Reply
        }));

        resolve(replies);
      })
  );
}

async function genBinaryReplyEnum(db: sql.Database) {
  const replies = await getBinaryReplyData(db);

  const type = {
    name: 'ReplyCode',
    description: ['Named constants for all Zaber Binary protocol reply-only command codes.'],
    values: replies,
  };

  await generate(__dirname, 'enum_def.ejs', path.join(GENERATED_DEFINITIONS, 'binary_replies.ts'), { type, _ });
}

function getBinarySettingsData(db: sql.Database) {
  return new Promise((resolve, reject) =>
    db.all(`
SELECT BinaryName, BinaryReturnCommand, BinarySetCommand
FROM (SELECT * FROM Data_Settings ORDER BY Id DESC)
WHERE BinaryName != ""
GROUP BY BinaryReturnCommand, BinarySetCommand
ORDER BY BinaryName;
    `, [], (err, rows: { BinaryReturnCommand?: number; BinarySetCommand?: number; BinaryName: string }[]) => {
      if (err) {
        return reject(err);
      }
      const uniqueRows = dedupBinarySettings(rows);

      const settings = uniqueRows.map((row, index) => ({
        name: _.startCase(row.BinaryName).replace(/[\s-_]/g, ''),
        dbName: row.BinaryName,
        value: index,
        returnCommand: row.BinaryReturnCommand ?? 0,
        setCommand: row.BinarySetCommand ?? 0,
      }));

      resolve(settings);
    })
  );
}

async function genBinarySettingsInfo(db: sql.Database) {
  const settings = await getBinarySettingsData(db);

  const type = {
    name: 'BinarySettings',
    description: ['Named constants for all Zaber Binary protocol settings.'],
    values: settings,
  };

  await generate(__dirname, 'enum_def.ejs', path.join(GENERATED_DEFINITIONS, 'binary_settings.ts'), { type, _ });

  await generate(__dirname, 'binary_settings.go.ejs', 'internal/generated/binary_settings.go', { settings });
}

export async function genEnums() {
  await withDeviceDatabase(async db => {
    await Promise.all([
      genBinaryCommandEnum(db),
      genBinaryErrorEnum(db),
      genBinaryReplyEnum(db),
      genBinarySettingsInfo(db),
    ]);
  });
}
