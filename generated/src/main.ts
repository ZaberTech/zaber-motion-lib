import { genUnits } from './units';
import { genStringConstants } from './string_constants';
import { genEnums as genBinaryEnums } from './binary_enums';
import { genParamTypeDecimalPlaces as genParamTypeInfo } from './param_type_info';

async function main() {
  await genUnits();
  await genStringConstants();
  await genBinaryEnums();
  await genParamTypeInfo();
}

main().catch(err => {
  console.log(err);
  process.exit(1);
});
