import fs from 'fs/promises';
import path from 'path';
import { EOL } from 'os';

import _ from 'lodash';
import { renderFile } from 'ejs';
import sql from 'sqlite3';

import { withDeviceDatabase } from '../common';

const ROOT = '..';

interface Dimension {
  Id: string;
  DimensionName: string;
}

interface Unit {
  DimensionId: string;
  LongName: string;
  ShortName: string;
  Scale: string;
  Offset: string;
}

const FILE_WARNING = 'This file is generated from the Zaber device database. Do not manually edit this file.';
const LITERAL_DIMENSIONS = [
  'Length', 'Velocity', 'Acceleration',
  'Angle', 'Angular Velocity', 'Angular Acceleration',
  'Percent', 'Time', 'Frequency',
];

function getDimensionName(dimension: Dimension) {
  return dimension.DimensionName.replace(/\s/g, '');
}

const ASCII_REPLACEMENTS = {
  '²': '^2',
  '°': 'deg',
  'µ': 'u',
} as Record<string, string>;
function makeShortNameVariants(shortname: string) {
  const ascii = shortname.replace(/[µ°²]/g, c => ASCII_REPLACEMENTS[c]);
  if (ascii == shortname) {
    return [shortname];
  }
  return [shortname, ascii];
}

const unitName = {
  ts: (dimension, unit) => _.toUpper(_.snakeCase(unit.LongName.replace(' ', '_'))),
  cs: (dimension, unit) =>
    `${getDimensionName(dimension)}_${_.upperFirst(_.camelCase(unit.LongName.replace(' ', '_')))}`,
  py: (dimension, unit) =>
    `${_.toUpper(_.snakeCase(getDimensionName(dimension)))}_${_.toUpper(_.snakeCase(unit.LongName.replace(' ', '_')))}`,
  java: (dimension, unit) => unitName.py(dimension, unit),
  cpp: (dimension, unit) => unitName.py(dimension, unit),
  swift: (dimension, unit) => `${_.lowerFirst(getDimensionName(dimension))}${_.upperFirst(_.camelCase(unit.LongName.replace(' ', '_')))}`,
} as Record<string, (dimension: Dimension, unit: Unit) => string>;

const generateTs = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const output: string[] = [];
  const dimensionNames = ['Native'];
  output.push(`// ${FILE_WARNING}`);
  output.push(`
export namespace Units {
  export const NATIVE: Native = '';
}

export type Native = '';
  `.trim());

  for (const [dimensionId, dimensionUnits] of _.toPairs(unitsByDimensions)) {
    const dimension = dimensions[dimensionId];
    const dimensionName = getDimensionName(dimension);

    dimensionNames.push(dimensionName);

    output.push('');
    output.push(`export enum ${dimensionName} {`);

    const uniqueUnits = _.uniqBy(dimensionUnits, 'LongName');

    for (const unit of uniqueUnits) {
      const name = unitName.ts(dimension, unit);
      const id = `'${dimension.DimensionName}:${unit.LongName}'`;
      output.push(`  ${name} = ${id},`);
      output.push(`  '${unit.ShortName}' = ${id},`);
    }

    output.push('}');
  }

  output.push('');
  output.push(`export type Units = ${dimensionNames.join(' | ')};`);
  output.push('');

  await fs.writeFile(path.join(ROOT, 'js', 'src', 'units.ts'), output.join(EOL));
};


const generateCSharp = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const output: string[] = [];

  output.push(`// ${FILE_WARNING}`);
  output.push('#pragma warning disable CA1707 // Identifiers should not contain underscores');
  output.push('namespace Zaber.Motion');
  output.push('{');
  output.push('    /// <summary>');
  output.push('    /// Units used by various functions.');
  output.push('    /// </summary>');
  output.push('    [Newtonsoft.Json.JsonConverter(typeof(UnitJsonConverter))]');
  output.push('    public enum Units');
  output.push('    {');
  output.push('        /// <summary>');
  output.push('        /// Unit native for the device. Conversion won\'t be performed.');
  output.push('        /// </summary>');
  output.push('        [UnitKey("")]');
  output.push('        Native,');
  output.push('');

  for (const [dimensionId, dimensionUnits] of _.toPairs(unitsByDimensions)) {
    const dimension = dimensions[dimensionId];

    const uniqueUnits = _.uniqBy(dimensionUnits, 'LongName');

    if (uniqueUnits.length > 0) {
      output.push(`        #region -- ${dimension.DimensionName} units --`);
      output.push('');
    }

    for (const unit of uniqueUnits) {
      const name = unitName.cs(dimension, unit);
      const id = `${dimension.DimensionName}:${unit.LongName}`;
      output.push('        /// <summary>');
      output.push(`        /// Dimension: ${dimension.DimensionName}, unit: ${unit.LongName}`);
      output.push('        /// </summary>');
      output.push(`        [UnitKey("${id}")]`);
      output.push(`        ${name},`);
      output.push('');
    }

    if (uniqueUnits.length > 0) {
      output.push('        #endregion');
    }
  }

  output.push('    }');
  output.push('}');
  output.push('#pragma warning restore CA1707 // Identifiers should not contain underscores');
  output.push('');

  await fs.writeFile(path.join(ROOT, 'csharp', 'Zaber.Motion', 'Units', 'Units.cs'),
    `\ufeff${output.join(EOL)}`,
    'utf-8');
};

const generatePy = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const templateFile = path.join(__dirname, 'units.py.ejs');

  const data = {
    dimensions,
    unitsByDimensions,
    unitName,
    _,
    LITERAL_DIMENSIONS,
    makeShortNameVariants,
  };
  const compiled = await renderFile(templateFile, data, {});
  await fs.writeFile(path.join(ROOT, 'py', 'zaber_motion', 'units.py'), compiled);
};

const generateJava = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const templateFile = path.join(__dirname, 'units.java.ejs');

  const data = {
    dimensions,
    unitsByDimensions,
    unitName,
    _,
  };

  const compiled = await renderFile(templateFile, data, {});

  const outputFile = path.join(ROOT, 'java', 'src', 'main', 'java', 'zaber', 'motion', 'Units.java');
  await fs.writeFile(outputFile, compiled);
};

const generateCpp = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>, units: Unit[]) => {
  const headerTemplateFile = path.join(__dirname, 'units.cpph.ejs');
  const privateHeaderTemplateFile = path.join(__dirname, 'units.cppprivate.ejs');
  const sourceTemplateFile = path.join(__dirname, 'units.cpp.ejs');

  const data = {
    dimensions,
    unitsByDimensions,
    unitName,
    numUnits: units.length,
    _,
  };

  const headerCompiled = await renderFile(headerTemplateFile, data, {});
  const privateHeaderCompiled = await renderFile(privateHeaderTemplateFile, data, {});
  const sourceCompiled = await renderFile(sourceTemplateFile, data, {});
  const outputDir = path.join(ROOT, 'cpp', 'src', 'zaber', 'motion');
  await fs.writeFile(path.join(outputDir, 'units.h'), headerCompiled);
  await fs.writeFile(path.join(outputDir, 'units.private.h'), privateHeaderCompiled);
  await fs.writeFile(path.join(outputDir, 'units.cpp'), sourceCompiled);
};

const generateSwift = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const ILLEGAL_CHARACTERS = ['°', '/', '⋅', '(', ')', '%'];


  const output: string[] = [];
  output.push(`// ${FILE_WARNING}`);
  output.push('');
  output.push(`
public enum Units: String, Codable, Sendable {
`.trim());

  output.push('    case native = ""');

  for (const [dimensionId, dimensionUnits] of _.toPairs(unitsByDimensions)) {
    const dimension = dimensions[dimensionId];

    output.push('');

    // Unit enum definition for dimension
    const uniqueUnits = _.uniqBy(dimensionUnits, 'LongName');
    for (const unit of uniqueUnits) {
      const name = unitName.swift(dimension, unit);
      const id = `"${dimension.DimensionName}:${unit.LongName}"`;
      output.push(`    case ${name} = ${id}`);
    }
    output.push('');

    // Unit shortname hashmap
    const dimensionName = getDimensionName(dimension);
    const shortNames: string[] = uniqueUnits.map(unit => unit.ShortName);
    const hasIllegalCharacters = shortNames.some(shortName => ILLEGAL_CHARACTERS.some(char => shortName.includes(char)));

    if (hasIllegalCharacters) {
      output.push(`    public static let ${_.camelCase(dimensionName)}: [String: Units] = [`);
      for (const unit of uniqueUnits) {
        const name = unitName.swift(dimension, unit);
        const shortNames = makeShortNameVariants(unit.ShortName);
        for (const shortName of shortNames) {
          output.push(`        "${shortName}": .${name},`);
        }
      }
      output.push('    ]');
    } else {
      output.push(`    public struct ${dimensionName} {`);
      output.push('        @available(*, unavailable) private init() { }');
      output.push('');
      for (const unit of uniqueUnits) {
        const name = unitName.swift(dimension, unit);
        const shortNames = makeShortNameVariants(unit.ShortName);
        for (const shortName of shortNames) {
          output.push(`        public static let \`${shortName}\` = Units.${name}`);
        }
      }
      output.push('    }');
    }
  }
  output.push('}');
  output.push('');

  await fs.writeFile(path.join(ROOT, 'swift', 'Sources', 'ZaberMotion', 'Units', 'Units.swift'), output.join(EOL));
};

const generateDefinitions = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const units: {
    DimensionName: string;
    dimensionName: string;
    units: {
      LongName: string;
      ShortName: string | null;
      names: Record<string, string>;
    }[];
  }[] = [];

  units.push({
    DimensionName: 'Native',
    dimensionName: 'Native',
    units: [{
      LongName: 'Device native units',
      ShortName: null,
      names: {
        ts: 'NATIVE',
        cs: 'Native',
        py: 'NATIVE',
        java: 'NATIVE',
        cpp: 'NATIVE',
        swift: 'native',
      }
    }]
  });

  for (const [dimensionId, dimensionUnits] of _.toPairs(unitsByDimensions)) {
    const dimension = dimensions[dimensionId];
    const dimensionName = getDimensionName(dimension);

    const uniqueUnits = _.uniqBy(dimensionUnits, 'LongName');

    const dimensionData = {
      ..._.pick(dimension, 'DimensionName'),
      dimensionName,
      units: uniqueUnits.map(unit => ({
        ..._.pick(unit, 'LongName', 'ShortName'),
        names: _.toPairs(unitName).reduce(
          (names, [language, name]) => ({ ...names, [language]: name(dimension, unit) }),
          {}
        ),
      })),
    };

    units.push(dimensionData);
  }

  const output: string[] = [];
  output.push('/* eslint-disable */');
  output.push(`// ${FILE_WARNING}`);
  const json = JSON.stringify(units, null, 2);
  output.push(`export default ${json};`);
  output.push('');

  await fs.writeFile(path.join(ROOT, 'templates', 'src', 'templates', 'definitions', 'units.ts'), output.join(EOL));
};

const generateGo = async (dimensions: _.Dictionary<Dimension>, unitsByDimensions: _.Dictionary<Unit[]>) => {
  const units: {
    DimensionName: string;
    DimensionId: string;
    LongName: string;
    ShortName: string;
    Scale: number;
    Offset: number;
  }[] = [];

  for (const [dimensionId, dimensionUnits] of _.toPairs(unitsByDimensions)) {
    const dimension = dimensions[dimensionId];

    const uniqueUnits = _.uniqBy(dimensionUnits, 'LongName');

    for (const unit of uniqueUnits) {
      units.push({
        DimensionName: dimension.DimensionName,
        ..._.pick(unit, 'DimensionId', 'LongName', 'ShortName'),
        Scale: +unit.Scale,
        Offset: +unit.Offset,
      });
    }
  }

  const output = `// ${FILE_WARNING}

package units

var staticUnitsJSON = \`
${JSON.stringify(units, null, 2)}
\`
`;

  await fs.writeFile(path.join(ROOT, 'internal', 'units', 'data.go'), output);
};

function getDimensionsData(db: sql.Database) {
  return new Promise<Dimension[]>((resolve, reject) =>
    db.all('SELECT DimensionName, Id FROM Data_Dimensions ORDER BY Id;', [], (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows as Dimension[]);
    })
  );
}

function getUnitData(db: sql.Database) {
  return new Promise<Unit[]>((resolve, reject) =>
    db.all('SELECT DimensionId, LongName, Offset, Scale, ShortName FROM Data_Units;', [], (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows as Unit[]);
    })
  );
}

export async function genUnits() {
  const { dimensionsRaw, units } = await withDeviceDatabase(async db => {
    const dimensionsRaw = await getDimensionsData(db);
    const units = await getUnitData(db);
    return { dimensionsRaw, units };
  });

  const dimensions = _.keyBy(dimensionsRaw, 'Id');
  const unitsByDimensions = _.groupBy(units, unit => unit.DimensionId);

  await generateDefinitions(dimensions, unitsByDimensions);
  await generateTs(dimensions, unitsByDimensions);
  await generateCSharp(dimensions, unitsByDimensions);
  await generatePy(dimensions, unitsByDimensions);
  await generateGo(dimensions, unitsByDimensions);
  await generateJava(dimensions, unitsByDimensions);
  await generateCpp(dimensions, unitsByDimensions, units);
  await generateSwift(dimensions, unitsByDimensions);
}
