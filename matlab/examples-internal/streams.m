import zaber.motion.Library;
import zaber.motion.DeviceDbSourceType;
import zaber.motion.ascii.Connection;
import zaber.motion.Measurement;
import zaber.motion.RotationDirection;
import zaber.motion.Units;
import zaber.motion.protobufs.Main;

Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, "https://api.zaber.io/device-db/master");

connection = Connection.openTcp('localhost', 11321);
try
    deviceList = connection.detectDevices();
    fprintf('Found %d devices.\n', deviceList.length);

    device = deviceList(1);

    device.getAllAxes().home();

    numStreams = device.getSettings().get("stream.numstreams");
    fprintf('Number of streams possible: %d\n', numStreams);

    stream = device.getStream(1);
    streamBuffer = device.getStreamBuffer(1);
    streamBuffer.erase();

    stream.setupStore(streamBuffer, [1, 2]);

    stream.lineAbsolute([
        Measurement(29.0047, Units.LENGTH_MILLIMETRES)
        Measurement(40.49, Units.LENGTH_MILLIMETRES)
    ]);
    stream.lineRelative([
        Measurement(0)
        Measurement(50.5, Units.LENGTH_MILLIMETRES)
    ]);

    points = [0.00 3.00; 2.25 7.10; 5.35 0.15; 1.45 10.20; 9.00 9.00];
    for row=1:size(points,1)
        stream.lineAbsolute([
            Measurement(points(row, 1), Units.LENGTH_CENTIMETRES)
            Measurement(points(row, 2), Units.LENGTH_CENTIMETRES)
        ]);
    end

    content = streamBuffer.getContent();
    for i=1:content.size
        fprintf("%s,\n", content(i));
    end

    stream.disable();

    stream.setupLive([1, 2]);

    stream.call(streamBuffer);

    circleCenterAbs = [
        Measurement(2, Units.LENGTH_CENTIMETRES)
        Measurement(4, Units.LENGTH_CENTIMETRES)
    ];
    stream.circleAbsolute(RotationDirection.CW, circleCenterAbs(1), circleCenterAbs(2));

    circleCenterRel = [
        Measurement(-2, Units.LENGTH_CENTIMETRES)
        Measurement(0, Units.LENGTH_CENTIMETRES)
    ];
    stream.circleRelative(RotationDirection.CCW, circleCenterRel(1), circleCenterRel(2));

    arcCircleCenterRel = [
        Measurement(-2, Units.LENGTH_CENTIMETRES)
        Measurement(0, Units.LENGTH_CENTIMETRES)
    ];
    arcEndRel = [
        Measurement(-2, Units.LENGTH_CENTIMETRES)
        Measurement(0, Units.LENGTH_CENTIMETRES)
    ];
    stream.arcRelative( ...
        RotationDirection.CCW, ...
        arcCircleCenterRel(1), arcCircleCenterRel(2), ...
        arcEndRel(1), arcEndRel(2));

    arcCircleCenterAbs = [
        Measurement(2, Units.LENGTH_CENTIMETRES)
        Measurement(4, Units.LENGTH_CENTIMETRES)
    ];
    arcEndAbs = [
        Measurement(4, Units.LENGTH_CENTIMETRES)
        Measurement(4, Units.LENGTH_CENTIMETRES)
    ];
    stream.arcAbsolute( ...
        RotationDirection.CW, ...
        arcCircleCenterAbs(1), arcCircleCenterAbs(2), ...
        arcEndAbs(1), arcEndAbs(2));

    axes = [ 1 ];
    endpoints = javaArray('zaber.motion.Measurement', 1);
    endpoints(1) = Measurement(1);
    stream.lineAbsoluteOn(axes, endpoints);

    stream.setMaxCentripetalAcceleration(5, Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
    stream.setMaxTangentialAcceleration(5, Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
    stream.setMaxSpeed(0.5, Units.VELOCITY_MILLIMETRES_PER_SECOND);

    stream.wait(2, Units.TIME_SECONDS);

    stream.setDigitalOutput(1, DigitalOutputAction.ON);
    stream.waitDigitalInput(1, true);

    stream.setDigitalOutput(1, DigitalOutputAction.TOGGLE);
    stream.setDigitalOutput(1, DigitalOutputAction.TOGGLE);

    stream.setAnalogOutput(1, 0.42);
    stream.waitAnalogInput(1, ">=", 0.50);

    stream.waitUntilIdle();

    fprintf("%s\n", stream.toString());
    fprintf("%d\n", stream.getAxes().size);
    fprintf("%f\n",stream.getMaxSpeed(Units.VELOCITY_CENTIMETRES_PER_SECOND));
    fprintf("%f\n",stream.getMaxTangentialAcceleration(Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED));
    fprintf("%f\n",stream.getMaxCentripetalAcceleration(Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED));

    stream.cork();
    stream.uncork();

    if stream.isBusy()
        fprintf("Stream is busy")
    end

    stream.waitUntilIdle();

    connection.close();
catch exception
    connection.close();
    rethrow(exception);
end
