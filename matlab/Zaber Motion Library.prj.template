<deployment-project plugin="plugin.toolbox" plugin-version="1.0">
  <configuration file="~~PATH~~/matlab/Zaber Motion Library.prj" location="~~PATH~~/matlab" name="Zaber Motion Library" target="target.toolbox" target-name="Package Toolbox">
    <param.appname>Zaber Motion Library</param.appname>
    <param.authnamewatermark>Zaber Technologies Inc.</param.authnamewatermark>
    <param.email>contact@zaber.com</param.email>
    <param.company>Zaber Technologies Inc.</param.company>
    <param.summary>The Zaber Motion Library aims to provide easy-to-use API for communication with Zaber devices.</param.summary>
    <param.description>The Zaber Motion Library aims to provide easy-to-use API for communication with Zaber devices.
API Reference: https://software.zaber.com/motion-library/api/matlab

For devices using Zaber ASCII Protocol:
Getting Started Guide: https://software.zaber.com/motion-library/docs/tutorials/code

For devices using Zaber Binary Protocol:
Getting Started Guide: https://software.zaber.com/motion-library/docs/binary</param.description>
    <param.screenshot>${PROJECT_ROOT}/logo_zaber.gif</param.screenshot>
    <param.version>7.5.0</param.version>
    <param.output>${PROJECT_ROOT}/Zaber Motion Library.mltbx</param.output>
    <param.products.name />
    <param.products.id />
    <param.products.version />
    <param.platforms />
    <param.guid>86ea3197-b131-4364-8650-dae8bacf51e4</param.guid>
    <param.exclude.filters>.gitignore
.DS_Store
*.gif
Test*
examples-internal
Zaber Motion Library.prj.template</param.exclude.filters>
    <param.exclude.pcodedmfiles>true</param.exclude.pcodedmfiles>
    <param.examples>&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;examples&gt;
   &lt;exampleCategory name="matlab"&gt;
      &lt;example name="Example_Ascii" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Ascii.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Ascii.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_Binary" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Binary.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Binary.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
   &lt;/exampleCategory&gt;
&lt;/examples&gt;</param.examples>
    <param.demosxml />
    <param.apps />
    <param.registered.apps />
    <param.docs />
    <param.getting.started.guide />
    <param.matlabpath.excludes />
    <param.javaclasspath.excludes />
    <param.exported.on.package>true</param.exported.on.package>
    <param.required.addons />
    <param.matlab.project.id />
    <param.matlab.project.name />
    <param.release.start>R2017b</param.release.start>
    <param.release.end>latest</param.release.end>
    <param.release.current.only>false</param.release.current.only>
    <param.compatiblity.windows>true</param.compatiblity.windows>
    <param.compatiblity.macos>true</param.compatiblity.macos>
    <param.compatiblity.linux>true</param.compatiblity.linux>
    <param.compatiblity.matlabonline>false</param.compatiblity.matlabonline>
    <param.installation.map />
    <param.additional.sw.names />
    <param.additional.sw.licenses />
    <param.additional.sw.win.url />
    <param.additional.sw.mac.url />
    <param.additional.sw.linux.url />
    <unset>
      <param.output />
      <param.products.name />
      <param.products.id />
      <param.products.version />
      <param.platforms />
      <param.exclude.pcodedmfiles />
      <param.demosxml />
      <param.apps />
      <param.registered.apps />
      <param.docs />
      <param.getting.started.guide />
      <param.matlabpath.excludes />
      <param.javaclasspath.excludes />
      <param.required.addons />
      <param.matlab.project.id />
      <param.matlab.project.name />
      <param.release.current.only />
      <param.compatiblity.windows />
      <param.compatiblity.macos />
      <param.compatiblity.linux />
      <param.installation.map />
      <param.additional.sw.names />
      <param.additional.sw.licenses />
      <param.additional.sw.win.url />
      <param.additional.sw.mac.url />
      <param.additional.sw.linux.url />
    </unset>
    <fileset.rootdir>
      <file>${PROJECT_ROOT}</file>
    </fileset.rootdir>
    <fileset.rootfiles>
      <file>${PROJECT_ROOT}/Example_Ascii.m</file>
      <file>${PROJECT_ROOT}/Example_Binary.m</file>
      <file>${PROJECT_ROOT}/LICENSE.txt</file>
      <file>${PROJECT_ROOT}/demos.xml</file>
      <file>${PROJECT_ROOT}/html</file>
      <file>${PROJECT_ROOT}/motion-library-jar-with-dependencies.jar</file>
    </fileset.rootfiles>
    <fileset.depfun.included />
    <fileset.depfun.excluded />
    <fileset.package />
    <build-deliverables>
      <file location="${PROJECT_ROOT}" name="Zaber Motion Library.mltbx" optional="false">~~PATH~~/matlab/Zaber Motion Library.mltbx</file>
    </build-deliverables>
    <workflow />
    <matlab>
      <root>/usr/local/MATLAB/R2019b</root>
      <toolboxes />
    </matlab>
    <platform>
      <unix>true</unix>
      <mac>false</mac>
      <windows>false</windows>
      <win2k>false</win2k>
      <winxp>false</winxp>
      <vista>false</vista>
      <linux>true</linux>
      <solaris>false</solaris>
      <osver>4.15.0-72-generic</osver>
      <os32>false</os32>
      <os64>true</os64>
      <arch>glnxa64</arch>
      <matlab>true</matlab>
    </platform>
  </configuration>
</deployment-project>
