javaaddpath(fullfile(pwd,'motion-library-jar-with-dependencies.jar'))

import zaber.motion.ascii.*;
import zaber.motion.*;

%% Test 1: Can call the library
Library.setLogOutput(LogOutputMode.STDERR);


%% Test 2: Can handle an exception
try
  Connection.openSerialPort('COMX');
  assert(0, 'No exception thrown');
catch exception
  assert(contains(exception.message, 'zaber.motion.exceptions.ConnectionFailedException'))
end
