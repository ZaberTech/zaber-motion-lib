/* eslint-disable camelcase */
import fs, { promises as fsp } from 'fs';
import path from 'path';
import os from 'os';

import _ from 'lodash';
import { series, parallel } from 'gulp';
import { argv } from 'yargs';
import { glob } from 'glob';
import { rimraf } from 'rimraf';
import { dir as tmpdir } from 'tmp-promise';
import hasha from 'hasha';
import { notNil } from '@zaber/toolbox';
import { compareVersions } from 'compare-versions';

import packageJson from './package.json';
import { exec, execOut, replaceInFile, isCommandAvailable } from './scripts/tools';
import { upgrade_all_dependencies } from './scripts/upgrade_dependencies';
import { prepare_maven_sign } from './scripts/maven_sign';
import { checkAwaitsCsharp } from './scripts/check_await';
import { checkPackageExports } from './scripts/check_exports_js';
import { collectLicenses } from './licensing/generate';
import { generateCard } from './scripts/gchat';


const REQUIRED_GO_VERSION = 'go1.23.';
const GO_LINTERS_VERSION = '1.64.6';

const PYTHON_VERSION = '3.8';
const PYTHON_VERSION_ALT = '3.11';
const ANY_PYTHON = os.platform() === 'win32' ? 'py -3' : 'python3';
const PYTHON_VENV = os.platform() === 'win32' ? 'CALL .venv\\Scripts\\activate.bat' : '. .venv/bin/activate';
const PYTHON_PDM = os.platform() === 'win32' ? `${ANY_PYTHON} -m pdm` : 'pdm';
const IS_WIN_ARM64 = os.platform() === 'win32' && os.arch() === 'arm64';

const MAX_BUFFER = 4 * 1024 * 1024;

const fileExists = async (path: string) => !!(await fsp.stat(path).catch(() => false));
const mkdirp = (dir: string) => fsp.mkdir(dir, { recursive: true });

if (!process.env.GOPATH) {
  console.log('set GOPATH');
  process.exit(1);
}

const isCI = !!process.env.CI_COMMIT_REF_NAME;
const isRelease = (process.env.CI_COMMIT_REF_NAME ?? '').match(/^(release|test)/i);

const checkIsRelease = () => {
  if (!isRelease) {
    throw new Error('This build is not release');
  }
};

const gen_code = async () => {
  await exec('cd templates && npm start');
  await gen_license();
};

const gen_license = () => collectLicenses();

const gen_from_device_db = async () => {
  await exec('cd generated && npm start');
};

const set_version_global = async () => {
  await exec(`npm --no-git-tag-version version "${argv.new_version}"`);
};

const check_go_version = async () => {
  const { stdout } = await execOut('go version');
  const goVersion = stdout.trim().split(' ')[2];
  if (!goVersion.startsWith(REQUIRED_GO_VERSION)) {
    throw new Error(`Go version ${goVersion} does not match required version ${REQUIRED_GO_VERSION}`);
  }
};

const LIB_EXTENSIONS = {
  windows: 'dll',
  linux: 'so',
  darwin: 'dylib',
} as Record<string, string>;


const build_go_temp = async (GOOS: string, GOARCH: string) => {
  let additionalArgs = '';
  if (isRelease) {
    // Remove the ldflags to enable debugging info in the runtime DLLs in a release.
    additionalArgs += ' -tags "release" --ldflags "-s -w"';
  }

  const ext = LIB_EXTENSIONS[GOOS];
  await exec(`go build -buildmode=c-shared -buildvcs=false -o ./build/zaber-motion-core-${GOOS}-${GOARCH}.${ext} ${additionalArgs}`, {
    env: {
      CGO_ENABLED: '1',
      GOOS,
      GOARCH,
      ...process.env,
    }
  });
};

const build_go_wasm = async () => {
  const webDistDir = path.join(__dirname, 'js', 'dist', 'binding', 'wasm');
  await mkdirp(webDistDir);

  const GOOS = 'js';
  const GOARCH = 'wasm';
  await exec('go build -buildvcs=false -o ./js/dist/binding/wasm/zaber-motion-core.wasm', {
    env: {
      GOOS,
      GOARCH,
      ...process.env,
    }
  });
};

const build_go_linux_386 = () => build_go_temp('linux', '386');
const build_go_linux_amd64 = () => build_go_temp('linux', 'amd64');
const build_go_linux = series(build_go_linux_386, build_go_linux_amd64);

const build_go_linux_arm = () => build_go_temp('linux', 'arm');
const build_go_linux_arm64 = () => build_go_temp('linux', 'arm64');

const build_go_windows_386 = () => build_go_temp('windows', '386');
const build_go_windows_amd64 = () => build_go_temp('windows', 'amd64');
const build_go_windows_arm64 = () => build_go_temp('windows', 'arm64');
const build_go_windows = async () => {
  const arch = os.arch();
  switch (arch) {
    case 'arm64':
      await build_go_windows_arm64();
      break;
    default:
      await build_go_windows_386();
      await build_go_windows_amd64();
      break;
  }
};

const build_go_darwin_amd64 = () => build_go_temp('darwin', 'amd64');
const build_go_darwin_arm64 = () => build_go_temp('darwin', 'arm64');

const build_go_darwin = async () => {
  await build_go_darwin_amd64();
  await build_go_darwin_arm64();

  await exec(
    'cd build && lipo -create -output zaber-motion-core-darwin-uni.dylib ' +
    'zaber-motion-core-darwin-amd64.dylib zaber-motion-core-darwin-arm64.dylib'
  );
};

const build_go = (() => {
  const platform = os.platform();
  const arch = os.arch();
  switch (platform) {
    case 'win32':
      return build_go_windows;
    case 'linux':
      if (arch === 'arm') {
        return build_go_linux_arm;
      } else if (arch == 'arm64') {
        return build_go_linux_arm64;
      }
      return build_go_linux;
    case 'darwin':
      return build_go_darwin;
    default:
      throw new Error('Unsupported OS');
  }
})();

const clean_go = async () => {
  await rimraf('build');
};

const test_go = async () => {
  const golint = `${process.env.GOPATH}/bin/golangci-lint${os.platform() === 'win32' ? '.exe' : ''}`;
  await exec(`${golint} run`);

  await exec('go test -v ./internal/...');
};

const set_version_go = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'internal', 'library', 'version.go'),
    from: /const version = ".*"/g,
    to: `const version = "${argv.new_version}"`,
  });
};


const build_py = async () => {
  await exec([
    'cd py',
    PYTHON_VENV,
    'invoke build'
  ].join(' && '));
};

const clean_py = async () => {
  await exec([
    'cd py',
    PYTHON_VENV,
    'invoke clean'
  ].join(' && '));
  await rimraf('py/.venv');
};

const test_py = async () => {
  await exec([
    'cd py',
    PYTHON_VENV,
    'invoke test lint'
  ].join(' && '));
};

const publish_py = async () => {
  checkIsRelease();

  await exec([
    'cd py',
    PYTHON_VENV,
    'invoke publish'
  ].join(' && '));
};
const publish_py_prod = async () => {
  checkIsRelease();

  await exec([
    'cd py',
    PYTHON_VENV,
    'invoke publish-prod'
  ].join(' && '));
};

const set_version_py = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'py', 'pyproject.toml'),
    from: /version = ".*"/g,
    to: `version = "${argv.new_version}"`,
  });
  await replaceInFile({
    files: path.join(__dirname, 'py', '**', 'version.py'),
    from: /__version__ = ".*"/g,
    to: `__version__ = "${argv.new_version}"`,
  });
};

/** Regenerates the Python lock file. */
const py_lock_dependencies = async () => {
  await exec([
    'cd py',
    `${PYTHON_PDM} lock --group dev`,
  ].join(' && '));
};

const buildWasm = os.arch() === 'x64';

const build_js = async () => {
  await exec('cd js && npm install');
  await exec('cd js && npm run build-all');
  if (isRelease && (os.arch() === 'x64' && os.platform() !== 'darwin')) {
    await exec('cd js && npm run build-bindings-ia32');
  }
  if (!isCI || (os.arch() === 'x64' && os.platform() === 'linux')) {
    await exec('cd js && npm run pack');
  }
  if (os.platform() === 'darwin' && os.arch() === 'arm64') {
    await exec('cd js && npm run build-bindings -- --arch=x64 --target_arch=x64');
  }
};

const clean_js = async () => {
  await exec('cd js && npm run clean');
};

const test_js = async () => {
  await exec('cd js && npm run lint && npm run test');

  /* The wasm tests don't work in gitlab runner windows docker because of bug in glob (dependency of karma). */
  const ignoreWasm = process.env.CI_RUNNER_TAGS?.includes('docker-executor-windows');
  if (buildWasm && !ignoreWasm) {
    const browser = os.platform() === 'linux' ? ' FirefoxHeadless' : 'ChromeHeadless';
    await exec(`cd js && npm run test-web -- --browsers ${browser}`);
  }

  await checkPackageExports();
};

const set_version_js = async () => {
  await exec(`cd js && npm --no-git-tag-version version "${argv.new_version}"`);
};

const check_is_latest_version_js = async () => {
  const latestVersion = await execOut('npm view @zaber/motion version');
  return compareVersions(packageJson.version, latestVersion.stdout.trim()) >= 0;
};

const publish_js = async () => {
  checkIsRelease();

  for (const [os, platform] of [
    ['win32', 'x64'],
    ['win32', 'ia32'],
    ['win32', 'arm64'],
    ['linux', 'x64'],
    ['linux', 'ia32'],
    ['linux', 'arm64'],
    ['linux', 'arm'],
    ['darwin', 'x64'],
    ['darwin', 'arm64'],
  ]) {
    await exec(`cd js && npm run publish-bindings -- --target_platform=${os} --target_arch=${platform}`);
  }

  const tag = await check_is_latest_version_js() ? 'next' : 'legacy';
  await exec(`cd js && npm publish zaber-motion*.tgz --tag ${tag} --access public`);
};

const publish_js_prod = async () => {
  checkIsRelease();
  if (await check_is_latest_version_js()) {
    await exec(`npm dist-tag add "@zaber/motion@${packageJson.version}" latest`);
  }
};

const build_cs = async () => {
  await exec('cd csharp && dotnet build --configuration Release');
};

const clean_cs = async () => {
  await exec('cd csharp && dotnet clean');
};

const test_cs = async () => {
  await checkAwaitsCsharp();
  await exec('cd csharp && dotnet test Zaber.Motion.Test');
};

// Create the NuGet package for C#.
// Relies on the build_cs targets having been run and the correct native libs
// being in the Go build directory.
// On success, package will be in csharp/Zaber.Motion/*.nupkg.
const sign_and_package_cs = async () => {
  try {
    await exec([
      'git clone --depth 1 --branch',
      process.env.KEYS_BRANCH ?? 'master',
      `${process.env.GIT_SERVER}/software-internal/software-keys.git keys`
    ].join(' '));

    const dotnetDlls = await glob('csharp/Zaber.Motion/bin/Release/*/Zaber.Motion.dll');
    const nativeDlls = await glob('build/zaber-motion-core-windows*.dll');

    const vsDevCmd = await getVsDevCmdBatch();
    const commands = [
      `call "${vsDevCmd}"`,
      ...dotnetDlls.map(file => `sn -R "${file}" "keys/keys/libs/zaberKeyFile.snk"`),
      ...dotnetDlls.concat(nativeDlls).map(file => [
        'signtool',
        'sign',
        '/fd certHash',
        '/csp "eToken Base Cryptographic Provider"',
        '/f "keys/keys/apps/sectigo2023.cer"',
        `/kc "[SafeNet Token JC 0{{${process.env.SIGNING_PASSWORD}}}]=Sectigo_20231102140616"`,
        '/t http://timestamp.sectigo.com/authenticode',
        `"${file}"`
      ].join(' ')),
    ];
    await exec(commands.join(' && '));

    await exec('cd csharp/Zaber.Motion && nuget.exe pack Zaber.Motion.nuspec -Symbols -SymbolPackageFormat snupkg');
  } finally {
    await rimraf('keys');
  }
};

const check_cs_packages = async (isRc: boolean) => {
  const monikers = ['net6.0', 'netstandard2.1'];
  const binPkg = path.join('csharp', 'Zaber.Motion', `Zaber.Motion.${packageJson.version}${isRc ? '-rc' : ''}.nupkg`);
  let tmpDir = await tmpdir({ unsafeCleanup: true });
  try {
    await exec(`tar -xf ${binPkg} -C ${tmpDir.path}/`);
    let hashes = await Promise.all(monikers.map(async tfm => {
      const expectedPath = path.join(tmpDir.path, 'build', tfm, 'Zaber.Motion.targets');
      if (!await fileExists(expectedPath)) {
        throw new Error(`NuGet package is missing custom build targets for ${tfm}`);
      }

      return await hasha.fromFile(expectedPath, { algorithm: 'md5' });
    }));

    if (_.sortedUniq(hashes).length !== 1) {
      throw new Error('NuGet package custom build targets are expected to all be identical.');
    }

    hashes = await Promise.all(monikers.map(async tfm => {
      const expectedPath = path.join(tmpDir.path, 'lib', tfm, 'Zaber.Motion.dll');
      if (!await fileExists(expectedPath)) {
        throw new Error(`NuGet package is missing ZML DLL for ${tfm}`);
      }

      if (!await fileExists(path.join(tmpDir.path, 'lib', tfm, 'Zaber.Motion.xml'))) {
        throw new Error(`NuGet package is missing XMLDoc for ${tfm}`);
      }

      return await hasha.fromFile(expectedPath, { algorithm: 'md5' });
    }));

    if (_.sortedUniq(hashes).length !== hashes.length) {
      throw new Error('NuGet package unexpectedly contains identical Zaber.Motion.dlls.');
    }

    const nativeDlls: string[] = [];
    nativeDlls.push(
      ...['386', 'amd64', 'arm', 'arm64']
        .map(plt => `runtimes/linux/native/zaber-motion-core-linux-${plt}.so`));
    nativeDlls.push('runtimes/osx/native/zaber-motion-core-darwin-uni.dylib');
    nativeDlls.push(
      ...['386', 'amd64', 'arm64']
        .map(plt => `runtimes/win/native/zaber-motion-core-windows-${plt}.dll`));

    await Promise.all(nativeDlls.map(async native => {
      if (!await fileExists(path.join(tmpDir.path, native))) {
        throw new Error(`NuGet package is missing native library ${native}`);
      }
    }));
  } finally {
    await tmpDir.cleanup();
  }

  const symPkg = path.join('csharp', 'Zaber.Motion', `Zaber.Motion.${packageJson.version}${isRc ? '-rc' : ''}.snupkg`);
  tmpDir = await tmpdir({ unsafeCleanup: true });
  try {
    await exec(`tar -xf ${symPkg} -C ${tmpDir.path}/`);
    await Promise.all(monikers.map(async tfm => {
      const expectedPath = path.join(tmpDir.path, 'lib', tfm, 'Zaber.Motion.pdb');
      if (!await fileExists(expectedPath)) {
        throw new Error(`NuGet package is missing debug symbols for ${tfm}`);
      }
    }));
  } finally {
    await tmpDir.cleanup();
  }

  console.log('NuGet package checks passed.');
};

const publish_cs_push = async () => {
  const packagespec = path.join('csharp', 'Zaber.Motion', 'Zaber.Motion*.nupkg');
  await exec([
    `nuget.exe push ${packagespec} ${process.env.NUGET_API_KEY}`,
    '-NonInteractive -source https://api.nuget.org/v3/index.json'
  ].join(' '));
};

const publish_cs = async () => {
  checkIsRelease();

  await sign_and_package_cs();
  await check_cs_packages(true);
  await publish_cs_push();
};

const publish_cs_prod = async () => {
  checkIsRelease();

  // strip -rc
  await set_version_cs_nuspec(packageJson.version);

  await sign_and_package_cs();
  await check_cs_packages(false);
  await publish_cs_push();
};

const set_version_cs_nuspec = async (version: string) => {
  await replaceInFile({
    files: path.join(__dirname, 'csharp', '**', '*.nuspec'),
    from: /<version>.*<\/version>/g,
    to: `<version>${version}</version>`,
  });
};

const set_version_cs_csproj = async (version: string) => {
  await replaceInFile({
    files: path.join(__dirname, 'csharp', '**', '*.csproj'),
    from: /<Version>.*<\/Version>/g,
    to: `<Version>${version}</Version>`,
  });
};

const set_version_cs = async () => {
  await set_version_cs_csproj(`${argv.new_version}`);
  await set_version_cs_nuspec(`${argv.new_version}-rc`);
};

const copy_dll_to_java_resources = async () => {
  const files = await glob('build/*+(.so|-uni.dylib|.dll)');

  const toDir = path.join(__dirname, 'java', 'src', 'main', 'resources', 'jne');
  await mkdirp(toDir);

  for (const file of files) {
    await fsp.copyFile(file, path.join(toDir, path.basename(file)));
  }
};

const build_java = async () => {
  await copy_dll_to_java_resources();

  await exec('cd java && mvn compile', { maxBuffer: MAX_BUFFER });
};

const test_java = async () => {
  await exec('cd java && mvn test-compile && mvn surefire:test');
};

const JAVA_SKIP_COMPILE = '-DskipTests -Dmaven.main.skip';

const package_java = async () => {
  await exec(`cd java && mvn ${JAVA_SKIP_COMPILE} package --quiet`);
};

const clean_java = async () => {
  await exec('cd java && mvn clean');
};

const publish_to_maven = async () => {
  await package_java();
  await exec(`cd java && mvn deploy ${JAVA_SKIP_COMPILE} -Prelease -Dgpg.passphrase=${process.env.MAVEN_KEY_PWD} --quiet`);
};

const publish_java = async () => {
  checkIsRelease();

  await publish_to_maven();
};

const publish_java_prod = async () => {
  checkIsRelease();

  // strip -rc
  await set_version_java_pom(packageJson.version);

  await publish_to_maven();
};

const set_version_java = async () => {
  await set_version_java_pom(`${argv.new_version}-RC`);
};

const set_version_java_pom = async (version: string) => {
  await exec(`cd java && mvn versions:set -DnewVersion=${version}`);
};

const set_version_examples_csharp = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'dev-portal', 'examples', 'csharp', 'zaber-example', 'zaber-example.csproj'),
    from: /<PackageReference Include="Zaber.Motion" Version=".*" \/>/g,
    to: `<PackageReference Include="Zaber.Motion" Version="${argv.new_version}" />`
  });
};

const set_version_example_swift = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'dev-portal', 'examples', 'swift', 'Package.swift'),
    from: /from: "\d+\.\d+\.\d+(-rc)?"/g,
    to: `from: "${argv.new_version}"`,
  });
};

const set_version_examples_java = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'dev-portal', 'examples', 'java', 'zaber_example', 'pom.xml'),
    from: /<zaber\.version>.*<\/zaber\.version>/g,
    to: `<zaber.version>${argv.new_version}</zaber.version>`,
  });
};

const getCppBuildDirs = () => ({
  main: path.join(__dirname, 'cpp', 'build'),
  alt: path.join(__dirname, 'cpp', 'build-alt'),
});

const getCmakeArgs = (arch: string) => [
  `-DZML_ARCH=${arch}`,
  `-DZML_BUILD_INSTALLERS=${isRelease ? 'ON' : 'OFF'}`,
  '-DZML_BUILD_TESTS=ON',
  `-DZML_BUILD_EXAMPLES=${process.env.ZML_BUILD_EXAMPLES ?? 'OFF'}`,
  `-DZML_LINT=${process.env.ZML_LINT ?? 'ON'}`,
].join(' ');

const generateDefinitionsAndLibFromDll = async (vsDevCmdBat: string, arch: string) => {
  const goArch = {
    x86: '386',
    x64: 'amd64',
    arm64: 'arm64',
  }[arch];

  await mkdirp('build');
  await fsp.writeFile('build/zaber-motion-core.def', [
    'EXPORTS',
    'zml_call',
    'zml_setEventHandler',
    ''
  ].join('\r\n'));

  const libCmd = [
    'lib',
    '/def:zaber-motion-core.def',
    `/out:zaber-motion-core-windows-${goArch}.lib`,
    `/machine:${arch.toUpperCase()}`,
  ].join(' ');

  await exec([
    `call "${vsDevCmdBat}"`,
    'cd build',
    libCmd,
  ].join(' && '));
};

async function getVsDevCmdBatch() {
  const globResults = await Promise.all(
    ['programfiles', 'programfiles(x86)']
      .map(programFiles => path.join(process.env[programFiles]!, 'Microsoft Visual Studio', '2022', '**', 'VsDevCmd.bat'))
      .map(pattern => glob(pattern, { windowsPathsNoEscape: true })));
  const batchFiles = _.flatten(globResults);
  if (batchFiles.length < 1) {
    throw new Error('Could not locate VsDevCmd.bat');
  }

  if (batchFiles.length > 1) {
    console.log('Warning: Found multiple copies of VsDevCmd.bat:\n', batchFiles);
  }
  console.log(`Using Visual Studio environment configured by ${batchFiles[0]}`);
  return batchFiles[0];
}

const build_cpp_visual_studio = async (buildDir: string, arch: string) => {
  await mkdirp(buildDir);

  const vsDevCmd = await getVsDevCmdBatch();

  await generateDefinitionsAndLibFromDll(vsDevCmd, arch);

  const cmakeArch = {
    x86: 'Win32',
    x64: 'x64',
    arm64: 'arm64',
  }[arch];

  await exec([
    `call "${vsDevCmd}"`,
    `cd "${buildDir}"`,
    `cmake -G "Visual Studio 17 2022" -A ${cmakeArch} ../ ${getCmakeArgs(arch)}`,
    'msbuild "Zaber Motion Library.sln" /p:Configuration=Debug /maxcpucount',
    isRelease && 'msbuild "Zaber Motion Library.sln" /p:Configuration=Release /maxcpucount',
    isRelease && 'cpack',
  ].filter(cmd => !!cmd).join(' && '), { maxBuffer: MAX_BUFFER });
};

const build_cpp_mingw = async () => {
  const buildDir = getCppBuildDirs().alt;
  const arch = os.arch();

  await mkdirp(buildDir);

  await exec([
    `cd "${buildDir}"`,
    `cmake -G "MinGW Makefiles" ../ ${getCmakeArgs(arch)}`,
    'mingw32-make -j',
  ].filter(cmd => !!cmd).join(' && '), { maxBuffer: MAX_BUFFER });
};

const build_cpp_unix = async (buildDir: string, arch: string, alterCmd?: (cmd: string) => string) => {
  await mkdirp(buildDir);

  let cmd = [
    `cd ${buildDir}`,
    `cmake ../ ${getCmakeArgs(arch)}`,
    `make -j${os.cpus().length}`,
    isRelease && 'cpack',
  ].filter(cmd => !!cmd).join(' && ');

  if (alterCmd) {
    cmd = alterCmd(cmd);
  }

  await exec(cmd, { maxBuffer: MAX_BUFFER });
};

const build_cpp = async () => {
  const buildDirs = getCppBuildDirs();
  const binFolderPrefix = 'cpp/build/binaries';

  if (os.platform() == 'win32') {
    if (os.arch() === 'arm64') {
      await build_cpp_visual_studio(buildDirs.main, 'arm64');
      await copy_lib_artifacts_windows(buildDirs.main, `${binFolderPrefix}_win_arm64`);
    } else {
      await build_cpp_visual_studio(buildDirs.main, 'x64');
      await copy_lib_artifacts_windows(buildDirs.main, `${binFolderPrefix}_win_x64`);

      if (isRelease) {
        await build_cpp_visual_studio(buildDirs.alt, 'x86');
        await copy_lib_artifacts_windows(buildDirs.alt, `${binFolderPrefix}_win_x86`);

        const installers = await glob(path.join(buildDirs.alt, 'ZaberMotionCppInstaller*'), { windowsPathsNoEscape: true });
        for (const installer of installers) {
          await fsp.copyFile(installer, path.join(buildDirs.main, path.basename(installer)));
        }
      }
    }
  } else {
    if (isRelease && os.platform() === 'darwin' && os.arch() === 'arm64') {
      await build_cpp_unix(buildDirs.alt, 'x64', cmd => `arch -x86_64 zsh -c '${cmd}'`);
      await copy_lib_artifacts_unix(buildDirs.alt, `${binFolderPrefix}_${os.platform()}_x64`);
    }
    await build_cpp_unix(buildDirs.main, os.arch());
    await copy_lib_artifacts_unix(buildDirs.main, `${binFolderPrefix}_${os.platform()}_${os.arch()}`);
  }
};

const copy_lib_artifacts = async (libPath: string, libs: string[], binFolder: string) => {
  await fsp.rm(binFolder, { recursive: true }).catch(() => false);
  await fsp.mkdir(binFolder, { recursive: true });

  await Promise.all(libs.map(async lib =>
    await fsp.cp(path.join(libPath, lib), path.join(binFolder, lib), { verbatimSymlinks: true })
  ));
};

const copy_lib_artifacts_unix = async (libPath: string, binFolder: string) => {
  const libs = await glob('libzaber-motion*', { cwd: libPath });
  await copy_lib_artifacts(libPath, libs, binFolder);
};

const copy_lib_artifacts_windows = async (libPath: string, binFolder: string) => {
  const libs = await glob(
    ['Release/zaber-motion*', 'Debug/zaber-motion*'],
    { cwd: libPath, ignore: ['**/*.exp', '**/*.pdb'] }
  );
  await copy_lib_artifacts(libPath, libs, binFolder);
};

const clean_cpp = async () => {
  for (const buildDir of _.values(getCppBuildDirs())) {
    await rimraf(buildDir);
  }

  const files = await glob('ZaberMotionCpp*');
  await Promise.all(files.map(file => rimraf(file)));
};

const test_cpp = async () => {
  const buildDirs = getCppBuildDirs();
  const testDirs = [buildDirs.main];
  if (isRelease && (os.platform() == 'win32' && os.arch() == 'x64')) {
    testDirs.push(buildDirs.alt);
  }

  for (const testDir of testDirs) {
    await exec(`cd ${testDir} && ctest --verbose -C Debug`);
    if (isRelease) {
      await exec(`cd ${testDir} && ctest --verbose -C Release`);
    }
  }
};

const publish_cpp_src = async (version: string, toFolder: string) => {
  checkIsRelease();

  const commands = [
    'mkdir ZaberMotionCppSource',
    'cp -r build ZaberMotionCppSource',
    'rsync -av --exclude=\'cpp/build\' cpp ZaberMotionCppSource',
    `zip -r ZaberMotionCppSource-${version}.zip ZaberMotionCppSource`,
  ];
  await exec(commands.join(' && '));
  await exec(`aws s3 cp ZaberMotionCppSource-${version}.zip ${process.env.BUCKET}/${toFolder}`);
};

const publish_cpp_support_pkg = async (version: string, toFolder: string) => {
  checkIsRelease();

  // include
  const srcDir = 'cpp/src';
  const headers = await glob('zaber/motion/**/*.h', { cwd: srcDir, ignore: 'zaber/motion/**/*.private.h' });
  const includeDir = path.join('ZaberMotionCppSupport', 'include');
  await fsp.rm(includeDir, { recursive: true }).catch(() => false);
  await fsp.mkdir(includeDir, { recursive: true });
  await Promise.all(headers.map(async header =>
    await fsp.cp(path.join(srcDir, header), path.join(includeDir, header), { recursive: true })
  ));

  // lib
  const buildDir = 'cpp/build';
  const libFolders = await glob('binaries*', { cwd: buildDir });
  const libDir = path.join('ZaberMotionCppSupport', 'lib');
  await fsp.rm(libDir, { recursive: true }).catch(() => false);
  await fsp.mkdir(libDir, { recursive: true });
  await Promise.all(libFolders.map(async libFolder => {
    const destPath = path.join(libDir, libFolder.replace('binaries_', ''));
    await fsp.cp(path.join(buildDir, libFolder), destPath, { recursive: true, verbatimSymlinks: true });
  }));

  // zip -y to preserve symlinks
  await exec(`zip -ry ZaberMotionCppSupport-${version}.zip ZaberMotionCppSupport`);
  await exec(`aws s3 cp ZaberMotionCppSupport-${version}.zip ${process.env.BUCKET}/${toFolder}`);
};


const publish_cpp = async () => {
  checkIsRelease();

  const files = await glob('cpp/build/ZaberMotionCppInstaller*');
  files.forEach(file => fs.renameSync(file, file.replace(/-.*-/g, `-${packageJson.version}-rc-`)));

  // Should have been packaged in the build step
  const toFolder = `downloads/ZML/CPP/${packageJson.version}-rc/`;
  await exec(`aws s3 cp cpp/build/ ${process.env.BUCKET}/${toFolder} --recursive --exclude "*" --include "ZaberMotionCppInstaller*"`);
  await publish_cpp_src(`${packageJson.version}-rc`, toFolder);
  await publish_cpp_support_pkg(`${packageJson.version}-rc`, toFolder);
};

const publish_cpp_prod = async () => {
  checkIsRelease();

  // Should have been packaged in the build step
  const toFolder = `downloads/ZML/CPP/${packageJson.version}/`;
  await exec(`aws s3 cp cpp/build/ ${process.env.BUCKET}/${toFolder} --recursive --exclude "*" --include "ZaberMotionCppInstaller*"`);
  await publish_cpp_src(packageJson.version, toFolder);
  await publish_cpp_support_pkg(packageJson.version, toFolder);
};

const set_version_cpp = async () => {
  const versions = argv.new_version!.split('.');

  await replaceInFile({
    files: path.join(__dirname, 'cpp', 'src', 'zaber', 'motion', 'version.h'),
    from: [
      /#define ZML_VERSION_MAJOR.*$/gm,
      /#define ZML_VERSION_MINOR.*$/gm,
      /#define ZML_VERSION_PATCH.*$/gm,
      /#define ZML_VERSION_STR ".*"/g
    ],
    to: [
      `#define ZML_VERSION_MAJOR ${versions[0]}`,
      `#define ZML_VERSION_MINOR ${versions[1]}`,
      `#define ZML_VERSION_PATCH ${versions[2]}`,
      `#define ZML_VERSION_STR "${argv.new_version}"`
    ]
  });

  await replaceInFile({
    files: path.join(__dirname, 'cpp', 'CMakeLists.txt'),
    from: /set\( ZML_VERSION ".*" \)/g,
    to: `set( ZML_VERSION "${argv.new_version}" )`,
  });
};

const clean_matlab = async () => {
  await rimraf(path.join(__dirname, 'matlab', 'motion-library-jar-with-dependencies.jar'));
  await rimraf(path.join(__dirname, 'matlab', 'Zaber Motion Library.mltbx'));
  await rimraf(path.join(__dirname, 'matlab', 'Zaber Motion Library.prj'));
  await rimraf(path.join(__dirname, 'matlab', 'html'));
};

const set_matlab_project_paths = async () => {
  const projectFile = path.join(__dirname, 'matlab', 'Zaber Motion Library.prj');
  await fsp.copyFile(`${projectFile}.template`, projectFile);

  await replaceInFile({
    files: projectFile,
    from: /~~PATH~~/g,
    to: __dirname,
  });
};

const run_matlab = async (cmd: string) => {
  await exec(
    `matlab -nodisplay -nosplash -nodesktop -r "${cmd}" -wait`,
    { cwd: path.join(__dirname, 'matlab') }
  );
};

const SINGLE_JAR_NAME = 'motion-library-jar-with-dependencies.jar';

const build_matlab = async () => {
  await copy_dll_to_java_resources();

  await exec('cd java && mvn compile assembly:single');

  const fromPath = path.join(__dirname, 'java', 'target', SINGLE_JAR_NAME);
  const toPath = path.join(__dirname, 'matlab', SINGLE_JAR_NAME);

  // rename internal dependencies to avoid conflict with matlab libraries
  await exec(`java -jar tools/jarjar/jarjar-1.4.jar process tools/jarjar/rules.txt "${fromPath}" "${toPath}"`);

  await set_matlab_project_paths();

  const projectFile = 'Zaber Motion Library.prj';
  await run_matlab(`matlab.addons.toolbox.packageToolbox('${projectFile}'); exit;`);
};

const test_matlab = async () => {
  const cmd = [
    'result = runtests(\'Test_Integration\');',
    'if (sum(vertcat(result.Failed)) > 0); exit(1); else; exit(0); end;',
  ].join(' ');
  await run_matlab(cmd);
};

const publish_matlab_prod = async () => {
  checkIsRelease();

  const toFolder = `downloads/ZML/MATLAB/${packageJson.version}/`;
  await exec(`aws s3 cp "matlab/Zaber Motion Library.mltbx" ${process.env.BUCKET}/${toFolder}`);
};

const publish_octave_prod = async () => {
  checkIsRelease();

  const toFolder = `downloads/ZML/jar/${packageJson.version}/`;
  await exec(`aws s3 cp "matlab/${SINGLE_JAR_NAME}" ${process.env.BUCKET}/${toFolder}`);
};

const set_version_matlab = async () => {
  await replaceInFile({
    files: path.join(__dirname, 'matlab', '*.prj.template'),
    from: /<param.version>.*<\/param.version>/g,
    to: `<param.version>${argv.new_version}</param.version>`,
  });
};

const build_swift = async () => {
  const dyLibName = `libzaber-motion-core.${packageJson.version}.dylib`;
  const xcFrameworkName = 'ZaberMotionCore.xcframework';

  await run_swift_job(async () => {
    await exec(`cp build/zaber-motion-core-darwin-uni.dylib swift/ZaberMotionCore/${dyLibName}`);
    await exec(`cd swift/ZaberMotionCore && install_name_tool -id @rpath/${dyLibName} ${dyLibName}`);
    await exec(
      'cd swift/ZaberMotionCore && ' +
      `xcodebuild -create-xcframework -library ${dyLibName} -headers Headers -output ${xcFrameworkName} && ` +
      `zip -r ${xcFrameworkName}.zip ${xcFrameworkName}`
    );
    await exec(`cd swift/ZaberMotionCore && rm ${dyLibName} && rm -r ${xcFrameworkName}`);
    await exec('cd swift && swift build');
  });

  if (isCI) {
    await build_swift_examples();
  }
};

const test_swift = async () => {
  await run_swift_job(exec.bind(null, 'cd swift && swift test --no-parallel'));
};

const build_swift_examples = async () => {
  const examplesDir = path.join(__dirname, 'swift', 'Examples');
  const entries = await fsp.readdir(examplesDir, { withFileTypes: true });
  await run_swift_job(async () => {
    for (const entry of entries) {
      if (entry.isDirectory()) {
        await exec(`cd ${path.join('swift', 'Examples', entry.name)} && swift build`);
      }
    }
  });
};

const SWIFT_REPO_NAME = 'zaber-motion-lib-swift';

const publish_swift = async () => {
  await run_swift_job(async () => {
    await get_swift_github_repo();
    await rimraf(`temp/${SWIFT_REPO_NAME}/*`, { glob: true });
    await rimraf(`temp/${SWIFT_REPO_NAME}/.gitignore`);
    await exec(`cp -r swift/* temp/${SWIFT_REPO_NAME}`);
    await exec(`cp swift/.gitignore temp/${SWIFT_REPO_NAME}`);
    await upload_swift_binary(packageJson.version);
    await update_swift_package_dependency(packageJson.version);
    await exec(`cd temp/${SWIFT_REPO_NAME} && git add .`);

    await exec(`cd temp/${SWIFT_REPO_NAME} && git commit -m "${packageJson.version}"`)
      .then(() =>
        exec(
          `cd temp/${SWIFT_REPO_NAME} && git tag -a "${packageJson.version}-rc" -m "${packageJson.version}-rc" && git push --follow-tags`
        )
      ).catch(() => {
        throw new Error('Failed to publish Swift. No changes detected in the repository.');
      });
  });
};

const upload_swift_binary = async (version: string) => {
  const fromFile = path.join(__dirname, 'temp', SWIFT_REPO_NAME, 'ZaberMotionCore', 'ZaberMotionCore.xcframework.zip');
  const toFile = `downloads/ZML/Swift/${version}/ZaberMotionCore.xcframework.zip`;
  await exec(`aws s3 cp ${fromFile} ${process.env.BUCKET}/${toFile}`);
};

const update_swift_package_dependency = async (version: string) => {
  const xcFrameworkPath = path.join(__dirname, 'temp', SWIFT_REPO_NAME, 'ZaberMotionCore', 'ZaberMotionCore.xcframework.zip');
  const url = `https://software.zaber.com/downloads/ZML/Swift/${version}/ZaberMotionCore.xcframework.zip`;
  const checksumResult = await execOut(`swift package compute-checksum ${xcFrameworkPath}`);
  const checksum = checksumResult.stdout.trim();

  await replaceInFile({
    files: path.join(__dirname, 'temp', SWIFT_REPO_NAME, 'Package.swift'),
    from: /path: "ZaberMotionCore\/ZaberMotionCore.xcframework\.zip"/g,
    to: `url: "${url}",
        checksum: "${checksum}"`,
  });
};

const publish_swift_prod = async () => {
  await run_swift_job(async () => {
    await get_swift_github_repo();
    await exec(`cd temp/${SWIFT_REPO_NAME} && git checkout tags/${packageJson.version}-rc`);
    await exec(`cd temp/${SWIFT_REPO_NAME} && git tag -a "${packageJson.version}" -m "${packageJson.version}"`);
    await exec(`cd temp/${SWIFT_REPO_NAME} && git push origin tag ${packageJson.version}`);
  });
};

const get_swift_github_repo = async () => {
  const keyPath = path.join(__dirname, '.secure_files', 'github-zabermotionlib-swift-key');
  await exec(`chmod 600 ${keyPath}`);
  await mkdirp('temp');
  await exec(`cd temp && git clone -c core.sshCommand="/usr/bin/ssh -i ${keyPath}" git@github.com:zabertech/${SWIFT_REPO_NAME}.git`);
};

const clean_swift = async () => {
  const buildDirs = await glob(path.join(__dirname, 'swift', '**', '.build'));
  for (const dir of buildDirs) {
    await rimraf(dir);
  }
  await rimraf(path.join(__dirname, 'swift', 'ZaberMotionCore', 'ZaberMotionCore.xcframework.zip'));
  await rimraf(path.join(__dirname, 'temp', SWIFT_REPO_NAME));
};

const run_swift_job = async (job: () => Promise<void>) => {
  switch (os.platform()) {
    case 'darwin':
      await job();
      break;
    case 'linux':
      console.log('Warning: Swift builds are not currently supported on Linux');
      break;
    case 'win32':
      console.log('Warning: Swift builds are not currently supported on Windows');
      break;
  }
};

const test_tooling = async () => {
  await exec('npm run lint');

  await exec('cd templates && npm run check');
  await exec('cd generated && npm run check');
};

const clean = series(clean_go, clean_js, clean_py, clean_cs, clean_java, clean_cpp, clean_matlab, clean_swift);

const build = series(...[
  check_go_version,
  build_go,
  buildWasm ? build_go_wasm : null,
  build_js,
  build_py,
  build_cs,
  !IS_WIN_ARM64 ? build_java : null,
  build_cpp,
  build_swift,
].filter(notNil));

const test = series(...[
  test_tooling,
  test_go,
  test_js,
  test_py,
  test_cs,
  !IS_WIN_ARM64 ? test_java : null,
  test_cpp,
  test_swift,
].filter(notNil));

const set_version = parallel(set_version_global, set_version_js, set_version_py,
  set_version_cs, set_version_go, set_version_java, set_version_matlab,
  set_version_examples_java, set_version_examples_csharp, set_version_example_swift,
  set_version_cpp, gen_code);

const dependencies = async () => {
  await exec('go mod download');

  for (const packageStr of [
    `github.com/golangci/golangci-lint/cmd/golangci-lint@v${GO_LINTERS_VERSION}`,
  ]) {
    await exec(`go install ${packageStr}`);
  }

  let pythonVersion = PYTHON_VERSION;
  if (IS_WIN_ARM64) {
    pythonVersion = PYTHON_VERSION_ALT;
  }

  let py_commands = [
    'cd py',
    `${PYTHON_PDM} venv create --force ${pythonVersion}`,
    `${PYTHON_PDM} install --dev`,
  ];

  // avoid externally managed environment error
  if (os.platform() == 'darwin' || os.platform() == 'linux') {
    const pdmInstalled = await isCommandAvailable('pdm');
    if (!pdmInstalled) {
      console.log('Error: pdm is not installed');
      console.log(`On ${os.platform()}, users must install python package manager pdm themselves.`);
      if (os.platform() == 'darwin') {
        console.log('Recommended installation method is homebrew:');
        console.log('First install homebrew package manager, then run: brew install pdm');
      } else {
        console.log('Installation instructions here: https://github.com/pdm-project/pdm');
      }
      throw new Error('macOS and linux users must install pdm manually');
    }
  } else {
    py_commands = [`${ANY_PYTHON} -m pip install --user pdm`, ...py_commands];
  }

  await exec(py_commands.join(' && '), { env: { ...process.env, SETUP_SKIP_ZML_BINDINGS: '1' } });

  await exec('cd js && npm install --ignore-scripts');

  await exec('cd templates && npm install');
  await exec('cd generated && npm install');
};

const copy_readme = async () => {
  const readmePath = path.join(__dirname, 'README.md');
  for (const destination of [
    path.join(__dirname, 'js', 'README.md'),
    path.join(__dirname, 'py', 'README.md'),
    path.join(__dirname, 'py', 'DESCRIPTION.md'),
    path.join(__dirname, 'csharp', 'Zaber.Motion', 'README.md'),
    path.join(__dirname, 'cpp', 'README.md'),
  ]) {
    await fsp.copyFile(readmePath, destination);
  }
};

const broadcast = async () => {
  const changelog = getChangelog(packageJson.version);
  const card = generateCard(packageJson.version, changelog);

  const webhookUrls = process.env.GCHAT_WEBHOOK_URLS?.split(' ');
  if (webhookUrls) {
    for (const webhookUrl of webhookUrls) {
      await fetch(webhookUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
        body: JSON.stringify(card)
      });
    }
  }
};

const getChangelog = (version: string) => {
  const changelog = fs.readFileSync(path.join(__dirname, 'CHANGELOG.md'), 'utf-8');
  const versionIndex = changelog.indexOf(version);
  const lastChangeStartIndex = changelog.indexOf('*', versionIndex);
  const lastChangeEndIndex = changelog.indexOf('##', versionIndex);
  return changelog.slice(lastChangeStartIndex, lastChangeEndIndex).trim();
};

module.exports = {
  build,
  build_go_windows_386,
  build_go_windows_amd64,
  build_go_windows_arm64,
  build_go_windows,
  build_go_linux_386,
  build_go_linux_amd64,
  build_go_linux,
  build_go_darwin,
  build_go_darwin_amd64,
  build_go_darwin_arm64,
  build_go_wasm,
  build_go,
  build_py,
  build_js,
  build_cs,
  build_java,
  build_cpp,
  build_cpp_mingw,
  build_matlab,
  build_swift,
  build_swift_examples,
  publish_js,
  publish_js_prod,
  publish_cs,
  publish_cs_prod,
  publish_py,
  publish_py_prod,
  publish_java,
  publish_java_prod,
  publish_matlab_prod,
  publish_octave_prod,
  publish_cpp,
  publish_cpp_prod,
  publish_swift,
  publish_swift_prod,
  clean,
  clean_go,
  clean_js,
  clean_py,
  clean_cs,
  clean_java,
  clean_cpp,
  clean_matlab,
  clean_swift,
  dependencies,
  test,
  test_java,
  test_go,
  test_cs,
  test_py,
  test_tooling,
  test_js,
  test_cpp,
  test_matlab,
  test_swift,
  set_version,
  gen_from_device_db,
  gen_code,
  copy_readme,
  gen_license,
  upgrade_all_dependencies,
  prepare_maven_sign,
  py_lock_dependencies,
  broadcast,
  check_is_latest_version_js,
};
