FROM registry.izaber.com/software-public/zaber-motion-lib/linux_amd64

RUN mkdir -p /home/ubuntu/go/src/zaber-motion-lib
WORKDIR /home/ubuntu/go/src/zaber-motion-lib

ARG CI_COMMIT_REF_NAME
COPY --chown=ubuntu:root . .
RUN npm install --omit=dev
RUN npx gulp dependencies
RUN npx gulp build
RUN npx gulp test
