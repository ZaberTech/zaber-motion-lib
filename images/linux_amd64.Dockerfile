FROM ubuntu:20.04

WORKDIR /opt

ENV TZ=America/Vancouver
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y bash curl wget zip

# CIFS support for network drives
RUN apt-get install -y keyutils cifs-utils rsync

# AWS CLI
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip && ./aws/install

# C++
RUN apt-get install -y build-essential gcc-multilib g++-multilib cmake git clang clang-tidy

# Js
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && apt-get install -y nodejs
RUN npm install -g npm@8.19.4
RUN apt-get install -y firefox

# Go
RUN curl -L https://go.dev/dl/go1.23.6.linux-amd64.tar.gz --output go.tar.gz && tar -xf go.tar.gz
ENV PATH="${PATH}:/opt/go/bin"
RUN go version

# .NET
RUN wget https://dot.net/v1/dotnet-install.sh -O dotnet-install.sh && chmod +x ./dotnet-install.sh
RUN ./dotnet-install.sh --install-dir /opt/dotnet --channel 6.0
ENV DOTNET_ROOT=/opt/dotnet
ENV PATH="${PATH}:${DOTNET_ROOT}:${DOTNET_ROOT}/tools"
RUN dotnet --version

# Java
RUN apt-get install -y openjdk-11-jdk-headless
RUN curl -L https://dlcdn.apache.org/maven/maven-3/3.9.6/binaries/apache-maven-3.9.6-bin.tar.gz --output maven.tar.gz && tar -xf maven.tar.gz
ENV PATH="${PATH}:/opt/apache-maven-3.9.6/bin"
RUN mvn --version

# Python
RUN apt-get install -y zlib1g-dev libssl-dev libbz2-dev libreadline-dev libsqlite3-dev libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
RUN curl -L https://www.python.org/ftp/python/3.8.16/Python-3.8.16.tgz --output python.tgz && tar zxvf python.tgz
RUN cd Python-3.8.16 && ./configure --with-ensurepip=install --enable-optimizations && make -j8 && make install
RUN python3 --version
RUN python3 -m pip install pdm
RUN pdm --version

# add regular user
RUN apt-get install -y sudo
RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1001 ubuntu
RUN echo "ubuntu ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER ubuntu

# Force git to ignore repo ownership
RUN git config --global --add safe.directory '*'

ENV GOPATH=/home/ubuntu/go
