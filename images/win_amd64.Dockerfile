# escape=`

FROM mcr.microsoft.com/windows:20H2

USER ContainerAdministrator
WORKDIR "C:\"

# https://learn.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2022
SHELL ["cmd", "/S", "/C"]
RUN `
    curl -SL --output vs_buildtools.exe https://aka.ms/vs/17/release/vs_buildtools.exe `
    `
    && (start /w vs_buildtools.exe --quiet --wait --norestart --nocache `
        --includeRecommended --includeOptional `
        --add Microsoft.VisualStudio.Workload.ManagedDesktopBuildTools `
        --add Microsoft.VisualStudio.Workload.MSBuildTools `
        --add Microsoft.VisualStudio.Workload.VCTools `
        --add Microsoft.NetCore.Component.Runtime.6.0 `
        --add Microsoft.Net.Component.4.8.SDK `
        || IF "%ERRORLEVEL%"=="3010" EXIT 0) `
    `
    && del /q vs_buildtools.exe

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# I wanted to use winget but it just does not work in docker, it's a shame.
# Chocolatey install
RUN Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

RUN choco install git --version 2.44.0 -y
RUN choco install python --version 3.8.10 -y
RUN choco install nodejs --version 18.18.2 -y
RUN npm i -g npm@latest
RUN choco install golang --version 1.23.6 -y
RUN choco install oraclejdk --version 17.0.2 -y
RUN choco install 7zip --version 23.1.0 -y
RUN choco install maven --version 3.9.6 -y
RUN choco install cmake --version 3.28.3 -y; `
    $env:PATH += ';C:\Program Files\CMake\bin'; `
    [Environment]::SetEnvironmentVariable('PATH', $env:Path, [EnvironmentVariableTarget]::Machine)
RUN choco install googlechrome -y
RUN choco install nsis --version 3.09 -y

# GCC for cgo
# from: https://github.com/jmeubank/tdm-gcc-installer/issues/4
RUN Invoke-WebRequest -Uri 'https://github.com/jmeubank/tdm-gcc/releases/download/v10.3.0-tdm64-2/tdm64-gcc-10.3.0-2.exe' -O 'C:\TDM-GCC-64-setup.exe'
RUN mkdir 'C:\TDM-GCC-64'; `
    Start-Process 7z -ArgumentList 'e C:\TDM-GCC-64-setup.exe -oC:\TDM-GCC-64 -y' -Wait; `
    Start-Process 7z -ArgumentList 'e C:\TDM-GCC-64\*.tar.xz -oC:\TDM-GCC-64 -y' -Wait; `
    Start-Process 7z -ArgumentList 'x C:\TDM-GCC-64\*.tar -oC:\TDM-GCC-64 -y' -Wait; `
    Remove-Item 'C:\TDM-GCC-64\*' -Include *.tar.xz, *.tar -Force; `
    Remove-Item -Path 'C:\TDM-GCC-64-setup.exe' -Force; `
    $env:PATH += ';C:\TDM-GCC-64\bin'; `
    [Environment]::SetEnvironmentVariable('PATH', $env:Path, [EnvironmentVariableTarget]::Machine)
RUN gcc --version

USER ContainerUser

# Fix for cmake git clone recipe
RUN git config --global --add safe.directory '*'

RUN mkdir -p 'C:\go'; setx GOPATH 'C:\go'
