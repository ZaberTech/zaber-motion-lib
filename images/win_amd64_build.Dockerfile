# escape=`

FROM registry.izaber.com/software-public/zaber-motion-lib/win_amd64

RUN mkdir -p 'C:\build'
WORKDIR "C:\build"

COPY . .
RUN npm install --omit=dev
RUN npx gulp dependencies
RUN npx gulp build
RUN npx gulp test
