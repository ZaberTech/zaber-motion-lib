# Docker images

These are the images used to build ZML on gitlab. Each of them must be build on the specific platform and then pushed to gitlab.

## Commands

Building and pushing the image:

```sh
docker build -f images/linux_amd64.Dockerfile -t registry.izaber.com/software-public/zaber-motion-lib/linux_amd64 .
docker push registry.izaber.com/software-public/zaber-motion-lib/linux_amd64
```

You can build for a different platform with the `--platform` flag like:
```
docker build -f images/linux_arm64.Dockerfile -t registry.izaber.com/software-public/zaber-motion-lib/linux_arm64 --platform linux/arm64 .
docker build -f images/linux_arm.Dockerfile -t registry.izaber.com/software-public/zaber-motion-lib/linux_arm --platform linux/arm/v7 .
```

Testing the image:

```sh
docker build -f images/linux_amd64_build.Dockerfile -t zml .
```

## Troubleshooting

### Exit Code 159

```
ERROR: Job failed: adding cache volume: set volume permissions: running permission container "ec7c4ffc93c986ba69556a41dc6eaa0fe172ea33a62159546655172310fd7689" for volume "runner-4sakuz83-project-307-concurrent-0-cache-3c3f060a0374fc8bc39395164f415a70": waiting for permission container to finish: exit code 159
```

If you get this omnious error, it's because giltab runner for armv7l has a bug in it. It tries to run arm64 image on 32bit armv7l.
You must go to `config.toml` and overwrite the helper image.

```toml
helper_image = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:arm-v16.9.0"
```

You should see this in the build log after (notice the architecture):

```
Using helper image:  registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:arm-v16.9.0  (overridden, default would be  registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:arm64-v16.9.0 )
```

### Access Denied

If you see `denied: requested access to the resource is denied`, it means you will need to log in by running:

```
docker login registry.izaber.com -u "yourname@zaber.com"
```
