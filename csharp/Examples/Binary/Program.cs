﻿using System;
using System.Threading;
using Zaber.Motion;
using Zaber.Motion.Binary;

namespace Examples.Binary
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Run();
            Console.WriteLine("Press return to exit.");
            Console.ReadLine();
        }

        private static void Run()
        {
            Console.WriteLine("Scanning devices...");

            using (var conn = Connection.OpenSerialPort("COM3"))
            {
                var devices = conn.DetectDevices();
                Console.WriteLine($"Detected {devices.Length}.");

                var device = devices[0];
                Console.WriteLine($"Device {device.DeviceAddress} has device ID {device.Identity.DeviceId}.");

                var pos = device.Home(Units.Length_Centimetres);
                Console.WriteLine($"Position after home: {pos} cm.");

                pos = device.MoveAbsolute(1.0, Units.Length_Centimetres);
                Console.WriteLine($"Position after move absolute: {pos} cm.");

                pos = device.MoveRelative(5.0, Units.Length_Millimetres);
                Console.WriteLine($"Position after move relative: {pos} mm.");

                var velocity = device.MoveVelocity(1.0, Units.Velocity_MillimetresPerSecond);
                Console.WriteLine($"Starting move velocity with speed: {velocity} mm/s.");

                Thread.Sleep(2000);

                pos = device.Stop(Units.Length_Centimetres);
                Console.WriteLine($"Position after stop: {pos} cm.");

                Console.WriteLine($"Final position in microsteps: {device.GetPosition()}.");
            }
        }
    }
}
