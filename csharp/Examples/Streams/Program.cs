﻿using System;
using System.Threading;

using Zaber.Motion;
using Zaber.Motion.Ascii;

namespace Examples.Streams
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            Library.SetLogOutput(LogOutputMode.Stdout);
            Library.SetDeviceDbSource(DeviceDbSourceType.WebService, "https://api.zaber.io/device-db/master");

            using (var comm = Connection.OpenTcp("localhost", 11321))
            {
                var devices = comm.DetectDevices();
                var device = devices[0];

                device.AllAxes.Home();

                var numStreams = device.Settings.Get("stream.numstreams");
                Console.WriteLine($"Number of streams possible: {numStreams}");

                var stream = device.Streams.GetStream(1);

                var streamBuffer = device.Streams.GetBuffer(1);
                streamBuffer.Erase();

                stream.SetupStore(streamBuffer, 1, 2);

                stream.LineAbsolute(
                    new Measurement { Value = 29.0047, Unit = Units.Length_Millimetres },
                    new Measurement { Value = 40.49, Unit = Units.Length_Millimetres }
                );

                stream.LineRelative(
                    new Measurement { Value = 0 },
                    new Measurement { Value = 50.5, Unit = Units.Length_Millimetres }
                );

                double[,] pathInCm = new double[,] { { 0.00, 3.00 }, { 2.25, 7.10 }, { 5.35, 0.15 }, { 1.45, 10.20 }, { 9.00, 9.00 } };
                for (int i = 0; i < pathInCm.GetLength(0); i++)
                {
                    stream.LineAbsolute(
                        new Measurement { Value = pathInCm[i, 0], Unit = Units.Length_Centimetres },
                        new Measurement { Value = pathInCm[i, 1], Unit = Units.Length_Centimetres }
                    );
                }

                var content = streamBuffer.GetContent();
                Console.WriteLine("[{0}]", string.Join(",\n", content));

                stream.Disable();

                stream.SetupLive(1, 2);

                stream.Call(streamBuffer);

                var circleCenterAbs = new Measurement[]
                {
                    new Measurement { Value = 2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 4, Unit = Units.Length_Centimetres }
                };
                stream.CircleAbsolute(RotationDirection.CW, circleCenterAbs[0], circleCenterAbs[1]);

                var circleCenterRel = new Measurement[]
                {
                    new Measurement { Value = -2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 0, Unit = Units.Length_Centimetres }
                };
                stream.CircleRelative(RotationDirection.CCW, circleCenterRel[0], circleCenterRel[1]);

                var arcCircleCenterRel = new Measurement[]
                {
                    new Measurement { Value = -2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 0, Unit = Units.Length_Centimetres }
                };
                var arcEndRel = new Measurement[]
                {
                    new Measurement { Value = -2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 0, Unit = Units.Length_Centimetres }
                };
                stream.ArcRelative(
                    RotationDirection.CCW,
                    arcCircleCenterRel[0], arcCircleCenterRel[1],
                    arcEndRel[0], arcEndRel[1]
                );

                var arcCircleCenterAbs = new Measurement[]
                {
                    new Measurement { Value = -2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 0, Unit = Units.Length_Centimetres }
                };
                var arcEndAbs = new Measurement[]
                {
                    new Measurement { Value = -2, Unit = Units.Length_Centimetres },
                    new Measurement { Value = 0, Unit = Units.Length_Centimetres }
                };
                stream.ArcRelative(
                    RotationDirection.CW,
                    arcCircleCenterAbs[0], arcCircleCenterAbs[1],
                    arcEndAbs[0], arcEndAbs[1]
                );

                stream.LineAbsoluteOn(new int[] { 1 }, new Measurement[] { new Measurement { Value = 1 } });

                stream.SetMaxCentripetalAcceleration(5, Units.Acceleration_CentimetresPerSecondSquared);
                stream.SetMaxTangentialAcceleration(5, Units.Acceleration_CentimetresPerSecondSquared);
                stream.SetMaxSpeed(0.5, Units.Velocity_MillimetresPerSecond);

                stream.Wait(2, Units.Time_Seconds);

                stream.Io.SetDigitalOutput(1, DigitalOutputAction.On);
                stream.Io.WaitDigitalInput(1, true);

                stream.Io.SetDigitalOutput(1, DigitalOutputAction.Toggle);
                stream.Io.SetDigitalOutput(1, DigitalOutputAction.Toggle);

                stream.Io.SetAnalogOutput(1, 0.42);
                stream.Io.WaitAnalogInput(1, ">=", 0.50);

                stream.WaitUntilIdle();

                Console.WriteLine(stream.ToString());
                Console.WriteLine(string.Join<StreamAxisDefinition>(", ", stream.Axes));
                Console.WriteLine(stream.GetMaxSpeed(Units.Velocity_CentimetresPerSecond));
                Console.WriteLine(stream.GetMaxTangentialAcceleration(Units.Acceleration_CentimetresPerSecondSquared));
                Console.WriteLine(stream.GetMaxCentripetalAcceleration(Units.Acceleration_CentimetresPerSecondSquared));

                stream.Cork();
                stream.Uncork();

                if (stream.IsBusy())
                {
                    stream.WaitUntilIdle();
                }

                stream.Disable();
            }

            Thread.Sleep(2000);
        }
    }
}
