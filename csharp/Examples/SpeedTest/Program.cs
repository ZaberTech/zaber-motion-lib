﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zaber.Motion.Ascii;

namespace Examples.SpeedTest
{
    class Program
    {
        const int CYCLES = 10000;
        const int FIBER_COUNT = 12;

        List<double> results = new List<double>();

        static void Main(string[] args)
        {
            try
            {
                if (args.Contains("--threads"))
                {
                    new Program().RunThreads();
                }
                else
                {
                    new Program().Run().Wait();
                }
            }
            catch (Exception er)
            {
                Console.WriteLine(er);
            }
        }

        private void RunThreads()
        {
            for (var fibers = 1; fibers <= FIBER_COUNT; fibers++)
            {
                results.Clear();

                var threads = Enumerable.Range(1, fibers).Select(id => new Thread(ThreadSpeedTest)).ToArray();

                foreach (var thread in threads)
                {
                    thread.Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                var totalAverage = results.Average();

                Console.WriteLine($"{fibers}: {totalAverage}");
                Thread.Sleep(500);
            }
        }

        private void ThreadSpeedTest()
        {
            var conn = Connection.OpenTcp("127.0.0.1", 11234);
            var watch = new Stopwatch();

            var data = new List<double>();
            for (var i = 0; i < CYCLES; i++)
            {
                watch.Reset();
                watch.Start();

                conn.GenericCommand("");

                watch.Stop();

                var cost = watch.Elapsed.TotalMilliseconds;
                data.Add(cost);
            }

            var min = data.Min();
            var max = data.Max();
            var avg = data.Average();

            conn.Close();

            lock (results)
            {
                results.Add(avg);
            }
        }

        private async Task Run()
        {
            for (var fibers = 1; fibers <= FIBER_COUNT; fibers++)
            {
                var tasks = Enumerable.Range(1, fibers).Select(id => SpeedTest(id)).ToArray();
                await Task.WhenAll(tasks);
                var totalAverage = tasks.Select(task => task.Result).Average();

                Console.WriteLine($"{fibers}: {totalAverage}");
                await Task.Delay(500);
            }
        }

        private async Task<double> SpeedTest(int id)
        {
            var conn = await Connection.OpenTcpAsync("127.0.0.1", 11234);
            try {
                var watch = new Stopwatch();

                var data = new List<double>();
                for (var i = 0; i < CYCLES; i++)
                {
                    watch.Reset();
                    watch.Start();

                    await conn.GenericCommandAsync("");

                    watch.Stop();

                    var cost = watch.Elapsed.TotalMilliseconds;
                    data.Add(cost);
                }

                var min = data.Min();
                var max = data.Max();
                var avg = data.Average();
                return avg;
            }
            finally
            {
                await conn.CloseAsync();
            }
        }
    }
}
