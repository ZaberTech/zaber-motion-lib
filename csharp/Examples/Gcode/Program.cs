﻿using System;
using System.Threading;

using Zaber.Motion;
using Zaber.Motion.Ascii;
using Zaber.Motion.Gcode;

namespace Examples.Streams
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            Library.SetLogOutput(LogOutputMode.Stdout);
            Library.SetDeviceDbSource(DeviceDbSourceType.WebService, "https://api.zaber.io/device-db/master");

            using (var comm = Connection.OpenTcp("localhost", 11321))
            {
                var devices = comm.DetectDevices();
                var device = devices[0];

                device.AllAxes.Home();

                var stream = device.Streams.GetStream(1);
                stream.SetupLive(1, 2);

                var translator = Translator.Setup(stream, new TranslatorConfig {
                    AxisMappings = new AxisMapping[]
                    {
                        new AxisMapping() { AxisIndex = 0, AxisLetter = "Y" },
                        new AxisMapping() { AxisIndex = 1, AxisLetter = "X" }
                    }
                });

                translator.Translate("G28 Y10");
                translator.Translate("G0 X10 Y20");
                translator.Flush();

                var y = translator.GetAxisPosition("Y", Units.Length_Millimetres);
                translator.SetAxisPosition("Y", y * 2, Units.Length_Millimetres);

                translator.SetTraverseRate(3, Units.Velocity_MillimetresPerSecond);

                translator.Translate("G0 X10 Y20");

                translator.Flush();

                stream.Disable();
            }

            Thread.Sleep(2000);
        }
    }
}
