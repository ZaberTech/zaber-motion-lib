﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Zaber.Motion;
using Zaber.Motion.Ascii;

namespace Examples.Oscilloscope
{
    public class Program
    {
        // This example is fully async to exercise different library code paths, but
        // the same code can be written to be fully synchronous by removing all the
        // await statements and changing all the Async function calls to the non-async versions.
        public static async Task Main(string[] args)
        {
            Library.SetLogOutput(LogOutputMode.Stdout);

            string csvText;
            string fileName = null;
            if (args.Length >= 1)
            {
                fileName = args[0];
            }

            await using (var comm = await Connection.OpenSerialPortAsync("COM2"))
            {
                var devices = await comm.DetectDevicesAsync();
                var oscilloscope = devices[0].Oscilloscope;
                await oscilloscope.ClearAsync();

                await Task.WhenAll(
                    oscilloscope.SetTimebaseAsync(0.1f, Units.Time_Milliseconds),
                    oscilloscope.SetDelayAsync(0.0f));

                await oscilloscope.AddChannelAsync(1, "pos");
                await oscilloscope.AddChannelAsync(1, "encoder.pos");

                await oscilloscope.StartAsync();
                var data = await oscilloscope.ReadAsync();
                csvText = Program.WriteCSV(data);
            }

            if (string.IsNullOrEmpty(fileName))
            {
                Console.WriteLine($"Captured data in CSV format:\n----------\n{csvText}");
            }
            else
            {
                File.WriteAllText(fileName, csvText);
                Console.WriteLine($"Captured data written to file {fileName}");
            }
        }

        private static string WriteCSV(OscilloscopeData[] data)
        {
            var sb = new StringBuilder();
            var dataValues = data.Select(d => d.GetData(Units.Length_Millimetres)).ToArray();
            var headers = new string[] { "Time (ms)" }.Concat(data.Select(d =>
            {
                var label = new StringBuilder();
                label.Append(d.Setting);
                label.Append(" (");
                if (d.AxisNumber > 0)
                {
                    label.AppendFormat("axis {0} ", d.AxisNumber);
                }

                label.Append("mm)");
                return label.ToString();
            })).ToArray();

            sb.AppendLine(string.Join(", ", headers));

            for (int rowIndex = 0; rowIndex < dataValues[0].Length; rowIndex++)
            {
                var time = data[0].GetSampleTime(rowIndex, Units.Time_Milliseconds);
                sb.AppendLine(string.Join(", ",
                    new string[] { time.ToString() }.Concat(
                        Enumerable.Range(0, dataValues.Length)
                            .Select(colIndex => dataValues[colIndex][rowIndex].ToString("F6")))));
            }

            return sb.ToString();
        }
    }
}
