﻿using System;
using System.Threading;

using Zaber.Motion;
using Zaber.Motion.Ascii;

namespace Examples.BasicMoves
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            Library.SetLogOutput(LogOutputMode.Stdout);

            using (var comm = Connection.OpenSerialPort("COM2"))
            {
                var devices = comm.DetectDevices();
                var device = devices[0];

                device.AllAxes.Home();

                var axis = device.GetAxis(1);

                axis.MoveAbsolute(1, Units.Length_Centimetres);

                axis.MoveRelative(-5, Units.Length_Millimetres);

                axis.MoveVelocity(1, Units.Velocity_MillimetresPerSecond);
                Thread.Sleep(2000);
                axis.Stop();

                var position = axis.GetPosition(Units.Length_Millimetres);
                Console.WriteLine($"Position: {position}");
            }

            Thread.Sleep(2000);
        }
    }
}
