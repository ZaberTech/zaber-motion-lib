﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Zaber.Motion.Exceptions;
using Zaber.Motion.Runtime;

namespace Zaber.Motion.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class GatewayTest
    {
        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TestRequestWithResponse(bool isAsync)
        {
            var request = new Requests.TestRequest()
            {
                DataPing = "Hello",
            };

            Requests.TestResponse response;
            if (isAsync)
            {
                response = await Gateway.CallAsync("test/request", request, Requests.TestResponse.FromByteArray);
            }
            else
            {
                response = Gateway.CallSync("test/request", request, Requests.TestResponse.FromByteArray);
            }

            Assert.AreEqual("Hello...", response.DataPong);
        }


        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void TestRequestWithError(bool isAsync)
        {
            var request = new Requests.TestRequest()
            {
                DataPing = "Hello",
                ReturnError = true,
            };

            RequestTimeoutException error;
            if (isAsync)
            {
                error = Assert.ThrowsAsync<RequestTimeoutException>(async () =>
                    await Gateway.CallAsync("test/request", request, Requests.TestResponse.FromByteArray)
                );
            }
            else
            {
                error = Assert.Throws<RequestTimeoutException>(() =>
                    Gateway.CallSync("test/request", request, Requests.TestResponse.FromByteArray)
                );
            }

            Assert.AreEqual("Device has not responded in given timeout", error.Message);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void TestRequestWithErrorAndErrorData(bool isAsync)
        {
            var request = new Requests.TestRequest()
            {
                ReturnErrorWithData = true,
            };

            InvalidPacketException error;
            if (isAsync)
            {
                error = Assert.ThrowsAsync<InvalidPacketException>(async () =>
                    await Gateway.CallAsync("test/request", request, Requests.TestResponse.FromByteArray)
                );
            }
            else
            {
                error = Assert.Throws<InvalidPacketException>(() =>
                    Gateway.CallSync("test/request", request, Requests.TestResponse.FromByteArray)
                );
            }

            var expectedPacket = "123";
            var expectedReason = "For test";
            Assert.AreEqual($"Cannot parse incoming packet: \"{expectedPacket}\" ({expectedReason})", error.Message);
            Assert.AreEqual(expectedPacket, error.Details.Packet);
            Assert.AreEqual(expectedReason, error.Details.Reason);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TestRequestWithoutResponse(bool isAsync)
        {
            if (isAsync) {
                await Gateway.CallAsync("test/request_no_response");
            } else {
                Gateway.CallSync("test/request_no_response");
            }
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TestRequestWithLongResponse(bool isAsync)
        {
            var request = new Requests.TestRequest()
            {
                DataPing = "Hello",
            };

            Requests.TestResponseLong response;
            if (isAsync) {
                response = await Gateway.CallAsync("test/request_long", request, Requests.TestResponseLong.FromByteArray);
            } else {
                response = Gateway.CallSync("test/request_long", request, Requests.TestResponseLong.FromByteArray);
            }

            for (int i = 0; i < response.DataPong.Length; i++) {
                Assert.AreEqual($"Hello...{i}", response.DataPong[i]);
            }
        }

        [Test]
        public async Task TestEmitEvent()
        {
            var testEventPromise = new TaskCompletionSource<Requests.TestEvent>();
            Action<Requests.TestEvent> resolve = testEvent => testEventPromise.SetResult(testEvent);

            try
            {
                Events.TestEvent += resolve;

                await Gateway.CallAsync("test/emit_event");

                if (await Task.WhenAny(testEventPromise.Task, Task.Delay(10000)) != testEventPromise.Task)
                {
                    Assert.Fail("Timeout");
                }

                var testEvent = await testEventPromise.Task;
                Assert.AreEqual("testing event data", testEvent.Data);
            }
            finally
            {
                Events.TestEvent -= resolve;
            }
        }
    }
}
