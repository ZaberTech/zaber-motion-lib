﻿using NUnit.Framework;

namespace Zaber.Motion.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class UnitKeyMapTest
    {
        [Test]
        [TestCase(Units.Native, "")]
        [TestCase(Units.Angle_Degrees, "Angle:degrees")]
        [TestCase(Units.Velocity_MillimetresPerSecond, "Velocity:millimetres per second")]
        public void MapsUnitsToKeys(Units units, string key)
        {
            Assert.AreEqual(key, UnitKeyMap.Instance.GetKey(units));
            Assert.AreEqual(units, UnitKeyMap.Instance.GetEnum(key));
        }
    }
}
