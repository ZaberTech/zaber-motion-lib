﻿using NUnit.Framework;
using System;

namespace Zaber.Motion.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class ObjectDumperTest
    {
        [Test]
        public void TestPopulatedObject()
        {
            var testInstance = new TestObject
            {
                A = 13,
                B = "Box",
                C = new TestObject2
                {
                    E = 17
                },
                Ds = new TestObject2[]
                {
                    new TestObject2
                    {
                        E = 23
                    },
                    new TestObject2
                    {
                        E = 29
                    },
                    null,
                },
                F=42,
            };

            var dumped = testInstance.ToString();
            Console.WriteLine(dumped);
            Assert.AreEqual("TestObject(A=13,B=Box,C=TestObject2(E=17),D=(null),Ds=[TestObject2(E=23),TestObject2(E=29),(null)],F=42)", dumped);
        }

        [Test]
        public void TestEmptyObject()
        {
            var testInstance = new TestObject{
                Ds = Array.Empty<TestObject2>(),
            };
            var dumped = testInstance.ToString();
            Console.WriteLine(dumped);
            Assert.AreEqual("TestObject(A=0,B=(null),C=(null),D=(null),Ds=[],F=(null))", dumped);
        }

        private class TestObject
        {
            public int A { get; set; }
            public string B { get; set; }
            public TestObject2 C { get; set; }
            public TestObject2 D { get; set; }
            public TestObject2[] Ds { get; set; }
            public int? F { get; set; }

            public override string ToString()
            {
                return ObjectDumper.Dump(this);
            }
        }

        private class TestObject2
        {
            public int E { get; set; }

            public override string ToString()
            {
                return ObjectDumper.Dump(this);
            }
        }
    }
}
