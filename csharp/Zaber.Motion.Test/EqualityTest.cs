using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Zaber.Motion.Ascii;
using Zaber.Motion.Exceptions;
using Zaber.Motion.Runtime;

namespace Zaber.Motion.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class EqualityTest
    {
        [Test]
        public void TestEquals()
        {
            FirmwareVersion versionA = new FirmwareVersion()
            {
                Major = 1,
                Minor = 2,
                Build = 3,
            };
            FirmwareVersion versionB = new FirmwareVersion()
            {
                Major = 1,
                Minor = 2,
                Build = 3,
            };

            Assert.IsTrue(versionA.Equals(versionB));

            versionB.Major = 2;

            Assert.IsFalse(versionA.Equals(versionB));
        }

        [Test]
        public void TestEqualsMemberClass()
        {
            DeviceIdentity identityA = new DeviceIdentity()
            {
                DeviceId = 1,
                SerialNumber = 12,
                FirmwareVersion = new FirmwareVersion()
                {
                    Major = 1,
                    Minor = 2,
                    Build = 3,
                },
            };

            DeviceIdentity identityB = new DeviceIdentity()
            {
                DeviceId = 1,
                SerialNumber = 12,
                FirmwareVersion = new FirmwareVersion()
                {
                    Major = 1,
                    Minor = 2,
                    Build = 3,
                },
            };

            Assert.IsTrue(identityA.Equals(identityB));

            identityB.FirmwareVersion.Major = 2;

            Assert.IsFalse(identityA.Equals(identityB));
        }

        [Test]
        public void TestEqualsMemberArray()
        {
            CanSetStateDeviceResponse responseA = new CanSetStateDeviceResponse()
            {
                Error = "Test",
                AxisErrors = new CanSetStateAxisResponse[]
                {
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test",
                        AxisNumber = 1,
                    },
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test 2",
                        AxisNumber = 2,
                    },
                },
            };

            CanSetStateDeviceResponse responseB = new CanSetStateDeviceResponse()
            {
                Error = "Test",
                AxisErrors = new CanSetStateAxisResponse[]
                {
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test",
                        AxisNumber = 1,
                    },
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test 2",
                        AxisNumber = 2,
                    },
                },
            };

            Assert.IsTrue(responseA.Equals(responseB));

            responseB.AxisErrors = responseB.AxisErrors[..1];

            Assert.IsFalse(responseA.Equals(responseB));
        }

        [Test]
        public void TestHash()
        {
            FirmwareVersion versionA = new FirmwareVersion()
            {
                Major = 1,
                Minor = 2,
                Build = 3,
            };
            FirmwareVersion versionB = new FirmwareVersion()
            {
                Major = 1,
                Minor = 2,
                Build = 3,
            };
            FirmwareVersion versionC = new FirmwareVersion()
            {
                Major = 4,
                Minor = 5,
                Build = 6,
            };

            HashSet<FirmwareVersion> set = new HashSet<FirmwareVersion>();
            set.Add(versionA);
            set.Add(versionB);
            set.Add(versionC);

            Assert.AreEqual(2, set.Count);
        }

        [Test]
        public void TestHashMemberClass()
        {
            DeviceIdentity identityA = new DeviceIdentity()
            {
                DeviceId = 1,
                SerialNumber = 12,
                FirmwareVersion = new FirmwareVersion()
                {
                    Major = 1,
                    Minor = 2,
                    Build = 3,
                },
            };

            DeviceIdentity identityB = new DeviceIdentity()
            {
                DeviceId = 1,
                SerialNumber = 12,
                FirmwareVersion = new FirmwareVersion()
                {
                    Major = 1,
                    Minor = 2,
                    Build = 3,
                },
            };

            DeviceIdentity identityC = new DeviceIdentity()
            {
                DeviceId = 1,
                SerialNumber = 12,
                FirmwareVersion = new FirmwareVersion()
                {
                    Major = 4,
                    Minor = 5,
                    Build = 6,
                },
            };

            HashSet<DeviceIdentity> set = new HashSet<DeviceIdentity>();
            set.Add(identityA);
            set.Add(identityB);
            set.Add(identityC);

            Assert.AreEqual(2, set.Count);
        }

        [Test]
        public void TestHashMemberArray()
        {
            CanSetStateDeviceResponse responseA = new CanSetStateDeviceResponse()
            {
                Error = "Test",
                AxisErrors = new CanSetStateAxisResponse[]
                {
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test",
                        AxisNumber = 1,
                    },
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test 2",
                        AxisNumber = 2,
                    },
                },
            };

            CanSetStateDeviceResponse responseB = new CanSetStateDeviceResponse()
            {
                Error = "Test",
                AxisErrors = new CanSetStateAxisResponse[]
                {
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test",
                        AxisNumber = 1,
                    },
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test 2",
                        AxisNumber = 2,
                    },
                },
            };

            CanSetStateDeviceResponse responseC = new CanSetStateDeviceResponse()
            {
                Error = "Test",
                AxisErrors = new CanSetStateAxisResponse[]
                {
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test",
                        AxisNumber = 1,
                    },
                    new CanSetStateAxisResponse()
                    {
                        Error = "Test 3",
                        AxisNumber = 2,
                    },
                },
            };

            HashSet<CanSetStateDeviceResponse> set = new HashSet<CanSetStateDeviceResponse>();
            set.Add(responseA);
            set.Add(responseB);
            set.Add(responseC);

            Console.WriteLine(responseA.GetHashCode());
            Console.WriteLine(responseB.GetHashCode());
            Console.WriteLine(responseC.GetHashCode());

            Assert.AreEqual(2, set.Count);
        }
    }
}
