﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Zaber.Motion
{
    internal class UnitJsonConverter : JsonConverter<Units>
    {
        public override Units ReadJson(JsonReader reader, Type objectType, Units existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var key = reader.ReadAsString();
            return UnitKeyMap.Instance.GetEnum(key ?? string.Empty);
        }

        public override void WriteJson(JsonWriter writer, Units value, JsonSerializer serializer)
        {
            var key = UnitKeyMap.Instance.GetKey(value);
            writer.WriteValue(key);
        }

    }
}
