﻿// This file is generated from the Zaber device database. Do not manually edit this file.
#pragma warning disable CA1707 // Identifiers should not contain underscores
namespace Zaber.Motion
{
    /// <summary>
    /// Units used by various functions.
    /// </summary>
    [Newtonsoft.Json.JsonConverter(typeof(UnitJsonConverter))]
    public enum Units
    {
        /// <summary>
        /// Unit native for the device. Conversion won't be performed.
        /// </summary>
        [UnitKey("")]
        Native,

        #region -- Length units --

        /// <summary>
        /// Dimension: Length, unit: metres
        /// </summary>
        [UnitKey("Length:metres")]
        Length_Metres,

        /// <summary>
        /// Dimension: Length, unit: centimetres
        /// </summary>
        [UnitKey("Length:centimetres")]
        Length_Centimetres,

        /// <summary>
        /// Dimension: Length, unit: millimetres
        /// </summary>
        [UnitKey("Length:millimetres")]
        Length_Millimetres,

        /// <summary>
        /// Dimension: Length, unit: micrometres
        /// </summary>
        [UnitKey("Length:micrometres")]
        Length_Micrometres,

        /// <summary>
        /// Dimension: Length, unit: nanometres
        /// </summary>
        [UnitKey("Length:nanometres")]
        Length_Nanometres,

        /// <summary>
        /// Dimension: Length, unit: inches
        /// </summary>
        [UnitKey("Length:inches")]
        Length_Inches,

        #endregion
        #region -- Velocity units --

        /// <summary>
        /// Dimension: Velocity, unit: metres per second
        /// </summary>
        [UnitKey("Velocity:metres per second")]
        Velocity_MetresPerSecond,

        /// <summary>
        /// Dimension: Velocity, unit: centimetres per second
        /// </summary>
        [UnitKey("Velocity:centimetres per second")]
        Velocity_CentimetresPerSecond,

        /// <summary>
        /// Dimension: Velocity, unit: millimetres per second
        /// </summary>
        [UnitKey("Velocity:millimetres per second")]
        Velocity_MillimetresPerSecond,

        /// <summary>
        /// Dimension: Velocity, unit: micrometres per second
        /// </summary>
        [UnitKey("Velocity:micrometres per second")]
        Velocity_MicrometresPerSecond,

        /// <summary>
        /// Dimension: Velocity, unit: nanometres per second
        /// </summary>
        [UnitKey("Velocity:nanometres per second")]
        Velocity_NanometresPerSecond,

        /// <summary>
        /// Dimension: Velocity, unit: inches per second
        /// </summary>
        [UnitKey("Velocity:inches per second")]
        Velocity_InchesPerSecond,

        #endregion
        #region -- Acceleration units --

        /// <summary>
        /// Dimension: Acceleration, unit: metres per second squared
        /// </summary>
        [UnitKey("Acceleration:metres per second squared")]
        Acceleration_MetresPerSecondSquared,

        /// <summary>
        /// Dimension: Acceleration, unit: centimetres per second squared
        /// </summary>
        [UnitKey("Acceleration:centimetres per second squared")]
        Acceleration_CentimetresPerSecondSquared,

        /// <summary>
        /// Dimension: Acceleration, unit: millimetres per second squared
        /// </summary>
        [UnitKey("Acceleration:millimetres per second squared")]
        Acceleration_MillimetresPerSecondSquared,

        /// <summary>
        /// Dimension: Acceleration, unit: micrometres per second squared
        /// </summary>
        [UnitKey("Acceleration:micrometres per second squared")]
        Acceleration_MicrometresPerSecondSquared,

        /// <summary>
        /// Dimension: Acceleration, unit: nanometres per second squared
        /// </summary>
        [UnitKey("Acceleration:nanometres per second squared")]
        Acceleration_NanometresPerSecondSquared,

        /// <summary>
        /// Dimension: Acceleration, unit: inches per second squared
        /// </summary>
        [UnitKey("Acceleration:inches per second squared")]
        Acceleration_InchesPerSecondSquared,

        #endregion
        #region -- Angle units --

        /// <summary>
        /// Dimension: Angle, unit: degrees
        /// </summary>
        [UnitKey("Angle:degrees")]
        Angle_Degrees,

        /// <summary>
        /// Dimension: Angle, unit: radians
        /// </summary>
        [UnitKey("Angle:radians")]
        Angle_Radians,

        #endregion
        #region -- Angular Velocity units --

        /// <summary>
        /// Dimension: Angular Velocity, unit: degrees per second
        /// </summary>
        [UnitKey("Angular Velocity:degrees per second")]
        AngularVelocity_DegreesPerSecond,

        /// <summary>
        /// Dimension: Angular Velocity, unit: radians per second
        /// </summary>
        [UnitKey("Angular Velocity:radians per second")]
        AngularVelocity_RadiansPerSecond,

        #endregion
        #region -- Angular Acceleration units --

        /// <summary>
        /// Dimension: Angular Acceleration, unit: degrees per second squared
        /// </summary>
        [UnitKey("Angular Acceleration:degrees per second squared")]
        AngularAcceleration_DegreesPerSecondSquared,

        /// <summary>
        /// Dimension: Angular Acceleration, unit: radians per second squared
        /// </summary>
        [UnitKey("Angular Acceleration:radians per second squared")]
        AngularAcceleration_RadiansPerSecondSquared,

        #endregion
        #region -- AC Electric Current units --

        /// <summary>
        /// Dimension: AC Electric Current, unit: amperes peak
        /// </summary>
        [UnitKey("AC Electric Current:amperes peak")]
        ACElectricCurrent_AmperesPeak,

        /// <summary>
        /// Dimension: AC Electric Current, unit: amperes RMS
        /// </summary>
        [UnitKey("AC Electric Current:amperes RMS")]
        ACElectricCurrent_AmperesRms,

        #endregion
        #region -- Percent units --

        /// <summary>
        /// Dimension: Percent, unit: percent
        /// </summary>
        [UnitKey("Percent:percent")]
        Percent_Percent,

        #endregion
        #region -- DC Electric Current units --

        /// <summary>
        /// Dimension: DC Electric Current, unit: amperes
        /// </summary>
        [UnitKey("DC Electric Current:amperes")]
        DCElectricCurrent_Amperes,

        #endregion
        #region -- Force units --

        /// <summary>
        /// Dimension: Force, unit: newtons
        /// </summary>
        [UnitKey("Force:newtons")]
        Force_Newtons,

        /// <summary>
        /// Dimension: Force, unit: millinewtons
        /// </summary>
        [UnitKey("Force:millinewtons")]
        Force_Millinewtons,

        /// <summary>
        /// Dimension: Force, unit: pounds-force
        /// </summary>
        [UnitKey("Force:pounds-force")]
        Force_PoundsForce,

        /// <summary>
        /// Dimension: Force, unit: kilonewtons
        /// </summary>
        [UnitKey("Force:kilonewtons")]
        Force_Kilonewtons,

        #endregion
        #region -- Time units --

        /// <summary>
        /// Dimension: Time, unit: seconds
        /// </summary>
        [UnitKey("Time:seconds")]
        Time_Seconds,

        /// <summary>
        /// Dimension: Time, unit: milliseconds
        /// </summary>
        [UnitKey("Time:milliseconds")]
        Time_Milliseconds,

        /// <summary>
        /// Dimension: Time, unit: microseconds
        /// </summary>
        [UnitKey("Time:microseconds")]
        Time_Microseconds,

        #endregion
        #region -- Torque units --

        /// <summary>
        /// Dimension: Torque, unit: newton metres
        /// </summary>
        [UnitKey("Torque:newton metres")]
        Torque_NewtonMetres,

        /// <summary>
        /// Dimension: Torque, unit: newton centimetres
        /// </summary>
        [UnitKey("Torque:newton centimetres")]
        Torque_NewtonCentimetres,

        /// <summary>
        /// Dimension: Torque, unit: pound-force-feet
        /// </summary>
        [UnitKey("Torque:pound-force-feet")]
        Torque_PoundForceFeet,

        /// <summary>
        /// Dimension: Torque, unit: ounce-force-inches
        /// </summary>
        [UnitKey("Torque:ounce-force-inches")]
        Torque_OunceForceInches,

        #endregion
        #region -- Inertia units --

        /// <summary>
        /// Dimension: Inertia, unit: grams
        /// </summary>
        [UnitKey("Inertia:grams")]
        Inertia_Grams,

        /// <summary>
        /// Dimension: Inertia, unit: kilograms
        /// </summary>
        [UnitKey("Inertia:kilograms")]
        Inertia_Kilograms,

        /// <summary>
        /// Dimension: Inertia, unit: milligrams
        /// </summary>
        [UnitKey("Inertia:milligrams")]
        Inertia_Milligrams,

        /// <summary>
        /// Dimension: Inertia, unit: pounds
        /// </summary>
        [UnitKey("Inertia:pounds")]
        Inertia_Pounds,

        /// <summary>
        /// Dimension: Inertia, unit: ounces
        /// </summary>
        [UnitKey("Inertia:ounces")]
        Inertia_Ounces,

        #endregion
        #region -- Rotational Inertia units --

        /// <summary>
        /// Dimension: Rotational Inertia, unit: gram-square metre
        /// </summary>
        [UnitKey("Rotational Inertia:gram-square metre")]
        RotationalInertia_GramSquareMetre,

        /// <summary>
        /// Dimension: Rotational Inertia, unit: kilogram-square metre
        /// </summary>
        [UnitKey("Rotational Inertia:kilogram-square metre")]
        RotationalInertia_KilogramSquareMetre,

        /// <summary>
        /// Dimension: Rotational Inertia, unit: pound-square-feet
        /// </summary>
        [UnitKey("Rotational Inertia:pound-square-feet")]
        RotationalInertia_PoundSquareFeet,

        #endregion
        #region -- Force Constant units --

        /// <summary>
        /// Dimension: Force Constant, unit: newtons per amp
        /// </summary>
        [UnitKey("Force Constant:newtons per amp")]
        ForceConstant_NewtonsPerAmp,

        /// <summary>
        /// Dimension: Force Constant, unit: millinewtons per amp
        /// </summary>
        [UnitKey("Force Constant:millinewtons per amp")]
        ForceConstant_MillinewtonsPerAmp,

        /// <summary>
        /// Dimension: Force Constant, unit: kilonewtons per amp
        /// </summary>
        [UnitKey("Force Constant:kilonewtons per amp")]
        ForceConstant_KilonewtonsPerAmp,

        /// <summary>
        /// Dimension: Force Constant, unit: pounds-force per amp
        /// </summary>
        [UnitKey("Force Constant:pounds-force per amp")]
        ForceConstant_PoundsForcePerAmp,

        #endregion
        #region -- Torque Constant units --

        /// <summary>
        /// Dimension: Torque Constant, unit: newton metres per amp
        /// </summary>
        [UnitKey("Torque Constant:newton metres per amp")]
        TorqueConstant_NewtonMetresPerAmp,

        /// <summary>
        /// Dimension: Torque Constant, unit: millinewton metres per amp
        /// </summary>
        [UnitKey("Torque Constant:millinewton metres per amp")]
        TorqueConstant_MillinewtonMetresPerAmp,

        /// <summary>
        /// Dimension: Torque Constant, unit: kilonewton metres per amp
        /// </summary>
        [UnitKey("Torque Constant:kilonewton metres per amp")]
        TorqueConstant_KilonewtonMetresPerAmp,

        /// <summary>
        /// Dimension: Torque Constant, unit: pound-force-feet per amp
        /// </summary>
        [UnitKey("Torque Constant:pound-force-feet per amp")]
        TorqueConstant_PoundForceFeetPerAmp,

        #endregion
        #region -- Voltage units --

        /// <summary>
        /// Dimension: Voltage, unit: volts
        /// </summary>
        [UnitKey("Voltage:volts")]
        Voltage_Volts,

        /// <summary>
        /// Dimension: Voltage, unit: millivolts
        /// </summary>
        [UnitKey("Voltage:millivolts")]
        Voltage_Millivolts,

        /// <summary>
        /// Dimension: Voltage, unit: microvolts
        /// </summary>
        [UnitKey("Voltage:microvolts")]
        Voltage_Microvolts,

        #endregion
        #region -- Current Controller Proportional Gain units --

        /// <summary>
        /// Dimension: Current Controller Proportional Gain, unit: volts per amp
        /// </summary>
        [UnitKey("Current Controller Proportional Gain:volts per amp")]
        CurrentControllerProportionalGain_VoltsPerAmp,

        /// <summary>
        /// Dimension: Current Controller Proportional Gain, unit: millivolts per amp
        /// </summary>
        [UnitKey("Current Controller Proportional Gain:millivolts per amp")]
        CurrentControllerProportionalGain_MillivoltsPerAmp,

        /// <summary>
        /// Dimension: Current Controller Proportional Gain, unit: microvolts per amp
        /// </summary>
        [UnitKey("Current Controller Proportional Gain:microvolts per amp")]
        CurrentControllerProportionalGain_MicrovoltsPerAmp,

        #endregion
        #region -- Current Controller Integral Gain units --

        /// <summary>
        /// Dimension: Current Controller Integral Gain, unit: volts per amp per second
        /// </summary>
        [UnitKey("Current Controller Integral Gain:volts per amp per second")]
        CurrentControllerIntegralGain_VoltsPerAmpPerSecond,

        /// <summary>
        /// Dimension: Current Controller Integral Gain, unit: millivolts per amp per second
        /// </summary>
        [UnitKey("Current Controller Integral Gain:millivolts per amp per second")]
        CurrentControllerIntegralGain_MillivoltsPerAmpPerSecond,

        /// <summary>
        /// Dimension: Current Controller Integral Gain, unit: microvolts per amp per second
        /// </summary>
        [UnitKey("Current Controller Integral Gain:microvolts per amp per second")]
        CurrentControllerIntegralGain_MicrovoltsPerAmpPerSecond,

        #endregion
        #region -- Current Controller Derivative Gain units --

        /// <summary>
        /// Dimension: Current Controller Derivative Gain, unit: volts second per amp
        /// </summary>
        [UnitKey("Current Controller Derivative Gain:volts second per amp")]
        CurrentControllerDerivativeGain_VoltsSecondPerAmp,

        /// <summary>
        /// Dimension: Current Controller Derivative Gain, unit: millivolts second per amp
        /// </summary>
        [UnitKey("Current Controller Derivative Gain:millivolts second per amp")]
        CurrentControllerDerivativeGain_MillivoltsSecondPerAmp,

        /// <summary>
        /// Dimension: Current Controller Derivative Gain, unit: microvolts second per amp
        /// </summary>
        [UnitKey("Current Controller Derivative Gain:microvolts second per amp")]
        CurrentControllerDerivativeGain_MicrovoltsSecondPerAmp,

        #endregion
        #region -- Resistance units --

        /// <summary>
        /// Dimension: Resistance, unit: kiloohms
        /// </summary>
        [UnitKey("Resistance:kiloohms")]
        Resistance_Kiloohms,

        /// <summary>
        /// Dimension: Resistance, unit: ohms
        /// </summary>
        [UnitKey("Resistance:ohms")]
        Resistance_Ohms,

        /// <summary>
        /// Dimension: Resistance, unit: milliohms
        /// </summary>
        [UnitKey("Resistance:milliohms")]
        Resistance_Milliohms,

        /// <summary>
        /// Dimension: Resistance, unit: microohms
        /// </summary>
        [UnitKey("Resistance:microohms")]
        Resistance_Microohms,

        /// <summary>
        /// Dimension: Resistance, unit: nanoohms
        /// </summary>
        [UnitKey("Resistance:nanoohms")]
        Resistance_Nanoohms,

        #endregion
        #region -- Inductance units --

        /// <summary>
        /// Dimension: Inductance, unit: henries
        /// </summary>
        [UnitKey("Inductance:henries")]
        Inductance_Henries,

        /// <summary>
        /// Dimension: Inductance, unit: millihenries
        /// </summary>
        [UnitKey("Inductance:millihenries")]
        Inductance_Millihenries,

        /// <summary>
        /// Dimension: Inductance, unit: microhenries
        /// </summary>
        [UnitKey("Inductance:microhenries")]
        Inductance_Microhenries,

        /// <summary>
        /// Dimension: Inductance, unit: nanohenries
        /// </summary>
        [UnitKey("Inductance:nanohenries")]
        Inductance_Nanohenries,

        #endregion
        #region -- Voltage Constant units --

        /// <summary>
        /// Dimension: Voltage Constant, unit: volt seconds per radian
        /// </summary>
        [UnitKey("Voltage Constant:volt seconds per radian")]
        VoltageConstant_VoltSecondsPerRadian,

        /// <summary>
        /// Dimension: Voltage Constant, unit: millivolt seconds per radian
        /// </summary>
        [UnitKey("Voltage Constant:millivolt seconds per radian")]
        VoltageConstant_MillivoltSecondsPerRadian,

        /// <summary>
        /// Dimension: Voltage Constant, unit: microvolt seconds per radian
        /// </summary>
        [UnitKey("Voltage Constant:microvolt seconds per radian")]
        VoltageConstant_MicrovoltSecondsPerRadian,

        #endregion
        #region -- Absolute Temperature units --

        /// <summary>
        /// Dimension: Absolute Temperature, unit: degrees Celsius
        /// </summary>
        [UnitKey("Absolute Temperature:degrees Celsius")]
        AbsoluteTemperature_DegreesCelsius,

        /// <summary>
        /// Dimension: Absolute Temperature, unit: kelvins
        /// </summary>
        [UnitKey("Absolute Temperature:kelvins")]
        AbsoluteTemperature_Kelvins,

        /// <summary>
        /// Dimension: Absolute Temperature, unit: degrees Fahrenheit
        /// </summary>
        [UnitKey("Absolute Temperature:degrees Fahrenheit")]
        AbsoluteTemperature_DegreesFahrenheit,

        /// <summary>
        /// Dimension: Absolute Temperature, unit: degrees Rankine
        /// </summary>
        [UnitKey("Absolute Temperature:degrees Rankine")]
        AbsoluteTemperature_DegreesRankine,

        #endregion
        #region -- Relative Temperature units --

        /// <summary>
        /// Dimension: Relative Temperature, unit: degrees Celsius
        /// </summary>
        [UnitKey("Relative Temperature:degrees Celsius")]
        RelativeTemperature_DegreesCelsius,

        /// <summary>
        /// Dimension: Relative Temperature, unit: kelvins
        /// </summary>
        [UnitKey("Relative Temperature:kelvins")]
        RelativeTemperature_Kelvins,

        /// <summary>
        /// Dimension: Relative Temperature, unit: degrees Fahrenheit
        /// </summary>
        [UnitKey("Relative Temperature:degrees Fahrenheit")]
        RelativeTemperature_DegreesFahrenheit,

        /// <summary>
        /// Dimension: Relative Temperature, unit: degrees Rankine
        /// </summary>
        [UnitKey("Relative Temperature:degrees Rankine")]
        RelativeTemperature_DegreesRankine,

        #endregion
        #region -- Frequency units --

        /// <summary>
        /// Dimension: Frequency, unit: gigahertz
        /// </summary>
        [UnitKey("Frequency:gigahertz")]
        Frequency_Gigahertz,

        /// <summary>
        /// Dimension: Frequency, unit: megahertz
        /// </summary>
        [UnitKey("Frequency:megahertz")]
        Frequency_Megahertz,

        /// <summary>
        /// Dimension: Frequency, unit: kilohertz
        /// </summary>
        [UnitKey("Frequency:kilohertz")]
        Frequency_Kilohertz,

        /// <summary>
        /// Dimension: Frequency, unit: hertz
        /// </summary>
        [UnitKey("Frequency:hertz")]
        Frequency_Hertz,

        /// <summary>
        /// Dimension: Frequency, unit: millihertz
        /// </summary>
        [UnitKey("Frequency:millihertz")]
        Frequency_Millihertz,

        /// <summary>
        /// Dimension: Frequency, unit: microhertz
        /// </summary>
        [UnitKey("Frequency:microhertz")]
        Frequency_Microhertz,

        /// <summary>
        /// Dimension: Frequency, unit: nanohertz
        /// </summary>
        [UnitKey("Frequency:nanohertz")]
        Frequency_Nanohertz,

        #endregion
    }
}
#pragma warning restore CA1707 // Identifiers should not contain underscores
