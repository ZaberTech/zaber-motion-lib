using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Zaber.Motion
{
    /// <summary>
    /// Collects the mapping between Units keys and enum values.
    /// </summary>
    public class UnitKeyMap
    {
        /// <summary>
        /// Gets the unit key from the Enum value.
        /// </summary>
        /// <param name="value">The enumeration value to get the key of.</param>
        /// <returns>The identifier for the given enum value.</returns>
        public string GetKey(Units value)
        {
            if (!_keyUnitMap.TryGetValue(value, out var key))
            {
                throw new ArgumentException("Invalid Units value");
            }

            return key;
        }

        /// <summary>
        /// Gets the Enum value from the unit key.
        /// </summary>
        /// <param name="key">The key to get the enum value of.</param>
        /// <returns>The enum value for the given key.</returns>
        public Units GetEnum(string key)
        {
            if (!_unitKeyMap.TryGetValue(key, out var value))
            {
                throw new ArgumentException("Invalid Units key");
            }

            return value;
        }

        internal UnitKeyMap()
        {
            var values = Enum.GetValues(typeof(Units)).Cast<Units>();
            foreach (var value in values)
            {
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString())!;
                var attrs = (UnitKeyAttribute[])fi.GetCustomAttributes(typeof(UnitKeyAttribute), false);
                if (attrs.Length != 1)
                {
                    throw new InvalidOperationException("UnitKeyAttribute not found for " + value);
                }

                _unitKeyMap[attrs[0].Key] = value;
                _keyUnitMap[value] = attrs[0].Key;
            }
        }


        /// <summary>
        /// Map of unit keys to enum values.
        /// </summary>
        public static UnitKeyMap Instance { get; } = new UnitKeyMap();

        private Dictionary<string, Units> _unitKeyMap = new Dictionary<string, Units>();
        private Dictionary<Units, string> _keyUnitMap = new Dictionary<Units, string>();
    }
}
