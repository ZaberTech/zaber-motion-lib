﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Zaber.Motion
{
    /// <summary>
    /// Class used to specify internal key for dimension and unit.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class UnitKeyAttribute : Attribute
    {
        /// <summary>
        /// Constructs UnitKey.
        /// </summary>
        /// <param name="key">Unique identifier for the unit of measure.</param>
        public UnitKeyAttribute(string key)
        {
            Key = key;
        }


        /// <summary>
        /// Dimension and unit string key.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1721:Property names should not match get methods", Justification = "Yes")]
        public string Key { get; private set; }
    }
}
