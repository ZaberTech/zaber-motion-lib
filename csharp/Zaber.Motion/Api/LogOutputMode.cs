/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion
{
    /// <summary>
    /// Mode of logging output of the library.
    /// </summary>
    public enum LogOutputMode
    {
        /// <summary>Off.</summary>
        Off = 0,

        /// <summary>Stdout.</summary>
        Stdout = 1,

        /// <summary>Stderr.</summary>
        Stderr = 2,

        /// <summary>File.</summary>
        File = 3,

    }
}
