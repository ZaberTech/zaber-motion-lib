/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion
{
    /// <summary>
    /// Type of source of Device DB data.
    /// </summary>
    public enum DeviceDbSourceType
    {
        /// <summary>WebService.</summary>
        WebService = 0,

        /// <summary>File.</summary>
        File = 1,

    }
}
