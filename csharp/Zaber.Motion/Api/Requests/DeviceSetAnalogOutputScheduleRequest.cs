/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class DeviceSetAnalogOutputScheduleRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("channelNumber")]
        public int ChannelNumber { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("futureValue")]
        public double FutureValue { get; set; }

        [JsonProperty("delay")]
        public double Delay { get; set; }

        [JsonProperty("unit")]
        public Units Unit { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is DeviceSetAnalogOutputScheduleRequest))
            {
                return false;
            }

            DeviceSetAnalogOutputScheduleRequest other = (DeviceSetAnalogOutputScheduleRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(ChannelNumber, other.ChannelNumber) &&
                EqualityUtils.CheckEquals(Value, other.Value) &&
                EqualityUtils.CheckEquals(FutureValue, other.FutureValue) &&
                EqualityUtils.CheckEquals(Delay, other.Delay) &&
                EqualityUtils.CheckEquals(Unit, other.Unit)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(ChannelNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Value));
            hash.Add(EqualityUtils.GenerateHashCode(FutureValue));
            hash.Add(EqualityUtils.GenerateHashCode(Delay));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => DeviceSetAnalogOutputScheduleRequest.ToByteArray(this);

        internal static DeviceSetAnalogOutputScheduleRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<DeviceSetAnalogOutputScheduleRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(DeviceSetAnalogOutputScheduleRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
