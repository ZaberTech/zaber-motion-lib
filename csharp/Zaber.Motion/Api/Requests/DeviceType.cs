/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum DeviceType
    {
        /// <summary>Any.</summary>
        Any = 0,

        /// <summary>ProcessController.</summary>
        ProcessController = 1,

    }
}
