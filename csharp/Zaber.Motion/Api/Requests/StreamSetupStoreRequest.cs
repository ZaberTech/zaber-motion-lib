/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class StreamSetupStoreRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("streamId")]
        public int StreamId { get; set; }

        [JsonProperty("pvt")]
        public bool Pvt { get; set; }

        [JsonProperty("streamBuffer")]
        public int StreamBuffer { get; set; }

        [JsonProperty("pvtBuffer")]
        public int PvtBuffer { get; set; }

        [JsonProperty("axes")]
        public int[] Axes { get; set; } = System.Array.Empty<int>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is StreamSetupStoreRequest))
            {
                return false;
            }

            StreamSetupStoreRequest other = (StreamSetupStoreRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(StreamId, other.StreamId) &&
                EqualityUtils.CheckEquals(Pvt, other.Pvt) &&
                EqualityUtils.CheckEquals(StreamBuffer, other.StreamBuffer) &&
                EqualityUtils.CheckEquals(PvtBuffer, other.PvtBuffer) &&
                EqualityUtils.CheckEquals(Axes, other.Axes)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(StreamId));
            hash.Add(EqualityUtils.GenerateHashCode(Pvt));
            hash.Add(EqualityUtils.GenerateHashCode(StreamBuffer));
            hash.Add(EqualityUtils.GenerateHashCode(PvtBuffer));
            hash.Add(EqualityUtils.GenerateHashCode(Axes));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => StreamSetupStoreRequest.ToByteArray(this);

        internal static StreamSetupStoreRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<StreamSetupStoreRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(StreamSetupStoreRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
