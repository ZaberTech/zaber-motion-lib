/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class TriggerOnFireSetToSettingRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("triggerNumber")]
        public int TriggerNumber { get; set; }

        [JsonProperty("action")]
        public TriggerAction Action { get; set; }

        [JsonProperty("axis")]
        public int Axis { get; set; }

        [JsonProperty("setting")]
        public string Setting { get; set; } = string.Empty;

        [JsonProperty("operation")]
        public TriggerOperation Operation { get; set; }

        [JsonProperty("fromAxis")]
        public int FromAxis { get; set; }

        [JsonProperty("fromSetting")]
        public string FromSetting { get; set; } = string.Empty;

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TriggerOnFireSetToSettingRequest))
            {
                return false;
            }

            TriggerOnFireSetToSettingRequest other = (TriggerOnFireSetToSettingRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(TriggerNumber, other.TriggerNumber) &&
                EqualityUtils.CheckEquals(Action, other.Action) &&
                EqualityUtils.CheckEquals(Axis, other.Axis) &&
                EqualityUtils.CheckEquals(Setting, other.Setting) &&
                EqualityUtils.CheckEquals(Operation, other.Operation) &&
                EqualityUtils.CheckEquals(FromAxis, other.FromAxis) &&
                EqualityUtils.CheckEquals(FromSetting, other.FromSetting)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(TriggerNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Action));
            hash.Add(EqualityUtils.GenerateHashCode(Axis));
            hash.Add(EqualityUtils.GenerateHashCode(Setting));
            hash.Add(EqualityUtils.GenerateHashCode(Operation));
            hash.Add(EqualityUtils.GenerateHashCode(FromAxis));
            hash.Add(EqualityUtils.GenerateHashCode(FromSetting));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TriggerOnFireSetToSettingRequest.ToByteArray(this);

        internal static TriggerOnFireSetToSettingRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TriggerOnFireSetToSettingRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TriggerOnFireSetToSettingRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
