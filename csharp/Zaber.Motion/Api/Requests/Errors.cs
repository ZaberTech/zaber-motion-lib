/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum Errors
    {
        /// <summary>BadCommand.</summary>
        BadCommand = 0,

        /// <summary>BadData.</summary>
        BadData = 1,

        /// <summary>BinaryCommandFailed.</summary>
        BinaryCommandFailed = 2,

        /// <summary>CommandFailed.</summary>
        CommandFailed = 3,

        /// <summary>CommandPreempted.</summary>
        CommandPreempted = 4,

        /// <summary>CommandTooLong.</summary>
        CommandTooLong = 5,

        /// <summary>ConnectionClosed.</summary>
        ConnectionClosed = 6,

        /// <summary>ConnectionFailed.</summary>
        ConnectionFailed = 7,

        /// <summary>ConversionFailed.</summary>
        ConversionFailed = 8,

        /// <summary>DeviceAddressConflict.</summary>
        DeviceAddressConflict = 9,

        /// <summary>DeviceBusy.</summary>
        DeviceBusy = 10,

        /// <summary>DeviceDbFailed.</summary>
        DeviceDbFailed = 11,

        /// <summary>DeviceDetectionFailed.</summary>
        DeviceDetectionFailed = 12,

        /// <summary>DeviceFailed.</summary>
        DeviceFailed = 13,

        /// <summary>DeviceNotIdentified.</summary>
        DeviceNotIdentified = 14,

        /// <summary>DriverDisabled.</summary>
        DriverDisabled = 15,

        /// <summary>GCodeExecution.</summary>
        GCodeExecution = 16,

        /// <summary>GCodeSyntax.</summary>
        GCodeSyntax = 17,

        /// <summary>IncompatibleSharedLibrary.</summary>
        IncompatibleSharedLibrary = 18,

        /// <summary>InternalError.</summary>
        InternalError = 19,

        /// <summary>InvalidArgument.</summary>
        InvalidArgument = 20,

        /// <summary>InvalidData.</summary>
        InvalidData = 21,

        /// <summary>InvalidOperation.</summary>
        InvalidOperation = 22,

        /// <summary>InvalidPacket.</summary>
        InvalidPacket = 23,

        /// <summary>InvalidParkState.</summary>
        InvalidParkState = 24,

        /// <summary>InvalidRequestData.</summary>
        InvalidRequestData = 25,

        /// <summary>InvalidResponse.</summary>
        InvalidResponse = 26,

        /// <summary>IoChannelOutOfRange.</summary>
        IoChannelOutOfRange = 27,

        /// <summary>IoFailed.</summary>
        IoFailed = 28,

        /// <summary>LockstepEnabled.</summary>
        LockstepEnabled = 29,

        /// <summary>LockstepNotEnabled.</summary>
        LockstepNotEnabled = 30,

        /// <summary>MovementFailed.</summary>
        MovementFailed = 31,

        /// <summary>MovementInterrupted.</summary>
        MovementInterrupted = 32,

        /// <summary>NoDeviceFound.</summary>
        NoDeviceFound = 33,

        /// <summary>NoValueForKey.</summary>
        NoValueForKey = 34,

        /// <summary>NotSupported.</summary>
        NotSupported = 35,

        /// <summary>OperationFailed.</summary>
        OperationFailed = 36,

        /// <summary>OsFailed.</summary>
        OsFailed = 37,

        /// <summary>OutOfRequestIds.</summary>
        OutOfRequestIds = 38,

        /// <summary>PvtDiscontinuity.</summary>
        PvtDiscontinuity = 39,

        /// <summary>PvtExecution.</summary>
        PvtExecution = 40,

        /// <summary>PvtMode.</summary>
        PvtMode = 41,

        /// <summary>PvtMovementFailed.</summary>
        PvtMovementFailed = 42,

        /// <summary>PvtMovementInterrupted.</summary>
        PvtMovementInterrupted = 43,

        /// <summary>PvtSetupFailed.</summary>
        PvtSetupFailed = 44,

        /// <summary>RemoteMode.</summary>
        RemoteMode = 45,

        /// <summary>RequestTimeout.</summary>
        RequestTimeout = 46,

        /// <summary>SerialPortBusy.</summary>
        SerialPortBusy = 47,

        /// <summary>SetDeviceStateFailed.</summary>
        SetDeviceStateFailed = 48,

        /// <summary>SetPeripheralStateFailed.</summary>
        SetPeripheralStateFailed = 49,

        /// <summary>SettingNotFound.</summary>
        SettingNotFound = 50,

        /// <summary>StreamDiscontinuity.</summary>
        StreamDiscontinuity = 51,

        /// <summary>StreamExecution.</summary>
        StreamExecution = 52,

        /// <summary>StreamMode.</summary>
        StreamMode = 53,

        /// <summary>StreamMovementFailed.</summary>
        StreamMovementFailed = 54,

        /// <summary>StreamMovementInterrupted.</summary>
        StreamMovementInterrupted = 55,

        /// <summary>StreamSetupFailed.</summary>
        StreamSetupFailed = 56,

        /// <summary>Timeout.</summary>
        Timeout = 57,

        /// <summary>TransportAlreadyUsed.</summary>
        TransportAlreadyUsed = 58,

        /// <summary>UnknownRequest.</summary>
        UnknownRequest = 59,

    }
}
