/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class SetServoTuningPIDRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("axis")]
        public int Axis { get; set; }

        [JsonProperty("paramset")]
        public ServoTuningParamset Paramset { get; set; }

        [JsonProperty("p")]
        public double P { get; set; }

        [JsonProperty("i")]
        public double I { get; set; }

        [JsonProperty("d")]
        public double D { get; set; }

        [JsonProperty("fc")]
        public double Fc { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SetServoTuningPIDRequest))
            {
                return false;
            }

            SetServoTuningPIDRequest other = (SetServoTuningPIDRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(Axis, other.Axis) &&
                EqualityUtils.CheckEquals(Paramset, other.Paramset) &&
                EqualityUtils.CheckEquals(P, other.P) &&
                EqualityUtils.CheckEquals(I, other.I) &&
                EqualityUtils.CheckEquals(D, other.D) &&
                EqualityUtils.CheckEquals(Fc, other.Fc)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(Axis));
            hash.Add(EqualityUtils.GenerateHashCode(Paramset));
            hash.Add(EqualityUtils.GenerateHashCode(P));
            hash.Add(EqualityUtils.GenerateHashCode(I));
            hash.Add(EqualityUtils.GenerateHashCode(D));
            hash.Add(EqualityUtils.GenerateHashCode(Fc));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SetServoTuningPIDRequest.ToByteArray(this);

        internal static SetServoTuningPIDRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SetServoTuningPIDRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SetServoTuningPIDRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
