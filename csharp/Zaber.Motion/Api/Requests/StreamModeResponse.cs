/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class StreamModeResponse : IMessage
    {
        [JsonProperty("streamMode")]
        public StreamMode StreamMode { get; set; }

        [JsonProperty("pvtMode")]
        public PvtMode PvtMode { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is StreamModeResponse))
            {
                return false;
            }

            StreamModeResponse other = (StreamModeResponse)obj;
            return (
                EqualityUtils.CheckEquals(StreamMode, other.StreamMode) &&
                EqualityUtils.CheckEquals(PvtMode, other.PvtMode)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(StreamMode));
            hash.Add(EqualityUtils.GenerateHashCode(PvtMode));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => StreamModeResponse.ToByteArray(this);

        internal static StreamModeResponse FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<StreamModeResponse>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(StreamModeResponse instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
