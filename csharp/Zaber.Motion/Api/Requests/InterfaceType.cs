/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum InterfaceType
    {
        /// <summary>SerialPort.</summary>
        SerialPort = 0,

        /// <summary>Tcp.</summary>
        Tcp = 1,

        /// <summary>Custom.</summary>
        Custom = 2,

        /// <summary>Iot.</summary>
        Iot = 3,

        /// <summary>NetworkShare.</summary>
        NetworkShare = 4,

    }
}
