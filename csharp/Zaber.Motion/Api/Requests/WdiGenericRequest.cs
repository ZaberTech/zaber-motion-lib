/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class WdiGenericRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("registerId")]
        public int RegisterId { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }

        [JsonProperty("registerBank")]
        public string RegisterBank { get; set; } = string.Empty;

        [JsonProperty("data")]
        public int[] Data { get; set; } = System.Array.Empty<int>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is WdiGenericRequest))
            {
                return false;
            }

            WdiGenericRequest other = (WdiGenericRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(RegisterId, other.RegisterId) &&
                EqualityUtils.CheckEquals(Size, other.Size) &&
                EqualityUtils.CheckEquals(Count, other.Count) &&
                EqualityUtils.CheckEquals(Offset, other.Offset) &&
                EqualityUtils.CheckEquals(RegisterBank, other.RegisterBank) &&
                EqualityUtils.CheckEquals(Data, other.Data)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(RegisterId));
            hash.Add(EqualityUtils.GenerateHashCode(Size));
            hash.Add(EqualityUtils.GenerateHashCode(Count));
            hash.Add(EqualityUtils.GenerateHashCode(Offset));
            hash.Add(EqualityUtils.GenerateHashCode(RegisterBank));
            hash.Add(EqualityUtils.GenerateHashCode(Data));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => WdiGenericRequest.ToByteArray(this);

        internal static WdiGenericRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<WdiGenericRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(WdiGenericRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
