/* This file is generated. Do not modify by hand. */
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;

namespace Zaber.Motion.Requests
{
    internal class RenumberAllRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("firstAddress")]
        public int FirstAddress { get; set; }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => RenumberAllRequest.ToByteArray(this);

        internal static RenumberAllRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<RenumberAllRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(RenumberAllRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
