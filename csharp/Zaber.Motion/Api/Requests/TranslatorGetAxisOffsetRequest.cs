/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class TranslatorGetAxisOffsetRequest : IMessage
    {
        [JsonProperty("translatorId")]
        public int TranslatorId { get; set; }

        [JsonProperty("coordinateSystem")]
        public string CoordinateSystem { get; set; } = string.Empty;

        [JsonProperty("axis")]
        public string Axis { get; set; } = string.Empty;

        [JsonProperty("unit")]
        public Units Unit { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TranslatorGetAxisOffsetRequest))
            {
                return false;
            }

            TranslatorGetAxisOffsetRequest other = (TranslatorGetAxisOffsetRequest)obj;
            return (
                EqualityUtils.CheckEquals(TranslatorId, other.TranslatorId) &&
                EqualityUtils.CheckEquals(CoordinateSystem, other.CoordinateSystem) &&
                EqualityUtils.CheckEquals(Axis, other.Axis) &&
                EqualityUtils.CheckEquals(Unit, other.Unit)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(TranslatorId));
            hash.Add(EqualityUtils.GenerateHashCode(CoordinateSystem));
            hash.Add(EqualityUtils.GenerateHashCode(Axis));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TranslatorGetAxisOffsetRequest.ToByteArray(this);

        internal static TranslatorGetAxisOffsetRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TranslatorGetAxisOffsetRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TranslatorGetAxisOffsetRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
