/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class AxesEmptyRequest : IMessage
    {
        [JsonProperty("interfaces")]
        public int[] Interfaces { get; set; } = System.Array.Empty<int>();

        [JsonProperty("devices")]
        public int[] Devices { get; set; } = System.Array.Empty<int>();

        [JsonProperty("axes")]
        public int[] Axes { get; set; } = System.Array.Empty<int>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AxesEmptyRequest))
            {
                return false;
            }

            AxesEmptyRequest other = (AxesEmptyRequest)obj;
            return (
                EqualityUtils.CheckEquals(Interfaces, other.Interfaces) &&
                EqualityUtils.CheckEquals(Devices, other.Devices) &&
                EqualityUtils.CheckEquals(Axes, other.Axes)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Interfaces));
            hash.Add(EqualityUtils.GenerateHashCode(Devices));
            hash.Add(EqualityUtils.GenerateHashCode(Axes));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AxesEmptyRequest.ToByteArray(this);

        internal static AxesEmptyRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AxesEmptyRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AxesEmptyRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
