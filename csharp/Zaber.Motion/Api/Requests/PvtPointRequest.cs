/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class PvtPointRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("streamId")]
        public int StreamId { get; set; }

        [JsonProperty("pvt")]
        public bool Pvt { get; set; }

        [JsonProperty("type")]
        public StreamSegmentType Type { get; set; }

        [JsonProperty("positions")]
        public Measurement[] Positions { get; set; } = System.Array.Empty<Measurement>();

        [JsonProperty("velocities")]
        public Measurement?[] Velocities { get; set; } = System.Array.Empty<Measurement?>();

        [JsonProperty("time")]
        public Measurement Time { get; set; } = new Measurement { };

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is PvtPointRequest))
            {
                return false;
            }

            PvtPointRequest other = (PvtPointRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(StreamId, other.StreamId) &&
                EqualityUtils.CheckEquals(Pvt, other.Pvt) &&
                EqualityUtils.CheckEquals(Type, other.Type) &&
                EqualityUtils.CheckEquals(Positions, other.Positions) &&
                EqualityUtils.CheckEquals(Velocities, other.Velocities) &&
                EqualityUtils.CheckEquals(Time, other.Time)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(StreamId));
            hash.Add(EqualityUtils.GenerateHashCode(Pvt));
            hash.Add(EqualityUtils.GenerateHashCode(Type));
            hash.Add(EqualityUtils.GenerateHashCode(Positions));
            hash.Add(EqualityUtils.GenerateHashCode(Velocities));
            hash.Add(EqualityUtils.GenerateHashCode(Time));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => PvtPointRequest.ToByteArray(this);

        internal static PvtPointRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<PvtPointRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(PvtPointRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
