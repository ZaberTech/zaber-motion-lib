/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class OpenBinaryInterfaceRequest : IMessage
    {
        [JsonProperty("interfaceType")]
        public InterfaceType InterfaceType { get; set; }

        [JsonProperty("portName")]
        public string PortName { get; set; } = string.Empty;

        [JsonProperty("baudRate")]
        public int BaudRate { get; set; }

        [JsonProperty("hostName")]
        public string HostName { get; set; } = string.Empty;

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("useMessageIds")]
        public bool UseMessageIds { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is OpenBinaryInterfaceRequest))
            {
                return false;
            }

            OpenBinaryInterfaceRequest other = (OpenBinaryInterfaceRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceType, other.InterfaceType) &&
                EqualityUtils.CheckEquals(PortName, other.PortName) &&
                EqualityUtils.CheckEquals(BaudRate, other.BaudRate) &&
                EqualityUtils.CheckEquals(HostName, other.HostName) &&
                EqualityUtils.CheckEquals(Port, other.Port) &&
                EqualityUtils.CheckEquals(UseMessageIds, other.UseMessageIds)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceType));
            hash.Add(EqualityUtils.GenerateHashCode(PortName));
            hash.Add(EqualityUtils.GenerateHashCode(BaudRate));
            hash.Add(EqualityUtils.GenerateHashCode(HostName));
            hash.Add(EqualityUtils.GenerateHashCode(Port));
            hash.Add(EqualityUtils.GenerateHashCode(UseMessageIds));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => OpenBinaryInterfaceRequest.ToByteArray(this);

        internal static OpenBinaryInterfaceRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<OpenBinaryInterfaceRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(OpenBinaryInterfaceRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
