/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class StreamArcRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("streamId")]
        public int StreamId { get; set; }

        [JsonProperty("pvt")]
        public bool Pvt { get; set; }

        [JsonProperty("type")]
        public StreamSegmentType Type { get; set; }

        [JsonProperty("rotationDirection")]
        public RotationDirection RotationDirection { get; set; }

        [JsonProperty("centerX")]
        public Measurement CenterX { get; set; } = new Measurement { };

        [JsonProperty("centerY")]
        public Measurement CenterY { get; set; } = new Measurement { };

        [JsonProperty("endX")]
        public Measurement EndX { get; set; } = new Measurement { };

        [JsonProperty("endY")]
        public Measurement EndY { get; set; } = new Measurement { };

        [JsonProperty("targetAxesIndices")]
        public int[] TargetAxesIndices { get; set; } = System.Array.Empty<int>();

        [JsonProperty("endpoint")]
        public Measurement[] Endpoint { get; set; } = System.Array.Empty<Measurement>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is StreamArcRequest))
            {
                return false;
            }

            StreamArcRequest other = (StreamArcRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(StreamId, other.StreamId) &&
                EqualityUtils.CheckEquals(Pvt, other.Pvt) &&
                EqualityUtils.CheckEquals(Type, other.Type) &&
                EqualityUtils.CheckEquals(RotationDirection, other.RotationDirection) &&
                EqualityUtils.CheckEquals(CenterX, other.CenterX) &&
                EqualityUtils.CheckEquals(CenterY, other.CenterY) &&
                EqualityUtils.CheckEquals(EndX, other.EndX) &&
                EqualityUtils.CheckEquals(EndY, other.EndY) &&
                EqualityUtils.CheckEquals(TargetAxesIndices, other.TargetAxesIndices) &&
                EqualityUtils.CheckEquals(Endpoint, other.Endpoint)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(StreamId));
            hash.Add(EqualityUtils.GenerateHashCode(Pvt));
            hash.Add(EqualityUtils.GenerateHashCode(Type));
            hash.Add(EqualityUtils.GenerateHashCode(RotationDirection));
            hash.Add(EqualityUtils.GenerateHashCode(CenterX));
            hash.Add(EqualityUtils.GenerateHashCode(CenterY));
            hash.Add(EqualityUtils.GenerateHashCode(EndX));
            hash.Add(EqualityUtils.GenerateHashCode(EndY));
            hash.Add(EqualityUtils.GenerateHashCode(TargetAxesIndices));
            hash.Add(EqualityUtils.GenerateHashCode(Endpoint));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => StreamArcRequest.ToByteArray(this);

        internal static StreamArcRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<StreamArcRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(StreamArcRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
