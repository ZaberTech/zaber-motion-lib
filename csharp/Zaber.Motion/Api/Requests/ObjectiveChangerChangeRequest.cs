/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class ObjectiveChangerChangeRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("turretAddress")]
        public int TurretAddress { get; set; }

        [JsonProperty("focusAddress")]
        public int FocusAddress { get; set; }

        [JsonProperty("focusAxis")]
        public int FocusAxis { get; set; }

        [JsonProperty("objective")]
        public int Objective { get; set; }

        [JsonProperty("focusOffset")]
        public Measurement? FocusOffset { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is ObjectiveChangerChangeRequest))
            {
                return false;
            }

            ObjectiveChangerChangeRequest other = (ObjectiveChangerChangeRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(TurretAddress, other.TurretAddress) &&
                EqualityUtils.CheckEquals(FocusAddress, other.FocusAddress) &&
                EqualityUtils.CheckEquals(FocusAxis, other.FocusAxis) &&
                EqualityUtils.CheckEquals(Objective, other.Objective) &&
                EqualityUtils.CheckEquals(FocusOffset, other.FocusOffset)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(TurretAddress));
            hash.Add(EqualityUtils.GenerateHashCode(FocusAddress));
            hash.Add(EqualityUtils.GenerateHashCode(FocusAxis));
            hash.Add(EqualityUtils.GenerateHashCode(Objective));
            hash.Add(EqualityUtils.GenerateHashCode(FocusOffset));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => ObjectiveChangerChangeRequest.ToByteArray(this);

        internal static ObjectiveChangerChangeRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<ObjectiveChangerChangeRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(ObjectiveChangerChangeRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
