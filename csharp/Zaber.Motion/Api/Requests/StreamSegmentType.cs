/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum StreamSegmentType
    {
        /// <summary>Abs.</summary>
        Abs = 0,

        /// <summary>Rel.</summary>
        Rel = 1,

    }
}
