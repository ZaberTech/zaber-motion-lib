/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class ObjectiveChangerCreateResponse : IMessage
    {
        [JsonProperty("turret")]
        public int Turret { get; set; }

        [JsonProperty("focusAddress")]
        public int FocusAddress { get; set; }

        [JsonProperty("focusAxis")]
        public int FocusAxis { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is ObjectiveChangerCreateResponse))
            {
                return false;
            }

            ObjectiveChangerCreateResponse other = (ObjectiveChangerCreateResponse)obj;
            return (
                EqualityUtils.CheckEquals(Turret, other.Turret) &&
                EqualityUtils.CheckEquals(FocusAddress, other.FocusAddress) &&
                EqualityUtils.CheckEquals(FocusAxis, other.FocusAxis)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Turret));
            hash.Add(EqualityUtils.GenerateHashCode(FocusAddress));
            hash.Add(EqualityUtils.GenerateHashCode(FocusAxis));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => ObjectiveChangerCreateResponse.ToByteArray(this);

        internal static ObjectiveChangerCreateResponse FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<ObjectiveChangerCreateResponse>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(ObjectiveChangerCreateResponse instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
