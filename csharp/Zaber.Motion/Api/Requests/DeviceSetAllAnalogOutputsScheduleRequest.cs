/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class DeviceSetAllAnalogOutputsScheduleRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("values")]
        public double[] Values { get; set; } = System.Array.Empty<double>();

        [JsonProperty("futureValues")]
        public double[] FutureValues { get; set; } = System.Array.Empty<double>();

        [JsonProperty("delay")]
        public double Delay { get; set; }

        [JsonProperty("unit")]
        public Units Unit { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is DeviceSetAllAnalogOutputsScheduleRequest))
            {
                return false;
            }

            DeviceSetAllAnalogOutputsScheduleRequest other = (DeviceSetAllAnalogOutputsScheduleRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(Values, other.Values) &&
                EqualityUtils.CheckEquals(FutureValues, other.FutureValues) &&
                EqualityUtils.CheckEquals(Delay, other.Delay) &&
                EqualityUtils.CheckEquals(Unit, other.Unit)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(Values));
            hash.Add(EqualityUtils.GenerateHashCode(FutureValues));
            hash.Add(EqualityUtils.GenerateHashCode(Delay));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => DeviceSetAllAnalogOutputsScheduleRequest.ToByteArray(this);

        internal static DeviceSetAllAnalogOutputsScheduleRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<DeviceSetAllAnalogOutputsScheduleRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(DeviceSetAllAnalogOutputsScheduleRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
