/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class OscilloscopeDataGetSampleTimeRequest : IMessage
    {
        [JsonProperty("dataId")]
        public int DataId { get; set; }

        [JsonProperty("unit")]
        public Units Unit { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is OscilloscopeDataGetSampleTimeRequest))
            {
                return false;
            }

            OscilloscopeDataGetSampleTimeRequest other = (OscilloscopeDataGetSampleTimeRequest)obj;
            return (
                EqualityUtils.CheckEquals(DataId, other.DataId) &&
                EqualityUtils.CheckEquals(Unit, other.Unit) &&
                EqualityUtils.CheckEquals(Index, other.Index)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DataId));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            hash.Add(EqualityUtils.GenerateHashCode(Index));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => OscilloscopeDataGetSampleTimeRequest.ToByteArray(this);

        internal static OscilloscopeDataGetSampleTimeRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<OscilloscopeDataGetSampleTimeRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(OscilloscopeDataGetSampleTimeRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
