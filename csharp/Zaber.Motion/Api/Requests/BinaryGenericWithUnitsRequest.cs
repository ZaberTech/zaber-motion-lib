/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Binary;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class BinaryGenericWithUnitsRequest : IMessage
    {
        [JsonProperty("interfaceId")]
        public int InterfaceId { get; set; }

        [JsonProperty("device")]
        public int Device { get; set; }

        [JsonProperty("command")]
        public CommandCode Command { get; set; }

        [JsonProperty("data")]
        public double Data { get; set; }

        [JsonProperty("fromUnit")]
        public Units FromUnit { get; set; }

        [JsonProperty("toUnit")]
        public Units ToUnit { get; set; }

        [JsonProperty("timeout")]
        public double Timeout { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is BinaryGenericWithUnitsRequest))
            {
                return false;
            }

            BinaryGenericWithUnitsRequest other = (BinaryGenericWithUnitsRequest)obj;
            return (
                EqualityUtils.CheckEquals(InterfaceId, other.InterfaceId) &&
                EqualityUtils.CheckEquals(Device, other.Device) &&
                EqualityUtils.CheckEquals(Command, other.Command) &&
                EqualityUtils.CheckEquals(Data, other.Data) &&
                EqualityUtils.CheckEquals(FromUnit, other.FromUnit) &&
                EqualityUtils.CheckEquals(ToUnit, other.ToUnit) &&
                EqualityUtils.CheckEquals(Timeout, other.Timeout)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InterfaceId));
            hash.Add(EqualityUtils.GenerateHashCode(Device));
            hash.Add(EqualityUtils.GenerateHashCode(Command));
            hash.Add(EqualityUtils.GenerateHashCode(Data));
            hash.Add(EqualityUtils.GenerateHashCode(FromUnit));
            hash.Add(EqualityUtils.GenerateHashCode(ToUnit));
            hash.Add(EqualityUtils.GenerateHashCode(Timeout));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => BinaryGenericWithUnitsRequest.ToByteArray(this);

        internal static BinaryGenericWithUnitsRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<BinaryGenericWithUnitsRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(BinaryGenericWithUnitsRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
