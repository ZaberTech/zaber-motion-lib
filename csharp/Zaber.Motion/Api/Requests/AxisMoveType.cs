/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum AxisMoveType
    {
        /// <summary>Abs.</summary>
        Abs = 0,

        /// <summary>Rel.</summary>
        Rel = 1,

        /// <summary>Vel.</summary>
        Vel = 2,

        /// <summary>Max.</summary>
        Max = 3,

        /// <summary>Min.</summary>
        Min = 4,

        /// <summary>Index.</summary>
        Index = 5,

    }
}
