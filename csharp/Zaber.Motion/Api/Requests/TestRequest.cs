/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Requests
{
    internal class TestRequest : IMessage
    {
        [JsonProperty("returnError")]
        public bool ReturnError { get; set; }

        [JsonProperty("dataPing")]
        public string DataPing { get; set; } = string.Empty;

        [JsonProperty("returnErrorWithData")]
        public bool ReturnErrorWithData { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TestRequest))
            {
                return false;
            }

            TestRequest other = (TestRequest)obj;
            return (
                EqualityUtils.CheckEquals(ReturnError, other.ReturnError) &&
                EqualityUtils.CheckEquals(DataPing, other.DataPing) &&
                EqualityUtils.CheckEquals(ReturnErrorWithData, other.ReturnErrorWithData)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(ReturnError));
            hash.Add(EqualityUtils.GenerateHashCode(DataPing));
            hash.Add(EqualityUtils.GenerateHashCode(ReturnErrorWithData));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TestRequest.ToByteArray(this);

        internal static TestRequest FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TestRequest>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TestRequest instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
