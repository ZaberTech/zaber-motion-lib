/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Requests
{
    internal enum ResponseType
    {
        /// <summary>Ok.</summary>
        Ok = 0,

        /// <summary>Error.</summary>
        Error = 1,

    }
}
