/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion
{
    /// <summary>
    /// Direction of rotation.
    /// </summary>
    public enum RotationDirection
    {
        /// <summary>Clockwise.</summary>
        Clockwise = 0,

        /// <summary>Counterclockwise.</summary>
        Counterclockwise = 1,

        /// <summary>CW.</summary>
        CW = 0,

        /// <summary>CCW.</summary>
        CCW = 1,

    }
}
