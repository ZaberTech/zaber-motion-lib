﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma warning disable CA1054

using System;

using Zaber.Motion.Runtime;

namespace Zaber.Motion
{
    /// <summary>
    /// Access class to general library information and configuration.
    /// </summary>
    public static class Library
    {
        static Library()
        {
            Library.CheckVersion();
        }

        /// <summary>
        /// Sets library logging output.
        /// </summary>
        /// <param name="mode">Logging output mode.</param>
        /// <param name="filePath">Path of the file to open.</param>
        public static void SetLogOutput(LogOutputMode mode, string? filePath = null)
        {
            var request = new Requests.SetLogOutputRequest()
            {
                Mode = mode,
                FilePath = filePath,
            };

            Gateway.CallSync("logging/set_output", request);
        }


        /// <summary>
        /// Sets source of Device DB data. Allows selection of a web service or a local file.
        /// </summary>
        /// <param name="sourceType">Source type.</param>
        /// <param name="urlOrFilePath">URL of the web service or path to the local file.
        /// Leave empty for the default URL of Zaber web service.</param>
        public static void SetDeviceDbSource(DeviceDbSourceType sourceType, string? urlOrFilePath = null)
        {
            var request = new Requests.SetDeviceDbSourceRequest()
            {
                SourceType = sourceType,
                UrlOrFilePath = urlOrFilePath,
            };

            Gateway.CallSync("device_db/set_source", request);
        }


        /// <summary>
        /// Enables Device DB store.
        /// The store uses filesystem to save information obtained from the Device DB.
        /// The stored data are later used instead of the Device DB.
        /// </summary>
        /// <param name="storeLocation">Specifies relative or absolute path of the folder used by the store.
        /// If left empty defaults to a folder in user home directory.
        /// Must be accessible by the process.</param>
        public static void EnableDeviceDbStore(string? storeLocation = null)
        {
            var request = new Requests.ToggleDeviceDbStoreRequest()
            {
                ToggleOn = true,
                StoreLocation = storeLocation,
            };

            Gateway.CallSync("device_db/toggle_store", request);
        }


        /// <summary>
        /// Disables Device DB store.
        /// </summary>
        public static void DisableDeviceDbStore()
        {
            var request = new Requests.ToggleDeviceDbStoreRequest()
            {
            };

            Gateway.CallSync("device_db/toggle_store", request);
        }


        /// <summary>
        /// Disables certain customer checks (like FF flag).
        /// </summary>
        /// <param name="mode">Whether to turn internal mode on or off.</param>
        public static void SetInternalMode(bool mode)
        {
            var request = new Requests.SetInternalModeRequest()
            {
                Mode = mode,
            };

            Gateway.CallSync("library/set_internal_mode", request);
        }


        /// <summary>
        /// Sets the period between polling for IDLE during movements.
        /// Caution: Setting the period too low may cause performance issues.
        /// </summary>
        /// <param name="period">Period in milliseconds.
        /// Negative value restores the default period.</param>
        public static void SetIdlePollingPeriod(int period)
        {
            var request = new Requests.IntRequest()
            {
                Value = period,
            };

            Gateway.CallSync("library/set_idle_polling_period", request);
        }


        /// <summary>
        /// Throws an error if the version of the loaded shared library does not match the caller's version.
        /// </summary>
        public static void CheckVersion()
        {
            var request = new Requests.CheckVersionRequest()
            {
                Host = "cs",
                Version = "7.5.0",
            };

            Gateway.CallSync("library/check_version", request);
        }


    }
}
