/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Product
{
    /// <summary>
    /// Servo Tuning Parameter Set to target.
    /// </summary>
    public enum ProcessControllerMode
    {
        /// <summary>Manual.</summary>
        Manual = 0,

        /// <summary>Pid.</summary>
        Pid = 1,

        /// <summary>PidHeater.</summary>
        PidHeater = 2,

        /// <summary>OnOff.</summary>
        OnOff = 3,

    }
}
