﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;

namespace Zaber.Motion.Product
{
    /// <summary>
    /// Represents the controller part of one device - may be either a standalone controller or an integrated controller.
    /// </summary>
    public class ProcessController
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `ProcessController` of the given device.
        /// If the device is identified, this constructor will ensure it is a process controller.
        /// </summary>
        public ProcessController(Device device)
        {
            Device = device;
            VerifyIsProcessController();
        }
#pragma warning restore SA1611


        /// <summary>
        /// The base device of this process controller.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Detects the process controllers on the connection.
        /// </summary>
        /// <param name="connection">The connection to detect process controllers on.</param>
        /// <param name="identify">If the Process Controllers should be identified upon detection.</param>
        /// <returns>A Task that can be awaited to get the result: A list of all `ProcessController`s on the connection.</returns>
        public static async Task<ProcessController[]> DetectAsync(Connection connection, bool identify = true)
        {
            var request = new Requests.DeviceDetectRequest()
            {
                Type = Requests.DeviceType.ProcessController,
                InterfaceId = connection.InterfaceId,
                IdentifyDevices = identify,
            };

            var response = await Gateway.CallAsync("device/detect", request, Requests.DeviceDetectResponse.FromByteArray).ConfigureAwait(false);
            return response.Devices.Select(device => new ProcessController(connection.GetDevice(device))).ToArray();
        }


        /// <summary>
        /// Detects the process controllers on the connection.
        /// </summary>
        /// <param name="connection">The connection to detect process controllers on.</param>
        /// <param name="identify">If the Process Controllers should be identified upon detection.</param>
        /// <returns>A list of all `ProcessController`s on the connection.</returns>
        public static ProcessController[] Detect(Connection connection, bool identify = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DetectAsync(connection, identify);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets an Process class instance which allows you to control a particular voltage source.
        /// Axes are numbered from 1.
        /// </summary>
        /// <param name="processNumber">Number of process to control.</param>
        /// <returns>Process instance.</returns>
        public Process GetProcess(int processNumber)
        {
            if (processNumber <= 0)
            {
                throw new ArgumentException("Invalid value; processes are numbered from 1.");
            }

            return new Process(this, processNumber);
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Checks if this is a process controller or some other type of device and throws an error if it is not.
        /// </summary>
        private void VerifyIsProcessController()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            Gateway.CallSync("process_controller/verify", request);
        }


    }
}
