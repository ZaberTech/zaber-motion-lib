/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Product
{
    /// <summary>
    /// The source used by a process in a closed-loop mode.
    /// </summary>
    public class ProcessControllerSource : IMessage
    {
        /// <summary>
        /// The type of input sensor.
        /// </summary>
        [JsonProperty("sensor")]
        public ProcessControllerSourceSensor Sensor { get; set; }

        /// <summary>
        /// The specific input to use.
        /// </summary>
        [JsonProperty("port")]
        public int Port { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is ProcessControllerSource))
            {
                return false;
            }

            ProcessControllerSource other = (ProcessControllerSource)obj;
            return (
                EqualityUtils.CheckEquals(Sensor, other.Sensor) &&
                EqualityUtils.CheckEquals(Port, other.Port)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Sensor));
            hash.Add(EqualityUtils.GenerateHashCode(Port));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => ProcessControllerSource.ToByteArray(this);

        internal static ProcessControllerSource FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<ProcessControllerSource>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(ProcessControllerSource instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
