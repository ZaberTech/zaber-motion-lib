﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion;
using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;

namespace Zaber.Motion.Product
{
    /// <summary>
    /// Use to drive voltage for a process such as a heater, valve, Peltier device, etc.
    /// Requires at least Firmware 7.35.
    /// </summary>
    public class Process
    {
        internal Process(ProcessController controller, int processNumber)
        {
            Controller = controller;
            ProcessNumber = processNumber;
            _axis = new Axis(controller.Device, processNumber);
            Settings = new AxisSettings(_axis);
            Storage = new AxisStorage(_axis);
            Warnings = new Warnings(controller.Device, processNumber);
        }


        /// <summary>
        /// Controller for this process.
        /// </summary>
        public ProcessController Controller { get; private set; }


        /// <summary>
        /// The process number identifies the process on the controller.
        /// </summary>
        public int ProcessNumber { get; private set; }


        /// <summary>
        /// Settings and properties of this process.
        /// </summary>
        public AxisSettings Settings { get; private set; }


        /// <summary>
        /// Key-value storage of this process.
        /// </summary>
        public AxisStorage Storage { get; private set; }


        /// <summary>
        /// Warnings and faults of this process.
        /// </summary>
        public Warnings Warnings { get; private set; }


        /// <summary>
        /// Sets the enabled state of the driver.
        /// </summary>
        /// <param name="enabled">If true (default) enables drive. If false disables.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EnableAsync(bool enabled = true)
        {
            var request = new Requests.ProcessOn()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                On = enabled,
            };

            await Gateway.CallAsync("process-controller/enable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the enabled state of the driver.
        /// </summary>
        /// <param name="enabled">If true (default) enables drive. If false disables.</param>
        public void Enable(bool enabled = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EnableAsync(enabled);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
        /// </summary>
        /// <param name="duration">How long to leave the process on.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OnAsync(double duration = 0, Units unit = Units.Native)
        {
            var request = new Requests.ProcessOn()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                On = true,
                Duration = duration,
                Unit = unit,
            };

            await Gateway.CallAsync("process-controller/on", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
        /// </summary>
        /// <param name="duration">How long to leave the process on.</param>
        /// <param name="unit">Units of time.</param>
        public void On(double duration = 0, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OnAsync(duration, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Turns this process off.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OffAsync()
        {
            var request = new Requests.ProcessOn()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                On = false,
            };

            await Gateway.CallAsync("process-controller/on", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Turns this process off.
        /// </summary>
        public void Off()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OffAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the control mode of this process.
        /// </summary>
        /// <param name="mode">Mode to set this process to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetModeAsync(ProcessControllerMode mode)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Setting = "process.control.mode",
                Value = (double)mode,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the control mode of this process.
        /// </summary>
        /// <param name="mode">Mode to set this process to.</param>
        public void SetMode(ProcessControllerMode mode)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetModeAsync(mode);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the control mode of this process.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Control mode.</returns>
        public async Task<ProcessControllerMode> GetModeAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Setting = "process.control.mode",
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return (ProcessControllerMode)response.Value;
        }


        /// <summary>
        /// Gets the control mode of this process.
        /// </summary>
        /// <returns>Control mode.</returns>
        public ProcessControllerMode GetMode()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetModeAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the source used to control this process.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The source providing feedback for this process.</returns>
        public async Task<ProcessControllerSource> GetSourceAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
            };

            var response = await Gateway.CallAsync("process_controller/get_source", request, ProcessControllerSource.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the source used to control this process.
        /// </summary>
        /// <returns>The source providing feedback for this process.</returns>
        public ProcessControllerSource GetSource()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetSourceAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the source used to control this process.
        /// </summary>
        /// <param name="source">Sets the source that should provide feedback for this process.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetSourceAsync(ProcessControllerSource source)
        {
            var request = new Requests.SetProcessControllerSource()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Source = source,
            };

            await Gateway.CallAsync("process_controller/set_source", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the source used to control this process.
        /// </summary>
        /// <param name="source">Sets the source that should provide feedback for this process.</param>
        public void SetSource(ProcessControllerSource source)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetSourceAsync(source);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the current value of the source used to control this process.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The current value of this process's controlling source.</returns>
        public async Task<Measurement> GetInputAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
            };

            var response = await Gateway.CallAsync("process_controller/get_input", request, Measurement.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the current value of the source used to control this process.
        /// </summary>
        /// <returns>The current value of this process's controlling source.</returns>
        public Measurement GetInput()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetInputAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task BridgeAsync()
        {
            var request = new Requests.ProcessOn()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                On = true,
            };

            await Gateway.CallAsync("process_controller/bridge", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
        /// </summary>
        public void Bridge()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = BridgeAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
        /// This method is only callable on axis 1 and 3.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UnbridgeAsync()
        {
            var request = new Requests.ProcessOn()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                On = false,
            };

            await Gateway.CallAsync("process_controller/bridge", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
        /// This method is only callable on axis 1 and 3.
        /// </summary>
        public void Unbridge()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UnbridgeAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Detects if the given process is in bridging mode.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Whether this process is bridged with its neighbor.</returns>
        public async Task<bool> IsBridgeAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
            };

            var response = await Gateway.CallAsync("process_controller/is_bridge", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Detects if the given process is in bridging mode.
        /// </summary>
        /// <returns>Whether this process is bridged with its neighbor.</returns>
        public bool IsBridge()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBridgeAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this process' underlying axis.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Response> GenericCommandAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command", request, Response.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic ASCII command to this process' underlying axis.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A response to the command.</returns>
        public Response GenericCommand(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this process and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Response[]> GenericCommandMultiResponseAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command_multi_response", request, Requests.GenericCommandResponseCollection.FromByteArray).ConfigureAwait(false);
            return response.Responses;
        }


        /// <summary>
        /// Sends a generic ASCII command to this process and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>All responses to the command.</returns>
        public Response[] GenericCommandMultiResponse(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this process without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(string command)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                Command = command,
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to this process without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        public void GenericCommandNoResponse(string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a serialization of the current process state that can be saved and reapplied.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A serialization of the current state of the process.</returns>
        public async Task<string> GetStateAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
            };

            var response = await Gateway.CallAsync("device/get_state", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current process state that can be saved and reapplied.
        /// </summary>
        /// <returns>A serialization of the current state of the process.</returns>
        public string GetState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Applies a saved state to this process.
        /// </summary>
        /// <param name="state">The state object to apply to this process.</param>
        /// <returns>A Task that can be awaited to get the result: Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public async Task<SetStateAxisResponse> SetStateAsync(string state)
        {
            var request = new Requests.SetStateRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                State = state,
            };

            var response = await Gateway.CallAsync("device/set_axis_state", request, SetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Applies a saved state to this process.
        /// </summary>
        /// <param name="state">The state object to apply to this process.</param>
        /// <returns>Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public SetStateAxisResponse SetState(string state)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStateAsync(state);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if a state can be applied to this process.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>A Task that can be awaited to get the result: An explanation of why this state cannot be set to this process.</returns>
        public async Task<string?> CanSetStateAsync(string state, FirmwareVersion? firmwareVersion = null)
        {
            var request = new Requests.CanSetStateRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                State = state,
                FirmwareVersion = firmwareVersion,
            };

            var response = await Gateway.CallAsync("device/can_set_axis_state", request, Requests.CanSetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response.Error;
        }


        /// <summary>
        /// Checks if a state can be applied to this process.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>An explanation of why this state cannot be set to this process.</returns>
        public string? CanSetState(string state, FirmwareVersion? firmwareVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CanSetStateAsync(state, firmwareVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the process.
        /// </summary>
        /// <returns>A string that represents the process.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Controller.Device.Connection.InterfaceId,
                Device = Controller.Device.DeviceAddress,
                Axis = ProcessNumber,
                TypeOverride = "Process",
            };

            var response = Gateway.CallSync("device/axis_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        private Axis _axis;


    }
}
