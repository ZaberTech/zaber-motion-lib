/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Product
{
    /// <summary>
    /// Servo Tuning Parameter Set to target.
    /// </summary>
    public enum ProcessControllerSourceSensor
    {
        /// <summary>Thermistor.</summary>
        Thermistor = 10,

        /// <summary>AnalogInput.</summary>
        AnalogInput = 20,

    }
}
