/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion
{
    /// <summary>
    /// Class representing version of firmware in the controller.
    /// </summary>
    public class FirmwareVersion : IMessage
    {
        /// <summary>
        /// Major version number.
        /// </summary>
        [JsonProperty("major")]
        public int Major { get; set; }

        /// <summary>
        /// Minor version number.
        /// </summary>
        [JsonProperty("minor")]
        public int Minor { get; set; }

        /// <summary>
        /// Build version number.
        /// </summary>
        [JsonProperty("build")]
        public int Build { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is FirmwareVersion))
            {
                return false;
            }

            FirmwareVersion other = (FirmwareVersion)obj;
            return (
                EqualityUtils.CheckEquals(Major, other.Major) &&
                EqualityUtils.CheckEquals(Minor, other.Minor) &&
                EqualityUtils.CheckEquals(Build, other.Build)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Major));
            hash.Add(EqualityUtils.GenerateHashCode(Minor));
            hash.Add(EqualityUtils.GenerateHashCode(Build));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => FirmwareVersion.ToByteArray(this);

        internal static FirmwareVersion FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<FirmwareVersion>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(FirmwareVersion instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
