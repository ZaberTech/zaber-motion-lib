/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Denotes type of the stream axis.
    /// </summary>
    public enum StreamAxisType
    {
        /// <summary>Physical.</summary>
        Physical = 0,

        /// <summary>Lockstep.</summary>
        Lockstep = 1,

    }
}
