/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Mode of a PVT sequence.
    /// </summary>
    public enum PvtMode
    {
        /// <summary>Disabled.</summary>
        Disabled = 0,

        /// <summary>Store.</summary>
        Store = 1,

        /// <summary>Live.</summary>
        Live = 2,

    }
}
