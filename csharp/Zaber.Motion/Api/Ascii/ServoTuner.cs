﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //
#pragma warning disable 612, 618

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Exposes the capabilities to inspect and edit an axis' servo tuning.
    /// Requires at least Firmware 6.25 or 7.00.
    /// </summary>
    public class ServoTuner
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of ServoTuner for the given axis.
        /// </summary>
        public ServoTuner(Axis axis)
        {
            Axis = axis;
        }
#pragma warning restore SA1611


        /// <summary>
        /// The axis that will be tuned.
        /// </summary>
        public Axis Axis { get; private set; }


        /// <summary>
        /// Get the paramset that this device uses by default when it starts up.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The paramset used when the device restarts.</returns>
        public async Task<ServoTuningParamset> GetStartupParamsetAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
            };

            var response = await Gateway.CallAsync("servotuning/get_startup_set", request, Requests.ServoTuningParamsetResponse.FromByteArray).ConfigureAwait(false);
            return response.Paramset;
        }


        /// <summary>
        /// Get the paramset that this device uses by default when it starts up.
        /// </summary>
        /// <returns>The paramset used when the device restarts.</returns>
        public ServoTuningParamset GetStartupParamset()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStartupParamsetAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set the paramset that this device uses by default when it starts up.
        /// </summary>
        /// <param name="paramset">The paramset to use at startup.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetStartupParamsetAsync(ServoTuningParamset paramset)
        {
            var request = new Requests.ServoTuningRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
            };

            await Gateway.CallAsync("servotuning/set_startup_set", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set the paramset that this device uses by default when it starts up.
        /// </summary>
        /// <param name="paramset">The paramset to use at startup.</param>
        public void SetStartupParamset(ServoTuningParamset paramset)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStartupParamsetAsync(paramset);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Load the values from one paramset into another.
        /// </summary>
        /// <param name="toParamset">The paramset to load into.</param>
        /// <param name="fromParamset">The paramset to load from.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task LoadParamsetAsync(ServoTuningParamset toParamset, ServoTuningParamset fromParamset)
        {
            var request = new Requests.LoadParamset()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                ToParamset = toParamset,
                FromParamset = fromParamset,
            };

            await Gateway.CallAsync("servotuning/load_paramset", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Load the values from one paramset into another.
        /// </summary>
        /// <param name="toParamset">The paramset to load into.</param>
        /// <param name="fromParamset">The paramset to load from.</param>
        public void LoadParamset(ServoTuningParamset toParamset, ServoTuningParamset fromParamset)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = LoadParamsetAsync(toParamset, fromParamset);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the full set of tuning parameters used by the firmware driving this axis.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>A Task that can be awaited to get the result: The raw representation of the current tuning.</returns>
        public async Task<ParamsetInfo> GetTuningAsync(ServoTuningParamset paramset)
        {
            var request = new Requests.ServoTuningRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
            };

            var response = await Gateway.CallAsync("servotuning/get_raw", request, ParamsetInfo.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Get the full set of tuning parameters used by the firmware driving this axis.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>The raw representation of the current tuning.</returns>
        public ParamsetInfo GetTuning(ServoTuningParamset paramset)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetTuningAsync(paramset);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set individual tuning parameters.
        /// Only use this method if you have a strong understanding of Zaber specific tuning parameters.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning of.</param>
        /// <param name="tuningParams">The params to set.</param>
        /// <param name="setUnspecifiedToDefault">If true, any tuning parameters not included in TuningParams
        /// are reset to their default values.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetTuningAsync(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, bool setUnspecifiedToDefault = false)
        {
            var request = new Requests.SetServoTuningRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
                TuningParams = tuningParams,
                SetUnspecifiedToDefault = setUnspecifiedToDefault,
            };

            await Gateway.CallAsync("servotuning/set_raw", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set individual tuning parameters.
        /// Only use this method if you have a strong understanding of Zaber specific tuning parameters.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning of.</param>
        /// <param name="tuningParams">The params to set.</param>
        /// <param name="setUnspecifiedToDefault">If true, any tuning parameters not included in TuningParams
        /// are reset to their default values.</param>
        public void SetTuning(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, bool setUnspecifiedToDefault = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetTuningAsync(paramset, tuningParams, setUnspecifiedToDefault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the tuning of a paramset using the PID method.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <param name="p">The proportional gain. Must be in units of N/m.</param>
        /// <param name="i">The integral gain. Must be in units of N/m⋅s.</param>
        /// <param name="d">The derivative gain. Must be in units of N⋅s/m.</param>
        /// <param name="fc">The cutoff frequency. Must be in units of Hz.</param>
        /// <returns>A Task that can be awaited to get the result: The PID representation of the current tuning after your changes have been applied.</returns>
        public async Task<PidTuning> SetPidTuningAsync(ServoTuningParamset paramset, double p, double i, double d, double fc)
        {
            var request = new Requests.SetServoTuningPIDRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
                P = p,
                I = i,
                D = d,
                Fc = fc,
            };

            var response = await Gateway.CallAsync("servotuning/set_pid", request, PidTuning.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sets the tuning of a paramset using the PID method.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <param name="p">The proportional gain. Must be in units of N/m.</param>
        /// <param name="i">The integral gain. Must be in units of N/m⋅s.</param>
        /// <param name="d">The derivative gain. Must be in units of N⋅s/m.</param>
        /// <param name="fc">The cutoff frequency. Must be in units of Hz.</param>
        /// <returns>The PID representation of the current tuning after your changes have been applied.</returns>
        public PidTuning SetPidTuning(ServoTuningParamset paramset, double p, double i, double d, double fc)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetPidTuningAsync(paramset, p, i, d, fc);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the PID representation of this paramset's servo tuning.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>A Task that can be awaited to get the result: The PID representation of the current tuning.</returns>
        public async Task<PidTuning> GetPidTuningAsync(ServoTuningParamset paramset)
        {
            var request = new Requests.ServoTuningRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
            };

            var response = await Gateway.CallAsync("servotuning/get_pid", request, PidTuning.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the PID representation of this paramset's servo tuning.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>The PID representation of the current tuning.</returns>
        public PidTuning GetPidTuning(ServoTuningParamset paramset)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetPidTuningAsync(paramset);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the parameters that are required to tune this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The tuning parameters.</returns>
        public async Task<SimpleTuningParamDefinition[]> GetSimpleTuningParamDefinitionsAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
            };

            var response = await Gateway.CallAsync("servotuning/get_simple_params_definition", request, Requests.GetSimpleTuningParamDefinitionResponse.FromByteArray).ConfigureAwait(false);
            return response.Params;
        }


        /// <summary>
        /// Gets the parameters that are required to tune this device.
        /// </summary>
        /// <returns>The tuning parameters.</returns>
        public SimpleTuningParamDefinition[] GetSimpleTuningParamDefinitions()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetSimpleTuningParamDefinitionsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set the tuning of this device using the simple input method.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning for.</param>
        /// <param name="tuningParams">The params used to tune this device.
        /// To get what parameters are expected, call GetSimpleTuningParamList.
        /// All values must be between 0 and 1.</param>
        /// <param name="loadMass">The mass loaded on the stage (excluding the mass of the carriage itself) in kg.</param>
        /// <param name="carriageMass">The mass of the carriage in kg. If this value is not set the default carriage mass is used.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetSimpleTuningAsync(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, double loadMass, double? carriageMass = null)
        {
            var request = new Requests.SetSimpleTuning()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
                TuningParams = tuningParams,
                LoadMass = loadMass,
                CarriageMass = carriageMass,
            };

            await Gateway.CallAsync("servotuning/set_simple_tuning", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set the tuning of this device using the simple input method.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning for.</param>
        /// <param name="tuningParams">The params used to tune this device.
        /// To get what parameters are expected, call GetSimpleTuningParamList.
        /// All values must be between 0 and 1.</param>
        /// <param name="loadMass">The mass loaded on the stage (excluding the mass of the carriage itself) in kg.</param>
        /// <param name="carriageMass">The mass of the carriage in kg. If this value is not set the default carriage mass is used.</param>
        public void SetSimpleTuning(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, double loadMass, double? carriageMass = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetSimpleTuningAsync(paramset, tuningParams, loadMass, carriageMass);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the simple tuning parameters for this device.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>A Task that can be awaited to get the result: The simple tuning parameters.</returns>
        public async Task<SimpleTuning> GetSimpleTuningAsync(ServoTuningParamset paramset)
        {
            var request = new Requests.ServoTuningRequest()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
            };

            var response = await Gateway.CallAsync("servotuning/get_simple_tuning", request, SimpleTuning.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Get the simple tuning parameters for this device.
        /// </summary>
        /// <param name="paramset">The paramset to get tuning for.</param>
        /// <returns>The simple tuning parameters.</returns>
        public SimpleTuning GetSimpleTuning(ServoTuningParamset paramset)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetSimpleTuningAsync(paramset);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if the provided simple tuning is being stored by this paramset.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning for.</param>
        /// <param name="tuningParams">The params used to tune this device.
        /// To get what parameters are expected, call GetSimpleTuningParamList.
        /// All values must be between 0 and 1.</param>
        /// <param name="loadMass">The mass loaded on the stage (excluding the mass of the carriage itself) in kg.</param>
        /// <param name="carriageMass">The mass of the carriage in kg. If this value is not set the default carriage mass is used.</param>
        /// <returns>A Task that can be awaited to get the result: True if the provided simple tuning is currently stored in this paramset.</returns>
        [Obsolete("Use GetSimpleTuning instead.")]
        public async Task<bool> IsUsingSimpleTuningAsync(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, double loadMass, double? carriageMass = null)
        {
            var request = new Requests.SetSimpleTuning()
            {
                InterfaceId = Axis.Device.Connection.InterfaceId,
                Device = Axis.Device.DeviceAddress,
                Axis = Axis.AxisNumber,
                Paramset = paramset,
                TuningParams = tuningParams,
                LoadMass = loadMass,
                CarriageMass = carriageMass,
            };

            var response = await Gateway.CallAsync("servotuning/is_using_simple_tuning", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Checks if the provided simple tuning is being stored by this paramset.
        /// </summary>
        /// <param name="paramset">The paramset to set tuning for.</param>
        /// <param name="tuningParams">The params used to tune this device.
        /// To get what parameters are expected, call GetSimpleTuningParamList.
        /// All values must be between 0 and 1.</param>
        /// <param name="loadMass">The mass loaded on the stage (excluding the mass of the carriage itself) in kg.</param>
        /// <param name="carriageMass">The mass of the carriage in kg. If this value is not set the default carriage mass is used.</param>
        /// <returns>True if the provided simple tuning is currently stored in this paramset.</returns>
        [Obsolete("Use GetSimpleTuning instead.")]
        public bool IsUsingSimpleTuning(ServoTuningParamset paramset, ServoTuningParam[] tuningParams, double loadMass, double? carriageMass = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsUsingSimpleTuningAsync(paramset, tuningParams, loadMass, carriageMass);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}

#pragma warning restore 612, 618
