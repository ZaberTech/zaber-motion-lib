﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// A handle for a PVT sequence with this number on the device.
    /// PVT sequences provide a way execute or store trajectory
    /// consisting of points with defined position, velocity, and time.
    /// PVT sequence methods append actions to a queue which executes
    /// or stores actions in a first in, first out order.
    /// </summary>
    public class PvtSequence
    {
        internal PvtSequence(Device device, int pvtId)
        {
            Device = device;
            PvtId = pvtId;
            Io = new PvtIo(Device, pvtId);
        }


        /// <summary>
        /// Device that controls this PVT sequence.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The number that identifies the PVT sequence on the device.
        /// </summary>
        public int PvtId { get; private set; }


        /// <summary>
        /// Current mode of the PVT sequence.
        /// </summary>
        public PvtMode Mode
        {
            get { return RetrieveMode(); }
        }


        /// <summary>
        /// An array of axes definitions the PVT sequence is set up to control.
        /// </summary>
        public PvtAxisDefinition[] Axes
        {
            get { return RetrieveAxes(); }
        }


        /// <summary>
        /// Gets an object that provides access to I/O for this sequence.
        /// </summary>
        public PvtIo Io { get; private set; }


        /// <summary>
        /// Setup the PVT sequence to control the specified axes and to queue actions on the device.
        /// Allows use of lockstep axes in a PVT sequence.
        /// </summary>
        /// <param name="pvtAxes">Definition of the PVT sequence axes.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupLiveCompositeAsync(params PvtAxisDefinition[] pvtAxes)
        {
            var request = new Requests.StreamSetupLiveCompositeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                PvtAxes = pvtAxes,
            };

            await Gateway.CallAsync("device/stream_setup_live_composite", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the PVT sequence to control the specified axes and to queue actions on the device.
        /// Allows use of lockstep axes in a PVT sequence.
        /// </summary>
        /// <param name="pvtAxes">Definition of the PVT sequence axes.</param>
        public void SetupLiveComposite(params PvtAxisDefinition[] pvtAxes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupLiveCompositeAsync(pvtAxes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the PVT sequence to control the specified axes and to queue actions on the device.
        /// </summary>
        /// <param name="axes">Numbers of physical axes to setup the PVT sequence on.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupLiveAsync(params int[] axes)
        {
            var request = new Requests.StreamSetupLiveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_live", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the PVT sequence to control the specified axes and to queue actions on the device.
        /// </summary>
        /// <param name="axes">Numbers of physical axes to setup the PVT sequence on.</param>
        public void SetupLive(params int[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupLiveAsync(axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
        /// Allows use of lockstep axes in a PVT sequence.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to queue actions in.</param>
        /// <param name="pvtAxes">Definition of the PVT sequence axes.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupStoreCompositeAsync(PvtBuffer pvtBuffer, params PvtAxisDefinition[] pvtAxes)
        {
            var request = new Requests.StreamSetupStoreCompositeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                PvtBuffer = pvtBuffer.BufferId,
                PvtAxes = pvtAxes,
            };

            await Gateway.CallAsync("device/stream_setup_store_composite", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
        /// Allows use of lockstep axes in a PVT sequence.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to queue actions in.</param>
        /// <param name="pvtAxes">Definition of the PVT sequence axes.</param>
        public void SetupStoreComposite(PvtBuffer pvtBuffer, params PvtAxisDefinition[] pvtAxes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupStoreCompositeAsync(pvtBuffer, pvtAxes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to queue actions in.</param>
        /// <param name="axes">Numbers of physical axes to setup the PVT sequence on.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupStoreAsync(PvtBuffer pvtBuffer, params int[] axes)
        {
            var request = new Requests.StreamSetupStoreRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                PvtBuffer = pvtBuffer.BufferId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_store", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to queue actions in.</param>
        /// <param name="axes">Numbers of physical axes to setup the PVT sequence on.</param>
        public void SetupStore(PvtBuffer pvtBuffer, params int[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupStoreAsync(pvtBuffer, axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Append the actions in a PVT buffer to the sequence's queue.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to call.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CallAsync(PvtBuffer pvtBuffer)
        {
            var request = new Requests.StreamCallRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                PvtBuffer = pvtBuffer.BufferId,
            };

            await Gateway.CallAsync("device/stream_call", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Append the actions in a PVT buffer to the sequence's queue.
        /// </summary>
        /// <param name="pvtBuffer">The PVT buffer to call.</param>
        public void Call(PvtBuffer pvtBuffer)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CallAsync(pvtBuffer);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queues a point with absolute coordinates in the PVT sequence.
        /// If some or all velocities are not provided, the sequence calculates the velocities
        /// from surrounding points using finite difference.
        /// The last point of the sequence must have defined velocity (likely zero).
        /// </summary>
        /// <param name="positions">Positions for the axes to move through, relative to their home positions.</param>
        /// <param name="velocities">The axes velocities at the given point.
        /// Specify an empty array or null for specific axes to make the sequence calculate the velocity.</param>
        /// <param name="time">The duration between the previous point in the sequence and this one.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task PointAsync(Measurement[] positions, Measurement?[] velocities, Measurement time)
        {
            var request = new Requests.PvtPointRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Type = Requests.StreamSegmentType.Abs,
                Positions = positions,
                Velocities = velocities,
                Time = time,
            };

            await Gateway.CallAsync("device/stream_point", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queues a point with absolute coordinates in the PVT sequence.
        /// If some or all velocities are not provided, the sequence calculates the velocities
        /// from surrounding points using finite difference.
        /// The last point of the sequence must have defined velocity (likely zero).
        /// </summary>
        /// <param name="positions">Positions for the axes to move through, relative to their home positions.</param>
        /// <param name="velocities">The axes velocities at the given point.
        /// Specify an empty array or null for specific axes to make the sequence calculate the velocity.</param>
        /// <param name="time">The duration between the previous point in the sequence and this one.</param>
        public void Point(Measurement[] positions, Measurement?[] velocities, Measurement time)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = PointAsync(positions, velocities, time);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queues a point with coordinates relative to the previous point in the PVT sequence.
        /// If some or all velocities are not provided, the sequence calculates the velocities
        /// from surrounding points using finite difference.
        /// The last point of the sequence must have defined velocity (likely zero).
        /// </summary>
        /// <param name="positions">Positions for the axes to move through, relative to the previous point.</param>
        /// <param name="velocities">The axes velocities at the given point.
        /// Specify an empty array or null for specific axes to make the sequence calculate the velocity.</param>
        /// <param name="time">The duration between the previous point in the sequence and this one.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task PointRelativeAsync(Measurement[] positions, Measurement?[] velocities, Measurement time)
        {
            var request = new Requests.PvtPointRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Type = Requests.StreamSegmentType.Rel,
                Positions = positions,
                Velocities = velocities,
                Time = time,
            };

            await Gateway.CallAsync("device/stream_point", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queues a point with coordinates relative to the previous point in the PVT sequence.
        /// If some or all velocities are not provided, the sequence calculates the velocities
        /// from surrounding points using finite difference.
        /// The last point of the sequence must have defined velocity (likely zero).
        /// </summary>
        /// <param name="positions">Positions for the axes to move through, relative to the previous point.</param>
        /// <param name="velocities">The axes velocities at the given point.
        /// Specify an empty array or null for specific axes to make the sequence calculate the velocity.</param>
        /// <param name="time">The duration between the previous point in the sequence and this one.</param>
        public void PointRelative(Measurement[] positions, Measurement?[] velocities, Measurement time)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = PointRelativeAsync(positions, velocities, time);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until the live PVT sequence executes all queued actions.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync(bool throwErrorOnFault = true)
        {
            var request = new Requests.StreamWaitUntilIdleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                ThrowErrorOnFault = throwErrorOnFault,
            };

            await Gateway.CallAsync("device/stream_wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until the live PVT sequence executes all queued actions.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        public void WaitUntilIdle(bool throwErrorOnFault = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync(throwErrorOnFault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cork the front of the PVT sequences's action queue, blocking execution.
        /// Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
        /// Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
        /// You can only cork an idle live PVT sequence.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CorkAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            await Gateway.CallAsync("device/stream_cork", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cork the front of the PVT sequences's action queue, blocking execution.
        /// Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
        /// Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
        /// You can only cork an idle live PVT sequence.
        /// </summary>
        public void Cork()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CorkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Uncork the front of the queue, unblocking command execution.
        /// You can only uncork an idle live PVT sequence that is corked.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UncorkAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            await Gateway.CallAsync("device/stream_uncork", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Uncork the front of the queue, unblocking command execution.
        /// You can only uncork an idle live PVT sequence that is corked.
        /// </summary>
        public void Uncork()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UncorkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the PVT sequence is executing a queued action.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            var response = await Gateway.CallAsync("device/stream_is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
        /// </summary>
        /// <returns>True if the PVT sequence is executing a queued action.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string which represents the PVT sequence.
        /// </summary>
        /// <returns>String which represents the PVT sequence.</returns>
        public override string ToString()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            var response = Gateway.CallSync("device/stream_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Disables the PVT sequence.
        /// If the PVT sequence is not setup, this command does nothing.
        /// Once disabled, the PVT sequence will no longer accept PVT commands.
        /// The PVT sequence will process the rest of the commands in the queue until it is empty.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            await Gateway.CallAsync("device/stream_disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables the PVT sequence.
        /// If the PVT sequence is not setup, this command does nothing.
        /// Once disabled, the PVT sequence will no longer accept PVT commands.
        /// The PVT sequence will process the rest of the commands in the queue until it is empty.
        /// </summary>
        public void Disable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to the PVT sequence.
        /// Keeps resending the command while the device rejects with AGAIN reason.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandAsync(string command)
        {
            var request = new Requests.StreamGenericCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Command = command,
            };

            await Gateway.CallAsync("device/stream_generic_command", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to the PVT sequence.
        /// Keeps resending the command while the device rejects with AGAIN reason.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        public void GenericCommand(string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a batch of generic ASCII commands to the PVT sequence.
        /// Keeps resending command while the device rejects with AGAIN reason.
        /// The batch is atomic in terms of thread safety.
        /// </summary>
        /// <param name="batch">Array of commands.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandBatchAsync(string[] batch)
        {
            var request = new Requests.StreamGenericCommandBatchRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Batch = batch,
            };

            await Gateway.CallAsync("device/stream_generic_command_batch", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a batch of generic ASCII commands to the PVT sequence.
        /// Keeps resending command while the device rejects with AGAIN reason.
        /// The batch is atomic in terms of thread safety.
        /// </summary>
        /// <param name="batch">Array of commands.</param>
        public void GenericCommandBatch(string[] batch)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandBatchAsync(batch);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queries the PVT sequence status from the device
        /// and returns boolean indicating whether the PVT sequence is disabled.
        /// Useful to determine if execution was interrupted by other movements.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the PVT sequence is disabled.</returns>
        public async Task<bool> CheckDisabledAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            var response = await Gateway.CallAsync("device/stream_check_disabled", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Queries the PVT sequence status from the device
        /// and returns boolean indicating whether the PVT sequence is disabled.
        /// Useful to determine if execution was interrupted by other movements.
        /// </summary>
        /// <returns>True if the PVT sequence is disabled.</returns>
        public bool CheckDisabled()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CheckDisabledAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Makes the PVT sequence throw PvtDiscontinuityException when it encounters discontinuities (ND warning flag).
        /// </summary>
        public void TreatDiscontinuitiesAsError()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            Gateway.CallSync("device/stream_treat_discontinuities", request);
        }


        /// <summary>
        /// Prevents PvtDiscontinuityException as a result of expected discontinuity when resuming the sequence.
        /// </summary>
        public void IgnoreCurrentDiscontinuity()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            Gateway.CallSync("device/stream_ignore_discontinuity", request);
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use PvtSequence.Io.SetDigitalOutput instead.")]
        public async Task SetDigitalOutputAsync(int channelNumber, DigitalOutputAction value)
        {
            var request = new Requests.StreamSetDigitalOutputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_digital_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        [Obsolete("Use PvtSequence.Io.SetDigitalOutput instead.")]
        public void SetDigitalOutput(int channelNumber, DigitalOutputAction value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use PvtSequence.Io.SetAllDigitalOutputs instead.")]
        public async Task SetAllDigitalOutputsAsync(DigitalOutputAction[] values)
        {
            var request = new Requests.StreamSetAllDigitalOutputsRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_digital_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        [Obsolete("Use PvtSequence.Io.SetAllDigitalOutputs instead.")]
        public void SetAllDigitalOutputs(DigitalOutputAction[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use PvtSequence.Io.SetAnalogOutput instead.")]
        public async Task SetAnalogOutputAsync(int channelNumber, double value)
        {
            var request = new Requests.StreamSetAnalogOutputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_analog_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        [Obsolete("Use PvtSequence.Io.SetAnalogOutput instead.")]
        public void SetAnalogOutput(int channelNumber, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use PvtSequence.Io.SetAllAnalogOutputs instead.")]
        public async Task SetAllAnalogOutputsAsync(double[] values)
        {
            var request = new Requests.StreamSetAllAnalogOutputsRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_analog_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        [Obsolete("Use PvtSequence.Io.SetAllAnalogOutputs instead.")]
        public void SetAllAnalogOutputs(double[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the axes of the PVT sequence.
        /// </summary>
        /// <returns>An array of axis numbers of the axes the PVT sequence is set up to control.</returns>
        private PvtAxisDefinition[] RetrieveAxes()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            var response = Gateway.CallSync("device/stream_get_axes", request, Requests.StreamGetAxesResponse.FromByteArray);
            return response.PvtAxes;
        }


        /// <summary>
        /// Get the mode of the PVT sequence.
        /// </summary>
        /// <returns>Mode of the PVT sequence.</returns>
        private PvtMode RetrieveMode()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = PvtId,
                Pvt = true,
            };

            var response = Gateway.CallSync("device/stream_get_mode", request, Requests.StreamModeResponse.FromByteArray);
            return response.PvtMode;
        }


    }
}
