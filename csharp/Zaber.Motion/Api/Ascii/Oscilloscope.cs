﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Provides a convenient way to control the oscilloscope data recording feature of some devices.
    /// The oscilloscope can record the values of some settings over time at high resolution.
    /// Requires at least Firmware 7.00.
    /// </summary>
    public class Oscilloscope
    {
        internal Oscilloscope(Device device)
        {
            Device = device;
        }


        /// <summary>
        /// Device that this Oscilloscope measures.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Select a setting to be recorded.
        /// </summary>
        /// <param name="axis">The 1-based index of the axis to record the value from.</param>
        /// <param name="setting">The name of a setting to record.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task AddChannelAsync(int axis, string setting)
        {
            var request = new Requests.OscilloscopeAddSettingChannelRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = axis,
                Setting = setting,
            };

            await Gateway.CallAsync("oscilloscope/add_setting_channel", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Select a setting to be recorded.
        /// </summary>
        /// <param name="axis">The 1-based index of the axis to record the value from.</param>
        /// <param name="setting">The name of a setting to record.</param>
        public void AddChannel(int axis, string setting)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = AddChannelAsync(axis, setting);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Select an I/O pin to be recorded.
        /// Requires at least Firmware 7.33.
        /// </summary>
        /// <param name="ioType">The I/O port type to read data from.</param>
        /// <param name="ioChannel">The 1-based index of the I/O pin to read from.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task AddIoChannelAsync(IoPortType ioType, int ioChannel)
        {
            var request = new Requests.OscilloscopeAddIoChannelRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                IoType = ioType,
                IoChannel = ioChannel,
            };

            await Gateway.CallAsync("oscilloscope/add_io_channel", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Select an I/O pin to be recorded.
        /// Requires at least Firmware 7.33.
        /// </summary>
        /// <param name="ioType">The I/O port type to read data from.</param>
        /// <param name="ioChannel">The 1-based index of the I/O pin to read from.</param>
        public void AddIoChannel(IoPortType ioType, int ioChannel)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = AddIoChannelAsync(ioType, ioChannel);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Clear the list of channels to record.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ClearAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            await Gateway.CallAsync("oscilloscope/clear_channels", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Clear the list of channels to record.
        /// </summary>
        public void Clear()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ClearAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the current sampling interval.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the timebase in.</param>
        /// <returns>A Task that can be awaited to get the result: The current sampling interval in the selected time units.</returns>
        public async Task<double> GetTimebaseAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.timebase",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the current sampling interval.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the timebase in.</param>
        /// <returns>The current sampling interval in the selected time units.</returns>
        public double GetTimebase(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetTimebaseAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set the sampling interval.
        /// </summary>
        /// <param name="interval">Sample interval for the next oscilloscope recording. Minimum value is 100µs.</param>
        /// <param name="unit">Unit of measure the timebase is represented in.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetTimebaseAsync(double interval, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.timebase",
                Value = interval,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set the sampling interval.
        /// </summary>
        /// <param name="interval">Sample interval for the next oscilloscope recording. Minimum value is 100µs.</param>
        /// <param name="unit">Unit of measure the timebase is represented in.</param>
        public void SetTimebase(double interval, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetTimebaseAsync(interval, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the current sampling frequency.
        /// The values is calculated as the inverse of the current sampling interval.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the frequency in.</param>
        /// <returns>A Task that can be awaited to get the result: The inverse of current sampling interval in the selected units.</returns>
        public async Task<double> GetFrequencyAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.timebase",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("oscilloscope/get_frequency", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the current sampling frequency.
        /// The values is calculated as the inverse of the current sampling interval.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the frequency in.</param>
        /// <returns>The inverse of current sampling interval in the selected units.</returns>
        public double GetFrequency(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetFrequencyAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set the sampling frequency (inverse of the sampling interval).
        /// The value is quantized to the next closest value supported by the firmware.
        /// </summary>
        /// <param name="frequency">Sample frequency for the next oscilloscope recording.</param>
        /// <param name="unit">Unit of measure the frequency is represented in.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetFrequencyAsync(double frequency, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.timebase",
                Value = frequency,
                Unit = unit,
            };

            await Gateway.CallAsync("oscilloscope/set_frequency", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set the sampling frequency (inverse of the sampling interval).
        /// The value is quantized to the next closest value supported by the firmware.
        /// </summary>
        /// <param name="frequency">Sample frequency for the next oscilloscope recording.</param>
        /// <param name="unit">Unit of measure the frequency is represented in.</param>
        public void SetFrequency(double frequency, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetFrequencyAsync(frequency, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the delay before oscilloscope recording starts.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the delay in.</param>
        /// <returns>A Task that can be awaited to get the result: The current start delay in the selected time units.</returns>
        public async Task<double> GetDelayAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.delay",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the delay before oscilloscope recording starts.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the delay in.</param>
        /// <returns>The current start delay in the selected time units.</returns>
        public double GetDelay(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetDelayAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set the sampling start delay.
        /// </summary>
        /// <param name="interval">Delay time between triggering a recording and the first data point being recorded.</param>
        /// <param name="unit">Unit of measure the delay is represented in.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetDelayAsync(double interval, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.delay",
                Value = interval,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set the sampling start delay.
        /// </summary>
        /// <param name="interval">Delay time between triggering a recording and the first data point being recorded.</param>
        /// <param name="unit">Unit of measure the delay is represented in.</param>
        public void SetDelay(double interval, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDelayAsync(interval, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the maximum number of channels that can be recorded.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The maximum number of channels that can be added to an Oscilloscope recording.</returns>
        public async Task<int> GetMaxChannelsAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.numchannels",
            };

            var response = await Gateway.CallAsync("oscilloscope/get_setting", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the maximum number of channels that can be recorded.
        /// </summary>
        /// <returns>The maximum number of channels that can be added to an Oscilloscope recording.</returns>
        public int GetMaxChannels()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetMaxChannelsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the maximum number of samples that can be recorded per Oscilloscope channel.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The maximum number of samples that can be recorded per Oscilloscope channel.</returns>
        public async Task<int> GetMaxBufferSizeAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.channel.size.max",
            };

            var response = await Gateway.CallAsync("oscilloscope/get_setting", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the maximum number of samples that can be recorded per Oscilloscope channel.
        /// </summary>
        /// <returns>The maximum number of samples that can be recorded per Oscilloscope channel.</returns>
        public int GetMaxBufferSize()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetMaxBufferSizeAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the number of samples that can be recorded per channel given the current number of channels added.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of samples that will be recorded per channel with the current channels. Zero if none have been added.</returns>
        public async Task<int> GetBufferSizeAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "scope.channel.size",
            };

            var response = await Gateway.CallAsync("oscilloscope/get_setting", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the number of samples that can be recorded per channel given the current number of channels added.
        /// </summary>
        /// <returns>Number of samples that will be recorded per channel with the current channels. Zero if none have been added.</returns>
        public int GetBufferSize()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetBufferSizeAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Trigger data recording.
        /// </summary>
        /// <param name="captureLength">Optional number of samples to record per channel.
        /// If left empty, the device records samples until the buffer fills.
        /// Requires at least Firmware 7.29.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StartAsync(int captureLength = 0)
        {
            var request = new Requests.OscilloscopeStartRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                CaptureLength = captureLength,
            };

            await Gateway.CallAsync("oscilloscope/start", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Trigger data recording.
        /// </summary>
        /// <param name="captureLength">Optional number of samples to record per channel.
        /// If left empty, the device records samples until the buffer fills.
        /// Requires at least Firmware 7.29.</param>
        public void Start(int captureLength = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StartAsync(captureLength);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// End data recording if currently in progress.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopAsync()
        {
            var request = new Requests.OscilloscopeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            await Gateway.CallAsync("oscilloscope/stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// End data recording if currently in progress.
        /// </summary>
        public void Stop()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Array of recorded channel data arrays, in the order added.</returns>
        public async Task<OscilloscopeData[]> ReadAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("oscilloscope/read", request, Requests.OscilloscopeReadResponse.FromByteArray).ConfigureAwait(false);
            return response.DataIds.Select(id => new OscilloscopeData(id)).ToArray();
        }


        /// <summary>
        /// Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
        /// </summary>
        /// <returns>Array of recorded channel data arrays, in the order added.</returns>
        public OscilloscopeData[] Read()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ReadAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
