﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// A handle for a stream with this number on the device.
    /// Streams provide a way to execute or store a sequence of actions.
    /// Stream methods append actions to a queue which executes or stores actions in a first in, first out order.
    /// </summary>
    public class Stream
    {
        internal Stream(Device device, int streamId)
        {
            Device = device;
            StreamId = streamId;
            Io = new StreamIo(Device, StreamId);
        }


        /// <summary>
        /// Device that controls this stream.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The number that identifies the stream on the device.
        /// </summary>
        public int StreamId { get; private set; }


        /// <summary>
        /// Current mode of the stream.
        /// </summary>
        public StreamMode Mode
        {
            get { return RetrieveMode(); }
        }


        /// <summary>
        /// An array of axes definitions the stream is set up to control.
        /// </summary>
        public StreamAxisDefinition[] Axes
        {
            get { return RetrieveAxes(); }
        }


        /// <summary>
        /// Gets an object that provides access to I/O for this stream.
        /// </summary>
        public StreamIo Io { get; private set; }


        /// <summary>
        /// Setup the stream to control the specified axes and to queue actions on the device.
        /// Allows use of lockstep axes in a stream.
        /// </summary>
        /// <param name="axes">Definition of the stream axes.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupLiveCompositeAsync(params StreamAxisDefinition[] axes)
        {
            var request = new Requests.StreamSetupLiveCompositeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_live_composite", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the stream to control the specified axes and to queue actions on the device.
        /// Allows use of lockstep axes in a stream.
        /// </summary>
        /// <param name="axes">Definition of the stream axes.</param>
        public void SetupLiveComposite(params StreamAxisDefinition[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupLiveCompositeAsync(axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the stream to control the specified axes and to queue actions on the device.
        /// </summary>
        /// <param name="axes">Numbers of physical axes to setup the stream on.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupLiveAsync(params int[] axes)
        {
            var request = new Requests.StreamSetupLiveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_live", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the stream to control the specified axes and to queue actions on the device.
        /// </summary>
        /// <param name="axes">Numbers of physical axes to setup the stream on.</param>
        public void SetupLive(params int[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupLiveAsync(axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the stream to control the specified axes and queue actions into a stream buffer.
        /// Allows use of lockstep axes in a stream.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axes">Definition of the stream axes.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupStoreCompositeAsync(StreamBuffer streamBuffer, params StreamAxisDefinition[] axes)
        {
            var request = new Requests.StreamSetupStoreCompositeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                StreamBuffer = streamBuffer.BufferId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_store_composite", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the stream to control the specified axes and queue actions into a stream buffer.
        /// Allows use of lockstep axes in a stream.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axes">Definition of the stream axes.</param>
        public void SetupStoreComposite(StreamBuffer streamBuffer, params StreamAxisDefinition[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupStoreCompositeAsync(streamBuffer, axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the stream to control the specified axes and queue actions into a stream buffer.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axes">Numbers of physical axes to setup the stream on.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupStoreAsync(StreamBuffer streamBuffer, params int[] axes)
        {
            var request = new Requests.StreamSetupStoreRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                StreamBuffer = streamBuffer.BufferId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/stream_setup_store", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the stream to control the specified axes and queue actions into a stream buffer.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axes">Numbers of physical axes to setup the stream on.</param>
        public void SetupStore(StreamBuffer streamBuffer, params int[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupStoreAsync(streamBuffer, axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
        /// Afterwards, you may call the resulting stream buffer on arbitrary axes.
        /// This mode does not allow for unit conversions.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axesCount">The number of axes in the stream.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetupStoreArbitraryAxesAsync(StreamBuffer streamBuffer, int axesCount)
        {
            var request = new Requests.StreamSetupStoreArbitraryAxesRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                StreamBuffer = streamBuffer.BufferId,
                AxesCount = axesCount,
            };

            await Gateway.CallAsync("device/stream_setup_store_arbitrary_axes", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
        /// Afterwards, you may call the resulting stream buffer on arbitrary axes.
        /// This mode does not allow for unit conversions.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to queue actions in.</param>
        /// <param name="axesCount">The number of axes in the stream.</param>
        public void SetupStoreArbitraryAxes(StreamBuffer streamBuffer, int axesCount)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupStoreArbitraryAxesAsync(streamBuffer, axesCount);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Append the actions in a stream buffer to the queue.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to call.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CallAsync(StreamBuffer streamBuffer)
        {
            var request = new Requests.StreamCallRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                StreamBuffer = streamBuffer.BufferId,
            };

            await Gateway.CallAsync("device/stream_call", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Append the actions in a stream buffer to the queue.
        /// </summary>
        /// <param name="streamBuffer">The stream buffer to call.</param>
        public void Call(StreamBuffer streamBuffer)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CallAsync(streamBuffer);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute line movement in the stream.
        /// </summary>
        /// <param name="endpoint">Positions for the axes to move to, relative to their home positions.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task LineAbsoluteAsync(params Measurement[] endpoint)
        {
            var request = new Requests.StreamLineRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_line", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute line movement in the stream.
        /// </summary>
        /// <param name="endpoint">Positions for the axes to move to, relative to their home positions.</param>
        public void LineAbsolute(params Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = LineAbsoluteAsync(endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative line movement in the stream.
        /// </summary>
        /// <param name="endpoint">Positions for the axes to move to, relative to their positions before movement.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task LineRelativeAsync(params Measurement[] endpoint)
        {
            var request = new Requests.StreamLineRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_line", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative line movement in the stream.
        /// </summary>
        /// <param name="endpoint">Positions for the axes to move to, relative to their positions before movement.</param>
        public void LineRelative(params Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = LineRelativeAsync(endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute line movement in the stream, targeting a subset of the stream axes.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="endpoint">Positions for the axes to move to, relative to their home positions.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task LineAbsoluteOnAsync(int[] targetAxesIndices, Measurement[] endpoint)
        {
            var request = new Requests.StreamLineRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                TargetAxesIndices = targetAxesIndices,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_line", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute line movement in the stream, targeting a subset of the stream axes.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="endpoint">Positions for the axes to move to, relative to their home positions.</param>
        public void LineAbsoluteOn(int[] targetAxesIndices, Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = LineAbsoluteOnAsync(targetAxesIndices, endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative line movement in the stream, targeting a subset of the stream axes.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="endpoint">Positions for the axes to move to, relative to their positions before movement.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task LineRelativeOnAsync(int[] targetAxesIndices, Measurement[] endpoint)
        {
            var request = new Requests.StreamLineRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                TargetAxesIndices = targetAxesIndices,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_line", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative line movement in the stream, targeting a subset of the stream axes.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="endpoint">Positions for the axes to move to, relative to their positions before movement.</param>
        public void LineRelativeOn(int[] targetAxesIndices, Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = LineRelativeOnAsync(targetAxesIndices, endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute arc movement on the first two axes of the stream.
        /// Absolute meaning that the home positions of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ArcAbsoluteAsync(RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
            };

            await Gateway.CallAsync("device/stream_arc", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute arc movement on the first two axes of the stream.
        /// Absolute meaning that the home positions of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        public void ArcAbsolute(RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ArcAbsoluteAsync(rotationDirection, centerX, centerY, endX, endY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative arc movement on the first two axes of the stream.
        /// Relative meaning that the current position of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ArcRelativeAsync(RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
            };

            await Gateway.CallAsync("device/stream_arc", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative arc movement on the first two axes of the stream.
        /// Relative meaning that the current position of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        public void ArcRelative(RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ArcRelativeAsync(rotationDirection, centerX, centerY, endX, endY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute arc movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ArcAbsoluteOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
            };

            await Gateway.CallAsync("device/stream_arc", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute arc movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        public void ArcAbsoluteOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ArcAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative arc movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ArcRelativeOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
            };

            await Gateway.CallAsync("device/stream_arc", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative arc movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the arc exists.</param>
        /// <param name="endX">The first dimension of the end position of the arc.</param>
        /// <param name="endY">The second dimension of the end position of the arc.</param>
        public void ArcRelativeOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ArcRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute helix movement in the stream.
        /// Requires at least Firmware 7.28.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.
        /// The first two axes refer to the helix's arc component,
        /// while the rest refers to the helix's line component.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="endX">The first dimension of the end position of the helix's arc component.</param>
        /// <param name="endY">The second dimension of the end position of the helix's arc component.</param>
        /// <param name="endpoint">Positions for the helix's line component axes, relative to their home positions.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HelixAbsoluteOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY, params Measurement[] endpoint)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_helix", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute helix movement in the stream.
        /// Requires at least Firmware 7.28.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.
        /// The first two axes refer to the helix's arc component,
        /// while the rest refers to the helix's line component.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="endX">The first dimension of the end position of the helix's arc component.</param>
        /// <param name="endY">The second dimension of the end position of the helix's arc component.</param>
        /// <param name="endpoint">Positions for the helix's line component axes, relative to their home positions.</param>
        public void HelixAbsoluteOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY, params Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HelixAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY, endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative helix movement in the stream.
        /// Requires at least Firmware 7.28.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.
        /// The first two axes refer to the helix's arc component,
        /// while the rest refers to the helix's line component.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="endX">The first dimension of the end position of the helix's arc component.</param>
        /// <param name="endY">The second dimension of the end position of the helix's arc component.</param>
        /// <param name="endpoint">Positions for the helix's line component axes, relative to their positions before movement.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HelixRelativeOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY, params Measurement[] endpoint)
        {
            var request = new Requests.StreamArcRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
                EndX = endX,
                EndY = endY,
                Endpoint = endpoint,
            };

            await Gateway.CallAsync("device/stream_helix", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative helix movement in the stream.
        /// Requires at least Firmware 7.28.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.
        /// The first two axes refer to the helix's arc component,
        /// while the rest refers to the helix's line component.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle on which the helix projects.</param>
        /// <param name="endX">The first dimension of the end position of the helix's arc component.</param>
        /// <param name="endY">The second dimension of the end position of the helix's arc component.</param>
        /// <param name="endpoint">Positions for the helix's line component axes, relative to their positions before movement.</param>
        public void HelixRelativeOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY, Measurement endX, Measurement endY, params Measurement[] endpoint)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HelixRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY, endpoint);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute circle movement on the first two axes of the stream.
        /// Absolute meaning that the home positions of the axes are treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CircleAbsoluteAsync(RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
            var request = new Requests.StreamCircleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
            };

            await Gateway.CallAsync("device/stream_circle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute circle movement on the first two axes of the stream.
        /// Absolute meaning that the home positions of the axes are treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        public void CircleAbsolute(RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CircleAbsoluteAsync(rotationDirection, centerX, centerY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative circle movement on the first two axes of the stream.
        /// Relative meaning that the current position of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CircleRelativeAsync(RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
            var request = new Requests.StreamCircleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
            };

            await Gateway.CallAsync("device/stream_circle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative circle movement on the first two axes of the stream.
        /// Relative meaning that the current position of the axes is treated as the origin.
        /// </summary>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        public void CircleRelative(RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CircleRelativeAsync(rotationDirection, centerX, centerY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue an absolute circle movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CircleAbsoluteOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
            var request = new Requests.StreamCircleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Abs,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
            };

            await Gateway.CallAsync("device/stream_circle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue an absolute circle movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        public void CircleAbsoluteOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CircleAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queue a relative circle movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CircleRelativeOnAsync(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
            var request = new Requests.StreamCircleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Type = Requests.StreamSegmentType.Rel,
                TargetAxesIndices = targetAxesIndices,
                RotationDirection = rotationDirection,
                CenterX = centerX,
                CenterY = centerY,
            };

            await Gateway.CallAsync("device/stream_circle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Queue a relative circle movement in the stream.
        /// The movement will only target the specified subset of axes in the stream.
        /// Requires at least Firmware 7.11.
        /// </summary>
        /// <param name="targetAxesIndices">Indices of the axes in the stream the movement targets.
        /// Refers to the axes provided during the stream setup or further execution.
        /// Indices are zero-based.</param>
        /// <param name="rotationDirection">The direction of the rotation.</param>
        /// <param name="centerX">The first dimension of the position of the center of the circle.</param>
        /// <param name="centerY">The second dimension of the position of the center of the circle.</param>
        public void CircleRelativeOn(int[] targetAxesIndices, RotationDirection rotationDirection, Measurement centerX, Measurement centerY)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CircleRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Wait a specified time.
        /// </summary>
        /// <param name="time">Amount of time to wait.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitAsync(double time, Units unit = Units.Native)
        {
            var request = new Requests.StreamWaitRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Time = time,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_wait", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Wait a specified time.
        /// </summary>
        /// <param name="time">Amount of time to wait.</param>
        /// <param name="unit">Units of time.</param>
        public void Wait(double time, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitAsync(time, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until the live stream executes all queued actions.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync(bool throwErrorOnFault = true)
        {
            var request = new Requests.StreamWaitUntilIdleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                ThrowErrorOnFault = throwErrorOnFault,
            };

            await Gateway.CallAsync("device/stream_wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until the live stream executes all queued actions.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        public void WaitUntilIdle(bool throwErrorOnFault = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync(throwErrorOnFault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cork the front of the stream's action queue, blocking execution.
        /// Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
        /// Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
        /// You can only cork an idle live stream.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CorkAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            await Gateway.CallAsync("device/stream_cork", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cork the front of the stream's action queue, blocking execution.
        /// Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
        /// Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
        /// You can only cork an idle live stream.
        /// </summary>
        public void Cork()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CorkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Uncork the front of the queue, unblocking command execution.
        /// You can only uncork an idle live stream that is corked.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UncorkAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            await Gateway.CallAsync("device/stream_uncork", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Uncork the front of the queue, unblocking command execution.
        /// You can only uncork an idle live stream that is corked.
        /// </summary>
        public void Uncork()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UncorkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Pauses or resumes execution of the stream in live mode.
        /// The hold only takes effect during execution of motion segments.
        /// </summary>
        /// <param name="hold">True to pause execution, false to resume.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetHoldAsync(bool hold)
        {
            var request = new Requests.StreamSetHoldRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Hold = hold,
            };

            await Gateway.CallAsync("device/stream_set_hold", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Pauses or resumes execution of the stream in live mode.
        /// The hold only takes effect during execution of motion segments.
        /// </summary>
        /// <param name="hold">True to pause execution, false to resume.</param>
        public void SetHold(bool hold)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetHoldAsync(hold);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a boolean value indicating whether the live stream is executing a queued action.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the stream is executing a queued action.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            var response = await Gateway.CallAsync("device/stream_is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a boolean value indicating whether the live stream is executing a queued action.
        /// </summary>
        /// <returns>True if the stream is executing a queued action.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the maximum speed of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of velocity.</param>
        /// <returns>A Task that can be awaited to get the result: The maximum speed of the stream.</returns>
        public async Task<double> GetMaxSpeedAsync(Units unit = Units.Native)
        {
            var request = new Requests.StreamGetMaxSpeedRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/stream_get_max_speed", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the maximum speed of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of velocity.</param>
        /// <returns>The maximum speed of the stream.</returns>
        public double GetMaxSpeed(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetMaxSpeedAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the maximum speed of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxSpeed">Maximum speed at which any stream action is executed.</param>
        /// <param name="unit">Units of velocity.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetMaxSpeedAsync(double maxSpeed, Units unit = Units.Native)
        {
            var request = new Requests.StreamSetMaxSpeedRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                MaxSpeed = maxSpeed,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_max_speed", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the maximum speed of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxSpeed">Maximum speed at which any stream action is executed.</param>
        /// <param name="unit">Units of velocity.</param>
        public void SetMaxSpeed(double maxSpeed, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetMaxSpeedAsync(maxSpeed, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the maximum tangential acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to get the result: The maximum tangential acceleration of the live stream.</returns>
        public async Task<double> GetMaxTangentialAccelerationAsync(Units unit = Units.Native)
        {
            var request = new Requests.StreamGetMaxTangentialAccelerationRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/stream_get_max_tangential_acceleration", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the maximum tangential acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>The maximum tangential acceleration of the live stream.</returns>
        public double GetMaxTangentialAcceleration(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetMaxTangentialAccelerationAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the maximum tangential acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxTangentialAcceleration">Maximum tangential acceleration at which any stream action is executed.</param>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetMaxTangentialAccelerationAsync(double maxTangentialAcceleration, Units unit = Units.Native)
        {
            var request = new Requests.StreamSetMaxTangentialAccelerationRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                MaxTangentialAcceleration = maxTangentialAcceleration,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_max_tangential_acceleration", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the maximum tangential acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxTangentialAcceleration">Maximum tangential acceleration at which any stream action is executed.</param>
        /// <param name="unit">Units of acceleration.</param>
        public void SetMaxTangentialAcceleration(double maxTangentialAcceleration, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetMaxTangentialAccelerationAsync(maxTangentialAcceleration, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the maximum centripetal acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to get the result: The maximum centripetal acceleration of the live stream.</returns>
        public async Task<double> GetMaxCentripetalAccelerationAsync(Units unit = Units.Native)
        {
            var request = new Requests.StreamGetMaxCentripetalAccelerationRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/stream_get_max_centripetal_acceleration", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the maximum centripetal acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>The maximum centripetal acceleration of the live stream.</returns>
        public double GetMaxCentripetalAcceleration(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetMaxCentripetalAccelerationAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the maximum centripetal acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxCentripetalAcceleration">Maximum centripetal acceleration at which any stream action is executed.</param>
        /// <param name="unit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetMaxCentripetalAccelerationAsync(double maxCentripetalAcceleration, Units unit = Units.Native)
        {
            var request = new Requests.StreamSetMaxCentripetalAccelerationRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                MaxCentripetalAcceleration = maxCentripetalAcceleration,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_max_centripetal_acceleration", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the maximum centripetal acceleration of the live stream.
        /// Converts the units using the first axis of the stream.
        /// </summary>
        /// <param name="maxCentripetalAcceleration">Maximum centripetal acceleration at which any stream action is executed.</param>
        /// <param name="unit">Units of acceleration.</param>
        public void SetMaxCentripetalAcceleration(double maxCentripetalAcceleration, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetMaxCentripetalAccelerationAsync(maxCentripetalAcceleration, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string which represents the stream.
        /// </summary>
        /// <returns>String which represents the stream.</returns>
        public override string ToString()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            var response = Gateway.CallSync("device/stream_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Disables the stream.
        /// If the stream is not setup, this command does nothing.
        /// Once disabled, the stream will no longer accept stream commands.
        /// The stream will process the rest of the commands in the queue until it is empty.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            await Gateway.CallAsync("device/stream_disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables the stream.
        /// If the stream is not setup, this command does nothing.
        /// Once disabled, the stream will no longer accept stream commands.
        /// The stream will process the rest of the commands in the queue until it is empty.
        /// </summary>
        public void Disable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to the stream.
        /// Keeps resending the command while the device rejects with AGAIN reason.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandAsync(string command)
        {
            var request = new Requests.StreamGenericCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Command = command,
            };

            await Gateway.CallAsync("device/stream_generic_command", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to the stream.
        /// Keeps resending the command while the device rejects with AGAIN reason.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        public void GenericCommand(string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a batch of generic ASCII commands to the stream.
        /// Keeps resending command while the device rejects with AGAIN reason.
        /// The batch is atomic in terms of thread safety.
        /// </summary>
        /// <param name="batch">Array of commands.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandBatchAsync(string[] batch)
        {
            var request = new Requests.StreamGenericCommandBatchRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Batch = batch,
            };

            await Gateway.CallAsync("device/stream_generic_command_batch", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a batch of generic ASCII commands to the stream.
        /// Keeps resending command while the device rejects with AGAIN reason.
        /// The batch is atomic in terms of thread safety.
        /// </summary>
        /// <param name="batch">Array of commands.</param>
        public void GenericCommandBatch(string[] batch)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandBatchAsync(batch);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queries the stream status from the device
        /// and returns boolean indicating whether the stream is disabled.
        /// Useful to determine if streaming was interrupted by other movements.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the stream is disabled.</returns>
        public async Task<bool> CheckDisabledAsync()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            var response = await Gateway.CallAsync("device/stream_check_disabled", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Queries the stream status from the device
        /// and returns boolean indicating whether the stream is disabled.
        /// Useful to determine if streaming was interrupted by other movements.
        /// </summary>
        /// <returns>True if the stream is disabled.</returns>
        public bool CheckDisabled()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CheckDisabledAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Makes the stream throw StreamDiscontinuityException when it encounters discontinuities (ND warning flag).
        /// </summary>
        public void TreatDiscontinuitiesAsError()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            Gateway.CallSync("device/stream_treat_discontinuities", request);
        }


        /// <summary>
        /// Prevents StreamDiscontinuityException as a result of expected discontinuity when resuming streaming.
        /// </summary>
        public void IgnoreCurrentDiscontinuity()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            Gateway.CallSync("device/stream_ignore_discontinuity", request);
        }


        /// <summary>
        /// Wait for a digital input channel to reach a given value.
        /// </summary>
        /// <param name="channelNumber">The number of the digital input channel.
        /// Channel numbers are numbered from one.</param>
        /// <param name="value">The value that the stream should wait for.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.WaitDigitalInput instead.")]
        public async Task WaitDigitalInputAsync(int channelNumber, bool value)
        {
            var request = new Requests.StreamWaitDigitalInputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_wait_digital_input", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Wait for a digital input channel to reach a given value.
        /// </summary>
        /// <param name="channelNumber">The number of the digital input channel.
        /// Channel numbers are numbered from one.</param>
        /// <param name="value">The value that the stream should wait for.</param>
        [Obsolete("Use Stream.Io.WaitDigitalInput instead.")]
        public void WaitDigitalInput(int channelNumber, bool value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitDigitalInputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Wait for the value of a analog input channel to reach a condition concerning a given value.
        /// </summary>
        /// <param name="channelNumber">The number of the analog input channel.
        /// Channel numbers are numbered from one.</param>
        /// <param name="condition">A condition (e.g. &lt;, &lt;=, ==, !=).</param>
        /// <param name="value">The value that the condition concerns, in Volts.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.WaitAnalogInput instead.")]
        public async Task WaitAnalogInputAsync(int channelNumber, string condition, double value)
        {
            var request = new Requests.StreamWaitAnalogInputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                ChannelNumber = channelNumber,
                Condition = condition,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_wait_analog_input", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Wait for the value of a analog input channel to reach a condition concerning a given value.
        /// </summary>
        /// <param name="channelNumber">The number of the analog input channel.
        /// Channel numbers are numbered from one.</param>
        /// <param name="condition">A condition (e.g. &lt;, &lt;=, ==, !=).</param>
        /// <param name="value">The value that the condition concerns, in Volts.</param>
        [Obsolete("Use Stream.Io.WaitAnalogInput instead.")]
        public void WaitAnalogInput(int channelNumber, string condition, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitAnalogInputAsync(channelNumber, condition, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.SetDigitalOutput instead.")]
        public async Task SetDigitalOutputAsync(int channelNumber, DigitalOutputAction value)
        {
            var request = new Requests.StreamSetDigitalOutputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_digital_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        [Obsolete("Use Stream.Io.SetDigitalOutput instead.")]
        public void SetDigitalOutput(int channelNumber, DigitalOutputAction value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.SetAllDigitalOutputs instead.")]
        public async Task SetAllDigitalOutputsAsync(DigitalOutputAction[] values)
        {
            var request = new Requests.StreamSetAllDigitalOutputsRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_digital_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        [Obsolete("Use Stream.Io.SetAllDigitalOutputs instead.")]
        public void SetAllDigitalOutputs(DigitalOutputAction[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.setAnalogOutput instead.")]
        public async Task SetAnalogOutputAsync(int channelNumber, double value)
        {
            var request = new Requests.StreamSetAnalogOutputRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_analog_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        [Obsolete("Use Stream.Io.setAnalogOutput instead.")]
        public void SetAnalogOutput(int channelNumber, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        [Obsolete("Use Stream.Io.setAllAnalogOutputs instead.")]
        public async Task SetAllAnalogOutputsAsync(double[] values)
        {
            var request = new Requests.StreamSetAllAnalogOutputsRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_analog_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        [Obsolete("Use Stream.Io.setAllAnalogOutputs instead.")]
        public void SetAllAnalogOutputs(double[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the axes of the stream.
        /// </summary>
        /// <returns>An array of axis numbers of the axes the stream is set up to control.</returns>
        private StreamAxisDefinition[] RetrieveAxes()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            var response = Gateway.CallSync("device/stream_get_axes", request, Requests.StreamGetAxesResponse.FromByteArray);
            return response.Axes;
        }


        /// <summary>
        /// Get the mode of the stream.
        /// </summary>
        /// <returns>Mode of the stream.</returns>
        private StreamMode RetrieveMode()
        {
            var request = new Requests.StreamEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                StreamId = StreamId,
            };

            var response = Gateway.CallSync("device/stream_get_mode", request, Requests.StreamModeResponse.FromByteArray);
            return response.StreamMode;
        }


    }
}
