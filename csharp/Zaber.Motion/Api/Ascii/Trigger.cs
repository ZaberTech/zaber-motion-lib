﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// A handle for a trigger with this number on the device.
    /// Triggers allow setting up actions that occur when a certain condition has been met or an event has occurred.
    /// Please note that the Triggers API is currently an experimental feature.
    /// Requires at least Firmware 7.06.
    /// </summary>
    public class Trigger
    {
        internal Trigger(Device device, int triggerNumber)
        {
            Device = device;
            TriggerNumber = triggerNumber;
        }


        /// <summary>
        /// Device that this trigger belongs to.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Number of this trigger.
        /// </summary>
        public int TriggerNumber { get; private set; }


        /// <summary>
        /// Enables the trigger.
        /// Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
        /// If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
        /// </summary>
        /// <param name="count">Number of times the trigger will fire before disabling itself.
        /// If count is not specified, or 0, the trigger will fire indefinitely.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EnableAsync(int count = 0)
        {
            if (count < 0)
            {
                throw new ArgumentException("Invalid value; count must be 0 or positive.");
            }

            var request = new Requests.TriggerEnableRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Count = count,
            };

            await Gateway.CallAsync("trigger/enable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Enables the trigger.
        /// Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
        /// If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
        /// </summary>
        /// <param name="count">Number of times the trigger will fire before disabling itself.
        /// If count is not specified, or 0, the trigger will fire indefinitely.</param>
        public void Enable(int count = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EnableAsync(count);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disables the trigger.
        /// Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableAsync()
        {
            var request = new Requests.TriggerEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
            };

            await Gateway.CallAsync("trigger/disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables the trigger.
        /// Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
        /// </summary>
        public void Disable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the state of the trigger.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Complete state of the trigger.</returns>
        public async Task<TriggerState> GetStateAsync()
        {
            var request = new Requests.TriggerEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
            };

            var response = await Gateway.CallAsync("trigger/get_state", request, TriggerState.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the state of the trigger.
        /// </summary>
        /// <returns>Complete state of the trigger.</returns>
        public TriggerState GetState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the enabled state of the trigger.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Whether the trigger is enabled and the number of times it will fire.</returns>
        public async Task<TriggerEnabledState> GetEnabledStateAsync()
        {
            var request = new Requests.TriggerEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
            };

            var response = await Gateway.CallAsync("trigger/get_enabled_state", request, TriggerEnabledState.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the enabled state of the trigger.
        /// </summary>
        /// <returns>Whether the trigger is enabled and the number of times it will fire.</returns>
        public TriggerEnabledState GetEnabledState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetEnabledStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a generic trigger condition.
        /// </summary>
        /// <param name="condition">The condition to set for this trigger.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenAsync(string condition)
        {
            var request = new Requests.TriggerFireWhenRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Condition = condition,
            };

            await Gateway.CallAsync("trigger/fire_when", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a generic trigger condition.
        /// </summary>
        /// <param name="condition">The condition to set for this trigger.</param>
        public void FireWhen(string condition)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenAsync(condition);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition for when an encoder position has changed by a specific distance.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// May be set to 0 on single-axis devices only.</param>
        /// <param name="distance">The measured encoder distance between trigger fires.</param>
        /// <param name="unit">Units of dist.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenEncoderDistanceTravelledAsync(int axis, double distance, Units unit = Units.Native)
        {
            if (distance <= 0)
            {
                throw new ArgumentException("Invalid value; encoder distance must be a positive value.");
            }

            var request = new Requests.TriggerFireWhenDistanceTravelledRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Axis = axis,
                Distance = distance,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/fire_when_encoder_distance_travelled", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition for when an encoder position has changed by a specific distance.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// May be set to 0 on single-axis devices only.</param>
        /// <param name="distance">The measured encoder distance between trigger fires.</param>
        /// <param name="unit">Units of dist.</param>
        public void FireWhenEncoderDistanceTravelled(int axis, double distance, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenEncoderDistanceTravelledAsync(axis, distance, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition for when an axis position has changed by a specific distance.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// May be set to 0 on single-axis devices only.</param>
        /// <param name="distance">The measured distance between trigger fires.</param>
        /// <param name="unit">Units of dist.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenDistanceTravelledAsync(int axis, double distance, Units unit = Units.Native)
        {
            if (distance <= 0)
            {
                throw new ArgumentException("Invalid value; distance must be a positive value.");
            }

            var request = new Requests.TriggerFireWhenDistanceTravelledRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Axis = axis,
                Distance = distance,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/fire_when_distance_travelled", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition for when an axis position has changed by a specific distance.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// May be set to 0 on single-axis devices only.</param>
        /// <param name="distance">The measured distance between trigger fires.</param>
        /// <param name="unit">Units of dist.</param>
        public void FireWhenDistanceTravelled(int axis, double distance, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenDistanceTravelledAsync(axis, distance, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition based on an IO channel value.
        /// </summary>
        /// <param name="portType">The type of IO channel to monitor.</param>
        /// <param name="channel">The IO channel to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenIoAsync(IoPortType portType, int channel, TriggerCondition triggerCondition, double value)
        {
            if (channel <= 0)
            {
                throw new ArgumentException("Invalid value; channel must be a positive value.");
            }

            var request = new Requests.TriggerFireWhenIoRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                PortType = portType,
                Channel = channel,
                TriggerCondition = triggerCondition,
                Value = value,
            };

            await Gateway.CallAsync("trigger/fire_when_io", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition based on an IO channel value.
        /// </summary>
        /// <param name="portType">The type of IO channel to monitor.</param>
        /// <param name="channel">The IO channel to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        public void FireWhenIo(IoPortType portType, int channel, TriggerCondition triggerCondition, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenIoAsync(portType, channel, triggerCondition, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition based on a setting value.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// Set to 0 for device-scope settings.</param>
        /// <param name="setting">The setting to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        /// <param name="unit">Units of value.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenSettingAsync(int axis, string setting, TriggerCondition triggerCondition, double value, Units unit = Units.Native)
        {
            var request = new Requests.TriggerFireWhenSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Axis = axis,
                Setting = setting,
                TriggerCondition = triggerCondition,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/fire_when_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition based on a setting value.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// Set to 0 for device-scope settings.</param>
        /// <param name="setting">The setting to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        /// <param name="unit">Units of value.</param>
        public void FireWhenSetting(int axis, string setting, TriggerCondition triggerCondition, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenSettingAsync(axis, setting, triggerCondition, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition based on an absolute setting value.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// Set to 0 for device-scope settings.</param>
        /// <param name="setting">The setting to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        /// <param name="unit">Units of value.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireWhenAbsoluteSettingAsync(int axis, string setting, TriggerCondition triggerCondition, double value, Units unit = Units.Native)
        {
            var request = new Requests.TriggerFireWhenSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Axis = axis,
                Setting = setting,
                TriggerCondition = triggerCondition,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/fire_when_setting_absolute", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition based on an absolute setting value.
        /// </summary>
        /// <param name="axis">The axis to monitor for this condition.
        /// Set to 0 for device-scope settings.</param>
        /// <param name="setting">The setting to monitor.</param>
        /// <param name="triggerCondition">Comparison operator.</param>
        /// <param name="value">Comparison value.</param>
        /// <param name="unit">Units of value.</param>
        public void FireWhenAbsoluteSetting(int axis, string setting, TriggerCondition triggerCondition, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireWhenAbsoluteSettingAsync(axis, setting, triggerCondition, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger condition based on a time interval.
        /// </summary>
        /// <param name="interval">The time interval between trigger fires.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FireAtIntervalAsync(double interval, Units unit = Units.Native)
        {
            if (interval <= 0)
            {
                throw new ArgumentException("Invalid value; interval must be a positive value.");
            }

            var request = new Requests.TriggerFireAtIntervalRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Interval = interval,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/fire_at_interval", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger condition based on a time interval.
        /// </summary>
        /// <param name="interval">The time interval between trigger fires.</param>
        /// <param name="unit">Units of time.</param>
        public void FireAtInterval(double interval, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FireAtIntervalAsync(interval, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a command to be a trigger action.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis to on which to run this command.
        /// Set to 0 for device-scope settings or to run command on all axes.</param>
        /// <param name="command">The command to run when the action is triggered.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OnFireAsync(TriggerAction action, int axis, string command)
        {
            var request = new Requests.TriggerOnFireRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Action = action,
                Axis = axis,
                Command = command,
            };

            await Gateway.CallAsync("trigger/on_fire", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a command to be a trigger action.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis to on which to run this command.
        /// Set to 0 for device-scope settings or to run command on all axes.</param>
        /// <param name="command">The command to run when the action is triggered.</param>
        public void OnFire(TriggerAction action, int axis, string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OnFireAsync(action, axis, command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger action to update a setting.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis on which to change the setting.
        /// Set to 0 to change the setting for the device.</param>
        /// <param name="setting">The name of the setting to change.</param>
        /// <param name="operation">The operation to apply to the setting.</param>
        /// <param name="value">Operation value.</param>
        /// <param name="unit">Units of value.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OnFireSetAsync(TriggerAction action, int axis, string setting, TriggerOperation operation, double value, Units unit = Units.Native)
        {
            var request = new Requests.TriggerOnFireSetRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Action = action,
                Axis = axis,
                Setting = setting,
                Operation = operation,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("trigger/on_fire_set", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger action to update a setting.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis on which to change the setting.
        /// Set to 0 to change the setting for the device.</param>
        /// <param name="setting">The name of the setting to change.</param>
        /// <param name="operation">The operation to apply to the setting.</param>
        /// <param name="value">Operation value.</param>
        /// <param name="unit">Units of value.</param>
        public void OnFireSet(TriggerAction action, int axis, string setting, TriggerOperation operation, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OnFireSetAsync(action, axis, setting, operation, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Set a trigger action to update a setting with the value of another setting.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis on which to change the setting.
        /// Set to 0 to change the setting for the device.</param>
        /// <param name="setting">The name of the setting to change.
        /// Must have either integer or boolean type.</param>
        /// <param name="operation">The operation to apply to the setting.</param>
        /// <param name="fromAxis">The axis from which to read the setting.
        /// Set to 0 to read the setting from the device.</param>
        /// <param name="fromSetting">The name of the setting to read.
        /// Must have either integer or boolean type.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OnFireSetToSettingAsync(TriggerAction action, int axis, string setting, TriggerOperation operation, int fromAxis, string fromSetting)
        {
            var request = new Requests.TriggerOnFireSetToSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Action = action,
                Axis = axis,
                Setting = setting,
                Operation = operation,
                FromAxis = fromAxis,
                FromSetting = fromSetting,
            };

            await Gateway.CallAsync("trigger/on_fire_set_to_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Set a trigger action to update a setting with the value of another setting.
        /// </summary>
        /// <param name="action">The action number to assign the command to.</param>
        /// <param name="axis">The axis on which to change the setting.
        /// Set to 0 to change the setting for the device.</param>
        /// <param name="setting">The name of the setting to change.
        /// Must have either integer or boolean type.</param>
        /// <param name="operation">The operation to apply to the setting.</param>
        /// <param name="fromAxis">The axis from which to read the setting.
        /// Set to 0 to read the setting from the device.</param>
        /// <param name="fromSetting">The name of the setting to read.
        /// Must have either integer or boolean type.</param>
        public void OnFireSetToSetting(TriggerAction action, int axis, string setting, TriggerOperation operation, int fromAxis, string fromSetting)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OnFireSetToSettingAsync(action, axis, setting, operation, fromAxis, fromSetting);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Clear a trigger action.
        /// </summary>
        /// <param name="action">The action number to clear.
        /// The default option is to clear all actions.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ClearActionAsync(TriggerAction action = TriggerAction.All)
        {
            var request = new Requests.TriggerClearActionRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Action = action,
            };

            await Gateway.CallAsync("trigger/clear_action", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Clear a trigger action.
        /// </summary>
        /// <param name="action">The action number to clear.
        /// The default option is to clear all actions.</param>
        public void ClearAction(TriggerAction action = TriggerAction.All)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ClearActionAsync(action);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the label for the trigger.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The label for the trigger.</returns>
        public async Task<string> GetLabelAsync()
        {
            var request = new Requests.TriggerEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
            };

            var response = await Gateway.CallAsync("trigger/get_label", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the label for the trigger.
        /// </summary>
        /// <returns>The label for the trigger.</returns>
        public string GetLabel()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetLabelAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the label for the trigger.
        /// </summary>
        /// <param name="label">The label to set for this trigger.
        /// If no value or an empty string is provided, this label is deleted.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLabelAsync(string? label)
        {
            var request = new Requests.TriggerSetLabelRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                TriggerNumber = TriggerNumber,
                Label = label,
            };

            await Gateway.CallAsync("trigger/set_label", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the label for the trigger.
        /// </summary>
        /// <param name="label">The label to set for this trigger.
        /// If no value or an empty string is provided, this label is deleted.</param>
        public void SetLabel(string? label)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLabelAsync(label);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
