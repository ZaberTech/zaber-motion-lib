/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Denotes type of an axis and units it accepts.
    /// </summary>
    public enum AxisType
    {
        /// <summary>Unknown.</summary>
        Unknown = 0,

        /// <summary>Linear.</summary>
        Linear = 1,

        /// <summary>Rotary.</summary>
        Rotary = 2,

        /// <summary>Process.</summary>
        Process = 3,

        /// <summary>Lamp.</summary>
        Lamp = 4,

    }
}
