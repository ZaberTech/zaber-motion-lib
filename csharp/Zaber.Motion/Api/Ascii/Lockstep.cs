﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //
#pragma warning disable 612, 618

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents a lockstep group with this ID on a device.
    /// A lockstep group is a movement synchronized pair of axes on a device.
    /// Requires at least Firmware 6.15 or 7.11.
    /// </summary>
    public class Lockstep
    {
        internal Lockstep(Device device, int lockstepGroupId)
        {
            Device = device;
            LockstepGroupId = lockstepGroupId;
        }


        /// <summary>
        /// Device that controls this lockstep group.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The number that identifies the lockstep group on the device.
        /// </summary>
        public int LockstepGroupId { get; private set; }


        /// <summary>
        /// Activate the lockstep group on the axes specified.
        /// </summary>
        /// <param name="axes">The numbers of axes in the lockstep group.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EnableAsync(params int[] axes)
        {
            var request = new Requests.LockstepEnableRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Axes = axes,
            };

            await Gateway.CallAsync("device/lockstep_enable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Activate the lockstep group on the axes specified.
        /// </summary>
        /// <param name="axes">The numbers of axes in the lockstep group.</param>
        public void Enable(params int[] axes)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EnableAsync(axes);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disable the lockstep group.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableAsync()
        {
            var request = new Requests.LockstepDisableRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            await Gateway.CallAsync("device/lockstep_disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disable the lockstep group.
        /// </summary>
        public void Disable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops ongoing lockstep group movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.LockstepStopRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/lockstep_stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops ongoing lockstep group movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Stop(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HomeAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.LockstepHomeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/lockstep_home", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Home(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move the first axis of the lockstep group to an absolute position.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveAbsoluteAsync(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.LockstepMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Type = Requests.AxisMoveType.Abs,
                Arg = position,
                Unit = unit,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/lockstep_move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Move the first axis of the lockstep group to an absolute position.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveAbsolute(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move the first axis of the lockstep group to a position relative to its current position.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveRelativeAsync(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.LockstepMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Type = Requests.AxisMoveType.Rel,
                Arg = position,
                Unit = unit,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/lockstep_move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Move the first axis of the lockstep group to a position relative to its current position.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveRelative(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the first axis of the lockstep group at the specified speed.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveVelocityAsync(double velocity, Units unit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.LockstepMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Type = Requests.AxisMoveType.Vel,
                Arg = velocity,
                Unit = unit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/lockstep_move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the first axis of the lockstep group at the specified speed.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveVelocity(double velocity, Units unit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveVelocityAsync(velocity, unit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the first axis of the lockstep group in a sinusoidal trajectory.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="amplitude">Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).</param>
        /// <param name="amplitudeUnits">Units of position.</param>
        /// <param name="period">Period of the sinusoidal motion in milliseconds.</param>
        /// <param name="periodUnits">Units of time.</param>
        /// <param name="count">Number of sinusoidal cycles to complete.
        /// Must be a multiple of 0.5
        /// If count is not specified or set to 0, the axis will move indefinitely.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveSinAsync(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count = 0, bool waitUntilIdle = true)
        {
            var request = new Requests.LockstepMoveSinRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Amplitude = amplitude,
                AmplitudeUnits = amplitudeUnits,
                Period = period,
                PeriodUnits = periodUnits,
                Count = count,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/lockstep_move_sin", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the first axis of the lockstep group in a sinusoidal trajectory.
        /// The other axes in the lockstep group maintain their offsets throughout movement.
        /// </summary>
        /// <param name="amplitude">Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).</param>
        /// <param name="amplitudeUnits">Units of position.</param>
        /// <param name="period">Period of the sinusoidal motion in milliseconds.</param>
        /// <param name="periodUnits">Units of time.</param>
        /// <param name="count">Number of sinusoidal cycles to complete.
        /// Must be a multiple of 0.5
        /// If count is not specified or set to 0, the axis will move indefinitely.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void MoveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count = 0, bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
        /// If the sinusoidal motion was started with an integer-plus-half cycle count,
        /// the motion ends at the half-way point of the sinusoidal trajectory.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveSinStopAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.LockstepStopRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/lockstep_move_sin_stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
        /// If the sinusoidal motion was started with an integer-plus-half cycle count,
        /// the motion ends at the half-way point of the sinusoidal trajectory.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished.</param>
        public void MoveSinStop(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveSinStopAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axes to the maximum valid position.
        /// The axes in the lockstep group maintain their offsets throughout movement.
        /// Respects lim.max for all axes in the group.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMaxAsync(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.LockstepMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Type = Requests.AxisMoveType.Max,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/lockstep_move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axes to the maximum valid position.
        /// The axes in the lockstep group maintain their offsets throughout movement.
        /// Respects lim.max for all axes in the group.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveMax(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axes to the minimum valid position.
        /// The axes in the lockstep group maintain their offsets throughout movement.
        /// Respects lim.min for all axes in the group.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMinAsync(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.LockstepMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Type = Requests.AxisMoveType.Min,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/lockstep_move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axes to the minimum valid position.
        /// The axes in the lockstep group maintain their offsets throughout movement.
        /// Respects lim.min for all axes in the group.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveMin(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until the lockstep group stops moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync(bool throwErrorOnFault = true)
        {
            var request = new Requests.LockstepWaitUntilIdleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                ThrowErrorOnFault = throwErrorOnFault,
            };

            await Gateway.CallAsync("device/lockstep_wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until the lockstep group stops moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        public void WaitUntilIdle(bool throwErrorOnFault = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync(throwErrorOnFault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether the lockstep group is executing a motion command.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the axes are currently executing a motion command.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.LockstepEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            var response = await Gateway.CallAsync("device/lockstep_is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether the lockstep group is executing a motion command.
        /// </summary>
        /// <returns>True if the axes are currently executing a motion command.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the axes of the lockstep group.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: LockstepAxes instance which contains the axes numbers of the lockstep group.</returns>
        [Obsolete("Use GetAxisNumbers instead.")]
        public async Task<LockstepAxes> GetAxesAsync()
        {
            var request = new Requests.LockstepEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            var response = await Gateway.CallAsync("device/lockstep_get_axes", request, LockstepAxes.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Gets the axes of the lockstep group.
        /// </summary>
        /// <returns>LockstepAxes instance which contains the axes numbers of the lockstep group.</returns>
        [Obsolete("Use GetAxisNumbers instead.")]
        public LockstepAxes GetAxes()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAxesAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the axis numbers of the lockstep group.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Axis numbers in order specified when enabling lockstep.</returns>
        public async Task<int[]> GetAxisNumbersAsync()
        {
            var request = new Requests.LockstepEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            var response = await Gateway.CallAsync("device/lockstep_get_axis_numbers", request, Requests.LockstepGetAxisNumbersResponse.FromByteArray).ConfigureAwait(false);
            return response.Axes;
        }


        /// <summary>
        /// Gets the axis numbers of the lockstep group.
        /// </summary>
        /// <returns>Axis numbers in order specified when enabling lockstep.</returns>
        public int[] GetAxisNumbers()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAxisNumbersAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the initial offsets of secondary axes of an enabled lockstep group.
        /// </summary>
        /// <param name="unit">Units of position.
        /// Uses primary axis unit conversion.</param>
        /// <returns>A Task that can be awaited to get the result: Initial offset for each axis of the lockstep group.</returns>
        public async Task<double[]> GetOffsetsAsync(Units unit = Units.Native)
        {
            var request = new Requests.LockstepGetRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/lockstep_get_offsets", request, Requests.DoubleArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Gets the initial offsets of secondary axes of an enabled lockstep group.
        /// </summary>
        /// <param name="unit">Units of position.
        /// Uses primary axis unit conversion.</param>
        /// <returns>Initial offset for each axis of the lockstep group.</returns>
        public double[] GetOffsets(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetOffsetsAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the twists of secondary axes of an enabled lockstep group.
        /// </summary>
        /// <param name="unit">Units of position.
        /// Uses primary axis unit conversion.</param>
        /// <returns>A Task that can be awaited to get the result: Difference between the initial offset and the actual offset for each axis of the lockstep group.</returns>
        public async Task<double[]> GetTwistsAsync(Units unit = Units.Native)
        {
            var request = new Requests.LockstepGetRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/lockstep_get_twists", request, Requests.DoubleArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Gets the twists of secondary axes of an enabled lockstep group.
        /// </summary>
        /// <param name="unit">Units of position.
        /// Uses primary axis unit conversion.</param>
        /// <returns>Difference between the initial offset and the actual offset for each axis of the lockstep group.</returns>
        public double[] GetTwists(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetTwistsAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current position of the primary axis.
        /// </summary>
        /// <param name="unit">Units of the position.</param>
        /// <returns>A Task that can be awaited to get the result: Primary axis position.</returns>
        public async Task<double> GetPositionAsync(Units unit = Units.Native)
        {
            var request = new Requests.LockstepGetRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/lockstep_get_pos", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns current position of the primary axis.
        /// </summary>
        /// <param name="unit">Units of the position.</param>
        /// <returns>Primary axis position.</returns>
        public double GetPosition(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetPositionAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets lockstep twist tolerance.
        /// Twist tolerances that do not match the system configuration can reduce performance or damage the system.
        /// </summary>
        /// <param name="tolerance">Twist tolerance.</param>
        /// <param name="unit">Units of the tolerance.
        /// Uses primary axis unit conversion when setting to all axes,
        /// otherwise uses specified secondary axis unit conversion.</param>
        /// <param name="axisIndex">Optional index of a secondary axis to set the tolerance for.
        /// If left empty or set to 0, the tolerance is set to all the secondary axes.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetToleranceAsync(double tolerance, Units unit = Units.Native, int axisIndex = 0)
        {
            var request = new Requests.LockstepSetRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
                Value = tolerance,
                Unit = unit,
                AxisIndex = axisIndex,
            };

            await Gateway.CallAsync("device/lockstep_set_tolerance", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets lockstep twist tolerance.
        /// Twist tolerances that do not match the system configuration can reduce performance or damage the system.
        /// </summary>
        /// <param name="tolerance">Twist tolerance.</param>
        /// <param name="unit">Units of the tolerance.
        /// Uses primary axis unit conversion when setting to all axes,
        /// otherwise uses specified secondary axis unit conversion.</param>
        /// <param name="axisIndex">Optional index of a secondary axis to set the tolerance for.
        /// If left empty or set to 0, the tolerance is set to all the secondary axes.</param>
        public void SetTolerance(double tolerance, Units unit = Units.Native, int axisIndex = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetToleranceAsync(tolerance, unit, axisIndex);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if the lockstep group is currently enabled on the device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if a lockstep group with this ID is enabled on the device.</returns>
        public async Task<bool> IsEnabledAsync()
        {
            var request = new Requests.LockstepEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            var response = await Gateway.CallAsync("device/lockstep_is_enabled", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Checks if the lockstep group is currently enabled on the device.
        /// </summary>
        /// <returns>True if a lockstep group with this ID is enabled on the device.</returns>
        public bool IsEnabled()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsEnabledAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string which represents the enabled lockstep group.
        /// </summary>
        /// <returns>String which represents the enabled lockstep group.</returns>
        public override string ToString()
        {
            var request = new Requests.LockstepEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                LockstepGroupId = LockstepGroupId,
            };

            var response = Gateway.CallSync("device/lockstep_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}

#pragma warning restore 612, 618
