/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Information about a parameter used for the simple tuning method.
    /// </summary>
    public class SimpleTuningParamDefinition : IMessage
    {
        /// <summary>
        /// The name of the parameter.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// The human readable description of the effect of a lower value on this setting.
        /// </summary>
        [JsonProperty("minLabel")]
        public string MinLabel { get; set; } = string.Empty;

        /// <summary>
        /// The human readable description of the effect of a higher value on this setting.
        /// </summary>
        [JsonProperty("maxLabel")]
        public string MaxLabel { get; set; } = string.Empty;

        /// <summary>
        /// How this parameter will be parsed by the tuner.
        /// </summary>
        [JsonProperty("dataType")]
        public string DataType { get; set; } = string.Empty;

        /// <summary>
        /// The default value of this parameter.
        /// </summary>
        [JsonProperty("defaultValue")]
        public double? DefaultValue { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SimpleTuningParamDefinition))
            {
                return false;
            }

            SimpleTuningParamDefinition other = (SimpleTuningParamDefinition)obj;
            return (
                EqualityUtils.CheckEquals(Name, other.Name) &&
                EqualityUtils.CheckEquals(MinLabel, other.MinLabel) &&
                EqualityUtils.CheckEquals(MaxLabel, other.MaxLabel) &&
                EqualityUtils.CheckEquals(DataType, other.DataType) &&
                EqualityUtils.CheckEquals(DefaultValue, other.DefaultValue)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Name));
            hash.Add(EqualityUtils.GenerateHashCode(MinLabel));
            hash.Add(EqualityUtils.GenerateHashCode(MaxLabel));
            hash.Add(EqualityUtils.GenerateHashCode(DataType));
            hash.Add(EqualityUtils.GenerateHashCode(DefaultValue));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SimpleTuningParamDefinition.ToByteArray(this);

        internal static SimpleTuningParamDefinition FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SimpleTuningParamDefinition>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SimpleTuningParamDefinition instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
