/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The response from a multi-get command.
    /// </summary>
    public class GetSettingResult : IMessage
    {
        /// <summary>
        /// The setting read.
        /// </summary>
        [JsonProperty("setting")]
        public string Setting { get; set; } = string.Empty;

        /// <summary>
        /// The list of values returned.
        /// </summary>
        [JsonProperty("values")]
        public double[] Values { get; set; } = System.Array.Empty<double>();

        /// <summary>
        /// The unit of the values.
        /// </summary>
        [JsonProperty("unit")]
        public Units Unit { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is GetSettingResult))
            {
                return false;
            }

            GetSettingResult other = (GetSettingResult)obj;
            return (
                EqualityUtils.CheckEquals(Setting, other.Setting) &&
                EqualityUtils.CheckEquals(Values, other.Values) &&
                EqualityUtils.CheckEquals(Unit, other.Unit)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Setting));
            hash.Add(EqualityUtils.GenerateHashCode(Values));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => GetSettingResult.ToByteArray(this);

        internal static GetSettingResult FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<GetSettingResult>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(GetSettingResult instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
