/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// An object containing any setup issues that will prevent setting a state to a given device.
    /// </summary>
    public class CanSetStateDeviceResponse : IMessage
    {
        /// <summary>
        /// The error blocking applying this state to the given device.
        /// </summary>
        [JsonProperty("error")]
        public string? Error { get; set; }

        /// <summary>
        /// A list of errors that block setting state of device's axes.
        /// </summary>
        [JsonProperty("axisErrors")]
        public CanSetStateAxisResponse[] AxisErrors { get; set; } = System.Array.Empty<CanSetStateAxisResponse>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is CanSetStateDeviceResponse))
            {
                return false;
            }

            CanSetStateDeviceResponse other = (CanSetStateDeviceResponse)obj;
            return (
                EqualityUtils.CheckEquals(Error, other.Error) &&
                EqualityUtils.CheckEquals(AxisErrors, other.AxisErrors)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Error));
            hash.Add(EqualityUtils.GenerateHashCode(AxisErrors));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => CanSetStateDeviceResponse.ToByteArray(this);

        internal static CanSetStateDeviceResponse FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<CanSetStateDeviceResponse>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(CanSetStateDeviceResponse instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
