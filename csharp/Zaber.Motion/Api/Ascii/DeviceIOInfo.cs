/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class representing information on the I/O channels of the device.
    /// </summary>
    public class DeviceIOInfo : IMessage
    {
        /// <summary>
        /// Number of analog output channels.
        /// </summary>
        [JsonProperty("numberAnalogOutputs")]
        public int NumberAnalogOutputs { get; set; }

        /// <summary>
        /// Number of analog input channels.
        /// </summary>
        [JsonProperty("numberAnalogInputs")]
        public int NumberAnalogInputs { get; set; }

        /// <summary>
        /// Number of digital output channels.
        /// </summary>
        [JsonProperty("numberDigitalOutputs")]
        public int NumberDigitalOutputs { get; set; }

        /// <summary>
        /// Number of digital input channels.
        /// </summary>
        [JsonProperty("numberDigitalInputs")]
        public int NumberDigitalInputs { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is DeviceIOInfo))
            {
                return false;
            }

            DeviceIOInfo other = (DeviceIOInfo)obj;
            return (
                EqualityUtils.CheckEquals(NumberAnalogOutputs, other.NumberAnalogOutputs) &&
                EqualityUtils.CheckEquals(NumberAnalogInputs, other.NumberAnalogInputs) &&
                EqualityUtils.CheckEquals(NumberDigitalOutputs, other.NumberDigitalOutputs) &&
                EqualityUtils.CheckEquals(NumberDigitalInputs, other.NumberDigitalInputs)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(NumberAnalogOutputs));
            hash.Add(EqualityUtils.GenerateHashCode(NumberAnalogInputs));
            hash.Add(EqualityUtils.GenerateHashCode(NumberDigitalOutputs));
            hash.Add(EqualityUtils.GenerateHashCode(NumberDigitalInputs));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => DeviceIOInfo.ToByteArray(this);

        internal static DeviceIOInfo FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<DeviceIOInfo>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(DeviceIOInfo instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
