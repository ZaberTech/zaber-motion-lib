﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to the I/O channels of the device.
    /// </summary>
    public class DeviceIO
    {
        internal DeviceIO(Device device)
        {
            _device = device;
        }


        /// <summary>
        /// Returns the number of I/O channels the device has.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: An object containing the number of I/O channels the device has.</returns>
        public async Task<DeviceIOInfo> GetChannelsInfoAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("device/get_io_info", request, DeviceIOInfo.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Returns the number of I/O channels the device has.
        /// </summary>
        /// <returns>An object containing the number of I/O channels the device has.</returns>
        public DeviceIOInfo GetChannelsInfo()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetChannelsInfoAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the label of the specified channel.
        /// </summary>
        /// <param name="portType">The type of channel to set the label of.</param>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="label">The label to set for the specified channel.
        /// If no value or an empty string is provided, this label is deleted.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLabelAsync(IoPortType portType, int channelNumber, string? label)
        {
            var request = new Requests.SetIoPortLabelRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                PortType = portType,
                ChannelNumber = channelNumber,
                Label = label,
            };

            await Gateway.CallAsync("device/set_io_label", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the label of the specified channel.
        /// </summary>
        /// <param name="portType">The type of channel to set the label of.</param>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="label">The label to set for the specified channel.
        /// If no value or an empty string is provided, this label is deleted.</param>
        public void SetLabel(IoPortType portType, int channelNumber, string? label)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLabelAsync(portType, channelNumber, label);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the label of the specified channel.
        /// </summary>
        /// <param name="portType">The type of channel to get the label of.</param>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to get the result: The label of the specified channel.</returns>
        public async Task<string> GetLabelAsync(IoPortType portType, int channelNumber)
        {
            var request = new Requests.GetIoPortLabelRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                PortType = portType,
                ChannelNumber = channelNumber,
            };

            var response = await Gateway.CallAsync("device/get_io_label", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the label of the specified channel.
        /// </summary>
        /// <param name="portType">The type of channel to get the label of.</param>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>The label of the specified channel.</returns>
        public string GetLabel(IoPortType portType, int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetLabelAsync(portType, channelNumber);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns every label assigned to an IO port on this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The labels set for this device's IO.</returns>
        public async Task<IoPortLabel[]> GetAllLabelsAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("device/get_all_io_labels", request, Requests.GetAllIoPortLabelsResponse.FromByteArray).ConfigureAwait(false);
            return response.Labels;
        }


        /// <summary>
        /// Returns every label assigned to an IO port on this device.
        /// </summary>
        /// <returns>The labels set for this device's IO.</returns>
        public IoPortLabel[] GetAllLabels()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllLabelsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current value of the specified digital input channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to get the result: True if voltage is present on the input channel and false otherwise.</returns>
        public async Task<bool> GetDigitalInputAsync(int channelNumber)
        {
            var request = new Requests.DeviceGetDigitalIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "di",
                ChannelNumber = channelNumber,
            };

            var response = await Gateway.CallAsync("device/get_digital_io", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the current value of the specified digital input channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>True if voltage is present on the input channel and false otherwise.</returns>
        public bool GetDigitalInput(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetDigitalInputAsync(channelNumber);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current value of the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to get the result: True if the output channel is conducting and false otherwise.</returns>
        public async Task<bool> GetDigitalOutputAsync(int channelNumber)
        {
            var request = new Requests.DeviceGetDigitalIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "do",
                ChannelNumber = channelNumber,
            };

            var response = await Gateway.CallAsync("device/get_digital_io", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the current value of the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>True if the output channel is conducting and false otherwise.</returns>
        public bool GetDigitalOutput(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetDigitalOutputAsync(channelNumber);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current value of the specified analog input channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to get the result:  A measurement of the voltage present on the input channel.</returns>
        public async Task<double> GetAnalogInputAsync(int channelNumber)
        {
            var request = new Requests.DeviceGetAnalogIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "ai",
                ChannelNumber = channelNumber,
            };

            var response = await Gateway.CallAsync("device/get_analog_io", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the current value of the specified analog input channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns> A measurement of the voltage present on the input channel.</returns>
        public double GetAnalogInput(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAnalogInputAsync(channelNumber);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current values of the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to get the result: A measurement of voltage that the output channel is conducting.</returns>
        public async Task<double> GetAnalogOutputAsync(int channelNumber)
        {
            var request = new Requests.DeviceGetAnalogIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "ao",
                ChannelNumber = channelNumber,
            };

            var response = await Gateway.CallAsync("device/get_analog_io", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the current values of the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A measurement of voltage that the output channel is conducting.</returns>
        public double GetAnalogOutput(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAnalogOutputAsync(channelNumber);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current values of all digital input channels.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if voltage is present on the input channel and false otherwise.</returns>
        public async Task<bool[]> GetAllDigitalInputsAsync()
        {
            var request = new Requests.DeviceGetAllDigitalIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "di",
            };

            var response = await Gateway.CallAsync("device/get_all_digital_io", request, Requests.DeviceGetAllDigitalIOResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Returns the current values of all digital input channels.
        /// </summary>
        /// <returns>True if voltage is present on the input channel and false otherwise.</returns>
        public bool[] GetAllDigitalInputs()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllDigitalInputsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current values of all digital output channels.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the output channel is conducting and false otherwise.</returns>
        public async Task<bool[]> GetAllDigitalOutputsAsync()
        {
            var request = new Requests.DeviceGetAllDigitalIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "do",
            };

            var response = await Gateway.CallAsync("device/get_all_digital_io", request, Requests.DeviceGetAllDigitalIOResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Returns the current values of all digital output channels.
        /// </summary>
        /// <returns>True if the output channel is conducting and false otherwise.</returns>
        public bool[] GetAllDigitalOutputs()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllDigitalOutputsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current values of all analog input channels.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Measurements of the voltages present on the input channels.</returns>
        public async Task<double[]> GetAllAnalogInputsAsync()
        {
            var request = new Requests.DeviceGetAllAnalogIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "ai",
            };

            var response = await Gateway.CallAsync("device/get_all_analog_io", request, Requests.DeviceGetAllAnalogIOResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Returns the current values of all analog input channels.
        /// </summary>
        /// <returns>Measurements of the voltages present on the input channels.</returns>
        public double[] GetAllAnalogInputs()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllAnalogInputsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current values of all analog output channels.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Measurements of voltage that the output channels are conducting.</returns>
        public async Task<double[]> GetAllAnalogOutputsAsync()
        {
            var request = new Requests.DeviceGetAllAnalogIORequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelType = "ao",
            };

            var response = await Gateway.CallAsync("device/get_all_analog_io", request, Requests.DeviceGetAllAnalogIOResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Returns the current values of all analog output channels.
        /// </summary>
        /// <returns>Measurements of voltage that the output channels are conducting.</returns>
        public double[] GetAllAnalogOutputs()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllAnalogOutputsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetDigitalOutputAsync(int channelNumber, DigitalOutputAction value)
        {
            var request = new Requests.DeviceSetDigitalOutputRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/set_digital_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        public void SetDigitalOutput(int channelNumber, DigitalOutputAction value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllDigitalOutputsAsync(DigitalOutputAction[] values)
        {
            var request = new Requests.DeviceSetAllDigitalOutputsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Values = values,
            };

            await Gateway.CallAsync("device/set_all_digital_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        public void SetAllDigitalOutputs(DigitalOutputAction[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future value for the specified digital output channel.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform immediately on the channel.</param>
        /// <param name="futureValue">The type of action to perform in the future on the channel.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetDigitalOutputScheduleAsync(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.DeviceSetDigitalOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelNumber = channelNumber,
                Value = value,
                FutureValue = futureValue,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_digital_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future value for the specified digital output channel.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform immediately on the channel.</param>
        /// <param name="futureValue">The type of action to perform in the future on the channel.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        public void SetDigitalOutputSchedule(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future values for all digital output channels.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="values">The type of actions to perform immediately on output channels.</param>
        /// <param name="futureValues">The type of actions to perform in the future on output channels.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllDigitalOutputsScheduleAsync(DigitalOutputAction[] values, DigitalOutputAction[] futureValues, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.DeviceSetAllDigitalOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Values = values,
                FutureValues = futureValues,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_all_digital_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future values for all digital output channels.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="values">The type of actions to perform immediately on output channels.</param>
        /// <param name="futureValues">The type of actions to perform in the future on output channels.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAllDigitalOutputsSchedule(DigitalOutputAction[] values, DigitalOutputAction[] futureValues, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsScheduleAsync(values, futureValues, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAnalogOutputAsync(int channelNumber, double value)
        {
            var request = new Requests.DeviceSetAnalogOutputRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/set_analog_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        public void SetAnalogOutput(int channelNumber, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllAnalogOutputsAsync(double[] values)
        {
            var request = new Requests.DeviceSetAllAnalogOutputsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Values = values,
            };

            await Gateway.CallAsync("device/set_all_analog_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        public void SetAllAnalogOutputs(double[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future value for the specified analog output channel.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to immediately.</param>
        /// <param name="futureValue">Value to set the output channel voltage to in the future.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAnalogOutputScheduleAsync(int channelNumber, double value, double futureValue, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.DeviceSetAnalogOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelNumber = channelNumber,
                Value = value,
                FutureValue = futureValue,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_analog_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future value for the specified analog output channel.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to immediately.</param>
        /// <param name="futureValue">Value to set the output channel voltage to in the future.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAnalogOutputSchedule(int channelNumber, double value, double futureValue, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future values for all analog output channels.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to immediately.</param>
        /// <param name="futureValues">Voltage values to set the output channels to in the future.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllAnalogOutputsScheduleAsync(double[] values, double[] futureValues, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.DeviceSetAllAnalogOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Values = values,
                FutureValues = futureValues,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_all_analog_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future values for all analog output channels.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to immediately.</param>
        /// <param name="futureValues">Voltage values to set the output channels to in the future.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAllAnalogOutputsSchedule(double[] values, double[] futureValues, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsScheduleAsync(values, futureValues, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
        /// Set the frequency to 0 to disable the filter.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="cutoffFrequency">Cutoff frequency of the low-pass filter.</param>
        /// <param name="unit">Units of frequency.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAnalogInputLowpassFilterAsync(int channelNumber, double cutoffFrequency, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetLowpassFilterRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                ChannelNumber = channelNumber,
                CutoffFrequency = cutoffFrequency,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_lowpass_filter", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
        /// Set the frequency to 0 to disable the filter.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="cutoffFrequency">Cutoff frequency of the low-pass filter.</param>
        /// <param name="unit">Units of frequency.</param>
        public void SetAnalogInputLowpassFilter(int channelNumber, double cutoffFrequency, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogInputLowpassFilterAsync(channelNumber, cutoffFrequency, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancels a scheduled digital output action.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelDigitalOutputScheduleAsync(int channelNumber)
        {
            var request = new Requests.DeviceCancelOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = false,
                ChannelNumber = channelNumber,
            };

            await Gateway.CallAsync("device/cancel_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancels a scheduled digital output action.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        public void CancelDigitalOutputSchedule(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelDigitalOutputScheduleAsync(channelNumber);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancel all scheduled digital output actions.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled digital output action for that channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAllDigitalOutputsScheduleAsync(bool[] channels = null!)
        {
            var request = new Requests.DeviceCancelAllOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = false,
                Channels = channels,
            };

            await Gateway.CallAsync("device/cancel_all_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancel all scheduled digital output actions.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled digital output action for that channel.</param>
        public void CancelAllDigitalOutputsSchedule(bool[] channels = null!)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAllDigitalOutputsScheduleAsync(channels);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancels a scheduled analog output value.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAnalogOutputScheduleAsync(int channelNumber)
        {
            var request = new Requests.DeviceCancelOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = true,
                ChannelNumber = channelNumber,
            };

            await Gateway.CallAsync("device/cancel_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancels a scheduled analog output value.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        public void CancelAnalogOutputSchedule(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAnalogOutputScheduleAsync(channelNumber);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancel all scheduled analog output actions.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled analog output value for that channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAllAnalogOutputsScheduleAsync(bool[] channels = null!)
        {
            var request = new Requests.DeviceCancelAllOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = true,
                Channels = channels,
            };

            await Gateway.CallAsync("device/cancel_all_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancel all scheduled analog output actions.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled analog output value for that channel.</param>
        public void CancelAllAnalogOutputsSchedule(bool[] channels = null!)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAllAnalogOutputsScheduleAsync(channels);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Device _device;


    }
}
