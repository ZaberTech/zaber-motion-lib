/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents unit conversion factor for a single dimension.
    /// </summary>
    public class ConversionFactor : IMessage
    {
        /// <summary>
        /// Setting representing the dimension.
        /// </summary>
        [JsonProperty("setting")]
        public string Setting { get; set; } = string.Empty;

        /// <summary>
        /// Value representing 1 native device unit in specified real-word units.
        /// </summary>
        [JsonProperty("value")]
        public double Value { get; set; }

        /// <summary>
        /// Units of the value.
        /// </summary>
        [JsonProperty("unit")]
        public Units Unit { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is ConversionFactor))
            {
                return false;
            }

            ConversionFactor other = (ConversionFactor)obj;
            return (
                EqualityUtils.CheckEquals(Setting, other.Setting) &&
                EqualityUtils.CheckEquals(Value, other.Value) &&
                EqualityUtils.CheckEquals(Unit, other.Unit)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Setting));
            hash.Add(EqualityUtils.GenerateHashCode(Value));
            hash.Add(EqualityUtils.GenerateHashCode(Unit));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => ConversionFactor.ToByteArray(this);

        internal static ConversionFactor FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<ConversionFactor>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(ConversionFactor instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
