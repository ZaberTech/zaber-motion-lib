/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The masses and parameters last used by simple tuning.
    /// </summary>
    public class SimpleTuning : IMessage
    {
        /// <summary>
        /// Whether the tuning returned is currently in use by this paramset,
        /// or if it has been overwritten by a later change.
        /// </summary>
        [JsonProperty("isUsed")]
        public bool IsUsed { get; set; }

        /// <summary>
        /// The mass of the carriage in kg.
        /// </summary>
        [JsonProperty("carriageMass")]
        public double? CarriageMass { get; set; }

        /// <summary>
        /// The mass of the load in kg, excluding the mass of the carriage.
        /// </summary>
        [JsonProperty("loadMass")]
        public double LoadMass { get; set; }

        /// <summary>
        /// The parameters used by simple tuning.
        /// </summary>
        [JsonProperty("tuningParams")]
        public ServoTuningParam[] TuningParams { get; set; } = System.Array.Empty<ServoTuningParam>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SimpleTuning))
            {
                return false;
            }

            SimpleTuning other = (SimpleTuning)obj;
            return (
                EqualityUtils.CheckEquals(IsUsed, other.IsUsed) &&
                EqualityUtils.CheckEquals(CarriageMass, other.CarriageMass) &&
                EqualityUtils.CheckEquals(LoadMass, other.LoadMass) &&
                EqualityUtils.CheckEquals(TuningParams, other.TuningParams)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(IsUsed));
            hash.Add(EqualityUtils.GenerateHashCode(CarriageMass));
            hash.Add(EqualityUtils.GenerateHashCode(LoadMass));
            hash.Add(EqualityUtils.GenerateHashCode(TuningParams));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SimpleTuning.ToByteArray(this);

        internal static SimpleTuning FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SimpleTuning>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SimpleTuning instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
