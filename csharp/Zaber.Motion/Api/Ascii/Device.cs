﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents the controller part of one device - may be either a standalone controller or an integrated controller.
    /// </summary>
    public class Device
    {
        internal Device(Connection connection, int deviceAddress)
        {
            Connection = connection;
            DeviceAddress = deviceAddress;
            Settings = new DeviceSettings(this);
            Storage = new DeviceStorage(this);
            IO = new DeviceIO(this);
            AllAxes = new AllAxes(this);
            Warnings = new Warnings(this, 0);
            Oscilloscope = new Oscilloscope(this);
            Triggers = new Triggers(this);
            Streams = new Streams(this);
            Pvt = new Pvt(this);
        }


        /// <summary>
        /// Connection of this device.
        /// </summary>
        public Connection Connection { get; private set; }


        /// <summary>
        /// The device address uniquely identifies the device on the connection.
        /// It can be configured or automatically assigned by the renumber command.
        /// </summary>
        public int DeviceAddress { get; private set; }


        /// <summary>
        /// Settings and properties of this device.
        /// </summary>
        public DeviceSettings Settings { get; private set; }


        /// <summary>
        /// Key-value storage of this device.
        /// </summary>
        public DeviceStorage Storage { get; private set; }


        /// <summary>
        /// I/O channels of this device.
        /// </summary>
        public DeviceIO IO { get; private set; }


        /// <summary>
        /// Virtual axis which allows you to target all axes of this device.
        /// </summary>
        public AllAxes AllAxes { get; private set; }


        /// <summary>
        /// Warnings and faults of this device and all its axes.
        /// </summary>
        public Warnings Warnings { get; private set; }


        /// <summary>
        /// Identity of the device.
        /// </summary>
        public DeviceIdentity Identity
        {
            get { return RetrieveIdentity(); }
        }


        /// <summary>
        /// Indicates whether or not the device has been identified.
        /// </summary>
        public bool IsIdentified
        {
            get { return RetrieveIsIdentified(); }
        }


        /// <summary>
        /// Oscilloscope recording helper for this device.
        /// Requires at least Firmware 7.00.
        /// </summary>
        public Oscilloscope Oscilloscope { get; private set; }


        /// <summary>
        /// Unique ID of the device hardware.
        /// </summary>
        public int DeviceId
        {
            get { return Identity.DeviceId; }
        }


        /// <summary>
        /// Serial number of the device.
        /// </summary>
        public long SerialNumber
        {
            get { return Identity.SerialNumber; }
        }


        /// <summary>
        /// Name of the product.
        /// </summary>
        public string Name
        {
            get { return Identity.Name; }
        }


        /// <summary>
        /// Number of axes this device has.
        /// </summary>
        public int AxisCount
        {
            get { return Identity.AxisCount; }
        }


        /// <summary>
        /// Version of the firmware.
        /// </summary>
        public FirmwareVersion FirmwareVersion
        {
            get { return Identity.FirmwareVersion; }
        }


        /// <summary>
        /// The device is an integrated product.
        /// </summary>
        public bool IsIntegrated
        {
            get { return Identity.IsIntegrated; }
        }


        /// <summary>
        /// User-assigned label of the device.
        /// </summary>
        public string Label
        {
            get { return RetrieveLabel(); }
        }


        /// <summary>
        /// Triggers for this device.
        /// Requires at least Firmware 7.06.
        /// </summary>
        public Triggers Triggers { get; private set; }


        /// <summary>
        /// Gets an object that provides access to Streams on this device.
        /// Requires at least Firmware 7.05.
        /// </summary>
        public Streams Streams { get; private set; }


        /// <summary>
        /// Gets an object that provides access to PVT functions of this device.
        /// Note that as of ZML v5.0.0, this returns a Pvt object and NOT a PvtSequence object.
        /// The PvtSequence can now be obtained from the Pvt object.
        /// Requires at least Firmware 7.33.
        /// </summary>
        public Pvt Pvt { get; private set; }


        /// <summary>
        /// Queries the device and the database, gathering information about the product.
        /// Without this information features such as unit conversions will not work.
        /// Usually, called automatically by detect devices method.
        /// </summary>
        /// <param name="assumeVersion">The identification assumes the specified firmware version
        /// instead of the version queried from the device.
        /// Providing this argument can lead to unexpected compatibility issues.</param>
        /// <returns>A Task that can be awaited to get the result: Device identification data.</returns>
        public async Task<DeviceIdentity> IdentifyAsync(FirmwareVersion? assumeVersion = null)
        {
            var request = new Requests.DeviceIdentifyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                AssumeVersion = assumeVersion,
            };

            var response = await Gateway.CallAsync("device/identify", request, DeviceIdentity.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Queries the device and the database, gathering information about the product.
        /// Without this information features such as unit conversions will not work.
        /// Usually, called automatically by detect devices method.
        /// </summary>
        /// <param name="assumeVersion">The identification assumes the specified firmware version
        /// instead of the version queried from the device.
        /// Providing this argument can lead to unexpected compatibility issues.</param>
        /// <returns>Device identification data.</returns>
        public DeviceIdentity Identify(FirmwareVersion? assumeVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IdentifyAsync(assumeVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this device.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Response> GenericCommandAsync(string command, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Axis = axis,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command", request, Response.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic ASCII command to this device.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A response to the command.</returns>
        public Response GenericCommand(string command, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, axis, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this device and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Response[]> GenericCommandMultiResponseAsync(string command, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Axis = axis,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command_multi_response", request, Requests.GenericCommandResponseCollection.FromByteArray).ConfigureAwait(false);
            return response.Responses;
        }


        /// <summary>
        /// Sends a generic ASCII command to this device and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>All responses to the command.</returns>
        public Response[] GenericCommandMultiResponse(string command, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, axis, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this device without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.
        /// Specifying -1 omits the number completely.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(string command, int axis = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Axis = axis,
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to this device without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="axis">Optional axis number to send the command to.
        /// Specifying -1 omits the number completely.</param>
        public void GenericCommandNoResponse(string command, int axis = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command, axis);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets an Axis class instance which allows you to control a particular axis on this device.
        /// Axes are numbered from 1.
        /// </summary>
        /// <param name="axisNumber">Number of axis intended to control.</param>
        /// <returns>Axis instance.</returns>
        public Axis GetAxis(int axisNumber)
        {
            if (axisNumber <= 0)
            {
                throw new ArgumentException("Invalid value; physical axes are numbered from 1.");
            }

            return new Axis(this, axisNumber);
        }


        /// <summary>
        /// Gets a Lockstep class instance which allows you to control a particular lockstep group on the device.
        /// Requires at least Firmware 6.15 or 7.11.
        /// </summary>
        /// <param name="lockstepGroupId">The ID of the lockstep group to control. Lockstep group IDs start at one.</param>
        /// <returns>Lockstep instance.</returns>
        public Lockstep GetLockstep(int lockstepGroupId)
        {
            if (lockstepGroupId <= 0)
            {
                throw new ArgumentException("Invalid value; lockstep groups are numbered from 1.");
            }

            return new Lockstep(this, lockstepGroupId);
        }


        /// <summary>
        /// Formats parameters into a command and performs unit conversions.
        /// Parameters in the command template are denoted by a question mark.
        /// Command returned is only valid for this device.
        /// Unit conversion is not supported for commands where axes can be remapped, such as stream and PVT commands.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="commandTemplate">Template of a command to prepare. Parameters are denoted by question marks.</param>
        /// <param name="parameters">Variable number of command parameters.</param>
        /// <returns>Command with converted parameters.</returns>
        public string PrepareCommand(string commandTemplate, params Measurement[] parameters)
        {
            var request = new Requests.PrepareCommandRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                CommandTemplate = commandTemplate,
                Parameters = parameters,
            };

            var response = Gateway.CallSync("device/prepare_command", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets the user-assigned device label.
        /// The label is stored on the controller and recognized by other software.
        /// </summary>
        /// <param name="label">Label to set.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLabelAsync(string label)
        {
            var request = new Requests.DeviceSetStorageRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Value = label,
            };

            await Gateway.CallAsync("device/set_label", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the user-assigned device label.
        /// The label is stored on the controller and recognized by other software.
        /// </summary>
        /// <param name="label">Label to set.</param>
        public void SetLabel(string label)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLabelAsync(label);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current device state that can be saved and reapplied.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A serialization of the current state of the device.</returns>
        public async Task<string> GetStateAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = await Gateway.CallAsync("device/get_state", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current device state that can be saved and reapplied.
        /// </summary>
        /// <returns>A serialization of the current state of the device.</returns>
        public string GetState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Applies a saved state to this device.
        /// </summary>
        /// <param name="state">The state object to apply to this device.</param>
        /// <param name="deviceOnly">If true, only device scope settings and features will be set.</param>
        /// <returns>A Task that can be awaited to get the result: Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public async Task<SetStateDeviceResponse> SetStateAsync(string state, bool deviceOnly = false)
        {
            var request = new Requests.SetStateRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                State = state,
                DeviceOnly = deviceOnly,
            };

            var response = await Gateway.CallAsync("device/set_device_state", request, SetStateDeviceResponse.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Applies a saved state to this device.
        /// </summary>
        /// <param name="state">The state object to apply to this device.</param>
        /// <param name="deviceOnly">If true, only device scope settings and features will be set.</param>
        /// <returns>Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public SetStateDeviceResponse SetState(string state, bool deviceOnly = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStateAsync(state, deviceOnly);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if a state can be applied to this device.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>A Task that can be awaited to get the result: An object listing errors that come up when trying to set the state.</returns>
        public async Task<CanSetStateDeviceResponse> CanSetStateAsync(string state, FirmwareVersion? firmwareVersion = null)
        {
            var request = new Requests.CanSetStateRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                State = state,
                FirmwareVersion = firmwareVersion,
            };

            var response = await Gateway.CallAsync("device/can_set_state", request, CanSetStateDeviceResponse.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Checks if a state can be applied to this device.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>An object listing errors that come up when trying to set the state.</returns>
        public CanSetStateDeviceResponse CanSetState(string state, FirmwareVersion? firmwareVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CanSetStateAsync(state, firmwareVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits for the device to start responding to messages.
        /// Useful to call after resetting the device.
        /// Throws RequestTimeoutException upon timeout.
        /// </summary>
        /// <param name="timeout">For how long to wait in milliseconds for the device to start responding.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitToRespondAsync(double timeout)
        {
            var request = new Requests.WaitToRespondRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Timeout = timeout,
            };

            await Gateway.CallAsync("device/wait_to_respond", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits for the device to start responding to messages.
        /// Useful to call after resetting the device.
        /// Throws RequestTimeoutException upon timeout.
        /// </summary>
        /// <param name="timeout">For how long to wait in milliseconds for the device to start responding.</param>
        public void WaitToRespond(double timeout)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitToRespondAsync(timeout);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Changes the address of this device.
        /// After the address is successfully changed, the existing device class instance no longer represents the device.
        /// Instead, use the new device instance returned by this method.
        /// </summary>
        /// <param name="address">The new address to assign to the device.</param>
        /// <returns>A Task that can be awaited to get the result: New device instance with the new address.</returns>
        public async Task<Device> RenumberAsync(int address)
        {
            if (address < 1 || address > 99)
            {
                throw new ArgumentException("Invalid value; device addresses are numbered from 1 to 99.");
            }

            var request = new Requests.RenumberRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Address = address,
            };

            var response = await Gateway.CallAsync("device/renumber", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return new Device(Connection, response.Value);
        }


        /// <summary>
        /// Changes the address of this device.
        /// After the address is successfully changed, the existing device class instance no longer represents the device.
        /// Instead, use the new device instance returned by this method.
        /// </summary>
        /// <param name="address">The new address to assign to the device.</param>
        /// <returns>New device instance with the new address.</returns>
        public Device Renumber(int address)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = RenumberAsync(address);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Restores most of the settings to their default values.
        /// Deletes all triggers, stream and PVT buffers, servo tunings.
        /// Deletes all zaber storage keys.
        /// Disables locksteps, unparks axes.
        /// Preserves storage, communication settings, peripherals (unless hard is specified).
        /// The device needs to be identified again after the restore.
        /// </summary>
        /// <param name="hard">If true, completely erases device's memory. The device also resets.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task RestoreAsync(bool hard = false)
        {
            var request = new Requests.DeviceRestoreRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Hard = hard,
            };

            await Gateway.CallAsync("device/restore", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Restores most of the settings to their default values.
        /// Deletes all triggers, stream and PVT buffers, servo tunings.
        /// Deletes all zaber storage keys.
        /// Disables locksteps, unparks axes.
        /// Preserves storage, communication settings, peripherals (unless hard is specified).
        /// The device needs to be identified again after the restore.
        /// </summary>
        /// <param name="hard">If true, completely erases device's memory. The device also resets.</param>
        public void Restore(bool hard = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = RestoreAsync(hard);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the device name.
        /// </summary>
        /// <returns>The label.</returns>
        private string RetrieveLabel()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("device/get_label", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns identity.
        /// </summary>
        /// <returns>Device identity.</returns>
        private DeviceIdentity RetrieveIdentity()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("device/get_identity", request, DeviceIdentity.FromByteArray);
            return response;
        }


        /// <summary>
        /// Returns whether or not the device have been identified.
        /// </summary>
        /// <returns>True if the device has already been identified. False otherwise.</returns>
        private bool RetrieveIsIdentified()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("device/get_is_identified", request, Requests.BoolResponse.FromByteArray);
            return response.Value;
        }


    }
}
