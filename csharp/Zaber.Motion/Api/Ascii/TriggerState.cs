/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The complete state of a trigger.
    /// </summary>
    public class TriggerState : IMessage
    {
        /// <summary>
        /// The firing condition for a trigger.
        /// </summary>
        [JsonProperty("condition")]
        public string Condition { get; set; } = string.Empty;

        /// <summary>
        /// The actions for a trigger.
        /// </summary>
        [JsonProperty("actions")]
        public string[] Actions { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The enabled state for a trigger.
        /// </summary>
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        /// <summary>
        /// The number of total fires for this trigger.
        /// A value of -1 indicates unlimited fires.
        /// </summary>
        [JsonProperty("firesTotal")]
        public int FiresTotal { get; set; }

        /// <summary>
        /// The number of remaining fires for this trigger.
        /// A value of -1 indicates unlimited fires remaining.
        /// </summary>
        [JsonProperty("firesRemaining")]
        public int FiresRemaining { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TriggerState))
            {
                return false;
            }

            TriggerState other = (TriggerState)obj;
            return (
                EqualityUtils.CheckEquals(Condition, other.Condition) &&
                EqualityUtils.CheckEquals(Actions, other.Actions) &&
                EqualityUtils.CheckEquals(Enabled, other.Enabled) &&
                EqualityUtils.CheckEquals(FiresTotal, other.FiresTotal) &&
                EqualityUtils.CheckEquals(FiresRemaining, other.FiresRemaining)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Condition));
            hash.Add(EqualityUtils.GenerateHashCode(Actions));
            hash.Add(EqualityUtils.GenerateHashCode(Enabled));
            hash.Add(EqualityUtils.GenerateHashCode(FiresTotal));
            hash.Add(EqualityUtils.GenerateHashCode(FiresRemaining));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TriggerState.ToByteArray(this);

        internal static TriggerState FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TriggerState>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TriggerState instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
