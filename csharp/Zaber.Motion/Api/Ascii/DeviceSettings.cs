﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to various device settings and properties.
    /// </summary>
    public class DeviceSettings
    {
        internal DeviceSettings(Device device)
        {
            _device = device;
        }


        /// <summary>
        /// Returns any device setting or property.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to get the result: Setting value.</returns>
        public async Task<double> GetAsync(string setting, Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns any device setting or property.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>Setting value.</returns>
        public double Get(string setting, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAsync(setting, unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets any device setting.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAsync(string setting, double value, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets any device setting.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        public void Set(string setting, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAsync(setting, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns any device setting or property as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>A Task that can be awaited to get the result: Setting value.</returns>
        public async Task<string> GetStringAsync(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
            };

            var response = await Gateway.CallAsync("device/get_setting_str", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns any device setting or property as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>Setting value.</returns>
        public string GetString(string setting)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStringAsync(setting);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets any device setting as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetStringAsync(string setting, string value)
        {
            var request = new Requests.DeviceSetSettingStrRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Value = value,
            };

            await Gateway.CallAsync("device/set_setting_str", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets any device setting as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        public void SetString(string setting, string value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStringAsync(setting, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Convert arbitrary setting value to Zaber native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting in units specified by following argument.</param>
        /// <param name="unit">Units of the value.</param>
        /// <returns>Setting value.</returns>
        public double ConvertToNativeUnits(string setting, double value, Units unit)
        {
            var request = new Requests.DeviceConvertSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/convert_setting", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Convert arbitrary setting value from Zaber native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting in Zaber native units.</param>
        /// <param name="unit">Units to convert value to.</param>
        /// <returns>Setting value.</returns>
        public double ConvertFromNativeUnits(string setting, double value, Units unit)
        {
            var request = new Requests.DeviceConvertSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                FromNative = true,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/convert_setting", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns the default value of a setting.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>Default setting value.</returns>
        public double GetDefault(string setting, Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/get_setting_default", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns the default value of a setting as a string.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>Default setting value.</returns>
        public string GetDefaultString(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
            };

            var response = Gateway.CallSync("device/get_setting_default_str", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Indicates if given setting can be converted from and to native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>True if unit conversion can be performed.</returns>
        public bool CanConvertNativeUnits(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
            };

            var response = Gateway.CallSync("device/can_convert_setting", request, Requests.BoolResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Gets the value of an axis scope setting for each axis on the device.
        /// Values may be NaN where the setting is not applicable.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>A Task that can be awaited to get the result: The setting values on each axis.</returns>
        public async Task<double[]> GetFromAllAxesAsync(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
            };

            var response = await Gateway.CallAsync("device/get_setting_from_all_axes", request, Requests.DoubleArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Gets the value of an axis scope setting for each axis on the device.
        /// Values may be NaN where the setting is not applicable.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>The setting values on each axis.</returns>
        public double[] GetFromAllAxes(string setting)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetFromAllAxesAsync(setting);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets many setting values in as few device requests as possible.
        /// </summary>
        /// <param name="settings">The settings to read.</param>
        /// <returns>A Task that can be awaited to get the result: The setting values read.</returns>
        public async Task<GetSettingResult[]> GetManyAsync(params GetSetting[] settings)
        {
            var request = new Requests.DeviceMultiGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Settings = settings,
            };

            var response = await Gateway.CallAsync("device/get_many_settings", request, Requests.GetSettingResults.FromByteArray).ConfigureAwait(false);
            return response.Results;
        }


        /// <summary>
        /// Gets many setting values in as few device requests as possible.
        /// </summary>
        /// <param name="settings">The settings to read.</param>
        /// <returns>The setting values read.</returns>
        public GetSettingResult[] GetMany(params GetSetting[] settings)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetManyAsync(settings);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets many setting values in the same tick, ensuring their values are synchronized.
        /// Requires at least Firmware 7.35.
        /// </summary>
        /// <param name="settings">The settings to read.</param>
        /// <returns>A Task that can be awaited to get the result: The setting values read.</returns>
        public async Task<GetSettingResult[]> GetSynchronizedAsync(params GetSetting[] settings)
        {
            var request = new Requests.DeviceMultiGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Settings = settings,
            };

            var response = await Gateway.CallAsync("device/get_sync_settings", request, Requests.GetSettingResults.FromByteArray).ConfigureAwait(false);
            return response.Results;
        }


        /// <summary>
        /// Gets many setting values in the same tick, ensuring their values are synchronized.
        /// Requires at least Firmware 7.35.
        /// </summary>
        /// <param name="settings">The settings to read.</param>
        /// <returns>The setting values read.</returns>
        public GetSettingResult[] GetSynchronized(params GetSetting[] settings)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetSynchronizedAsync(settings);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Device _device;


    }
}
