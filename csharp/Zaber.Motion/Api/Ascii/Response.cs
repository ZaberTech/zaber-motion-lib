/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Response message from the device.
    /// </summary>
    public class Response : IMessage
    {
        /// <summary>
        /// Number of the device that sent the message.
        /// </summary>
        [JsonProperty("deviceAddress")]
        public int DeviceAddress { get; set; }

        /// <summary>
        /// Number of the axis which the response applies to. Zero denotes device scope.
        /// </summary>
        [JsonProperty("axisNumber")]
        public int AxisNumber { get; set; }

        /// <summary>
        /// The reply flag indicates if the request was accepted (OK) or rejected (RJ).
        /// </summary>
        [JsonProperty("replyFlag")]
        public string ReplyFlag { get; set; } = string.Empty;

        /// <summary>
        /// The device status contains BUSY when the axis is moving and IDLE otherwise.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        /// <summary>
        /// The warning flag contains the highest priority warning currently active for the device or axis.
        /// </summary>
        [JsonProperty("warningFlag")]
        public string WarningFlag { get; set; } = string.Empty;

        /// <summary>
        /// Response data which varies depending on the request.
        /// </summary>
        [JsonProperty("data")]
        public string Data { get; set; } = string.Empty;

        /// <summary>
        /// Type of the reply received.
        /// </summary>
        [JsonProperty("messageType")]
        public MessageType MessageType { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is Response))
            {
                return false;
            }

            Response other = (Response)obj;
            return (
                EqualityUtils.CheckEquals(DeviceAddress, other.DeviceAddress) &&
                EqualityUtils.CheckEquals(AxisNumber, other.AxisNumber) &&
                EqualityUtils.CheckEquals(ReplyFlag, other.ReplyFlag) &&
                EqualityUtils.CheckEquals(Status, other.Status) &&
                EqualityUtils.CheckEquals(WarningFlag, other.WarningFlag) &&
                EqualityUtils.CheckEquals(Data, other.Data) &&
                EqualityUtils.CheckEquals(MessageType, other.MessageType)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DeviceAddress));
            hash.Add(EqualityUtils.GenerateHashCode(AxisNumber));
            hash.Add(EqualityUtils.GenerateHashCode(ReplyFlag));
            hash.Add(EqualityUtils.GenerateHashCode(Status));
            hash.Add(EqualityUtils.GenerateHashCode(WarningFlag));
            hash.Add(EqualityUtils.GenerateHashCode(Data));
            hash.Add(EqualityUtils.GenerateHashCode(MessageType));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => Response.ToByteArray(this);

        internal static Response FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<Response>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(Response instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
