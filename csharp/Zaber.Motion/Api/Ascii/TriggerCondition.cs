/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Comparison operator for trigger condition.
    /// </summary>
    public enum TriggerCondition
    {
        /// <summary>EQ.</summary>
        EQ = 0,

        /// <summary>NE.</summary>
        NE = 1,

        /// <summary>GT.</summary>
        GT = 2,

        /// <summary>GE.</summary>
        GE = 3,

        /// <summary>LT.</summary>
        LT = 4,

        /// <summary>LE.</summary>
        LE = 5,

    }
}
