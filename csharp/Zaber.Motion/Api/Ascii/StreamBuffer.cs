﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents a stream buffer with this number on a device.
    /// A stream buffer is a place to store a queue of stream actions.
    /// </summary>
    public class StreamBuffer
    {
        internal StreamBuffer(Device device, int bufferId)
        {
            Device = device;
            BufferId = bufferId;
        }


        /// <summary>
        /// The Device this buffer exists on.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The number identifying the buffer on the device.
        /// </summary>
        public int BufferId { get; private set; }


        /// <summary>
        /// Get the buffer contents as an array of strings.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A string array containing all the stream commands stored in the buffer.</returns>
        public async Task<string[]> GetContentAsync()
        {
            var request = new Requests.StreamBufferGetContentRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                BufferId = BufferId,
            };

            var response = await Gateway.CallAsync("device/stream_buffer_get_content", request, Requests.StreamBufferGetContentResponse.FromByteArray).ConfigureAwait(false);
            return response.BufferLines;
        }


        /// <summary>
        /// Get the buffer contents as an array of strings.
        /// </summary>
        /// <returns>A string array containing all the stream commands stored in the buffer.</returns>
        public string[] GetContent()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetContentAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Erase the contents of the buffer.
        /// This method fails if there is a stream writing to the buffer.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EraseAsync()
        {
            var request = new Requests.StreamBufferEraseRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                BufferId = BufferId,
            };

            await Gateway.CallAsync("device/stream_buffer_erase", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Erase the contents of the buffer.
        /// This method fails if there is a stream writing to the buffer.
        /// </summary>
        public void Erase()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EraseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
