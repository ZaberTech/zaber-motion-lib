﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents all axes of motion associated with a device.
    /// </summary>
    public class AllAxes
    {
        internal AllAxes(Device device)
        {
            Device = device;
        }


        /// <summary>
        /// Device that controls this axis.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Homes all axes. Axes return to their homing positions.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HomeAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceHomeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/home", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Homes all axes. Axes return to their homing positions.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Home(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops ongoing axes movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceStopRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops ongoing axes movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Stop(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until all axes of device stop moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync(bool throwErrorOnFault = true)
        {
            var request = new Requests.DeviceWaitUntilIdleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
                ThrowErrorOnFault = throwErrorOnFault,
            };

            await Gateway.CallAsync("device/wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until all axes of device stop moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        public void WaitUntilIdle(bool throwErrorOnFault = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync(throwErrorOnFault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Parks the device in anticipation of turning the power off.
        /// It can later be powered on, unparked, and moved without first having to home it.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ParkAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            await Gateway.CallAsync("device/park", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Parks the device in anticipation of turning the power off.
        /// It can later be powered on, unparked, and moved without first having to home it.
        /// </summary>
        public void Park()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ParkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Unparks the device. The device will now be able to move.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UnparkAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            await Gateway.CallAsync("device/unpark", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Unparks the device. The device will now be able to move.
        /// </summary>
        public void Unpark()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UnparkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether any axis is executing a motion command.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if any axis is currently executing a motion command.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            var response = await Gateway.CallAsync("device/is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether any axis is executing a motion command.
        /// </summary>
        /// <returns>True if any axis is currently executing a motion command.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether all axes have position reference and were homed.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if all axes have position reference and were homed.</returns>
        public async Task<bool> IsHomedAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            var response = await Gateway.CallAsync("device/is_homed", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether all axes have position reference and were homed.
        /// </summary>
        /// <returns>True if all axes have position reference and were homed.</returns>
        public bool IsHomed()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsHomedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disables all axes drivers, which prevents current from being sent to the motor or load.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DriverDisableAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            await Gateway.CallAsync("device/driver_disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables all axes drivers, which prevents current from being sent to the motor or load.
        /// </summary>
        public void DriverDisable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DriverDisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
        /// If the driver is already enabled, the driver remains enabled.
        /// </summary>
        /// <param name="timeout">Timeout in seconds. Specify 0 to attempt to enable the driver once.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DriverEnableAsync(double timeout = 10)
        {
            var request = new Requests.DriverEnableRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
                Timeout = timeout,
            };

            await Gateway.CallAsync("device/driver_enable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
        /// If the driver is already enabled, the driver remains enabled.
        /// </summary>
        /// <param name="timeout">Timeout in seconds. Specify 0 to attempt to enable the driver once.</param>
        public void DriverEnable(double timeout = 10)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DriverEnableAsync(timeout);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the axes.
        /// </summary>
        /// <returns>A string that represents the axes.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 0,
            };

            var response = Gateway.CallSync("device/all_axes_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
