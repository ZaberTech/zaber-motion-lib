﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to device triggers.
    /// Please note that the Triggers API is currently an experimental feature.
    /// Requires at least Firmware 7.06.
    /// </summary>
    public class Triggers
    {
        internal Triggers(Device device)
        {
            Device = device;
        }


        /// <summary>
        /// Device that these triggers belong to.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Get the number of triggers for this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of triggers for this device.</returns>
        public async Task<int> GetNumberOfTriggersAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "trigger.numtriggers",
            };

            var response = await Gateway.CallAsync("triggers/get_setting", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the number of triggers for this device.
        /// </summary>
        /// <returns>Number of triggers for this device.</returns>
        public int GetNumberOfTriggers()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberOfTriggersAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get the number of actions for each trigger for this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of actions for each trigger for this device.</returns>
        public async Task<int> GetNumberOfActionsAsync()
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Setting = "trigger.numactions",
            };

            var response = await Gateway.CallAsync("triggers/get_setting", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Get the number of actions for each trigger for this device.
        /// </summary>
        /// <returns>Number of actions for each trigger for this device.</returns>
        public int GetNumberOfActions()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberOfActionsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Get a specific trigger for this device.
        /// </summary>
        /// <param name="triggerNumber">The number of the trigger to control. Trigger numbers start at 1.</param>
        /// <returns>Trigger instance.</returns>
        public Trigger GetTrigger(int triggerNumber)
        {
            if (triggerNumber <= 0)
            {
                throw new ArgumentException("Invalid value; triggers are numbered from 1.");
            }

            return new Trigger(Device, triggerNumber);
        }


        /// <summary>
        /// Get the state for every trigger for this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Complete state for every trigger.</returns>
        public async Task<TriggerState[]> GetTriggerStatesAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("triggers/get_trigger_states", request, Requests.TriggerStates.FromByteArray).ConfigureAwait(false);
            return response.States;
        }


        /// <summary>
        /// Get the state for every trigger for this device.
        /// </summary>
        /// <returns>Complete state for every trigger.</returns>
        public TriggerState[] GetTriggerStates()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetTriggerStatesAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the enabled state for every trigger for this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Whether triggers are enabled and the number of times they will fire.</returns>
        public async Task<TriggerEnabledState[]> GetEnabledStatesAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("triggers/get_enabled_states", request, Requests.TriggerEnabledStates.FromByteArray).ConfigureAwait(false);
            return response.States;
        }


        /// <summary>
        /// Gets the enabled state for every trigger for this device.
        /// </summary>
        /// <returns>Whether triggers are enabled and the number of times they will fire.</returns>
        public TriggerEnabledState[] GetEnabledStates()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetEnabledStatesAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the labels for every trigger for this device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.</returns>
        public async Task<string[]> GetAllLabelsAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = await Gateway.CallAsync("triggers/get_all_labels", request, Requests.StringArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Gets the labels for every trigger for this device.
        /// </summary>
        /// <returns>The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.</returns>
        public string[] GetAllLabels()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAllLabelsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
