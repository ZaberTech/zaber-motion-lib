/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The label of an IO port.
    /// </summary>
    public class IoPortLabel : IMessage
    {
        /// <summary>
        /// The type of the port.
        /// </summary>
        [JsonProperty("portType")]
        public IoPortType PortType { get; set; }

        /// <summary>
        /// The number of the port.
        /// </summary>
        [JsonProperty("channelNumber")]
        public int ChannelNumber { get; set; }

        /// <summary>
        /// The label of the port.
        /// </summary>
        [JsonProperty("label")]
        public string Label { get; set; } = string.Empty;

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is IoPortLabel))
            {
                return false;
            }

            IoPortLabel other = (IoPortLabel)obj;
            return (
                EqualityUtils.CheckEquals(PortType, other.PortType) &&
                EqualityUtils.CheckEquals(ChannelNumber, other.ChannelNumber) &&
                EqualityUtils.CheckEquals(Label, other.Label)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(PortType));
            hash.Add(EqualityUtils.GenerateHashCode(ChannelNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Label));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => IoPortLabel.ToByteArray(this);

        internal static IoPortLabel FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<IoPortLabel>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(IoPortLabel instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
