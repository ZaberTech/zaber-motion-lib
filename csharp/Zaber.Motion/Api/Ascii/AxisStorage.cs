﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to axis storage.
    /// Requires at least Firmware 7.30.
    /// </summary>
    public class AxisStorage
    {
        internal AxisStorage(Axis axis)
        {
            _axis = axis;
        }


        /// <summary>
        /// Sets the axis value stored at the provided key.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="encode">Whether the stored value should be base64 encoded before being stored.
        /// This makes the string unreadable to humans using the ASCII protocol,
        /// however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetStringAsync(string key, string value, bool encode = false)
        {
            var request = new Requests.DeviceSetStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
                Value = value,
                Encode = encode,
            };

            await Gateway.CallAsync("device/set_storage", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the axis value stored at the provided key.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="encode">Whether the stored value should be base64 encoded before being stored.
        /// This makes the string unreadable to humans using the ASCII protocol,
        /// however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.</param>
        public void SetString(string key, string value, bool encode = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStringAsync(key, value, encode);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the axis value stored with the provided key.
        /// </summary>
        /// <param name="key">Key to read the value of.</param>
        /// <param name="decode">Whether the stored value should be decoded.
        /// Only use this when reading values set by storage.set with "encode" true.</param>
        /// <returns>A Task that can be awaited to get the result: Stored value.</returns>
        public async Task<string> GetStringAsync(string key, bool decode = false)
        {
            var request = new Requests.DeviceGetStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
                Decode = decode,
            };

            var response = await Gateway.CallAsync("device/get_storage", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the axis value stored with the provided key.
        /// </summary>
        /// <param name="key">Key to read the value of.</param>
        /// <param name="decode">Whether the stored value should be decoded.
        /// Only use this when reading values set by storage.set with "encode" true.</param>
        /// <returns>Stored value.</returns>
        public string GetString(string key, bool decode = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStringAsync(key, decode);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the value at the provided key to the provided number.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetNumberAsync(string key, double value)
        {
            var request = new Requests.DeviceSetStorageNumberRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
                Value = value,
            };

            await Gateway.CallAsync("device/set_storage_number", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the value at the provided key to the provided number.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        public void SetNumber(string key, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetNumberAsync(key, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the value at the provided key interpreted as a number.
        /// </summary>
        /// <param name="key">Key to get the value at.</param>
        /// <returns>A Task that can be awaited to get the result: Stored value.</returns>
        public async Task<double> GetNumberAsync(string key)
        {
            var request = new Requests.DeviceStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
            };

            var response = await Gateway.CallAsync("device/get_storage_number", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the value at the provided key interpreted as a number.
        /// </summary>
        /// <param name="key">Key to get the value at.</param>
        /// <returns>Stored value.</returns>
        public double GetNumber(string key)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberAsync(key);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the value at the provided key to the provided boolean.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetBoolAsync(string key, bool value)
        {
            var request = new Requests.DeviceSetStorageBoolRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
                Value = value,
            };

            await Gateway.CallAsync("device/set_storage_bool", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the value at the provided key to the provided boolean.
        /// </summary>
        /// <param name="key">Key to set the value at.</param>
        /// <param name="value">Value to set.</param>
        public void SetBool(string key, bool value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetBoolAsync(key, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the value at the provided key interpreted as a boolean.
        /// </summary>
        /// <param name="key">Key to get the value at.</param>
        /// <returns>A Task that can be awaited to get the result: Stored value.</returns>
        public async Task<bool> GetBoolAsync(string key)
        {
            var request = new Requests.DeviceStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
            };

            var response = await Gateway.CallAsync("device/get_storage_bool", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the value at the provided key interpreted as a boolean.
        /// </summary>
        /// <param name="key">Key to get the value at.</param>
        /// <returns>Stored value.</returns>
        public bool GetBool(string key)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetBoolAsync(key);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Erases the axis value stored at the provided key.
        /// </summary>
        /// <param name="key">Key to erase.</param>
        /// <returns>A Task that can be awaited to get the result: A boolean indicating if the key existed.</returns>
        public async Task<bool> EraseKeyAsync(string key)
        {
            var request = new Requests.DeviceStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
            };

            var response = await Gateway.CallAsync("device/erase_storage", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Erases the axis value stored at the provided key.
        /// </summary>
        /// <param name="key">Key to erase.</param>
        /// <returns>A boolean indicating if the key existed.</returns>
        public bool EraseKey(string key)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EraseKeyAsync(key);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Lists the axis storage keys matching a given prefix.
        /// Omit the prefix to list all the keys.
        /// </summary>
        /// <param name="prefix">Optional key prefix.</param>
        /// <returns>A Task that can be awaited to get the result: Storage keys matching the given prefix.</returns>
        public async Task<string[]> ListKeysAsync(string? prefix = null)
        {
            var request = new Requests.DeviceStorageListKeysRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Prefix = prefix,
            };

            var response = await Gateway.CallAsync("device/storage_list_keys", request, Requests.StringArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Lists the axis storage keys matching a given prefix.
        /// Omit the prefix to list all the keys.
        /// </summary>
        /// <param name="prefix">Optional key prefix.</param>
        /// <returns>Storage keys matching the given prefix.</returns>
        public string[] ListKeys(string? prefix = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ListKeysAsync(prefix);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Determines whether a given key exists in axis storage.
        /// </summary>
        /// <param name="key">Key which existence to determine.</param>
        /// <returns>A Task that can be awaited to get the result: True indicating that the key exists, false otherwise.</returns>
        public async Task<bool> KeyExistsAsync(string key)
        {
            var request = new Requests.DeviceStorageRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Key = key,
            };

            var response = await Gateway.CallAsync("device/storage_key_exists", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Determines whether a given key exists in axis storage.
        /// </summary>
        /// <param name="key">Key which existence to determine.</param>
        /// <returns>True indicating that the key exists, false otherwise.</returns>
        public bool KeyExists(string key)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = KeyExistsAsync(key);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Axis _axis;


    }
}
