﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to device streams.
    /// Requires at least Firmware 7.05.
    /// </summary>
    public class Streams
    {
        internal Streams(Device device)
        {
            Device = device;
        }


        /// <summary>
        /// Device that these streams belong to.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Gets a Stream class instance which allows you to control a particular stream on the device.
        /// </summary>
        /// <param name="streamId">The ID of the stream to control. Stream IDs start at one.</param>
        /// <returns>Stream instance.</returns>
        public Stream GetStream(int streamId)
        {
            if (streamId <= 0)
            {
                throw new ArgumentException("Invalid value; streams are numbered from 1.");
            }

            return new Stream(Device, streamId);
        }


        /// <summary>
        /// Gets a StreamBuffer class instance which is a handle for a stream buffer on the device.
        /// </summary>
        /// <param name="streamBufferId">The ID of the stream buffer to control. Stream buffer IDs start at one.</param>
        /// <returns>StreamBuffer instance.</returns>
        public StreamBuffer GetBuffer(int streamBufferId)
        {
            if (streamBufferId <= 0)
            {
                throw new ArgumentException("Invalid value; stream buffers are numbered from 1.");
            }

            return new StreamBuffer(Device, streamBufferId);
        }


        /// <summary>
        /// Get a list of buffer IDs that are currently in use.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: List of buffer IDs.</returns>
        public async Task<int[]> ListBufferIdsAsync()
        {
            var request = new Requests.StreamBufferList()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Pvt = false,
            };

            var response = await Gateway.CallAsync("device/stream_buffer_list", request, Requests.IntArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Get a list of buffer IDs that are currently in use.
        /// </summary>
        /// <returns>List of buffer IDs.</returns>
        public int[] ListBufferIds()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ListBufferIdsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
