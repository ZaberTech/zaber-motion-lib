/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Mode of a stream.
    /// </summary>
    public enum StreamMode
    {
        /// <summary>Disabled.</summary>
        Disabled = 0,

        /// <summary>Store.</summary>
        Store = 1,

        /// <summary>StoreArbitraryAxes.</summary>
        StoreArbitraryAxes = 2,

        /// <summary>Live.</summary>
        Live = 3,

    }
}
