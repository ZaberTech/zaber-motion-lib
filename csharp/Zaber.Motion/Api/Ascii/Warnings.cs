﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Collections.Generic;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class used to check and reset warnings and faults on device or axis.
    /// </summary>
    public class Warnings
    {
        internal Warnings(Device device, int axisNumber)
        {
            _device = device;
            _axisNumber = axisNumber;
        }


        /// <summary>
        /// Returns current warnings and faults on axis or device.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.</returns>
        public async Task<ISet<string>> GetFlagsAsync()
        {
            var request = new Requests.DeviceGetWarningsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Axis = _axisNumber,
                Clear = false,
            };

            var response = await Gateway.CallAsync("device/get_warnings", request, Requests.DeviceGetWarningsResponse.FromByteArray).ConfigureAwait(false);
            return new HashSet<string>(response.Flags);
        }


        /// <summary>
        /// Returns current warnings and faults on axis or device.
        /// </summary>
        /// <returns>Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.</returns>
        public ISet<string> GetFlags()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetFlagsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Clears (acknowledges) current warnings and faults on axis or device and returns them.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.</returns>
        public async Task<ISet<string>> ClearFlagsAsync()
        {
            var request = new Requests.DeviceGetWarningsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Axis = _axisNumber,
                Clear = true,
            };

            var response = await Gateway.CallAsync("device/get_warnings", request, Requests.DeviceGetWarningsResponse.FromByteArray).ConfigureAwait(false);
            return new HashSet<string>(response.Flags);
        }


        /// <summary>
        /// Clears (acknowledges) current warnings and faults on axis or device and returns them.
        /// </summary>
        /// <returns>Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.</returns>
        public ISet<string> ClearFlags()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ClearFlagsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits for the specified flags to clear.
        /// Use for warnings flags that clear on their own.
        /// Does not clear clearable warnings flags.
        /// Throws TimeoutException if the flags don't clear in the specified time.
        /// </summary>
        /// <param name="timeout">For how long to wait in milliseconds for the flags to clear.</param>
        /// <param name="warningFlags">The specific warning flags for which to wait to clear.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitToClearAsync(double timeout, params string[] warningFlags)
        {
            var request = new Requests.WaitToClearWarningsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Axis = _axisNumber,
                Timeout = timeout,
                WarningFlags = warningFlags,
            };

            await Gateway.CallAsync("device/wait_to_clear_warnings", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits for the specified flags to clear.
        /// Use for warnings flags that clear on their own.
        /// Does not clear clearable warnings flags.
        /// Throws TimeoutException if the flags don't clear in the specified time.
        /// </summary>
        /// <param name="timeout">For how long to wait in milliseconds for the flags to clear.</param>
        /// <param name="warningFlags">The specific warning flags for which to wait to clear.</param>
        public void WaitToClear(double timeout, params string[] warningFlags)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitToClearAsync(timeout, warningFlags);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Device _device;


        private int _axisNumber;


    }
}
