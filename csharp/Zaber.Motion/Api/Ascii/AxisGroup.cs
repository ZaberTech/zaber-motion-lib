﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Groups multiple axes across devices into a single group to allow for simultaneous movement.
    /// Note that the movement is not coordinated and trajectory is inconsistent and not repeatable between calls.
    /// Make sure that any possible trajectory is clear of potential obstacles.
    /// The movement methods return after all the axes finish the movement successfully
    /// or throw an error as soon as possible.
    /// </summary>
    public class AxisGroup
    {
#pragma warning disable SA1611
        /// <summary>
        /// Initializes the group with the axes to be controlled.
        /// </summary>
        public AxisGroup(Axis[] axes)
        {
            Axes = axes.ToArray();
        }
#pragma warning restore SA1611


        /// <summary>
        /// Axes of the group.
        /// </summary>
        public Axis[] Axes { get; private set; }


        /// <summary>
        /// Homes the axes.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HomeAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            await Gateway.CallAsync("axes/home", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Homes the axes.
        /// </summary>
        public void Home()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops the axes.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            await Gateway.CallAsync("axes/stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops the axes.
        /// </summary>
        public void Stop()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axes to absolute position.
        /// </summary>
        /// <param name="position">Position.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveAbsoluteAsync(params Measurement[] position)
        {
            var request = new Requests.AxesMoveRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
                Position = position,
            };

            await Gateway.CallAsync("axes/move_absolute", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axes to absolute position.
        /// </summary>
        /// <param name="position">Position.</param>
        public void MoveAbsolute(params Measurement[] position)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveAbsoluteAsync(position);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move axes to position relative to the current position.
        /// </summary>
        /// <param name="position">Position.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveRelativeAsync(params Measurement[] position)
        {
            var request = new Requests.AxesMoveRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
                Position = position,
            };

            await Gateway.CallAsync("axes/move_relative", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Move axes to position relative to the current position.
        /// </summary>
        /// <param name="position">Position.</param>
        public void MoveRelative(params Measurement[] position)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveRelativeAsync(position);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves axes to the minimum position as specified by limit.min.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMinAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            await Gateway.CallAsync("axes/move_min", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves axes to the minimum position as specified by limit.min.
        /// </summary>
        public void MoveMin()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMinAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves axes to the maximum position as specified by limit.max.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMaxAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            await Gateway.CallAsync("axes/move_max", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves axes to the maximum position as specified by limit.max.
        /// </summary>
        public void MoveMax()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMaxAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until all the axes stop moving.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            await Gateway.CallAsync("axes/wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until all the axes stop moving.
        /// </summary>
        public void WaitUntilIdle()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether any of the axes is executing a motion command.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if any of the axes is currently executing a motion command. False otherwise.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            var response = await Gateway.CallAsync("axes/is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether any of the axes is executing a motion command.
        /// </summary>
        /// <returns>True if any of the axes is currently executing a motion command. False otherwise.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether all the axes are homed.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if all the axes are homed. False otherwise.</returns>
        public async Task<bool> IsHomedAsync()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            var response = await Gateway.CallAsync("axes/is_homed", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether all the axes are homed.
        /// </summary>
        /// <returns>True if all the axes are homed. False otherwise.</returns>
        public bool IsHomed()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsHomedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current axes position.
        /// The positions are requested sequentially.
        /// The result position may not be accurate if the axes are moving.
        /// </summary>
        /// <param name="unit">Units of position. You can specify units once or for each axis separately.</param>
        /// <returns>A Task that can be awaited to get the result: Axes position.</returns>
        public async Task<double[]> GetPositionAsync(params Units[] unit)
        {
            var request = new Requests.AxesGetSettingRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
                Setting = "pos",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("axes/get_setting", request, Requests.DoubleArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Returns current axes position.
        /// The positions are requested sequentially.
        /// The result position may not be accurate if the axes are moving.
        /// </summary>
        /// <param name="unit">Units of position. You can specify units once or for each axis separately.</param>
        /// <returns>Axes position.</returns>
        public double[] GetPosition(params Units[] unit)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetPositionAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the axes.
        /// </summary>
        /// <returns>A string that represents the axes.</returns>
        public override string ToString()
        {
            var request = new Requests.AxesEmptyRequest()
            {
                Interfaces = Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray(),
                Devices = Axes.Select(axis => axis.Device.DeviceAddress).ToArray(),
                Axes = Axes.Select(axis => axis.AxisNumber).ToArray(),
            };

            var response = Gateway.CallSync("axes/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
