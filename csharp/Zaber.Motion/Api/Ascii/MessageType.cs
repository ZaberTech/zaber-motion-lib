/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Denotes type of the response message.
    /// For more information refer to:
    /// [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format).
    /// </summary>
    public enum MessageType
    {
        /// <summary>Reply.</summary>
        Reply = 0,

        /// <summary>Info.</summary>
        Info = 1,

        /// <summary>Alert.</summary>
        Alert = 2,

    }
}
