/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The tuning of this axis represented by PID parameters.
    /// </summary>
    public class PidTuning : IMessage
    {
        /// <summary>
        /// The tuning algorithm used to tune this axis.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; } = string.Empty;

        /// <summary>
        /// The version of the tuning algorithm used to tune this axis.
        /// </summary>
        [JsonProperty("version")]
        public int Version { get; set; }

        /// <summary>
        /// The positional tuning argument.
        /// </summary>
        [JsonProperty("p")]
        public double P { get; set; }

        /// <summary>
        /// The integral tuning argument.
        /// </summary>
        [JsonProperty("i")]
        public double I { get; set; }

        /// <summary>
        /// The derivative tuning argument.
        /// </summary>
        [JsonProperty("d")]
        public double D { get; set; }

        /// <summary>
        /// The frequency cutoff for the tuning.
        /// </summary>
        [JsonProperty("fc")]
        public double Fc { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is PidTuning))
            {
                return false;
            }

            PidTuning other = (PidTuning)obj;
            return (
                EqualityUtils.CheckEquals(Type, other.Type) &&
                EqualityUtils.CheckEquals(Version, other.Version) &&
                EqualityUtils.CheckEquals(P, other.P) &&
                EqualityUtils.CheckEquals(I, other.I) &&
                EqualityUtils.CheckEquals(D, other.D) &&
                EqualityUtils.CheckEquals(Fc, other.Fc)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Type));
            hash.Add(EqualityUtils.GenerateHashCode(Version));
            hash.Add(EqualityUtils.GenerateHashCode(P));
            hash.Add(EqualityUtils.GenerateHashCode(I));
            hash.Add(EqualityUtils.GenerateHashCode(D));
            hash.Add(EqualityUtils.GenerateHashCode(Fc));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => PidTuning.ToByteArray(this);

        internal static PidTuning FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<PidTuning>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(PidTuning instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
