﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Contains a block of contiguous recorded data for one channel of the device's oscilloscope.
    /// </summary>
    public class OscilloscopeData
    {
        internal OscilloscopeData(int dataId)
        {
            DataId = dataId;
        }


        /// <summary>
        /// Unique ID for this block of recorded data.
        /// </summary>
        public int DataId { get; private set; }


        /// <summary>
        /// Indicates whether the data came from a setting or an I/O pin.
        /// </summary>
        public OscilloscopeDataSource DataSource
        {
            get { return RetrieveProperties().DataSource; }
        }


        /// <summary>
        /// The name of the recorded setting.
        /// </summary>
        public string Setting
        {
            get { return RetrieveProperties().Setting; }
        }


        /// <summary>
        /// The number of the axis the data was recorded from, or 0 for the controller.
        /// </summary>
        public int AxisNumber
        {
            get { return RetrieveProperties().AxisNumber; }
        }


        /// <summary>
        /// Which kind of I/O port data was recorded from.
        /// </summary>
        public IoPortType IoType
        {
            get { return RetrieveProperties().IoType; }
        }


        /// <summary>
        /// Which I/O pin within the port was recorded.
        /// </summary>
        public int IoChannel
        {
            get { return RetrieveProperties().IoChannel; }
        }


        /// <summary>
        /// Get the sample interval that this data was recorded with.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the timebase in.</param>
        /// <returns>The timebase setting at the time the data was recorded.</returns>
        public double GetTimebase(Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetRequest()
            {
                DataId = DataId,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_timebase", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Get the sampling frequency that this data was recorded with.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the frequency in.</param>
        /// <returns>The frequency (inverse of the timebase setting) at the time the data was recorded.</returns>
        public double GetFrequency(Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetRequest()
            {
                DataId = DataId,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_frequency", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Get the user-specified time period between receipt of the start command and the first data point.
        /// Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the delay in.</param>
        /// <returns>The delay setting at the time the data was recorded.</returns>
        public double GetDelay(Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetRequest()
            {
                DataId = DataId,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_delay", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Calculate the time a sample was recorded, relative to when the recording was triggered.
        /// </summary>
        /// <param name="index">0-based index of the sample to calculate the time of.</param>
        /// <param name="unit">Unit of measure to represent the calculated time in.</param>
        /// <returns>The calculated time offset of the data sample at the given index.</returns>
        public double GetSampleTime(int index, Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetSampleTimeRequest()
            {
                DataId = DataId,
                Index = index,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_sample_time", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Calculate the time for all samples, relative to when the recording was triggered.
        /// </summary>
        /// <param name="unit">Unit of measure to represent the calculated time in.</param>
        /// <returns>The calculated time offsets of all data samples.</returns>
        public double[] GetSampleTimes(Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetSampleTimeRequest()
            {
                DataId = DataId,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_sample_times", request, Requests.DoubleArrayResponse.FromByteArray);
            return response.Values;
        }


        /// <summary>
        /// Get the recorded data as an array of doubles, with optional unit conversion.
        /// Note that not all quantities can be unit converted.
        /// For example, digital I/O channels and pure numbers such as device mode settings have no units.
        /// </summary>
        /// <param name="unit">Unit of measure to convert the data to.</param>
        /// <returns>The recorded data for one oscilloscope channel, converted to the units specified.</returns>
        public double[] GetData(Units unit = Units.Native)
        {
            var request = new Requests.OscilloscopeDataGetRequest()
            {
                DataId = DataId,
                Unit = unit,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_samples", request, Requests.OscilloscopeDataGetSamplesResponse.FromByteArray);
            return response.Data;
        }


        /// <summary>
        /// Releases native resources of an oscilloscope data buffer.
        /// </summary>
        /// <param name="dataId">The ID of the data buffer to delete.</param>
        private static void Free(int dataId)
        {
            var request = new Requests.OscilloscopeDataIdentifier()
            {
                DataId = dataId,
            };

            Gateway.CallSync("oscilloscopedata/free", request);
        }


        /// <summary>
        /// Returns recording properties.
        /// </summary>
        /// <returns>Capture properties.</returns>
        private OscilloscopeCaptureProperties RetrieveProperties()
        {
            var request = new Requests.OscilloscopeDataIdentifier()
            {
                DataId = DataId,
            };

            var response = Gateway.CallSync("oscilloscopedata/get_properties", request, OscilloscopeCaptureProperties.FromByteArray);
            return response;
        }



        /// <summary>
        /// Finalizes an instance of the <see cref="OscilloscopeData"/> class.
        /// </summary>
        ~OscilloscopeData()
        {
            OscilloscopeData.Free(DataId);
        }
    }
}
