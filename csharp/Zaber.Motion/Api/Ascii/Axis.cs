﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents an axis of motion associated with a device.
    /// </summary>
    public class Axis
    {
        internal Axis(Device device, int axisNumber)
        {
            Device = device;
            AxisNumber = axisNumber;
            Settings = new AxisSettings(this);
            Storage = new AxisStorage(this);
            Warnings = new Warnings(device, axisNumber);
        }


        /// <summary>
        /// Device that controls this axis.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The axis number identifies the axis on the device.
        /// The first axis has the number one.
        /// </summary>
        public int AxisNumber { get; private set; }


        /// <summary>
        /// Settings and properties of this axis.
        /// </summary>
        public AxisSettings Settings { get; private set; }


        /// <summary>
        /// Key-value storage of this axis.
        /// Requires at least Firmware 7.30.
        /// </summary>
        public AxisStorage Storage { get; private set; }


        /// <summary>
        /// Warnings and faults of this axis.
        /// </summary>
        public Warnings Warnings { get; private set; }


        /// <summary>
        /// Identity of the axis.
        /// </summary>
        public AxisIdentity Identity
        {
            get { return RetrieveIdentity(); }
        }


        /// <summary>
        /// Unique ID of the peripheral hardware.
        /// </summary>
        public int PeripheralId
        {
            get { return Identity.PeripheralId; }
        }


        /// <summary>
        /// Name of the peripheral.
        /// </summary>
        public string PeripheralName
        {
            get { return Identity.PeripheralName; }
        }


        /// <summary>
        /// Serial number of the peripheral, or 0 when not applicable.
        /// </summary>
        public long PeripheralSerialNumber
        {
            get { return Identity.PeripheralSerialNumber; }
        }


        /// <summary>
        /// Indicates whether the axis is a peripheral or part of an integrated device.
        /// </summary>
        public bool IsPeripheral
        {
            get { return Identity.IsPeripheral; }
        }


        /// <summary>
        /// Determines the type of an axis and units it accepts.
        /// </summary>
        public AxisType AxisType
        {
            get { return Identity.AxisType; }
        }


        /// <summary>
        /// User-assigned label of the peripheral.
        /// </summary>
        public string Label
        {
            get { return RetrieveLabel(); }
        }


        /// <summary>
        /// Homes axis. Axis returns to its homing position.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task HomeAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceHomeRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/home", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Homes axis. Axis returns to its homing position.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Home(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops ongoing axis movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceStopRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops ongoing axis movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void Stop(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Parks the axis in anticipation of turning the power off.
        /// It can later be powered on, unparked, and moved without first having to home it.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ParkAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            await Gateway.CallAsync("device/park", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Parks the axis in anticipation of turning the power off.
        /// It can later be powered on, unparked, and moved without first having to home it.
        /// </summary>
        public void Park()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ParkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Unparks axis. Axis will now be able to move.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UnparkAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            await Gateway.CallAsync("device/unpark", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Unparks axis. Axis will now be able to move.
        /// </summary>
        public void Unpark()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UnparkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether the axis is parked or not.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the axis is currently parked. False otherwise.</returns>
        public async Task<bool> IsParkedAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/is_parked", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether the axis is parked or not.
        /// </summary>
        /// <returns>True if the axis is currently parked. False otherwise.</returns>
        public bool IsParked()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsParkedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until axis stops moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync(bool throwErrorOnFault = true)
        {
            var request = new Requests.DeviceWaitUntilIdleRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                ThrowErrorOnFault = throwErrorOnFault,
            };

            await Gateway.CallAsync("device/wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until axis stops moving.
        /// </summary>
        /// <param name="throwErrorOnFault">Determines whether to throw error when fault is observed.</param>
        public void WaitUntilIdle(bool throwErrorOnFault = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync(throwErrorOnFault);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether the axis is executing a motion command.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the axis is currently executing a motion command.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether the axis is executing a motion command.
        /// </summary>
        /// <returns>True if the axis is currently executing a motion command.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether the axis has position reference and was homed.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the axis has position reference and was homed.</returns>
        public async Task<bool> IsHomedAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/is_homed", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether the axis has position reference and was homed.
        /// </summary>
        /// <returns>True if the axis has position reference and was homed.</returns>
        public bool IsHomed()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsHomedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move axis to absolute position.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveAbsoluteAsync(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Abs,
                Arg = position,
                Unit = unit,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Move axis to absolute position.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveAbsolute(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axis to the maximum position as specified by limit.max.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMaxAsync(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Max,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axis to the maximum position as specified by limit.max.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveMax(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axis to the minimum position as specified by limit.min.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveMinAsync(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Min,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axis to the minimum position as specified by limit.min.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveMin(bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move axis to position relative to current position.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveRelativeAsync(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Rel,
                Arg = position,
                Unit = unit,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Move axis to position relative to current position.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Units of position.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveRelative(double position, Units unit = Units.Native, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Begins to move axis at specified speed.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveVelocityAsync(double velocity, Units unit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Vel,
                Arg = velocity,
                Unit = unit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Begins to move axis at specified speed.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveVelocity(double velocity, Units unit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveVelocityAsync(velocity, unit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current axis position.
        /// </summary>
        /// <param name="unit">Units of position.</param>
        /// <returns>A Task that can be awaited to get the result: Axis position.</returns>
        public async Task<double> GetPositionAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Setting = "pos",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns current axis position.
        /// </summary>
        /// <param name="unit">Units of position.</param>
        /// <returns>Axis position.</returns>
        public double GetPosition(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetPositionAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets number of index positions of the axis.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of index positions.</returns>
        public async Task<int> GetNumberOfIndexPositionsAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/get_index_count", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets number of index positions of the axis.
        /// </summary>
        /// <returns>Number of index positions.</returns>
        public int GetNumberOfIndexPositions()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberOfIndexPositionsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current axis index position.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Index position starting from 1 or 0 if the position is not an index position.</returns>
        public async Task<int> GetIndexPositionAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/get_index_position", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns current axis index position.
        /// </summary>
        /// <returns>Index position starting from 1 or 0 if the position is not an index position.</returns>
        public int GetIndexPosition()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetIndexPositionAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axis to index position.
        /// </summary>
        /// <param name="index">Index position. Index positions are numbered from 1.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveIndexAsync(int index, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Type = Requests.AxisMoveType.Index,
                ArgInt = index,
                WaitUntilIdle = waitUntilIdle,
                Velocity = velocity,
                VelocityUnit = velocityUnit,
                Acceleration = acceleration,
                AccelerationUnit = accelerationUnit,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axis to index position.
        /// </summary>
        /// <param name="index">Index position. Index positions are numbered from 1.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <param name="velocity">Movement velocity.
        /// Default value of 0 indicates that the maxspeed setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="velocityUnit">Units of velocity.</param>
        /// <param name="acceleration">Movement acceleration.
        /// Default value of 0 indicates that the accel setting is used instead.
        /// Requires at least Firmware 7.25.</param>
        /// <param name="accelerationUnit">Units of acceleration.</param>
        public void MoveIndex(int index, bool waitUntilIdle = true, double velocity = 0, Units velocityUnit = Units.Native, double acceleration = 0, Units accelerationUnit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveIndexAsync(index, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Response> GenericCommandAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command", request, Response.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A response to the command.</returns>
        public Response GenericCommand(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Response[]> GenericCommandMultiResponseAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command_multi_response", request, Requests.GenericCommandResponseCollection.FromByteArray).ConfigureAwait(false);
            return response.Responses;
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis and expect multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>All responses to the command.</returns>
        public Response[] GenericCommandMultiResponse(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(string command)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Command = command,
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        public void GenericCommandNoResponse(string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Formats parameters into a command and performs unit conversions.
        /// Parameters in the command template are denoted by a question mark.
        /// Command returned is only valid for this axis and this device.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="commandTemplate">Template of a command to prepare. Parameters are denoted by question marks.</param>
        /// <param name="parameters">Variable number of command parameters.</param>
        /// <returns>Command with converted parameters.</returns>
        public string PrepareCommand(string commandTemplate, params Measurement[] parameters)
        {
            var request = new Requests.PrepareCommandRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                CommandTemplate = commandTemplate,
                Parameters = parameters,
            };

            var response = Gateway.CallSync("device/prepare_command", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets the user-assigned peripheral label.
        /// The label is stored on the controller and recognized by other software.
        /// </summary>
        /// <param name="label">Label to set.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLabelAsync(string label)
        {
            var request = new Requests.DeviceSetStorageRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Value = label,
            };

            await Gateway.CallAsync("device/set_label", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the user-assigned peripheral label.
        /// The label is stored on the controller and recognized by other software.
        /// </summary>
        /// <param name="label">Label to set.</param>
        public void SetLabel(string label)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLabelAsync(label);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the axis.
        /// </summary>
        /// <returns>A string that represents the axis.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = Gateway.CallSync("device/axis_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current axis state that can be saved and reapplied.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A serialization of the current state of the axis.</returns>
        public async Task<string> GetStateAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = await Gateway.CallAsync("device/get_state", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current axis state that can be saved and reapplied.
        /// </summary>
        /// <returns>A serialization of the current state of the axis.</returns>
        public string GetState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Applies a saved state to this axis.
        /// </summary>
        /// <param name="state">The state object to apply to this axis.</param>
        /// <returns>A Task that can be awaited to get the result: Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public async Task<SetStateAxisResponse> SetStateAsync(string state)
        {
            var request = new Requests.SetStateRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                State = state,
            };

            var response = await Gateway.CallAsync("device/set_axis_state", request, SetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Applies a saved state to this axis.
        /// </summary>
        /// <param name="state">The state object to apply to this axis.</param>
        /// <returns>Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public SetStateAxisResponse SetState(string state)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStateAsync(state);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if a state can be applied to this axis.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>A Task that can be awaited to get the result: An explanation of why this state cannot be set to this axis.</returns>
        public async Task<string?> CanSetStateAsync(string state, FirmwareVersion? firmwareVersion = null)
        {
            var request = new Requests.CanSetStateRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                State = state,
                FirmwareVersion = firmwareVersion,
            };

            var response = await Gateway.CallAsync("device/can_set_axis_state", request, Requests.CanSetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response.Error;
        }


        /// <summary>
        /// Checks if a state can be applied to this axis.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>An explanation of why this state cannot be set to this axis.</returns>
        public string? CanSetState(string state, FirmwareVersion? firmwareVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CanSetStateAsync(state, firmwareVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disables the driver, which prevents current from being sent to the motor or load.
        /// If the driver is already disabled, the driver remains disabled.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DriverDisableAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            await Gateway.CallAsync("device/driver_disable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables the driver, which prevents current from being sent to the motor or load.
        /// If the driver is already disabled, the driver remains disabled.
        /// </summary>
        public void DriverDisable()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DriverDisableAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Attempts to enable the driver repeatedly for the specified timeout.
        /// If the driver is already enabled, the driver remains enabled.
        /// </summary>
        /// <param name="timeout">Timeout in seconds. Specify 0 to attempt to enable the driver once.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DriverEnableAsync(double timeout = 10)
        {
            var request = new Requests.DriverEnableRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Timeout = timeout,
            };

            await Gateway.CallAsync("device/driver_enable", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Attempts to enable the driver repeatedly for the specified timeout.
        /// If the driver is already enabled, the driver remains enabled.
        /// </summary>
        /// <param name="timeout">Timeout in seconds. Specify 0 to attempt to enable the driver once.</param>
        public void DriverEnable(double timeout = 10)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DriverEnableAsync(timeout);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Activates a peripheral on this axis.
        /// Removes all identity information for the device.
        /// Run the identify method on the device after activating to refresh the information.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ActivateAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            await Gateway.CallAsync("device/activate", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Activates a peripheral on this axis.
        /// Removes all identity information for the device.
        /// Run the identify method on the device after activating to refresh the information.
        /// </summary>
        public void Activate()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ActivateAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Restores all axis settings to their default values.
        /// Deletes all zaber axis storage keys.
        /// Disables lockstep if the axis is part of one. Unparks the axis.
        /// Preserves storage.
        /// The device needs to be identified again after the restore.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task RestoreAsync()
        {
            var request = new Requests.DeviceRestoreRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            await Gateway.CallAsync("device/restore", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Restores all axis settings to their default values.
        /// Deletes all zaber axis storage keys.
        /// Disables lockstep if the axis is part of one. Unparks the axis.
        /// Preserves storage.
        /// The device needs to be identified again after the restore.
        /// </summary>
        public void Restore()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = RestoreAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the axis in a sinusoidal trajectory.
        /// </summary>
        /// <param name="amplitude">Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).</param>
        /// <param name="amplitudeUnits">Units of position.</param>
        /// <param name="period">Period of the sinusoidal motion in milliseconds.</param>
        /// <param name="periodUnits">Units of time.</param>
        /// <param name="count">Number of sinusoidal cycles to complete.
        /// Must be a multiple of 0.5
        /// If count is not specified or set to 0, the axis will move indefinitely.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveSinAsync(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count = 0, bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceMoveSinRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                Amplitude = amplitude,
                AmplitudeUnits = amplitudeUnits,
                Period = period,
                PeriodUnits = periodUnits,
                Count = count,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/move_sin", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the axis in a sinusoidal trajectory.
        /// </summary>
        /// <param name="amplitude">Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).</param>
        /// <param name="amplitudeUnits">Units of position.</param>
        /// <param name="period">Period of the sinusoidal motion in milliseconds.</param>
        /// <param name="periodUnits">Units of time.</param>
        /// <param name="count">Number of sinusoidal cycles to complete.
        /// Must be a multiple of 0.5
        /// If count is not specified or set to 0, the axis will move indefinitely.</param>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished or just started.</param>
        public void MoveSin(double amplitude, Units amplitudeUnits, double period, Units periodUnits, double count = 0, bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops the axis at the end of the sinusoidal trajectory.
        /// If the sinusoidal motion was started with an integer-plus-half cycle count,
        /// the motion ends at the half-way point of the sinusoidal trajectory.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task MoveSinStopAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceStopRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
                WaitUntilIdle = waitUntilIdle,
            };

            await Gateway.CallAsync("device/move_sin_stop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops the axis at the end of the sinusoidal trajectory.
        /// If the sinusoidal motion was started with an integer-plus-half cycle count,
        /// the motion ends at the half-way point of the sinusoidal trajectory.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether function should return after the movement is finished.</param>
        public void MoveSinStop(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveSinStopAsync(waitUntilIdle);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the peripheral name.
        /// </summary>
        /// <returns>The label.</returns>
        private string RetrieveLabel()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = Gateway.CallSync("device/get_label", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns identity.
        /// </summary>
        /// <returns>Axis identity.</returns>
        private AxisIdentity RetrieveIdentity()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = AxisNumber,
            };

            var response = Gateway.CallSync("device/get_axis_identity", request, AxisIdentity.FromByteArray);
            return response;
        }


    }
}
