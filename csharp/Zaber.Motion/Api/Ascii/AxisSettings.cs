﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to various axis settings and properties.
    /// </summary>
    public class AxisSettings
    {
        internal AxisSettings(Axis axis)
        {
            _axis = axis;
        }


        /// <summary>
        /// Returns any axis setting or property.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to get the result: Setting value.</returns>
        public async Task<double> GetAsync(string setting, Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns any axis setting or property.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>Setting value.</returns>
        public double Get(string setting, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAsync(setting, unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets any axis setting.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAsync(string setting, double value, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets any axis setting.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        public void Set(string setting, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAsync(setting, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns any axis setting or property as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>A Task that can be awaited to get the result: Setting value.</returns>
        public async Task<string> GetStringAsync(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
            };

            var response = await Gateway.CallAsync("device/get_setting_str", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns any axis setting or property as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>Setting value.</returns>
        public string GetString(string setting)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStringAsync(setting);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets any axis setting as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetStringAsync(string setting, string value)
        {
            var request = new Requests.DeviceSetSettingStrRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
                Value = value,
            };

            await Gateway.CallAsync("device/set_setting_str", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets any axis setting as a string.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting.</param>
        public void SetString(string setting, string value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStringAsync(setting, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Convert arbitrary setting value to Zaber native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting in units specified by following argument.</param>
        /// <param name="unit">Units of the value.</param>
        /// <returns>Setting value.</returns>
        public double ConvertToNativeUnits(string setting, double value, Units unit)
        {
            var request = new Requests.DeviceConvertSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/convert_setting", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Convert arbitrary setting value from Zaber native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="value">Value of the setting in Zaber native units.</param>
        /// <param name="unit">Units to convert value to.</param>
        /// <returns>Setting value.</returns>
        public double ConvertFromNativeUnits(string setting, double value, Units unit)
        {
            var request = new Requests.DeviceConvertSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                FromNative = true,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/convert_setting", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns the default value of a setting.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>Default setting value.</returns>
        public double GetDefault(string setting, Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
                Unit = unit,
            };

            var response = Gateway.CallSync("device/get_setting_default", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns the default value of a setting as a string.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>Default setting value.</returns>
        public string GetDefaultString(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
            };

            var response = Gateway.CallSync("device/get_setting_default_str", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Indicates if given setting can be converted from and to native units.
        /// </summary>
        /// <param name="setting">Name of the setting.</param>
        /// <returns>True if unit conversion can be performed.</returns>
        public bool CanConvertNativeUnits(string setting)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Setting = setting,
            };

            var response = Gateway.CallSync("device/can_convert_setting", request, Requests.BoolResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Overrides default unit conversions.
        /// Conversion factors are specified by setting names representing underlying dimensions.
        /// Requires at least Firmware 7.30.
        /// </summary>
        /// <param name="conversions">Factors of all conversions to override.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetCustomUnitConversionsAsync(ConversionFactor[] conversions)
        {
            var request = new Requests.DeviceSetUnitConversionsRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                Conversions = conversions,
            };

            await Gateway.CallAsync("device/set_unit_conversions", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Overrides default unit conversions.
        /// Conversion factors are specified by setting names representing underlying dimensions.
        /// Requires at least Firmware 7.30.
        /// </summary>
        /// <param name="conversions">Factors of all conversions to override.</param>
        public void SetCustomUnitConversions(ConversionFactor[] conversions)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetCustomUnitConversionsAsync(conversions);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets many setting values in as few requests as possible.
        /// </summary>
        /// <param name="axisSettings">The settings to read.</param>
        /// <returns>A Task that can be awaited to get the result: The setting values read.</returns>
        public async Task<GetAxisSettingResult[]> GetManyAsync(params GetAxisSetting[] axisSettings)
        {
            var request = new Requests.DeviceMultiGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                AxisSettings = axisSettings,
            };

            var response = await Gateway.CallAsync("device/get_many_settings", request, Requests.GetAxisSettingResults.FromByteArray).ConfigureAwait(false);
            return response.Results;
        }


        /// <summary>
        /// Gets many setting values in as few requests as possible.
        /// </summary>
        /// <param name="axisSettings">The settings to read.</param>
        /// <returns>The setting values read.</returns>
        public GetAxisSettingResult[] GetMany(params GetAxisSetting[] axisSettings)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetManyAsync(axisSettings);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets many setting values in the same tick, ensuring their values are synchronized.
        /// Requires at least Firmware 7.35.
        /// </summary>
        /// <param name="axisSettings">The settings to read.</param>
        /// <returns>A Task that can be awaited to get the result: The setting values read.</returns>
        public async Task<GetAxisSettingResult[]> GetSynchronizedAsync(params GetAxisSetting[] axisSettings)
        {
            var request = new Requests.DeviceMultiGetSettingRequest()
            {
                InterfaceId = _axis.Device.Connection.InterfaceId,
                Device = _axis.Device.DeviceAddress,
                Axis = _axis.AxisNumber,
                AxisSettings = axisSettings,
            };

            var response = await Gateway.CallAsync("device/get_sync_settings", request, Requests.GetAxisSettingResults.FromByteArray).ConfigureAwait(false);
            return response.Results;
        }


        /// <summary>
        /// Gets many setting values in the same tick, ensuring their values are synchronized.
        /// Requires at least Firmware 7.35.
        /// </summary>
        /// <param name="axisSettings">The settings to read.</param>
        /// <returns>The setting values read.</returns>
        public GetAxisSettingResult[] GetSynchronized(params GetAxisSetting[] axisSettings)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetSynchronizedAsync(axisSettings);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Axis _axis;


    }
}
