/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Kind of I/O pin to use.
    /// </summary>
    public enum IoPortType
    {
        /// <summary>None.</summary>
        None = 0,

        /// <summary>AnalogInput.</summary>
        AnalogInput = 1,

        /// <summary>AnalogOutput.</summary>
        AnalogOutput = 2,

        /// <summary>DigitalInput.</summary>
        DigitalInput = 3,

        /// <summary>DigitalOutput.</summary>
        DigitalOutput = 4,

    }
}
