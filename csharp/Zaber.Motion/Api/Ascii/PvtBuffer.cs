﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Represents a PVT buffer with this number on a device.
    /// A PVT buffer is a place to store a queue of PVT actions.
    /// </summary>
    public class PvtBuffer
    {
        internal PvtBuffer(Device device, int bufferId)
        {
            Device = device;
            BufferId = bufferId;
        }


        /// <summary>
        /// The Device this buffer exists on.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The number identifying the buffer on the device.
        /// </summary>
        public int BufferId { get; private set; }


        /// <summary>
        /// Gets the buffer contents as an array of strings.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A string array containing all the PVT commands stored in the buffer.</returns>
        public async Task<string[]> GetContentAsync()
        {
            var request = new Requests.StreamBufferGetContentRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                BufferId = BufferId,
                Pvt = true,
            };

            var response = await Gateway.CallAsync("device/stream_buffer_get_content", request, Requests.StreamBufferGetContentResponse.FromByteArray).ConfigureAwait(false);
            return response.BufferLines;
        }


        /// <summary>
        /// Gets the buffer contents as an array of strings.
        /// </summary>
        /// <returns>A string array containing all the PVT commands stored in the buffer.</returns>
        public string[] GetContent()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetContentAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Erases the contents of the buffer.
        /// This method fails if there is a PVT sequence writing to the buffer.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EraseAsync()
        {
            var request = new Requests.StreamBufferEraseRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                BufferId = BufferId,
                Pvt = true,
            };

            await Gateway.CallAsync("device/stream_buffer_erase", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Erases the contents of the buffer.
        /// This method fails if there is a PVT sequence writing to the buffer.
        /// </summary>
        public void Erase()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EraseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
