﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma warning disable CA1708 // We cannot differentiate PeripheralId from Peripheralid without removing either of them.

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Named constants for all Zaber ASCII protocol settings.
    /// For more information please refer to the
    /// <see href="https://www.zaber.com/protocol-manual#topic_settings">ASCII Protocol Manual</see>.
    /// </summary>
    public static class SettingConstants
    {
        /// <summary>
        /// Accel.
        /// </summary>
        public static readonly string Accel = "accel";

        /// <summary>
        /// Brake Mode.
        /// </summary>
        public static readonly string BrakeMode = "brake.mode";

        /// <summary>
        /// Brake State.
        /// </summary>
        public static readonly string BrakeState = "brake.state";

        /// <summary>
        /// Calibration Type.
        /// </summary>
        public static readonly string CalibrationType = "calibration.type";

        /// <summary>
        /// Cloop Continuous Enable.
        /// </summary>
        public static readonly string CloopContinuousEnable = "cloop.continuous.enable";

        /// <summary>
        /// Cloop Counts.
        /// </summary>
        public static readonly string CloopCounts = "cloop.counts";

        /// <summary>
        /// Cloop Displace Tolerance.
        /// </summary>
        public static readonly string CloopDisplaceTolerance = "cloop.displace.tolerance";

        /// <summary>
        /// Cloop Duration Max.
        /// </summary>
        public static readonly string CloopDurationMax = "cloop.duration.max";

        /// <summary>
        /// Cloop Enable.
        /// </summary>
        public static readonly string CloopEnable = "cloop.enable";

        /// <summary>
        /// Cloop Following Tolerance.
        /// </summary>
        public static readonly string CloopFollowingTolerance = "cloop.following.tolerance";

        /// <summary>
        /// Cloop Mode.
        /// </summary>
        public static readonly string CloopMode = "cloop.mode";

        /// <summary>
        /// Cloop Recovery Enable.
        /// </summary>
        public static readonly string CloopRecoveryEnable = "cloop.recovery.enable";

        /// <summary>
        /// Cloop Servo Effort.
        /// </summary>
        public static readonly string CloopServoEffort = "cloop.servo.effort";

        /// <summary>
        /// Cloop Servo Enable.
        /// </summary>
        public static readonly string CloopServoEnable = "cloop.servo.enable";

        /// <summary>
        /// Cloop Settle Period.
        /// </summary>
        public static readonly string CloopSettlePeriod = "cloop.settle.period";

        /// <summary>
        /// Cloop Settle Tolerance.
        /// </summary>
        public static readonly string CloopSettleTolerance = "cloop.settle.tolerance";

        /// <summary>
        /// Cloop Stall Action.
        /// </summary>
        public static readonly string CloopStallAction = "cloop.stall.action";

        /// <summary>
        /// Cloop Stall Detect Mode.
        /// </summary>
        public static readonly string CloopStallDetectMode = "cloop.stall.detect.mode";

        /// <summary>
        /// Cloop Stall Tolerance.
        /// </summary>
        public static readonly string CloopStallTolerance = "cloop.stall.tolerance";

        /// <summary>
        /// Cloop Stalltimeout.
        /// </summary>
        public static readonly string CloopStalltimeout = "cloop.stalltimeout";

        /// <summary>
        /// Cloop Steps.
        /// </summary>
        public static readonly string CloopSteps = "cloop.steps";

        /// <summary>
        /// Cloop Timeout.
        /// </summary>
        public static readonly string CloopTimeout = "cloop.timeout";

        /// <summary>
        /// Comm Address.
        /// </summary>
        public static readonly string CommAddress = "comm.address";

        /// <summary>
        /// Comm Alert.
        /// </summary>
        public static readonly string CommAlert = "comm.alert";

        /// <summary>
        /// Comm Checksum.
        /// </summary>
        public static readonly string CommChecksum = "comm.checksum";

        /// <summary>
        /// Comm Command Packets Max.
        /// </summary>
        public static readonly string CommCommandPacketsMax = "comm.command.packets.max";

        /// <summary>
        /// Comm En Ipv 4 Address.
        /// </summary>
        public static readonly string CommEnIpv4Address = "comm.en.ipv4.address";

        /// <summary>
        /// Comm En Ipv 4 Dhcp Enabled.
        /// </summary>
        public static readonly string CommEnIpv4DhcpEnabled = "comm.en.ipv4.dhcp.enabled";

        /// <summary>
        /// Comm En Ipv 4 Gateway.
        /// </summary>
        public static readonly string CommEnIpv4Gateway = "comm.en.ipv4.gateway";

        /// <summary>
        /// Comm En Ipv 4 Netmask.
        /// </summary>
        public static readonly string CommEnIpv4Netmask = "comm.en.ipv4.netmask";

        /// <summary>
        /// Comm En Mac.
        /// </summary>
        public static readonly string CommEnMac = "comm.en.mac";

        /// <summary>
        /// Comm En Mdns Enable.
        /// </summary>
        public static readonly string CommEnMdnsEnable = "comm.en.mdns.enable";

        /// <summary>
        /// Comm Ethercat Remote.
        /// </summary>
        public static readonly string CommEthercatRemote = "comm.ethercat.remote";

        /// <summary>
        /// Comm Next Owner.
        /// </summary>
        public static readonly string CommNextOwner = "comm.next.owner";

        /// <summary>
        /// Comm Packet Size Max.
        /// </summary>
        public static readonly string CommPacketSizeMax = "comm.packet.size.max";

        /// <summary>
        /// Comm Protocol.
        /// </summary>
        public static readonly string CommProtocol = "comm.protocol";

        /// <summary>
        /// Comm Rs 232 Baud.
        /// </summary>
        public static readonly string CommRs232Baud = "comm.rs232.baud";

        /// <summary>
        /// Comm Rs 232 Protocol.
        /// </summary>
        public static readonly string CommRs232Protocol = "comm.rs232.protocol";

        /// <summary>
        /// Comm Rs 485 Baud.
        /// </summary>
        public static readonly string CommRs485Baud = "comm.rs485.baud";

        /// <summary>
        /// Comm Rs 485 Enable.
        /// </summary>
        public static readonly string CommRs485Enable = "comm.rs485.enable";

        /// <summary>
        /// Comm Rs 485 Protocol.
        /// </summary>
        public static readonly string CommRs485Protocol = "comm.rs485.protocol";

        /// <summary>
        /// Comm Usb Protocol.
        /// </summary>
        public static readonly string CommUsbProtocol = "comm.usb.protocol";

        /// <summary>
        /// Comm Word Size Max.
        /// </summary>
        public static readonly string CommWordSizeMax = "comm.word.size.max";

        /// <summary>
        /// Device Hw Modified.
        /// </summary>
        public static readonly string DeviceHwModified = "device.hw.modified";

        /// <summary>
        /// Device ID (Firmware 7 and higher).
        /// </summary>
        public static readonly string DeviceId = "device.id";

        /// <summary>
        /// Device ID (Firmware 6 and lower).
        /// </summary>
        public static readonly string DeviceIdLegacy = "deviceid";

        /// <summary>
        /// Driver Bipolar.
        /// </summary>
        public static readonly string DriverBipolar = "driver.bipolar";

        /// <summary>
        /// Driver Current Approach.
        /// </summary>
        public static readonly string DriverCurrentApproach = "driver.current.approach";

        /// <summary>
        /// Driver Current Continuous.
        /// </summary>
        public static readonly string DriverCurrentContinuous = "driver.current.continuous";

        /// <summary>
        /// Driver Current Continuous Max.
        /// </summary>
        public static readonly string DriverCurrentContinuousMax = "driver.current.continuous.max";

        /// <summary>
        /// Driver Current Hold.
        /// </summary>
        public static readonly string DriverCurrentHold = "driver.current.hold";

        /// <summary>
        /// Driver Current Inject Noise Amplitude.
        /// </summary>
        public static readonly string DriverCurrentInjectNoiseAmplitude = "driver.current.inject.noise.amplitude";

        /// <summary>
        /// Driver Current Max.
        /// </summary>
        public static readonly string DriverCurrentMax = "driver.current.max";

        /// <summary>
        /// Driver Current Overdrive.
        /// </summary>
        public static readonly string DriverCurrentOverdrive = "driver.current.overdrive";

        /// <summary>
        /// Driver Current Overdrive Duration.
        /// </summary>
        public static readonly string DriverCurrentOverdriveDuration = "driver.current.overdrive.duration";

        /// <summary>
        /// Driver Current Overdrive Max.
        /// </summary>
        public static readonly string DriverCurrentOverdriveMax = "driver.current.overdrive.max";

        /// <summary>
        /// Driver Current Run.
        /// </summary>
        public static readonly string DriverCurrentRun = "driver.current.run";

        /// <summary>
        /// Driver Current Servo.
        /// </summary>
        public static readonly string DriverCurrentServo = "driver.current.servo";

        /// <summary>
        /// Driver Dir.
        /// </summary>
        public static readonly string DriverDir = "driver.dir";

        /// <summary>
        /// Driver Enable Mode.
        /// </summary>
        public static readonly string DriverEnableMode = "driver.enable.mode";

        /// <summary>
        /// Driver Enabled.
        /// </summary>
        public static readonly string DriverEnabled = "driver.enabled";

        /// <summary>
        /// Driver I 2 T Measured.
        /// </summary>
        public static readonly string DriverI2TMeasured = "driver.i2t.measured";

        /// <summary>
        /// Driver Overdrive State.
        /// </summary>
        public static readonly string DriverOverdriveState = "driver.overdrive.state";

        /// <summary>
        /// Driver Temperature.
        /// </summary>
        public static readonly string DriverTemperature = "driver.temperature";

        /// <summary>
        /// Encoder 1 Count.
        /// </summary>
        public static readonly string Encoder1Count = "encoder.1.count";

        /// <summary>
        /// Encoder 1 Count Cal.
        /// </summary>
        public static readonly string Encoder1CountCal = "encoder.1.count.cal";

        /// <summary>
        /// Encoder 1 Dir.
        /// </summary>
        public static readonly string Encoder1Dir = "encoder.1.dir";

        /// <summary>
        /// Encoder 1 Fault Type.
        /// </summary>
        public static readonly string Encoder1FaultType = "encoder.1.fault.type";

        /// <summary>
        /// Encoder 1 Filter.
        /// </summary>
        public static readonly string Encoder1Filter = "encoder.1.filter";

        /// <summary>
        /// Encoder 1 Index Mode.
        /// </summary>
        public static readonly string Encoder1IndexMode = "encoder.1.index.mode";

        /// <summary>
        /// Encoder 1 Mode.
        /// </summary>
        public static readonly string Encoder1Mode = "encoder.1.mode";

        /// <summary>
        /// Encoder 1 Pos.
        /// </summary>
        public static readonly string Encoder1Pos = "encoder.1.pos";

        /// <summary>
        /// Encoder 1 Pos Error.
        /// </summary>
        public static readonly string Encoder1PosError = "encoder.1.pos.error";

        /// <summary>
        /// Encoder 1 Power Up Delay.
        /// </summary>
        public static readonly string Encoder1PowerUpDelay = "encoder.1.power.up.delay";

        /// <summary>
        /// Encoder 1 Ratio Div.
        /// </summary>
        public static readonly string Encoder1RatioDiv = "encoder.1.ratio.div";

        /// <summary>
        /// Encoder 1 Ratio Mult.
        /// </summary>
        public static readonly string Encoder1RatioMult = "encoder.1.ratio.mult";

        /// <summary>
        /// Encoder 1 Ref Phase.
        /// </summary>
        public static readonly string Encoder1RefPhase = "encoder.1.ref.phase";

        /// <summary>
        /// Encoder 1 Type.
        /// </summary>
        public static readonly string Encoder1Type = "encoder.1.type";

        /// <summary>
        /// Encoder 2 Cos.
        /// </summary>
        public static readonly string Encoder2Cos = "encoder.2.cos";

        /// <summary>
        /// Encoder 2 Cos Dc.
        /// </summary>
        public static readonly string Encoder2CosDc = "encoder.2.cos.dc";

        /// <summary>
        /// Encoder 2 Cos Dc Tune.
        /// </summary>
        public static readonly string Encoder2CosDcTune = "encoder.2.cos.dc.tune";

        /// <summary>
        /// Encoder 2 Cos Gain.
        /// </summary>
        public static readonly string Encoder2CosGain = "encoder.2.cos.gain";

        /// <summary>
        /// Encoder 2 Cos Gain Tune.
        /// </summary>
        public static readonly string Encoder2CosGainTune = "encoder.2.cos.gain.tune";

        /// <summary>
        /// Encoder 2 Count.
        /// </summary>
        public static readonly string Encoder2Count = "encoder.2.count";

        /// <summary>
        /// Encoder 2 Count Cal.
        /// </summary>
        public static readonly string Encoder2CountCal = "encoder.2.count.cal";

        /// <summary>
        /// Encoder 2 Dir.
        /// </summary>
        public static readonly string Encoder2Dir = "encoder.2.dir";

        /// <summary>
        /// Encoder 2 Fault Type.
        /// </summary>
        public static readonly string Encoder2FaultType = "encoder.2.fault.type";

        /// <summary>
        /// Encoder 2 Filter.
        /// </summary>
        public static readonly string Encoder2Filter = "encoder.2.filter";

        /// <summary>
        /// Encoder 2 Index Mode.
        /// </summary>
        public static readonly string Encoder2IndexMode = "encoder.2.index.mode";

        /// <summary>
        /// Encoder 2 Interpolation.
        /// </summary>
        public static readonly string Encoder2Interpolation = "encoder.2.interpolation";

        /// <summary>
        /// Encoder 2 Mode.
        /// </summary>
        public static readonly string Encoder2Mode = "encoder.2.mode";

        /// <summary>
        /// Encoder 2 Out Enable.
        /// </summary>
        public static readonly string Encoder2OutEnable = "encoder.2.out.enable";

        /// <summary>
        /// Encoder 2 Out Interpolation.
        /// </summary>
        public static readonly string Encoder2OutInterpolation = "encoder.2.out.interpolation";

        /// <summary>
        /// Encoder 2 Out Width.
        /// </summary>
        public static readonly string Encoder2OutWidth = "encoder.2.out.width";

        /// <summary>
        /// Encoder 2 Pos.
        /// </summary>
        public static readonly string Encoder2Pos = "encoder.2.pos";

        /// <summary>
        /// Encoder 2 Pos Error.
        /// </summary>
        public static readonly string Encoder2PosError = "encoder.2.pos.error";

        /// <summary>
        /// Encoder 2 Power Up Delay.
        /// </summary>
        public static readonly string Encoder2PowerUpDelay = "encoder.2.power.up.delay";

        /// <summary>
        /// Encoder 2 Ratio Div.
        /// </summary>
        public static readonly string Encoder2RatioDiv = "encoder.2.ratio.div";

        /// <summary>
        /// Encoder 2 Ratio Mult.
        /// </summary>
        public static readonly string Encoder2RatioMult = "encoder.2.ratio.mult";

        /// <summary>
        /// Encoder 2 Signal Min.
        /// </summary>
        public static readonly string Encoder2SignalMin = "encoder.2.signal.min";

        /// <summary>
        /// Encoder 2 Sin.
        /// </summary>
        public static readonly string Encoder2Sin = "encoder.2.sin";

        /// <summary>
        /// Encoder 2 Sin Dc.
        /// </summary>
        public static readonly string Encoder2SinDc = "encoder.2.sin.dc";

        /// <summary>
        /// Encoder 2 Sin Dc Tune.
        /// </summary>
        public static readonly string Encoder2SinDcTune = "encoder.2.sin.dc.tune";

        /// <summary>
        /// Encoder 2 Sin Gain.
        /// </summary>
        public static readonly string Encoder2SinGain = "encoder.2.sin.gain";

        /// <summary>
        /// Encoder 2 Sin Gain Tune.
        /// </summary>
        public static readonly string Encoder2SinGainTune = "encoder.2.sin.gain.tune";

        /// <summary>
        /// Encoder 2 Type.
        /// </summary>
        public static readonly string Encoder2Type = "encoder.2.type";

        /// <summary>
        /// Encoder Count.
        /// </summary>
        public static readonly string EncoderCount = "encoder.count";

        /// <summary>
        /// Encoder Count Cal.
        /// </summary>
        public static readonly string EncoderCountCal = "encoder.count.cal";

        /// <summary>
        /// Encoder Count Calibrated.
        /// </summary>
        public static readonly string EncoderCountCalibrated = "encoder.count.calibrated";

        /// <summary>
        /// Encoder Dir.
        /// </summary>
        public static readonly string EncoderDir = "encoder.dir";

        /// <summary>
        /// Encoder Error.
        /// </summary>
        public static readonly string EncoderError = "encoder.error";

        /// <summary>
        /// Encoder Fault Type.
        /// </summary>
        public static readonly string EncoderFaultType = "encoder.fault.type";

        /// <summary>
        /// Encoder Filter.
        /// </summary>
        public static readonly string EncoderFilter = "encoder.filter";

        /// <summary>
        /// Encoder Index Count.
        /// </summary>
        public static readonly string EncoderIndexCount = "encoder.index.count";

        /// <summary>
        /// Encoder Index Mode.
        /// </summary>
        public static readonly string EncoderIndexMode = "encoder.index.mode";

        /// <summary>
        /// Encoder Index Phase.
        /// </summary>
        public static readonly string EncoderIndexPhase = "encoder.index.phase";

        /// <summary>
        /// Encoder Mode.
        /// </summary>
        public static readonly string EncoderMode = "encoder.mode";

        /// <summary>
        /// Encoder Port Default.
        /// </summary>
        public static readonly string EncoderPortDefault = "encoder.port.default";

        /// <summary>
        /// Encoder Pos.
        /// </summary>
        public static readonly string EncoderPos = "encoder.pos";

        /// <summary>
        /// Encoder Pos Error.
        /// </summary>
        public static readonly string EncoderPosError = "encoder.pos.error";

        /// <summary>
        /// Encoder Power Up Delay.
        /// </summary>
        public static readonly string EncoderPowerUpDelay = "encoder.power.up.delay";

        /// <summary>
        /// Encoder Ratio Div.
        /// </summary>
        public static readonly string EncoderRatioDiv = "encoder.ratio.div";

        /// <summary>
        /// Encoder Ratio Mult.
        /// </summary>
        public static readonly string EncoderRatioMult = "encoder.ratio.mult";

        /// <summary>
        /// Encoder Vel.
        /// </summary>
        public static readonly string EncoderVel = "encoder.vel";

        /// <summary>
        /// Filter Holderid.
        /// </summary>
        public static readonly string FilterHolderid = "filter.holderid";

        /// <summary>
        /// Force Average.
        /// </summary>
        public static readonly string ForceAverage = "force.average";

        /// <summary>
        /// Force Max.
        /// </summary>
        public static readonly string ForceMax = "force.max";

        /// <summary>
        /// Get Settings Max.
        /// </summary>
        public static readonly string GetSettingsMax = "get.settings.max";

        /// <summary>
        /// Ictrl Advance A.
        /// </summary>
        public static readonly string IctrlAdvanceA = "ictrl.advance.a";

        /// <summary>
        /// Ictrl Advance Offset.
        /// </summary>
        public static readonly string IctrlAdvanceOffset = "ictrl.advance.offset";

        /// <summary>
        /// Ictrl Afcff Inductance.
        /// </summary>
        public static readonly string IctrlAfcffInductance = "ictrl.afcff.inductance";

        /// <summary>
        /// Ictrl Afcff Ke.
        /// </summary>
        public static readonly string IctrlAfcffKe = "ictrl.afcff.ke";

        /// <summary>
        /// Ictrl Afcff Ki.
        /// </summary>
        public static readonly string IctrlAfcffKi = "ictrl.afcff.ki";

        /// <summary>
        /// Ictrl Afcff Max.
        /// </summary>
        public static readonly string IctrlAfcffMax = "ictrl.afcff.max";

        /// <summary>
        /// Ictrl Afcff Ss.
        /// </summary>
        public static readonly string IctrlAfcffSs = "ictrl.afcff.ss";

        /// <summary>
        /// Ictrl Afcff Ss Max.
        /// </summary>
        public static readonly string IctrlAfcffSsMax = "ictrl.afcff.ss.max";

        /// <summary>
        /// Ictrl Delay.
        /// </summary>
        public static readonly string IctrlDelay = "ictrl.delay";

        /// <summary>
        /// Ictrl Ff Kd.
        /// </summary>
        public static readonly string IctrlFfKd = "ictrl.ff.kd";

        /// <summary>
        /// Ictrl Ff Kp.
        /// </summary>
        public static readonly string IctrlFfKp = "ictrl.ff.kp";

        /// <summary>
        /// Ictrl Gain Coldmult.
        /// </summary>
        public static readonly string IctrlGainColdmult = "ictrl.gain.coldmult";

        /// <summary>
        /// Ictrl Period.
        /// </summary>
        public static readonly string IctrlPeriod = "ictrl.period";

        /// <summary>
        /// Ictrl Pi Ki.
        /// </summary>
        public static readonly string IctrlPiKi = "ictrl.pi.ki";

        /// <summary>
        /// Ictrl Pi Kp.
        /// </summary>
        public static readonly string IctrlPiKp = "ictrl.pi.kp";

        /// <summary>
        /// Ictrl Type.
        /// </summary>
        public static readonly string IctrlType = "ictrl.type";

        /// <summary>
        /// Io Ai 1 Fc.
        /// </summary>
        public static readonly string IoAi1Fc = "io.ai.1.fc";

        /// <summary>
        /// Io Ai 2 Fc.
        /// </summary>
        public static readonly string IoAi2Fc = "io.ai.2.fc";

        /// <summary>
        /// Io Ai 3 Fc.
        /// </summary>
        public static readonly string IoAi3Fc = "io.ai.3.fc";

        /// <summary>
        /// Io Ai 4 Fc.
        /// </summary>
        public static readonly string IoAi4Fc = "io.ai.4.fc";

        /// <summary>
        /// Io Di Port.
        /// </summary>
        public static readonly string IoDiPort = "io.di.port";

        /// <summary>
        /// Io Do Port.
        /// </summary>
        public static readonly string IoDoPort = "io.do.port";

        /// <summary>
        /// Joy Debug.
        /// </summary>
        public static readonly string JoyDebug = "joy.debug";

        /// <summary>
        /// Knob Dir.
        /// </summary>
        public static readonly string KnobDir = "knob.dir";

        /// <summary>
        /// Knob Distance.
        /// </summary>
        public static readonly string KnobDistance = "knob.distance";

        /// <summary>
        /// Knob Enable.
        /// </summary>
        public static readonly string KnobEnable = "knob.enable";

        /// <summary>
        /// Knob Force.
        /// </summary>
        public static readonly string KnobForce = "knob.force";

        /// <summary>
        /// Knob Forceprofile.
        /// </summary>
        public static readonly string KnobForceprofile = "knob.forceprofile";

        /// <summary>
        /// Knob Maxspeed.
        /// </summary>
        public static readonly string KnobMaxspeed = "knob.maxspeed";

        /// <summary>
        /// Knob Mode.
        /// </summary>
        public static readonly string KnobMode = "knob.mode";

        /// <summary>
        /// Knob Speedprofile.
        /// </summary>
        public static readonly string KnobSpeedprofile = "knob.speedprofile";

        /// <summary>
        /// Lamp Current.
        /// </summary>
        public static readonly string LampCurrent = "lamp.current";

        /// <summary>
        /// Lamp Current Max.
        /// </summary>
        public static readonly string LampCurrentMax = "lamp.current.max";

        /// <summary>
        /// Lamp Flux.
        /// </summary>
        public static readonly string LampFlux = "lamp.flux";

        /// <summary>
        /// Lamp Flux Max.
        /// </summary>
        public static readonly string LampFluxMax = "lamp.flux.max";

        /// <summary>
        /// Lamp Status.
        /// </summary>
        public static readonly string LampStatus = "lamp.status";

        /// <summary>
        /// Lamp Temperature.
        /// </summary>
        public static readonly string LampTemperature = "lamp.temperature";

        /// <summary>
        /// Lamp Wavelength Fwhm.
        /// </summary>
        public static readonly string LampWavelengthFwhm = "lamp.wavelength.fwhm";

        /// <summary>
        /// Lamp Wavelength Peak.
        /// </summary>
        public static readonly string LampWavelengthPeak = "lamp.wavelength.peak";

        /// <summary>
        /// Limit Approach Accel.
        /// </summary>
        public static readonly string LimitApproachAccel = "limit.approach.accel";

        /// <summary>
        /// Limit Approach Maxspeed.
        /// </summary>
        public static readonly string LimitApproachMaxspeed = "limit.approach.maxspeed";

        /// <summary>
        /// Limit Away Action.
        /// </summary>
        public static readonly string LimitAwayAction = "limit.away.action";

        /// <summary>
        /// Limit Away Edge.
        /// </summary>
        public static readonly string LimitAwayEdge = "limit.away.edge";

        /// <summary>
        /// Limit Away Offset.
        /// </summary>
        public static readonly string LimitAwayOffset = "limit.away.offset";

        /// <summary>
        /// Limit Away Pos.
        /// </summary>
        public static readonly string LimitAwayPos = "limit.away.pos";

        /// <summary>
        /// Limit Away Posupdate.
        /// </summary>
        public static readonly string LimitAwayPosupdate = "limit.away.posupdate";

        /// <summary>
        /// Limit Away Preset.
        /// </summary>
        public static readonly string LimitAwayPreset = "limit.away.preset";

        /// <summary>
        /// Limit Away Source.
        /// </summary>
        public static readonly string LimitAwaySource = "limit.away.source";

        /// <summary>
        /// Limit Away State.
        /// </summary>
        public static readonly string LimitAwayState = "limit.away.state";

        /// <summary>
        /// Limit Away Triggered.
        /// </summary>
        public static readonly string LimitAwayTriggered = "limit.away.triggered";

        /// <summary>
        /// Limit Away Tune.
        /// </summary>
        public static readonly string LimitAwayTune = "limit.away.tune";

        /// <summary>
        /// Limit Away Type.
        /// </summary>
        public static readonly string LimitAwayType = "limit.away.type";

        /// <summary>
        /// Limit Away Width.
        /// </summary>
        public static readonly string LimitAwayWidth = "limit.away.width";

        /// <summary>
        /// Limit C Action.
        /// </summary>
        public static readonly string LimitCAction = "limit.c.action";

        /// <summary>
        /// Limit C Edge.
        /// </summary>
        public static readonly string LimitCEdge = "limit.c.edge";

        /// <summary>
        /// Limit C Offset.
        /// </summary>
        public static readonly string LimitCOffset = "limit.c.offset";

        /// <summary>
        /// Limit C Pos.
        /// </summary>
        public static readonly string LimitCPos = "limit.c.pos";

        /// <summary>
        /// Limit C Posupdate.
        /// </summary>
        public static readonly string LimitCPosupdate = "limit.c.posupdate";

        /// <summary>
        /// Limit C Preset.
        /// </summary>
        public static readonly string LimitCPreset = "limit.c.preset";

        /// <summary>
        /// Limit C Source.
        /// </summary>
        public static readonly string LimitCSource = "limit.c.source";

        /// <summary>
        /// Limit C State.
        /// </summary>
        public static readonly string LimitCState = "limit.c.state";

        /// <summary>
        /// Limit C Triggered.
        /// </summary>
        public static readonly string LimitCTriggered = "limit.c.triggered";

        /// <summary>
        /// Limit C Tune.
        /// </summary>
        public static readonly string LimitCTune = "limit.c.tune";

        /// <summary>
        /// Limit C Type.
        /// </summary>
        public static readonly string LimitCType = "limit.c.type";

        /// <summary>
        /// Limit C Width.
        /// </summary>
        public static readonly string LimitCWidth = "limit.c.width";

        /// <summary>
        /// Limit Cycle Dist.
        /// </summary>
        public static readonly string LimitCycleDist = "limit.cycle.dist";

        /// <summary>
        /// Limit D Action.
        /// </summary>
        public static readonly string LimitDAction = "limit.d.action";

        /// <summary>
        /// Limit D Edge.
        /// </summary>
        public static readonly string LimitDEdge = "limit.d.edge";

        /// <summary>
        /// Limit D Pos.
        /// </summary>
        public static readonly string LimitDPos = "limit.d.pos";

        /// <summary>
        /// Limit D Posupdate.
        /// </summary>
        public static readonly string LimitDPosupdate = "limit.d.posupdate";

        /// <summary>
        /// Limit D Preset.
        /// </summary>
        public static readonly string LimitDPreset = "limit.d.preset";

        /// <summary>
        /// Limit D State.
        /// </summary>
        public static readonly string LimitDState = "limit.d.state";

        /// <summary>
        /// Limit D Triggered.
        /// </summary>
        public static readonly string LimitDTriggered = "limit.d.triggered";

        /// <summary>
        /// Limit D Type.
        /// </summary>
        public static readonly string LimitDType = "limit.d.type";

        /// <summary>
        /// Limit Detect Decelonly.
        /// </summary>
        public static readonly string LimitDetectDecelonly = "limit.detect.decelonly";

        /// <summary>
        /// Limit Detect Maxspeed.
        /// </summary>
        public static readonly string LimitDetectMaxspeed = "limit.detect.maxspeed";

        /// <summary>
        /// Limit Hardstop Retraction.
        /// </summary>
        public static readonly string LimitHardstopRetraction = "limit.hardstop.retraction";

        /// <summary>
        /// Limit Home Action.
        /// </summary>
        public static readonly string LimitHomeAction = "limit.home.action";

        /// <summary>
        /// Limit Home Bidirectional.
        /// </summary>
        public static readonly string LimitHomeBidirectional = "limit.home.bidirectional";

        /// <summary>
        /// Limit Home Edge.
        /// </summary>
        public static readonly string LimitHomeEdge = "limit.home.edge";

        /// <summary>
        /// Limit Home Offset.
        /// </summary>
        public static readonly string LimitHomeOffset = "limit.home.offset";

        /// <summary>
        /// Limit Home Pos.
        /// </summary>
        public static readonly string LimitHomePos = "limit.home.pos";

        /// <summary>
        /// Limit Home Posupdate.
        /// </summary>
        public static readonly string LimitHomePosupdate = "limit.home.posupdate";

        /// <summary>
        /// Limit Home Preset.
        /// </summary>
        public static readonly string LimitHomePreset = "limit.home.preset";

        /// <summary>
        /// Limit Home Source.
        /// </summary>
        public static readonly string LimitHomeSource = "limit.home.source";

        /// <summary>
        /// Limit Home State.
        /// </summary>
        public static readonly string LimitHomeState = "limit.home.state";

        /// <summary>
        /// Limit Home Triggered.
        /// </summary>
        public static readonly string LimitHomeTriggered = "limit.home.triggered";

        /// <summary>
        /// Limit Home Tune.
        /// </summary>
        public static readonly string LimitHomeTune = "limit.home.tune";

        /// <summary>
        /// Limit Home Type.
        /// </summary>
        public static readonly string LimitHomeType = "limit.home.type";

        /// <summary>
        /// Limit Home Width.
        /// </summary>
        public static readonly string LimitHomeWidth = "limit.home.width";

        /// <summary>
        /// Limit Max.
        /// </summary>
        public static readonly string LimitMax = "limit.max";

        /// <summary>
        /// Limit Min.
        /// </summary>
        public static readonly string LimitMin = "limit.min";

        /// <summary>
        /// Limit Range Mode.
        /// </summary>
        public static readonly string LimitRangeMode = "limit.range.mode";

        /// <summary>
        /// Limit Ref Phase.
        /// </summary>
        public static readonly string LimitRefPhase = "limit.ref.phase";

        /// <summary>
        /// Limit Ref Phase Measured.
        /// </summary>
        public static readonly string LimitRefPhaseMeasured = "limit.ref.phase.measured";

        /// <summary>
        /// Limit Start Pos.
        /// </summary>
        public static readonly string LimitStartPos = "limit.start.pos";

        /// <summary>
        /// Limit Swapinputs.
        /// </summary>
        public static readonly string LimitSwapinputs = "limit.swapinputs";

        /// <summary>
        /// Lockstep Numgroups.
        /// </summary>
        public static readonly string LockstepNumgroups = "lockstep.numgroups";

        /// <summary>
        /// Lockstep Tolerance.
        /// </summary>
        public static readonly string LockstepTolerance = "lockstep.tolerance";

        /// <summary>
        /// Maxspeed.
        /// </summary>
        public static readonly string Maxspeed = "maxspeed";

        /// <summary>
        /// Motion Accel Ramptime.
        /// </summary>
        public static readonly string MotionAccelRamptime = "motion.accel.ramptime";

        /// <summary>
        /// Motion Accelonly.
        /// </summary>
        public static readonly string MotionAccelonly = "motion.accelonly";

        /// <summary>
        /// Motion Busy.
        /// </summary>
        public static readonly string MotionBusy = "motion.busy";

        /// <summary>
        /// Motion Decelonly.
        /// </summary>
        public static readonly string MotionDecelonly = "motion.decelonly";

        /// <summary>
        /// Motion Index Dist.
        /// </summary>
        public static readonly string MotionIndexDist = "motion.index.dist";

        /// <summary>
        /// Motion Index Num.
        /// </summary>
        public static readonly string MotionIndexNum = "motion.index.num";

        /// <summary>
        /// Motion Tracking Ai.
        /// </summary>
        public static readonly string MotionTrackingAi = "motion.tracking.ai";

        /// <summary>
        /// Motion Tracking Dir.
        /// </summary>
        public static readonly string MotionTrackingDir = "motion.tracking.dir";

        /// <summary>
        /// Motion Tracking Ki.
        /// </summary>
        public static readonly string MotionTrackingKi = "motion.tracking.ki";

        /// <summary>
        /// Motion Tracking Kp.
        /// </summary>
        public static readonly string MotionTrackingKp = "motion.tracking.kp";

        /// <summary>
        /// Motion Tracking Limit Max.
        /// </summary>
        public static readonly string MotionTrackingLimitMax = "motion.tracking.limit.max";

        /// <summary>
        /// Motion Tracking Limit Min.
        /// </summary>
        public static readonly string MotionTrackingLimitMin = "motion.tracking.limit.min";

        /// <summary>
        /// Motion Tracking Mode.
        /// </summary>
        public static readonly string MotionTrackingMode = "motion.tracking.mode";

        /// <summary>
        /// Motion Tracking Scan Dir.
        /// </summary>
        public static readonly string MotionTrackingScanDir = "motion.tracking.scan.dir";

        /// <summary>
        /// Motion Tracking Scan Maxspeed.
        /// </summary>
        public static readonly string MotionTrackingScanMaxspeed = "motion.tracking.scan.maxspeed";

        /// <summary>
        /// Motion Tracking Scan Offset.
        /// </summary>
        public static readonly string MotionTrackingScanOffset = "motion.tracking.scan.offset";

        /// <summary>
        /// Motion Tracking Scan Period.
        /// </summary>
        public static readonly string MotionTrackingScanPeriod = "motion.tracking.scan.period";

        /// <summary>
        /// Motion Tracking Scan Signal Valid Delay.
        /// </summary>
        public static readonly string MotionTrackingScanSignalValidDelay = "motion.tracking.scan.signal.valid.delay";

        /// <summary>
        /// Motion Tracking Scan Tolerance.
        /// </summary>
        public static readonly string MotionTrackingScanTolerance = "motion.tracking.scan.tolerance";

        /// <summary>
        /// Motion Tracking Setpoint.
        /// </summary>
        public static readonly string MotionTrackingSetpoint = "motion.tracking.setpoint";

        /// <summary>
        /// Motion Tracking Settle Mode.
        /// </summary>
        public static readonly string MotionTrackingSettleMode = "motion.tracking.settle.mode";

        /// <summary>
        /// Motion Tracking Settle Period.
        /// </summary>
        public static readonly string MotionTrackingSettlePeriod = "motion.tracking.settle.period";

        /// <summary>
        /// Motion Tracking Settle Tolerance.
        /// </summary>
        public static readonly string MotionTrackingSettleTolerance = "motion.tracking.settle.tolerance";

        /// <summary>
        /// Motion Tracking Settle Tolerance Met.
        /// </summary>
        public static readonly string MotionTrackingSettleToleranceMet = "motion.tracking.settle.tolerance.met";

        /// <summary>
        /// Motion Tracking Settled.
        /// </summary>
        public static readonly string MotionTrackingSettled = "motion.tracking.settled";

        /// <summary>
        /// Motion Tracking Signal Valid Di.
        /// </summary>
        public static readonly string MotionTrackingSignalValidDi = "motion.tracking.signal.valid.di";

        /// <summary>
        /// Motor Current Continuous Max.
        /// </summary>
        public static readonly string MotorCurrentContinuousMax = "motor.current.continuous.max";

        /// <summary>
        /// Motor Current Max.
        /// </summary>
        public static readonly string MotorCurrentMax = "motor.current.max";

        /// <summary>
        /// Motor Current Overdrive Duration.
        /// </summary>
        public static readonly string MotorCurrentOverdriveDuration = "motor.current.overdrive.duration";

        /// <summary>
        /// Motor Current Overdrive Max.
        /// </summary>
        public static readonly string MotorCurrentOverdriveMax = "motor.current.overdrive.max";

        /// <summary>
        /// Motor I 2 T Measured.
        /// </summary>
        public static readonly string MotorI2TMeasured = "motor.i2t.measured";

        /// <summary>
        /// Motor Inductance.
        /// </summary>
        public static readonly string MotorInductance = "motor.inductance";

        /// <summary>
        /// Motor Ke.
        /// </summary>
        public static readonly string MotorKe = "motor.ke";

        /// <summary>
        /// Motor Phase.
        /// </summary>
        public static readonly string MotorPhase = "motor.phase";

        /// <summary>
        /// Motor Phase Ratio Div 1.
        /// </summary>
        public static readonly string MotorPhaseRatioDiv1 = "motor.phase.ratio.div1";

        /// <summary>
        /// Motor Phase Ratio Div 2.
        /// </summary>
        public static readonly string MotorPhaseRatioDiv2 = "motor.phase.ratio.div2";

        /// <summary>
        /// Motor Phase Ratio Mult.
        /// </summary>
        public static readonly string MotorPhaseRatioMult = "motor.phase.ratio.mult";

        /// <summary>
        /// Motor Resistance.
        /// </summary>
        public static readonly string MotorResistance = "motor.resistance";

        /// <summary>
        /// Parking State.
        /// </summary>
        public static readonly string ParkingState = "parking.state";

        /// <summary>
        /// Peripheral Hw Modified.
        /// </summary>
        public static readonly string PeripheralHwModified = "peripheral.hw.modified";

        /// <summary>
        /// Peripheral ID (Firmware 7 and higher).
        /// </summary>
        public static readonly string PeripheralId = "peripheral.id";

        /// <summary>
        /// Peripheral Id Pending.
        /// </summary>
        public static readonly string PeripheralIdPending = "peripheral.id.pending";

        /// <summary>
        /// Peripheral Serial.
        /// </summary>
        public static readonly string PeripheralSerial = "peripheral.serial";

        /// <summary>
        /// Peripheral Serial Pending.
        /// </summary>
        public static readonly string PeripheralSerialPending = "peripheral.serial.pending";

        /// <summary>
        /// Peripheral ID (Firmware 6 and lower).
        /// </summary>
        public static readonly string PeripheralIdLegacy = "peripheralid";

        /// <summary>
        /// Pos.
        /// </summary>
        public static readonly string Pos = "pos";

        /// <summary>
        /// Process Control Dir.
        /// </summary>
        public static readonly string ProcessControlDir = "process.control.dir";

        /// <summary>
        /// Process Control Hysteresis Temperature.
        /// </summary>
        public static readonly string ProcessControlHysteresisTemperature = "process.control.hysteresis.temperature";

        /// <summary>
        /// Process Control Hysteresis Voltage.
        /// </summary>
        public static readonly string ProcessControlHysteresisVoltage = "process.control.hysteresis.voltage";

        /// <summary>
        /// Process Control Mode.
        /// </summary>
        public static readonly string ProcessControlMode = "process.control.mode";

        /// <summary>
        /// Process Control Setpoint Temperature.
        /// </summary>
        public static readonly string ProcessControlSetpointTemperature = "process.control.setpoint.temperature";

        /// <summary>
        /// Process Control Setpoint Temperature Filtered.
        /// </summary>
        public static readonly string ProcessControlSetpointTemperatureFiltered = "process.control.setpoint.temperature.filtered";

        /// <summary>
        /// Process Control Setpoint Tf.
        /// </summary>
        public static readonly string ProcessControlSetpointTf = "process.control.setpoint.tf";

        /// <summary>
        /// Process Control Setpoint Voltage.
        /// </summary>
        public static readonly string ProcessControlSetpointVoltage = "process.control.setpoint.voltage";

        /// <summary>
        /// Process Control Setpoint Voltage Filtered.
        /// </summary>
        public static readonly string ProcessControlSetpointVoltageFiltered = "process.control.setpoint.voltage.filtered";

        /// <summary>
        /// Process Control Source.
        /// </summary>
        public static readonly string ProcessControlSource = "process.control.source";

        /// <summary>
        /// Process Control Voltage Max.
        /// </summary>
        public static readonly string ProcessControlVoltageMax = "process.control.voltage.max";

        /// <summary>
        /// Process Control Voltage Min.
        /// </summary>
        public static readonly string ProcessControlVoltageMin = "process.control.voltage.min";

        /// <summary>
        /// Process Current.
        /// </summary>
        public static readonly string ProcessCurrent = "process.current";

        /// <summary>
        /// Process Current Max.
        /// </summary>
        public static readonly string ProcessCurrentMax = "process.current.max";

        /// <summary>
        /// Process Pid Kd.
        /// </summary>
        public static readonly string ProcessPidKd = "process.pid.kd";

        /// <summary>
        /// Process Pid Ki.
        /// </summary>
        public static readonly string ProcessPidKi = "process.pid.ki";

        /// <summary>
        /// Process Pid Kp.
        /// </summary>
        public static readonly string ProcessPidKp = "process.pid.kp";

        /// <summary>
        /// Process Pid Offset.
        /// </summary>
        public static readonly string ProcessPidOffset = "process.pid.offset";

        /// <summary>
        /// Process Startup Mode.
        /// </summary>
        public static readonly string ProcessStartupMode = "process.startup.mode";

        /// <summary>
        /// Process State.
        /// </summary>
        public static readonly string ProcessState = "process.state";

        /// <summary>
        /// Process Voltage.
        /// </summary>
        public static readonly string ProcessVoltage = "process.voltage";

        /// <summary>
        /// Process Voltage On.
        /// </summary>
        public static readonly string ProcessVoltageOn = "process.voltage.on";

        /// <summary>
        /// Process Voltage Start.
        /// </summary>
        public static readonly string ProcessVoltageStart = "process.voltage.start";

        /// <summary>
        /// Process Voltage Start Duration.
        /// </summary>
        public static readonly string ProcessVoltageStartDuration = "process.voltage.start.duration";

        /// <summary>
        /// Process Voltage Tf.
        /// </summary>
        public static readonly string ProcessVoltageTf = "process.voltage.tf";

        /// <summary>
        /// Pvt Numseqs.
        /// </summary>
        public static readonly string PvtNumseqs = "pvt.numseqs";

        /// <summary>
        /// Resolution.
        /// </summary>
        public static readonly string Resolution = "resolution";

        /// <summary>
        /// Scope Channel Size.
        /// </summary>
        public static readonly string ScopeChannelSize = "scope.channel.size";

        /// <summary>
        /// Scope Channel Size Max.
        /// </summary>
        public static readonly string ScopeChannelSizeMax = "scope.channel.size.max";

        /// <summary>
        /// Scope Delay.
        /// </summary>
        public static readonly string ScopeDelay = "scope.delay";

        /// <summary>
        /// Scope Numchannels.
        /// </summary>
        public static readonly string ScopeNumchannels = "scope.numchannels";

        /// <summary>
        /// Scope Timebase.
        /// </summary>
        public static readonly string ScopeTimebase = "scope.timebase";

        /// <summary>
        /// Sensor Temperature 1.
        /// </summary>
        public static readonly string SensorTemperature1 = "sensor.temperature.1";

        /// <summary>
        /// Sensor Temperature 2.
        /// </summary>
        public static readonly string SensorTemperature2 = "sensor.temperature.2";

        /// <summary>
        /// Stream Numbufs.
        /// </summary>
        public static readonly string StreamNumbufs = "stream.numbufs";

        /// <summary>
        /// Stream Numstreams.
        /// </summary>
        public static readonly string StreamNumstreams = "stream.numstreams";

        /// <summary>
        /// System Access.
        /// </summary>
        public static readonly string SystemAccess = "system.access";

        /// <summary>
        /// System Axiscount.
        /// </summary>
        public static readonly string SystemAxiscount = "system.axiscount";

        /// <summary>
        /// System Current.
        /// </summary>
        public static readonly string SystemCurrent = "system.current";

        /// <summary>
        /// System Current Max.
        /// </summary>
        public static readonly string SystemCurrentMax = "system.current.max";

        /// <summary>
        /// System Led Enable.
        /// </summary>
        public static readonly string SystemLedEnable = "system.led.enable";

        /// <summary>
        /// System Serial.
        /// </summary>
        public static readonly string SystemSerial = "system.serial";

        /// <summary>
        /// System Temperature.
        /// </summary>
        public static readonly string SystemTemperature = "system.temperature";

        /// <summary>
        /// System Uptime.
        /// </summary>
        public static readonly string SystemUptime = "system.uptime";

        /// <summary>
        /// System Voltage.
        /// </summary>
        public static readonly string SystemVoltage = "system.voltage";

        /// <summary>
        /// Trigger Numactions.
        /// </summary>
        public static readonly string TriggerNumactions = "trigger.numactions";

        /// <summary>
        /// Trigger Numtriggers.
        /// </summary>
        public static readonly string TriggerNumtriggers = "trigger.numtriggers";

        /// <summary>
        /// User Data 0.
        /// </summary>
        public static readonly string UserData0 = "user.data.0";

        /// <summary>
        /// User Data 1.
        /// </summary>
        public static readonly string UserData1 = "user.data.1";

        /// <summary>
        /// User Data 10.
        /// </summary>
        public static readonly string UserData10 = "user.data.10";

        /// <summary>
        /// User Data 11.
        /// </summary>
        public static readonly string UserData11 = "user.data.11";

        /// <summary>
        /// User Data 12.
        /// </summary>
        public static readonly string UserData12 = "user.data.12";

        /// <summary>
        /// User Data 13.
        /// </summary>
        public static readonly string UserData13 = "user.data.13";

        /// <summary>
        /// User Data 14.
        /// </summary>
        public static readonly string UserData14 = "user.data.14";

        /// <summary>
        /// User Data 15.
        /// </summary>
        public static readonly string UserData15 = "user.data.15";

        /// <summary>
        /// User Data 2.
        /// </summary>
        public static readonly string UserData2 = "user.data.2";

        /// <summary>
        /// User Data 3.
        /// </summary>
        public static readonly string UserData3 = "user.data.3";

        /// <summary>
        /// User Data 4.
        /// </summary>
        public static readonly string UserData4 = "user.data.4";

        /// <summary>
        /// User Data 5.
        /// </summary>
        public static readonly string UserData5 = "user.data.5";

        /// <summary>
        /// User Data 6.
        /// </summary>
        public static readonly string UserData6 = "user.data.6";

        /// <summary>
        /// User Data 7.
        /// </summary>
        public static readonly string UserData7 = "user.data.7";

        /// <summary>
        /// User Data 8.
        /// </summary>
        public static readonly string UserData8 = "user.data.8";

        /// <summary>
        /// User Data 9.
        /// </summary>
        public static readonly string UserData9 = "user.data.9";

        /// <summary>
        /// User Vdata 0.
        /// </summary>
        public static readonly string UserVdata0 = "user.vdata.0";

        /// <summary>
        /// User Vdata 1.
        /// </summary>
        public static readonly string UserVdata1 = "user.vdata.1";

        /// <summary>
        /// User Vdata 2.
        /// </summary>
        public static readonly string UserVdata2 = "user.vdata.2";

        /// <summary>
        /// User Vdata 3.
        /// </summary>
        public static readonly string UserVdata3 = "user.vdata.3";

        /// <summary>
        /// Vel.
        /// </summary>
        public static readonly string Vel = "vel";

        /// <summary>
        /// Version.
        /// </summary>
        public static readonly string Version = "version";

        /// <summary>
        /// Version Build.
        /// </summary>
        public static readonly string VersionBuild = "version.build";

        /// <summary>
        /// Virtual Numvirtual.
        /// </summary>
        public static readonly string VirtualNumvirtual = "virtual.numvirtual";

    }
}

#pragma warning restore CA1708
