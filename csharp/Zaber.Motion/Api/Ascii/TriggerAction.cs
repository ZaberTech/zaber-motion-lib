/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Trigger action identifier.
    /// </summary>
    public enum TriggerAction
    {
        /// <summary>All.</summary>
        All = 0,

        /// <summary>A.</summary>
        A = 1,

        /// <summary>B.</summary>
        B = 2,

    }
}
