/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Operation for trigger action.
    /// </summary>
    public enum TriggerOperation
    {
        /// <summary>SetTo.</summary>
        SetTo = 0,

        /// <summary>IncrementBy.</summary>
        IncrementBy = 1,

        /// <summary>DecrementBy.</summary>
        DecrementBy = 2,

    }
}
