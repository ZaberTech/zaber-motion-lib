/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The public properties of one channel of recorded oscilloscope data.
    /// </summary>
    public class OscilloscopeCaptureProperties : IMessage
    {
        /// <summary>
        /// Indicates whether the data came from a setting or an I/O pin.
        /// </summary>
        [JsonProperty("dataSource")]
        public OscilloscopeDataSource DataSource { get; set; }

        /// <summary>
        /// The name of the recorded setting.
        /// </summary>
        [JsonProperty("setting")]
        public string Setting { get; set; } = string.Empty;

        /// <summary>
        /// The number of the axis the data was recorded from, or 0 for the controller.
        /// </summary>
        [JsonProperty("axisNumber")]
        public int AxisNumber { get; set; }

        /// <summary>
        /// Which kind of I/O port data was recorded from.
        /// </summary>
        [JsonProperty("ioType")]
        public IoPortType IoType { get; set; }

        /// <summary>
        /// Which I/O pin within the port was recorded.
        /// </summary>
        [JsonProperty("ioChannel")]
        public int IoChannel { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is OscilloscopeCaptureProperties))
            {
                return false;
            }

            OscilloscopeCaptureProperties other = (OscilloscopeCaptureProperties)obj;
            return (
                EqualityUtils.CheckEquals(DataSource, other.DataSource) &&
                EqualityUtils.CheckEquals(Setting, other.Setting) &&
                EqualityUtils.CheckEquals(AxisNumber, other.AxisNumber) &&
                EqualityUtils.CheckEquals(IoType, other.IoType) &&
                EqualityUtils.CheckEquals(IoChannel, other.IoChannel)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DataSource));
            hash.Add(EqualityUtils.GenerateHashCode(Setting));
            hash.Add(EqualityUtils.GenerateHashCode(AxisNumber));
            hash.Add(EqualityUtils.GenerateHashCode(IoType));
            hash.Add(EqualityUtils.GenerateHashCode(IoChannel));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => OscilloscopeCaptureProperties.ToByteArray(this);

        internal static OscilloscopeCaptureProperties FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<OscilloscopeCaptureProperties>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(OscilloscopeCaptureProperties instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
