﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to device PVT (Position-Velocity-Time) features.
    /// Requires at least Firmware 7.33.
    /// </summary>
    public class Pvt
    {
        internal Pvt(Device device)
        {
            Device = device;
        }


        /// <summary>
        /// Device that this PVT belongs to.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Gets a PvtSequence class instance which allows you to control a particular PVT sequence on the device.
        /// </summary>
        /// <param name="pvtId">The ID of the PVT sequence to control. The IDs start at 1.</param>
        /// <returns>PvtSequence instance.</returns>
        public PvtSequence GetSequence(int pvtId)
        {
            if (pvtId <= 0)
            {
                throw new ArgumentException("Invalid value; PVT sequences are numbered from 1.");
            }

            return new PvtSequence(Device, pvtId);
        }


        /// <summary>
        /// Gets a PvtBuffer class instance which is a handle for a PVT buffer on the device.
        /// </summary>
        /// <param name="pvtBufferId">The ID of the PVT buffer to control. PVT buffer IDs start at one.</param>
        /// <returns>PvtBuffer instance.</returns>
        public PvtBuffer GetBuffer(int pvtBufferId)
        {
            if (pvtBufferId <= 0)
            {
                throw new ArgumentException("Invalid value; PVT buffers are numbered from 1.");
            }

            return new PvtBuffer(Device, pvtBufferId);
        }


        /// <summary>
        /// Get a list of buffer IDs that are currently in use.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: List of buffer IDs.</returns>
        public async Task<int[]> ListBufferIdsAsync()
        {
            var request = new Requests.StreamBufferList()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Pvt = true,
            };

            var response = await Gateway.CallAsync("device/stream_buffer_list", request, Requests.IntArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Get a list of buffer IDs that are currently in use.
        /// </summary>
        /// <returns>List of buffer IDs.</returns>
        public int[] ListBufferIds()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ListBufferIdsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


    }
}
