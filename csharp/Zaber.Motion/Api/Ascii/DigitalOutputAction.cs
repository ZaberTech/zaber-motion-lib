/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Action type for digital output.
    /// </summary>
    public enum DigitalOutputAction
    {
        /// <summary>Off.</summary>
        Off = 0,

        /// <summary>On.</summary>
        On = 1,

        /// <summary>Toggle.</summary>
        Toggle = 2,

        /// <summary>Keep.</summary>
        Keep = 3,

    }
}
