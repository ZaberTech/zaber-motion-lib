/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Denotes type of the PVT sequence axis.
    /// </summary>
    public enum PvtAxisType
    {
        /// <summary>Physical.</summary>
        Physical = 0,

        /// <summary>Lockstep.</summary>
        Lockstep = 1,

    }
}
