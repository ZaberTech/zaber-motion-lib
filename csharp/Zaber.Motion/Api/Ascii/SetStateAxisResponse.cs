/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// An object containing any non-blocking issues encountered when loading a saved state to an axis.
    /// </summary>
    public class SetStateAxisResponse : IMessage
    {
        /// <summary>
        /// The warnings encountered when applying this state to the given axis.
        /// </summary>
        [JsonProperty("warnings")]
        public string[] Warnings { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The number of the axis that was set.
        /// </summary>
        [JsonProperty("axisNumber")]
        public int AxisNumber { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SetStateAxisResponse))
            {
                return false;
            }

            SetStateAxisResponse other = (SetStateAxisResponse)obj;
            return (
                EqualityUtils.CheckEquals(Warnings, other.Warnings) &&
                EqualityUtils.CheckEquals(AxisNumber, other.AxisNumber)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Warnings));
            hash.Add(EqualityUtils.GenerateHashCode(AxisNumber));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SetStateAxisResponse.ToByteArray(this);

        internal static SetStateAxisResponse FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SetStateAxisResponse>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SetStateAxisResponse instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
