/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// The axis numbers of a lockstep group.
    /// </summary>
    public class LockstepAxes : IMessage
    {
        /// <summary>
        /// The axis number used to set the first axis.
        /// </summary>
        [JsonProperty("axis1")]
        public int Axis1 { get; set; }

        /// <summary>
        /// The axis number used to set the second axis.
        /// </summary>
        [JsonProperty("axis2")]
        public int Axis2 { get; set; }

        /// <summary>
        /// The axis number used to set the third axis.
        /// </summary>
        [JsonProperty("axis3")]
        public int Axis3 { get; set; }

        /// <summary>
        /// The axis number used to set the fourth axis.
        /// </summary>
        [JsonProperty("axis4")]
        public int Axis4 { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is LockstepAxes))
            {
                return false;
            }

            LockstepAxes other = (LockstepAxes)obj;
            return (
                EqualityUtils.CheckEquals(Axis1, other.Axis1) &&
                EqualityUtils.CheckEquals(Axis2, other.Axis2) &&
                EqualityUtils.CheckEquals(Axis3, other.Axis3) &&
                EqualityUtils.CheckEquals(Axis4, other.Axis4)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Axis1));
            hash.Add(EqualityUtils.GenerateHashCode(Axis2));
            hash.Add(EqualityUtils.GenerateHashCode(Axis3));
            hash.Add(EqualityUtils.GenerateHashCode(Axis4));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => LockstepAxes.ToByteArray(this);

        internal static LockstepAxes FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<LockstepAxes>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(LockstepAxes instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
