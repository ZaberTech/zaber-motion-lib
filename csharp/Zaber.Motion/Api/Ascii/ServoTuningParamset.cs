/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Servo Tuning Parameter Set to target.
    /// </summary>
    public enum ServoTuningParamset
    {
        /// <summary>Live.</summary>
        Live = 0,

        /// <summary>P1.</summary>
        P1 = 1,

        /// <summary>P2.</summary>
        P2 = 2,

        /// <summary>P3.</summary>
        P3 = 3,

        /// <summary>P4.</summary>
        P4 = 4,

        /// <summary>P5.</summary>
        P5 = 5,

        /// <summary>P6.</summary>
        P6 = 6,

        /// <summary>P7.</summary>
        P7 = 7,

        /// <summary>P8.</summary>
        P8 = 8,

        /// <summary>P9.</summary>
        P9 = 9,

        /// <summary>Staging.</summary>
        Staging = 10,

        /// <summary>Default.</summary>
        Default = 11,

    }
}
