﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class representing access to particular connection (serial port, TCP connection).
    /// </summary>
#if NET || NETSTANDARD
    public class Connection : IDisposable, IAsyncDisposable
#else
    public class Connection : IDisposable
#endif
    {
        /// <summary>
        /// Delegate used to invoke Connection.Disconnected event.
        /// </summary>
        /// <param name="disconnectionError">Error that caused the disconnection.</param>
        public delegate void DisconnectedHandler(MotionLibException disconnectionError);


        /// <summary>
        /// Event invoked when a response from a device cannot be matched to any known request.
        /// </summary>
        public event Action<UnknownResponseEvent>? UnknownResponse;


        /// <summary>
        /// Event invoked when an alert is received from a device.
        /// </summary>
        public event Action<AlertEvent>? Alert;


        /// <summary>
        /// Event invoked when connection is interrupted or closed.
        /// </summary>
        public event DisconnectedHandler? Disconnected
        {
            add
            {
                lock (_lock)
                {
                    CheckConnected();
                    _disconnected += value;
                }
            }

            remove
            {
                _disconnected -= value;
            }
        }


        /// <summary>
        /// Indicates whether the connection is currently connected.
        /// </summary>
        [Obsolete("No longer supported: This method does not reliably represent the connection state, and will be removed in a future release.")]
        public bool IsConnected
        {
            get
            {
                lock (_lock)
                {
                    return _isConnected;
                }
            }
        }


        /// <summary>
        /// Finalizes an instance of the <see cref="Connection"/> class.
        /// </summary>
        ~Connection()
        {
            Dispose(false);
        }


        internal Connection(int interfaceId)
        {
            InterfaceId = interfaceId;
            Subscribe();
        }


        /// <summary>
        /// Default baud rate for serial connections.
        /// </summary>
        public const int DefaultBaudRate = 115200;


        /// <summary>
        /// Commands sent over this port are forwarded to the device chain.
        /// The bandwidth may be limited as the commands are forwarded over a serial connection.
        /// </summary>
        public const int TcpPortChain = 55550;


        /// <summary>
        /// Local area network share port.
        /// </summary>
        public const int NetworkSharePort = 11421;


        /// <summary>
        /// Commands send over this port are processed only by the device
        /// and not forwarded to the rest of the chain.
        /// Using this port typically makes the communication faster.
        /// </summary>
        public const int TcpPortDeviceOnly = 55551;


        /// <summary>
        /// The interface ID identifies this Connection instance with the underlying library.
        /// </summary>
        public int InterfaceId { get; private set; }


        /// <summary>
        /// The default timeout, in milliseconds, for a device to respond to a request.
        /// Setting the timeout to a too low value may cause request timeout exceptions.
        /// The initial value is 1000 (one second).
        /// </summary>
        public int DefaultRequestTimeout
        {
            get { return RetrieveTimeout(); }
            set { ChangeTimeout(value); }
        }


        /// <summary>
        /// Controls whether outgoing messages contain checksum.
        /// </summary>
        public bool ChecksumEnabled
        {
            get { return RetrieveChecksumEnabled(); }
            set { ChangeChecksumEnabled(value); }
        }


        /// <summary>
        /// Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
        /// Zaber Launcher allows sharing of the port between multiple applications,
        /// If port sharing is not desirable, use the `direct` parameter.
        /// </summary>
        /// <param name="portName">Name of the port to open.</param>
        /// <param name="baudRate">Optional baud rate (defaults to 115200).</param>
        /// <param name="direct">If true will connect to the serial port directly,
        /// failing if the connection is already opened by a message router instance.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the port.</returns>
        public static async Task<Connection> OpenSerialPortAsync(string portName, int baudRate = DefaultBaudRate, bool direct = false)
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.SerialPort,
                PortName = portName,
                BaudRate = baudRate,
                RejectRoutedConnection = direct,
            };

            var response = await Gateway.CallAsync("interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
        /// Zaber Launcher allows sharing of the port between multiple applications,
        /// If port sharing is not desirable, use the `direct` parameter.
        /// </summary>
        /// <param name="portName">Name of the port to open.</param>
        /// <param name="baudRate">Optional baud rate (defaults to 115200).</param>
        /// <param name="direct">If true will connect to the serial port directly,
        /// failing if the connection is already opened by a message router instance.</param>
        /// <returns>An object representing the port.</returns>
        public static Connection OpenSerialPort(string portName, int baudRate = DefaultBaudRate, bool direct = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenSerialPortAsync(portName, baudRate, direct);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Opens a TCP connection.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Optional port number (defaults to 55550).</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the connection.</returns>
        public static async Task<Connection> OpenTcpAsync(string hostName, int port = TcpPortChain)
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.Tcp,
                HostName = hostName,
                Port = port,
            };

            var response = await Gateway.CallAsync("interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a TCP connection.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Optional port number (defaults to 55550).</param>
        /// <returns>An object representing the connection.</returns>
        public static Connection OpenTcp(string hostName, int port = TcpPortChain)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenTcpAsync(hostName, port);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Opens a connection using a custom transport.
        /// </summary>
        /// <param name="transport">The custom connection transport.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the connection.</returns>
        public static async Task<Connection> OpenCustomAsync(Transport transport)
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.Custom,
                Transport = transport.TransportId,
            };

            var response = await Gateway.CallAsync("interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a connection using a custom transport.
        /// </summary>
        /// <param name="transport">The custom connection transport.</param>
        /// <returns>An object representing the connection.</returns>
        public static Connection OpenCustom(Transport transport)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenCustomAsync(transport);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Opens a secured connection to a cloud connected device chain.
        /// Use this method to connect to devices on your account.
        /// </summary>
        /// <param name="cloudId">The cloud ID to connect to.</param>
        /// <param name="token">The token to authenticate with. By default the connection will be unauthenticated.</param>
        /// <param name="connectionName">The name of the connection to open.
        /// Can be left empty to default to the only connection present.
        /// Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.</param>
        /// <param name="realm">The realm to connect to.
        /// Can be left empty for the default account realm.</param>
        /// <param name="api">The URL of the API to receive connection info from.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the connection.</returns>
        public static async Task<Connection> OpenIotAsync(string cloudId, string token = "unauthenticated", string? connectionName = null, string? realm = null, string api = "https://api.zaber.io")
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.Iot,
                CloudId = cloudId,
                Token = token,
                ConnectionName = connectionName,
                Realm = realm,
                Api = api,
            };

            var response = await Gateway.CallAsync("interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a secured connection to a cloud connected device chain.
        /// Use this method to connect to devices on your account.
        /// </summary>
        /// <param name="cloudId">The cloud ID to connect to.</param>
        /// <param name="token">The token to authenticate with. By default the connection will be unauthenticated.</param>
        /// <param name="connectionName">The name of the connection to open.
        /// Can be left empty to default to the only connection present.
        /// Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.</param>
        /// <param name="realm">The realm to connect to.
        /// Can be left empty for the default account realm.</param>
        /// <param name="api">The URL of the API to receive connection info from.</param>
        /// <returns>An object representing the connection.</returns>
        public static Connection OpenIot(string cloudId, string token = "unauthenticated", string? connectionName = null, string? realm = null, string api = "https://api.zaber.io")
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenIotAsync(cloudId, token, connectionName, realm, api);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Opens a connection to Zaber Launcher in your Local Area Network.
        /// The connection is not secured.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Port number.</param>
        /// <param name="connectionName">The name of the connection to open.
        /// Can be left empty to default to the only connection present.
        /// Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the connection.</returns>
        public static async Task<Connection> OpenNetworkShareAsync(string hostName, int port = NetworkSharePort, string? connectionName = null)
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.NetworkShare,
                HostName = hostName,
                Port = port,
                ConnectionName = connectionName,
            };

            var response = await Gateway.CallAsync("interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a connection to Zaber Launcher in your Local Area Network.
        /// The connection is not secured.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Port number.</param>
        /// <param name="connectionName">The name of the connection to open.
        /// Can be left empty to default to the only connection present.
        /// Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.</param>
        /// <returns>An object representing the connection.</returns>
        public static Connection OpenNetworkShare(string hostName, int port = NetworkSharePort, string? connectionName = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenNetworkShareAsync(hostName, port, connectionName);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Response> GenericCommandAsync(string command, int device = 0, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = InterfaceId,
                Command = command,
                Device = device,
                Axis = axis,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command", request, Response.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A response to the command.</returns>
        public Response GenericCommand(string command, int device = 0, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, device, axis, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.
        /// Specifying -1 omits the number completely.</param>
        /// <param name="axis">Optional axis number to send the command to.
        /// Specifying -1 omits the number completely.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(string command, int device = 0, int axis = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = InterfaceId,
                Command = command,
                Device = device,
                Axis = axis,
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.
        /// Specifying -1 omits the number completely.</param>
        /// <param name="axis">Optional axis number to send the command to.
        /// Specifying -1 omits the number completely.</param>
        public void GenericCommandNoResponse(string command, int device = 0, int axis = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command, device, axis);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection and expect multiple responses,
        /// either from one device or from many devices.
        /// Responses are returned in order of arrival.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Response[]> GenericCommandMultiResponseAsync(string command, int device = 0, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = InterfaceId,
                Command = command,
                Device = device,
                Axis = axis,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command_multi_response", request, Requests.GenericCommandResponseCollection.FromByteArray).ConfigureAwait(false);
            return response.Responses;
        }


        /// <summary>
        /// Sends a generic ASCII command to this connection and expect multiple responses,
        /// either from one device or from many devices.
        /// Responses are returned in order of arrival.
        /// For more information refer to the <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="device">Optional device address to send the command to.</param>
        /// <param name="axis">Optional axis number to send the command to.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>All responses to the command.</returns>
        public Response[] GenericCommandMultiResponse(string command, int device = 0, int axis = 0, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, device, axis, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Enables alerts for all devices on the connection.
        /// This will change the "comm.alert" setting to 1 on all supported devices.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EnableAlertsAsync()
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = InterfaceId,
                Command = "set comm.alert 1",
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Enables alerts for all devices on the connection.
        /// This will change the "comm.alert" setting to 1 on all supported devices.
        /// </summary>
        public void EnableAlerts()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EnableAlertsAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disables alerts for all devices on the connection.
        /// This will change the "comm.alert" setting to 0 on all supported devices.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableAlertsAsync()
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = InterfaceId,
                Command = "set comm.alert 0",
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables alerts for all devices on the connection.
        /// This will change the "comm.alert" setting to 0 on all supported devices.
        /// </summary>
        public void DisableAlerts()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableAlertsAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Resets ASCII protocol message IDs. Only for testing purposes.
        /// </summary>
        public void ResetIds()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            Gateway.CallSync("interface/reset_ids", request);
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CloseAsync()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            await Gateway.CallAsync("interface/close", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        public void Close()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CloseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets a Device class instance which allows you to control a particular device on this connection.
        /// Devices are numbered from 1.
        /// </summary>
        /// <param name="deviceAddress">Address of device intended to control. Address is configured for each device.</param>
        /// <returns>Device instance.</returns>
        public Device GetDevice(int deviceAddress)
        {
            if (deviceAddress <= 0)
            {
                throw new ArgumentException("Invalid value; physical devices are numbered from 1.");
            }

            return new Device(this, deviceAddress);
        }


        /// <summary>
        /// Renumbers devices present on this connection. After renumbering, devices need to be identified again.
        /// </summary>
        /// <param name="firstAddress">This is the address that the device closest to the computer is given.
        /// Remaining devices are numbered consecutively.</param>
        /// <returns>A Task that can be awaited to get the result: Total number of devices that responded to the renumber.</returns>
        public async Task<int> RenumberDevicesAsync(int firstAddress = 1)
        {
            if (firstAddress <= 0)
            {
                throw new ArgumentException("Invalid value; device addresses are numbered from 1.");
            }

            var request = new Requests.RenumberRequest()
            {
                InterfaceId = InterfaceId,
                Address = firstAddress,
            };

            var response = await Gateway.CallAsync("device/renumber_all", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Renumbers devices present on this connection. After renumbering, devices need to be identified again.
        /// </summary>
        /// <param name="firstAddress">This is the address that the device closest to the computer is given.
        /// Remaining devices are numbered consecutively.</param>
        /// <returns>Total number of devices that responded to the renumber.</returns>
        public int RenumberDevices(int firstAddress = 1)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = RenumberDevicesAsync(firstAddress);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Attempts to detect any devices present on this connection.
        /// </summary>
        /// <param name="identifyDevices">Determines whether device identification should be performed as well.</param>
        /// <returns>A Task that can be awaited to get the result: Array of detected devices.</returns>
        public async Task<Device[]> DetectDevicesAsync(bool identifyDevices = true)
        {
            var request = new Requests.DeviceDetectRequest()
            {
                InterfaceId = InterfaceId,
                IdentifyDevices = identifyDevices,
            };

            var response = await Gateway.CallAsync("device/detect", request, Requests.DeviceDetectResponse.FromByteArray).ConfigureAwait(false);
            return response.Devices.Select(device => GetDevice(device)).ToArray();
        }


        /// <summary>
        /// Attempts to detect any devices present on this connection.
        /// </summary>
        /// <param name="identifyDevices">Determines whether device identification should be performed as well.</param>
        /// <returns>Array of detected devices.</returns>
        public Device[] DetectDevices(bool identifyDevices = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DetectDevicesAsync(identifyDevices);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Forgets all the information associated with devices on the connection.
        /// Useful when devices are removed from the chain indefinitely.
        /// </summary>
        /// <param name="exceptDevices">Addresses of devices that should not be forgotten.</param>
        public void ForgetDevices(int[] exceptDevices = null!)
        {
            var request = new Requests.ForgetDevicesRequest()
            {
                InterfaceId = InterfaceId,
                ExceptDevices = exceptDevices,
            };

            Gateway.CallSync("device/forget", request);
        }


        /// <summary>
        /// Stops all of the devices on this connection.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether the function should return immediately
        /// or wait until the devices are stopped.</param>
        /// <returns>A Task that can be awaited to get the result: The addresses of the devices that were stopped by this command.</returns>
        public async Task<int[]> StopAllAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceOnAllRequest()
            {
                InterfaceId = InterfaceId,
                WaitUntilIdle = waitUntilIdle,
            };

            var response = await Gateway.CallAsync("device/stop_all", request, Requests.DeviceOnAllResponse.FromByteArray).ConfigureAwait(false);
            return response.DeviceAddresses;
        }


        /// <summary>
        /// Stops all of the devices on this connection.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether the function should return immediately
        /// or wait until the devices are stopped.</param>
        /// <returns>The addresses of the devices that were stopped by this command.</returns>
        public int[] StopAll(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAllAsync(waitUntilIdle);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Homes all of the devices on this connection.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether the function should return immediately
        /// or wait until the devices are homed.</param>
        /// <returns>A Task that can be awaited to get the result: The addresses of the devices that were homed by this command.</returns>
        public async Task<int[]> HomeAllAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.DeviceOnAllRequest()
            {
                InterfaceId = InterfaceId,
                WaitUntilIdle = waitUntilIdle,
            };

            var response = await Gateway.CallAsync("device/home_all", request, Requests.DeviceOnAllResponse.FromByteArray).ConfigureAwait(false);
            return response.DeviceAddresses;
        }


        /// <summary>
        /// Homes all of the devices on this connection.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether the function should return immediately
        /// or wait until the devices are homed.</param>
        /// <returns>The addresses of the devices that were homed by this command.</returns>
        public int[] HomeAll(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAllAsync(waitUntilIdle);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the connection.
        /// </summary>
        /// <returns>A string that represents the connection.</returns>
        public override string ToString()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            var response = Gateway.CallSync("interface/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns default request timeout.
        /// </summary>
        /// <returns>Default request timeout.</returns>
        private int RetrieveTimeout()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            var response = Gateway.CallSync("interface/get_timeout", request, Requests.IntResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets default request timeout.
        /// </summary>
        /// <param name="timeout">Default request timeout.</param>
        private void ChangeTimeout(int timeout)
        {
            var request = new Requests.SetInterfaceTimeoutRequest()
            {
                InterfaceId = InterfaceId,
                Timeout = timeout,
            };

            Gateway.CallSync("interface/set_timeout", request);
        }


        /// <summary>
        /// Returns checksum enabled.
        /// </summary>
        /// <returns>Checksum enabled.</returns>
        private bool RetrieveChecksumEnabled()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            var response = Gateway.CallSync("interface/get_checksum_enabled", request, Requests.BoolResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets checksum enabled.
        /// </summary>
        /// <param name="isEnabled">Checksum enabled.</param>
        private void ChangeChecksumEnabled(bool isEnabled)
        {
            var request = new Requests.SetInterfaceChecksumEnabledRequest()
            {
                InterfaceId = InterfaceId,
                IsEnabled = isEnabled,
            };

            Gateway.CallSync("interface/set_checksum_enabled", request);
        }


#pragma warning disable SA1202
        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#pragma warning restore SA1202


        /// <summary>
        /// Dispose pattern function.
        /// </summary>
        /// <param name="disposing">True when function is called from Dispose().</param>
        protected virtual void Dispose(bool disposing)
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            Close();
        }


#if NET || NETSTANDARD
#pragma warning disable SA1202
        /// <summary>
        /// IAsyncDisposable implementation.
        /// </summary>
        /// <returns>A task meant to be awaited by the "await using" statement.</returns>
        public async ValueTask DisposeAsync()
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            await DisposeAsyncCore().ConfigureAwait(false);
            await CloseAsync().ConfigureAwait(false);

#pragma warning disable CA1816
            GC.SuppressFinalize(this);
#pragma warning restore CA1816
        }
#pragma warning restore SA1202


        /// <summary>
        /// Enables subclasses to dispose of their own resources.
        /// </summary>
        /// <returns>Immediately.</returns>
        protected virtual async ValueTask DisposeAsyncCore()
            => await Task.FromResult(0).ConfigureAwait(false);
#endif


        private void CheckConnected()
        {
            if (!_isConnected)
            {
                throw new InvalidOperationException("Connection is already closed.");
            }
        }


        private void Subscribe()
        {
            Events.UnknownResponse += Events_UnknownResponse;
            Events.Alert += Events_Alert;
            Events.Disconnected += Events_Disconnected;
        }


        private void Unsubscribe()
        {
            Events.Disconnected -= Events_Disconnected;
            Events.UnknownResponse -= Events_UnknownResponse;
            Events.Alert -= Events_Alert;
        }


        private void Events_UnknownResponse(Requests.UnknownResponseEventWrapper replyEvent)
        {
            if (replyEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            UnknownResponse?.Invoke(replyEvent.UnknownResponse);
        }

        private void Events_Alert(Requests.AlertEventWrapper replyEvent)
        {
            if (replyEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            Alert?.Invoke(replyEvent.Alert);
        }

        private void Events_Disconnected(Requests.DisconnectedEvent disconnectedEvent)
        {
            if (disconnectedEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            lock (_lock)
            {
                _isConnected = false;
            }

            Unsubscribe();
            _disconnected?.Invoke(Zaber.Motion.Exceptions.ExceptionConverter.Convert(disconnectedEvent.ErrorType, disconnectedEvent.ErrorMessage));
        }


        private readonly object _lock = new object();
        private int _disposed;
        private bool _isConnected = true;
        private DisconnectedHandler? _disconnected;
    }
}
