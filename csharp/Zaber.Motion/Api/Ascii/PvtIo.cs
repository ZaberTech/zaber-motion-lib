﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Class providing access to I/O for a PVT sequence.
    /// </summary>
    public class PvtIo
    {
        internal PvtIo(Device device, int streamId)
        {
            _device = device;
            _streamId = streamId;
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetDigitalOutputAsync(int channelNumber, DigitalOutputAction value)
        {
            var request = new Requests.StreamSetDigitalOutputRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_digital_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified digital output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform on the channel.</param>
        public void SetDigitalOutput(int channelNumber, DigitalOutputAction value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllDigitalOutputsAsync(DigitalOutputAction[] values)
        {
            var request = new Requests.StreamSetAllDigitalOutputsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_digital_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all digital output channels.
        /// </summary>
        /// <param name="values">The type of action to perform on the channel.</param>
        public void SetAllDigitalOutputs(DigitalOutputAction[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAnalogOutputAsync(int channelNumber, double value)
        {
            var request = new Requests.StreamSetAnalogOutputRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
            };

            await Gateway.CallAsync("device/stream_set_analog_output", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets value for the specified analog output channel.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to.</param>
        public void SetAnalogOutput(int channelNumber, double value)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputAsync(channelNumber, value);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllAnalogOutputsAsync(double[] values)
        {
            var request = new Requests.StreamSetAllAnalogOutputsRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                Values = values,
            };

            await Gateway.CallAsync("device/stream_set_all_analog_outputs", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets values for all analog output channels.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to.</param>
        public void SetAllAnalogOutputs(double[] values)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsAsync(values);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future value for the specified digital output channel.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform immediately on the channel.</param>
        /// <param name="futureValue">The type of action to perform in the future on the channel.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetDigitalOutputScheduleAsync(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.StreamSetDigitalOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
                FutureValue = futureValue,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_digital_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future value for the specified digital output channel.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">The type of action to perform immediately on the channel.</param>
        /// <param name="futureValue">The type of action to perform in the future on the channel.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        public void SetDigitalOutputSchedule(int channelNumber, DigitalOutputAction value, DigitalOutputAction futureValue, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future values for all digital output channels.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="values">The type of actions to perform immediately on output channels.</param>
        /// <param name="futureValues">The type of actions to perform in the future on output channels.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllDigitalOutputsScheduleAsync(DigitalOutputAction[] values, DigitalOutputAction[] futureValues, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.StreamSetAllDigitalOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                Values = values,
                FutureValues = futureValues,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_all_digital_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future values for all digital output channels.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="values">The type of actions to perform immediately on output channels.</param>
        /// <param name="futureValues">The type of actions to perform in the future on output channels.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAllDigitalOutputsSchedule(DigitalOutputAction[] values, DigitalOutputAction[] futureValues, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllDigitalOutputsScheduleAsync(values, futureValues, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future value for the specified analog output channel.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to immediately.</param>
        /// <param name="futureValue">Value to set the output channel voltage to in the future.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAnalogOutputScheduleAsync(int channelNumber, double value, double futureValue, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.StreamSetAnalogOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
                Value = value,
                FutureValue = futureValue,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_analog_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future value for the specified analog output channel.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <param name="value">Value to set the output channel voltage to immediately.</param>
        /// <param name="futureValue">Value to set the output channel voltage to in the future.</param>
        /// <param name="delay">Delay between setting current value and setting future value.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAnalogOutputSchedule(int channelNumber, double value, double futureValue, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets current and future values for all analog output channels.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to immediately.</param>
        /// <param name="futureValues">Voltage values to set the output channels to in the future.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAllAnalogOutputsScheduleAsync(double[] values, double[] futureValues, double delay, Units unit = Units.Native)
        {
            if (delay <= 0)
            {
                throw new ArgumentException("Delay must be a positive value.");
            }

            var request = new Requests.StreamSetAllAnalogOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                StreamId = _streamId,
                Pvt = true,
                Values = values,
                FutureValues = futureValues,
                Delay = delay,
                Unit = unit,
            };

            await Gateway.CallAsync("device/stream_set_all_analog_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets current and future values for all analog output channels.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="values">Voltage values to set the output channels to immediately.</param>
        /// <param name="futureValues">Voltage values to set the output channels to in the future.</param>
        /// <param name="delay">Delay between setting current values and setting future values.</param>
        /// <param name="unit">Units of time.</param>
        public void SetAllAnalogOutputsSchedule(double[] values, double[] futureValues, double delay, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAllAnalogOutputsScheduleAsync(values, futureValues, delay, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancels a scheduled digital output action.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelDigitalOutputScheduleAsync(int channelNumber)
        {
            var request = new Requests.StreamCancelOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = false,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
            };

            await Gateway.CallAsync("device/stream_cancel_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancels a scheduled digital output action.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        public void CancelDigitalOutputSchedule(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelDigitalOutputScheduleAsync(channelNumber);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancel all scheduled digital output actions.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled digital output action for that channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAllDigitalOutputsScheduleAsync(bool[] channels = null!)
        {
            var request = new Requests.StreamCancelAllOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = false,
                StreamId = _streamId,
                Pvt = true,
                Channels = channels,
            };

            await Gateway.CallAsync("device/stream_cancel_all_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancel all scheduled digital output actions.
        /// Requires at least Firmware 7.37.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled digital output action for that channel.</param>
        public void CancelAllDigitalOutputsSchedule(bool[] channels = null!)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAllDigitalOutputsScheduleAsync(channels);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancels a scheduled analog output value.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAnalogOutputScheduleAsync(int channelNumber)
        {
            var request = new Requests.StreamCancelOutputScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = true,
                StreamId = _streamId,
                Pvt = true,
                ChannelNumber = channelNumber,
            };

            await Gateway.CallAsync("device/stream_cancel_output_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancels a scheduled analog output value.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channelNumber">Channel number starting at 1.</param>
        public void CancelAnalogOutputSchedule(int channelNumber)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAnalogOutputScheduleAsync(channelNumber);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Cancel all scheduled analog output actions.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled analog output value for that channel.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CancelAllAnalogOutputsScheduleAsync(bool[] channels = null!)
        {
            var request = new Requests.StreamCancelAllOutputsScheduleRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Analog = true,
                StreamId = _streamId,
                Pvt = true,
                Channels = channels,
            };

            await Gateway.CallAsync("device/stream_cancel_all_outputs_schedule", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Cancel all scheduled analog output actions.
        /// Requires at least Firmware 7.38.
        /// </summary>
        /// <param name="channels">Optionally specify which channels to cancel.
        /// Array length must be empty or equal to the number of channels on device.
        /// Specifying "True" for a channel will cancel the scheduled analog output value for that channel.</param>
        public void CancelAllAnalogOutputsSchedule(bool[] channels = null!)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CancelAllAnalogOutputsScheduleAsync(channels);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Device _device;


        private int _streamId;


    }
}
