﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma warning disable CA1708 // We cannot differentiate PeripheralId from Peripheralid without removing either of them.

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Warning flag constants that indicate whether any device fault or warning is active.
    /// For more information please refer to the
    /// <see href="https://www.zaber.com/protocol-manual#topic_message_format_warning_flags">ASCII Protocol Manual</see>.
    /// </summary>
    public static class WarningFlags
    {
        /// <summary>
        /// Critical System Error.
        /// </summary>
        public static readonly string CriticalSystemError = "FF";

        /// <summary>
        /// Peripheral Not Supported.
        /// </summary>
        public static readonly string PeripheralNotSupported = "FN";

        /// <summary>
        /// Peripheral Inactive.
        /// </summary>
        public static readonly string PeripheralInactive = "FZ";

        /// <summary>
        /// Hardware Emergency Stop Driver Disabled.
        /// </summary>
        public static readonly string HardwareEmergencyStop = "FH";

        /// <summary>
        /// Overvoltage or Undervoltage Driver Disabled.
        /// </summary>
        public static readonly string OvervoltageOrUndervoltage = "FV";

        /// <summary>
        /// Driver Disabled on Startup or by Command.
        /// Devices with Firmware 7.11 and above.
        /// </summary>
        public static readonly string DriverDisabledNoFault = "FO";

        /// <summary>
        /// Current Inrush Error.
        /// </summary>
        public static readonly string CurrentInrushError = "FC";

        /// <summary>
        /// Motor Temperature Error.
        /// </summary>
        public static readonly string MotorTemperatureError = "FM";

        /// <summary>
        /// Driver Disabled.
        /// Devices with Firmware 7.10 and lower.
        /// </summary>
        public static readonly string DriverDisabled = "FD";

        /// <summary>
        /// Encoder Error.
        /// </summary>
        public static readonly string EncoderError = "FQ";

        /// <summary>
        /// Index Error.
        /// </summary>
        public static readonly string IndexError = "FI";

        /// <summary>
        /// Analog Encoder Sync Error.
        /// </summary>
        public static readonly string AnalogEncoderSyncError = "FA";

        /// <summary>
        /// Overdrive Limit Exceeded.
        /// </summary>
        public static readonly string OverdriveLimitExceeded = "FR";

        /// <summary>
        /// Stalled and Stopped.
        /// </summary>
        public static readonly string StalledAndStopped = "FS";

        /// <summary>
        /// Stream Bounds Error.
        /// </summary>
        public static readonly string StreamBoundsError = "FB";

        /// <summary>
        /// Interpolated Path Deviation.
        /// </summary>
        public static readonly string InterpolatedPathDeviation = "FP";

        /// <summary>
        /// Limit Error.
        /// </summary>
        public static readonly string LimitError = "FE";

        /// <summary>
        /// Excessive Twist.
        /// </summary>
        public static readonly string ExcessiveTwist = "FT";

        /// <summary>
        /// Unexpected Limit Trigger.
        /// </summary>
        public static readonly string UnexpectedLimitTrigger = "WL";

        /// <summary>
        /// Voltage Out of Range.
        /// </summary>
        public static readonly string VoltageOutOfRange = "WV";

        /// <summary>
        /// Controller Temperature High.
        /// </summary>
        public static readonly string ControllerTemperatureHigh = "WT";

        /// <summary>
        /// Stalled with Recovery.
        /// </summary>
        public static readonly string StalledWithRecovery = "WS";

        /// <summary>
        /// Displaced When Stationary.
        /// </summary>
        public static readonly string DisplacedWhenStationary = "WM";

        /// <summary>
        /// Invalid Calibration Type.
        /// </summary>
        public static readonly string InvalidCalibrationType = "WP";

        /// <summary>
        /// No Reference Position.
        /// </summary>
        public static readonly string NoReferencePosition = "WR";

        /// <summary>
        /// Device Not Homed.
        /// </summary>
        public static readonly string DeviceNotHomed = "WH";

        /// <summary>
        /// Manual Control.
        /// </summary>
        public static readonly string ManualControl = "NC";

        /// <summary>
        /// Movement Interrupted.
        /// </summary>
        public static readonly string MovementInterrupted = "NI";

        /// <summary>
        /// Stream Discontinuity.
        /// </summary>
        public static readonly string StreamDiscontinuity = "ND";

        /// <summary>
        /// Value Rounded.
        /// </summary>
        public static readonly string ValueRounded = "NR";

        /// <summary>
        /// Value Truncated.
        /// </summary>
        public static readonly string ValueTruncated = "NT";

        /// <summary>
        /// Setting Update Pending.
        /// </summary>
        public static readonly string SettingUpdatePending = "NU";

        /// <summary>
        /// Joystick Calibrating.
        /// </summary>
        public static readonly string JoystickCalibrating = "NJ";

        /// <summary>
        /// Device in Firmware Update Mode.
        /// Firmware 6.xx only.
        /// </summary>
        public static readonly string FirmwareUpdateMode = "NB";

    }
}

#pragma warning restore CA1708
