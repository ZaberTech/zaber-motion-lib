/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Kind of channel to record in the Oscilloscope.
    /// </summary>
    public enum OscilloscopeDataSource
    {
        /// <summary>Setting.</summary>
        Setting = 0,

        /// <summary>Io.</summary>
        Io = 1,

    }
}
