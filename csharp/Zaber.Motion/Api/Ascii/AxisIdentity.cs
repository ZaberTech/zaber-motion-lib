/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Ascii
{
    /// <summary>
    /// Representation of data gathered during axis identification.
    /// </summary>
    public class AxisIdentity : IMessage
    {
        /// <summary>
        /// Unique ID of the peripheral hardware.
        /// </summary>
        [JsonProperty("peripheralId")]
        public int PeripheralId { get; set; }

        /// <summary>
        /// Name of the peripheral.
        /// </summary>
        [JsonProperty("peripheralName")]
        public string PeripheralName { get; set; } = string.Empty;

        /// <summary>
        /// Serial number of the peripheral, or 0 when not applicable.
        /// </summary>
        [JsonProperty("peripheralSerialNumber")]
        public long PeripheralSerialNumber { get; set; }

        /// <summary>
        /// Indicates whether the axis is a peripheral or part of an integrated device.
        /// </summary>
        [JsonProperty("isPeripheral")]
        public bool IsPeripheral { get; set; }

        /// <summary>
        /// Determines the type of an axis and units it accepts.
        /// </summary>
        [JsonProperty("axisType")]
        public AxisType AxisType { get; set; }

        /// <summary>
        /// The peripheral has hardware modifications.
        /// </summary>
        [JsonProperty("isModified")]
        public bool IsModified { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AxisIdentity))
            {
                return false;
            }

            AxisIdentity other = (AxisIdentity)obj;
            return (
                EqualityUtils.CheckEquals(PeripheralId, other.PeripheralId) &&
                EqualityUtils.CheckEquals(PeripheralName, other.PeripheralName) &&
                EqualityUtils.CheckEquals(PeripheralSerialNumber, other.PeripheralSerialNumber) &&
                EqualityUtils.CheckEquals(IsPeripheral, other.IsPeripheral) &&
                EqualityUtils.CheckEquals(AxisType, other.AxisType) &&
                EqualityUtils.CheckEquals(IsModified, other.IsModified)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralId));
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralName));
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralSerialNumber));
            hash.Add(EqualityUtils.GenerateHashCode(IsPeripheral));
            hash.Add(EqualityUtils.GenerateHashCode(AxisType));
            hash.Add(EqualityUtils.GenerateHashCode(IsModified));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AxisIdentity.ToByteArray(this);

        internal static AxisIdentity FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AxisIdentity>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AxisIdentity instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
