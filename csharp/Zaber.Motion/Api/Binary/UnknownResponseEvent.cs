/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Reply that could not be matched to a request.
    /// </summary>
    public class UnknownResponseEvent : IMessage
    {
        /// <summary>
        /// Number of the device that sent or should receive the message.
        /// </summary>
        [JsonProperty("deviceAddress")]
        public int DeviceAddress { get; set; }

        /// <summary>
        /// The warning flag contains the highest priority warning currently active for the device or axis.
        /// </summary>
        [JsonProperty("command")]
        public int Command { get; set; }

        /// <summary>
        /// Data payload of the message, if applicable, or zero otherwise.
        /// </summary>
        [JsonProperty("data")]
        public int Data { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is UnknownResponseEvent))
            {
                return false;
            }

            UnknownResponseEvent other = (UnknownResponseEvent)obj;
            return (
                EqualityUtils.CheckEquals(DeviceAddress, other.DeviceAddress) &&
                EqualityUtils.CheckEquals(Command, other.Command) &&
                EqualityUtils.CheckEquals(Data, other.Data)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DeviceAddress));
            hash.Add(EqualityUtils.GenerateHashCode(Command));
            hash.Add(EqualityUtils.GenerateHashCode(Data));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => UnknownResponseEvent.ToByteArray(this);

        internal static UnknownResponseEvent FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<UnknownResponseEvent>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(UnknownResponseEvent instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
