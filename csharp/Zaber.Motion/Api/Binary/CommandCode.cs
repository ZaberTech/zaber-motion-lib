/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Named constants for all Zaber Binary protocol commands.
    /// </summary>
    public enum CommandCode
    {
        /// <summary>Reset.</summary>
        Reset = 0,

        /// <summary>Home.</summary>
        Home = 1,

        /// <summary>Renumber.</summary>
        Renumber = 2,

        /// <summary>StoreCurrentPosition.</summary>
        StoreCurrentPosition = 16,

        /// <summary>ReturnStoredPosition.</summary>
        ReturnStoredPosition = 17,

        /// <summary>MoveToStoredPosition.</summary>
        MoveToStoredPosition = 18,

        /// <summary>MoveAbsolute.</summary>
        MoveAbsolute = 20,

        /// <summary>MoveRelative.</summary>
        MoveRelative = 21,

        /// <summary>MoveAtConstantSpeed.</summary>
        MoveAtConstantSpeed = 22,

        /// <summary>Stop.</summary>
        Stop = 23,

        /// <summary>SetActiveAxis.</summary>
        SetActiveAxis = 25,

        /// <summary>SetAxisDeviceNumber.</summary>
        SetAxisDeviceNumber = 26,

        /// <summary>SetAxisInversion.</summary>
        SetAxisInversion = 27,

        /// <summary>SetAxisVelocityProfile.</summary>
        SetAxisVelocityProfile = 28,

        /// <summary>SetAxisVelocityScale.</summary>
        SetAxisVelocityScale = 29,

        /// <summary>LoadEventInstruction.</summary>
        LoadEventInstruction = 30,

        /// <summary>ReturnEventInstruction.</summary>
        ReturnEventInstruction = 31,

        /// <summary>SetCalibrationMode.</summary>
        SetCalibrationMode = 33,

        /// <summary>SetJoystickCalibrationMode.</summary>
        SetJoystickCalibrationMode = 33,

        /// <summary>ReadOrWriteMemory.</summary>
        ReadOrWriteMemory = 35,

        /// <summary>RestoreSettings.</summary>
        RestoreSettings = 36,

        /// <summary>SetMicrostepResolution.</summary>
        SetMicrostepResolution = 37,

        /// <summary>SetRunningCurrent.</summary>
        SetRunningCurrent = 38,

        /// <summary>SetHoldCurrent.</summary>
        SetHoldCurrent = 39,

        /// <summary>SetDeviceMode.</summary>
        SetDeviceMode = 40,

        /// <summary>SetHomeSpeed.</summary>
        SetHomeSpeed = 41,

        /// <summary>SetStartSpeed.</summary>
        SetStartSpeed = 41,

        /// <summary>SetTargetSpeed.</summary>
        SetTargetSpeed = 42,

        /// <summary>SetAcceleration.</summary>
        SetAcceleration = 43,

        /// <summary>SetMaximumPosition.</summary>
        SetMaximumPosition = 44,

        /// <summary>SetCurrentPosition.</summary>
        SetCurrentPosition = 45,

        /// <summary>SetMaximumRelativeMove.</summary>
        SetMaximumRelativeMove = 46,

        /// <summary>SetHomeOffset.</summary>
        SetHomeOffset = 47,

        /// <summary>SetAliasNumber.</summary>
        SetAliasNumber = 48,

        /// <summary>SetLockState.</summary>
        SetLockState = 49,

        /// <summary>ReturnDeviceID.</summary>
        ReturnDeviceID = 50,

        /// <summary>ReturnFirmwareVersion.</summary>
        ReturnFirmwareVersion = 51,

        /// <summary>ReturnPowerSupplyVoltage.</summary>
        ReturnPowerSupplyVoltage = 52,

        /// <summary>ReturnSetting.</summary>
        ReturnSetting = 53,

        /// <summary>ReturnStatus.</summary>
        ReturnStatus = 54,

        /// <summary>EchoData.</summary>
        EchoData = 55,

        /// <summary>ReturnFirmwareBuild.</summary>
        ReturnFirmwareBuild = 56,

        /// <summary>ReturnCurrentPosition.</summary>
        ReturnCurrentPosition = 60,

        /// <summary>ReturnSerialNumber.</summary>
        ReturnSerialNumber = 63,

        /// <summary>SetParkState.</summary>
        SetParkState = 65,

        /// <summary>SetPeripheralID.</summary>
        SetPeripheralID = 66,

        /// <summary>ReturnDigitalInputCount.</summary>
        ReturnDigitalInputCount = 67,

        /// <summary>ReadDigitalInput.</summary>
        ReadDigitalInput = 68,

        /// <summary>ReadAllDigitalInputs.</summary>
        ReadAllDigitalInputs = 69,

        /// <summary>ReturnDigitalOutputCount.</summary>
        ReturnDigitalOutputCount = 70,

        /// <summary>ReadDigitalOutput.</summary>
        ReadDigitalOutput = 71,

        /// <summary>ReadAllDigitalOutputs.</summary>
        ReadAllDigitalOutputs = 72,

        /// <summary>WriteDigitalOutput.</summary>
        WriteDigitalOutput = 73,

        /// <summary>WriteAllDigitalOutputs.</summary>
        WriteAllDigitalOutputs = 74,

        /// <summary>ReturnAnalogInputCount.</summary>
        ReturnAnalogInputCount = 75,

        /// <summary>ReadAnalogInput.</summary>
        ReadAnalogInput = 76,

        /// <summary>ReturnAnalogOutputCount.</summary>
        ReturnAnalogOutputCount = 77,

        /// <summary>MoveIndex.</summary>
        MoveIndex = 78,

        /// <summary>SetIndexDistance.</summary>
        SetIndexDistance = 79,

        /// <summary>SetCycleDistance.</summary>
        SetCycleDistance = 80,

        /// <summary>SetFilterHolderID.</summary>
        SetFilterHolderID = 81,

        /// <summary>ReturnEncoderCount.</summary>
        ReturnEncoderCount = 82,

        /// <summary>ReturnCalibratedEncoderCount.</summary>
        ReturnCalibratedEncoderCount = 83,

        /// <summary>ReturnCalibrationType.</summary>
        ReturnCalibrationType = 84,

        /// <summary>ReturnCalibrationError.</summary>
        ReturnCalibrationError = 85,

        /// <summary>ReturnPeripheralSerialNumber.</summary>
        ReturnPeripheralSerialNumber = 86,

        /// <summary>SetPeripheralSerialNumber.</summary>
        SetPeripheralSerialNumber = 86,

        /// <summary>ForceAbsolute.</summary>
        ForceAbsolute = 87,

        /// <summary>ForceOff.</summary>
        ForceOff = 88,

        /// <summary>ReturnEncoderPosition.</summary>
        ReturnEncoderPosition = 89,

        /// <summary>ReturnPeripheralIDPending.</summary>
        ReturnPeripheralIDPending = 91,

        /// <summary>ReturnPeripheralSerialPending.</summary>
        ReturnPeripheralSerialPending = 92,

        /// <summary>Activate.</summary>
        Activate = 93,

        /// <summary>SetAutoReplyDisabledMode.</summary>
        SetAutoReplyDisabledMode = 101,

        /// <summary>SetMessageIDMode.</summary>
        SetMessageIDMode = 102,

        /// <summary>SetHomeStatus.</summary>
        SetHomeStatus = 103,

        /// <summary>SetHomeSensorType.</summary>
        SetHomeSensorType = 104,

        /// <summary>SetAutoHomeDisabledMode.</summary>
        SetAutoHomeDisabledMode = 105,

        /// <summary>SetMinimumPosition.</summary>
        SetMinimumPosition = 106,

        /// <summary>SetKnobDisabledMode.</summary>
        SetKnobDisabledMode = 107,

        /// <summary>SetKnobDirection.</summary>
        SetKnobDirection = 108,

        /// <summary>SetKnobMovementMode.</summary>
        SetKnobMovementMode = 109,

        /// <summary>SetKnobJogSize.</summary>
        SetKnobJogSize = 110,

        /// <summary>SetKnobVelocityScale.</summary>
        SetKnobVelocityScale = 111,

        /// <summary>SetKnobVelocityProfile.</summary>
        SetKnobVelocityProfile = 112,

        /// <summary>SetAccelerationOnly.</summary>
        SetAccelerationOnly = 113,

        /// <summary>SetDecelerationOnly.</summary>
        SetDecelerationOnly = 114,

        /// <summary>SetMoveTrackingMode.</summary>
        SetMoveTrackingMode = 115,

        /// <summary>SetManualMoveTrackingDisabledMode.</summary>
        SetManualMoveTrackingDisabledMode = 116,

        /// <summary>SetMoveTrackingPeriod.</summary>
        SetMoveTrackingPeriod = 117,

        /// <summary>SetClosedLoopMode.</summary>
        SetClosedLoopMode = 118,

        /// <summary>SetSlipTrackingPeriod.</summary>
        SetSlipTrackingPeriod = 119,

        /// <summary>SetStallTimeout.</summary>
        SetStallTimeout = 120,

        /// <summary>SetDeviceDirection.</summary>
        SetDeviceDirection = 121,

        /// <summary>SetBaudRate.</summary>
        SetBaudRate = 122,

        /// <summary>SetProtocol.</summary>
        SetProtocol = 123,

        /// <summary>ConvertToASCII.</summary>
        ConvertToASCII = 124,

    }
}
