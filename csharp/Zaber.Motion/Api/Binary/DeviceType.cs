/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Denotes type of an device and units it accepts.
    /// </summary>
    public enum DeviceType
    {
        /// <summary>Unknown.</summary>
        Unknown = 0,

        /// <summary>Linear.</summary>
        Linear = 1,

        /// <summary>Rotary.</summary>
        Rotary = 2,

    }
}
