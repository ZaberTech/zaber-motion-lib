/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Named constants for all Zaber Binary protocol reply-only command codes.
    /// </summary>
    public enum ReplyCode
    {
        /// <summary>MoveTracking.</summary>
        MoveTracking = 8,

        /// <summary>LimitActive.</summary>
        LimitActive = 9,

        /// <summary>ManualMoveTracking.</summary>
        ManualMoveTracking = 10,

        /// <summary>ManualMove.</summary>
        ManualMove = 11,

        /// <summary>SlipTracking.</summary>
        SlipTracking = 12,

        /// <summary>UnexpectedPosition.</summary>
        UnexpectedPosition = 13,

        /// <summary>Error.</summary>
        Error = 255,

    }
}
