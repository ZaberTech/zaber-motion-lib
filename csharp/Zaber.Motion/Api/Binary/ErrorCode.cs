/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Named constants for all Zaber Binary protocol error codes.
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>CannotHome.</summary>
        CannotHome = 1,

        /// <summary>DeviceNumberInvalid.</summary>
        DeviceNumberInvalid = 2,

        /// <summary>AddressInvalid.</summary>
        AddressInvalid = 5,

        /// <summary>VoltageLow.</summary>
        VoltageLow = 14,

        /// <summary>VoltageHigh.</summary>
        VoltageHigh = 15,

        /// <summary>StoredPositionInvalid.</summary>
        StoredPositionInvalid = 18,

        /// <summary>AbsolutePositionInvalid.</summary>
        AbsolutePositionInvalid = 20,

        /// <summary>RelativePositionInvalid.</summary>
        RelativePositionInvalid = 21,

        /// <summary>VelocityInvalid.</summary>
        VelocityInvalid = 22,

        /// <summary>AxisInvalid.</summary>
        AxisInvalid = 25,

        /// <summary>AxisDeviceNumberInvalid.</summary>
        AxisDeviceNumberInvalid = 26,

        /// <summary>InversionInvalid.</summary>
        InversionInvalid = 27,

        /// <summary>VelocityProfileInvalid.</summary>
        VelocityProfileInvalid = 28,

        /// <summary>VelocityScaleInvalid.</summary>
        VelocityScaleInvalid = 29,

        /// <summary>LoadEventInvalid.</summary>
        LoadEventInvalid = 30,

        /// <summary>ReturnEventInvalid.</summary>
        ReturnEventInvalid = 31,

        /// <summary>JoystickCalibrationModeInvalid.</summary>
        JoystickCalibrationModeInvalid = 33,

        /// <summary>PeripheralIDInvalid.</summary>
        PeripheralIDInvalid = 36,

        /// <summary>ResolutionInvalid.</summary>
        ResolutionInvalid = 37,

        /// <summary>RunCurrentInvalid.</summary>
        RunCurrentInvalid = 38,

        /// <summary>HoldCurrentInvalid.</summary>
        HoldCurrentInvalid = 39,

        /// <summary>ModeInvalid.</summary>
        ModeInvalid = 40,

        /// <summary>HomeSpeedInvalid.</summary>
        HomeSpeedInvalid = 41,

        /// <summary>SpeedInvalid.</summary>
        SpeedInvalid = 42,

        /// <summary>AccelerationInvalid.</summary>
        AccelerationInvalid = 43,

        /// <summary>MaximumPositionInvalid.</summary>
        MaximumPositionInvalid = 44,

        /// <summary>CurrentPositionInvalid.</summary>
        CurrentPositionInvalid = 45,

        /// <summary>MaximumRelativeMoveInvalid.</summary>
        MaximumRelativeMoveInvalid = 46,

        /// <summary>OffsetInvalid.</summary>
        OffsetInvalid = 47,

        /// <summary>AliasInvalid.</summary>
        AliasInvalid = 48,

        /// <summary>LockStateInvalid.</summary>
        LockStateInvalid = 49,

        /// <summary>DeviceIDUnknown.</summary>
        DeviceIDUnknown = 50,

        /// <summary>SettingInvalid.</summary>
        SettingInvalid = 53,

        /// <summary>CommandInvalid.</summary>
        CommandInvalid = 64,

        /// <summary>ParkStateInvalid.</summary>
        ParkStateInvalid = 65,

        /// <summary>TemperatureHigh.</summary>
        TemperatureHigh = 67,

        /// <summary>DigitalInputPinInvalid.</summary>
        DigitalInputPinInvalid = 68,

        /// <summary>DigitalOutputPinInvalid.</summary>
        DigitalOutputPinInvalid = 71,

        /// <summary>DigitalOutputMaskInvalid.</summary>
        DigitalOutputMaskInvalid = 74,

        /// <summary>AnalogInputPinInvalid.</summary>
        AnalogInputPinInvalid = 76,

        /// <summary>MoveIndexNumberInvalid.</summary>
        MoveIndexNumberInvalid = 78,

        /// <summary>IndexDistanceInvalid.</summary>
        IndexDistanceInvalid = 79,

        /// <summary>CycleDistanceInvalid.</summary>
        CycleDistanceInvalid = 80,

        /// <summary>FilterHolderIDInvalid.</summary>
        FilterHolderIDInvalid = 81,

        /// <summary>AbsoluteForceInvalid.</summary>
        AbsoluteForceInvalid = 87,

        /// <summary>AutoReplyDisabledModeInvalid.</summary>
        AutoReplyDisabledModeInvalid = 101,

        /// <summary>MessageIDModeInvalid.</summary>
        MessageIDModeInvalid = 102,

        /// <summary>HomeStatusInvalid.</summary>
        HomeStatusInvalid = 103,

        /// <summary>HomeSensorTypeInvalid.</summary>
        HomeSensorTypeInvalid = 104,

        /// <summary>AutoHomeDisabledModeInvalid.</summary>
        AutoHomeDisabledModeInvalid = 105,

        /// <summary>MinimumPositionInvalid.</summary>
        MinimumPositionInvalid = 106,

        /// <summary>KnobDisabledModeInvalid.</summary>
        KnobDisabledModeInvalid = 107,

        /// <summary>KnobDirectionInvalid.</summary>
        KnobDirectionInvalid = 108,

        /// <summary>KnobMovementModeInvalid.</summary>
        KnobMovementModeInvalid = 109,

        /// <summary>KnobJogSizeInvalid.</summary>
        KnobJogSizeInvalid = 110,

        /// <summary>KnobVelocityScaleInvalid.</summary>
        KnobVelocityScaleInvalid = 111,

        /// <summary>KnobVelocityProfileInvalid.</summary>
        KnobVelocityProfileInvalid = 112,

        /// <summary>AccelerationOnlyInvalid.</summary>
        AccelerationOnlyInvalid = 113,

        /// <summary>DecelerationOnlyInvalid.</summary>
        DecelerationOnlyInvalid = 114,

        /// <summary>MoveTrackingModeInvalid.</summary>
        MoveTrackingModeInvalid = 115,

        /// <summary>ManualMoveTrackingDisabledModeInvalid.</summary>
        ManualMoveTrackingDisabledModeInvalid = 116,

        /// <summary>MoveTrackingPeriodInvalid.</summary>
        MoveTrackingPeriodInvalid = 117,

        /// <summary>ClosedLoopModeInvalid.</summary>
        ClosedLoopModeInvalid = 118,

        /// <summary>SlipTrackingPeriodInvalid.</summary>
        SlipTrackingPeriodInvalid = 119,

        /// <summary>StallTimeoutInvalid.</summary>
        StallTimeoutInvalid = 120,

        /// <summary>DeviceDirectionInvalid.</summary>
        DeviceDirectionInvalid = 121,

        /// <summary>BaudRateInvalid.</summary>
        BaudRateInvalid = 122,

        /// <summary>ProtocolInvalid.</summary>
        ProtocolInvalid = 123,

        /// <summary>BaudRateOrProtocolInvalid.</summary>
        BaudRateOrProtocolInvalid = 124,

        /// <summary>Busy.</summary>
        Busy = 255,

        /// <summary>SystemError.</summary>
        SystemError = 257,

        /// <summary>StorageFull.</summary>
        StorageFull = 401,

        /// <summary>RegisterAddressInvalid.</summary>
        RegisterAddressInvalid = 701,

        /// <summary>RegisterValueInvalid.</summary>
        RegisterValueInvalid = 702,

        /// <summary>SavePositionInvalid.</summary>
        SavePositionInvalid = 1600,

        /// <summary>SavePositionNotHomed.</summary>
        SavePositionNotHomed = 1601,

        /// <summary>ReturnPositionInvalid.</summary>
        ReturnPositionInvalid = 1700,

        /// <summary>MovePositionInvalid.</summary>
        MovePositionInvalid = 1800,

        /// <summary>MovePositionNotHomed.</summary>
        MovePositionNotHomed = 1801,

        /// <summary>RelativePositionLimited.</summary>
        RelativePositionLimited = 2146,

        /// <summary>SettingsLocked.</summary>
        SettingsLocked = 3600,

        /// <summary>Bit1Invalid.</summary>
        Bit1Invalid = 4001,

        /// <summary>Bit2Invalid.</summary>
        Bit2Invalid = 4002,

        /// <summary>DisableAutoHomeInvalid.</summary>
        DisableAutoHomeInvalid = 4008,

        /// <summary>Bit10Invalid.</summary>
        Bit10Invalid = 4010,

        /// <summary>Bit11Invalid.</summary>
        Bit11Invalid = 4011,

        /// <summary>HomeSwitchInvalid.</summary>
        HomeSwitchInvalid = 4012,

        /// <summary>Bit13Invalid.</summary>
        Bit13Invalid = 4013,

        /// <summary>Bit14Invalid.</summary>
        Bit14Invalid = 4014,

        /// <summary>Bit15Invalid.</summary>
        Bit15Invalid = 4015,

        /// <summary>DeviceParked.</summary>
        DeviceParked = 6501,

        /// <summary>DriverDisabled.</summary>
        DriverDisabled = 9001,

    }
}
