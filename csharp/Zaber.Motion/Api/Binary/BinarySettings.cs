/* This file is generated. Do not modify by hand. */
namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Named constants for all Zaber Binary protocol settings.
    /// </summary>
    public enum BinarySettings
    {
        /// <summary>Acceleration.</summary>
        Acceleration = 0,

        /// <summary>AccelerationOnly.</summary>
        AccelerationOnly = 1,

        /// <summary>ActiveAxis.</summary>
        ActiveAxis = 2,

        /// <summary>AliasNumber.</summary>
        AliasNumber = 3,

        /// <summary>AnalogInputCount.</summary>
        AnalogInputCount = 4,

        /// <summary>AnalogOutputCount.</summary>
        AnalogOutputCount = 5,

        /// <summary>AutoHomeDisabledMode.</summary>
        AutoHomeDisabledMode = 6,

        /// <summary>AutoReplyDisabledMode.</summary>
        AutoReplyDisabledMode = 7,

        /// <summary>AxisDeviceNumber.</summary>
        AxisDeviceNumber = 8,

        /// <summary>AxisInversion.</summary>
        AxisInversion = 9,

        /// <summary>AxisVelocityProfile.</summary>
        AxisVelocityProfile = 10,

        /// <summary>AxisVelocityScale.</summary>
        AxisVelocityScale = 11,

        /// <summary>BaudRate.</summary>
        BaudRate = 12,

        /// <summary>CalibratedEncoderCount.</summary>
        CalibratedEncoderCount = 13,

        /// <summary>CalibrationError.</summary>
        CalibrationError = 14,

        /// <summary>CalibrationType.</summary>
        CalibrationType = 15,

        /// <summary>ClosedLoopMode.</summary>
        ClosedLoopMode = 16,

        /// <summary>CurrentPosition.</summary>
        CurrentPosition = 17,

        /// <summary>CycleDistance.</summary>
        CycleDistance = 18,

        /// <summary>DecelerationOnly.</summary>
        DecelerationOnly = 19,

        /// <summary>DeviceDirection.</summary>
        DeviceDirection = 20,

        /// <summary>DeviceID.</summary>
        DeviceID = 21,

        /// <summary>DeviceMode.</summary>
        DeviceMode = 22,

        /// <summary>DigitalInputCount.</summary>
        DigitalInputCount = 23,

        /// <summary>DigitalOutputCount.</summary>
        DigitalOutputCount = 24,

        /// <summary>EncoderCount.</summary>
        EncoderCount = 25,

        /// <summary>EncoderPosition.</summary>
        EncoderPosition = 26,

        /// <summary>FilterHolderID.</summary>
        FilterHolderID = 27,

        /// <summary>FirmwareBuild.</summary>
        FirmwareBuild = 28,

        /// <summary>FirmwareVersion.</summary>
        FirmwareVersion = 29,

        /// <summary>HoldCurrent.</summary>
        HoldCurrent = 30,

        /// <summary>HomeOffset.</summary>
        HomeOffset = 31,

        /// <summary>HomeSensorType.</summary>
        HomeSensorType = 32,

        /// <summary>HomeSpeed.</summary>
        HomeSpeed = 33,

        /// <summary>HomeStatus.</summary>
        HomeStatus = 34,

        /// <summary>IndexDistance.</summary>
        IndexDistance = 35,

        /// <summary>JoystickCalibrationMode.</summary>
        JoystickCalibrationMode = 36,

        /// <summary>KnobDirection.</summary>
        KnobDirection = 37,

        /// <summary>KnobDisabledMode.</summary>
        KnobDisabledMode = 38,

        /// <summary>KnobJogSize.</summary>
        KnobJogSize = 39,

        /// <summary>KnobMovementMode.</summary>
        KnobMovementMode = 40,

        /// <summary>KnobVelocityProfile.</summary>
        KnobVelocityProfile = 41,

        /// <summary>KnobVelocityScale.</summary>
        KnobVelocityScale = 42,

        /// <summary>LockState.</summary>
        LockState = 43,

        /// <summary>ManualMoveTrackingDisabledMode.</summary>
        ManualMoveTrackingDisabledMode = 44,

        /// <summary>MaximumPosition.</summary>
        MaximumPosition = 45,

        /// <summary>MaximumRelativeMove.</summary>
        MaximumRelativeMove = 46,

        /// <summary>MessageIDMode.</summary>
        MessageIDMode = 47,

        /// <summary>MicrostepResolution.</summary>
        MicrostepResolution = 48,

        /// <summary>MinimumPosition.</summary>
        MinimumPosition = 49,

        /// <summary>MoveTrackingMode.</summary>
        MoveTrackingMode = 50,

        /// <summary>MoveTrackingPeriod.</summary>
        MoveTrackingPeriod = 51,

        /// <summary>ParkState.</summary>
        ParkState = 52,

        /// <summary>PeripheralID.</summary>
        PeripheralID = 53,

        /// <summary>PeripheralIDPending.</summary>
        PeripheralIDPending = 54,

        /// <summary>PeripheralSerialNumber.</summary>
        PeripheralSerialNumber = 55,

        /// <summary>PeripheralSerialPending.</summary>
        PeripheralSerialPending = 56,

        /// <summary>PowerSupplyVoltage.</summary>
        PowerSupplyVoltage = 57,

        /// <summary>Protocol.</summary>
        Protocol = 58,

        /// <summary>RunningCurrent.</summary>
        RunningCurrent = 59,

        /// <summary>SerialNumber.</summary>
        SerialNumber = 60,

        /// <summary>SlipTrackingPeriod.</summary>
        SlipTrackingPeriod = 61,

        /// <summary>StallTimeout.</summary>
        StallTimeout = 62,

        /// <summary>Status.</summary>
        Status = 63,

        /// <summary>TargetSpeed.</summary>
        TargetSpeed = 64,

    }
}
