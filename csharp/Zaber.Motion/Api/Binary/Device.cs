﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Represents a device using the binary protocol.
    /// </summary>
    public class Device
    {
        internal Device(Connection connection, int deviceAddress)
        {
            Connection = connection;
            Settings = new DeviceSettings(this);
            DeviceAddress = deviceAddress;
        }


        /// <summary>
        /// Default timeout for move commands in seconds.
        /// </summary>
        public const double DefaultMovementTimeout = 60;


        /// <summary>
        /// Connection of this device.
        /// </summary>
        public Connection Connection { get; private set; }


        /// <summary>
        /// Settings and properties of this axis.
        /// </summary>
        public DeviceSettings Settings { get; private set; }


        /// <summary>
        /// The device address uniquely identifies the device on the connection.
        /// It can be configured or automatically assigned by the renumber command.
        /// </summary>
        public int DeviceAddress { get; private set; }


        /// <summary>
        /// Identity of the device.
        /// </summary>
        public DeviceIdentity Identity
        {
            get { return RetrieveIdentity(); }
        }


        /// <summary>
        /// Indicates whether or not the device has been identified.
        /// </summary>
        public bool IsIdentified
        {
            get { return RetrieveIsIdentified(); }
        }


        /// <summary>
        /// Unique ID of the device hardware.
        /// </summary>
        public int DeviceId
        {
            get { return Identity.DeviceId; }
        }


        /// <summary>
        /// Serial number of the device.
        /// Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
        /// </summary>
        public long SerialNumber
        {
            get { return Identity.SerialNumber; }
        }


        /// <summary>
        /// Name of the product.
        /// </summary>
        public string Name
        {
            get { return Identity.Name; }
        }


        /// <summary>
        /// Version of the firmware.
        /// </summary>
        public FirmwareVersion FirmwareVersion
        {
            get { return Identity.FirmwareVersion; }
        }


        /// <summary>
        /// Indicates whether the device is a peripheral or part of an integrated device.
        /// </summary>
        public bool IsPeripheral
        {
            get { return Identity.IsPeripheral; }
        }


        /// <summary>
        /// Unique ID of the peripheral hardware.
        /// </summary>
        public int PeripheralId
        {
            get { return Identity.PeripheralId; }
        }


        /// <summary>
        /// Name of the peripheral hardware.
        /// </summary>
        public string PeripheralName
        {
            get { return Identity.PeripheralName; }
        }


        /// <summary>
        /// Determines the type of an device and units it accepts.
        /// </summary>
        public DeviceType DeviceType
        {
            get { return Identity.DeviceType; }
        }


        /// <summary>
        /// Sends a generic Binary command to this device.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Message> GenericCommandAsync(CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
            var request = new Requests.GenericBinaryRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Data = data,
                Timeout = timeout,
                CheckErrors = checkErrors,
            };

            var response = await Gateway.CallAsync("binary/interface/generic_command", request, Message.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic Binary command to this device.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <returns>A response to the command.</returns>
        public Message GenericCommand(CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, data, timeout, checkErrors);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic Binary command to this device without expecting a response.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(CommandCode command, int data = 0)
        {
            var request = new Requests.GenericBinaryRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Data = data,
            };

            await Gateway.CallAsync("binary/interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic Binary command to this device without expecting a response.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        public void GenericCommandNoResponse(CommandCode command, int data = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command, data);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Data argument to the command. Defaults to zero.</param>
        /// <param name="fromUnit">Unit to convert sent data from.</param>
        /// <param name="toUnit">Unit to convert retrieved data to.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <returns>A Task that can be awaited to get the result: Data that has been converted to the provided unit.</returns>
        public async Task<double> GenericCommandWithUnitsAsync(CommandCode command, double data = 0, Units fromUnit = Units.Native, Units toUnit = Units.Native, double timeout = 0.0)
        {
            var request = new Requests.BinaryGenericWithUnitsRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Command = command,
                Data = data,
                FromUnit = fromUnit,
                ToUnit = toUnit,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("binary/device/generic_command_with_units", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Data argument to the command. Defaults to zero.</param>
        /// <param name="fromUnit">Unit to convert sent data from.</param>
        /// <param name="toUnit">Unit to convert retrieved data to.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <returns>Data that has been converted to the provided unit.</returns>
        public double GenericCommandWithUnits(CommandCode command, double data = 0, Units fromUnit = Units.Native, Units toUnit = Units.Native, double timeout = 0.0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandWithUnitsAsync(command, data, fromUnit, toUnit, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Homes device. Device returns to its homing position.
        /// </summary>
        /// <param name="unit">Unit to convert returned position to.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>A Task that can be awaited to get the result: Current position that has been converted to the provided unit.</returns>
        public async Task<double> HomeAsync(Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
            var request = new Requests.BinaryDeviceHomeRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Unit = unit,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("binary/device/home", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Homes device. Device returns to its homing position.
        /// </summary>
        /// <param name="unit">Unit to convert returned position to.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>Current position that has been converted to the provided unit.</returns>
        public double Home(Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = HomeAsync(unit, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops ongoing device movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="unit">Unit to convert returned position to.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>A Task that can be awaited to get the result: Current position that has been converted to the provided unit.</returns>
        public async Task<double> StopAsync(Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
            var request = new Requests.BinaryDeviceStopRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Unit = unit,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("binary/device/stop", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Stops ongoing device movement. Decelerates until zero speed.
        /// </summary>
        /// <param name="unit">Unit to convert returned position to.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>Current position that has been converted to the provided unit.</returns>
        public double Stop(Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopAsync(unit, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move device to absolute position.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Unit for the provided position as well as position returned by the device.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>A Task that can be awaited to get the result: Current position that has been converted to the provided unit.</returns>
        public async Task<double> MoveAbsoluteAsync(double position, Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
            var request = new Requests.BinaryDeviceMoveRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Type = Requests.AxisMoveType.Abs,
                Arg = position,
                Unit = unit,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("binary/device/move", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Move device to absolute position.
        /// </summary>
        /// <param name="position">Absolute position.</param>
        /// <param name="unit">Unit for the provided position as well as position returned by the device.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>Current position that has been converted to the provided unit.</returns>
        public double MoveAbsolute(double position, Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveAbsoluteAsync(position, unit, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Move device to position relative to current position.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Unit for the provided position as well as position returned by the device.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>A Task that can be awaited to get the result: Current position that has been converted to the provided unit.</returns>
        public async Task<double> MoveRelativeAsync(double position, Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
            var request = new Requests.BinaryDeviceMoveRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Type = Requests.AxisMoveType.Rel,
                Arg = position,
                Unit = unit,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("binary/device/move", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Move device to position relative to current position.
        /// </summary>
        /// <param name="position">Relative position.</param>
        /// <param name="unit">Unit for the provided position as well as position returned by the device.</param>
        /// <param name="timeout">Number of seconds to wait for response from the device chain (defaults to 60s).</param>
        /// <returns>Current position that has been converted to the provided unit.</returns>
        public double MoveRelative(double position, Units unit = Units.Native, double timeout = DefaultMovementTimeout)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveRelativeAsync(position, unit, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Begins to move device at specified speed.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Unit to convert returned velocity to.</param>
        /// <returns>A Task that can be awaited to get the result: Device velocity that has been converted to the provided unit.</returns>
        public async Task<double> MoveVelocityAsync(double velocity, Units unit = Units.Native)
        {
            var request = new Requests.BinaryDeviceMoveRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Type = Requests.AxisMoveType.Vel,
                Arg = velocity,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("binary/device/move", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Begins to move device at specified speed.
        /// </summary>
        /// <param name="velocity">Movement velocity.</param>
        /// <param name="unit">Unit to convert returned velocity to.</param>
        /// <returns>Device velocity that has been converted to the provided unit.</returns>
        public double MoveVelocity(double velocity, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = MoveVelocityAsync(velocity, unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Waits until device stops moving.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task WaitUntilIdleAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            await Gateway.CallAsync("binary/device/wait_until_idle", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Waits until device stops moving.
        /// </summary>
        public void WaitUntilIdle()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = WaitUntilIdleAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Check whether the device is moving.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the device is currently executing a motion command.</returns>
        public async Task<bool> IsBusyAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = await Gateway.CallAsync("binary/device/is_busy", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Check whether the device is moving.
        /// </summary>
        /// <returns>True if the device is currently executing a motion command.</returns>
        public bool IsBusy()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsBusyAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Queries the device and the database, gathering information about the product.
        /// Without this information features such as unit conversions will not work.
        /// Usually, called automatically by detect devices method.
        /// </summary>
        /// <param name="assumeVersion">The identification assumes the specified firmware version
        /// instead of the version queried from the device.
        /// Providing this argument can lead to unexpected compatibility issues.</param>
        /// <returns>A Task that can be awaited to get the result: Device identification data.</returns>
        public async Task<DeviceIdentity> IdentifyAsync(FirmwareVersion? assumeVersion = null)
        {
            var request = new Requests.DeviceIdentifyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                AssumeVersion = assumeVersion,
            };

            var response = await Gateway.CallAsync("binary/device/identify", request, DeviceIdentity.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Queries the device and the database, gathering information about the product.
        /// Without this information features such as unit conversions will not work.
        /// Usually, called automatically by detect devices method.
        /// </summary>
        /// <param name="assumeVersion">The identification assumes the specified firmware version
        /// instead of the version queried from the device.
        /// Providing this argument can lead to unexpected compatibility issues.</param>
        /// <returns>Device identification data.</returns>
        public DeviceIdentity Identify(FirmwareVersion? assumeVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IdentifyAsync(assumeVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Parks the axis.
        /// Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
        /// It can later be unparked and moved without first having to home it.
        /// Requires at least Firmware 6.06.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ParkAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            await Gateway.CallAsync("binary/device/park", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Parks the axis.
        /// Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
        /// It can later be unparked and moved without first having to home it.
        /// Requires at least Firmware 6.06.
        /// </summary>
        public void Park()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ParkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Unparks axis. Axis will now be able to move.
        /// Requires at least Firmware 6.06.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task UnparkAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            await Gateway.CallAsync("binary/device/unpark", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Unparks axis. Axis will now be able to move.
        /// Requires at least Firmware 6.06.
        /// </summary>
        public void Unpark()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = UnparkAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns bool indicating whether the axis is parked or not.
        /// Requires at least Firmware 6.06.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if the axis is currently parked. False otherwise.</returns>
        public async Task<bool> IsParkedAsync()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = await Gateway.CallAsync("binary/device/is_parked", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns bool indicating whether the axis is parked or not.
        /// Requires at least Firmware 6.06.
        /// </summary>
        /// <returns>True if the axis is currently parked. False otherwise.</returns>
        public bool IsParked()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsParkedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current device position.
        /// </summary>
        /// <param name="unit">Units of position.</param>
        /// <returns>A Task that can be awaited to get the result: Axis position.</returns>
        public async Task<double> GetPositionAsync(Units unit = Units.Native)
        {
            var request = new Requests.BinaryDeviceGetSettingRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
                Setting = BinarySettings.CurrentPosition,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("binary/device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns current device position.
        /// </summary>
        /// <param name="unit">Units of position.</param>
        /// <returns>Axis position.</returns>
        public double GetPosition(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetPositionAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("binary/device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns identity.
        /// </summary>
        /// <returns>Device identity.</returns>
        private DeviceIdentity RetrieveIdentity()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("binary/device/get_identity", request, DeviceIdentity.FromByteArray);
            return response;
        }


        /// <summary>
        /// Returns whether or not the device have been identified.
        /// </summary>
        /// <returns>True if the device has already been identified. False otherwise.</returns>
        private bool RetrieveIsIdentified()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Device = DeviceAddress,
            };

            var response = Gateway.CallSync("binary/device/get_is_identified", request, Requests.BoolResponse.FromByteArray);
            return response.Value;
        }


    }
}
