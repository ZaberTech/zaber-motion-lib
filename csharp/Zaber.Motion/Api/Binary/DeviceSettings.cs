﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Class providing access to various device settings and properties.
    /// </summary>
    public class DeviceSettings
    {
        internal DeviceSettings(Device device)
        {
            _device = device;
        }


        /// <summary>
        /// Returns any device setting or property.
        /// </summary>
        /// <param name="setting">Setting to get.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to get the result: Setting value.</returns>
        public async Task<double> GetAsync(BinarySettings setting, Units unit = Units.Native)
        {
            var request = new Requests.BinaryDeviceGetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("binary/device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns any device setting or property.
        /// </summary>
        /// <param name="setting">Setting to get.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>Setting value.</returns>
        public double Get(BinarySettings setting, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetAsync(setting, unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets any device setting.
        /// </summary>
        /// <param name="setting">Setting to set.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetAsync(BinarySettings setting, double value, Units unit = Units.Native)
        {
            var request = new Requests.BinaryDeviceSetSettingRequest()
            {
                InterfaceId = _device.Connection.InterfaceId,
                Device = _device.DeviceAddress,
                Setting = setting,
                Value = value,
                Unit = unit,
            };

            await Gateway.CallAsync("binary/device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets any device setting.
        /// </summary>
        /// <param name="setting">Setting to set.</param>
        /// <param name="value">Value of the setting.</param>
        /// <param name="unit">Units of setting.</param>
        public void Set(BinarySettings setting, double value, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetAsync(setting, value, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        private Device _device;


    }
}
