﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Class representing access to particular connection (serial port, TCP connection) using the legacy Binary protocol.
    /// </summary>
#if NET || NETSTANDARD
    public class Connection : IDisposable, IAsyncDisposable
#else
    public class Connection : IDisposable
#endif
    {
        /// <summary>
        /// Delegate used to invoke <see cref="Disconnected"/>> event.
        /// </summary>
        /// <param name="disconnectionError">Error that caused the disconnection.</param>
        public delegate void DisconnectedHandler(MotionLibException disconnectionError);


        /// <summary>
        /// Event invoked when a response from a device cannot be matched to any known request.
        /// </summary>
        public event Action<UnknownResponseEvent>? UnknownResponse;


        /// <summary>
        /// Event invoked when a reply-only command such as a move tracking message is received from a device.
        /// </summary>
        public event Action<ReplyOnlyEvent>? ReplyOnly;


        /// <summary>
        /// Event invoked when connection is interrupted or closed.
        /// </summary>
        public event DisconnectedHandler? Disconnected
        {
            add
            {
                lock (_lock)
                {
                    CheckConnected();
                    _disconnected += value;
                }
            }

            remove
            {
                _disconnected -= value;
            }
        }


        /// <summary>
        /// Indicates whether the connection is currently connected.
        /// </summary>
        [Obsolete("No longer supported: This method does not reliably represent the connection state, and will be removed in a future release.")]
        public bool IsConnected
        {
            get
            {
                lock (_lock)
                {
                    return _isConnected;
                }
            }
        }


        /// <summary>
        /// Finalizes an instance of the <see cref="Connection"/> class.
        /// </summary>
        ~Connection()
        {
            Dispose(false);
        }


        internal Connection(int interfaceId)
        {
            InterfaceId = interfaceId;
            Subscribe();
        }


        /// <summary>
        /// Default baud rate for serial connections.
        /// </summary>
        public const int DefaultBaudRate = 9600;


        /// <summary>
        /// The interface ID identifies thisConnection instance with the underlying library.
        /// </summary>
        public int InterfaceId { get; private set; }


        /// <summary>
        /// Opens a serial port.
        /// </summary>
        /// <param name="portName">Name of the port to open.</param>
        /// <param name="baudRate">Optional baud rate (defaults to 9600).</param>
        /// <param name="useMessageIds">Enable use of message IDs (defaults to disabled).
        /// All your devices must be pre-configured to match.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the port.</returns>
        public static async Task<Connection> OpenSerialPortAsync(string portName, int baudRate = DefaultBaudRate, bool useMessageIds = false)
        {
            var request = new Requests.OpenBinaryInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.SerialPort,
                PortName = portName,
                BaudRate = baudRate,
                UseMessageIds = useMessageIds,
            };

            var response = await Gateway.CallAsync("binary/interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a serial port.
        /// </summary>
        /// <param name="portName">Name of the port to open.</param>
        /// <param name="baudRate">Optional baud rate (defaults to 9600).</param>
        /// <param name="useMessageIds">Enable use of message IDs (defaults to disabled).
        /// All your devices must be pre-configured to match.</param>
        /// <returns>An object representing the port.</returns>
        public static Connection OpenSerialPort(string portName, int baudRate = DefaultBaudRate, bool useMessageIds = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenSerialPortAsync(portName, baudRate, useMessageIds);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Opens a TCP connection.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Port number.</param>
        /// <param name="useMessageIds">Enable use of message IDs (defaults to disabled).
        /// All your devices must be pre-configured to match.</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the connection.</returns>
        public static async Task<Connection> OpenTcpAsync(string hostName, int port, bool useMessageIds = false)
        {
            var request = new Requests.OpenBinaryInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.Tcp,
                HostName = hostName,
                Port = port,
                UseMessageIds = useMessageIds,
            };

            var response = await Gateway.CallAsync("binary/interface/open", request, Requests.OpenInterfaceResponse.FromByteArray).ConfigureAwait(false);
            return new Connection(response.InterfaceId);
        }


        /// <summary>
        /// Opens a TCP connection.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Port number.</param>
        /// <param name="useMessageIds">Enable use of message IDs (defaults to disabled).
        /// All your devices must be pre-configured to match.</param>
        /// <returns>An object representing the connection.</returns>
        public static Connection OpenTcp(string hostName, int port, bool useMessageIds = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenTcpAsync(hostName, port, useMessageIds);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CloseAsync()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            await Gateway.CallAsync("interface/close", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        public void Close()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CloseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic Binary command to this connection.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="device">Device address to send the command to. Use zero for broadcast.</param>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Message> GenericCommandAsync(int device, CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
            var request = new Requests.GenericBinaryRequest()
            {
                InterfaceId = InterfaceId,
                Device = device,
                Command = command,
                Data = data,
                Timeout = timeout,
                CheckErrors = checkErrors,
            };

            var response = await Gateway.CallAsync("binary/interface/generic_command", request, Message.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic Binary command to this connection.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="device">Device address to send the command to. Use zero for broadcast.</param>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <returns>A response to the command.</returns>
        public Message GenericCommand(int device, CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(device, command, data, timeout, checkErrors);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic Binary command to this connection without expecting a response.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="device">Device address to send the command to. Use zero for broadcast.</param>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(int device, CommandCode command, int data = 0)
        {
            var request = new Requests.GenericBinaryRequest()
            {
                InterfaceId = InterfaceId,
                Device = device,
                Command = command,
                Data = data,
            };

            await Gateway.CallAsync("binary/interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic Binary command to this connection without expecting a response.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="device">Device address to send the command to. Use zero for broadcast.</param>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        public void GenericCommandNoResponse(int device, CommandCode command, int data = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(device, command, data);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic Binary command to this connection and expects responses from one or more devices.
        /// Responses are returned in order of arrival.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when any device rejects the command.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Message[]> GenericCommandMultiResponseAsync(CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
            var request = new Requests.GenericBinaryRequest()
            {
                InterfaceId = InterfaceId,
                Command = command,
                Data = data,
                Timeout = timeout,
                CheckErrors = checkErrors,
            };

            var response = await Gateway.CallAsync("binary/interface/generic_command_multi_response", request, Requests.BinaryMessageCollection.FromByteArray).ConfigureAwait(false);
            return response.Messages;
        }


        /// <summary>
        /// Sends a generic Binary command to this connection and expects responses from one or more devices.
        /// Responses are returned in order of arrival.
        /// For more information please refer to the
        /// <see href="https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference">Binary Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command to send.</param>
        /// <param name="data">Optional data argument to the command. Defaults to zero.</param>
        /// <param name="timeout">Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when any device rejects the command.</param>
        /// <returns>All responses to the command.</returns>
        public Message[] GenericCommandMultiResponse(CommandCode command, int data = 0, double timeout = 0.0, bool checkErrors = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, data, timeout, checkErrors);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Renumbers devices present on this connection. After renumbering, you must identify devices again.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Total number of devices that responded to the renumber.</returns>
        public async Task<int> RenumberDevicesAsync()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            var response = await Gateway.CallAsync("binary/device/renumber", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Renumbers devices present on this connection. After renumbering, you must identify devices again.
        /// </summary>
        /// <returns>Total number of devices that responded to the renumber.</returns>
        public int RenumberDevices()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = RenumberDevicesAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Attempts to detect any devices present on this connection.
        /// </summary>
        /// <param name="identifyDevices">Determines whether device identification should be performed as well.</param>
        /// <returns>A Task that can be awaited to get the result: Array of detected devices.</returns>
        public async Task<Device[]> DetectDevicesAsync(bool identifyDevices = true)
        {
            var request = new Requests.BinaryDeviceDetectRequest()
            {
                InterfaceId = InterfaceId,
                IdentifyDevices = identifyDevices,
            };

            var response = await Gateway.CallAsync("binary/device/detect", request, Requests.BinaryDeviceDetectResponse.FromByteArray).ConfigureAwait(false);
            return response.Devices.Select(device => GetDevice(device)).ToArray();
        }


        /// <summary>
        /// Attempts to detect any devices present on this connection.
        /// </summary>
        /// <param name="identifyDevices">Determines whether device identification should be performed as well.</param>
        /// <returns>Array of detected devices.</returns>
        public Device[] DetectDevices(bool identifyDevices = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DetectDevicesAsync(identifyDevices);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets a Device class instance which allows you to control a particular device on this connection.
        /// Devices are numbered from 1.
        /// </summary>
        /// <param name="deviceAddress">Address of device intended to control. Address is configured for each device.</param>
        /// <returns>Device instance.</returns>
        public Device GetDevice(int deviceAddress)
        {
            if (deviceAddress <= 0)
            {
                throw new ArgumentException("Invalid value; physical devices are numbered from 1.");
            }

            return new Device(this, deviceAddress);
        }


        /// <summary>
        /// Returns a string that represents the connection.
        /// </summary>
        /// <returns>A string that represents the connection.</returns>
        public override string ToString()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = InterfaceId,
            };

            var response = Gateway.CallSync("interface/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Dispose pattern function.
        /// </summary>
        /// <param name="disposing">True when function is called from Dispose().</param>
        protected virtual void Dispose(bool disposing)
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            Close();
        }


#if NET || NETSTANDARD
#pragma warning disable SA1202
        /// <summary>
        /// IAsyncDisposable implementation.
        /// </summary>
        /// <returns>A task meant to be awaited by the "await using" statement.</returns>
        public async ValueTask DisposeAsync()
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            await DisposeAsyncCore().ConfigureAwait(false);
            await CloseAsync().ConfigureAwait(false);

#pragma warning disable CA1816
            GC.SuppressFinalize(this);
#pragma warning restore CA1816
        }
#pragma warning restore SA1202


        /// <summary>
        /// Enables subclasses to dispose of their own resources.
        /// </summary>
        /// <returns>Immediately.</returns>
        protected virtual async ValueTask DisposeAsyncCore()
            => await Task.FromResult(0).ConfigureAwait(false);
#endif


        private void CheckConnected()
        {
            if (!_isConnected)
            {
                throw new InvalidOperationException("Binary Connection is already closed.");
            }
        }


        private void Subscribe()
        {
            Events.UnknownBinaryResponse += Events_UnknownResponse;
            Events.BinaryReplyOnly += Events_ReplyOnly;
            Events.Disconnected += Events_Disconnected;
        }


        private void Unsubscribe()
        {
            Events.Disconnected -= Events_Disconnected;
            Events.UnknownBinaryResponse -= Events_UnknownResponse;
            Events.BinaryReplyOnly -= Events_ReplyOnly;
        }


        private void Events_UnknownResponse(Requests.UnknownBinaryResponseEventWrapper replyEvent)
        {
            if (replyEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            UnknownResponse?.Invoke(replyEvent.UnknownResponse);
        }

        private void Events_ReplyOnly(Requests.BinaryReplyOnlyEventWrapper replyEvent)
        {
            if (replyEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            ReplyOnly?.Invoke(replyEvent.Reply);
        }

        private void Events_Disconnected(Requests.DisconnectedEvent disconnectedEvent)
        {
            if (disconnectedEvent.InterfaceId != InterfaceId)
            {
                return;
            }

            lock (_lock)
            {
                _isConnected = false;
            }

            Unsubscribe();
            _disconnected?.Invoke(Zaber.Motion.Exceptions.ExceptionConverter.Convert(disconnectedEvent.ErrorType, disconnectedEvent.ErrorMessage));
        }


        private readonly object _lock = new object();
        private int _disposed;
        private bool _isConnected = true;
        private DisconnectedHandler? _disconnected;
    }
}
