/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Binary
{
    /// <summary>
    /// Representation of data gathered during device identification.
    /// </summary>
    public class DeviceIdentity : IMessage
    {
        /// <summary>
        /// Unique ID of the device hardware.
        /// </summary>
        [JsonProperty("deviceId")]
        public int DeviceId { get; set; }

        /// <summary>
        /// Serial number of the device.
        /// Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
        /// </summary>
        [JsonProperty("serialNumber")]
        public long SerialNumber { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Version of the firmware.
        /// </summary>
        [JsonProperty("firmwareVersion")]
        public FirmwareVersion FirmwareVersion { get; set; } = new FirmwareVersion { };

        /// <summary>
        /// Indicates whether the device is a peripheral or part of an integrated device.
        /// </summary>
        [JsonProperty("isPeripheral")]
        public bool IsPeripheral { get; set; }

        /// <summary>
        /// Unique ID of the peripheral hardware.
        /// </summary>
        [JsonProperty("peripheralId")]
        public int PeripheralId { get; set; }

        /// <summary>
        /// Name of the peripheral hardware.
        /// </summary>
        [JsonProperty("peripheralName")]
        public string PeripheralName { get; set; } = string.Empty;

        /// <summary>
        /// Determines the type of an device and units it accepts.
        /// </summary>
        [JsonProperty("deviceType")]
        public DeviceType DeviceType { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is DeviceIdentity))
            {
                return false;
            }

            DeviceIdentity other = (DeviceIdentity)obj;
            return (
                EqualityUtils.CheckEquals(DeviceId, other.DeviceId) &&
                EqualityUtils.CheckEquals(SerialNumber, other.SerialNumber) &&
                EqualityUtils.CheckEquals(Name, other.Name) &&
                EqualityUtils.CheckEquals(FirmwareVersion, other.FirmwareVersion) &&
                EqualityUtils.CheckEquals(IsPeripheral, other.IsPeripheral) &&
                EqualityUtils.CheckEquals(PeripheralId, other.PeripheralId) &&
                EqualityUtils.CheckEquals(PeripheralName, other.PeripheralName) &&
                EqualityUtils.CheckEquals(DeviceType, other.DeviceType)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DeviceId));
            hash.Add(EqualityUtils.GenerateHashCode(SerialNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Name));
            hash.Add(EqualityUtils.GenerateHashCode(FirmwareVersion));
            hash.Add(EqualityUtils.GenerateHashCode(IsPeripheral));
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralId));
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralName));
            hash.Add(EqualityUtils.GenerateHashCode(DeviceType));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => DeviceIdentity.ToByteArray(this);

        internal static DeviceIdentity FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<DeviceIdentity>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(DeviceIdentity instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
