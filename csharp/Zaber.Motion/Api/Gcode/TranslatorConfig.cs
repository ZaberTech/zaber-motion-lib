/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Configuration of a translator.
    /// </summary>
    public class TranslatorConfig : IMessage
    {
        /// <summary>
        /// Optional custom mapping of translator axes to stream axes.
        /// </summary>
        [JsonProperty("axisMappings")]
        public AxisMapping[] AxisMappings { get; set; } = System.Array.Empty<AxisMapping>();

        /// <summary>
        /// Optional transformation of axes.
        /// </summary>
        [JsonProperty("axisTransformations")]
        public AxisTransformation[] AxisTransformations { get; set; } = System.Array.Empty<AxisTransformation>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TranslatorConfig))
            {
                return false;
            }

            TranslatorConfig other = (TranslatorConfig)obj;
            return (
                EqualityUtils.CheckEquals(AxisMappings, other.AxisMappings) &&
                EqualityUtils.CheckEquals(AxisTransformations, other.AxisTransformations)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(AxisMappings));
            hash.Add(EqualityUtils.GenerateHashCode(AxisTransformations));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TranslatorConfig.ToByteArray(this);

        internal static TranslatorConfig FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TranslatorConfig>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TranslatorConfig instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
