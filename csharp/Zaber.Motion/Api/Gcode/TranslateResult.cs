/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Represents a result of a G-code block translation.
    /// </summary>
    public class TranslateResult : IMessage
    {
        /// <summary>
        /// Stream commands resulting from the block.
        /// </summary>
        [JsonProperty("commands")]
        public string[] Commands { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// Messages informing about unsupported codes and features.
        /// </summary>
        [JsonProperty("warnings")]
        public TranslateMessage[] Warnings { get; set; } = System.Array.Empty<TranslateMessage>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is TranslateResult))
            {
                return false;
            }

            TranslateResult other = (TranslateResult)obj;
            return (
                EqualityUtils.CheckEquals(Commands, other.Commands) &&
                EqualityUtils.CheckEquals(Warnings, other.Warnings)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Commands));
            hash.Add(EqualityUtils.GenerateHashCode(Warnings));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => TranslateResult.ToByteArray(this);

        internal static TranslateResult FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<TranslateResult>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(TranslateResult instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
