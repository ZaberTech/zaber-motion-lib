/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Defines an axis of the translator.
    /// </summary>
    public class AxisDefinition : IMessage
    {
        /// <summary>
        /// ID of the peripheral.
        /// </summary>
        [JsonProperty("peripheralId")]
        public int PeripheralId { get; set; }

        /// <summary>
        /// Microstep resolution of the axis.
        /// Can be obtained by reading the resolution setting.
        /// Leave empty if the axis does not have the setting.
        /// </summary>
        [JsonProperty("microstepResolution")]
        public int? MicrostepResolution { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AxisDefinition))
            {
                return false;
            }

            AxisDefinition other = (AxisDefinition)obj;
            return (
                EqualityUtils.CheckEquals(PeripheralId, other.PeripheralId) &&
                EqualityUtils.CheckEquals(MicrostepResolution, other.MicrostepResolution)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(PeripheralId));
            hash.Add(EqualityUtils.GenerateHashCode(MicrostepResolution));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AxisDefinition.ToByteArray(this);

        internal static AxisDefinition FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AxisDefinition>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AxisDefinition instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
