/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Maps a translator axis to a Zaber stream axis.
    /// </summary>
    public class AxisMapping : IMessage
    {
        /// <summary>
        /// Letter of the translator axis (X,Y,Z,A,B,C,E).
        /// </summary>
        [JsonProperty("axisLetter")]
        public string AxisLetter { get; set; } = string.Empty;

        /// <summary>
        /// Index of the stream axis.
        /// </summary>
        [JsonProperty("axisIndex")]
        public int AxisIndex { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AxisMapping))
            {
                return false;
            }

            AxisMapping other = (AxisMapping)obj;
            return (
                EqualityUtils.CheckEquals(AxisLetter, other.AxisLetter) &&
                EqualityUtils.CheckEquals(AxisIndex, other.AxisIndex)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(AxisLetter));
            hash.Add(EqualityUtils.GenerateHashCode(AxisIndex));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AxisMapping.ToByteArray(this);

        internal static AxisMapping FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AxisMapping>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AxisMapping instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
