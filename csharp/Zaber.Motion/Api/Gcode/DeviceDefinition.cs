/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Holds information about device and its axes for purpose of a translator.
    /// </summary>
    public class DeviceDefinition : IMessage
    {
        /// <summary>
        /// Device ID of the controller.
        /// Can be obtained from device settings.
        /// </summary>
        [JsonProperty("deviceId")]
        public int DeviceId { get; set; }

        /// <summary>
        /// Applicable axes of the device.
        /// </summary>
        [JsonProperty("axes")]
        public AxisDefinition[] Axes { get; set; } = System.Array.Empty<AxisDefinition>();

        /// <summary>
        /// The smallest of each axis' maxspeed setting value.
        /// This value becomes the traverse rate of the translator.
        /// </summary>
        [JsonProperty("maxSpeed")]
        public Measurement MaxSpeed { get; set; } = new Measurement { };

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is DeviceDefinition))
            {
                return false;
            }

            DeviceDefinition other = (DeviceDefinition)obj;
            return (
                EqualityUtils.CheckEquals(DeviceId, other.DeviceId) &&
                EqualityUtils.CheckEquals(Axes, other.Axes) &&
                EqualityUtils.CheckEquals(MaxSpeed, other.MaxSpeed)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(DeviceId));
            hash.Add(EqualityUtils.GenerateHashCode(Axes));
            hash.Add(EqualityUtils.GenerateHashCode(MaxSpeed));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => DeviceDefinition.ToByteArray(this);

        internal static DeviceDefinition FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<DeviceDefinition>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(DeviceDefinition instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
