/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Represents a transformation of a translator axis.
    /// </summary>
    public class AxisTransformation : IMessage
    {
        /// <summary>
        /// Letter of the translator axis (X,Y,Z,A,B,C,E).
        /// </summary>
        [JsonProperty("axisLetter")]
        public string AxisLetter { get; set; } = string.Empty;

        /// <summary>
        /// Scaling factor.
        /// </summary>
        [JsonProperty("scaling")]
        public double? Scaling { get; set; }

        /// <summary>
        /// Translation distance.
        /// </summary>
        [JsonProperty("translation")]
        public Measurement? Translation { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AxisTransformation))
            {
                return false;
            }

            AxisTransformation other = (AxisTransformation)obj;
            return (
                EqualityUtils.CheckEquals(AxisLetter, other.AxisLetter) &&
                EqualityUtils.CheckEquals(Scaling, other.Scaling) &&
                EqualityUtils.CheckEquals(Translation, other.Translation)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(AxisLetter));
            hash.Add(EqualityUtils.GenerateHashCode(Scaling));
            hash.Add(EqualityUtils.GenerateHashCode(Translation));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AxisTransformation.ToByteArray(this);

        internal static AxisTransformation FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AxisTransformation>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AxisTransformation instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
