﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Represents an offline G-Code translator.
    /// It allows to translate G-Code blocks to Zaber ASCII protocol stream commands.
    /// This translator does not need a connected device to perform translation.
    /// Requires at least Firmware 7.11.
    /// </summary>
    public class OfflineTranslator
    {
        internal OfflineTranslator(int translatorId)
        {
            TranslatorId = translatorId;
        }


        /// <summary>
        /// The ID of the translator that serves to identify native resources.
        /// </summary>
        public int TranslatorId { get; private set; }


        /// <summary>
        /// Current coordinate system.
        /// </summary>
        public string CoordinateSystem
        {
            get { return GetCurrentCoordinateSystem(); }
        }


        /// <summary>
        /// Sets up translator from provided device definition and configuration.
        /// </summary>
        /// <param name="definition">Definition of device and its peripherals.
        /// The definition must match a device that later performs the commands.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>A Task that can be awaited to get the result: New instance of translator.</returns>
        public static async Task<OfflineTranslator> SetupAsync(DeviceDefinition definition, TranslatorConfig? config = null)
        {
            var request = new Requests.TranslatorCreateRequest()
            {
                Definition = definition,
                Config = config,
            };

            var response = await Gateway.CallAsync("gcode/create", request, Requests.TranslatorCreateResponse.FromByteArray).ConfigureAwait(false);
            return new OfflineTranslator(response.TranslatorId);
        }


        /// <summary>
        /// Sets up translator from provided device definition and configuration.
        /// </summary>
        /// <param name="definition">Definition of device and its peripherals.
        /// The definition must match a device that later performs the commands.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>New instance of translator.</returns>
        public static OfflineTranslator Setup(DeviceDefinition definition, TranslatorConfig? config = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupAsync(definition, config);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets up an offline translator from provided device, axes, and configuration.
        /// </summary>
        /// <param name="device">Device that later performs the command streaming.</param>
        /// <param name="axes">Axis numbers that are later used to setup the stream.
        /// For a lockstep group specify only the first axis of the group.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>A Task that can be awaited to get the result: New instance of translator.</returns>
        public static async Task<OfflineTranslator> SetupFromDeviceAsync(Device device, int[] axes, TranslatorConfig? config = null)
        {
            var request = new Requests.TranslatorCreateFromDeviceRequest()
            {
                InterfaceId = device.Connection.InterfaceId,
                Device = device.DeviceAddress,
                Axes = axes,
                Config = config,
            };

            var response = await Gateway.CallAsync("gcode/create_from_device", request, Requests.TranslatorCreateResponse.FromByteArray).ConfigureAwait(false);
            return new OfflineTranslator(response.TranslatorId);
        }


        /// <summary>
        /// Sets up an offline translator from provided device, axes, and configuration.
        /// </summary>
        /// <param name="device">Device that later performs the command streaming.</param>
        /// <param name="axes">Axis numbers that are later used to setup the stream.
        /// For a lockstep group specify only the first axis of the group.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>New instance of translator.</returns>
        public static OfflineTranslator SetupFromDevice(Device device, int[] axes, TranslatorConfig? config = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupFromDeviceAsync(device, axes, config);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Translates a single block (line) of G-code.
        /// </summary>
        /// <param name="block">Block (line) of G-code.</param>
        /// <returns>Result of translation containing the stream commands.</returns>
        public TranslateResult Translate(string block)
        {
            var request = new Requests.TranslatorTranslateRequest()
            {
                TranslatorId = TranslatorId,
                Block = block,
            };

            var response = Gateway.CallSync("gcode/translate", request, TranslateResult.FromByteArray);
            return response;
        }


        /// <summary>
        /// Flushes the remaining stream commands waiting in optimization buffer.
        /// The flush is also performed by M2 and M30 codes.
        /// </summary>
        /// <returns>The remaining stream commands.</returns>
        public string[] Flush()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            var response = Gateway.CallSync("gcode/flush", request, Requests.TranslatorFlushResponse.FromByteArray);
            return response.Commands;
        }


        /// <summary>
        /// Sets the speed at which the device moves when traversing (G0).
        /// </summary>
        /// <param name="traverseRate">The traverse rate.</param>
        /// <param name="unit">Units of the traverse rate.</param>
        public void SetTraverseRate(double traverseRate, Units unit)
        {
            var request = new Requests.TranslatorSetTraverseRateRequest()
            {
                TranslatorId = TranslatorId,
                TraverseRate = traverseRate,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_traverse_rate", request);
        }


        /// <summary>
        /// Sets position of translator's axis.
        /// Use this method to set position after performing movement outside of the translator.
        /// This method does not cause any movement.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisPosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_position", request);
        }


        /// <summary>
        /// Gets position of translator's axis.
        /// This method does not query device but returns value from translator's state.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="unit">Units of position.</param>
        /// <returns>Position of translator's axis.</returns>
        public double GetAxisPosition(string axis, Units unit)
        {
            var request = new Requests.TranslatorGetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Unit = unit,
            };

            var response = Gateway.CallSync("gcode/get_axis_position", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets the home position of translator's axis.
        /// This position is used by G28.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The home position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisHomePosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_home", request);
        }


        /// <summary>
        /// Sets the secondary home position of translator's axis.
        /// This position is used by G30.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The home position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisSecondaryHomePosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_secondary_home", request);
        }


        /// <summary>
        /// Gets offset of an axis in a given coordinate system.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system (e.g. G54).</param>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="unit">Units of position.</param>
        /// <returns>Offset in translator units of the axis.</returns>
        public double GetAxisCoordinateSystemOffset(string coordinateSystem, string axis, Units unit)
        {
            var request = new Requests.TranslatorGetAxisOffsetRequest()
            {
                TranslatorId = TranslatorId,
                CoordinateSystem = coordinateSystem,
                Axis = axis,
                Unit = unit,
            };

            var response = Gateway.CallSync("gcode/get_axis_offset", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Resets internal state after device rejected generated command.
        /// Axis positions become uninitialized.
        /// </summary>
        public void ResetAfterStreamError()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            Gateway.CallSync("gcode/reset_after_stream_error", request);
        }


        /// <summary>
        /// Allows to scale feed rate of the translated code by a coefficient.
        /// </summary>
        /// <param name="coefficient">Coefficient of the original feed rate.</param>
        public void SetFeedRateOverride(double coefficient)
        {
            var request = new Requests.TranslatorSetFeedRateOverrideRequest()
            {
                TranslatorId = TranslatorId,
                Coefficient = coefficient,
            };

            Gateway.CallSync("gcode/set_feed_rate_override", request);
        }


        /// <summary>
        /// Releases native resources of a translator.
        /// </summary>
        /// <param name="translatorId">The ID of the translator.</param>
        private static void Free(int translatorId)
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = translatorId,
            };

            Gateway.CallSync("gcode/free", request);
        }


        /// <summary>
        /// Gets current coordinate system (e.g. G54).
        /// </summary>
        /// <returns>Current coordinate system.</returns>
        private string GetCurrentCoordinateSystem()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            var response = Gateway.CallSync("gcode/get_current_coordinate_system", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }



        /// <summary>
        /// Finalizes an instance of the <see cref="OfflineTranslator"/> class.
        /// </summary>
        ~OfflineTranslator()
        {
            OfflineTranslator.Free(TranslatorId);
        }
    }
}
