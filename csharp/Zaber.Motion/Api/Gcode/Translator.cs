﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Stream = Zaber.Motion.Ascii.Stream;

namespace Zaber.Motion.Gcode
{
    /// <summary>
    /// Represents a live G-Code translator.
    /// It allows to stream G-Code blocks to a connected device.
    /// It requires a stream to be setup on the device.
    /// Requires at least Firmware 7.11.
    /// </summary>
    public class Translator
    {
        internal Translator(int translatorId)
        {
            TranslatorId = translatorId;
        }


        /// <summary>
        /// The ID of the translator that serves to identify native resources.
        /// </summary>
        public int TranslatorId { get; private set; }


        /// <summary>
        /// Current coordinate system.
        /// </summary>
        public string CoordinateSystem
        {
            get { return GetCurrentCoordinateSystem(); }
        }


        /// <summary>
        /// Sets up the translator on top of a provided stream.
        /// </summary>
        /// <param name="stream">The stream to setup the translator on.
        /// The stream must be already setup in a live or a store mode.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>A Task that can be awaited to get the result: New instance of translator.</returns>
        public static async Task<Translator> SetupAsync(Stream stream, TranslatorConfig? config = null)
        {
            var request = new Requests.TranslatorCreateLiveRequest()
            {
                Device = stream.Device.DeviceAddress,
                InterfaceId = stream.Device.Connection.InterfaceId,
                StreamId = stream.StreamId,
                Config = config,
            };

            var response = await Gateway.CallAsync("gcode/create_live", request, Requests.TranslatorCreateResponse.FromByteArray).ConfigureAwait(false);
            return new Translator(response.TranslatorId);
        }


        /// <summary>
        /// Sets up the translator on top of a provided stream.
        /// </summary>
        /// <param name="stream">The stream to setup the translator on.
        /// The stream must be already setup in a live or a store mode.</param>
        /// <param name="config">Configuration of the translator.</param>
        /// <returns>New instance of translator.</returns>
        public static Translator Setup(Stream stream, TranslatorConfig? config = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetupAsync(stream, config);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Translates a single block (line) of G-code.
        /// The commands are queued in the underlying stream to ensure smooth continues movement.
        /// Returning of this method indicates that the commands are queued (not necessarily executed).
        /// </summary>
        /// <param name="block">Block (line) of G-code.</param>
        /// <returns>A Task that can be awaited to get the result: Result of translation containing the commands sent to the device.</returns>
        public async Task<TranslateResult> TranslateAsync(string block)
        {
            var request = new Requests.TranslatorTranslateRequest()
            {
                TranslatorId = TranslatorId,
                Block = block,
            };

            var response = await Gateway.CallAsync("gcode/translate_live", request, TranslateResult.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Translates a single block (line) of G-code.
        /// The commands are queued in the underlying stream to ensure smooth continues movement.
        /// Returning of this method indicates that the commands are queued (not necessarily executed).
        /// </summary>
        /// <param name="block">Block (line) of G-code.</param>
        /// <returns>Result of translation containing the commands sent to the device.</returns>
        public TranslateResult Translate(string block)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = TranslateAsync(block);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
        /// The flush is also performed by M2 and M30 codes.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether to wait for the stream to finish all the movements.</param>
        /// <returns>A Task that can be awaited to get the result: The remaining stream commands.</returns>
        public async Task<string[]> FlushAsync(bool waitUntilIdle = true)
        {
            var request = new Requests.TranslatorFlushLiveRequest()
            {
                TranslatorId = TranslatorId,
                WaitUntilIdle = waitUntilIdle,
            };

            var response = await Gateway.CallAsync("gcode/flush_live", request, Requests.TranslatorFlushResponse.FromByteArray).ConfigureAwait(false);
            return response.Commands;
        }


        /// <summary>
        /// Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
        /// The flush is also performed by M2 and M30 codes.
        /// </summary>
        /// <param name="waitUntilIdle">Determines whether to wait for the stream to finish all the movements.</param>
        /// <returns>The remaining stream commands.</returns>
        public string[] Flush(bool waitUntilIdle = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FlushAsync(waitUntilIdle);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Resets position of the translator from the underlying stream.
        /// Call this method after performing a movement outside of translator.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ResetPositionAsync()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            await Gateway.CallAsync("gcode/reset_position_from_stream", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Resets position of the translator from the underlying stream.
        /// Call this method after performing a movement outside of translator.
        /// </summary>
        public void ResetPosition()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ResetPositionAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the speed at which the device moves when traversing (G0).
        /// </summary>
        /// <param name="traverseRate">The traverse rate.</param>
        /// <param name="unit">Units of the traverse rate.</param>
        public void SetTraverseRate(double traverseRate, Units unit)
        {
            var request = new Requests.TranslatorSetTraverseRateRequest()
            {
                TranslatorId = TranslatorId,
                TraverseRate = traverseRate,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_traverse_rate", request);
        }


        /// <summary>
        /// Sets position of translator's axis.
        /// Use this method to set position after performing movement outside of the translator.
        /// This method does not cause any movement.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisPosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_position", request);
        }


        /// <summary>
        /// Gets position of translator's axis.
        /// This method does not query device but returns value from translator's state.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="unit">Units of position.</param>
        /// <returns>Position of translator's axis.</returns>
        public double GetAxisPosition(string axis, Units unit)
        {
            var request = new Requests.TranslatorGetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Unit = unit,
            };

            var response = Gateway.CallSync("gcode/get_axis_position", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Sets the home position of translator's axis.
        /// This position is used by G28.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The home position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisHomePosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_home", request);
        }


        /// <summary>
        /// Sets the secondary home position of translator's axis.
        /// This position is used by G30.
        /// </summary>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="position">The home position.</param>
        /// <param name="unit">Units of position.</param>
        public void SetAxisSecondaryHomePosition(string axis, double position, Units unit)
        {
            var request = new Requests.TranslatorSetAxisPositionRequest()
            {
                TranslatorId = TranslatorId,
                Axis = axis,
                Position = position,
                Unit = unit,
            };

            Gateway.CallSync("gcode/set_axis_secondary_home", request);
        }


        /// <summary>
        /// Gets offset of an axis in a given coordinate system.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system (e.g. G54).</param>
        /// <param name="axis">Letter of the axis.</param>
        /// <param name="unit">Units of position.</param>
        /// <returns>Offset in translator units of the axis.</returns>
        public double GetAxisCoordinateSystemOffset(string coordinateSystem, string axis, Units unit)
        {
            var request = new Requests.TranslatorGetAxisOffsetRequest()
            {
                TranslatorId = TranslatorId,
                CoordinateSystem = coordinateSystem,
                Axis = axis,
                Unit = unit,
            };

            var response = Gateway.CallSync("gcode/get_axis_offset", request, Requests.DoubleResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Resets internal state after device rejected generated command.
        /// Axis positions become uninitialized.
        /// </summary>
        public void ResetAfterStreamError()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            Gateway.CallSync("gcode/reset_after_stream_error", request);
        }


        /// <summary>
        /// Allows to scale feed rate of the translated code by a coefficient.
        /// </summary>
        /// <param name="coefficient">Coefficient of the original feed rate.</param>
        public void SetFeedRateOverride(double coefficient)
        {
            var request = new Requests.TranslatorSetFeedRateOverrideRequest()
            {
                TranslatorId = TranslatorId,
                Coefficient = coefficient,
            };

            Gateway.CallSync("gcode/set_feed_rate_override", request);
        }


        /// <summary>
        /// Releases native resources of a translator.
        /// </summary>
        /// <param name="translatorId">The ID of the translator.</param>
        private static void Free(int translatorId)
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = translatorId,
            };

            Gateway.CallSync("gcode/free", request);
        }


        /// <summary>
        /// Gets current coordinate system (e.g. G54).
        /// </summary>
        /// <returns>Current coordinate system.</returns>
        private string GetCurrentCoordinateSystem()
        {
            var request = new Requests.TranslatorEmptyRequest()
            {
                TranslatorId = TranslatorId,
            };

            var response = Gateway.CallSync("gcode/get_current_coordinate_system", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }



        /// <summary>
        /// Finalizes an instance of the <see cref="Translator"/> class.
        /// </summary>
        ~Translator()
        {
            Translator.Free(TranslatorId);
        }
    }
}
