﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

#pragma warning disable CA1054

using System.Linq;

using Zaber.Motion.Exceptions;
using Zaber.Motion.Runtime;

namespace Zaber.Motion
{
    /// <summary>
    /// Helper for working with units of measure.
    /// </summary>
    public static class UnitTable
    {
        /// <summary>
        /// Gets the standard symbol associated with a given unit.
        /// </summary>
        /// <param name="unit">Unit of measure.</param>
        /// <returns>Symbols corresponding to the given unit. Throws NoValueForKey if no symbol is defined.</returns>
        public static string GetSymbol(Units unit)
        {
            var request = new Requests.UnitGetSymbolRequest()
            {
                Unit = unit,
            };

            var response = Gateway.CallSync("units/get_symbol", request, Requests.UnitGetSymbolResponse.FromByteArray);
            return response.Symbol;
        }


        /// <summary>
        /// Gets the unit enum value associated with a standard symbol.
        /// Note not all units can be retrieved this way.
        /// </summary>
        /// <param name="symbol">Symbol to look up.</param>
        /// <returns>The unit enum value with the given symbols. Throws NoValueForKey if the symbol is not supported for lookup.</returns>
        public static Units GetUnit(string symbol)
        {
            var request = new Requests.UnitGetEnumRequest()
            {
                Symbol = symbol,
            };

            var response = Gateway.CallSync("units/get_enum", request, Requests.UnitGetEnumResponse.FromByteArray);
            return response.Unit;
        }



        /// <summary>
        /// Get unit symbol from a variable or constant.
        /// </summary>
        /// <param name="unit">Unit of measure.</param>
        /// <returns>Standard symbols for the given unit on measure.</returns>
        /// <exception cref="NoValueForKeyException">There is no symbol defined for the given unit.</exception>
        public static string Symbol(this Units unit) => GetSymbol(unit);
    }
}
