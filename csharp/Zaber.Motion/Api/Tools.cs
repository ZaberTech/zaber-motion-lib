﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion
{
    /// <summary>
    /// Class providing various utility functions.
    /// </summary>
    public static class Tools
    {
        /// <summary>
        /// Lists all serial ports on the computer.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Array of serial port names.</returns>
        public static async Task<string[]> ListSerialPortsAsync()
        {
            var request = new Requests.EmptyRequest()
            {
            };

            var response = await Gateway.CallAsync("tools/list_serial_ports", request, Requests.ToolsListSerialPortsResponse.FromByteArray).ConfigureAwait(false);
            return response.Ports;
        }


        /// <summary>
        /// Lists all serial ports on the computer.
        /// </summary>
        /// <returns>Array of serial port names.</returns>
        public static string[] ListSerialPorts()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ListSerialPortsAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns path of message router named pipe on Windows
        /// or file path of unix domain socket on UNIX.
        /// </summary>
        /// <returns>Path of message router's named pipe or unix domain socket.</returns>
        public static string GetMessageRouterPipePath()
        {
            var request = new Requests.EmptyRequest()
            {
            };

            var response = Gateway.CallSync("tools/get_message_router_pipe", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Returns the path for communicating with a local device database service.
        /// This will be a named pipe on Windows and the file path of a unix domain socket on UNIX.
        /// </summary>
        /// <returns>Path of database service's named pipe or unix domain socket.</returns>
        public static string GetDbServicePipePath()
        {
            var request = new Requests.EmptyRequest()
            {
            };

            var response = Gateway.CallSync("tools/get_db_service_pipe", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
