/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Configuration representing a microscope setup.
    /// Device address of value 0 means that the part is not present.
    /// </summary>
    public class MicroscopeConfig : IMessage
    {
        /// <summary>
        /// Focus axis of the microscope.
        /// </summary>
        [JsonProperty("focusAxis")]
        public AxisAddress? FocusAxis { get; set; }

        /// <summary>
        /// X axis of the microscope.
        /// </summary>
        [JsonProperty("xAxis")]
        public AxisAddress? XAxis { get; set; }

        /// <summary>
        /// Y axis of the microscope.
        /// </summary>
        [JsonProperty("yAxis")]
        public AxisAddress? YAxis { get; set; }

        /// <summary>
        /// Illuminator device address.
        /// </summary>
        [JsonProperty("illuminator")]
        public int? Illuminator { get; set; }

        /// <summary>
        /// Filter changer device address.
        /// </summary>
        [JsonProperty("filterChanger")]
        public int? FilterChanger { get; set; }

        /// <summary>
        /// Objective changer device address.
        /// </summary>
        [JsonProperty("objectiveChanger")]
        public int? ObjectiveChanger { get; set; }

        /// <summary>
        /// Autofocus identifier.
        /// </summary>
        [JsonProperty("autofocus")]
        public int? Autofocus { get; set; }

        /// <summary>
        /// Camera trigger digital output address.
        /// </summary>
        [JsonProperty("cameraTrigger")]
        public ChannelAddress? CameraTrigger { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is MicroscopeConfig))
            {
                return false;
            }

            MicroscopeConfig other = (MicroscopeConfig)obj;
            return (
                EqualityUtils.CheckEquals(FocusAxis, other.FocusAxis) &&
                EqualityUtils.CheckEquals(XAxis, other.XAxis) &&
                EqualityUtils.CheckEquals(YAxis, other.YAxis) &&
                EqualityUtils.CheckEquals(Illuminator, other.Illuminator) &&
                EqualityUtils.CheckEquals(FilterChanger, other.FilterChanger) &&
                EqualityUtils.CheckEquals(ObjectiveChanger, other.ObjectiveChanger) &&
                EqualityUtils.CheckEquals(Autofocus, other.Autofocus) &&
                EqualityUtils.CheckEquals(CameraTrigger, other.CameraTrigger)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(FocusAxis));
            hash.Add(EqualityUtils.GenerateHashCode(XAxis));
            hash.Add(EqualityUtils.GenerateHashCode(YAxis));
            hash.Add(EqualityUtils.GenerateHashCode(Illuminator));
            hash.Add(EqualityUtils.GenerateHashCode(FilterChanger));
            hash.Add(EqualityUtils.GenerateHashCode(ObjectiveChanger));
            hash.Add(EqualityUtils.GenerateHashCode(Autofocus));
            hash.Add(EqualityUtils.GenerateHashCode(CameraTrigger));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => MicroscopeConfig.ToByteArray(this);

        internal static MicroscopeConfig FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<MicroscopeConfig>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(MicroscopeConfig instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
