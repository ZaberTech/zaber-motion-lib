﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;
using DeviceIO = Zaber.Motion.Ascii.DeviceIO;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Use to manage an LED controller.
    /// Requires at least Firmware 7.09.
    /// </summary>
    public class Illuminator
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `Illuminator` based on the given device.
        /// If the device is identified, this constructor will ensure it is an illuminator.
        /// </summary>
        public Illuminator(Device device)
        {
            Device = device;
            IO = new DeviceIO(device);
            VerifyIsIlluminator();
        }
#pragma warning restore SA1611


        /// <summary>
        /// The base device of this illuminator.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// I/O channels of this device.
        /// </summary>
        public DeviceIO IO { get; private set; }


        /// <summary>
        /// Finds an illuminator on a connection.
        /// In case of conflict, specify the optional device address.
        /// </summary>
        /// <param name="connection">Connection on which to detect the illuminator.</param>
        /// <param name="deviceAddress">Optional device address of the illuminator.</param>
        /// <returns>A Task that can be awaited to get the result: New instance of illuminator.</returns>
        public static async Task<Illuminator> FindAsync(Connection connection, int deviceAddress = 0)
        {
            var request = new Requests.FindDeviceRequest()
            {
                InterfaceId = connection.InterfaceId,
                DeviceAddress = deviceAddress,
            };

            var response = await Gateway.CallAsync("illuminator/detect", request, Requests.FindDeviceResponse.FromByteArray).ConfigureAwait(false);
            return new Illuminator(new Device(connection, response.Address));
        }


        /// <summary>
        /// Finds an illuminator on a connection.
        /// In case of conflict, specify the optional device address.
        /// </summary>
        /// <param name="connection">Connection on which to detect the illuminator.</param>
        /// <param name="deviceAddress">Optional device address of the illuminator.</param>
        /// <returns>New instance of illuminator.</returns>
        public static Illuminator Find(Connection connection, int deviceAddress = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FindAsync(connection, deviceAddress);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets an IlluminatorChannel class instance that allows control of a particular channel.
        /// Channels are numbered from 1.
        /// </summary>
        /// <param name="channelNumber">Number of channel to control.</param>
        /// <returns>Illuminator channel instance.</returns>
        public IlluminatorChannel GetChannel(int channelNumber)
        {
            if (channelNumber <= 0)
            {
                throw new ArgumentException("Invalid value; channels are numbered from 1.");
            }

            return new IlluminatorChannel(this, channelNumber);
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Checks if this is an illuminator or some other type of device and throws an error if it is not.
        /// </summary>
        private void VerifyIsIlluminator()
        {
            var request = new Requests.DeviceEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            Gateway.CallSync("illuminator/verify", request);
        }


    }
}
