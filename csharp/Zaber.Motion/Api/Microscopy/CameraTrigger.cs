﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// An abstraction over a device and it's digital output channel.
    /// </summary>
    public class CameraTrigger
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `CameraTrigger` based on the given device and digital output channel.
        /// </summary>
        public CameraTrigger(Device device, int channel)
        {
            Device = device;
            Channel = channel;
        }
#pragma warning restore SA1611


        /// <summary>
        /// The device whose digital output triggers the camera.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// The digital output channel that triggers the camera.
        /// </summary>
        public int Channel { get; private set; }


        /// <summary>
        /// Triggers the camera.
        /// Schedules trigger pulse on the digital output channel.
        /// By default, the method waits until the trigger pulse is finished.
        /// </summary>
        /// <param name="pulseWidth">The time duration of the trigger pulse.
        /// Depending on the camera setting, the argument can be use to specify exposure.</param>
        /// <param name="unit">Units of time.</param>
        /// <param name="wait">If false, the method does not wait until the trigger pulse is finished.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task TriggerAsync(double pulseWidth, Units unit = Units.Native, bool wait = true)
        {
            var request = new Requests.MicroscopeTriggerCameraRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                ChannelNumber = Channel,
                Delay = pulseWidth,
                Unit = unit,
                Wait = wait,
            };

            await Gateway.CallAsync("microscope/trigger_camera", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Triggers the camera.
        /// Schedules trigger pulse on the digital output channel.
        /// By default, the method waits until the trigger pulse is finished.
        /// </summary>
        /// <param name="pulseWidth">The time duration of the trigger pulse.
        /// Depending on the camera setting, the argument can be use to specify exposure.</param>
        /// <param name="unit">Units of time.</param>
        /// <param name="wait">If false, the method does not wait until the trigger pulse is finished.</param>
        public void Trigger(double pulseWidth, Units unit = Units.Native, bool wait = true)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = TriggerAsync(pulseWidth, unit, wait);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
