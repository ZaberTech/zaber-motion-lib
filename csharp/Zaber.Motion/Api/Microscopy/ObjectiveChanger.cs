﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Represents an objective changer of a microscope.
    /// Unstable. Expect breaking changes in future releases.
    /// Requires at least Firmware 7.32.
    /// </summary>
    public class ObjectiveChanger
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `ObjectiveChanger` based on the given device.
        /// If the device is identified, this constructor will ensure it is an objective changer.
        /// </summary>
        public ObjectiveChanger(Device turret, Axis focusAxis)
        {
            Turret = turret;
            FocusAxis = focusAxis;
            VerifyIsChanger();
        }
#pragma warning restore SA1611


        /// <summary>
        /// Device address of the turret.
        /// </summary>
        public Device Turret { get; private set; }


        /// <summary>
        /// The focus axis.
        /// </summary>
        public Axis FocusAxis { get; private set; }


        /// <summary>
        /// Finds an objective changer on a connection.
        /// In case of conflict, specify the optional device addresses.
        /// Devices on the connection must be identified.
        /// </summary>
        /// <param name="connection">Connection on which to detect the objective changer.</param>
        /// <param name="turretAddress">Optional device address of the turret device (X-MOR).</param>
        /// <param name="focusAddress">Optional device address of the focus device (X-LDA).</param>
        /// <returns>A Task that can be awaited to get the result: New instance of objective changer.</returns>
        [Obsolete("Use microscope's `Find` method instead or instantiate manually.")]
        public static async Task<ObjectiveChanger> FindAsync(Connection connection, int turretAddress = 0, int focusAddress = 0)
        {
            var request = new Requests.ObjectiveChangerRequest()
            {
                InterfaceId = connection.InterfaceId,
                TurretAddress = turretAddress,
                FocusAddress = focusAddress,
            };

            var response = await Gateway.CallAsync("objective_changer/detect", request, Requests.ObjectiveChangerCreateResponse.FromByteArray).ConfigureAwait(false);
            return new ObjectiveChanger(new Device(connection, response.Turret), new Axis(new Device(connection, response.FocusAddress), response.FocusAxis));
        }


        /// <summary>
        /// Finds an objective changer on a connection.
        /// In case of conflict, specify the optional device addresses.
        /// Devices on the connection must be identified.
        /// </summary>
        /// <param name="connection">Connection on which to detect the objective changer.</param>
        /// <param name="turretAddress">Optional device address of the turret device (X-MOR).</param>
        /// <param name="focusAddress">Optional device address of the focus device (X-LDA).</param>
        /// <returns>New instance of objective changer.</returns>
        [Obsolete("Use microscope's `Find` method instead or instantiate manually.")]
        public static ObjectiveChanger Find(Connection connection, int turretAddress = 0, int focusAddress = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FindAsync(connection, turretAddress, focusAddress);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Changes the objective.
        /// Runs a sequence of movements switching from the current objective to the new one.
        /// The focus stage moves to the focus datum after the objective change.
        /// </summary>
        /// <param name="objective">Objective number starting from 1.</param>
        /// <param name="focusOffset">Optional offset from the focus datum.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ChangeAsync(int objective, Measurement? focusOffset = null)
        {
            var request = new Requests.ObjectiveChangerChangeRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                TurretAddress = Turret.DeviceAddress,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                Objective = objective,
                FocusOffset = focusOffset,
            };

            await Gateway.CallAsync("objective_changer/change", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Changes the objective.
        /// Runs a sequence of movements switching from the current objective to the new one.
        /// The focus stage moves to the focus datum after the objective change.
        /// </summary>
        /// <param name="objective">Objective number starting from 1.</param>
        /// <param name="focusOffset">Optional offset from the focus datum.</param>
        public void Change(int objective, Measurement? focusOffset = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ChangeAsync(objective, focusOffset);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the focus stage out of the turret releasing the current objective.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ReleaseAsync()
        {
            var request = new Requests.ObjectiveChangerRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                TurretAddress = Turret.DeviceAddress,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
            };

            await Gateway.CallAsync("objective_changer/release", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the focus stage out of the turret releasing the current objective.
        /// </summary>
        public void Release()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ReleaseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns current objective number starting from 1.
        /// The value of 0 indicates that the position is either unknown or between two objectives.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Current objective number starting from 1 or 0 if not applicable.</returns>
        public async Task<int> GetCurrentObjectiveAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                Device = Turret.DeviceAddress,
                Axis = 1,
            };

            var response = await Gateway.CallAsync("device/get_index_position", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns current objective number starting from 1.
        /// The value of 0 indicates that the position is either unknown or between two objectives.
        /// </summary>
        /// <returns>Current objective number starting from 1 or 0 if not applicable.</returns>
        public int GetCurrentObjective()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetCurrentObjectiveAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets number of objectives that the turret can accommodate.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of positions.</returns>
        public async Task<int> GetNumberOfObjectivesAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                Device = Turret.DeviceAddress,
                Axis = 1,
            };

            var response = await Gateway.CallAsync("device/get_index_count", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets number of objectives that the turret can accommodate.
        /// </summary>
        /// <returns>Number of positions.</returns>
        public int GetNumberOfObjectives()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberOfObjectivesAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the focus datum.
        /// The focus datum is the position that the focus stage moves to after an objective change.
        /// It is backed by the limit.home.offset setting.
        /// </summary>
        /// <param name="unit">Units of datum.</param>
        /// <returns>A Task that can be awaited to get the result: The datum.</returns>
        public async Task<double> GetFocusDatumAsync(Units unit = Units.Native)
        {
            var request = new Requests.ObjectiveChangerSetRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                TurretAddress = Turret.DeviceAddress,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                Unit = unit,
            };

            var response = await Gateway.CallAsync("objective_changer/get_datum", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the focus datum.
        /// The focus datum is the position that the focus stage moves to after an objective change.
        /// It is backed by the limit.home.offset setting.
        /// </summary>
        /// <param name="unit">Units of datum.</param>
        /// <returns>The datum.</returns>
        public double GetFocusDatum(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetFocusDatumAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the focus datum.
        /// The focus datum is the position that the focus stage moves to after an objective change.
        /// It is backed by the limit.home.offset setting.
        /// </summary>
        /// <param name="datum">Value of datum.</param>
        /// <param name="unit">Units of datum.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetFocusDatumAsync(double datum, Units unit = Units.Native)
        {
            var request = new Requests.ObjectiveChangerSetRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                TurretAddress = Turret.DeviceAddress,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                Value = datum,
                Unit = unit,
            };

            await Gateway.CallAsync("objective_changer/set_datum", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the focus datum.
        /// The focus datum is the position that the focus stage moves to after an objective change.
        /// It is backed by the limit.home.offset setting.
        /// </summary>
        /// <param name="datum">Value of datum.</param>
        /// <param name="unit">Units of datum.</param>
        public void SetFocusDatum(double datum, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetFocusDatumAsync(datum, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                Device = Turret.DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        /// <summary>
        /// Checks if this is a objective changer and throws an error if it is not.
        /// </summary>
        private void VerifyIsChanger()
        {
            var request = new Requests.ObjectiveChangerRequest()
            {
                InterfaceId = Turret.Connection.InterfaceId,
                TurretAddress = Turret.DeviceAddress,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
            };

            Gateway.CallSync("objective_changer/verify", request);
        }


    }
}
