/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Status of the autofocus.
    /// </summary>
    public class AutofocusStatus : IMessage
    {
        /// <summary>
        /// Indicates whether the autofocus is in focus.
        /// </summary>
        [JsonProperty("inFocus")]
        public bool InFocus { get; set; }

        /// <summary>
        /// Indicates whether the autofocus is in range.
        /// </summary>
        [JsonProperty("inRange")]
        public bool InRange { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is AutofocusStatus))
            {
                return false;
            }

            AutofocusStatus other = (AutofocusStatus)obj;
            return (
                EqualityUtils.CheckEquals(InFocus, other.InFocus) &&
                EqualityUtils.CheckEquals(InRange, other.InRange)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(InFocus));
            hash.Add(EqualityUtils.GenerateHashCode(InRange));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => AutofocusStatus.ToByteArray(this);

        internal static AutofocusStatus FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<AutofocusStatus>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(AutofocusStatus instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
