﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Class representing access to WDI Autofocus connection.
    /// </summary>
#if NET || NETSTANDARD
    public class WdiAutofocusProvider : IDisposable, IAsyncDisposable
#else
    public class WdiAutofocusProvider : IDisposable
#endif
    {
        /// <summary>
        /// Finalizes an instance of the <see cref="WdiAutofocusProvider"/> class.
        /// </summary>
        ~WdiAutofocusProvider()
        {
            Dispose(false);
        }


        internal WdiAutofocusProvider(int providerId)
        {
            ProviderId = providerId;
        }


        /// <summary>
        /// Default port number for TCP connections to WDI autofocus.
        /// </summary>
        public const int DefaultTcpPort = 27;


        /// <summary>
        /// The ID identifies the autofocus with the underlying library.
        /// </summary>
        public int ProviderId { get; private set; }


        /// <summary>
        /// Opens a TCP connection to WDI autofocus.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Optional port number (defaults to 27).</param>
        /// <returns>A Task that can be awaited to get the result: An object representing the autofocus connection.</returns>
        public static async Task<WdiAutofocusProvider> OpenTcpAsync(string hostName, int port = DefaultTcpPort)
        {
            var request = new Requests.OpenInterfaceRequest()
            {
                InterfaceType = Requests.InterfaceType.Tcp,
                HostName = hostName,
                Port = port,
            };

            var response = await Gateway.CallAsync("wdi/open", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return new WdiAutofocusProvider(response.Value);
        }


        /// <summary>
        /// Opens a TCP connection to WDI autofocus.
        /// </summary>
        /// <param name="hostName">Hostname or IP address.</param>
        /// <param name="port">Optional port number (defaults to 27).</param>
        /// <returns>An object representing the autofocus connection.</returns>
        public static WdiAutofocusProvider OpenTcp(string hostName, int port = DefaultTcpPort)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OpenTcpAsync(hostName, port);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task CloseAsync()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = ProviderId,
            };

            await Gateway.CallAsync("wdi/close", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Close the connection.
        /// </summary>
        public void Close()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CloseAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Generic read operation.
        /// </summary>
        /// <param name="registerId">Register address to read from.</param>
        /// <param name="size">Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).</param>
        /// <param name="count">Number of values to read (defaults to 1).</param>
        /// <param name="offset">Offset within the register (defaults to 0).</param>
        /// <param name="registerBank">Register bank letter (defaults to U for user bank).</param>
        /// <returns>A Task that can be awaited to get the result: Array of integers read from the device.</returns>
        public async Task<int[]> GenericReadAsync(int registerId, int size, int count = 1, int offset = 0, string registerBank = "U")
        {
            var request = new Requests.WdiGenericRequest()
            {
                InterfaceId = ProviderId,
                RegisterId = registerId,
                Size = size,
                Count = count,
                Offset = offset,
                RegisterBank = registerBank,
            };

            var response = await Gateway.CallAsync("wdi/read", request, Requests.IntArrayResponse.FromByteArray).ConfigureAwait(false);
            return response.Values;
        }


        /// <summary>
        /// Generic read operation.
        /// </summary>
        /// <param name="registerId">Register address to read from.</param>
        /// <param name="size">Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).</param>
        /// <param name="count">Number of values to read (defaults to 1).</param>
        /// <param name="offset">Offset within the register (defaults to 0).</param>
        /// <param name="registerBank">Register bank letter (defaults to U for user bank).</param>
        /// <returns>Array of integers read from the device.</returns>
        public int[] GenericRead(int registerId, int size, int count = 1, int offset = 0, string registerBank = "U")
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericReadAsync(registerId, size, count, offset, registerBank);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Generic write operation.
        /// </summary>
        /// <param name="registerId">Register address to read from.</param>
        /// <param name="size">Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).</param>
        /// <param name="data">Array of values to write to the register. Empty array is allowed.</param>
        /// <param name="offset">Offset within the register (defaults to 0).</param>
        /// <param name="registerBank">Register bank letter (defaults to U for user bank).</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericWriteAsync(int registerId, int size = 0, int[] data = null!, int offset = 0, string registerBank = "U")
        {
            var request = new Requests.WdiGenericRequest()
            {
                InterfaceId = ProviderId,
                RegisterId = registerId,
                Size = size,
                Data = data,
                Offset = offset,
                RegisterBank = registerBank,
            };

            await Gateway.CallAsync("wdi/write", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Generic write operation.
        /// </summary>
        /// <param name="registerId">Register address to read from.</param>
        /// <param name="size">Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).</param>
        /// <param name="data">Array of values to write to the register. Empty array is allowed.</param>
        /// <param name="offset">Offset within the register (defaults to 0).</param>
        /// <param name="registerBank">Register bank letter (defaults to U for user bank).</param>
        public void GenericWrite(int registerId, int size = 0, int[] data = null!, int offset = 0, string registerBank = "U")
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericWriteAsync(registerId, size, data, offset, registerBank);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Enables the laser.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task EnableLaserAsync()
        {
            var request = new Requests.WdiGenericRequest()
            {
                InterfaceId = ProviderId,
                RegisterId = 1,
            };

            await Gateway.CallAsync("wdi/write", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Enables the laser.
        /// </summary>
        public void EnableLaser()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = EnableLaserAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Disables the laser.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task DisableLaserAsync()
        {
            var request = new Requests.WdiGenericRequest()
            {
                InterfaceId = ProviderId,
                RegisterId = 2,
            };

            await Gateway.CallAsync("wdi/write", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Disables the laser.
        /// </summary>
        public void DisableLaser()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = DisableLaserAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the status of the autofocus.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The status of the autofocus.</returns>
        public async Task<WdiAutofocusProviderStatus> GetStatusAsync()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = ProviderId,
            };

            var response = await Gateway.CallAsync("wdi/get_status", request, Requests.WdiGetStatusResponse.FromByteArray).ConfigureAwait(false);
            return response.Status;
        }


        /// <summary>
        /// Gets the status of the autofocus.
        /// </summary>
        /// <returns>The status of the autofocus.</returns>
        public WdiAutofocusProviderStatus GetStatus()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStatusAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the autofocus connection.
        /// </summary>
        /// <returns>A string that represents the connection.</returns>
        public override string ToString()
        {
            var request = new Requests.InterfaceEmptyRequest()
            {
                InterfaceId = ProviderId,
            };

            var response = Gateway.CallSync("wdi/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


#pragma warning disable SA1202
        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#pragma warning restore SA1202


        /// <summary>
        /// Dispose pattern function.
        /// </summary>
        /// <param name="disposing">True when function is called from Dispose().</param>
        protected virtual void Dispose(bool disposing)
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            Close();
        }


#if NET || NETSTANDARD
#pragma warning disable SA1202
        /// <summary>
        /// IAsyncDisposable implementation.
        /// </summary>
        /// <returns>A task meant to be awaited by the "await using" statement.</returns>
        public async ValueTask DisposeAsync()
        {
            if (Interlocked.CompareExchange(ref _disposed, 1, 0) != 0)
            {
                return;
            }

            await DisposeAsyncCore().ConfigureAwait(false);
            await CloseAsync().ConfigureAwait(false);

#pragma warning disable CA1816
            GC.SuppressFinalize(this);
#pragma warning restore CA1816
        }
#pragma warning restore SA1202


        /// <summary>
        /// Enables subclasses to dispose of their own resources.
        /// </summary>
        /// <returns>Immediately.</returns>
        protected virtual async ValueTask DisposeAsyncCore()
            => await Task.FromResult(0).ConfigureAwait(false);
#endif

        private int _disposed;
    }
}
