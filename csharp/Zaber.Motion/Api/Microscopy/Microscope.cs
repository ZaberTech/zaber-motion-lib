﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using AxisGroup = Zaber.Motion.Ascii.AxisGroup;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Represent a microscope.
    /// Parts of the microscope may or may not be instantiated depending on the configuration.
    /// Requires at least Firmware 7.34.
    /// </summary>
    public class Microscope
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `Microscope` from the given config.
        /// Parts are instantiated depending on device addresses in the config.
        /// </summary>
        public Microscope(Connection connection, MicroscopeConfig config)
        {
            Connection = connection;
            _config = MicroscopeConfig.FromByteArray((config as Runtime.IMessage).ToByteArray());
            Illuminator = config.Illuminator > 0 ? new Illuminator(new Device(connection, config.Illuminator.Value)) : null;
            FocusAxis = config.FocusAxis?.Device > 0 ? new Axis(new Device(connection, config.FocusAxis.Device), config.FocusAxis.Axis) : null;
            XAxis = config.XAxis?.Device > 0 ? new Axis(new Device(connection, config.XAxis.Device), config.XAxis.Axis) : null;
            YAxis = config.YAxis?.Device > 0 ? new Axis(new Device(connection, config.YAxis.Device), config.YAxis.Axis) : null;
            Plate = XAxis != null && YAxis != null ? new AxisGroup(new Axis[] { XAxis, YAxis }) : null;
            ObjectiveChanger = config.ObjectiveChanger > 0 && FocusAxis != null ? new ObjectiveChanger(new Device(connection, config.ObjectiveChanger.Value), FocusAxis) : null;
            FilterChanger = config.FilterChanger > 0 ? new FilterChanger(new Device(connection, config.FilterChanger.Value)) : null;
            Autofocus = config.Autofocus > 0 && FocusAxis != null ? new Autofocus(config.Autofocus.Value, FocusAxis, ObjectiveChanger?.Turret) : null;
            CameraTrigger = config.CameraTrigger?.Device > 0 ? new CameraTrigger(new Device(connection, config.CameraTrigger.Device), config.CameraTrigger.Channel) : null;
        }
#pragma warning restore SA1611


        /// <summary>
        /// Connection of the microscope.
        /// </summary>
        public Connection Connection { get; private set; }


        /// <summary>
        /// The illuminator.
        /// </summary>
        public Illuminator? Illuminator { get; private set; }


        /// <summary>
        /// The focus axis.
        /// </summary>
        public Axis? FocusAxis { get; private set; }


        /// <summary>
        /// The X axis.
        /// </summary>
        public Axis? XAxis { get; private set; }


        /// <summary>
        /// The Y axis.
        /// </summary>
        public Axis? YAxis { get; private set; }


        /// <summary>
        /// Axis group consisting of X and Y axes representing the plate of the microscope.
        /// </summary>
        public AxisGroup? Plate { get; private set; }


        /// <summary>
        /// The objective changer.
        /// </summary>
        public ObjectiveChanger? ObjectiveChanger { get; private set; }


        /// <summary>
        /// The filter changer.
        /// </summary>
        public FilterChanger? FilterChanger { get; private set; }


        /// <summary>
        /// The autofocus feature.
        /// </summary>
        public Autofocus? Autofocus { get; private set; }


        /// <summary>
        /// The camera trigger.
        /// </summary>
        public CameraTrigger? CameraTrigger { get; private set; }


        /// <summary>
        /// Finds a microscope on a connection.
        /// </summary>
        /// <param name="connection">Connection on which to detect the microscope.</param>
        /// <param name="thirdPartyComponents">Third party components of the microscope that cannot be found on the connection.</param>
        /// <returns>A Task that can be awaited to get the result: New instance of microscope.</returns>
        public static async Task<Microscope> FindAsync(Connection connection, ThirdPartyComponents? thirdPartyComponents = null)
        {
            var request = new Requests.MicroscopeFindRequest()
            {
                InterfaceId = connection.InterfaceId,
                ThirdParty = thirdPartyComponents,
            };

            var response = await Gateway.CallAsync("microscope/detect", request, Requests.MicroscopeConfigResponse.FromByteArray).ConfigureAwait(false);
            return new Microscope(connection, response.Config);
        }


        /// <summary>
        /// Finds a microscope on a connection.
        /// </summary>
        /// <param name="connection">Connection on which to detect the microscope.</param>
        /// <param name="thirdPartyComponents">Third party components of the microscope that cannot be found on the connection.</param>
        /// <returns>New instance of microscope.</returns>
        public static Microscope Find(Connection connection, ThirdPartyComponents? thirdPartyComponents = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FindAsync(connection, thirdPartyComponents);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Initializes the microscope.
        /// Homes all axes, filter changer, and objective changer if they require it.
        /// </summary>
        /// <param name="force">Forces all devices to home even when not required.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task InitializeAsync(bool force = false)
        {
            var request = new Requests.MicroscopeInitRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Config = _config,
                Force = force,
            };

            await Gateway.CallAsync("microscope/initialize", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Initializes the microscope.
        /// Homes all axes, filter changer, and objective changer if they require it.
        /// </summary>
        /// <param name="force">Forces all devices to home even when not required.</param>
        public void Initialize(bool force = false)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = InitializeAsync(force);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks whether the microscope is initialized.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True, when the microscope is initialized. False, otherwise.</returns>
        public async Task<bool> IsInitializedAsync()
        {
            var request = new Requests.MicroscopeEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Config = _config,
            };

            var response = await Gateway.CallAsync("microscope/is_initialized", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Checks whether the microscope is initialized.
        /// </summary>
        /// <returns>True, when the microscope is initialized. False, otherwise.</returns>
        public bool IsInitialized()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsInitializedAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the microscope.
        /// </summary>
        /// <returns>A string that represents the microscope.</returns>
        public override string ToString()
        {
            var request = new Requests.MicroscopeEmptyRequest()
            {
                InterfaceId = Connection.InterfaceId,
                Config = _config,
            };

            var response = Gateway.CallSync("microscope/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        private MicroscopeConfig _config;


    }
}
