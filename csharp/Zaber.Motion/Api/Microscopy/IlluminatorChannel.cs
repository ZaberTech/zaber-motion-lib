﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System.Threading.Tasks;

using Zaber.Motion.Ascii;
using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using AxisSettings = Zaber.Motion.Ascii.AxisSettings;
using AxisStorage = Zaber.Motion.Ascii.AxisStorage;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;
using Response = Zaber.Motion.Ascii.Response;
using Warnings = Zaber.Motion.Ascii.Warnings;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// Use to control a channel (LED lamp) on an illuminator.
    /// Requires at least Firmware 7.09.
    /// </summary>
    public class IlluminatorChannel
    {
        internal IlluminatorChannel(Illuminator illuminator, int channelNumber)
        {
            Illuminator = illuminator;
            ChannelNumber = channelNumber;
            _axis = new Axis(illuminator.Device, channelNumber);
            Settings = new AxisSettings(_axis);
            Storage = new AxisStorage(_axis);
            Warnings = new Warnings(illuminator.Device, channelNumber);
        }


        /// <summary>
        /// Illuminator of this channel.
        /// </summary>
        public Illuminator Illuminator { get; private set; }


        /// <summary>
        /// The channel number identifies the channel on the illuminator.
        /// </summary>
        public int ChannelNumber { get; private set; }


        /// <summary>
        /// Settings and properties of this channel.
        /// </summary>
        public AxisSettings Settings { get; private set; }


        /// <summary>
        /// Key-value storage of this channel.
        /// </summary>
        public AxisStorage Storage { get; private set; }


        /// <summary>
        /// Warnings and faults of this channel.
        /// </summary>
        public Warnings Warnings { get; private set; }


        /// <summary>
        /// Turns this channel on.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OnAsync()
        {
            var request = new Requests.ChannelOn()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                On = true,
            };

            await Gateway.CallAsync("illuminator/on", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Turns this channel on.
        /// </summary>
        public void On()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OnAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Turns this channel off.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task OffAsync()
        {
            var request = new Requests.ChannelOn()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                On = false,
            };

            await Gateway.CallAsync("illuminator/on", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Turns this channel off.
        /// </summary>
        public void Off()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = OffAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Turns this channel on or off.
        /// </summary>
        /// <param name="on">True to turn channel on, false to turn it off.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetOnAsync(bool on)
        {
            var request = new Requests.ChannelOn()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                On = on,
            };

            await Gateway.CallAsync("illuminator/on", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Turns this channel on or off.
        /// </summary>
        /// <param name="on">True to turn channel on, false to turn it off.</param>
        public void SetOn(bool on)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetOnAsync(on);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if this channel is on.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: True if channel is on, false otherwise.</returns>
        public async Task<bool> IsOnAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
            };

            var response = await Gateway.CallAsync("illuminator/is_on", request, Requests.BoolResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Checks if this channel is on.
        /// </summary>
        /// <returns>True if channel is on, false otherwise.</returns>
        public bool IsOn()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = IsOnAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets channel intensity as a fraction of the maximum flux.
        /// </summary>
        /// <param name="intensity">Fraction of intensity to set (between 0 and 1).</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetIntensityAsync(double intensity)
        {
            var request = new Requests.ChannelSetIntensity()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                Intensity = intensity,
            };

            await Gateway.CallAsync("illuminator/set_intensity", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets channel intensity as a fraction of the maximum flux.
        /// </summary>
        /// <param name="intensity">Fraction of intensity to set (between 0 and 1).</param>
        public void SetIntensity(double intensity)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetIntensityAsync(intensity);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the current intensity of this channel.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Current intensity as fraction of maximum flux.</returns>
        public async Task<double> GetIntensityAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
            };

            var response = await Gateway.CallAsync("illuminator/get_intensity", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the current intensity of this channel.
        /// </summary>
        /// <returns>Current intensity as fraction of maximum flux.</returns>
        public double GetIntensity()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetIntensityAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: A response to the command.</returns>
        public async Task<Response> GenericCommandAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command", request, Response.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when the device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A response to the command.</returns>
        public Response GenericCommand(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel and expects multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>A Task that can be awaited to get the result: All responses to the command.</returns>
        public async Task<Response[]> GenericCommandMultiResponseAsync(string command, bool checkErrors = true, int timeout = 0)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                Command = command,
                CheckErrors = checkErrors,
                Timeout = timeout,
            };

            var response = await Gateway.CallAsync("interface/generic_command_multi_response", request, Requests.GenericCommandResponseCollection.FromByteArray).ConfigureAwait(false);
            return response.Responses;
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel and expects multiple responses.
        /// Responses are returned in order of arrival.
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <param name="checkErrors">Controls whether to throw an exception when a device rejects the command.</param>
        /// <param name="timeout">The timeout, in milliseconds, for a device to respond to the command.
        /// Overrides the connection default request timeout.</param>
        /// <returns>All responses to the command.</returns>
        public Response[] GenericCommandMultiResponse(string command, bool checkErrors = true, int timeout = 0)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandMultiResponseAsync(command, checkErrors, timeout);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task GenericCommandNoResponseAsync(string command)
        {
            var request = new Requests.GenericCommandRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                Command = command,
            };

            await Gateway.CallAsync("interface/generic_command_no_response", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
        /// For more information refer to: <see href="https://www.zaber.com/protocol-manual#topic_commands">ASCII Protocol Manual</see>.
        /// </summary>
        /// <param name="command">Command and its parameters.</param>
        public void GenericCommandNoResponse(string command)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GenericCommandNoResponseAsync(command);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a serialization of the current channel state that can be saved and reapplied.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: A serialization of the current state of the channel.</returns>
        public async Task<string> GetStateAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
            };

            var response = await Gateway.CallAsync("device/get_state", request, Requests.StringResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns a serialization of the current channel state that can be saved and reapplied.
        /// </summary>
        /// <returns>A serialization of the current state of the channel.</returns>
        public string GetState()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStateAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Applies a saved state to this channel.
        /// </summary>
        /// <param name="state">The state object to apply to this channel.</param>
        /// <returns>A Task that can be awaited to get the result: Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public async Task<SetStateAxisResponse> SetStateAsync(string state)
        {
            var request = new Requests.SetStateRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                State = state,
            };

            var response = await Gateway.CallAsync("device/set_axis_state", request, SetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response;
        }


        /// <summary>
        /// Applies a saved state to this channel.
        /// </summary>
        /// <param name="state">The state object to apply to this channel.</param>
        /// <returns>Reports of any issues that were handled, but caused the state to not be exactly restored.</returns>
        public SetStateAxisResponse SetState(string state)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetStateAsync(state);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Checks if a state can be applied to this channel.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>A Task that can be awaited to get the result: An explanation of why this state cannot be set to this channel.</returns>
        public async Task<string?> CanSetStateAsync(string state, FirmwareVersion? firmwareVersion = null)
        {
            var request = new Requests.CanSetStateRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                State = state,
                FirmwareVersion = firmwareVersion,
            };

            var response = await Gateway.CallAsync("device/can_set_axis_state", request, Requests.CanSetStateAxisResponse.FromByteArray).ConfigureAwait(false);
            return response.Error;
        }


        /// <summary>
        /// Checks if a state can be applied to this channel.
        /// This only covers exceptions that can be determined statically such as mismatches of ID or version,
        /// the process of applying the state can still fail when running.
        /// </summary>
        /// <param name="state">The state object to check against.</param>
        /// <param name="firmwareVersion">The firmware version of the device to apply the state to.
        /// Use this to ensure the state will still be compatible after an update.</param>
        /// <returns>An explanation of why this state cannot be set to this channel.</returns>
        public string? CanSetState(string state, FirmwareVersion? firmwareVersion = null)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = CanSetStateAsync(state, firmwareVersion);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the channel.
        /// </summary>
        /// <returns>A string that represents the channel.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Illuminator.Device.Connection.InterfaceId,
                Device = Illuminator.Device.DeviceAddress,
                Axis = ChannelNumber,
                TypeOverride = "Channel",
            };

            var response = Gateway.CallSync("device/axis_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


        private Axis _axis;


    }
}
