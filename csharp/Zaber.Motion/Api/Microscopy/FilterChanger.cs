﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// A generic turret device.
    /// </summary>
    public class FilterChanger
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `FilterChanger` based on the given device.
        /// </summary>
        public FilterChanger(Device device)
        {
            Device = device;
        }
#pragma warning restore SA1611


        /// <summary>
        /// The base device of this turret.
        /// </summary>
        public Device Device { get; private set; }


        /// <summary>
        /// Gets number of filters of the changer.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Number of positions.</returns>
        public async Task<int> GetNumberOfFiltersAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 1,
            };

            var response = await Gateway.CallAsync("device/get_index_count", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets number of filters of the changer.
        /// </summary>
        /// <returns>Number of positions.</returns>
        public int GetNumberOfFilters()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetNumberOfFiltersAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the current filter number starting from 1.
        /// The value of 0 indicates that the position is either unknown or between two filters.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: Filter number starting from 1 or 0 if the position cannot be determined.</returns>
        public async Task<int> GetCurrentFilterAsync()
        {
            var request = new Requests.AxisEmptyRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 1,
            };

            var response = await Gateway.CallAsync("device/get_index_position", request, Requests.IntResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Returns the current filter number starting from 1.
        /// The value of 0 indicates that the position is either unknown or between two filters.
        /// </summary>
        /// <returns>Filter number starting from 1 or 0 if the position cannot be determined.</returns>
        public int GetCurrentFilter()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetCurrentFilterAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Changes to the specified filter.
        /// </summary>
        /// <param name="filter">Filter number starting from 1.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task ChangeAsync(int filter)
        {
            var request = new Requests.DeviceMoveRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
                Axis = 1,
                Type = Requests.AxisMoveType.Index,
                WaitUntilIdle = true,
                ArgInt = filter,
            };

            await Gateway.CallAsync("device/move", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Changes to the specified filter.
        /// </summary>
        /// <param name="filter">Filter number starting from 1.</param>
        public void Change(int filter)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = ChangeAsync(filter);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the device.
        /// </summary>
        /// <returns>A string that represents the device.</returns>
        public override string ToString()
        {
            var request = new Requests.AxisToStringRequest()
            {
                InterfaceId = Device.Connection.InterfaceId,
                Device = Device.DeviceAddress,
            };

            var response = Gateway.CallSync("device/device_to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
