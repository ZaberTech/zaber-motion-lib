﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Linq;
using System.Threading.Tasks;

using Zaber.Motion.Runtime;

using Axis = Zaber.Motion.Ascii.Axis;
using Connection = Zaber.Motion.Ascii.Connection;
using Device = Zaber.Motion.Ascii.Device;

namespace Zaber.Motion.Microscopy
{
    /// <summary>
    /// A generic autofocus device.
    /// </summary>
    public class Autofocus
    {
#pragma warning disable SA1611
        /// <summary>
        /// Creates instance of `Autofocus` based on the given provider id.
        /// </summary>
        public Autofocus(int providerId, Axis focusAxis, Device? objectiveTurret)
        {
            ProviderId = providerId;
            FocusAxis = focusAxis;
            ObjectiveTurret = objectiveTurret;
        }
#pragma warning restore SA1611


        /// <summary>
        /// The identification of external device providing the capability.
        /// </summary>
        public int ProviderId { get; private set; }


        /// <summary>
        /// The focus axis.
        /// </summary>
        public Axis FocusAxis { get; private set; }


        /// <summary>
        /// The objective turret device if the microscope has one.
        /// </summary>
        public Device? ObjectiveTurret { get; private set; }


        /// <summary>
        /// Sets the current focus to be target for the autofocus control loop.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetFocusZeroAsync()
        {
            var request = new Requests.EmptyAutofocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            await Gateway.CallAsync("autofocus/set_zero", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the current focus to be target for the autofocus control loop.
        /// </summary>
        public void SetFocusZero()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetFocusZeroAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the status of the autofocus.
        /// </summary>
        /// <returns>A Task that can be awaited to get the result: The status of the autofocus.</returns>
        public async Task<AutofocusStatus> GetStatusAsync()
        {
            var request = new Requests.EmptyAutofocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            var response = await Gateway.CallAsync("autofocus/get_status", request, Requests.AutofocusGetStatusResponse.FromByteArray).ConfigureAwait(false);
            return response.Status;
        }


        /// <summary>
        /// Returns the status of the autofocus.
        /// </summary>
        /// <returns>The status of the autofocus.</returns>
        public AutofocusStatus GetStatus()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetStatusAsync();
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the device until it's in focus.
        /// </summary>
        /// <param name="scan">If true, the autofocus will approach from the limit moving until it's in range.</param>
        /// <param name="timeout">Sets autofocus timeout duration in milliseconds.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task FocusOnceAsync(bool scan = false, int timeout = -1)
        {
            var request = new Requests.AutofocusFocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
                Once = true,
                Scan = scan,
                Timeout = timeout,
            };

            await Gateway.CallAsync("autofocus/focus_once", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the device until it's in focus.
        /// </summary>
        /// <param name="scan">If true, the autofocus will approach from the limit moving until it's in range.</param>
        /// <param name="timeout">Sets autofocus timeout duration in milliseconds.</param>
        public void FocusOnce(bool scan = false, int timeout = -1)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = FocusOnceAsync(scan, timeout);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Moves the focus axis continuously maintaining focus.
        /// Starts the autofocus control loop.
        /// Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StartFocusLoopAsync()
        {
            var request = new Requests.AutofocusFocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            await Gateway.CallAsync("autofocus/start_focus_loop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Moves the focus axis continuously maintaining focus.
        /// Starts the autofocus control loop.
        /// Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
        /// </summary>
        public void StartFocusLoop()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StartFocusLoopAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Stops autofocus control loop.
        /// If the focus axis already stopped moving because of an error, an exception will be thrown.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task StopFocusLoopAsync()
        {
            var request = new Requests.EmptyAutofocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            await Gateway.CallAsync("autofocus/stop_focus_loop", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Stops autofocus control loop.
        /// If the focus axis already stopped moving because of an error, an exception will be thrown.
        /// </summary>
        public void StopFocusLoop()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = StopFocusLoopAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the lower motion limit for the autofocus control loop.
        /// Gets motion.tracking.limit.min setting of the focus axis.
        /// </summary>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>A Task that can be awaited to get the result: Limit value.</returns>
        public async Task<double> GetLimitMinAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                Device = FocusAxis.Device.DeviceAddress,
                Axis = FocusAxis.AxisNumber,
                Setting = "motion.tracking.limit.min",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the lower motion limit for the autofocus control loop.
        /// Gets motion.tracking.limit.min setting of the focus axis.
        /// </summary>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>Limit value.</returns>
        public double GetLimitMin(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetLimitMinAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Gets the upper motion limit for the autofocus control loop.
        /// Gets motion.tracking.limit.max setting of the focus axis.
        /// </summary>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>A Task that can be awaited to get the result: Limit value.</returns>
        public async Task<double> GetLimitMaxAsync(Units unit = Units.Native)
        {
            var request = new Requests.DeviceGetSettingRequest()
            {
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                Device = FocusAxis.Device.DeviceAddress,
                Axis = FocusAxis.AxisNumber,
                Setting = "motion.tracking.limit.max",
                Unit = unit,
            };

            var response = await Gateway.CallAsync("device/get_setting", request, Requests.DoubleResponse.FromByteArray).ConfigureAwait(false);
            return response.Value;
        }


        /// <summary>
        /// Gets the upper motion limit for the autofocus control loop.
        /// Gets motion.tracking.limit.max setting of the focus axis.
        /// </summary>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>Limit value.</returns>
        public double GetLimitMax(Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetLimitMaxAsync(unit);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the lower motion limit for the autofocus control loop.
        /// Use the limits to prevent the focus axis from crashing into the sample.
        /// Changes motion.tracking.limit.min setting of the focus axis.
        /// </summary>
        /// <param name="limit">The lower limit of the focus axis.</param>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLimitMinAsync(double limit, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                Device = FocusAxis.Device.DeviceAddress,
                Axis = FocusAxis.AxisNumber,
                Setting = "motion.tracking.limit.min",
                Value = limit,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the lower motion limit for the autofocus control loop.
        /// Use the limits to prevent the focus axis from crashing into the sample.
        /// Changes motion.tracking.limit.min setting of the focus axis.
        /// </summary>
        /// <param name="limit">The lower limit of the focus axis.</param>
        /// <param name="unit">The units of the limit.</param>
        public void SetLimitMin(double limit, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLimitMinAsync(limit, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the upper motion limit for the autofocus control loop.
        /// Use the limits to prevent the focus axis from crashing into the sample.
        /// Changes motion.tracking.limit.max setting of the focus axis.
        /// </summary>
        /// <param name="limit">The upper limit of the focus axis.</param>
        /// <param name="unit">The units of the limit.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetLimitMaxAsync(double limit, Units unit = Units.Native)
        {
            var request = new Requests.DeviceSetSettingRequest()
            {
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                Device = FocusAxis.Device.DeviceAddress,
                Axis = FocusAxis.AxisNumber,
                Setting = "motion.tracking.limit.max",
                Value = limit,
                Unit = unit,
            };

            await Gateway.CallAsync("device/set_setting", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the upper motion limit for the autofocus control loop.
        /// Use the limits to prevent the focus axis from crashing into the sample.
        /// Changes motion.tracking.limit.max setting of the focus axis.
        /// </summary>
        /// <param name="limit">The upper limit of the focus axis.</param>
        /// <param name="unit">The units of the limit.</param>
        public void SetLimitMax(double limit, Units unit = Units.Native)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetLimitMaxAsync(limit, unit);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Typically, the control loop parameters and objective are kept synchronized by the library.
        /// If the parameters or current objective changes outside of the library, call this method to synchronize them.
        /// </summary>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SynchronizeParametersAsync()
        {
            var request = new Requests.EmptyAutofocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            await Gateway.CallAsync("autofocus/sync_params", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Typically, the control loop parameters and objective are kept synchronized by the library.
        /// If the parameters or current objective changes outside of the library, call this method to synchronize them.
        /// </summary>
        public void SynchronizeParameters()
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SynchronizeParametersAsync();
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Sets the parameters for the autofocus objective.
        /// Note that the method temporarily switches current objective to set the parameters.
        /// </summary>
        /// <param name="objective">The objective (numbered from 1) to set the parameters for.
        /// If your microscope has only one objective, use value of 1.</param>
        /// <param name="parameters">The parameters for the autofocus objective.</param>
        /// <returns>A Task that can be awaited to know when the work is complete.</returns>
        public async Task SetObjectiveParametersAsync(int objective, NamedParameter[] parameters)
        {
            var request = new Requests.AutofocusSetObjectiveParamsRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
                Objective = objective,
                Parameters = parameters,
            };

            await Gateway.CallAsync("autofocus/set_objective_params", request).ConfigureAwait(false);
        }


        /// <summary>
        /// Sets the parameters for the autofocus objective.
        /// Note that the method temporarily switches current objective to set the parameters.
        /// </summary>
        /// <param name="objective">The objective (numbered from 1) to set the parameters for.
        /// If your microscope has only one objective, use value of 1.</param>
        /// <param name="parameters">The parameters for the autofocus objective.</param>
        public void SetObjectiveParameters(int objective, NamedParameter[] parameters)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = SetObjectiveParametersAsync(objective, parameters);
            task.WaitAndUnwindException();
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns the parameters for the autofocus objective.
        /// </summary>
        /// <param name="objective">The objective (numbered from 1) to get the parameters for.
        /// If your microscope has only one objective, use value of 1.
        /// Note that the method temporarily switches current objective to get the parameters.</param>
        /// <returns>A Task that can be awaited to get the result: The parameters for the autofocus objective.</returns>
        public async Task<NamedParameter[]> GetObjectiveParametersAsync(int objective)
        {
            var request = new Requests.AutofocusGetObjectiveParamsRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
                Objective = objective,
            };

            var response = await Gateway.CallAsync("autofocus/get_objective_params", request, Requests.AutofocusGetObjectiveParamsResponse.FromByteArray).ConfigureAwait(false);
            return response.Parameters;
        }


        /// <summary>
        /// Returns the parameters for the autofocus objective.
        /// </summary>
        /// <param name="objective">The objective (numbered from 1) to get the parameters for.
        /// If your microscope has only one objective, use value of 1.
        /// Note that the method temporarily switches current objective to get the parameters.</param>
        /// <returns>The parameters for the autofocus objective.</returns>
        public NamedParameter[] GetObjectiveParameters(int objective)
        {
#pragma warning disable CA2000 // Dispose objects before losing scope
            var task = GetObjectiveParametersAsync(objective);
            task.WaitAndUnwindException();
            return task.Result;
#pragma warning restore CA2000 // Dispose objects before losing scope
        }


        /// <summary>
        /// Returns a string that represents the autofocus.
        /// </summary>
        /// <returns>A string that represents the autofocus.</returns>
        public override string ToString()
        {
            var request = new Requests.EmptyAutofocusRequest()
            {
                ProviderId = ProviderId,
                InterfaceId = FocusAxis.Device.Connection.InterfaceId,
                FocusAddress = FocusAxis.Device.DeviceAddress,
                FocusAxis = FocusAxis.AxisNumber,
                TurretAddress = ObjectiveTurret?.DeviceAddress ?? 0,
            };

            var response = Gateway.CallSync("autofocus/to_string", request, Requests.StringResponse.FromByteArray);
            return response.Value;
        }


    }
}
