/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for StreamMovementInterruptedException.
    /// </summary>
    public class StreamMovementInterruptedExceptionData : IMessage
    {
        /// <summary>
        /// The full list of warnings.
        /// </summary>
        [JsonProperty("warnings")]
        public string[] Warnings { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason for the Exception.
        /// </summary>
        [JsonProperty("reason")]
        public string Reason { get; set; } = string.Empty;

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is StreamMovementInterruptedExceptionData))
            {
                return false;
            }

            StreamMovementInterruptedExceptionData other = (StreamMovementInterruptedExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(Warnings, other.Warnings) &&
                EqualityUtils.CheckEquals(Reason, other.Reason)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Warnings));
            hash.Add(EqualityUtils.GenerateHashCode(Reason));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => StreamMovementInterruptedExceptionData.ToByteArray(this);

        internal static StreamMovementInterruptedExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<StreamMovementInterruptedExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(StreamMovementInterruptedExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
