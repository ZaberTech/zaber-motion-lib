/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for GCodeExecutionException.
    /// </summary>
    public class GCodeExecutionExceptionData : IMessage
    {
        /// <summary>
        /// The index in the block string that caused the exception.
        /// </summary>
        [JsonProperty("fromBlock")]
        public int FromBlock { get; set; }

        /// <summary>
        /// The end index in the block string that caused the exception.
        /// The end index is exclusive.
        /// </summary>
        [JsonProperty("toBlock")]
        public int ToBlock { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is GCodeExecutionExceptionData))
            {
                return false;
            }

            GCodeExecutionExceptionData other = (GCodeExecutionExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(FromBlock, other.FromBlock) &&
                EqualityUtils.CheckEquals(ToBlock, other.ToBlock)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(FromBlock));
            hash.Add(EqualityUtils.GenerateHashCode(ToBlock));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => GCodeExecutionExceptionData.ToByteArray(this);

        internal static GCodeExecutionExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<GCodeExecutionExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(GCodeExecutionExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
