/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for a SetDeviceStateFailedException.
    /// </summary>
    public class SetDeviceStateExceptionData : IMessage
    {
        /// <summary>
        /// A list of settings which could not be set.
        /// </summary>
        [JsonProperty("settings")]
        public string[] Settings { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason the stream buffers could not be set.
        /// </summary>
        [JsonProperty("streamBuffers")]
        public string[] StreamBuffers { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason the pvt buffers could not be set.
        /// </summary>
        [JsonProperty("pvtBuffers")]
        public string[] PvtBuffers { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason the triggers could not be set.
        /// </summary>
        [JsonProperty("triggers")]
        public string[] Triggers { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason servo tuning could not be set.
        /// </summary>
        [JsonProperty("servoTuning")]
        public string ServoTuning { get; set; } = string.Empty;

        /// <summary>
        /// The reasons stored positions could not be set.
        /// </summary>
        [JsonProperty("storedPositions")]
        public string[] StoredPositions { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reasons storage could not be set.
        /// </summary>
        [JsonProperty("storage")]
        public string[] Storage { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// Errors for any peripherals that could not be set.
        /// </summary>
        [JsonProperty("peripherals")]
        public SetPeripheralStateExceptionData[] Peripherals { get; set; } = System.Array.Empty<SetPeripheralStateExceptionData>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SetDeviceStateExceptionData))
            {
                return false;
            }

            SetDeviceStateExceptionData other = (SetDeviceStateExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(Settings, other.Settings) &&
                EqualityUtils.CheckEquals(StreamBuffers, other.StreamBuffers) &&
                EqualityUtils.CheckEquals(PvtBuffers, other.PvtBuffers) &&
                EqualityUtils.CheckEquals(Triggers, other.Triggers) &&
                EqualityUtils.CheckEquals(ServoTuning, other.ServoTuning) &&
                EqualityUtils.CheckEquals(StoredPositions, other.StoredPositions) &&
                EqualityUtils.CheckEquals(Storage, other.Storage) &&
                EqualityUtils.CheckEquals(Peripherals, other.Peripherals)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Settings));
            hash.Add(EqualityUtils.GenerateHashCode(StreamBuffers));
            hash.Add(EqualityUtils.GenerateHashCode(PvtBuffers));
            hash.Add(EqualityUtils.GenerateHashCode(Triggers));
            hash.Add(EqualityUtils.GenerateHashCode(ServoTuning));
            hash.Add(EqualityUtils.GenerateHashCode(StoredPositions));
            hash.Add(EqualityUtils.GenerateHashCode(Storage));
            hash.Add(EqualityUtils.GenerateHashCode(Peripherals));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SetDeviceStateExceptionData.ToByteArray(this);

        internal static SetDeviceStateExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SetDeviceStateExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SetDeviceStateExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
