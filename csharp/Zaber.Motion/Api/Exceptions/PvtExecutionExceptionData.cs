/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for PvtExecutionException.
    /// </summary>
    public class PvtExecutionExceptionData : IMessage
    {
        /// <summary>
        /// The error flag that caused the exception.
        /// </summary>
        [JsonProperty("errorFlag")]
        public string ErrorFlag { get; set; } = string.Empty;

        /// <summary>
        /// The reason for the exception.
        /// </summary>
        [JsonProperty("reason")]
        public string Reason { get; set; } = string.Empty;

        /// <summary>
        /// A list of points that cause the error (if applicable).
        /// </summary>
        [JsonProperty("invalidPoints")]
        public InvalidPvtPoint[] InvalidPoints { get; set; } = System.Array.Empty<InvalidPvtPoint>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is PvtExecutionExceptionData))
            {
                return false;
            }

            PvtExecutionExceptionData other = (PvtExecutionExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(ErrorFlag, other.ErrorFlag) &&
                EqualityUtils.CheckEquals(Reason, other.Reason) &&
                EqualityUtils.CheckEquals(InvalidPoints, other.InvalidPoints)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(ErrorFlag));
            hash.Add(EqualityUtils.GenerateHashCode(Reason));
            hash.Add(EqualityUtils.GenerateHashCode(InvalidPoints));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => PvtExecutionExceptionData.ToByteArray(this);

        internal static PvtExecutionExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<PvtExecutionExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(PvtExecutionExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
