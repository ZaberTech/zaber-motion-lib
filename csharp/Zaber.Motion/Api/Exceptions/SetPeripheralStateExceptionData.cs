/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for a SetPeripheralStateFailedException.
    /// </summary>
    public class SetPeripheralStateExceptionData : IMessage
    {
        /// <summary>
        /// The number of axis where the exception originated.
        /// </summary>
        [JsonProperty("axisNumber")]
        public int AxisNumber { get; set; }

        /// <summary>
        /// A list of settings which could not be set.
        /// </summary>
        [JsonProperty("settings")]
        public string[] Settings { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reason servo tuning could not be set.
        /// </summary>
        [JsonProperty("servoTuning")]
        public string ServoTuning { get; set; } = string.Empty;

        /// <summary>
        /// The reasons stored positions could not be set.
        /// </summary>
        [JsonProperty("storedPositions")]
        public string[] StoredPositions { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// The reasons storage could not be set.
        /// </summary>
        [JsonProperty("storage")]
        public string[] Storage { get; set; } = System.Array.Empty<string>();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is SetPeripheralStateExceptionData))
            {
                return false;
            }

            SetPeripheralStateExceptionData other = (SetPeripheralStateExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(AxisNumber, other.AxisNumber) &&
                EqualityUtils.CheckEquals(Settings, other.Settings) &&
                EqualityUtils.CheckEquals(ServoTuning, other.ServoTuning) &&
                EqualityUtils.CheckEquals(StoredPositions, other.StoredPositions) &&
                EqualityUtils.CheckEquals(Storage, other.Storage)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(AxisNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Settings));
            hash.Add(EqualityUtils.GenerateHashCode(ServoTuning));
            hash.Add(EqualityUtils.GenerateHashCode(StoredPositions));
            hash.Add(EqualityUtils.GenerateHashCode(Storage));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => SetPeripheralStateExceptionData.ToByteArray(this);

        internal static SetPeripheralStateExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<SetPeripheralStateExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(SetPeripheralStateExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
