/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Contains additional data for CommandFailedException.
    /// </summary>
    public class CommandFailedExceptionData : IMessage
    {
        /// <summary>
        /// The command that got rejected.
        /// </summary>
        [JsonProperty("command")]
        public string Command { get; set; } = string.Empty;

        /// <summary>
        /// The data from the reply containing the rejection reason.
        /// </summary>
        [JsonProperty("responseData")]
        public string ResponseData { get; set; } = string.Empty;

        /// <summary>
        /// The flag indicating that the command was rejected.
        /// </summary>
        [JsonProperty("replyFlag")]
        public string ReplyFlag { get; set; } = string.Empty;

        /// <summary>
        /// The current device or axis status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        /// <summary>
        /// The highest priority warning flag on the device or axis.
        /// </summary>
        [JsonProperty("warningFlag")]
        public string WarningFlag { get; set; } = string.Empty;

        /// <summary>
        /// The address of the device that rejected the command.
        /// </summary>
        [JsonProperty("deviceAddress")]
        public int DeviceAddress { get; set; }

        /// <summary>
        /// The number of the axis which the rejection relates to.
        /// </summary>
        [JsonProperty("axisNumber")]
        public int AxisNumber { get; set; }

        /// <summary>
        /// The message ID of the reply.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is CommandFailedExceptionData))
            {
                return false;
            }

            CommandFailedExceptionData other = (CommandFailedExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(Command, other.Command) &&
                EqualityUtils.CheckEquals(ResponseData, other.ResponseData) &&
                EqualityUtils.CheckEquals(ReplyFlag, other.ReplyFlag) &&
                EqualityUtils.CheckEquals(Status, other.Status) &&
                EqualityUtils.CheckEquals(WarningFlag, other.WarningFlag) &&
                EqualityUtils.CheckEquals(DeviceAddress, other.DeviceAddress) &&
                EqualityUtils.CheckEquals(AxisNumber, other.AxisNumber) &&
                EqualityUtils.CheckEquals(Id, other.Id)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Command));
            hash.Add(EqualityUtils.GenerateHashCode(ResponseData));
            hash.Add(EqualityUtils.GenerateHashCode(ReplyFlag));
            hash.Add(EqualityUtils.GenerateHashCode(Status));
            hash.Add(EqualityUtils.GenerateHashCode(WarningFlag));
            hash.Add(EqualityUtils.GenerateHashCode(DeviceAddress));
            hash.Add(EqualityUtils.GenerateHashCode(AxisNumber));
            hash.Add(EqualityUtils.GenerateHashCode(Id));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => CommandFailedExceptionData.ToByteArray(this);

        internal static CommandFailedExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<CommandFailedExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(CommandFailedExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
