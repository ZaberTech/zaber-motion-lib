/* This file is generated. Do not modify by hand. */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Zaber.Motion.Runtime;
using Zaber.Motion.Utils;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Information describing why the command could not fit.
    /// </summary>
    public class CommandTooLongExceptionData : IMessage
    {
        /// <summary>
        /// The part of the command that could be successfully fit in the space provided by the protocol.
        /// </summary>
        [JsonProperty("fit")]
        public string Fit { get; set; } = string.Empty;

        /// <summary>
        /// The part of the command that could not fit within the space provided.
        /// </summary>
        [JsonProperty("remainder")]
        public string Remainder { get; set; } = string.Empty;

        /// <summary>
        /// The length of the ascii string that can be written to a single line.
        /// </summary>
        [JsonProperty("packetSize")]
        public int PacketSize { get; set; }

        /// <summary>
        /// The number of lines a command can be split over using continuations.
        /// </summary>
        [JsonProperty("packetsMax")]
        public int PacketsMax { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        #pragma warning disable CA1309, CS0472, CA1502
        public override bool Equals(object? obj)
        {
            if (obj == null || !(obj is CommandTooLongExceptionData))
            {
                return false;
            }

            CommandTooLongExceptionData other = (CommandTooLongExceptionData)obj;
            return (
                EqualityUtils.CheckEquals(Fit, other.Fit) &&
                EqualityUtils.CheckEquals(Remainder, other.Remainder) &&
                EqualityUtils.CheckEquals(PacketSize, other.PacketSize) &&
                EqualityUtils.CheckEquals(PacketsMax, other.PacketsMax)
            );
        }
        #pragma warning restore CA1309, CS0472, CA1502

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            HashCode hash = default(HashCode);
            hash.Add(EqualityUtils.GenerateHashCode(Fit));
            hash.Add(EqualityUtils.GenerateHashCode(Remainder));
            hash.Add(EqualityUtils.GenerateHashCode(PacketSize));
            hash.Add(EqualityUtils.GenerateHashCode(PacketsMax));
            return hash.ToHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current instance.
        /// </summary>
        /// <returns>A string that represents the current instance.</returns>
        public override string ToString() => ObjectDumper.Dump(this);

        byte[] IMessage.ToByteArray() => CommandTooLongExceptionData.ToByteArray(this);

        internal static CommandTooLongExceptionData FromByteArray(byte[] buffer)
        {
            using (var stream = new System.IO.MemoryStream(buffer))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    return Serialization.Serializer.Deserialize<CommandTooLongExceptionData>(reader);
                }
            }
        }

        internal static byte[] ToByteArray(CommandTooLongExceptionData instance)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    Serialization.Serializer.Serialize(writer, instance);
                }

                return stream.ToArray();
            }
        }
    }
}
