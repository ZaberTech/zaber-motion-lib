using System;
using System.Linq;
using System.Text;

namespace Zaber.Motion.Utils
{
    /// <summary>
    /// Helper class which compares objects for equality.
    /// </summary>
    public static class EqualityUtils
    {
        /// <summary>
        /// Checks the value equality of two objects.
        /// </summary>
        /// <returns>Returns true if object values are equal, false otherwise.</returns>
        /// <param name="objA">The first object to compare.</param>
        /// <param name="objB">The second object to compare.</param>
        public static bool CheckEquals(object? objA, object? objB)
        {
            if (objA == null || objB == null)
            {
                return objA == objB;
            }

            if (objA is Array arrayA && objB is Array arrayB)
            {
                if (arrayA.Length != arrayB.Length)
                {
                    return false;
                }

                for (int i = 0; i < arrayA.Length; i++)
                {
                    if (!CheckEquals(arrayA.GetValue(i), arrayB.GetValue(i)))
                    {
                        return false;
                    }
                }

                return true;
            }

            return objA.Equals(objB);
        }

        /// <summary>
        /// Generates a hashcode for an object.
        /// If object is array, generates a hashcode by combining each element.
        ///
        /// </summary>
        /// <returns>Returns hashcode for object.</returns>
        /// <param name="obj">The object to generate a hashcode for.</param>
        public static int GenerateHashCode(object? obj)
        {
            if (obj == null)
            {
                return 0;
            }

            if (obj is Array array)
            {
                HashCode hash = default(HashCode);
                for (int i = 0; i < array.Length; i++)
                {
                    hash.Add(array.GetValue(i));
                }

                return hash.ToHashCode();
            }

            return obj.GetHashCode();
        }
    }
}
