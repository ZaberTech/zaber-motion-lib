﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace Zaber.Motion
{
    internal static class TaskExtension
    {
        internal static void WaitAndUnwindException(this Task task)
        {
            try
            {
                task.Wait();
            }
            catch (AggregateException e)
            {
                ExceptionDispatchInfo.Capture(e.GetBaseException()).Throw();
            }
        }
    }
}
