using System.Text;

namespace Zaber.Motion
{
    /// <summary>
    /// Helper class which dumps an object to a string.
    /// </summary>
    public static class ObjectDumper
    {
        private const string NULL = "(null)";

        /// <summary>
        /// Dumps the object to string.
        /// </summary>
        /// <returns>String representation of the object.</returns>
        /// <param name="toDump">Object to dump.</param>
        public static string Dump(object toDump)
        {
            if (toDump is null)
            {
                return NULL;
            }

            var type = toDump.GetType();
            var properties = type.GetProperties();

            var sb = new StringBuilder();
            sb.Append(type.Name);
            sb.Append("(");

            foreach (var info in properties)
            {
                var value = info.GetValue(toDump, null) ?? NULL;
                if (value.GetType().IsArray)
                {
                    var array = (object[])value;
                    sb.Append($"{info.Name}=[");
                    foreach (var item in array)
                    {
                        sb.Append($"{(item ?? NULL)},");
                    }

                    if (array.Length > 0)
                    {
                        sb[sb.Length - 1] = ']';
                    }
                    else
                    {
                        sb.Append("]");
                    }

                    sb.Append(",");
                }
                else
                {
                    sb.Append($"{info.Name}={value},");
                }
            }

            sb[sb.Length - 1] = ')';

            return sb.ToString();
        }
    }
}
