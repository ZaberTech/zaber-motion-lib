﻿using System;
using System.Runtime.InteropServices;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Type definition for callbacks passed to the runtime.
    /// </summary>
    /// <param name="aData">Arguments to the aCallback; content depends on calling context.</param>
    /// <param name="aTag">Contextual reference for the aCallback.</param>
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    internal delegate void Callback(
        [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
        long aTag);


    /// <summary>
    /// Interface implemented by each of the platform-specific unmanaged DLLs we use.
    /// </summary>
    internal interface IZaberGoRuntime
    {
        /// <inheritdoc cref="ZaberGoRuntime.CallLib(ProtoData, long, Callback, byte)"/>
        int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync);

        /// <inheritdoc cref="ZaberGoRuntime.SetEventHandler(long, Callback)"/>
        void SetEventHandlerDirector(long aTag, Callback aCallback);
    }
}
