﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Wrapper for the Zaber native communication library written in Go.
    /// All P/Invoke calls to the Go library should be routed through here.
    /// </summary>
    internal static partial class ZaberGoRuntime
    {
#pragma warning disable CA1065 // Static constructors should not throw exceptions. Disabled here because the program can't work if this fails.

        /// <summary>
        /// Static constructor - determines which version of the native library
        /// is appropriate for the platform, and instantiates a director that
        /// will route calls to that version.
        /// </summary>
        /// <remarks>ECMA-335 section II.10.5.3.1 (Type initialization guarantees)
        /// promises that there should not be a race condition here caused by two
        /// threads triggering the static constructor at the same time; one of them
        /// will automatically be blocked until the static constructor exits.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1810:Initialize reference type static fields inline", Justification = "Preference")]
        static ZaberGoRuntime()
        {
            string os;
            string architecture = RuntimeInformation.ProcessArchitecture.ToString();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                os = "Linux";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                os = "OSX";
                architecture = "uni";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                os = "Windows";
            }
            else
            {
                // TODO (Soleil): Improve error messages.
                // TODO (Soleil): Localize error messages.
                throw new InvalidProgramException("Current operating system is not supported.");
            }

            // Look for a concrete runtime loader that matches the OS and architecture.
            // This only searches the current assembly because we don't expect to want to
            // provide a plugin mechanism for platform support - people should recompile
            // from source if they need that.
            foreach (var type in Assembly.GetExecutingAssembly().DefinedTypes
                    .Where(t => typeof(IZaberGoRuntime).IsAssignableFrom(t) && !t.IsAbstract))
            {
                foreach (var attr in type.GetCustomAttributes<RuntimePlatformAttribute>())
                {
                    if (attr.Match(os, architecture))
                    {
                        _runtime = (IZaberGoRuntime)Activator.CreateInstance(type)!;
                        break;
                    }
                }
            }

            if (null == _runtime)
            {
                throw new InvalidProgramException("Could not identify the correct native runtime to use.");
            }
        }

#pragma warning restore CA1065

        private static IZaberGoRuntime _runtime;
    }
}
