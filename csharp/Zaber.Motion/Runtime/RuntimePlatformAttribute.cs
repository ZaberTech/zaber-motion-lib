﻿using System;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Helper attribute to select which runtime loader class to use.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    internal sealed class RuntimePlatformAttribute : Attribute
    {
        /// <summary>
        /// Required constructor - all properties must be provided.
        /// </summary>
        /// <param name="aOsName">Name of the operating system supported by the class applied to.</param>
        /// <param name="aArchitecture">Name of the CPU architecture supported by the class applied to.</param>
        public RuntimePlatformAttribute(string aOsName, string aArchitecture)
        {
            OsName = aOsName;
            Architecture = aArchitecture;
        }


        /// <summary>
        /// Name of the operating system supported.
        /// </summary>
        public string OsName { get; private set; }


        /// <summary>
        /// Name of the CPU architecture supported.
        /// </summary>
        public string Architecture { get; private set; }


        /// <summary>
        /// Determines if this attribute indicates a match for a discovered platform.
        /// </summary>
        /// <param name="aOsName">Name of the current operating system.</param>
        /// <param name="aArchitecture">Name of the current CPU architecture.</param>
        /// <returns>True of the class this attribute is attached to supports the current platform.</returns>
        public bool Match(string aOsName, string aArchitecture)
            => ((aOsName == OsName) && (aArchitecture == Architecture));
    }
}
