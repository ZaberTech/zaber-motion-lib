using System;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Exception thrown when an error occurs in the Gateway.
    /// </summary>
    public class GatewayException : Exception
    {
        /// <summary>
        /// Creates a new GatewayException with the given message.
        /// </summary>
        /// <param name="message">The message to include in the exception.</param>
        public GatewayException(string message)
        : base(message)
        {
        }
    }
}
