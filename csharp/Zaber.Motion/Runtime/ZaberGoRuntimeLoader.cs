﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;
using System.Runtime.InteropServices;

#pragma warning disable SA1402 // File may only contain a single type
#pragma warning disable CA1812 // Class apparently never instantiated.

// These classes exist because the argument to the DllImport attribute must be a constant expression.
namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Helper class for loading the correct runtime and redirecting calls to it.
    /// This part of the class is defined here to keep all the native API definitions together.
    /// </summary>
    internal static partial class ZaberGoRuntime
    {
        /// <summary>
        /// Instance method that redirects calls to the appropriate static method for each runtime library.
        /// </summary>
        /// <param name="aData">Arguments to the aCallback; content depends on calling context.</param>
        /// <param name="aTag">Contextual reference for the aCallback.</param>
        /// <param name="aCallback">Callback to use to return results asynchronously.</param>
        /// <param name="aAsync">Determines whether function should be called in go thread.</param>
        /// <returns>Return code is not yet defined.</returns>
        public static int CallLib(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => _runtime.CallLibDirector(aData, aTag, aCallback, aAsync);


        /// <summary>
        /// Instance method that redirects calls to the appropriate static method for each runtime library.
        /// </summary>
        /// <param name="aTag">Contextual reference for the aCallback.</param>
        /// <param name="aCallback">Callback to use to emit events asynchronously.</param>
        public static void SetEventHandler(long aTag, Callback aCallback)
            => _runtime.SetEventHandlerDirector(aTag, aCallback);
    }


    #region -- IZaberSdk implementation for Linux32 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Linux/X86 platform.
    /// </summary>
    [RuntimePlatform("Linux", "X86")]
    internal class ZaberGoRuntimeLinux32 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-linux-386.so", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-linux-386.so", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for Linux64 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Linux/X64 platform.
    /// </summary>
    [RuntimePlatform("Linux", "X64")]
    internal class ZaberGoRuntimeLinux64 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-linux-amd64.so", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-linux-amd64.so", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for LinuxArm --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Linux/Arm platform.
    /// </summary>
    [RuntimePlatform("Linux", "Arm")]
    internal class ZaberGoRuntimeLinuxArm : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-linux-arm.so", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-linux-arm.so", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for LinuxArm64 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Linux/Arm64 platform.
    /// </summary>
    [RuntimePlatform("Linux", "Arm64")]
    internal class ZaberGoRuntimeLinuxArm64 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-linux-arm64.so", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-linux-arm64.so", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for Win32 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Windows/X86 platform.
    /// </summary>
    [RuntimePlatform("Windows", "X86")]
    internal class ZaberGoRuntimeWin32 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-windows-386.dll", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-windows-386.dll", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for Win64 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Windows/X64 platform.
    /// </summary>
    [RuntimePlatform("Windows", "X64")]
    internal class ZaberGoRuntimeWin64 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-windows-amd64.dll", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-windows-amd64.dll", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for WinArm64 --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the Windows/Arm64 platform.
    /// </summary>
    [RuntimePlatform("Windows", "Arm64")]
    internal class ZaberGoRuntimeWinArm64 : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-windows-arm64.dll", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-windows-arm64.dll", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
    #region -- IZaberSdk implementation for OSXUni --

    /// <summary>
    /// Implementation of the runtime interface that loads the native DLL for the OSX/uni platform.
    /// </summary>
    [RuntimePlatform("OSX", "uni")]
    internal class ZaberGoRuntimeOSXUni : IZaberGoRuntime
    {
        /// <inheritdoc cref="IZaberGoRuntime.CallLibDirector"/>
        public int CallLibDirector(ProtoData aData, long aTag, Callback aCallback, byte aAsync)
            => CallLib(aData, aTag, aCallback, aAsync);


        /// <inheritdoc cref="IZaberGoRuntime.SetEventHandlerDirector"/>
        public void SetEventHandlerDirector(long aTag, Callback aCallback)
            => SetEventHandler(aTag, aCallback);


        [DllImport("zaber-motion-core-darwin-uni.dylib", EntryPoint = "zml_call", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CallLib(
            [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(ProtoMarshaler))] ProtoData aData,
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback,
            byte aAsync);


        [DllImport("zaber-motion-core-darwin-uni.dylib", EntryPoint = "zml_setEventHandler", CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetEventHandler(
            long aTag,
            [MarshalAs(UnmanagedType.FunctionPtr)] Callback aCallback);
    }

    #endregion
}

#pragma warning restore SA1402
#pragma warning restore CA1812
