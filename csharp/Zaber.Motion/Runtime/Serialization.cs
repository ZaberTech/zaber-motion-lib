﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Zaber.Motion.Runtime
{
    internal static class Serialization
    {
        public static JsonSerializer Serializer { get; } = JsonSerializer.CreateDefault();
    }
}
