﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zaber.Motion.Runtime
{
    internal interface IMessage
    {
        byte[] ToByteArray();
    }
}
