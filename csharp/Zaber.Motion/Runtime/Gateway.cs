﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Zaber.Motion.Requests;

namespace Zaber.Motion.Runtime
{
    internal delegate T MessageParser<T>(byte[] data) where T : IMessage;

    /// <summary>
    /// Main gateway to the native runtime library. Most SDK functions should route through here.
    /// </summary>
    internal static class Gateway
    {
        /// <summary>
        /// The delegate has to be allocated statically.
        /// There appears to be a problem with allocating and calling the delegates
        /// from multiple threads. It causes random failures on the go side.
        /// The pointer is most likely not valid in some cases.
        /// The chances of failing increase substantially with number of threads.
        /// </summary>
        private static readonly Runtime.Callback _callback;
        private static readonly Runtime.Callback _callbackSync;

        private static int _lastCallbackId;

        private static ConcurrentDictionary<int, TaskCompletionSource<ProtoData>> _callbacks
            = new ConcurrentDictionary<int, TaskCompletionSource<ProtoData>>();

        private static ConcurrentDictionary<int, ProtoData> _callbacksSync
            = new ConcurrentDictionary<int, ProtoData>();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1810:Initialize reference type static fields inline", Justification = "Preference")]
        static Gateway()
        {
            _callback = new Runtime.Callback(Callback);
            _callbackSync = new Runtime.Callback(CallbackSync);
        }

        /// <summary>
        /// Invoke the native runtime call dispatcher.
        /// </summary>
        /// <param name="request">Name of the function to invoke.</param>
        /// <param name="requestData">Optional function argument.</param>
        /// <returns>Task representing the asynchronous operation.</returns>
        public static async Task CallAsync(string request, IMessage? requestData = null)
        {
            var responseBuffers = await RequestAsync(request, requestData).ConfigureAwait(false);
            if (responseBuffers.Data.Count > 1)
            {
                throw new InvalidOperationException("Parser not provided but response returned");
            }
        }

        /// <summary>
        /// Invoke the native runtime call dispatcher.
        /// </summary>
        /// <typeparam name="T">A specific message type to expect in response.</typeparam>
        /// <param name="request">Name of the function to invoke.</param>
        /// <param name="requestData">Optional function argument.</param>
        /// <param name="parser">Parser for the response message.</param>
        /// <returns>Response message.</returns>
        public static async Task<T> CallAsync<T>(string request, IMessage? requestData, MessageParser<T> parser) where T : IMessage
        {
            var responseBuffers = await RequestAsync(request, requestData).ConfigureAwait(false);
            return ProcessResponseData<T>(responseBuffers, parser);
        }

        /// <summary>
        /// Invoke the native runtime call dispatcher.
        /// </summary>
        /// <param name="request">Name of the function to invoke.</param>
        /// <param name="requestData">Optional function argument.</param>
        public static void CallSync(string request, IMessage? requestData = null)
        {
            var responseBuffers = RequestSync(request, requestData);
            if (responseBuffers.Data.Count > 1)
            {
                throw new InvalidOperationException("Parser not provided but response returned");
            }
        }

        /// <summary>
        /// Invoke the native runtime call dispatcher.
        /// </summary>
        /// <typeparam name="T">A specific message type to expect in response.</typeparam>
        /// <param name="request">Name of the function to invoke.</param>
        /// <param name="requestData">Optional function argument.</param>
        /// <param name="parser">Parser for the response message.</param>
        /// <returns>Response message.</returns>
        public static T CallSync<T>(string request, IMessage? requestData, MessageParser<T> parser) where T : IMessage
        {
            var responseBuffers = RequestSync(request, requestData);
            return ProcessResponseData<T>(responseBuffers, parser);
        }

        private static async Task<ProtoData> RequestAsync(string request, IMessage? requestData)
        {
            var requestProto = new Requests.GatewayRequest() { Request = request };

            var buffer = new ProtoData();
            buffer.Data.Add(GatewayRequest.ToByteArray(requestProto));

            if (requestData != null)
            {
                buffer.Data.Add(requestData.ToByteArray());
            }

            TaskCompletionSource<ProtoData> taskDefer;

            int callbackId = AllocateCallback(out taskDefer);

            var result = Runtime.ZaberGoRuntime.CallLib(buffer, callbackId, _callback, 1);

            if (result != 0)
            {
                throw new GatewayException($"Invalid return code: ${result}");
            }

            var responseBuffers = await taskDefer.Task.ConfigureAwait(false);

            CheckResponseError(responseBuffers);
            return responseBuffers;
        }

        private static ProtoData RequestSync(string request, IMessage? requestData)
        {
            var requestProto = new Requests.GatewayRequest() { Request = request };

            var buffer = new ProtoData();
            buffer.Data.Add(GatewayRequest.ToByteArray(requestProto));

            if (requestData != null)
            {
                buffer.Data.Add(requestData.ToByteArray());
            }

            var callbackId = Interlocked.Increment(ref _lastCallbackId);
            var result = Runtime.ZaberGoRuntime.CallLib(buffer, callbackId, _callbackSync, 0);

            if (result != 0)
            {
                throw new GatewayException($"Invalid return code: ${result}");
            }

            if (!_callbacksSync.TryRemove(callbackId, out var responseBuffers))
            {
                throw new GatewayException("Invalid callback");
            }

            CheckResponseError(responseBuffers);
            return responseBuffers;
        }

        private static void CheckResponseError(ProtoData responseBuffers)
        {
            var response = Requests.GatewayResponse.FromByteArray(responseBuffers.Data[0]);
            if (response.Response != Requests.ResponseType.Ok)
            {
                if (responseBuffers.Data.Count > 1)
                {
                    throw Exceptions.ExceptionConverter.Convert(response.ErrorType, response.ErrorMessage, responseBuffers.Data[1]);
                }
                else
                {
                    throw Exceptions.ExceptionConverter.Convert(response.ErrorType, response.ErrorMessage);
                }
            }
        }

        private static T ProcessResponseData<T>(ProtoData responseBuffers, MessageParser<T> parser) where T : IMessage
        {
            if (responseBuffers.Data.Count < 2)
            {
                throw new InvalidOperationException("Parser provided but no response returned");
            }

            return parser(responseBuffers.Data[1]);
        }

        private static int AllocateCallback(out TaskCompletionSource<ProtoData> taskDefer)
        {
            int callbackId;
            taskDefer = new TaskCompletionSource<ProtoData>(TaskCreationOptions.RunContinuationsAsynchronously);

            do
            {
                callbackId = Interlocked.Increment(ref _lastCallbackId);
            }
            while (!_callbacks.TryAdd(callbackId, taskDefer));

            return callbackId;
        }

        private static void Callback(ProtoData data, long tag)
        {
            if (!_callbacks.TryRemove((int)tag, out var taskCompletion))
            {
                throw new GatewayException("Invalid callback");
            }

            taskCompletion.SetResult(data);
        }

        private static void CallbackSync(ProtoData data, long tag)
        {
            if (!_callbacksSync.TryAdd((int)tag, data))
            {
                throw new GatewayException("Invalid callback");
            }
        }

    }
}
