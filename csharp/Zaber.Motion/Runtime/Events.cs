using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Class handling events from the native library.
    /// </summary>
    internal static class Events
    {
        /// <summary>
        /// Event invoked when a reply from a device cannot be matched to any request.
        /// </summary>
        public static event Action<Requests.UnknownResponseEventWrapper>? UnknownResponse;

        /// <summary>
        /// Event invoked when a reply from a binary device cannot be matched to any request.
        /// </summary>
        public static event Action<Requests.UnknownBinaryResponseEventWrapper>? UnknownBinaryResponse;

        /// <summary>
        /// Event invoked when an alert is sent from device.
        /// </summary>
        public static event Action<Requests.AlertEventWrapper>? Alert;

        /// <summary>
        /// Event invoked when a reply-only command is sent from a binary device.
        /// </summary>
        public static event Action<Requests.BinaryReplyOnlyEventWrapper>? BinaryReplyOnly;

        /// <summary>
        /// Event invoked when connection is interrupted unexpectedly.
        /// </summary>
        public static event Action<Requests.DisconnectedEvent>? Disconnected;


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1810:Initialize reference type static fields inline", Justification = "Preference")]
        static Events()
        {
            // Provide the Go runtime with a consistent callback vector for events.
            _eventHandler = EventHandler;
            Runtime.ZaberGoRuntime.SetEventHandler(0, _eventHandler);
        }


        /// <summary>
        /// Task Scheduler which is used to invoke asynchronous events from native library.
        /// </summary>
        public static TaskScheduler EventTaskScheduler { get; set; } = TaskScheduler.Default;


        private static void EventHandler(ProtoData eventData, long tag)
        {
            Task.Factory.StartNew(
                () =>
                    {
                        ProcessEventCatch(eventData);
                    },
                CancellationToken.None,
                TaskCreationOptions.None,
                EventTaskScheduler);
        }


        private static void ProcessEventCatch(ProtoData eventData)
        {
            try
            {
                ProcessEvent(eventData);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unhandled exception in event handling:");
                Console.WriteLine(e);
                System.Diagnostics.Debug.WriteLine(e);
            }
        }


        private static void ProcessEvent(ProtoData eventData)
        {
            var eventProto = Requests.GatewayEvent.FromByteArray(eventData.Data[0]);
            if (!_parsers.TryGetValue(eventProto.Event, out var info))
            {
                throw new InvalidOperationException($"Unknown event: ${eventProto.Event}");
            }

            info.Event(info.Parser(eventData.Data[1]));
        }


        private interface IEventInfo
        {
            MessageParser<IMessage> Parser { get; }

            Action<IMessage> Event { get; }
        }

        private class EventInfo<T> : IEventInfo
            where T : IMessage
        {
            public MessageParser<T> Parser { get; set; } = _ => throw new NotImplementedException();

            public Action<T> Event { get; set; } = _ => throw new NotImplementedException();

            MessageParser<IMessage> IEventInfo.Parser => message => Parser(message);

            Action<IMessage> IEventInfo.Event => message => Event((T)message);
        }

        private static readonly Dictionary<string, IEventInfo> _parsers = new Dictionary<string, IEventInfo>()
        {
            {
                "test/event", new EventInfo<Requests.TestEvent>
                {
                    Parser = Requests.TestEvent.FromByteArray,
                    Event = message => TestEvent?.Invoke(message),
                }
            },
            {
                "interface/unknown_response", new EventInfo<Requests.UnknownResponseEventWrapper>
                {
                    Parser = Requests.UnknownResponseEventWrapper.FromByteArray,
                    Event = message => UnknownResponse?.Invoke(message),
                }
            },
            {
                "binary/interface/unknown_response", new EventInfo<Requests.UnknownBinaryResponseEventWrapper>
                {
                    Parser = Requests.UnknownBinaryResponseEventWrapper.FromByteArray,
                    Event = message => UnknownBinaryResponse?.Invoke(message),
                }
            },
            {
                "interface/alert", new EventInfo<Requests.AlertEventWrapper>
                {
                    Parser = Requests.AlertEventWrapper.FromByteArray,
                    Event = message => Alert?.Invoke(message),
                }
            },
            {
                "binary/interface/reply_only", new EventInfo<Requests.BinaryReplyOnlyEventWrapper>
                {
                    Parser = Requests.BinaryReplyOnlyEventWrapper.FromByteArray,
                    Event = message => BinaryReplyOnly?.Invoke(message),
                }
            },
            {
                "interface/disconnected", new EventInfo<Requests.DisconnectedEvent>
                {
                    Parser = Requests.DisconnectedEvent.FromByteArray,
                    Event = message => Disconnected?.Invoke(message),
                }
            },
        };


        /// <summary>
        /// This delegate has to kept here to prevent it from being garbage collected.
        /// </summary>
        private static readonly Runtime.Callback _eventHandler;

        /// <summary>
        /// Event used for testing purposes. Do not use in production code.
        /// </summary>
        public static event Action<Requests.TestEvent>? TestEvent;
    }
}
