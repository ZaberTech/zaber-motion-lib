﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

#pragma warning disable SA1313 // Parameter name should begin with a lowercase letter - disabled because of base class param names.

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Custom marshaller for protocol buffers.
    /// </summary>
    internal class ProtoMarshaler : ICustomMarshaler
    {
#pragma warning disable CA1801 // Parameter not used.
        /// <summary>
        /// Map this marshaller type to its string cookit.
        /// </summary>
        /// <param name="cookie">Data that can be used bythe custom marshaler.</param>
        /// <returns>A new instance of <see cref="ProtoMarshaler"/>.</returns>
        public static ICustomMarshaler GetInstance(string cookie) => new ProtoMarshaler();
#pragma warning restore CA1801


        /// <inheritdoc cref="ICustomMarshaler.MarshalNativeToManaged"/>
        public object MarshalNativeToManaged(IntPtr pNativeData)
        {
            if (IntPtr.Zero == pNativeData)
            {
                return null!;
            }

            var totalLength = Marshal.ReadInt32(pNativeData);
            int offset = sizeof(uint);

            var buffers = new ProtoData();

            while (offset < totalLength)
            {
                var length = Marshal.ReadInt32(pNativeData, offset);
                offset += sizeof(uint);

                byte[] buffer = new byte[length];
                Marshal.Copy(pNativeData + offset, buffer, 0, length);
                buffers.Data.Add(buffer);
                offset += length;
            }

            return buffers;
        }


        /// <inheritdoc cref="ICustomMarshaler.MarshalManagedToNative"/>
        public IntPtr MarshalManagedToNative(object ManagedObj)
        {
            if (null == ManagedObj)
            {
                return IntPtr.Zero;
            }

            var buffers = (ProtoData)ManagedObj;

            var size = sizeof(uint) + buffers.Data.Sum(buffer => buffer.Length + sizeof(uint));

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.WriteInt32(ptr, size);
            var offset = sizeof(uint);

            foreach (var buffer in buffers.Data)
            {
                Marshal.WriteInt32(ptr, offset, buffer.Length);
                offset += sizeof(uint);

                Marshal.Copy(buffer, 0, ptr + offset, buffer.Length);
                offset += buffer.Length;
            }

            return ptr;
        }


        /// <inheritdoc cref="ICustomMarshaler.CleanUpNativeData"/>
        public void CleanUpNativeData(IntPtr pNativeData) => Marshal.FreeHGlobal(pNativeData);


        /// <inheritdoc cref="ICustomMarshaler.CleanUpManagedData"/>
        public void CleanUpManagedData(object ManagedObj)
        {
        }


        /// <inheritdoc cref="ICustomMarshaler.GetNativeDataSize"/>
        public int GetNativeDataSize() => -1;
    }
}

#pragma warning restore SA1313
