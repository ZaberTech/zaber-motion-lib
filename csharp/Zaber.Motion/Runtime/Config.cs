using System.Threading.Tasks;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Provides means to configure the library's runtime.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Task Scheduler which is used to invoke asynchronous events from native library.
        /// This can be set in e.g. WPF application to make all events invoke in UI thread.
        /// </summary>
        public static TaskScheduler EventTaskScheduler
        {
            get { return Events.EventTaskScheduler; }
            set { Events.EventTaskScheduler = value; }
        }
    }
}
