﻿using System.Collections.Generic;

namespace Zaber.Motion.Runtime
{
    /// <summary>
    /// Serves as a data carier for <see cref="ProtoMarshaler"/>.
    /// It exists only because Mono does not like to marshall generics.
    /// </summary>
    internal class ProtoData
    {
        public List<byte[]> Data { get; set; } = new List<byte[]>(2);
    }
}
