﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// TODO (Soleil): Make CLS compliant. Protocol buffer generated code is blocking this.
// [assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
#if DEBUG
[assembly: InternalsVisibleTo("Zaber.Motion.Test")]
#else
[assembly: InternalsVisibleTo("Zaber.Motion.Test, PublicKey=00240000048000009400000006020000002400005253413100040000010001009d987c8c836c8f6e95d25705928767f824093a0d9c49c6876fefcfbd5ed2f834ce5737d06554093f5a7cbd0413f778fb24724ffaa074a4b280dc14f43cc404549c6c044322ccdc3ff4fae302a4e4262accf83d95bca06e5665ba73bb27ccc5c02ac5752d05ec2c9f864efaf32bd75532caa6064398ce398f66ad8d2cf636efd1")]
#endif
