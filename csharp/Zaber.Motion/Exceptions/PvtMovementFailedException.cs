﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a device registers a fault during PVT movement.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class PvtMovementFailedException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="PvtMovementFailedException"/>.
        /// </summary>
        public PvtMovementFailedExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PvtMovementFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public PvtMovementFailedException(string message, byte[] customData)
        : base(message)
        {
            Details = PvtMovementFailedExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PvtMovementFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public PvtMovementFailedException(string message, PvtMovementFailedExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
