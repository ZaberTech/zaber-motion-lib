﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Used for internal error handling.
    /// Indicates passing values of incorrect type from scripting languages or mixed library binary files.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class InvalidRequestDataException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidRequestDataException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public InvalidRequestDataException(string message)
        : base(message)
        {
        }
    }
}
