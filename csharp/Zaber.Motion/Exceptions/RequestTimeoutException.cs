﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a device does not respond to a request in time.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class RequestTimeoutException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestTimeoutException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public RequestTimeoutException(string message)
        : base(message)
        {
        }
    }
}
