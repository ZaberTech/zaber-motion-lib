﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a block of G-Code cannot be executed.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class GCodeExecutionException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="GCodeExecutionException"/>.
        /// </summary>
        public GCodeExecutionExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GCodeExecutionException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public GCodeExecutionException(string message, byte[] customData)
        : base(message)
        {
            Details = GCodeExecutionExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GCodeExecutionException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public GCodeExecutionException(string message, GCodeExecutionExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
