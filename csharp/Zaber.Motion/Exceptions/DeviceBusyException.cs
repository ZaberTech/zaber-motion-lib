﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a requested operation fails because the device is currently busy.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class DeviceBusyException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceBusyException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public DeviceBusyException(string message)
        : base(message)
        {
        }
    }
}
