﻿using System;

namespace Zaber.Motion
{
    /// <summary>
    /// Error propagated from the native library.
    /// </summary>
    public class MotionLibException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MotionLibException"/> class.
        /// </summary>
        public MotionLibException()
        : base()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MotionLibException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public MotionLibException(string message)
        : base(message)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MotionLibException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="innerException">Exception to store as inner exception.</param>
        public MotionLibException(string message, Exception innerException)
        : base(message, innerException)
        {
        }
    }
}
