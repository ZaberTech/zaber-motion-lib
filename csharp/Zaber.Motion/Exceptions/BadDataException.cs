﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a parameter of a command is judged to be out of range by the receiving device.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class BadDataException : CommandFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BadDataException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public BadDataException(string message, byte[] customData)
        : base(message, customData)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadDataException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public BadDataException(string message, CommandFailedExceptionData customData)
        : base(message, customData)
        {
        }
    }
}
