﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when an axis cannot be set to the supplied state.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class SetPeripheralStateFailedException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="SetPeripheralStateFailedException"/>.
        /// </summary>
        public SetPeripheralStateExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPeripheralStateFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public SetPeripheralStateFailedException(string message, byte[] customData)
        : base(message)
        {
            Details = SetPeripheralStateExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPeripheralStateFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public SetPeripheralStateFailedException(string message, SetPeripheralStateExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
