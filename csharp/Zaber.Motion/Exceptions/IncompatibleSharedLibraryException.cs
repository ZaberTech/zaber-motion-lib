﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when the loaded shared library is incompatible with the running code.
    /// Typically caused by mixed library binary files. Reinstall the library.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class IncompatibleSharedLibraryException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncompatibleSharedLibraryException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public IncompatibleSharedLibraryException(string message)
        : base(message)
        {
        }
    }
}
