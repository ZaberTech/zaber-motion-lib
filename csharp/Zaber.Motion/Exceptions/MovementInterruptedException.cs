﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when ongoing movement is interrupted by another command or user input.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class MovementInterruptedException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="MovementInterruptedException"/>.
        /// </summary>
        public MovementInterruptedExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MovementInterruptedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public MovementInterruptedException(string message, byte[] customData)
        : base(message)
        {
            Details = MovementInterruptedExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MovementInterruptedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public MovementInterruptedException(string message, MovementInterruptedExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
