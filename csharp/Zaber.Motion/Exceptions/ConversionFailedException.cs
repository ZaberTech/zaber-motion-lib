﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a value cannot be converted using the provided units.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class ConversionFailedException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public ConversionFailedException(string message)
        : base(message)
        {
        }
    }
}
