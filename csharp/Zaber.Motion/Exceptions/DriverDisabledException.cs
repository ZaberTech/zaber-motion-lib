﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a device cannot carry out a movement command because the motor driver is disabled.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class DriverDisabledException : CommandFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriverDisabledException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public DriverDisabledException(string message, byte[] customData)
        : base(message, customData)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverDisabledException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public DriverDisabledException(string message, CommandFailedExceptionData customData)
        : base(message, customData)
        {
        }
    }
}
