﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a command is rejected because the device is in EtherCAT Control (remote) mode.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class RemoteModeException : CommandFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteModeException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public RemoteModeException(string message, byte[] customData)
        : base(message, customData)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteModeException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public RemoteModeException(string message, CommandFailedExceptionData customData)
        : base(message, customData)
        {
        }
    }
}
