﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a device sends a response with unexpected type or data.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class InvalidResponseException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="InvalidResponseException"/>.
        /// </summary>
        public InvalidResponseExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidResponseException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public InvalidResponseException(string message, byte[] customData)
        : base(message)
        {
            Details = InvalidResponseExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidResponseException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public InvalidResponseException(string message, InvalidResponseExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
