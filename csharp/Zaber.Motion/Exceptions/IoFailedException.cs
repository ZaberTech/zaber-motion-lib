﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when the library cannot perform an operation on a file.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class IoFailedException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IoFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public IoFailedException(string message)
        : base(message)
        {
        }
    }
}
