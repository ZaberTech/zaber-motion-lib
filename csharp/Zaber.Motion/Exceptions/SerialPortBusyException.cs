﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a serial port cannot be opened because it is in use by another application.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class SerialPortBusyException : ConnectionFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SerialPortBusyException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public SerialPortBusyException(string message)
        : base(message)
        {
        }
    }
}
