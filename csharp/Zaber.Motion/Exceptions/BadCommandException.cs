﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a device receives an invalid command.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class BadCommandException : CommandFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BadCommandException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public BadCommandException(string message, byte[] customData)
        : base(message, customData)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadCommandException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public BadCommandException(string message, CommandFailedExceptionData customData)
        : base(message, customData)
        {
        }
    }
}
