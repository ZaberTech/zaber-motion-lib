﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a PVT sequence encounters discontinuity and interrupts the sequence.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class PvtDiscontinuityException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PvtDiscontinuityException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public PvtDiscontinuityException(string message)
        : base(message)
        {
        }
    }
}
