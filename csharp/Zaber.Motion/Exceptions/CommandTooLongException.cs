﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when a command is too long to be written by the ASCII protocol, even when continued across multiple lines.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class CommandTooLongException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="CommandTooLongException"/>.
        /// </summary>
        public CommandTooLongExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandTooLongException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public CommandTooLongException(string message, byte[] customData)
        : base(message)
        {
            Details = CommandTooLongExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandTooLongException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public CommandTooLongException(string message, CommandTooLongExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
