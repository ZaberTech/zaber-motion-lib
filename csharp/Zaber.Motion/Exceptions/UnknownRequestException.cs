﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Used for internal error handling. Indicates mixed library binary files. Reinstall the library.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class UnknownRequestException : MotionLibException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnknownRequestException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        public UnknownRequestException(string message)
        : base(message)
        {
        }
    }
}
