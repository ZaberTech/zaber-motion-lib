﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //
#pragma warning disable CA1502, CA1506
using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Converts errors from the native library to specific exceptions.
    /// </summary>
    public static class ExceptionConverter
    {
        /// <summary>
        /// Converts errors from the native library to specific exceptions.
        /// </summary>
        /// <param name="error">Error type from the native library.</param>
        /// <param name="message">Error message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data for the exception.</param>
        /// <returns>Specific exception converted from native library error.</returns>
        internal static MotionLibException Convert(Requests.Errors error, string message, byte[]? customData = null)
        {
            switch (error)
            {
                case Requests.Errors.BadCommand:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new BadCommandException(message, customData);
                case Requests.Errors.BadData:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new BadDataException(message, customData);
                case Requests.Errors.BinaryCommandFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new BinaryCommandFailedException(message, customData);
                case Requests.Errors.CommandFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new CommandFailedException(message, customData);
                case Requests.Errors.CommandPreempted:
                    return new CommandPreemptedException(message);
                case Requests.Errors.CommandTooLong:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new CommandTooLongException(message, customData);
                case Requests.Errors.ConnectionClosed:
                    return new ConnectionClosedException(message);
                case Requests.Errors.ConnectionFailed:
                    return new ConnectionFailedException(message);
                case Requests.Errors.ConversionFailed:
                    return new ConversionFailedException(message);
                case Requests.Errors.DeviceAddressConflict:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new DeviceAddressConflictException(message, customData);
                case Requests.Errors.DeviceBusy:
                    return new DeviceBusyException(message);
                case Requests.Errors.DeviceDbFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new DeviceDbFailedException(message, customData);
                case Requests.Errors.DeviceDetectionFailed:
                    return new DeviceDetectionFailedException(message);
                case Requests.Errors.DeviceFailed:
                    return new DeviceFailedException(message);
                case Requests.Errors.DeviceNotIdentified:
                    return new DeviceNotIdentifiedException(message);
                case Requests.Errors.DriverDisabled:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new DriverDisabledException(message, customData);
                case Requests.Errors.GCodeExecution:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new GCodeExecutionException(message, customData);
                case Requests.Errors.GCodeSyntax:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new GCodeSyntaxException(message, customData);
                case Requests.Errors.IncompatibleSharedLibrary:
                    return new IncompatibleSharedLibraryException(message);
                case Requests.Errors.InternalError:
                    return new InternalErrorException(message);
                case Requests.Errors.InvalidArgument:
                    return new InvalidArgumentException(message);
                case Requests.Errors.InvalidData:
                    return new InvalidDataException(message);
                case Requests.Errors.InvalidOperation:
                    return new InvalidOperationException(message);
                case Requests.Errors.InvalidPacket:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new InvalidPacketException(message, customData);
                case Requests.Errors.InvalidParkState:
                    return new InvalidParkStateException(message);
                case Requests.Errors.InvalidRequestData:
                    return new InvalidRequestDataException(message);
                case Requests.Errors.InvalidResponse:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new InvalidResponseException(message, customData);
                case Requests.Errors.IoChannelOutOfRange:
                    return new IoChannelOutOfRangeException(message);
                case Requests.Errors.IoFailed:
                    return new IoFailedException(message);
                case Requests.Errors.LockstepEnabled:
                    return new LockstepEnabledException(message);
                case Requests.Errors.LockstepNotEnabled:
                    return new LockstepNotEnabledException(message);
                case Requests.Errors.MovementFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new MovementFailedException(message, customData);
                case Requests.Errors.MovementInterrupted:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new MovementInterruptedException(message, customData);
                case Requests.Errors.NoDeviceFound:
                    return new NoDeviceFoundException(message);
                case Requests.Errors.NoValueForKey:
                    return new NoValueForKeyException(message);
                case Requests.Errors.NotSupported:
                    return new NotSupportedException(message);
                case Requests.Errors.OperationFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new OperationFailedException(message, customData);
                case Requests.Errors.OsFailed:
                    return new OsFailedException(message);
                case Requests.Errors.OutOfRequestIds:
                    return new OutOfRequestIdsException(message);
                case Requests.Errors.PvtDiscontinuity:
                    return new PvtDiscontinuityException(message);
                case Requests.Errors.PvtExecution:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new PvtExecutionException(message, customData);
                case Requests.Errors.PvtMode:
                    return new PvtModeException(message);
                case Requests.Errors.PvtMovementFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new PvtMovementFailedException(message, customData);
                case Requests.Errors.PvtMovementInterrupted:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new PvtMovementInterruptedException(message, customData);
                case Requests.Errors.PvtSetupFailed:
                    return new PvtSetupFailedException(message);
                case Requests.Errors.RemoteMode:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new RemoteModeException(message, customData);
                case Requests.Errors.RequestTimeout:
                    return new RequestTimeoutException(message);
                case Requests.Errors.SerialPortBusy:
                    return new SerialPortBusyException(message);
                case Requests.Errors.SetDeviceStateFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new SetDeviceStateFailedException(message, customData);
                case Requests.Errors.SetPeripheralStateFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new SetPeripheralStateFailedException(message, customData);
                case Requests.Errors.SettingNotFound:
                    return new SettingNotFoundException(message);
                case Requests.Errors.StreamDiscontinuity:
                    return new StreamDiscontinuityException(message);
                case Requests.Errors.StreamExecution:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new StreamExecutionException(message, customData);
                case Requests.Errors.StreamMode:
                    return new StreamModeException(message);
                case Requests.Errors.StreamMovementFailed:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new StreamMovementFailedException(message, customData);
                case Requests.Errors.StreamMovementInterrupted:
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

                    return new StreamMovementInterruptedException(message, customData);
                case Requests.Errors.StreamSetupFailed:
                    return new StreamSetupFailedException(message);
                case Requests.Errors.Timeout:
                    return new TimeoutException(message);
                case Requests.Errors.TransportAlreadyUsed:
                    return new TransportAlreadyUsedException(message);
                case Requests.Errors.UnknownRequest:
                    return new UnknownRequestException(message);
                default:
                    return new MotionLibException(message);
            }
        }
    }
}
