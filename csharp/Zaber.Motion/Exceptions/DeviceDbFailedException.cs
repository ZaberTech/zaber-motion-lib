﻿// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Thrown when device information cannot be retrieved from the device database.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design",
        "CA1032: Implement standard exception constructors",
        Justification = "This exception type is intended to be instantiated only by the Zaber Motion Library.")]
    public class DeviceDbFailedException : MotionLibException
    {
        /// <summary>
        /// Additional data for <see cref="DeviceDbFailedException"/>.
        /// </summary>
        public DeviceDbFailedExceptionData Details { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceDbFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data from the native library for the exception.</param>
        public DeviceDbFailedException(string message, byte[] customData)
        : base(message)
        {
            Details = DeviceDbFailedExceptionData.FromByteArray(customData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceDbFailedException"/> class.
        /// </summary>
        /// <param name="message">Message from the native library.</param>
        /// <param name="customData">Additional data about the exception.</param>
        public DeviceDbFailedException(string message, DeviceDbFailedExceptionData customData)
        : base(message)
        {
            Details = customData;
        }
    }
}
