package main

import (
	"log"
	"time"
	c "zaber-motion-lib/internal/communication"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func Comm() {
	port, err := cBase.OpenSerial("/dev/ttyUSB0", 0)
	if err != nil {
		log.Fatal(err)
	}

	protocol := c.NewProtocol(port, c.ProtocolOptions{})

	requests := c.NewRequestManager(protocol, c.RequestManagerEvents{})

	reply, cErr := requests.Request(c.Command{Command: "get version"}, nil)
	if cErr != nil {
		panic(cErr)
	}
	log.Print(reply[0])

	reply, cErr = requests.Request(c.Command{}, &c.RequestOptions{
		CollectMultiple: true,
	})
	if cErr != nil {
		panic(cErr)
	}
	log.Print("Replies: ", len(reply))

	<-time.After(1 * time.Second)

	if err = port.Close(); err != nil {
		panic(err)
	}

	<-time.After(1 * time.Second)
}

func main() {
	log.Print("Hello!")

	Comm()
}
