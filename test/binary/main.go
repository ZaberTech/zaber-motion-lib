package main

import (
	"log"
	"time"
	c "zaber-motion-lib/internal/communication"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func Comm() {
	port, err := cBase.OpenSerial("COM3", 9600)
	if err != nil {
		log.Fatal(err)
	}

	protocol := c.NewBinaryProtocol(port, c.BinaryProtocolOptions{UseMessageIDs: false})

	requests := c.NewBinaryRequestManager(protocol, c.BinaryRequestManagerEvents{})

	log.Print("Homing.")
	reply, cErr := requests.Request(c.BinaryMessage{Device: 1, Command: 1}, nil)
	if cErr != nil {
		panic(cErr)
	}

	log.Print("Replies: ", len(reply), reply[0])
	c1 := make(chan bool)
	c2 := make(chan bool)

	go func() {
		log.Print("Moving 1.")
		reply2, cErr2 := requests.Request(c.BinaryMessage{Device: 1, Command: 20, Data: 300000}, nil)
		if cErr2 != nil {
			panic(cErr2)
		}

		log.Print("Replies1: ", len(reply2), reply2[0])
		c1 <- true
	}()

	<-time.After(2 * time.Second)

	go func() {
		log.Print("Moving 2.")
		reply2, err2 := requests.Request(c.BinaryMessage{Device: 1, Command: 21, Data: -50000}, nil)
		if err2 != nil {
			panic(err2)
		}

		log.Print("Replies2: ", len(reply2), reply2[0])
		c2 <- true
	}()

	<-c1
	<-c2

	if err = port.Close(); err != nil {
		panic(err)
	}
}

func main() {
	log.Print("Hello!")

	Comm()
}
