
const path = require('path');
const { glob } = require('glob');
const fs = require('fs').promises;

async function main() {
  const files = await glob(path.join(__dirname, '..', 'src-web', '**', '*.web.*'), { windowsPathsNoEscape: true });
  for (const file of files) {
    await fs.rename(file, file.replace('.web.', '.'));
  }
}

main().catch(err => {
  console.log(err);
  process.exit(1);
});
