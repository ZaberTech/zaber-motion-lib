{
  "name": "@zaber/motion",
  "version": "7.5.0",
  "description": "Zaber Motion Library is a multi-platform library used to operate Zaber devices.",
  "keywords": [],
  "main": "dist/lib/index.js",
  "module": "dist/lib/index.js",
  "types": "dist/lib/index.d.ts",
  "exports": {
    ".": {
      "types": "./dist/lib/index.d.ts",
      "require": "./dist/lib/index.js",
      "import": "./dist/lib/index.js"
    },
    "./ascii": {
      "types": "./dist/lib/ascii/index.d.ts",
      "require": "./dist/lib/ascii/index.js",
      "import": "./dist/lib/ascii/index.js"
    },
    "./binary": {
      "types": "./dist/lib/binary/index.d.ts",
      "require": "./dist/lib/binary/index.js",
      "import": "./dist/lib/binary/index.js"
    },
    "./gcode": {
      "types": "./dist/lib/gcode/index.d.ts",
      "require": "./dist/lib/gcode/index.js",
      "import": "./dist/lib/gcode/index.js"
    },
    "./microscopy": {
      "types": "./dist/lib/microscopy/index.d.ts",
      "require": "./dist/lib/microscopy/index.js",
      "import": "./dist/lib/microscopy/index.js"
    },
    "./product": {
      "types": "./dist/lib/product/index.d.ts",
      "require": "./dist/lib/product/index.js",
      "import": "./dist/lib/product/index.js"
    },
    "./exceptions": {
      "types": "./dist/lib/exceptions/index.d.ts",
      "require": "./dist/lib/exceptions/index.js",
      "import": "./dist/lib/exceptions/index.js"
    },
    "./dist/binding/*": "./dist/binding/*",
    "./dist/lib": {
      "types": "./dist/lib/index.d.ts",
      "require": "./dist/lib/index.js",
      "import": "./dist/lib/index.js"
    },
    "./wasm": {
      "types": "./dist/lib/wasm.d.ts",
      "require": "./dist/lib/wasm.js",
      "import": "./dist/lib/wasm.js"
    },
    "./custom_bindings": {
      "types": "./dist/lib/custom_bindings.d.ts",
      "require": "./dist/lib/custom_bindings.js",
      "import": "./dist/lib/custom_bindings.js"
    }
  },
  "files": [
    "dist/lib/",
    "dist/binding/wasm/",
    "binding.gyp"
  ],
  "author": "Zaber Technologies Inc.",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/ZaberTech/zaber-motion-lib.git"
  },
  "bugs": {
    "url": "https://software.zaber.com/motion-library/docs/support/contact/"
  },
  "homepage": "https://software.zaber.com/motion-library",
  "license": "MIT",
  "engines": {
    "node": ">=16.0.0"
  },
  "scripts": {
    "clean": "npm run clean-dist && node-pre-gyp clean",
    "install": "node-pre-gyp install --fallback-to-build",
    "pack": "npm pack",
    "build": "tsc --project tsconfig.build.json",
    "clean-dist": "rimraf dist",
    "generate-doc": "typedoc --out docs --target es6 --theme minimal --mode file src",
    "build-bindings": "node-pre-gyp clean configure build",
    "build-bindings-ia32": "node-pre-gyp configure build --target_arch=ia32",
    "clean-bindings": "rimraf dist/binding",
    "publish-bindings": "node-pre-gyp package publish",
    "build-all": "npm run build && npm run build-bindings",
    "lint:js": "eslint .",
    "lint:test-web": "cd test-web && eslint .",
    "lint": "run-s lint:*",
    "start": "npm run test:watch",
    "start-web-example": "npx webpack serve --mode development --config examples/web/webpack.config.js",
    "test": "jest",
    "test:watch": "npm run test -- --watch",
    "test-web": "npx karma start test-web/karma.conf.js --single-run",
    "test-web:watch": "npx karma start test-web/karma.conf.js --browsers Chrome",
    "node-pre-gyp": "node-pre-gyp",
    "ts-node": "ts-node",
    "tsc": "tsc"
  },
  "binary": {
    "module_name": "zaber-motion",
    "module_path": "./dist/binding/{configuration}/{napi_build_version}-{platform}-{arch}/",
    "remote_path": "./{module_name}/v{version}/{configuration}/",
    "host": "https://zaber-motion-node-pre-gyp.s3-us-west-2.amazonaws.com",
    "package_name": "{napi_build_version}-{platform}-{arch}.tar.gz",
    "napi_versions": [
      6
    ]
  },
  "nyc": {
    "extension": [
      ".ts",
      ".tsx"
    ],
    "include": [
      "src/**"
    ],
    "exclude": [
      "**/*.d.ts"
    ],
    "reporter": [
      "html"
    ],
    "all": true
  },
  "dependencies": {
    "@mapbox/node-pre-gyp": "^1.0.5",
    "bson": "^6.6.0",
    "node-addon-api": "^4.3.0",
    "pkg-up": "^3.1.0",
    "rxjs": "^7.2.0"
  },
  "devDependencies": {
    "@types/bluebird": "^3.5.36",
    "@types/chai": "^4.2.21",
    "@types/chai-as-promised": "^7.1.4",
    "@types/chai-string": "^1.4.2",
    "@types/glob": "^7.1.1",
    "@types/got": "9.6.12",
    "@types/jasmine": "^3.8.1",
    "@types/jest": "^29.5.12",
    "@types/lodash": "^4.14.171",
    "@types/mqtt": "2.5.0",
    "@types/node": "~22.5.5",
    "@types/pkg-up": "^3.1.0",
    "@types/split2": "^3.2.1",
    "@types/uuid": "^8.3.1",
    "aws-sdk": "^2.942.0",
    "bluebird": "^3.7.2",
    "chai": "^4.3.4",
    "chai-as-promised": "^7.1.1",
    "chai-string": "^1.5.0",
    "copy-webpack-plugin": "^9.0.1",
    "copyfiles": "^2.4.1",
    "coveralls": "^3.1.1",
    "glob": "^10.3.14",
    "got": "11.8.2",
    "jasmine": "^3.8.0",
    "jest": "^29.7.0",
    "karma": "^6.3.4",
    "karma-chrome-launcher": "^3.1.0",
    "karma-firefox-launcher": "^2.1.1",
    "karma-jasmine": "^4.0.1",
    "karma-typescript": "^5.5.4",
    "karma-webpack": "^5.0.0",
    "lodash": "^4.17.21",
    "mqtt": "4.2.8",
    "node-polyfill-webpack-plugin": "^1.1.4",
    "nyc": "^15.1.0",
    "rimraf": "^3.0.2",
    "simple-jsonrpc-js": "^1.2.0",
    "split2": "^3.2.2",
    "ts-jest": "^29.1.4",
    "ts-loader": "^9.2.3",
    "ts-node": "^10.0.0",
    "tsx": "^4.16.2",
    "typescript": "5.4.3",
    "uuid": "^8.3.2",
    "webpack": "^5.44.0",
    "webpack-cli": "^4.7.2",
    "webpack-dev-server": "^3.11.2"
  }
}
