module.exports = function(config) {
  config.set({
    basePath: '../',
    files: [
      { pattern: 'test-web/**/*.test.ts' },
      { pattern: 'dist/binding/wasm/zaber-motion-core.wasm', served: true, included: false, type: 'wasm' },
    ],
    mime: {
      'application/wasm': ['wasm'],
      'text/x-typescript': ['ts', 'tsx'],
    },
    frameworks: ['jasmine'],
    preprocessors: {
      'test-web/**/*.test.ts': ['webpack'],
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    karmaTypescriptConfig: {
    },
    webpack: require('./webpack.config'),
    logLevel: config.LOG_INFO,
    browsers: ['Chrome'],
    autoWatch: true,
    concurrency: Infinity,
    customLaunchers: {
      FirefoxHeadless: {
        base: 'Firefox',
        flags: ['-headless'],
      },
    },
  });
};
