import _ from 'lodash';
import { lastValueFrom } from 'rxjs';

import * as zmlWasm from '../dist/lib/wasm';
import * as zml from '../dist/lib';

import { MockTransport } from './mock_transport';

describe('Web Assembly Library Test', () => {
  const pathToWasm = 'base/dist/binding/wasm/zaber-motion-core.wasm';
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

  beforeAll(async () => {
    await zmlWasm.init(fetch(pathToWasm));
  });

  afterAll(async () => {
    await zmlWasm.shutdown();
  });

  describe('init/shutdown', async () => {
    it('rejects if initialized', async () => {
      const runWasmPromise = zmlWasm.init(fetch(pathToWasm));
      const exception: Error = await runWasmPromise.catch(e => e);
      expect(exception.toString()).toBe('Error: Zaber motion library already running or is initializing.');
    });
  });

  describe('general integration', async () => {
    let connection: zml.ascii.Connection;
    let mock: MockTransport;
    let device: zml.ascii.Device;
    let axis: zml.ascii.Axis;

    beforeEach(async () => {
      mock = new MockTransport();
      connection = await zml.ascii.Connection.openCustom(mock.transport);

      const detectPromise = connection.detectDevices({ identifyDevices: false });
      expect(await mock.pop()).toBe('/0 0 00');
      await mock.push('@01 0 00 OK IDLE -- 0');
      expect(await mock.pop()).toBe('/1 0 01 get comm.packet.size.max');
      await mock.push('@1 0 1 OK IDLE -- 80');
      const deviceList = await detectPromise;

      device = deviceList[0];
      axis = device.getAxis(1);
    });

    afterEach(async () => {
      await mock.close();
      await connection.close();
    });

    it('runs an example script', async () => {
      const homePromise = axis.home();
      expect(await mock.pop()).toBe('/1 1 02 home');
      await mock.push('@01 1 02 OK BUSY -- 0');
      expect(await mock.pop()).toBe('/1 1 03');
      await mock.push('@01 1 03 OK IDLE -- 0');
      await homePromise;

      const axisMovePromise = axis.moveAbsolute(1000);
      expect(await mock.pop()).toBe('/1 1 04 move abs 1000');
      await mock.push('@01 1 04 OK BUSY -- 0');
      expect(await mock.pop()).toBe('/1 1 05');
      await mock.push('@01 1 05 OK IDLE -- 0');
      await axisMovePromise;
    });

    it('waits until device sends alert', async () => {
      const promise = axis.waitUntilIdle();
      expect(await mock.pop()).toBe('/1 1 02');
      await mock.push('@01 1 02 OK BUSY -- 0');
      expect(await mock.pop()).toBe('/1 1 03');
      await mock.push('@01 1 03 OK BUSY -- 0');
      expect(await mock.pop()).toBe('/1 1 04');
      await mock.push('@01 1 04 OK BUSY -- 0');

      await mock.push('!01 1 IDLE --');

      // avoid race condition
      mock.pop().then(() => mock.push('@01 2 05 OK BUSY -- 0')).catch(() => null);

      await promise;
    });

    it('handles connection closing during script', async () => {
      const homePromise = axis.home();
      expect(await mock.pop()).toBe('/1 1 02 home');
      await mock.push('@01 1 02 OK BUSY -- 0');
      expect(await mock.pop()).toBe('/1 1 03');

      await mock.close();

      const error: zml.ConnectionClosedException = await homePromise.catch(e => e);
      expect(error.toString()).toBe('RequestTimeoutException: Device has not responded in given timeout');

      const homePromiseTwo = axis.home();
      const errorTwo: zml.ConnectionClosedException = await homePromiseTwo.catch(e => e);
      expect(errorTwo.toString()).toBe('ConnectionClosedException: Connection has been closed');
    });

    it('handles connection erroring during script', async () => {
      await mock.close(true);

      const homePromise = axis.home();
      const error: zml.ConnectionClosedException = await homePromise.catch(e => e);
      expect(error.toString()).toBe('ConnectionClosedException: Connection has been closed');
    });
  });

  describe('js connection', async () => {
    it('opens multiple connections and sends requests in parallel', async () => {
      await Promise.all(_.range(0, 9).map(async axis => {
        const mock = new MockTransport();
        const connection = await zml.ascii.Connection.openCustom(mock.transport);

        const homePromise = connection.genericCommand('home', { device: 1, axis });
        expect(await mock.pop()).toBe(`/1 ${axis} 00 home`);
        await mock.push(`@01 ${axis} 00 OK IDLE -- 0`);
        await homePromise;

        await mock.close();
        await connection.close();
      }));
    });

    it('prints connection string', async () => {
      const mock = new MockTransport();
      const connection = await zml.ascii.Connection.openCustom(mock.transport);
      expect(connection.toString()).toMatch(/Connection \d+ \(ASCII Custom: \d+\)/);
    });

    it('raises disconnected event', async () => {
      await Promise.all(_.range(0, 9).map(async () => {
        const mock = new MockTransport();
        const connection = await zml.ascii.Connection.openCustom(mock.transport);

        const disconnectPromise = lastValueFrom(connection.disconnected);

        await mock.close();
        await connection.close();

        const error = await disconnectPromise;
        expect(error.toString()).toBe('ConnectionClosedException: Connection has been closed');
      }));
    });
  });
});
