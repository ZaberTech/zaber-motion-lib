const tsEslint = require('typescript-eslint');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');

module.exports = tsEslint.config(
  {
    files: ['**/*.js'],
    extends: [zaberEslintConfig.javascript],
  },
  {
    files: ['**/*.ts'],
    extends: [
      zaberEslintConfig.typescript(),
      zaberEslintConfig.test,
    ],
    rules: {
      '@typescript-eslint/no-misused-promises': [
        'error',
        { checksVoidReturn: false }
      ],
      'no-console': 'error',
      'max-len': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/no-namespace': 'off',
      '@typescript-eslint/no-duplicate-enum-values': 'off',
      'import/order': 'off',
      'object-shorthand': 'off',
      '@typescript-eslint/unbound-method': 'off',
      // Doesn't work as strict nulls aren't on for tests
      '@typescript-eslint/prefer-nullish-coalescing': 'off',
    }
  },
);
