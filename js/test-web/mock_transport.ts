import { ascii } from '../dist/lib';

export class MockTransport {
  private receivedStack: string[] = [];
  private onReceived: () => void;
  private loopPromise: Promise<void>;

  public transport: ascii.Transport;
  public close: (withError?: boolean) => Promise<void>;

  constructor() {
    this.transport = ascii.Transport.open();
    this.loopPromise = this.readLoop();
  }

  public async readLoop(): Promise<void> {
    const stopSymbol = Symbol('stop');
    const stopWithErrorSymbol = Symbol('errorStop');

    let stopPromiseResolve: (resolveSymbol: symbol) => void = null;
    const stopPromise = new Promise<symbol>(resolve => stopPromiseResolve = resolve);

    this.close = async (withError?: boolean) => {
      stopPromiseResolve(withError ? stopWithErrorSymbol : stopSymbol);
      await this.loopPromise;
    };

    for (;;) {
      const line = await Promise.race([this.transport.read(), stopPromise]);
      if (line === stopSymbol) {
        await this.transport.close();
        return;
      }
      if (line === stopWithErrorSymbol) {
        await this.transport.closeWithError('Some error');
        return;
      }

      this.receivedStack.push(line as string);

      const onReceived = this.onReceived;
      if (onReceived) {
        this.onReceived = null;
        onReceived();
      }
    }
  }

  public async push(line: string): Promise<void> {
    await this.transport.write(line);
  }

  public async pop(keepChecksum: boolean = false): Promise<string> {
    if (this.receivedStack.length === 0) {
      if (this.onReceived) {
        throw new Error('Already receiving.');
      }

      await new Promise<void>(resolve => this.onReceived = resolve);
    }

    let line = this.receivedStack.pop()!;
    if (!keepChecksum) {
      line = line.replace(/:.{2}$/, '');
    }
    return line;
  }
}
