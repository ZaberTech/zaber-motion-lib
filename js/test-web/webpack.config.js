const webpack = require('webpack');
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

module.exports = {
  mode: 'none',
  devtool: 'inline-source-map',
  module: {
    rules: [{
      test: /\.tsx?$/,
      exclude: [/node_modules/],
      loader: 'ts-loader',
    }]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new NodePolyfillPlugin(),
    new webpack.IgnorePlugin({
      checkResource(resource, context) {
        if (resource === './bindings.node' && context.includes('zaber-motion-lib')) {
          return true;
        }
        return false;
      },
    }),
  ],
};
