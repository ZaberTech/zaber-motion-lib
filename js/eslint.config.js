const tsEslint = require('typescript-eslint');
const { includeIgnoreFile } = require('@eslint/compat');
const path = require('node:path');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');

const gitignore = path.resolve(__dirname, '.gitignore');

module.exports = tsEslint.config(
  includeIgnoreFile(gitignore),
  {
    ignores: ['examples/web/**/*', 'src/gateway/wasm-exec.js', 'test-web/'],
  },
  {
    files: ['**/*.js'],
    extends: [
      zaberEslintConfig.javascript,
    ],
    rules: {
      'no-console': 'off',
    }
  },
  {
    files: ['**/*.ts'],
    extends: [
      zaberEslintConfig.typescript(),
    ],
    rules: {
      '@typescript-eslint/no-misused-promises': [
        'error',
        { checksVoidReturn: false }
      ],
      'no-console': 'error',
      'max-len': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/no-namespace': 'off',
      '@typescript-eslint/no-duplicate-enum-values': 'off',
      'import/order': 'off',
      'object-shorthand': 'off',
      '@typescript-eslint/unbound-method': 'off',
    }
  },
  {
    files: ['test/**/*', 'test-e2e/**/*'],
    extends: [
      zaberEslintConfig.test,
    ],
    rules: {
      '@typescript-eslint/no-unused-expressions': 'off',
      '@typescript-eslint/unbound-method': 'off',
    }
  },
  {
    files: ['src/gateway/**/*'],
    rules: {
      '@typescript-eslint/no-unsafe-call': 'off',
      '@typescript-eslint/no-unsafe-member-access': 'off',
      '@typescript-eslint/no-unsafe-return': 'off',
      '@typescript-eslint/no-unsafe-argument': 'off',
    }
  },
  {
    files: ['examples/**/*'],
    rules: {
      'no-console': 'off',
    }
  },
);
