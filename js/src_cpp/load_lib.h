#pragma once

#include "includes.h"

typedef int (*callType)(void *data, int64_t tag, void *callback, uint8_t async);
typedef void (*setEventHandlerType)(int64_t tag, void *callback);

namespace motion_lib
{
    extern callType callAsync;
    extern setEventHandlerType setEventHandler;
    bool loadLibrary(const Napi::String &directory);
}
