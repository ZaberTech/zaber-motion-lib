#pragma once

#include "includes.h"

namespace motion_lib
{
    void callback(const void *data, int64_t tag);
    void callbackEvent(const void *data, int64_t tag);
    void callbackSync(const void *data, int64_t tag);
    void CallAsync(const Napi::CallbackInfo &info);
    Napi::Value CallSync(const Napi::CallbackInfo &info);
    void SetEventHandler(const Napi::CallbackInfo &info);
    void SetEventsEnabled(const Napi::CallbackInfo &info);
    void LoadDll(const Napi::CallbackInfo &info);
    Napi::Object Init(Napi::Env env, Napi::Object exports);
}
