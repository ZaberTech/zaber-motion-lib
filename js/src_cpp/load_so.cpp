#include <iostream>
#include <dlfcn.h>

#include "load_lib.h"

callType motion_lib::callAsync = nullptr;
setEventHandlerType motion_lib::setEventHandler = nullptr;

bool motion_lib::loadLibrary(const Napi::String &directory)
{
    auto directoryStr = directory.Utf8Value();

#if __APPLE__ && __aarch64__
    auto libFile = directoryStr + "zaber-motion-core-darwin-arm64.dylib";
#elif __APPLE__ && __x86_64__
    auto libFile = directoryStr + "zaber-motion-core-darwin-amd64.dylib";
#elif __aarch64__
    auto libFile = directoryStr + "zaber-motion-core-linux-arm64.so";
#elif __arm__
    auto libFile = directoryStr + "zaber-motion-core-linux-arm.so";
#elif __x86_64__
    auto libFile = directoryStr + "zaber-motion-core-linux-amd64.so";
#elif __i386__
    auto libFile = directoryStr + "zaber-motion-core-linux-386.so";
#else
    #error "Platform not supported"
#endif

    void *handle = dlopen(libFile.c_str(), RTLD_NOW);
    if (handle == nullptr)
    {
        std::cerr << "could not load the dynamic library " << libFile << std::endl;
        return false;
    }

    callAsync = reinterpret_cast<callType>(dlsym(handle, "zml_call"));
    setEventHandler = reinterpret_cast<setEventHandlerType>(dlsym(handle, "zml_setEventHandler"));

    if (callAsync == nullptr || setEventHandler == nullptr)
    {
        std::cerr << "could not locate the functions" << std::endl;
        return false;
    }

    return true;
}
