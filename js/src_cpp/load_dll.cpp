#include <iostream>
#include <windows.h>

#include "load_lib.h"

callType motion_lib::callAsync = nullptr;
setEventHandlerType motion_lib::setEventHandler = nullptr;

bool motion_lib::loadLibrary(const Napi::String &directory)
{
    auto directoryU = directory.Utf16Value();
    std::wstring directoryW(directoryU.begin(), directoryU.end());

#if _M_ARM64 || _M_ARM64EC
    auto libFileW = directoryW + L"zaber-motion-core-windows-arm64.dll";
#elif _WIN64
    auto libFileW = directoryW + L"zaber-motion-core-windows-amd64.dll";
#else
    auto libFileW = directoryW + L"zaber-motion-core-windows-386.dll";
#endif

    HINSTANCE hGetProcIDDLL = LoadLibraryW(libFileW.c_str());
    if (hGetProcIDDLL == nullptr)
    {
        std::wcerr << L"could not load the dynamic library " << libFileW << std::endl;
        return false;
    }

    callAsync = reinterpret_cast<callType>(GetProcAddress(hGetProcIDDLL, "zml_call"));
    setEventHandler = reinterpret_cast<setEventHandlerType>(GetProcAddress(hGetProcIDDLL, "zml_setEventHandler"));

    if (callAsync == nullptr || setEventHandler == nullptr)
    {
        std::wcerr << L"could not locate the functions" << std::endl;
        return false;
    }

    return true;
}
