#include "callback.h"

#define UNLIMITED_QUEUE (0)

motion_lib::Callback::Callback(const Napi::Function &callback):
    _callback(Napi::ThreadSafeFunction::New<const char *, void, decltype(Callback::Done), Callback>(
        callback.Env(), callback, "ZML Callback", UNLIMITED_QUEUE, 1, nullptr, Callback::Done, this))
{
}

motion_lib::Callback::~Callback()
{
}

void motion_lib::Callback::Call(const void *data)
{
    uint32_t size = *reinterpret_cast<const uint32_t *>(data);
    // copy data for callback so that data can be deallocated in Go
    uint8_t* buffer = new uint8_t[size];
    memcpy(buffer, data, size);

    auto callResult = this->_callback.BlockingCall(buffer, [](Napi::Env env, Napi::Function callback, uint8_t* buffer) {
        uint32_t size = *reinterpret_cast<const uint32_t *>(buffer);
        auto response = Napi::Buffer<uint8_t>::Copy(env, buffer, size);
        delete[] buffer;

        callback.Call({response});
    });

    if (callResult != napi_ok)
    {
        delete[] buffer;
    }
}

void motion_lib::Callback::Release()
{
    this->_callback.Release();
}

void motion_lib::Callback::Done(const Napi::Env env, Callback* self, void* ctx) {
    delete self;
}
