#pragma once

#define NAPI_VERSION (6)

#include <napi.h>
#include <cstdint>
#include <string>
#include <mutex>
#include <shared_mutex>
#include <set>
#include <atomic>
