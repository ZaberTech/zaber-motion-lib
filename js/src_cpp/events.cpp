#include "events.h"

motion_lib::Events::Events(const Napi::Function &callback):
    _callback(Napi::Persistent(callback)), _current(nullptr)
{
}

motion_lib::Events::~Events()
{
    this->Close();
}

void motion_lib::Events::Open()
{
    std::lock_guard<std::mutex> guard(this->_lock);

    if (this->_current != nullptr)
    {
        return;
    }

    this->_current = new Callback(this->_callback.Value());
}

void motion_lib::Events::Close()
{
    std::lock_guard<std::mutex> guard(this->_lock);

    if (this->_current == nullptr)
    {
        return;
    }

    this->_current->Release();
    // the callback will safely delete itself
    this->_current = nullptr;
}

void motion_lib::Events::Call(const void *data)
{
    std::lock_guard<std::mutex> guard(this->_lock);

    if (this->_current == nullptr)
    {
        return;
    }

    this->_current->Call(data);
}
