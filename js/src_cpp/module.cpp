#include "load_lib.h"

#include "module.h"
#include "callback.h"
#include "events.h"

static_assert(sizeof(int64_t) >= sizeof(void *), "Cannot use tag to store pointers.");

bool dllLoaded;
std::mutex dllLock;

class InstanceData {
    public:
        InstanceData(): eventMarshal(nullptr) {
            std::unique_lock<std::shared_mutex> lock(instancesLock);
            instances.insert(this);
        }
        ~InstanceData() {
            {
                std::unique_lock<std::shared_mutex> lock(instancesLock);
                instances.erase(this);
            }

            if (this->eventMarshal != nullptr) {
                delete this->eventMarshal;
                this->eventMarshal = nullptr;
            }
        }

        motion_lib::Events *eventMarshal;

        static std::shared_mutex instancesLock;
        static std::set<InstanceData*> instances;
};

std::set<InstanceData*> InstanceData::instances;
std::shared_mutex InstanceData::instancesLock;

void motion_lib::callback(const void *data, int64_t tag)
{
    auto callback = reinterpret_cast<Callback *>(tag);
    callback->Call(data);
    callback->Release();
}

void motion_lib::callbackEvent(const void *data, int64_t tag)
{
    std::shared_lock<std::shared_mutex> lock(InstanceData::instancesLock);
    for (auto instance : InstanceData::instances) {
        auto eventMarshal = instance->eventMarshal;
        if (eventMarshal != nullptr) {
            eventMarshal->Call(data);
        }
    }
}

void motion_lib::callbackSync(const void *data, int64_t tag)
{
    uint32_t size = *reinterpret_cast<const uint32_t *>(data);
    auto buffer = reinterpret_cast<std::string *>(tag);
    buffer->assign(reinterpret_cast<const char *>(data), size_t(size));
}

void motion_lib::CallAsync(const Napi::CallbackInfo &info)
{
    Napi::Env env = info.Env();
    auto buffer = info[0].As<Napi::Buffer<char>>();
    auto callbackFuncJs = info[1].As<Napi::Function>();

    auto callback = new Callback(callbackFuncJs);

    auto result = motion_lib::callAsync(buffer.Data(), reinterpret_cast<int64_t>(callback), reinterpret_cast<void *>(motion_lib::callback), 1);
    if (result != 0)
    {
        Napi::Error::New(env, "Invalid result code: " + std::to_string(result)).ThrowAsJavaScriptException();
        return;
    }
}

Napi::Value motion_lib::CallSync(const Napi::CallbackInfo &info)
{
    Napi::Env env = info.Env();
    auto buffer = info[0].As<Napi::Buffer<char>>();
    std::string callbackBuffer;

    auto result = motion_lib::callAsync(buffer.Data(), reinterpret_cast<int64_t>(&callbackBuffer), reinterpret_cast<void *>(motion_lib::callbackSync), 0);
    if (result != 0)
    {
        Napi::Error::New(env, "Invalid result code: " + std::to_string(result)).ThrowAsJavaScriptException();
        return env.Undefined();
    }

    auto response = Napi::Buffer<char>::Copy(env, callbackBuffer.data(), callbackBuffer.size());
    return response;
}

void motion_lib::SetEventHandler(const Napi::CallbackInfo &info)
{
    auto env = info.Env();
    auto instanceData = env.GetInstanceData<InstanceData>();
    if (instanceData->eventMarshal != nullptr) {
        // jest uses the same context over and over, so we will tolerate setting the handler again.
        auto isJestTest = std::getenv("JEST_WORKER_ID") != nullptr;
        if (!isJestTest) {
            Napi::Error::New(env, "SetEventHandler can only be called once").ThrowAsJavaScriptException();
            return;
        }

        delete instanceData->eventMarshal;
        instanceData->eventMarshal = nullptr;
    }

    auto callbackFuncJs = info[0].As<Napi::Function>();
    instanceData->eventMarshal = new Events(callbackFuncJs);
}

void motion_lib::SetEventsEnabled(const Napi::CallbackInfo &info)
{
    auto env = info.Env();
    auto instanceData = env.GetInstanceData<InstanceData>();
    if (instanceData->eventMarshal == nullptr) {
        Napi::Error::New(env, "SetEventHandler has not been called yet").ThrowAsJavaScriptException();
        return;
    }

    auto areEnabled = info[0].As<Napi::Boolean>().Value();
    if (areEnabled)
    {
        instanceData->eventMarshal->Open();
    }
    else
    {
        instanceData->eventMarshal->Close();
    }
}

void motion_lib::LoadDll(const Napi::CallbackInfo &info)
{
    std::lock_guard<std::mutex> guard(dllLock);
    if (dllLoaded) {
        return;
    }

    Napi::Env env = info.Env();
    auto directory = info[0].As<Napi::String>();

    if (!motion_lib::loadLibrary(directory))
    {
        Napi::Error::New(env, "Cannot Load zaber-motion-core dll/so").ThrowAsJavaScriptException();
        return;
    }

    motion_lib::setEventHandler(0, reinterpret_cast<void *>(motion_lib::callbackEvent));
    dllLoaded = true;
}

Napi::Object motion_lib::Init(Napi::Env env, Napi::Object exports)
{
    auto instanceData = new InstanceData();
    env.SetInstanceData(instanceData); // deletes instanceData

    exports.Set("callAsync", Napi::Function::New(env, motion_lib::CallAsync));
    exports.Set("callSync", Napi::Function::New(env, motion_lib::CallSync));
    exports.Set("setEventHandler", Napi::Function::New(env, motion_lib::SetEventHandler));
    exports.Set("loadDll", Napi::Function::New(env, motion_lib::LoadDll));
    exports.Set("setEventsEnabled", Napi::Function::New(env, motion_lib::SetEventsEnabled));

    return exports;
}

Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    return motion_lib::Init(env, exports);
}

NODE_API_MODULE(testaddon, InitAll)
