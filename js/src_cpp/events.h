#pragma once

#include "includes.h"
#include "callback.h"

namespace motion_lib
{
    class Events
    {
    public:
        Events(const Napi::Function &callback);
        virtual ~Events();

        Events(const Events &) = delete;

        void Call(const void *data);

        void Open();
        void Close();
    private:
        Napi::FunctionReference _callback;
        std::mutex _lock;
        Callback* _current;
    };
}
