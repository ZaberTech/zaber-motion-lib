#pragma once

#include "includes.h"

namespace motion_lib
{
    class Callback
    {
    public:
        Callback(const Napi::Function &callback);
        virtual ~Callback();

        Callback(const Callback &) = delete;

        void Call(const void *data);
        void Release();
    private:
        static void Done(const Napi::Env env, Callback* callback, void* ctx);

        Napi::ThreadSafeFunction _callback;
    };
}
