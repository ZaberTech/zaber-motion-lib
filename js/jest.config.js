module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.(ts|tsx)?$': ['ts-jest', { tsconfig: 'tsconfig.json', isolatedModules: true }],
  },
  setupFilesAfterEnv: ['<rootDir>/test/setup.ts'],
  moduleDirectories: ['node_modules'],
  testMatch: [
    '<rootDir>/test-e2e/**/*.test.ts',
    '<rootDir>/test/**/*.test.ts',
  ],
  testTimeout: 10000,
};
