
import { Library, LogOutputMode, Length, Velocity } from '../src';
import { Connection } from '../src/ascii';

(async () => {
  Library.setLogOutput(LogOutputMode.STDOUT);

  const conn = await Connection.openSerialPort('COM3');
  try {
    await conn.enableAlerts();

    const [dev] = await conn.detectDevices();

    await dev.allAxes.home();

    const axis = dev.getAxis(1);

    await axis.moveAbsolute(1, Length.cm);

    await axis.moveRelative(-5, Length.mm);

    await axis.moveVelocity(1, Velocity['mm/s']);
    await new Promise<void>(r => setTimeout(r, 2000));
    await axis.stop();

    const position = await axis.getPosition(Length.mm);
    console.log('Position', position);
  } finally {
    await conn.close();
  }
})().catch(console.log);
