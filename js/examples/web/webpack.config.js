const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const dest = path.join(__dirname, 'build');

module.exports = {
  entry: path.join(__dirname, '..', '..', 'dist', 'lib', 'index.js'),
  devtool: 'inline-source-map',
  output: {
    path: dest,
    filename: 'index.js',
    library: 'zml',
    libraryTarget: 'umd',
  },
  resolve: {
    extensions: ['.js'],
  },
  devServer: {
    contentBase: __dirname,
  },
  plugins: [
    new CopyPlugin([
      { from: path.join(__dirname, '..', '..', 'dist', 'binding', 'wasm', 'zaber-motion-core.wasm'), to: dest },
    ]),
  ],
};
