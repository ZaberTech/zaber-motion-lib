class LineBreakTransformer {
  constructor() {
    this.container = '';
  }

  transform(chunk, controller) {
    this.container += chunk;
    const lines = this.container.split('\r\n');
    this.container = lines.pop();
    lines.forEach(line => controller.enqueue(line));
  }

  flush(controller) {
    controller.enqueue(this.container);
  }
}

async function portReaderLoop(port, transport) {
  const portReader = port.readable
    .pipeThrough(new TextDecoderStream())
    .pipeThrough(new TransformStream(new LineBreakTransformer()))
    .getReader();

  while (true) {
    const data = await portReader.read();
    console.log('PORT REPLY:', data);
    if (data.value) {
      transport.write(data.value);
    }
    if (data.done) {
      console.log('portReader done');
      portReader.releaseLock();
      break;
    }
  }
}

async function transportReaderLoop(port, transport) {
  const encoder = new TextEncoder();
  while (true) {
    const line = await transport.read();
    console.log('ZML QUERY:', line);

    const writer = port.writable.getWriter();
    await writer.write(encoder.encode(`${line.trim()}\r\n`));
    writer.releaseLock();
  }
}

window.startWebSerial = async () => {
  const messageElement = document.getElementById('webserial-msg');
  messageElement.classList.remove('hidden');

  if ('serial' in navigator) {
    messageElement.innerText = 'WebSerial API is supported';

    const port = await navigator.serial.requestPort();
    await port.open({ baudRate: 115200 });

    const transport = zml.ascii.Transport.open();
    const connection = await zml.ascii.Connection.openCustom(transport);

    portReaderLoop(port, transport);
    transportReaderLoop(port, transport);

    window.conn = connection;
    console.log('WebSerial setup done. Reload page to release serial port');

    try {
      console.log('Generic command test:', await connection.genericCommand(''));
    } catch (e) {
      console.error('The device on the COM port does not appear to be a Zaber device\n', e);
    }
  } else {
    messageElement.innerText = 'WebSerial API is not supported on this browser';
  }
};
