const CYCLES = 5000;
const FIBER_COUNT = 12;

async function makeConnection() {
  const transport = zml.ascii.Transport.open();
  const connection = await zml.ascii.Connection.openCustom(transport);

  const stopSymbol = Symbol('stop');
  let stopPromiseResolve = null;
  const stopPromise = new Promise(resolve => stopPromiseResolve = resolve);

  const loop = async () => {
    while (true) {
      const input = await Promise.race([transport.read(), stopPromise]);
      if (input === stopSymbol) {
        await transport.close();
        console.log('Read loop stopped');
        return;
      }

      const id = input.match(/\d+ \d+ (\d+)/)[1];
      transport.write(`@1 0 ${id} OK IDLE -- 0`);
    }
  };
  const loopPromise = loop();

  return {
    connection,
    stopReadLoop: async () => {
      stopPromiseResolve(stopSymbol);
      await loopPromise;
    },
  };
}

async function measureSpeed(id) {
  const { connection, stopReadLoop } = await makeConnection();

  const values = [];
  for (let i = 0; i < CYCLES; ++i) {
    const ts = performance.now();
    await connection.genericCommand('');
    const cost = performance.now() - ts;
    values.push(cost);
  }

  await stopReadLoop();
  await connection.close();

  const sum = values.reduce((total, num) => total + num);

  const avg = sum / CYCLES;
  return avg;
}

window.performanceTest = async () => {
  for (let fibers = 1; fibers <= FIBER_COUNT; fibers++) {
    const arr = [];
    for (let i = 1; i <= fibers; i++) {
      arr[i - 1] = i;
    }
    const promises = arr.map(async id => measureSpeed(id));
    const results = await Promise.all(promises);

    const sum = results.reduce((total, num) => total + num);
    const totalAvg = sum / fibers;

    console.log(`Avg Time for Generic Command (WebSocket, ${fibers} threads, ${CYCLES} cycles each): ${totalAvg}`);
  }
};
