window.wasmCustomTransportTest = async () => {
  const transport = zml.ascii.Transport.open();
  const connection = await zml.ascii.Connection.openCustom(transport);

  connection.genericCommand('').then(response => console.log('Generic command response', response));

  const input = await transport.read();
  console.log('Transport read:', input);
  transport.write('@1 0 0 OK IDLE -- 0');

  // If we do not call `transport.read()` then the `genericCommand()` call above never resolves
  // because custom transports use this as an acknowledgement of the previous read being processed
  // and need this for error handling
  await Promise.race([
    transport.read(),
    new Promise(res => setTimeout(res, 1))
  ]);

  await transport.close();
  await connection.close();
  console.log('Transport and connection closed');
};
