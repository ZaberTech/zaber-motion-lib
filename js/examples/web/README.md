# Test app for ZML on browsers

- This little webpage allows the user to verify that ZML can be used on the browser
  - Install and build the dependencies
  
    ```
    cd ..
    npx gulp --series dependencies gen_code build_go_wasm build_js
    ```
  - `cd` back into this directory and run `npm start`
- Running ZML on browsers relies on the `ascii.Connection.openCustom()` functionality
  - Serial port and TCP/IP device connections are not supported on the web
  - This only works for ascii communications for now
