
import { Library, LogOutputMode } from '../src';
import { AxisGroup, Connection } from '../src/ascii';

(async () => {
  Library.setLogOutput(LogOutputMode.STDOUT);

  const conn2 = await Connection.openTcp('localhost', 11321);
  const conn = await Connection.openSerialPort('COM1');
  try {
    const devs1 = await conn.detectDevices();
    const devs2 = await conn2.detectDevices();
    const devs = [...devs1, ...devs2];

    const axes = new AxisGroup(devs.map(dev => dev.getAxis(1)));
    await axes.home();
    console.log('done!');
  } finally {
    await conn.close();
    await conn2.close();
  }
})().catch(console.log);
