
import { Library, LogOutputMode, DeviceDbSourceType, Velocity, Length } from '../src';
import { Connection } from '../src/ascii';
import { Translator } from '../src/gcode';

(async () => {
  Library.enableDeviceDbStore();
  Library.setLogOutput(LogOutputMode.STDOUT);
  Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'https://api.zaber.io/device-db/master');

  const conn = await Connection.openTcp('localhost', 11321);
  try {
    const [device] = await conn.detectDevices();

    await device.allAxes.home();

    const stream = device.streams.getStream(1);
    await stream.setupLive(1, 2);

    const translator = await Translator.setup(stream, {
      axisMappings: [{
        axisIndex: 0, axisLetter: 'Y',
      }, {
        axisIndex: 1, axisLetter: 'X',
      }]
    });

    await translator.translate('G28 Y10');
    await translator.translate('G0 X10 Y20');
    await translator.flush();

    const y = translator.getAxisPosition('Y', Length.mm);
    translator.setAxisPosition('Y', y * 2, Length.mm);

    translator.setTraverseRate(3, Velocity['mm/s']);

    await translator.translate('G0 X10 Y20');

    await translator.flush();
    await stream.disable();
  } finally {
    await conn.close();
  }
})().catch(console.log);
