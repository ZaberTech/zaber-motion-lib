
import { Library, LogOutputMode, Length, DeviceDbSourceType, RotationDirection, Acceleration, Velocity, Time } from '../src';
import { Connection, DigitalOutputAction } from '../src/ascii';

(async () => {
  Library.enableDeviceDbStore();
  Library.setLogOutput(LogOutputMode.STDOUT);
  Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'https://api.zaber.io/device-db/master');

  const conn = await Connection.openTcp('localhost', Connection.TCP_PORT_DEVICE_ONLY);
  try {
    const [device] = await conn.detectDevices();

    await device.allAxes.home();

    const numStreams = await device.settings.get('stream.numstreams');
    console.log(`Number of streams possible: ${numStreams}`);

    const stream = device.streams.getStream(1);

    const streamBuffer = device.streams.getBuffer(1);
    await streamBuffer.erase();

    await stream.setupStore(streamBuffer, 1, 2);

    await stream.lineAbsolute({
      value: 29.0047, unit: Length.mm,
    }, {
      value: 40.49, unit: Length.mm,
    });

    await stream.lineRelative({
      value: 0,
    }, {
      value: 50.5, unit: Length.mm,
    });

    const pathInCm = [[0.00, 3.00], [2.25, 7.10], [5.35, 0.15], [1.45, 10.20], [9.00, 9.00]];
    for (const point of pathInCm) {
      await stream.lineAbsolute(
        { value: point[0], unit: Length.cm },
        { value: point[1], unit: Length.cm },
      );
    }

    console.log('Stream buffer content:');
    console.log(await streamBuffer.getContent());

    await stream.disable();

    await stream.setupLive(1, 2);

    await stream.call(streamBuffer);

    const circleCenterAbs = [
      { value: 2, unit: Length.cm },
      { value: 4, unit: Length.cm },
    ];
    await stream.circleAbsolute(RotationDirection.CW, circleCenterAbs[0], circleCenterAbs[1]);

    const circleCenterRel = [
      { value: -2, unit: Length.cm },
      { value: 0, unit: Length.cm },
    ];
    await stream.circleRelative(RotationDirection.CCW, circleCenterRel[0], circleCenterRel[1]);

    const arcCircleCenterRel = [
      { value: -2, unit: Length.cm },
      { value: 0, unit: Length.cm },
    ];
    const arcEndRel = [
      { value: -2, unit: Length.cm },
      { value: 0, unit: Length.cm },
    ];
    await stream.arcRelative(
      RotationDirection.CCW,
      arcCircleCenterRel[0], arcCircleCenterRel[1],
      arcEndRel[0], arcEndRel[1],
    );

    const arcCircleCenterAbs = [
      { value: 2, unit: Length.cm },
      { value: 4, unit: Length.cm },
    ];
    const arcEndAbs = [
      { value: 4, unit: Length.cm },
      { value: 4, unit: Length.cm },
    ];
    await stream.arcAbsolute(
      RotationDirection.CW,
      arcCircleCenterAbs[0], arcCircleCenterAbs[1],
      arcEndAbs[0], arcEndAbs[1],
    );

    await stream.lineAbsoluteOn([1], [{ value: 1 }]);

    await stream.setMaxCentripetalAcceleration(5, Acceleration.CENTIMETRES_PER_SECOND_SQUARED);
    await stream.setMaxTangentialAcceleration(5, Acceleration.CENTIMETRES_PER_SECOND_SQUARED);
    await stream.setMaxSpeed(0.5, Velocity.MILLIMETRES_PER_SECOND);

    await stream.wait(2, Time.SECONDS);

    await stream.setDigitalOutput(1, DigitalOutputAction.ON);
    await stream.waitDigitalInput(1, true);

    await stream.setDigitalOutput(1, DigitalOutputAction.TOGGLE);
    await stream.setDigitalOutput(1, DigitalOutputAction.TOGGLE);

    await stream.setAnalogOutput(1, 0.42);
    await stream.waitAnalogInput(1, '>=', 0.50);

    await stream.waitUntilIdle();

    console.log(stream.toString());
    console.log(stream.axes);
    console.log(await stream.getMaxSpeed(Velocity.CENTIMETRES_PER_SECOND));
    console.log(await stream.getMaxTangentialAcceleration(Acceleration.CENTIMETRES_PER_SECOND_SQUARED));
    console.log(await stream.getMaxCentripetalAcceleration(Acceleration.CENTIMETRES_PER_SECOND_SQUARED));

    await stream.cork();

    await stream.uncork();

    if (await stream.isBusy()) {
      await stream.waitUntilIdle();
    }

    await stream.disable();
  } finally {
    await conn.close();
  }
})().catch(console.log);
