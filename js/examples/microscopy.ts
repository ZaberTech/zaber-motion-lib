
import { LogOutputMode, Library } from '../src';
import { Connection } from '../src/ascii';
import { Microscope } from '../src/microscopy';

(async () => {
  Library.setLogOutput(LogOutputMode.STDOUT);

  const conn = await Connection.openNetworkShare('cs-mvr', Connection.NETWORK_SHARE_PORT, 'COM1');
  // const conn = await Connection.openSerialPort('COM1');
  try {
    const all = await conn.detectDevices();
    console.log(all.map(d => `${d.deviceAddress}: ${d.toString()}`));

    const microscope = await Microscope.find(conn);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    console.log((microscope as any)._config);

    await microscope.initialize();

    if (microscope.illuminator) {
      const channel = microscope.illuminator.getChannel(1);
      console.log(channel.toString());
      await channel.setIntensity(0.3);
      console.log('intensity', await channel.getIntensity());
      await channel.on();
      await channel.off();
    }

    if (microscope.filterChanger) {
      console.log(await microscope.filterChanger.getNumberOfFilters());
      await microscope.filterChanger.change(1);
      await microscope.filterChanger.change(2);
      console.log('turret', await microscope.filterChanger.getCurrentFilter());
    }

    if (microscope.objectiveChanger) {
      for (let i = 0; i < 4; i++) {
        await microscope.objectiveChanger.change(i + 1);
      }
    }

    if (microscope.focusAxis) {
      const pos = await microscope.focusAxis.getPosition();
      console.log('focus', pos);
      await microscope.focusAxis.moveMax();
      await microscope.focusAxis.moveAbsolute(pos);
    }

    console.log('x', await microscope.xAxis?.getPosition());
    console.log('y', await microscope.yAxis?.getPosition());
  } finally {
    await conn.close();
  }
})().catch(console.log);
