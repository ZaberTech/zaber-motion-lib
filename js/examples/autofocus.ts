
import readline from 'node:readline';

import { LogOutputMode, Library } from '../src';
import { Connection } from '../src/ascii';
import { Microscope, WdiAutofocusProvider } from '../src/microscopy';

const terminal = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

async function ask(question: string, signal?: AbortSignal): Promise<string> {
  return new Promise(resolve => terminal.question(question, { signal }, resolve));
}

const HOST = 'mech-tdicamera';
const PORT = 'COM5';

function withConnection<T>(fn: (connection: Connection) => Promise<T>): Promise<T> {
  return Connection
    .openNetworkShare(HOST, Connection.NETWORK_SHARE_PORT, PORT)
    .then(connection => fn(connection).finally(() => connection.close()));
}

function withWdi<T>(fn: (connection: WdiAutofocusProvider) => Promise<T>): Promise<T> {
  return WdiAutofocusProvider.openTcp('mech-tdicamera').then(connection => fn(connection).finally(() => connection.close()));
}

(async () => {
  Library.setLogOutput(LogOutputMode.STDOUT);

  await withWdi(async wdi => {
    await withConnection(async connection => {
      await connection.detectDevices();

      const microscope = await Microscope.find(connection, { autofocus: wdi.providerId });
      console.log(microscope.toString());

      const autofocus = microscope.autofocus!;

      for (;;) {
        const resp = await ask('Command: ');
        switch (resp) {
          case 'focus':
            await autofocus.focusOnce();
            break;
          case 'zero':
            await autofocus.setFocusZero();
            break;
          case 'loop':
            {
              const abort = new AbortController();
              await autofocus.startFocusLoop();
              await Promise.race([
                ask('Press Enter to stop...', abort.signal),
                autofocus.focusAxis.waitUntilIdle({ throwErrorOnFault: false }),
              ]);
              abort.abort();
              await autofocus.stopFocusLoop();
            }
            break;
          case 'generic': {
            let data = await wdi.genericRead(54, 2);
            console.log(data[0]);
            data = await wdi.genericRead(54, 2, { offset: 1 });
            console.log(data[0]);
            data = await wdi.genericRead(78, 2, { count: 16 });
            console.log(data);
          } break;
          case 'reset':
            await autofocus.synchronizeParameters();
            break;
          case 'get_params':
            console.log(await autofocus.getObjectiveParameters(1));
            console.log(await autofocus.getObjectiveParameters(2));
            break;
          case 'exit':
            return;
          default: {
            const position = (await wdi.genericRead(41, 4, { registerBank: 't' }))[0] / 1024.0;
            console.log(position);
            console.log(await autofocus.getStatus());
            break;
          }
        }
      }
    });
  });
})().catch(err => {
  console.error(err);
  process.exit(1);
}).finally(() => {
  terminal.close();
});
