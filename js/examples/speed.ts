import { Connection } from '../src/ascii';
import { performance } from 'perf_hooks';
import _ from 'lodash';

const CYCLES = 10000;
const FIBER_COUNT = 12;

(async () => {
  for (let fibers = 1; fibers <= FIBER_COUNT; fibers++) {
    const promises = _.range(1, fibers + 1).map(() => measureSpeed());

    const results = await Promise.all(promises);

    const totalAvg = _.sum(results) / results.length;
    console.log(`${fibers}: ${totalAvg}`);
  }
})().catch(console.log);

async function measureSpeed(): Promise<number> {
  const conn = await Connection.openTcp('127.0.0.1', 11234);

  const values = [];
  for (let i = 0; i < CYCLES; ++i) {
    const ts = performance.now();

    await conn.genericCommand('');

    const cost = performance.now() - ts;

    values.push(cost);
  }

  await conn.close();

  const avg = _.sum(values) / CYCLES;

  return avg;
}
