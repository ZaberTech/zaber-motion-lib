
import { Library, LogOutputMode, Length, Velocity } from '../src';
import { Connection } from '../src/binary';

(async () => {
  Library.setLogOutput(LogOutputMode.STDOUT);

  const conn = await Connection.openSerialPort('/dev/ttyUSB0');
  try {
    const [dev] = await conn.detectDevices();
    console.log(`Device ${dev.deviceAddress} has device ID ${dev.identity.deviceId}.`);

    let pos = await dev.home();
    console.log(`Position after home: ${pos} microsteps.`);

    pos = await dev.moveAbsolute(1.0, Length.cm);
    console.log(`Position after move absolute: ${pos} cm.`);

    pos = await dev.moveRelative(5.0, Length.mm);
    console.log(`Position after move relative: ${pos} mm.`);

    const velocity = await dev.moveVelocity(1.0, Velocity['mm/s']);
    console.log(`Starting move velocity with speed: ${velocity} mm/s.`);

    await new Promise(r => setTimeout(r, 2000));
    pos = await dev.stop(Length.mm);
    console.log(`Position after stop: ${pos} mm.`);

    console.log(`Final position in microsteps: ${await dev.getPosition()}.`);
  } finally {
    await conn.close();
  }
})().catch(console.log);
