/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from './gateway/sanitizer';
import { Units } from './units';

/**
 * Represents a numerical value with optional units specified.
 */
export interface Measurement {
  /**
   * Value of the measurement.
   */
  value: number;

  /**
   * Optional units of the measurement.
   */
  unit?: (Units | null);

}

export const Measurement = {
  fromBinary: (buffer: Uint8Array): Measurement => BSON.deserialize(buffer) as Measurement,
  toBinary: (value: Measurement): Uint8Array => BSON.serialize(Measurement.sanitize(value)),
  DEFAULT: Object.freeze({
    value: 0,
    unit: null,
  }) as Readonly<Measurement>,
  sanitize: (value: Measurement): Measurement => {
    if (value == null) { throw new TypeError('Expected Measurement object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected Measurement object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: value.unit != null ? sanitizer.sanitizeUnits(value.unit, 'unit') : null,
    };
  },
};
