﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { PvtMovementInterruptedExceptionData } from './pvt_movement_interrupted_exception_data';

/**
 * Thrown when ongoing PVT movement is interrupted by another command or user input.
 */
export class PvtMovementInterruptedException extends MotionLibException {
  /**
   * Additional data for PvtMovementInterruptedException
   */
  public get details(): PvtMovementInterruptedExceptionData {
    return this._details;
  }
  private _details: PvtMovementInterruptedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | PvtMovementInterruptedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, PvtMovementInterruptedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = PvtMovementInterruptedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
