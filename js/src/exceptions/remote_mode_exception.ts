﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { CommandFailedException } from './command_failed_exception';
import { CommandFailedExceptionData } from './command_failed_exception_data';

/**
 * Thrown when a command is rejected because the device is in EtherCAT Control (remote) mode.
 */
export class RemoteModeException extends CommandFailedException {
  constructor(
    message: string,
    customData: Uint8Array | CommandFailedExceptionData
  ) {
    super(message, customData);
    Object.setPrototypeOf(this, RemoteModeException.prototype);
  }
}
