/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for StreamExecutionException.
 */
export interface StreamExecutionExceptionData {
  /**
   * The error flag that caused the exception.
   */
  errorFlag: string;

  /**
   * The reason for the exception.
   */
  reason: string;

}

export const StreamExecutionExceptionData = {
  fromBinary: (buffer: Uint8Array): StreamExecutionExceptionData => BSON.deserialize(buffer) as StreamExecutionExceptionData,
  toBinary: (value: StreamExecutionExceptionData): Uint8Array => BSON.serialize(StreamExecutionExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    errorFlag: '',
    reason: '',
  }) as Readonly<StreamExecutionExceptionData>,
  sanitize: (value: StreamExecutionExceptionData): StreamExecutionExceptionData => {
    if (value == null) { throw new TypeError('Expected StreamExecutionExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamExecutionExceptionData object but got ${typeof value}.`) }
    return {
      errorFlag: sanitizer.sanitizeString(value.errorFlag, 'errorFlag'),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
