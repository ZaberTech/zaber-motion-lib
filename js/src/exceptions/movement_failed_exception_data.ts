/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for MovementFailedException.
 */
export interface MovementFailedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

  /**
   * The address of the device that performed the failed movement.
   */
  device: number;

  /**
   * The number of the axis that performed the failed movement.
   */
  axis: number;

}

export const MovementFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): MovementFailedExceptionData => BSON.deserialize(buffer) as MovementFailedExceptionData,
  toBinary: (value: MovementFailedExceptionData): Uint8Array => BSON.serialize(MovementFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
    device: 0,
    axis: 0,
  }) as Readonly<MovementFailedExceptionData>,
  sanitize: (value: MovementFailedExceptionData): MovementFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected MovementFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MovementFailedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
    };
  },
};
