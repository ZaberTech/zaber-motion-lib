﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { CommandTooLongExceptionData } from './command_too_long_exception_data';

/**
 * Thrown when a command is too long to be written by the ASCII protocol, even when continued across multiple lines.
 */
export class CommandTooLongException extends MotionLibException {
  /**
   * Additional data for CommandTooLongException
   */
  public get details(): CommandTooLongExceptionData {
    return this._details;
  }
  private _details: CommandTooLongExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | CommandTooLongExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, CommandTooLongException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = CommandTooLongExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
