/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for GCodeExecutionException.
 */
export interface GCodeExecutionExceptionData {
  /**
   * The index in the block string that caused the exception.
   */
  fromBlock: number;

  /**
   * The end index in the block string that caused the exception.
   * The end index is exclusive.
   */
  toBlock: number;

}

export const GCodeExecutionExceptionData = {
  fromBinary: (buffer: Uint8Array): GCodeExecutionExceptionData => BSON.deserialize(buffer) as GCodeExecutionExceptionData,
  toBinary: (value: GCodeExecutionExceptionData): Uint8Array => BSON.serialize(GCodeExecutionExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    fromBlock: 0,
    toBlock: 0,
  }) as Readonly<GCodeExecutionExceptionData>,
  sanitize: (value: GCodeExecutionExceptionData): GCodeExecutionExceptionData => {
    if (value == null) { throw new TypeError('Expected GCodeExecutionExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GCodeExecutionExceptionData object but got ${typeof value}.`) }
    return {
      fromBlock: sanitizer.sanitizeInt(value.fromBlock, 'fromBlock'),
      toBlock: sanitizer.sanitizeInt(value.toBlock, 'toBlock'),
    };
  },
};
