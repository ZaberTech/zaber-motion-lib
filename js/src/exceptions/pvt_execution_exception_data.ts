/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { InvalidPvtPoint } from '../exceptions/invalid_pvt_point';

/**
 * Contains additional data for PvtExecutionException.
 */
export interface PvtExecutionExceptionData {
  /**
   * The error flag that caused the exception.
   */
  errorFlag: string;

  /**
   * The reason for the exception.
   */
  reason: string;

  /**
   * A list of points that cause the error (if applicable).
   */
  invalidPoints: InvalidPvtPoint[];

}

export const PvtExecutionExceptionData = {
  fromBinary: (buffer: Uint8Array): PvtExecutionExceptionData => BSON.deserialize(buffer) as PvtExecutionExceptionData,
  toBinary: (value: PvtExecutionExceptionData): Uint8Array => BSON.serialize(PvtExecutionExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    errorFlag: '',
    reason: '',
    invalidPoints: [],
  }) as Readonly<PvtExecutionExceptionData>,
  sanitize: (value: PvtExecutionExceptionData): PvtExecutionExceptionData => {
    if (value == null) { throw new TypeError('Expected PvtExecutionExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PvtExecutionExceptionData object but got ${typeof value}.`) }
    return {
      errorFlag: sanitizer.sanitizeString(value.errorFlag, 'errorFlag'),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
      invalidPoints: Array.from(value.invalidPoints ?? [], item => InvalidPvtPoint.sanitize(item)),
    };
  },
};
