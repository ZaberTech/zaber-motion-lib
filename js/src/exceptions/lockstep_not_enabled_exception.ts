﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when an operation cannot be performed because lockstep motion is not enabled.
 */
export class LockstepNotEnabledException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, LockstepNotEnabledException.prototype);
  }
}
