﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { StreamMovementFailedExceptionData } from './stream_movement_failed_exception_data';

/**
 * Thrown when a device registers a fault during streamed movement.
 */
export class StreamMovementFailedException extends MotionLibException {
  /**
   * Additional data for StreamMovementFailedException
   */
  public get details(): StreamMovementFailedExceptionData {
    return this._details;
  }
  private _details: StreamMovementFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | StreamMovementFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, StreamMovementFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = StreamMovementFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
