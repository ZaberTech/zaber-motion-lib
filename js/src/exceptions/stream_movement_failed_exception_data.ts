/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for StreamMovementFailedException.
 */
export interface StreamMovementFailedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

}

export const StreamMovementFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): StreamMovementFailedExceptionData => BSON.deserialize(buffer) as StreamMovementFailedExceptionData,
  toBinary: (value: StreamMovementFailedExceptionData): Uint8Array => BSON.serialize(StreamMovementFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
  }) as Readonly<StreamMovementFailedExceptionData>,
  sanitize: (value: StreamMovementFailedExceptionData): StreamMovementFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected StreamMovementFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamMovementFailedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
