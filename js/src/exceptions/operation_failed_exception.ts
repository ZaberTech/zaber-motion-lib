﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { OperationFailedExceptionData } from './operation_failed_exception_data';

/**
 * Thrown when a non-motion device fails to perform a requested operation.
 */
export class OperationFailedException extends MotionLibException {
  /**
   * Additional data for OperationFailedException
   */
  public get details(): OperationFailedExceptionData {
    return this._details;
  }
  private _details: OperationFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | OperationFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, OperationFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = OperationFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
