/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for MovementInterruptedException.
 */
export interface MovementInterruptedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

  /**
   * The address of the device that caused the interruption.
   */
  device: number;

  /**
   * The number of the axis that caused the interruption.
   */
  axis: number;

}

export const MovementInterruptedExceptionData = {
  fromBinary: (buffer: Uint8Array): MovementInterruptedExceptionData => BSON.deserialize(buffer) as MovementInterruptedExceptionData,
  toBinary: (value: MovementInterruptedExceptionData): Uint8Array => BSON.serialize(MovementInterruptedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
    device: 0,
    axis: 0,
  }) as Readonly<MovementInterruptedExceptionData>,
  sanitize: (value: MovementInterruptedExceptionData): MovementInterruptedExceptionData => {
    if (value == null) { throw new TypeError('Expected MovementInterruptedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MovementInterruptedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
    };
  },
};
