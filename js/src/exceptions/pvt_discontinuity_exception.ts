﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when a PVT sequence encounters discontinuity and interrupts the sequence.
 */
export class PvtDiscontinuityException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, PvtDiscontinuityException.prototype);
  }
}
