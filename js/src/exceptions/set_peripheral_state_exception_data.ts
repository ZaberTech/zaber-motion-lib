/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for a SetPeripheralStateFailedException.
 */
export interface SetPeripheralStateExceptionData {
  /**
   * The number of axis where the exception originated.
   */
  axisNumber: number;

  /**
   * A list of settings which could not be set.
   */
  settings: string[];

  /**
   * The reason servo tuning could not be set.
   */
  servoTuning: string;

  /**
   * The reasons stored positions could not be set.
   */
  storedPositions: string[];

  /**
   * The reasons storage could not be set.
   */
  storage: string[];

}

export const SetPeripheralStateExceptionData = {
  fromBinary: (buffer: Uint8Array): SetPeripheralStateExceptionData => BSON.deserialize(buffer) as SetPeripheralStateExceptionData,
  toBinary: (value: SetPeripheralStateExceptionData): Uint8Array => BSON.serialize(SetPeripheralStateExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    axisNumber: 0,
    settings: [],
    servoTuning: '',
    storedPositions: [],
    storage: [],
  }) as Readonly<SetPeripheralStateExceptionData>,
  sanitize: (value: SetPeripheralStateExceptionData): SetPeripheralStateExceptionData => {
    if (value == null) { throw new TypeError('Expected SetPeripheralStateExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetPeripheralStateExceptionData object but got ${typeof value}.`) }
    return {
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      settings: Array.from(value.settings ?? [], item => sanitizer.sanitizeString(item, 'items of settings')),
      servoTuning: sanitizer.sanitizeString(value.servoTuning, 'servoTuning'),
      storedPositions: Array.from(value.storedPositions ?? [], item => sanitizer.sanitizeString(item, 'items of storedPositions')),
      storage: Array.from(value.storage ?? [], item => sanitizer.sanitizeString(item, 'items of storage')),
    };
  },
};
