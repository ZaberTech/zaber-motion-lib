/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for the InvalidPacketException.
 */
export interface InvalidPacketExceptionData {
  /**
   * The invalid packet that caused the exception.
   */
  packet: string;

  /**
   * The reason for the exception.
   */
  reason: string;

}

export const InvalidPacketExceptionData = {
  fromBinary: (buffer: Uint8Array): InvalidPacketExceptionData => BSON.deserialize(buffer) as InvalidPacketExceptionData,
  toBinary: (value: InvalidPacketExceptionData): Uint8Array => BSON.serialize(InvalidPacketExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    packet: '',
    reason: '',
  }) as Readonly<InvalidPacketExceptionData>,
  sanitize: (value: InvalidPacketExceptionData): InvalidPacketExceptionData => {
    if (value == null) { throw new TypeError('Expected InvalidPacketExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected InvalidPacketExceptionData object but got ${typeof value}.`) }
    return {
      packet: sanitizer.sanitizeString(value.packet, 'packet'),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
