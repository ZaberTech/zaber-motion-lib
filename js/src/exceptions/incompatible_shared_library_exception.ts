﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when the loaded shared library is incompatible with the running code.
 * Typically caused by mixed library binary files. Reinstall the library.
 */
export class IncompatibleSharedLibraryException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, IncompatibleSharedLibraryException.prototype);
  }
}
