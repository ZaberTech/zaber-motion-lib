﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { PvtMovementFailedExceptionData } from './pvt_movement_failed_exception_data';

/**
 * Thrown when a device registers a fault during PVT movement.
 */
export class PvtMovementFailedException extends MotionLibException {
  /**
   * Additional data for PvtMovementFailedException
   */
  public get details(): PvtMovementFailedExceptionData {
    return this._details;
  }
  private _details: PvtMovementFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | PvtMovementFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, PvtMovementFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = PvtMovementFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
