﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { CommandFailedExceptionData } from './command_failed_exception_data';

/**
 * Thrown when a device rejects a command.
 */
export class CommandFailedException extends MotionLibException {
  /**
   * Additional data for CommandFailedException
   */
  public get details(): CommandFailedExceptionData {
    return this._details;
  }
  private _details: CommandFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | CommandFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, CommandFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = CommandFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
