﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { CommandFailedException } from './command_failed_exception';
import { CommandFailedExceptionData } from './command_failed_exception_data';

/**
 * Thrown when a parameter of a command is judged to be out of range by the receiving device.
 */
export class BadDataException extends CommandFailedException {
  constructor(
    message: string,
    customData: Uint8Array | CommandFailedExceptionData
  ) {
    super(message, customData);
    Object.setPrototypeOf(this, BadDataException.prototype);
  }
}
