/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for BinaryCommandFailedException.
 */
export interface BinaryCommandFailedExceptionData {
  /**
   * The response data.
   */
  responseData: number;

}

export const BinaryCommandFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): BinaryCommandFailedExceptionData => BSON.deserialize(buffer) as BinaryCommandFailedExceptionData,
  toBinary: (value: BinaryCommandFailedExceptionData): Uint8Array => BSON.serialize(BinaryCommandFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    responseData: 0,
  }) as Readonly<BinaryCommandFailedExceptionData>,
  sanitize: (value: BinaryCommandFailedExceptionData): BinaryCommandFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected BinaryCommandFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryCommandFailedExceptionData object but got ${typeof value}.`) }
    return {
      responseData: sanitizer.sanitizeInt(value.responseData, 'responseData'),
    };
  },
};
