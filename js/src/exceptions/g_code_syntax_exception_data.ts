/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for GCodeSyntaxException.
 */
export interface GCodeSyntaxExceptionData {
  /**
   * The index in the block string that caused the exception.
   */
  fromBlock: number;

  /**
   * The end index in the block string that caused the exception.
   * The end index is exclusive.
   */
  toBlock: number;

}

export const GCodeSyntaxExceptionData = {
  fromBinary: (buffer: Uint8Array): GCodeSyntaxExceptionData => BSON.deserialize(buffer) as GCodeSyntaxExceptionData,
  toBinary: (value: GCodeSyntaxExceptionData): Uint8Array => BSON.serialize(GCodeSyntaxExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    fromBlock: 0,
    toBlock: 0,
  }) as Readonly<GCodeSyntaxExceptionData>,
  sanitize: (value: GCodeSyntaxExceptionData): GCodeSyntaxExceptionData => {
    if (value == null) { throw new TypeError('Expected GCodeSyntaxExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GCodeSyntaxExceptionData object but got ${typeof value}.`) }
    return {
      fromBlock: sanitizer.sanitizeInt(value.fromBlock, 'fromBlock'),
      toBlock: sanitizer.sanitizeInt(value.toBlock, 'toBlock'),
    };
  },
};
