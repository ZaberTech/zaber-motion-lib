﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { GCodeExecutionExceptionData } from './g_code_execution_exception_data';

/**
 * Thrown when a block of G-Code cannot be executed.
 */
export class GCodeExecutionException extends MotionLibException {
  /**
   * Additional data for GCodeExecutionException
   */
  public get details(): GCodeExecutionExceptionData {
    return this._details;
  }
  private _details: GCodeExecutionExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | GCodeExecutionExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, GCodeExecutionException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = GCodeExecutionExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
