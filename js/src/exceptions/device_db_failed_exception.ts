﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { DeviceDbFailedExceptionData } from './device_db_failed_exception_data';

/**
 * Thrown when device information cannot be retrieved from the device database.
 */
export class DeviceDbFailedException extends MotionLibException {
  /**
   * Additional data for DeviceDbFailedException
   */
  public get details(): DeviceDbFailedExceptionData {
    return this._details;
  }
  private _details: DeviceDbFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | DeviceDbFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, DeviceDbFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = DeviceDbFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
