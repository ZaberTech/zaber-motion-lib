export class MotionLibException extends Error {
  /**
   * Error message of the exception.
   */
  public readonly message: string;

  constructor(
    message: string
  ) {
    super();
    Object.setPrototypeOf(this, MotionLibException.prototype);

    this.message = message;
  }

  public toString(): string {
    return `${this.constructor.name}: ${this.message}`;
  }
}
