﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when trying to access a key that has not been set.
 */
export class NoValueForKeyException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, NoValueForKeyException.prototype);
  }
}
