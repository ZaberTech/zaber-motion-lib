﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { SetDeviceStateExceptionData } from './set_device_state_exception_data';

/**
 * Thrown when a device cannot be set to the supplied state.
 */
export class SetDeviceStateFailedException extends MotionLibException {
  /**
   * Additional data for SetDeviceStateFailedException
   */
  public get details(): SetDeviceStateExceptionData {
    return this._details;
  }
  private _details: SetDeviceStateExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | SetDeviceStateExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, SetDeviceStateFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = SetDeviceStateExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
