﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { MovementFailedExceptionData } from './movement_failed_exception_data';

/**
 * Thrown when a device registers a fault during movement.
 */
export class MovementFailedException extends MotionLibException {
  /**
   * Additional data for MovementFailedException
   */
  public get details(): MovementFailedExceptionData {
    return this._details;
  }
  private _details: MovementFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | MovementFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, MovementFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = MovementFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
