/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for CommandFailedException.
 */
export interface CommandFailedExceptionData {
  /**
   * The command that got rejected.
   */
  command: string;

  /**
   * The data from the reply containing the rejection reason.
   */
  responseData: string;

  /**
   * The flag indicating that the command was rejected.
   */
  replyFlag: string;

  /**
   * The current device or axis status.
   */
  status: string;

  /**
   * The highest priority warning flag on the device or axis.
   */
  warningFlag: string;

  /**
   * The address of the device that rejected the command.
   */
  deviceAddress: number;

  /**
   * The number of the axis which the rejection relates to.
   */
  axisNumber: number;

  /**
   * The message ID of the reply.
   */
  id: number;

}

export const CommandFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): CommandFailedExceptionData => BSON.deserialize(buffer) as CommandFailedExceptionData,
  toBinary: (value: CommandFailedExceptionData): Uint8Array => BSON.serialize(CommandFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    command: '',
    responseData: '',
    replyFlag: '',
    status: '',
    warningFlag: '',
    deviceAddress: 0,
    axisNumber: 0,
    id: 0,
  }) as Readonly<CommandFailedExceptionData>,
  sanitize: (value: CommandFailedExceptionData): CommandFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected CommandFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CommandFailedExceptionData object but got ${typeof value}.`) }
    return {
      command: sanitizer.sanitizeString(value.command, 'command'),
      responseData: sanitizer.sanitizeString(value.responseData, 'responseData'),
      replyFlag: sanitizer.sanitizeString(value.replyFlag, 'replyFlag'),
      status: sanitizer.sanitizeString(value.status, 'status'),
      warningFlag: sanitizer.sanitizeString(value.warningFlag, 'warningFlag'),
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      id: sanitizer.sanitizeInt(value.id, 'id'),
    };
  },
};
