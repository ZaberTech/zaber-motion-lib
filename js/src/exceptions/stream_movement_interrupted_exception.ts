﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { StreamMovementInterruptedExceptionData } from './stream_movement_interrupted_exception_data';

/**
 * Thrown when ongoing stream movement is interrupted by another command or user input.
 */
export class StreamMovementInterruptedException extends MotionLibException {
  /**
   * Additional data for StreamMovementInterruptedException
   */
  public get details(): StreamMovementInterruptedExceptionData {
    return this._details;
  }
  private _details: StreamMovementInterruptedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | StreamMovementInterruptedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, StreamMovementInterruptedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = StreamMovementInterruptedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
