/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for InvalidResponseException.
 */
export interface InvalidResponseExceptionData {
  /**
   * The response data.
   */
  response: string;

}

export const InvalidResponseExceptionData = {
  fromBinary: (buffer: Uint8Array): InvalidResponseExceptionData => BSON.deserialize(buffer) as InvalidResponseExceptionData,
  toBinary: (value: InvalidResponseExceptionData): Uint8Array => BSON.serialize(InvalidResponseExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    response: '',
  }) as Readonly<InvalidResponseExceptionData>,
  sanitize: (value: InvalidResponseExceptionData): InvalidResponseExceptionData => {
    if (value == null) { throw new TypeError('Expected InvalidResponseExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected InvalidResponseExceptionData object but got ${typeof value}.`) }
    return {
      response: sanitizer.sanitizeString(value.response, 'response'),
    };
  },
};
