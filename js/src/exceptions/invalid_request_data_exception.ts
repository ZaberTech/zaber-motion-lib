﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Used for internal error handling.
 * Indicates passing values of incorrect type from scripting languages or mixed library binary files.
 */
export class InvalidRequestDataException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, InvalidRequestDataException.prototype);
  }
}
