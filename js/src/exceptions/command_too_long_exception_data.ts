/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Information describing why the command could not fit.
 */
export interface CommandTooLongExceptionData {
  /**
   * The part of the command that could be successfully fit in the space provided by the protocol.
   */
  fit: string;

  /**
   * The part of the command that could not fit within the space provided.
   */
  remainder: string;

  /**
   * The length of the ascii string that can be written to a single line.
   */
  packetSize: number;

  /**
   * The number of lines a command can be split over using continuations.
   */
  packetsMax: number;

}

export const CommandTooLongExceptionData = {
  fromBinary: (buffer: Uint8Array): CommandTooLongExceptionData => BSON.deserialize(buffer) as CommandTooLongExceptionData,
  toBinary: (value: CommandTooLongExceptionData): Uint8Array => BSON.serialize(CommandTooLongExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    fit: '',
    remainder: '',
    packetSize: 0,
    packetsMax: 0,
  }) as Readonly<CommandTooLongExceptionData>,
  sanitize: (value: CommandTooLongExceptionData): CommandTooLongExceptionData => {
    if (value == null) { throw new TypeError('Expected CommandTooLongExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CommandTooLongExceptionData object but got ${typeof value}.`) }
    return {
      fit: sanitizer.sanitizeString(value.fit, 'fit'),
      remainder: sanitizer.sanitizeString(value.remainder, 'remainder'),
      packetSize: sanitizer.sanitizeInt(value.packetSize, 'packetSize'),
      packetsMax: sanitizer.sanitizeInt(value.packetsMax, 'packetsMax'),
    };
  },
};
