/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { SetPeripheralStateExceptionData } from '../exceptions/set_peripheral_state_exception_data';

/**
 * Contains additional data for a SetDeviceStateFailedException.
 */
export interface SetDeviceStateExceptionData {
  /**
   * A list of settings which could not be set.
   */
  settings: string[];

  /**
   * The reason the stream buffers could not be set.
   */
  streamBuffers: string[];

  /**
   * The reason the pvt buffers could not be set.
   */
  pvtBuffers: string[];

  /**
   * The reason the triggers could not be set.
   */
  triggers: string[];

  /**
   * The reason servo tuning could not be set.
   */
  servoTuning: string;

  /**
   * The reasons stored positions could not be set.
   */
  storedPositions: string[];

  /**
   * The reasons storage could not be set.
   */
  storage: string[];

  /**
   * Errors for any peripherals that could not be set.
   */
  peripherals: SetPeripheralStateExceptionData[];

}

export const SetDeviceStateExceptionData = {
  fromBinary: (buffer: Uint8Array): SetDeviceStateExceptionData => BSON.deserialize(buffer) as SetDeviceStateExceptionData,
  toBinary: (value: SetDeviceStateExceptionData): Uint8Array => BSON.serialize(SetDeviceStateExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    settings: [],
    streamBuffers: [],
    pvtBuffers: [],
    triggers: [],
    servoTuning: '',
    storedPositions: [],
    storage: [],
    peripherals: [],
  }) as Readonly<SetDeviceStateExceptionData>,
  sanitize: (value: SetDeviceStateExceptionData): SetDeviceStateExceptionData => {
    if (value == null) { throw new TypeError('Expected SetDeviceStateExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetDeviceStateExceptionData object but got ${typeof value}.`) }
    return {
      settings: Array.from(value.settings ?? [], item => sanitizer.sanitizeString(item, 'items of settings')),
      streamBuffers: Array.from(value.streamBuffers ?? [], item => sanitizer.sanitizeString(item, 'items of streamBuffers')),
      pvtBuffers: Array.from(value.pvtBuffers ?? [], item => sanitizer.sanitizeString(item, 'items of pvtBuffers')),
      triggers: Array.from(value.triggers ?? [], item => sanitizer.sanitizeString(item, 'items of triggers')),
      servoTuning: sanitizer.sanitizeString(value.servoTuning, 'servoTuning'),
      storedPositions: Array.from(value.storedPositions ?? [], item => sanitizer.sanitizeString(item, 'items of storedPositions')),
      storage: Array.from(value.storage ?? [], item => sanitizer.sanitizeString(item, 'items of storage')),
      peripherals: Array.from(value.peripherals ?? [], item => SetPeripheralStateExceptionData.sanitize(item)),
    };
  },
};
