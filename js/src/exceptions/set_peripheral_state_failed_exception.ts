﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { SetPeripheralStateExceptionData } from './set_peripheral_state_exception_data';

/**
 * Thrown when an axis cannot be set to the supplied state.
 */
export class SetPeripheralStateFailedException extends MotionLibException {
  /**
   * Additional data for SetPeripheralStateFailedException
   */
  public get details(): SetPeripheralStateExceptionData {
    return this._details;
  }
  private _details: SetPeripheralStateExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | SetPeripheralStateExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, SetPeripheralStateFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = SetPeripheralStateExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
