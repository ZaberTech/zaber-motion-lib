﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { StreamExecutionExceptionData } from './stream_execution_exception_data';

/**
 * Thrown when a streamed motion fails.
 */
export class StreamExecutionException extends MotionLibException {
  /**
   * Additional data for StreamExecutionException
   */
  public get details(): StreamExecutionExceptionData {
    return this._details;
  }
  private _details: StreamExecutionExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | StreamExecutionExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, StreamExecutionException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = StreamExecutionExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
