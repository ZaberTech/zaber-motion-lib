﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { InvalidPacketExceptionData } from './invalid_packet_exception_data';

/**
 * Thrown when a packet from a device cannot be parsed.
 */
export class InvalidPacketException extends MotionLibException {
  /**
   * Additional data for InvalidPacketException
   */
  public get details(): InvalidPacketExceptionData {
    return this._details;
  }
  private _details: InvalidPacketExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | InvalidPacketExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, InvalidPacketException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = InvalidPacketExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
