﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { InvalidResponseExceptionData } from './invalid_response_exception_data';

/**
 * Thrown when a device sends a response with unexpected type or data.
 */
export class InvalidResponseException extends MotionLibException {
  /**
   * Additional data for InvalidResponseException
   */
  public get details(): InvalidResponseExceptionData {
    return this._details;
  }
  private _details: InvalidResponseExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | InvalidResponseExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, InvalidResponseException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = InvalidResponseExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
