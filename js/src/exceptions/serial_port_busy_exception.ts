﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { ConnectionFailedException } from './connection_failed_exception';

/**
 * Thrown when a serial port cannot be opened because it is in use by another application.
 */
export class SerialPortBusyException extends ConnectionFailedException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, SerialPortBusyException.prototype);
  }
}
