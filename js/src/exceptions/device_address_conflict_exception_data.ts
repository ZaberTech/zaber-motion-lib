/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for DeviceAddressConflictException.
 */
export interface DeviceAddressConflictExceptionData {
  /**
   * The full list of detected device addresses.
   */
  deviceAddresses: number[];

}

export const DeviceAddressConflictExceptionData = {
  fromBinary: (buffer: Uint8Array): DeviceAddressConflictExceptionData => BSON.deserialize(buffer) as DeviceAddressConflictExceptionData,
  toBinary: (value: DeviceAddressConflictExceptionData): Uint8Array => BSON.serialize(DeviceAddressConflictExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddresses: [],
  }) as Readonly<DeviceAddressConflictExceptionData>,
  sanitize: (value: DeviceAddressConflictExceptionData): DeviceAddressConflictExceptionData => {
    if (value == null) { throw new TypeError('Expected DeviceAddressConflictExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceAddressConflictExceptionData object but got ${typeof value}.`) }
    return {
      deviceAddresses: Array.from(value.deviceAddresses ?? [], item => sanitizer.sanitizeInt(item, 'items of deviceAddresses')),
    };
  },
};
