﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { PvtExecutionExceptionData } from './pvt_execution_exception_data';

/**
 * Thrown when a PVT sequence motion fails.
 */
export class PvtExecutionException extends MotionLibException {
  /**
   * Additional data for PvtExecutionException
   */
  public get details(): PvtExecutionExceptionData {
    return this._details;
  }
  private _details: PvtExecutionExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | PvtExecutionExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, PvtExecutionException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = PvtExecutionExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
