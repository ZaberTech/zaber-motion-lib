/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for StreamMovementInterruptedException.
 */
export interface StreamMovementInterruptedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

}

export const StreamMovementInterruptedExceptionData = {
  fromBinary: (buffer: Uint8Array): StreamMovementInterruptedExceptionData => BSON.deserialize(buffer) as StreamMovementInterruptedExceptionData,
  toBinary: (value: StreamMovementInterruptedExceptionData): Uint8Array => BSON.serialize(StreamMovementInterruptedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
  }) as Readonly<StreamMovementInterruptedExceptionData>,
  sanitize: (value: StreamMovementInterruptedExceptionData): StreamMovementInterruptedExceptionData => {
    if (value == null) { throw new TypeError('Expected StreamMovementInterruptedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamMovementInterruptedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
