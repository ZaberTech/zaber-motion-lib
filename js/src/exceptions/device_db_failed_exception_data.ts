/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for a DeviceDbFailedException.
 */
export interface DeviceDbFailedExceptionData {
  /**
   * Code describing type of the error.
   */
  code: string;

}

export const DeviceDbFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): DeviceDbFailedExceptionData => BSON.deserialize(buffer) as DeviceDbFailedExceptionData,
  toBinary: (value: DeviceDbFailedExceptionData): Uint8Array => BSON.serialize(DeviceDbFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    code: '',
  }) as Readonly<DeviceDbFailedExceptionData>,
  sanitize: (value: DeviceDbFailedExceptionData): DeviceDbFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected DeviceDbFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceDbFailedExceptionData object but got ${typeof value}.`) }
    return {
      code: sanitizer.sanitizeString(value.code, 'code'),
    };
  },
};
