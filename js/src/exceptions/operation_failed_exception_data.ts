/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for OperationFailedException.
 */
export interface OperationFailedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

  /**
   * The address of the device that attempted the failed operation.
   */
  device: number;

  /**
   * The number of the axis that attempted the failed operation.
   */
  axis: number;

}

export const OperationFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): OperationFailedExceptionData => BSON.deserialize(buffer) as OperationFailedExceptionData,
  toBinary: (value: OperationFailedExceptionData): Uint8Array => BSON.serialize(OperationFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
    device: 0,
    axis: 0,
  }) as Readonly<OperationFailedExceptionData>,
  sanitize: (value: OperationFailedExceptionData): OperationFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected OperationFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OperationFailedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
    };
  },
};
