﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { GCodeSyntaxExceptionData } from './g_code_syntax_exception_data';

/**
 * Thrown when a block of G-Code cannot be parsed.
 */
export class GCodeSyntaxException extends MotionLibException {
  /**
   * Additional data for GCodeSyntaxException
   */
  public get details(): GCodeSyntaxExceptionData {
    return this._details;
  }
  private _details: GCodeSyntaxExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | GCodeSyntaxExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, GCodeSyntaxException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = GCodeSyntaxExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
