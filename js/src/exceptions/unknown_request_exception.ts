﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Used for internal error handling. Indicates mixed library binary files. Reinstall the library.
 */
export class UnknownRequestException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, UnknownRequestException.prototype);
  }
}
