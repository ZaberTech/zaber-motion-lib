﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when no devices can be found on a connection.
 */
export class NoDeviceFoundException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, NoDeviceFoundException.prototype);
  }
}
