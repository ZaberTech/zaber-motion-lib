﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when an operation is not supported by a mode the PVT sequence is currently set up in.
 */
export class PvtModeException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, PvtModeException.prototype);
  }
}
