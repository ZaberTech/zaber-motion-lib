﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when operation cannot be performed at given time or context.
 */
export class InvalidOperationException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, InvalidOperationException.prototype);
  }
}
