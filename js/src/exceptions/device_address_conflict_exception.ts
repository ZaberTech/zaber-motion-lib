﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { DeviceAddressConflictExceptionData } from './device_address_conflict_exception_data';

/**
 * Thrown when there is a conflict in device numbers preventing unique addressing.
 */
export class DeviceAddressConflictException extends MotionLibException {
  /**
   * Additional data for DeviceAddressConflictException
   */
  public get details(): DeviceAddressConflictExceptionData {
    return this._details;
  }
  private _details: DeviceAddressConflictExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | DeviceAddressConflictExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, DeviceAddressConflictException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = DeviceAddressConflictExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
