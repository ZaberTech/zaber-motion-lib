/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for PvtMovementInterruptedException.
 */
export interface PvtMovementInterruptedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

}

export const PvtMovementInterruptedExceptionData = {
  fromBinary: (buffer: Uint8Array): PvtMovementInterruptedExceptionData => BSON.deserialize(buffer) as PvtMovementInterruptedExceptionData,
  toBinary: (value: PvtMovementInterruptedExceptionData): Uint8Array => BSON.serialize(PvtMovementInterruptedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
  }) as Readonly<PvtMovementInterruptedExceptionData>,
  sanitize: (value: PvtMovementInterruptedExceptionData): PvtMovementInterruptedExceptionData => {
    if (value == null) { throw new TypeError('Expected PvtMovementInterruptedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PvtMovementInterruptedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
