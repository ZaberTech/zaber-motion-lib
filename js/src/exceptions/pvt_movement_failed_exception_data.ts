/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains additional data for PvtMovementFailedException.
 */
export interface PvtMovementFailedExceptionData {
  /**
   * The full list of warnings.
   */
  warnings: string[];

  /**
   * The reason for the Exception.
   */
  reason: string;

}

export const PvtMovementFailedExceptionData = {
  fromBinary: (buffer: Uint8Array): PvtMovementFailedExceptionData => BSON.deserialize(buffer) as PvtMovementFailedExceptionData,
  toBinary: (value: PvtMovementFailedExceptionData): Uint8Array => BSON.serialize(PvtMovementFailedExceptionData.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    reason: '',
  }) as Readonly<PvtMovementFailedExceptionData>,
  sanitize: (value: PvtMovementFailedExceptionData): PvtMovementFailedExceptionData => {
    if (value == null) { throw new TypeError('Expected PvtMovementFailedExceptionData object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PvtMovementFailedExceptionData object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      reason: sanitizer.sanitizeString(value.reason, 'reason'),
    };
  },
};
