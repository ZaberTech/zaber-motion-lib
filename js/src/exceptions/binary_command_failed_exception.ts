﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { BinaryCommandFailedExceptionData } from './binary_command_failed_exception_data';

/**
 * Thrown when a device rejects a binary command with an error.
 */
export class BinaryCommandFailedException extends MotionLibException {
  /**
   * Additional data for BinaryCommandFailedException
   */
  public get details(): BinaryCommandFailedExceptionData {
    return this._details;
  }
  private _details: BinaryCommandFailedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | BinaryCommandFailedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, BinaryCommandFailedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = BinaryCommandFailedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
