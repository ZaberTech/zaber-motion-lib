﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { CommandFailedException } from './command_failed_exception';
import { CommandFailedExceptionData } from './command_failed_exception_data';

/**
 * Thrown when a device receives an invalid command.
 */
export class BadCommandException extends CommandFailedException {
  constructor(
    message: string,
    customData: Uint8Array | CommandFailedExceptionData
  ) {
    super(message, customData);
    Object.setPrototypeOf(this, BadCommandException.prototype);
  }
}
