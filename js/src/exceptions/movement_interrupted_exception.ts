﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';
import { MovementInterruptedExceptionData } from './movement_interrupted_exception_data';

/**
 * Thrown when ongoing movement is interrupted by another command or user input.
 */
export class MovementInterruptedException extends MotionLibException {
  /**
   * Additional data for MovementInterruptedException
   */
  public get details(): MovementInterruptedExceptionData {
    return this._details;
  }
  private _details: MovementInterruptedExceptionData;
  constructor(
    message: string,
    customData: Uint8Array | MovementInterruptedExceptionData
  ) {
    super(message);
    Object.setPrototypeOf(this, MovementInterruptedException.prototype);

    if (customData instanceof Uint8Array) {
      this._details = MovementInterruptedExceptionData.fromBinary(customData);
    } else {
      this._details = customData;
    }
  }
}
