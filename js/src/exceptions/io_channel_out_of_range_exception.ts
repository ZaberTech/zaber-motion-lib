﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { MotionLibException } from './motion_lib_exception';

/**
 * Thrown when a device IO operation cannot be performed because the provided channel is not valid.
 */
export class IoChannelOutOfRangeException extends MotionLibException {
  constructor(
    message: string
  ) {
    super(message);
    Object.setPrototypeOf(this, IoChannelOutOfRangeException.prototype);
  }
}
