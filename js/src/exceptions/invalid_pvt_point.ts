/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Contains invalid PVT points for PvtExecutionException.
 */
export interface InvalidPvtPoint {
  /**
   * Index of the point numbered from the last submitted point.
   */
  index: number;

  /**
   * The textual representation of the point.
   */
  point: string;

}

export const InvalidPvtPoint = {
  fromBinary: (buffer: Uint8Array): InvalidPvtPoint => BSON.deserialize(buffer) as InvalidPvtPoint,
  toBinary: (value: InvalidPvtPoint): Uint8Array => BSON.serialize(InvalidPvtPoint.sanitize(value)),
  DEFAULT: Object.freeze({
    index: 0,
    point: '',
  }) as Readonly<InvalidPvtPoint>,
  sanitize: (value: InvalidPvtPoint): InvalidPvtPoint => {
    if (value == null) { throw new TypeError('Expected InvalidPvtPoint object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected InvalidPvtPoint object but got ${typeof value}.`) }
    return {
      index: sanitizer.sanitizeInt(value.index, 'index'),
      point: sanitizer.sanitizeString(value.point, 'point'),
    };
  },
};
