/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from './gateway/sanitizer';

/**
 * Class representing version of firmware in the controller.
 */
export interface FirmwareVersion {
  /**
   * Major version number.
   */
  major: number;

  /**
   * Minor version number.
   */
  minor: number;

  /**
   * Build version number.
   */
  build: number;

}

export const FirmwareVersion = {
  fromBinary: (buffer: Uint8Array): FirmwareVersion => BSON.deserialize(buffer) as FirmwareVersion,
  toBinary: (value: FirmwareVersion): Uint8Array => BSON.serialize(FirmwareVersion.sanitize(value)),
  DEFAULT: Object.freeze({
    major: 0,
    minor: 0,
    build: 0,
  }) as Readonly<FirmwareVersion>,
  sanitize: (value: FirmwareVersion): FirmwareVersion => {
    if (value == null) { throw new TypeError('Expected FirmwareVersion object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected FirmwareVersion object but got ${typeof value}.`) }
    return {
      major: sanitizer.sanitizeInt(value.major, 'major'),
      minor: sanitizer.sanitizeInt(value.minor, 'minor'),
      build: sanitizer.sanitizeInt(value.build, 'build'),
    };
  },
};
