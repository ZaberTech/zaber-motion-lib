// This file is generated from the Zaber device database. Do not manually edit this file.
export namespace Units {
  export const NATIVE: Native = '';
}

export type Native = '';

export enum Length {
  METRES = 'Length:metres',
  'm' = 'Length:metres',
  CENTIMETRES = 'Length:centimetres',
  'cm' = 'Length:centimetres',
  MILLIMETRES = 'Length:millimetres',
  'mm' = 'Length:millimetres',
  MICROMETRES = 'Length:micrometres',
  'µm' = 'Length:micrometres',
  NANOMETRES = 'Length:nanometres',
  'nm' = 'Length:nanometres',
  INCHES = 'Length:inches',
  'in' = 'Length:inches',
}

export enum Velocity {
  METRES_PER_SECOND = 'Velocity:metres per second',
  'm/s' = 'Velocity:metres per second',
  CENTIMETRES_PER_SECOND = 'Velocity:centimetres per second',
  'cm/s' = 'Velocity:centimetres per second',
  MILLIMETRES_PER_SECOND = 'Velocity:millimetres per second',
  'mm/s' = 'Velocity:millimetres per second',
  MICROMETRES_PER_SECOND = 'Velocity:micrometres per second',
  'µm/s' = 'Velocity:micrometres per second',
  NANOMETRES_PER_SECOND = 'Velocity:nanometres per second',
  'nm/s' = 'Velocity:nanometres per second',
  INCHES_PER_SECOND = 'Velocity:inches per second',
  'in/s' = 'Velocity:inches per second',
}

export enum Acceleration {
  METRES_PER_SECOND_SQUARED = 'Acceleration:metres per second squared',
  'm/s²' = 'Acceleration:metres per second squared',
  CENTIMETRES_PER_SECOND_SQUARED = 'Acceleration:centimetres per second squared',
  'cm/s²' = 'Acceleration:centimetres per second squared',
  MILLIMETRES_PER_SECOND_SQUARED = 'Acceleration:millimetres per second squared',
  'mm/s²' = 'Acceleration:millimetres per second squared',
  MICROMETRES_PER_SECOND_SQUARED = 'Acceleration:micrometres per second squared',
  'µm/s²' = 'Acceleration:micrometres per second squared',
  NANOMETRES_PER_SECOND_SQUARED = 'Acceleration:nanometres per second squared',
  'nm/s²' = 'Acceleration:nanometres per second squared',
  INCHES_PER_SECOND_SQUARED = 'Acceleration:inches per second squared',
  'in/s²' = 'Acceleration:inches per second squared',
}

export enum Angle {
  DEGREES = 'Angle:degrees',
  '°' = 'Angle:degrees',
  RADIANS = 'Angle:radians',
  'rad' = 'Angle:radians',
}

export enum AngularVelocity {
  DEGREES_PER_SECOND = 'Angular Velocity:degrees per second',
  '°/s' = 'Angular Velocity:degrees per second',
  RADIANS_PER_SECOND = 'Angular Velocity:radians per second',
  'rad/s' = 'Angular Velocity:radians per second',
}

export enum AngularAcceleration {
  DEGREES_PER_SECOND_SQUARED = 'Angular Acceleration:degrees per second squared',
  '°/s²' = 'Angular Acceleration:degrees per second squared',
  RADIANS_PER_SECOND_SQUARED = 'Angular Acceleration:radians per second squared',
  'rad/s²' = 'Angular Acceleration:radians per second squared',
}

export enum ACElectricCurrent {
  AMPERES_PEAK = 'AC Electric Current:amperes peak',
  'A(peak)' = 'AC Electric Current:amperes peak',
  AMPERES_RMS = 'AC Electric Current:amperes RMS',
  'A(RMS)' = 'AC Electric Current:amperes RMS',
}

export enum Percent {
  PERCENT = 'Percent:percent',
  '%' = 'Percent:percent',
}

export enum DCElectricCurrent {
  AMPERES = 'DC Electric Current:amperes',
  'A' = 'DC Electric Current:amperes',
}

export enum Force {
  NEWTONS = 'Force:newtons',
  'N' = 'Force:newtons',
  MILLINEWTONS = 'Force:millinewtons',
  'mN' = 'Force:millinewtons',
  POUNDS_FORCE = 'Force:pounds-force',
  'lbf' = 'Force:pounds-force',
  KILONEWTONS = 'Force:kilonewtons',
  'kN' = 'Force:kilonewtons',
}

export enum Time {
  SECONDS = 'Time:seconds',
  's' = 'Time:seconds',
  MILLISECONDS = 'Time:milliseconds',
  'ms' = 'Time:milliseconds',
  MICROSECONDS = 'Time:microseconds',
  'µs' = 'Time:microseconds',
}

export enum Torque {
  NEWTON_METRES = 'Torque:newton metres',
  'N⋅m' = 'Torque:newton metres',
  NEWTON_CENTIMETRES = 'Torque:newton centimetres',
  'N⋅cm' = 'Torque:newton centimetres',
  POUND_FORCE_FEET = 'Torque:pound-force-feet',
  'lbf⋅ft' = 'Torque:pound-force-feet',
  OUNCE_FORCE_INCHES = 'Torque:ounce-force-inches',
  'ozf⋅in' = 'Torque:ounce-force-inches',
}

export enum Inertia {
  GRAMS = 'Inertia:grams',
  'g' = 'Inertia:grams',
  KILOGRAMS = 'Inertia:kilograms',
  'kg' = 'Inertia:kilograms',
  MILLIGRAMS = 'Inertia:milligrams',
  'mg' = 'Inertia:milligrams',
  POUNDS = 'Inertia:pounds',
  'lb' = 'Inertia:pounds',
  OUNCES = 'Inertia:ounces',
  'oz' = 'Inertia:ounces',
}

export enum RotationalInertia {
  GRAM_SQUARE_METRE = 'Rotational Inertia:gram-square metre',
  'g⋅m²' = 'Rotational Inertia:gram-square metre',
  KILOGRAM_SQUARE_METRE = 'Rotational Inertia:kilogram-square metre',
  'kg⋅m²' = 'Rotational Inertia:kilogram-square metre',
  POUND_SQUARE_FEET = 'Rotational Inertia:pound-square-feet',
  'lb⋅ft²' = 'Rotational Inertia:pound-square-feet',
}

export enum ForceConstant {
  NEWTONS_PER_AMP = 'Force Constant:newtons per amp',
  'N/A' = 'Force Constant:newtons per amp',
  MILLINEWTONS_PER_AMP = 'Force Constant:millinewtons per amp',
  'mN/A' = 'Force Constant:millinewtons per amp',
  KILONEWTONS_PER_AMP = 'Force Constant:kilonewtons per amp',
  'kN/A' = 'Force Constant:kilonewtons per amp',
  POUNDS_FORCE_PER_AMP = 'Force Constant:pounds-force per amp',
  'lbf/A' = 'Force Constant:pounds-force per amp',
}

export enum TorqueConstant {
  NEWTON_METRES_PER_AMP = 'Torque Constant:newton metres per amp',
  'N⋅m/A' = 'Torque Constant:newton metres per amp',
  MILLINEWTON_METRES_PER_AMP = 'Torque Constant:millinewton metres per amp',
  'mN⋅m/A' = 'Torque Constant:millinewton metres per amp',
  KILONEWTON_METRES_PER_AMP = 'Torque Constant:kilonewton metres per amp',
  'kN⋅m/A' = 'Torque Constant:kilonewton metres per amp',
  POUND_FORCE_FEET_PER_AMP = 'Torque Constant:pound-force-feet per amp',
  'lbf⋅ft/A' = 'Torque Constant:pound-force-feet per amp',
}

export enum Voltage {
  VOLTS = 'Voltage:volts',
  'V' = 'Voltage:volts',
  MILLIVOLTS = 'Voltage:millivolts',
  'mV' = 'Voltage:millivolts',
  MICROVOLTS = 'Voltage:microvolts',
  'µV' = 'Voltage:microvolts',
}

export enum CurrentControllerProportionalGain {
  VOLTS_PER_AMP = 'Current Controller Proportional Gain:volts per amp',
  'V/A' = 'Current Controller Proportional Gain:volts per amp',
  MILLIVOLTS_PER_AMP = 'Current Controller Proportional Gain:millivolts per amp',
  'mV/A' = 'Current Controller Proportional Gain:millivolts per amp',
  MICROVOLTS_PER_AMP = 'Current Controller Proportional Gain:microvolts per amp',
  'µV/A' = 'Current Controller Proportional Gain:microvolts per amp',
}

export enum CurrentControllerIntegralGain {
  VOLTS_PER_AMP_PER_SECOND = 'Current Controller Integral Gain:volts per amp per second',
  'V/(A⋅s)' = 'Current Controller Integral Gain:volts per amp per second',
  MILLIVOLTS_PER_AMP_PER_SECOND = 'Current Controller Integral Gain:millivolts per amp per second',
  'mV/(A⋅s)' = 'Current Controller Integral Gain:millivolts per amp per second',
  MICROVOLTS_PER_AMP_PER_SECOND = 'Current Controller Integral Gain:microvolts per amp per second',
  'µV/(A⋅s)' = 'Current Controller Integral Gain:microvolts per amp per second',
}

export enum CurrentControllerDerivativeGain {
  VOLTS_SECOND_PER_AMP = 'Current Controller Derivative Gain:volts second per amp',
  'V⋅s/A' = 'Current Controller Derivative Gain:volts second per amp',
  MILLIVOLTS_SECOND_PER_AMP = 'Current Controller Derivative Gain:millivolts second per amp',
  'mV⋅s/A' = 'Current Controller Derivative Gain:millivolts second per amp',
  MICROVOLTS_SECOND_PER_AMP = 'Current Controller Derivative Gain:microvolts second per amp',
  'µV⋅s/A' = 'Current Controller Derivative Gain:microvolts second per amp',
}

export enum Resistance {
  KILOOHMS = 'Resistance:kiloohms',
  'kΩ' = 'Resistance:kiloohms',
  OHMS = 'Resistance:ohms',
  'Ω' = 'Resistance:ohms',
  MILLIOHMS = 'Resistance:milliohms',
  'mΩ' = 'Resistance:milliohms',
  MICROOHMS = 'Resistance:microohms',
  'µΩ' = 'Resistance:microohms',
  NANOOHMS = 'Resistance:nanoohms',
  'nΩ' = 'Resistance:nanoohms',
}

export enum Inductance {
  HENRIES = 'Inductance:henries',
  'H' = 'Inductance:henries',
  MILLIHENRIES = 'Inductance:millihenries',
  'mH' = 'Inductance:millihenries',
  MICROHENRIES = 'Inductance:microhenries',
  'µH' = 'Inductance:microhenries',
  NANOHENRIES = 'Inductance:nanohenries',
  'nH' = 'Inductance:nanohenries',
}

export enum VoltageConstant {
  VOLT_SECONDS_PER_RADIAN = 'Voltage Constant:volt seconds per radian',
  'V·s/rad' = 'Voltage Constant:volt seconds per radian',
  MILLIVOLT_SECONDS_PER_RADIAN = 'Voltage Constant:millivolt seconds per radian',
  'mV·s/rad' = 'Voltage Constant:millivolt seconds per radian',
  MICROVOLT_SECONDS_PER_RADIAN = 'Voltage Constant:microvolt seconds per radian',
  'µV·s/rad' = 'Voltage Constant:microvolt seconds per radian',
}

export enum AbsoluteTemperature {
  DEGREES_CELSIUS = 'Absolute Temperature:degrees Celsius',
  '°C' = 'Absolute Temperature:degrees Celsius',
  KELVINS = 'Absolute Temperature:kelvins',
  'K' = 'Absolute Temperature:kelvins',
  DEGREES_FAHRENHEIT = 'Absolute Temperature:degrees Fahrenheit',
  '°F' = 'Absolute Temperature:degrees Fahrenheit',
  DEGREES_RANKINE = 'Absolute Temperature:degrees Rankine',
  '°R' = 'Absolute Temperature:degrees Rankine',
}

export enum RelativeTemperature {
  DEGREES_CELSIUS = 'Relative Temperature:degrees Celsius',
  '°C' = 'Relative Temperature:degrees Celsius',
  KELVINS = 'Relative Temperature:kelvins',
  'K' = 'Relative Temperature:kelvins',
  DEGREES_FAHRENHEIT = 'Relative Temperature:degrees Fahrenheit',
  '°F' = 'Relative Temperature:degrees Fahrenheit',
  DEGREES_RANKINE = 'Relative Temperature:degrees Rankine',
  '°R' = 'Relative Temperature:degrees Rankine',
}

export enum Frequency {
  GIGAHERTZ = 'Frequency:gigahertz',
  'GHz' = 'Frequency:gigahertz',
  MEGAHERTZ = 'Frequency:megahertz',
  'MHz' = 'Frequency:megahertz',
  KILOHERTZ = 'Frequency:kilohertz',
  'kHz' = 'Frequency:kilohertz',
  HERTZ = 'Frequency:hertz',
  'Hz' = 'Frequency:hertz',
  MILLIHERTZ = 'Frequency:millihertz',
  'mHz' = 'Frequency:millihertz',
  MICROHERTZ = 'Frequency:microhertz',
  'µHz' = 'Frequency:microhertz',
  NANOHERTZ = 'Frequency:nanohertz',
  'nHz' = 'Frequency:nanohertz',
}

export type Units = Native | Length | Velocity | Acceleration | Angle | AngularVelocity | AngularAcceleration | ACElectricCurrent | Percent | DCElectricCurrent | Force | Time | Torque | Inertia | RotationalInertia | ForceConstant | TorqueConstant | Voltage | CurrentControllerProportionalGain | CurrentControllerIntegralGain | CurrentControllerDerivativeGain | Resistance | Inductance | VoltageConstant | AbsoluteTemperature | RelativeTemperature | Frequency;
