﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from './gateway';
import * as requests from './requests';
import { LogOutputMode } from './log_output_mode';
import { DeviceDbSourceType } from './device_db_source_type';

export class Library {
  /**
   * Sets library logging output.
   * @param mode Logging output mode.
   * @param filePath Path of the file to open.
   */
  public static setLogOutput(
    mode: LogOutputMode,
    filePath?: string
  ): void {
    const request: requests.SetLogOutputRequest & gateway.Message = {
      ...requests.SetLogOutputRequest.DEFAULT,
      mode: mode,
      filePath: filePath,
      toBinary() {
        return requests.SetLogOutputRequest.toBinary(this);
      },
    };

    gateway.callSync('logging/set_output', request);
  }

  /**
   * Sets source of Device DB data. Allows selection of a web service or a local file.
   * @param sourceType Source type.
   * @param urlOrFilePath URL of the web service or path to the local file.
   * Leave empty for the default URL of Zaber web service.
   */
  public static setDeviceDbSource(
    sourceType: DeviceDbSourceType,
    urlOrFilePath?: string
  ): void {
    const request: requests.SetDeviceDbSourceRequest & gateway.Message = {
      ...requests.SetDeviceDbSourceRequest.DEFAULT,
      sourceType: sourceType,
      urlOrFilePath: urlOrFilePath,
      toBinary() {
        return requests.SetDeviceDbSourceRequest.toBinary(this);
      },
    };

    gateway.callSync('device_db/set_source', request);
  }

  /**
   * Enables Device DB store.
   * The store uses filesystem to save information obtained from the Device DB.
   * The stored data are later used instead of the Device DB.
   * @param storeLocation Specifies relative or absolute path of the folder used by the store.
   * If left empty defaults to a folder in user home directory.
   * Must be accessible by the process.
   */
  public static enableDeviceDbStore(
    storeLocation?: string
  ): void {
    const request: requests.ToggleDeviceDbStoreRequest & gateway.Message = {
      ...requests.ToggleDeviceDbStoreRequest.DEFAULT,
      toggleOn: true,
      storeLocation: storeLocation,
      toBinary() {
        return requests.ToggleDeviceDbStoreRequest.toBinary(this);
      },
    };

    gateway.callSync('device_db/toggle_store', request);
  }

  /**
   * Disables Device DB store.
   */
  public static disableDeviceDbStore(): void {
    const request: requests.ToggleDeviceDbStoreRequest & gateway.Message = {
      ...requests.ToggleDeviceDbStoreRequest.DEFAULT,
      toBinary() {
        return requests.ToggleDeviceDbStoreRequest.toBinary(this);
      },
    };

    gateway.callSync('device_db/toggle_store', request);
  }

  /**
   * Disables certain customer checks (like FF flag).
   * @param mode Whether to turn internal mode on or off.
   */
  public static setInternalMode(
    mode: boolean
  ): void {
    const request: requests.SetInternalModeRequest & gateway.Message = {
      ...requests.SetInternalModeRequest.DEFAULT,
      mode: mode,
      toBinary() {
        return requests.SetInternalModeRequest.toBinary(this);
      },
    };

    gateway.callSync('library/set_internal_mode', request);
  }

  /**
   * Sets the period between polling for IDLE during movements.
   * Caution: Setting the period too low may cause performance issues.
   * @param period Period in milliseconds.
   * Negative value restores the default period.
   */
  public static setIdlePollingPeriod(
    period: number
  ): void {
    const request: requests.IntRequest & gateway.Message = {
      ...requests.IntRequest.DEFAULT,
      value: period,
      toBinary() {
        return requests.IntRequest.toBinary(this);
      },
    };

    gateway.callSync('library/set_idle_polling_period', request);
  }

  /**
   * Throws an error if the version of the loaded shared library does not match the caller's version.
   */
  public static checkVersion(): void {
    const request: requests.CheckVersionRequest & gateway.Message = {
      ...requests.CheckVersionRequest.DEFAULT,
      host: 'js',
      version: '7.5.0',
      toBinary() {
        return requests.CheckVersionRequest.toBinary(this);
      },
    };

    gateway.callSync('library/check_version', request);
  }
}

gateway.addInitCallback(Library.checkVersion);
