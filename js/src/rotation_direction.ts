/* This file is generated. Do not modify by hand. */
/**
 * Direction of rotation.
 */
export enum RotationDirection {
  /** Clockwise. */
  CLOCKWISE = 0,
  /** Counterclockwise. */
  COUNTERCLOCKWISE = 1,
  /** CW. */
  CW = 0,
  /** CCW. */
  CCW = 1,
}
