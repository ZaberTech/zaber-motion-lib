﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from './gateway';
import * as requests from './requests';
import { Units } from './units';

export class UnitTable {
  /**
   * Gets the standard symbol associated with a given unit.
   * @param unit Unit of measure.
   * @returns Symbols corresponding to the given unit. Throws NoValueForKey if no symbol is defined.
   */
  public static getSymbol(
    unit: Units
  ): string {
    const request: requests.UnitGetSymbolRequest & gateway.Message = {
      ...requests.UnitGetSymbolRequest.DEFAULT,
      unit: unit,
      toBinary() {
        return requests.UnitGetSymbolRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.UnitGetSymbolResponse>(
      'units/get_symbol',
      request,
      requests.UnitGetSymbolResponse.fromBinary);
    return response.symbol;
  }

  /**
   * Gets the unit enum value associated with a standard symbol.
   * Note not all units can be retrieved this way.
   * @param symbol Symbol to look up.
   * @returns The unit enum value with the given symbols. Throws NoValueForKey if the symbol is not supported for lookup.
   */
  public static getUnit(
    symbol: string
  ): Units {
    const request: requests.UnitGetEnumRequest & gateway.Message = {
      ...requests.UnitGetEnumRequest.DEFAULT,
      symbol: symbol,
      toBinary() {
        return requests.UnitGetEnumRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.UnitGetEnumResponse>(
      'units/get_enum',
      request,
      requests.UnitGetEnumResponse.fromBinary);
    return response.unit;
  }
}
