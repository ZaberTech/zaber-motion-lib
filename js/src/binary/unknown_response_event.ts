/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Reply that could not be matched to a request.
 */
export interface UnknownResponseEvent {
  /**
   * Number of the device that sent or should receive the message.
   */
  deviceAddress: number;

  /**
   * The warning flag contains the highest priority warning currently active for the device or axis.
   */
  command: number;

  /**
   * Data payload of the message, if applicable, or zero otherwise.
   */
  data: number;

}

export const UnknownResponseEvent = {
  fromBinary: (buffer: Uint8Array): UnknownResponseEvent => BSON.deserialize(buffer) as UnknownResponseEvent,
  toBinary: (value: UnknownResponseEvent): Uint8Array => BSON.serialize(UnknownResponseEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddress: 0,
    command: 0,
    data: 0,
  }) as Readonly<UnknownResponseEvent>,
  sanitize: (value: UnknownResponseEvent): UnknownResponseEvent => {
    if (value == null) { throw new TypeError('Expected UnknownResponseEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnknownResponseEvent object but got ${typeof value}.`) }
    return {
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      command: sanitizer.sanitizeInt(value.command, 'command'),
      data: sanitizer.sanitizeInt(value.data, 'data'),
    };
  },
};
