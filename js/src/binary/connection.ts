﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Observable, ReplaySubject } from 'rxjs';
import { map, filter, takeUntil, take } from 'rxjs/operators';
import * as gateway from '../gateway';
import { events, filterEvent } from '../gateway';
import * as requests from '../requests';
import { Device } from './device';
import { UnknownResponseEvent } from './unknown_response_event';
import { ReplyOnlyEvent } from './reply_only_event';
import { CommandCode } from './command_code';
import { Message } from './message';
import { MotionLibException } from '../exceptions';

/**
 * Class representing access to particular connection (serial port, TCP connection) using the legacy Binary protocol.
 */
export class Connection {
  /**
   * Event invoked when a response from a device cannot be matched to any known request.
   */
  public unknownResponse!: Observable<UnknownResponseEvent>;

  /**
   * Event invoked when a reply-only command such as a move tracking message is received from a device.
   */
  public replyOnly!: Observable<ReplyOnlyEvent>;

  /**
   * Default baud rate for serial connections.
   */
  public static readonly DEFAULT_BAUD_RATE: number = 9600;

  /**
   * The interface ID identifies thisConnection instance with the underlying library.
   */
  public get interfaceId(): number {
    return this._interfaceId;
  }
  private _interfaceId: number;

  constructor(interfaceId: number) {
    this._interfaceId = interfaceId;
    this._subscribe();
  }

  /**
   * Opens a serial port.
   * @param portName Name of the port to open.
   * @param [options.baudRate=Connection.DEFAULT_BAUD_RATE] Optional baud rate (defaults to 9600).
   * @param [options.useMessageIds=false] Enable use of message IDs (defaults to disabled).
   * All your devices must be pre-configured to match.
   * @returns An object representing the port.
   */
  public static async openSerialPort(
    portName: string,
    options: Connection.OpenSerialPortOptions = {}
  ): Promise<Connection> {
    const {
      baudRate = Connection.DEFAULT_BAUD_RATE,
      useMessageIds = false,
    } = options;
    const request: requests.OpenBinaryInterfaceRequest & gateway.Message = {
      ...requests.OpenBinaryInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.SERIAL_PORT,
      portName: portName,
      baudRate: baudRate,
      useMessageIds: useMessageIds,
      toBinary() {
        return requests.OpenBinaryInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'binary/interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Opens a TCP connection.
   * @param hostName Hostname or IP address.
   * @param port Port number.
   * @param [options.useMessageIds=false] Enable use of message IDs (defaults to disabled).
   * All your devices must be pre-configured to match.
   * @returns An object representing the connection.
   */
  public static async openTcp(
    hostName: string,
    port: number,
    options: Connection.OpenTcpOptions = {}
  ): Promise<Connection> {
    const {
      useMessageIds = false,
    } = options;
    const request: requests.OpenBinaryInterfaceRequest & gateway.Message = {
      ...requests.OpenBinaryInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.TCP,
      hostName: hostName,
      port: port,
      useMessageIds: useMessageIds,
      toBinary() {
        return requests.OpenBinaryInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'binary/interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Close the connection.
   */
  public async close(): Promise<void> {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/close', request);
  }

  /**
   * Sends a generic Binary command to this connection.
   * For more information please refer to the
   * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
   * @param device Device address to send the command to. Use zero for broadcast.
   * @param command Command to send.
   * @param [data=0] Optional data argument to the command. Defaults to zero.
   * @param [options.timeout=0.0] Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @returns A response to the command.
   */
  public async genericCommand(
    device: number,
    command: CommandCode,
    data: number = 0,
    options: Connection.GenericCommandOptions = {}
  ): Promise<Message> {
    const {
      timeout = 0.0,
      checkErrors = true,
    } = options;
    const request: requests.GenericBinaryRequest & gateway.Message = {
      ...requests.GenericBinaryRequest.DEFAULT,
      interfaceId: this.interfaceId,
      device: device,
      command: command,
      data: data,
      timeout: timeout,
      checkErrors: checkErrors,
      toBinary() {
        return requests.GenericBinaryRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Message>(
      'binary/interface/generic_command',
      request,
      Message.fromBinary);
    return response;
  }

  /**
   * Sends a generic Binary command to this connection without expecting a response.
   * For more information please refer to the
   * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
   * @param device Device address to send the command to. Use zero for broadcast.
   * @param command Command to send.
   * @param [data=0] Optional data argument to the command. Defaults to zero.
   */
  public async genericCommandNoResponse(
    device: number,
    command: CommandCode,
    data: number = 0
  ): Promise<void> {
    const request: requests.GenericBinaryRequest & gateway.Message = {
      ...requests.GenericBinaryRequest.DEFAULT,
      interfaceId: this.interfaceId,
      device: device,
      command: command,
      data: data,
      toBinary() {
        return requests.GenericBinaryRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/interface/generic_command_no_response', request);
  }

  /**
   * Sends a generic Binary command to this connection and expects responses from one or more devices.
   * Responses are returned in order of arrival.
   * For more information please refer to the
   * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
   * @param command Command to send.
   * @param [data=0] Optional data argument to the command. Defaults to zero.
   * @param [options.timeout=0.0] Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
   * @param [options.checkErrors=true] Controls whether to throw an exception when any device rejects the command.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: CommandCode,
    data: number = 0,
    options: Connection.GenericCommandMultiResponseOptions = {}
  ): Promise<Message[]> {
    const {
      timeout = 0.0,
      checkErrors = true,
    } = options;
    const request: requests.GenericBinaryRequest & gateway.Message = {
      ...requests.GenericBinaryRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: command,
      data: data,
      timeout: timeout,
      checkErrors: checkErrors,
      toBinary() {
        return requests.GenericBinaryRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BinaryMessageCollection>(
      'binary/interface/generic_command_multi_response',
      request,
      requests.BinaryMessageCollection.fromBinary);
    return response.messages;
  }

  /**
   * Renumbers devices present on this connection. After renumbering, you must identify devices again.
   * @returns Total number of devices that responded to the renumber.
   */
  public async renumberDevices(): Promise<number> {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'binary/device/renumber',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Attempts to detect any devices present on this connection.
   * @param [options.identifyDevices=true] Determines whether device identification should be performed as well.
   * @returns Array of detected devices.
   */
  public async detectDevices(
    options: Connection.DetectDevicesOptions = {}
  ): Promise<Device[]> {
    const {
      identifyDevices = true,
    } = options;
    const request: requests.BinaryDeviceDetectRequest & gateway.Message = {
      ...requests.BinaryDeviceDetectRequest.DEFAULT,
      interfaceId: this.interfaceId,
      identifyDevices: identifyDevices,
      toBinary() {
        return requests.BinaryDeviceDetectRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BinaryDeviceDetectResponse>(
      'binary/device/detect',
      request,
      requests.BinaryDeviceDetectResponse.fromBinary);
    return response.devices.map(deviceAddress => this.getDevice(deviceAddress));
  }

  /**
   * Gets a Device class instance which allows you to control a particular device on this connection.
   * Devices are numbered from 1.
   * @param deviceAddress Address of device intended to control. Address is configured for each device.
   * @returns Device instance.
   */
  public getDevice(
    deviceAddress: number
  ): Device {
    if (deviceAddress <= 0) {
      throw new TypeError('Invalid value; physical devices are numbered from 1.');
    }
    return new Device(this, deviceAddress);
  }

  /**
   * Returns a string that represents the connection.
   * @returns A string that represents the connection.
   */
  public toString(): string {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'interface/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  private _disconnected = new ReplaySubject<MotionLibException>();

  /**
   * Event invoked when connection is interrupted or closed.
   */
  public get disconnected(): Observable<MotionLibException> {
    return this._disconnected;
  }

  private _subscribe(): void {
    this.unknownResponse = events.pipe(
      takeUntil(this._disconnected),
      filterEvent<requests.UnknownBinaryResponseEventWrapper>('binary/interface/unknown_response'),
      filter(event => event.interfaceId === this.interfaceId),
      map(event => event.unknownResponse)
    );

    this.replyOnly = events.pipe(
      takeUntil(this._disconnected),
      filterEvent<requests.BinaryReplyOnlyEventWrapper>('binary/interface/reply_only'),
      filter(event => event.interfaceId === this.interfaceId),
      map(event => event.reply)
    );

    events.pipe(
      filterEvent<requests.DisconnectedEvent>('interface/disconnected'),
      filter(event => event.interfaceId === this.interfaceId),
      take(1),
      map(({ errorMessage, errorType }) => gateway.convertToException(errorType, errorMessage))
    ).subscribe(this._disconnected);
  }
}

namespace Connection {
  export interface OpenSerialPortOptions {
      /**
       * Optional baud rate (defaults to 9600).
       */
      baudRate?: number;
      /**
       * Enable use of message IDs (defaults to disabled).
       * All your devices must be pre-configured to match.
       */
      useMessageIds?: boolean;
  }
  export interface OpenTcpOptions {
      /**
       * Enable use of message IDs (defaults to disabled).
       * All your devices must be pre-configured to match.
       */
      useMessageIds?: boolean;
  }
  export interface GenericCommandOptions {
      /**
       * Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
       */
      timeout?: number;
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
       */
      timeout?: number;
      /**
       * Controls whether to throw an exception when any device rejects the command.
       */
      checkErrors?: boolean;
  }
  export interface DetectDevicesOptions {
      /**
       * Determines whether device identification should be performed as well.
       */
      identifyDevices?: boolean;
  }
}
