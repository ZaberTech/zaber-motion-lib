﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { BinarySettings } from './binary_settings';
import { Connection } from './connection';
import { DeviceIdentity } from './device_identity';
import { DeviceSettings } from './device_settings';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { CommandCode } from './command_code';
import { Message } from './message';
import { Angle, Length, Native, AngularVelocity, Velocity, Units } from '../units';
import { DeviceType } from './device_type';
import { FirmwareVersion } from '../firmware_version';

/**
 * Represents a device using the binary protocol.
 */
export class Device {
  /**
   * Default timeout for move commands in seconds.
   */
  public static readonly DEFAULT_MOVEMENT_TIMEOUT: number = 60;

  /**
   * Connection of this device.
   */
  public get connection(): Connection {
    return this._connection;
  }
  private _connection: Connection;

  /**
   * Settings and properties of this axis.
   */
  public get settings(): DeviceSettings {
    return this._settings;
  }
  private _settings: DeviceSettings;

  /**
   * The device address uniquely identifies the device on the connection.
   * It can be configured or automatically assigned by the renumber command.
   */
  public get deviceAddress(): number {
    return this._deviceAddress;
  }
  private _deviceAddress: number;

  /**
   * Identity of the device.
   */
  public get identity(): DeviceIdentity {
    return this._retrieveIdentity();
  }

  /**
   * Indicates whether or not the device has been identified.
   */
  public get isIdentified(): boolean {
    return this._retrieveIsIdentified();
  }

  /**
   * Unique ID of the device hardware.
   */
  public get deviceId(): number {
    return this.identity.deviceId;
  }

  /**
   * Serial number of the device.
   * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
   */
  public get serialNumber(): number {
    return this.identity.serialNumber;
  }

  /**
   * Name of the product.
   */
  public get name(): string {
    return this.identity.name;
  }

  /**
   * Version of the firmware.
   */
  public get firmwareVersion(): FirmwareVersion {
    return this.identity.firmwareVersion;
  }

  /**
   * Indicates whether the device is a peripheral or part of an integrated device.
   */
  public get isPeripheral(): boolean {
    return this.identity.isPeripheral;
  }

  /**
   * Unique ID of the peripheral hardware.
   */
  public get peripheralId(): number {
    return this.identity.peripheralId;
  }

  /**
   * Name of the peripheral hardware.
   */
  public get peripheralName(): string {
    return this.identity.peripheralName;
  }

  /**
   * Determines the type of an device and units it accepts.
   */
  public get deviceType(): DeviceType {
    return this.identity.deviceType;
  }

  constructor(connection: Connection, deviceAddress: number) {
    this._connection = connection;
    this._settings = new DeviceSettings(this);
    this._deviceAddress = deviceAddress;
  }

  /**
   * Sends a generic Binary command to this device.
   * For more information please refer to the
   * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
   * @param command Command to send.
   * @param [data=0] Optional data argument to the command. Defaults to zero.
   * @param [options.timeout=0.0] Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: CommandCode,
    data: number = 0,
    options: Device.GenericCommandOptions = {}
  ): Promise<Message> {
    const {
      timeout = 0.0,
      checkErrors = true,
    } = options;
    const request: requests.GenericBinaryRequest & gateway.Message = {
      ...requests.GenericBinaryRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      data: data,
      timeout: timeout,
      checkErrors: checkErrors,
      toBinary() {
        return requests.GenericBinaryRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Message>(
      'binary/interface/generic_command',
      request,
      Message.fromBinary);
    return response;
  }

  /**
   * Sends a generic Binary command to this device without expecting a response.
   * For more information please refer to the
   * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
   * @param command Command to send.
   * @param [data=0] Optional data argument to the command. Defaults to zero.
   */
  public async genericCommandNoResponse(
    command: CommandCode,
    data: number = 0
  ): Promise<void> {
    const request: requests.GenericBinaryRequest & gateway.Message = {
      ...requests.GenericBinaryRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      data: data,
      toBinary() {
        return requests.GenericBinaryRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/interface/generic_command_no_response', request);
  }

  /**
   * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
   * @param command Command to send.
   * @param [data=0] Data argument to the command. Defaults to zero.
   * @param [fromUnit=Units.NATIVE] Unit to convert sent data from.
   * @param [toUnit=Units.NATIVE] Unit to convert retrieved data to.
   * @param [options.timeout=0.0] Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
   * @returns Data that has been converted to the provided unit.
   */
  public async genericCommandWithUnits(
    command: CommandCode,
    data: number = 0,
    fromUnit: Units = Units.NATIVE,
    toUnit: Units = Units.NATIVE,
    options: Device.GenericCommandWithUnitsOptions = {}
  ): Promise<number> {
    const {
      timeout = 0.0,
    } = options;
    const request: requests.BinaryGenericWithUnitsRequest & gateway.Message = {
      ...requests.BinaryGenericWithUnitsRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      data: data,
      fromUnit: fromUnit,
      toUnit: toUnit,
      timeout: timeout,
      toBinary() {
        return requests.BinaryGenericWithUnitsRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/generic_command_with_units',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Homes device. Device returns to its homing position.
   * @param [options.unit=Units.NATIVE] Unit to convert returned position to.
   * @param [options.timeout=Device.DEFAULT_MOVEMENT_TIMEOUT] Number of seconds to wait for response from the device chain (defaults to 60s).
   * @returns Current position that has been converted to the provided unit.
   */
  public async home(
    options: Device.HomeOptions = {}
  ): Promise<number> {
    const {
      unit = Units.NATIVE,
      timeout = Device.DEFAULT_MOVEMENT_TIMEOUT,
    } = options;
    const request: requests.BinaryDeviceHomeRequest & gateway.Message = {
      ...requests.BinaryDeviceHomeRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      unit: unit,
      timeout: timeout,
      toBinary() {
        return requests.BinaryDeviceHomeRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/home',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Stops ongoing device movement. Decelerates until zero speed.
   * @param [options.unit=Units.NATIVE] Unit to convert returned position to.
   * @param [options.timeout=Device.DEFAULT_MOVEMENT_TIMEOUT] Number of seconds to wait for response from the device chain (defaults to 60s).
   * @returns Current position that has been converted to the provided unit.
   */
  public async stop(
    options: Device.StopOptions = {}
  ): Promise<number> {
    const {
      unit = Units.NATIVE,
      timeout = Device.DEFAULT_MOVEMENT_TIMEOUT,
    } = options;
    const request: requests.BinaryDeviceStopRequest & gateway.Message = {
      ...requests.BinaryDeviceStopRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      unit: unit,
      timeout: timeout,
      toBinary() {
        return requests.BinaryDeviceStopRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/stop',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Move device to absolute position.
   * @param position Absolute position.
   * @param [unit=Units.NATIVE] Unit for the provided position as well as position returned by the device.
   * @param [options.timeout=Device.DEFAULT_MOVEMENT_TIMEOUT] Number of seconds to wait for response from the device chain (defaults to 60s).
   * @returns Current position that has been converted to the provided unit.
   */
  public async moveAbsolute(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Device.MoveAbsoluteOptions = {}
  ): Promise<number> {
    const {
      timeout = Device.DEFAULT_MOVEMENT_TIMEOUT,
    } = options;
    const request: requests.BinaryDeviceMoveRequest & gateway.Message = {
      ...requests.BinaryDeviceMoveRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      type: requests.AxisMoveType.ABS,
      arg: position,
      unit: unit,
      timeout: timeout,
      toBinary() {
        return requests.BinaryDeviceMoveRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/move',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Move device to position relative to current position.
   * @param position Relative position.
   * @param [unit=Units.NATIVE] Unit for the provided position as well as position returned by the device.
   * @param [options.timeout=Device.DEFAULT_MOVEMENT_TIMEOUT] Number of seconds to wait for response from the device chain (defaults to 60s).
   * @returns Current position that has been converted to the provided unit.
   */
  public async moveRelative(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Device.MoveRelativeOptions = {}
  ): Promise<number> {
    const {
      timeout = Device.DEFAULT_MOVEMENT_TIMEOUT,
    } = options;
    const request: requests.BinaryDeviceMoveRequest & gateway.Message = {
      ...requests.BinaryDeviceMoveRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      type: requests.AxisMoveType.REL,
      arg: position,
      unit: unit,
      timeout: timeout,
      toBinary() {
        return requests.BinaryDeviceMoveRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/move',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Begins to move device at specified speed.
   * @param velocity Movement velocity.
   * @param [unit=Units.NATIVE] Unit to convert returned velocity to.
   * @returns Device velocity that has been converted to the provided unit.
   */
  public async moveVelocity(
    velocity: number,
    unit: Velocity | AngularVelocity | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.BinaryDeviceMoveRequest & gateway.Message = {
      ...requests.BinaryDeviceMoveRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      type: requests.AxisMoveType.VEL,
      arg: velocity,
      unit: unit,
      toBinary() {
        return requests.BinaryDeviceMoveRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/move',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Waits until device stops moving.
   */
  public async waitUntilIdle(): Promise<void> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/device/wait_until_idle', request);
  }

  /**
   * Check whether the device is moving.
   * @returns True if the device is currently executing a motion command.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'binary/device/is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Queries the device and the database, gathering information about the product.
   * Without this information features such as unit conversions will not work.
   * Usually, called automatically by detect devices method.
   * @param options.assumeVersion The identification assumes the specified firmware version
   * instead of the version queried from the device.
   * Providing this argument can lead to unexpected compatibility issues.
   * @returns Device identification data.
   */
  public async identify(
    options: Device.IdentifyOptions = {}
  ): Promise<DeviceIdentity> {
    const {
      assumeVersion,
    } = options;
    const request: requests.DeviceIdentifyRequest & gateway.Message = {
      ...requests.DeviceIdentifyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      assumeVersion: assumeVersion,
      toBinary() {
        return requests.DeviceIdentifyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<DeviceIdentity>(
      'binary/device/identify',
      request,
      DeviceIdentity.fromBinary);
    return response;
  }

  /**
   * Parks the axis.
   * Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
   * It can later be unparked and moved without first having to home it.
   * Requires at least Firmware 6.06.
   */
  public async park(): Promise<void> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/device/park', request);
  }

  /**
   * Unparks axis. Axis will now be able to move.
   * Requires at least Firmware 6.06.
   */
  public async unpark(): Promise<void> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/device/unpark', request);
  }

  /**
   * Returns bool indicating whether the axis is parked or not.
   * Requires at least Firmware 6.06.
   * @returns True if the axis is currently parked. False otherwise.
   */
  public async isParked(): Promise<boolean> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'binary/device/is_parked',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns current device position.
   * @param [unit=Units.NATIVE] Units of position.
   * @returns Axis position.
   */
  public async getPosition(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.BinaryDeviceGetSettingRequest & gateway.Message = {
      ...requests.BinaryDeviceGetSettingRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      setting: BinarySettings.CURRENT_POSITION,
      unit: unit,
      toBinary() {
        return requests.BinaryDeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'binary/device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns identity.
   * @returns Device identity.
   */
  private _retrieveIdentity(): DeviceIdentity {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<DeviceIdentity>(
      'binary/device/get_identity',
      request,
      DeviceIdentity.fromBinary);
    return response;
  }

  /**
   * Returns whether or not the device have been identified.
   * @returns True if the device has already been identified. False otherwise.
   */
  private _retrieveIsIdentified(): boolean {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.BoolResponse>(
      'binary/device/get_is_identified',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }
}

namespace Device {
  export interface GenericCommandOptions {
      /**
       * Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
       */
      timeout?: number;
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
  }
  export interface GenericCommandWithUnitsOptions {
      /**
       * Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
       */
      timeout?: number;
  }
  export interface HomeOptions {
      /**
       * Unit to convert returned position to.
       */
      unit?: Length | Angle | Native;
      /**
       * Number of seconds to wait for response from the device chain (defaults to 60s).
       */
      timeout?: number;
  }
  export interface StopOptions {
      /**
       * Unit to convert returned position to.
       */
      unit?: Length | Angle | Native;
      /**
       * Number of seconds to wait for response from the device chain (defaults to 60s).
       */
      timeout?: number;
  }
  export interface MoveAbsoluteOptions {
      /**
       * Number of seconds to wait for response from the device chain (defaults to 60s).
       */
      timeout?: number;
  }
  export interface MoveRelativeOptions {
      /**
       * Number of seconds to wait for response from the device chain (defaults to 60s).
       */
      timeout?: number;
  }
  export interface IdentifyOptions {
      /**
       * The identification assumes the specified firmware version
       * instead of the version queried from the device.
       * Providing this argument can lead to unexpected compatibility issues.
       */
      assumeVersion?: FirmwareVersion;
  }
}
