/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * A message in the Binary protocol.
 */
export interface Message {
  /**
   * Number of the device that sent or should receive the message.
   */
  deviceAddress: number;

  /**
   * The warning flag contains the highest priority warning currently active for the device or axis.
   */
  command: number;

  /**
   * Data payload of the message, if applicable, or zero otherwise.
   */
  data: number;

}

export const Message = {
  fromBinary: (buffer: Uint8Array): Message => BSON.deserialize(buffer) as Message,
  toBinary: (value: Message): Uint8Array => BSON.serialize(Message.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddress: 0,
    command: 0,
    data: 0,
  }) as Readonly<Message>,
  sanitize: (value: Message): Message => {
    if (value == null) { throw new TypeError('Expected Message object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected Message object but got ${typeof value}.`) }
    return {
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      command: sanitizer.sanitizeInt(value.command, 'command'),
      data: sanitizer.sanitizeInt(value.data, 'data'),
    };
  },
};
