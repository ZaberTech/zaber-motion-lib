/* This file is generated. Do not modify by hand. */
/**
 * Denotes type of an device and units it accepts.
 */
export enum DeviceType {
  /** Unknown. */
  UNKNOWN = 0,
  /** Linear. */
  LINEAR = 1,
  /** Rotary. */
  ROTARY = 2,
}
