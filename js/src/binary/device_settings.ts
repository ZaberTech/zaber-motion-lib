﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { BinarySettings } from './binary_settings';
import { Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Class providing access to various device settings and properties.
 */
export class DeviceSettings {
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Returns any device setting or property.
   * @param setting Setting to get.
   * @param [unit=Units.NATIVE] Units of setting.
   * @returns Setting value.
   */
  public async get(
    setting: BinarySettings,
    unit: Units = Units.NATIVE
  ): Promise<number> {
    const request: requests.BinaryDeviceGetSettingRequest & gateway.Message = {
      ...requests.BinaryDeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      unit: unit,
      toBinary() {
        return requests.BinaryDeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'binary/device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets any device setting.
   * @param setting Setting to set.
   * @param value Value of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   */
  public async set(
    setting: BinarySettings,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.BinaryDeviceSetSettingRequest & gateway.Message = {
      ...requests.BinaryDeviceSetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.BinaryDeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('binary/device/set_setting', request);
  }
}
