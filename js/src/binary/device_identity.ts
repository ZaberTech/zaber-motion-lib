/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { FirmwareVersion } from '../firmware_version';
import { DeviceType } from '../binary/device_type';

/**
 * Representation of data gathered during device identification.
 */
export interface DeviceIdentity {
  /**
   * Unique ID of the device hardware.
   */
  deviceId: number;

  /**
   * Serial number of the device.
   * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
   */
  serialNumber: number;

  /**
   * Name of the product.
   */
  name: string;

  /**
   * Version of the firmware.
   */
  firmwareVersion: FirmwareVersion;

  /**
   * Indicates whether the device is a peripheral or part of an integrated device.
   */
  isPeripheral: boolean;

  /**
   * Unique ID of the peripheral hardware.
   */
  peripheralId: number;

  /**
   * Name of the peripheral hardware.
   */
  peripheralName: string;

  /**
   * Determines the type of an device and units it accepts.
   */
  deviceType: DeviceType;

}

export const DeviceIdentity = {
  fromBinary: (buffer: Uint8Array): DeviceIdentity => BSON.deserialize(buffer) as DeviceIdentity,
  toBinary: (value: DeviceIdentity): Uint8Array => BSON.serialize(DeviceIdentity.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceId: 0,
    serialNumber: 0,
    name: '',
    firmwareVersion: FirmwareVersion.DEFAULT,
    isPeripheral: false,
    peripheralId: 0,
    peripheralName: '',
    deviceType: 0 as DeviceType,
  }) as Readonly<DeviceIdentity>,
  sanitize: (value: DeviceIdentity): DeviceIdentity => {
    if (value == null) { throw new TypeError('Expected DeviceIdentity object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceIdentity object but got ${typeof value}.`) }
    return {
      deviceId: sanitizer.sanitizeInt(value.deviceId, 'deviceId'),
      serialNumber: sanitizer.sanitizeInt(value.serialNumber, 'serialNumber'),
      name: sanitizer.sanitizeString(value.name, 'name'),
      firmwareVersion: FirmwareVersion.sanitize(value.firmwareVersion),
      isPeripheral: sanitizer.sanitizeBool(value.isPeripheral, 'isPeripheral'),
      peripheralId: sanitizer.sanitizeInt(value.peripheralId, 'peripheralId'),
      peripheralName: sanitizer.sanitizeString(value.peripheralName, 'peripheralName'),
      deviceType: sanitizer.sanitizeEnum(value.deviceType, 'DeviceType', DeviceType, 'deviceType'),
    };
  },
};
