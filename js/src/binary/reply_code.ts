/* This file is generated. Do not modify by hand. */
/**
 * Named constants for all Zaber Binary protocol reply-only command codes.
 */
export enum ReplyCode {
  /** MoveTracking. */
  MOVE_TRACKING = 8,
  /** LimitActive. */
  LIMIT_ACTIVE = 9,
  /** ManualMoveTracking. */
  MANUAL_MOVE_TRACKING = 10,
  /** ManualMove. */
  MANUAL_MOVE = 11,
  /** SlipTracking. */
  SLIP_TRACKING = 12,
  /** UnexpectedPosition. */
  UNEXPECTED_POSITION = 13,
  /** Error. */
  ERROR = 255,
}
