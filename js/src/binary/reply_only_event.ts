/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Spontaneous message received from the device.
 */
export interface ReplyOnlyEvent {
  /**
   * Number of the device that sent or should receive the message.
   */
  deviceAddress: number;

  /**
   * The warning flag contains the highest priority warning currently active for the device or axis.
   */
  command: number;

  /**
   * Data payload of the message, if applicable, or zero otherwise.
   */
  data: number;

}

export const ReplyOnlyEvent = {
  fromBinary: (buffer: Uint8Array): ReplyOnlyEvent => BSON.deserialize(buffer) as ReplyOnlyEvent,
  toBinary: (value: ReplyOnlyEvent): Uint8Array => BSON.serialize(ReplyOnlyEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddress: 0,
    command: 0,
    data: 0,
  }) as Readonly<ReplyOnlyEvent>,
  sanitize: (value: ReplyOnlyEvent): ReplyOnlyEvent => {
    if (value == null) { throw new TypeError('Expected ReplyOnlyEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ReplyOnlyEvent object but got ${typeof value}.`) }
    return {
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      command: sanitizer.sanitizeInt(value.command, 'command'),
      data: sanitizer.sanitizeInt(value.data, 'data'),
    };
  },
};
