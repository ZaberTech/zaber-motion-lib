let finalizationRegistry: FinalizationRegistry<() => void> | undefined;

if (typeof FinalizationRegistry !== 'undefined') {
  finalizationRegistry = new FinalizationRegistry(cleanup => cleanup());
}

/**
 * @param instance what to finalize
 * @param cleanup callback performing the finalization (MUST NOT hold reference to instance)
 */
export const registerForFinalization = (instance: object, cleanup: () => void) => {
  if (finalizationRegistry == null) {
    // eslint-disable-next-line no-console
    console.warn(`FinalizationRegistry is not supported by your environment, instances of ${instance.constructor.name} must be freed manually.`);
    return;
  }
  finalizationRegistry.register(instance, cleanup);
};
