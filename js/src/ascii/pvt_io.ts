﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { Time, Native, Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { DigitalOutputAction } from './digital_output_action';

/**
 * Class providing access to I/O for a PVT sequence.
 */
export class PvtIo {
  private _device: Device;

  private _streamId: number;

  constructor(device: Device, streamId: number) {
    this._device = device;
    this._streamId = streamId;
  }

  /**
   * Sets value for the specified digital output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform on the channel.
   */
  public async setDigitalOutput(
    channelNumber: number,
    value: DigitalOutputAction
  ): Promise<void> {
    const request: requests.StreamSetDigitalOutputRequest & gateway.Message = {
      ...requests.StreamSetDigitalOutputRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetDigitalOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_digital_output', request);
  }

  /**
   * Sets values for all digital output channels.
   * @param values The type of action to perform on the channel.
   */
  public async setAllDigitalOutputs(
    values: DigitalOutputAction[]
  ): Promise<void> {
    const request: requests.StreamSetAllDigitalOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllDigitalOutputsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      values: values,
      toBinary() {
        return requests.StreamSetAllDigitalOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_digital_outputs', request);
  }

  /**
   * Sets value for the specified analog output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to.
   */
  public async setAnalogOutput(
    channelNumber: number,
    value: number
  ): Promise<void> {
    const request: requests.StreamSetAnalogOutputRequest & gateway.Message = {
      ...requests.StreamSetAnalogOutputRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetAnalogOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_analog_output', request);
  }

  /**
   * Sets values for all analog output channels.
   * @param values Voltage values to set the output channels to.
   */
  public async setAllAnalogOutputs(
    values: number[]
  ): Promise<void> {
    const request: requests.StreamSetAllAnalogOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllAnalogOutputsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      values: values,
      toBinary() {
        return requests.StreamSetAllAnalogOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_analog_outputs', request);
  }

  /**
   * Sets current and future value for the specified digital output channel.
   * Requires at least Firmware 7.37.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform immediately on the channel.
   * @param futureValue The type of action to perform in the future on the channel.
   * @param delay Delay between setting current value and setting future value.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setDigitalOutputSchedule(
    channelNumber: number,
    value: DigitalOutputAction,
    futureValue: DigitalOutputAction,
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.StreamSetDigitalOutputScheduleRequest & gateway.Message = {
      ...requests.StreamSetDigitalOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      futureValue: futureValue,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.StreamSetDigitalOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_digital_output_schedule', request);
  }

  /**
   * Sets current and future values for all digital output channels.
   * Requires at least Firmware 7.37.
   * @param values The type of actions to perform immediately on output channels.
   * @param futureValues The type of actions to perform in the future on output channels.
   * @param delay Delay between setting current values and setting future values.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAllDigitalOutputsSchedule(
    values: DigitalOutputAction[],
    futureValues: DigitalOutputAction[],
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.StreamSetAllDigitalOutputsScheduleRequest & gateway.Message = {
      ...requests.StreamSetAllDigitalOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      values: values,
      futureValues: futureValues,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.StreamSetAllDigitalOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_digital_outputs_schedule', request);
  }

  /**
   * Sets current and future value for the specified analog output channel.
   * Requires at least Firmware 7.38.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to immediately.
   * @param futureValue Value to set the output channel voltage to in the future.
   * @param delay Delay between setting current value and setting future value.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAnalogOutputSchedule(
    channelNumber: number,
    value: number,
    futureValue: number,
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.StreamSetAnalogOutputScheduleRequest & gateway.Message = {
      ...requests.StreamSetAnalogOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      futureValue: futureValue,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.StreamSetAnalogOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_analog_output_schedule', request);
  }

  /**
   * Sets current and future values for all analog output channels.
   * Requires at least Firmware 7.38.
   * @param values Voltage values to set the output channels to immediately.
   * @param futureValues Voltage values to set the output channels to in the future.
   * @param delay Delay between setting current values and setting future values.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAllAnalogOutputsSchedule(
    values: number[],
    futureValues: number[],
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.StreamSetAllAnalogOutputsScheduleRequest & gateway.Message = {
      ...requests.StreamSetAllAnalogOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      streamId: this._streamId,
      pvt: true,
      values: values,
      futureValues: futureValues,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.StreamSetAllAnalogOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_analog_outputs_schedule', request);
  }

  /**
   * Cancels a scheduled digital output action.
   * Requires at least Firmware 7.37.
   * @param channelNumber Channel number starting at 1.
   */
  public async cancelDigitalOutputSchedule(
    channelNumber: number
  ): Promise<void> {
    const request: requests.StreamCancelOutputScheduleRequest & gateway.Message = {
      ...requests.StreamCancelOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: false,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      toBinary() {
        return requests.StreamCancelOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cancel_output_schedule', request);
  }

  /**
   * Cancel all scheduled digital output actions.
   * Requires at least Firmware 7.37.
   * @param [channels=[]] Optionally specify which channels to cancel.
   * Array length must be empty or equal to the number of channels on device.
   * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
   */
  public async cancelAllDigitalOutputsSchedule(
    channels: boolean[] = []
  ): Promise<void> {
    const request: requests.StreamCancelAllOutputsScheduleRequest & gateway.Message = {
      ...requests.StreamCancelAllOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: false,
      streamId: this._streamId,
      pvt: true,
      channels: channels,
      toBinary() {
        return requests.StreamCancelAllOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cancel_all_outputs_schedule', request);
  }

  /**
   * Cancels a scheduled analog output value.
   * Requires at least Firmware 7.38.
   * @param channelNumber Channel number starting at 1.
   */
  public async cancelAnalogOutputSchedule(
    channelNumber: number
  ): Promise<void> {
    const request: requests.StreamCancelOutputScheduleRequest & gateway.Message = {
      ...requests.StreamCancelOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: true,
      streamId: this._streamId,
      pvt: true,
      channelNumber: channelNumber,
      toBinary() {
        return requests.StreamCancelOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cancel_output_schedule', request);
  }

  /**
   * Cancel all scheduled analog output actions.
   * Requires at least Firmware 7.38.
   * @param [channels=[]] Optionally specify which channels to cancel.
   * Array length must be empty or equal to the number of channels on device.
   * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
   */
  public async cancelAllAnalogOutputsSchedule(
    channels: boolean[] = []
  ): Promise<void> {
    const request: requests.StreamCancelAllOutputsScheduleRequest & gateway.Message = {
      ...requests.StreamCancelAllOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: true,
      streamId: this._streamId,
      pvt: true,
      channels: channels,
      toBinary() {
        return requests.StreamCancelAllOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cancel_all_outputs_schedule', request);
  }
}

namespace PvtIo {
}
