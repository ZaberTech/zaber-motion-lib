﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Axis } from './axis';
import { Measurement } from '../measurement';
import { Angle, Length, Native } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Groups multiple axes across devices into a single group to allow for simultaneous movement.
 * Note that the movement is not coordinated and trajectory is inconsistent and not repeatable between calls.
 * Make sure that any possible trajectory is clear of potential obstacles.
 * The movement methods return after all the axes finish the movement successfully
 * or throw an error as soon as possible.
 */
export class AxisGroup {
  /**
   * Axes of the group.
   */
  public get axes(): Axis[] {
    return this._axes;
  }
  private _axes: Axis[];

  /**
   * Initializes the group with the axes to be controlled.
   */
  constructor(axes: Axis[]) {
    this._axes = axes.slice();
  }

  /**
   * Homes the axes.
   */
  public async home(): Promise<void> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/home', request);
  }

  /**
   * Stops the axes.
   */
  public async stop(): Promise<void> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/stop', request);
  }

  /**
   * Moves the axes to absolute position.
   * @param position Position.
   */
  public async moveAbsolute(
    ...position: Measurement[]
  ): Promise<void> {
    const request: requests.AxesMoveRequest & gateway.Message = {
      ...requests.AxesMoveRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      position: position,
      toBinary() {
        return requests.AxesMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/move_absolute', request);
  }

  /**
   * Move axes to position relative to the current position.
   * @param position Position.
   */
  public async moveRelative(
    ...position: Measurement[]
  ): Promise<void> {
    const request: requests.AxesMoveRequest & gateway.Message = {
      ...requests.AxesMoveRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      position: position,
      toBinary() {
        return requests.AxesMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/move_relative', request);
  }

  /**
   * Moves axes to the minimum position as specified by limit.min.
   */
  public async moveMin(): Promise<void> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/move_min', request);
  }

  /**
   * Moves axes to the maximum position as specified by limit.max.
   */
  public async moveMax(): Promise<void> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/move_max', request);
  }

  /**
   * Waits until all the axes stop moving.
   */
  public async waitUntilIdle(): Promise<void> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('axes/wait_until_idle', request);
  }

  /**
   * Returns bool indicating whether any of the axes is executing a motion command.
   * @returns True if any of the axes is currently executing a motion command. False otherwise.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'axes/is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns bool indicating whether all the axes are homed.
   * @returns True if all the axes are homed. False otherwise.
   */
  public async isHomed(): Promise<boolean> {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'axes/is_homed',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns current axes position.
   * The positions are requested sequentially.
   * The result position may not be accurate if the axes are moving.
   * @param unit Units of position. You can specify units once or for each axis separately.
   * @returns Axes position.
   */
  public async getPosition(
    ...unit: (Length | Angle | Native)[]
  ): Promise<number[]> {
    const request: requests.AxesGetSettingRequest & gateway.Message = {
      ...requests.AxesGetSettingRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      setting: 'pos',
      unit: unit,
      toBinary() {
        return requests.AxesGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleArrayResponse>(
      'axes/get_setting',
      request,
      requests.DoubleArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Returns a string that represents the axes.
   * @returns A string that represents the axes.
   */
  public toString(): string {
    const request: requests.AxesEmptyRequest & gateway.Message = {
      ...requests.AxesEmptyRequest.DEFAULT,
      interfaces: this._axes.map(axis => axis.device.connection.interfaceId),
      devices: this._axes.map(axis => axis.device.deviceAddress),
      axes: this._axes.map(axis => axis.axisNumber),
      toBinary() {
        return requests.AxesEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'axes/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace AxisGroup {
}
