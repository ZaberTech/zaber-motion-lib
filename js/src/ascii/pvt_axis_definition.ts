/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { PvtAxisType } from '../ascii/pvt_axis_type';

/**
 * Defines an axis of the PVT sequence.
 */
export interface PvtAxisDefinition {
  /**
   * Number of a physical axis or a lockstep group.
   */
  axisNumber: number;

  /**
   * Defines the type of the axis.
   */
  axisType?: (PvtAxisType | null);

}

export const PvtAxisDefinition = {
  fromBinary: (buffer: Uint8Array): PvtAxisDefinition => BSON.deserialize(buffer) as PvtAxisDefinition,
  toBinary: (value: PvtAxisDefinition): Uint8Array => BSON.serialize(PvtAxisDefinition.sanitize(value)),
  DEFAULT: Object.freeze({
    axisNumber: 0,
    axisType: null,
  }) as Readonly<PvtAxisDefinition>,
  sanitize: (value: PvtAxisDefinition): PvtAxisDefinition => {
    if (value == null) { throw new TypeError('Expected PvtAxisDefinition object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PvtAxisDefinition object but got ${typeof value}.`) }
    return {
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      axisType: value.axisType != null ? sanitizer.sanitizeEnum(value.axisType, 'PvtAxisType', PvtAxisType, 'axisType') : null,
    };
  },
};
