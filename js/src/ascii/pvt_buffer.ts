﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Represents a PVT buffer with this number on a device.
 * A PVT buffer is a place to store a queue of PVT actions.
 */
export class PvtBuffer {
  /**
   * The Device this buffer exists on.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The number identifying the buffer on the device.
   */
  public get bufferId(): number {
    return this._bufferId;
  }
  private _bufferId: number;

  constructor(device: Device, bufferId: number) {
    this._device = device;
    this._bufferId = bufferId;
  }

  /**
   * Gets the buffer contents as an array of strings.
   * @returns A string array containing all the PVT commands stored in the buffer.
   */
  public async getContent(): Promise<string[]> {
    const request: requests.StreamBufferGetContentRequest & gateway.Message = {
      ...requests.StreamBufferGetContentRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      bufferId: this.bufferId,
      pvt: true,
      toBinary() {
        return requests.StreamBufferGetContentRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StreamBufferGetContentResponse>(
      'device/stream_buffer_get_content',
      request,
      requests.StreamBufferGetContentResponse.fromBinary);
    return response.bufferLines;
  }

  /**
   * Erases the contents of the buffer.
   * This method fails if there is a PVT sequence writing to the buffer.
   */
  public async erase(): Promise<void> {
    const request: requests.StreamBufferEraseRequest & gateway.Message = {
      ...requests.StreamBufferEraseRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      bufferId: this.bufferId,
      pvt: true,
      toBinary() {
        return requests.StreamBufferEraseRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_buffer_erase', request);
  }
}
