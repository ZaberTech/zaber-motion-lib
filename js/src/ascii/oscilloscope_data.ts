﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Native, Time, Frequency, Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { registerForFinalization } from '../finalizer';

import { IoPortType } from './io_port_type';
import { OscilloscopeDataSource } from './oscilloscope_data_source';
import { OscilloscopeCaptureProperties } from './oscilloscope_capture_properties';

/**
 * Contains a block of contiguous recorded data for one channel of the device's oscilloscope.
 */
export class OscilloscopeData {
  /**
   * Unique ID for this block of recorded data.
   */
  public get dataId(): number {
    return this._dataId;
  }
  private _dataId: number;

  /**
   * Indicates whether the data came from a setting or an I/O pin.
   */
  public get dataSource(): OscilloscopeDataSource {
    return this._retrieveProperties().dataSource;
  }

  /**
   * The name of the recorded setting.
   */
  public get setting(): string {
    return this._retrieveProperties().setting;
  }

  /**
   * The number of the axis the data was recorded from, or 0 for the controller.
   */
  public get axisNumber(): number {
    return this._retrieveProperties().axisNumber;
  }

  /**
   * Which kind of I/O port data was recorded from.
   */
  public get ioType(): IoPortType {
    return this._retrieveProperties().ioType;
  }

  /**
   * Which I/O pin within the port was recorded.
   */
  public get ioChannel(): number {
    return this._retrieveProperties().ioChannel;
  }

  constructor(dataId: number) {
    this._dataId = dataId;
    registerForFinalization(this, OscilloscopeData._free.bind(null, dataId));
  }

  /**
   * Get the sample interval that this data was recorded with.
   * @param [unit=Units.NATIVE] Unit of measure to represent the timebase in.
   * @returns The timebase setting at the time the data was recorded.
   */
  public getTimebase(
    unit: Time | Native = Units.NATIVE
  ): number {
    const request: requests.OscilloscopeDataGetRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetRequest.DEFAULT,
      dataId: this.dataId,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'oscilloscopedata/get_timebase',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Get the sampling frequency that this data was recorded with.
   * @param [unit=Units.NATIVE] Unit of measure to represent the frequency in.
   * @returns The frequency (inverse of the timebase setting) at the time the data was recorded.
   */
  public getFrequency(
    unit: Frequency | Native = Units.NATIVE
  ): number {
    const request: requests.OscilloscopeDataGetRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetRequest.DEFAULT,
      dataId: this.dataId,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'oscilloscopedata/get_frequency',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Get the user-specified time period between receipt of the start command and the first data point.
   * Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.
   * @param [unit=Units.NATIVE] Unit of measure to represent the delay in.
   * @returns The delay setting at the time the data was recorded.
   */
  public getDelay(
    unit: Time | Native = Units.NATIVE
  ): number {
    const request: requests.OscilloscopeDataGetRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetRequest.DEFAULT,
      dataId: this.dataId,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'oscilloscopedata/get_delay',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Calculate the time a sample was recorded, relative to when the recording was triggered.
   * @param index 0-based index of the sample to calculate the time of.
   * @param [unit=Units.NATIVE] Unit of measure to represent the calculated time in.
   * @returns The calculated time offset of the data sample at the given index.
   */
  public getSampleTime(
    index: number,
    unit: Time | Native = Units.NATIVE
  ): number {
    const request: requests.OscilloscopeDataGetSampleTimeRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetSampleTimeRequest.DEFAULT,
      dataId: this.dataId,
      index: index,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetSampleTimeRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'oscilloscopedata/get_sample_time',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Calculate the time for all samples, relative to when the recording was triggered.
   * @param [unit=Units.NATIVE] Unit of measure to represent the calculated time in.
   * @returns The calculated time offsets of all data samples.
   */
  public getSampleTimes(
    unit: Time | Native = Units.NATIVE
  ): number[] {
    const request: requests.OscilloscopeDataGetSampleTimeRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetSampleTimeRequest.DEFAULT,
      dataId: this.dataId,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetSampleTimeRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleArrayResponse>(
      'oscilloscopedata/get_sample_times',
      request,
      requests.DoubleArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Get the recorded data as an array of doubles, with optional unit conversion.
   * Note that not all quantities can be unit converted.
   * For example, digital I/O channels and pure numbers such as device mode settings have no units.
   * @param [unit=Units.NATIVE] Unit of measure to convert the data to.
   * @returns The recorded data for one oscilloscope channel, converted to the units specified.
   */
  public getData(
    unit: Units = Units.NATIVE
  ): number[] {
    const request: requests.OscilloscopeDataGetRequest & gateway.Message = {
      ...requests.OscilloscopeDataGetRequest.DEFAULT,
      dataId: this.dataId,
      unit: unit,
      toBinary() {
        return requests.OscilloscopeDataGetRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.OscilloscopeDataGetSamplesResponse>(
      'oscilloscopedata/get_samples',
      request,
      requests.OscilloscopeDataGetSamplesResponse.fromBinary);
    return response.data;
  }

  /**
   * Releases native resources of an oscilloscope data buffer.
   * @param dataId The ID of the data buffer to delete.
   */
  private static _free(
    dataId: number
  ): void {
    const request: requests.OscilloscopeDataIdentifier & gateway.Message = {
      ...requests.OscilloscopeDataIdentifier.DEFAULT,
      dataId: dataId,
      toBinary() {
        return requests.OscilloscopeDataIdentifier.toBinary(this);
      },
    };

    gateway.callSync('oscilloscopedata/free', request);
  }

  /**
   * Returns recording properties.
   * @returns Capture properties.
   */
  private _retrieveProperties(): OscilloscopeCaptureProperties {
    const request: requests.OscilloscopeDataIdentifier & gateway.Message = {
      ...requests.OscilloscopeDataIdentifier.DEFAULT,
      dataId: this.dataId,
      toBinary() {
        return requests.OscilloscopeDataIdentifier.toBinary(this);
      },
    };

    const response = gateway.callSync<OscilloscopeCaptureProperties>(
      'oscilloscopedata/get_properties',
      request,
      OscilloscopeCaptureProperties.fromBinary);
    return response;
  }

  /**
   * Releases the native resources of the oscilloscope data.
   * Should only be called if your environment does not support FinalizationRegistry.
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry
   */
  public free(): void {
    OscilloscopeData._free(this.dataId);
  }
}
