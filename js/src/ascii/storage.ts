﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Axis } from './axis';
import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Class providing access to axis storage.
 * Requires at least Firmware 7.30.
 */
export class AxisStorage {
  private _axis: Axis;

  constructor(axis: Axis) {
    this._axis = axis;
  }

  /**
   * Sets the axis value stored at the provided key.
   * @param key Key to set the value at.
   * @param value Value to set.
   * @param [options.encode=false] Whether the stored value should be base64 encoded before being stored.
   * This makes the string unreadable to humans using the ASCII protocol,
   * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
   */
  public async setString(
    key: string,
    value: string,
    options: AxisStorage.SetStringOptions = {}
  ): Promise<void> {
    const {
      encode = false,
    } = options;
    const request: requests.DeviceSetStorageRequest & gateway.Message = {
      ...requests.DeviceSetStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      value: value,
      encode: encode,
      toBinary() {
        return requests.DeviceSetStorageRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage', request);
  }

  /**
   * Gets the axis value stored with the provided key.
   * @param key Key to read the value of.
   * @param [options.decode=false] Whether the stored value should be decoded.
   * Only use this when reading values set by storage.set with "encode" true.
   * @returns Stored value.
   */
  public async getString(
    key: string,
    options: AxisStorage.GetStringOptions = {}
  ): Promise<string> {
    const {
      decode = false,
    } = options;
    const request: requests.DeviceGetStorageRequest & gateway.Message = {
      ...requests.DeviceGetStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      decode: decode,
      toBinary() {
        return requests.DeviceGetStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_storage',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the value at the provided key to the provided number.
   * @param key Key to set the value at.
   * @param value Value to set.
   */
  public async setNumber(
    key: string,
    value: number
  ): Promise<void> {
    const request: requests.DeviceSetStorageNumberRequest & gateway.Message = {
      ...requests.DeviceSetStorageNumberRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      value: value,
      toBinary() {
        return requests.DeviceSetStorageNumberRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage_number', request);
  }

  /**
   * Gets the value at the provided key interpreted as a number.
   * @param key Key to get the value at.
   * @returns Stored value.
   */
  public async getNumber(
    key: string
  ): Promise<number> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_storage_number',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the value at the provided key to the provided boolean.
   * @param key Key to set the value at.
   * @param value Value to set.
   */
  public async setBool(
    key: string,
    value: boolean
  ): Promise<void> {
    const request: requests.DeviceSetStorageBoolRequest & gateway.Message = {
      ...requests.DeviceSetStorageBoolRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      value: value,
      toBinary() {
        return requests.DeviceSetStorageBoolRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage_bool', request);
  }

  /**
   * Gets the value at the provided key interpreted as a boolean.
   * @param key Key to get the value at.
   * @returns Stored value.
   */
  public async getBool(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/get_storage_bool',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Erases the axis value stored at the provided key.
   * @param key Key to erase.
   * @returns A boolean indicating if the key existed.
   */
  public async eraseKey(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/erase_storage',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Lists the axis storage keys matching a given prefix.
   * Omit the prefix to list all the keys.
   * @param options.prefix Optional key prefix.
   * @returns Storage keys matching the given prefix.
   */
  public async listKeys(
    options: AxisStorage.ListKeysOptions = {}
  ): Promise<string[]> {
    const {
      prefix,
    } = options;
    const request: requests.DeviceStorageListKeysRequest & gateway.Message = {
      ...requests.DeviceStorageListKeysRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      prefix: prefix,
      toBinary() {
        return requests.DeviceStorageListKeysRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringArrayResponse>(
      'device/storage_list_keys',
      request,
      requests.StringArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Determines whether a given key exists in axis storage.
   * @param key Key which existence to determine.
   * @returns True indicating that the key exists, false otherwise.
   */
  public async keyExists(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/storage_key_exists',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }
}

namespace AxisStorage {
  export interface SetStringOptions {
      /**
       * Whether the stored value should be base64 encoded before being stored.
       * This makes the string unreadable to humans using the ASCII protocol,
       * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
       */
      encode?: boolean;
  }
  export interface GetStringOptions {
      /**
       * Whether the stored value should be decoded.
       * Only use this when reading values set by storage.set with "encode" true.
       */
      decode?: boolean;
  }
  export interface ListKeysOptions {
      /**
       * Optional key prefix.
       */
      prefix?: string;
  }
}

/**
 * Class providing access to device storage.
 * Requires at least Firmware 7.30.
 */
export class DeviceStorage {
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Sets the device value stored at the provided key.
   * @param key Key to set the value at.
   * @param value Value to set.
   * @param [options.encode=false] Whether the stored value should be base64 encoded before being stored.
   * This makes the string unreadable to humans using the ASCII protocol,
   * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
   */
  public async setString(
    key: string,
    value: string,
    options: DeviceStorage.SetStringOptions = {}
  ): Promise<void> {
    const {
      encode = false,
    } = options;
    const request: requests.DeviceSetStorageRequest & gateway.Message = {
      ...requests.DeviceSetStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      value: value,
      encode: encode,
      toBinary() {
        return requests.DeviceSetStorageRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage', request);
  }

  /**
   * Gets the device value stored with the provided key.
   * @param key Key to read the value of.
   * @param [options.decode=false] Whether the stored value should be decoded.
   * Only use this when reading values set by storage.set with "encode" true.
   * @returns Stored value.
   */
  public async getString(
    key: string,
    options: DeviceStorage.GetStringOptions = {}
  ): Promise<string> {
    const {
      decode = false,
    } = options;
    const request: requests.DeviceGetStorageRequest & gateway.Message = {
      ...requests.DeviceGetStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      decode: decode,
      toBinary() {
        return requests.DeviceGetStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_storage',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the value at the provided key to the provided number.
   * @param key Key to set the value at.
   * @param value Value to set.
   */
  public async setNumber(
    key: string,
    value: number
  ): Promise<void> {
    const request: requests.DeviceSetStorageNumberRequest & gateway.Message = {
      ...requests.DeviceSetStorageNumberRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      value: value,
      toBinary() {
        return requests.DeviceSetStorageNumberRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage_number', request);
  }

  /**
   * Gets the value at the provided key interpreted as a number.
   * @param key Key to get the value at.
   * @returns Stored value.
   */
  public async getNumber(
    key: string
  ): Promise<number> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_storage_number',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the value at the provided key to the provided boolean.
   * @param key Key to set the value at.
   * @param value Value to set.
   */
  public async setBool(
    key: string,
    value: boolean
  ): Promise<void> {
    const request: requests.DeviceSetStorageBoolRequest & gateway.Message = {
      ...requests.DeviceSetStorageBoolRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      value: value,
      toBinary() {
        return requests.DeviceSetStorageBoolRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_storage_bool', request);
  }

  /**
   * Gets the value at the provided key interpreted as a boolean.
   * @param key Key to get the value at.
   * @returns Stored value.
   */
  public async getBool(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/get_storage_bool',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Erases the device value stored at the provided key.
   * @param key Key to erase.
   * @returns A boolean indicating if the key existed.
   */
  public async eraseKey(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/erase_storage',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Lists the device storage keys matching a given prefix.
   * Omit the prefix to list all the keys.
   * @param options.prefix Optional key prefix.
   * @returns Storage keys matching the given prefix.
   */
  public async listKeys(
    options: DeviceStorage.ListKeysOptions = {}
  ): Promise<string[]> {
    const {
      prefix,
    } = options;
    const request: requests.DeviceStorageListKeysRequest & gateway.Message = {
      ...requests.DeviceStorageListKeysRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      prefix: prefix,
      toBinary() {
        return requests.DeviceStorageListKeysRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringArrayResponse>(
      'device/storage_list_keys',
      request,
      requests.StringArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Determines whether a given key exists in device storage.
   * @param key Key which existence to determine.
   * @returns True indicating that the key exists, false otherwise.
   */
  public async keyExists(
    key: string
  ): Promise<boolean> {
    const request: requests.DeviceStorageRequest & gateway.Message = {
      ...requests.DeviceStorageRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      key: key,
      toBinary() {
        return requests.DeviceStorageRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/storage_key_exists',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }
}

namespace DeviceStorage {
  export interface SetStringOptions {
      /**
       * Whether the stored value should be base64 encoded before being stored.
       * This makes the string unreadable to humans using the ASCII protocol,
       * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
       */
      encode?: boolean;
  }
  export interface GetStringOptions {
      /**
       * Whether the stored value should be decoded.
       * Only use this when reading values set by storage.set with "encode" true.
       */
      decode?: boolean;
  }
  export interface ListKeysOptions {
      /**
       * Optional key prefix.
       */
      prefix?: string;
  }
}
