/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParam } from '../ascii/servo_tuning_param';

/**
 * The raw parameters currently saved to a given paramset.
 */
export interface ParamsetInfo {
  /**
   * The tuning algorithm used for this axis.
   */
  type: string;

  /**
   * The version of the tuning algorithm used for this axis.
   */
  version: number;

  /**
   * The raw tuning parameters of this device.
   */
  params: ServoTuningParam[];

}

export const ParamsetInfo = {
  fromBinary: (buffer: Uint8Array): ParamsetInfo => BSON.deserialize(buffer) as ParamsetInfo,
  toBinary: (value: ParamsetInfo): Uint8Array => BSON.serialize(ParamsetInfo.sanitize(value)),
  DEFAULT: Object.freeze({
    type: '',
    version: 0,
    params: [],
  }) as Readonly<ParamsetInfo>,
  sanitize: (value: ParamsetInfo): ParamsetInfo => {
    if (value == null) { throw new TypeError('Expected ParamsetInfo object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ParamsetInfo object but got ${typeof value}.`) }
    return {
      type: sanitizer.sanitizeString(value.type, 'type'),
      version: sanitizer.sanitizeInt(value.version, 'version'),
      params: Array.from(value.params ?? [], item => ServoTuningParam.sanitize(item)),
    };
  },
};
