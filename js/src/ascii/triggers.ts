﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Trigger } from './trigger';
import { TriggerState } from './trigger_state';
import { TriggerEnabledState } from './trigger_enabled_state';

/**
 * Class providing access to device triggers.
 * Please note that the Triggers API is currently an experimental feature.
 * Requires at least Firmware 7.06.
 */
export class Triggers {
  /**
   * Device that these triggers belong to.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Get the number of triggers for this device.
   * @returns Number of triggers for this device.
   */
  public async getNumberOfTriggers(): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'trigger.numtriggers',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'triggers/get_setting',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Get the number of actions for each trigger for this device.
   * @returns Number of actions for each trigger for this device.
   */
  public async getNumberOfActions(): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'trigger.numactions',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'triggers/get_setting',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Get a specific trigger for this device.
   * @param triggerNumber The number of the trigger to control. Trigger numbers start at 1.
   * @returns Trigger instance.
   */
  public getTrigger(
    triggerNumber: number
  ): Trigger {
    if (triggerNumber <= 0) {
      throw new TypeError('Invalid value; triggers are numbered from 1.');
    }
    return new Trigger(this.device, triggerNumber);
  }

  /**
   * Get the state for every trigger for this device.
   * @returns Complete state for every trigger.
   */
  public async getTriggerStates(): Promise<TriggerState[]> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.TriggerStates>(
      'triggers/get_trigger_states',
      request,
      requests.TriggerStates.fromBinary);
    return response.states;
  }

  /**
   * Gets the enabled state for every trigger for this device.
   * @returns Whether triggers are enabled and the number of times they will fire.
   */
  public async getEnabledStates(): Promise<TriggerEnabledState[]> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.TriggerEnabledStates>(
      'triggers/get_enabled_states',
      request,
      requests.TriggerEnabledStates.fromBinary);
    return response.states;
  }

  /**
   * Gets the labels for every trigger for this device.
   * @returns The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.
   */
  public async getAllLabels(): Promise<string[]> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringArrayResponse>(
      'triggers/get_all_labels',
      request,
      requests.StringArrayResponse.fromBinary);
    return response.values;
  }
}
