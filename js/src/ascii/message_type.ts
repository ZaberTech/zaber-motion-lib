/* This file is generated. Do not modify by hand. */
/**
 * Denotes type of the response message.
 * For more information refer to:
 * [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format).
 */
export enum MessageType {
  /** Reply. */
  REPLY = 0,
  /** Info. */
  INFO = 1,
  /** Alert. */
  ALERT = 2,
}
