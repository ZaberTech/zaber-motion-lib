/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { FirmwareVersion } from '../firmware_version';

/**
 * Representation of data gathered during device identification.
 */
export interface DeviceIdentity {
  /**
   * Unique ID of the device hardware.
   */
  deviceId: number;

  /**
   * Serial number of the device.
   */
  serialNumber: number;

  /**
   * Name of the product.
   */
  name: string;

  /**
   * Number of axes this device has.
   */
  axisCount: number;

  /**
   * Version of the firmware.
   */
  firmwareVersion: FirmwareVersion;

  /**
   * The device has hardware modifications.
   */
  isModified: boolean;

  /**
   * The device is an integrated product.
   */
  isIntegrated: boolean;

}

export const DeviceIdentity = {
  fromBinary: (buffer: Uint8Array): DeviceIdentity => BSON.deserialize(buffer) as DeviceIdentity,
  toBinary: (value: DeviceIdentity): Uint8Array => BSON.serialize(DeviceIdentity.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceId: 0,
    serialNumber: 0,
    name: '',
    axisCount: 0,
    firmwareVersion: FirmwareVersion.DEFAULT,
    isModified: false,
    isIntegrated: false,
  }) as Readonly<DeviceIdentity>,
  sanitize: (value: DeviceIdentity): DeviceIdentity => {
    if (value == null) { throw new TypeError('Expected DeviceIdentity object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceIdentity object but got ${typeof value}.`) }
    return {
      deviceId: sanitizer.sanitizeInt(value.deviceId, 'deviceId'),
      serialNumber: sanitizer.sanitizeInt(value.serialNumber, 'serialNumber'),
      name: sanitizer.sanitizeString(value.name, 'name'),
      axisCount: sanitizer.sanitizeInt(value.axisCount, 'axisCount'),
      firmwareVersion: FirmwareVersion.sanitize(value.firmwareVersion),
      isModified: sanitizer.sanitizeBool(value.isModified, 'isModified'),
      isIntegrated: sanitizer.sanitizeBool(value.isIntegrated, 'isIntegrated'),
    };
  },
};
