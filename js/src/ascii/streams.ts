﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Device } from './device';
import { Stream } from './stream';
import { StreamBuffer } from './stream_buffer';

/**
 * Class providing access to device streams.
 * Requires at least Firmware 7.05.
 */
export class Streams {
  /**
   * Device that these streams belong to.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Gets a Stream class instance which allows you to control a particular stream on the device.
   * @param streamId The ID of the stream to control. Stream IDs start at one.
   * @returns Stream instance.
   */
  public getStream(
    streamId: number
  ): Stream {
    if (streamId <= 0) {
      throw new TypeError('Invalid value; streams are numbered from 1.');
    }
    return new Stream(this.device, streamId);
  }

  /**
   * Gets a StreamBuffer class instance which is a handle for a stream buffer on the device.
   * @param streamBufferId The ID of the stream buffer to control. Stream buffer IDs start at one.
   * @returns StreamBuffer instance.
   */
  public getBuffer(
    streamBufferId: number
  ): StreamBuffer {
    if (streamBufferId <= 0) {
      throw new TypeError('Invalid value; stream buffers are numbered from 1.');
    }
    return new StreamBuffer(this.device, streamBufferId);
  }

  /**
   * Get a list of buffer IDs that are currently in use.
   * @returns List of buffer IDs.
   */
  public async listBufferIds(): Promise<number[]> {
    const request: requests.StreamBufferList & gateway.Message = {
      ...requests.StreamBufferList.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      pvt: false,
      toBinary() {
        return requests.StreamBufferList.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntArrayResponse>(
      'device/stream_buffer_list',
      request,
      requests.IntArrayResponse.fromBinary);
    return response.values;
  }
}
