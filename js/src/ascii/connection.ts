﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Observable, ReplaySubject } from 'rxjs';
import { map, filter, takeUntil, take } from 'rxjs/operators';
import * as gateway from '../gateway';
import { events, filterEvent } from '../gateway';
import * as requests from '../requests';
import { Transport } from './transport';
import { Device } from './device';
import { Response } from './response';
import { UnknownResponseEvent } from './unknown_response_event';
import { AlertEvent } from './alert_event';
import { MotionLibException } from '../exceptions';

/**
 * Class representing access to particular connection (serial port, TCP connection).
 */
export class Connection {
  /**
   * Event invoked when a response from a device cannot be matched to any known request.
   */
  public unknownResponse!: Observable<UnknownResponseEvent>;

  /**
   * Event invoked when an alert is received from a device.
   */
  public alert!: Observable<AlertEvent>;

  /**
   * Default baud rate for serial connections.
   */
  public static readonly DEFAULT_BAUD_RATE: number = 115200;

  /**
   * Commands sent over this port are forwarded to the device chain.
   * The bandwidth may be limited as the commands are forwarded over a serial connection.
   */
  public static readonly TCP_PORT_CHAIN: number = 55550;

  /**
   * Local area network share port.
   */
  public static readonly NETWORK_SHARE_PORT: number = 11421;

  /**
   * Commands send over this port are processed only by the device
   * and not forwarded to the rest of the chain.
   * Using this port typically makes the communication faster.
   */
  public static readonly TCP_PORT_DEVICE_ONLY: number = 55551;

  /**
   * The interface ID identifies this Connection instance with the underlying library.
   */
  public get interfaceId(): number {
    return this._interfaceId;
  }
  private _interfaceId: number;

  /**
   * The default timeout, in milliseconds, for a device to respond to a request.
   * Setting the timeout to a too low value may cause request timeout exceptions.
   * The initial value is 1000 (one second).
   */
  public get defaultRequestTimeout(): number {
    return this._retrieveTimeout();
  }

  public set defaultRequestTimeout(value: number) {
    this._changeTimeout(value);
  }

  /**
   * Controls whether outgoing messages contain checksum.
   */
  public get checksumEnabled(): boolean {
    return this._retrieveChecksumEnabled();
  }

  public set checksumEnabled(value: boolean) {
    this._changeChecksumEnabled(value);
  }

  constructor(interfaceId: number) {
    this._interfaceId = interfaceId;
    this._subscribe();
  }

  /**
   * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
   * Zaber Launcher allows sharing of the port between multiple applications,
   * If port sharing is not desirable, use the `direct` parameter.
   * @param portName Name of the port to open.
   * @param [options.baudRate=Connection.DEFAULT_BAUD_RATE] Optional baud rate (defaults to 115200).
   * @param [options.direct=false] If true will connect to the serial port directly,
   * failing if the connection is already opened by a message router instance.
   * @returns An object representing the port.
   */
  public static async openSerialPort(
    portName: string,
    options: Connection.OpenSerialPortOptions = {}
  ): Promise<Connection> {
    const {
      baudRate = Connection.DEFAULT_BAUD_RATE,
      direct = false,
    } = options;
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.SERIAL_PORT,
      portName: portName,
      baudRate: baudRate,
      rejectRoutedConnection: direct,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Opens a TCP connection.
   * @param hostName Hostname or IP address.
   * @param [port=Connection.TCP_PORT_CHAIN] Optional port number (defaults to 55550).
   * @returns An object representing the connection.
   */
  public static async openTcp(
    hostName: string,
    port: number = Connection.TCP_PORT_CHAIN
  ): Promise<Connection> {
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.TCP,
      hostName: hostName,
      port: port,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Opens a connection using a custom transport.
   * @param transport The custom connection transport.
   * @returns An object representing the connection.
   */
  public static async openCustom(
    transport: Transport
  ): Promise<Connection> {
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.CUSTOM,
      transport: transport.transportId,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Opens a secured connection to a cloud connected device chain.
   * Use this method to connect to devices on your account.
   * @param cloudId The cloud ID to connect to.
   * @param [options.token='unauthenticated'] The token to authenticate with. By default the connection will be unauthenticated.
   * @param options.connectionName The name of the connection to open.
   * Can be left empty to default to the only connection present.
   * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
   * @param options.realm The realm to connect to.
   * Can be left empty for the default account realm.
   * @param [options.api='https://api.zaber.io'] The URL of the API to receive connection info from.
   * @returns An object representing the connection.
   */
  public static async openIot(
    cloudId: string,
    options: Connection.OpenIotOptions = {}
  ): Promise<Connection> {
    const {
      token = 'unauthenticated',
      connectionName,
      realm,
      api = 'https://api.zaber.io',
    } = options;
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.IOT,
      cloudId: cloudId,
      token: token,
      connectionName: connectionName,
      realm: realm,
      api: api,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Opens a connection to Zaber Launcher in your Local Area Network.
   * The connection is not secured.
   * @param hostName Hostname or IP address.
   * @param [port=Connection.NETWORK_SHARE_PORT] Port number.
   * @param connectionName The name of the connection to open.
   * Can be left empty to default to the only connection present.
   * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
   * @returns An object representing the connection.
   */
  public static async openNetworkShare(
    hostName: string,
    port: number = Connection.NETWORK_SHARE_PORT,
    connectionName?: string
  ): Promise<Connection> {
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.NETWORK_SHARE,
      hostName: hostName,
      port: port,
      connectionName: connectionName,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OpenInterfaceResponse>(
      'interface/open',
      request,
      requests.OpenInterfaceResponse.fromBinary);
    return new Connection(response.interfaceId);
  }

  /**
   * Sends a generic ASCII command to this connection.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.device=0] Optional device address to send the command to.
   * @param [options.axis=0] Optional axis number to send the command to.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: string,
    options: Connection.GenericCommandOptions = {}
  ): Promise<Response> {
    const {
      device = 0,
      axis = 0,
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: command,
      device: device,
      axis: axis,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Response>(
      'interface/generic_command',
      request,
      Response.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.device=0] Optional device address to send the command to.
   * Specifying -1 omits the number completely.
   * @param [options.axis=0] Optional axis number to send the command to.
   * Specifying -1 omits the number completely.
   */
  public async genericCommandNoResponse(
    command: string,
    options: Connection.GenericCommandNoResponseOptions = {}
  ): Promise<void> {
    const {
      device = 0,
      axis = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: command,
      device: device,
      axis: axis,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Sends a generic ASCII command to this connection and expect multiple responses,
   * either from one device or from many devices.
   * Responses are returned in order of arrival.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.device=0] Optional device address to send the command to.
   * @param [options.axis=0] Optional axis number to send the command to.
   * @param [options.checkErrors=true] Controls whether to throw an exception when a device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: string,
    options: Connection.GenericCommandMultiResponseOptions = {}
  ): Promise<Response[]> {
    const {
      device = 0,
      axis = 0,
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: command,
      device: device,
      axis: axis,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GenericCommandResponseCollection>(
      'interface/generic_command_multi_response',
      request,
      requests.GenericCommandResponseCollection.fromBinary);
    return response.responses;
  }

  /**
   * Enables alerts for all devices on the connection.
   * This will change the "comm.alert" setting to 1 on all supported devices.
   */
  public async enableAlerts(): Promise<void> {
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: 'set comm.alert 1',
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Disables alerts for all devices on the connection.
   * This will change the "comm.alert" setting to 0 on all supported devices.
   */
  public async disableAlerts(): Promise<void> {
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.interfaceId,
      command: 'set comm.alert 0',
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Resets ASCII protocol message IDs. Only for testing purposes.
   */
  public resetIds(): void {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('interface/reset_ids', request);
  }

  /**
   * Close the connection.
   */
  public async close(): Promise<void> {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/close', request);
  }

  /**
   * Gets a Device class instance which allows you to control a particular device on this connection.
   * Devices are numbered from 1.
   * @param deviceAddress Address of device intended to control. Address is configured for each device.
   * @returns Device instance.
   */
  public getDevice(
    deviceAddress: number
  ): Device {
    if (deviceAddress <= 0) {
      throw new TypeError('Invalid value; physical devices are numbered from 1.');
    }
    return new Device(this, deviceAddress);
  }

  /**
   * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
   * @param [options.firstAddress=1] This is the address that the device closest to the computer is given.
   * Remaining devices are numbered consecutively.
   * @returns Total number of devices that responded to the renumber.
   */
  public async renumberDevices(
    options: Connection.RenumberDevicesOptions = {}
  ): Promise<number> {
    const {
      firstAddress = 1,
    } = options;
    if (firstAddress <= 0) {
      throw new TypeError('Invalid value; device addresses are numbered from 1.');
    }
    const request: requests.RenumberRequest & gateway.Message = {
      ...requests.RenumberRequest.DEFAULT,
      interfaceId: this.interfaceId,
      address: firstAddress,
      toBinary() {
        return requests.RenumberRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/renumber_all',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Attempts to detect any devices present on this connection.
   * @param [options.identifyDevices=true] Determines whether device identification should be performed as well.
   * @returns Array of detected devices.
   */
  public async detectDevices(
    options: Connection.DetectDevicesOptions = {}
  ): Promise<Device[]> {
    const {
      identifyDevices = true,
    } = options;
    const request: requests.DeviceDetectRequest & gateway.Message = {
      ...requests.DeviceDetectRequest.DEFAULT,
      interfaceId: this.interfaceId,
      identifyDevices: identifyDevices,
      toBinary() {
        return requests.DeviceDetectRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceDetectResponse>(
      'device/detect',
      request,
      requests.DeviceDetectResponse.fromBinary);
    return response.devices.map(deviceAddress => this.getDevice(deviceAddress));
  }

  /**
   * Forgets all the information associated with devices on the connection.
   * Useful when devices are removed from the chain indefinitely.
   * @param [exceptDevices=[]] Addresses of devices that should not be forgotten.
   */
  public forgetDevices(
    exceptDevices: number[] = []
  ): void {
    const request: requests.ForgetDevicesRequest & gateway.Message = {
      ...requests.ForgetDevicesRequest.DEFAULT,
      interfaceId: this.interfaceId,
      exceptDevices: exceptDevices,
      toBinary() {
        return requests.ForgetDevicesRequest.toBinary(this);
      },
    };

    gateway.callSync('device/forget', request);
  }

  /**
   * Stops all of the devices on this connection.
   * @param [options.waitUntilIdle=true] Determines whether the function should return immediately
   * or wait until the devices are stopped.
   * @returns The addresses of the devices that were stopped by this command.
   */
  public async stopAll(
    options: Connection.StopAllOptions = {}
  ): Promise<number[]> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceOnAllRequest & gateway.Message = {
      ...requests.DeviceOnAllRequest.DEFAULT,
      interfaceId: this.interfaceId,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceOnAllRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceOnAllResponse>(
      'device/stop_all',
      request,
      requests.DeviceOnAllResponse.fromBinary);
    return response.deviceAddresses;
  }

  /**
   * Homes all of the devices on this connection.
   * @param [options.waitUntilIdle=true] Determines whether the function should return immediately
   * or wait until the devices are homed.
   * @returns The addresses of the devices that were homed by this command.
   */
  public async homeAll(
    options: Connection.HomeAllOptions = {}
  ): Promise<number[]> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceOnAllRequest & gateway.Message = {
      ...requests.DeviceOnAllRequest.DEFAULT,
      interfaceId: this.interfaceId,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceOnAllRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceOnAllResponse>(
      'device/home_all',
      request,
      requests.DeviceOnAllResponse.fromBinary);
    return response.deviceAddresses;
  }

  /**
   * Returns a string that represents the connection.
   * @returns A string that represents the connection.
   */
  public toString(): string {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'interface/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns default request timeout.
   * @returns Default request timeout.
   */
  private _retrieveTimeout(): number {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.IntResponse>(
      'interface/get_timeout',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets default request timeout.
   * @param timeout Default request timeout.
   */
  private _changeTimeout(
    timeout: number
  ): void {
    const request: requests.SetInterfaceTimeoutRequest & gateway.Message = {
      ...requests.SetInterfaceTimeoutRequest.DEFAULT,
      interfaceId: this.interfaceId,
      timeout: timeout,
      toBinary() {
        return requests.SetInterfaceTimeoutRequest.toBinary(this);
      },
    };

    gateway.callSync('interface/set_timeout', request);
  }

  /**
   * Returns checksum enabled.
   * @returns Checksum enabled.
   */
  private _retrieveChecksumEnabled(): boolean {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.interfaceId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.BoolResponse>(
      'interface/get_checksum_enabled',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets checksum enabled.
   * @param isEnabled Checksum enabled.
   */
  private _changeChecksumEnabled(
    isEnabled: boolean
  ): void {
    const request: requests.SetInterfaceChecksumEnabledRequest & gateway.Message = {
      ...requests.SetInterfaceChecksumEnabledRequest.DEFAULT,
      interfaceId: this.interfaceId,
      isEnabled: isEnabled,
      toBinary() {
        return requests.SetInterfaceChecksumEnabledRequest.toBinary(this);
      },
    };

    gateway.callSync('interface/set_checksum_enabled', request);
  }

  private _disconnected = new ReplaySubject<MotionLibException>();

  /**
   * Event invoked when connection is interrupted or closed.
   */
  public get disconnected(): Observable<MotionLibException> {
    return this._disconnected;
  }

  private _subscribe(): void {
    this.unknownResponse = events.pipe(
      takeUntil(this._disconnected),
      filterEvent<requests.UnknownResponseEventWrapper>('interface/unknown_response'),
      filter(event => event.interfaceId === this.interfaceId),
      map(event => event.unknownResponse)
    );

    this.alert = events.pipe(
      takeUntil(this._disconnected),
      filterEvent<requests.AlertEventWrapper>('interface/alert'),
      filter(event => event.interfaceId === this.interfaceId),
      map(event => event.alert)
    );

    events.pipe(
      filterEvent<requests.DisconnectedEvent>('interface/disconnected'),
      filter(event => event.interfaceId === this.interfaceId),
      take(1),
      map(({ errorMessage, errorType }) => gateway.convertToException(errorType, errorMessage))
    ).subscribe(this._disconnected);
  }
}

namespace Connection {
  export interface OpenSerialPortOptions {
      /**
       * Optional baud rate (defaults to 115200).
       */
      baudRate?: number;
      /**
       * If true will connect to the serial port directly,
       * failing if the connection is already opened by a message router instance.
       */
      direct?: boolean;
  }
  export interface OpenIotOptions {
      /**
       * The token to authenticate with. By default the connection will be unauthenticated.
       */
      token?: string;
      /**
       * The name of the connection to open.
       * Can be left empty to default to the only connection present.
       * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
       */
      connectionName?: string;
      /**
       * The realm to connect to.
       * Can be left empty for the default account realm.
       */
      realm?: string;
      /**
       * The URL of the API to receive connection info from.
       */
      api?: string;
  }
  export interface GenericCommandOptions {
      /**
       * Optional device address to send the command to.
       */
      device?: number;
      /**
       * Optional axis number to send the command to.
       */
      axis?: number;
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandNoResponseOptions {
      /**
       * Optional device address to send the command to.
       * Specifying -1 omits the number completely.
       */
      device?: number;
      /**
       * Optional axis number to send the command to.
       * Specifying -1 omits the number completely.
       */
      axis?: number;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Optional device address to send the command to.
       */
      device?: number;
      /**
       * Optional axis number to send the command to.
       */
      axis?: number;
      /**
       * Controls whether to throw an exception when a device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface RenumberDevicesOptions {
      /**
       * This is the address that the device closest to the computer is given.
       * Remaining devices are numbered consecutively.
       */
      firstAddress?: number;
  }
  export interface DetectDevicesOptions {
      /**
       * Determines whether device identification should be performed as well.
       */
      identifyDevices?: boolean;
  }
  export interface StopAllOptions {
      /**
       * Determines whether the function should return immediately
       * or wait until the devices are stopped.
       */
      waitUntilIdle?: boolean;
  }
  export interface HomeAllOptions {
      /**
       * Determines whether the function should return immediately
       * or wait until the devices are homed.
       */
      waitUntilIdle?: boolean;
  }
}
