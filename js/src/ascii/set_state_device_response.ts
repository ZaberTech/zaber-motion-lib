/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { SetStateAxisResponse } from '../ascii/set_state_axis_response';

/**
 * An object containing any non-blocking issues encountered when loading a saved state to a device.
 */
export interface SetStateDeviceResponse {
  /**
   * The warnings encountered when applying this state to the given device.
   */
  warnings: string[];

  /**
   * A list of warnings encountered when applying this state to the device's axes.
   */
  axisResponses: SetStateAxisResponse[];

}

export const SetStateDeviceResponse = {
  fromBinary: (buffer: Uint8Array): SetStateDeviceResponse => BSON.deserialize(buffer) as SetStateDeviceResponse,
  toBinary: (value: SetStateDeviceResponse): Uint8Array => BSON.serialize(SetStateDeviceResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    axisResponses: [],
  }) as Readonly<SetStateDeviceResponse>,
  sanitize: (value: SetStateDeviceResponse): SetStateDeviceResponse => {
    if (value == null) { throw new TypeError('Expected SetStateDeviceResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetStateDeviceResponse object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      axisResponses: Array.from(value.axisResponses ?? [], item => SetStateAxisResponse.sanitize(item)),
    };
  },
};
