/* This file is generated. Do not modify by hand. */
/**
 * Servo Tuning Parameter Set to target.
 */
export enum ServoTuningParamset {
  /** Live. */
  LIVE = 0,
  /** P1. */
  P_1 = 1,
  /** P2. */
  P_2 = 2,
  /** P3. */
  P_3 = 3,
  /** P4. */
  P_4 = 4,
  /** P5. */
  P_5 = 5,
  /** P6. */
  P_6 = 6,
  /** P7. */
  P_7 = 7,
  /** P8. */
  P_8 = 8,
  /** P9. */
  P_9 = 9,
  /** Staging. */
  STAGING = 10,
  /** Default. */
  DEFAULT = 11,
}
