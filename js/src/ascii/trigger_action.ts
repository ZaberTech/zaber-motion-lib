/* This file is generated. Do not modify by hand. */
/**
 * Trigger action identifier.
 */
export enum TriggerAction {
  /** All. */
  ALL = 0,
  /** A. */
  A = 1,
  /** B. */
  B = 2,
}
