/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Class representing information on the I/O channels of the device.
 */
export interface DeviceIOInfo {
  /**
   * Number of analog output channels.
   */
  numberAnalogOutputs: number;

  /**
   * Number of analog input channels.
   */
  numberAnalogInputs: number;

  /**
   * Number of digital output channels.
   */
  numberDigitalOutputs: number;

  /**
   * Number of digital input channels.
   */
  numberDigitalInputs: number;

}

export const DeviceIOInfo = {
  fromBinary: (buffer: Uint8Array): DeviceIOInfo => BSON.deserialize(buffer) as DeviceIOInfo,
  toBinary: (value: DeviceIOInfo): Uint8Array => BSON.serialize(DeviceIOInfo.sanitize(value)),
  DEFAULT: Object.freeze({
    numberAnalogOutputs: 0,
    numberAnalogInputs: 0,
    numberDigitalOutputs: 0,
    numberDigitalInputs: 0,
  }) as Readonly<DeviceIOInfo>,
  sanitize: (value: DeviceIOInfo): DeviceIOInfo => {
    if (value == null) { throw new TypeError('Expected DeviceIOInfo object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceIOInfo object but got ${typeof value}.`) }
    return {
      numberAnalogOutputs: sanitizer.sanitizeInt(value.numberAnalogOutputs, 'numberAnalogOutputs'),
      numberAnalogInputs: sanitizer.sanitizeInt(value.numberAnalogInputs, 'numberAnalogInputs'),
      numberDigitalOutputs: sanitizer.sanitizeInt(value.numberDigitalOutputs, 'numberDigitalOutputs'),
      numberDigitalInputs: sanitizer.sanitizeInt(value.numberDigitalInputs, 'numberDigitalInputs'),
    };
  },
};
