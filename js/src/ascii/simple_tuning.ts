/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParam } from '../ascii/servo_tuning_param';

/**
 * The masses and parameters last used by simple tuning.
 */
export interface SimpleTuning {
  /**
   * Whether the tuning returned is currently in use by this paramset,
   * or if it has been overwritten by a later change.
   */
  isUsed: boolean;

  /**
   * The mass of the carriage in kg.
   */
  carriageMass?: (number | null);

  /**
   * The mass of the load in kg, excluding the mass of the carriage.
   */
  loadMass: number;

  /**
   * The parameters used by simple tuning.
   */
  tuningParams: ServoTuningParam[];

}

export const SimpleTuning = {
  fromBinary: (buffer: Uint8Array): SimpleTuning => BSON.deserialize(buffer) as SimpleTuning,
  toBinary: (value: SimpleTuning): Uint8Array => BSON.serialize(SimpleTuning.sanitize(value)),
  DEFAULT: Object.freeze({
    isUsed: false,
    carriageMass: null,
    loadMass: 0,
    tuningParams: [],
  }) as Readonly<SimpleTuning>,
  sanitize: (value: SimpleTuning): SimpleTuning => {
    if (value == null) { throw new TypeError('Expected SimpleTuning object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SimpleTuning object but got ${typeof value}.`) }
    return {
      isUsed: sanitizer.sanitizeBool(value.isUsed, 'isUsed'),
      carriageMass: value.carriageMass != null ? sanitizer.sanitizeNumber(value.carriageMass, 'carriageMass') : null,
      loadMass: sanitizer.sanitizeNumber(value.loadMass, 'loadMass'),
      tuningParams: Array.from(value.tuningParams ?? [], item => ServoTuningParam.sanitize(item)),
    };
  },
};
