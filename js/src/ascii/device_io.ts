﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Native, Units, Time, Frequency } from '../units';
import { DeviceIOInfo } from './device_io_info';
import { DigitalOutputAction } from './digital_output_action';
import { IoPortType } from './io_port_type';
import { IoPortLabel } from './io_port_label';

/**
 * Class providing access to the I/O channels of the device.
 */
export class DeviceIO {
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Returns the number of I/O channels the device has.
   * @returns An object containing the number of I/O channels the device has.
   */
  public async getChannelsInfo(): Promise<DeviceIOInfo> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<DeviceIOInfo>(
      'device/get_io_info',
      request,
      DeviceIOInfo.fromBinary);
    return response;
  }

  /**
   * Sets the label of the specified channel.
   * @param portType The type of channel to set the label of.
   * @param channelNumber Channel number starting at 1.
   * @param label The label to set for the specified channel.
   * If no value or an empty string is provided, this label is deleted.
   */
  public async setLabel(
    portType: IoPortType,
    channelNumber: number,
    label?: string
  ): Promise<void> {
    const request: requests.SetIoPortLabelRequest & gateway.Message = {
      ...requests.SetIoPortLabelRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      portType: portType,
      channelNumber: channelNumber,
      label: label,
      toBinary() {
        return requests.SetIoPortLabelRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_io_label', request);
  }

  /**
   * Returns the label of the specified channel.
   * @param portType The type of channel to get the label of.
   * @param channelNumber Channel number starting at 1.
   * @returns The label of the specified channel.
   */
  public async getLabel(
    portType: IoPortType,
    channelNumber: number
  ): Promise<string> {
    const request: requests.GetIoPortLabelRequest & gateway.Message = {
      ...requests.GetIoPortLabelRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      portType: portType,
      channelNumber: channelNumber,
      toBinary() {
        return requests.GetIoPortLabelRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_io_label',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns every label assigned to an IO port on this device.
   * @returns The labels set for this device's IO.
   */
  public async getAllLabels(): Promise<IoPortLabel[]> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetAllIoPortLabelsResponse>(
      'device/get_all_io_labels',
      request,
      requests.GetAllIoPortLabelsResponse.fromBinary);
    return response.labels;
  }

  /**
   * Returns the current value of the specified digital input channel.
   * @param channelNumber Channel number starting at 1.
   * @returns True if voltage is present on the input channel and false otherwise.
   */
  public async getDigitalInput(
    channelNumber: number
  ): Promise<boolean> {
    const request: requests.DeviceGetDigitalIORequest & gateway.Message = {
      ...requests.DeviceGetDigitalIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'di',
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceGetDigitalIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/get_digital_io',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the current value of the specified digital output channel.
   * @param channelNumber Channel number starting at 1.
   * @returns True if the output channel is conducting and false otherwise.
   */
  public async getDigitalOutput(
    channelNumber: number
  ): Promise<boolean> {
    const request: requests.DeviceGetDigitalIORequest & gateway.Message = {
      ...requests.DeviceGetDigitalIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'do',
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceGetDigitalIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/get_digital_io',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the current value of the specified analog input channel.
   * @param channelNumber Channel number starting at 1.
   * @returns  A measurement of the voltage present on the input channel.
   */
  public async getAnalogInput(
    channelNumber: number
  ): Promise<number> {
    const request: requests.DeviceGetAnalogIORequest & gateway.Message = {
      ...requests.DeviceGetAnalogIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'ai',
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceGetAnalogIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_analog_io',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the current values of the specified analog output channel.
   * @param channelNumber Channel number starting at 1.
   * @returns A measurement of voltage that the output channel is conducting.
   */
  public async getAnalogOutput(
    channelNumber: number
  ): Promise<number> {
    const request: requests.DeviceGetAnalogIORequest & gateway.Message = {
      ...requests.DeviceGetAnalogIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'ao',
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceGetAnalogIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_analog_io',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the current values of all digital input channels.
   * @returns True if voltage is present on the input channel and false otherwise.
   */
  public async getAllDigitalInputs(): Promise<boolean[]> {
    const request: requests.DeviceGetAllDigitalIORequest & gateway.Message = {
      ...requests.DeviceGetAllDigitalIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'di',
      toBinary() {
        return requests.DeviceGetAllDigitalIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetAllDigitalIOResponse>(
      'device/get_all_digital_io',
      request,
      requests.DeviceGetAllDigitalIOResponse.fromBinary);
    return response.values;
  }

  /**
   * Returns the current values of all digital output channels.
   * @returns True if the output channel is conducting and false otherwise.
   */
  public async getAllDigitalOutputs(): Promise<boolean[]> {
    const request: requests.DeviceGetAllDigitalIORequest & gateway.Message = {
      ...requests.DeviceGetAllDigitalIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'do',
      toBinary() {
        return requests.DeviceGetAllDigitalIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetAllDigitalIOResponse>(
      'device/get_all_digital_io',
      request,
      requests.DeviceGetAllDigitalIOResponse.fromBinary);
    return response.values;
  }

  /**
   * Returns the current values of all analog input channels.
   * @returns Measurements of the voltages present on the input channels.
   */
  public async getAllAnalogInputs(): Promise<number[]> {
    const request: requests.DeviceGetAllAnalogIORequest & gateway.Message = {
      ...requests.DeviceGetAllAnalogIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'ai',
      toBinary() {
        return requests.DeviceGetAllAnalogIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetAllAnalogIOResponse>(
      'device/get_all_analog_io',
      request,
      requests.DeviceGetAllAnalogIOResponse.fromBinary);
    return response.values;
  }

  /**
   * Returns the current values of all analog output channels.
   * @returns Measurements of voltage that the output channels are conducting.
   */
  public async getAllAnalogOutputs(): Promise<number[]> {
    const request: requests.DeviceGetAllAnalogIORequest & gateway.Message = {
      ...requests.DeviceGetAllAnalogIORequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelType: 'ao',
      toBinary() {
        return requests.DeviceGetAllAnalogIORequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetAllAnalogIOResponse>(
      'device/get_all_analog_io',
      request,
      requests.DeviceGetAllAnalogIOResponse.fromBinary);
    return response.values;
  }

  /**
   * Sets value for the specified digital output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform on the channel.
   */
  public async setDigitalOutput(
    channelNumber: number,
    value: DigitalOutputAction
  ): Promise<void> {
    const request: requests.DeviceSetDigitalOutputRequest & gateway.Message = {
      ...requests.DeviceSetDigitalOutputRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.DeviceSetDigitalOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_digital_output', request);
  }

  /**
   * Sets values for all digital output channels.
   * @param values The type of action to perform on the channel.
   */
  public async setAllDigitalOutputs(
    values: DigitalOutputAction[]
  ): Promise<void> {
    const request: requests.DeviceSetAllDigitalOutputsRequest & gateway.Message = {
      ...requests.DeviceSetAllDigitalOutputsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      values: values,
      toBinary() {
        return requests.DeviceSetAllDigitalOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_all_digital_outputs', request);
  }

  /**
   * Sets current and future value for the specified digital output channel.
   * Requires at least Firmware 7.37.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform immediately on the channel.
   * @param futureValue The type of action to perform in the future on the channel.
   * @param delay Delay between setting current value and setting future value.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setDigitalOutputSchedule(
    channelNumber: number,
    value: DigitalOutputAction,
    futureValue: DigitalOutputAction,
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.DeviceSetDigitalOutputScheduleRequest & gateway.Message = {
      ...requests.DeviceSetDigitalOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelNumber: channelNumber,
      value: value,
      futureValue: futureValue,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.DeviceSetDigitalOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_digital_output_schedule', request);
  }

  /**
   * Sets current and future values for all digital output channels.
   * Requires at least Firmware 7.37.
   * @param values The type of actions to perform immediately on output channels.
   * @param futureValues The type of actions to perform in the future on output channels.
   * @param delay Delay between setting current values and setting future values.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAllDigitalOutputsSchedule(
    values: DigitalOutputAction[],
    futureValues: DigitalOutputAction[],
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.DeviceSetAllDigitalOutputsScheduleRequest & gateway.Message = {
      ...requests.DeviceSetAllDigitalOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      values: values,
      futureValues: futureValues,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.DeviceSetAllDigitalOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_all_digital_outputs_schedule', request);
  }

  /**
   * Sets value for the specified analog output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to.
   */
  public async setAnalogOutput(
    channelNumber: number,
    value: number
  ): Promise<void> {
    const request: requests.DeviceSetAnalogOutputRequest & gateway.Message = {
      ...requests.DeviceSetAnalogOutputRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.DeviceSetAnalogOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_analog_output', request);
  }

  /**
   * Sets values for all analog output channels.
   * @param values Voltage values to set the output channels to.
   */
  public async setAllAnalogOutputs(
    values: number[]
  ): Promise<void> {
    const request: requests.DeviceSetAllAnalogOutputsRequest & gateway.Message = {
      ...requests.DeviceSetAllAnalogOutputsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      values: values,
      toBinary() {
        return requests.DeviceSetAllAnalogOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_all_analog_outputs', request);
  }

  /**
   * Sets current and future value for the specified analog output channel.
   * Requires at least Firmware 7.38.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to immediately.
   * @param futureValue Value to set the output channel voltage to in the future.
   * @param delay Delay between setting current value and setting future value.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAnalogOutputSchedule(
    channelNumber: number,
    value: number,
    futureValue: number,
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.DeviceSetAnalogOutputScheduleRequest & gateway.Message = {
      ...requests.DeviceSetAnalogOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelNumber: channelNumber,
      value: value,
      futureValue: futureValue,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.DeviceSetAnalogOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_analog_output_schedule', request);
  }

  /**
   * Sets current and future values for all analog output channels.
   * Requires at least Firmware 7.38.
   * @param values Voltage values to set the output channels to immediately.
   * @param futureValues Voltage values to set the output channels to in the future.
   * @param delay Delay between setting current values and setting future values.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async setAllAnalogOutputsSchedule(
    values: number[],
    futureValues: number[],
    delay: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (delay <= 0) {
      throw new TypeError('Delay must be a positive value.');
    }
    const request: requests.DeviceSetAllAnalogOutputsScheduleRequest & gateway.Message = {
      ...requests.DeviceSetAllAnalogOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      values: values,
      futureValues: futureValues,
      delay: delay,
      unit: unit,
      toBinary() {
        return requests.DeviceSetAllAnalogOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_all_analog_outputs_schedule', request);
  }

  /**
   * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
   * Set the frequency to 0 to disable the filter.
   * @param channelNumber Channel number starting at 1.
   * @param cutoffFrequency Cutoff frequency of the low-pass filter.
   * @param [unit=Units.NATIVE] Units of frequency.
   */
  public async setAnalogInputLowpassFilter(
    channelNumber: number,
    cutoffFrequency: number,
    unit: Frequency | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetLowpassFilterRequest & gateway.Message = {
      ...requests.DeviceSetLowpassFilterRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      channelNumber: channelNumber,
      cutoffFrequency: cutoffFrequency,
      unit: unit,
      toBinary() {
        return requests.DeviceSetLowpassFilterRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_lowpass_filter', request);
  }

  /**
   * Cancels a scheduled digital output action.
   * Requires at least Firmware 7.37.
   * @param channelNumber Channel number starting at 1.
   */
  public async cancelDigitalOutputSchedule(
    channelNumber: number
  ): Promise<void> {
    const request: requests.DeviceCancelOutputScheduleRequest & gateway.Message = {
      ...requests.DeviceCancelOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: false,
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceCancelOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/cancel_output_schedule', request);
  }

  /**
   * Cancel all scheduled digital output actions.
   * Requires at least Firmware 7.37.
   * @param [channels=[]] Optionally specify which channels to cancel.
   * Array length must be empty or equal to the number of channels on device.
   * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
   */
  public async cancelAllDigitalOutputsSchedule(
    channels: boolean[] = []
  ): Promise<void> {
    const request: requests.DeviceCancelAllOutputsScheduleRequest & gateway.Message = {
      ...requests.DeviceCancelAllOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: false,
      channels: channels,
      toBinary() {
        return requests.DeviceCancelAllOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/cancel_all_outputs_schedule', request);
  }

  /**
   * Cancels a scheduled analog output value.
   * Requires at least Firmware 7.38.
   * @param channelNumber Channel number starting at 1.
   */
  public async cancelAnalogOutputSchedule(
    channelNumber: number
  ): Promise<void> {
    const request: requests.DeviceCancelOutputScheduleRequest & gateway.Message = {
      ...requests.DeviceCancelOutputScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: true,
      channelNumber: channelNumber,
      toBinary() {
        return requests.DeviceCancelOutputScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/cancel_output_schedule', request);
  }

  /**
   * Cancel all scheduled analog output actions.
   * Requires at least Firmware 7.38.
   * @param [channels=[]] Optionally specify which channels to cancel.
   * Array length must be empty or equal to the number of channels on device.
   * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
   */
  public async cancelAllAnalogOutputsSchedule(
    channels: boolean[] = []
  ): Promise<void> {
    const request: requests.DeviceCancelAllOutputsScheduleRequest & gateway.Message = {
      ...requests.DeviceCancelAllOutputsScheduleRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      analog: true,
      channels: channels,
      toBinary() {
        return requests.DeviceCancelAllOutputsScheduleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/cancel_all_outputs_schedule', request);
  }
}
