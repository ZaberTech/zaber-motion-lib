﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { AxisIdentity } from './axis_identity';
import { AxisType } from './axis_type';
import { Warnings } from './warnings';
import { AxisSettings } from './axis_settings';
import { Angle, Length, Native, AngularVelocity, Velocity, Acceleration, AngularAcceleration, Units, Time } from '../units';
import { Response } from './response';
import { Measurement } from '../measurement';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { AxisStorage } from './storage';
import { SetStateAxisResponse } from './set_state_axis_response';
import { FirmwareVersion } from '../firmware_version';

/**
 * Represents an axis of motion associated with a device.
 */
export class Axis {
  /**
   * Device that controls this axis.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The axis number identifies the axis on the device.
   * The first axis has the number one.
   */
  public get axisNumber(): number {
    return this._axisNumber;
  }
  private _axisNumber: number;

  /**
   * Settings and properties of this axis.
   */
  public get settings(): AxisSettings {
    return this._settings;
  }
  private _settings: AxisSettings;

  /**
   * Key-value storage of this axis.
   * Requires at least Firmware 7.30.
   */
  public get storage(): AxisStorage {
    return this._storage;
  }
  private _storage: AxisStorage;

  /**
   * Warnings and faults of this axis.
   */
  public get warnings(): Warnings {
    return this._warnings;
  }
  private _warnings: Warnings;

  /**
   * Identity of the axis.
   */
  public get identity(): AxisIdentity {
    return this._retrieveIdentity();
  }

  /**
   * Unique ID of the peripheral hardware.
   */
  public get peripheralId(): number {
    return this.identity.peripheralId;
  }

  /**
   * Name of the peripheral.
   */
  public get peripheralName(): string {
    return this.identity.peripheralName;
  }

  /**
   * Serial number of the peripheral, or 0 when not applicable.
   */
  public get peripheralSerialNumber(): number {
    return this.identity.peripheralSerialNumber;
  }

  /**
   * Indicates whether the axis is a peripheral or part of an integrated device.
   */
  public get isPeripheral(): boolean {
    return this.identity.isPeripheral;
  }

  /**
   * Determines the type of an axis and units it accepts.
   */
  public get axisType(): AxisType {
    return this.identity.axisType;
  }

  /**
   * User-assigned label of the peripheral.
   */
  public get label(): string {
    return this._retrieveLabel();
  }

  constructor(device: Device, axisNumber: number) {
    this._device = device;
    this._axisNumber = axisNumber;
    this._settings = new AxisSettings(this);
    this._storage = new AxisStorage(this);
    this._warnings = new Warnings(device, axisNumber);
  }

  /**
   * Homes axis. Axis returns to its homing position.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async home(
    options: Axis.HomeOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceHomeRequest & gateway.Message = {
      ...requests.DeviceHomeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceHomeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/home', request);
  }

  /**
   * Stops ongoing axis movement. Decelerates until zero speed.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async stop(
    options: Axis.StopOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceStopRequest & gateway.Message = {
      ...requests.DeviceStopRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceStopRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stop', request);
  }

  /**
   * Parks the axis in anticipation of turning the power off.
   * It can later be powered on, unparked, and moved without first having to home it.
   */
  public async park(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/park', request);
  }

  /**
   * Unparks axis. Axis will now be able to move.
   */
  public async unpark(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/unpark', request);
  }

  /**
   * Returns bool indicating whether the axis is parked or not.
   * @returns True if the axis is currently parked. False otherwise.
   */
  public async isParked(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/is_parked',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Waits until axis stops moving.
   * @param [options.throwErrorOnFault=true] Determines whether to throw error when fault is observed.
   */
  public async waitUntilIdle(
    options: Axis.WaitUntilIdleOptions = {}
  ): Promise<void> {
    const {
      throwErrorOnFault = true,
    } = options;
    const request: requests.DeviceWaitUntilIdleRequest & gateway.Message = {
      ...requests.DeviceWaitUntilIdleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      throwErrorOnFault: throwErrorOnFault,
      toBinary() {
        return requests.DeviceWaitUntilIdleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/wait_until_idle', request);
  }

  /**
   * Returns bool indicating whether the axis is executing a motion command.
   * @returns True if the axis is currently executing a motion command.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns bool indicating whether the axis has position reference and was homed.
   * @returns True if the axis has position reference and was homed.
   */
  public async isHomed(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/is_homed',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Move axis to absolute position.
   * @param position Absolute position.
   * @param [unit=Units.NATIVE] Units of position.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveAbsolute(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Axis.MoveAbsoluteOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.ABS,
      arg: position,
      unit: unit,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Moves the axis to the maximum position as specified by limit.max.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveMax(
    options: Axis.MoveMaxOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.MAX,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Moves the axis to the minimum position as specified by limit.min.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveMin(
    options: Axis.MoveMinOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.MIN,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Move axis to position relative to current position.
   * @param position Relative position.
   * @param [unit=Units.NATIVE] Units of position.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveRelative(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Axis.MoveRelativeOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.REL,
      arg: position,
      unit: unit,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Begins to move axis at specified speed.
   * @param velocity Movement velocity.
   * @param [unit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveVelocity(
    velocity: number,
    unit: Velocity | AngularVelocity | Native = Units.NATIVE,
    options: Axis.MoveVelocityOptions = {}
  ): Promise<void> {
    const {
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.VEL,
      arg: velocity,
      unit: unit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Returns current axis position.
   * @param [unit=Units.NATIVE] Units of position.
   * @returns Axis position.
   */
  public async getPosition(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      setting: 'pos',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets number of index positions of the axis.
   * @returns Number of index positions.
   */
  public async getNumberOfIndexPositions(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_count',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns current axis index position.
   * @returns Index position starting from 1 or 0 if the position is not an index position.
   */
  public async getIndexPosition(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_position',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Moves the axis to index position.
   * @param index Index position. Index positions are numbered from 1.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveIndex(
    index: number,
    options: Axis.MoveIndexOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      type: requests.AxisMoveType.INDEX,
      argInt: index,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Sends a generic ASCII command to this axis.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: string,
    options: Axis.GenericCommandOptions = {}
  ): Promise<Response> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Response>(
      'interface/generic_command',
      request,
      Response.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this axis and expect multiple responses.
   * Responses are returned in order of arrival.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when a device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: string,
    options: Axis.GenericCommandMultiResponseOptions = {}
  ): Promise<Response[]> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GenericCommandResponseCollection>(
      'interface/generic_command_multi_response',
      request,
      requests.GenericCommandResponseCollection.fromBinary);
    return response.responses;
  }

  /**
   * Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   */
  public async genericCommandNoResponse(
    command: string
  ): Promise<void> {
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      command: command,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Formats parameters into a command and performs unit conversions.
   * Parameters in the command template are denoted by a question mark.
   * Command returned is only valid for this axis and this device.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
   * @param parameters Variable number of command parameters.
   * @returns Command with converted parameters.
   */
  public prepareCommand(
    commandTemplate: string,
    ...parameters: Measurement[]
  ): string {
    const request: requests.PrepareCommandRequest & gateway.Message = {
      ...requests.PrepareCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      commandTemplate: commandTemplate,
      parameters: parameters,
      toBinary() {
        return requests.PrepareCommandRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/prepare_command',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the user-assigned peripheral label.
   * The label is stored on the controller and recognized by other software.
   * @param label Label to set.
   */
  public async setLabel(
    label: string
  ): Promise<void> {
    const request: requests.DeviceSetStorageRequest & gateway.Message = {
      ...requests.DeviceSetStorageRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      value: label,
      toBinary() {
        return requests.DeviceSetStorageRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_label', request);
  }

  /**
   * Gets the peripheral name.
   * @returns The label.
   */
  private _retrieveLabel(): string {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/get_label',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string that represents the axis.
   * @returns A string that represents the axis.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/axis_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a serialization of the current axis state that can be saved and reapplied.
   * @returns A serialization of the current state of the axis.
   */
  public async getState(): Promise<string> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_state',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Applies a saved state to this axis.
   * @param state The state object to apply to this axis.
   * @returns Reports of any issues that were handled, but caused the state to not be exactly restored.
   */
  public async setState(
    state: string
  ): Promise<SetStateAxisResponse> {
    const request: requests.SetStateRequest & gateway.Message = {
      ...requests.SetStateRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      state: state,
      toBinary() {
        return requests.SetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<SetStateAxisResponse>(
      'device/set_axis_state',
      request,
      SetStateAxisResponse.fromBinary);
    return response;
  }

  /**
   * Checks if a state can be applied to this axis.
   * This only covers exceptions that can be determined statically such as mismatches of ID or version,
   * the process of applying the state can still fail when running.
   * @param state The state object to check against.
   * @param options.firmwareVersion The firmware version of the device to apply the state to.
   * Use this to ensure the state will still be compatible after an update.
   * @returns An explanation of why this state cannot be set to this axis.
   */
  public async canSetState(
    state: string,
    options: Axis.CanSetStateOptions = {}
  ): Promise<string | null> {
    const {
      firmwareVersion,
    } = options;
    const request: requests.CanSetStateRequest & gateway.Message = {
      ...requests.CanSetStateRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      state: state,
      firmwareVersion: firmwareVersion,
      toBinary() {
        return requests.CanSetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.CanSetStateAxisResponse>(
      'device/can_set_axis_state',
      request,
      requests.CanSetStateAxisResponse.fromBinary);
    return response.error ?? null;
  }

  /**
   * Returns identity.
   * @returns Axis identity.
   */
  private _retrieveIdentity(): AxisIdentity {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<AxisIdentity>(
      'device/get_axis_identity',
      request,
      AxisIdentity.fromBinary);
    return response;
  }

  /**
   * Disables the driver, which prevents current from being sent to the motor or load.
   * If the driver is already disabled, the driver remains disabled.
   */
  public async driverDisable(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/driver_disable', request);
  }

  /**
   * Attempts to enable the driver repeatedly for the specified timeout.
   * If the driver is already enabled, the driver remains enabled.
   * @param [options.timeout=10] Timeout in seconds. Specify 0 to attempt to enable the driver once.
   */
  public async driverEnable(
    options: Axis.DriverEnableOptions = {}
  ): Promise<void> {
    const {
      timeout = 10,
    } = options;
    const request: requests.DriverEnableRequest & gateway.Message = {
      ...requests.DriverEnableRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      timeout: timeout,
      toBinary() {
        return requests.DriverEnableRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/driver_enable', request);
  }

  /**
   * Activates a peripheral on this axis.
   * Removes all identity information for the device.
   * Run the identify method on the device after activating to refresh the information.
   */
  public async activate(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/activate', request);
  }

  /**
   * Restores all axis settings to their default values.
   * Deletes all zaber axis storage keys.
   * Disables lockstep if the axis is part of one. Unparks the axis.
   * Preserves storage.
   * The device needs to be identified again after the restore.
   */
  public async restore(): Promise<void> {
    const request: requests.DeviceRestoreRequest & gateway.Message = {
      ...requests.DeviceRestoreRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      toBinary() {
        return requests.DeviceRestoreRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/restore', request);
  }

  /**
   * Moves the axis in a sinusoidal trajectory.
   * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
   * @param amplitudeUnits Units of position.
   * @param period Period of the sinusoidal motion in milliseconds.
   * @param periodUnits Units of time.
   * @param [options.count=0] Number of sinusoidal cycles to complete.
   * Must be a multiple of 0.5
   * If count is not specified or set to 0, the axis will move indefinitely.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async moveSin(
    amplitude: number,
    amplitudeUnits: Length | Angle | Native,
    period: number,
    periodUnits: Time | Native,
    options: Axis.MoveSinOptions = {}
  ): Promise<void> {
    const {
      count = 0,
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceMoveSinRequest & gateway.Message = {
      ...requests.DeviceMoveSinRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      amplitude: amplitude,
      amplitudeUnits: amplitudeUnits,
      period: period,
      periodUnits: periodUnits,
      count: count,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceMoveSinRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move_sin', request);
  }

  /**
   * Stops the axis at the end of the sinusoidal trajectory.
   * If the sinusoidal motion was started with an integer-plus-half cycle count,
   * the motion ends at the half-way point of the sinusoidal trajectory.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished.
   */
  public async moveSinStop(
    options: Axis.MoveSinStopOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceStopRequest & gateway.Message = {
      ...requests.DeviceStopRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: this.axisNumber,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceStopRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move_sin_stop', request);
  }
}

namespace Axis {
  export interface HomeOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface StopOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface WaitUntilIdleOptions {
      /**
       * Determines whether to throw error when fault is observed.
       */
      throwErrorOnFault?: boolean;
  }
  export interface MoveAbsoluteOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveMaxOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveMinOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveRelativeOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveVelocityOptions {
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveIndexOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface GenericCommandOptions {
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Controls whether to throw an exception when a device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface CanSetStateOptions {
      /**
       * The firmware version of the device to apply the state to.
       * Use this to ensure the state will still be compatible after an update.
       */
      firmwareVersion?: FirmwareVersion;
  }
  export interface DriverEnableOptions {
      /**
       * Timeout in seconds. Specify 0 to attempt to enable the driver once.
       */
      timeout?: number;
  }
  export interface MoveSinOptions {
      /**
       * Number of sinusoidal cycles to complete.
       * Must be a multiple of 0.5
       * If count is not specified or set to 0, the axis will move indefinitely.
       */
      count?: number;
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface MoveSinStopOptions {
      /**
       * Determines whether function should return after the movement is finished.
       */
      waitUntilIdle?: boolean;
  }
}
