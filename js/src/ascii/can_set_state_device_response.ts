/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { CanSetStateAxisResponse } from '../ascii/can_set_state_axis_response';

/**
 * An object containing any setup issues that will prevent setting a state to a given device.
 */
export interface CanSetStateDeviceResponse {
  /**
   * The error blocking applying this state to the given device.
   */
  error?: (string | null);

  /**
   * A list of errors that block setting state of device's axes.
   */
  axisErrors: CanSetStateAxisResponse[];

}

export const CanSetStateDeviceResponse = {
  fromBinary: (buffer: Uint8Array): CanSetStateDeviceResponse => BSON.deserialize(buffer) as CanSetStateDeviceResponse,
  toBinary: (value: CanSetStateDeviceResponse): Uint8Array => BSON.serialize(CanSetStateDeviceResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    error: null,
    axisErrors: [],
  }) as Readonly<CanSetStateDeviceResponse>,
  sanitize: (value: CanSetStateDeviceResponse): CanSetStateDeviceResponse => {
    if (value == null) { throw new TypeError('Expected CanSetStateDeviceResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CanSetStateDeviceResponse object but got ${typeof value}.`) }
    return {
      error: value.error != null ? sanitizer.sanitizeString(value.error, 'error') : null,
      axisErrors: Array.from(value.axisErrors ?? [], item => CanSetStateAxisResponse.sanitize(item)),
    };
  },
};
