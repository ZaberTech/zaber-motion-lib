/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Alert message received from the device.
 */
export interface AlertEvent {
  /**
   * Number of the device that sent the message.
   */
  deviceAddress: number;

  /**
   * Number of the axis which the response applies to. Zero denotes device scope.
   */
  axisNumber: number;

  /**
   * The device status contains BUSY when the axis is moving and IDLE otherwise.
   */
  status: string;

  /**
   * The warning flag contains the highest priority warning currently active for the device or axis.
   */
  warningFlag: string;

  /**
   * Response data which varies depending on the request.
   */
  data: string;

}

export const AlertEvent = {
  fromBinary: (buffer: Uint8Array): AlertEvent => BSON.deserialize(buffer) as AlertEvent,
  toBinary: (value: AlertEvent): Uint8Array => BSON.serialize(AlertEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddress: 0,
    axisNumber: 0,
    status: '',
    warningFlag: '',
    data: '',
  }) as Readonly<AlertEvent>,
  sanitize: (value: AlertEvent): AlertEvent => {
    if (value == null) { throw new TypeError('Expected AlertEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AlertEvent object but got ${typeof value}.`) }
    return {
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      status: sanitizer.sanitizeString(value.status, 'status'),
      warningFlag: sanitizer.sanitizeString(value.warningFlag, 'warningFlag'),
      data: sanitizer.sanitizeString(value.data, 'data'),
    };
  },
};
