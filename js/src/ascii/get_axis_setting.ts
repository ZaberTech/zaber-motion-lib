/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

/**
 * Specifies a setting to get with one of the multi-get commands.
 */
export interface GetAxisSetting {
  /**
   * The setting to read.
   */
  setting: string;

  /**
   * The unit to convert the read setting to.
   */
  unit?: (Units | null);

}

export const GetAxisSetting = {
  fromBinary: (buffer: Uint8Array): GetAxisSetting => BSON.deserialize(buffer) as GetAxisSetting,
  toBinary: (value: GetAxisSetting): Uint8Array => BSON.serialize(GetAxisSetting.sanitize(value)),
  DEFAULT: Object.freeze({
    setting: '',
    unit: null,
  }) as Readonly<GetAxisSetting>,
  sanitize: (value: GetAxisSetting): GetAxisSetting => {
    if (value == null) { throw new TypeError('Expected GetAxisSetting object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetAxisSetting object but got ${typeof value}.`) }
    return {
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      unit: value.unit != null ? sanitizer.sanitizeUnits(value.unit, 'unit') : null,
    };
  },
};
