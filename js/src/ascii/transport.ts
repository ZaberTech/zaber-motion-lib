﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Connection transport backend allowing to carry Zaber ASCII protocol over arbitrary protocols.
 * Can only be used with a single connection.
 */
export class Transport {
  /**
   * The transport ID identifies this transport instance with the underlying library.
   */
  public get transportId(): number {
    return this._transportId;
  }
  private _transportId: number;

  constructor(transportId: number) {
    this._transportId = transportId;
  }

  /**
   * Creates new instance allowing to read/write messages from/to a single connection.
   * @returns New instance of transport.
   */
  public static open(): Transport {
    const request: requests.EmptyRequest & gateway.Message = {
      ...requests.EmptyRequest.DEFAULT,
      toBinary() {
        return requests.EmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.CustomInterfaceOpenResponse>(
      'custom/interface/open',
      request,
      requests.CustomInterfaceOpenResponse.fromBinary);
    return new Transport(response.transportId);
  }

  /**
   * Closes the transport.
   * Also closes the connection using the transport.
   */
  public async close(): Promise<void> {
    const request: requests.CustomInterfaceCloseRequest & gateway.Message = {
      ...requests.CustomInterfaceCloseRequest.DEFAULT,
      transportId: this.transportId,
      toBinary() {
        return requests.CustomInterfaceCloseRequest.toBinary(this);
      },
    };

    await gateway.callAsync('custom/interface/close', request);
  }

  /**
   * Closes the transport with error.
   * Also closes the connection using the transport propagating the error.
   * @param errorMessage Error to be propagated.
   */
  public async closeWithError(
    errorMessage: string
  ): Promise<void> {
    const request: requests.CustomInterfaceCloseRequest & gateway.Message = {
      ...requests.CustomInterfaceCloseRequest.DEFAULT,
      transportId: this.transportId,
      errorMessage: errorMessage,
      toBinary() {
        return requests.CustomInterfaceCloseRequest.toBinary(this);
      },
    };

    await gateway.callAsync('custom/interface/close', request);
  }

  /**
   * Writes a single message to the connection.
   * The message will be processed as a reply from the device.
   * @param message Single message of Zaber ASCII protocol.
   */
  public async write(
    message: string
  ): Promise<void> {
    const request: requests.CustomInterfaceWriteRequest & gateway.Message = {
      ...requests.CustomInterfaceWriteRequest.DEFAULT,
      transportId: this.transportId,
      message: message,
      toBinary() {
        return requests.CustomInterfaceWriteRequest.toBinary(this);
      },
    };

    await gateway.callAsync('custom/interface/write', request);
  }

  /**
   * Reads a single message generated by the connection.
   * The message is a request for the device.
   * Read should be called continuously in a loop to ensure all generated messages are processed.
   * Subsequent read call confirms that previous message was delivered to the device.
   * @returns Message generated by the connection.
   */
  public async read(): Promise<string> {
    const request: requests.CustomInterfaceReadRequest & gateway.Message = {
      ...requests.CustomInterfaceReadRequest.DEFAULT,
      transportId: this.transportId,
      toBinary() {
        return requests.CustomInterfaceReadRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'custom/interface/read',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}
