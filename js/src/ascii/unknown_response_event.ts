/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { MessageType } from '../ascii/message_type';

/**
 * Reply that could not be matched to a request.
 */
export interface UnknownResponseEvent {
  /**
   * Number of the device that sent the message.
   */
  deviceAddress: number;

  /**
   * Number of the axis which the response applies to. Zero denotes device scope.
   */
  axisNumber: number;

  /**
   * The reply flag indicates if the request was accepted (OK) or rejected (RJ).
   */
  replyFlag: string;

  /**
   * The device status contains BUSY when the axis is moving and IDLE otherwise.
   */
  status: string;

  /**
   * The warning flag contains the highest priority warning currently active for the device or axis.
   */
  warningFlag: string;

  /**
   * Response data which varies depending on the request.
   */
  data: string;

  /**
   * Type of the reply received.
   */
  messageType: MessageType;

}

export const UnknownResponseEvent = {
  fromBinary: (buffer: Uint8Array): UnknownResponseEvent => BSON.deserialize(buffer) as UnknownResponseEvent,
  toBinary: (value: UnknownResponseEvent): Uint8Array => BSON.serialize(UnknownResponseEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddress: 0,
    axisNumber: 0,
    replyFlag: '',
    status: '',
    warningFlag: '',
    data: '',
    messageType: 0 as MessageType,
  }) as Readonly<UnknownResponseEvent>,
  sanitize: (value: UnknownResponseEvent): UnknownResponseEvent => {
    if (value == null) { throw new TypeError('Expected UnknownResponseEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnknownResponseEvent object but got ${typeof value}.`) }
    return {
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      replyFlag: sanitizer.sanitizeString(value.replyFlag, 'replyFlag'),
      status: sanitizer.sanitizeString(value.status, 'status'),
      warningFlag: sanitizer.sanitizeString(value.warningFlag, 'warningFlag'),
      data: sanitizer.sanitizeString(value.data, 'data'),
      messageType: sanitizer.sanitizeEnum(value.messageType, 'MessageType', MessageType, 'messageType'),
    };
  },
};
