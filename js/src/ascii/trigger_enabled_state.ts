/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * The enabled state of a single trigger.
 * Returns whether the given trigger is enabled and the number of times it will fire.
 * This is a subset of the complete state, and is faster to query.
 */
export interface TriggerEnabledState {
  /**
   * The enabled state for a trigger.
   */
  enabled: boolean;

  /**
   * The number of remaining fires for this trigger.
   * A value of -1 indicates unlimited fires remaining.
   */
  firesRemaining: number;

}

export const TriggerEnabledState = {
  fromBinary: (buffer: Uint8Array): TriggerEnabledState => BSON.deserialize(buffer) as TriggerEnabledState,
  toBinary: (value: TriggerEnabledState): Uint8Array => BSON.serialize(TriggerEnabledState.sanitize(value)),
  DEFAULT: Object.freeze({
    enabled: false,
    firesRemaining: 0,
  }) as Readonly<TriggerEnabledState>,
  sanitize: (value: TriggerEnabledState): TriggerEnabledState => {
    if (value == null) { throw new TypeError('Expected TriggerEnabledState object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerEnabledState object but got ${typeof value}.`) }
    return {
      enabled: sanitizer.sanitizeBool(value.enabled, 'enabled'),
      firesRemaining: sanitizer.sanitizeInt(value.firesRemaining, 'firesRemaining'),
    };
  },
};
