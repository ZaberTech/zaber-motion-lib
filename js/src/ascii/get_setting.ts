/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

/**
 * Specifies a setting to get with one of the multi-get commands.
 */
export interface GetSetting {
  /**
   * The setting to read.
   */
  setting: string;

  /**
   * The list of axes to read.
   */
  axes?: (number[] | null);

  /**
   * The unit to convert the read settings to.
   */
  unit?: (Units | null);

}

export const GetSetting = {
  fromBinary: (buffer: Uint8Array): GetSetting => BSON.deserialize(buffer) as GetSetting,
  toBinary: (value: GetSetting): Uint8Array => BSON.serialize(GetSetting.sanitize(value)),
  DEFAULT: Object.freeze({
    setting: '',
    axes: null,
    unit: null,
  }) as Readonly<GetSetting>,
  sanitize: (value: GetSetting): GetSetting => {
    if (value == null) { throw new TypeError('Expected GetSetting object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetSetting object but got ${typeof value}.`) }
    return {
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
      unit: value.unit != null ? sanitizer.sanitizeUnits(value.unit, 'unit') : null,
    };
  },
};
