/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * The axis numbers of a lockstep group.
 */
export interface LockstepAxes {
  /**
   * The axis number used to set the first axis.
   */
  axis1: number;

  /**
   * The axis number used to set the second axis.
   */
  axis2: number;

  /**
   * The axis number used to set the third axis.
   */
  axis3: number;

  /**
   * The axis number used to set the fourth axis.
   */
  axis4: number;

}

export const LockstepAxes = {
  fromBinary: (buffer: Uint8Array): LockstepAxes => BSON.deserialize(buffer) as LockstepAxes,
  toBinary: (value: LockstepAxes): Uint8Array => BSON.serialize(LockstepAxes.sanitize(value)),
  DEFAULT: Object.freeze({
    axis1: 0,
    axis2: 0,
    axis3: 0,
    axis4: 0,
  }) as Readonly<LockstepAxes>,
  sanitize: (value: LockstepAxes): LockstepAxes => {
    if (value == null) { throw new TypeError('Expected LockstepAxes object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepAxes object but got ${typeof value}.`) }
    return {
      axis1: sanitizer.sanitizeInt(value.axis1, 'axis1'),
      axis2: sanitizer.sanitizeInt(value.axis2, 'axis2'),
      axis3: sanitizer.sanitizeInt(value.axis3, 'axis3'),
      axis4: sanitizer.sanitizeInt(value.axis4, 'axis4'),
    };
  },
};
