/* This file is generated. Do not modify by hand. */
/**
 * Kind of channel to record in the Oscilloscope.
 */
export enum OscilloscopeDataSource {
  /** Setting. */
  SETTING = 0,
  /** Io. */
  IO = 1,
}
