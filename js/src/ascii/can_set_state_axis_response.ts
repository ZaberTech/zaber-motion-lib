/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * An object containing any setup issues that will prevent setting a state to a given axis.
 */
export interface CanSetStateAxisResponse {
  /**
   * The error blocking applying this state to the given axis.
   */
  error?: (string | null);

  /**
   * The number of the axis that cannot be set.
   */
  axisNumber: number;

}

export const CanSetStateAxisResponse = {
  fromBinary: (buffer: Uint8Array): CanSetStateAxisResponse => BSON.deserialize(buffer) as CanSetStateAxisResponse,
  toBinary: (value: CanSetStateAxisResponse): Uint8Array => BSON.serialize(CanSetStateAxisResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    error: null,
    axisNumber: 0,
  }) as Readonly<CanSetStateAxisResponse>,
  sanitize: (value: CanSetStateAxisResponse): CanSetStateAxisResponse => {
    if (value == null) { throw new TypeError('Expected CanSetStateAxisResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CanSetStateAxisResponse object but got ${typeof value}.`) }
    return {
      error: value.error != null ? sanitizer.sanitizeString(value.error, 'error') : null,
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
    };
  },
};
