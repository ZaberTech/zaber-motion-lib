﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Device } from './device';
import { PvtSequence } from './pvt_sequence';
import { PvtBuffer } from './pvt_buffer';

/**
 * Class providing access to device PVT (Position-Velocity-Time) features.
 * Requires at least Firmware 7.33.
 */
export class Pvt {
  /**
   * Device that this PVT belongs to.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Gets a PvtSequence class instance which allows you to control a particular PVT sequence on the device.
   * @param pvtId The ID of the PVT sequence to control. The IDs start at 1.
   * @returns PvtSequence instance.
   */
  public getSequence(
    pvtId: number
  ): PvtSequence {
    if (pvtId <= 0) {
      throw new TypeError('Invalid value; PVT sequences are numbered from 1.');
    }
    return new PvtSequence(this.device, pvtId);
  }

  /**
   * Gets a PvtBuffer class instance which is a handle for a PVT buffer on the device.
   * @param pvtBufferId The ID of the PVT buffer to control. PVT buffer IDs start at one.
   * @returns PvtBuffer instance.
   */
  public getBuffer(
    pvtBufferId: number
  ): PvtBuffer {
    if (pvtBufferId <= 0) {
      throw new TypeError('Invalid value; PVT buffers are numbered from 1.');
    }
    return new PvtBuffer(this.device, pvtBufferId);
  }

  /**
   * Get a list of buffer IDs that are currently in use.
   * @returns List of buffer IDs.
   */
  public async listBufferIds(): Promise<number[]> {
    const request: requests.StreamBufferList & gateway.Message = {
      ...requests.StreamBufferList.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      pvt: true,
      toBinary() {
        return requests.StreamBufferList.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntArrayResponse>(
      'device/stream_buffer_list',
      request,
      requests.IntArrayResponse.fromBinary);
    return response.values;
  }
}
