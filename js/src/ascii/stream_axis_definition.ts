/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamAxisType } from '../ascii/stream_axis_type';

/**
 * Defines an axis of the stream.
 */
export interface StreamAxisDefinition {
  /**
   * Number of a physical axis or a lockstep group.
   */
  axisNumber: number;

  /**
   * Defines the type of the axis.
   */
  axisType?: (StreamAxisType | null);

}

export const StreamAxisDefinition = {
  fromBinary: (buffer: Uint8Array): StreamAxisDefinition => BSON.deserialize(buffer) as StreamAxisDefinition,
  toBinary: (value: StreamAxisDefinition): Uint8Array => BSON.serialize(StreamAxisDefinition.sanitize(value)),
  DEFAULT: Object.freeze({
    axisNumber: 0,
    axisType: null,
  }) as Readonly<StreamAxisDefinition>,
  sanitize: (value: StreamAxisDefinition): StreamAxisDefinition => {
    if (value == null) { throw new TypeError('Expected StreamAxisDefinition object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamAxisDefinition object but got ${typeof value}.`) }
    return {
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      axisType: value.axisType != null ? sanitizer.sanitizeEnum(value.axisType, 'StreamAxisType', StreamAxisType, 'axisType') : null,
    };
  },
};
