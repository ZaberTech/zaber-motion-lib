/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * The tuning of this axis represented by PID parameters.
 */
export interface PidTuning {
  /**
   * The tuning algorithm used to tune this axis.
   */
  type: string;

  /**
   * The version of the tuning algorithm used to tune this axis.
   */
  version: number;

  /**
   * The positional tuning argument.
   */
  p: number;

  /**
   * The integral tuning argument.
   */
  i: number;

  /**
   * The derivative tuning argument.
   */
  d: number;

  /**
   * The frequency cutoff for the tuning.
   */
  fc: number;

}

export const PidTuning = {
  fromBinary: (buffer: Uint8Array): PidTuning => BSON.deserialize(buffer) as PidTuning,
  toBinary: (value: PidTuning): Uint8Array => BSON.serialize(PidTuning.sanitize(value)),
  DEFAULT: Object.freeze({
    type: '',
    version: 0,
    p: 0,
    i: 0,
    d: 0,
    fc: 0,
  }) as Readonly<PidTuning>,
  sanitize: (value: PidTuning): PidTuning => {
    if (value == null) { throw new TypeError('Expected PidTuning object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PidTuning object but got ${typeof value}.`) }
    return {
      type: sanitizer.sanitizeString(value.type, 'type'),
      version: sanitizer.sanitizeInt(value.version, 'version'),
      p: sanitizer.sanitizeNumber(value.p, 'p'),
      i: sanitizer.sanitizeNumber(value.i, 'i'),
      d: sanitizer.sanitizeNumber(value.d, 'd'),
      fc: sanitizer.sanitizeNumber(value.fc, 'fc'),
    };
  },
};
