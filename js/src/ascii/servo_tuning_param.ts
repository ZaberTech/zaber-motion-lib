/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * A parameter used to establish the servo tuning of an axis.
 */
export interface ServoTuningParam {
  /**
   * The name of the parameter to set.
   */
  name: string;

  /**
   * The value to use for this parameter.
   */
  value: number;

}

export const ServoTuningParam = {
  fromBinary: (buffer: Uint8Array): ServoTuningParam => BSON.deserialize(buffer) as ServoTuningParam,
  toBinary: (value: ServoTuningParam): Uint8Array => BSON.serialize(ServoTuningParam.sanitize(value)),
  DEFAULT: Object.freeze({
    name: '',
    value: 0,
  }) as Readonly<ServoTuningParam>,
  sanitize: (value: ServoTuningParam): ServoTuningParam => {
    if (value == null) { throw new TypeError('Expected ServoTuningParam object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ServoTuningParam object but got ${typeof value}.`) }
    return {
      name: sanitizer.sanitizeString(value.name, 'name'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
