/* This file is generated. Do not modify by hand. */
/**
 * Denotes type of the PVT sequence axis.
 */
export enum PvtAxisType {
  /** Physical. */
  PHYSICAL = 0,
  /** Lockstep. */
  LOCKSTEP = 1,
}
