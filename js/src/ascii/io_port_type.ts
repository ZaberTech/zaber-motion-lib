/* This file is generated. Do not modify by hand. */
/**
 * Kind of I/O pin to use.
 */
export enum IoPortType {
  /** None. */
  NONE = 0,
  /** AnalogInput. */
  ANALOG_INPUT = 1,
  /** AnalogOutput. */
  ANALOG_OUTPUT = 2,
  /** DigitalInput. */
  DIGITAL_INPUT = 3,
  /** DigitalOutput. */
  DIGITAL_OUTPUT = 4,
}
