/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * An object containing any non-blocking issues encountered when loading a saved state to an axis.
 */
export interface SetStateAxisResponse {
  /**
   * The warnings encountered when applying this state to the given axis.
   */
  warnings: string[];

  /**
   * The number of the axis that was set.
   */
  axisNumber: number;

}

export const SetStateAxisResponse = {
  fromBinary: (buffer: Uint8Array): SetStateAxisResponse => BSON.deserialize(buffer) as SetStateAxisResponse,
  toBinary: (value: SetStateAxisResponse): Uint8Array => BSON.serialize(SetStateAxisResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    warnings: [],
    axisNumber: 0,
  }) as Readonly<SetStateAxisResponse>,
  sanitize: (value: SetStateAxisResponse): SetStateAxisResponse => {
    if (value == null) { throw new TypeError('Expected SetStateAxisResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetStateAxisResponse object but got ${typeof value}.`) }
    return {
      warnings: Array.from(value.warnings ?? [], item => sanitizer.sanitizeString(item, 'items of warnings')),
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
    };
  },
};
