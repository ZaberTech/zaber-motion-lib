﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Represents all axes of motion associated with a device.
 */
export class AllAxes {
  /**
   * Device that controls this axis.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Homes all axes. Axes return to their homing positions.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async home(
    options: AllAxes.HomeOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceHomeRequest & gateway.Message = {
      ...requests.DeviceHomeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceHomeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/home', request);
  }

  /**
   * Stops ongoing axes movement. Decelerates until zero speed.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async stop(
    options: AllAxes.StopOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.DeviceStopRequest & gateway.Message = {
      ...requests.DeviceStopRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.DeviceStopRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stop', request);
  }

  /**
   * Waits until all axes of device stop moving.
   * @param [options.throwErrorOnFault=true] Determines whether to throw error when fault is observed.
   */
  public async waitUntilIdle(
    options: AllAxes.WaitUntilIdleOptions = {}
  ): Promise<void> {
    const {
      throwErrorOnFault = true,
    } = options;
    const request: requests.DeviceWaitUntilIdleRequest & gateway.Message = {
      ...requests.DeviceWaitUntilIdleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      throwErrorOnFault: throwErrorOnFault,
      toBinary() {
        return requests.DeviceWaitUntilIdleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/wait_until_idle', request);
  }

  /**
   * Parks the device in anticipation of turning the power off.
   * It can later be powered on, unparked, and moved without first having to home it.
   */
  public async park(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/park', request);
  }

  /**
   * Unparks the device. The device will now be able to move.
   */
  public async unpark(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/unpark', request);
  }

  /**
   * Returns bool indicating whether any axis is executing a motion command.
   * @returns True if any axis is currently executing a motion command.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns bool indicating whether all axes have position reference and were homed.
   * @returns True if all axes have position reference and were homed.
   */
  public async isHomed(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/is_homed',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Disables all axes drivers, which prevents current from being sent to the motor or load.
   */
  public async driverDisable(): Promise<void> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/driver_disable', request);
  }

  /**
   * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
   * If the driver is already enabled, the driver remains enabled.
   * @param [options.timeout=10] Timeout in seconds. Specify 0 to attempt to enable the driver once.
   */
  public async driverEnable(
    options: AllAxes.DriverEnableOptions = {}
  ): Promise<void> {
    const {
      timeout = 10,
    } = options;
    const request: requests.DriverEnableRequest & gateway.Message = {
      ...requests.DriverEnableRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      timeout: timeout,
      toBinary() {
        return requests.DriverEnableRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/driver_enable', request);
  }

  /**
   * Returns a string that represents the axes.
   * @returns A string that represents the axes.
   */
  public toString(): string {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 0,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/all_axes_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace AllAxes {
  export interface HomeOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface StopOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface WaitUntilIdleOptions {
      /**
       * Determines whether to throw error when fault is observed.
       */
      throwErrorOnFault?: boolean;
  }
  export interface DriverEnableOptions {
      /**
       * Timeout in seconds. Specify 0 to attempt to enable the driver once.
       */
      timeout?: number;
  }
}
