/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Information about a parameter used for the simple tuning method.
 */
export interface SimpleTuningParamDefinition {
  /**
   * The name of the parameter.
   */
  name: string;

  /**
   * The human readable description of the effect of a lower value on this setting.
   */
  minLabel: string;

  /**
   * The human readable description of the effect of a higher value on this setting.
   */
  maxLabel: string;

  /**
   * How this parameter will be parsed by the tuner.
   */
  dataType: string;

  /**
   * The default value of this parameter.
   */
  defaultValue?: (number | null);

}

export const SimpleTuningParamDefinition = {
  fromBinary: (buffer: Uint8Array): SimpleTuningParamDefinition => BSON.deserialize(buffer) as SimpleTuningParamDefinition,
  toBinary: (value: SimpleTuningParamDefinition): Uint8Array => BSON.serialize(SimpleTuningParamDefinition.sanitize(value)),
  DEFAULT: Object.freeze({
    name: '',
    minLabel: '',
    maxLabel: '',
    dataType: '',
    defaultValue: null,
  }) as Readonly<SimpleTuningParamDefinition>,
  sanitize: (value: SimpleTuningParamDefinition): SimpleTuningParamDefinition => {
    if (value == null) { throw new TypeError('Expected SimpleTuningParamDefinition object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SimpleTuningParamDefinition object but got ${typeof value}.`) }
    return {
      name: sanitizer.sanitizeString(value.name, 'name'),
      minLabel: sanitizer.sanitizeString(value.minLabel, 'minLabel'),
      maxLabel: sanitizer.sanitizeString(value.maxLabel, 'maxLabel'),
      dataType: sanitizer.sanitizeString(value.dataType, 'dataType'),
      defaultValue: value.defaultValue != null ? sanitizer.sanitizeNumber(value.defaultValue, 'defaultValue') : null,
    };
  },
};
