﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { GetSetting } from './get_setting';
import { GetSettingResult } from './get_setting_result';
import { Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Class providing access to various device settings and properties.
 */
export class DeviceSettings {
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Returns any device setting or property.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   * @returns Setting value.
   */
  public async get(
    setting: string,
    unit: Units = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets any device setting.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param value Value of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   */
  public async set(
    setting: string,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Returns any device setting or property as a string.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @returns Setting value.
   */
  public async getString(
    setting: string
  ): Promise<string> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_setting_str',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets any device setting as a string.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param value Value of the setting.
   */
  public async setString(
    setting: string,
    value: string
  ): Promise<void> {
    const request: requests.DeviceSetSettingStrRequest & gateway.Message = {
      ...requests.DeviceSetSettingStrRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      value: value,
      toBinary() {
        return requests.DeviceSetSettingStrRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting_str', request);
  }

  /**
   * Convert arbitrary setting value to Zaber native units.
   * @param setting Name of the setting.
   * @param value Value of the setting in units specified by following argument.
   * @param unit Units of the value.
   * @returns Setting value.
   */
  public convertToNativeUnits(
    setting: string,
    value: number,
    unit: Units
  ): number {
    const request: requests.DeviceConvertSettingRequest & gateway.Message = {
      ...requests.DeviceConvertSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceConvertSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/convert_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Convert arbitrary setting value from Zaber native units.
   * @param setting Name of the setting.
   * @param value Value of the setting in Zaber native units.
   * @param unit Units to convert value to.
   * @returns Setting value.
   */
  public convertFromNativeUnits(
    setting: string,
    value: number,
    unit: Units
  ): number {
    const request: requests.DeviceConvertSettingRequest & gateway.Message = {
      ...requests.DeviceConvertSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      fromNative: true,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceConvertSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/convert_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the default value of a setting.
   * @param setting Name of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   * @returns Default setting value.
   */
  public getDefault(
    setting: string,
    unit: Units = Units.NATIVE
  ): number {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/get_setting_default',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the default value of a setting as a string.
   * @param setting Name of the setting.
   * @returns Default setting value.
   */
  public getDefaultString(
    setting: string
  ): string {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/get_setting_default_str',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Indicates if given setting can be converted from and to native units.
   * @param setting Name of the setting.
   * @returns True if unit conversion can be performed.
   */
  public canConvertNativeUnits(
    setting: string
  ): boolean {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.BoolResponse>(
      'device/can_convert_setting',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets the value of an axis scope setting for each axis on the device.
   * Values may be NaN where the setting is not applicable.
   * @param setting Name of the setting.
   * @returns The setting values on each axis.
   */
  public async getFromAllAxes(
    setting: string
  ): Promise<number[]> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleArrayResponse>(
      'device/get_setting_from_all_axes',
      request,
      requests.DoubleArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Gets many setting values in as few device requests as possible.
   * @param settings The settings to read.
   * @returns The setting values read.
   */
  public async getMany(
    ...settings: GetSetting[]
  ): Promise<GetSettingResult[]> {
    const request: requests.DeviceMultiGetSettingRequest & gateway.Message = {
      ...requests.DeviceMultiGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      settings: settings,
      toBinary() {
        return requests.DeviceMultiGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetSettingResults>(
      'device/get_many_settings',
      request,
      requests.GetSettingResults.fromBinary);
    return response.results;
  }

  /**
   * Gets many setting values in the same tick, ensuring their values are synchronized.
   * Requires at least Firmware 7.35.
   * @param settings The settings to read.
   * @returns The setting values read.
   */
  public async getSynchronized(
    ...settings: GetSetting[]
  ): Promise<GetSettingResult[]> {
    const request: requests.DeviceMultiGetSettingRequest & gateway.Message = {
      ...requests.DeviceMultiGetSettingRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      settings: settings,
      toBinary() {
        return requests.DeviceMultiGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetSettingResults>(
      'device/get_sync_settings',
      request,
      requests.GetSettingResults.fromBinary);
    return response.results;
  }
}
