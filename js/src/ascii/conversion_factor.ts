/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

/**
 * Represents unit conversion factor for a single dimension.
 */
export interface ConversionFactor {
  /**
   * Setting representing the dimension.
   */
  setting: string;

  /**
   * Value representing 1 native device unit in specified real-word units.
   */
  value: number;

  /**
   * Units of the value.
   */
  unit: Units;

}

export const ConversionFactor = {
  fromBinary: (buffer: Uint8Array): ConversionFactor => BSON.deserialize(buffer) as ConversionFactor,
  toBinary: (value: ConversionFactor): Uint8Array => BSON.serialize(ConversionFactor.sanitize(value)),
  DEFAULT: Object.freeze({
    setting: '',
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<ConversionFactor>,
  sanitize: (value: ConversionFactor): ConversionFactor => {
    if (value == null) { throw new TypeError('Expected ConversionFactor object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ConversionFactor object but got ${typeof value}.`) }
    return {
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
