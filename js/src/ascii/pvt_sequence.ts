﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Measurement } from '../measurement';
import { PvtBuffer } from './pvt_buffer';
import { PvtMode } from './pvt_mode';
import { PvtAxisDefinition } from './pvt_axis_definition';
import { PvtIo } from './pvt_io';
import { DigitalOutputAction } from './digital_output_action';

/**
 * A handle for a PVT sequence with this number on the device.
 * PVT sequences provide a way execute or store trajectory
 * consisting of points with defined position, velocity, and time.
 * PVT sequence methods append actions to a queue which executes
 * or stores actions in a first in, first out order.
 */
export class PvtSequence {
  /**
   * Device that controls this PVT sequence.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The number that identifies the PVT sequence on the device.
   */
  public get pvtId(): number {
    return this._pvtId;
  }
  private _pvtId: number;

  /**
   * Current mode of the PVT sequence.
   */
  public get mode(): PvtMode {
    return this._retrieveMode();
  }

  /**
   * An array of axes definitions the PVT sequence is set up to control.
   */
  public get axes(): PvtAxisDefinition[] {
    return this._retrieveAxes();
  }

  /**
   * Gets an object that provides access to I/O for this sequence.
   */
  public get io(): PvtIo {
    return this._io;
  }
  private _io: PvtIo;

  constructor(device: Device, pvtId: number) {
    this._device = device;
    this._pvtId = pvtId;
    this._io = new PvtIo(device, pvtId);
  }

  /**
   * Setup the PVT sequence to control the specified axes and to queue actions on the device.
   * Allows use of lockstep axes in a PVT sequence.
   * @param pvtAxes Definition of the PVT sequence axes.
   */
  public async setupLiveComposite(
    ...pvtAxes: PvtAxisDefinition[]
  ): Promise<void> {
    const request: requests.StreamSetupLiveCompositeRequest & gateway.Message = {
      ...requests.StreamSetupLiveCompositeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      pvtAxes: pvtAxes,
      toBinary() {
        return requests.StreamSetupLiveCompositeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_live_composite', request);
  }

  /**
   * Setup the PVT sequence to control the specified axes and to queue actions on the device.
   * @param axes Numbers of physical axes to setup the PVT sequence on.
   */
  public async setupLive(
    ...axes: number[]
  ): Promise<void> {
    const request: requests.StreamSetupLiveRequest & gateway.Message = {
      ...requests.StreamSetupLiveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      axes: axes,
      toBinary() {
        return requests.StreamSetupLiveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_live', request);
  }

  /**
   * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
   * Allows use of lockstep axes in a PVT sequence.
   * @param pvtBuffer The PVT buffer to queue actions in.
   * @param pvtAxes Definition of the PVT sequence axes.
   */
  public async setupStoreComposite(
    pvtBuffer: PvtBuffer,
    ...pvtAxes: PvtAxisDefinition[]
  ): Promise<void> {
    const request: requests.StreamSetupStoreCompositeRequest & gateway.Message = {
      ...requests.StreamSetupStoreCompositeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      pvtBuffer: pvtBuffer.bufferId,
      pvtAxes: pvtAxes,
      toBinary() {
        return requests.StreamSetupStoreCompositeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_store_composite', request);
  }

  /**
   * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
   * @param pvtBuffer The PVT buffer to queue actions in.
   * @param axes Numbers of physical axes to setup the PVT sequence on.
   */
  public async setupStore(
    pvtBuffer: PvtBuffer,
    ...axes: number[]
  ): Promise<void> {
    const request: requests.StreamSetupStoreRequest & gateway.Message = {
      ...requests.StreamSetupStoreRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      pvtBuffer: pvtBuffer.bufferId,
      axes: axes,
      toBinary() {
        return requests.StreamSetupStoreRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_store', request);
  }

  /**
   * Append the actions in a PVT buffer to the sequence's queue.
   * @param pvtBuffer The PVT buffer to call.
   */
  public async call(
    pvtBuffer: PvtBuffer
  ): Promise<void> {
    const request: requests.StreamCallRequest & gateway.Message = {
      ...requests.StreamCallRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      pvtBuffer: pvtBuffer.bufferId,
      toBinary() {
        return requests.StreamCallRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_call', request);
  }

  /**
   * Queues a point with absolute coordinates in the PVT sequence.
   * If some or all velocities are not provided, the sequence calculates the velocities
   * from surrounding points using finite difference.
   * The last point of the sequence must have defined velocity (likely zero).
   * @param positions Positions for the axes to move through, relative to their home positions.
   * @param velocities The axes velocities at the given point.
   * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
   * @param time The duration between the previous point in the sequence and this one.
   */
  public async point(
    positions: Measurement[],
    velocities: (Measurement | null)[],
    time: Measurement
  ): Promise<void> {
    const request: requests.PvtPointRequest & gateway.Message = {
      ...requests.PvtPointRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      type: requests.StreamSegmentType.ABS,
      positions: positions,
      velocities: velocities,
      time: time,
      toBinary() {
        return requests.PvtPointRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_point', request);
  }

  /**
   * Queues a point with coordinates relative to the previous point in the PVT sequence.
   * If some or all velocities are not provided, the sequence calculates the velocities
   * from surrounding points using finite difference.
   * The last point of the sequence must have defined velocity (likely zero).
   * @param positions Positions for the axes to move through, relative to the previous point.
   * @param velocities The axes velocities at the given point.
   * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
   * @param time The duration between the previous point in the sequence and this one.
   */
  public async pointRelative(
    positions: Measurement[],
    velocities: (Measurement | null)[],
    time: Measurement
  ): Promise<void> {
    const request: requests.PvtPointRequest & gateway.Message = {
      ...requests.PvtPointRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      type: requests.StreamSegmentType.REL,
      positions: positions,
      velocities: velocities,
      time: time,
      toBinary() {
        return requests.PvtPointRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_point', request);
  }

  /**
   * Waits until the live PVT sequence executes all queued actions.
   * @param [options.throwErrorOnFault=true] Determines whether to throw error when fault is observed.
   */
  public async waitUntilIdle(
    options: PvtSequence.WaitUntilIdleOptions = {}
  ): Promise<void> {
    const {
      throwErrorOnFault = true,
    } = options;
    const request: requests.StreamWaitUntilIdleRequest & gateway.Message = {
      ...requests.StreamWaitUntilIdleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      throwErrorOnFault: throwErrorOnFault,
      toBinary() {
        return requests.StreamWaitUntilIdleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_wait_until_idle', request);
  }

  /**
   * Cork the front of the PVT sequences's action queue, blocking execution.
   * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
   * Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
   * You can only cork an idle live PVT sequence.
   */
  public async cork(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cork', request);
  }

  /**
   * Uncork the front of the queue, unblocking command execution.
   * You can only uncork an idle live PVT sequence that is corked.
   */
  public async uncork(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_uncork', request);
  }

  /**
   * Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
   * @returns True if the PVT sequence is executing a queued action.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/stream_is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string which represents the PVT sequence.
   * @returns String which represents the PVT sequence.
   */
  public toString(): string {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/stream_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Disables the PVT sequence.
   * If the PVT sequence is not setup, this command does nothing.
   * Once disabled, the PVT sequence will no longer accept PVT commands.
   * The PVT sequence will process the rest of the commands in the queue until it is empty.
   */
  public async disable(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_disable', request);
  }

  /**
   * Sends a generic ASCII command to the PVT sequence.
   * Keeps resending the command while the device rejects with AGAIN reason.
   * @param command Command and its parameters.
   */
  public async genericCommand(
    command: string
  ): Promise<void> {
    const request: requests.StreamGenericCommandRequest & gateway.Message = {
      ...requests.StreamGenericCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      command: command,
      toBinary() {
        return requests.StreamGenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_generic_command', request);
  }

  /**
   * Sends a batch of generic ASCII commands to the PVT sequence.
   * Keeps resending command while the device rejects with AGAIN reason.
   * The batch is atomic in terms of thread safety.
   * @param batch Array of commands.
   */
  public async genericCommandBatch(
    batch: string[]
  ): Promise<void> {
    const request: requests.StreamGenericCommandBatchRequest & gateway.Message = {
      ...requests.StreamGenericCommandBatchRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      batch: batch,
      toBinary() {
        return requests.StreamGenericCommandBatchRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_generic_command_batch', request);
  }

  /**
   * Queries the PVT sequence status from the device
   * and returns boolean indicating whether the PVT sequence is disabled.
   * Useful to determine if execution was interrupted by other movements.
   * @returns True if the PVT sequence is disabled.
   */
  public async checkDisabled(): Promise<boolean> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/stream_check_disabled',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Makes the PVT sequence throw PvtDiscontinuityException when it encounters discontinuities (ND warning flag).
   */
  public treatDiscontinuitiesAsError(): void {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('device/stream_treat_discontinuities', request);
  }

  /**
   * Prevents PvtDiscontinuityException as a result of expected discontinuity when resuming the sequence.
   */
  public ignoreCurrentDiscontinuity(): void {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('device/stream_ignore_discontinuity', request);
  }

  /**
   * Gets the axes of the PVT sequence.
   * @returns An array of axis numbers of the axes the PVT sequence is set up to control.
   */
  private _retrieveAxes(): PvtAxisDefinition[] {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StreamGetAxesResponse>(
      'device/stream_get_axes',
      request,
      requests.StreamGetAxesResponse.fromBinary);
    return response.pvtAxes;
  }

  /**
   * Get the mode of the PVT sequence.
   * @returns Mode of the PVT sequence.
   */
  private _retrieveMode(): PvtMode {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StreamModeResponse>(
      'device/stream_get_mode',
      request,
      requests.StreamModeResponse.fromBinary);
    return response.pvtMode;
  }

  /**
   * @deprecated Use PvtSequence.Io.SetDigitalOutput instead.
   *
   * Sets value for the specified digital output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform on the channel.
   */
  public async setDigitalOutput(
    channelNumber: number,
    value: DigitalOutputAction
  ): Promise<void> {
    const request: requests.StreamSetDigitalOutputRequest & gateway.Message = {
      ...requests.StreamSetDigitalOutputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetDigitalOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_digital_output', request);
  }

  /**
   * @deprecated Use PvtSequence.Io.SetAllDigitalOutputs instead.
   *
   * Sets values for all digital output channels.
   * @param values The type of action to perform on the channel.
   */
  public async setAllDigitalOutputs(
    values: DigitalOutputAction[]
  ): Promise<void> {
    const request: requests.StreamSetAllDigitalOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllDigitalOutputsRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      values: values,
      toBinary() {
        return requests.StreamSetAllDigitalOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_digital_outputs', request);
  }

  /**
   * @deprecated Use PvtSequence.Io.SetAnalogOutput instead.
   *
   * Sets value for the specified analog output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to.
   */
  public async setAnalogOutput(
    channelNumber: number,
    value: number
  ): Promise<void> {
    const request: requests.StreamSetAnalogOutputRequest & gateway.Message = {
      ...requests.StreamSetAnalogOutputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetAnalogOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_analog_output', request);
  }

  /**
   * @deprecated Use PvtSequence.Io.SetAllAnalogOutputs instead.
   *
   * Sets values for all analog output channels.
   * @param values Voltage values to set the output channels to.
   */
  public async setAllAnalogOutputs(
    values: number[]
  ): Promise<void> {
    const request: requests.StreamSetAllAnalogOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllAnalogOutputsRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.pvtId,
      pvt: true,
      values: values,
      toBinary() {
        return requests.StreamSetAllAnalogOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_analog_outputs', request);
  }
}

namespace PvtSequence {
  export interface WaitUntilIdleOptions {
      /**
       * Determines whether to throw error when fault is observed.
       */
      throwErrorOnFault?: boolean;
  }
}
