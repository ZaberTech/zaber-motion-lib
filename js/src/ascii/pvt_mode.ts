/* This file is generated. Do not modify by hand. */
/**
 * Mode of a PVT sequence.
 */
export enum PvtMode {
  /** Disabled. */
  DISABLED = 0,
  /** Store. */
  STORE = 1,
  /** Live. */
  LIVE = 2,
}
