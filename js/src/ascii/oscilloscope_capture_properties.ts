/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { OscilloscopeDataSource } from '../ascii/oscilloscope_data_source';
import { IoPortType } from '../ascii/io_port_type';

/**
 * The public properties of one channel of recorded oscilloscope data.
 */
export interface OscilloscopeCaptureProperties {
  /**
   * Indicates whether the data came from a setting or an I/O pin.
   */
  dataSource: OscilloscopeDataSource;

  /**
   * The name of the recorded setting.
   */
  setting: string;

  /**
   * The number of the axis the data was recorded from, or 0 for the controller.
   */
  axisNumber: number;

  /**
   * Which kind of I/O port data was recorded from.
   */
  ioType: IoPortType;

  /**
   * Which I/O pin within the port was recorded.
   */
  ioChannel: number;

}

export const OscilloscopeCaptureProperties = {
  fromBinary: (buffer: Uint8Array): OscilloscopeCaptureProperties => BSON.deserialize(buffer) as OscilloscopeCaptureProperties,
  toBinary: (value: OscilloscopeCaptureProperties): Uint8Array => BSON.serialize(OscilloscopeCaptureProperties.sanitize(value)),
  DEFAULT: Object.freeze({
    dataSource: 0 as OscilloscopeDataSource,
    setting: '',
    axisNumber: 0,
    ioType: 0 as IoPortType,
    ioChannel: 0,
  }) as Readonly<OscilloscopeCaptureProperties>,
  sanitize: (value: OscilloscopeCaptureProperties): OscilloscopeCaptureProperties => {
    if (value == null) { throw new TypeError('Expected OscilloscopeCaptureProperties object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeCaptureProperties object but got ${typeof value}.`) }
    return {
      dataSource: sanitizer.sanitizeEnum(value.dataSource, 'OscilloscopeDataSource', OscilloscopeDataSource, 'dataSource'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      axisNumber: sanitizer.sanitizeInt(value.axisNumber, 'axisNumber'),
      ioType: sanitizer.sanitizeEnum(value.ioType, 'IoPortType', IoPortType, 'ioType'),
      ioChannel: sanitizer.sanitizeInt(value.ioChannel, 'ioChannel'),
    };
  },
};
