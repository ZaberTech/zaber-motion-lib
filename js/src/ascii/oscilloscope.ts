﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { IoPortType } from './io_port_type';
import { OscilloscopeData } from './oscilloscope_data';
import { Native, Time, Frequency, Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Provides a convenient way to control the oscilloscope data recording feature of some devices.
 * The oscilloscope can record the values of some settings over time at high resolution.
 * Requires at least Firmware 7.00.
 */
export class Oscilloscope {
  /**
   * Device that this Oscilloscope measures.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Select a setting to be recorded.
   * @param axis The 1-based index of the axis to record the value from.
   * @param setting The name of a setting to record.
   */
  public async addChannel(
    axis: number,
    setting: string
  ): Promise<void> {
    const request: requests.OscilloscopeAddSettingChannelRequest & gateway.Message = {
      ...requests.OscilloscopeAddSettingChannelRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: axis,
      setting: setting,
      toBinary() {
        return requests.OscilloscopeAddSettingChannelRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/add_setting_channel', request);
  }

  /**
   * Select an I/O pin to be recorded.
   * Requires at least Firmware 7.33.
   * @param ioType The I/O port type to read data from.
   * @param ioChannel The 1-based index of the I/O pin to read from.
   */
  public async addIoChannel(
    ioType: IoPortType,
    ioChannel: number
  ): Promise<void> {
    const request: requests.OscilloscopeAddIoChannelRequest & gateway.Message = {
      ...requests.OscilloscopeAddIoChannelRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      ioType: ioType,
      ioChannel: ioChannel,
      toBinary() {
        return requests.OscilloscopeAddIoChannelRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/add_io_channel', request);
  }

  /**
   * Clear the list of channels to record.
   */
  public async clear(): Promise<void> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/clear_channels', request);
  }

  /**
   * Get the current sampling interval.
   * @param [unit=Units.NATIVE] Unit of measure to represent the timebase in.
   * @returns The current sampling interval in the selected time units.
   */
  public async getTimebase(
    unit: Time | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.timebase',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Set the sampling interval.
   * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
   * @param [unit=Units.NATIVE] Unit of measure the timebase is represented in.
   */
  public async setTimebase(
    interval: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.timebase',
      value: interval,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Get the current sampling frequency.
   * The values is calculated as the inverse of the current sampling interval.
   * @param [unit=Units.NATIVE] Unit of measure to represent the frequency in.
   * @returns The inverse of current sampling interval in the selected units.
   */
  public async getFrequency(
    unit: Frequency | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.timebase',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'oscilloscope/get_frequency',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Set the sampling frequency (inverse of the sampling interval).
   * The value is quantized to the next closest value supported by the firmware.
   * @param frequency Sample frequency for the next oscilloscope recording.
   * @param [unit=Units.NATIVE] Unit of measure the frequency is represented in.
   */
  public async setFrequency(
    frequency: number,
    unit: Frequency | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.timebase',
      value: frequency,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/set_frequency', request);
  }

  /**
   * Get the delay before oscilloscope recording starts.
   * @param [unit=Units.NATIVE] Unit of measure to represent the delay in.
   * @returns The current start delay in the selected time units.
   */
  public async getDelay(
    unit: Time | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.delay',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Set the sampling start delay.
   * @param interval Delay time between triggering a recording and the first data point being recorded.
   * @param [unit=Units.NATIVE] Unit of measure the delay is represented in.
   */
  public async setDelay(
    interval: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.delay',
      value: interval,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Get the maximum number of channels that can be recorded.
   * @returns The maximum number of channels that can be added to an Oscilloscope recording.
   */
  public async getMaxChannels(): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.numchannels',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'oscilloscope/get_setting',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Get the maximum number of samples that can be recorded per Oscilloscope channel.
   * @returns The maximum number of samples that can be recorded per Oscilloscope channel.
   */
  public async getMaxBufferSize(): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.channel.size.max',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'oscilloscope/get_setting',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Get the number of samples that can be recorded per channel given the current number of channels added.
   * @returns Number of samples that will be recorded per channel with the current channels. Zero if none have been added.
   */
  public async getBufferSize(): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      setting: 'scope.channel.size',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'oscilloscope/get_setting',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Trigger data recording.
   * @param [captureLength=0] Optional number of samples to record per channel.
   * If left empty, the device records samples until the buffer fills.
   * Requires at least Firmware 7.29.
   */
  public async start(
    captureLength: number = 0
  ): Promise<void> {
    const request: requests.OscilloscopeStartRequest & gateway.Message = {
      ...requests.OscilloscopeStartRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      captureLength: captureLength,
      toBinary() {
        return requests.OscilloscopeStartRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/start', request);
  }

  /**
   * End data recording if currently in progress.
   */
  public async stop(): Promise<void> {
    const request: requests.OscilloscopeRequest & gateway.Message = {
      ...requests.OscilloscopeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.OscilloscopeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('oscilloscope/stop', request);
  }

  /**
   * Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
   * @returns Array of recorded channel data arrays, in the order added.
   */
  public async read(): Promise<OscilloscopeData[]> {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.OscilloscopeReadResponse>(
      'oscilloscope/read',
      request,
      requests.OscilloscopeReadResponse.fromBinary);
    return response.dataIds.map(id => new OscilloscopeData(id));
  }
}
