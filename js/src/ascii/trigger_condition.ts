/* This file is generated. Do not modify by hand. */
/**
 * Comparison operator for trigger condition.
 */
export enum TriggerCondition {
  /** EQ. */
  EQ = 0,
  /** NE. */
  NE = 1,
  /** GT. */
  GT = 2,
  /** GE. */
  GE = 3,
  /** LT. */
  LT = 4,
  /** LE. */
  LE = 5,
}
