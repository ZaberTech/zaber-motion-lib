﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { Angle, Length, Native, AngularVelocity, Velocity, Acceleration, AngularAcceleration, Units, Time } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { LockstepAxes } from './lockstep_axes';

/**
 * Represents a lockstep group with this ID on a device.
 * A lockstep group is a movement synchronized pair of axes on a device.
 * Requires at least Firmware 6.15 or 7.11.
 */
export class Lockstep {
  /**
   * Device that controls this lockstep group.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The number that identifies the lockstep group on the device.
   */
  public get lockstepGroupId(): number {
    return this._lockstepGroupId;
  }
  private _lockstepGroupId: number;

  constructor(device: Device, lockstepGroupId: number) {
    this._device = device;
    this._lockstepGroupId = lockstepGroupId;
  }

  /**
   * Activate the lockstep group on the axes specified.
   * @param axes The numbers of axes in the lockstep group.
   */
  public async enable(
    ...axes: number[]
  ): Promise<void> {
    const request: requests.LockstepEnableRequest & gateway.Message = {
      ...requests.LockstepEnableRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      axes: axes,
      toBinary() {
        return requests.LockstepEnableRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_enable', request);
  }

  /**
   * Disable the lockstep group.
   */
  public async disable(): Promise<void> {
    const request: requests.LockstepDisableRequest & gateway.Message = {
      ...requests.LockstepDisableRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepDisableRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_disable', request);
  }

  /**
   * Stops ongoing lockstep group movement. Decelerates until zero speed.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async stop(
    options: Lockstep.StopOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.LockstepStopRequest & gateway.Message = {
      ...requests.LockstepStopRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.LockstepStopRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_stop', request);
  }

  /**
   * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async home(
    options: Lockstep.HomeOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.LockstepHomeRequest & gateway.Message = {
      ...requests.LockstepHomeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.LockstepHomeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_home', request);
  }

  /**
   * Move the first axis of the lockstep group to an absolute position.
   * The other axes in the lockstep group maintain their offsets throughout movement.
   * @param position Absolute position.
   * @param [unit=Units.NATIVE] Units of position.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveAbsolute(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Lockstep.MoveAbsoluteOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.LockstepMoveRequest & gateway.Message = {
      ...requests.LockstepMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      type: requests.AxisMoveType.ABS,
      arg: position,
      unit: unit,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.LockstepMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move', request);
  }

  /**
   * Move the first axis of the lockstep group to a position relative to its current position.
   * The other axes in the lockstep group maintain their offsets throughout movement.
   * @param position Relative position.
   * @param [unit=Units.NATIVE] Units of position.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveRelative(
    position: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Lockstep.MoveRelativeOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.LockstepMoveRequest & gateway.Message = {
      ...requests.LockstepMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      type: requests.AxisMoveType.REL,
      arg: position,
      unit: unit,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.LockstepMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move', request);
  }

  /**
   * Moves the first axis of the lockstep group at the specified speed.
   * The other axes in the lockstep group maintain their offsets throughout movement.
   * @param velocity Movement velocity.
   * @param [unit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveVelocity(
    velocity: number,
    unit: Velocity | AngularVelocity | Native = Units.NATIVE,
    options: Lockstep.MoveVelocityOptions = {}
  ): Promise<void> {
    const {
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.LockstepMoveRequest & gateway.Message = {
      ...requests.LockstepMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      type: requests.AxisMoveType.VEL,
      arg: velocity,
      unit: unit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.LockstepMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move', request);
  }

  /**
   * Moves the first axis of the lockstep group in a sinusoidal trajectory.
   * The other axes in the lockstep group maintain their offsets throughout movement.
   * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
   * @param amplitudeUnits Units of position.
   * @param period Period of the sinusoidal motion in milliseconds.
   * @param periodUnits Units of time.
   * @param [options.count=0] Number of sinusoidal cycles to complete.
   * Must be a multiple of 0.5
   * If count is not specified or set to 0, the axis will move indefinitely.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   */
  public async moveSin(
    amplitude: number,
    amplitudeUnits: Length | Angle | Native,
    period: number,
    periodUnits: Time | Native,
    options: Lockstep.MoveSinOptions = {}
  ): Promise<void> {
    const {
      count = 0,
      waitUntilIdle = true,
    } = options;
    const request: requests.LockstepMoveSinRequest & gateway.Message = {
      ...requests.LockstepMoveSinRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      amplitude: amplitude,
      amplitudeUnits: amplitudeUnits,
      period: period,
      periodUnits: periodUnits,
      count: count,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.LockstepMoveSinRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move_sin', request);
  }

  /**
   * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
   * If the sinusoidal motion was started with an integer-plus-half cycle count,
   * the motion ends at the half-way point of the sinusoidal trajectory.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished.
   */
  public async moveSinStop(
    options: Lockstep.MoveSinStopOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
    } = options;
    const request: requests.LockstepStopRequest & gateway.Message = {
      ...requests.LockstepStopRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      waitUntilIdle: waitUntilIdle,
      toBinary() {
        return requests.LockstepStopRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move_sin_stop', request);
  }

  /**
   * Moves the axes to the maximum valid position.
   * The axes in the lockstep group maintain their offsets throughout movement.
   * Respects lim.max for all axes in the group.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveMax(
    options: Lockstep.MoveMaxOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.LockstepMoveRequest & gateway.Message = {
      ...requests.LockstepMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      type: requests.AxisMoveType.MAX,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.LockstepMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move', request);
  }

  /**
   * Moves the axes to the minimum valid position.
   * The axes in the lockstep group maintain their offsets throughout movement.
   * Respects lim.min for all axes in the group.
   * @param [options.waitUntilIdle=true] Determines whether function should return after the movement is finished or just started.
   * @param [options.velocity=0] Movement velocity.
   * Default value of 0 indicates that the maxspeed setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.velocityUnit=Units.NATIVE] Units of velocity.
   * @param [options.acceleration=0] Movement acceleration.
   * Default value of 0 indicates that the accel setting is used instead.
   * Requires at least Firmware 7.25.
   * @param [options.accelerationUnit=Units.NATIVE] Units of acceleration.
   */
  public async moveMin(
    options: Lockstep.MoveMinOptions = {}
  ): Promise<void> {
    const {
      waitUntilIdle = true,
      velocity = 0,
      velocityUnit = Units.NATIVE,
      acceleration = 0,
      accelerationUnit = Units.NATIVE,
    } = options;
    const request: requests.LockstepMoveRequest & gateway.Message = {
      ...requests.LockstepMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      type: requests.AxisMoveType.MIN,
      waitUntilIdle: waitUntilIdle,
      velocity: velocity,
      velocityUnit: velocityUnit,
      acceleration: acceleration,
      accelerationUnit: accelerationUnit,
      toBinary() {
        return requests.LockstepMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_move', request);
  }

  /**
   * Waits until the lockstep group stops moving.
   * @param [options.throwErrorOnFault=true] Determines whether to throw error when fault is observed.
   */
  public async waitUntilIdle(
    options: Lockstep.WaitUntilIdleOptions = {}
  ): Promise<void> {
    const {
      throwErrorOnFault = true,
    } = options;
    const request: requests.LockstepWaitUntilIdleRequest & gateway.Message = {
      ...requests.LockstepWaitUntilIdleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      throwErrorOnFault: throwErrorOnFault,
      toBinary() {
        return requests.LockstepWaitUntilIdleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_wait_until_idle', request);
  }

  /**
   * Returns bool indicating whether the lockstep group is executing a motion command.
   * @returns True if the axes are currently executing a motion command.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.LockstepEmptyRequest & gateway.Message = {
      ...requests.LockstepEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/lockstep_is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * @deprecated Use GetAxisNumbers instead.
   *
   * Gets the axes of the lockstep group.
   * @returns LockstepAxes instance which contains the axes numbers of the lockstep group.
   */
  public async getAxes(): Promise<LockstepAxes> {
    const request: requests.LockstepEmptyRequest & gateway.Message = {
      ...requests.LockstepEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<LockstepAxes>(
      'device/lockstep_get_axes',
      request,
      LockstepAxes.fromBinary);
    return response;
  }

  /**
   * Gets the axis numbers of the lockstep group.
   * @returns Axis numbers in order specified when enabling lockstep.
   */
  public async getAxisNumbers(): Promise<number[]> {
    const request: requests.LockstepEmptyRequest & gateway.Message = {
      ...requests.LockstepEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.LockstepGetAxisNumbersResponse>(
      'device/lockstep_get_axis_numbers',
      request,
      requests.LockstepGetAxisNumbersResponse.fromBinary);
    return response.axes;
  }

  /**
   * Gets the initial offsets of secondary axes of an enabled lockstep group.
   * @param [unit=Units.NATIVE] Units of position.
   * Uses primary axis unit conversion.
   * @returns Initial offset for each axis of the lockstep group.
   */
  public async getOffsets(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number[]> {
    const request: requests.LockstepGetRequest & gateway.Message = {
      ...requests.LockstepGetRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      unit: unit,
      toBinary() {
        return requests.LockstepGetRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleArrayResponse>(
      'device/lockstep_get_offsets',
      request,
      requests.DoubleArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Gets the twists of secondary axes of an enabled lockstep group.
   * @param [unit=Units.NATIVE] Units of position.
   * Uses primary axis unit conversion.
   * @returns Difference between the initial offset and the actual offset for each axis of the lockstep group.
   */
  public async getTwists(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number[]> {
    const request: requests.LockstepGetRequest & gateway.Message = {
      ...requests.LockstepGetRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      unit: unit,
      toBinary() {
        return requests.LockstepGetRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleArrayResponse>(
      'device/lockstep_get_twists',
      request,
      requests.DoubleArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Returns current position of the primary axis.
   * @param [unit=Units.NATIVE] Units of the position.
   * @returns Primary axis position.
   */
  public async getPosition(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.LockstepGetRequest & gateway.Message = {
      ...requests.LockstepGetRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      unit: unit,
      toBinary() {
        return requests.LockstepGetRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/lockstep_get_pos',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets lockstep twist tolerance.
   * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
   * @param tolerance Twist tolerance.
   * @param [unit=Units.NATIVE] Units of the tolerance.
   * Uses primary axis unit conversion when setting to all axes,
   * otherwise uses specified secondary axis unit conversion.
   * @param [options.axisIndex=0] Optional index of a secondary axis to set the tolerance for.
   * If left empty or set to 0, the tolerance is set to all the secondary axes.
   */
  public async setTolerance(
    tolerance: number,
    unit: Length | Angle | Native = Units.NATIVE,
    options: Lockstep.SetToleranceOptions = {}
  ): Promise<void> {
    const {
      axisIndex = 0,
    } = options;
    const request: requests.LockstepSetRequest & gateway.Message = {
      ...requests.LockstepSetRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      value: tolerance,
      unit: unit,
      axisIndex: axisIndex,
      toBinary() {
        return requests.LockstepSetRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/lockstep_set_tolerance', request);
  }

  /**
   * Checks if the lockstep group is currently enabled on the device.
   * @returns True if a lockstep group with this ID is enabled on the device.
   */
  public async isEnabled(): Promise<boolean> {
    const request: requests.LockstepEmptyRequest & gateway.Message = {
      ...requests.LockstepEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/lockstep_is_enabled',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string which represents the enabled lockstep group.
   * @returns String which represents the enabled lockstep group.
   */
  public toString(): string {
    const request: requests.LockstepEmptyRequest & gateway.Message = {
      ...requests.LockstepEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      lockstepGroupId: this.lockstepGroupId,
      toBinary() {
        return requests.LockstepEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/lockstep_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace Lockstep {
  export interface StopOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface HomeOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface MoveAbsoluteOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveRelativeOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveVelocityOptions {
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveSinOptions {
      /**
       * Number of sinusoidal cycles to complete.
       * Must be a multiple of 0.5
       * If count is not specified or set to 0, the axis will move indefinitely.
       */
      count?: number;
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
  }
  export interface MoveSinStopOptions {
      /**
       * Determines whether function should return after the movement is finished.
       */
      waitUntilIdle?: boolean;
  }
  export interface MoveMaxOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface MoveMinOptions {
      /**
       * Determines whether function should return after the movement is finished or just started.
       */
      waitUntilIdle?: boolean;
      /**
       * Movement velocity.
       * Default value of 0 indicates that the maxspeed setting is used instead.
       * Requires at least Firmware 7.25.
       */
      velocity?: number;
      /**
       * Units of velocity.
       */
      velocityUnit?: Velocity | AngularVelocity | Native;
      /**
       * Movement acceleration.
       * Default value of 0 indicates that the accel setting is used instead.
       * Requires at least Firmware 7.25.
       */
      acceleration?: number;
      /**
       * Units of acceleration.
       */
      accelerationUnit?: Acceleration | AngularAcceleration | Native;
  }
  export interface WaitUntilIdleOptions {
      /**
       * Determines whether to throw error when fault is observed.
       */
      throwErrorOnFault?: boolean;
  }
  export interface SetToleranceOptions {
      /**
       * Optional index of a secondary axis to set the tolerance for.
       * If left empty or set to 0, the tolerance is set to all the secondary axes.
       */
      axisIndex?: number;
  }
}
