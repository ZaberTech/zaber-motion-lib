﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Class used to check and reset warnings and faults on device or axis.
 */
export class Warnings {
  private _device: Device;

  private _axisNumber: number;

  constructor(device: Device, axisNumber: number) {
    this._device = device;
    this._axisNumber = axisNumber;
  }

  /**
   * Returns current warnings and faults on axis or device.
   * @returns Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.
   */
  public async getFlags(): Promise<Set<string>> {
    const request: requests.DeviceGetWarningsRequest & gateway.Message = {
      ...requests.DeviceGetWarningsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      axis: this._axisNumber,
      clear: false,
      toBinary() {
        return requests.DeviceGetWarningsRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetWarningsResponse>(
      'device/get_warnings',
      request,
      requests.DeviceGetWarningsResponse.fromBinary);
    return new Set<string>(response.flags);
  }

  /**
   * Clears (acknowledges) current warnings and faults on axis or device and returns them.
   * @returns Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.
   */
  public async clearFlags(): Promise<Set<string>> {
    const request: requests.DeviceGetWarningsRequest & gateway.Message = {
      ...requests.DeviceGetWarningsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      axis: this._axisNumber,
      clear: true,
      toBinary() {
        return requests.DeviceGetWarningsRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceGetWarningsResponse>(
      'device/get_warnings',
      request,
      requests.DeviceGetWarningsResponse.fromBinary);
    return new Set<string>(response.flags);
  }

  /**
   * Waits for the specified flags to clear.
   * Use for warnings flags that clear on their own.
   * Does not clear clearable warnings flags.
   * Throws TimeoutException if the flags don't clear in the specified time.
   * @param timeout For how long to wait in milliseconds for the flags to clear.
   * @param warningFlags The specific warning flags for which to wait to clear.
   */
  public async waitToClear(
    timeout: number,
    ...warningFlags: string[]
  ): Promise<void> {
    const request: requests.WaitToClearWarningsRequest & gateway.Message = {
      ...requests.WaitToClearWarningsRequest.DEFAULT,
      interfaceId: this._device.connection.interfaceId,
      device: this._device.deviceAddress,
      axis: this._axisNumber,
      timeout: timeout,
      warningFlags: warningFlags,
      toBinary() {
        return requests.WaitToClearWarningsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/wait_to_clear_warnings', request);
  }
}
