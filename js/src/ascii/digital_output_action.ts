/* This file is generated. Do not modify by hand. */
/**
 * Action type for digital output.
 */
export enum DigitalOutputAction {
  /** Off. */
  OFF = 0,
  /** On. */
  ON = 1,
  /** Toggle. */
  TOGGLE = 2,
  /** Keep. */
  KEEP = 3,
}
