/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { IoPortType } from '../ascii/io_port_type';

/**
 * The label of an IO port.
 */
export interface IoPortLabel {
  /**
   * The type of the port.
   */
  portType: IoPortType;

  /**
   * The number of the port.
   */
  channelNumber: number;

  /**
   * The label of the port.
   */
  label: string;

}

export const IoPortLabel = {
  fromBinary: (buffer: Uint8Array): IoPortLabel => BSON.deserialize(buffer) as IoPortLabel,
  toBinary: (value: IoPortLabel): Uint8Array => BSON.serialize(IoPortLabel.sanitize(value)),
  DEFAULT: Object.freeze({
    portType: 0 as IoPortType,
    channelNumber: 0,
    label: '',
  }) as Readonly<IoPortLabel>,
  sanitize: (value: IoPortLabel): IoPortLabel => {
    if (value == null) { throw new TypeError('Expected IoPortLabel object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected IoPortLabel object but got ${typeof value}.`) }
    return {
      portType: sanitizer.sanitizeEnum(value.portType, 'IoPortType', IoPortType, 'portType'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      label: sanitizer.sanitizeString(value.label, 'label'),
    };
  },
};
