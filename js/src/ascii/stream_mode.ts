/* This file is generated. Do not modify by hand. */
/**
 * Mode of a stream.
 */
export enum StreamMode {
  /** Disabled. */
  DISABLED = 0,
  /** Store. */
  STORE = 1,
  /** StoreArbitraryAxes. */
  STORE_ARBITRARY_AXES = 2,
  /** Live. */
  LIVE = 3,
}
