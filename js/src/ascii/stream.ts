﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Device } from './device';
import { Time, Native, Acceleration, AngularAcceleration, AngularVelocity, Velocity, Units } from '../units';
import { RotationDirection } from '../rotation_direction';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Measurement } from '../measurement';
import { StreamBuffer } from './stream_buffer';
import { StreamMode } from './stream_mode';
import { StreamAxisDefinition } from './stream_axis_definition';
import { StreamIo } from './stream_io';
import { DigitalOutputAction } from './digital_output_action';


/**
 * A handle for a stream with this number on the device.
 * Streams provide a way to execute or store a sequence of actions.
 * Stream methods append actions to a queue which executes or stores actions in a first in, first out order.
 */
export class Stream {
  /**
   * Device that controls this stream.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The number that identifies the stream on the device.
   */
  public get streamId(): number {
    return this._streamId;
  }
  private _streamId: number;

  /**
   * Current mode of the stream.
   */
  public get mode(): StreamMode {
    return this._retrieveMode();
  }

  /**
   * An array of axes definitions the stream is set up to control.
   */
  public get axes(): StreamAxisDefinition[] {
    return this._retrieveAxes();
  }

  /**
   * Gets an object that provides access to I/O for this stream.
   */
  public get io(): StreamIo {
    return this._io;
  }
  private _io: StreamIo;

  constructor(device: Device, streamId: number) {
    this._device = device;
    this._streamId = streamId;
    this._io = new StreamIo(device, streamId);
  }

  /**
   * Setup the stream to control the specified axes and to queue actions on the device.
   * Allows use of lockstep axes in a stream.
   * @param axes Definition of the stream axes.
   */
  public async setupLiveComposite(
    ...axes: StreamAxisDefinition[]
  ): Promise<void> {
    const request: requests.StreamSetupLiveCompositeRequest & gateway.Message = {
      ...requests.StreamSetupLiveCompositeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      axes: axes,
      toBinary() {
        return requests.StreamSetupLiveCompositeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_live_composite', request);
  }

  /**
   * Setup the stream to control the specified axes and to queue actions on the device.
   * @param axes Numbers of physical axes to setup the stream on.
   */
  public async setupLive(
    ...axes: number[]
  ): Promise<void> {
    const request: requests.StreamSetupLiveRequest & gateway.Message = {
      ...requests.StreamSetupLiveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      axes: axes,
      toBinary() {
        return requests.StreamSetupLiveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_live', request);
  }

  /**
   * Setup the stream to control the specified axes and queue actions into a stream buffer.
   * Allows use of lockstep axes in a stream.
   * @param streamBuffer The stream buffer to queue actions in.
   * @param axes Definition of the stream axes.
   */
  public async setupStoreComposite(
    streamBuffer: StreamBuffer,
    ...axes: StreamAxisDefinition[]
  ): Promise<void> {
    const request: requests.StreamSetupStoreCompositeRequest & gateway.Message = {
      ...requests.StreamSetupStoreCompositeRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      streamBuffer: streamBuffer.bufferId,
      axes: axes,
      toBinary() {
        return requests.StreamSetupStoreCompositeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_store_composite', request);
  }

  /**
   * Setup the stream to control the specified axes and queue actions into a stream buffer.
   * @param streamBuffer The stream buffer to queue actions in.
   * @param axes Numbers of physical axes to setup the stream on.
   */
  public async setupStore(
    streamBuffer: StreamBuffer,
    ...axes: number[]
  ): Promise<void> {
    const request: requests.StreamSetupStoreRequest & gateway.Message = {
      ...requests.StreamSetupStoreRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      streamBuffer: streamBuffer.bufferId,
      axes: axes,
      toBinary() {
        return requests.StreamSetupStoreRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_store', request);
  }

  /**
   * Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
   * Afterwards, you may call the resulting stream buffer on arbitrary axes.
   * This mode does not allow for unit conversions.
   * @param streamBuffer The stream buffer to queue actions in.
   * @param axesCount The number of axes in the stream.
   */
  public async setupStoreArbitraryAxes(
    streamBuffer: StreamBuffer,
    axesCount: number
  ): Promise<void> {
    const request: requests.StreamSetupStoreArbitraryAxesRequest & gateway.Message = {
      ...requests.StreamSetupStoreArbitraryAxesRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      streamBuffer: streamBuffer.bufferId,
      axesCount: axesCount,
      toBinary() {
        return requests.StreamSetupStoreArbitraryAxesRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_setup_store_arbitrary_axes', request);
  }

  /**
   * Append the actions in a stream buffer to the queue.
   * @param streamBuffer The stream buffer to call.
   */
  public async call(
    streamBuffer: StreamBuffer
  ): Promise<void> {
    const request: requests.StreamCallRequest & gateway.Message = {
      ...requests.StreamCallRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      streamBuffer: streamBuffer.bufferId,
      toBinary() {
        return requests.StreamCallRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_call', request);
  }

  /**
   * Queue an absolute line movement in the stream.
   * @param endpoint Positions for the axes to move to, relative to their home positions.
   */
  public async lineAbsolute(
    ...endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamLineRequest & gateway.Message = {
      ...requests.StreamLineRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamLineRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_line', request);
  }

  /**
   * Queue a relative line movement in the stream.
   * @param endpoint Positions for the axes to move to, relative to their positions before movement.
   */
  public async lineRelative(
    ...endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamLineRequest & gateway.Message = {
      ...requests.StreamLineRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamLineRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_line', request);
  }

  /**
   * Queue an absolute line movement in the stream, targeting a subset of the stream axes.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param endpoint Positions for the axes to move to, relative to their home positions.
   */
  public async lineAbsoluteOn(
    targetAxesIndices: number[],
    endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamLineRequest & gateway.Message = {
      ...requests.StreamLineRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      targetAxesIndices: targetAxesIndices,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamLineRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_line', request);
  }

  /**
   * Queue a relative line movement in the stream, targeting a subset of the stream axes.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param endpoint Positions for the axes to move to, relative to their positions before movement.
   */
  public async lineRelativeOn(
    targetAxesIndices: number[],
    endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamLineRequest & gateway.Message = {
      ...requests.StreamLineRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      targetAxesIndices: targetAxesIndices,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamLineRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_line', request);
  }

  /**
   * Queue an absolute arc movement on the first two axes of the stream.
   * Absolute meaning that the home positions of the axes is treated as the origin.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
   * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
   * @param endX The first dimension of the end position of the arc.
   * @param endY The second dimension of the end position of the arc.
   */
  public async arcAbsolute(
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_arc', request);
  }

  /**
   * Queue a relative arc movement on the first two axes of the stream.
   * Relative meaning that the current position of the axes is treated as the origin.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
   * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
   * @param endX The first dimension of the end position of the arc.
   * @param endY The second dimension of the end position of the arc.
   */
  public async arcRelative(
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_arc', request);
  }

  /**
   * Queue an absolute arc movement in the stream.
   * The movement will only target the specified subset of axes in the stream.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
   * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
   * @param endX The first dimension of the end position of the arc.
   * @param endY The second dimension of the end position of the arc.
   */
  public async arcAbsoluteOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_arc', request);
  }

  /**
   * Queue a relative arc movement in the stream.
   * The movement will only target the specified subset of axes in the stream.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
   * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
   * @param endX The first dimension of the end position of the arc.
   * @param endY The second dimension of the end position of the arc.
   */
  public async arcRelativeOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_arc', request);
  }

  /**
   * Queue an absolute helix movement in the stream.
   * Requires at least Firmware 7.28.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * The first two axes refer to the helix's arc component,
   * while the rest refers to the helix's line component.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
   * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
   * @param endX The first dimension of the end position of the helix's arc component.
   * @param endY The second dimension of the end position of the helix's arc component.
   * @param endpoint Positions for the helix's line component axes, relative to their home positions.
   */
  public async helixAbsoluteOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement,
    ...endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_helix', request);
  }

  /**
   * Queue a relative helix movement in the stream.
   * Requires at least Firmware 7.28.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * The first two axes refer to the helix's arc component,
   * while the rest refers to the helix's line component.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
   * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
   * @param endX The first dimension of the end position of the helix's arc component.
   * @param endY The second dimension of the end position of the helix's arc component.
   * @param endpoint Positions for the helix's line component axes, relative to their positions before movement.
   */
  public async helixRelativeOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement,
    endX: Measurement,
    endY: Measurement,
    ...endpoint: Measurement[]
  ): Promise<void> {
    const request: requests.StreamArcRequest & gateway.Message = {
      ...requests.StreamArcRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      endX: endX,
      endY: endY,
      endpoint: endpoint,
      toBinary() {
        return requests.StreamArcRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_helix', request);
  }

  /**
   * Queue an absolute circle movement on the first two axes of the stream.
   * Absolute meaning that the home positions of the axes are treated as the origin.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle.
   * @param centerY The second dimension of the position of the center of the circle.
   */
  public async circleAbsolute(
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement
  ): Promise<void> {
    const request: requests.StreamCircleRequest & gateway.Message = {
      ...requests.StreamCircleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      toBinary() {
        return requests.StreamCircleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_circle', request);
  }

  /**
   * Queue a relative circle movement on the first two axes of the stream.
   * Relative meaning that the current position of the axes is treated as the origin.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle.
   * @param centerY The second dimension of the position of the center of the circle.
   */
  public async circleRelative(
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement
  ): Promise<void> {
    const request: requests.StreamCircleRequest & gateway.Message = {
      ...requests.StreamCircleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      toBinary() {
        return requests.StreamCircleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_circle', request);
  }

  /**
   * Queue an absolute circle movement in the stream.
   * The movement will only target the specified subset of axes in the stream.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle.
   * @param centerY The second dimension of the position of the center of the circle.
   */
  public async circleAbsoluteOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement
  ): Promise<void> {
    const request: requests.StreamCircleRequest & gateway.Message = {
      ...requests.StreamCircleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.ABS,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      toBinary() {
        return requests.StreamCircleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_circle', request);
  }

  /**
   * Queue a relative circle movement in the stream.
   * The movement will only target the specified subset of axes in the stream.
   * Requires at least Firmware 7.11.
   * @param targetAxesIndices Indices of the axes in the stream the movement targets.
   * Refers to the axes provided during the stream setup or further execution.
   * Indices are zero-based.
   * @param rotationDirection The direction of the rotation.
   * @param centerX The first dimension of the position of the center of the circle.
   * @param centerY The second dimension of the position of the center of the circle.
   */
  public async circleRelativeOn(
    targetAxesIndices: number[],
    rotationDirection: RotationDirection,
    centerX: Measurement,
    centerY: Measurement
  ): Promise<void> {
    const request: requests.StreamCircleRequest & gateway.Message = {
      ...requests.StreamCircleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      type: requests.StreamSegmentType.REL,
      targetAxesIndices: targetAxesIndices,
      rotationDirection: rotationDirection,
      centerX: centerX,
      centerY: centerY,
      toBinary() {
        return requests.StreamCircleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_circle', request);
  }

  /**
   * Wait a specified time.
   * @param time Amount of time to wait.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async wait(
    time: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.StreamWaitRequest & gateway.Message = {
      ...requests.StreamWaitRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      time: time,
      unit: unit,
      toBinary() {
        return requests.StreamWaitRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_wait', request);
  }

  /**
   * Waits until the live stream executes all queued actions.
   * @param [options.throwErrorOnFault=true] Determines whether to throw error when fault is observed.
   */
  public async waitUntilIdle(
    options: Stream.WaitUntilIdleOptions = {}
  ): Promise<void> {
    const {
      throwErrorOnFault = true,
    } = options;
    const request: requests.StreamWaitUntilIdleRequest & gateway.Message = {
      ...requests.StreamWaitUntilIdleRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      throwErrorOnFault: throwErrorOnFault,
      toBinary() {
        return requests.StreamWaitUntilIdleRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_wait_until_idle', request);
  }

  /**
   * Cork the front of the stream's action queue, blocking execution.
   * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
   * Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
   * You can only cork an idle live stream.
   */
  public async cork(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_cork', request);
  }

  /**
   * Uncork the front of the queue, unblocking command execution.
   * You can only uncork an idle live stream that is corked.
   */
  public async uncork(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_uncork', request);
  }

  /**
   * Pauses or resumes execution of the stream in live mode.
   * The hold only takes effect during execution of motion segments.
   * @param hold True to pause execution, false to resume.
   */
  public async setHold(
    hold: boolean
  ): Promise<void> {
    const request: requests.StreamSetHoldRequest & gateway.Message = {
      ...requests.StreamSetHoldRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      hold: hold,
      toBinary() {
        return requests.StreamSetHoldRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_hold', request);
  }

  /**
   * Returns a boolean value indicating whether the live stream is executing a queued action.
   * @returns True if the stream is executing a queued action.
   */
  public async isBusy(): Promise<boolean> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/stream_is_busy',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets the maximum speed of the live stream.
   * Converts the units using the first axis of the stream.
   * @param [unit=Units.NATIVE] Units of velocity.
   * @returns The maximum speed of the stream.
   */
  public async getMaxSpeed(
    unit: Velocity | AngularVelocity | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.StreamGetMaxSpeedRequest & gateway.Message = {
      ...requests.StreamGetMaxSpeedRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      unit: unit,
      toBinary() {
        return requests.StreamGetMaxSpeedRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/stream_get_max_speed',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the maximum speed of the live stream.
   * Converts the units using the first axis of the stream.
   * @param maxSpeed Maximum speed at which any stream action is executed.
   * @param [unit=Units.NATIVE] Units of velocity.
   */
  public async setMaxSpeed(
    maxSpeed: number,
    unit: Velocity | AngularVelocity | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.StreamSetMaxSpeedRequest & gateway.Message = {
      ...requests.StreamSetMaxSpeedRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      maxSpeed: maxSpeed,
      unit: unit,
      toBinary() {
        return requests.StreamSetMaxSpeedRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_max_speed', request);
  }

  /**
   * Gets the maximum tangential acceleration of the live stream.
   * Converts the units using the first axis of the stream.
   * @param [unit=Units.NATIVE] Units of acceleration.
   * @returns The maximum tangential acceleration of the live stream.
   */
  public async getMaxTangentialAcceleration(
    unit: Acceleration | AngularAcceleration | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.StreamGetMaxTangentialAccelerationRequest & gateway.Message = {
      ...requests.StreamGetMaxTangentialAccelerationRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      unit: unit,
      toBinary() {
        return requests.StreamGetMaxTangentialAccelerationRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/stream_get_max_tangential_acceleration',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the maximum tangential acceleration of the live stream.
   * Converts the units using the first axis of the stream.
   * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
   * @param [unit=Units.NATIVE] Units of acceleration.
   */
  public async setMaxTangentialAcceleration(
    maxTangentialAcceleration: number,
    unit: Acceleration | AngularAcceleration | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.StreamSetMaxTangentialAccelerationRequest & gateway.Message = {
      ...requests.StreamSetMaxTangentialAccelerationRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      maxTangentialAcceleration: maxTangentialAcceleration,
      unit: unit,
      toBinary() {
        return requests.StreamSetMaxTangentialAccelerationRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_max_tangential_acceleration', request);
  }

  /**
   * Gets the maximum centripetal acceleration of the live stream.
   * Converts the units using the first axis of the stream.
   * @param [unit=Units.NATIVE] Units of acceleration.
   * @returns The maximum centripetal acceleration of the live stream.
   */
  public async getMaxCentripetalAcceleration(
    unit: Acceleration | AngularAcceleration | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.StreamGetMaxCentripetalAccelerationRequest & gateway.Message = {
      ...requests.StreamGetMaxCentripetalAccelerationRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      unit: unit,
      toBinary() {
        return requests.StreamGetMaxCentripetalAccelerationRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/stream_get_max_centripetal_acceleration',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the maximum centripetal acceleration of the live stream.
   * Converts the units using the first axis of the stream.
   * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
   * @param [unit=Units.NATIVE] Units of acceleration.
   */
  public async setMaxCentripetalAcceleration(
    maxCentripetalAcceleration: number,
    unit: Acceleration | AngularAcceleration | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.StreamSetMaxCentripetalAccelerationRequest & gateway.Message = {
      ...requests.StreamSetMaxCentripetalAccelerationRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      maxCentripetalAcceleration: maxCentripetalAcceleration,
      unit: unit,
      toBinary() {
        return requests.StreamSetMaxCentripetalAccelerationRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_max_centripetal_acceleration', request);
  }

  /**
   * Returns a string which represents the stream.
   * @returns String which represents the stream.
   */
  public toString(): string {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/stream_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Disables the stream.
   * If the stream is not setup, this command does nothing.
   * Once disabled, the stream will no longer accept stream commands.
   * The stream will process the rest of the commands in the queue until it is empty.
   */
  public async disable(): Promise<void> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_disable', request);
  }

  /**
   * Sends a generic ASCII command to the stream.
   * Keeps resending the command while the device rejects with AGAIN reason.
   * @param command Command and its parameters.
   */
  public async genericCommand(
    command: string
  ): Promise<void> {
    const request: requests.StreamGenericCommandRequest & gateway.Message = {
      ...requests.StreamGenericCommandRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      command: command,
      toBinary() {
        return requests.StreamGenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_generic_command', request);
  }

  /**
   * Sends a batch of generic ASCII commands to the stream.
   * Keeps resending command while the device rejects with AGAIN reason.
   * The batch is atomic in terms of thread safety.
   * @param batch Array of commands.
   */
  public async genericCommandBatch(
    batch: string[]
  ): Promise<void> {
    const request: requests.StreamGenericCommandBatchRequest & gateway.Message = {
      ...requests.StreamGenericCommandBatchRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      batch: batch,
      toBinary() {
        return requests.StreamGenericCommandBatchRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_generic_command_batch', request);
  }

  /**
   * Queries the stream status from the device
   * and returns boolean indicating whether the stream is disabled.
   * Useful to determine if streaming was interrupted by other movements.
   * @returns True if the stream is disabled.
   */
  public async checkDisabled(): Promise<boolean> {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'device/stream_check_disabled',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Makes the stream throw StreamDiscontinuityException when it encounters discontinuities (ND warning flag).
   */
  public treatDiscontinuitiesAsError(): void {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('device/stream_treat_discontinuities', request);
  }

  /**
   * Prevents StreamDiscontinuityException as a result of expected discontinuity when resuming streaming.
   */
  public ignoreCurrentDiscontinuity(): void {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('device/stream_ignore_discontinuity', request);
  }

  /**
   * Gets the axes of the stream.
   * @returns An array of axis numbers of the axes the stream is set up to control.
   */
  private _retrieveAxes(): StreamAxisDefinition[] {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StreamGetAxesResponse>(
      'device/stream_get_axes',
      request,
      requests.StreamGetAxesResponse.fromBinary);
    return response.axes;
  }

  /**
   * Get the mode of the stream.
   * @returns Mode of the stream.
   */
  private _retrieveMode(): StreamMode {
    const request: requests.StreamEmptyRequest & gateway.Message = {
      ...requests.StreamEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      toBinary() {
        return requests.StreamEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StreamModeResponse>(
      'device/stream_get_mode',
      request,
      requests.StreamModeResponse.fromBinary);
    return response.streamMode;
  }

  /**
   * @deprecated Use Stream.Io.WaitDigitalInput instead.
   *
   * Wait for a digital input channel to reach a given value.
   * @param channelNumber The number of the digital input channel.
   * Channel numbers are numbered from one.
   * @param value The value that the stream should wait for.
   */
  public async waitDigitalInput(
    channelNumber: number,
    value: boolean
  ): Promise<void> {
    const request: requests.StreamWaitDigitalInputRequest & gateway.Message = {
      ...requests.StreamWaitDigitalInputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamWaitDigitalInputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_wait_digital_input', request);
  }

  /**
   * @deprecated Use Stream.Io.WaitAnalogInput instead.
   *
   * Wait for the value of a analog input channel to reach a condition concerning a given value.
   * @param channelNumber The number of the analog input channel.
   * Channel numbers are numbered from one.
   * @param condition A condition (e.g. <, <=, ==, !=).
   * @param value The value that the condition concerns, in Volts.
   */
  public async waitAnalogInput(
    channelNumber: number,
    condition: string,
    value: number
  ): Promise<void> {
    const request: requests.StreamWaitAnalogInputRequest & gateway.Message = {
      ...requests.StreamWaitAnalogInputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      channelNumber: channelNumber,
      condition: condition,
      value: value,
      toBinary() {
        return requests.StreamWaitAnalogInputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_wait_analog_input', request);
  }

  /**
   * @deprecated Use Stream.Io.SetDigitalOutput instead.
   *
   * Sets value for the specified digital output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value The type of action to perform on the channel.
   */
  public async setDigitalOutput(
    channelNumber: number,
    value: DigitalOutputAction
  ): Promise<void> {
    const request: requests.StreamSetDigitalOutputRequest & gateway.Message = {
      ...requests.StreamSetDigitalOutputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetDigitalOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_digital_output', request);
  }

  /**
   * @deprecated Use Stream.Io.SetAllDigitalOutputs instead.
   *
   * Sets values for all digital output channels.
   * @param values The type of action to perform on the channel.
   */
  public async setAllDigitalOutputs(
    values: DigitalOutputAction[]
  ): Promise<void> {
    const request: requests.StreamSetAllDigitalOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllDigitalOutputsRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      values: values,
      toBinary() {
        return requests.StreamSetAllDigitalOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_digital_outputs', request);
  }

  /**
   * @deprecated Use Stream.Io.setAnalogOutput instead.
   *
   * Sets value for the specified analog output channel.
   * @param channelNumber Channel number starting at 1.
   * @param value Value to set the output channel voltage to.
   */
  public async setAnalogOutput(
    channelNumber: number,
    value: number
  ): Promise<void> {
    const request: requests.StreamSetAnalogOutputRequest & gateway.Message = {
      ...requests.StreamSetAnalogOutputRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      channelNumber: channelNumber,
      value: value,
      toBinary() {
        return requests.StreamSetAnalogOutputRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_analog_output', request);
  }

  /**
   * @deprecated Use Stream.Io.setAllAnalogOutputs instead.
   *
   * Sets values for all analog output channels.
   * @param values Voltage values to set the output channels to.
   */
  public async setAllAnalogOutputs(
    values: number[]
  ): Promise<void> {
    const request: requests.StreamSetAllAnalogOutputsRequest & gateway.Message = {
      ...requests.StreamSetAllAnalogOutputsRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      streamId: this.streamId,
      values: values,
      toBinary() {
        return requests.StreamSetAllAnalogOutputsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/stream_set_all_analog_outputs', request);
  }
}

namespace Stream {
  export interface WaitUntilIdleOptions {
      /**
       * Determines whether to throw error when fault is observed.
       */
      throwErrorOnFault?: boolean;
  }
}
