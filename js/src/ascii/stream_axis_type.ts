/* This file is generated. Do not modify by hand. */
/**
 * Denotes type of the stream axis.
 */
export enum StreamAxisType {
  /** Physical. */
  PHYSICAL = 0,
  /** Lockstep. */
  LOCKSTEP = 1,
}
