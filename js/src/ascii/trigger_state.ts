/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * The complete state of a trigger.
 */
export interface TriggerState {
  /**
   * The firing condition for a trigger.
   */
  condition: string;

  /**
   * The actions for a trigger.
   */
  actions: string[];

  /**
   * The enabled state for a trigger.
   */
  enabled: boolean;

  /**
   * The number of total fires for this trigger.
   * A value of -1 indicates unlimited fires.
   */
  firesTotal: number;

  /**
   * The number of remaining fires for this trigger.
   * A value of -1 indicates unlimited fires remaining.
   */
  firesRemaining: number;

}

export const TriggerState = {
  fromBinary: (buffer: Uint8Array): TriggerState => BSON.deserialize(buffer) as TriggerState,
  toBinary: (value: TriggerState): Uint8Array => BSON.serialize(TriggerState.sanitize(value)),
  DEFAULT: Object.freeze({
    condition: '',
    actions: [],
    enabled: false,
    firesTotal: 0,
    firesRemaining: 0,
  }) as Readonly<TriggerState>,
  sanitize: (value: TriggerState): TriggerState => {
    if (value == null) { throw new TypeError('Expected TriggerState object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerState object but got ${typeof value}.`) }
    return {
      condition: sanitizer.sanitizeString(value.condition, 'condition'),
      actions: Array.from(value.actions ?? [], item => sanitizer.sanitizeString(item, 'items of actions')),
      enabled: sanitizer.sanitizeBool(value.enabled, 'enabled'),
      firesTotal: sanitizer.sanitizeInt(value.firesTotal, 'firesTotal'),
      firesRemaining: sanitizer.sanitizeInt(value.firesRemaining, 'firesRemaining'),
    };
  },
};
