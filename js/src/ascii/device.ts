﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Connection } from './connection';
import { DeviceSettings } from './device_settings';
import { Axis } from './axis';
import { AllAxes } from './all_axes';
import { Warnings } from './warnings';
import { DeviceIdentity } from './device_identity';
import { DeviceIO } from './device_io';
import { Response } from './response';
import { Lockstep } from './lockstep';
import { Oscilloscope } from './oscilloscope';
import { CanSetStateDeviceResponse } from './can_set_state_device_response';
import { Pvt } from './pvt';
import { Measurement } from '../measurement';
import { FirmwareVersion } from '../firmware_version';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { DeviceStorage } from './storage';
import { Triggers } from './triggers';
import { Streams } from './streams';
import { SetStateDeviceResponse } from './set_state_device_response';

/**
 * Represents the controller part of one device - may be either a standalone controller or an integrated controller.
 */
export class Device {
  /**
   * Connection of this device.
   */
  public get connection(): Connection {
    return this._connection;
  }
  private _connection: Connection;

  /**
   * The device address uniquely identifies the device on the connection.
   * It can be configured or automatically assigned by the renumber command.
   */
  public get deviceAddress(): number {
    return this._deviceAddress;
  }
  private _deviceAddress: number;

  /**
   * Settings and properties of this device.
   */
  public get settings(): DeviceSettings {
    return this._settings;
  }
  private _settings: DeviceSettings;

  /**
   * Key-value storage of this device.
   */
  public get storage(): DeviceStorage {
    return this._storage;
  }
  private _storage: DeviceStorage;

  /**
   * I/O channels of this device.
   */
  public get io(): DeviceIO {
    return this._io;
  }
  private _io: DeviceIO;

  /**
   * Virtual axis which allows you to target all axes of this device.
   */
  public get allAxes(): AllAxes {
    return this._allAxes;
  }
  private _allAxes: AllAxes;

  /**
   * Warnings and faults of this device and all its axes.
   */
  public get warnings(): Warnings {
    return this._warnings;
  }
  private _warnings: Warnings;

  /**
   * Identity of the device.
   */
  public get identity(): DeviceIdentity {
    return this._retrieveIdentity();
  }

  /**
   * Indicates whether or not the device has been identified.
   */
  public get isIdentified(): boolean {
    return this._retrieveIsIdentified();
  }

  /**
   * Oscilloscope recording helper for this device.
   * Requires at least Firmware 7.00.
   */
  public get oscilloscope(): Oscilloscope {
    return this._oscilloscope;
  }
  private _oscilloscope: Oscilloscope;

  /**
   * Unique ID of the device hardware.
   */
  public get deviceId(): number {
    return this.identity.deviceId;
  }

  /**
   * Serial number of the device.
   */
  public get serialNumber(): number {
    return this.identity.serialNumber;
  }

  /**
   * Name of the product.
   */
  public get name(): string {
    return this.identity.name;
  }

  /**
   * Number of axes this device has.
   */
  public get axisCount(): number {
    return this.identity.axisCount;
  }

  /**
   * Version of the firmware.
   */
  public get firmwareVersion(): FirmwareVersion {
    return this.identity.firmwareVersion;
  }

  /**
   * The device is an integrated product.
   */
  public get isIntegrated(): boolean {
    return this.identity.isIntegrated;
  }

  /**
   * User-assigned label of the device.
   */
  public get label(): string {
    return this._retrieveLabel();
  }

  /**
   * Triggers for this device.
   * Requires at least Firmware 7.06.
   */
  public get triggers(): Triggers {
    return this._triggers;
  }
  private _triggers: Triggers;

  /**
   * Gets an object that provides access to Streams on this device.
   * Requires at least Firmware 7.05.
   */
  public get streams(): Streams {
    return this._streams;
  }
  private _streams: Streams;

  /**
   * Gets an object that provides access to PVT functions of this device.
   * Note that as of ZML v5.0.0, this returns a Pvt object and NOT a PvtSequence object.
   * The PvtSequence can now be obtained from the Pvt object.
   * Requires at least Firmware 7.33.
   */
  public get pvt(): Pvt {
    return this._pvt;
  }
  private _pvt: Pvt;

  constructor(connection: Connection, deviceAddress: number) {
    this._connection = connection;
    this._deviceAddress = deviceAddress;
    this._settings = new DeviceSettings(this);
    this._storage = new DeviceStorage(this);
    this._io = new DeviceIO(this);
    this._allAxes = new AllAxes(this);
    this._warnings = new Warnings(this, 0);
    this._oscilloscope = new Oscilloscope(this);
    this._triggers = new Triggers(this);
    this._streams = new Streams(this);
    this._pvt = new Pvt(this);
  }

  /**
   * Queries the device and the database, gathering information about the product.
   * Without this information features such as unit conversions will not work.
   * Usually, called automatically by detect devices method.
   * @param options.assumeVersion The identification assumes the specified firmware version
   * instead of the version queried from the device.
   * Providing this argument can lead to unexpected compatibility issues.
   * @returns Device identification data.
   */
  public async identify(
    options: Device.IdentifyOptions = {}
  ): Promise<DeviceIdentity> {
    const {
      assumeVersion,
    } = options;
    const request: requests.DeviceIdentifyRequest & gateway.Message = {
      ...requests.DeviceIdentifyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      assumeVersion: assumeVersion,
      toBinary() {
        return requests.DeviceIdentifyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<DeviceIdentity>(
      'device/identify',
      request,
      DeviceIdentity.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this device.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.axis=0] Optional axis number to send the command to.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: string,
    options: Device.GenericCommandOptions = {}
  ): Promise<Response> {
    const {
      axis = 0,
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      axis: axis,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Response>(
      'interface/generic_command',
      request,
      Response.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this device and expect multiple responses.
   * Responses are returned in order of arrival.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.axis=0] Optional axis number to send the command to.
   * @param [options.checkErrors=true] Controls whether to throw an exception when a device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: string,
    options: Device.GenericCommandMultiResponseOptions = {}
  ): Promise<Response[]> {
    const {
      axis = 0,
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      axis: axis,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GenericCommandResponseCollection>(
      'interface/generic_command_multi_response',
      request,
      requests.GenericCommandResponseCollection.fromBinary);
    return response.responses;
  }

  /**
   * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.axis=0] Optional axis number to send the command to.
   * Specifying -1 omits the number completely.
   */
  public async genericCommandNoResponse(
    command: string,
    options: Device.GenericCommandNoResponseOptions = {}
  ): Promise<void> {
    const {
      axis = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      command: command,
      axis: axis,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Gets an Axis class instance which allows you to control a particular axis on this device.
   * Axes are numbered from 1.
   * @param axisNumber Number of axis intended to control.
   * @returns Axis instance.
   */
  public getAxis(
    axisNumber: number
  ): Axis {
    if (axisNumber <= 0) {
      throw new TypeError('Invalid value; physical axes are numbered from 1.');
    }
    return new Axis(this, axisNumber);
  }

  /**
   * Gets a Lockstep class instance which allows you to control a particular lockstep group on the device.
   * Requires at least Firmware 6.15 or 7.11.
   * @param lockstepGroupId The ID of the lockstep group to control. Lockstep group IDs start at one.
   * @returns Lockstep instance.
   */
  public getLockstep(
    lockstepGroupId: number
  ): Lockstep {
    if (lockstepGroupId <= 0) {
      throw new TypeError('Invalid value; lockstep groups are numbered from 1.');
    }
    return new Lockstep(this, lockstepGroupId);
  }

  /**
   * Formats parameters into a command and performs unit conversions.
   * Parameters in the command template are denoted by a question mark.
   * Command returned is only valid for this device.
   * Unit conversion is not supported for commands where axes can be remapped, such as stream and PVT commands.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
   * @param parameters Variable number of command parameters.
   * @returns Command with converted parameters.
   */
  public prepareCommand(
    commandTemplate: string,
    ...parameters: Measurement[]
  ): string {
    const request: requests.PrepareCommandRequest & gateway.Message = {
      ...requests.PrepareCommandRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      commandTemplate: commandTemplate,
      parameters: parameters,
      toBinary() {
        return requests.PrepareCommandRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/prepare_command',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the user-assigned device label.
   * The label is stored on the controller and recognized by other software.
   * @param label Label to set.
   */
  public async setLabel(
    label: string
  ): Promise<void> {
    const request: requests.DeviceSetStorageRequest & gateway.Message = {
      ...requests.DeviceSetStorageRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      value: label,
      toBinary() {
        return requests.DeviceSetStorageRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_label', request);
  }

  /**
   * Gets the device name.
   * @returns The label.
   */
  private _retrieveLabel(): string {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/get_label',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a serialization of the current device state that can be saved and reapplied.
   * @returns A serialization of the current state of the device.
   */
  public async getState(): Promise<string> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_state',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Applies a saved state to this device.
   * @param state The state object to apply to this device.
   * @param [options.deviceOnly=false] If true, only device scope settings and features will be set.
   * @returns Reports of any issues that were handled, but caused the state to not be exactly restored.
   */
  public async setState(
    state: string,
    options: Device.SetStateOptions = {}
  ): Promise<SetStateDeviceResponse> {
    const {
      deviceOnly = false,
    } = options;
    const request: requests.SetStateRequest & gateway.Message = {
      ...requests.SetStateRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      state: state,
      deviceOnly: deviceOnly,
      toBinary() {
        return requests.SetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<SetStateDeviceResponse>(
      'device/set_device_state',
      request,
      SetStateDeviceResponse.fromBinary);
    return response;
  }

  /**
   * Checks if a state can be applied to this device.
   * This only covers exceptions that can be determined statically such as mismatches of ID or version,
   * the process of applying the state can still fail when running.
   * @param state The state object to check against.
   * @param options.firmwareVersion The firmware version of the device to apply the state to.
   * Use this to ensure the state will still be compatible after an update.
   * @returns An object listing errors that come up when trying to set the state.
   */
  public async canSetState(
    state: string,
    options: Device.CanSetStateOptions = {}
  ): Promise<CanSetStateDeviceResponse> {
    const {
      firmwareVersion,
    } = options;
    const request: requests.CanSetStateRequest & gateway.Message = {
      ...requests.CanSetStateRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      state: state,
      firmwareVersion: firmwareVersion,
      toBinary() {
        return requests.CanSetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<CanSetStateDeviceResponse>(
      'device/can_set_state',
      request,
      CanSetStateDeviceResponse.fromBinary);
    return response;
  }

  /**
   * Waits for the device to start responding to messages.
   * Useful to call after resetting the device.
   * Throws RequestTimeoutException upon timeout.
   * @param timeout For how long to wait in milliseconds for the device to start responding.
   */
  public async waitToRespond(
    timeout: number
  ): Promise<void> {
    const request: requests.WaitToRespondRequest & gateway.Message = {
      ...requests.WaitToRespondRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      timeout: timeout,
      toBinary() {
        return requests.WaitToRespondRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/wait_to_respond', request);
  }

  /**
   * Changes the address of this device.
   * After the address is successfully changed, the existing device class instance no longer represents the device.
   * Instead, use the new device instance returned by this method.
   * @param address The new address to assign to the device.
   * @returns New device instance with the new address.
   */
  public async renumber(
    address: number
  ): Promise<Device> {
    if (address < 1 || address > 99) {
      throw new TypeError('Invalid value; device addresses are numbered from 1 to 99.');
    }
    const request: requests.RenumberRequest & gateway.Message = {
      ...requests.RenumberRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      address: address,
      toBinary() {
        return requests.RenumberRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/renumber',
      request,
      requests.IntResponse.fromBinary);
    return new Device(this.connection, response.value);
  }

  /**
   * Restores most of the settings to their default values.
   * Deletes all triggers, stream and PVT buffers, servo tunings.
   * Deletes all zaber storage keys.
   * Disables locksteps, unparks axes.
   * Preserves storage, communication settings, peripherals (unless hard is specified).
   * The device needs to be identified again after the restore.
   * @param [hard=false] If true, completely erases device's memory. The device also resets.
   */
  public async restore(
    hard: boolean = false
  ): Promise<void> {
    const request: requests.DeviceRestoreRequest & gateway.Message = {
      ...requests.DeviceRestoreRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      hard: hard,
      toBinary() {
        return requests.DeviceRestoreRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/restore', request);
  }

  /**
   * Returns identity.
   * @returns Device identity.
   */
  private _retrieveIdentity(): DeviceIdentity {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<DeviceIdentity>(
      'device/get_identity',
      request,
      DeviceIdentity.fromBinary);
    return response;
  }

  /**
   * Returns whether or not the device have been identified.
   * @returns True if the device has already been identified. False otherwise.
   */
  private _retrieveIsIdentified(): boolean {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      device: this.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.BoolResponse>(
      'device/get_is_identified',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }
}

namespace Device {
  export interface IdentifyOptions {
      /**
       * The identification assumes the specified firmware version
       * instead of the version queried from the device.
       * Providing this argument can lead to unexpected compatibility issues.
       */
      assumeVersion?: FirmwareVersion;
  }
  export interface GenericCommandOptions {
      /**
       * Optional axis number to send the command to.
       */
      axis?: number;
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Optional axis number to send the command to.
       */
      axis?: number;
      /**
       * Controls whether to throw an exception when a device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandNoResponseOptions {
      /**
       * Optional axis number to send the command to.
       * Specifying -1 omits the number completely.
       */
      axis?: number;
  }
  export interface SetStateOptions {
      /**
       * If true, only device scope settings and features will be set.
       */
      deviceOnly?: boolean;
  }
  export interface CanSetStateOptions {
      /**
       * The firmware version of the device to apply the state to.
       * Use this to ensure the state will still be compatible after an update.
       */
      firmwareVersion?: FirmwareVersion;
  }
}
