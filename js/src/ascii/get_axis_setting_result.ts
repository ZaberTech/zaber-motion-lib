/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

/**
 * The response from a multi-get axis command.
 */
export interface GetAxisSettingResult {
  /**
   * The setting read.
   */
  setting: string;

  /**
   * The value read.
   */
  value: number;

  /**
   * The unit of the values.
   */
  unit: Units;

}

export const GetAxisSettingResult = {
  fromBinary: (buffer: Uint8Array): GetAxisSettingResult => BSON.deserialize(buffer) as GetAxisSettingResult,
  toBinary: (value: GetAxisSettingResult): Uint8Array => BSON.serialize(GetAxisSettingResult.sanitize(value)),
  DEFAULT: Object.freeze({
    setting: '',
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<GetAxisSettingResult>,
  sanitize: (value: GetAxisSettingResult): GetAxisSettingResult => {
    if (value == null) { throw new TypeError('Expected GetAxisSettingResult object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetAxisSettingResult object but got ${typeof value}.`) }
    return {
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
