/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

/**
 * The response from a multi-get command.
 */
export interface GetSettingResult {
  /**
   * The setting read.
   */
  setting: string;

  /**
   * The list of values returned.
   */
  values: number[];

  /**
   * The unit of the values.
   */
  unit: Units;

}

export const GetSettingResult = {
  fromBinary: (buffer: Uint8Array): GetSettingResult => BSON.deserialize(buffer) as GetSettingResult,
  toBinary: (value: GetSettingResult): Uint8Array => BSON.serialize(GetSettingResult.sanitize(value)),
  DEFAULT: Object.freeze({
    setting: '',
    values: [],
    unit: Units.NATIVE,
  }) as Readonly<GetSettingResult>,
  sanitize: (value: GetSettingResult): GetSettingResult => {
    if (value == null) { throw new TypeError('Expected GetSettingResult object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetSettingResult object but got ${typeof value}.`) }
    return {
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
