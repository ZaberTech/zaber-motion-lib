﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Axis } from './axis';
import { GetAxisSetting } from './get_axis_setting';
import { GetAxisSettingResult } from './get_axis_setting_result';
import { ConversionFactor } from './conversion_factor';
import { Units } from '../units';
import * as gateway from '../gateway';
import * as requests from '../requests';

/**
 * Class providing access to various axis settings and properties.
 */
export class AxisSettings {
  private _axis: Axis;

  constructor(axis: Axis) {
    this._axis = axis;
  }

  /**
   * Returns any axis setting or property.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   * @returns Setting value.
   */
  public async get(
    setting: string,
    unit: Units = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets any axis setting.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param value Value of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   */
  public async set(
    setting: string,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Returns any axis setting or property as a string.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @returns Setting value.
   */
  public async getString(
    setting: string
  ): Promise<string> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_setting_str',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets any axis setting as a string.
   * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
   * @param setting Name of the setting.
   * @param value Value of the setting.
   */
  public async setString(
    setting: string,
    value: string
  ): Promise<void> {
    const request: requests.DeviceSetSettingStrRequest & gateway.Message = {
      ...requests.DeviceSetSettingStrRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      value: value,
      toBinary() {
        return requests.DeviceSetSettingStrRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting_str', request);
  }

  /**
   * Convert arbitrary setting value to Zaber native units.
   * @param setting Name of the setting.
   * @param value Value of the setting in units specified by following argument.
   * @param unit Units of the value.
   * @returns Setting value.
   */
  public convertToNativeUnits(
    setting: string,
    value: number,
    unit: Units
  ): number {
    const request: requests.DeviceConvertSettingRequest & gateway.Message = {
      ...requests.DeviceConvertSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceConvertSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/convert_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Convert arbitrary setting value from Zaber native units.
   * @param setting Name of the setting.
   * @param value Value of the setting in Zaber native units.
   * @param unit Units to convert value to.
   * @returns Setting value.
   */
  public convertFromNativeUnits(
    setting: string,
    value: number,
    unit: Units
  ): number {
    const request: requests.DeviceConvertSettingRequest & gateway.Message = {
      ...requests.DeviceConvertSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      fromNative: true,
      setting: setting,
      value: value,
      unit: unit,
      toBinary() {
        return requests.DeviceConvertSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/convert_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the default value of a setting.
   * @param setting Name of the setting.
   * @param [unit=Units.NATIVE] Units of setting.
   * @returns Default setting value.
   */
  public getDefault(
    setting: string,
    unit: Units = Units.NATIVE
  ): number {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'device/get_setting_default',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the default value of a setting as a string.
   * @param setting Name of the setting.
   * @returns Default setting value.
   */
  public getDefaultString(
    setting: string
  ): string {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/get_setting_default_str',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Indicates if given setting can be converted from and to native units.
   * @param setting Name of the setting.
   * @returns True if unit conversion can be performed.
   */
  public canConvertNativeUnits(
    setting: string
  ): boolean {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      setting: setting,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.BoolResponse>(
      'device/can_convert_setting',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Overrides default unit conversions.
   * Conversion factors are specified by setting names representing underlying dimensions.
   * Requires at least Firmware 7.30.
   * @param conversions Factors of all conversions to override.
   */
  public async setCustomUnitConversions(
    conversions: ConversionFactor[]
  ): Promise<void> {
    const request: requests.DeviceSetUnitConversionsRequest & gateway.Message = {
      ...requests.DeviceSetUnitConversionsRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      conversions: conversions,
      toBinary() {
        return requests.DeviceSetUnitConversionsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_unit_conversions', request);
  }

  /**
   * Gets many setting values in as few requests as possible.
   * @param axisSettings The settings to read.
   * @returns The setting values read.
   */
  public async getMany(
    ...axisSettings: GetAxisSetting[]
  ): Promise<GetAxisSettingResult[]> {
    const request: requests.DeviceMultiGetSettingRequest & gateway.Message = {
      ...requests.DeviceMultiGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      axisSettings: axisSettings,
      toBinary() {
        return requests.DeviceMultiGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetAxisSettingResults>(
      'device/get_many_settings',
      request,
      requests.GetAxisSettingResults.fromBinary);
    return response.results;
  }

  /**
   * Gets many setting values in the same tick, ensuring their values are synchronized.
   * Requires at least Firmware 7.35.
   * @param axisSettings The settings to read.
   * @returns The setting values read.
   */
  public async getSynchronized(
    ...axisSettings: GetAxisSetting[]
  ): Promise<GetAxisSettingResult[]> {
    const request: requests.DeviceMultiGetSettingRequest & gateway.Message = {
      ...requests.DeviceMultiGetSettingRequest.DEFAULT,
      interfaceId: this._axis.device.connection.interfaceId,
      device: this._axis.device.deviceAddress,
      axis: this._axis.axisNumber,
      axisSettings: axisSettings,
      toBinary() {
        return requests.DeviceMultiGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetAxisSettingResults>(
      'device/get_sync_settings',
      request,
      requests.GetAxisSettingResults.fromBinary);
    return response.results;
  }
}
