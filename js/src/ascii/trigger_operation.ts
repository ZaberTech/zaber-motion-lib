/* This file is generated. Do not modify by hand. */
/**
 * Operation for trigger action.
 */
export enum TriggerOperation {
  /** SetTo. */
  SET_TO = 0,
  /** IncrementBy. */
  INCREMENT_BY = 1,
  /** DecrementBy. */
  DECREMENT_BY = 2,
}
