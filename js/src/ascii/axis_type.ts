/* This file is generated. Do not modify by hand. */
/**
 * Denotes type of an axis and units it accepts.
 */
export enum AxisType {
  /** Unknown. */
  UNKNOWN = 0,
  /** Linear. */
  LINEAR = 1,
  /** Rotary. */
  ROTARY = 2,
  /** Process. */
  PROCESS = 3,
  /** Lamp. */
  LAMP = 4,
}
