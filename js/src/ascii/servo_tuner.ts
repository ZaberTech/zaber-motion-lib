﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Axis } from './axis';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { ServoTuningParamset } from './servo_tuning_paramset';
import { ParamsetInfo } from './paramset_info';
import { ServoTuningParam } from './servo_tuning_param';
import { PidTuning } from './pid_tuning';
import { SimpleTuningParamDefinition } from './simple_tuning_param_definition';
import { SimpleTuning } from './simple_tuning';

/**
 * Exposes the capabilities to inspect and edit an axis' servo tuning.
 * Requires at least Firmware 6.25 or 7.00.
 */
export class ServoTuner {
  /**
   * The axis that will be tuned.
   */
  public get axis(): Axis {
    return this._axis;
  }
  private _axis: Axis;

  /**
   * Creates instance of ServoTuner for the given axis.
   */
  constructor(axis: Axis) {
    this._axis = axis;
  }

  /**
   * Get the paramset that this device uses by default when it starts up.
   * @returns The paramset used when the device restarts.
   */
  public async getStartupParamset(): Promise<ServoTuningParamset> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.ServoTuningParamsetResponse>(
      'servotuning/get_startup_set',
      request,
      requests.ServoTuningParamsetResponse.fromBinary);
    return response.paramset;
  }

  /**
   * Set the paramset that this device uses by default when it starts up.
   * @param paramset The paramset to use at startup.
   */
  public async setStartupParamset(
    paramset: ServoTuningParamset
  ): Promise<void> {
    const request: requests.ServoTuningRequest & gateway.Message = {
      ...requests.ServoTuningRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      toBinary() {
        return requests.ServoTuningRequest.toBinary(this);
      },
    };

    await gateway.callAsync('servotuning/set_startup_set', request);
  }

  /**
   * Load the values from one paramset into another.
   * @param toParamset The paramset to load into.
   * @param fromParamset The paramset to load from.
   */
  public async loadParamset(
    toParamset: ServoTuningParamset,
    fromParamset: ServoTuningParamset
  ): Promise<void> {
    const request: requests.LoadParamset & gateway.Message = {
      ...requests.LoadParamset.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      toParamset: toParamset,
      fromParamset: fromParamset,
      toBinary() {
        return requests.LoadParamset.toBinary(this);
      },
    };

    await gateway.callAsync('servotuning/load_paramset', request);
  }

  /**
   * Get the full set of tuning parameters used by the firmware driving this axis.
   * @param paramset The paramset to get tuning for.
   * @returns The raw representation of the current tuning.
   */
  public async getTuning(
    paramset: ServoTuningParamset
  ): Promise<ParamsetInfo> {
    const request: requests.ServoTuningRequest & gateway.Message = {
      ...requests.ServoTuningRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      toBinary() {
        return requests.ServoTuningRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<ParamsetInfo>(
      'servotuning/get_raw',
      request,
      ParamsetInfo.fromBinary);
    return response;
  }

  /**
   * Set individual tuning parameters.
   * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
   * @param paramset The paramset to set tuning of.
   * @param tuningParams The params to set.
   * @param [setUnspecifiedToDefault=false] If true, any tuning parameters not included in TuningParams
   * are reset to their default values.
   */
  public async setTuning(
    paramset: ServoTuningParamset,
    tuningParams: ServoTuningParam[],
    setUnspecifiedToDefault: boolean = false
  ): Promise<void> {
    const request: requests.SetServoTuningRequest & gateway.Message = {
      ...requests.SetServoTuningRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      tuningParams: tuningParams,
      setUnspecifiedToDefault: setUnspecifiedToDefault,
      toBinary() {
        return requests.SetServoTuningRequest.toBinary(this);
      },
    };

    await gateway.callAsync('servotuning/set_raw', request);
  }

  /**
   * Sets the tuning of a paramset using the PID method.
   * @param paramset The paramset to get tuning for.
   * @param p The proportional gain. Must be in units of N/m.
   * @param i The integral gain. Must be in units of N/m⋅s.
   * @param d The derivative gain. Must be in units of N⋅s/m.
   * @param fc The cutoff frequency. Must be in units of Hz.
   * @returns The PID representation of the current tuning after your changes have been applied.
   */
  public async setPidTuning(
    paramset: ServoTuningParamset,
    p: number,
    i: number,
    d: number,
    fc: number
  ): Promise<PidTuning> {
    const request: requests.SetServoTuningPIDRequest & gateway.Message = {
      ...requests.SetServoTuningPIDRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      p: p,
      i: i,
      d: d,
      fc: fc,
      toBinary() {
        return requests.SetServoTuningPIDRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<PidTuning>(
      'servotuning/set_pid',
      request,
      PidTuning.fromBinary);
    return response;
  }

  /**
   * Gets the PID representation of this paramset's servo tuning.
   * @param paramset The paramset to get tuning for.
   * @returns The PID representation of the current tuning.
   */
  public async getPidTuning(
    paramset: ServoTuningParamset
  ): Promise<PidTuning> {
    const request: requests.ServoTuningRequest & gateway.Message = {
      ...requests.ServoTuningRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      toBinary() {
        return requests.ServoTuningRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<PidTuning>(
      'servotuning/get_pid',
      request,
      PidTuning.fromBinary);
    return response;
  }

  /**
   * Gets the parameters that are required to tune this device.
   * @returns The tuning parameters.
   */
  public async getSimpleTuningParamDefinitions(): Promise<SimpleTuningParamDefinition[]> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GetSimpleTuningParamDefinitionResponse>(
      'servotuning/get_simple_params_definition',
      request,
      requests.GetSimpleTuningParamDefinitionResponse.fromBinary);
    return response.params;
  }

  /**
   * Set the tuning of this device using the simple input method.
   * @param paramset The paramset to set tuning for.
   * @param tuningParams The params used to tune this device.
   * To get what parameters are expected, call GetSimpleTuningParamList.
   * All values must be between 0 and 1.
   * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
   * @param [options.carriageMass=null] The mass of the carriage in kg. If this value is not set the default carriage mass is used.
   */
  public async setSimpleTuning(
    paramset: ServoTuningParamset,
    tuningParams: ServoTuningParam[],
    loadMass: number,
    options: ServoTuner.SetSimpleTuningOptions = {}
  ): Promise<void> {
    const {
      carriageMass = null,
    } = options;
    const request: requests.SetSimpleTuning & gateway.Message = {
      ...requests.SetSimpleTuning.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      tuningParams: tuningParams,
      loadMass: loadMass,
      carriageMass: carriageMass,
      toBinary() {
        return requests.SetSimpleTuning.toBinary(this);
      },
    };

    await gateway.callAsync('servotuning/set_simple_tuning', request);
  }

  /**
   * Get the simple tuning parameters for this device.
   * @param paramset The paramset to get tuning for.
   * @returns The simple tuning parameters.
   */
  public async getSimpleTuning(
    paramset: ServoTuningParamset
  ): Promise<SimpleTuning> {
    const request: requests.ServoTuningRequest & gateway.Message = {
      ...requests.ServoTuningRequest.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      toBinary() {
        return requests.ServoTuningRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<SimpleTuning>(
      'servotuning/get_simple_tuning',
      request,
      SimpleTuning.fromBinary);
    return response;
  }

  /**
   * @deprecated Use GetSimpleTuning instead.
   *
   * Checks if the provided simple tuning is being stored by this paramset.
   * @param paramset The paramset to set tuning for.
   * @param tuningParams The params used to tune this device.
   * To get what parameters are expected, call GetSimpleTuningParamList.
   * All values must be between 0 and 1.
   * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
   * @param [options.carriageMass=null] The mass of the carriage in kg. If this value is not set the default carriage mass is used.
   * @returns True if the provided simple tuning is currently stored in this paramset.
   */
  public async isUsingSimpleTuning(
    paramset: ServoTuningParamset,
    tuningParams: ServoTuningParam[],
    loadMass: number,
    options: ServoTuner.IsUsingSimpleTuningOptions = {}
  ): Promise<boolean> {
    const {
      carriageMass = null,
    } = options;
    const request: requests.SetSimpleTuning & gateway.Message = {
      ...requests.SetSimpleTuning.DEFAULT,
      interfaceId: this.axis.device.connection.interfaceId,
      device: this.axis.device.deviceAddress,
      axis: this.axis.axisNumber,
      paramset: paramset,
      tuningParams: tuningParams,
      loadMass: loadMass,
      carriageMass: carriageMass,
      toBinary() {
        return requests.SetSimpleTuning.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'servotuning/is_using_simple_tuning',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }
}

namespace ServoTuner {
  export interface SetSimpleTuningOptions {
      /**
       * The mass of the carriage in kg. If this value is not set the default carriage mass is used.
       */
      carriageMass?: number;
  }
  export interface IsUsingSimpleTuningOptions {
      /**
       * The mass of the carriage in kg. If this value is not set the default carriage mass is used.
       */
      carriageMass?: number;
  }
}
