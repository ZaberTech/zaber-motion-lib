/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisType } from '../ascii/axis_type';

/**
 * Representation of data gathered during axis identification.
 */
export interface AxisIdentity {
  /**
   * Unique ID of the peripheral hardware.
   */
  peripheralId: number;

  /**
   * Name of the peripheral.
   */
  peripheralName: string;

  /**
   * Serial number of the peripheral, or 0 when not applicable.
   */
  peripheralSerialNumber: number;

  /**
   * Indicates whether the axis is a peripheral or part of an integrated device.
   */
  isPeripheral: boolean;

  /**
   * Determines the type of an axis and units it accepts.
   */
  axisType: AxisType;

  /**
   * The peripheral has hardware modifications.
   */
  isModified: boolean;

}

export const AxisIdentity = {
  fromBinary: (buffer: Uint8Array): AxisIdentity => BSON.deserialize(buffer) as AxisIdentity,
  toBinary: (value: AxisIdentity): Uint8Array => BSON.serialize(AxisIdentity.sanitize(value)),
  DEFAULT: Object.freeze({
    peripheralId: 0,
    peripheralName: '',
    peripheralSerialNumber: 0,
    isPeripheral: false,
    axisType: 0 as AxisType,
    isModified: false,
  }) as Readonly<AxisIdentity>,
  sanitize: (value: AxisIdentity): AxisIdentity => {
    if (value == null) { throw new TypeError('Expected AxisIdentity object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisIdentity object but got ${typeof value}.`) }
    return {
      peripheralId: sanitizer.sanitizeInt(value.peripheralId, 'peripheralId'),
      peripheralName: sanitizer.sanitizeString(value.peripheralName, 'peripheralName'),
      peripheralSerialNumber: sanitizer.sanitizeInt(value.peripheralSerialNumber, 'peripheralSerialNumber'),
      isPeripheral: sanitizer.sanitizeBool(value.isPeripheral, 'isPeripheral'),
      axisType: sanitizer.sanitizeEnum(value.axisType, 'AxisType', AxisType, 'axisType'),
      isModified: sanitizer.sanitizeBool(value.isModified, 'isModified'),
    };
  },
};
