﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Native, Units, Length, Angle, Time } from '../units';
import { Device } from './device';
import { TriggerState } from './trigger_state';
import { TriggerEnabledState } from './trigger_enabled_state';
import { IoPortType } from './io_port_type';
import { TriggerCondition } from './trigger_condition';
import { TriggerAction } from './trigger_action';
import { TriggerOperation } from './trigger_operation';

/**
 * A handle for a trigger with this number on the device.
 * Triggers allow setting up actions that occur when a certain condition has been met or an event has occurred.
 * Please note that the Triggers API is currently an experimental feature.
 * Requires at least Firmware 7.06.
 */
export class Trigger {
  /**
   * Device that this trigger belongs to.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * Number of this trigger.
   */
  public get triggerNumber(): number {
    return this._triggerNumber;
  }
  private _triggerNumber: number;

  constructor(device: Device, triggerNumber: number) {
    this._device = device;
    this._triggerNumber = triggerNumber;
  }

  /**
   * Enables the trigger.
   * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
   * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
   * @param [count=0] Number of times the trigger will fire before disabling itself.
   * If count is not specified, or 0, the trigger will fire indefinitely.
   */
  public async enable(
    count: number = 0
  ): Promise<void> {
    if (count < 0) {
      throw new TypeError('Invalid value; count must be 0 or positive.');
    }
    const request: requests.TriggerEnableRequest & gateway.Message = {
      ...requests.TriggerEnableRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      count: count,
      toBinary() {
        return requests.TriggerEnableRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/enable', request);
  }

  /**
   * Disables the trigger.
   * Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
   */
  public async disable(): Promise<void> {
    const request: requests.TriggerEmptyRequest & gateway.Message = {
      ...requests.TriggerEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      toBinary() {
        return requests.TriggerEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/disable', request);
  }

  /**
   * Gets the state of the trigger.
   * @returns Complete state of the trigger.
   */
  public async getState(): Promise<TriggerState> {
    const request: requests.TriggerEmptyRequest & gateway.Message = {
      ...requests.TriggerEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      toBinary() {
        return requests.TriggerEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<TriggerState>(
      'trigger/get_state',
      request,
      TriggerState.fromBinary);
    return response;
  }

  /**
   * Gets the enabled state of the trigger.
   * @returns Whether the trigger is enabled and the number of times it will fire.
   */
  public async getEnabledState(): Promise<TriggerEnabledState> {
    const request: requests.TriggerEmptyRequest & gateway.Message = {
      ...requests.TriggerEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      toBinary() {
        return requests.TriggerEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<TriggerEnabledState>(
      'trigger/get_enabled_state',
      request,
      TriggerEnabledState.fromBinary);
    return response;
  }

  /**
   * Set a generic trigger condition.
   * @param condition The condition to set for this trigger.
   */
  public async fireWhen(
    condition: string
  ): Promise<void> {
    const request: requests.TriggerFireWhenRequest & gateway.Message = {
      ...requests.TriggerFireWhenRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      condition: condition,
      toBinary() {
        return requests.TriggerFireWhenRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when', request);
  }

  /**
   * Set a trigger condition for when an encoder position has changed by a specific distance.
   * @param axis The axis to monitor for this condition.
   * May be set to 0 on single-axis devices only.
   * @param distance The measured encoder distance between trigger fires.
   * @param [unit=Units.NATIVE] Units of dist.
   */
  public async fireWhenEncoderDistanceTravelled(
    axis: number,
    distance: number,
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<void> {
    if (distance <= 0) {
      throw new TypeError('Invalid value; encoder distance must be a positive value.');
    }
    const request: requests.TriggerFireWhenDistanceTravelledRequest & gateway.Message = {
      ...requests.TriggerFireWhenDistanceTravelledRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      axis: axis,
      distance: distance,
      unit: unit,
      toBinary() {
        return requests.TriggerFireWhenDistanceTravelledRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when_encoder_distance_travelled', request);
  }

  /**
   * Set a trigger condition for when an axis position has changed by a specific distance.
   * @param axis The axis to monitor for this condition.
   * May be set to 0 on single-axis devices only.
   * @param distance The measured distance between trigger fires.
   * @param [unit=Units.NATIVE] Units of dist.
   */
  public async fireWhenDistanceTravelled(
    axis: number,
    distance: number,
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<void> {
    if (distance <= 0) {
      throw new TypeError('Invalid value; distance must be a positive value.');
    }
    const request: requests.TriggerFireWhenDistanceTravelledRequest & gateway.Message = {
      ...requests.TriggerFireWhenDistanceTravelledRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      axis: axis,
      distance: distance,
      unit: unit,
      toBinary() {
        return requests.TriggerFireWhenDistanceTravelledRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when_distance_travelled', request);
  }

  /**
   * Set a trigger condition based on an IO channel value.
   * @param portType The type of IO channel to monitor.
   * @param channel The IO channel to monitor.
   * @param triggerCondition Comparison operator.
   * @param value Comparison value.
   */
  public async fireWhenIo(
    portType: IoPortType,
    channel: number,
    triggerCondition: TriggerCondition,
    value: number
  ): Promise<void> {
    if (channel <= 0) {
      throw new TypeError('Invalid value; channel must be a positive value.');
    }
    const request: requests.TriggerFireWhenIoRequest & gateway.Message = {
      ...requests.TriggerFireWhenIoRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      portType: portType,
      channel: channel,
      triggerCondition: triggerCondition,
      value: value,
      toBinary() {
        return requests.TriggerFireWhenIoRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when_io', request);
  }

  /**
   * Set a trigger condition based on a setting value.
   * @param axis The axis to monitor for this condition.
   * Set to 0 for device-scope settings.
   * @param setting The setting to monitor.
   * @param triggerCondition Comparison operator.
   * @param value Comparison value.
   * @param [unit=Units.NATIVE] Units of value.
   */
  public async fireWhenSetting(
    axis: number,
    setting: string,
    triggerCondition: TriggerCondition,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.TriggerFireWhenSettingRequest & gateway.Message = {
      ...requests.TriggerFireWhenSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      axis: axis,
      setting: setting,
      triggerCondition: triggerCondition,
      value: value,
      unit: unit,
      toBinary() {
        return requests.TriggerFireWhenSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when_setting', request);
  }

  /**
   * Set a trigger condition based on an absolute setting value.
   * @param axis The axis to monitor for this condition.
   * Set to 0 for device-scope settings.
   * @param setting The setting to monitor.
   * @param triggerCondition Comparison operator.
   * @param value Comparison value.
   * @param [unit=Units.NATIVE] Units of value.
   */
  public async fireWhenAbsoluteSetting(
    axis: number,
    setting: string,
    triggerCondition: TriggerCondition,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.TriggerFireWhenSettingRequest & gateway.Message = {
      ...requests.TriggerFireWhenSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      axis: axis,
      setting: setting,
      triggerCondition: triggerCondition,
      value: value,
      unit: unit,
      toBinary() {
        return requests.TriggerFireWhenSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_when_setting_absolute', request);
  }

  /**
   * Set a trigger condition based on a time interval.
   * @param interval The time interval between trigger fires.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async fireAtInterval(
    interval: number,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    if (interval <= 0) {
      throw new TypeError('Invalid value; interval must be a positive value.');
    }
    const request: requests.TriggerFireAtIntervalRequest & gateway.Message = {
      ...requests.TriggerFireAtIntervalRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      interval: interval,
      unit: unit,
      toBinary() {
        return requests.TriggerFireAtIntervalRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/fire_at_interval', request);
  }

  /**
   * Set a command to be a trigger action.
   * @param action The action number to assign the command to.
   * @param axis The axis to on which to run this command.
   * Set to 0 for device-scope settings or to run command on all axes.
   * @param command The command to run when the action is triggered.
   */
  public async onFire(
    action: TriggerAction,
    axis: number,
    command: string
  ): Promise<void> {
    const request: requests.TriggerOnFireRequest & gateway.Message = {
      ...requests.TriggerOnFireRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      action: action,
      axis: axis,
      command: command,
      toBinary() {
        return requests.TriggerOnFireRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/on_fire', request);
  }

  /**
   * Set a trigger action to update a setting.
   * @param action The action number to assign the command to.
   * @param axis The axis on which to change the setting.
   * Set to 0 to change the setting for the device.
   * @param setting The name of the setting to change.
   * @param operation The operation to apply to the setting.
   * @param value Operation value.
   * @param [unit=Units.NATIVE] Units of value.
   */
  public async onFireSet(
    action: TriggerAction,
    axis: number,
    setting: string,
    operation: TriggerOperation,
    value: number,
    unit: Units = Units.NATIVE
  ): Promise<void> {
    const request: requests.TriggerOnFireSetRequest & gateway.Message = {
      ...requests.TriggerOnFireSetRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      action: action,
      axis: axis,
      setting: setting,
      operation: operation,
      value: value,
      unit: unit,
      toBinary() {
        return requests.TriggerOnFireSetRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/on_fire_set', request);
  }

  /**
   * Set a trigger action to update a setting with the value of another setting.
   * @param action The action number to assign the command to.
   * @param axis The axis on which to change the setting.
   * Set to 0 to change the setting for the device.
   * @param setting The name of the setting to change.
   * Must have either integer or boolean type.
   * @param operation The operation to apply to the setting.
   * @param fromAxis The axis from which to read the setting.
   * Set to 0 to read the setting from the device.
   * @param fromSetting The name of the setting to read.
   * Must have either integer or boolean type.
   */
  public async onFireSetToSetting(
    action: TriggerAction,
    axis: number,
    setting: string,
    operation: TriggerOperation,
    fromAxis: number,
    fromSetting: string
  ): Promise<void> {
    const request: requests.TriggerOnFireSetToSettingRequest & gateway.Message = {
      ...requests.TriggerOnFireSetToSettingRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      action: action,
      axis: axis,
      setting: setting,
      operation: operation,
      fromAxis: fromAxis,
      fromSetting: fromSetting,
      toBinary() {
        return requests.TriggerOnFireSetToSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/on_fire_set_to_setting', request);
  }

  /**
   * Clear a trigger action.
   * @param [action=TriggerAction.ALL] The action number to clear.
   * The default option is to clear all actions.
   */
  public async clearAction(
    action: TriggerAction = TriggerAction.ALL
  ): Promise<void> {
    const request: requests.TriggerClearActionRequest & gateway.Message = {
      ...requests.TriggerClearActionRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      action: action,
      toBinary() {
        return requests.TriggerClearActionRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/clear_action', request);
  }

  /**
   * Returns the label for the trigger.
   * @returns The label for the trigger.
   */
  public async getLabel(): Promise<string> {
    const request: requests.TriggerEmptyRequest & gateway.Message = {
      ...requests.TriggerEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      toBinary() {
        return requests.TriggerEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'trigger/get_label',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the label for the trigger.
   * @param label The label to set for this trigger.
   * If no value or an empty string is provided, this label is deleted.
   */
  public async setLabel(
    label?: string
  ): Promise<void> {
    const request: requests.TriggerSetLabelRequest & gateway.Message = {
      ...requests.TriggerSetLabelRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      triggerNumber: this.triggerNumber,
      label: label,
      toBinary() {
        return requests.TriggerSetLabelRequest.toBinary(this);
      },
    };

    await gateway.callAsync('trigger/set_label', request);
  }
}
