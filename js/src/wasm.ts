import { setEnv } from './gateway/bindings';
import { runWasm, RunWasmResult } from './gateway/wasm';

let initializing = false;
let runningWasm: RunWasmResult | null = null;

export const isRunning = (): boolean => runningWasm !== null;

export const init = async (fetchResponse: Promise<any>): Promise<RunWasmResult> => {
  if (isRunning() || initializing) {
    throw new Error('Zaber motion library already running or is initializing.');
  }

  setEnv('wasm');

  try {
    initializing = true;

    runningWasm = await runWasm(fetchResponse);
    runningWasm.runPromise.then(() => runningWasm = null, () => runningWasm = null);
  } finally {
    initializing = false;
  }

  return runningWasm;
};

export const shutdown = async () => {
  const currentWasm = runningWasm;
  if (!currentWasm) {
    return;
  }

  window.__zmlShutdown();
  await currentWasm.runPromise;
};
