﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from './gateway';
import * as requests from './requests';

/**
 * Class providing various utility functions.
 */
export class Tools {
  /**
   * Lists all serial ports on the computer.
   * @returns Array of serial port names.
   */
  public static async listSerialPorts(): Promise<string[]> {
    const request: requests.EmptyRequest & gateway.Message = {
      ...requests.EmptyRequest.DEFAULT,
      toBinary() {
        return requests.EmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.ToolsListSerialPortsResponse>(
      'tools/list_serial_ports',
      request,
      requests.ToolsListSerialPortsResponse.fromBinary);
    return response.ports;
  }

  /**
   * Returns path of message router named pipe on Windows
   * or file path of unix domain socket on UNIX.
   * @returns Path of message router's named pipe or unix domain socket.
   */
  public static getMessageRouterPipePath(): string {
    const request: requests.EmptyRequest & gateway.Message = {
      ...requests.EmptyRequest.DEFAULT,
      toBinary() {
        return requests.EmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'tools/get_message_router_pipe',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the path for communicating with a local device database service.
   * This will be a named pipe on Windows and the file path of a unix domain socket on UNIX.
   * @returns Path of database service's named pipe or unix domain socket.
   */
  public static getDbServicePipePath(): string {
    const request: requests.EmptyRequest & gateway.Message = {
      ...requests.EmptyRequest.DEFAULT,
      toBinary() {
        return requests.EmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'tools/get_db_service_pipe',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}
