/* This file is generated. Do not modify by hand. */
/**
 * Type of source of Device DB data.
 */
export enum DeviceDbSourceType {
  /** WebService. */
  WEB_SERVICE = 0,
  /** File. */
  FILE = 1,
}
