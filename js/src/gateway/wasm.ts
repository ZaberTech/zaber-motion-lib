import { wasmExec } from './wasm-exec';

export interface RunWasmResult {
  runPromise: Promise<void>;
}

export const runWasm = async (fetchResponse: Promise<any>): Promise<RunWasmResult> => {
  if (window.Go == null) {
    wasmExec();
  }
  const go = new window.Go();
  const result = await window.WebAssembly.instantiateStreaming(
    fetchResponse,
    go.importObject
  );
  const startPromise = new Promise<void>(resolve => window.__zmlStarted = resolve);
  const runPromise = go.run(result.instance);

  await Promise.race([startPromise, runPromise]);
  return { runPromise };
};
