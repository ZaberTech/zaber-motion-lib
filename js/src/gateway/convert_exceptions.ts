﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Errors as PbErrors } from '../requests/errors';
import * as exceptions from '../exceptions';

const errorMapping = new Map<PbErrors, new (message: string, customData: Uint8Array) => exceptions.MotionLibException>();
errorMapping.set(PbErrors.BAD_COMMAND, exceptions.BadCommandException);
errorMapping.set(PbErrors.BAD_DATA, exceptions.BadDataException);
errorMapping.set(PbErrors.BINARY_COMMAND_FAILED, exceptions.BinaryCommandFailedException);
errorMapping.set(PbErrors.COMMAND_FAILED, exceptions.CommandFailedException);
errorMapping.set(PbErrors.COMMAND_PREEMPTED, exceptions.CommandPreemptedException);
errorMapping.set(PbErrors.COMMAND_TOO_LONG, exceptions.CommandTooLongException);
errorMapping.set(PbErrors.CONNECTION_CLOSED, exceptions.ConnectionClosedException);
errorMapping.set(PbErrors.CONNECTION_FAILED, exceptions.ConnectionFailedException);
errorMapping.set(PbErrors.CONVERSION_FAILED, exceptions.ConversionFailedException);
errorMapping.set(PbErrors.DEVICE_ADDRESS_CONFLICT, exceptions.DeviceAddressConflictException);
errorMapping.set(PbErrors.DEVICE_BUSY, exceptions.DeviceBusyException);
errorMapping.set(PbErrors.DEVICE_DB_FAILED, exceptions.DeviceDbFailedException);
errorMapping.set(PbErrors.DEVICE_DETECTION_FAILED, exceptions.DeviceDetectionFailedException);
errorMapping.set(PbErrors.DEVICE_FAILED, exceptions.DeviceFailedException);
errorMapping.set(PbErrors.DEVICE_NOT_IDENTIFIED, exceptions.DeviceNotIdentifiedException);
errorMapping.set(PbErrors.DRIVER_DISABLED, exceptions.DriverDisabledException);
errorMapping.set(PbErrors.G_CODE_EXECUTION, exceptions.GCodeExecutionException);
errorMapping.set(PbErrors.G_CODE_SYNTAX, exceptions.GCodeSyntaxException);
errorMapping.set(PbErrors.INCOMPATIBLE_SHARED_LIBRARY, exceptions.IncompatibleSharedLibraryException);
errorMapping.set(PbErrors.INTERNAL_ERROR, exceptions.InternalErrorException);
errorMapping.set(PbErrors.INVALID_ARGUMENT, exceptions.InvalidArgumentException);
errorMapping.set(PbErrors.INVALID_DATA, exceptions.InvalidDataException);
errorMapping.set(PbErrors.INVALID_OPERATION, exceptions.InvalidOperationException);
errorMapping.set(PbErrors.INVALID_PACKET, exceptions.InvalidPacketException);
errorMapping.set(PbErrors.INVALID_PARK_STATE, exceptions.InvalidParkStateException);
errorMapping.set(PbErrors.INVALID_REQUEST_DATA, exceptions.InvalidRequestDataException);
errorMapping.set(PbErrors.INVALID_RESPONSE, exceptions.InvalidResponseException);
errorMapping.set(PbErrors.IO_CHANNEL_OUT_OF_RANGE, exceptions.IoChannelOutOfRangeException);
errorMapping.set(PbErrors.IO_FAILED, exceptions.IoFailedException);
errorMapping.set(PbErrors.LOCKSTEP_ENABLED, exceptions.LockstepEnabledException);
errorMapping.set(PbErrors.LOCKSTEP_NOT_ENABLED, exceptions.LockstepNotEnabledException);
errorMapping.set(PbErrors.MOVEMENT_FAILED, exceptions.MovementFailedException);
errorMapping.set(PbErrors.MOVEMENT_INTERRUPTED, exceptions.MovementInterruptedException);
errorMapping.set(PbErrors.NO_DEVICE_FOUND, exceptions.NoDeviceFoundException);
errorMapping.set(PbErrors.NO_VALUE_FOR_KEY, exceptions.NoValueForKeyException);
errorMapping.set(PbErrors.NOT_SUPPORTED, exceptions.NotSupportedException);
errorMapping.set(PbErrors.OPERATION_FAILED, exceptions.OperationFailedException);
errorMapping.set(PbErrors.OS_FAILED, exceptions.OsFailedException);
errorMapping.set(PbErrors.OUT_OF_REQUEST_IDS, exceptions.OutOfRequestIdsException);
errorMapping.set(PbErrors.PVT_DISCONTINUITY, exceptions.PvtDiscontinuityException);
errorMapping.set(PbErrors.PVT_EXECUTION, exceptions.PvtExecutionException);
errorMapping.set(PbErrors.PVT_MODE, exceptions.PvtModeException);
errorMapping.set(PbErrors.PVT_MOVEMENT_FAILED, exceptions.PvtMovementFailedException);
errorMapping.set(PbErrors.PVT_MOVEMENT_INTERRUPTED, exceptions.PvtMovementInterruptedException);
errorMapping.set(PbErrors.PVT_SETUP_FAILED, exceptions.PvtSetupFailedException);
errorMapping.set(PbErrors.REMOTE_MODE, exceptions.RemoteModeException);
errorMapping.set(PbErrors.REQUEST_TIMEOUT, exceptions.RequestTimeoutException);
errorMapping.set(PbErrors.SERIAL_PORT_BUSY, exceptions.SerialPortBusyException);
errorMapping.set(PbErrors.SET_DEVICE_STATE_FAILED, exceptions.SetDeviceStateFailedException);
errorMapping.set(PbErrors.SET_PERIPHERAL_STATE_FAILED, exceptions.SetPeripheralStateFailedException);
errorMapping.set(PbErrors.SETTING_NOT_FOUND, exceptions.SettingNotFoundException);
errorMapping.set(PbErrors.STREAM_DISCONTINUITY, exceptions.StreamDiscontinuityException);
errorMapping.set(PbErrors.STREAM_EXECUTION, exceptions.StreamExecutionException);
errorMapping.set(PbErrors.STREAM_MODE, exceptions.StreamModeException);
errorMapping.set(PbErrors.STREAM_MOVEMENT_FAILED, exceptions.StreamMovementFailedException);
errorMapping.set(PbErrors.STREAM_MOVEMENT_INTERRUPTED, exceptions.StreamMovementInterruptedException);
errorMapping.set(PbErrors.STREAM_SETUP_FAILED, exceptions.StreamSetupFailedException);
errorMapping.set(PbErrors.TIMEOUT, exceptions.TimeoutException);
errorMapping.set(PbErrors.TRANSPORT_ALREADY_USED, exceptions.TransportAlreadyUsedException);
errorMapping.set(PbErrors.UNKNOWN_REQUEST, exceptions.UnknownRequestException);

export function convertToException(error: PbErrors, message: string, customData?: Uint8Array): exceptions.MotionLibException {
  const SpecificException = errorMapping.get(error);

  /* istanbul ignore if */
  if (!SpecificException) {
    return new exceptions.MotionLibException(message);
  }

  return new SpecificException(message, customData!);
}
