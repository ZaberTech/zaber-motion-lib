export interface Serializable { toBinary(): Uint8Array }

const SIZE_TYPE_SIZE = 4; // uint32

export const serialize = (...messages: (Serializable | undefined)[]) => {
  const buffers: Uint8Array[] = [];

  let size = SIZE_TYPE_SIZE; // total length
  for (const message of messages.filter(m => m != null)) {
    const bytes = message!.toBinary();
    buffers.push(bytes);
    size += SIZE_TYPE_SIZE + bytes.length; // length + structure
  }

  const buffer = new Uint8Array(size);
  const dataView = new DataView(buffer.buffer);
  let offset = 0;

  dataView.setUint32(offset, size, true);
  offset += SIZE_TYPE_SIZE;

  for (const message of buffers) {
    dataView.setUint32(offset, message.length, true);
    offset += SIZE_TYPE_SIZE;

    buffer.set(message, offset);
    offset += message.length;
  }

  return buffer;
};

export const deserialize = (buffer: Uint8Array) => {
  const dataView = new DataView(buffer.buffer);
  let offset = SIZE_TYPE_SIZE;
  const buffers: Uint8Array[] = [];
  while (offset < buffer.length) {
    const size = dataView.getUint32(offset, true);
    offset += SIZE_TYPE_SIZE;

    const structBuffer = buffer.slice(offset, offset + size);
    buffers.push(structBuffer);

    offset += size;
  }
  return buffers;
};
