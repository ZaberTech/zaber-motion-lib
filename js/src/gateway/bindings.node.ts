import path from 'path';

import pkgUp from 'pkg-up';
import binary from '@mapbox/node-pre-gyp';

const packageJsonPath = pkgUp.sync({ cwd: __dirname });
const bindingPath = binary.find(packageJsonPath);
// eslint-disable-next-line @typescript-eslint/no-require-imports
export const binding = require(bindingPath);

let dllPath = path.dirname(bindingPath) + path.sep;

const isElectron = !!(process.versions as unknown as Record<string, string>).electron;
if (isElectron && !dllPath.includes('app.asar.unpacked')) {
  dllPath = dllPath.replace('app.asar', 'app.asar.unpacked');
}

binding.loadDll(dllPath);
