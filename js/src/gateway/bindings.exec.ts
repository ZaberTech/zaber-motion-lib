import path from 'path';
import { bindingLibPath } from './bindings';

const execLibPath = bindingLibPath.at(-1) !== path.sep ? `${bindingLibPath}${path.sep}` : bindingLibPath;
try {
  process.dlopen(module, path.join(execLibPath, 'zaber-motion.node'));
  module.exports.loadDll(execLibPath);
} catch (error) {
  const errorStr = error instanceof Error ? String(error) : JSON.stringify(error);
  throw new Error(`Failed to load bindings at "${execLibPath}" (${errorStr})`);
}
