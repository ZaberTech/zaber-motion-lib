export interface Message {
  toBinary(): Uint8Array;
}

export type FromBinary<T> = (buffer: Uint8Array) => T;
