export const binding = {
  setEventHandler: (eventHandler: (eventUint8Array: Uint8Array) => void): void => {
    window.__zmlSetEventHandler((eventString: string) =>
      eventHandler(decodeBase64(eventString))
    );
  },
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setEventsEnabled: (): void => {},
  callAsync: (data: Uint8Array, resolve: (buffer: Uint8Array) => void): void => {
    const requestBytesString = encodeBase64(data);
    window.__zmlRequest(requestBytesString, (responseString: string) =>
      resolve(decodeBase64(responseString))
    );
  },
  callSync: (data: Uint8Array): Uint8Array => {
    const requestBytesString = encodeBase64(data);
    const responseString = window.__zmlRequest(requestBytesString);
    return decodeBase64(responseString);
  }
};

function encodeBase64(buffer: Uint8Array) {
  const output = [];
  for (const segment of buffer) {
    output.push(String.fromCharCode(segment));
  }
  return window.btoa(output.join(''));
}

function decodeBase64(chars: string) {
  return Uint8Array.from(window.atob(chars), c => c.charCodeAt(0));
}
