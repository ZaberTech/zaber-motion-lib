/* eslint-disable @typescript-eslint/no-require-imports */
type Env = 'node' | 'wasm' | 'exec';

let execEnv: Env = 'node';
export let bindingLibPath = '';
let locked = false;
let bindings: any;
let initCallbacks: (() => void)[] | null = [];

export function setEnv(env: Exclude<Env, 'exec'>): void;
export function setEnv(env: 'exec', libPath: string): void;
export function setEnv(env: Env, libPath?: string): void {
  if (locked) {
    throw new Error('Library was already loaded.');
  }
  execEnv = env;
  if (libPath != null) {
    bindingLibPath = libPath;
  }
}

export const getBinding = (): any => {
  if (bindings != null) {
    return bindings;
  }
  locked = true;
  if (execEnv === 'exec') {
    bindings = require('./bindings.exec');
  } else if (execEnv === 'node') {
    bindings = require('./bindings.node').binding;
  } else {
    bindings = require('./bindings.wasm').binding;
  }

  initCallbacks!.forEach(callback => callback());
  initCallbacks = null;

  return bindings;
};

export const addInitCallback = (cb: () => void) => {
  if (initCallbacks == null) {
    throw new Error('Library was already loaded.');
  }
  initCallbacks.push(cb);
};
