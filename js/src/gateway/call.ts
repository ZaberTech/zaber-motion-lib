import { getBinding } from './bindings';
import { Serializable, serialize, deserialize } from './serialization';
import { convertToException } from './convert_exceptions';
import { FromBinary, Message } from './message';
import { GatewayRequest, GatewayResponse, ResponseType } from '../requests';

export async function callAsync(
  request: string,
  requestData?: Serializable
): Promise<null>;

export async function callAsync<TReturn>(
  request: string,
  requestData: Serializable | undefined,
  responseProto: FromBinary<TReturn>,
): Promise<TReturn>;

export async function callAsync<TReturn>(
  request: string,
  requestData?: Serializable,
  responseProto?: FromBinary<TReturn>,
): Promise<TReturn | null> {
  const requestProto: GatewayRequest & Message = {
    request,
    toBinary() {
      return GatewayRequest.toBinary(this);
    },
  };

  const data = serialize(requestProto, requestData);

  const resultBuffer = await new Promise<Buffer>(resolve =>
    getBinding().callAsync(data, resolve)
  );

  const structBuffers = deserialize(resultBuffer);

  return processResponse(structBuffers, responseProto);
}

export function callSync(
  request: string,
  requestData?: Serializable,
): null;

export function callSync<TReturn>(
  request: string,
  requestData: Serializable | undefined,
  responseProto: FromBinary<TReturn>,
): TReturn;

export function callSync<TReturn>(
  request: string,
  requestData?: Serializable,
  responseProto?: FromBinary<TReturn>,
): TReturn | null {
  const requestProto: GatewayRequest & Message = {
    request,
    toBinary() {
      return GatewayRequest.toBinary(this);
    },
  };

  const data = serialize(requestProto, requestData);

  const resultBuffer: Buffer = getBinding().callSync(data);

  const structBuffers = deserialize(resultBuffer);

  return processResponse(structBuffers, responseProto);
}

function processResponse<TReturn>(structBuffers: Uint8Array[], responseProto?: FromBinary<TReturn>): TReturn | null {
  const response = GatewayResponse.fromBinary(structBuffers[0]);

  if (response.response !== ResponseType.OK) {
    throw convertToException(response.errorType, response.errorMessage, structBuffers[1]);
  }

  /* istanbul ignore if */
  if (responseProto && structBuffers.length <= 1) {
    throw new Error('No response from library');
  }
  /* istanbul ignore if */
  if (!responseProto && structBuffers.length > 1) {
    throw new Error('Response from library ignored');
  }
  if (responseProto) {
    return responseProto(structBuffers[1]);
  } else {
    return null;
  }
}
