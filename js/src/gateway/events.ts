import { Observable, Observer } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import {
  GatewayEvent, TestEvent, DisconnectedEvent, UnknownResponseEventWrapper,
  UnknownBinaryResponseEventWrapper, AlertEventWrapper, BinaryReplyOnlyEventWrapper,
} from '../requests';

import { addInitCallback, getBinding } from './bindings';
import { deserialize } from './serialization';

interface SdkEvent {
  eventType: string;
  event: unknown;
}

const subscriptions = new Set<Observer<SdkEvent>>();

export const events = new Observable<SdkEvent>((observer: Observer<SdkEvent>) => {
  if (subscriptions.size === 0) {
    getBinding().setEventsEnabled(true);
  }

  subscriptions.add(observer);

  return () => {
    subscriptions.delete(observer);

    if (subscriptions.size === 0) {
      getBinding().setEventsEnabled(false);
    }
  };
});

const eventParsers: Record<string, (buffer: Uint8Array) => unknown> = {
  'test/event': TestEvent.fromBinary,
  'interface/unknown_response': UnknownResponseEventWrapper.fromBinary,
  'binary/interface/unknown_response': UnknownBinaryResponseEventWrapper.fromBinary,
  'interface/alert': AlertEventWrapper.fromBinary,
  'binary/interface/reply_only': BinaryReplyOnlyEventWrapper.fromBinary,
  'interface/disconnected': DisconnectedEvent.fromBinary,
};

const eventHandler = (eventBuffer: Buffer) => {
  const structBuffers = deserialize(eventBuffer);
  const event = GatewayEvent.fromBinary(structBuffers[0]);
  const eventData = structBuffers[1] || null;

  const eventName = event.event;
  const eventParser = eventParsers[eventName];
  /* istanbul ignore if */
  if (eventParser == null) {
    throw new Error(`Unknown event: ${eventName}`);
  }

  const eventObject: SdkEvent = { eventType: event.event, event: eventParser(eventData) };

  for (const observer of subscriptions) {
    observer.next(eventObject);
  }
};

addInitCallback(() => getBinding().setEventHandler(eventHandler));

export const filterEvent = <T>(eventTypeToFilter: string) => (source: Observable<SdkEvent>) =>
  source.pipe(
    filter(({ eventType }) => eventType === eventTypeToFilter),
    map<SdkEvent, T>(({ event }) => event as T)
  );
