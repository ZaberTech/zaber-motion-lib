import type { Units } from '../units';

type EnumDefinition<T> = Record<string | number, string | T>;

export const sanitizer = {
  sanitizeNumber(value: number, label: string): number {
    if (typeof value !== 'number') {
      throw new TypeError(`Expected ${label} to be number but got ${JSON.stringify(value)}.`);
    }
    return value;
  },
  sanitizeInt(value: number, label: string): number {
    const safeNumber = this.sanitizeNumber(value, label);
    if (!Number.isSafeInteger(safeNumber)) {
      throw new TypeError(`Expected ${label} to be integer but got ${safeNumber}.`);
    }
    return safeNumber;
  },
  sanitizeBool(value: boolean, label: string): boolean { // eslint-disable-line @typescript-eslint/no-unused-vars
    return Boolean(value);
  },
  sanitizeString(value: string, label: string): string {
    // null is tolerated for strings defaulting in empty string.
    if (value == null) { return '' }

    if (typeof value !== 'string') {
      throw new TypeError(`Expected ${label} to be string but got ${JSON.stringify(value)}.`);
    }
    return value;
  },
  sanitizeEnum<TEnum extends number>(
    value: TEnum,
    enumName: string,
    enumType: EnumDefinition<TEnum>,
    label: string,
  ): TEnum {
    if (typeof value !== 'number' || enumType[value] == null) {
      throw new TypeError(`Expected ${label} to be value of ${enumName} enum but got ${value}.`);
    }
    return value;
  },
  sanitizeUnits(value: Units, label: string): Units {
    // null is tolerated for units defaulting to NATIVE.
    if (value == null) { return '' } // Units.NATIVE

    if (typeof value !== 'string') {
      throw new TypeError(`Expected ${label} to be Units string but got ${JSON.stringify(value)}.`);
    }
    return value;
  },
};
