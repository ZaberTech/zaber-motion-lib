/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from './gateway/sanitizer';

/**
 * Holds device address and axis number.
 */
export interface AxisAddress {
  /**
   * Device address.
   */
  device: number;

  /**
   * Axis number.
   */
  axis: number;

}

export const AxisAddress = {
  fromBinary: (buffer: Uint8Array): AxisAddress => BSON.deserialize(buffer) as AxisAddress,
  toBinary: (value: AxisAddress): Uint8Array => BSON.serialize(AxisAddress.sanitize(value)),
  DEFAULT: Object.freeze({
    device: 0,
    axis: 0,
  }) as Readonly<AxisAddress>,
  sanitize: (value: AxisAddress): AxisAddress => {
    if (value == null) { throw new TypeError('Expected AxisAddress object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisAddress object but got ${typeof value}.`) }
    return {
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
    };
  },
};
