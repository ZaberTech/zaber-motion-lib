/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { CommandCode } from '../binary/command_code';

export interface GenericBinaryRequest {
  interfaceId: number;

  device: number;

  command: CommandCode;

  data: number;

  checkErrors: boolean;

  timeout: number;

}

export const GenericBinaryRequest = {
  fromBinary: (buffer: Uint8Array): GenericBinaryRequest => BSON.deserialize(buffer) as GenericBinaryRequest,
  toBinary: (value: GenericBinaryRequest): Uint8Array => BSON.serialize(GenericBinaryRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    command: 0 as CommandCode,
    data: 0,
    checkErrors: false,
    timeout: 0,
  }) as Readonly<GenericBinaryRequest>,
  sanitize: (value: GenericBinaryRequest): GenericBinaryRequest => {
    if (value == null) { throw new TypeError('Expected GenericBinaryRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GenericBinaryRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      command: sanitizer.sanitizeEnum(value.command, 'CommandCode', CommandCode, 'command'),
      data: sanitizer.sanitizeInt(value.data, 'data'),
      checkErrors: sanitizer.sanitizeBool(value.checkErrors, 'checkErrors'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
    };
  },
};
