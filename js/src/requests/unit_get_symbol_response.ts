/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface UnitGetSymbolResponse {
  symbol: string;

}

export const UnitGetSymbolResponse = {
  fromBinary: (buffer: Uint8Array): UnitGetSymbolResponse => BSON.deserialize(buffer) as UnitGetSymbolResponse,
  toBinary: (value: UnitGetSymbolResponse): Uint8Array => BSON.serialize(UnitGetSymbolResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    symbol: '',
  }) as Readonly<UnitGetSymbolResponse>,
  sanitize: (value: UnitGetSymbolResponse): UnitGetSymbolResponse => {
    if (value == null) { throw new TypeError('Expected UnitGetSymbolResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnitGetSymbolResponse object but got ${typeof value}.`) }
    return {
      symbol: sanitizer.sanitizeString(value.symbol, 'symbol'),
    };
  },
};
