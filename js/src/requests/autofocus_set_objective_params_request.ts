/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { NamedParameter } from '../named_parameter';

export interface AutofocusSetObjectiveParamsRequest {
  providerId: number;

  interfaceId: number;

  focusAddress: number;

  focusAxis: number;

  turretAddress: number;

  objective: number;

  parameters: NamedParameter[];

}

export const AutofocusSetObjectiveParamsRequest = {
  fromBinary: (buffer: Uint8Array): AutofocusSetObjectiveParamsRequest => BSON.deserialize(buffer) as AutofocusSetObjectiveParamsRequest,
  toBinary: (value: AutofocusSetObjectiveParamsRequest): Uint8Array => BSON.serialize(AutofocusSetObjectiveParamsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    providerId: 0,
    interfaceId: 0,
    focusAddress: 0,
    focusAxis: 0,
    turretAddress: 0,
    objective: 0,
    parameters: [],
  }) as Readonly<AutofocusSetObjectiveParamsRequest>,
  sanitize: (value: AutofocusSetObjectiveParamsRequest): AutofocusSetObjectiveParamsRequest => {
    if (value == null) { throw new TypeError('Expected AutofocusSetObjectiveParamsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusSetObjectiveParamsRequest object but got ${typeof value}.`) }
    return {
      providerId: sanitizer.sanitizeInt(value.providerId, 'providerId'),
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      objective: sanitizer.sanitizeInt(value.objective, 'objective'),
      parameters: Array.from(value.parameters ?? [], item => NamedParameter.sanitize(item)),
    };
  },
};
