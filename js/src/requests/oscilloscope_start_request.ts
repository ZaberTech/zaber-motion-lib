/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeStartRequest {
  interfaceId: number;

  device: number;

  captureLength: number;

}

export const OscilloscopeStartRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeStartRequest => BSON.deserialize(buffer) as OscilloscopeStartRequest,
  toBinary: (value: OscilloscopeStartRequest): Uint8Array => BSON.serialize(OscilloscopeStartRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    captureLength: 0,
  }) as Readonly<OscilloscopeStartRequest>,
  sanitize: (value: OscilloscopeStartRequest): OscilloscopeStartRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeStartRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeStartRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      captureLength: sanitizer.sanitizeInt(value.captureLength, 'captureLength'),
    };
  },
};
