/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { BinarySettings } from '../binary/binary_settings';
import { Units } from '../units';

export interface BinaryDeviceGetSettingRequest {
  interfaceId: number;

  device: number;

  setting: BinarySettings;

  unit: Units;

}

export const BinaryDeviceGetSettingRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceGetSettingRequest => BSON.deserialize(buffer) as BinaryDeviceGetSettingRequest,
  toBinary: (value: BinaryDeviceGetSettingRequest): Uint8Array => BSON.serialize(BinaryDeviceGetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    setting: 0 as BinarySettings,
    unit: Units.NATIVE,
  }) as Readonly<BinaryDeviceGetSettingRequest>,
  sanitize: (value: BinaryDeviceGetSettingRequest): BinaryDeviceGetSettingRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceGetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceGetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      setting: sanitizer.sanitizeEnum(value.setting, 'BinarySettings', BinarySettings, 'setting'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
