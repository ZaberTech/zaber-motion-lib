/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetHoldRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  hold: boolean;

}

export const StreamSetHoldRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetHoldRequest => BSON.deserialize(buffer) as StreamSetHoldRequest,
  toBinary: (value: StreamSetHoldRequest): Uint8Array => BSON.serialize(StreamSetHoldRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    hold: false,
  }) as Readonly<StreamSetHoldRequest>,
  sanitize: (value: StreamSetHoldRequest): StreamSetHoldRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetHoldRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetHoldRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      hold: sanitizer.sanitizeBool(value.hold, 'hold'),
    };
  },
};
