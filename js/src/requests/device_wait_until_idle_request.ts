/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceWaitUntilIdleRequest {
  interfaceId: number;

  device: number;

  axis: number;

  throwErrorOnFault: boolean;

}

export const DeviceWaitUntilIdleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceWaitUntilIdleRequest => BSON.deserialize(buffer) as DeviceWaitUntilIdleRequest,
  toBinary: (value: DeviceWaitUntilIdleRequest): Uint8Array => BSON.serialize(DeviceWaitUntilIdleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    throwErrorOnFault: false,
  }) as Readonly<DeviceWaitUntilIdleRequest>,
  sanitize: (value: DeviceWaitUntilIdleRequest): DeviceWaitUntilIdleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceWaitUntilIdleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceWaitUntilIdleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      throwErrorOnFault: sanitizer.sanitizeBool(value.throwErrorOnFault, 'throwErrorOnFault'),
    };
  },
};
