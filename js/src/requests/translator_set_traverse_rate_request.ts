/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TranslatorSetTraverseRateRequest {
  translatorId: number;

  traverseRate: number;

  unit: Units;

}

export const TranslatorSetTraverseRateRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorSetTraverseRateRequest => BSON.deserialize(buffer) as TranslatorSetTraverseRateRequest,
  toBinary: (value: TranslatorSetTraverseRateRequest): Uint8Array => BSON.serialize(TranslatorSetTraverseRateRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    traverseRate: 0,
    unit: Units.NATIVE,
  }) as Readonly<TranslatorSetTraverseRateRequest>,
  sanitize: (value: TranslatorSetTraverseRateRequest): TranslatorSetTraverseRateRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorSetTraverseRateRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorSetTraverseRateRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      traverseRate: sanitizer.sanitizeNumber(value.traverseRate, 'traverseRate'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
