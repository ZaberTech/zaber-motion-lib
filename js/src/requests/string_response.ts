/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StringResponse {
  value: string;

}

export const StringResponse = {
  fromBinary: (buffer: Uint8Array): StringResponse => BSON.deserialize(buffer) as StringResponse,
  toBinary: (value: StringResponse): Uint8Array => BSON.serialize(StringResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    value: '',
  }) as Readonly<StringResponse>,
  sanitize: (value: StringResponse): StringResponse => {
    if (value == null) { throw new TypeError('Expected StringResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StringResponse object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeString(value.value, 'value'),
    };
  },
};
