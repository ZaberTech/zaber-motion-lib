/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface MicroscopeTriggerCameraRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  delay: number;

  unit: Units;

  wait: boolean;

}

export const MicroscopeTriggerCameraRequest = {
  fromBinary: (buffer: Uint8Array): MicroscopeTriggerCameraRequest => BSON.deserialize(buffer) as MicroscopeTriggerCameraRequest,
  toBinary: (value: MicroscopeTriggerCameraRequest): Uint8Array => BSON.serialize(MicroscopeTriggerCameraRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    delay: 0,
    unit: Units.NATIVE,
    wait: false,
  }) as Readonly<MicroscopeTriggerCameraRequest>,
  sanitize: (value: MicroscopeTriggerCameraRequest): MicroscopeTriggerCameraRequest => {
    if (value == null) { throw new TypeError('Expected MicroscopeTriggerCameraRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeTriggerCameraRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      wait: sanitizer.sanitizeBool(value.wait, 'wait'),
    };
  },
};
