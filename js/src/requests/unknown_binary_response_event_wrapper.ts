/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { UnknownResponseEvent } from '../binary/unknown_response_event';

export interface UnknownBinaryResponseEventWrapper {
  interfaceId: number;

  unknownResponse: UnknownResponseEvent;

}

export const UnknownBinaryResponseEventWrapper = {
  fromBinary: (buffer: Uint8Array): UnknownBinaryResponseEventWrapper => BSON.deserialize(buffer) as UnknownBinaryResponseEventWrapper,
  toBinary: (value: UnknownBinaryResponseEventWrapper): Uint8Array => BSON.serialize(UnknownBinaryResponseEventWrapper.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    unknownResponse: UnknownResponseEvent.DEFAULT,
  }) as Readonly<UnknownBinaryResponseEventWrapper>,
  sanitize: (value: UnknownBinaryResponseEventWrapper): UnknownBinaryResponseEventWrapper => {
    if (value == null) { throw new TypeError('Expected UnknownBinaryResponseEventWrapper object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnknownBinaryResponseEventWrapper object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      unknownResponse: UnknownResponseEvent.sanitize(value.unknownResponse),
    };
  },
};
