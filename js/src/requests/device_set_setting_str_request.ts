/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetSettingStrRequest {
  interfaceId: number;

  device: number;

  axis: number;

  setting: string;

  value: string;

}

export const DeviceSetSettingStrRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetSettingStrRequest => BSON.deserialize(buffer) as DeviceSetSettingStrRequest,
  toBinary: (value: DeviceSetSettingStrRequest): Uint8Array => BSON.serialize(DeviceSetSettingStrRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    setting: '',
    value: '',
  }) as Readonly<DeviceSetSettingStrRequest>,
  sanitize: (value: DeviceSetSettingStrRequest): DeviceSetSettingStrRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetSettingStrRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetSettingStrRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      value: sanitizer.sanitizeString(value.value, 'value'),
    };
  },
};
