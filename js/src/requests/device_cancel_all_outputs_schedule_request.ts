/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceCancelAllOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  analog: boolean;

  channels: boolean[];

}

export const DeviceCancelAllOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceCancelAllOutputsScheduleRequest => BSON.deserialize(buffer) as DeviceCancelAllOutputsScheduleRequest,
  toBinary: (value: DeviceCancelAllOutputsScheduleRequest): Uint8Array => BSON.serialize(DeviceCancelAllOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    analog: false,
    channels: [],
  }) as Readonly<DeviceCancelAllOutputsScheduleRequest>,
  sanitize: (value: DeviceCancelAllOutputsScheduleRequest): DeviceCancelAllOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceCancelAllOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceCancelAllOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      analog: sanitizer.sanitizeBool(value.analog, 'analog'),
      channels: Array.from(value.channels ?? [], item => sanitizer.sanitizeBool(item, 'items of channels')),
    };
  },
};
