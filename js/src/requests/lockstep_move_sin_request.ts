/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface LockstepMoveSinRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  amplitude: number;

  amplitudeUnits: Units;

  period: number;

  periodUnits: Units;

  count: number;

  waitUntilIdle: boolean;

}

export const LockstepMoveSinRequest = {
  fromBinary: (buffer: Uint8Array): LockstepMoveSinRequest => BSON.deserialize(buffer) as LockstepMoveSinRequest,
  toBinary: (value: LockstepMoveSinRequest): Uint8Array => BSON.serialize(LockstepMoveSinRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    amplitude: 0,
    amplitudeUnits: Units.NATIVE,
    period: 0,
    periodUnits: Units.NATIVE,
    count: 0,
    waitUntilIdle: false,
  }) as Readonly<LockstepMoveSinRequest>,
  sanitize: (value: LockstepMoveSinRequest): LockstepMoveSinRequest => {
    if (value == null) { throw new TypeError('Expected LockstepMoveSinRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepMoveSinRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      amplitude: sanitizer.sanitizeNumber(value.amplitude, 'amplitude'),
      amplitudeUnits: sanitizer.sanitizeUnits(value.amplitudeUnits, 'amplitudeUnits'),
      period: sanitizer.sanitizeNumber(value.period, 'period'),
      periodUnits: sanitizer.sanitizeUnits(value.periodUnits, 'periodUnits'),
      count: sanitizer.sanitizeNumber(value.count, 'count'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
