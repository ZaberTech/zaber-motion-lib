/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface LockstepSetRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  value: number;

  unit: Units;

  axisIndex: number;

}

export const LockstepSetRequest = {
  fromBinary: (buffer: Uint8Array): LockstepSetRequest => BSON.deserialize(buffer) as LockstepSetRequest,
  toBinary: (value: LockstepSetRequest): Uint8Array => BSON.serialize(LockstepSetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    value: 0,
    unit: Units.NATIVE,
    axisIndex: 0,
  }) as Readonly<LockstepSetRequest>,
  sanitize: (value: LockstepSetRequest): LockstepSetRequest => {
    if (value == null) { throw new TypeError('Expected LockstepSetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepSetRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      axisIndex: sanitizer.sanitizeInt(value.axisIndex, 'axisIndex'),
    };
  },
};
