/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface IntResponse {
  value: number;

}

export const IntResponse = {
  fromBinary: (buffer: Uint8Array): IntResponse => BSON.deserialize(buffer) as IntResponse,
  toBinary: (value: IntResponse): Uint8Array => BSON.serialize(IntResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    value: 0,
  }) as Readonly<IntResponse>,
  sanitize: (value: IntResponse): IntResponse => {
    if (value == null) { throw new TypeError('Expected IntResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected IntResponse object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeInt(value.value, 'value'),
    };
  },
};
