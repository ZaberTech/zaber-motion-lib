/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OpenInterfaceResponse {
  interfaceId: number;

}

export const OpenInterfaceResponse = {
  fromBinary: (buffer: Uint8Array): OpenInterfaceResponse => BSON.deserialize(buffer) as OpenInterfaceResponse,
  toBinary: (value: OpenInterfaceResponse): Uint8Array => BSON.serialize(OpenInterfaceResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
  }) as Readonly<OpenInterfaceResponse>,
  sanitize: (value: OpenInterfaceResponse): OpenInterfaceResponse => {
    if (value == null) { throw new TypeError('Expected OpenInterfaceResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OpenInterfaceResponse object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
    };
  },
};
