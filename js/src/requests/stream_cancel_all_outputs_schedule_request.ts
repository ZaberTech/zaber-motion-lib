/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamCancelAllOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  analog: boolean;

  channels: boolean[];

}

export const StreamCancelAllOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamCancelAllOutputsScheduleRequest => BSON.deserialize(buffer) as StreamCancelAllOutputsScheduleRequest,
  toBinary: (value: StreamCancelAllOutputsScheduleRequest): Uint8Array => BSON.serialize(StreamCancelAllOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    analog: false,
    channels: [],
  }) as Readonly<StreamCancelAllOutputsScheduleRequest>,
  sanitize: (value: StreamCancelAllOutputsScheduleRequest): StreamCancelAllOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamCancelAllOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamCancelAllOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      analog: sanitizer.sanitizeBool(value.analog, 'analog'),
      channels: Array.from(value.channels ?? [], item => sanitizer.sanitizeBool(item, 'items of channels')),
    };
  },
};
