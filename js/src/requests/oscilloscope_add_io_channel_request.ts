/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { IoPortType } from '../ascii/io_port_type';

export interface OscilloscopeAddIoChannelRequest {
  interfaceId: number;

  device: number;

  ioType: IoPortType;

  ioChannel: number;

}

export const OscilloscopeAddIoChannelRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeAddIoChannelRequest => BSON.deserialize(buffer) as OscilloscopeAddIoChannelRequest,
  toBinary: (value: OscilloscopeAddIoChannelRequest): Uint8Array => BSON.serialize(OscilloscopeAddIoChannelRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    ioType: 0 as IoPortType,
    ioChannel: 0,
  }) as Readonly<OscilloscopeAddIoChannelRequest>,
  sanitize: (value: OscilloscopeAddIoChannelRequest): OscilloscopeAddIoChannelRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeAddIoChannelRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeAddIoChannelRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      ioType: sanitizer.sanitizeEnum(value.ioType, 'IoPortType', IoPortType, 'ioType'),
      ioChannel: sanitizer.sanitizeInt(value.ioChannel, 'ioChannel'),
    };
  },
};
