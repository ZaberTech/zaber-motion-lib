/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface AutofocusGetObjectiveParamsRequest {
  providerId: number;

  interfaceId: number;

  focusAddress: number;

  focusAxis: number;

  turretAddress: number;

  objective: number;

}

export const AutofocusGetObjectiveParamsRequest = {
  fromBinary: (buffer: Uint8Array): AutofocusGetObjectiveParamsRequest => BSON.deserialize(buffer) as AutofocusGetObjectiveParamsRequest,
  toBinary: (value: AutofocusGetObjectiveParamsRequest): Uint8Array => BSON.serialize(AutofocusGetObjectiveParamsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    providerId: 0,
    interfaceId: 0,
    focusAddress: 0,
    focusAxis: 0,
    turretAddress: 0,
    objective: 0,
  }) as Readonly<AutofocusGetObjectiveParamsRequest>,
  sanitize: (value: AutofocusGetObjectiveParamsRequest): AutofocusGetObjectiveParamsRequest => {
    if (value == null) { throw new TypeError('Expected AutofocusGetObjectiveParamsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusGetObjectiveParamsRequest object but got ${typeof value}.`) }
    return {
      providerId: sanitizer.sanitizeInt(value.providerId, 'providerId'),
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      objective: sanitizer.sanitizeInt(value.objective, 'objective'),
    };
  },
};
