/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetAllAnalogIOResponse {
  values: number[];

}

export const DeviceGetAllAnalogIOResponse = {
  fromBinary: (buffer: Uint8Array): DeviceGetAllAnalogIOResponse => BSON.deserialize(buffer) as DeviceGetAllAnalogIOResponse,
  toBinary: (value: DeviceGetAllAnalogIOResponse): Uint8Array => BSON.serialize(DeviceGetAllAnalogIOResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    values: [],
  }) as Readonly<DeviceGetAllAnalogIOResponse>,
  sanitize: (value: DeviceGetAllAnalogIOResponse): DeviceGetAllAnalogIOResponse => {
    if (value == null) { throw new TypeError('Expected DeviceGetAllAnalogIOResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetAllAnalogIOResponse object but got ${typeof value}.`) }
    return {
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
    };
  },
};
