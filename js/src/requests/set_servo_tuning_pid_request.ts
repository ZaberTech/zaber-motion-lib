/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';

export interface SetServoTuningPIDRequest {
  interfaceId: number;

  device: number;

  axis: number;

  paramset: ServoTuningParamset;

  p: number;

  i: number;

  d: number;

  fc: number;

}

export const SetServoTuningPIDRequest = {
  fromBinary: (buffer: Uint8Array): SetServoTuningPIDRequest => BSON.deserialize(buffer) as SetServoTuningPIDRequest,
  toBinary: (value: SetServoTuningPIDRequest): Uint8Array => BSON.serialize(SetServoTuningPIDRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    paramset: 0 as ServoTuningParamset,
    p: 0,
    i: 0,
    d: 0,
    fc: 0,
  }) as Readonly<SetServoTuningPIDRequest>,
  sanitize: (value: SetServoTuningPIDRequest): SetServoTuningPIDRequest => {
    if (value == null) { throw new TypeError('Expected SetServoTuningPIDRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetServoTuningPIDRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      paramset: sanitizer.sanitizeEnum(value.paramset, 'ServoTuningParamset', ServoTuningParamset, 'paramset'),
      p: sanitizer.sanitizeNumber(value.p, 'p'),
      i: sanitizer.sanitizeNumber(value.i, 'i'),
      d: sanitizer.sanitizeNumber(value.d, 'd'),
      fc: sanitizer.sanitizeNumber(value.fc, 'fc'),
    };
  },
};
