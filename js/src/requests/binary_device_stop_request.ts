/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface BinaryDeviceStopRequest {
  interfaceId: number;

  device: number;

  timeout: number;

  unit: Units;

}

export const BinaryDeviceStopRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceStopRequest => BSON.deserialize(buffer) as BinaryDeviceStopRequest,
  toBinary: (value: BinaryDeviceStopRequest): Uint8Array => BSON.serialize(BinaryDeviceStopRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    timeout: 0,
    unit: Units.NATIVE,
  }) as Readonly<BinaryDeviceStopRequest>,
  sanitize: (value: BinaryDeviceStopRequest): BinaryDeviceStopRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceStopRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceStopRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
