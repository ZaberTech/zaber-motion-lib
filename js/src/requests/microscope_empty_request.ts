/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { MicroscopeConfig } from '../microscopy/microscope_config';

export interface MicroscopeEmptyRequest {
  interfaceId: number;

  config: MicroscopeConfig;

}

export const MicroscopeEmptyRequest = {
  fromBinary: (buffer: Uint8Array): MicroscopeEmptyRequest => BSON.deserialize(buffer) as MicroscopeEmptyRequest,
  toBinary: (value: MicroscopeEmptyRequest): Uint8Array => BSON.serialize(MicroscopeEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    config: MicroscopeConfig.DEFAULT,
  }) as Readonly<MicroscopeEmptyRequest>,
  sanitize: (value: MicroscopeEmptyRequest): MicroscopeEmptyRequest => {
    if (value == null) { throw new TypeError('Expected MicroscopeEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      config: MicroscopeConfig.sanitize(value.config),
    };
  },
};
