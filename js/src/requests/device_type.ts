/* This file is generated. Do not modify by hand. */
export enum DeviceType {
  /** Any. */
  ANY = 0,
  /** ProcessController. */
  PROCESS_CONTROLLER = 1,
}
