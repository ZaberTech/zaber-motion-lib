/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DeviceDbSourceType } from '../device_db_source_type';

export interface SetDeviceDbSourceRequest {
  sourceType: DeviceDbSourceType;

  urlOrFilePath?: (string | null);

}

export const SetDeviceDbSourceRequest = {
  fromBinary: (buffer: Uint8Array): SetDeviceDbSourceRequest => BSON.deserialize(buffer) as SetDeviceDbSourceRequest,
  toBinary: (value: SetDeviceDbSourceRequest): Uint8Array => BSON.serialize(SetDeviceDbSourceRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    sourceType: 0 as DeviceDbSourceType,
    urlOrFilePath: null,
  }) as Readonly<SetDeviceDbSourceRequest>,
  sanitize: (value: SetDeviceDbSourceRequest): SetDeviceDbSourceRequest => {
    if (value == null) { throw new TypeError('Expected SetDeviceDbSourceRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetDeviceDbSourceRequest object but got ${typeof value}.`) }
    return {
      sourceType: sanitizer.sanitizeEnum(value.sourceType, 'DeviceDbSourceType', DeviceDbSourceType, 'sourceType'),
      urlOrFilePath: value.urlOrFilePath != null ? sanitizer.sanitizeString(value.urlOrFilePath, 'urlOrFilePath') : null,
    };
  },
};
