/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamBufferGetContentResponse {
  bufferLines: string[];

}

export const StreamBufferGetContentResponse = {
  fromBinary: (buffer: Uint8Array): StreamBufferGetContentResponse => BSON.deserialize(buffer) as StreamBufferGetContentResponse,
  toBinary: (value: StreamBufferGetContentResponse): Uint8Array => BSON.serialize(StreamBufferGetContentResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    bufferLines: [],
  }) as Readonly<StreamBufferGetContentResponse>,
  sanitize: (value: StreamBufferGetContentResponse): StreamBufferGetContentResponse => {
    if (value == null) { throw new TypeError('Expected StreamBufferGetContentResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamBufferGetContentResponse object but got ${typeof value}.`) }
    return {
      bufferLines: Array.from(value.bufferLines ?? [], item => sanitizer.sanitizeString(item, 'items of bufferLines')),
    };
  },
};
