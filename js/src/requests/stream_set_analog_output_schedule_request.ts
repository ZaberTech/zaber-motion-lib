/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamSetAnalogOutputScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  value: number;

  futureValue: number;

  delay: number;

  unit: Units;

}

export const StreamSetAnalogOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAnalogOutputScheduleRequest => BSON.deserialize(buffer) as StreamSetAnalogOutputScheduleRequest,
  toBinary: (value: StreamSetAnalogOutputScheduleRequest): Uint8Array => BSON.serialize(StreamSetAnalogOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    value: 0,
    futureValue: 0,
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetAnalogOutputScheduleRequest>,
  sanitize: (value: StreamSetAnalogOutputScheduleRequest): StreamSetAnalogOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAnalogOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAnalogOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      futureValue: sanitizer.sanitizeNumber(value.futureValue, 'futureValue'),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
