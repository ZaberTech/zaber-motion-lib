/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TestEvent {
  data: string;

}

export const TestEvent = {
  fromBinary: (buffer: Uint8Array): TestEvent => BSON.deserialize(buffer) as TestEvent,
  toBinary: (value: TestEvent): Uint8Array => BSON.serialize(TestEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    data: '',
  }) as Readonly<TestEvent>,
  sanitize: (value: TestEvent): TestEvent => {
    if (value == null) { throw new TypeError('Expected TestEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TestEvent object but got ${typeof value}.`) }
    return {
      data: sanitizer.sanitizeString(value.data, 'data'),
    };
  },
};
