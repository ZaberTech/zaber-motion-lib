/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';

export interface ServoTuningParamsetResponse {
  paramset: ServoTuningParamset;

}

export const ServoTuningParamsetResponse = {
  fromBinary: (buffer: Uint8Array): ServoTuningParamsetResponse => BSON.deserialize(buffer) as ServoTuningParamsetResponse,
  toBinary: (value: ServoTuningParamsetResponse): Uint8Array => BSON.serialize(ServoTuningParamsetResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    paramset: 0 as ServoTuningParamset,
  }) as Readonly<ServoTuningParamsetResponse>,
  sanitize: (value: ServoTuningParamsetResponse): ServoTuningParamsetResponse => {
    if (value == null) { throw new TypeError('Expected ServoTuningParamsetResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ServoTuningParamsetResponse object but got ${typeof value}.`) }
    return {
      paramset: sanitizer.sanitizeEnum(value.paramset, 'ServoTuningParamset', ServoTuningParamset, 'paramset'),
    };
  },
};
