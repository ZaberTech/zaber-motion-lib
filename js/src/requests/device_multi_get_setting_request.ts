/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { GetSetting } from '../ascii/get_setting';
import { GetAxisSetting } from '../ascii/get_axis_setting';

export interface DeviceMultiGetSettingRequest {
  interfaceId: number;

  device: number;

  axis: number;

  settings: GetSetting[];

  axisSettings: GetAxisSetting[];

}

export const DeviceMultiGetSettingRequest = {
  fromBinary: (buffer: Uint8Array): DeviceMultiGetSettingRequest => BSON.deserialize(buffer) as DeviceMultiGetSettingRequest,
  toBinary: (value: DeviceMultiGetSettingRequest): Uint8Array => BSON.serialize(DeviceMultiGetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    settings: [],
    axisSettings: [],
  }) as Readonly<DeviceMultiGetSettingRequest>,
  sanitize: (value: DeviceMultiGetSettingRequest): DeviceMultiGetSettingRequest => {
    if (value == null) { throw new TypeError('Expected DeviceMultiGetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceMultiGetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      settings: Array.from(value.settings ?? [], item => GetSetting.sanitize(item)),
      axisSettings: Array.from(value.axisSettings ?? [], item => GetAxisSetting.sanitize(item)),
    };
  },
};
