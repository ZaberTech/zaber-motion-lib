/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ObjectiveChangerRequest {
  interfaceId: number;

  turretAddress: number;

  focusAddress: number;

  focusAxis: number;

}

export const ObjectiveChangerRequest = {
  fromBinary: (buffer: Uint8Array): ObjectiveChangerRequest => BSON.deserialize(buffer) as ObjectiveChangerRequest,
  toBinary: (value: ObjectiveChangerRequest): Uint8Array => BSON.serialize(ObjectiveChangerRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    turretAddress: 0,
    focusAddress: 0,
    focusAxis: 0,
  }) as Readonly<ObjectiveChangerRequest>,
  sanitize: (value: ObjectiveChangerRequest): ObjectiveChangerRequest => {
    if (value == null) { throw new TypeError('Expected ObjectiveChangerRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ObjectiveChangerRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
    };
  },
};
