/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetWarningsRequest {
  interfaceId: number;

  device: number;

  axis: number;

  clear: boolean;

}

export const DeviceGetWarningsRequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetWarningsRequest => BSON.deserialize(buffer) as DeviceGetWarningsRequest,
  toBinary: (value: DeviceGetWarningsRequest): Uint8Array => BSON.serialize(DeviceGetWarningsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    clear: false,
  }) as Readonly<DeviceGetWarningsRequest>,
  sanitize: (value: DeviceGetWarningsRequest): DeviceGetWarningsRequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetWarningsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetWarningsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      clear: sanitizer.sanitizeBool(value.clear, 'clear'),
    };
  },
};
