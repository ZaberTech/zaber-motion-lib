/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamSetMaxCentripetalAccelerationRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  maxCentripetalAcceleration: number;

  unit: Units;

}

export const StreamSetMaxCentripetalAccelerationRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetMaxCentripetalAccelerationRequest => BSON.deserialize(buffer) as StreamSetMaxCentripetalAccelerationRequest,
  toBinary: (value: StreamSetMaxCentripetalAccelerationRequest): Uint8Array => BSON.serialize(StreamSetMaxCentripetalAccelerationRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    maxCentripetalAcceleration: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetMaxCentripetalAccelerationRequest>,
  sanitize: (value: StreamSetMaxCentripetalAccelerationRequest): StreamSetMaxCentripetalAccelerationRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetMaxCentripetalAccelerationRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetMaxCentripetalAccelerationRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      maxCentripetalAcceleration: sanitizer.sanitizeNumber(value.maxCentripetalAcceleration, 'maxCentripetalAcceleration'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
