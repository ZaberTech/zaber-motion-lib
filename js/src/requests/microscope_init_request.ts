/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { MicroscopeConfig } from '../microscopy/microscope_config';

export interface MicroscopeInitRequest {
  interfaceId: number;

  config: MicroscopeConfig;

  force: boolean;

}

export const MicroscopeInitRequest = {
  fromBinary: (buffer: Uint8Array): MicroscopeInitRequest => BSON.deserialize(buffer) as MicroscopeInitRequest,
  toBinary: (value: MicroscopeInitRequest): Uint8Array => BSON.serialize(MicroscopeInitRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    config: MicroscopeConfig.DEFAULT,
    force: false,
  }) as Readonly<MicroscopeInitRequest>,
  sanitize: (value: MicroscopeInitRequest): MicroscopeInitRequest => {
    if (value == null) { throw new TypeError('Expected MicroscopeInitRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeInitRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      config: MicroscopeConfig.sanitize(value.config),
      force: sanitizer.sanitizeBool(value.force, 'force'),
    };
  },
};
