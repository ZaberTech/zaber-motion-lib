/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { FirmwareVersion } from '../firmware_version';

export interface CanSetStateRequest {
  interfaceId: number;

  device: number;

  axis: number;

  state: string;

  firmwareVersion?: (FirmwareVersion | null);

}

export const CanSetStateRequest = {
  fromBinary: (buffer: Uint8Array): CanSetStateRequest => BSON.deserialize(buffer) as CanSetStateRequest,
  toBinary: (value: CanSetStateRequest): Uint8Array => BSON.serialize(CanSetStateRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    state: '',
    firmwareVersion: null,
  }) as Readonly<CanSetStateRequest>,
  sanitize: (value: CanSetStateRequest): CanSetStateRequest => {
    if (value == null) { throw new TypeError('Expected CanSetStateRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CanSetStateRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      state: sanitizer.sanitizeString(value.state, 'state'),
      firmwareVersion: value.firmwareVersion != null ? FirmwareVersion.sanitize(value.firmwareVersion) : null,
    };
  },
};
