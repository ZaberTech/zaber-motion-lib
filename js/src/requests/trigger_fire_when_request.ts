/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TriggerFireWhenRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  condition: string;

}

export const TriggerFireWhenRequest = {
  fromBinary: (buffer: Uint8Array): TriggerFireWhenRequest => BSON.deserialize(buffer) as TriggerFireWhenRequest,
  toBinary: (value: TriggerFireWhenRequest): Uint8Array => BSON.serialize(TriggerFireWhenRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    condition: '',
  }) as Readonly<TriggerFireWhenRequest>,
  sanitize: (value: TriggerFireWhenRequest): TriggerFireWhenRequest => {
    if (value == null) { throw new TypeError('Expected TriggerFireWhenRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerFireWhenRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      condition: sanitizer.sanitizeString(value.condition, 'condition'),
    };
  },
};
