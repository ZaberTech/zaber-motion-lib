/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface UnitGetEnumResponse {
  unit: Units;

}

export const UnitGetEnumResponse = {
  fromBinary: (buffer: Uint8Array): UnitGetEnumResponse => BSON.deserialize(buffer) as UnitGetEnumResponse,
  toBinary: (value: UnitGetEnumResponse): Uint8Array => BSON.serialize(UnitGetEnumResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    unit: Units.NATIVE,
  }) as Readonly<UnitGetEnumResponse>,
  sanitize: (value: UnitGetEnumResponse): UnitGetEnumResponse => {
    if (value == null) { throw new TypeError('Expected UnitGetEnumResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnitGetEnumResponse object but got ${typeof value}.`) }
    return {
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
