/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TriggerAction } from '../ascii/trigger_action';
import { TriggerOperation } from '../ascii/trigger_operation';
import { Units } from '../units';

export interface TriggerOnFireSetRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  action: TriggerAction;

  axis: number;

  setting: string;

  operation: TriggerOperation;

  value: number;

  unit: Units;

}

export const TriggerOnFireSetRequest = {
  fromBinary: (buffer: Uint8Array): TriggerOnFireSetRequest => BSON.deserialize(buffer) as TriggerOnFireSetRequest,
  toBinary: (value: TriggerOnFireSetRequest): Uint8Array => BSON.serialize(TriggerOnFireSetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    action: 0 as TriggerAction,
    axis: 0,
    setting: '',
    operation: 0 as TriggerOperation,
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<TriggerOnFireSetRequest>,
  sanitize: (value: TriggerOnFireSetRequest): TriggerOnFireSetRequest => {
    if (value == null) { throw new TypeError('Expected TriggerOnFireSetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerOnFireSetRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      action: sanitizer.sanitizeEnum(value.action, 'TriggerAction', TriggerAction, 'action'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      operation: sanitizer.sanitizeEnum(value.operation, 'TriggerOperation', TriggerOperation, 'operation'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
