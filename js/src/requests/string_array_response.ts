/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StringArrayResponse {
  values: string[];

}

export const StringArrayResponse = {
  fromBinary: (buffer: Uint8Array): StringArrayResponse => BSON.deserialize(buffer) as StringArrayResponse,
  toBinary: (value: StringArrayResponse): Uint8Array => BSON.serialize(StringArrayResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    values: [],
  }) as Readonly<StringArrayResponse>,
  sanitize: (value: StringArrayResponse): StringArrayResponse => {
    if (value == null) { throw new TypeError('Expected StringArrayResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StringArrayResponse object but got ${typeof value}.`) }
    return {
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeString(item, 'items of values')),
    };
  },
};
