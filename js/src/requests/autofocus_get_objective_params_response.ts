/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { NamedParameter } from '../named_parameter';

export interface AutofocusGetObjectiveParamsResponse {
  parameters: NamedParameter[];

}

export const AutofocusGetObjectiveParamsResponse = {
  fromBinary: (buffer: Uint8Array): AutofocusGetObjectiveParamsResponse => BSON.deserialize(buffer) as AutofocusGetObjectiveParamsResponse,
  toBinary: (value: AutofocusGetObjectiveParamsResponse): Uint8Array => BSON.serialize(AutofocusGetObjectiveParamsResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    parameters: [],
  }) as Readonly<AutofocusGetObjectiveParamsResponse>,
  sanitize: (value: AutofocusGetObjectiveParamsResponse): AutofocusGetObjectiveParamsResponse => {
    if (value == null) { throw new TypeError('Expected AutofocusGetObjectiveParamsResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusGetObjectiveParamsResponse object but got ${typeof value}.`) }
    return {
      parameters: Array.from(value.parameters ?? [], item => NamedParameter.sanitize(item)),
    };
  },
};
