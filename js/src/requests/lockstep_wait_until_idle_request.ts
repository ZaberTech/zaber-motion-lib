/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepWaitUntilIdleRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  throwErrorOnFault: boolean;

}

export const LockstepWaitUntilIdleRequest = {
  fromBinary: (buffer: Uint8Array): LockstepWaitUntilIdleRequest => BSON.deserialize(buffer) as LockstepWaitUntilIdleRequest,
  toBinary: (value: LockstepWaitUntilIdleRequest): Uint8Array => BSON.serialize(LockstepWaitUntilIdleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    throwErrorOnFault: false,
  }) as Readonly<LockstepWaitUntilIdleRequest>,
  sanitize: (value: LockstepWaitUntilIdleRequest): LockstepWaitUntilIdleRequest => {
    if (value == null) { throw new TypeError('Expected LockstepWaitUntilIdleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepWaitUntilIdleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      throwErrorOnFault: sanitizer.sanitizeBool(value.throwErrorOnFault, 'throwErrorOnFault'),
    };
  },
};
