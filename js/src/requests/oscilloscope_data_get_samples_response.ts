/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeDataGetSamplesResponse {
  data: number[];

}

export const OscilloscopeDataGetSamplesResponse = {
  fromBinary: (buffer: Uint8Array): OscilloscopeDataGetSamplesResponse => BSON.deserialize(buffer) as OscilloscopeDataGetSamplesResponse,
  toBinary: (value: OscilloscopeDataGetSamplesResponse): Uint8Array => BSON.serialize(OscilloscopeDataGetSamplesResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    data: [],
  }) as Readonly<OscilloscopeDataGetSamplesResponse>,
  sanitize: (value: OscilloscopeDataGetSamplesResponse): OscilloscopeDataGetSamplesResponse => {
    if (value == null) { throw new TypeError('Expected OscilloscopeDataGetSamplesResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeDataGetSamplesResponse object but got ${typeof value}.`) }
    return {
      data: Array.from(value.data ?? [], item => sanitizer.sanitizeNumber(item, 'items of data')),
    };
  },
};
