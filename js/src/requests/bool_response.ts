/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface BoolResponse {
  value: boolean;

}

export const BoolResponse = {
  fromBinary: (buffer: Uint8Array): BoolResponse => BSON.deserialize(buffer) as BoolResponse,
  toBinary: (value: BoolResponse): Uint8Array => BSON.serialize(BoolResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    value: false,
  }) as Readonly<BoolResponse>,
  sanitize: (value: BoolResponse): BoolResponse => {
    if (value == null) { throw new TypeError('Expected BoolResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BoolResponse object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeBool(value.value, 'value'),
    };
  },
};
