/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { BinarySettings } from '../binary/binary_settings';
import { Units } from '../units';

export interface BinaryDeviceSetSettingRequest {
  interfaceId: number;

  device: number;

  setting: BinarySettings;

  value: number;

  unit: Units;

}

export const BinaryDeviceSetSettingRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceSetSettingRequest => BSON.deserialize(buffer) as BinaryDeviceSetSettingRequest,
  toBinary: (value: BinaryDeviceSetSettingRequest): Uint8Array => BSON.serialize(BinaryDeviceSetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    setting: 0 as BinarySettings,
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<BinaryDeviceSetSettingRequest>,
  sanitize: (value: BinaryDeviceSetSettingRequest): BinaryDeviceSetSettingRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceSetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceSetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      setting: sanitizer.sanitizeEnum(value.setting, 'BinarySettings', BinarySettings, 'setting'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
