/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ResponseType } from '../requests/response_type';
import { Errors } from '../requests/errors';

export interface GatewayResponse {
  response: ResponseType;

  errorType: Errors;

  errorMessage: string;

}

export const GatewayResponse = {
  fromBinary: (buffer: Uint8Array): GatewayResponse => BSON.deserialize(buffer) as GatewayResponse,
  toBinary: (value: GatewayResponse): Uint8Array => BSON.serialize(GatewayResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    response: 0 as ResponseType,
    errorType: 0 as Errors,
    errorMessage: '',
  }) as Readonly<GatewayResponse>,
  sanitize: (value: GatewayResponse): GatewayResponse => {
    if (value == null) { throw new TypeError('Expected GatewayResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GatewayResponse object but got ${typeof value}.`) }
    return {
      response: sanitizer.sanitizeEnum(value.response, 'ResponseType', ResponseType, 'response'),
      errorType: sanitizer.sanitizeEnum(value.errorType, 'Errors', Errors, 'errorType'),
      errorMessage: sanitizer.sanitizeString(value.errorMessage, 'errorMessage'),
    };
  },
};
