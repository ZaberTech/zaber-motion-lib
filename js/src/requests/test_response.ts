/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TestResponse {
  dataPong: string;

}

export const TestResponse = {
  fromBinary: (buffer: Uint8Array): TestResponse => BSON.deserialize(buffer) as TestResponse,
  toBinary: (value: TestResponse): Uint8Array => BSON.serialize(TestResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    dataPong: '',
  }) as Readonly<TestResponse>,
  sanitize: (value: TestResponse): TestResponse => {
    if (value == null) { throw new TypeError('Expected TestResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TestResponse object but got ${typeof value}.`) }
    return {
      dataPong: sanitizer.sanitizeString(value.dataPong, 'dataPong'),
    };
  },
};
