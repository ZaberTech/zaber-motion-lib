/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeDataIdentifier {
  dataId: number;

}

export const OscilloscopeDataIdentifier = {
  fromBinary: (buffer: Uint8Array): OscilloscopeDataIdentifier => BSON.deserialize(buffer) as OscilloscopeDataIdentifier,
  toBinary: (value: OscilloscopeDataIdentifier): Uint8Array => BSON.serialize(OscilloscopeDataIdentifier.sanitize(value)),
  DEFAULT: Object.freeze({
    dataId: 0,
  }) as Readonly<OscilloscopeDataIdentifier>,
  sanitize: (value: OscilloscopeDataIdentifier): OscilloscopeDataIdentifier => {
    if (value == null) { throw new TypeError('Expected OscilloscopeDataIdentifier object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeDataIdentifier object but got ${typeof value}.`) }
    return {
      dataId: sanitizer.sanitizeInt(value.dataId, 'dataId'),
    };
  },
};
