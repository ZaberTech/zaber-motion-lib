/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';

export interface DeviceSetDigitalOutputRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  value: DigitalOutputAction;

}

export const DeviceSetDigitalOutputRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetDigitalOutputRequest => BSON.deserialize(buffer) as DeviceSetDigitalOutputRequest,
  toBinary: (value: DeviceSetDigitalOutputRequest): Uint8Array => BSON.serialize(DeviceSetDigitalOutputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    value: 0 as DigitalOutputAction,
  }) as Readonly<DeviceSetDigitalOutputRequest>,
  sanitize: (value: DeviceSetDigitalOutputRequest): DeviceSetDigitalOutputRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetDigitalOutputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetDigitalOutputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeEnum(value.value, 'DigitalOutputAction', DigitalOutputAction, 'value'),
    };
  },
};
