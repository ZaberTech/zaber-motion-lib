/* This file is generated. Do not modify by hand. */
export enum InterfaceType {
  /** SerialPort. */
  SERIAL_PORT = 0,
  /** Tcp. */
  TCP = 1,
  /** Custom. */
  CUSTOM = 2,
  /** Iot. */
  IOT = 3,
  /** NetworkShare. */
  NETWORK_SHARE = 4,
}
