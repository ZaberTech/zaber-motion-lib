/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { UnknownResponseEvent } from '../ascii/unknown_response_event';

export interface UnknownResponseEventWrapper {
  interfaceId: number;

  unknownResponse: UnknownResponseEvent;

}

export const UnknownResponseEventWrapper = {
  fromBinary: (buffer: Uint8Array): UnknownResponseEventWrapper => BSON.deserialize(buffer) as UnknownResponseEventWrapper,
  toBinary: (value: UnknownResponseEventWrapper): Uint8Array => BSON.serialize(UnknownResponseEventWrapper.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    unknownResponse: UnknownResponseEvent.DEFAULT,
  }) as Readonly<UnknownResponseEventWrapper>,
  sanitize: (value: UnknownResponseEventWrapper): UnknownResponseEventWrapper => {
    if (value == null) { throw new TypeError('Expected UnknownResponseEventWrapper object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnknownResponseEventWrapper object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      unknownResponse: UnknownResponseEvent.sanitize(value.unknownResponse),
    };
  },
};
