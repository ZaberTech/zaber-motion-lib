/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ReplyOnlyEvent } from '../binary/reply_only_event';

export interface BinaryReplyOnlyEventWrapper {
  interfaceId: number;

  reply: ReplyOnlyEvent;

}

export const BinaryReplyOnlyEventWrapper = {
  fromBinary: (buffer: Uint8Array): BinaryReplyOnlyEventWrapper => BSON.deserialize(buffer) as BinaryReplyOnlyEventWrapper,
  toBinary: (value: BinaryReplyOnlyEventWrapper): Uint8Array => BSON.serialize(BinaryReplyOnlyEventWrapper.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    reply: ReplyOnlyEvent.DEFAULT,
  }) as Readonly<BinaryReplyOnlyEventWrapper>,
  sanitize: (value: BinaryReplyOnlyEventWrapper): BinaryReplyOnlyEventWrapper => {
    if (value == null) { throw new TypeError('Expected BinaryReplyOnlyEventWrapper object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryReplyOnlyEventWrapper object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      reply: ReplyOnlyEvent.sanitize(value.reply),
    };
  },
};
