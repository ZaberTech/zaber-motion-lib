/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ObjectiveChangerGetCurrentResponse {
  value: number;

}

export const ObjectiveChangerGetCurrentResponse = {
  fromBinary: (buffer: Uint8Array): ObjectiveChangerGetCurrentResponse => BSON.deserialize(buffer) as ObjectiveChangerGetCurrentResponse,
  toBinary: (value: ObjectiveChangerGetCurrentResponse): Uint8Array => BSON.serialize(ObjectiveChangerGetCurrentResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    value: 0,
  }) as Readonly<ObjectiveChangerGetCurrentResponse>,
  sanitize: (value: ObjectiveChangerGetCurrentResponse): ObjectiveChangerGetCurrentResponse => {
    if (value == null) { throw new TypeError('Expected ObjectiveChangerGetCurrentResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ObjectiveChangerGetCurrentResponse object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeInt(value.value, 'value'),
    };
  },
};
