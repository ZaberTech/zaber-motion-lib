/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepHomeRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  waitUntilIdle: boolean;

}

export const LockstepHomeRequest = {
  fromBinary: (buffer: Uint8Array): LockstepHomeRequest => BSON.deserialize(buffer) as LockstepHomeRequest,
  toBinary: (value: LockstepHomeRequest): Uint8Array => BSON.serialize(LockstepHomeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    waitUntilIdle: false,
  }) as Readonly<LockstepHomeRequest>,
  sanitize: (value: LockstepHomeRequest): LockstepHomeRequest => {
    if (value == null) { throw new TypeError('Expected LockstepHomeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepHomeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
