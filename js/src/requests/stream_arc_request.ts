/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamSegmentType } from '../requests/stream_segment_type';
import { RotationDirection } from '../rotation_direction';
import { Measurement } from '../measurement';

export interface StreamArcRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  type: StreamSegmentType;

  rotationDirection: RotationDirection;

  centerX: Measurement;

  centerY: Measurement;

  endX: Measurement;

  endY: Measurement;

  targetAxesIndices: number[];

  endpoint: Measurement[];

}

export const StreamArcRequest = {
  fromBinary: (buffer: Uint8Array): StreamArcRequest => BSON.deserialize(buffer) as StreamArcRequest,
  toBinary: (value: StreamArcRequest): Uint8Array => BSON.serialize(StreamArcRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    type: 0 as StreamSegmentType,
    rotationDirection: 0 as RotationDirection,
    centerX: Measurement.DEFAULT,
    centerY: Measurement.DEFAULT,
    endX: Measurement.DEFAULT,
    endY: Measurement.DEFAULT,
    targetAxesIndices: [],
    endpoint: [],
  }) as Readonly<StreamArcRequest>,
  sanitize: (value: StreamArcRequest): StreamArcRequest => {
    if (value == null) { throw new TypeError('Expected StreamArcRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamArcRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      type: sanitizer.sanitizeEnum(value.type, 'StreamSegmentType', StreamSegmentType, 'type'),
      rotationDirection: sanitizer.sanitizeEnum(value.rotationDirection, 'RotationDirection', RotationDirection, 'rotationDirection'),
      centerX: Measurement.sanitize(value.centerX),
      centerY: Measurement.sanitize(value.centerY),
      endX: Measurement.sanitize(value.endX),
      endY: Measurement.sanitize(value.endY),
      targetAxesIndices: Array.from(value.targetAxesIndices ?? [], item => sanitizer.sanitizeInt(item, 'items of targetAxesIndices')),
      endpoint: Array.from(value.endpoint ?? [], item => Measurement.sanitize(item)),
    };
  },
};
