/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface IntRequest {
  value: number;

}

export const IntRequest = {
  fromBinary: (buffer: Uint8Array): IntRequest => BSON.deserialize(buffer) as IntRequest,
  toBinary: (value: IntRequest): Uint8Array => BSON.serialize(IntRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    value: 0,
  }) as Readonly<IntRequest>,
  sanitize: (value: IntRequest): IntRequest => {
    if (value == null) { throw new TypeError('Expected IntRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected IntRequest object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeInt(value.value, 'value'),
    };
  },
};
