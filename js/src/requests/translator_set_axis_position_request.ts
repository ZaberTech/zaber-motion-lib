/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TranslatorSetAxisPositionRequest {
  translatorId: number;

  axis: string;

  position: number;

  unit: Units;

}

export const TranslatorSetAxisPositionRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorSetAxisPositionRequest => BSON.deserialize(buffer) as TranslatorSetAxisPositionRequest,
  toBinary: (value: TranslatorSetAxisPositionRequest): Uint8Array => BSON.serialize(TranslatorSetAxisPositionRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    axis: '',
    position: 0,
    unit: Units.NATIVE,
  }) as Readonly<TranslatorSetAxisPositionRequest>,
  sanitize: (value: TranslatorSetAxisPositionRequest): TranslatorSetAxisPositionRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorSetAxisPositionRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorSetAxisPositionRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      axis: sanitizer.sanitizeString(value.axis, 'axis'),
      position: sanitizer.sanitizeNumber(value.position, 'position'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
