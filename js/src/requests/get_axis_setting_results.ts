/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { GetAxisSettingResult } from '../ascii/get_axis_setting_result';

export interface GetAxisSettingResults {
  results: GetAxisSettingResult[];

}

export const GetAxisSettingResults = {
  fromBinary: (buffer: Uint8Array): GetAxisSettingResults => BSON.deserialize(buffer) as GetAxisSettingResults,
  toBinary: (value: GetAxisSettingResults): Uint8Array => BSON.serialize(GetAxisSettingResults.sanitize(value)),
  DEFAULT: Object.freeze({
    results: [],
  }) as Readonly<GetAxisSettingResults>,
  sanitize: (value: GetAxisSettingResults): GetAxisSettingResults => {
    if (value == null) { throw new TypeError('Expected GetAxisSettingResults object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetAxisSettingResults object but got ${typeof value}.`) }
    return {
      results: Array.from(value.results ?? [], item => GetAxisSettingResult.sanitize(item)),
    };
  },
};
