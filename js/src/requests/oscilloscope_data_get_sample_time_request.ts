/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface OscilloscopeDataGetSampleTimeRequest {
  dataId: number;

  unit: Units;

  index: number;

}

export const OscilloscopeDataGetSampleTimeRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeDataGetSampleTimeRequest => BSON.deserialize(buffer) as OscilloscopeDataGetSampleTimeRequest,
  toBinary: (value: OscilloscopeDataGetSampleTimeRequest): Uint8Array => BSON.serialize(OscilloscopeDataGetSampleTimeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    dataId: 0,
    unit: Units.NATIVE,
    index: 0,
  }) as Readonly<OscilloscopeDataGetSampleTimeRequest>,
  sanitize: (value: OscilloscopeDataGetSampleTimeRequest): OscilloscopeDataGetSampleTimeRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeDataGetSampleTimeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeDataGetSampleTimeRequest object but got ${typeof value}.`) }
    return {
      dataId: sanitizer.sanitizeInt(value.dataId, 'dataId'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      index: sanitizer.sanitizeInt(value.index, 'index'),
    };
  },
};
