/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceSetSettingRequest {
  interfaceId: number;

  device: number;

  axis: number;

  setting: string;

  value: number;

  unit: Units;

}

export const DeviceSetSettingRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetSettingRequest => BSON.deserialize(buffer) as DeviceSetSettingRequest,
  toBinary: (value: DeviceSetSettingRequest): Uint8Array => BSON.serialize(DeviceSetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    setting: '',
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetSettingRequest>,
  sanitize: (value: DeviceSetSettingRequest): DeviceSetSettingRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
