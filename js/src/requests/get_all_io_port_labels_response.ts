/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { IoPortLabel } from '../ascii/io_port_label';

export interface GetAllIoPortLabelsResponse {
  labels: IoPortLabel[];

}

export const GetAllIoPortLabelsResponse = {
  fromBinary: (buffer: Uint8Array): GetAllIoPortLabelsResponse => BSON.deserialize(buffer) as GetAllIoPortLabelsResponse,
  toBinary: (value: GetAllIoPortLabelsResponse): Uint8Array => BSON.serialize(GetAllIoPortLabelsResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    labels: [],
  }) as Readonly<GetAllIoPortLabelsResponse>,
  sanitize: (value: GetAllIoPortLabelsResponse): GetAllIoPortLabelsResponse => {
    if (value == null) { throw new TypeError('Expected GetAllIoPortLabelsResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetAllIoPortLabelsResponse object but got ${typeof value}.`) }
    return {
      labels: Array.from(value.labels ?? [], item => IoPortLabel.sanitize(item)),
    };
  },
};
