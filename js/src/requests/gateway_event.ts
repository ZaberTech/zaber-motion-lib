/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface GatewayEvent {
  event: string;

}

export const GatewayEvent = {
  fromBinary: (buffer: Uint8Array): GatewayEvent => BSON.deserialize(buffer) as GatewayEvent,
  toBinary: (value: GatewayEvent): Uint8Array => BSON.serialize(GatewayEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    event: '',
  }) as Readonly<GatewayEvent>,
  sanitize: (value: GatewayEvent): GatewayEvent => {
    if (value == null) { throw new TypeError('Expected GatewayEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GatewayEvent object but got ${typeof value}.`) }
    return {
      event: sanitizer.sanitizeString(value.event, 'event'),
    };
  },
};
