/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ConversionFactor } from '../ascii/conversion_factor';

export interface DeviceSetUnitConversionsRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

  conversions: ConversionFactor[];

}

export const DeviceSetUnitConversionsRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetUnitConversionsRequest => BSON.deserialize(buffer) as DeviceSetUnitConversionsRequest,
  toBinary: (value: DeviceSetUnitConversionsRequest): Uint8Array => BSON.serialize(DeviceSetUnitConversionsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
    conversions: [],
  }) as Readonly<DeviceSetUnitConversionsRequest>,
  sanitize: (value: DeviceSetUnitConversionsRequest): DeviceSetUnitConversionsRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetUnitConversionsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetUnitConversionsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
      conversions: Array.from(value.conversions ?? [], item => ConversionFactor.sanitize(item)),
    };
  },
};
