/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DoubleArrayResponse {
  values: number[];

}

export const DoubleArrayResponse = {
  fromBinary: (buffer: Uint8Array): DoubleArrayResponse => BSON.deserialize(buffer) as DoubleArrayResponse,
  toBinary: (value: DoubleArrayResponse): Uint8Array => BSON.serialize(DoubleArrayResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    values: [],
  }) as Readonly<DoubleArrayResponse>,
  sanitize: (value: DoubleArrayResponse): DoubleArrayResponse => {
    if (value == null) { throw new TypeError('Expected DoubleArrayResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DoubleArrayResponse object but got ${typeof value}.`) }
    return {
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
    };
  },
};
