/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface OscilloscopeDataGetRequest {
  dataId: number;

  unit: Units;

}

export const OscilloscopeDataGetRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeDataGetRequest => BSON.deserialize(buffer) as OscilloscopeDataGetRequest,
  toBinary: (value: OscilloscopeDataGetRequest): Uint8Array => BSON.serialize(OscilloscopeDataGetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    dataId: 0,
    unit: Units.NATIVE,
  }) as Readonly<OscilloscopeDataGetRequest>,
  sanitize: (value: OscilloscopeDataGetRequest): OscilloscopeDataGetRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeDataGetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeDataGetRequest object but got ${typeof value}.`) }
    return {
      dataId: sanitizer.sanitizeInt(value.dataId, 'dataId'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
