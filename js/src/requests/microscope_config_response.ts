/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { MicroscopeConfig } from '../microscopy/microscope_config';

export interface MicroscopeConfigResponse {
  config: MicroscopeConfig;

}

export const MicroscopeConfigResponse = {
  fromBinary: (buffer: Uint8Array): MicroscopeConfigResponse => BSON.deserialize(buffer) as MicroscopeConfigResponse,
  toBinary: (value: MicroscopeConfigResponse): Uint8Array => BSON.serialize(MicroscopeConfigResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    config: MicroscopeConfig.DEFAULT,
  }) as Readonly<MicroscopeConfigResponse>,
  sanitize: (value: MicroscopeConfigResponse): MicroscopeConfigResponse => {
    if (value == null) { throw new TypeError('Expected MicroscopeConfigResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeConfigResponse object but got ${typeof value}.`) }
    return {
      config: MicroscopeConfig.sanitize(value.config),
    };
  },
};
