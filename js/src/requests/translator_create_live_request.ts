/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TranslatorConfig } from '../gcode/translator_config';

export interface TranslatorCreateLiveRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  config?: (TranslatorConfig | null);

}

export const TranslatorCreateLiveRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorCreateLiveRequest => BSON.deserialize(buffer) as TranslatorCreateLiveRequest,
  toBinary: (value: TranslatorCreateLiveRequest): Uint8Array => BSON.serialize(TranslatorCreateLiveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    config: null,
  }) as Readonly<TranslatorCreateLiveRequest>,
  sanitize: (value: TranslatorCreateLiveRequest): TranslatorCreateLiveRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorCreateLiveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorCreateLiveRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      config: value.config != null ? TranslatorConfig.sanitize(value.config) : null,
    };
  },
};
