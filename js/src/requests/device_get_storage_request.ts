/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetStorageRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

  decode: boolean;

}

export const DeviceGetStorageRequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetStorageRequest => BSON.deserialize(buffer) as DeviceGetStorageRequest,
  toBinary: (value: DeviceGetStorageRequest): Uint8Array => BSON.serialize(DeviceGetStorageRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
    decode: false,
  }) as Readonly<DeviceGetStorageRequest>,
  sanitize: (value: DeviceGetStorageRequest): DeviceGetStorageRequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetStorageRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetStorageRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
      decode: sanitizer.sanitizeBool(value.decode, 'decode'),
    };
  },
};
