/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface CheckVersionRequest {
  version: string;

  host: string;

}

export const CheckVersionRequest = {
  fromBinary: (buffer: Uint8Array): CheckVersionRequest => BSON.deserialize(buffer) as CheckVersionRequest,
  toBinary: (value: CheckVersionRequest): Uint8Array => BSON.serialize(CheckVersionRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    version: '',
    host: '',
  }) as Readonly<CheckVersionRequest>,
  sanitize: (value: CheckVersionRequest): CheckVersionRequest => {
    if (value == null) { throw new TypeError('Expected CheckVersionRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CheckVersionRequest object but got ${typeof value}.`) }
    return {
      version: sanitizer.sanitizeString(value.version, 'version'),
      host: sanitizer.sanitizeString(value.host, 'host'),
    };
  },
};
