/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceOnAllRequest {
  interfaceId: number;

  waitUntilIdle: boolean;

}

export const DeviceOnAllRequest = {
  fromBinary: (buffer: Uint8Array): DeviceOnAllRequest => BSON.deserialize(buffer) as DeviceOnAllRequest,
  toBinary: (value: DeviceOnAllRequest): Uint8Array => BSON.serialize(DeviceOnAllRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    waitUntilIdle: false,
  }) as Readonly<DeviceOnAllRequest>,
  sanitize: (value: DeviceOnAllRequest): DeviceOnAllRequest => {
    if (value == null) { throw new TypeError('Expected DeviceOnAllRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceOnAllRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
