/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorCreateResponse {
  translatorId: number;

}

export const TranslatorCreateResponse = {
  fromBinary: (buffer: Uint8Array): TranslatorCreateResponse => BSON.deserialize(buffer) as TranslatorCreateResponse,
  toBinary: (value: TranslatorCreateResponse): Uint8Array => BSON.serialize(TranslatorCreateResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
  }) as Readonly<TranslatorCreateResponse>,
  sanitize: (value: TranslatorCreateResponse): TranslatorCreateResponse => {
    if (value == null) { throw new TypeError('Expected TranslatorCreateResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorCreateResponse object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
    };
  },
};
