/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';
import { ServoTuningParam } from '../ascii/servo_tuning_param';

export interface SetServoTuningRequest {
  interfaceId: number;

  device: number;

  axis: number;

  paramset: ServoTuningParamset;

  tuningParams: ServoTuningParam[];

  setUnspecifiedToDefault: boolean;

}

export const SetServoTuningRequest = {
  fromBinary: (buffer: Uint8Array): SetServoTuningRequest => BSON.deserialize(buffer) as SetServoTuningRequest,
  toBinary: (value: SetServoTuningRequest): Uint8Array => BSON.serialize(SetServoTuningRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    paramset: 0 as ServoTuningParamset,
    tuningParams: [],
    setUnspecifiedToDefault: false,
  }) as Readonly<SetServoTuningRequest>,
  sanitize: (value: SetServoTuningRequest): SetServoTuningRequest => {
    if (value == null) { throw new TypeError('Expected SetServoTuningRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetServoTuningRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      paramset: sanitizer.sanitizeEnum(value.paramset, 'ServoTuningParamset', ServoTuningParamset, 'paramset'),
      tuningParams: Array.from(value.tuningParams ?? [], item => ServoTuningParam.sanitize(item)),
      setUnspecifiedToDefault: sanitizer.sanitizeBool(value.setUnspecifiedToDefault, 'setUnspecifiedToDefault'),
    };
  },
};
