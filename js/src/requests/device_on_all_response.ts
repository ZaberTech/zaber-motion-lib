/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceOnAllResponse {
  deviceAddresses: number[];

}

export const DeviceOnAllResponse = {
  fromBinary: (buffer: Uint8Array): DeviceOnAllResponse => BSON.deserialize(buffer) as DeviceOnAllResponse,
  toBinary: (value: DeviceOnAllResponse): Uint8Array => BSON.serialize(DeviceOnAllResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceAddresses: [],
  }) as Readonly<DeviceOnAllResponse>,
  sanitize: (value: DeviceOnAllResponse): DeviceOnAllResponse => {
    if (value == null) { throw new TypeError('Expected DeviceOnAllResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceOnAllResponse object but got ${typeof value}.`) }
    return {
      deviceAddresses: Array.from(value.deviceAddresses ?? [], item => sanitizer.sanitizeInt(item, 'items of deviceAddresses')),
    };
  },
};
