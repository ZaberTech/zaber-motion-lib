/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetAnalogOutputRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  value: number;

}

export const DeviceSetAnalogOutputRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAnalogOutputRequest => BSON.deserialize(buffer) as DeviceSetAnalogOutputRequest,
  toBinary: (value: DeviceSetAnalogOutputRequest): Uint8Array => BSON.serialize(DeviceSetAnalogOutputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    value: 0,
  }) as Readonly<DeviceSetAnalogOutputRequest>,
  sanitize: (value: DeviceSetAnalogOutputRequest): DeviceSetAnalogOutputRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAnalogOutputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAnalogOutputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
