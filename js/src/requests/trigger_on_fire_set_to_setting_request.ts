/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TriggerAction } from '../ascii/trigger_action';
import { TriggerOperation } from '../ascii/trigger_operation';

export interface TriggerOnFireSetToSettingRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  action: TriggerAction;

  axis: number;

  setting: string;

  operation: TriggerOperation;

  fromAxis: number;

  fromSetting: string;

}

export const TriggerOnFireSetToSettingRequest = {
  fromBinary: (buffer: Uint8Array): TriggerOnFireSetToSettingRequest => BSON.deserialize(buffer) as TriggerOnFireSetToSettingRequest,
  toBinary: (value: TriggerOnFireSetToSettingRequest): Uint8Array => BSON.serialize(TriggerOnFireSetToSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    action: 0 as TriggerAction,
    axis: 0,
    setting: '',
    operation: 0 as TriggerOperation,
    fromAxis: 0,
    fromSetting: '',
  }) as Readonly<TriggerOnFireSetToSettingRequest>,
  sanitize: (value: TriggerOnFireSetToSettingRequest): TriggerOnFireSetToSettingRequest => {
    if (value == null) { throw new TypeError('Expected TriggerOnFireSetToSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerOnFireSetToSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      action: sanitizer.sanitizeEnum(value.action, 'TriggerAction', TriggerAction, 'action'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      operation: sanitizer.sanitizeEnum(value.operation, 'TriggerOperation', TriggerOperation, 'operation'),
      fromAxis: sanitizer.sanitizeInt(value.fromAxis, 'fromAxis'),
      fromSetting: sanitizer.sanitizeString(value.fromSetting, 'fromSetting'),
    };
  },
};
