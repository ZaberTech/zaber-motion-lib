/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface FindDeviceResponse {
  address: number;

}

export const FindDeviceResponse = {
  fromBinary: (buffer: Uint8Array): FindDeviceResponse => BSON.deserialize(buffer) as FindDeviceResponse,
  toBinary: (value: FindDeviceResponse): Uint8Array => BSON.serialize(FindDeviceResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    address: 0,
  }) as Readonly<FindDeviceResponse>,
  sanitize: (value: FindDeviceResponse): FindDeviceResponse => {
    if (value == null) { throw new TypeError('Expected FindDeviceResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected FindDeviceResponse object but got ${typeof value}.`) }
    return {
      address: sanitizer.sanitizeInt(value.address, 'address'),
    };
  },
};
