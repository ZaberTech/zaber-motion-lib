/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceSetAnalogOutputScheduleRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  value: number;

  futureValue: number;

  delay: number;

  unit: Units;

}

export const DeviceSetAnalogOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAnalogOutputScheduleRequest => BSON.deserialize(buffer) as DeviceSetAnalogOutputScheduleRequest,
  toBinary: (value: DeviceSetAnalogOutputScheduleRequest): Uint8Array => BSON.serialize(DeviceSetAnalogOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    value: 0,
    futureValue: 0,
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetAnalogOutputScheduleRequest>,
  sanitize: (value: DeviceSetAnalogOutputScheduleRequest): DeviceSetAnalogOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAnalogOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAnalogOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      futureValue: sanitizer.sanitizeNumber(value.futureValue, 'futureValue'),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
