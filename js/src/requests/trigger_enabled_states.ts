/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { TriggerEnabledState } from '../ascii/trigger_enabled_state';

export interface TriggerEnabledStates {
  states: TriggerEnabledState[];

}

export const TriggerEnabledStates = {
  fromBinary: (buffer: Uint8Array): TriggerEnabledStates => BSON.deserialize(buffer) as TriggerEnabledStates,
  toBinary: (value: TriggerEnabledStates): Uint8Array => BSON.serialize(TriggerEnabledStates.sanitize(value)),
  DEFAULT: Object.freeze({
    states: [],
  }) as Readonly<TriggerEnabledStates>,
  sanitize: (value: TriggerEnabledStates): TriggerEnabledStates => {
    if (value == null) { throw new TypeError('Expected TriggerEnabledStates object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerEnabledStates object but got ${typeof value}.`) }
    return {
      states: Array.from(value.states ?? [], item => TriggerEnabledState.sanitize(item)),
    };
  },
};
