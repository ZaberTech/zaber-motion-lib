/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';
import { Units } from '../units';

export interface StreamSetAllDigitalOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  values: DigitalOutputAction[];

  futureValues: DigitalOutputAction[];

  delay: number;

  unit: Units;

}

export const StreamSetAllDigitalOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAllDigitalOutputsScheduleRequest => BSON.deserialize(buffer) as StreamSetAllDigitalOutputsScheduleRequest,
  toBinary: (value: StreamSetAllDigitalOutputsScheduleRequest): Uint8Array => BSON.serialize(StreamSetAllDigitalOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    values: [],
    futureValues: [],
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetAllDigitalOutputsScheduleRequest>,
  sanitize: (value: StreamSetAllDigitalOutputsScheduleRequest): StreamSetAllDigitalOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAllDigitalOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAllDigitalOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of values')),
      futureValues: Array.from(value.futureValues ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of futureValues')),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
