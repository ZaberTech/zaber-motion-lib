/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetupStoreArbitraryAxesRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  streamBuffer: number;

  pvtBuffer: number;

  axesCount: number;

}

export const StreamSetupStoreArbitraryAxesRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetupStoreArbitraryAxesRequest => BSON.deserialize(buffer) as StreamSetupStoreArbitraryAxesRequest,
  toBinary: (value: StreamSetupStoreArbitraryAxesRequest): Uint8Array => BSON.serialize(StreamSetupStoreArbitraryAxesRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    streamBuffer: 0,
    pvtBuffer: 0,
    axesCount: 0,
  }) as Readonly<StreamSetupStoreArbitraryAxesRequest>,
  sanitize: (value: StreamSetupStoreArbitraryAxesRequest): StreamSetupStoreArbitraryAxesRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetupStoreArbitraryAxesRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetupStoreArbitraryAxesRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      streamBuffer: sanitizer.sanitizeInt(value.streamBuffer, 'streamBuffer'),
      pvtBuffer: sanitizer.sanitizeInt(value.pvtBuffer, 'pvtBuffer'),
      axesCount: sanitizer.sanitizeInt(value.axesCount, 'axesCount'),
    };
  },
};
