/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface LockstepGetRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  unit: Units;

}

export const LockstepGetRequest = {
  fromBinary: (buffer: Uint8Array): LockstepGetRequest => BSON.deserialize(buffer) as LockstepGetRequest,
  toBinary: (value: LockstepGetRequest): Uint8Array => BSON.serialize(LockstepGetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    unit: Units.NATIVE,
  }) as Readonly<LockstepGetRequest>,
  sanitize: (value: LockstepGetRequest): LockstepGetRequest => {
    if (value == null) { throw new TypeError('Expected LockstepGetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepGetRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
