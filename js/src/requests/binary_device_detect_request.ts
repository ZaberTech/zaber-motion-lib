/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface BinaryDeviceDetectRequest {
  interfaceId: number;

  identifyDevices: boolean;

}

export const BinaryDeviceDetectRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceDetectRequest => BSON.deserialize(buffer) as BinaryDeviceDetectRequest,
  toBinary: (value: BinaryDeviceDetectRequest): Uint8Array => BSON.serialize(BinaryDeviceDetectRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    identifyDevices: false,
  }) as Readonly<BinaryDeviceDetectRequest>,
  sanitize: (value: BinaryDeviceDetectRequest): BinaryDeviceDetectRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceDetectRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceDetectRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      identifyDevices: sanitizer.sanitizeBool(value.identifyDevices, 'identifyDevices'),
    };
  },
};
