/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceCancelOutputScheduleRequest {
  interfaceId: number;

  device: number;

  analog: boolean;

  channelNumber: number;

}

export const DeviceCancelOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceCancelOutputScheduleRequest => BSON.deserialize(buffer) as DeviceCancelOutputScheduleRequest,
  toBinary: (value: DeviceCancelOutputScheduleRequest): Uint8Array => BSON.serialize(DeviceCancelOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    analog: false,
    channelNumber: 0,
  }) as Readonly<DeviceCancelOutputScheduleRequest>,
  sanitize: (value: DeviceCancelOutputScheduleRequest): DeviceCancelOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceCancelOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceCancelOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      analog: sanitizer.sanitizeBool(value.analog, 'analog'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
    };
  },
};
