/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { IoPortType } from '../ascii/io_port_type';
import { TriggerCondition } from '../ascii/trigger_condition';

export interface TriggerFireWhenIoRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  portType: IoPortType;

  channel: number;

  triggerCondition: TriggerCondition;

  value: number;

}

export const TriggerFireWhenIoRequest = {
  fromBinary: (buffer: Uint8Array): TriggerFireWhenIoRequest => BSON.deserialize(buffer) as TriggerFireWhenIoRequest,
  toBinary: (value: TriggerFireWhenIoRequest): Uint8Array => BSON.serialize(TriggerFireWhenIoRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    portType: 0 as IoPortType,
    channel: 0,
    triggerCondition: 0 as TriggerCondition,
    value: 0,
  }) as Readonly<TriggerFireWhenIoRequest>,
  sanitize: (value: TriggerFireWhenIoRequest): TriggerFireWhenIoRequest => {
    if (value == null) { throw new TypeError('Expected TriggerFireWhenIoRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerFireWhenIoRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      portType: sanitizer.sanitizeEnum(value.portType, 'IoPortType', IoPortType, 'portType'),
      channel: sanitizer.sanitizeInt(value.channel, 'channel'),
      triggerCondition: sanitizer.sanitizeEnum(value.triggerCondition, 'TriggerCondition', TriggerCondition, 'triggerCondition'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
