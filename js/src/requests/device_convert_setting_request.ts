/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceConvertSettingRequest {
  interfaceId: number;

  device: number;

  axis: number;

  setting: string;

  unit: Units;

  value: number;

  fromNative: boolean;

}

export const DeviceConvertSettingRequest = {
  fromBinary: (buffer: Uint8Array): DeviceConvertSettingRequest => BSON.deserialize(buffer) as DeviceConvertSettingRequest,
  toBinary: (value: DeviceConvertSettingRequest): Uint8Array => BSON.serialize(DeviceConvertSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    setting: '',
    unit: Units.NATIVE,
    value: 0,
    fromNative: false,
  }) as Readonly<DeviceConvertSettingRequest>,
  sanitize: (value: DeviceConvertSettingRequest): DeviceConvertSettingRequest => {
    if (value == null) { throw new TypeError('Expected DeviceConvertSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceConvertSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      fromNative: sanitizer.sanitizeBool(value.fromNative, 'fromNative'),
    };
  },
};
