/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceSetAllAnalogOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  values: number[];

  futureValues: number[];

  delay: number;

  unit: Units;

}

export const DeviceSetAllAnalogOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAllAnalogOutputsScheduleRequest => BSON.deserialize(buffer) as DeviceSetAllAnalogOutputsScheduleRequest,
  toBinary: (value: DeviceSetAllAnalogOutputsScheduleRequest): Uint8Array => BSON.serialize(DeviceSetAllAnalogOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    values: [],
    futureValues: [],
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetAllAnalogOutputsScheduleRequest>,
  sanitize: (value: DeviceSetAllAnalogOutputsScheduleRequest): DeviceSetAllAnalogOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAllAnalogOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAllAnalogOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
      futureValues: Array.from(value.futureValues ?? [], item => sanitizer.sanitizeNumber(item, 'items of futureValues')),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
