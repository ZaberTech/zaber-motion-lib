/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';

export interface LoadParamset {
  interfaceId: number;

  device: number;

  axis: number;

  toParamset: ServoTuningParamset;

  fromParamset: ServoTuningParamset;

}

export const LoadParamset = {
  fromBinary: (buffer: Uint8Array): LoadParamset => BSON.deserialize(buffer) as LoadParamset,
  toBinary: (value: LoadParamset): Uint8Array => BSON.serialize(LoadParamset.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    toParamset: 0 as ServoTuningParamset,
    fromParamset: 0 as ServoTuningParamset,
  }) as Readonly<LoadParamset>,
  sanitize: (value: LoadParamset): LoadParamset => {
    if (value == null) { throw new TypeError('Expected LoadParamset object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LoadParamset object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      toParamset: sanitizer.sanitizeEnum(value.toParamset, 'ServoTuningParamset', ServoTuningParamset, 'toParamset'),
      fromParamset: sanitizer.sanitizeEnum(value.fromParamset, 'ServoTuningParamset', ServoTuningParamset, 'fromParamset'),
    };
  },
};
