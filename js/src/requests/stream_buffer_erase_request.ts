/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamBufferEraseRequest {
  interfaceId: number;

  device: number;

  bufferId: number;

  pvt: boolean;

}

export const StreamBufferEraseRequest = {
  fromBinary: (buffer: Uint8Array): StreamBufferEraseRequest => BSON.deserialize(buffer) as StreamBufferEraseRequest,
  toBinary: (value: StreamBufferEraseRequest): Uint8Array => BSON.serialize(StreamBufferEraseRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    bufferId: 0,
    pvt: false,
  }) as Readonly<StreamBufferEraseRequest>,
  sanitize: (value: StreamBufferEraseRequest): StreamBufferEraseRequest => {
    if (value == null) { throw new TypeError('Expected StreamBufferEraseRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamBufferEraseRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      bufferId: sanitizer.sanitizeInt(value.bufferId, 'bufferId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
    };
  },
};
