/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface UnitGetEnumRequest {
  symbol: string;

}

export const UnitGetEnumRequest = {
  fromBinary: (buffer: Uint8Array): UnitGetEnumRequest => BSON.deserialize(buffer) as UnitGetEnumRequest,
  toBinary: (value: UnitGetEnumRequest): Uint8Array => BSON.serialize(UnitGetEnumRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    symbol: '',
  }) as Readonly<UnitGetEnumRequest>,
  sanitize: (value: UnitGetEnumRequest): UnitGetEnumRequest => {
    if (value == null) { throw new TypeError('Expected UnitGetEnumRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnitGetEnumRequest object but got ${typeof value}.`) }
    return {
      symbol: sanitizer.sanitizeString(value.symbol, 'symbol'),
    };
  },
};
