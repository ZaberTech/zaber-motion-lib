/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DoubleResponse {
  value: number;

}

export const DoubleResponse = {
  fromBinary: (buffer: Uint8Array): DoubleResponse => BSON.deserialize(buffer) as DoubleResponse,
  toBinary: (value: DoubleResponse): Uint8Array => BSON.serialize(DoubleResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    value: 0,
  }) as Readonly<DoubleResponse>,
  sanitize: (value: DoubleResponse): DoubleResponse => {
    if (value == null) { throw new TypeError('Expected DoubleResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DoubleResponse object but got ${typeof value}.`) }
    return {
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
