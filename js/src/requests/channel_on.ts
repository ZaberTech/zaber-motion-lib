/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ChannelOn {
  interfaceId: number;

  device: number;

  axis: number;

  on: boolean;

}

export const ChannelOn = {
  fromBinary: (buffer: Uint8Array): ChannelOn => BSON.deserialize(buffer) as ChannelOn,
  toBinary: (value: ChannelOn): Uint8Array => BSON.serialize(ChannelOn.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    on: false,
  }) as Readonly<ChannelOn>,
  sanitize: (value: ChannelOn): ChannelOn => {
    if (value == null) { throw new TypeError('Expected ChannelOn object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ChannelOn object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      on: sanitizer.sanitizeBool(value.on, 'on'),
    };
  },
};
