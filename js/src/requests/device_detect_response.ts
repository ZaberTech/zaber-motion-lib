/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceDetectResponse {
  devices: number[];

}

export const DeviceDetectResponse = {
  fromBinary: (buffer: Uint8Array): DeviceDetectResponse => BSON.deserialize(buffer) as DeviceDetectResponse,
  toBinary: (value: DeviceDetectResponse): Uint8Array => BSON.serialize(DeviceDetectResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    devices: [],
  }) as Readonly<DeviceDetectResponse>,
  sanitize: (value: DeviceDetectResponse): DeviceDetectResponse => {
    if (value == null) { throw new TypeError('Expected DeviceDetectResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceDetectResponse object but got ${typeof value}.`) }
    return {
      devices: Array.from(value.devices ?? [], item => sanitizer.sanitizeInt(item, 'items of devices')),
    };
  },
};
