/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceRestoreRequest {
  interfaceId: number;

  device: number;

  axis: number;

  hard: boolean;

}

export const DeviceRestoreRequest = {
  fromBinary: (buffer: Uint8Array): DeviceRestoreRequest => BSON.deserialize(buffer) as DeviceRestoreRequest,
  toBinary: (value: DeviceRestoreRequest): Uint8Array => BSON.serialize(DeviceRestoreRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    hard: false,
  }) as Readonly<DeviceRestoreRequest>,
  sanitize: (value: DeviceRestoreRequest): DeviceRestoreRequest => {
    if (value == null) { throw new TypeError('Expected DeviceRestoreRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceRestoreRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      hard: sanitizer.sanitizeBool(value.hard, 'hard'),
    };
  },
};
