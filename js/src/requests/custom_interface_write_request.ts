/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface CustomInterfaceWriteRequest {
  transportId: number;

  message: string;

}

export const CustomInterfaceWriteRequest = {
  fromBinary: (buffer: Uint8Array): CustomInterfaceWriteRequest => BSON.deserialize(buffer) as CustomInterfaceWriteRequest,
  toBinary: (value: CustomInterfaceWriteRequest): Uint8Array => BSON.serialize(CustomInterfaceWriteRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    transportId: 0,
    message: '',
  }) as Readonly<CustomInterfaceWriteRequest>,
  sanitize: (value: CustomInterfaceWriteRequest): CustomInterfaceWriteRequest => {
    if (value == null) { throw new TypeError('Expected CustomInterfaceWriteRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CustomInterfaceWriteRequest object but got ${typeof value}.`) }
    return {
      transportId: sanitizer.sanitizeInt(value.transportId, 'transportId'),
      message: sanitizer.sanitizeString(value.message, 'message'),
    };
  },
};
