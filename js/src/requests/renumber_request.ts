/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface RenumberRequest {
  interfaceId: number;

  device: number;

  address: number;

}

export const RenumberRequest = {
  fromBinary: (buffer: Uint8Array): RenumberRequest => BSON.deserialize(buffer) as RenumberRequest,
  toBinary: (value: RenumberRequest): Uint8Array => BSON.serialize(RenumberRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    address: 0,
  }) as Readonly<RenumberRequest>,
  sanitize: (value: RenumberRequest): RenumberRequest => {
    if (value == null) { throw new TypeError('Expected RenumberRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected RenumberRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      address: sanitizer.sanitizeInt(value.address, 'address'),
    };
  },
};
