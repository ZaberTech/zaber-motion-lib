/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface AxesEmptyRequest {
  interfaces: number[];

  devices: number[];

  axes: number[];

}

export const AxesEmptyRequest = {
  fromBinary: (buffer: Uint8Array): AxesEmptyRequest => BSON.deserialize(buffer) as AxesEmptyRequest,
  toBinary: (value: AxesEmptyRequest): Uint8Array => BSON.serialize(AxesEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaces: [],
    devices: [],
    axes: [],
  }) as Readonly<AxesEmptyRequest>,
  sanitize: (value: AxesEmptyRequest): AxesEmptyRequest => {
    if (value == null) { throw new TypeError('Expected AxesEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxesEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaces: Array.from(value.interfaces ?? [], item => sanitizer.sanitizeInt(item, 'items of interfaces')),
      devices: Array.from(value.devices ?? [], item => sanitizer.sanitizeInt(item, 'items of devices')),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
    };
  },
};
