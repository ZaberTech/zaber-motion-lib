/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceGetSettingRequest {
  interfaceId: number;

  device: number;

  axis: number;

  setting: string;

  unit: Units;

}

export const DeviceGetSettingRequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetSettingRequest => BSON.deserialize(buffer) as DeviceGetSettingRequest,
  toBinary: (value: DeviceGetSettingRequest): Uint8Array => BSON.serialize(DeviceGetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    setting: '',
    unit: Units.NATIVE,
  }) as Readonly<DeviceGetSettingRequest>,
  sanitize: (value: DeviceGetSettingRequest): DeviceGetSettingRequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
