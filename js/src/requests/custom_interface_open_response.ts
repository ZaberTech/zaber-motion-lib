/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface CustomInterfaceOpenResponse {
  transportId: number;

}

export const CustomInterfaceOpenResponse = {
  fromBinary: (buffer: Uint8Array): CustomInterfaceOpenResponse => BSON.deserialize(buffer) as CustomInterfaceOpenResponse,
  toBinary: (value: CustomInterfaceOpenResponse): Uint8Array => BSON.serialize(CustomInterfaceOpenResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    transportId: 0,
  }) as Readonly<CustomInterfaceOpenResponse>,
  sanitize: (value: CustomInterfaceOpenResponse): CustomInterfaceOpenResponse => {
    if (value == null) { throw new TypeError('Expected CustomInterfaceOpenResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CustomInterfaceOpenResponse object but got ${typeof value}.`) }
    return {
      transportId: sanitizer.sanitizeInt(value.transportId, 'transportId'),
    };
  },
};
