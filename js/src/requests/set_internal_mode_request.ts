/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface SetInternalModeRequest {
  mode: boolean;

}

export const SetInternalModeRequest = {
  fromBinary: (buffer: Uint8Array): SetInternalModeRequest => BSON.deserialize(buffer) as SetInternalModeRequest,
  toBinary: (value: SetInternalModeRequest): Uint8Array => BSON.serialize(SetInternalModeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    mode: false,
  }) as Readonly<SetInternalModeRequest>,
  sanitize: (value: SetInternalModeRequest): SetInternalModeRequest => {
    if (value == null) { throw new TypeError('Expected SetInternalModeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetInternalModeRequest object but got ${typeof value}.`) }
    return {
      mode: sanitizer.sanitizeBool(value.mode, 'mode'),
    };
  },
};
