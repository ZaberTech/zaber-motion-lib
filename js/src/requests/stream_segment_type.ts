/* This file is generated. Do not modify by hand. */
export enum StreamSegmentType {
  /** Abs. */
  ABS = 0,
  /** Rel. */
  REL = 1,
}
