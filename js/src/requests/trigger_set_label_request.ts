/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TriggerSetLabelRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  label?: (string | null);

}

export const TriggerSetLabelRequest = {
  fromBinary: (buffer: Uint8Array): TriggerSetLabelRequest => BSON.deserialize(buffer) as TriggerSetLabelRequest,
  toBinary: (value: TriggerSetLabelRequest): Uint8Array => BSON.serialize(TriggerSetLabelRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    label: null,
  }) as Readonly<TriggerSetLabelRequest>,
  sanitize: (value: TriggerSetLabelRequest): TriggerSetLabelRequest => {
    if (value == null) { throw new TypeError('Expected TriggerSetLabelRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerSetLabelRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      label: value.label != null ? sanitizer.sanitizeString(value.label, 'label') : null,
    };
  },
};
