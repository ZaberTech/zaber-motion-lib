/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceStorageListKeysRequest {
  interfaceId: number;

  device: number;

  axis: number;

  prefix?: (string | null);

}

export const DeviceStorageListKeysRequest = {
  fromBinary: (buffer: Uint8Array): DeviceStorageListKeysRequest => BSON.deserialize(buffer) as DeviceStorageListKeysRequest,
  toBinary: (value: DeviceStorageListKeysRequest): Uint8Array => BSON.serialize(DeviceStorageListKeysRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    prefix: null,
  }) as Readonly<DeviceStorageListKeysRequest>,
  sanitize: (value: DeviceStorageListKeysRequest): DeviceStorageListKeysRequest => {
    if (value == null) { throw new TypeError('Expected DeviceStorageListKeysRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceStorageListKeysRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      prefix: value.prefix != null ? sanitizer.sanitizeString(value.prefix, 'prefix') : null,
    };
  },
};
