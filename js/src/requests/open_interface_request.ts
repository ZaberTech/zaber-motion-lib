/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { InterfaceType } from '../requests/interface_type';

export interface OpenInterfaceRequest {
  interfaceType: InterfaceType;

  portName: string;

  baudRate: number;

  hostName: string;

  port: number;

  transport: number;

  rejectRoutedConnection: boolean;

  cloudId: string;

  connectionName?: (string | null);

  realm?: (string | null);

  token: string;

  api: string;

}

export const OpenInterfaceRequest = {
  fromBinary: (buffer: Uint8Array): OpenInterfaceRequest => BSON.deserialize(buffer) as OpenInterfaceRequest,
  toBinary: (value: OpenInterfaceRequest): Uint8Array => BSON.serialize(OpenInterfaceRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceType: 0 as InterfaceType,
    portName: '',
    baudRate: 0,
    hostName: '',
    port: 0,
    transport: 0,
    rejectRoutedConnection: false,
    cloudId: '',
    connectionName: null,
    realm: null,
    token: '',
    api: '',
  }) as Readonly<OpenInterfaceRequest>,
  sanitize: (value: OpenInterfaceRequest): OpenInterfaceRequest => {
    if (value == null) { throw new TypeError('Expected OpenInterfaceRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OpenInterfaceRequest object but got ${typeof value}.`) }
    return {
      interfaceType: sanitizer.sanitizeEnum(value.interfaceType, 'InterfaceType', InterfaceType, 'interfaceType'),
      portName: sanitizer.sanitizeString(value.portName, 'portName'),
      baudRate: sanitizer.sanitizeInt(value.baudRate, 'baudRate'),
      hostName: sanitizer.sanitizeString(value.hostName, 'hostName'),
      port: sanitizer.sanitizeInt(value.port, 'port'),
      transport: sanitizer.sanitizeInt(value.transport, 'transport'),
      rejectRoutedConnection: sanitizer.sanitizeBool(value.rejectRoutedConnection, 'rejectRoutedConnection'),
      cloudId: sanitizer.sanitizeString(value.cloudId, 'cloudId'),
      connectionName: value.connectionName != null ? sanitizer.sanitizeString(value.connectionName, 'connectionName') : null,
      realm: value.realm != null ? sanitizer.sanitizeString(value.realm, 'realm') : null,
      token: sanitizer.sanitizeString(value.token, 'token'),
      api: sanitizer.sanitizeString(value.api, 'api'),
    };
  },
};
