/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeReadResponse {
  dataIds: number[];

}

export const OscilloscopeReadResponse = {
  fromBinary: (buffer: Uint8Array): OscilloscopeReadResponse => BSON.deserialize(buffer) as OscilloscopeReadResponse,
  toBinary: (value: OscilloscopeReadResponse): Uint8Array => BSON.serialize(OscilloscopeReadResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    dataIds: [],
  }) as Readonly<OscilloscopeReadResponse>,
  sanitize: (value: OscilloscopeReadResponse): OscilloscopeReadResponse => {
    if (value == null) { throw new TypeError('Expected OscilloscopeReadResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeReadResponse object but got ${typeof value}.`) }
    return {
      dataIds: Array.from(value.dataIds ?? [], item => sanitizer.sanitizeInt(item, 'items of dataIds')),
    };
  },
};
