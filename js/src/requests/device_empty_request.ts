/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceEmptyRequest {
  interfaceId: number;

  device: number;

}

export const DeviceEmptyRequest = {
  fromBinary: (buffer: Uint8Array): DeviceEmptyRequest => BSON.deserialize(buffer) as DeviceEmptyRequest,
  toBinary: (value: DeviceEmptyRequest): Uint8Array => BSON.serialize(DeviceEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
  }) as Readonly<DeviceEmptyRequest>,
  sanitize: (value: DeviceEmptyRequest): DeviceEmptyRequest => {
    if (value == null) { throw new TypeError('Expected DeviceEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
    };
  },
};
