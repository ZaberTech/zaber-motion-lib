/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface BinaryDeviceHomeRequest {
  interfaceId: number;

  device: number;

  timeout: number;

  unit: Units;

}

export const BinaryDeviceHomeRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceHomeRequest => BSON.deserialize(buffer) as BinaryDeviceHomeRequest,
  toBinary: (value: BinaryDeviceHomeRequest): Uint8Array => BSON.serialize(BinaryDeviceHomeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    timeout: 0,
    unit: Units.NATIVE,
  }) as Readonly<BinaryDeviceHomeRequest>,
  sanitize: (value: BinaryDeviceHomeRequest): BinaryDeviceHomeRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceHomeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceHomeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
