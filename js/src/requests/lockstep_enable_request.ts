/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepEnableRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  axes: number[];

}

export const LockstepEnableRequest = {
  fromBinary: (buffer: Uint8Array): LockstepEnableRequest => BSON.deserialize(buffer) as LockstepEnableRequest,
  toBinary: (value: LockstepEnableRequest): Uint8Array => BSON.serialize(LockstepEnableRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    axes: [],
  }) as Readonly<LockstepEnableRequest>,
  sanitize: (value: LockstepEnableRequest): LockstepEnableRequest => {
    if (value == null) { throw new TypeError('Expected LockstepEnableRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepEnableRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
    };
  },
};
