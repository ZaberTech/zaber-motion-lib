/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisMoveType } from '../requests/axis_move_type';
import { Units } from '../units';

export interface DeviceMoveRequest {
  interfaceId: number;

  device: number;

  axis: number;

  waitUntilIdle: boolean;

  type: AxisMoveType;

  arg: number;

  argInt: number;

  unit: Units;

  velocity: number;

  velocityUnit: Units;

  acceleration: number;

  accelerationUnit: Units;

}

export const DeviceMoveRequest = {
  fromBinary: (buffer: Uint8Array): DeviceMoveRequest => BSON.deserialize(buffer) as DeviceMoveRequest,
  toBinary: (value: DeviceMoveRequest): Uint8Array => BSON.serialize(DeviceMoveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    waitUntilIdle: false,
    type: 0 as AxisMoveType,
    arg: 0,
    argInt: 0,
    unit: Units.NATIVE,
    velocity: 0,
    velocityUnit: Units.NATIVE,
    acceleration: 0,
    accelerationUnit: Units.NATIVE,
  }) as Readonly<DeviceMoveRequest>,
  sanitize: (value: DeviceMoveRequest): DeviceMoveRequest => {
    if (value == null) { throw new TypeError('Expected DeviceMoveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceMoveRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
      type: sanitizer.sanitizeEnum(value.type, 'AxisMoveType', AxisMoveType, 'type'),
      arg: sanitizer.sanitizeNumber(value.arg, 'arg'),
      argInt: sanitizer.sanitizeInt(value.argInt, 'argInt'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      velocity: sanitizer.sanitizeNumber(value.velocity, 'velocity'),
      velocityUnit: sanitizer.sanitizeUnits(value.velocityUnit, 'velocityUnit'),
      acceleration: sanitizer.sanitizeNumber(value.acceleration, 'acceleration'),
      accelerationUnit: sanitizer.sanitizeUnits(value.accelerationUnit, 'accelerationUnit'),
    };
  },
};
