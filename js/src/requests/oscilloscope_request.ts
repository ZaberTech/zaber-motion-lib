/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeRequest {
  interfaceId: number;

  device: number;

}

export const OscilloscopeRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeRequest => BSON.deserialize(buffer) as OscilloscopeRequest,
  toBinary: (value: OscilloscopeRequest): Uint8Array => BSON.serialize(OscilloscopeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
  }) as Readonly<OscilloscopeRequest>,
  sanitize: (value: OscilloscopeRequest): OscilloscopeRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
    };
  },
};
