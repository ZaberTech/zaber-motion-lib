/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Measurement } from '../measurement';

export interface AxesMoveRequest {
  interfaces: number[];

  devices: number[];

  axes: number[];

  position: Measurement[];

}

export const AxesMoveRequest = {
  fromBinary: (buffer: Uint8Array): AxesMoveRequest => BSON.deserialize(buffer) as AxesMoveRequest,
  toBinary: (value: AxesMoveRequest): Uint8Array => BSON.serialize(AxesMoveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaces: [],
    devices: [],
    axes: [],
    position: [],
  }) as Readonly<AxesMoveRequest>,
  sanitize: (value: AxesMoveRequest): AxesMoveRequest => {
    if (value == null) { throw new TypeError('Expected AxesMoveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxesMoveRequest object but got ${typeof value}.`) }
    return {
      interfaces: Array.from(value.interfaces ?? [], item => sanitizer.sanitizeInt(item, 'items of interfaces')),
      devices: Array.from(value.devices ?? [], item => sanitizer.sanitizeInt(item, 'items of devices')),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
      position: Array.from(value.position ?? [], item => Measurement.sanitize(item)),
    };
  },
};
