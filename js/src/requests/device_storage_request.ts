/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceStorageRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

}

export const DeviceStorageRequest = {
  fromBinary: (buffer: Uint8Array): DeviceStorageRequest => BSON.deserialize(buffer) as DeviceStorageRequest,
  toBinary: (value: DeviceStorageRequest): Uint8Array => BSON.serialize(DeviceStorageRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
  }) as Readonly<DeviceStorageRequest>,
  sanitize: (value: DeviceStorageRequest): DeviceStorageRequest => {
    if (value == null) { throw new TypeError('Expected DeviceStorageRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceStorageRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
    };
  },
};
