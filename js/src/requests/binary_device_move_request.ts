/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisMoveType } from '../requests/axis_move_type';
import { Units } from '../units';

export interface BinaryDeviceMoveRequest {
  interfaceId: number;

  device: number;

  timeout: number;

  type: AxisMoveType;

  arg: number;

  unit: Units;

}

export const BinaryDeviceMoveRequest = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceMoveRequest => BSON.deserialize(buffer) as BinaryDeviceMoveRequest,
  toBinary: (value: BinaryDeviceMoveRequest): Uint8Array => BSON.serialize(BinaryDeviceMoveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    timeout: 0,
    type: 0 as AxisMoveType,
    arg: 0,
    unit: Units.NATIVE,
  }) as Readonly<BinaryDeviceMoveRequest>,
  sanitize: (value: BinaryDeviceMoveRequest): BinaryDeviceMoveRequest => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceMoveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceMoveRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
      type: sanitizer.sanitizeEnum(value.type, 'AxisMoveType', AxisMoveType, 'type'),
      arg: sanitizer.sanitizeNumber(value.arg, 'arg'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
