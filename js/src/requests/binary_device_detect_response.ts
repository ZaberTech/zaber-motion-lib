/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface BinaryDeviceDetectResponse {
  devices: number[];

}

export const BinaryDeviceDetectResponse = {
  fromBinary: (buffer: Uint8Array): BinaryDeviceDetectResponse => BSON.deserialize(buffer) as BinaryDeviceDetectResponse,
  toBinary: (value: BinaryDeviceDetectResponse): Uint8Array => BSON.serialize(BinaryDeviceDetectResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    devices: [],
  }) as Readonly<BinaryDeviceDetectResponse>,
  sanitize: (value: BinaryDeviceDetectResponse): BinaryDeviceDetectResponse => {
    if (value == null) { throw new TypeError('Expected BinaryDeviceDetectResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryDeviceDetectResponse object but got ${typeof value}.`) }
    return {
      devices: Array.from(value.devices ?? [], item => sanitizer.sanitizeInt(item, 'items of devices')),
    };
  },
};
