/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Measurement } from '../measurement';

export interface ObjectiveChangerChangeRequest {
  interfaceId: number;

  turretAddress: number;

  focusAddress: number;

  focusAxis: number;

  objective: number;

  focusOffset?: (Measurement | null);

}

export const ObjectiveChangerChangeRequest = {
  fromBinary: (buffer: Uint8Array): ObjectiveChangerChangeRequest => BSON.deserialize(buffer) as ObjectiveChangerChangeRequest,
  toBinary: (value: ObjectiveChangerChangeRequest): Uint8Array => BSON.serialize(ObjectiveChangerChangeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    turretAddress: 0,
    focusAddress: 0,
    focusAxis: 0,
    objective: 0,
    focusOffset: null,
  }) as Readonly<ObjectiveChangerChangeRequest>,
  sanitize: (value: ObjectiveChangerChangeRequest): ObjectiveChangerChangeRequest => {
    if (value == null) { throw new TypeError('Expected ObjectiveChangerChangeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ObjectiveChangerChangeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      objective: sanitizer.sanitizeInt(value.objective, 'objective'),
      focusOffset: value.focusOffset != null ? Measurement.sanitize(value.focusOffset) : null,
    };
  },
};
