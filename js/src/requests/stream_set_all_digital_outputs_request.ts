/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';

export interface StreamSetAllDigitalOutputsRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  values: DigitalOutputAction[];

}

export const StreamSetAllDigitalOutputsRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAllDigitalOutputsRequest => BSON.deserialize(buffer) as StreamSetAllDigitalOutputsRequest,
  toBinary: (value: StreamSetAllDigitalOutputsRequest): Uint8Array => BSON.serialize(StreamSetAllDigitalOutputsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    values: [],
  }) as Readonly<StreamSetAllDigitalOutputsRequest>,
  sanitize: (value: StreamSetAllDigitalOutputsRequest): StreamSetAllDigitalOutputsRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAllDigitalOutputsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAllDigitalOutputsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of values')),
    };
  },
};
