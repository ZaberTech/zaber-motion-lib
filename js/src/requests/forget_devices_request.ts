/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ForgetDevicesRequest {
  interfaceId: number;

  exceptDevices: number[];

}

export const ForgetDevicesRequest = {
  fromBinary: (buffer: Uint8Array): ForgetDevicesRequest => BSON.deserialize(buffer) as ForgetDevicesRequest,
  toBinary: (value: ForgetDevicesRequest): Uint8Array => BSON.serialize(ForgetDevicesRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    exceptDevices: [],
  }) as Readonly<ForgetDevicesRequest>,
  sanitize: (value: ForgetDevicesRequest): ForgetDevicesRequest => {
    if (value == null) { throw new TypeError('Expected ForgetDevicesRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ForgetDevicesRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      exceptDevices: Array.from(value.exceptDevices ?? [], item => sanitizer.sanitizeInt(item, 'items of exceptDevices')),
    };
  },
};
