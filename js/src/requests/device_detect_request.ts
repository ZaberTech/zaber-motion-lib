/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DeviceType } from '../requests/device_type';

export interface DeviceDetectRequest {
  interfaceId: number;

  identifyDevices: boolean;

  type: DeviceType;

}

export const DeviceDetectRequest = {
  fromBinary: (buffer: Uint8Array): DeviceDetectRequest => BSON.deserialize(buffer) as DeviceDetectRequest,
  toBinary: (value: DeviceDetectRequest): Uint8Array => BSON.serialize(DeviceDetectRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    identifyDevices: false,
    type: 0 as DeviceType,
  }) as Readonly<DeviceDetectRequest>,
  sanitize: (value: DeviceDetectRequest): DeviceDetectRequest => {
    if (value == null) { throw new TypeError('Expected DeviceDetectRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceDetectRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      identifyDevices: sanitizer.sanitizeBool(value.identifyDevices, 'identifyDevices'),
      type: sanitizer.sanitizeEnum(value.type, 'DeviceType', DeviceType, 'type'),
    };
  },
};
