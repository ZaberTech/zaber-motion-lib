/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepDisableRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  waitUntilIdle: boolean;

}

export const LockstepDisableRequest = {
  fromBinary: (buffer: Uint8Array): LockstepDisableRequest => BSON.deserialize(buffer) as LockstepDisableRequest,
  toBinary: (value: LockstepDisableRequest): Uint8Array => BSON.serialize(LockstepDisableRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    waitUntilIdle: false,
  }) as Readonly<LockstepDisableRequest>,
  sanitize: (value: LockstepDisableRequest): LockstepDisableRequest => {
    if (value == null) { throw new TypeError('Expected LockstepDisableRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepDisableRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
