/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamWaitAnalogInputRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  condition: string;

  value: number;

}

export const StreamWaitAnalogInputRequest = {
  fromBinary: (buffer: Uint8Array): StreamWaitAnalogInputRequest => BSON.deserialize(buffer) as StreamWaitAnalogInputRequest,
  toBinary: (value: StreamWaitAnalogInputRequest): Uint8Array => BSON.serialize(StreamWaitAnalogInputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    condition: '',
    value: 0,
  }) as Readonly<StreamWaitAnalogInputRequest>,
  sanitize: (value: StreamWaitAnalogInputRequest): StreamWaitAnalogInputRequest => {
    if (value == null) { throw new TypeError('Expected StreamWaitAnalogInputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamWaitAnalogInputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      condition: sanitizer.sanitizeString(value.condition, 'condition'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
