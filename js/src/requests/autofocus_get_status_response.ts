/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { AutofocusStatus } from '../microscopy/autofocus_status';

export interface AutofocusGetStatusResponse {
  status: AutofocusStatus;

}

export const AutofocusGetStatusResponse = {
  fromBinary: (buffer: Uint8Array): AutofocusGetStatusResponse => BSON.deserialize(buffer) as AutofocusGetStatusResponse,
  toBinary: (value: AutofocusGetStatusResponse): Uint8Array => BSON.serialize(AutofocusGetStatusResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    status: AutofocusStatus.DEFAULT,
  }) as Readonly<AutofocusGetStatusResponse>,
  sanitize: (value: AutofocusGetStatusResponse): AutofocusGetStatusResponse => {
    if (value == null) { throw new TypeError('Expected AutofocusGetStatusResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusGetStatusResponse object but got ${typeof value}.`) }
    return {
      status: AutofocusStatus.sanitize(value.status),
    };
  },
};
