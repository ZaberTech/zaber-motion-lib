/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetupLiveRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  axes: number[];

}

export const StreamSetupLiveRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetupLiveRequest => BSON.deserialize(buffer) as StreamSetupLiveRequest,
  toBinary: (value: StreamSetupLiveRequest): Uint8Array => BSON.serialize(StreamSetupLiveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    axes: [],
  }) as Readonly<StreamSetupLiveRequest>,
  sanitize: (value: StreamSetupLiveRequest): StreamSetupLiveRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetupLiveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetupLiveRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
    };
  },
};
