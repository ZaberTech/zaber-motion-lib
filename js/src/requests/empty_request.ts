/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
export interface EmptyRequest {
}

export const EmptyRequest = {
  fromBinary: (buffer: Uint8Array): EmptyRequest => BSON.deserialize(buffer) as EmptyRequest,
  toBinary: (value: EmptyRequest): Uint8Array => BSON.serialize(EmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
  }) as Readonly<EmptyRequest>,
  sanitize: (value: EmptyRequest): EmptyRequest => {
    if (value == null) { throw new TypeError('Expected EmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected EmptyRequest object but got ${typeof value}.`) }
    return {
    };
  },
};
