/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorFlushLiveRequest {
  translatorId: number;

  waitUntilIdle: boolean;

}

export const TranslatorFlushLiveRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorFlushLiveRequest => BSON.deserialize(buffer) as TranslatorFlushLiveRequest,
  toBinary: (value: TranslatorFlushLiveRequest): Uint8Array => BSON.serialize(TranslatorFlushLiveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    waitUntilIdle: false,
  }) as Readonly<TranslatorFlushLiveRequest>,
  sanitize: (value: TranslatorFlushLiveRequest): TranslatorFlushLiveRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorFlushLiveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorFlushLiveRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
