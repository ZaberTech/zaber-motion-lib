/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamAxisDefinition } from '../ascii/stream_axis_definition';
import { PvtAxisDefinition } from '../ascii/pvt_axis_definition';

export interface StreamSetupStoreCompositeRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  streamBuffer: number;

  pvtBuffer: number;

  axes: StreamAxisDefinition[];

  pvtAxes: PvtAxisDefinition[];

}

export const StreamSetupStoreCompositeRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetupStoreCompositeRequest => BSON.deserialize(buffer) as StreamSetupStoreCompositeRequest,
  toBinary: (value: StreamSetupStoreCompositeRequest): Uint8Array => BSON.serialize(StreamSetupStoreCompositeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    streamBuffer: 0,
    pvtBuffer: 0,
    axes: [],
    pvtAxes: [],
  }) as Readonly<StreamSetupStoreCompositeRequest>,
  sanitize: (value: StreamSetupStoreCompositeRequest): StreamSetupStoreCompositeRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetupStoreCompositeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetupStoreCompositeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      streamBuffer: sanitizer.sanitizeInt(value.streamBuffer, 'streamBuffer'),
      pvtBuffer: sanitizer.sanitizeInt(value.pvtBuffer, 'pvtBuffer'),
      axes: Array.from(value.axes ?? [], item => StreamAxisDefinition.sanitize(item)),
      pvtAxes: Array.from(value.pvtAxes ?? [], item => PvtAxisDefinition.sanitize(item)),
    };
  },
};
