/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetAllAnalogOutputsRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  values: number[];

}

export const StreamSetAllAnalogOutputsRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAllAnalogOutputsRequest => BSON.deserialize(buffer) as StreamSetAllAnalogOutputsRequest,
  toBinary: (value: StreamSetAllAnalogOutputsRequest): Uint8Array => BSON.serialize(StreamSetAllAnalogOutputsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    values: [],
  }) as Readonly<StreamSetAllAnalogOutputsRequest>,
  sanitize: (value: StreamSetAllAnalogOutputsRequest): StreamSetAllAnalogOutputsRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAllAnalogOutputsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAllAnalogOutputsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
    };
  },
};
