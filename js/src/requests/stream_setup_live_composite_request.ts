/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamAxisDefinition } from '../ascii/stream_axis_definition';
import { PvtAxisDefinition } from '../ascii/pvt_axis_definition';

export interface StreamSetupLiveCompositeRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  axes: StreamAxisDefinition[];

  pvtAxes: PvtAxisDefinition[];

}

export const StreamSetupLiveCompositeRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetupLiveCompositeRequest => BSON.deserialize(buffer) as StreamSetupLiveCompositeRequest,
  toBinary: (value: StreamSetupLiveCompositeRequest): Uint8Array => BSON.serialize(StreamSetupLiveCompositeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    axes: [],
    pvtAxes: [],
  }) as Readonly<StreamSetupLiveCompositeRequest>,
  sanitize: (value: StreamSetupLiveCompositeRequest): StreamSetupLiveCompositeRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetupLiveCompositeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetupLiveCompositeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      axes: Array.from(value.axes ?? [], item => StreamAxisDefinition.sanitize(item)),
      pvtAxes: Array.from(value.pvtAxes ?? [], item => PvtAxisDefinition.sanitize(item)),
    };
  },
};
