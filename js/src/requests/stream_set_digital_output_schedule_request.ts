/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';
import { Units } from '../units';

export interface StreamSetDigitalOutputScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  value: DigitalOutputAction;

  futureValue: DigitalOutputAction;

  delay: number;

  unit: Units;

}

export const StreamSetDigitalOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetDigitalOutputScheduleRequest => BSON.deserialize(buffer) as StreamSetDigitalOutputScheduleRequest,
  toBinary: (value: StreamSetDigitalOutputScheduleRequest): Uint8Array => BSON.serialize(StreamSetDigitalOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    value: 0 as DigitalOutputAction,
    futureValue: 0 as DigitalOutputAction,
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetDigitalOutputScheduleRequest>,
  sanitize: (value: StreamSetDigitalOutputScheduleRequest): StreamSetDigitalOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetDigitalOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetDigitalOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeEnum(value.value, 'DigitalOutputAction', DigitalOutputAction, 'value'),
      futureValue: sanitizer.sanitizeEnum(value.futureValue, 'DigitalOutputAction', DigitalOutputAction, 'futureValue'),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
