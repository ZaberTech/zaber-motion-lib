/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { IoPortType } from '../ascii/io_port_type';

export interface GetIoPortLabelRequest {
  interfaceId: number;

  device: number;

  portType: IoPortType;

  channelNumber: number;

}

export const GetIoPortLabelRequest = {
  fromBinary: (buffer: Uint8Array): GetIoPortLabelRequest => BSON.deserialize(buffer) as GetIoPortLabelRequest,
  toBinary: (value: GetIoPortLabelRequest): Uint8Array => BSON.serialize(GetIoPortLabelRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    portType: 0 as IoPortType,
    channelNumber: 0,
  }) as Readonly<GetIoPortLabelRequest>,
  sanitize: (value: GetIoPortLabelRequest): GetIoPortLabelRequest => {
    if (value == null) { throw new TypeError('Expected GetIoPortLabelRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetIoPortLabelRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      portType: sanitizer.sanitizeEnum(value.portType, 'IoPortType', IoPortType, 'portType'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
    };
  },
};
