/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetStorageRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

  value: string;

  encode: boolean;

}

export const DeviceSetStorageRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetStorageRequest => BSON.deserialize(buffer) as DeviceSetStorageRequest,
  toBinary: (value: DeviceSetStorageRequest): Uint8Array => BSON.serialize(DeviceSetStorageRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
    value: '',
    encode: false,
  }) as Readonly<DeviceSetStorageRequest>,
  sanitize: (value: DeviceSetStorageRequest): DeviceSetStorageRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetStorageRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetStorageRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
      value: sanitizer.sanitizeString(value.value, 'value'),
      encode: sanitizer.sanitizeBool(value.encode, 'encode'),
    };
  },
};
