/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamSetAllAnalogOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  values: number[];

  futureValues: number[];

  delay: number;

  unit: Units;

}

export const StreamSetAllAnalogOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAllAnalogOutputsScheduleRequest => BSON.deserialize(buffer) as StreamSetAllAnalogOutputsScheduleRequest,
  toBinary: (value: StreamSetAllAnalogOutputsScheduleRequest): Uint8Array => BSON.serialize(StreamSetAllAnalogOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    values: [],
    futureValues: [],
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetAllAnalogOutputsScheduleRequest>,
  sanitize: (value: StreamSetAllAnalogOutputsScheduleRequest): StreamSetAllAnalogOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAllAnalogOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAllAnalogOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
      futureValues: Array.from(value.futureValues ?? [], item => sanitizer.sanitizeNumber(item, 'items of futureValues')),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
