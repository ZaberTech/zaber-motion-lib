/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorEmptyRequest {
  translatorId: number;

}

export const TranslatorEmptyRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorEmptyRequest => BSON.deserialize(buffer) as TranslatorEmptyRequest,
  toBinary: (value: TranslatorEmptyRequest): Uint8Array => BSON.serialize(TranslatorEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
  }) as Readonly<TranslatorEmptyRequest>,
  sanitize: (value: TranslatorEmptyRequest): TranslatorEmptyRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorEmptyRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
    };
  },
};
