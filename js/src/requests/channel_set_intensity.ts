/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ChannelSetIntensity {
  interfaceId: number;

  device: number;

  axis: number;

  intensity: number;

}

export const ChannelSetIntensity = {
  fromBinary: (buffer: Uint8Array): ChannelSetIntensity => BSON.deserialize(buffer) as ChannelSetIntensity,
  toBinary: (value: ChannelSetIntensity): Uint8Array => BSON.serialize(ChannelSetIntensity.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    intensity: 0,
  }) as Readonly<ChannelSetIntensity>,
  sanitize: (value: ChannelSetIntensity): ChannelSetIntensity => {
    if (value == null) { throw new TypeError('Expected ChannelSetIntensity object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ChannelSetIntensity object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      intensity: sanitizer.sanitizeNumber(value.intensity, 'intensity'),
    };
  },
};
