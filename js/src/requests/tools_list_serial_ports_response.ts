/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ToolsListSerialPortsResponse {
  ports: string[];

}

export const ToolsListSerialPortsResponse = {
  fromBinary: (buffer: Uint8Array): ToolsListSerialPortsResponse => BSON.deserialize(buffer) as ToolsListSerialPortsResponse,
  toBinary: (value: ToolsListSerialPortsResponse): Uint8Array => BSON.serialize(ToolsListSerialPortsResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    ports: [],
  }) as Readonly<ToolsListSerialPortsResponse>,
  sanitize: (value: ToolsListSerialPortsResponse): ToolsListSerialPortsResponse => {
    if (value == null) { throw new TypeError('Expected ToolsListSerialPortsResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ToolsListSerialPortsResponse object but got ${typeof value}.`) }
    return {
      ports: Array.from(value.ports ?? [], item => sanitizer.sanitizeString(item, 'items of ports')),
    };
  },
};
