/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TriggerEmptyRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

}

export const TriggerEmptyRequest = {
  fromBinary: (buffer: Uint8Array): TriggerEmptyRequest => BSON.deserialize(buffer) as TriggerEmptyRequest,
  toBinary: (value: TriggerEmptyRequest): Uint8Array => BSON.serialize(TriggerEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
  }) as Readonly<TriggerEmptyRequest>,
  sanitize: (value: TriggerEmptyRequest): TriggerEmptyRequest => {
    if (value == null) { throw new TypeError('Expected TriggerEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
    };
  },
};
