/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorTranslateRequest {
  translatorId: number;

  block: string;

}

export const TranslatorTranslateRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorTranslateRequest => BSON.deserialize(buffer) as TranslatorTranslateRequest,
  toBinary: (value: TranslatorTranslateRequest): Uint8Array => BSON.serialize(TranslatorTranslateRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    block: '',
  }) as Readonly<TranslatorTranslateRequest>,
  sanitize: (value: TranslatorTranslateRequest): TranslatorTranslateRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorTranslateRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorTranslateRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      block: sanitizer.sanitizeString(value.block, 'block'),
    };
  },
};
