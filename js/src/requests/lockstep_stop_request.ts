/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepStopRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  waitUntilIdle: boolean;

}

export const LockstepStopRequest = {
  fromBinary: (buffer: Uint8Array): LockstepStopRequest => BSON.deserialize(buffer) as LockstepStopRequest,
  toBinary: (value: LockstepStopRequest): Uint8Array => BSON.serialize(LockstepStopRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    waitUntilIdle: false,
  }) as Readonly<LockstepStopRequest>,
  sanitize: (value: LockstepStopRequest): LockstepStopRequest => {
    if (value == null) { throw new TypeError('Expected LockstepStopRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepStopRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
