/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface IntArrayResponse {
  values: number[];

}

export const IntArrayResponse = {
  fromBinary: (buffer: Uint8Array): IntArrayResponse => BSON.deserialize(buffer) as IntArrayResponse,
  toBinary: (value: IntArrayResponse): Uint8Array => BSON.serialize(IntArrayResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    values: [],
  }) as Readonly<IntArrayResponse>,
  sanitize: (value: IntArrayResponse): IntArrayResponse => {
    if (value == null) { throw new TypeError('Expected IntArrayResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected IntArrayResponse object but got ${typeof value}.`) }
    return {
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeInt(item, 'items of values')),
    };
  },
};
