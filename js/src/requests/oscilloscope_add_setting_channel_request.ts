/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface OscilloscopeAddSettingChannelRequest {
  interfaceId: number;

  device: number;

  axis: number;

  setting: string;

}

export const OscilloscopeAddSettingChannelRequest = {
  fromBinary: (buffer: Uint8Array): OscilloscopeAddSettingChannelRequest => BSON.deserialize(buffer) as OscilloscopeAddSettingChannelRequest,
  toBinary: (value: OscilloscopeAddSettingChannelRequest): Uint8Array => BSON.serialize(OscilloscopeAddSettingChannelRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    setting: '',
  }) as Readonly<OscilloscopeAddSettingChannelRequest>,
  sanitize: (value: OscilloscopeAddSettingChannelRequest): OscilloscopeAddSettingChannelRequest => {
    if (value == null) { throw new TypeError('Expected OscilloscopeAddSettingChannelRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OscilloscopeAddSettingChannelRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
    };
  },
};
