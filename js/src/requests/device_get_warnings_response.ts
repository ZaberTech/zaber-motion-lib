/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetWarningsResponse {
  flags: string[];

}

export const DeviceGetWarningsResponse = {
  fromBinary: (buffer: Uint8Array): DeviceGetWarningsResponse => BSON.deserialize(buffer) as DeviceGetWarningsResponse,
  toBinary: (value: DeviceGetWarningsResponse): Uint8Array => BSON.serialize(DeviceGetWarningsResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    flags: [],
  }) as Readonly<DeviceGetWarningsResponse>,
  sanitize: (value: DeviceGetWarningsResponse): DeviceGetWarningsResponse => {
    if (value == null) { throw new TypeError('Expected DeviceGetWarningsResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetWarningsResponse object but got ${typeof value}.`) }
    return {
      flags: Array.from(value.flags ?? [], item => sanitizer.sanitizeString(item, 'items of flags')),
    };
  },
};
