/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface WaitToClearWarningsRequest {
  interfaceId: number;

  device: number;

  axis: number;

  timeout: number;

  warningFlags: string[];

}

export const WaitToClearWarningsRequest = {
  fromBinary: (buffer: Uint8Array): WaitToClearWarningsRequest => BSON.deserialize(buffer) as WaitToClearWarningsRequest,
  toBinary: (value: WaitToClearWarningsRequest): Uint8Array => BSON.serialize(WaitToClearWarningsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    timeout: 0,
    warningFlags: [],
  }) as Readonly<WaitToClearWarningsRequest>,
  sanitize: (value: WaitToClearWarningsRequest): WaitToClearWarningsRequest => {
    if (value == null) { throw new TypeError('Expected WaitToClearWarningsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected WaitToClearWarningsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
      warningFlags: Array.from(value.warningFlags ?? [], item => sanitizer.sanitizeString(item, 'items of warningFlags')),
    };
  },
};
