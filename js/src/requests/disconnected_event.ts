/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Errors } from '../requests/errors';

/**
 * Event that is sent when a connection is lost.
 */
export interface DisconnectedEvent {
  /**
   * The id of the interface that was disconnected.
   */
  interfaceId: number;

  /**
   * The type of error that caused the disconnection.
   */
  errorType: Errors;

  /**
   * The message describing the error.
   */
  errorMessage: string;

}

export const DisconnectedEvent = {
  fromBinary: (buffer: Uint8Array): DisconnectedEvent => BSON.deserialize(buffer) as DisconnectedEvent,
  toBinary: (value: DisconnectedEvent): Uint8Array => BSON.serialize(DisconnectedEvent.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    errorType: 0 as Errors,
    errorMessage: '',
  }) as Readonly<DisconnectedEvent>,
  sanitize: (value: DisconnectedEvent): DisconnectedEvent => {
    if (value == null) { throw new TypeError('Expected DisconnectedEvent object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DisconnectedEvent object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      errorType: sanitizer.sanitizeEnum(value.errorType, 'Errors', Errors, 'errorType'),
      errorMessage: sanitizer.sanitizeString(value.errorMessage, 'errorMessage'),
    };
  },
};
