/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TranslatorConfig } from '../gcode/translator_config';

export interface TranslatorCreateFromDeviceRequest {
  interfaceId: number;

  device: number;

  axes: number[];

  config?: (TranslatorConfig | null);

}

export const TranslatorCreateFromDeviceRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorCreateFromDeviceRequest => BSON.deserialize(buffer) as TranslatorCreateFromDeviceRequest,
  toBinary: (value: TranslatorCreateFromDeviceRequest): Uint8Array => BSON.serialize(TranslatorCreateFromDeviceRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axes: [],
    config: null,
  }) as Readonly<TranslatorCreateFromDeviceRequest>,
  sanitize: (value: TranslatorCreateFromDeviceRequest): TranslatorCreateFromDeviceRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorCreateFromDeviceRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorCreateFromDeviceRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
      config: value.config != null ? TranslatorConfig.sanitize(value.config) : null,
    };
  },
};
