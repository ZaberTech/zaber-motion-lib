/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface AxisToStringRequest {
  interfaceId: number;

  device: number;

  axis: number;

  typeOverride: string;

}

export const AxisToStringRequest = {
  fromBinary: (buffer: Uint8Array): AxisToStringRequest => BSON.deserialize(buffer) as AxisToStringRequest,
  toBinary: (value: AxisToStringRequest): Uint8Array => BSON.serialize(AxisToStringRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    typeOverride: '',
  }) as Readonly<AxisToStringRequest>,
  sanitize: (value: AxisToStringRequest): AxisToStringRequest => {
    if (value == null) { throw new TypeError('Expected AxisToStringRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisToStringRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      typeOverride: sanitizer.sanitizeString(value.typeOverride, 'typeOverride'),
    };
  },
};
