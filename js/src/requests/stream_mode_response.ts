/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamMode } from '../ascii/stream_mode';
import { PvtMode } from '../ascii/pvt_mode';

export interface StreamModeResponse {
  streamMode: StreamMode;

  pvtMode: PvtMode;

}

export const StreamModeResponse = {
  fromBinary: (buffer: Uint8Array): StreamModeResponse => BSON.deserialize(buffer) as StreamModeResponse,
  toBinary: (value: StreamModeResponse): Uint8Array => BSON.serialize(StreamModeResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    streamMode: 0 as StreamMode,
    pvtMode: 0 as PvtMode,
  }) as Readonly<StreamModeResponse>,
  sanitize: (value: StreamModeResponse): StreamModeResponse => {
    if (value == null) { throw new TypeError('Expected StreamModeResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamModeResponse object but got ${typeof value}.`) }
    return {
      streamMode: sanitizer.sanitizeEnum(value.streamMode, 'StreamMode', StreamMode, 'streamMode'),
      pvtMode: sanitizer.sanitizeEnum(value.pvtMode, 'PvtMode', PvtMode, 'pvtMode'),
    };
  },
};
