/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamSetMaxSpeedRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  maxSpeed: number;

  unit: Units;

}

export const StreamSetMaxSpeedRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetMaxSpeedRequest => BSON.deserialize(buffer) as StreamSetMaxSpeedRequest,
  toBinary: (value: StreamSetMaxSpeedRequest): Uint8Array => BSON.serialize(StreamSetMaxSpeedRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    maxSpeed: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetMaxSpeedRequest>,
  sanitize: (value: StreamSetMaxSpeedRequest): StreamSetMaxSpeedRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetMaxSpeedRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetMaxSpeedRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      maxSpeed: sanitizer.sanitizeNumber(value.maxSpeed, 'maxSpeed'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
