/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { TriggerState } from '../ascii/trigger_state';

export interface TriggerStates {
  states: TriggerState[];

}

export const TriggerStates = {
  fromBinary: (buffer: Uint8Array): TriggerStates => BSON.deserialize(buffer) as TriggerStates,
  toBinary: (value: TriggerStates): Uint8Array => BSON.serialize(TriggerStates.sanitize(value)),
  DEFAULT: Object.freeze({
    states: [],
  }) as Readonly<TriggerStates>,
  sanitize: (value: TriggerStates): TriggerStates => {
    if (value == null) { throw new TypeError('Expected TriggerStates object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerStates object but got ${typeof value}.`) }
    return {
      states: Array.from(value.states ?? [], item => TriggerState.sanitize(item)),
    };
  },
};
