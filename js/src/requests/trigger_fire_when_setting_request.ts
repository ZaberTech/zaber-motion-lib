/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TriggerCondition } from '../ascii/trigger_condition';
import { Units } from '../units';

export interface TriggerFireWhenSettingRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  axis: number;

  setting: string;

  triggerCondition: TriggerCondition;

  value: number;

  unit: Units;

}

export const TriggerFireWhenSettingRequest = {
  fromBinary: (buffer: Uint8Array): TriggerFireWhenSettingRequest => BSON.deserialize(buffer) as TriggerFireWhenSettingRequest,
  toBinary: (value: TriggerFireWhenSettingRequest): Uint8Array => BSON.serialize(TriggerFireWhenSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    axis: 0,
    setting: '',
    triggerCondition: 0 as TriggerCondition,
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<TriggerFireWhenSettingRequest>,
  sanitize: (value: TriggerFireWhenSettingRequest): TriggerFireWhenSettingRequest => {
    if (value == null) { throw new TypeError('Expected TriggerFireWhenSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerFireWhenSettingRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
      triggerCondition: sanitizer.sanitizeEnum(value.triggerCondition, 'TriggerCondition', TriggerCondition, 'triggerCondition'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
