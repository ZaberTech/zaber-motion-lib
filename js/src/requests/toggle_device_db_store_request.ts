/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ToggleDeviceDbStoreRequest {
  toggleOn: boolean;

  storeLocation?: (string | null);

}

export const ToggleDeviceDbStoreRequest = {
  fromBinary: (buffer: Uint8Array): ToggleDeviceDbStoreRequest => BSON.deserialize(buffer) as ToggleDeviceDbStoreRequest,
  toBinary: (value: ToggleDeviceDbStoreRequest): Uint8Array => BSON.serialize(ToggleDeviceDbStoreRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    toggleOn: false,
    storeLocation: null,
  }) as Readonly<ToggleDeviceDbStoreRequest>,
  sanitize: (value: ToggleDeviceDbStoreRequest): ToggleDeviceDbStoreRequest => {
    if (value == null) { throw new TypeError('Expected ToggleDeviceDbStoreRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ToggleDeviceDbStoreRequest object but got ${typeof value}.`) }
    return {
      toggleOn: sanitizer.sanitizeBool(value.toggleOn, 'toggleOn'),
      storeLocation: value.storeLocation != null ? sanitizer.sanitizeString(value.storeLocation, 'storeLocation') : null,
    };
  },
};
