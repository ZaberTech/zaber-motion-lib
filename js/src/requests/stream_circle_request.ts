/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamSegmentType } from '../requests/stream_segment_type';
import { RotationDirection } from '../rotation_direction';
import { Measurement } from '../measurement';

export interface StreamCircleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  type: StreamSegmentType;

  rotationDirection: RotationDirection;

  centerX: Measurement;

  centerY: Measurement;

  targetAxesIndices: number[];

}

export const StreamCircleRequest = {
  fromBinary: (buffer: Uint8Array): StreamCircleRequest => BSON.deserialize(buffer) as StreamCircleRequest,
  toBinary: (value: StreamCircleRequest): Uint8Array => BSON.serialize(StreamCircleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    type: 0 as StreamSegmentType,
    rotationDirection: 0 as RotationDirection,
    centerX: Measurement.DEFAULT,
    centerY: Measurement.DEFAULT,
    targetAxesIndices: [],
  }) as Readonly<StreamCircleRequest>,
  sanitize: (value: StreamCircleRequest): StreamCircleRequest => {
    if (value == null) { throw new TypeError('Expected StreamCircleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamCircleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      type: sanitizer.sanitizeEnum(value.type, 'StreamSegmentType', StreamSegmentType, 'type'),
      rotationDirection: sanitizer.sanitizeEnum(value.rotationDirection, 'RotationDirection', RotationDirection, 'rotationDirection'),
      centerX: Measurement.sanitize(value.centerX),
      centerY: Measurement.sanitize(value.centerY),
      targetAxesIndices: Array.from(value.targetAxesIndices ?? [], item => sanitizer.sanitizeInt(item, 'items of targetAxesIndices')),
    };
  },
};
