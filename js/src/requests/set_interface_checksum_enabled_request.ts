/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface SetInterfaceChecksumEnabledRequest {
  interfaceId: number;

  isEnabled: boolean;

}

export const SetInterfaceChecksumEnabledRequest = {
  fromBinary: (buffer: Uint8Array): SetInterfaceChecksumEnabledRequest => BSON.deserialize(buffer) as SetInterfaceChecksumEnabledRequest,
  toBinary: (value: SetInterfaceChecksumEnabledRequest): Uint8Array => BSON.serialize(SetInterfaceChecksumEnabledRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    isEnabled: false,
  }) as Readonly<SetInterfaceChecksumEnabledRequest>,
  sanitize: (value: SetInterfaceChecksumEnabledRequest): SetInterfaceChecksumEnabledRequest => {
    if (value == null) { throw new TypeError('Expected SetInterfaceChecksumEnabledRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetInterfaceChecksumEnabledRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      isEnabled: sanitizer.sanitizeBool(value.isEnabled, 'isEnabled'),
    };
  },
};
