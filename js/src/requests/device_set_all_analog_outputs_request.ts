/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetAllAnalogOutputsRequest {
  interfaceId: number;

  device: number;

  values: number[];

}

export const DeviceSetAllAnalogOutputsRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAllAnalogOutputsRequest => BSON.deserialize(buffer) as DeviceSetAllAnalogOutputsRequest,
  toBinary: (value: DeviceSetAllAnalogOutputsRequest): Uint8Array => BSON.serialize(DeviceSetAllAnalogOutputsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    values: [],
  }) as Readonly<DeviceSetAllAnalogOutputsRequest>,
  sanitize: (value: DeviceSetAllAnalogOutputsRequest): DeviceSetAllAnalogOutputsRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAllAnalogOutputsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAllAnalogOutputsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeNumber(item, 'items of values')),
    };
  },
};
