/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';
import { Units } from '../units';

export interface DeviceSetDigitalOutputScheduleRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  value: DigitalOutputAction;

  futureValue: DigitalOutputAction;

  delay: number;

  unit: Units;

}

export const DeviceSetDigitalOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetDigitalOutputScheduleRequest => BSON.deserialize(buffer) as DeviceSetDigitalOutputScheduleRequest,
  toBinary: (value: DeviceSetDigitalOutputScheduleRequest): Uint8Array => BSON.serialize(DeviceSetDigitalOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    value: 0 as DigitalOutputAction,
    futureValue: 0 as DigitalOutputAction,
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetDigitalOutputScheduleRequest>,
  sanitize: (value: DeviceSetDigitalOutputScheduleRequest): DeviceSetDigitalOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetDigitalOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetDigitalOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeEnum(value.value, 'DigitalOutputAction', DigitalOutputAction, 'value'),
      futureValue: sanitizer.sanitizeEnum(value.futureValue, 'DigitalOutputAction', DigitalOutputAction, 'futureValue'),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
