/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetAllDigitalIOResponse {
  values: boolean[];

}

export const DeviceGetAllDigitalIOResponse = {
  fromBinary: (buffer: Uint8Array): DeviceGetAllDigitalIOResponse => BSON.deserialize(buffer) as DeviceGetAllDigitalIOResponse,
  toBinary: (value: DeviceGetAllDigitalIOResponse): Uint8Array => BSON.serialize(DeviceGetAllDigitalIOResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    values: [],
  }) as Readonly<DeviceGetAllDigitalIOResponse>,
  sanitize: (value: DeviceGetAllDigitalIOResponse): DeviceGetAllDigitalIOResponse => {
    if (value == null) { throw new TypeError('Expected DeviceGetAllDigitalIOResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetAllDigitalIOResponse object but got ${typeof value}.`) }
    return {
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeBool(item, 'items of values')),
    };
  },
};
