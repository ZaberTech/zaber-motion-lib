/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { IoPortType } from '../ascii/io_port_type';

export interface SetIoPortLabelRequest {
  interfaceId: number;

  device: number;

  portType: IoPortType;

  channelNumber: number;

  label?: (string | null);

}

export const SetIoPortLabelRequest = {
  fromBinary: (buffer: Uint8Array): SetIoPortLabelRequest => BSON.deserialize(buffer) as SetIoPortLabelRequest,
  toBinary: (value: SetIoPortLabelRequest): Uint8Array => BSON.serialize(SetIoPortLabelRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    portType: 0 as IoPortType,
    channelNumber: 0,
    label: null,
  }) as Readonly<SetIoPortLabelRequest>,
  sanitize: (value: SetIoPortLabelRequest): SetIoPortLabelRequest => {
    if (value == null) { throw new TypeError('Expected SetIoPortLabelRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetIoPortLabelRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      portType: sanitizer.sanitizeEnum(value.portType, 'IoPortType', IoPortType, 'portType'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      label: value.label != null ? sanitizer.sanitizeString(value.label, 'label') : null,
    };
  },
};
