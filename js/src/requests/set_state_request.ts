/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface SetStateRequest {
  interfaceId: number;

  device: number;

  axis: number;

  state: string;

  deviceOnly: boolean;

}

export const SetStateRequest = {
  fromBinary: (buffer: Uint8Array): SetStateRequest => BSON.deserialize(buffer) as SetStateRequest,
  toBinary: (value: SetStateRequest): Uint8Array => BSON.serialize(SetStateRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    state: '',
    deviceOnly: false,
  }) as Readonly<SetStateRequest>,
  sanitize: (value: SetStateRequest): SetStateRequest => {
    if (value == null) { throw new TypeError('Expected SetStateRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetStateRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      state: sanitizer.sanitizeString(value.state, 'state'),
      deviceOnly: sanitizer.sanitizeBool(value.deviceOnly, 'deviceOnly'),
    };
  },
};
