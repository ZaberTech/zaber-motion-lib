/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetAnalogOutputRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  value: number;

}

export const StreamSetAnalogOutputRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetAnalogOutputRequest => BSON.deserialize(buffer) as StreamSetAnalogOutputRequest,
  toBinary: (value: StreamSetAnalogOutputRequest): Uint8Array => BSON.serialize(StreamSetAnalogOutputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    value: 0,
  }) as Readonly<StreamSetAnalogOutputRequest>,
  sanitize: (value: StreamSetAnalogOutputRequest): StreamSetAnalogOutputRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetAnalogOutputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetAnalogOutputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
