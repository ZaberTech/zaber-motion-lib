/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceMoveSinRequest {
  interfaceId: number;

  device: number;

  axis: number;

  amplitude: number;

  amplitudeUnits: Units;

  period: number;

  periodUnits: Units;

  count: number;

  waitUntilIdle: boolean;

}

export const DeviceMoveSinRequest = {
  fromBinary: (buffer: Uint8Array): DeviceMoveSinRequest => BSON.deserialize(buffer) as DeviceMoveSinRequest,
  toBinary: (value: DeviceMoveSinRequest): Uint8Array => BSON.serialize(DeviceMoveSinRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    amplitude: 0,
    amplitudeUnits: Units.NATIVE,
    period: 0,
    periodUnits: Units.NATIVE,
    count: 0,
    waitUntilIdle: false,
  }) as Readonly<DeviceMoveSinRequest>,
  sanitize: (value: DeviceMoveSinRequest): DeviceMoveSinRequest => {
    if (value == null) { throw new TypeError('Expected DeviceMoveSinRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceMoveSinRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      amplitude: sanitizer.sanitizeNumber(value.amplitude, 'amplitude'),
      amplitudeUnits: sanitizer.sanitizeUnits(value.amplitudeUnits, 'amplitudeUnits'),
      period: sanitizer.sanitizeNumber(value.period, 'period'),
      periodUnits: sanitizer.sanitizeUnits(value.periodUnits, 'periodUnits'),
      count: sanitizer.sanitizeNumber(value.count, 'count'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
