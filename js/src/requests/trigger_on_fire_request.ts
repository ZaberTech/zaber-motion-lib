/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TriggerAction } from '../ascii/trigger_action';

export interface TriggerOnFireRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  action: TriggerAction;

  axis: number;

  command: string;

}

export const TriggerOnFireRequest = {
  fromBinary: (buffer: Uint8Array): TriggerOnFireRequest => BSON.deserialize(buffer) as TriggerOnFireRequest,
  toBinary: (value: TriggerOnFireRequest): Uint8Array => BSON.serialize(TriggerOnFireRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    action: 0 as TriggerAction,
    axis: 0,
    command: '',
  }) as Readonly<TriggerOnFireRequest>,
  sanitize: (value: TriggerOnFireRequest): TriggerOnFireRequest => {
    if (value == null) { throw new TypeError('Expected TriggerOnFireRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerOnFireRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      action: sanitizer.sanitizeEnum(value.action, 'TriggerAction', TriggerAction, 'action'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      command: sanitizer.sanitizeString(value.command, 'command'),
    };
  },
};
