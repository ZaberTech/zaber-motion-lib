/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamWaitRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  time: number;

  unit: Units;

}

export const StreamWaitRequest = {
  fromBinary: (buffer: Uint8Array): StreamWaitRequest => BSON.deserialize(buffer) as StreamWaitRequest,
  toBinary: (value: StreamWaitRequest): Uint8Array => BSON.serialize(StreamWaitRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    time: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamWaitRequest>,
  sanitize: (value: StreamWaitRequest): StreamWaitRequest => {
    if (value == null) { throw new TypeError('Expected StreamWaitRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamWaitRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      time: sanitizer.sanitizeNumber(value.time, 'time'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
