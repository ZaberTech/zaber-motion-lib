/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface WdiGenericRequest {
  interfaceId: number;

  registerId: number;

  size: number;

  count: number;

  offset: number;

  registerBank: string;

  data: number[];

}

export const WdiGenericRequest = {
  fromBinary: (buffer: Uint8Array): WdiGenericRequest => BSON.deserialize(buffer) as WdiGenericRequest,
  toBinary: (value: WdiGenericRequest): Uint8Array => BSON.serialize(WdiGenericRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    registerId: 0,
    size: 0,
    count: 0,
    offset: 0,
    registerBank: '',
    data: [],
  }) as Readonly<WdiGenericRequest>,
  sanitize: (value: WdiGenericRequest): WdiGenericRequest => {
    if (value == null) { throw new TypeError('Expected WdiGenericRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected WdiGenericRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      registerId: sanitizer.sanitizeInt(value.registerId, 'registerId'),
      size: sanitizer.sanitizeInt(value.size, 'size'),
      count: sanitizer.sanitizeInt(value.count, 'count'),
      offset: sanitizer.sanitizeInt(value.offset, 'offset'),
      registerBank: sanitizer.sanitizeString(value.registerBank, 'registerBank'),
      data: Array.from(value.data ?? [], item => sanitizer.sanitizeInt(item, 'items of data')),
    };
  },
};
