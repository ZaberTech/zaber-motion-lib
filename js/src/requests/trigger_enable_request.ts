/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TriggerEnableRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  count: number;

}

export const TriggerEnableRequest = {
  fromBinary: (buffer: Uint8Array): TriggerEnableRequest => BSON.deserialize(buffer) as TriggerEnableRequest,
  toBinary: (value: TriggerEnableRequest): Uint8Array => BSON.serialize(TriggerEnableRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    count: 0,
  }) as Readonly<TriggerEnableRequest>,
  sanitize: (value: TriggerEnableRequest): TriggerEnableRequest => {
    if (value == null) { throw new TypeError('Expected TriggerEnableRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerEnableRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      count: sanitizer.sanitizeInt(value.count, 'count'),
    };
  },
};
