/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { SimpleTuningParamDefinition } from '../ascii/simple_tuning_param_definition';

export interface GetSimpleTuningParamDefinitionResponse {
  params: SimpleTuningParamDefinition[];

}

export const GetSimpleTuningParamDefinitionResponse = {
  fromBinary: (buffer: Uint8Array): GetSimpleTuningParamDefinitionResponse => BSON.deserialize(buffer) as GetSimpleTuningParamDefinitionResponse,
  toBinary: (value: GetSimpleTuningParamDefinitionResponse): Uint8Array => BSON.serialize(GetSimpleTuningParamDefinitionResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    params: [],
  }) as Readonly<GetSimpleTuningParamDefinitionResponse>,
  sanitize: (value: GetSimpleTuningParamDefinitionResponse): GetSimpleTuningParamDefinitionResponse => {
    if (value == null) { throw new TypeError('Expected GetSimpleTuningParamDefinitionResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetSimpleTuningParamDefinitionResponse object but got ${typeof value}.`) }
    return {
      params: Array.from(value.params ?? [], item => SimpleTuningParamDefinition.sanitize(item)),
    };
  },
};
