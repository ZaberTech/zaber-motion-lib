/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TriggerFireAtIntervalRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  interval: number;

  unit: Units;

}

export const TriggerFireAtIntervalRequest = {
  fromBinary: (buffer: Uint8Array): TriggerFireAtIntervalRequest => BSON.deserialize(buffer) as TriggerFireAtIntervalRequest,
  toBinary: (value: TriggerFireAtIntervalRequest): Uint8Array => BSON.serialize(TriggerFireAtIntervalRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    interval: 0,
    unit: Units.NATIVE,
  }) as Readonly<TriggerFireAtIntervalRequest>,
  sanitize: (value: TriggerFireAtIntervalRequest): TriggerFireAtIntervalRequest => {
    if (value == null) { throw new TypeError('Expected TriggerFireAtIntervalRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerFireAtIntervalRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      interval: sanitizer.sanitizeNumber(value.interval, 'interval'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
