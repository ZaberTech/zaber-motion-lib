/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamSegmentType } from '../requests/stream_segment_type';
import { Measurement } from '../measurement';

export interface StreamLineRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  type: StreamSegmentType;

  endpoint: Measurement[];

  targetAxesIndices: number[];

}

export const StreamLineRequest = {
  fromBinary: (buffer: Uint8Array): StreamLineRequest => BSON.deserialize(buffer) as StreamLineRequest,
  toBinary: (value: StreamLineRequest): Uint8Array => BSON.serialize(StreamLineRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    type: 0 as StreamSegmentType,
    endpoint: [],
    targetAxesIndices: [],
  }) as Readonly<StreamLineRequest>,
  sanitize: (value: StreamLineRequest): StreamLineRequest => {
    if (value == null) { throw new TypeError('Expected StreamLineRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamLineRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      type: sanitizer.sanitizeEnum(value.type, 'StreamSegmentType', StreamSegmentType, 'type'),
      endpoint: Array.from(value.endpoint ?? [], item => Measurement.sanitize(item)),
      targetAxesIndices: Array.from(value.targetAxesIndices ?? [], item => sanitizer.sanitizeInt(item, 'items of targetAxesIndices')),
    };
  },
};
