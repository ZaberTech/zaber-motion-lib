/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamGenericCommandBatchRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  batch: string[];

}

export const StreamGenericCommandBatchRequest = {
  fromBinary: (buffer: Uint8Array): StreamGenericCommandBatchRequest => BSON.deserialize(buffer) as StreamGenericCommandBatchRequest,
  toBinary: (value: StreamGenericCommandBatchRequest): Uint8Array => BSON.serialize(StreamGenericCommandBatchRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    batch: [],
  }) as Readonly<StreamGenericCommandBatchRequest>,
  sanitize: (value: StreamGenericCommandBatchRequest): StreamGenericCommandBatchRequest => {
    if (value == null) { throw new TypeError('Expected StreamGenericCommandBatchRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamGenericCommandBatchRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      batch: Array.from(value.batch ?? [], item => sanitizer.sanitizeString(item, 'items of batch')),
    };
  },
};
