/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TriggerAction } from '../ascii/trigger_action';

export interface TriggerClearActionRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  action: TriggerAction;

}

export const TriggerClearActionRequest = {
  fromBinary: (buffer: Uint8Array): TriggerClearActionRequest => BSON.deserialize(buffer) as TriggerClearActionRequest,
  toBinary: (value: TriggerClearActionRequest): Uint8Array => BSON.serialize(TriggerClearActionRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    action: 0 as TriggerAction,
  }) as Readonly<TriggerClearActionRequest>,
  sanitize: (value: TriggerClearActionRequest): TriggerClearActionRequest => {
    if (value == null) { throw new TypeError('Expected TriggerClearActionRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerClearActionRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      action: sanitizer.sanitizeEnum(value.action, 'TriggerAction', TriggerAction, 'action'),
    };
  },
};
