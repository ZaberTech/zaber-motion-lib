/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface CustomInterfaceCloseRequest {
  transportId: number;

  errorMessage: string;

}

export const CustomInterfaceCloseRequest = {
  fromBinary: (buffer: Uint8Array): CustomInterfaceCloseRequest => BSON.deserialize(buffer) as CustomInterfaceCloseRequest,
  toBinary: (value: CustomInterfaceCloseRequest): Uint8Array => BSON.serialize(CustomInterfaceCloseRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    transportId: 0,
    errorMessage: '',
  }) as Readonly<CustomInterfaceCloseRequest>,
  sanitize: (value: CustomInterfaceCloseRequest): CustomInterfaceCloseRequest => {
    if (value == null) { throw new TypeError('Expected CustomInterfaceCloseRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CustomInterfaceCloseRequest object but got ${typeof value}.`) }
    return {
      transportId: sanitizer.sanitizeInt(value.transportId, 'transportId'),
      errorMessage: sanitizer.sanitizeString(value.errorMessage, 'errorMessage'),
    };
  },
};
