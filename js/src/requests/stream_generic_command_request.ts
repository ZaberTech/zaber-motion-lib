/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamGenericCommandRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  command: string;

}

export const StreamGenericCommandRequest = {
  fromBinary: (buffer: Uint8Array): StreamGenericCommandRequest => BSON.deserialize(buffer) as StreamGenericCommandRequest,
  toBinary: (value: StreamGenericCommandRequest): Uint8Array => BSON.serialize(StreamGenericCommandRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    command: '',
  }) as Readonly<StreamGenericCommandRequest>,
  sanitize: (value: StreamGenericCommandRequest): StreamGenericCommandRequest => {
    if (value == null) { throw new TypeError('Expected StreamGenericCommandRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamGenericCommandRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      command: sanitizer.sanitizeString(value.command, 'command'),
    };
  },
};
