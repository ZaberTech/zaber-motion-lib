/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';
import { ServoTuningParam } from '../ascii/servo_tuning_param';

export interface SetSimpleTuning {
  interfaceId: number;

  device: number;

  axis: number;

  paramset: ServoTuningParamset;

  carriageMass?: (number | null);

  loadMass: number;

  tuningParams: ServoTuningParam[];

}

export const SetSimpleTuning = {
  fromBinary: (buffer: Uint8Array): SetSimpleTuning => BSON.deserialize(buffer) as SetSimpleTuning,
  toBinary: (value: SetSimpleTuning): Uint8Array => BSON.serialize(SetSimpleTuning.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    paramset: 0 as ServoTuningParamset,
    carriageMass: null,
    loadMass: 0,
    tuningParams: [],
  }) as Readonly<SetSimpleTuning>,
  sanitize: (value: SetSimpleTuning): SetSimpleTuning => {
    if (value == null) { throw new TypeError('Expected SetSimpleTuning object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetSimpleTuning object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      paramset: sanitizer.sanitizeEnum(value.paramset, 'ServoTuningParamset', ServoTuningParamset, 'paramset'),
      carriageMass: value.carriageMass != null ? sanitizer.sanitizeNumber(value.carriageMass, 'carriageMass') : null,
      loadMass: sanitizer.sanitizeNumber(value.loadMass, 'loadMass'),
      tuningParams: Array.from(value.tuningParams ?? [], item => ServoTuningParam.sanitize(item)),
    };
  },
};
