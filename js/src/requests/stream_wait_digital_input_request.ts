/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamWaitDigitalInputRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  value: boolean;

}

export const StreamWaitDigitalInputRequest = {
  fromBinary: (buffer: Uint8Array): StreamWaitDigitalInputRequest => BSON.deserialize(buffer) as StreamWaitDigitalInputRequest,
  toBinary: (value: StreamWaitDigitalInputRequest): Uint8Array => BSON.serialize(StreamWaitDigitalInputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    value: false,
  }) as Readonly<StreamWaitDigitalInputRequest>,
  sanitize: (value: StreamWaitDigitalInputRequest): StreamWaitDigitalInputRequest => {
    if (value == null) { throw new TypeError('Expected StreamWaitDigitalInputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamWaitDigitalInputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeBool(value.value, 'value'),
    };
  },
};
