/* This file is generated. Do not modify by hand. */
export enum ResponseType {
  /** Ok. */
  OK = 0,
  /** Error. */
  ERROR = 1,
}
