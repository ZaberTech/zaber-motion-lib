/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface InterfaceEmptyRequest {
  interfaceId: number;

}

export const InterfaceEmptyRequest = {
  fromBinary: (buffer: Uint8Array): InterfaceEmptyRequest => BSON.deserialize(buffer) as InterfaceEmptyRequest,
  toBinary: (value: InterfaceEmptyRequest): Uint8Array => BSON.serialize(InterfaceEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
  }) as Readonly<InterfaceEmptyRequest>,
  sanitize: (value: InterfaceEmptyRequest): InterfaceEmptyRequest => {
    if (value == null) { throw new TypeError('Expected InterfaceEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected InterfaceEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
    };
  },
};
