/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepEmptyRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

}

export const LockstepEmptyRequest = {
  fromBinary: (buffer: Uint8Array): LockstepEmptyRequest => BSON.deserialize(buffer) as LockstepEmptyRequest,
  toBinary: (value: LockstepEmptyRequest): Uint8Array => BSON.serialize(LockstepEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
  }) as Readonly<LockstepEmptyRequest>,
  sanitize: (value: LockstepEmptyRequest): LockstepEmptyRequest => {
    if (value == null) { throw new TypeError('Expected LockstepEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
    };
  },
};
