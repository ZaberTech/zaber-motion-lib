/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorFlushResponse {
  commands: string[];

}

export const TranslatorFlushResponse = {
  fromBinary: (buffer: Uint8Array): TranslatorFlushResponse => BSON.deserialize(buffer) as TranslatorFlushResponse,
  toBinary: (value: TranslatorFlushResponse): Uint8Array => BSON.serialize(TranslatorFlushResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    commands: [],
  }) as Readonly<TranslatorFlushResponse>,
  sanitize: (value: TranslatorFlushResponse): TranslatorFlushResponse => {
    if (value == null) { throw new TypeError('Expected TranslatorFlushResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorFlushResponse object but got ${typeof value}.`) }
    return {
      commands: Array.from(value.commands ?? [], item => sanitizer.sanitizeString(item, 'items of commands')),
    };
  },
};
