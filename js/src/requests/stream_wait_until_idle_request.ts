/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamWaitUntilIdleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  throwErrorOnFault: boolean;

}

export const StreamWaitUntilIdleRequest = {
  fromBinary: (buffer: Uint8Array): StreamWaitUntilIdleRequest => BSON.deserialize(buffer) as StreamWaitUntilIdleRequest,
  toBinary: (value: StreamWaitUntilIdleRequest): Uint8Array => BSON.serialize(StreamWaitUntilIdleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    throwErrorOnFault: false,
  }) as Readonly<StreamWaitUntilIdleRequest>,
  sanitize: (value: StreamWaitUntilIdleRequest): StreamWaitUntilIdleRequest => {
    if (value == null) { throw new TypeError('Expected StreamWaitUntilIdleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamWaitUntilIdleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      throwErrorOnFault: sanitizer.sanitizeBool(value.throwErrorOnFault, 'throwErrorOnFault'),
    };
  },
};
