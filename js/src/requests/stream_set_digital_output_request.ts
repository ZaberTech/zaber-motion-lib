/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';

export interface StreamSetDigitalOutputRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  channelNumber: number;

  value: DigitalOutputAction;

}

export const StreamSetDigitalOutputRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetDigitalOutputRequest => BSON.deserialize(buffer) as StreamSetDigitalOutputRequest,
  toBinary: (value: StreamSetDigitalOutputRequest): Uint8Array => BSON.serialize(StreamSetDigitalOutputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    channelNumber: 0,
    value: 0 as DigitalOutputAction,
  }) as Readonly<StreamSetDigitalOutputRequest>,
  sanitize: (value: StreamSetDigitalOutputRequest): StreamSetDigitalOutputRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetDigitalOutputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetDigitalOutputRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      value: sanitizer.sanitizeEnum(value.value, 'DigitalOutputAction', DigitalOutputAction, 'value'),
    };
  },
};
