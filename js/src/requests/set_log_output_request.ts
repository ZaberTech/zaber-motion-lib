/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { LogOutputMode } from '../log_output_mode';

export interface SetLogOutputRequest {
  mode: LogOutputMode;

  filePath?: (string | null);

}

export const SetLogOutputRequest = {
  fromBinary: (buffer: Uint8Array): SetLogOutputRequest => BSON.deserialize(buffer) as SetLogOutputRequest,
  toBinary: (value: SetLogOutputRequest): Uint8Array => BSON.serialize(SetLogOutputRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    mode: 0 as LogOutputMode,
    filePath: null,
  }) as Readonly<SetLogOutputRequest>,
  sanitize: (value: SetLogOutputRequest): SetLogOutputRequest => {
    if (value == null) { throw new TypeError('Expected SetLogOutputRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetLogOutputRequest object but got ${typeof value}.`) }
    return {
      mode: sanitizer.sanitizeEnum(value.mode, 'LogOutputMode', LogOutputMode, 'mode'),
      filePath: value.filePath != null ? sanitizer.sanitizeString(value.filePath, 'filePath') : null,
    };
  },
};
