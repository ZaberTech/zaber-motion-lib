/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamEmptyRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

}

export const StreamEmptyRequest = {
  fromBinary: (buffer: Uint8Array): StreamEmptyRequest => BSON.deserialize(buffer) as StreamEmptyRequest,
  toBinary: (value: StreamEmptyRequest): Uint8Array => BSON.serialize(StreamEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
  }) as Readonly<StreamEmptyRequest>,
  sanitize: (value: StreamEmptyRequest): StreamEmptyRequest => {
    if (value == null) { throw new TypeError('Expected StreamEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
    };
  },
};
