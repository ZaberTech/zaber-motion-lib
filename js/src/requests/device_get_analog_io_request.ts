/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetAnalogIORequest {
  interfaceId: number;

  device: number;

  channelType: string;

  channelNumber: number;

}

export const DeviceGetAnalogIORequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetAnalogIORequest => BSON.deserialize(buffer) as DeviceGetAnalogIORequest,
  toBinary: (value: DeviceGetAnalogIORequest): Uint8Array => BSON.serialize(DeviceGetAnalogIORequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelType: '',
    channelNumber: 0,
  }) as Readonly<DeviceGetAnalogIORequest>,
  sanitize: (value: DeviceGetAnalogIORequest): DeviceGetAnalogIORequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetAnalogIORequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetAnalogIORequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelType: sanitizer.sanitizeString(value.channelType, 'channelType'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
    };
  },
};
