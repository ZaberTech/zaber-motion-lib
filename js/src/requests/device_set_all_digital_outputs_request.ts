/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';

export interface DeviceSetAllDigitalOutputsRequest {
  interfaceId: number;

  device: number;

  values: DigitalOutputAction[];

}

export const DeviceSetAllDigitalOutputsRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAllDigitalOutputsRequest => BSON.deserialize(buffer) as DeviceSetAllDigitalOutputsRequest,
  toBinary: (value: DeviceSetAllDigitalOutputsRequest): Uint8Array => BSON.serialize(DeviceSetAllDigitalOutputsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    values: [],
  }) as Readonly<DeviceSetAllDigitalOutputsRequest>,
  sanitize: (value: DeviceSetAllDigitalOutputsRequest): DeviceSetAllDigitalOutputsRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAllDigitalOutputsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAllDigitalOutputsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of values')),
    };
  },
};
