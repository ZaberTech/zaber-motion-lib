/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface FindDeviceRequest {
  interfaceId: number;

  deviceAddress: number;

}

export const FindDeviceRequest = {
  fromBinary: (buffer: Uint8Array): FindDeviceRequest => BSON.deserialize(buffer) as FindDeviceRequest,
  toBinary: (value: FindDeviceRequest): Uint8Array => BSON.serialize(FindDeviceRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    deviceAddress: 0,
  }) as Readonly<FindDeviceRequest>,
  sanitize: (value: FindDeviceRequest): FindDeviceRequest => {
    if (value == null) { throw new TypeError('Expected FindDeviceRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected FindDeviceRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      deviceAddress: sanitizer.sanitizeInt(value.deviceAddress, 'deviceAddress'),
    };
  },
};
