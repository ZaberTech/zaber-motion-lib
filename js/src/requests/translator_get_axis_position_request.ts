/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TranslatorGetAxisPositionRequest {
  translatorId: number;

  axis: string;

  unit: Units;

}

export const TranslatorGetAxisPositionRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorGetAxisPositionRequest => BSON.deserialize(buffer) as TranslatorGetAxisPositionRequest,
  toBinary: (value: TranslatorGetAxisPositionRequest): Uint8Array => BSON.serialize(TranslatorGetAxisPositionRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    axis: '',
    unit: Units.NATIVE,
  }) as Readonly<TranslatorGetAxisPositionRequest>,
  sanitize: (value: TranslatorGetAxisPositionRequest): TranslatorGetAxisPositionRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorGetAxisPositionRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorGetAxisPositionRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      axis: sanitizer.sanitizeString(value.axis, 'axis'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
