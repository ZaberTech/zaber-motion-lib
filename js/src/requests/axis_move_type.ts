/* This file is generated. Do not modify by hand. */
export enum AxisMoveType {
  /** Abs. */
  ABS = 0,
  /** Rel. */
  REL = 1,
  /** Vel. */
  VEL = 2,
  /** Max. */
  MAX = 3,
  /** Min. */
  MIN = 4,
  /** Index. */
  INDEX = 5,
}
