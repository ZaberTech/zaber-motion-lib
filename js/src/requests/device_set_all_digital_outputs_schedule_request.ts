/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { DigitalOutputAction } from '../ascii/digital_output_action';
import { Units } from '../units';

export interface DeviceSetAllDigitalOutputsScheduleRequest {
  interfaceId: number;

  device: number;

  values: DigitalOutputAction[];

  futureValues: DigitalOutputAction[];

  delay: number;

  unit: Units;

}

export const DeviceSetAllDigitalOutputsScheduleRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetAllDigitalOutputsScheduleRequest => BSON.deserialize(buffer) as DeviceSetAllDigitalOutputsScheduleRequest,
  toBinary: (value: DeviceSetAllDigitalOutputsScheduleRequest): Uint8Array => BSON.serialize(DeviceSetAllDigitalOutputsScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    values: [],
    futureValues: [],
    delay: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetAllDigitalOutputsScheduleRequest>,
  sanitize: (value: DeviceSetAllDigitalOutputsScheduleRequest): DeviceSetAllDigitalOutputsScheduleRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetAllDigitalOutputsScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetAllDigitalOutputsScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      values: Array.from(value.values ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of values')),
      futureValues: Array.from(value.futureValues ?? [], item => sanitizer.sanitizeEnum(item, 'DigitalOutputAction', DigitalOutputAction, 'items of futureValues')),
      delay: sanitizer.sanitizeNumber(value.delay, 'delay'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
