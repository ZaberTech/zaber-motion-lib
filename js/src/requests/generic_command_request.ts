/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface GenericCommandRequest {
  interfaceId: number;

  device: number;

  axis: number;

  command: string;

  checkErrors: boolean;

  timeout: number;

}

export const GenericCommandRequest = {
  fromBinary: (buffer: Uint8Array): GenericCommandRequest => BSON.deserialize(buffer) as GenericCommandRequest,
  toBinary: (value: GenericCommandRequest): Uint8Array => BSON.serialize(GenericCommandRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    command: '',
    checkErrors: false,
    timeout: 0,
  }) as Readonly<GenericCommandRequest>,
  sanitize: (value: GenericCommandRequest): GenericCommandRequest => {
    if (value == null) { throw new TypeError('Expected GenericCommandRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GenericCommandRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      command: sanitizer.sanitizeString(value.command, 'command'),
      checkErrors: sanitizer.sanitizeBool(value.checkErrors, 'checkErrors'),
      timeout: sanitizer.sanitizeInt(value.timeout, 'timeout'),
    };
  },
};
