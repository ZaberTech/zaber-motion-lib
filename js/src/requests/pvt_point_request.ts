/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { StreamSegmentType } from '../requests/stream_segment_type';
import { Measurement } from '../measurement';

export interface PvtPointRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  type: StreamSegmentType;

  positions: Measurement[];

  velocities: (Measurement | null)[];

  time: Measurement;

}

export const PvtPointRequest = {
  fromBinary: (buffer: Uint8Array): PvtPointRequest => BSON.deserialize(buffer) as PvtPointRequest,
  toBinary: (value: PvtPointRequest): Uint8Array => BSON.serialize(PvtPointRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    type: 0 as StreamSegmentType,
    positions: [],
    velocities: [],
    time: Measurement.DEFAULT,
  }) as Readonly<PvtPointRequest>,
  sanitize: (value: PvtPointRequest): PvtPointRequest => {
    if (value == null) { throw new TypeError('Expected PvtPointRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PvtPointRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      type: sanitizer.sanitizeEnum(value.type, 'StreamSegmentType', StreamSegmentType, 'type'),
      positions: Array.from(value.positions ?? [], item => Measurement.sanitize(item)),
      velocities: Array.from(value.velocities ?? [], item => item != null ? Measurement.sanitize(item) : null),
      time: Measurement.sanitize(value.time),
    };
  },
};
