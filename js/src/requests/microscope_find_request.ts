/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ThirdPartyComponents } from '../microscopy/third_party_components';

export interface MicroscopeFindRequest {
  interfaceId: number;

  thirdParty?: (ThirdPartyComponents | null);

}

export const MicroscopeFindRequest = {
  fromBinary: (buffer: Uint8Array): MicroscopeFindRequest => BSON.deserialize(buffer) as MicroscopeFindRequest,
  toBinary: (value: MicroscopeFindRequest): Uint8Array => BSON.serialize(MicroscopeFindRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    thirdParty: null,
  }) as Readonly<MicroscopeFindRequest>,
  sanitize: (value: MicroscopeFindRequest): MicroscopeFindRequest => {
    if (value == null) { throw new TypeError('Expected MicroscopeFindRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeFindRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      thirdParty: value.thirdParty != null ? ThirdPartyComponents.sanitize(value.thirdParty) : null,
    };
  },
};
