/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface AxesGetSettingRequest {
  interfaces: number[];

  devices: number[];

  axes: number[];

  unit: Units[];

  setting: string;

}

export const AxesGetSettingRequest = {
  fromBinary: (buffer: Uint8Array): AxesGetSettingRequest => BSON.deserialize(buffer) as AxesGetSettingRequest,
  toBinary: (value: AxesGetSettingRequest): Uint8Array => BSON.serialize(AxesGetSettingRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaces: [],
    devices: [],
    axes: [],
    unit: [],
    setting: '',
  }) as Readonly<AxesGetSettingRequest>,
  sanitize: (value: AxesGetSettingRequest): AxesGetSettingRequest => {
    if (value == null) { throw new TypeError('Expected AxesGetSettingRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxesGetSettingRequest object but got ${typeof value}.`) }
    return {
      interfaces: Array.from(value.interfaces ?? [], item => sanitizer.sanitizeInt(item, 'items of interfaces')),
      devices: Array.from(value.devices ?? [], item => sanitizer.sanitizeInt(item, 'items of devices')),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
      unit: Array.from(value.unit ?? [], item => sanitizer.sanitizeUnits(item, 'items of unit')),
      setting: sanitizer.sanitizeString(value.setting, 'setting'),
    };
  },
};
