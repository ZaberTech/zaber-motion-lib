/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface ProcessOn {
  interfaceId: number;

  device: number;

  axis: number;

  on: boolean;

  duration: number;

  unit: Units;

}

export const ProcessOn = {
  fromBinary: (buffer: Uint8Array): ProcessOn => BSON.deserialize(buffer) as ProcessOn,
  toBinary: (value: ProcessOn): Uint8Array => BSON.serialize(ProcessOn.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    on: false,
    duration: 0,
    unit: Units.NATIVE,
  }) as Readonly<ProcessOn>,
  sanitize: (value: ProcessOn): ProcessOn => {
    if (value == null) { throw new TypeError('Expected ProcessOn object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ProcessOn object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      on: sanitizer.sanitizeBool(value.on, 'on'),
      duration: sanitizer.sanitizeNumber(value.duration, 'duration'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
