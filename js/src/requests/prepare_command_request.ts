/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Measurement } from '../measurement';

export interface PrepareCommandRequest {
  interfaceId: number;

  device: number;

  axis: number;

  commandTemplate: string;

  parameters: Measurement[];

}

export const PrepareCommandRequest = {
  fromBinary: (buffer: Uint8Array): PrepareCommandRequest => BSON.deserialize(buffer) as PrepareCommandRequest,
  toBinary: (value: PrepareCommandRequest): Uint8Array => BSON.serialize(PrepareCommandRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    commandTemplate: '',
    parameters: [],
  }) as Readonly<PrepareCommandRequest>,
  sanitize: (value: PrepareCommandRequest): PrepareCommandRequest => {
    if (value == null) { throw new TypeError('Expected PrepareCommandRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected PrepareCommandRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      commandTemplate: sanitizer.sanitizeString(value.commandTemplate, 'commandTemplate'),
      parameters: Array.from(value.parameters ?? [], item => Measurement.sanitize(item)),
    };
  },
};
