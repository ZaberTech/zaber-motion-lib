/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamBufferList {
  interfaceId: number;

  device: number;

  pvt: boolean;

}

export const StreamBufferList = {
  fromBinary: (buffer: Uint8Array): StreamBufferList => BSON.deserialize(buffer) as StreamBufferList,
  toBinary: (value: StreamBufferList): Uint8Array => BSON.serialize(StreamBufferList.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    pvt: false,
  }) as Readonly<StreamBufferList>,
  sanitize: (value: StreamBufferList): StreamBufferList => {
    if (value == null) { throw new TypeError('Expected StreamBufferList object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamBufferList object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
    };
  },
};
