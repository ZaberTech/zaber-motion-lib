/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface AxisEmptyRequest {
  interfaceId: number;

  device: number;

  axis: number;

}

export const AxisEmptyRequest = {
  fromBinary: (buffer: Uint8Array): AxisEmptyRequest => BSON.deserialize(buffer) as AxisEmptyRequest,
  toBinary: (value: AxisEmptyRequest): Uint8Array => BSON.serialize(AxisEmptyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
  }) as Readonly<AxisEmptyRequest>,
  sanitize: (value: AxisEmptyRequest): AxisEmptyRequest => {
    if (value == null) { throw new TypeError('Expected AxisEmptyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisEmptyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
    };
  },
};
