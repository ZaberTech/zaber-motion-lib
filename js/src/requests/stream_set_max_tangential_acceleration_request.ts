/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamSetMaxTangentialAccelerationRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  maxTangentialAcceleration: number;

  unit: Units;

}

export const StreamSetMaxTangentialAccelerationRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetMaxTangentialAccelerationRequest => BSON.deserialize(buffer) as StreamSetMaxTangentialAccelerationRequest,
  toBinary: (value: StreamSetMaxTangentialAccelerationRequest): Uint8Array => BSON.serialize(StreamSetMaxTangentialAccelerationRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    maxTangentialAcceleration: 0,
    unit: Units.NATIVE,
  }) as Readonly<StreamSetMaxTangentialAccelerationRequest>,
  sanitize: (value: StreamSetMaxTangentialAccelerationRequest): StreamSetMaxTangentialAccelerationRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetMaxTangentialAccelerationRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetMaxTangentialAccelerationRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      maxTangentialAcceleration: sanitizer.sanitizeNumber(value.maxTangentialAcceleration, 'maxTangentialAcceleration'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
