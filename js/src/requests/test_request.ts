/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TestRequest {
  returnError: boolean;

  dataPing: string;

  returnErrorWithData: boolean;

}

export const TestRequest = {
  fromBinary: (buffer: Uint8Array): TestRequest => BSON.deserialize(buffer) as TestRequest,
  toBinary: (value: TestRequest): Uint8Array => BSON.serialize(TestRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    returnError: false,
    dataPing: '',
    returnErrorWithData: false,
  }) as Readonly<TestRequest>,
  sanitize: (value: TestRequest): TestRequest => {
    if (value == null) { throw new TypeError('Expected TestRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TestRequest object but got ${typeof value}.`) }
    return {
      returnError: sanitizer.sanitizeBool(value.returnError, 'returnError'),
      dataPing: sanitizer.sanitizeString(value.dataPing, 'dataPing'),
      returnErrorWithData: sanitizer.sanitizeBool(value.returnErrorWithData, 'returnErrorWithData'),
    };
  },
};
