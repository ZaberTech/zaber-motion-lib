/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { InterfaceType } from '../requests/interface_type';

export interface OpenBinaryInterfaceRequest {
  interfaceType: InterfaceType;

  portName: string;

  baudRate: number;

  hostName: string;

  port: number;

  useMessageIds: boolean;

}

export const OpenBinaryInterfaceRequest = {
  fromBinary: (buffer: Uint8Array): OpenBinaryInterfaceRequest => BSON.deserialize(buffer) as OpenBinaryInterfaceRequest,
  toBinary: (value: OpenBinaryInterfaceRequest): Uint8Array => BSON.serialize(OpenBinaryInterfaceRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceType: 0 as InterfaceType,
    portName: '',
    baudRate: 0,
    hostName: '',
    port: 0,
    useMessageIds: false,
  }) as Readonly<OpenBinaryInterfaceRequest>,
  sanitize: (value: OpenBinaryInterfaceRequest): OpenBinaryInterfaceRequest => {
    if (value == null) { throw new TypeError('Expected OpenBinaryInterfaceRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected OpenBinaryInterfaceRequest object but got ${typeof value}.`) }
    return {
      interfaceType: sanitizer.sanitizeEnum(value.interfaceType, 'InterfaceType', InterfaceType, 'interfaceType'),
      portName: sanitizer.sanitizeString(value.portName, 'portName'),
      baudRate: sanitizer.sanitizeInt(value.baudRate, 'baudRate'),
      hostName: sanitizer.sanitizeString(value.hostName, 'hostName'),
      port: sanitizer.sanitizeInt(value.port, 'port'),
      useMessageIds: sanitizer.sanitizeBool(value.useMessageIds, 'useMessageIds'),
    };
  },
};
