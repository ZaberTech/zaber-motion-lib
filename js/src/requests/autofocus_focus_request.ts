/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface AutofocusFocusRequest {
  providerId: number;

  interfaceId: number;

  focusAddress: number;

  focusAxis: number;

  turretAddress: number;

  scan: boolean;

  once: boolean;

  timeout: number;

}

export const AutofocusFocusRequest = {
  fromBinary: (buffer: Uint8Array): AutofocusFocusRequest => BSON.deserialize(buffer) as AutofocusFocusRequest,
  toBinary: (value: AutofocusFocusRequest): Uint8Array => BSON.serialize(AutofocusFocusRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    providerId: 0,
    interfaceId: 0,
    focusAddress: 0,
    focusAxis: 0,
    turretAddress: 0,
    scan: false,
    once: false,
    timeout: 0,
  }) as Readonly<AutofocusFocusRequest>,
  sanitize: (value: AutofocusFocusRequest): AutofocusFocusRequest => {
    if (value == null) { throw new TypeError('Expected AutofocusFocusRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusFocusRequest object but got ${typeof value}.`) }
    return {
      providerId: sanitizer.sanitizeInt(value.providerId, 'providerId'),
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      scan: sanitizer.sanitizeBool(value.scan, 'scan'),
      once: sanitizer.sanitizeBool(value.once, 'once'),
      timeout: sanitizer.sanitizeInt(value.timeout, 'timeout'),
    };
  },
};
