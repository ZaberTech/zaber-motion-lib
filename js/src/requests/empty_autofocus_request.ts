/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface EmptyAutofocusRequest {
  providerId: number;

  interfaceId: number;

  focusAddress: number;

  focusAxis: number;

  turretAddress: number;

}

export const EmptyAutofocusRequest = {
  fromBinary: (buffer: Uint8Array): EmptyAutofocusRequest => BSON.deserialize(buffer) as EmptyAutofocusRequest,
  toBinary: (value: EmptyAutofocusRequest): Uint8Array => BSON.serialize(EmptyAutofocusRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    providerId: 0,
    interfaceId: 0,
    focusAddress: 0,
    focusAxis: 0,
    turretAddress: 0,
  }) as Readonly<EmptyAutofocusRequest>,
  sanitize: (value: EmptyAutofocusRequest): EmptyAutofocusRequest => {
    if (value == null) { throw new TypeError('Expected EmptyAutofocusRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected EmptyAutofocusRequest object but got ${typeof value}.`) }
    return {
      providerId: sanitizer.sanitizeInt(value.providerId, 'providerId'),
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
    };
  },
};
