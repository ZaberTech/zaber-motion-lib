/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { StreamAxisDefinition } from '../ascii/stream_axis_definition';
import { PvtAxisDefinition } from '../ascii/pvt_axis_definition';

export interface StreamGetAxesResponse {
  axes: StreamAxisDefinition[];

  pvtAxes: PvtAxisDefinition[];

}

export const StreamGetAxesResponse = {
  fromBinary: (buffer: Uint8Array): StreamGetAxesResponse => BSON.deserialize(buffer) as StreamGetAxesResponse,
  toBinary: (value: StreamGetAxesResponse): Uint8Array => BSON.serialize(StreamGetAxesResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    axes: [],
    pvtAxes: [],
  }) as Readonly<StreamGetAxesResponse>,
  sanitize: (value: StreamGetAxesResponse): StreamGetAxesResponse => {
    if (value == null) { throw new TypeError('Expected StreamGetAxesResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamGetAxesResponse object but got ${typeof value}.`) }
    return {
      axes: Array.from(value.axes ?? [], item => StreamAxisDefinition.sanitize(item)),
      pvtAxes: Array.from(value.pvtAxes ?? [], item => PvtAxisDefinition.sanitize(item)),
    };
  },
};
