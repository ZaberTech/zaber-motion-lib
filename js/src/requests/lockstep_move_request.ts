/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisMoveType } from '../requests/axis_move_type';
import { Units } from '../units';

export interface LockstepMoveRequest {
  interfaceId: number;

  device: number;

  lockstepGroupId: number;

  waitUntilIdle: boolean;

  type: AxisMoveType;

  arg: number;

  unit: Units;

  velocity: number;

  velocityUnit: Units;

  acceleration: number;

  accelerationUnit: Units;

}

export const LockstepMoveRequest = {
  fromBinary: (buffer: Uint8Array): LockstepMoveRequest => BSON.deserialize(buffer) as LockstepMoveRequest,
  toBinary: (value: LockstepMoveRequest): Uint8Array => BSON.serialize(LockstepMoveRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    lockstepGroupId: 0,
    waitUntilIdle: false,
    type: 0 as AxisMoveType,
    arg: 0,
    unit: Units.NATIVE,
    velocity: 0,
    velocityUnit: Units.NATIVE,
    acceleration: 0,
    accelerationUnit: Units.NATIVE,
  }) as Readonly<LockstepMoveRequest>,
  sanitize: (value: LockstepMoveRequest): LockstepMoveRequest => {
    if (value == null) { throw new TypeError('Expected LockstepMoveRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepMoveRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      lockstepGroupId: sanitizer.sanitizeInt(value.lockstepGroupId, 'lockstepGroupId'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
      type: sanitizer.sanitizeEnum(value.type, 'AxisMoveType', AxisMoveType, 'type'),
      arg: sanitizer.sanitizeNumber(value.arg, 'arg'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
      velocity: sanitizer.sanitizeNumber(value.velocity, 'velocity'),
      velocityUnit: sanitizer.sanitizeUnits(value.velocityUnit, 'velocityUnit'),
      acceleration: sanitizer.sanitizeNumber(value.acceleration, 'acceleration'),
      accelerationUnit: sanitizer.sanitizeUnits(value.accelerationUnit, 'accelerationUnit'),
    };
  },
};
