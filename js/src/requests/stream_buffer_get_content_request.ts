/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamBufferGetContentRequest {
  interfaceId: number;

  device: number;

  bufferId: number;

  pvt: boolean;

}

export const StreamBufferGetContentRequest = {
  fromBinary: (buffer: Uint8Array): StreamBufferGetContentRequest => BSON.deserialize(buffer) as StreamBufferGetContentRequest,
  toBinary: (value: StreamBufferGetContentRequest): Uint8Array => BSON.serialize(StreamBufferGetContentRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    bufferId: 0,
    pvt: false,
  }) as Readonly<StreamBufferGetContentRequest>,
  sanitize: (value: StreamBufferGetContentRequest): StreamBufferGetContentRequest => {
    if (value == null) { throw new TypeError('Expected StreamBufferGetContentRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamBufferGetContentRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      bufferId: sanitizer.sanitizeInt(value.bufferId, 'bufferId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
    };
  },
};
