/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface CustomInterfaceReadRequest {
  transportId: number;

}

export const CustomInterfaceReadRequest = {
  fromBinary: (buffer: Uint8Array): CustomInterfaceReadRequest => BSON.deserialize(buffer) as CustomInterfaceReadRequest,
  toBinary: (value: CustomInterfaceReadRequest): Uint8Array => BSON.serialize(CustomInterfaceReadRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    transportId: 0,
  }) as Readonly<CustomInterfaceReadRequest>,
  sanitize: (value: CustomInterfaceReadRequest): CustomInterfaceReadRequest => {
    if (value == null) { throw new TypeError('Expected CustomInterfaceReadRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected CustomInterfaceReadRequest object but got ${typeof value}.`) }
    return {
      transportId: sanitizer.sanitizeInt(value.transportId, 'transportId'),
    };
  },
};
