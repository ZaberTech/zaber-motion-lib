/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamCancelOutputScheduleRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  analog: boolean;

  channelNumber: number;

}

export const StreamCancelOutputScheduleRequest = {
  fromBinary: (buffer: Uint8Array): StreamCancelOutputScheduleRequest => BSON.deserialize(buffer) as StreamCancelOutputScheduleRequest,
  toBinary: (value: StreamCancelOutputScheduleRequest): Uint8Array => BSON.serialize(StreamCancelOutputScheduleRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    analog: false,
    channelNumber: 0,
  }) as Readonly<StreamCancelOutputScheduleRequest>,
  sanitize: (value: StreamCancelOutputScheduleRequest): StreamCancelOutputScheduleRequest => {
    if (value == null) { throw new TypeError('Expected StreamCancelOutputScheduleRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamCancelOutputScheduleRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      analog: sanitizer.sanitizeBool(value.analog, 'analog'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
    };
  },
};
