/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DriverEnableRequest {
  interfaceId: number;

  device: number;

  axis: number;

  timeout: number;

}

export const DriverEnableRequest = {
  fromBinary: (buffer: Uint8Array): DriverEnableRequest => BSON.deserialize(buffer) as DriverEnableRequest,
  toBinary: (value: DriverEnableRequest): Uint8Array => BSON.serialize(DriverEnableRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    timeout: 0,
  }) as Readonly<DriverEnableRequest>,
  sanitize: (value: DriverEnableRequest): DriverEnableRequest => {
    if (value == null) { throw new TypeError('Expected DriverEnableRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DriverEnableRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
    };
  },
};
