/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceHomeRequest {
  interfaceId: number;

  device: number;

  axis: number;

  waitUntilIdle: boolean;

}

export const DeviceHomeRequest = {
  fromBinary: (buffer: Uint8Array): DeviceHomeRequest => BSON.deserialize(buffer) as DeviceHomeRequest,
  toBinary: (value: DeviceHomeRequest): Uint8Array => BSON.serialize(DeviceHomeRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    waitUntilIdle: false,
  }) as Readonly<DeviceHomeRequest>,
  sanitize: (value: DeviceHomeRequest): DeviceHomeRequest => {
    if (value == null) { throw new TypeError('Expected DeviceHomeRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceHomeRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
