/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface StreamGetMaxTangentialAccelerationRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  unit: Units;

}

export const StreamGetMaxTangentialAccelerationRequest = {
  fromBinary: (buffer: Uint8Array): StreamGetMaxTangentialAccelerationRequest => BSON.deserialize(buffer) as StreamGetMaxTangentialAccelerationRequest,
  toBinary: (value: StreamGetMaxTangentialAccelerationRequest): Uint8Array => BSON.serialize(StreamGetMaxTangentialAccelerationRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    unit: Units.NATIVE,
  }) as Readonly<StreamGetMaxTangentialAccelerationRequest>,
  sanitize: (value: StreamGetMaxTangentialAccelerationRequest): StreamGetMaxTangentialAccelerationRequest => {
    if (value == null) { throw new TypeError('Expected StreamGetMaxTangentialAccelerationRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamGetMaxTangentialAccelerationRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
