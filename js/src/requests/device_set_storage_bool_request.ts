/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetStorageBoolRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

  value: boolean;

}

export const DeviceSetStorageBoolRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetStorageBoolRequest => BSON.deserialize(buffer) as DeviceSetStorageBoolRequest,
  toBinary: (value: DeviceSetStorageBoolRequest): Uint8Array => BSON.serialize(DeviceSetStorageBoolRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
    value: false,
  }) as Readonly<DeviceSetStorageBoolRequest>,
  sanitize: (value: DeviceSetStorageBoolRequest): DeviceSetStorageBoolRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetStorageBoolRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetStorageBoolRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
      value: sanitizer.sanitizeBool(value.value, 'value'),
    };
  },
};
