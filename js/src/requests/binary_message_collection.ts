/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { Message } from '../binary/message';

export interface BinaryMessageCollection {
  messages: Message[];

}

export const BinaryMessageCollection = {
  fromBinary: (buffer: Uint8Array): BinaryMessageCollection => BSON.deserialize(buffer) as BinaryMessageCollection,
  toBinary: (value: BinaryMessageCollection): Uint8Array => BSON.serialize(BinaryMessageCollection.sanitize(value)),
  DEFAULT: Object.freeze({
    messages: [],
  }) as Readonly<BinaryMessageCollection>,
  sanitize: (value: BinaryMessageCollection): BinaryMessageCollection => {
    if (value == null) { throw new TypeError('Expected BinaryMessageCollection object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryMessageCollection object but got ${typeof value}.`) }
    return {
      messages: Array.from(value.messages ?? [], item => Message.sanitize(item)),
    };
  },
};
