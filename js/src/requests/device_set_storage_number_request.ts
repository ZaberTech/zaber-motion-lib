/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceSetStorageNumberRequest {
  interfaceId: number;

  device: number;

  axis: number;

  key: string;

  value: number;

}

export const DeviceSetStorageNumberRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetStorageNumberRequest => BSON.deserialize(buffer) as DeviceSetStorageNumberRequest,
  toBinary: (value: DeviceSetStorageNumberRequest): Uint8Array => BSON.serialize(DeviceSetStorageNumberRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    key: '',
    value: 0,
  }) as Readonly<DeviceSetStorageNumberRequest>,
  sanitize: (value: DeviceSetStorageNumberRequest): DeviceSetStorageNumberRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetStorageNumberRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetStorageNumberRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      key: sanitizer.sanitizeString(value.key, 'key'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
    };
  },
};
