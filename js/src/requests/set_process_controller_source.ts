/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ProcessControllerSource } from '../product/process_controller_source';

export interface SetProcessControllerSource {
  interfaceId: number;

  device: number;

  axis: number;

  source: ProcessControllerSource;

}

export const SetProcessControllerSource = {
  fromBinary: (buffer: Uint8Array): SetProcessControllerSource => BSON.deserialize(buffer) as SetProcessControllerSource,
  toBinary: (value: SetProcessControllerSource): Uint8Array => BSON.serialize(SetProcessControllerSource.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    source: ProcessControllerSource.DEFAULT,
  }) as Readonly<SetProcessControllerSource>,
  sanitize: (value: SetProcessControllerSource): SetProcessControllerSource => {
    if (value == null) { throw new TypeError('Expected SetProcessControllerSource object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetProcessControllerSource object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      source: ProcessControllerSource.sanitize(value.source),
    };
  },
};
