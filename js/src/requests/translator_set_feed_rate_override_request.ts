/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TranslatorSetFeedRateOverrideRequest {
  translatorId: number;

  coefficient: number;

}

export const TranslatorSetFeedRateOverrideRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorSetFeedRateOverrideRequest => BSON.deserialize(buffer) as TranslatorSetFeedRateOverrideRequest,
  toBinary: (value: TranslatorSetFeedRateOverrideRequest): Uint8Array => BSON.serialize(TranslatorSetFeedRateOverrideRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    coefficient: 0,
  }) as Readonly<TranslatorSetFeedRateOverrideRequest>,
  sanitize: (value: TranslatorSetFeedRateOverrideRequest): TranslatorSetFeedRateOverrideRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorSetFeedRateOverrideRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorSetFeedRateOverrideRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      coefficient: sanitizer.sanitizeNumber(value.coefficient, 'coefficient'),
    };
  },
};
