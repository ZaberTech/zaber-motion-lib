/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetAllAnalogIORequest {
  interfaceId: number;

  device: number;

  channelType: string;

}

export const DeviceGetAllAnalogIORequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetAllAnalogIORequest => BSON.deserialize(buffer) as DeviceGetAllAnalogIORequest,
  toBinary: (value: DeviceGetAllAnalogIORequest): Uint8Array => BSON.serialize(DeviceGetAllAnalogIORequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelType: '',
  }) as Readonly<DeviceGetAllAnalogIORequest>,
  sanitize: (value: DeviceGetAllAnalogIORequest): DeviceGetAllAnalogIORequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetAllAnalogIORequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetAllAnalogIORequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelType: sanitizer.sanitizeString(value.channelType, 'channelType'),
    };
  },
};
