/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetAllDigitalIORequest {
  interfaceId: number;

  device: number;

  channelType: string;

}

export const DeviceGetAllDigitalIORequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetAllDigitalIORequest => BSON.deserialize(buffer) as DeviceGetAllDigitalIORequest,
  toBinary: (value: DeviceGetAllDigitalIORequest): Uint8Array => BSON.serialize(DeviceGetAllDigitalIORequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelType: '',
  }) as Readonly<DeviceGetAllDigitalIORequest>,
  sanitize: (value: DeviceGetAllDigitalIORequest): DeviceGetAllDigitalIORequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetAllDigitalIORequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetAllDigitalIORequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelType: sanitizer.sanitizeString(value.channelType, 'channelType'),
    };
  },
};
