/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TriggerFireWhenDistanceTravelledRequest {
  interfaceId: number;

  device: number;

  triggerNumber: number;

  axis: number;

  distance: number;

  unit: Units;

}

export const TriggerFireWhenDistanceTravelledRequest = {
  fromBinary: (buffer: Uint8Array): TriggerFireWhenDistanceTravelledRequest => BSON.deserialize(buffer) as TriggerFireWhenDistanceTravelledRequest,
  toBinary: (value: TriggerFireWhenDistanceTravelledRequest): Uint8Array => BSON.serialize(TriggerFireWhenDistanceTravelledRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    triggerNumber: 0,
    axis: 0,
    distance: 0,
    unit: Units.NATIVE,
  }) as Readonly<TriggerFireWhenDistanceTravelledRequest>,
  sanitize: (value: TriggerFireWhenDistanceTravelledRequest): TriggerFireWhenDistanceTravelledRequest => {
    if (value == null) { throw new TypeError('Expected TriggerFireWhenDistanceTravelledRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TriggerFireWhenDistanceTravelledRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      triggerNumber: sanitizer.sanitizeInt(value.triggerNumber, 'triggerNumber'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      distance: sanitizer.sanitizeNumber(value.distance, 'distance'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
