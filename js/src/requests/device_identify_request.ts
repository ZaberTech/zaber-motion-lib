/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { FirmwareVersion } from '../firmware_version';

export interface DeviceIdentifyRequest {
  interfaceId: number;

  device: number;

  assumeVersion?: (FirmwareVersion | null);

}

export const DeviceIdentifyRequest = {
  fromBinary: (buffer: Uint8Array): DeviceIdentifyRequest => BSON.deserialize(buffer) as DeviceIdentifyRequest,
  toBinary: (value: DeviceIdentifyRequest): Uint8Array => BSON.serialize(DeviceIdentifyRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    assumeVersion: null,
  }) as Readonly<DeviceIdentifyRequest>,
  sanitize: (value: DeviceIdentifyRequest): DeviceIdentifyRequest => {
    if (value == null) { throw new TypeError('Expected DeviceIdentifyRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceIdentifyRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      assumeVersion: value.assumeVersion != null ? FirmwareVersion.sanitize(value.assumeVersion) : null,
    };
  },
};
