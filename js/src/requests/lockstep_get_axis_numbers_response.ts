/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface LockstepGetAxisNumbersResponse {
  axes: number[];

}

export const LockstepGetAxisNumbersResponse = {
  fromBinary: (buffer: Uint8Array): LockstepGetAxisNumbersResponse => BSON.deserialize(buffer) as LockstepGetAxisNumbersResponse,
  toBinary: (value: LockstepGetAxisNumbersResponse): Uint8Array => BSON.serialize(LockstepGetAxisNumbersResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    axes: [],
  }) as Readonly<LockstepGetAxisNumbersResponse>,
  sanitize: (value: LockstepGetAxisNumbersResponse): LockstepGetAxisNumbersResponse => {
    if (value == null) { throw new TypeError('Expected LockstepGetAxisNumbersResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected LockstepGetAxisNumbersResponse object but got ${typeof value}.`) }
    return {
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
    };
  },
};
