/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface SetInterfaceTimeoutRequest {
  interfaceId: number;

  timeout: number;

}

export const SetInterfaceTimeoutRequest = {
  fromBinary: (buffer: Uint8Array): SetInterfaceTimeoutRequest => BSON.deserialize(buffer) as SetInterfaceTimeoutRequest,
  toBinary: (value: SetInterfaceTimeoutRequest): Uint8Array => BSON.serialize(SetInterfaceTimeoutRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    timeout: 0,
  }) as Readonly<SetInterfaceTimeoutRequest>,
  sanitize: (value: SetInterfaceTimeoutRequest): SetInterfaceTimeoutRequest => {
    if (value == null) { throw new TypeError('Expected SetInterfaceTimeoutRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected SetInterfaceTimeoutRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      timeout: sanitizer.sanitizeInt(value.timeout, 'timeout'),
    };
  },
};
