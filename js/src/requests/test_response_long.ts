/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface TestResponseLong {
  dataPong: string[];

}

export const TestResponseLong = {
  fromBinary: (buffer: Uint8Array): TestResponseLong => BSON.deserialize(buffer) as TestResponseLong,
  toBinary: (value: TestResponseLong): Uint8Array => BSON.serialize(TestResponseLong.sanitize(value)),
  DEFAULT: Object.freeze({
    dataPong: [],
  }) as Readonly<TestResponseLong>,
  sanitize: (value: TestResponseLong): TestResponseLong => {
    if (value == null) { throw new TypeError('Expected TestResponseLong object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TestResponseLong object but got ${typeof value}.`) }
    return {
      dataPong: Array.from(value.dataPong ?? [], item => sanitizer.sanitizeString(item, 'items of dataPong')),
    };
  },
};
