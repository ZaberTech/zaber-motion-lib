/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { GetSettingResult } from '../ascii/get_setting_result';

export interface GetSettingResults {
  results: GetSettingResult[];

}

export const GetSettingResults = {
  fromBinary: (buffer: Uint8Array): GetSettingResults => BSON.deserialize(buffer) as GetSettingResults,
  toBinary: (value: GetSettingResults): Uint8Array => BSON.serialize(GetSettingResults.sanitize(value)),
  DEFAULT: Object.freeze({
    results: [],
  }) as Readonly<GetSettingResults>,
  sanitize: (value: GetSettingResults): GetSettingResults => {
    if (value == null) { throw new TypeError('Expected GetSettingResults object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GetSettingResults object but got ${typeof value}.`) }
    return {
      results: Array.from(value.results ?? [], item => GetSettingResult.sanitize(item)),
    };
  },
};
