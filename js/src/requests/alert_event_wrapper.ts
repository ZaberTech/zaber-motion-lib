/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AlertEvent } from '../ascii/alert_event';

export interface AlertEventWrapper {
  interfaceId: number;

  alert: AlertEvent;

}

export const AlertEventWrapper = {
  fromBinary: (buffer: Uint8Array): AlertEventWrapper => BSON.deserialize(buffer) as AlertEventWrapper,
  toBinary: (value: AlertEventWrapper): Uint8Array => BSON.serialize(AlertEventWrapper.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    alert: AlertEvent.DEFAULT,
  }) as Readonly<AlertEventWrapper>,
  sanitize: (value: AlertEventWrapper): AlertEventWrapper => {
    if (value == null) { throw new TypeError('Expected AlertEventWrapper object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AlertEventWrapper object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      alert: AlertEvent.sanitize(value.alert),
    };
  },
};
