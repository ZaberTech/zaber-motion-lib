/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamCallRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  streamBuffer: number;

  pvtBuffer: number;

}

export const StreamCallRequest = {
  fromBinary: (buffer: Uint8Array): StreamCallRequest => BSON.deserialize(buffer) as StreamCallRequest,
  toBinary: (value: StreamCallRequest): Uint8Array => BSON.serialize(StreamCallRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    streamBuffer: 0,
    pvtBuffer: 0,
  }) as Readonly<StreamCallRequest>,
  sanitize: (value: StreamCallRequest): StreamCallRequest => {
    if (value == null) { throw new TypeError('Expected StreamCallRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamCallRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      streamBuffer: sanitizer.sanitizeInt(value.streamBuffer, 'streamBuffer'),
      pvtBuffer: sanitizer.sanitizeInt(value.pvtBuffer, 'pvtBuffer'),
    };
  },
};
