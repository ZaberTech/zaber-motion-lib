/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface ObjectiveChangerSetRequest {
  interfaceId: number;

  turretAddress: number;

  focusAddress: number;

  focusAxis: number;

  value: number;

  unit: Units;

}

export const ObjectiveChangerSetRequest = {
  fromBinary: (buffer: Uint8Array): ObjectiveChangerSetRequest => BSON.deserialize(buffer) as ObjectiveChangerSetRequest,
  toBinary: (value: ObjectiveChangerSetRequest): Uint8Array => BSON.serialize(ObjectiveChangerSetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    turretAddress: 0,
    focusAddress: 0,
    focusAxis: 0,
    value: 0,
    unit: Units.NATIVE,
  }) as Readonly<ObjectiveChangerSetRequest>,
  sanitize: (value: ObjectiveChangerSetRequest): ObjectiveChangerSetRequest => {
    if (value == null) { throw new TypeError('Expected ObjectiveChangerSetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ObjectiveChangerSetRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      turretAddress: sanitizer.sanitizeInt(value.turretAddress, 'turretAddress'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
      value: sanitizer.sanitizeNumber(value.value, 'value'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
