/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceGetDigitalIORequest {
  interfaceId: number;

  device: number;

  channelType: string;

  channelNumber: number;

}

export const DeviceGetDigitalIORequest = {
  fromBinary: (buffer: Uint8Array): DeviceGetDigitalIORequest => BSON.deserialize(buffer) as DeviceGetDigitalIORequest,
  toBinary: (value: DeviceGetDigitalIORequest): Uint8Array => BSON.serialize(DeviceGetDigitalIORequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelType: '',
    channelNumber: 0,
  }) as Readonly<DeviceGetDigitalIORequest>,
  sanitize: (value: DeviceGetDigitalIORequest): DeviceGetDigitalIORequest => {
    if (value == null) { throw new TypeError('Expected DeviceGetDigitalIORequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceGetDigitalIORequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelType: sanitizer.sanitizeString(value.channelType, 'channelType'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
    };
  },
};
