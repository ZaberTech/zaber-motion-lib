/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface TranslatorGetAxisOffsetRequest {
  translatorId: number;

  coordinateSystem: string;

  axis: string;

  unit: Units;

}

export const TranslatorGetAxisOffsetRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorGetAxisOffsetRequest => BSON.deserialize(buffer) as TranslatorGetAxisOffsetRequest,
  toBinary: (value: TranslatorGetAxisOffsetRequest): Uint8Array => BSON.serialize(TranslatorGetAxisOffsetRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    translatorId: 0,
    coordinateSystem: '',
    axis: '',
    unit: Units.NATIVE,
  }) as Readonly<TranslatorGetAxisOffsetRequest>,
  sanitize: (value: TranslatorGetAxisOffsetRequest): TranslatorGetAxisOffsetRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorGetAxisOffsetRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorGetAxisOffsetRequest object but got ${typeof value}.`) }
    return {
      translatorId: sanitizer.sanitizeInt(value.translatorId, 'translatorId'),
      coordinateSystem: sanitizer.sanitizeString(value.coordinateSystem, 'coordinateSystem'),
      axis: sanitizer.sanitizeString(value.axis, 'axis'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
