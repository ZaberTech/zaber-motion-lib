/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ServoTuningParamset } from '../ascii/servo_tuning_paramset';

export interface ServoTuningRequest {
  interfaceId: number;

  device: number;

  axis: number;

  paramset: ServoTuningParamset;

}

export const ServoTuningRequest = {
  fromBinary: (buffer: Uint8Array): ServoTuningRequest => BSON.deserialize(buffer) as ServoTuningRequest,
  toBinary: (value: ServoTuningRequest): Uint8Array => BSON.serialize(ServoTuningRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    paramset: 0 as ServoTuningParamset,
  }) as Readonly<ServoTuningRequest>,
  sanitize: (value: ServoTuningRequest): ServoTuningRequest => {
    if (value == null) { throw new TypeError('Expected ServoTuningRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ServoTuningRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      paramset: sanitizer.sanitizeEnum(value.paramset, 'ServoTuningParamset', ServoTuningParamset, 'paramset'),
    };
  },
};
