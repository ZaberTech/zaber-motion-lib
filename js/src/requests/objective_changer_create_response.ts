/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface ObjectiveChangerCreateResponse {
  turret: number;

  focusAddress: number;

  focusAxis: number;

}

export const ObjectiveChangerCreateResponse = {
  fromBinary: (buffer: Uint8Array): ObjectiveChangerCreateResponse => BSON.deserialize(buffer) as ObjectiveChangerCreateResponse,
  toBinary: (value: ObjectiveChangerCreateResponse): Uint8Array => BSON.serialize(ObjectiveChangerCreateResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    turret: 0,
    focusAddress: 0,
    focusAxis: 0,
  }) as Readonly<ObjectiveChangerCreateResponse>,
  sanitize: (value: ObjectiveChangerCreateResponse): ObjectiveChangerCreateResponse => {
    if (value == null) { throw new TypeError('Expected ObjectiveChangerCreateResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ObjectiveChangerCreateResponse object but got ${typeof value}.`) }
    return {
      turret: sanitizer.sanitizeInt(value.turret, 'turret'),
      focusAddress: sanitizer.sanitizeInt(value.focusAddress, 'focusAddress'),
      focusAxis: sanitizer.sanitizeInt(value.focusAxis, 'focusAxis'),
    };
  },
};
