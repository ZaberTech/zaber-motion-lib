/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface WaitToRespondRequest {
  interfaceId: number;

  device: number;

  timeout: number;

}

export const WaitToRespondRequest = {
  fromBinary: (buffer: Uint8Array): WaitToRespondRequest => BSON.deserialize(buffer) as WaitToRespondRequest,
  toBinary: (value: WaitToRespondRequest): Uint8Array => BSON.serialize(WaitToRespondRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    timeout: 0,
  }) as Readonly<WaitToRespondRequest>,
  sanitize: (value: WaitToRespondRequest): WaitToRespondRequest => {
    if (value == null) { throw new TypeError('Expected WaitToRespondRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected WaitToRespondRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
    };
  },
};
