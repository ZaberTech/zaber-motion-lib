/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface UnitGetSymbolRequest {
  unit: Units;

}

export const UnitGetSymbolRequest = {
  fromBinary: (buffer: Uint8Array): UnitGetSymbolRequest => BSON.deserialize(buffer) as UnitGetSymbolRequest,
  toBinary: (value: UnitGetSymbolRequest): Uint8Array => BSON.serialize(UnitGetSymbolRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    unit: Units.NATIVE,
  }) as Readonly<UnitGetSymbolRequest>,
  sanitize: (value: UnitGetSymbolRequest): UnitGetSymbolRequest => {
    if (value == null) { throw new TypeError('Expected UnitGetSymbolRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected UnitGetSymbolRequest object but got ${typeof value}.`) }
    return {
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
