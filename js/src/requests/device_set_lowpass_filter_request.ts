/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Units } from '../units';

export interface DeviceSetLowpassFilterRequest {
  interfaceId: number;

  device: number;

  channelNumber: number;

  cutoffFrequency: number;

  unit: Units;

}

export const DeviceSetLowpassFilterRequest = {
  fromBinary: (buffer: Uint8Array): DeviceSetLowpassFilterRequest => BSON.deserialize(buffer) as DeviceSetLowpassFilterRequest,
  toBinary: (value: DeviceSetLowpassFilterRequest): Uint8Array => BSON.serialize(DeviceSetLowpassFilterRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    channelNumber: 0,
    cutoffFrequency: 0,
    unit: Units.NATIVE,
  }) as Readonly<DeviceSetLowpassFilterRequest>,
  sanitize: (value: DeviceSetLowpassFilterRequest): DeviceSetLowpassFilterRequest => {
    if (value == null) { throw new TypeError('Expected DeviceSetLowpassFilterRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceSetLowpassFilterRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channelNumber: sanitizer.sanitizeInt(value.channelNumber, 'channelNumber'),
      cutoffFrequency: sanitizer.sanitizeNumber(value.cutoffFrequency, 'cutoffFrequency'),
      unit: sanitizer.sanitizeUnits(value.unit, 'unit'),
    };
  },
};
