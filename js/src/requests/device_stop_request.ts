/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface DeviceStopRequest {
  interfaceId: number;

  device: number;

  axis: number;

  waitUntilIdle: boolean;

}

export const DeviceStopRequest = {
  fromBinary: (buffer: Uint8Array): DeviceStopRequest => BSON.deserialize(buffer) as DeviceStopRequest,
  toBinary: (value: DeviceStopRequest): Uint8Array => BSON.serialize(DeviceStopRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    axis: 0,
    waitUntilIdle: false,
  }) as Readonly<DeviceStopRequest>,
  sanitize: (value: DeviceStopRequest): DeviceStopRequest => {
    if (value == null) { throw new TypeError('Expected DeviceStopRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceStopRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      axis: sanitizer.sanitizeInt(value.axis, 'axis'),
      waitUntilIdle: sanitizer.sanitizeBool(value.waitUntilIdle, 'waitUntilIdle'),
    };
  },
};
