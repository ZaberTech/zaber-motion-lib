/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface StreamSetupStoreRequest {
  interfaceId: number;

  device: number;

  streamId: number;

  pvt: boolean;

  streamBuffer: number;

  pvtBuffer: number;

  axes: number[];

}

export const StreamSetupStoreRequest = {
  fromBinary: (buffer: Uint8Array): StreamSetupStoreRequest => BSON.deserialize(buffer) as StreamSetupStoreRequest,
  toBinary: (value: StreamSetupStoreRequest): Uint8Array => BSON.serialize(StreamSetupStoreRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    streamId: 0,
    pvt: false,
    streamBuffer: 0,
    pvtBuffer: 0,
    axes: [],
  }) as Readonly<StreamSetupStoreRequest>,
  sanitize: (value: StreamSetupStoreRequest): StreamSetupStoreRequest => {
    if (value == null) { throw new TypeError('Expected StreamSetupStoreRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected StreamSetupStoreRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      streamId: sanitizer.sanitizeInt(value.streamId, 'streamId'),
      pvt: sanitizer.sanitizeBool(value.pvt, 'pvt'),
      streamBuffer: sanitizer.sanitizeInt(value.streamBuffer, 'streamBuffer'),
      pvtBuffer: sanitizer.sanitizeInt(value.pvtBuffer, 'pvtBuffer'),
      axes: Array.from(value.axes ?? [], item => sanitizer.sanitizeInt(item, 'items of axes')),
    };
  },
};
