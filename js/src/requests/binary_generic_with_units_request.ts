/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { CommandCode } from '../binary/command_code';
import { Units } from '../units';

export interface BinaryGenericWithUnitsRequest {
  interfaceId: number;

  device: number;

  command: CommandCode;

  data: number;

  fromUnit: Units;

  toUnit: Units;

  timeout: number;

}

export const BinaryGenericWithUnitsRequest = {
  fromBinary: (buffer: Uint8Array): BinaryGenericWithUnitsRequest => BSON.deserialize(buffer) as BinaryGenericWithUnitsRequest,
  toBinary: (value: BinaryGenericWithUnitsRequest): Uint8Array => BSON.serialize(BinaryGenericWithUnitsRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    interfaceId: 0,
    device: 0,
    command: 0 as CommandCode,
    data: 0,
    fromUnit: Units.NATIVE,
    toUnit: Units.NATIVE,
    timeout: 0,
  }) as Readonly<BinaryGenericWithUnitsRequest>,
  sanitize: (value: BinaryGenericWithUnitsRequest): BinaryGenericWithUnitsRequest => {
    if (value == null) { throw new TypeError('Expected BinaryGenericWithUnitsRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected BinaryGenericWithUnitsRequest object but got ${typeof value}.`) }
    return {
      interfaceId: sanitizer.sanitizeInt(value.interfaceId, 'interfaceId'),
      device: sanitizer.sanitizeInt(value.device, 'device'),
      command: sanitizer.sanitizeEnum(value.command, 'CommandCode', CommandCode, 'command'),
      data: sanitizer.sanitizeNumber(value.data, 'data'),
      fromUnit: sanitizer.sanitizeUnits(value.fromUnit, 'fromUnit'),
      toUnit: sanitizer.sanitizeUnits(value.toUnit, 'toUnit'),
      timeout: sanitizer.sanitizeNumber(value.timeout, 'timeout'),
    };
  },
};
