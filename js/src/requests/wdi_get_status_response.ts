/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { WdiAutofocusProviderStatus } from '../microscopy/wdi_autofocus_provider_status';

export interface WdiGetStatusResponse {
  status: WdiAutofocusProviderStatus;

}

export const WdiGetStatusResponse = {
  fromBinary: (buffer: Uint8Array): WdiGetStatusResponse => BSON.deserialize(buffer) as WdiGetStatusResponse,
  toBinary: (value: WdiGetStatusResponse): Uint8Array => BSON.serialize(WdiGetStatusResponse.sanitize(value)),
  DEFAULT: Object.freeze({
    status: WdiAutofocusProviderStatus.DEFAULT,
  }) as Readonly<WdiGetStatusResponse>,
  sanitize: (value: WdiGetStatusResponse): WdiGetStatusResponse => {
    if (value == null) { throw new TypeError('Expected WdiGetStatusResponse object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected WdiGetStatusResponse object but got ${typeof value}.`) }
    return {
      status: WdiAutofocusProviderStatus.sanitize(value.status),
    };
  },
};
