/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

export interface GatewayRequest {
  request: string;

}

export const GatewayRequest = {
  fromBinary: (buffer: Uint8Array): GatewayRequest => BSON.deserialize(buffer) as GatewayRequest,
  toBinary: (value: GatewayRequest): Uint8Array => BSON.serialize(GatewayRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    request: '',
  }) as Readonly<GatewayRequest>,
  sanitize: (value: GatewayRequest): GatewayRequest => {
    if (value == null) { throw new TypeError('Expected GatewayRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GatewayRequest object but got ${typeof value}.`) }
    return {
      request: sanitizer.sanitizeString(value.request, 'request'),
    };
  },
};
