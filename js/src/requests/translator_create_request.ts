/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { DeviceDefinition } from '../gcode/device_definition';
import { TranslatorConfig } from '../gcode/translator_config';

export interface TranslatorCreateRequest {
  definition: DeviceDefinition;

  config?: (TranslatorConfig | null);

}

export const TranslatorCreateRequest = {
  fromBinary: (buffer: Uint8Array): TranslatorCreateRequest => BSON.deserialize(buffer) as TranslatorCreateRequest,
  toBinary: (value: TranslatorCreateRequest): Uint8Array => BSON.serialize(TranslatorCreateRequest.sanitize(value)),
  DEFAULT: Object.freeze({
    definition: DeviceDefinition.DEFAULT,
    config: null,
  }) as Readonly<TranslatorCreateRequest>,
  sanitize: (value: TranslatorCreateRequest): TranslatorCreateRequest => {
    if (value == null) { throw new TypeError('Expected TranslatorCreateRequest object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorCreateRequest object but got ${typeof value}.`) }
    return {
      definition: DeviceDefinition.sanitize(value.definition),
      config: value.config != null ? TranslatorConfig.sanitize(value.config) : null,
    };
  },
};
