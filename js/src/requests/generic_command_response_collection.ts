/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { Response } from '../ascii/response';

export interface GenericCommandResponseCollection {
  responses: Response[];

}

export const GenericCommandResponseCollection = {
  fromBinary: (buffer: Uint8Array): GenericCommandResponseCollection => BSON.deserialize(buffer) as GenericCommandResponseCollection,
  toBinary: (value: GenericCommandResponseCollection): Uint8Array => BSON.serialize(GenericCommandResponseCollection.sanitize(value)),
  DEFAULT: Object.freeze({
    responses: [],
  }) as Readonly<GenericCommandResponseCollection>,
  sanitize: (value: GenericCommandResponseCollection): GenericCommandResponseCollection => {
    if (value == null) { throw new TypeError('Expected GenericCommandResponseCollection object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected GenericCommandResponseCollection object but got ${typeof value}.`) }
    return {
      responses: Array.from(value.responses ?? [], item => Response.sanitize(item)),
    };
  },
};
