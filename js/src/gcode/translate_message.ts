/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Represents a message from translator regarding a block translation.
 */
export interface TranslateMessage {
  /**
   * The message describing an occurrence.
   */
  message: string;

  /**
   * The index in the block string that the message regards to.
   */
  fromBlock: number;

  /**
   * The end index in the block string that the message regards to.
   * The end index is exclusive.
   */
  toBlock: number;

}

export const TranslateMessage = {
  fromBinary: (buffer: Uint8Array): TranslateMessage => BSON.deserialize(buffer) as TranslateMessage,
  toBinary: (value: TranslateMessage): Uint8Array => BSON.serialize(TranslateMessage.sanitize(value)),
  DEFAULT: Object.freeze({
    message: '',
    fromBlock: 0,
    toBlock: 0,
  }) as Readonly<TranslateMessage>,
  sanitize: (value: TranslateMessage): TranslateMessage => {
    if (value == null) { throw new TypeError('Expected TranslateMessage object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslateMessage object but got ${typeof value}.`) }
    return {
      message: sanitizer.sanitizeString(value.message, 'message'),
      fromBlock: sanitizer.sanitizeInt(value.fromBlock, 'fromBlock'),
      toBlock: sanitizer.sanitizeInt(value.toBlock, 'toBlock'),
    };
  },
};
