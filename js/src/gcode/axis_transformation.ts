/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { Measurement } from '../measurement';

/**
 * Represents a transformation of a translator axis.
 */
export interface AxisTransformation {
  /**
   * Letter of the translator axis (X,Y,Z,A,B,C,E).
   */
  axisLetter: string;

  /**
   * Scaling factor.
   */
  scaling?: (number | null);

  /**
   * Translation distance.
   */
  translation?: (Measurement | null);

}

export const AxisTransformation = {
  fromBinary: (buffer: Uint8Array): AxisTransformation => BSON.deserialize(buffer) as AxisTransformation,
  toBinary: (value: AxisTransformation): Uint8Array => BSON.serialize(AxisTransformation.sanitize(value)),
  DEFAULT: Object.freeze({
    axisLetter: '',
    scaling: null,
    translation: null,
  }) as Readonly<AxisTransformation>,
  sanitize: (value: AxisTransformation): AxisTransformation => {
    if (value == null) { throw new TypeError('Expected AxisTransformation object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisTransformation object but got ${typeof value}.`) }
    return {
      axisLetter: sanitizer.sanitizeString(value.axisLetter, 'axisLetter'),
      scaling: value.scaling != null ? sanitizer.sanitizeNumber(value.scaling, 'scaling') : null,
      translation: value.translation != null ? Measurement.sanitize(value.translation) : null,
    };
  },
};
