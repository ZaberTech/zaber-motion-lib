﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { DeviceDefinition } from './device_definition';
import { TranslatorConfig } from './translator_config';
import { TranslateResult } from './translate_result';
import { Device } from '../ascii/device';
import { Angle, Length, Native, AngularVelocity, Velocity } from '../units';
import { registerForFinalization } from '../finalizer';

/**
 * Represents an offline G-Code translator.
 * It allows to translate G-Code blocks to Zaber ASCII protocol stream commands.
 * This translator does not need a connected device to perform translation.
 * Requires at least Firmware 7.11.
 */
export class OfflineTranslator {
  /**
   * The ID of the translator that serves to identify native resources.
   */
  public get translatorId(): number {
    return this._translatorId;
  }
  private _translatorId: number;

  /**
   * Current coordinate system.
   */
  public get coordinateSystem(): string {
    return this._getCurrentCoordinateSystem();
  }

  constructor(translatorId: number) {
    this._translatorId = translatorId;
    registerForFinalization(this, OfflineTranslator._free.bind(null, translatorId));
  }

  /**
   * Sets up translator from provided device definition and configuration.
   * @param definition Definition of device and its peripherals.
   * The definition must match a device that later performs the commands.
   * @param config Configuration of the translator.
   * @returns New instance of translator.
   */
  public static async setup(
    definition: DeviceDefinition,
    config?: TranslatorConfig
  ): Promise<OfflineTranslator> {
    const request: requests.TranslatorCreateRequest & gateway.Message = {
      ...requests.TranslatorCreateRequest.DEFAULT,
      definition: definition,
      config: config,
      toBinary() {
        return requests.TranslatorCreateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.TranslatorCreateResponse>(
      'gcode/create',
      request,
      requests.TranslatorCreateResponse.fromBinary);
    return new OfflineTranslator(response.translatorId);
  }

  /**
   * Sets up an offline translator from provided device, axes, and configuration.
   * @param device Device that later performs the command streaming.
   * @param axes Axis numbers that are later used to setup the stream.
   * For a lockstep group specify only the first axis of the group.
   * @param config Configuration of the translator.
   * @returns New instance of translator.
   */
  public static async setupFromDevice(
    device: Device,
    axes: number[],
    config?: TranslatorConfig
  ): Promise<OfflineTranslator> {
    const request: requests.TranslatorCreateFromDeviceRequest & gateway.Message = {
      ...requests.TranslatorCreateFromDeviceRequest.DEFAULT,
      interfaceId: device.connection.interfaceId,
      device: device.deviceAddress,
      axes: axes,
      config: config,
      toBinary() {
        return requests.TranslatorCreateFromDeviceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.TranslatorCreateResponse>(
      'gcode/create_from_device',
      request,
      requests.TranslatorCreateResponse.fromBinary);
    return new OfflineTranslator(response.translatorId);
  }

  /**
   * Translates a single block (line) of G-code.
   * @param block Block (line) of G-code.
   * @returns Result of translation containing the stream commands.
   */
  public translate(
    block: string
  ): TranslateResult {
    const request: requests.TranslatorTranslateRequest & gateway.Message = {
      ...requests.TranslatorTranslateRequest.DEFAULT,
      translatorId: this.translatorId,
      block: block,
      toBinary() {
        return requests.TranslatorTranslateRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<TranslateResult>(
      'gcode/translate',
      request,
      TranslateResult.fromBinary);
    return response;
  }

  /**
   * Flushes the remaining stream commands waiting in optimization buffer.
   * The flush is also performed by M2 and M30 codes.
   * @returns The remaining stream commands.
   */
  public flush(): string[] {
    const request: requests.TranslatorEmptyRequest & gateway.Message = {
      ...requests.TranslatorEmptyRequest.DEFAULT,
      translatorId: this.translatorId,
      toBinary() {
        return requests.TranslatorEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.TranslatorFlushResponse>(
      'gcode/flush',
      request,
      requests.TranslatorFlushResponse.fromBinary);
    return response.commands;
  }

  /**
   * Sets the speed at which the device moves when traversing (G0).
   * @param traverseRate The traverse rate.
   * @param unit Units of the traverse rate.
   */
  public setTraverseRate(
    traverseRate: number,
    unit: Velocity | AngularVelocity | Native
  ): void {
    const request: requests.TranslatorSetTraverseRateRequest & gateway.Message = {
      ...requests.TranslatorSetTraverseRateRequest.DEFAULT,
      translatorId: this.translatorId,
      traverseRate: traverseRate,
      unit: unit,
      toBinary() {
        return requests.TranslatorSetTraverseRateRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/set_traverse_rate', request);
  }

  /**
   * Sets position of translator's axis.
   * Use this method to set position after performing movement outside of the translator.
   * This method does not cause any movement.
   * @param axis Letter of the axis.
   * @param position The position.
   * @param unit Units of position.
   */
  public setAxisPosition(
    axis: string,
    position: number,
    unit: Length | Angle | Native
  ): void {
    const request: requests.TranslatorSetAxisPositionRequest & gateway.Message = {
      ...requests.TranslatorSetAxisPositionRequest.DEFAULT,
      translatorId: this.translatorId,
      axis: axis,
      position: position,
      unit: unit,
      toBinary() {
        return requests.TranslatorSetAxisPositionRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/set_axis_position', request);
  }

  /**
   * Gets position of translator's axis.
   * This method does not query device but returns value from translator's state.
   * @param axis Letter of the axis.
   * @param unit Units of position.
   * @returns Position of translator's axis.
   */
  public getAxisPosition(
    axis: string,
    unit: Length | Angle | Native
  ): number {
    const request: requests.TranslatorGetAxisPositionRequest & gateway.Message = {
      ...requests.TranslatorGetAxisPositionRequest.DEFAULT,
      translatorId: this.translatorId,
      axis: axis,
      unit: unit,
      toBinary() {
        return requests.TranslatorGetAxisPositionRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'gcode/get_axis_position',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the home position of translator's axis.
   * This position is used by G28.
   * @param axis Letter of the axis.
   * @param position The home position.
   * @param unit Units of position.
   */
  public setAxisHomePosition(
    axis: string,
    position: number,
    unit: Length | Angle | Native
  ): void {
    const request: requests.TranslatorSetAxisPositionRequest & gateway.Message = {
      ...requests.TranslatorSetAxisPositionRequest.DEFAULT,
      translatorId: this.translatorId,
      axis: axis,
      position: position,
      unit: unit,
      toBinary() {
        return requests.TranslatorSetAxisPositionRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/set_axis_home', request);
  }

  /**
   * Sets the secondary home position of translator's axis.
   * This position is used by G30.
   * @param axis Letter of the axis.
   * @param position The home position.
   * @param unit Units of position.
   */
  public setAxisSecondaryHomePosition(
    axis: string,
    position: number,
    unit: Length | Angle | Native
  ): void {
    const request: requests.TranslatorSetAxisPositionRequest & gateway.Message = {
      ...requests.TranslatorSetAxisPositionRequest.DEFAULT,
      translatorId: this.translatorId,
      axis: axis,
      position: position,
      unit: unit,
      toBinary() {
        return requests.TranslatorSetAxisPositionRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/set_axis_secondary_home', request);
  }

  /**
   * Gets offset of an axis in a given coordinate system.
   * @param coordinateSystem Coordinate system (e.g. G54).
   * @param axis Letter of the axis.
   * @param unit Units of position.
   * @returns Offset in translator units of the axis.
   */
  public getAxisCoordinateSystemOffset(
    coordinateSystem: string,
    axis: string,
    unit: Length | Angle | Native
  ): number {
    const request: requests.TranslatorGetAxisOffsetRequest & gateway.Message = {
      ...requests.TranslatorGetAxisOffsetRequest.DEFAULT,
      translatorId: this.translatorId,
      coordinateSystem: coordinateSystem,
      axis: axis,
      unit: unit,
      toBinary() {
        return requests.TranslatorGetAxisOffsetRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.DoubleResponse>(
      'gcode/get_axis_offset',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Resets internal state after device rejected generated command.
   * Axis positions become uninitialized.
   */
  public resetAfterStreamError(): void {
    const request: requests.TranslatorEmptyRequest & gateway.Message = {
      ...requests.TranslatorEmptyRequest.DEFAULT,
      translatorId: this.translatorId,
      toBinary() {
        return requests.TranslatorEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/reset_after_stream_error', request);
  }

  /**
   * Allows to scale feed rate of the translated code by a coefficient.
   * @param coefficient Coefficient of the original feed rate.
   */
  public setFeedRateOverride(
    coefficient: number
  ): void {
    const request: requests.TranslatorSetFeedRateOverrideRequest & gateway.Message = {
      ...requests.TranslatorSetFeedRateOverrideRequest.DEFAULT,
      translatorId: this.translatorId,
      coefficient: coefficient,
      toBinary() {
        return requests.TranslatorSetFeedRateOverrideRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/set_feed_rate_override', request);
  }

  /**
   * Releases native resources of a translator.
   * @param translatorId The ID of the translator.
   */
  private static _free(
    translatorId: number
  ): void {
    const request: requests.TranslatorEmptyRequest & gateway.Message = {
      ...requests.TranslatorEmptyRequest.DEFAULT,
      translatorId: translatorId,
      toBinary() {
        return requests.TranslatorEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('gcode/free', request);
  }

  /**
   * Gets current coordinate system (e.g. G54).
   * @returns Current coordinate system.
   */
  private _getCurrentCoordinateSystem(): string {
    const request: requests.TranslatorEmptyRequest & gateway.Message = {
      ...requests.TranslatorEmptyRequest.DEFAULT,
      translatorId: this.translatorId,
      toBinary() {
        return requests.TranslatorEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'gcode/get_current_coordinate_system',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Releases the native resources of the translator.
   * Should only be called if your environment does not support FinalizationRegistry.
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry
   */
  public free(): void {
    OfflineTranslator._free(this.translatorId);
  }
}
