/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisDefinition } from '../gcode/axis_definition';
import { Measurement } from '../measurement';

/**
 * Holds information about device and its axes for purpose of a translator.
 */
export interface DeviceDefinition {
  /**
   * Device ID of the controller.
   * Can be obtained from device settings.
   */
  deviceId: number;

  /**
   * Applicable axes of the device.
   */
  axes: AxisDefinition[];

  /**
   * The smallest of each axis' maxspeed setting value.
   * This value becomes the traverse rate of the translator.
   */
  maxSpeed: Measurement;

}

export const DeviceDefinition = {
  fromBinary: (buffer: Uint8Array): DeviceDefinition => BSON.deserialize(buffer) as DeviceDefinition,
  toBinary: (value: DeviceDefinition): Uint8Array => BSON.serialize(DeviceDefinition.sanitize(value)),
  DEFAULT: Object.freeze({
    deviceId: 0,
    axes: [],
    maxSpeed: Measurement.DEFAULT,
  }) as Readonly<DeviceDefinition>,
  sanitize: (value: DeviceDefinition): DeviceDefinition => {
    if (value == null) { throw new TypeError('Expected DeviceDefinition object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected DeviceDefinition object but got ${typeof value}.`) }
    return {
      deviceId: sanitizer.sanitizeInt(value.deviceId, 'deviceId'),
      axes: Array.from(value.axes ?? [], item => AxisDefinition.sanitize(item)),
      maxSpeed: Measurement.sanitize(value.maxSpeed),
    };
  },
};
