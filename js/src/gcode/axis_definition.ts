/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Defines an axis of the translator.
 */
export interface AxisDefinition {
  /**
   * ID of the peripheral.
   */
  peripheralId: number;

  /**
   * Microstep resolution of the axis.
   * Can be obtained by reading the resolution setting.
   * Leave empty if the axis does not have the setting.
   */
  microstepResolution?: (number | null);

}

export const AxisDefinition = {
  fromBinary: (buffer: Uint8Array): AxisDefinition => BSON.deserialize(buffer) as AxisDefinition,
  toBinary: (value: AxisDefinition): Uint8Array => BSON.serialize(AxisDefinition.sanitize(value)),
  DEFAULT: Object.freeze({
    peripheralId: 0,
    microstepResolution: null,
  }) as Readonly<AxisDefinition>,
  sanitize: (value: AxisDefinition): AxisDefinition => {
    if (value == null) { throw new TypeError('Expected AxisDefinition object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisDefinition object but got ${typeof value}.`) }
    return {
      peripheralId: sanitizer.sanitizeInt(value.peripheralId, 'peripheralId'),
      microstepResolution: value.microstepResolution != null ? sanitizer.sanitizeInt(value.microstepResolution, 'microstepResolution') : null,
    };
  },
};
