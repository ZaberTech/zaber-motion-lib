/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { AxisMapping } from '../gcode/axis_mapping';
import { AxisTransformation } from '../gcode/axis_transformation';

/**
 * Configuration of a translator.
 */
export interface TranslatorConfig {
  /**
   * Optional custom mapping of translator axes to stream axes.
   */
  axisMappings?: (AxisMapping[] | null);

  /**
   * Optional transformation of axes.
   */
  axisTransformations?: (AxisTransformation[] | null);

}

export const TranslatorConfig = {
  fromBinary: (buffer: Uint8Array): TranslatorConfig => BSON.deserialize(buffer) as TranslatorConfig,
  toBinary: (value: TranslatorConfig): Uint8Array => BSON.serialize(TranslatorConfig.sanitize(value)),
  DEFAULT: Object.freeze({
    axisMappings: null,
    axisTransformations: null,
  }) as Readonly<TranslatorConfig>,
  sanitize: (value: TranslatorConfig): TranslatorConfig => {
    if (value == null) { throw new TypeError('Expected TranslatorConfig object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslatorConfig object but got ${typeof value}.`) }
    return {
      axisMappings: Array.from(value.axisMappings ?? [], item => AxisMapping.sanitize(item)),
      axisTransformations: Array.from(value.axisTransformations ?? [], item => AxisTransformation.sanitize(item)),
    };
  },
};
