/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { TranslateMessage } from '../gcode/translate_message';

/**
 * Represents a result of a G-code block translation.
 */
export interface TranslateResult {
  /**
   * Stream commands resulting from the block.
   */
  commands: string[];

  /**
   * Messages informing about unsupported codes and features.
   */
  warnings: TranslateMessage[];

}

export const TranslateResult = {
  fromBinary: (buffer: Uint8Array): TranslateResult => BSON.deserialize(buffer) as TranslateResult,
  toBinary: (value: TranslateResult): Uint8Array => BSON.serialize(TranslateResult.sanitize(value)),
  DEFAULT: Object.freeze({
    commands: [],
    warnings: [],
  }) as Readonly<TranslateResult>,
  sanitize: (value: TranslateResult): TranslateResult => {
    if (value == null) { throw new TypeError('Expected TranslateResult object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected TranslateResult object but got ${typeof value}.`) }
    return {
      commands: Array.from(value.commands ?? [], item => sanitizer.sanitizeString(item, 'items of commands')),
      warnings: Array.from(value.warnings ?? [], item => TranslateMessage.sanitize(item)),
    };
  },
};
