/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Maps a translator axis to a Zaber stream axis.
 */
export interface AxisMapping {
  /**
   * Letter of the translator axis (X,Y,Z,A,B,C,E).
   */
  axisLetter: string;

  /**
   * Index of the stream axis.
   */
  axisIndex: number;

}

export const AxisMapping = {
  fromBinary: (buffer: Uint8Array): AxisMapping => BSON.deserialize(buffer) as AxisMapping,
  toBinary: (value: AxisMapping): Uint8Array => BSON.serialize(AxisMapping.sanitize(value)),
  DEFAULT: Object.freeze({
    axisLetter: '',
    axisIndex: 0,
  }) as Readonly<AxisMapping>,
  sanitize: (value: AxisMapping): AxisMapping => {
    if (value == null) { throw new TypeError('Expected AxisMapping object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AxisMapping object but got ${typeof value}.`) }
    return {
      axisLetter: sanitizer.sanitizeString(value.axisLetter, 'axisLetter'),
      axisIndex: sanitizer.sanitizeInt(value.axisIndex, 'axisIndex'),
    };
  },
};
