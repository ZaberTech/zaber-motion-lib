/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { ProcessControllerSourceSensor } from '../product/process_controller_source_sensor';

/**
 * The source used by a process in a closed-loop mode.
 */
export interface ProcessControllerSource {
  /**
   * The type of input sensor.
   */
  sensor: ProcessControllerSourceSensor;

  /**
   * The specific input to use.
   */
  port: number;

}

export const ProcessControllerSource = {
  fromBinary: (buffer: Uint8Array): ProcessControllerSource => BSON.deserialize(buffer) as ProcessControllerSource,
  toBinary: (value: ProcessControllerSource): Uint8Array => BSON.serialize(ProcessControllerSource.sanitize(value)),
  DEFAULT: Object.freeze({
    sensor: 0 as ProcessControllerSourceSensor,
    port: 0,
  }) as Readonly<ProcessControllerSource>,
  sanitize: (value: ProcessControllerSource): ProcessControllerSource => {
    if (value == null) { throw new TypeError('Expected ProcessControllerSource object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ProcessControllerSource object but got ${typeof value}.`) }
    return {
      sensor: sanitizer.sanitizeEnum(value.sensor, 'ProcessControllerSourceSensor', ProcessControllerSourceSensor, 'sensor'),
      port: sanitizer.sanitizeInt(value.port, 'port'),
    };
  },
};
