/* This file is generated. Do not modify by hand. */
/**
 * Servo Tuning Parameter Set to target.
 */
export enum ProcessControllerSourceSensor {
  /** Thermistor. */
  THERMISTOR = 10,
  /** AnalogInput. */
  ANALOG_INPUT = 20,
}
