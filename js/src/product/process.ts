﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { ProcessController } from './process_controller';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Native, Units, Time } from '../units';
import { Axis } from '../ascii/axis';
import { Response } from '../ascii/response';
import { AxisSettings } from '../ascii/axis_settings';
import { AxisStorage } from '../ascii/storage';
import { Warnings } from '../ascii/warnings';
import { ProcessControllerMode } from './process_controller_mode';
import { ProcessControllerSource } from './process_controller_source';
import { Measurement } from '../measurement';
import { SetStateAxisResponse } from '../ascii';
import { FirmwareVersion } from '../firmware_version';

/**
 * Use to drive voltage for a process such as a heater, valve, Peltier device, etc.
 * Requires at least Firmware 7.35.
 */
export class Process {
  /**
   * Controller for this process.
   */
  public get controller(): ProcessController {
    return this._controller;
  }
  private _controller: ProcessController;

  /**
   * The process number identifies the process on the controller.
   */
  public get processNumber(): number {
    return this._processNumber;
  }
  private _processNumber: number;

  private _axis: Axis;

  /**
   * Settings and properties of this process.
   */
  public get settings(): AxisSettings {
    return this._settings;
  }
  private _settings: AxisSettings;

  /**
   * Key-value storage of this process.
   */
  public get storage(): AxisStorage {
    return this._storage;
  }
  private _storage: AxisStorage;

  /**
   * Warnings and faults of this process.
   */
  public get warnings(): Warnings {
    return this._warnings;
  }
  private _warnings: Warnings;

  constructor(controller: ProcessController, processNumber: number) {
    this._controller = controller;
    this._processNumber = processNumber;
    this._axis = new Axis(controller.device, processNumber);
    this._settings = new AxisSettings(this._axis);
    this._storage = new AxisStorage(this._axis);
    this._warnings = new Warnings(controller.device, processNumber);
  }

  /**
   * Sets the enabled state of the driver.
   * @param [enabled=true] If true (default) enables drive. If false disables.
   */
  public async enable(
    enabled: boolean = true
  ): Promise<void> {
    const request: requests.ProcessOn & gateway.Message = {
      ...requests.ProcessOn.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      on: enabled,
      toBinary() {
        return requests.ProcessOn.toBinary(this);
      },
    };

    await gateway.callAsync('process-controller/enable', request);
  }

  /**
   * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
   * @param [duration=0] How long to leave the process on.
   * @param [unit=Units.NATIVE] Units of time.
   */
  public async on(
    duration: number = 0,
    unit: Time | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.ProcessOn & gateway.Message = {
      ...requests.ProcessOn.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      on: true,
      duration: duration,
      unit: unit,
      toBinary() {
        return requests.ProcessOn.toBinary(this);
      },
    };

    await gateway.callAsync('process-controller/on', request);
  }

  /**
   * Turns this process off.
   */
  public async off(): Promise<void> {
    const request: requests.ProcessOn & gateway.Message = {
      ...requests.ProcessOn.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      on: false,
      toBinary() {
        return requests.ProcessOn.toBinary(this);
      },
    };

    await gateway.callAsync('process-controller/on', request);
  }

  /**
   * Sets the control mode of this process.
   * @param mode Mode to set this process to.
   */
  public async setMode(
    mode: ProcessControllerMode
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      setting: 'process.control.mode',
      value: mode,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Gets the control mode of this process.
   * @returns Control mode.
   */
  public async getMode(): Promise<ProcessControllerMode> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      setting: 'process.control.mode',
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets the source used to control this process.
   * @returns The source providing feedback for this process.
   */
  public async getSource(): Promise<ProcessControllerSource> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<ProcessControllerSource>(
      'process_controller/get_source',
      request,
      ProcessControllerSource.fromBinary);
    return response;
  }

  /**
   * Sets the source used to control this process.
   * @param source Sets the source that should provide feedback for this process.
   */
  public async setSource(
    source: ProcessControllerSource
  ): Promise<void> {
    const request: requests.SetProcessControllerSource & gateway.Message = {
      ...requests.SetProcessControllerSource.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      source: source,
      toBinary() {
        return requests.SetProcessControllerSource.toBinary(this);
      },
    };

    await gateway.callAsync('process_controller/set_source', request);
  }

  /**
   * Gets the current value of the source used to control this process.
   * @returns The current value of this process's controlling source.
   */
  public async getInput(): Promise<Measurement> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Measurement>(
      'process_controller/get_input',
      request,
      Measurement.fromBinary);
    return response;
  }

  /**
   * Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
   */
  public async bridge(): Promise<void> {
    const request: requests.ProcessOn & gateway.Message = {
      ...requests.ProcessOn.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      on: true,
      toBinary() {
        return requests.ProcessOn.toBinary(this);
      },
    };

    await gateway.callAsync('process_controller/bridge', request);
  }

  /**
   * Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
   * This method is only callable on axis 1 and 3.
   */
  public async unbridge(): Promise<void> {
    const request: requests.ProcessOn & gateway.Message = {
      ...requests.ProcessOn.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      on: false,
      toBinary() {
        return requests.ProcessOn.toBinary(this);
      },
    };

    await gateway.callAsync('process_controller/bridge', request);
  }

  /**
   * Detects if the given process is in bridging mode.
   * @returns Whether this process is bridged with its neighbor.
   */
  public async isBridge(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'process_controller/is_bridge',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Sends a generic ASCII command to this process' underlying axis.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: string,
    options: Process.GenericCommandOptions = {}
  ): Promise<Response> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Response>(
      'interface/generic_command',
      request,
      Response.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this process and expect multiple responses.
   * Responses are returned in order of arrival.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when a device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: string,
    options: Process.GenericCommandMultiResponseOptions = {}
  ): Promise<Response[]> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GenericCommandResponseCollection>(
      'interface/generic_command_multi_response',
      request,
      requests.GenericCommandResponseCollection.fromBinary);
    return response.responses;
  }

  /**
   * Sends a generic ASCII command to this process without expecting a response and without adding a message ID
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   */
  public async genericCommandNoResponse(
    command: string
  ): Promise<void> {
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      command: command,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Returns a serialization of the current process state that can be saved and reapplied.
   * @returns A serialization of the current state of the process.
   */
  public async getState(): Promise<string> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_state',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Applies a saved state to this process.
   * @param state The state object to apply to this process.
   * @returns Reports of any issues that were handled, but caused the state to not be exactly restored.
   */
  public async setState(
    state: string
  ): Promise<SetStateAxisResponse> {
    const request: requests.SetStateRequest & gateway.Message = {
      ...requests.SetStateRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      state: state,
      toBinary() {
        return requests.SetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<SetStateAxisResponse>(
      'device/set_axis_state',
      request,
      SetStateAxisResponse.fromBinary);
    return response;
  }

  /**
   * Checks if a state can be applied to this process.
   * This only covers exceptions that can be determined statically such as mismatches of ID or version,
   * the process of applying the state can still fail when running.
   * @param state The state object to check against.
   * @param options.firmwareVersion The firmware version of the device to apply the state to.
   * Use this to ensure the state will still be compatible after an update.
   * @returns An explanation of why this state cannot be set to this process.
   */
  public async canSetState(
    state: string,
    options: Process.CanSetStateOptions = {}
  ): Promise<string | null> {
    const {
      firmwareVersion,
    } = options;
    const request: requests.CanSetStateRequest & gateway.Message = {
      ...requests.CanSetStateRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      state: state,
      firmwareVersion: firmwareVersion,
      toBinary() {
        return requests.CanSetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.CanSetStateAxisResponse>(
      'device/can_set_axis_state',
      request,
      requests.CanSetStateAxisResponse.fromBinary);
    return response.error ?? null;
  }

  /**
   * Returns a string that represents the process.
   * @returns A string that represents the process.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.controller.device.connection.interfaceId,
      device: this.controller.device.deviceAddress,
      axis: this.processNumber,
      typeOverride: 'Process',
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/axis_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace Process {
  export interface GenericCommandOptions {
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Controls whether to throw an exception when a device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface CanSetStateOptions {
      /**
       * The firmware version of the device to apply the state to.
       * Use this to ensure the state will still be compatible after an update.
       */
      firmwareVersion?: FirmwareVersion;
  }
}
