/* This file is generated. Do not modify by hand. */
/**
 * Servo Tuning Parameter Set to target.
 */
export enum ProcessControllerMode {
  /** Manual. */
  MANUAL = 0,
  /** Pid. */
  PID = 1,
  /** PidHeater. */
  PID_HEATER = 2,
  /** OnOff. */
  ON_OFF = 3,
}
