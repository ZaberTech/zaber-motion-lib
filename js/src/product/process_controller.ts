﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import { Connection } from '../ascii/connection';
import { Device } from '../ascii/device';
import * as gateway from '../gateway';
import * as requests from '../requests';
import { Process } from './process';

/**
 * Use to manage a process controller.
 * Requires at least Firmware 7.35.
 */
export class ProcessController {
  /**
   * The base device of this process controller.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * Creates instance of `ProcessController` of the given device.
   * If the device is identified, this constructor will ensure it is a process controller.
   */
  constructor(device: Device) {
    this._device = device;
    this._verifyIsProcessController();
  }

  /**
   * Detects the process controllers on the connection.
   * @param connection The connection to detect process controllers on.
   * @param [identify=true] If the Process Controllers should be identified upon detection.
   * @returns A list of all `ProcessController`s on the connection.
   */
  public static async detect(
    connection: Connection,
    identify: boolean = true
  ): Promise<ProcessController[]> {
    const request: requests.DeviceDetectRequest & gateway.Message = {
      ...requests.DeviceDetectRequest.DEFAULT,
      type: requests.DeviceType.PROCESS_CONTROLLER,
      interfaceId: connection.interfaceId,
      identifyDevices: identify,
      toBinary() {
        return requests.DeviceDetectRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DeviceDetectResponse>(
      'device/detect',
      request,
      requests.DeviceDetectResponse.fromBinary);
    return response.devices.map(deviceAddress => new ProcessController(connection.getDevice(deviceAddress)));
  }

  /**
   * Gets an Process class instance which allows you to control a particular voltage source.
   * Axes are numbered from 1.
   * @param processNumber Number of process to control.
   * @returns Process instance.
   */
  public getProcess(
    processNumber: number
  ): Process {
    if (processNumber <= 0) {
      throw new TypeError('Invalid value; processes are numbered from 1.');
    }
    return new Process(this, processNumber);
  }

  /**
   * Checks if this is a process controller or some other type of device and throws an error if it is not.
   */
  private _verifyIsProcessController(): void {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('process_controller/verify', request);
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace ProcessController {
}
