/* This file is generated. Do not modify by hand. */
/**
 * Mode of logging output of the library.
 */
export enum LogOutputMode {
  /** Off. */
  OFF = 0,
  /** Stdout. */
  STDOUT = 1,
  /** Stderr. */
  STDERR = 2,
  /** File. */
  FILE = 3,
}
