﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { WdiAutofocusProviderStatus } from './wdi_autofocus_provider_status';

/**
 * Class representing access to WDI Autofocus connection.
 */
export class WdiAutofocusProvider {
  /**
   * Default port number for TCP connections to WDI autofocus.
   */
  public static readonly DEFAULT_TCP_PORT: number = 27;

  /**
   * The ID identifies the autofocus with the underlying library.
   */
  public get providerId(): number {
    return this._providerId;
  }
  private _providerId: number;

  constructor(providerId: number) {
    this._providerId = providerId;
  }

  /**
   * Opens a TCP connection to WDI autofocus.
   * @param hostName Hostname or IP address.
   * @param [port=WdiAutofocusProvider.DEFAULT_TCP_PORT] Optional port number (defaults to 27).
   * @returns An object representing the autofocus connection.
   */
  public static async openTcp(
    hostName: string,
    port: number = WdiAutofocusProvider.DEFAULT_TCP_PORT
  ): Promise<WdiAutofocusProvider> {
    const request: requests.OpenInterfaceRequest & gateway.Message = {
      ...requests.OpenInterfaceRequest.DEFAULT,
      interfaceType: requests.InterfaceType.TCP,
      hostName: hostName,
      port: port,
      toBinary() {
        return requests.OpenInterfaceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'wdi/open',
      request,
      requests.IntResponse.fromBinary);
    return new WdiAutofocusProvider(response.value);
  }

  /**
   * Close the connection.
   */
  public async close(): Promise<void> {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.providerId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    await gateway.callAsync('wdi/close', request);
  }

  /**
   * Generic read operation.
   * @param registerId Register address to read from.
   * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
   * @param [options.count=1] Number of values to read (defaults to 1).
   * @param [options.offset=0] Offset within the register (defaults to 0).
   * @param [options.registerBank='U'] Register bank letter (defaults to U for user bank).
   * @returns Array of integers read from the device.
   */
  public async genericRead(
    registerId: number,
    size: number,
    options: WdiAutofocusProvider.GenericReadOptions = {}
  ): Promise<number[]> {
    const {
      count = 1,
      offset = 0,
      registerBank = 'U',
    } = options;
    const request: requests.WdiGenericRequest & gateway.Message = {
      ...requests.WdiGenericRequest.DEFAULT,
      interfaceId: this.providerId,
      registerId: registerId,
      size: size,
      count: count,
      offset: offset,
      registerBank: registerBank,
      toBinary() {
        return requests.WdiGenericRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntArrayResponse>(
      'wdi/read',
      request,
      requests.IntArrayResponse.fromBinary);
    return response.values;
  }

  /**
   * Generic write operation.
   * @param registerId Register address to read from.
   * @param [size=0] Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
   * @param [data=[]] Array of values to write to the register. Empty array is allowed.
   * @param [options.offset=0] Offset within the register (defaults to 0).
   * @param [options.registerBank='U'] Register bank letter (defaults to U for user bank).
   */
  public async genericWrite(
    registerId: number,
    size: number = 0,
    data: number[] = [],
    options: WdiAutofocusProvider.GenericWriteOptions = {}
  ): Promise<void> {
    const {
      offset = 0,
      registerBank = 'U',
    } = options;
    const request: requests.WdiGenericRequest & gateway.Message = {
      ...requests.WdiGenericRequest.DEFAULT,
      interfaceId: this.providerId,
      registerId: registerId,
      size: size,
      data: data,
      offset: offset,
      registerBank: registerBank,
      toBinary() {
        return requests.WdiGenericRequest.toBinary(this);
      },
    };

    await gateway.callAsync('wdi/write', request);
  }

  /**
   * Enables the laser.
   */
  public async enableLaser(): Promise<void> {
    const request: requests.WdiGenericRequest & gateway.Message = {
      ...requests.WdiGenericRequest.DEFAULT,
      interfaceId: this.providerId,
      registerId: 1,
      toBinary() {
        return requests.WdiGenericRequest.toBinary(this);
      },
    };

    await gateway.callAsync('wdi/write', request);
  }

  /**
   * Disables the laser.
   */
  public async disableLaser(): Promise<void> {
    const request: requests.WdiGenericRequest & gateway.Message = {
      ...requests.WdiGenericRequest.DEFAULT,
      interfaceId: this.providerId,
      registerId: 2,
      toBinary() {
        return requests.WdiGenericRequest.toBinary(this);
      },
    };

    await gateway.callAsync('wdi/write', request);
  }

  /**
   * Gets the status of the autofocus.
   * @returns The status of the autofocus.
   */
  public async getStatus(): Promise<WdiAutofocusProviderStatus> {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.providerId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.WdiGetStatusResponse>(
      'wdi/get_status',
      request,
      requests.WdiGetStatusResponse.fromBinary);
    return response.status;
  }

  /**
   * Returns a string that represents the autofocus connection.
   * @returns A string that represents the connection.
   */
  public toString(): string {
    const request: requests.InterfaceEmptyRequest & gateway.Message = {
      ...requests.InterfaceEmptyRequest.DEFAULT,
      interfaceId: this.providerId,
      toBinary() {
        return requests.InterfaceEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'wdi/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace WdiAutofocusProvider {
  export interface GenericReadOptions {
      /**
       * Number of values to read (defaults to 1).
       */
      count?: number;
      /**
       * Offset within the register (defaults to 0).
       */
      offset?: number;
      /**
       * Register bank letter (defaults to U for user bank).
       */
      registerBank?: string;
  }
  export interface GenericWriteOptions {
      /**
       * Offset within the register (defaults to 0).
       */
      offset?: number;
      /**
       * Register bank letter (defaults to U for user bank).
       */
      registerBank?: string;
  }
}
