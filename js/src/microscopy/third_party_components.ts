/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Third party components of the microscope.
 */
export interface ThirdPartyComponents {
  /**
   * Autofocus provider identifier.
   */
  autofocus?: (number | null);

}

export const ThirdPartyComponents = {
  fromBinary: (buffer: Uint8Array): ThirdPartyComponents => BSON.deserialize(buffer) as ThirdPartyComponents,
  toBinary: (value: ThirdPartyComponents): Uint8Array => BSON.serialize(ThirdPartyComponents.sanitize(value)),
  DEFAULT: Object.freeze({
    autofocus: null,
  }) as Readonly<ThirdPartyComponents>,
  sanitize: (value: ThirdPartyComponents): ThirdPartyComponents => {
    if (value == null) { throw new TypeError('Expected ThirdPartyComponents object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ThirdPartyComponents object but got ${typeof value}.`) }
    return {
      autofocus: value.autofocus != null ? sanitizer.sanitizeInt(value.autofocus, 'autofocus') : null,
    };
  },
};
