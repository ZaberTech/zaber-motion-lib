﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Device } from '../ascii/device';

/**
 * A generic turret device.
 */
export class FilterChanger {
  /**
   * The base device of this turret.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * Creates instance of `FilterChanger` based on the given device.
   */
  constructor(device: Device) {
    this._device = device;
  }

  /**
   * Gets number of filters of the changer.
   * @returns Number of positions.
   */
  public async getNumberOfFilters(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 1,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_count',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns the current filter number starting from 1.
   * The value of 0 indicates that the position is either unknown or between two filters.
   * @returns Filter number starting from 1 or 0 if the position cannot be determined.
   */
  public async getCurrentFilter(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 1,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_position',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Changes to the specified filter.
   * @param filter Filter number starting from 1.
   */
  public async change(
    filter: number
  ): Promise<void> {
    const request: requests.DeviceMoveRequest & gateway.Message = {
      ...requests.DeviceMoveRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      axis: 1,
      type: requests.AxisMoveType.INDEX,
      waitUntilIdle: true,
      argInt: filter,
      toBinary() {
        return requests.DeviceMoveRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/move', request);
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace FilterChanger {
}
