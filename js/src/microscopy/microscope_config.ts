/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';
import { AxisAddress } from '../axis_address';
import { ChannelAddress } from '../channel_address';

/**
 * Configuration representing a microscope setup.
 * Device address of value 0 means that the part is not present.
 */
export interface MicroscopeConfig {
  /**
   * Focus axis of the microscope.
   */
  focusAxis?: (AxisAddress | null);

  /**
   * X axis of the microscope.
   */
  xAxis?: (AxisAddress | null);

  /**
   * Y axis of the microscope.
   */
  yAxis?: (AxisAddress | null);

  /**
   * Illuminator device address.
   */
  illuminator?: (number | null);

  /**
   * Filter changer device address.
   */
  filterChanger?: (number | null);

  /**
   * Objective changer device address.
   */
  objectiveChanger?: (number | null);

  /**
   * Autofocus identifier.
   */
  autofocus?: (number | null);

  /**
   * Camera trigger digital output address.
   */
  cameraTrigger?: (ChannelAddress | null);

}

export const MicroscopeConfig = {
  fromBinary: (buffer: Uint8Array): MicroscopeConfig => BSON.deserialize(buffer) as MicroscopeConfig,
  toBinary: (value: MicroscopeConfig): Uint8Array => BSON.serialize(MicroscopeConfig.sanitize(value)),
  DEFAULT: Object.freeze({
    focusAxis: null,
    xAxis: null,
    yAxis: null,
    illuminator: null,
    filterChanger: null,
    objectiveChanger: null,
    autofocus: null,
    cameraTrigger: null,
  }) as Readonly<MicroscopeConfig>,
  sanitize: (value: MicroscopeConfig): MicroscopeConfig => {
    if (value == null) { throw new TypeError('Expected MicroscopeConfig object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected MicroscopeConfig object but got ${typeof value}.`) }
    return {
      focusAxis: value.focusAxis != null ? AxisAddress.sanitize(value.focusAxis) : null,
      xAxis: value.xAxis != null ? AxisAddress.sanitize(value.xAxis) : null,
      yAxis: value.yAxis != null ? AxisAddress.sanitize(value.yAxis) : null,
      illuminator: value.illuminator != null ? sanitizer.sanitizeInt(value.illuminator, 'illuminator') : null,
      filterChanger: value.filterChanger != null ? sanitizer.sanitizeInt(value.filterChanger, 'filterChanger') : null,
      objectiveChanger: value.objectiveChanger != null ? sanitizer.sanitizeInt(value.objectiveChanger, 'objectiveChanger') : null,
      autofocus: value.autofocus != null ? sanitizer.sanitizeInt(value.autofocus, 'autofocus') : null,
      cameraTrigger: value.cameraTrigger != null ? ChannelAddress.sanitize(value.cameraTrigger) : null,
    };
  },
};
