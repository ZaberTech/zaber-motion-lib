﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Device } from '../ascii/device';
import { Axis } from '../ascii/axis';
import { Angle, Length, Native, Units } from '../units';
import { NamedParameter } from '../named_parameter';
import { AutofocusStatus } from './autofocus_status';


/**
 * A generic autofocus device.
 */
export class Autofocus {
  /**
   * The identification of external device providing the capability.
   */
  public get providerId(): number {
    return this._providerId;
  }
  private _providerId: number;

  /**
   * The focus axis.
   */
  public get focusAxis(): Axis {
    return this._focusAxis;
  }
  private _focusAxis: Axis;

  /**
   * The objective turret device if the microscope has one.
   */
  public get objectiveTurret(): Device | undefined {
    return this._objectiveTurret;
  }
  private _objectiveTurret: Device | undefined;

  /**
   * Creates instance of `Autofocus` based on the given provider id.
   */
  constructor(providerId: number, focusAxis: Axis, objectiveTurret?: Device) {
    this._providerId = providerId;
    this._focusAxis = focusAxis;
    this._objectiveTurret = objectiveTurret;
  }

  /**
   * Sets the current focus to be target for the autofocus control loop.
   */
  public async setFocusZero(): Promise<void> {
    const request: requests.EmptyAutofocusRequest & gateway.Message = {
      ...requests.EmptyAutofocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.EmptyAutofocusRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/set_zero', request);
  }

  /**
   * Returns the status of the autofocus.
   * @returns The status of the autofocus.
   */
  public async getStatus(): Promise<AutofocusStatus> {
    const request: requests.EmptyAutofocusRequest & gateway.Message = {
      ...requests.EmptyAutofocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.EmptyAutofocusRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.AutofocusGetStatusResponse>(
      'autofocus/get_status',
      request,
      requests.AutofocusGetStatusResponse.fromBinary);
    return response.status;
  }

  /**
   * Moves the device until it's in focus.
   * @param [scan=false] If true, the autofocus will approach from the limit moving until it's in range.
   * @param [timeout=-1] Sets autofocus timeout duration in milliseconds.
   */
  public async focusOnce(
    scan: boolean = false,
    timeout: number = -1
  ): Promise<void> {
    const request: requests.AutofocusFocusRequest & gateway.Message = {
      ...requests.AutofocusFocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      once: true,
      scan: scan,
      timeout: timeout,
      toBinary() {
        return requests.AutofocusFocusRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/focus_once', request);
  }

  /**
   * Moves the focus axis continuously maintaining focus.
   * Starts the autofocus control loop.
   * Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
   */
  public async startFocusLoop(): Promise<void> {
    const request: requests.AutofocusFocusRequest & gateway.Message = {
      ...requests.AutofocusFocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.AutofocusFocusRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/start_focus_loop', request);
  }

  /**
   * Stops autofocus control loop.
   * If the focus axis already stopped moving because of an error, an exception will be thrown.
   */
  public async stopFocusLoop(): Promise<void> {
    const request: requests.EmptyAutofocusRequest & gateway.Message = {
      ...requests.EmptyAutofocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.EmptyAutofocusRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/stop_focus_loop', request);
  }

  /**
   * Gets the lower motion limit for the autofocus control loop.
   * Gets motion.tracking.limit.min setting of the focus axis.
   * @param [unit=Units.NATIVE] The units of the limit.
   * @returns Limit value.
   */
  public async getLimitMin(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      device: this.focusAxis.device.deviceAddress,
      axis: this.focusAxis.axisNumber,
      setting: 'motion.tracking.limit.min',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets the upper motion limit for the autofocus control loop.
   * Gets motion.tracking.limit.max setting of the focus axis.
   * @param [unit=Units.NATIVE] The units of the limit.
   * @returns Limit value.
   */
  public async getLimitMax(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.DeviceGetSettingRequest & gateway.Message = {
      ...requests.DeviceGetSettingRequest.DEFAULT,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      device: this.focusAxis.device.deviceAddress,
      axis: this.focusAxis.axisNumber,
      setting: 'motion.tracking.limit.max',
      unit: unit,
      toBinary() {
        return requests.DeviceGetSettingRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'device/get_setting',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the lower motion limit for the autofocus control loop.
   * Use the limits to prevent the focus axis from crashing into the sample.
   * Changes motion.tracking.limit.min setting of the focus axis.
   * @param limit The lower limit of the focus axis.
   * @param [unit=Units.NATIVE] The units of the limit.
   */
  public async setLimitMin(
    limit: number,
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      device: this.focusAxis.device.deviceAddress,
      axis: this.focusAxis.axisNumber,
      setting: 'motion.tracking.limit.min',
      value: limit,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Sets the upper motion limit for the autofocus control loop.
   * Use the limits to prevent the focus axis from crashing into the sample.
   * Changes motion.tracking.limit.max setting of the focus axis.
   * @param limit The upper limit of the focus axis.
   * @param [unit=Units.NATIVE] The units of the limit.
   */
  public async setLimitMax(
    limit: number,
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.DeviceSetSettingRequest & gateway.Message = {
      ...requests.DeviceSetSettingRequest.DEFAULT,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      device: this.focusAxis.device.deviceAddress,
      axis: this.focusAxis.axisNumber,
      setting: 'motion.tracking.limit.max',
      value: limit,
      unit: unit,
      toBinary() {
        return requests.DeviceSetSettingRequest.toBinary(this);
      },
    };

    await gateway.callAsync('device/set_setting', request);
  }

  /**
   * Typically, the control loop parameters and objective are kept synchronized by the library.
   * If the parameters or current objective changes outside of the library, call this method to synchronize them.
   */
  public async synchronizeParameters(): Promise<void> {
    const request: requests.EmptyAutofocusRequest & gateway.Message = {
      ...requests.EmptyAutofocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.EmptyAutofocusRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/sync_params', request);
  }

  /**
   * Sets the parameters for the autofocus objective.
   * Note that the method temporarily switches current objective to set the parameters.
   * @param objective The objective (numbered from 1) to set the parameters for.
   * If your microscope has only one objective, use value of 1.
   * @param parameters The parameters for the autofocus objective.
   */
  public async setObjectiveParameters(
    objective: number,
    parameters: NamedParameter[]
  ): Promise<void> {
    const request: requests.AutofocusSetObjectiveParamsRequest & gateway.Message = {
      ...requests.AutofocusSetObjectiveParamsRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      objective: objective,
      parameters: parameters,
      toBinary() {
        return requests.AutofocusSetObjectiveParamsRequest.toBinary(this);
      },
    };

    await gateway.callAsync('autofocus/set_objective_params', request);
  }

  /**
   * Returns the parameters for the autofocus objective.
   * @param objective The objective (numbered from 1) to get the parameters for.
   * If your microscope has only one objective, use value of 1.
   * Note that the method temporarily switches current objective to get the parameters.
   * @returns The parameters for the autofocus objective.
   */
  public async getObjectiveParameters(
    objective: number
  ): Promise<NamedParameter[]> {
    const request: requests.AutofocusGetObjectiveParamsRequest & gateway.Message = {
      ...requests.AutofocusGetObjectiveParamsRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      objective: objective,
      toBinary() {
        return requests.AutofocusGetObjectiveParamsRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.AutofocusGetObjectiveParamsResponse>(
      'autofocus/get_objective_params',
      request,
      requests.AutofocusGetObjectiveParamsResponse.fromBinary);
    return response.parameters;
  }

  /**
   * Returns a string that represents the autofocus.
   * @returns A string that represents the autofocus.
   */
  public toString(): string {
    const request: requests.EmptyAutofocusRequest & gateway.Message = {
      ...requests.EmptyAutofocusRequest.DEFAULT,
      providerId: this.providerId,
      interfaceId: this.focusAxis.device.connection.interfaceId,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      turretAddress: this.objectiveTurret?.deviceAddress ?? 0,
      toBinary() {
        return requests.EmptyAutofocusRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'autofocus/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace Autofocus {
}
