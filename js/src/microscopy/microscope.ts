﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Axis } from '../ascii/axis';
import { AxisGroup } from '../ascii/axis_group';
import { Device } from '../ascii/device';
import { Connection } from '../ascii/connection';
import { Illuminator } from './illuminator';
import { ObjectiveChanger } from './objective_changer';
import { MicroscopeConfig } from './microscope_config';
import { FilterChanger } from './filter_changer';
import { Autofocus } from './autofocus';
import { CameraTrigger } from './camera_trigger';
import { ThirdPartyComponents } from './third_party_components';

/**
 * Represent a microscope.
 * Parts of the microscope may or may not be instantiated depending on the configuration.
 * Requires at least Firmware 7.34.
 */
export class Microscope {
  /**
   * Connection of the microscope.
   */
  public get connection(): Connection {
    return this._connection;
  }
  private _connection: Connection;

  private _config: MicroscopeConfig;

  /**
   * The illuminator.
   */
  public get illuminator(): Illuminator | undefined {
    return this._illuminator;
  }
  private _illuminator: Illuminator | undefined;

  /**
   * The focus axis.
   */
  public get focusAxis(): Axis | undefined {
    return this._focusAxis;
  }
  private _focusAxis: Axis | undefined;

  /**
   * The X axis.
   */
  public get xAxis(): Axis | undefined {
    return this._xAxis;
  }
  private _xAxis: Axis | undefined;

  /**
   * The Y axis.
   */
  public get yAxis(): Axis | undefined {
    return this._yAxis;
  }
  private _yAxis: Axis | undefined;

  /**
   * Axis group consisting of X and Y axes representing the plate of the microscope.
   */
  public get plate(): AxisGroup | undefined {
    return this._plate;
  }
  private _plate: AxisGroup | undefined;

  /**
   * The objective changer.
   */
  public get objectiveChanger(): ObjectiveChanger | undefined {
    return this._objectiveChanger;
  }
  private _objectiveChanger: ObjectiveChanger | undefined;

  /**
   * The filter changer.
   */
  public get filterChanger(): FilterChanger | undefined {
    return this._filterChanger;
  }
  private _filterChanger: FilterChanger | undefined;

  /**
   * The autofocus feature.
   */
  public get autofocus(): Autofocus | undefined {
    return this._autofocus;
  }
  private _autofocus: Autofocus | undefined;

  /**
   * The camera trigger.
   */
  public get cameraTrigger(): CameraTrigger | undefined {
    return this._cameraTrigger;
  }
  private _cameraTrigger: CameraTrigger | undefined;

  /**
   * Creates instance of `Microscope` from the given config.
   * Parts are instantiated depending on device addresses in the config.
   */
  constructor(connection: Connection, config: MicroscopeConfig) {
    this._connection = connection;
    this._config = MicroscopeConfig.fromBinary(MicroscopeConfig.toBinary(config));
    this._illuminator = config.illuminator ? new Illuminator(new Device(connection, config.illuminator)) : undefined;
    this._focusAxis = config.focusAxis?.device ? new Axis(new Device(connection, config.focusAxis.device), config.focusAxis.axis) : undefined;
    this._xAxis = config.xAxis?.device ? new Axis(new Device(connection, config.xAxis.device), config.xAxis.axis) : undefined;
    this._yAxis = config.yAxis?.device ? new Axis(new Device(connection, config.yAxis.device), config.yAxis.axis) : undefined;
    this._plate = this._xAxis && this._yAxis ? new AxisGroup([this._xAxis, this._yAxis]) : undefined;
    this._objectiveChanger = config.objectiveChanger && this._focusAxis ? new ObjectiveChanger(new Device(connection, config.objectiveChanger), this._focusAxis) : undefined;
    this._filterChanger = config.filterChanger ? new FilterChanger(new Device(connection, config.filterChanger)) : undefined;
    this._autofocus = config.autofocus && this._focusAxis ? new Autofocus(config.autofocus, this._focusAxis, this._objectiveChanger?.turret) : undefined;
    this._cameraTrigger = config.cameraTrigger?.device ? new CameraTrigger(new Device(connection, config.cameraTrigger.device), config.cameraTrigger.channel) : undefined;
  }

  /**
   * Finds a microscope on a connection.
   * @param connection Connection on which to detect the microscope.
   * @param thirdPartyComponents Third party components of the microscope that cannot be found on the connection.
   * @returns New instance of microscope.
   */
  public static async find(
    connection: Connection,
    thirdPartyComponents?: ThirdPartyComponents
  ): Promise<Microscope> {
    const request: requests.MicroscopeFindRequest & gateway.Message = {
      ...requests.MicroscopeFindRequest.DEFAULT,
      interfaceId: connection.interfaceId,
      thirdParty: thirdPartyComponents,
      toBinary() {
        return requests.MicroscopeFindRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.MicroscopeConfigResponse>(
      'microscope/detect',
      request,
      requests.MicroscopeConfigResponse.fromBinary);
    return new Microscope(connection, response.config);
  }

  /**
   * Initializes the microscope.
   * Homes all axes, filter changer, and objective changer if they require it.
   * @param [options.force=false] Forces all devices to home even when not required.
   */
  public async initialize(
    options: Microscope.InitializeOptions = {}
  ): Promise<void> {
    const {
      force = false,
    } = options;
    const request: requests.MicroscopeInitRequest & gateway.Message = {
      ...requests.MicroscopeInitRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      config: this._config,
      force: force,
      toBinary() {
        return requests.MicroscopeInitRequest.toBinary(this);
      },
    };

    await gateway.callAsync('microscope/initialize', request);
  }

  /**
   * Checks whether the microscope is initialized.
   * @returns True, when the microscope is initialized. False, otherwise.
   */
  public async isInitialized(): Promise<boolean> {
    const request: requests.MicroscopeEmptyRequest & gateway.Message = {
      ...requests.MicroscopeEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      config: this._config,
      toBinary() {
        return requests.MicroscopeEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'microscope/is_initialized',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Returns a string that represents the microscope.
   * @returns A string that represents the microscope.
   */
  public toString(): string {
    const request: requests.MicroscopeEmptyRequest & gateway.Message = {
      ...requests.MicroscopeEmptyRequest.DEFAULT,
      interfaceId: this.connection.interfaceId,
      config: this._config,
      toBinary() {
        return requests.MicroscopeEmptyRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'microscope/to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace Microscope {
  export interface InitializeOptions {
      /**
       * Forces all devices to home even when not required.
       */
      force?: boolean;
  }
}
