﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Time, Native, Units } from '../units';
import { Device } from '../ascii/device';

/**
 * An abstraction over a device and it's digital output channel.
 */
export class CameraTrigger {
  /**
   * The device whose digital output triggers the camera.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * The digital output channel that triggers the camera.
   */
  public get channel(): number {
    return this._channel;
  }
  private _channel: number;

  /**
   * Creates instance of `CameraTrigger` based on the given device and digital output channel.
   */
  constructor(device: Device, channel: number) {
    this._device = device;
    this._channel = channel;
  }

  /**
   * Triggers the camera.
   * Schedules trigger pulse on the digital output channel.
   * By default, the method waits until the trigger pulse is finished.
   * @param pulseWidth The time duration of the trigger pulse.
   * Depending on the camera setting, the argument can be use to specify exposure.
   * @param [unit=Units.NATIVE] Units of time.
   * @param [options.wait=true] If false, the method does not wait until the trigger pulse is finished.
   */
  public async trigger(
    pulseWidth: number,
    unit: Time | Native = Units.NATIVE,
    options: CameraTrigger.TriggerOptions = {}
  ): Promise<void> {
    const {
      wait = true,
    } = options;
    const request: requests.MicroscopeTriggerCameraRequest & gateway.Message = {
      ...requests.MicroscopeTriggerCameraRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      channelNumber: this.channel,
      delay: pulseWidth,
      unit: unit,
      wait: wait,
      toBinary() {
        return requests.MicroscopeTriggerCameraRequest.toBinary(this);
      },
    };

    await gateway.callAsync('microscope/trigger_camera', request);
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace CameraTrigger {
  export interface TriggerOptions {
      /**
       * If false, the method does not wait until the trigger pulse is finished.
       */
      wait?: boolean;
  }
}
