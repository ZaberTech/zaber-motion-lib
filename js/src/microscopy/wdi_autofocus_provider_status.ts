/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Status of the WDI autofocus.
 */
export interface WdiAutofocusProviderStatus {
  /**
   * Indicates whether the autofocus is in range.
   */
  inRange: boolean;

  /**
   * Indicates whether the laser is turned on.
   */
  laserOn: boolean;

}

export const WdiAutofocusProviderStatus = {
  fromBinary: (buffer: Uint8Array): WdiAutofocusProviderStatus => BSON.deserialize(buffer) as WdiAutofocusProviderStatus,
  toBinary: (value: WdiAutofocusProviderStatus): Uint8Array => BSON.serialize(WdiAutofocusProviderStatus.sanitize(value)),
  DEFAULT: Object.freeze({
    inRange: false,
    laserOn: false,
  }) as Readonly<WdiAutofocusProviderStatus>,
  sanitize: (value: WdiAutofocusProviderStatus): WdiAutofocusProviderStatus => {
    if (value == null) { throw new TypeError('Expected WdiAutofocusProviderStatus object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected WdiAutofocusProviderStatus object but got ${typeof value}.`) }
    return {
      inRange: sanitizer.sanitizeBool(value.inRange, 'inRange'),
      laserOn: sanitizer.sanitizeBool(value.laserOn, 'laserOn'),
    };
  },
};
