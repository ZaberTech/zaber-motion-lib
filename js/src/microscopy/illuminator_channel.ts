﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Axis } from '../ascii/axis';
import { AxisStorage } from '../ascii/storage';
import { AxisSettings } from '../ascii/axis_settings';
import { Warnings } from '../ascii/warnings';
import { Response } from '../ascii/response';
import type { Illuminator } from './illuminator';
import { SetStateAxisResponse } from '../ascii';
import { FirmwareVersion } from '../firmware_version';

/**
 * Use to control a channel (LED lamp) on an illuminator.
 * Requires at least Firmware 7.09.
 */
export class IlluminatorChannel {
  /**
   * Illuminator of this channel.
   */
  public get illuminator(): Illuminator {
    return this._illuminator;
  }
  private _illuminator: Illuminator;

  /**
   * The channel number identifies the channel on the illuminator.
   */
  public get channelNumber(): number {
    return this._channelNumber;
  }
  private _channelNumber: number;

  private _axis: Axis;

  /**
   * Settings and properties of this channel.
   */
  public get settings(): AxisSettings {
    return this._settings;
  }
  private _settings: AxisSettings;

  /**
   * Key-value storage of this channel.
   */
  public get storage(): AxisStorage {
    return this._storage;
  }
  private _storage: AxisStorage;

  /**
   * Warnings and faults of this channel.
   */
  public get warnings(): Warnings {
    return this._warnings;
  }
  private _warnings: Warnings;

  constructor(illuminator: Illuminator, channelNumber: number) {
    this._illuminator = illuminator;
    this._channelNumber = channelNumber;
    this._axis = new Axis(illuminator.device, channelNumber);
    this._settings = new AxisSettings(this._axis);
    this._storage = new AxisStorage(this._axis);
    this._warnings = new Warnings(illuminator.device, channelNumber);
  }

  /**
   * Turns this channel on.
   */
  public async on(): Promise<void> {
    const request: requests.ChannelOn & gateway.Message = {
      ...requests.ChannelOn.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      on: true,
      toBinary() {
        return requests.ChannelOn.toBinary(this);
      },
    };

    await gateway.callAsync('illuminator/on', request);
  }

  /**
   * Turns this channel off.
   */
  public async off(): Promise<void> {
    const request: requests.ChannelOn & gateway.Message = {
      ...requests.ChannelOn.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      on: false,
      toBinary() {
        return requests.ChannelOn.toBinary(this);
      },
    };

    await gateway.callAsync('illuminator/on', request);
  }

  /**
   * Turns this channel on or off.
   * @param on True to turn channel on, false to turn it off.
   */
  public async setOn(
    on: boolean
  ): Promise<void> {
    const request: requests.ChannelOn & gateway.Message = {
      ...requests.ChannelOn.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      on: on,
      toBinary() {
        return requests.ChannelOn.toBinary(this);
      },
    };

    await gateway.callAsync('illuminator/on', request);
  }

  /**
   * Checks if this channel is on.
   * @returns True if channel is on, false otherwise.
   */
  public async isOn(): Promise<boolean> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.BoolResponse>(
      'illuminator/is_on',
      request,
      requests.BoolResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets channel intensity as a fraction of the maximum flux.
   * @param intensity Fraction of intensity to set (between 0 and 1).
   */
  public async setIntensity(
    intensity: number
  ): Promise<void> {
    const request: requests.ChannelSetIntensity & gateway.Message = {
      ...requests.ChannelSetIntensity.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      intensity: intensity,
      toBinary() {
        return requests.ChannelSetIntensity.toBinary(this);
      },
    };

    await gateway.callAsync('illuminator/set_intensity', request);
  }

  /**
   * Gets the current intensity of this channel.
   * @returns Current intensity as fraction of maximum flux.
   */
  public async getIntensity(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'illuminator/get_intensity',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sends a generic ASCII command to this channel.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when the device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns A response to the command.
   */
  public async genericCommand(
    command: string,
    options: IlluminatorChannel.GenericCommandOptions = {}
  ): Promise<Response> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<Response>(
      'interface/generic_command',
      request,
      Response.fromBinary);
    return response;
  }

  /**
   * Sends a generic ASCII command to this channel and expects multiple responses.
   * Responses are returned in order of arrival.
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   * @param [options.checkErrors=true] Controls whether to throw an exception when a device rejects the command.
   * @param [options.timeout=0] The timeout, in milliseconds, for a device to respond to the command.
   * Overrides the connection default request timeout.
   * @returns All responses to the command.
   */
  public async genericCommandMultiResponse(
    command: string,
    options: IlluminatorChannel.GenericCommandMultiResponseOptions = {}
  ): Promise<Response[]> {
    const {
      checkErrors = true,
      timeout = 0,
    } = options;
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      command: command,
      checkErrors: checkErrors,
      timeout: timeout,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.GenericCommandResponseCollection>(
      'interface/generic_command_multi_response',
      request,
      requests.GenericCommandResponseCollection.fromBinary);
    return response.responses;
  }

  /**
   * Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
   * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
   * @param command Command and its parameters.
   */
  public async genericCommandNoResponse(
    command: string
  ): Promise<void> {
    const request: requests.GenericCommandRequest & gateway.Message = {
      ...requests.GenericCommandRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      command: command,
      toBinary() {
        return requests.GenericCommandRequest.toBinary(this);
      },
    };

    await gateway.callAsync('interface/generic_command_no_response', request);
  }

  /**
   * Returns a serialization of the current channel state that can be saved and reapplied.
   * @returns A serialization of the current state of the channel.
   */
  public async getState(): Promise<string> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.StringResponse>(
      'device/get_state',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }

  /**
   * Applies a saved state to this channel.
   * @param state The state object to apply to this channel.
   * @returns Reports of any issues that were handled, but caused the state to not be exactly restored.
   */
  public async setState(
    state: string
  ): Promise<SetStateAxisResponse> {
    const request: requests.SetStateRequest & gateway.Message = {
      ...requests.SetStateRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      state: state,
      toBinary() {
        return requests.SetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<SetStateAxisResponse>(
      'device/set_axis_state',
      request,
      SetStateAxisResponse.fromBinary);
    return response;
  }

  /**
   * Checks if a state can be applied to this channel.
   * This only covers exceptions that can be determined statically such as mismatches of ID or version,
   * the process of applying the state can still fail when running.
   * @param state The state object to check against.
   * @param options.firmwareVersion The firmware version of the device to apply the state to.
   * Use this to ensure the state will still be compatible after an update.
   * @returns An explanation of why this state cannot be set to this channel.
   */
  public async canSetState(
    state: string,
    options: IlluminatorChannel.CanSetStateOptions = {}
  ): Promise<string | null> {
    const {
      firmwareVersion,
    } = options;
    const request: requests.CanSetStateRequest & gateway.Message = {
      ...requests.CanSetStateRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      state: state,
      firmwareVersion: firmwareVersion,
      toBinary() {
        return requests.CanSetStateRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.CanSetStateAxisResponse>(
      'device/can_set_axis_state',
      request,
      requests.CanSetStateAxisResponse.fromBinary);
    return response.error ?? null;
  }

  /**
   * Returns a string that represents the channel.
   * @returns A string that represents the channel.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.illuminator.device.connection.interfaceId,
      device: this.illuminator.device.deviceAddress,
      axis: this.channelNumber,
      typeOverride: 'Channel',
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/axis_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace IlluminatorChannel {
  export interface GenericCommandOptions {
      /**
       * Controls whether to throw an exception when the device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface GenericCommandMultiResponseOptions {
      /**
       * Controls whether to throw an exception when a device rejects the command.
       */
      checkErrors?: boolean;
      /**
       * The timeout, in milliseconds, for a device to respond to the command.
       * Overrides the connection default request timeout.
       */
      timeout?: number;
  }
  export interface CanSetStateOptions {
      /**
       * The firmware version of the device to apply the state to.
       * Use this to ensure the state will still be compatible after an update.
       */
      firmwareVersion?: FirmwareVersion;
  }
}
