/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from '../gateway/sanitizer';

/**
 * Status of the autofocus.
 */
export interface AutofocusStatus {
  /**
   * Indicates whether the autofocus is in focus.
   */
  inFocus: boolean;

  /**
   * Indicates whether the autofocus is in range.
   */
  inRange: boolean;

}

export const AutofocusStatus = {
  fromBinary: (buffer: Uint8Array): AutofocusStatus => BSON.deserialize(buffer) as AutofocusStatus,
  toBinary: (value: AutofocusStatus): Uint8Array => BSON.serialize(AutofocusStatus.sanitize(value)),
  DEFAULT: Object.freeze({
    inFocus: false,
    inRange: false,
  }) as Readonly<AutofocusStatus>,
  sanitize: (value: AutofocusStatus): AutofocusStatus => {
    if (value == null) { throw new TypeError('Expected AutofocusStatus object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected AutofocusStatus object but got ${typeof value}.`) }
    return {
      inFocus: sanitizer.sanitizeBool(value.inFocus, 'inFocus'),
      inRange: sanitizer.sanitizeBool(value.inRange, 'inRange'),
    };
  },
};
