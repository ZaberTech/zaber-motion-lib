﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import * as requests from '../requests';
import { Device } from '../ascii/device';
import { DeviceIO } from '../ascii/device_io';
import { Connection } from '../ascii/connection';
import { IlluminatorChannel } from './illuminator_channel';

/**
 * Use to manage an LED controller.
 * Requires at least Firmware 7.09.
 */
export class Illuminator {
  /**
   * The base device of this illuminator.
   */
  public get device(): Device {
    return this._device;
  }
  private _device: Device;

  /**
   * I/O channels of this device.
   */
  public get io(): DeviceIO {
    return this._io;
  }
  private _io: DeviceIO;

  /**
   * Creates instance of `Illuminator` based on the given device.
   * If the device is identified, this constructor will ensure it is an illuminator.
   */
  constructor(device: Device) {
    this._device = device;
    this._io = new DeviceIO(device);
    this._verifyIsIlluminator();
  }

  /**
   * Gets an IlluminatorChannel class instance that allows control of a particular channel.
   * Channels are numbered from 1.
   * @param channelNumber Number of channel to control.
   * @returns Illuminator channel instance.
   */
  public getChannel(
    channelNumber: number
  ): IlluminatorChannel {
    if (channelNumber <= 0) {
      throw new TypeError('Invalid value; channels are numbered from 1.');
    }
    return new IlluminatorChannel(this, channelNumber);
  }

  /**
   * Checks if this is an illuminator or some other type of device and throws an error if it is not.
   */
  private _verifyIsIlluminator(): void {
    const request: requests.DeviceEmptyRequest & gateway.Message = {
      ...requests.DeviceEmptyRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.DeviceEmptyRequest.toBinary(this);
      },
    };

    gateway.callSync('illuminator/verify', request);
  }

  /**
   * Finds an illuminator on a connection.
   * In case of conflict, specify the optional device address.
   * @param connection Connection on which to detect the illuminator.
   * @param [options.deviceAddress=0] Optional device address of the illuminator.
   * @returns New instance of illuminator.
   */
  public static async find(
    connection: Connection,
    options: Illuminator.FindOptions = {}
  ): Promise<Illuminator> {
    const {
      deviceAddress = 0,
    } = options;
    const request: requests.FindDeviceRequest & gateway.Message = {
      ...requests.FindDeviceRequest.DEFAULT,
      interfaceId: connection.interfaceId,
      deviceAddress: deviceAddress,
      toBinary() {
        return requests.FindDeviceRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.FindDeviceResponse>(
      'illuminator/detect',
      request,
      requests.FindDeviceResponse.fromBinary);
    return new Illuminator(new Device(connection, response.address));
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.device.connection.interfaceId,
      device: this.device.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace Illuminator {
  export interface FindOptions {
      /**
       * Optional device address of the illuminator.
       */
      deviceAddress?: number;
  }
}
