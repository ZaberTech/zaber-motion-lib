﻿// ==== THIS FILE IS GENERATED FROM A TEMPLATE ==== //
// ============= DO NOT EDIT DIRECTLY ============= //

import * as gateway from '../gateway';
import { Connection } from '../ascii/connection';
import { Device } from '../ascii/device';
import { Axis } from '../ascii/axis';
import { Measurement } from '../measurement';
import * as requests from '../requests';
import { Angle, Length, Native, Units } from '../units';

/**
 * Represents an objective changer of a microscope.
 * Unstable. Expect breaking changes in future releases.
 * Requires at least Firmware 7.32.
 */
export class ObjectiveChanger {
  /**
   * Device address of the turret.
   */
  public get turret(): Device {
    return this._turret;
  }
  private _turret: Device;

  /**
   * The focus axis.
   */
  public get focusAxis(): Axis {
    return this._focusAxis;
  }
  private _focusAxis: Axis;

  /**
   * Creates instance of `ObjectiveChanger` based on the given device.
   * If the device is identified, this constructor will ensure it is an objective changer.
   */
  constructor(turret: Device, focusAxis: Axis) {
    this._turret = turret;
    this._focusAxis = focusAxis;
    this._verifyIsChanger();
  }

  /**
   * @deprecated Use microscope's `Find` method instead or instantiate manually.
   *
   * Finds an objective changer on a connection.
   * In case of conflict, specify the optional device addresses.
   * Devices on the connection must be identified.
   * @param connection Connection on which to detect the objective changer.
   * @param [options.turretAddress=0] Optional device address of the turret device (X-MOR).
   * @param [options.focusAddress=0] Optional device address of the focus device (X-LDA).
   * @returns New instance of objective changer.
   */
  public static async find(
    connection: Connection,
    options: ObjectiveChanger.FindOptions = {}
  ): Promise<ObjectiveChanger> {
    const {
      turretAddress = 0,
      focusAddress = 0,
    } = options;
    const request: requests.ObjectiveChangerRequest & gateway.Message = {
      ...requests.ObjectiveChangerRequest.DEFAULT,
      interfaceId: connection.interfaceId,
      turretAddress: turretAddress,
      focusAddress: focusAddress,
      toBinary() {
        return requests.ObjectiveChangerRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.ObjectiveChangerCreateResponse>(
      'objective_changer/detect',
      request,
      requests.ObjectiveChangerCreateResponse.fromBinary);
    return new ObjectiveChanger(new Device(connection, response.turret), new Axis(new Device(connection, response.focusAddress), response.focusAxis));
  }

  /**
   * Changes the objective.
   * Runs a sequence of movements switching from the current objective to the new one.
   * The focus stage moves to the focus datum after the objective change.
   * @param objective Objective number starting from 1.
   * @param options.focusOffset Optional offset from the focus datum.
   */
  public async change(
    objective: number,
    options: ObjectiveChanger.ChangeOptions = {}
  ): Promise<void> {
    const {
      focusOffset,
    } = options;
    const request: requests.ObjectiveChangerChangeRequest & gateway.Message = {
      ...requests.ObjectiveChangerChangeRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      turretAddress: this.turret.deviceAddress,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      objective: objective,
      focusOffset: focusOffset,
      toBinary() {
        return requests.ObjectiveChangerChangeRequest.toBinary(this);
      },
    };

    await gateway.callAsync('objective_changer/change', request);
  }

  /**
   * Moves the focus stage out of the turret releasing the current objective.
   */
  public async release(): Promise<void> {
    const request: requests.ObjectiveChangerRequest & gateway.Message = {
      ...requests.ObjectiveChangerRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      turretAddress: this.turret.deviceAddress,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      toBinary() {
        return requests.ObjectiveChangerRequest.toBinary(this);
      },
    };

    await gateway.callAsync('objective_changer/release', request);
  }

  /**
   * Returns current objective number starting from 1.
   * The value of 0 indicates that the position is either unknown or between two objectives.
   * @returns Current objective number starting from 1 or 0 if not applicable.
   */
  public async getCurrentObjective(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      device: this.turret.deviceAddress,
      axis: 1,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_position',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets number of objectives that the turret can accommodate.
   * @returns Number of positions.
   */
  public async getNumberOfObjectives(): Promise<number> {
    const request: requests.AxisEmptyRequest & gateway.Message = {
      ...requests.AxisEmptyRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      device: this.turret.deviceAddress,
      axis: 1,
      toBinary() {
        return requests.AxisEmptyRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.IntResponse>(
      'device/get_index_count',
      request,
      requests.IntResponse.fromBinary);
    return response.value;
  }

  /**
   * Gets the focus datum.
   * The focus datum is the position that the focus stage moves to after an objective change.
   * It is backed by the limit.home.offset setting.
   * @param [unit=Units.NATIVE] Units of datum.
   * @returns The datum.
   */
  public async getFocusDatum(
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<number> {
    const request: requests.ObjectiveChangerSetRequest & gateway.Message = {
      ...requests.ObjectiveChangerSetRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      turretAddress: this.turret.deviceAddress,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      unit: unit,
      toBinary() {
        return requests.ObjectiveChangerSetRequest.toBinary(this);
      },
    };

    const response = await gateway.callAsync<requests.DoubleResponse>(
      'objective_changer/get_datum',
      request,
      requests.DoubleResponse.fromBinary);
    return response.value;
  }

  /**
   * Sets the focus datum.
   * The focus datum is the position that the focus stage moves to after an objective change.
   * It is backed by the limit.home.offset setting.
   * @param datum Value of datum.
   * @param [unit=Units.NATIVE] Units of datum.
   */
  public async setFocusDatum(
    datum: number,
    unit: Length | Angle | Native = Units.NATIVE
  ): Promise<void> {
    const request: requests.ObjectiveChangerSetRequest & gateway.Message = {
      ...requests.ObjectiveChangerSetRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      turretAddress: this.turret.deviceAddress,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      value: datum,
      unit: unit,
      toBinary() {
        return requests.ObjectiveChangerSetRequest.toBinary(this);
      },
    };

    await gateway.callAsync('objective_changer/set_datum', request);
  }

  /**
   * Checks if this is a objective changer and throws an error if it is not.
   */
  private _verifyIsChanger(): void {
    const request: requests.ObjectiveChangerRequest & gateway.Message = {
      ...requests.ObjectiveChangerRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      turretAddress: this.turret.deviceAddress,
      focusAddress: this.focusAxis.device.deviceAddress,
      focusAxis: this.focusAxis.axisNumber,
      toBinary() {
        return requests.ObjectiveChangerRequest.toBinary(this);
      },
    };

    gateway.callSync('objective_changer/verify', request);
  }

  /**
   * Returns a string that represents the device.
   * @returns A string that represents the device.
   */
  public toString(): string {
    const request: requests.AxisToStringRequest & gateway.Message = {
      ...requests.AxisToStringRequest.DEFAULT,
      interfaceId: this.turret.connection.interfaceId,
      device: this.turret.deviceAddress,
      toBinary() {
        return requests.AxisToStringRequest.toBinary(this);
      },
    };

    const response = gateway.callSync<requests.StringResponse>(
      'device/device_to_string',
      request,
      requests.StringResponse.fromBinary);
    return response.value;
  }
}

namespace ObjectiveChanger {
  export interface FindOptions {
      /**
       * Optional device address of the turret device (X-MOR).
       */
      turretAddress?: number;
      /**
       * Optional device address of the focus device (X-LDA).
       */
      focusAddress?: number;
  }
  export interface ChangeOptions {
      /**
       * Optional offset from the focus datum.
       */
      focusOffset?: Measurement;
  }
}
