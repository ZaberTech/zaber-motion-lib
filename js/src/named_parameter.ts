/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from './gateway/sanitizer';

/**
 * Named parameter with optional value.
 */
export interface NamedParameter {
  /**
   * Name of the parameter.
   */
  name: string;

  /**
   * Optional value of the parameter.
   */
  value?: (number | null);

}

export const NamedParameter = {
  fromBinary: (buffer: Uint8Array): NamedParameter => BSON.deserialize(buffer) as NamedParameter,
  toBinary: (value: NamedParameter): Uint8Array => BSON.serialize(NamedParameter.sanitize(value)),
  DEFAULT: Object.freeze({
    name: '',
    value: null,
  }) as Readonly<NamedParameter>,
  sanitize: (value: NamedParameter): NamedParameter => {
    if (value == null) { throw new TypeError('Expected NamedParameter object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected NamedParameter object but got ${typeof value}.`) }
    return {
      name: sanitizer.sanitizeString(value.name, 'name'),
      value: value.value != null ? sanitizer.sanitizeNumber(value.value, 'value') : null,
    };
  },
};
