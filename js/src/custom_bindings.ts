import { setEnv } from './gateway/bindings';

/** Sets a custom directory to read the executable binding from if not running in a normal node environment */
export function useCustomBindings(libPath: string): void {
  setEnv('exec', libPath);
}
