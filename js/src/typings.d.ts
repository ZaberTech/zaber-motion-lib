declare module '@mapbox/node-pre-gyp';

declare const window: {
  __zmlRequest(request: string, cb?: (responseString: string) => void): string;
  __zmlSetEventHandler(cb: (event: string) => void): void;
  __zmlShutdown(): void;
  __zmlStarted: () => void;
  Go: new () => any;
  WebAssembly: any;
  btoa: (str: string) => string;
  atob: (str: string) => string;
};

/* Following are types copied from ES2020 */
/* eslint-disable */
interface FinalizationRegistry<T> {
  readonly [Symbol.toStringTag]: "FinalizationRegistry";

  /**
   * Registers an object with the registry.
   * @param target The target object to register.
   * @param heldValue The value to pass to the finalizer for this object. This cannot be the
   * target object.
   * @param unregisterToken The token to pass to the unregister method to unregister the target
   * object. If provided (and not undefined), this must be an object. If not provided, the target
   * cannot be unregistered.
   */
  register(target: object, heldValue: T, unregisterToken?: object): void;

  /**
   * Unregisters an object from the registry.
   * @param unregisterToken The token that was used as the unregisterToken argument when calling
   * register to register the target object.
   */
  unregister(unregisterToken: object): void;
}

interface FinalizationRegistryConstructor {
  readonly prototype: FinalizationRegistry<any>;

  /**
   * Creates a finalization registry with an associated cleanup callback
   * @param cleanupCallback The callback to call after an object in the registry has been reclaimed.
   */
  new<T>(cleanupCallback: (heldValue: T) => void): FinalizationRegistry<T>;
}

declare var FinalizationRegistry: FinalizationRegistryConstructor;
/* eslint-enable */
