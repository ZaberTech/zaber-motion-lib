/* This file is generated. Do not modify by hand. */
import { BSON } from 'bson';
import { sanitizer } from './gateway/sanitizer';

/**
 * Holds device address and IO channel number.
 */
export interface ChannelAddress {
  /**
   * Device address.
   */
  device: number;

  /**
   * IO channel number.
   */
  channel: number;

}

export const ChannelAddress = {
  fromBinary: (buffer: Uint8Array): ChannelAddress => BSON.deserialize(buffer) as ChannelAddress,
  toBinary: (value: ChannelAddress): Uint8Array => BSON.serialize(ChannelAddress.sanitize(value)),
  DEFAULT: Object.freeze({
    device: 0,
    channel: 0,
  }) as Readonly<ChannelAddress>,
  sanitize: (value: ChannelAddress): ChannelAddress => {
    if (value == null) { throw new TypeError('Expected ChannelAddress object but got null or undefined.') }
    if (typeof value !== 'object') { throw new TypeError(`Expected ChannelAddress object but got ${typeof value}.`) }
    return {
      device: sanitizer.sanitizeInt(value.device, 'device'),
      channel: sanitizer.sanitizeInt(value.channel, 'channel'),
    };
  },
};
