{
    "targets": [{
        "target_name": "<(module_name)",
        "cflags": [ "-std=c++17" ],
        "cflags_cc": [ "-std=c++17" ],
        "xcode_settings": {
            "OTHER_CFLAGS": [ "-std=c++17"],
        },
        "sources": [
            "src_cpp/module.cpp",
            "src_cpp/events.cpp",
            "src_cpp/callback.cpp",
        ],
        'include_dirs': [
            "<!@(node -p \"require('node-addon-api').include\")",
        ],
        'defines': [
            "NAPI_VERSION=<(napi_build_version)",
        ],
        'dependencies': [
            "<!(node -p \"require('node-addon-api').gyp\")"
        ],
        'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ],
        'conditions': [
            ['OS == "win"', {
                'sources': [
                    "src_cpp/load_dll.cpp"
                ],
                'conditions': [
                    ['target_arch == "ia32"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-windows-386.dll']
                            }
                        ]
                    }],
                    ['target_arch == "x64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-windows-amd64.dll']
                            }
                        ]
                    }],
                    ['target_arch == "arm64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-windows-arm64.dll']
                            }
                        ]
                    }]
                ]
            }, 'OS == "linux"', {
                'sources': [
                    "src_cpp/load_so.cpp"
                ],
                'conditions': [
                    ['target_arch == "ia32"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-linux-386.so']
                            }
                        ]
                    }],
                    ['target_arch == "x64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-linux-amd64.so']
                            }
                        ]
                    }],
                    ['target_arch == "arm"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-linux-arm.so']
                            }
                        ]
                    }],
                    ['target_arch == "arm64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-linux-arm64.so']
                            }
                        ]
                    }]
                ]
            }, {
                'sources': [
                    "src_cpp/load_so.cpp"
                ],
                'conditions': [
                    ['target_arch == "x64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-darwin-amd64.dylib']
                            }
                        ]
                    }],
                    ['target_arch == "arm64"', {
                        "copies": [
                            {
                                'destination': '<(module_path)',
                                'files': ['<(module_root_dir)/../build/zaber-motion-core-darwin-arm64.dylib']
                            }
                        ]
                    }]
                ],
            }
],
        ]
    },
    {
      "target_name": "action_after_build",
      "type": "none",
      "dependencies": [ "<(module_name)" ],
      "copies": [
          {
            "files": [ "<(PRODUCT_DIR)/<(module_name).node" ],
            "destination": "<(module_path)"
          }
      ]
    }]
}
