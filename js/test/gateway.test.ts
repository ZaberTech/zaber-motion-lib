import { expect } from 'chai';
import _ from 'lodash';
import { firstValueFrom } from 'rxjs';
import { BSON } from 'bson';

import {
  Message, callAsync, callSync, events, filterEvent,
} from '../src/gateway';
import {
  TestRequest, TestResponse, TestEvent, TestResponseLong,
} from '../src/requests';
import { InvalidPacketException, RequestTimeoutException, InvalidRequestDataException } from '../src';

function testRequest(partial: Partial<TestRequest> = {}) {
  const request: TestRequest & Message = {
    ...TestRequest.DEFAULT,
    ...partial,
    toBinary() {
      return TestRequest.toBinary(this);
    },
  };
  return request;
}

describe('Gateway', () => {
  it('calls and parses response', async () => {
    const request = testRequest({ dataPing: 'Hello' });
    const responseData = await callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);

    expect(responseData.dataPong).to.eq('Hello...');
  });

  it('allows sync calls', async () => {
    const request = testRequest({ dataPing: 'Hello' });
    const responseData = callSync<TestResponse>('test/request', request, TestResponse.fromBinary);

    expect(responseData.dataPong).to.eq('Hello...');
  });

  it('calls and raises error', async () => {
    const request = testRequest({ dataPing: 'Hello', returnError: true });
    const promise = callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);
    await expect(promise).to.be.rejectedWith(RequestTimeoutException);
    const error: RequestTimeoutException = await promise.catch(e => e);
    expect(error.message).to.eq('Device has not responded in given timeout');
    expect(error.toString()).to.eq('RequestTimeoutException: Device has not responded in given timeout');
  });

  it('calls and raises error with data', async () => {
    const request = testRequest({ dataPing: 'Hello', returnErrorWithData: true });
    const promise = callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);
    await expect(promise).to.be.rejectedWith(InvalidPacketException);

    const error: InvalidPacketException = await promise.catch(e => e);
    const expectedPacket = '123';
    const expectedReason = 'For test';
    const expectedMessage = `Cannot parse incoming packet: "${expectedPacket}" (${expectedReason})`;
    expect(error.message).to.equal(expectedMessage);
    expect(error.details.packet).to.equal(expectedPacket);
    expect(error.details.reason).to.equal(expectedReason);
  });

  it('calls and recieves no response', async () => {
    const response = await callAsync('test/request_no_response');
    expect(response).to.eq(null);
  });

  it('calls and handles long response', async () => {
    const request = testRequest({ dataPing: 'Hello' });
    const responseData = await callAsync<TestResponseLong>('test/request_long', request, TestResponseLong.fromBinary);

    responseData.dataPong.forEach((pong, i) => expect(pong).to.eq(`Hello...${i}`));
  });

  it('can handle multiple parallel requests', async () => {
    await Promise.all(_.range(0, 10).map(async i => {
      const request = testRequest({ dataPing: `Hello${i}` });
      const response = await callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);
      expect(response.dataPong).to.eq(`Hello${i}...`);
    }));
  });

  it('emits events', async () => {
    const eventPromise = firstValueFrom(
      events.pipe(filterEvent<TestEvent>('test/event'))
    );
    await callAsync('test/emit_event');

    const event = await eventPromise;
    expect(event?.data).to.eq('testing event data');
  });

  describe('Go validation', () => {
    function testRequest(anything: Record<string, unknown> = {}) {
      const request: Message = {
        ...anything,
        toBinary() {
          return BSON.serialize(this);
        },
      };
      return request;
    }

    it('throws invalid request error for data of wrong shape (wrong type)', async () => {
      const request = testRequest({ dataPing: 0 });
      const promise = callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);
      await expect(promise).to.be.rejectedWith(InvalidRequestDataException);

      const error: InvalidRequestDataException = await promise.catch(e => e);
      expect(error.message).to.eq(
        'Invalid request data: error decoding key dataPing: cannot decode 32-bit integer into a string type');
    });

    it('throws invalid request error for data of wrong shape (null ref)', async () => {
      const request = testRequest({ time: null });
      const promise = callAsync<TestResponse>('device/stream_point', request, TestResponse.fromBinary);
      await expect(promise).to.be.rejectedWith(InvalidRequestDataException);

      const error: InvalidRequestDataException = await promise.catch(e => e);
      expect(error.message).to.eq(
        'Invalid request data: Required property Time of PvtPointRequest is nil');
    });

    it('throws invalid request error for data of wrong shape (null array item)', async () => {
      const request = testRequest({ positions: [null] });
      const promise = callAsync<TestResponse>('device/stream_point', request, TestResponse.fromBinary);
      await expect(promise).to.be.rejectedWith(InvalidRequestDataException);

      const error: InvalidRequestDataException = await promise.catch(e => e);
      expect(error.message).to.eq(
        'Invalid request data: Required item in property Positions of PvtPointRequest is nil');
    });
  });
});
