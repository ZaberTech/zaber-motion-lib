import { expect } from 'chai';

import { ascii, binary } from '../src';
import * as asciiImport from '../src/ascii';
import * as binaryImport from '../src/binary';

function checkNamespace(namespace: any, imports: any): void {
  const keys = Object.keys(namespace).sort();
  expect(keys).to.deep.eq(Object.keys(imports).sort());

  for (const key of keys) {
    expect(namespace[key]).to.eq(imports[key], `${key} does not export proper class`);
  }
}

describe('namespace', () => {
  describe('ascii', () => {
    it('contains everything', () => {
      checkNamespace(ascii, asciiImport);
    });
  });

  describe('binary', () => {
    it('contains everything', () => {
      checkNamespace(binary, binaryImport);
    });
  });
});
