import chaiAsPromised from 'chai-as-promised';
import chai from 'chai';
import chaiString from 'chai-string';

chai.use(chaiAsPromised);
chai.use(chaiString);
