import { expect } from 'chai';
import fs from 'fs/promises';

import {
  Message, callAsync
} from '../src/gateway';
import {
  TestRequest, TestResponse
} from '../src/requests';
import { useCustomBindings } from '../src/custom_bindings';
import { arch, platform, tmpdir } from 'os';
import path from 'path';
import { glob } from 'glob';

function testRequest(partial: Partial<TestRequest> = {}) {
  const request: TestRequest & Message = {
    ...TestRequest.DEFAULT,
    ...partial,
    toBinary() {
      return TestRequest.toBinary(this);
    },
  };
  return request;
}

describe('Custom Gateway', () => {
  it('binds to the library in a custom location', async () => {
    const releaseSubDirs = await glob(`./dist/binding/Release/*-${platform()}-${arch()}`);
    if (releaseSubDirs.length !== 1) {
      throw new Error(`Got multiple or no release subdirs: ${JSON.stringify(releaseSubDirs)}`);
    }
    const releaseSubDir = releaseSubDirs[0];
    const dirName = path.basename(releaseSubDir);
    const tmpBinDir = path.join(tmpdir(), dirName);
    await fs.rm(tmpBinDir, { recursive: true, force: true });
    await fs.cp(releaseSubDir, tmpBinDir, { recursive: true });
    useCustomBindings(tmpBinDir);
    const request = testRequest({ dataPing: 'Hello' });
    const responseData = await callAsync<TestResponse>('test/request', request, TestResponse.fromBinary);

    expect(responseData.dataPong).to.eq('Hello...');
  });
});
