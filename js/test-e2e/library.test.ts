import { expect } from 'chai';
import fs from 'fs';

import { Library, LogOutputMode } from '../src';

const TEST_FILE = 'test_out.log';

describe('Library', () => {
  describe('setLogOutput', () => {
    it('creates logging file', () => {
      Library.setLogOutput(LogOutputMode.FILE, TEST_FILE);

      expect(fs.existsSync(TEST_FILE)).to.be.eq(true);
    });

    it('switches back to off and closes the file', () => {
      Library.setLogOutput(LogOutputMode.OFF);

      fs.unlinkSync(TEST_FILE); // can be safely deleted
    });

    it('throws exception on invalid file', () => {
      const test = () => Library.setLogOutput(LogOutputMode.FILE, '//');
      expect(test).to.throw(/^IO operation open failed/);
    });
  });
});
