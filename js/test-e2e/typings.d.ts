declare module 'rimraf';

declare module 'simple-jsonrpc-js';
declare namespace Chai {
  interface Assertion {
    onTime(time: number, marginToBeLate?: number): void;
  }
}
