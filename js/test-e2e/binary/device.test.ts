import { expect } from 'chai';

import { TestServerWrapper } from './test_server_wrapper';
import { Connection, Device, CommandCode, ErrorCode, ReplyCode } from '../../src/binary';
import { BinaryCommandFailedException, DeviceDbFailedException, DeviceNotIdentifiedException, Units } from '../../src';
import { TEST_PORT, TEST_HOST } from './tcp';
import { DeviceType } from '../../src/binary/device_type';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCB2 } from './devices/xmcb2';

const server = new TestServerWrapper();

describe('Device (Binary)', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('identify', () => {
    it('identifies simple one axis device', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 50081,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SERIAL_NUMBER,
        data: 1234,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 628,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_BUILD,
        data: 1347,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 50081,
        deviceType: DeviceType.LINEAR,
        firmwareVersion: {
          build: 1347,
          major: 6,
          minor: 28,
        },
        name: 'X-LHM100A',
        serialNumber: 1234,
        isPeripheral: false,
        peripheralName: '',
        peripheralId: 0,
      });
    });

    it('can assume provided version', async () => {
      const promise = device.identify({
        assumeVersion: {
          build: 1310,
          major: 6,
          minor: 27,
        },
      });

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 50081,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SERIAL_NUMBER,
        data: 1234,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 50081,
        deviceType: DeviceType.LINEAR,
        firmwareVersion: {
          build: 1310,
          major: 6,
          minor: 27,
        },
        name: 'X-LHM100A',
        serialNumber: 1234,
        isPeripheral: false,
        peripheralName: '',
        peripheralId: 0,
      });
    });

    it('throws exception when device cannot be identified', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 123,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SERIAL_NUMBER,
        data: 456,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 321,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      await expect(promise).to.be.rejectedWith(DeviceDbFailedException);
      const exception: DeviceDbFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Cannot find product with device ID 123 and firmware version 3.21.0');
      expect(exception.details).to.deep.eq({ code: 'not-found' });
    });

    it('identifies axis of composite device as a device', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 30222,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SERIAL_NUMBER,
        data: 1234,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 629,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_BUILD,
        data: 1432,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 44122,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 30222,
        deviceType: DeviceType.LINEAR,
        firmwareVersion: {
          build: 1432,
          major: 6,
          minor: 29,
        },
        name: 'X-MCB2',
        serialNumber: 1234,
        isPeripheral: true,
        peripheralId: 44122,
        peripheralName: 'ASR120B-E03T3 (Upper Axis)',
      });
    });

    it('identifies device with partial binary support', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 50797,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SERIAL_NUMBER,
        data: 12345,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 706,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_BUILD,
        data: 1932,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 50797,
        deviceType: DeviceType.UNKNOWN,
        firmwareVersion: {
          build: 1932,
          major: 7,
          minor: 6,
        },
        name: 'X-LDA150A-AE53',
        serialNumber: 12345,
        isPeripheral: false,
        peripheralId: 0,
        peripheralName: '',
      });
    });

    it('identifies device with old firmware 5', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 302,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.COMMAND_INVALID,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 535,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.SETTING_INVALID,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 302,
        deviceType: DeviceType.ROTARY,
        firmwareVersion: {
          build: 0,
          major: 5,
          minor: 35,
        },
        name: 'T-MM2',
        serialNumber: 0,
        isPeripheral: false,
        peripheralId: 0,
        peripheralName: '',
      });
    });

    it('identifies device with old firmware 6', async () => {
      const promise = device.identify();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 1,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 0,
        id: cmd.id,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
      expect(cmd.device).to.equal(1);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 30111,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.COMMAND_INVALID,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_FIRMWARE_VERSION,
        data: 605,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.COMMAND_INVALID,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 64,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
      await server.push({
        device: 1,
        command: CommandCode.RETURN_SETTING,
        data: 40133,
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        deviceId: 30111,
        deviceType: DeviceType.LINEAR,
        firmwareVersion: {
          build: 0,
          major: 6,
          minor: 5,
        },
        name: 'A-MCA',
        serialNumber: 0,
        isPeripheral: true,
        peripheralId: 40133,
        peripheralName: 'LSQ300D-E01T3',
      });
    });
  });

  it('bypasses bug when FW 6.04 returns peripheral id for integrated product', async () => {
    const promise = device.identify();

    let cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
    await server.push({
      device: 1,
      command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
      data: 0,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
    expect(cmd.device).to.equal(1);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_DEVICE_ID,
      data: 20111,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
    await server.push({
      device: 1,
      command: ReplyCode.ERROR,
      data: ErrorCode.COMMAND_INVALID,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_FIRMWARE_VERSION,
      data: 604,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_FIRMWARE_BUILD,
      data: 115,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_SETTING,
      data: 64,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_SETTING,
      data: 0,
    });

    const identity = await promise;
    expect(identity).to.deep.eq({
      deviceId: 20111,
      deviceType: DeviceType.LINEAR,
      firmwareVersion: {
        major: 6,
        minor: 4,
        build: 115,
      },
      name: 'A-LSQ075A-E01',
      serialNumber: 0,
      isPeripheral: false,
      peripheralId: 0,
      peripheralName: '',
    });
  });

  it('X-MCA still works with 6.04 despite the bypass of the bug', async () => {
    const promise = device.identify();

    let cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
    await server.push({
      device: 1,
      command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
      data: 0,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
    expect(cmd.device).to.equal(1);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_DEVICE_ID,
      data: 30111,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
    await server.push({
      device: 1,
      command: ReplyCode.ERROR,
      data: ErrorCode.COMMAND_INVALID,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_FIRMWARE_VERSION,
      data: 604,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_FIRMWARE_BUILD,
      data: 115,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_SETTING,
      data: 64,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
    await server.push({
      device: 1,
      command: CommandCode.RETURN_SETTING,
      data: 0,
    });

    const identity = await promise;
    expect(identity).to.deep.eq({
      deviceId: 30111,
      deviceType: DeviceType.UNKNOWN,
      firmwareVersion: {
        minor: 4,
        major: 6,
        build: 115,
      },
      name: 'A-MCA',
      serialNumber: 0,
      isPeripheral: true,
      peripheralId: 0,
      peripheralName: 'Safe Mode',
    });
  });

  it('failed identification erases previous identity', async () => {
    await DeviceXLHM.identify(device, server);

    expect(device.isIdentified).to.eq(true);

    const promise = device.identify();

    let cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
    await server.push({
      device: 1,
      command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
      data: 0,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
    await server.push({
      device: 1,
      command: ReplyCode.ERROR,
      data: ErrorCode.COMMAND_INVALID,
    });

    await expect(promise).to.rejectedWith(BinaryCommandFailedException);

    expect(device.isIdentified).to.eq(false);
  });

  describe('properties', () => {
    it('returns appropriate data', async () => {
      await DeviceXMCB2.identify(device, server);

      expect(device.deviceId).to.eq(30222);
      expect(device.serialNumber).to.eq(1234);
      expect(device.name).to.eq('X-MCB2');
      expect(device.deviceType).to.eq(DeviceType.LINEAR);
      expect(device.isPeripheral).to.eq(true);
      expect(device.peripheralId).to.eq(43211);
      expect(device.peripheralName).to.eq('LHM025A-T3');
      expect(device.firmwareVersion).to.deep.eq({
        build: 1432,
        major: 6,
        minor: 29,
      });
    });

    it('checks if isIdentified is false before identifying', async () => {
      expect(device.isIdentified).to.eq(false);
    });

    it('checks if isIdentified is true after identifying', async () => {
      await DeviceXMCB2.identify(device, server);
      expect(device.isIdentified).to.equal(true);
    });
  });

  describe('toString', () => {
    it('returns representation for non identified device', () => {
      expect(device.toString()).to.startWith('Device 1 (Unknown) -> Connection');
    });

    it('returns representation for identified device', async () => {
      await DeviceXLHM.identify(device, server);

      expect(device.toString()).to.startWith('Device 1 SN: 1234 (X-LHM100A) -> Connection');
    });

    it('returns representation for identified device with peripheral', async () => {
      await DeviceXMCB2.identify(device, server);

      expect(device.toString()).to.startWith('Device 1 SN: 1234 (LHM025A-T3 -> X-MCB2) -> Connection');
    });

    it('returns representation for closed connection', async () => {
      await connection.close();
      expect(device.toString()).to.eq('Device 1 -> Connection Closed');
    });
  });

  describe('identity', () => {
    it('returns null before identification and identity after', async () => {
      expect(() => device.identity).to.throw(/Device number 1 is not identified\./);

      await DeviceXLHM.identify(device, server);

      expect(device.identity.name).to.eq('X-LHM100A');
    });
  });

  describe('genericCommand', () => {
    it('sends proper command', async () => {
      const promise = device.genericCommand(CommandCode.ECHO_DATA, 123);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.ECHO_DATA);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);
      await server.push({
        device: 1,
        command: CommandCode.ECHO_DATA,
        data: 123,
      });

      const response = await promise;
      expect(response).to.deep.eq({
        deviceAddress: 1,
        command: CommandCode.ECHO_DATA,
        data: 123,
      });
    });
  });

  describe('genericCommandNoResponse', () => {
    it('sends proper command', async () => {
      const promise = device.genericCommandNoResponse(CommandCode.ECHO_DATA, 123);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.ECHO_DATA);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);

      await promise;
    });
  });

  describe('genericCommandWithUnits', () => {
    it('throws exception when device is not identified', async () => {
      const promise = device.genericCommandWithUnits(CommandCode.SET_CURRENT_POSITION, 1.0, Units.NATIVE, Units.NATIVE);

      await expect(promise).to.be.rejectedWith(DeviceNotIdentifiedException);
      const exception: DeviceNotIdentifiedException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot perform this operation. Device number 1 is not identified. Call device.identify() to enable this operation.');
    });
  });
});
