import { expect } from 'chai';

import { TestServerWrapper } from './test_server_wrapper';
import { performance } from 'perf_hooks';
import { Connection, Device, CommandCode } from '../../src/binary';
import { Length, Velocity, Angle, ConversionFailedException, RequestTimeoutException, Units } from '../../src';
import { TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCB2 } from './devices/xmcb2';
import { itTime } from '../test_util';

const server = new TestServerWrapper();

describe('Identified device (binary) with unit conversion', () => {
  let connection: Connection;
  let device: Device;
  let peripheral: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await DeviceXLHM.identify(device, server);

    peripheral = connection.getDevice(2);
    await DeviceXMCB2.identify(peripheral, server);

    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('device movement', () => {
    describe('exceptions', () => {
      it('throws exception when using wrong units', async () => {
        const promise = device.moveAbsolute(4.5, Angle.DEGREES);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
      });
    });

    describe('handle move requests to device with unit conversion', () => {
      it('home', async () => {
        const promise = device.home({ unit: Length.mm });

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.HOME);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.HOME,
          data: 0,
        });

        expect(await promise).to.equal(0);
      });

      it('stop', async () => {
        const promise = device.stop({ unit: Length.cm });

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.STOP);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.STOP,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1, 1e-3);
      });

      it('moveAbsolute', async () => {
        const promise = device.moveAbsolute(4.5, Length.mm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(36283);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 36283,
        });

        expect(await promise).to.be.closeTo(4.5, 1e-3);
      });

      it('moveRelative', async () => {
        const promise = device.moveRelative(3, Length.mm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_RELATIVE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(24189);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_RELATIVE,
          data: 24189,
        });

        expect(await promise).to.be.closeTo(3, 1e-3);
      });

      it('moveVelocity', async () => {
        const promise = device.moveVelocity(1.5, Velocity.MILLIMETRES_PER_SECOND);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_AT_CONSTANT_SPEED);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(19816);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_AT_CONSTANT_SPEED,
          data: 19816,
        });

        expect(await promise).to.be.closeTo(1.5, 1e-3);
      });
    });

    describe('handle move requests to peripheral with unit conversion', () => {
      it('moveAbsolute', async () => {
        const promise = peripheral.moveAbsolute(4.5, Length.MILLIMETRES);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(2);
        expect(cmd.data).to.equal(36283);

        await server.push({
          device: 2,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 36283,
        });

        expect(await promise).to.be.closeTo(4.5, 1e-3);
      });
    });
  });

  describe('getPosition', () => {
    it('sends proper command and returns response', async () => {
      const promise = device.getPosition();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_CURRENT_POSITION,
        data: 123,
      });
      expect(await promise).to.equal(123);
    });

    it('performs correct unit conversion', async () => {
      const promise = device.getPosition(Length.cm);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_CURRENT_POSITION,
        data: 80629,
      });

      expect(await promise).to.be.closeTo(1.0, 1e-3);
    });
  });

  describe('generic command with units', () => {
    describe('exceptions - regular commands', () => {
      it('throws exception with command that has no contextual dimension', async () => {
        let promise = device.genericCommandWithUnits(CommandCode.ECHO_DATA, 0, Length.cm);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        let exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Command argument does not support unit conversion.');

        promise = device.genericCommandWithUnits(CommandCode.ECHO_DATA, 1.0, Units.NATIVE, Angle.DEGREES);
        await server.pop();
        await server.push({
          device: 1,
          command: CommandCode.ECHO_DATA,
          data: 1,
        });

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        exception = await promise.catch(e => e);
        expect(exception.message).to.eq('Command reply does not support unit conversion.');
      });

      it('throws exception with wrong FromUnit', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1.0, Angle.DEGREES, Units.NATIVE);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
      });
      it('throws exception with wrong ToUnit', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1.0, Units.NATIVE, Angle.DEGREES);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(1.0);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 1,
        });

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
      });
    });

    describe('exceptions - setting commands', () => {
      it('throws exception with wrong FromUnit for set setting', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.SET_CURRENT_POSITION, 1.0, Angle.DEGREES);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
      });

      it('throws exception with wrong ToUnit for get setting', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.RETURN_CURRENT_POSITION, 0, Units.NATIVE, Angle.DEGREES);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.RETURN_CURRENT_POSITION,
          data: 80630,
        });

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
      });
    });

    describe('perform correct conversion for commands', () => {
      it('sends proper command', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1.0);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(1);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 1,
        });

        expect(await promise).to.eq(1);
      });

      it('performs proper toUnit conversion', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1.0, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(80630);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 80630,
        });

        expect(await promise).to.eq(80630);
      });

      it('performs proper toUnit conversion peripheral', async () => {
        const promise = peripheral.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1.0, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(2);
        expect(cmd.data).to.equal(80630);

        await server.push({
          device: 2,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 80630,
        });

        expect(await promise).to.eq(80630);
      });

      it('toUnit conversion for command with only ResponseContextualDimension', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.STOP, 0, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.STOP);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.STOP,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1.0, 1e-3);
      });

      it('toUnit conversion for command with only ResponseContextualDimension peripheral', async () => {
        const promise = peripheral.genericCommandWithUnits(CommandCode.STOP, 0, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.STOP);
        expect(cmd.device).to.equal(2);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 2,
          command: CommandCode.STOP,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1.0, 1e-3);
      });

      it('performs proper fromUnit conversion', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 80630, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(80630);

        await server.push({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1.0, 1e-3);
      });

      it('performs proper fromUnit conversion peripheral', async () => {
        const promise = peripheral.genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 80630, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(2);
        expect(cmd.data).to.equal(80630);

        await server.push({
          device: 2,
          command: CommandCode.MOVE_ABSOLUTE,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1.0, 1e-3);
      });

      itTime('honors timeout overrides', async () => {
        const timeoutMs = 200;
        const startTime = performance.now();

        const promise = device
          .genericCommandWithUnits(CommandCode.MOVE_ABSOLUTE, 1, Units.NATIVE, Units.NATIVE, { timeout: timeoutMs / 1000 });

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(1);

        await expect(promise).to.be.rejectedWith(RequestTimeoutException);

        const elapsedTime = performance.now() - startTime;
        expect(elapsedTime).to.be.onTime(timeoutMs);
      });
    });

    describe('perform correct conversion for settings commands', () => {
      it('performs fromUnit conversion for set setting', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.SET_CURRENT_POSITION, 1.0, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.SET_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(80630);

        await server.push({
          device: 1,
          command: CommandCode.SET_CURRENT_POSITION,
          data: 80630,
        });

        await promise;
      });

      it('performs fromUnit conversion for generic get setting command', async () => {
        const promise = device.genericCommandWithUnits(
          CommandCode.RETURN_SETTING, CommandCode.SET_MAXIMUM_POSITION, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(CommandCode.SET_MAXIMUM_POSITION);

        await server.push({
          device: 1,
          command: CommandCode.RETURN_SETTING,
          data: 161260,
        });

        expect(await promise).to.be.closeTo(2.0, 1e-3);
      });

      it('performs fromUnit conversion for specific get setting command', async () => {
        const promise = device.genericCommandWithUnits(CommandCode.RETURN_CURRENT_POSITION, 0, Units.NATIVE, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.RETURN_CURRENT_POSITION,
          data: 80630,
        });

        expect(await promise).to.be.closeTo(1.0, 1e-3);
      });
    });
  });
});
