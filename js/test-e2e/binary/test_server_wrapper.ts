import { BinaryTestServer } from './tcp';
import { BinaryMessage, parseBinaryMessage, formatBinaryMessage } from './protocol';

export class TestServerWrapper {
  private readonly server = new BinaryTestServer();

  constructor(public readonly withIds: boolean = false, existingServer?: BinaryTestServer) {
    if (existingServer !== undefined) {
      this.server = existingServer;
    }
  }

  public start(): Promise<void> {
    return this.server.start();
  }

  public stop(): Promise<void> {
    return this.server.stop();
  }

  public closeSocket(): void {
    return this.server.closeSocket();
  }

  public waitForConnection(): Promise<void> {
    return this.server.waitForConnection();
  }

  public async push(message: BinaryMessage): Promise<void> {
    await this.server.push(formatBinaryMessage(message, this.withIds));
  }

  public async pop(): Promise<BinaryMessage> {
    const message = await this.server.pop();
    return parseBinaryMessage(message, this.withIds);
  }
}
