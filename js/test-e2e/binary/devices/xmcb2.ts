import { expect } from 'chai';
import { TestServerWrapper } from '../test_server_wrapper';
import { Device, CommandCode } from '../../../src/binary';

enum Mode {
    Linear,
    Rotary,
    Empty,
}

export class DeviceXMCB2 {
  public static Mode = Mode;

  public static async identify(device: Device, server: TestServerWrapper, mode: Mode = Mode.Linear): Promise<void> {
    const promise = device.identify();

    await DeviceXMCB2.identifyNumber(device.deviceAddress, server, mode);

    await promise;
  }

  public static async identifyNumber(device: number, server: TestServerWrapper, mode: Mode = Mode.Linear): Promise<void> {
    let cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
    await server.push({
      device,
      command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
      data: 0,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
    expect(cmd.device).to.equal(device);
    await server.push({
      device,
      command: CommandCode.RETURN_DEVICE_ID,
      data: 30222,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
    await server.push({
      device,
      command: CommandCode.RETURN_SERIAL_NUMBER,
      data: 1234,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
    await server.push({
      device,
      command: CommandCode.RETURN_FIRMWARE_VERSION,
      data: 629,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
    await server.push({
      device,
      command: CommandCode.RETURN_FIRMWARE_BUILD,
      data: 1432,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
    await server.push({
      device,
      command: CommandCode.RETURN_SETTING,
      data: 64,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
    await server.push({
      device,
      command: CommandCode.RETURN_SETTING,
      data: DeviceXMCB2.peripherals[mode],
    });
  }

  private static peripherals = {
    [Mode.Linear]: 43211,
    [Mode.Rotary]: 46653,
    [Mode.Empty]: 0,
  };
}
