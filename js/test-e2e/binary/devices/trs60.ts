import { expect } from 'chai';
import { TestServerWrapper } from '../test_server_wrapper';
import { Device, CommandCode, ReplyCode, ErrorCode } from '../../../src/binary';

export class DeviceTRS60 {
  public static async identify(device: Device, server: TestServerWrapper): Promise<void> {
    const promise = device.identify();

    await DeviceTRS60.identifyNumber(device.deviceAddress, server);

    await promise;
  }

  public static async identifyNumber(device: number, server: TestServerWrapper): Promise<void> {
    let cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
    await server.push({
      device,
      command: ReplyCode.ERROR,
      data: ErrorCode.SETTING_INVALID,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);
    expect(cmd.device).to.equal(device);
    await server.push({
      device,
      command: CommandCode.RETURN_DEVICE_ID,
      data: 560,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SERIAL_NUMBER);
    await server.push({
      device,
      command: CommandCode.RETURN_SERIAL_NUMBER,
      data: 1234,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
    await server.push({
      device,
      command: CommandCode.RETURN_FIRMWARE_VERSION,
      data: 529,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_MICROSTEP_RESOLUTION);
    await server.push({
      device,
      command: CommandCode.RETURN_SETTING,
      data: 64,
      id: cmd.id,
    });

    cmd = await server.pop();
    expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
    expect(cmd.data).to.equal(CommandCode.SET_PERIPHERAL_ID);
    await server.push({
      device,
      command: ReplyCode.ERROR,
      data: ErrorCode.SETTING_INVALID,
      id: cmd.id,
    });
  }
}
