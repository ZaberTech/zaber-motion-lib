import { expect } from 'chai';
import { performance } from 'perf_hooks';

import { TestServerWrapper } from './test_server_wrapper';
import { Connection, Device, CommandCode, ErrorCode, ReplyCode } from '../../src/binary';
import { RequestTimeoutException, Units, BinaryCommandFailedException, InvalidParkStateException } from '../../src';
import { TEST_PORT, TEST_HOST } from './tcp';
import { itTime } from '../test_util';

const server = new TestServerWrapper();

describe('Device (Binary)', () => {
  let connection: Connection;
  let device: Device;

  const BinaryReplyCodeIdle = 0;
  const BinaryReplyCodeParked = 65;
  const BinaryReplyCodeDriverDisabled = 90;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('home', () => {
    it('sends proper command and returns position', async () => {
      const promise = device.home();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.HOME);
      expect(cmd.device).to.equal(1);

      await server.push({
        device: 1,
        command: CommandCode.HOME,
        data: 50081,
      });

      expect(await promise).to.eq(50081);
    });

    itTime('throws exception when device does not respond before custom timeout', async () => {
      const timeoutMs = 200;
      const startTime = performance.now();

      const promise = device.home({ timeout: timeoutMs / 1000.0 });

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.HOME);
      expect(cmd.device).to.equal(1);

      await expect(promise).to.be.rejectedWith(RequestTimeoutException);

      const elapsedTime = performance.now() - startTime;
      expect(elapsedTime).to.be.onTime(timeoutMs);

      const exception: RequestTimeoutException = await promise.catch(e => e);
      expect(exception.message).to.eq('Device has not responded in given timeout');
    });
  });

  describe('stop', () => {
    it('sends proper command and returns position', async () => {
      const promise = device.stop();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.STOP);
      expect(cmd.device).to.equal(1);

      await server.push({
        device: 1,
        command: CommandCode.STOP,
        data: 50081,
      });

      expect(await promise).to.eq(50081);
    });
  });

  describe('park', () => {
    it('sends proper command', async () => {
      const promise = device.park();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_PARK_STATE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(1);

      await server.push({
        device: 1,
        command: CommandCode.SET_PARK_STATE,
        data: 1,
      });

      await promise;
    });

    it('sends proper command and throws exception if device returns with parking state error', async () => {
      const promise = device.park();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_PARK_STATE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(1);

      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.PARK_STATE_INVALID,
      });

      const exception: InvalidParkStateException = await promise.catch(e => e);
      expect(exception.message).to.eq('The requested park state is invalid or the device cannot park/unpark because it is in motion.');
    });
  });

  describe('unpark', () => {
    it('sends proper command', async () => {
      const promise = device.unpark();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_PARK_STATE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.SET_PARK_STATE,
        data: 0,
      });

      await promise;
    });

    it('sends proper command and throws exception if device returns with parking state error', async () => {
      const promise = device.unpark();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_PARK_STATE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.PARK_STATE_INVALID,
      });

      const exception: InvalidParkStateException = await promise.catch(e => e);
      expect(exception.message).to.eq('The requested park state is invalid or the device cannot park/unpark because it is in motion.');
    });
  });

  describe('isParked', () => {
    it('sends proper command and returns true if the device is parked', async () => {
      const promise = device.isParked();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeParked,
      });

      expect(await promise).to.eq(true);
    });

    it('sends proper command and returns false if device is idle', async () => {
      const promise = device.isParked();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeIdle,
      });

      expect(await promise).to.eq(false);
    });
  });

  describe('moveAbsolute', () => {
    it('sends proper command and returns position', async () => {
      const promise = device.moveAbsolute(123, Units.NATIVE);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);

      await server.push({
        device: 1,
        command: CommandCode.MOVE_ABSOLUTE,
        data: 321,
      });

      expect(await promise).to.eq(321);
    });

    it('throws exception with bad data', async () => {
      const promise = device.moveAbsolute(-123, Units.NATIVE);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.MOVE_ABSOLUTE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(-123);

      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.ABSOLUTE_POSITION_INVALID,
      });

      await expect(promise).to.be.rejectedWith(BinaryCommandFailedException);
      const exception: BinaryCommandFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Command 20 with data -123 resulted in error code 20.');
      expect(exception.details.responseData).to.equal(ErrorCode.ABSOLUTE_POSITION_INVALID);
    });
  });

  describe('moveRelative', () => {
    it('sends proper command and returns position', async () => {
      const promise = device.moveRelative(123, Units.NATIVE);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.MOVE_RELATIVE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);

      await server.push({
        device: 1,
        command: CommandCode.MOVE_RELATIVE,
        data: 321,
      });

      expect(await promise).to.eq(321);
    });
  });

  describe('moveVelocity', () => {
    it('sends proper command and returns velocity', async () => {
      const promise = device.moveVelocity(123, Units.NATIVE);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.MOVE_AT_CONSTANT_SPEED);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);

      await server.push({
        device: 1,
        command: CommandCode.MOVE_AT_CONSTANT_SPEED,
        data: 321,
      });

      expect(await promise).to.eq(321);
    });
  });

  describe('waitUntilIdle', () => {
    it('sends proper command and returns if device status is idle', async () => {
      const promise = device.waitUntilIdle();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeIdle,
      });

      await promise;
    });

    it('returns if device status is parked', async () => {
      const promise = device.waitUntilIdle();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeParked,
      });

      await promise;
    });

    it('returns if device status is driver disabled', async () => {
      const promise = device.waitUntilIdle();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeDriverDisabled,
      });

      await promise;
    });

    it('continuously poll until device is idle', async () => {
      const promise = device.waitUntilIdle();

      let cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: CommandCode.MOVE_ABSOLUTE,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: CommandCode.MOVE_ABSOLUTE,
      });

      cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeIdle,
      });

      await promise;
    });
  });

  describe('isBusy', () => {
    it('returns true when device is moving', async () => {
      const promise = device.isBusy();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: CommandCode.MOVE_ABSOLUTE,
      });

      expect(await promise).to.equal(true);
    });

    it('returns false when device is idle', async () => {
      const promise = device.isBusy();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeIdle,
      });

      expect(await promise).to.equal(false);
    });

    it('returns false when driver is disabled', async () => {
      const promise = device.isBusy();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeDriverDisabled,
      });

      expect(await promise).to.equal(false);
    });

    it('returns false when device is parked', async () => {
      const promise = device.isBusy();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_STATUS);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_STATUS,
        data: BinaryReplyCodeParked,
      });

      expect(await promise).to.equal(false);
    });
  });
});
