import { CommandCode, ReplyCode, ErrorCode } from '../../src/binary';

export const PACKET_SIZE = 6;

export interface BinaryMessage {
  device: number;
  command: CommandCode | ReplyCode;
  id?: number;
  data: number | CommandCode | ErrorCode;
}

export const parseBinaryMessage = (command: Buffer, withId: boolean): BinaryMessage => {
  let data = 0;

  const id = withId ? command[5] : -1;

  for (let i = PACKET_SIZE - 1; i >= 2; i--) {
    data <<= 8;
    data |= command[i];
  }

  if (withId) {
    data = (data << 8) >> 8;
  }

  return {
    device: command[0],
    command: command[1],
    id,
    data,
  };
};

export const formatBinaryMessage = (message: BinaryMessage, withId: boolean): Buffer => {
  const buf = Buffer.alloc(PACKET_SIZE);

  buf[0] = message.device;
  buf[1] = message.command;

  let data = withId ? (message.data & 0x00FFFFFF) | ((message.id! & 0xFF) << 24) : message.data;

  for (let i = 2; i < PACKET_SIZE; i++) {
    buf[i] = data & 0xFF;
    data >>= 8;
  }

  return buf;
};
