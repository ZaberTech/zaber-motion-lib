import { expect } from 'chai';
import { performance } from 'perf_hooks';
import { firstValueFrom, lastValueFrom } from 'rxjs';

import { Connection, CommandCode, ReplyCode, ErrorCode } from '../../src/binary';
import {
  ConnectionFailedException, ConnectionClosedException, InvalidArgumentException,
  CommandPreemptedException, RequestTimeoutException, BinaryCommandFailedException, NoDeviceFoundException
} from '../../src';
import { BinaryTestServer, TEST_HOST, TEST_PORT } from './tcp';
import { parseBinaryMessage, formatBinaryMessage } from './protocol';
import { DeviceXLHM } from './devices/xlhm';
import { TestServerWrapper } from './test_server_wrapper';
import { itTime } from '../test_util';

const server = new BinaryTestServer();

[true, false].forEach(useMessageIds => {
  describe(`Connection (Binary, IDs ${useMessageIds ? 'enabled' : 'disabled'})`, () => {
    let connection: Connection;
    beforeAll(async () => {
      await server.start();
    });

    afterAll(async () => {
      await server.stop();
    });

    beforeEach(async () => {
      connection = await Connection.openTcp(TEST_HOST, TEST_PORT, { useMessageIds });
      await server.waitForConnection();
    });

    afterEach(async () => {
      await connection.close();
      server.closeSocket();
    });

    it('connects and sends command', async () => {
      await connection.genericCommandNoResponse(0, CommandCode.ECHO_DATA, 123);

      const packet = parseBinaryMessage(await server.pop(), useMessageIds);
      expect(packet).to.include({ device: 0, command: CommandCode.ECHO_DATA, data: 123 });
      if (useMessageIds) {
        expect(packet.id).to.not.equal(-1);
      }
    });

    it('emits disconnect when closed by other side', async () => {
      server.closeSocket();

      const error = await lastValueFrom(connection.disconnected);
      expect(error).to.be.an.instanceof(ConnectionFailedException);
    });

    it('emits disconnect when closed by call', async () => {
      await connection.close();

      const error = await lastValueFrom(connection.disconnected);
      expect(error).to.be.an.instanceof(ConnectionClosedException);
    });

    it('closes the connection', async () => {
      const waitForClose = server.waitForClose();
      await connection.close();
      await waitForClose;
    });

    describe('unknownResponse', () => {
      it('emits unknownResponse event when cannot find request', async () => {
        const replyPromise = firstValueFrom(connection.unknownResponse);

        await server.push(formatBinaryMessage({ device: 2, command: CommandCode.ECHO_DATA, id: 1, data: 123 }, useMessageIds));

        const reply = await replyPromise;

        expect(reply).to.deep.include({
          deviceAddress: 2,
          command: CommandCode.ECHO_DATA,
          data: 123,
        });
      });
      it('emits unknownResponse event when cannot match error', async () => {
        const replyPromise = firstValueFrom(connection.unknownResponse);

        await server
          .push(formatBinaryMessage({ device: 2, command: ReplyCode.ERROR, id: 1, data: ErrorCode.COMMAND_INVALID }, useMessageIds));

        const reply = await replyPromise;

        expect(reply).to.deep.include({
          deviceAddress: 2,
          command: ReplyCode.ERROR,
          data: ErrorCode.COMMAND_INVALID,
        });
      });
    });

    describe('replyOnly', () => {
      it('emits reply-only event when a tracking message is received from the device', async () => {
        const eventPromise = firstValueFrom(connection.replyOnly);

        await server.push(formatBinaryMessage({ device: 2, command: ReplyCode.MANUAL_MOVE_TRACKING, id: 0, data: 12345 }, useMessageIds));

        const reply = await eventPromise;

        expect(reply).to.deep.include({
          deviceAddress: 2,
          command: ReplyCode.MANUAL_MOVE_TRACKING,
          data: 12345,
        });
      });
    });

    [-1, 256].forEach(address => {
      it(`range checks device addresses (${address})`, async () => {
        const promise = connection.genericCommand(address, CommandCode.HOME);
        await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      });
    });

    describe('genericCommandNoResponse', () => {
      it('sends a command', async () => {
        await connection.genericCommandNoResponse(7, CommandCode.ECHO_DATA, 123);

        const packet = parseBinaryMessage(await server.pop(), useMessageIds);
        expect(packet).to.include({ device: 7, command: CommandCode.ECHO_DATA, data: 123 });
        if (useMessageIds) {
          expect(packet.id).to.equal(0);
        }
      });
    });

    describe('genericCommand', () => {
      it('matches responses', async () => {
        const replyPromise1 = connection.genericCommand(0, CommandCode.ECHO_DATA, 123);
        const packet1 = await server.pop();
        const cmd1 = parseBinaryMessage(packet1, useMessageIds);
        expect(cmd1.command).to.equal(CommandCode.ECHO_DATA);
        expect(cmd1.data).to.equal(123);

        const replyPromise2 = connection.genericCommand(0, CommandCode.RETURN_FIRMWARE_VERSION, 0);
        const packet2 = await server.pop();
        const cmd2 = parseBinaryMessage(packet2, useMessageIds);
        expect(cmd2.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
        expect(cmd2.data).to.equal(0);

        const replyPromise3 = connection.genericCommand(0, CommandCode.RETURN_FIRMWARE_BUILD, 0);
        const packet3 = await server.pop();
        const cmd3 = parseBinaryMessage(packet3, useMessageIds);
        expect(cmd3.command).to.equal(CommandCode.RETURN_FIRMWARE_BUILD);
        expect(cmd3.data).to.equal(0);

        const cmds = [cmd1, cmd2, cmd3];
        expect(cmds.map(cmd => cmd.device)).to.deep.equal([0, 0, 0]);
        if (useMessageIds) {
          cmds.forEach(c => {
            expect(c.id).to.not.equal(-1);
          });
        }

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: CommandCode.RETURN_FIRMWARE_BUILD,
            id: cmd3.id,
            data: 1234
          }, useMessageIds));

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: CommandCode.RETURN_FIRMWARE_VERSION,
            id: cmd2.id,
            data: 628
          }, useMessageIds));

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: CommandCode.ECHO_DATA,
            id: cmd1.id,
            data: 123
          }, useMessageIds));

        expect(await replyPromise1).to.deep.equal({
          deviceAddress: 1,
          command: CommandCode.ECHO_DATA,
          data: 123,
        });

        expect(await replyPromise2).to.deep.equal({
          deviceAddress: 1,
          command: CommandCode.RETURN_FIRMWARE_VERSION,
          data: 628,
        });

        expect(await replyPromise3).to.deep.equal({
          deviceAddress: 1,
          command: CommandCode.RETURN_FIRMWARE_BUILD,
          data: 1234,
        });
      });

      it('handles pre-emption', async () => {
        const replyPromise1 = connection.genericCommand(1, CommandCode.MOVE_ABSOLUTE, 1000);
        const packet1 = await server.pop();
        const replyPromise2 = connection.genericCommand(0, CommandCode.MOVE_ABSOLUTE, 2000);
        const packet2 = await server.pop();
        const replyPromise3 = connection.genericCommand(1, CommandCode.MOVE_ABSOLUTE, 3000);
        const packet3 = await server.pop();

        const cmds = [packet1, packet2, packet3].map(p => parseBinaryMessage(p, useMessageIds));
        expect(cmds.map(cmd => cmd.device)).to.deep.equal([1, 0, 1]);
        expect(cmds.map(cmd => cmd.command)).to.deep.equal(
          [CommandCode.MOVE_ABSOLUTE, CommandCode.MOVE_ABSOLUTE, CommandCode.MOVE_ABSOLUTE]);
        expect(cmds.map(cmd => cmd.data)).to.deep.equal([1000, 2000, 3000]);
        if (useMessageIds) {
          cmds.forEach(c => {
            expect(c.id).to.not.equal(-1);
          });
        }

        await server.push(formatBinaryMessage({
          device: 1,
          command: CommandCode.MOVE_ABSOLUTE,
          id: cmds[2].id,
          data: 500
        }, useMessageIds));

        await Promise.all([
          expect(replyPromise1).to.be.rejectedWith(CommandPreemptedException),
          expect(replyPromise2).to.be.rejectedWith(CommandPreemptedException),
        ]);

        const reply3 = await replyPromise3;
        expect(reply3.data).to.equal(500);
      });

      it('handles pre-emption by unknown reply', async () => {
        const replyPromise1 = connection.genericCommand(1, CommandCode.MOVE_ABSOLUTE, 1000);
        const packet1 = await server.pop();
        const cmd1 = parseBinaryMessage(packet1, useMessageIds);
        expect(cmd1.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd1.data).to.equal(1000);

        // Device sends this when unexpectedly reaching the home sensor.
        await server.push(formatBinaryMessage({
          device: 1,
          command: CommandCode.HOME,
          id: useMessageIds ? 0 : -1,
          data: 0
        }, useMessageIds));
        await expect(replyPromise1).to.be.rejectedWith(CommandPreemptedException);
      });

      it('does not preempt certain query commands when receive unexpected response', async () => {
        const replyPromise1 = connection.genericCommand(1, CommandCode.RETURN_STATUS);
        const packet1 = await server.pop();
        const cmd1 = parseBinaryMessage(packet1, useMessageIds);
        expect(cmd1.command).to.equal(CommandCode.RETURN_STATUS);

        // Device sends this when unexpectedly reaching the home sensor.
        await server.push(formatBinaryMessage({
          device: 1,
          command: CommandCode.HOME,
          id: useMessageIds ? 0 : -1,
          data: 0,
        }, useMessageIds));

        await server.push(formatBinaryMessage({
          device: 1, command: CommandCode.RETURN_STATUS,
          id: cmd1.id, data: 0,
        }, useMessageIds));
        expect(await replyPromise1).to.deep.include({
          deviceAddress: 1,
          command: CommandCode.RETURN_STATUS,
        });
      });

      it('times out when reply not received', async () => {
        const replyPromise1 = connection.genericCommand(0, CommandCode.ECHO_DATA, 123);
        const packet1 = await server.pop();
        const cmd1 = parseBinaryMessage(packet1, useMessageIds);
        expect(cmd1.command).to.equal(CommandCode.ECHO_DATA);
        expect(cmd1.data).to.equal(123);

        const replyPromise2 = connection.genericCommand(0, CommandCode.RETURN_FIRMWARE_VERSION, 0);
        const packet2 = await server.pop();
        const cmd2 = parseBinaryMessage(packet2, useMessageIds);
        expect(cmd2.command).to.equal(CommandCode.RETURN_FIRMWARE_VERSION);
        expect(cmd2.data).to.equal(0);

        const cmds = [cmd1, cmd2];
        expect(cmds.map(cmd => cmd.device)).to.deep.equal([0, 0]);
        if (useMessageIds) {
          cmds.forEach(c => {
            expect(c.id).to.not.equal(-1);
          });
        } else {
          cmds.forEach(c => {
            expect(c.id).to.equal(-1);
          });
        }

        // send back only one response
        await server.push(formatBinaryMessage({ device: 1, command: CommandCode.ECHO_DATA, id: cmd1.id, data: 123 }, useMessageIds));

        const reply1 = await replyPromise1;
        expect(reply1).to.deep.include({ data: 123 });

        await expect(replyPromise2).to.be.rejectedWith(RequestTimeoutException);
      });

      itTime('honors timeout overrides - less than default', async () => {
        const timeoutMs = 200;
        const startTime = performance.now();

        const promise = connection.genericCommand(1, CommandCode.ECHO_DATA, 123, { timeout: timeoutMs / 1000 });

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.command).to.equal(CommandCode.ECHO_DATA);
        expect(cmd.data).to.equal(123);
        expect(cmd.device).to.equal(1);
        if (useMessageIds) {
          expect(cmd.id).to.not.equal(-1);
        }

        await expect(promise).to.be.rejectedWith(RequestTimeoutException);

        const elapsedTime = performance.now() - startTime;
        expect(elapsedTime).to.be.onTime(timeoutMs);
      });

      it('honors timeout overrides - more than default', async () => {
        const promise = connection.genericCommand(2, CommandCode.ECHO_DATA, 456, { timeout: 10.0 });
        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.command).to.equal(CommandCode.ECHO_DATA);
        expect(cmd.data).to.equal(456);
        expect(cmd.device).to.equal(2);
        if (useMessageIds) {
          expect(cmd.id).to.not.equal(-1);
        }

        const waitingPromise = connection.genericCommand(3, CommandCode.ECHO_DATA);
        await server.pop();
        await expect(waitingPromise).to.rejectedWith(RequestTimeoutException);

        await server.push(formatBinaryMessage({ device: 2, command: CommandCode.ECHO_DATA, id: cmd.id, data: 456 }, useMessageIds));

        const reply = await promise;
        expect(reply).to.deep.include({ data: 456 });
      });

      it('checks for errors by default', async () => {
        const replyPromise = connection.genericCommand(0, CommandCode.MOVE_ABSOLUTE, -1);

        const packet = parseBinaryMessage(await server.pop(), useMessageIds);
        expect(packet).to.include({ device: 0, command: CommandCode.MOVE_ABSOLUTE, data: -1 });
        if (useMessageIds) {
          expect(packet.id).to.not.equal(-1);
        }

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: ReplyCode.ERROR,
            id: packet.id,
            data: CommandCode.MOVE_ABSOLUTE,
          }, useMessageIds));

        await expect(replyPromise).to.be.rejectedWith(BinaryCommandFailedException);
        const error: BinaryCommandFailedException = await replyPromise.catch(e => e);
        expect(error.details.responseData).to.equal(CommandCode.MOVE_ABSOLUTE);
      });

      it('returns errors when checking is disabled', async () => {
        const replyPromise = connection.genericCommand(0, CommandCode.MOVE_ABSOLUTE, -1, { checkErrors: false });

        const packet = parseBinaryMessage(await server.pop(), useMessageIds);
        expect(packet).to.include({ device: 0, command: CommandCode.MOVE_ABSOLUTE, data: -1 });
        if (useMessageIds) {
          expect(packet.id).to.not.equal(-1);
        }

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: ReplyCode.ERROR,
            id: packet.id,
            data: CommandCode.MOVE_ABSOLUTE,
          }, useMessageIds));

        const reply = await replyPromise;
        expect(reply).to.deep.include({
          deviceAddress: 1,
          command: ReplyCode.ERROR,
          data: CommandCode.MOVE_ABSOLUTE,
        });
      });
    });

    describe('renumberDevices', () => {
      it('returns correct number of devices that respond to the renumber', async () => {
        const renumberPromise = connection.renumberDevices();

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.command).to.equal(CommandCode.RENUMBER);
        expect(cmd.data).to.equal(0);

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: CommandCode.RENUMBER,
            id: cmd.id,
            data: 0,
          }, useMessageIds));
        await server.push(formatBinaryMessage(
          {
            device: 2,
            command: CommandCode.RENUMBER,
            id: cmd.id,
            data: 0,
          }, useMessageIds));
        await server.push(formatBinaryMessage(
          {
            device: 3,
            command: CommandCode.RENUMBER,
            id: cmd.id,
            data: 0,
          }, useMessageIds));

        const devices = await renumberPromise;
        expect(devices).to.eq(3);
      });

      it('resets device identities', async () => {
        const detectPromise = connection.detectDevices();

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

        await server.push(formatBinaryMessage(
          {
            device: 2,
            command: CommandCode.RETURN_DEVICE_ID,
            id: cmd.id,
            data: 0
          }, useMessageIds));

        const wrappedServer = new TestServerWrapper(useMessageIds, server);
        await DeviceXLHM.identifyNumber(2, wrappedServer);

        const devices = await detectPromise;
        expect(devices.map(device => device.deviceAddress)).to.deep.eq([2]);
        expect(devices[0].identity!.name).to.eq('X-LHM100A');

        const renumberPromise = connection.renumberDevices();

        const renumberPacket = await server.pop();
        const renumberCmd = parseBinaryMessage(renumberPacket, useMessageIds);
        expect(renumberCmd.command).to.equal(CommandCode.RENUMBER);

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: CommandCode.RENUMBER,
            id: renumberCmd.id,
            data: 0
          }, useMessageIds));

        const numberDevicesRenumbered = await renumberPromise;
        expect(numberDevicesRenumbered).to.eq(1);

        expect(() => devices[0].identity).to.throw(/Device number 2 is not identified\./);
      });

      it('throws exception when no device responds', async () => {
        const renumberPromise = connection.renumberDevices();

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.command).to.equal(CommandCode.RENUMBER);
        expect(cmd.data).to.equal(0);

        await expect(renumberPromise).to.be.rejectedWith(NoDeviceFoundException);
        const exception: NoDeviceFoundException = await renumberPromise.catch(e => e);
        expect(exception.message).to.eq(
          'Cannot find any device on the port. Make sure that you have opened correct port and devices are powered on.'
        );
      });
    });

    [
      {
        cmd: CommandCode.MOVE_ABSOLUTE,
        err: CommandCode.MOVE_ABSOLUTE,
        msg: 'Command 20 with data 0 resulted in error code 20.',
      },
      {
        cmd: CommandCode.WRITE_DIGITAL_OUTPUT,
        err: ErrorCode.DIGITAL_OUTPUT_PIN_INVALID,
        msg: 'Command 73 with data 0 resulted in error code 71.',
      },
      {
        cmd: CommandCode.SET_DEVICE_MODE,
        err: ErrorCode.DISABLE_AUTO_HOME_INVALID,
        msg: 'Command 40 with data 0 resulted in error code 4008.',
      },
    ].forEach(tuple => {
      it(`matches error code ${tuple.err} to command ${tuple.cmd}`, async () => {
        const replyPromise = connection.genericCommand(1, tuple.cmd, 0);

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd.device).to.equal(1);
        expect(cmd.command).to.equal(tuple.cmd);
        expect(cmd.data).to.equal(0);
        if (useMessageIds) {
          expect(cmd.id).to.not.equal(-1);
        }

        await server.push(formatBinaryMessage(
          {
            device: 1,
            command: ReplyCode.ERROR,
            id: cmd.id,
            data: tuple.err,
          }, useMessageIds));

        await expect(replyPromise).to.be.rejectedWith(BinaryCommandFailedException);
        const error: BinaryCommandFailedException = await replyPromise.catch(e => e);
        expect(error.message).to.equal(tuple.msg);
        expect(error.details.responseData).to.equal(tuple.err);
      });

      it('Matches error with correct request', async () => {
        const replyPromise1 = connection.genericCommand(1, CommandCode.MOVE_ABSOLUTE, -1);
        const packet1 = await server.pop();
        const cmd1 = parseBinaryMessage(packet1, useMessageIds);
        expect(cmd1.command).to.equal(CommandCode.MOVE_ABSOLUTE);
        expect(cmd1.data).to.equal(-1);

        const replyPromise2 = connection.genericCommand(1, CommandCode.RETURN_STATUS, 0);
        const packet2 = await server.pop();
        const cmd2 = parseBinaryMessage(packet2, useMessageIds);
        expect(cmd2.command).to.equal(CommandCode.RETURN_STATUS);
        expect(cmd2.data).to.equal(0);

        const cmds = [cmd1, cmd2];
        expect(cmds.map(cmd => cmd.device)).to.deep.equal([1, 1]);
        if (useMessageIds) {
          cmds.forEach(c => {
            expect(c.id).to.not.equal(-1);
          });
        } else {
          cmds.forEach(c => {
            expect(c.id).to.equal(-1);
          });
        }

        await server.push(formatBinaryMessage({
          device: 1, command: ReplyCode.ERROR,
          id: cmd1.id, data: CommandCode.MOVE_ABSOLUTE
        }, useMessageIds));
        await server.push(formatBinaryMessage({
          device: 1, command: CommandCode.RETURN_STATUS,
          id: cmd2.id, data: 0
        }, useMessageIds));

        await expect(replyPromise1).to.be.rejectedWith(BinaryCommandFailedException);
        const exception: BinaryCommandFailedException = await replyPromise1.catch(e => e);
        expect(exception.message).to.eq('Command 20 with data -1 resulted in error code 20.');
        expect(exception.details.responseData).to.equal(CommandCode.MOVE_ABSOLUTE);

        await replyPromise2;
      });
    });

    describe('genericCommandMultiResponse', () => {
      it('handles broadcast responses', async () => {
        const replyPromise = connection.genericCommandMultiResponse(CommandCode.ECHO_DATA, 123);

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd).to.include({ device: 0, command: CommandCode.ECHO_DATA, data: 123 });
        if (useMessageIds) {
          expect(cmd.id).to.not.equal(-1);
        }

        await server.push(formatBinaryMessage({ device: 1, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));
        await server.push(formatBinaryMessage({ device: 3, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));
        await server.push(formatBinaryMessage({ device: 2, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));
        await server.push(formatBinaryMessage({ device: 7, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));

        const result = await replyPromise;

        expect(result).to.deep.equal([
          {
            deviceAddress: 1,
            command: CommandCode.ECHO_DATA,
            data: 123
          },
          {
            deviceAddress: 3,
            command: CommandCode.ECHO_DATA,
            data: 123
          },
          {
            deviceAddress: 2,
            command: CommandCode.ECHO_DATA,
            data: 123
          },
          {
            deviceAddress: 7,
            command: CommandCode.ECHO_DATA,
            data: 123
          },
        ]);
      });

      it('handles error responses to broadcast responses', async () => {
        const replyPromise = connection.genericCommandMultiResponse(CommandCode.ECHO_DATA, 123);

        const packet = await server.pop();
        const cmd = parseBinaryMessage(packet, useMessageIds);
        expect(cmd).to.include({ device: 0, command: CommandCode.ECHO_DATA, data: 123 });
        if (useMessageIds) {
          expect(cmd.id).to.not.equal(-1);
        }

        await server.push(formatBinaryMessage({ device: 1, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));
        await server.push(formatBinaryMessage({ device: 3, command: CommandCode.ECHO_DATA, id: cmd.id, data: 123 }, useMessageIds));
        await server.push(formatBinaryMessage(
          { device: 2, command: ReplyCode.ERROR, id: cmd.id, data: CommandCode.ECHO_DATA }, useMessageIds));

        await expect(replyPromise).to.be.rejectedWith(BinaryCommandFailedException);
      });
    });

    describe('toString', () => {
      it('returns representation', () => {
        expect(connection.toString()).to.match(/^Connection \d+ \(Binary TCP: 127.0.0.1\)$/);
      });
      it('returns representation for closed connection', async () => {
        await connection.close();
        expect(connection.toString()).to.match(/^Connection \d+ \(Closed\)$/);
      });
    });
  });
});
