import { expect } from 'chai';

import { TestServerWrapper } from './test_server_wrapper';
import { BinarySettings, Connection, Device, DeviceSettings, CommandCode, ReplyCode, ErrorCode } from '../../src/binary';
import { Length, ConversionFailedException, Velocity, BinaryCommandFailedException, DeviceNotIdentifiedException } from '../../src';
import { TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';

const server = new TestServerWrapper();

describe(' BinaryDeviceSettings', () => {
  let connection: Connection;
  let device: Device;
  let settings: DeviceSettings;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    settings = device.settings;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('get without identification', () => {
    it('specific command code when GetCommandCode is available', async () => {
      const promise = settings.get(BinarySettings.CURRENT_POSITION);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_CURRENT_POSITION,
        data: 123,
      });

      const value = await promise;
      expect(value).to.deep.eq(123);
    });

    it('generic returnSetting command code when only SetCommandCode is available', async () => {
      const promise = settings.get(BinarySettings.MAXIMUM_POSITION);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(CommandCode.SET_MAXIMUM_POSITION);

      await server.push({
        device: 1,
        command: CommandCode.SET_MAXIMUM_POSITION,
        data: 321,
      });

      const value = await promise;
      expect(value).to.deep.eq(321);
    });

    it('throws exception when setting is not applicable to device', async () => {
      const promise = settings.get(BinarySettings.CYCLE_DISTANCE);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(CommandCode.SET_CYCLE_DISTANCE);

      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: CommandCode.RETURN_SETTING,
      });

      await expect(promise).to.be.rejectedWith(BinaryCommandFailedException);
      const exception: BinaryCommandFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Command 53 with data 80 resulted in error code 53.');
      expect(exception.details.responseData).to.equal(CommandCode.RETURN_SETTING);
    });

    it('throws exception with unit conversion', async () => {
      const promise = settings.get(BinarySettings.CURRENT_POSITION, Length.cm);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(0);

      await server.push({
        device: 1,
        command: CommandCode.RETURN_CURRENT_POSITION,
        data: 80629,
      });

      await expect(promise).to.be.rejectedWith(DeviceNotIdentifiedException);
      const exception: DeviceNotIdentifiedException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot perform this operation. Device number 1 is not identified. Call device.identify() to enable this operation.');
    });
  });

  describe('get with identification', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
    });

    describe('unit conversion', () => {
      it('performs correct unit conversion', async () => {
        const promise = settings.get(BinarySettings.CURRENT_POSITION, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.RETURN_CURRENT_POSITION,
          data: 80629,
        });

        expect(await promise).to.be.closeTo(1.0, 3);
      });

      it('throws exception with wrong unit provided', async () => {
        const promise = settings.get(BinarySettings.CURRENT_POSITION, Velocity.CENTIMETRES_PER_SECOND);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.RETURN_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.equal(0);

        await server.push({
          device: 1,
          command: CommandCode.RETURN_CURRENT_POSITION,
          data: 80629,
        });

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Velocity:centimetres per second. Provide units of dimension Length.');
      });
    });
  });

  describe('set without identification', () => {
    it('sends correct command code when SetCommandCode is available', async () => {
      const promise = settings.set(BinarySettings.CURRENT_POSITION, 123);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_CURRENT_POSITION);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(123);

      await server.push({
        device: 1,
        command: CommandCode.SET_CURRENT_POSITION,
        data: 123,
      });

      await promise;
    });

    it('throws exception when setting is not applicable to device', async () => {
      const promise = settings.set(BinarySettings.CYCLE_DISTANCE, 1);

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.SET_CYCLE_DISTANCE);
      expect(cmd.device).to.equal(1);
      expect(cmd.data).to.equal(1);

      await server.push({
        device: 1,
        command: ReplyCode.ERROR,
        data: ErrorCode.COMMAND_INVALID,
      });

      await expect(promise).to.be.rejectedWith(BinaryCommandFailedException);
      const exception: BinaryCommandFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Command 80 with data 1 resulted in error code 64.');
      expect(exception.details.responseData).to.equal(ErrorCode.COMMAND_INVALID);
    });

    it('throws exception with unit conversion', async () => {
      const promise = settings.set(BinarySettings.CURRENT_POSITION, 123, Length.cm);

      await expect(promise).to.be.rejectedWith(DeviceNotIdentifiedException);
      const exception: DeviceNotIdentifiedException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot perform this operation. Device number 1 is not identified. Call device.identify() to enable this operation.');
    });
  });

  describe('set with identification', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
    });

    describe('unit conversion', () => {
      it('performs correct unit conversion', async () => {
        const promise = settings.set(BinarySettings.CURRENT_POSITION, 1, Length.cm);

        const cmd = await server.pop();
        expect(cmd.command).to.equal(CommandCode.SET_CURRENT_POSITION);
        expect(cmd.device).to.equal(1);
        expect(cmd.data).to.be.equal(80630);

        await server.push({
          device: 1,
          command: CommandCode.SET_CURRENT_POSITION,
          data: 80629,
        });

        await promise;
      });

      it('throws exception with wrong unit provided', async () => {
        const promise = settings.set(BinarySettings.CURRENT_POSITION, 1, Velocity.CENTIMETRES_PER_SECOND);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Velocity:centimetres per second. Provide units of dimension Length.');
      });
    });
  });
});
