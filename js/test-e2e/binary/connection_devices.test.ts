import { expect } from 'chai';

import { Connection, CommandCode } from '../../src/binary';
import { TEST_HOST, TEST_PORT } from './tcp';
import { TestServerWrapper } from './test_server_wrapper';
import { DeviceAddressConflictException, InvalidDataException, NoDeviceFoundException, NotSupportedException } from '../../src';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCB2 } from './devices/xmcb2';
import { DeviceTRS60 } from './devices/trs60';

const server = new TestServerWrapper();

describe('Connection (Binary)', () => {
  let connection: Connection;
  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT, { useMessageIds: false });
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('detectDevices', () => {
    it('returns devices which respond to broadcast', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await server.push({
        device: 7,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });
      await server.push({
        device: 3,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      const devices = await detectPromise;
      expect(devices.map(device => device.deviceAddress)).to.deep.eq([1, 3, 7]);
    });

    it('throws exception when there is conflict in device numbers', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await server.push({
        device: 3,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });
      await server.push({
        device: 1,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });
      await server.push({
        device: 3,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      await expect(detectPromise).to.be.rejectedWith(DeviceAddressConflictException);

      const error: DeviceAddressConflictException = await detectPromise.catch(e => e);
      expect(error.details.deviceAddresses).to.have.members([1, 3, 3]);
    });

    it('throws exception when device address is invalid', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      await server.pop();
      await server.push({
        device: 0,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      await expect(detectPromise).to.be.rejectedWith(InvalidDataException);
      const error: InvalidDataException = await detectPromise.catch(e => e);
      expect(error.message).to.eq('Invalid device address: 0');
    });

    it('identifies devices upon detection', async () => {
      const detectPromise = connection.detectDevices();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await server.push({
        device: 2,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });
      await server.push({
        device: 5,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      await DeviceXLHM.identifyNumber(2, server);
      await DeviceXMCB2.identifyNumber(5, server);

      const devices = await detectPromise;
      expect(devices.map(device => device.deviceAddress)).to.deep.eq([2, 5]);

      expect(devices[0].identity!.name).to.eq('X-LHM100A');
      expect(devices[1].identity!.name).to.eq('X-MCB2');
    });

    it('throws exception when no device responds', async () => {
      const detectPromise = connection.detectDevices();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await expect(detectPromise).to.be.rejectedWith(NoDeviceFoundException);
      const exception: NoDeviceFoundException = await detectPromise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot find any device on the port. Make sure that you have opened correct port and devices are powered on.'
      );
    });

    it('throws exception when a device has Auto-Reply Disabled', async () => {
      const detectPromise = connection.detectDevices();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await server.push({
        device: 3,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      const autoReplyCmd = await server.pop();
      expect(autoReplyCmd.command).to.equal(CommandCode.RETURN_SETTING);
      expect(autoReplyCmd.data).to.equal(CommandCode.SET_AUTO_REPLY_DISABLED_MODE);
      await server.push({
        device: 3,
        command: CommandCode.SET_AUTO_REPLY_DISABLED_MODE,
        data: 1
      });

      await expect(detectPromise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await detectPromise.catch(e => e);
      expect(exception.message).to.eq(
        'The library cannot communicate with device 3 because it has Auto-Reply disabled. Please enable Auto-Reply on this device.'
      );
    });

    it('continues identification when Auto-Reply setting does not exist', async () => {
      const detectPromise = connection.detectDevices();

      const cmd = await server.pop();
      expect(cmd.command).to.equal(CommandCode.RETURN_DEVICE_ID);

      await server.push({
        device: 3,
        command: CommandCode.RETURN_DEVICE_ID,
        data: 0
      });

      // T-RS60 FW 5.29 does not have the setting
      await DeviceTRS60.identifyNumber(3, server);

      const devices = await detectPromise;
      expect(devices.map(device => device.deviceAddress)).to.deep.eq([3]);

      expect(devices[0].identity!.name).to.eq('T-RS60A');
    });
  });
});
