import Bluebird from 'bluebird';
import { createServer, Server, Socket } from 'net';

import { PACKET_SIZE } from './protocol';

export const TEST_PORT = 22789 + Number(process.env.JEST_WORKER_ID) * 10;
export const TEST_HOST = '127.0.0.1';

export class BinaryTestServer {
  private tcpListener?: Server;
  private socket: Socket | null = null;
  private packets: Buffer[] = [];
  private waitOnPacket: (() => void)[] = [];
  private waitOnConnection: (() => void) | null = null;

  constructor() {
    this.onPacket = this.onPacket.bind(this);
    this.handleSocket = this.handleSocket.bind(this);
  }

  public async start(): Promise<void> {
    this.tcpListener = createServer(this.handleSocket);

    await new Promise<void>(resolve => this.tcpListener!.listen({
      host: TEST_HOST,
      port: TEST_PORT,
    }, resolve));
  }

  public async stop(): Promise<void> {
    const closePromise = Bluebird.fromCallback(cb => this.tcpListener!.close(cb));
    this.closeSocket();
    await closePromise;
  }

  public closeSocket(): void {
    if (this.packets.length > 0) {
      throw new Error(`Unprocessed packets in the buffer (${this.packets.length}).`);
    }
    if (!this.socket) {
      return;
    }
    this.socket.end();
    this.socket.destroy();
    this.socket = null;

    this.packets = [];
    this.waitOnPacket = [];
    this.waitOnConnection = null;
  }

  public async waitForConnection(): Promise<void> {
    if (!this.socket) {
      await new Promise<void>(resolve => this.waitOnConnection = resolve);
    }
  }

  public async waitForClose(): Promise<void> {
    await new Promise(resolve => this.socket!.once('close', resolve));
  }

  public async pop(): Promise<Buffer> {
    if (this.packets.length === 0) {
      await new Promise<void>(resolve => this.waitOnPacket.push(resolve));
    }

    const [packet] = this.packets.splice(0, 1);
    return packet;
  }

  public async push(packet: Buffer): Promise<void> {
    const buffer = Buffer.from(packet);
    await Bluebird.fromCallback(cb => this.socket!.write(buffer, cb));
  }

  private handleSocket(socket: Socket): void {
    this.closeSocket();
    this.socket = socket;

    let leftOver = Buffer.alloc(0);
    this.socket.on('data', newData => {
      leftOver = Buffer.concat([leftOver, newData]);
      while (leftOver.length >= PACKET_SIZE) {
        const bytes = Buffer.from(leftOver.slice(0, PACKET_SIZE));
        this.onPacket(bytes);
        leftOver = leftOver.slice(PACKET_SIZE);
      }
    });

    const wait = this.waitOnConnection;
    if (wait) {
      this.waitOnConnection = null;
      wait();
    }
  }

  private onPacket(packet: Buffer): void {
    this.packets.push(packet);

    const [wait] = this.waitOnPacket.splice(0, 1);
    if (wait) {
      wait();
    }
  }
}
