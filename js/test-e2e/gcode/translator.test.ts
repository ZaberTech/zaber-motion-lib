import { expect } from 'chai';
import { InvalidOperationException, Length, StreamModeException, Units } from '../../src';
import { Connection, Device, Stream } from '../../src/ascii';
import { Translator } from '../../src/gcode';
import { DeviceXMCC4 } from '../ascii/devices/xmcc4';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { eqAndPush } from '../test_util';
import { noWarnings } from './utils';

describe('Translator', () => {
  const server = new TestServer();
  let eqAndPushX: (pushStr: string | RegExp | null, popStr: string) => Promise<unknown>;

  let translator: Translator;

  let connection: Connection;
  let device: Device;
  let stream: Stream;

  beforeAll(async () => {
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);
  });
  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();

    await DeviceXMCC4.identify(device, server);
    connection.resetIds();

    stream = device.streams.getStream(1);

    const promise = stream.setupLive(1, 2, 4);
    await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
    await eqAndPushX('stream 1 setup live 1 2 4', 'OK IDLE -- 0');
    await promise;
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
    translator = null!;
  });

  describe('setup', () => {
    it('throws on invalid axes', async () => {
      const promise = Translator.setup(stream, {
        axisMappings: [{
          axisIndex: 3,
          axisLetter: 'X',
        }]
      });
      await expect(promise).to.rejectedWith('Invalid axis index 3, only 3 axes are available.');
    });
    it('throws on duplicate axes', async () => {
      const promise = Translator.setup(stream, {
        axisMappings: [{
          axisIndex: 0,
          axisLetter: 'X',
        }, {
          axisIndex: 1,
          axisLetter: 'X',
        }]
      });
      await expect(promise).to.rejectedWith('Duplicate axis: X.');
    });
    it('throws on stream that is not setup', async () => {
      const promise = Translator.setup(device.streams.getStream(2));
      await expect(promise).to.rejectedWith('The stream is disabled. Setup the stream first.');
    });

    it('retrieves position from the stream', async () => {
      const promise = Translator.setup(stream);
      await eqAndPushX('get pos', 'OK IDLE -- 10000 1000 NA -10000');
      await eqAndPushX('get maxspeed', 'OK IDLE -- 1 1 1 1');
      translator = await promise;
      expect(translator.getAxisPosition('X', Length.mm)).to.be.closeTo(1.24, 0.01);
      expect(translator.getAxisPosition('Y', Length.mm)).to.be.closeTo(0.124, 0.01);
      expect(translator.getAxisPosition('Z', Length.mm)).to.be.closeTo(-1.24, 0.01);
    });

    it('uses smallest maxspeed as traverse rate', async () => {
      const promise = Translator.setup(stream);
      await eqAndPushX('get pos', 'OK IDLE -- 0 0 NA 0');
      await eqAndPushX('get maxspeed', 'OK IDLE -- 12345 1234 NA 123');
      translator = await promise;

      const promise2 = translator.translate('G61 G0 X1');
      await eqAndPushX('stream 1 set maxspeed 123', 'OK IDLE -- 0');
      await eqAndPushX(null, 'OK IDLE -- 0');
      noWarnings(await promise2);
    });
  });

  describe('stream buffers', () => {
    beforeEach(async () => {
      stream = device.streams.getStream(2);
      const buffer = device.streams.getBuffer(1);

      const setupPromise = stream.setupStore(buffer, 1, 2);
      await eqAndPushX('stream 2 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 2 setup store 1 2', 'OK IDLE -- 0');
      await setupPromise;

      const promise = Translator.setup(stream);
      await eqAndPushX('get maxspeed', 'OK IDLE -- 1 1 1 1');
      translator = await promise;
    });

    it('works with stream buffer streams', async () => {
      let promise = translator.translate('G90 G0 G9 X1 Y0');
      await eqAndPushX('stream 2 set maxspeed 1', 'OK IDLE -- 0');
      await eqAndPushX('stream 2 line abs 8063 0', 'OK IDLE -- 0');
      await eqAndPushX('stream 2 wait 1', 'OK IDLE -- 0');
      noWarnings(await promise);

      promise = translator.translate('G0 X1 Y1');
      noWarnings(await promise);

      const flushPromise = translator.flush();
      await eqAndPushX('stream 2 line abs 8063 8063', 'OK IDLE -- 0');
      await flushPromise;
    });

    it('does not initialize position from the device', async () => {
      const promise = translator.translate('G91 X1');
      await expect(promise).to.rejectedWith(/Axis X is not initialized/);
    });
  });

  describe('after setup', () => {
    beforeEach(async () => {
      const promise = Translator.setup(stream);
      await eqAndPushX('get pos', 'OK IDLE -- 0 0 0 0');
      await eqAndPushX('get maxspeed', 'OK IDLE -- 1 1 1 1');
      translator = await promise;
    });

    describe('translate', () => {
      it('sends translated commands to stream', async () => {
        const promise = translator.translate('G61 G0 X1 Y2 Z3');
        await eqAndPushX('stream 1 set maxspeed 1', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 line abs 8063 16126 24189', 'OK IDLE -- 0');
        noWarnings(await promise);
      });

      it('throws error on rejected command', async () => {
        const promise = translator.translate('G28');
        await eqAndPushX('stream 1 set maxspeed 1', 'RJ IDLE -- BADDATA');
        await eqAndPushX('stream 1 info', 'OK IDLE -- disabled - - - - -');

        const err = await expect(promise).to.rejectedWith(/disabled/);
        expect(err).be.instanceOf(StreamModeException);
      });

      it('rejected command causes translator to reset position', async () => {
        let promise = translator.translate('G61 G0 X10');
        await eqAndPushX('stream 1 set maxspeed 1', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 on a line abs 80630', 'RJ IDLE -- BADDATA');
        await eqAndPushX('stream 1 info', 'OK IDLE -- disabled - - - - -');
        await expect(promise).to.rejectedWith();

        promise = translator.translate('G91 G0 X10');
        await expect(promise).to.rejectedWith(/Axis X is not initialized/);
      });
    });

    describe('flush', () => {
      it('flushes queued segments', async () => {
        noWarnings(await translator.translate('G64 P1'));
        noWarnings(await translator.translate('G0 X10'));

        const promise2 = translator.flush({ waitUntilIdle: false });
        await eqAndPushX('stream 1 set maxspeed 1', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 on a line abs 80630', 'OK IDLE -- 0');
        await promise2;
      });

      it('waits for stream to become idle', async () => {
        const promise = translator.translate('G28');
        await eqAndPushX(null, 'OK IDLE -- 0');
        await eqAndPushX(null, 'OK IDLE -- 0');
        noWarnings(await promise);

        const promise2 = translator.flush();
        await eqAndPush(server, 1, 1, null, 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, null, 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, null, 'OK IDLE -- 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 4, 'warnings', 'OK IDLE -- 0');
        await promise2;
      });
    });

    describe('resetPosition', () => {
      it('resets position of the translator from the stream', async () => {
        const promise = translator.resetPosition();
        await eqAndPushX('get pos', 'OK IDLE -- 10 -10 NA 20');
        await promise;

        expect(translator.getAxisPosition('X', Units.NATIVE)).to.eq(10);
        expect(translator.getAxisPosition('Y', Units.NATIVE)).to.eq(-10);
        expect(translator.getAxisPosition('Z', Units.NATIVE)).to.eq(20);

        const promise2 = translator.translate('G61 G0 X1 Y1 Z1');
        await eqAndPushX(null, 'OK IDLE -- 0');
        await eqAndPushX('stream 1 line abs 8073 8053 8083', 'OK IDLE -- 0');
        noWarnings(await promise2);
      });

      it('throws error when axis is busy', async () => {
        const promise = translator.resetPosition();
        await eqAndPushX('get pos', 'OK BUSY -- 0 0 0 0');
        await eqAndPush(server, 1, 1, 'get pos', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 2, 'get pos', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 4, 'get pos', 'OK BUSY -- 0');

        const err = await expect(promise).to.rejectedWith(/pos cannot be reliably determined/);
        expect(err).be.instanceOf(InvalidOperationException);
      });

      it('throws error when stream is disabled', async () => {
        let promise = stream.disable();
        await eqAndPushX(null, 'OK IDLE -- 0');
        await promise;

        promise = translator.resetPosition();
        await eqAndPushX('get pos', 'OK IDLE -- 0 0 0 0');
        const err = await expect(promise).to.rejectedWith(/disabled/);
        expect(err).be.instanceOf(StreamModeException);
      });

      it('throws error when segments are queued', async () => {
        noWarnings(await translator.translate('G64 G0 X1 Y1 Z1'));

        const promise = translator.resetPosition();
        const err = await expect(promise).to.rejectedWith(/Position cannot be set/);
        expect(err).be.instanceOf(InvalidOperationException);
      });
    });
  });
});
