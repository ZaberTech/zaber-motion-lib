import { expect } from 'chai';
import { Angle, DeviceDbFailedException, InvalidArgumentException, Length, Units, Velocity } from '../../src';
import { Connection, Device } from '../../src/ascii';
import { AxisTransformation, OfflineTranslator } from '../../src/gcode';
import { DeviceXMCC4 } from '../ascii/devices/xmcc4';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { eqAndPush } from '../test_util';
import { ADR_XY, MCC_XYZ } from './devices';
import { noWarnings, translateBatch } from './utils';

describe('OfflineTranslator', () => {
  let translator: OfflineTranslator;

  afterEach(() => {
    translator = null!;
  });

  describe('setup', () => {
    it('throws on empty axes', async () => {
      const promise = OfflineTranslator.setup({
        deviceId: 0,
        axes: [],
        maxSpeed: { value: 1 }
      });
      const err = await expect(promise).to.rejectedWith('No axes defined.');
      expect(err).to.be.instanceOf(InvalidArgumentException);
    });

    it('throws invalid IDs', async () => {
      const promise = OfflineTranslator.setup({
        deviceId: 0,
        axes: [{
          peripheralId: 1,
          microstepResolution: 1,
        }],
        maxSpeed: { value: 1 }
      });
      const err = await expect(promise).to.rejectedWith(/Cannot find product with device ID 0/);
      expect(err).be.instanceOf(DeviceDbFailedException);
    });

    it('throws on duplicate mapping letter', async () => {
      const promise = OfflineTranslator.setup(MCC_XYZ, {
        axisMappings: [{
          axisLetter: 'X',
          axisIndex: 1,
        }, {
          axisLetter: 'X',
          axisIndex: 2,
        }]
      });
      const err = await expect(promise).to.rejectedWith('Duplicate axis: X.');
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('throws on duplicate mapping index', async () => {
      const promise = OfflineTranslator.setup(MCC_XYZ, {
        axisMappings: [{
          axisLetter: 'X',
          axisIndex: 1
        }, {
          axisLetter: 'Y',
          axisIndex: 1,
        }]
      });
      const err = await expect(promise).to.rejectedWith('Duplicate axis index: 1.');
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('throws on invalid mapping', async () => {
      const promise = OfflineTranslator.setup(MCC_XYZ, {
        axisMappings: [{
          axisLetter: 'W',
          axisIndex: 1
        }]
      });
      const err = await expect(promise).to.rejectedWith('Invalid axis letter: W.');
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('throws on immobile axes', async () => {
      const promise = OfflineTranslator.setup({
        deviceId: 30331,
        axes: [{
          peripheralId: 0,
          microstepResolution: 1,
        }],
        maxSpeed: { value: 1 }
      });
      const err = await expect(promise).to.rejectedWith('Axis X appears to be immobile.');
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('throws on invalid units for traverse rate', async () => {
      const promise = OfflineTranslator.setup({
        deviceId: 30331,
        axes: [{
          peripheralId: 43211,
          microstepResolution: 1,
        }],
        maxSpeed: { value: 1, unit: Length.mm }
      });
      const err = await expect(promise).to.rejectedWith([
        'Cannot convert traverse rate using axis X: ConversionFailed:',
        'Invalid units: Length:millimetres. Provide units of dimension Velocity.'
      ].join(' '));
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('works with integrated products', async () => {
      translator = await OfflineTranslator.setup({
        deviceId: 50081,
        axes: [{
          peripheralId: 0,
          microstepResolution: 64,
        }],
        maxSpeed: { value: 1, unit: Velocity.MILLIMETRES_PER_SECOND }
      });

      let result = translator.translate('G28');
      expect(result).to.deep.eq({
        warnings: [],
        commands: [
          'set maxspeed 13210',
          'line abs 0',
        ],
      });
      result = translator.translate('G61 G0 X10');
      expect(result).to.deep.eq({
        warnings: [],
        commands: [
          'line abs 80630',
        ],
      });
    });

    it('throws when resolution is not provided', async () => {
      const promise = OfflineTranslator.setup({
        deviceId: 30331,
        axes: [{
          peripheralId: 43211,
        }],
        maxSpeed: {
          value: 1,
          unit: Velocity['mm/s'],
        },
      });
      const err = await expect(promise).to.rejectedWith(
        'Resolution not provided for axis with peripheral 43211.'
      );
      expect(err).be.instanceOf(InvalidArgumentException);
    });

    it('uses resolution of 1 for axes without resolution', async () => {
      translator = await OfflineTranslator.setup(ADR_XY);

      const result = translator.translate('G61 G90 G0 X1');
      expect(result).to.deep.eq({
        warnings: [],
        commands: [
          'set maxspeed 1638400',
          'on a line abs 1000000',
        ],
      });
    });

    it('throws on invalid transformations', async () => {
      const setup = (axisTransformations: AxisTransformation[]) => OfflineTranslator.setup({
        deviceId: 30331,
        axes: [{
          peripheralId: 43211,
          microstepResolution: 1,
        }, {
          peripheralId: 43211,
          microstepResolution: 1,
        }],
        maxSpeed: { value: 1 },
      }, {
        axisTransformations,
      });
      let err = await expect(setup([{
        axisLetter: 'R',
      }])).to.rejectedWith('Transformation specified for unknown axis: R.');
      expect(err).be.instanceOf(InvalidArgumentException);

      err = await expect(setup([{
        axisLetter: 'X',
        scaling: 1,
      }, {
        axisLetter: 'X',
        translation: { value: 10 },
      }])).to.rejectedWith('Transformation already specified for axis: X.');
      expect(err).be.instanceOf(InvalidArgumentException);
    });
  });

  describe('setup from device', () => {
    const server = new TestServer();
    let eqAndPushX: (pushStr: string | RegExp | null, popStr: string) => Promise<unknown>;

    let connection: Connection;
    let device: Device;

    beforeAll(async () => {
      eqAndPushX = eqAndPush.bind(null, server, 1, 0);
      await server.start();
    });
    afterAll(async () => {
      await server.stop();
    });

    beforeEach(async () => {
      connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
      device = connection.getDevice(1);
      await server.waitForConnection();

      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary);
      connection.resetIds();
    });

    afterEach(async () => {
      await connection.close();
      server.closeSocket();
    });

    it('applies the unit conversions from selected access', async () => {
      const promise = OfflineTranslator.setupFromDevice(device, [2, 3]);
      await eqAndPushX('get maxspeed', 'OK IDLE -- 200 200 200 200');
      translator = await promise;

      translator.setAxisPosition('X', 1, Length.mm);
      translator.setAxisPosition('Y', 10, Angle.DEGREES);

      translateBatch(translator, [
        'G91 G0 X1 Y10',
      ]);

      expect(translator.getAxisPosition('X', Length.mm)).to.be.closeTo(2, 0.001);
      expect(translator.getAxisPosition('Y', Angle.DEGREES)).to.be.closeTo(20, 0.001);
    });

    it('allows for complex axis/stream configurations', async () => {
      const promise = OfflineTranslator.setupFromDevice(device, [2, 3, 4], {
        axisMappings: [{
          axisIndex: 1,
          axisLetter: 'X',
        }, {
          axisIndex: 2,
          axisLetter: 'Y',
        }]
      });
      await eqAndPushX('get maxspeed', 'OK IDLE -- 200 200 200 200');
      translator = await promise;

      const commands = translateBatch(translator, [
        'G90 G0 X1 Y10',
      ]);
      expect(commands).to.deep.eq([
        'set maxspeed 200',
        'on b c line abs 6400 64000',
      ]);
    });

    it('sets traverse rate to minimal maxspeed of participating axes', async () => {
      const promise = OfflineTranslator.setupFromDevice(device, [2, 3]);
      await eqAndPushX('get maxspeed', 'OK IDLE -- 200 300 400 NA');
      translator = await promise;

      const commands = translateBatch(translator, [
        'G90 G0 X100 Y360',
        'M30',
      ]);
      expect(commands[0]).to.eq('set maxspeed 300');
    });

    it('throws error in invalid axes', async () => {
      let promise = OfflineTranslator.setupFromDevice(device, [0]);
      let err = await expect(promise).to.rejectedWith(
        'Target axis out of range: 0'
      );
      expect(err).be.instanceOf(InvalidArgumentException);

      promise = OfflineTranslator.setupFromDevice(device, [4, 5]);
      err = await expect(promise).to.rejectedWith(
        'Target axis out of range: 5'
      );
      expect(err).be.instanceOf(InvalidArgumentException);
    });
  });

  describe('parsing', () => {
    beforeEach(async () => {
      translator = await OfflineTranslator.setup(MCC_XYZ);
      noWarnings(translator.translate('G61 G28'));
    });

    it('parses g-code with all its weirdness', () => {
      const result = translator.translate('G0X-10Y+0.(comment)9 Z - 5.;bla bla G1');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'line abs -80630 7257 -40315',
      ]);
    });

    it('throws error on all sort of incorrect syntax', () => {
      expect(() => translator.translate('G90(')).to.throw('Comment is missing closing bracket.');
      expect(() => translator.translate('G90((')).to.throw('Nested comment is forbidden.');
      expect(() => translator.translate('G90)')).to.throw('Unexpected comment end.');
      expect(() => translator.translate('X[1+2]')).to.throw('Expressions are not supported.');
      expect(() => translator.translate('#345=1231')).to.throw('Parameters are not supported.');
      expect(() => translator.translate('GM')).to.throw('Word is missing a value.');
      expect(() => translator.translate('G1.2.3')).to.throw('Cannot parse word value: strconv.ParseFloat: parsing "1.2.3": invalid syntax');
    });

    it('sorts G-code according to execution order', () => {
      const result = translator.translate('X10 G1 Y5 F2');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'set maxspeed 440',
        'on a b line abs 80630 40315',
      ]);
    });

    it('throws error on colliding modal word', () => {
      expect(() => translator.translate('G90 G91')).to.throw('Conflict between G90 and G91, they both belong the same modal group.');
    });
    it('throws error on colliding word with coordinates', () => {
      expect(() => translator.translate('G28 G0')).to.throw('Conflict between G28 and G0, they both use coordinates.');
    });

    it('returns warning on unknown code or parameter', () => {
      const result = translator.translate('G666 G-13 J10');
      expect(result).to.deep.eq({
        warnings: [{
          message: 'Unknown command G666.',
          fromBlock: 0,
          toBlock: 4,
        }, {
          message: 'Unknown command G-13.',
          fromBlock: 5,
          toBlock: 9,
        }, {
          message: 'Axes not defined, movement ignored.',
          fromBlock: 10,
          toBlock: 13,
        }, {
          message: 'Word unused J10.',
          fromBlock: 10,
          toBlock: 13,
        }],
        commands: [],
      });
    });
  });

  describe('translation', () => {
    beforeEach(async () => {
      translator = await OfflineTranslator.setup(MCC_XYZ);
    });

    it('throws an exception if axis is not initialized', () => {
      expect(() => translator.translate('G91 G0 X10')).to.throw(
        'Axis X is not initialized. Use G28, G90 or setAxisPosition method'
      );
    });

    describe('G28', () => {
      it('initializes axes', () => {
        const result = translator.translate('G28');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 0 0 0',
        ]);

        // incremental move requires known position
        translator.translate('G91 G0 X10 Y10 Z10');
      });

      it('performs additional movement if axes are specified', () => {
        translator.setAxisPosition('X', 0, Length.mm);
        translator.setAxisPosition('Y', 0, Length.mm);
        translator.setAxisPosition('Z', 0, Length.mm);

        const result = translator.translate('G28 G91 X20 Y10 Z5');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 161260 80630 40315',
          'line abs 0 0 0',
        ]);
      });

      it('moves only specified axes', () => {
        translator.setAxisPosition('Z', 0, Length.mm);

        const result = translator.translate('G28 G91 Z5');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 13210',
          'on c line abs 40315',
          'on c line abs 0',
        ]);
      });

      it('moves to position set by setAxisHomePosition', () => {
        translator.setAxisHomePosition('X', 123, Units.NATIVE);
        translator.setAxisHomePosition('Z', 456, Units.NATIVE);
        const result = translator.translate('G28');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 123 0 456',
        ]);
      });

      it('moves to coordinates saved by G28.1', () => {
        const commands = translateBatch(translator, [
          'G90 G0 X1 Y2 Z3',
          'G28.1',
          'G28 X0 Y0 Z0',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 8063 16126 24189',
          'line abs 0 0 0',
          'line abs 8063 16126 24189',
        ]);
      });
    });

    describe('G30/G30.1', () => {
      it('acts like G28 except with secondary coordinates', () => {
        const commands = translateBatch(translator, [
          'G30',
          'G90 G0 X1 Y2 Z3',
          'G30.1',
          'G30 X0 Y0 Z0',
          'G30 Z0',
          'G28',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 0 0 0',
          'line abs 8063 16126 24189',
          'line abs 0 0 0',
          'line abs 8063 16126 24189',
          'on c line abs 0',
          'on c line abs 24189',
          'line abs 0 0 0',
        ]);
      });

      it('moves to position set by setAxisSecondaryHomePosition', () => {
        translator.setAxisSecondaryHomePosition('X', 123, Units.NATIVE);
        translator.setAxisSecondaryHomePosition('Z', 456, Units.NATIVE);
        const result = translator.translate('G30');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 123 0 456',
        ]);
      });
    });

    describe('initialized', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G61 G28'));
      });

      it('throws error if feedrate is not set', () => {
        expect(() => translator.translate('G1 X10')).to.throw('Feed rate is not set.');
        expect(() => translator.translate('G2 I10')).to.throw('Feed rate is not set.');
      });

      it('returns warning if feed rate is rounded away from zero', () => {
        const result = translator.translate('G1 X1 F0.0001');
        expect(result.warnings).to.deep.eq([{
          fromBlock: 0,
          toBlock: 2,
          message: 'Feed rate rounded to minimum speed.',
        }]);
        expect(result.commands).to.deep.eq([
          'set maxspeed 1',
          'on a line abs 8063',
        ]);
      });

      describe('setTraverseRate', () => {
        it('changes the traverse rate', () => {
          translator.setTraverseRate(2, Velocity['mm/s']);

          let result = translator.translate('G0 X0');
          noWarnings(result);
          expect(result.commands).to.deep.eq([
            'set maxspeed 26421',
            'on a line abs 0',
          ]);

          translator.setTraverseRate(1, Velocity['mm/s']);

          result = translator.translate('G0 X0');
          noWarnings(result);
          expect(result.commands).to.deep.eq([
            'set maxspeed 13210',
            'on a line abs 0',
          ]);
        });
        it('throws error on invalid traverse rate', () => {
          expect(() =>
            translator.setTraverseRate(-2, Units.NATIVE)
          ).to.throw('Invalid traverse rate -2');
        });
      });

      describe('setFeedRateOverride', () => {
        it('changes the feed rate for next command', () => {
          let result = translator.translate('G1 X0 F100');
          noWarnings(result);
          expect(result.commands).to.deep.eq([
            'set maxspeed 22017',
            'on a line abs 0',
          ]);

          translator.setFeedRateOverride(2);

          result = translator.translate('X0');
          noWarnings(result);
          expect(result.commands).to.deep.eq([
            'set maxspeed 44035',
            'on a line abs 0',
          ]);
        });

        it('sets maxspeed to lowest for 0', () => {
          translator.setFeedRateOverride(0);

          const result = translator.translate('G1 X0 F100');
          expect(result.commands).to.deep.eq([
            'set maxspeed 1',
            'on a line abs 0',
          ]);
        });

        it('throws error on invalid override', () => {
          expect(() =>
            translator.setFeedRateOverride(-1)
          ).to.throw('Invalid feed rate override -1.00000');
        });
      });
    });

    describe('line', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G61 G28'));
      });

      it('interprets position as absolute on G90', () => {
        let result = translator.translate('G90 G0 X10 Y20 Z30');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 80630 161260 241890',
        ]);
        result = translator.translate('G0 X30 Y20 Z10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 241890 161260 80630',
        ]);
      });

      it('interprets position as relative on G91', () => {
        let result = translator.translate('G91 G0 X10 Y20 Z30');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 80630 161260 241890',
        ]);
        result = translator.translate('G0 X30 Y20 Z10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 322520 322520 322520',
        ]);
        result = translator.translate('G0 X-40 Y-40 Z-40');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 0 0 0',
        ]);
      });

      it('interprets block without all axes and mode', () => {
        let result = translator.translate('G0 Z20');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on c line abs 161260',
        ]);
        result = translator.translate('Y30');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on b line abs 241890',
        ]);
      });

      it('gives warning on empty movement', () => {
        const result = translator.translate('G0');
        expect(result).to.deep.eq({
          warnings: [{
            message: 'Axes not defined, movement ignored.',
            fromBlock: 0,
            toBlock: 2,
          }],
          commands: [],
        });
      });
    });

    describe('arc', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G61 G28 F1'));
      });

      it('interprets center offset as absolute on G90.1', () => {
        let result = translator.translate('G90.1 G2 X10 Y10 I0 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'on a b arc abs cw 0 80630 80630 80630',
        ]);
        result = translator.translate('G3 X20 Y20 I20 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs ccw 161260 80630 161260 161260',
        ]);
        result = translator.translate('X0 Y0 I20 J0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs ccw 161260 0 0 0',
        ]);
      });

      it('interprets center offset as relative on G91.1', () => {
        let result = translator.translate('G91.1 G2 X10 Y10 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'on a b arc abs cw 0 80630 80630 80630',
        ]);
        result = translator.translate('G3 X20 Y20 I10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs ccw 161260 80630 161260 161260',
        ]);
        result = translator.translate('X0 Y0 J-20');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs ccw 161260 0 0 0',
        ]);
      });

      it('does circles when no end is specified', () => {
        const result = translator.translate('G2 I10 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'on a b arc abs cw 80629 80629 0 0',
        ]);
      });

      it('throws error when center is not defined', () => {
        expect(() => translator.translate('G17 G2 X10')).to.throw('At least one of parameter I or J must be specified as non-zero number.');
        expect(() => translator.translate('G17 G2 X10 I0 J0'))
          .to.throw('At least one of parameter I or J must be specified as non-zero number.');
        expect(() => translator.translate('G18 G2 X10')).to.throw('At least one of parameter I or K must be specified as non-zero number.');
        expect(() => translator.translate('G19 G2 Y10')).to.throw('At least one of parameter J or K must be specified as non-zero number.');
      });
      it('throws error when center is not defined (G90.1)', () => {
        noWarnings(translator.translate('G90.1'));
        expect(() => translator.translate('G17 G2 X10')).to.throw('Both parameters I and J must be specified.');
        expect(() => translator.translate('G17 G2 X10 I10')).to.throw('Both parameters I and J must be specified.');
        expect(() => translator.translate('G17 G2 X10 J10')).to.throw('Both parameters I and J must be specified.');
        expect(() => translator.translate('G18 G2 X10')).to.throw('Both parameters I and K must be specified.');
        expect(() => translator.translate('G19 G2 Y10')).to.throw('Both parameters J and K must be specified.');
      });
      it('throws error on radius format', () => {
        expect(() => translator.translate('G2 X10 R10')).to.throw('Radius arc not supported.');
      });
      it('throws error on movement that is not an arc', () => {
        expect(() => translator.translate('G2 X10 Y10 I-10')).to.throw(
          'Specified coordinates do not form an arc (radius difference 12.360680).');
      });

      it('does arcs on all planes', () => {
        let result = translator.translate('G18 G2 X10 Z10 I10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'on a c arc abs cw 80630 0 80630 80630',
        ]);
        result = translator.translate('G17 G2 X20 Y10 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 80630 80630 161260 80630',
        ]);
        result = translator.translate('G19 G2 Y20 Z0 K-10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on b c arc abs cw 80630 0 161260 0',
        ]);
      });

      it('does helix when additional coordinates are specified', () => {
        let result = translator.translate('G17 G2 X10 Y10 J10 Z1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'helix abs cw 0 80630 80630 80630 8063',
        ]);
        result = translator.translate('G18 G2 X0 Z11 I-10 Y1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a c b helix abs cw 0 8063 0 88693 8063',
        ]);
        result = translator.translate('G19 G3 Y11 Z1 K-10 X-1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on b c a helix abs ccw 8062 8062 88693 8063 -8063',
        ]);
      });

      it('replaces arc with line if the arc is below device resolution', () => {
        const result = translator.translate('G2 X0.00001 Y0 I0.000005 J100');
        expect(result.warnings[0].message).to.eq('Arc is below device resolution.');
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'on a b line abs 0 0',
        ]);
      });
    });

    describe('get/setAxisPosition', () => {
      it('setting position initializes axes', () => {
        translator.setAxisPosition('X', 10, Length.mm);
        const result = translator.translate('G0 X10');
        noWarnings(result);
      });

      it('setting position changes internal position', () => {
        translator.setAxisPosition('X', 10, Length.INCHES);
        const result = translator.translate('G91 G0 X25.4');
        noWarnings(result);
        expect(translator.getAxisPosition('X', Length.INCHES)).to.be.closeTo(11, 0.0001);
      });

      it('setting position changes internal position (native units)', () => {
        translator.setAxisPosition('X', 10000, Units.NATIVE);
        const result = translator.translate('G91 G0 X0.01');
        noWarnings(result);
        expect(translator.getAxisPosition('X', Units.NATIVE)).to.be.closeTo(10080.63, 0.0001);
      });

      it('throws error on invalid axis or units', () => {
        translator.setAxisPosition('X', 0, Units.NATIVE);
        expect(() => translator.getAxisPosition('B', Length.mm)).to.throw('Invalid axis B.');
        expect(() => translator.getAxisPosition('X', Angle.DEGREES)).to.throw(
          'Dimensions of unit Length:millimetres and Angle:degrees are not compatible.'
        );
        expect(() => translator.setAxisPosition('B', 10, Length.mm)).to.throw('Invalid axis B.');
        expect(() => translator.setAxisPosition('X', 10, Angle.DEGREES)).to.throw(
          'Dimensions of unit Length:millimetres and Angle:degrees are not compatible.'
        );
      });

      it('throws if the position is not initialized', () => {
        expect(() => translator.getAxisPosition('X', Length.mm)).to.throw(
          'Position of axis X is not initialized.'
        );
      });

      it('resets the maxspeed when position is set', () => {
        let result = translator.translate('G61 G28');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');

        translator.setAxisPosition('X', 10, Length.mm);

        result = translator.translate('X20');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');
      });
    });

    describe('G4 Dwell', () => {
      it('results in wait command', () => {
        const result = translator.translate('G4 P0.32111');
        noWarnings(result);
        expect(result.commands).to.deep.eq(['wait 321']);
      });

      it('no parameter or small value result in minimal stop of 1 ms', () => {
        let result = translator.translate('G4');
        noWarnings(result);
        expect(result.commands).to.deep.eq(['wait 1']);

        result = translator.translate('G4 P0.00001');
        noWarnings(result);
        expect(result.commands).to.deep.eq(['wait 1']);
      });

      it('throws error on negative wait', () => {
        expect(() => translator.translate('G4 P-0.0001')).to.throw('Invalid dwell time P-0.0001.');
      });
    });

    it('preserves previous state if error in translation occurs', () => {
      noWarnings(translator.translate('G61 G91 G28'));
      let result = translator.translate('F1 G1 Y10');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'set maxspeed 220', 'on b line abs 80630',
      ]);

      expect(() => translator.translate('F10 G18 G2 X10 Z20 I10 K10')).to.throw();

      // expectation: axes positions, movement mode, feed rate are unchanged

      expect(translator.getAxisPosition('X', Length.mm)).to.eq(0);
      expect(translator.getAxisPosition('Y', Length.mm)).to.eq(10);
      expect(translator.getAxisPosition('Z', Length.mm)).to.eq(0);

      result = translator.translate('X5 Y-10 Z5');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'line abs 40315 0 40315',
      ]);
    });

    describe('M30,M2', () => {
      it('resets maxspeed of stream', () => {
        let result = translator.translate('G61 G90 G0 X0');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');

        noWarnings(translator.translate('M30'));

        result = translator.translate('X0');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');
      });

      it('resets axis positions', () => {
        const result = translator.translate('G28');
        noWarnings(result);

        noWarnings(translator.translate('M30'));

        expect(() => translator.translate('G91 G0 X10')).to.throw(/Axis X is not initialized/);
      });
    });

    describe('resetAfterStreamError', () => {
      /* After stream error translator may be out of sync with the stream.
      Therefore position needs to be initialized again and maxspeed reset. */

      it('resets maxspeed of stream', () => {
        let result = translator.translate('G61 G90 G0 X0');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');

        translator.resetAfterStreamError();

        result = translator.translate('X0');
        noWarnings(result);
        expect(result.commands[0]).to.eq('set maxspeed 13210');
      });

      it('resets axis positions', () => {
        const result = translator.translate('G28');
        noWarnings(result);

        translator.resetAfterStreamError();

        expect(() => translator.translate('G91 G0 X10')).to.throw(/Axis X is not initialized/);
      });
    });

    describe('coordinate systems', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G61 G28 F1'));
        noWarnings(translator.translate('G10 L2 P1 X1 Y-10 Z5'));
        noWarnings(translator.translate('G10 L2 P2 X-10 Y1 Z-5'));
      });

      it('offsets coordinates of movements', () => {
        let result = translator.translate('G90 G0 X0 Y0 Z0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 8063 -80630 40315',
        ]);
        result = translator.translate('G91 G1 X-1 Y10 Z-5');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'line abs 0 0 0',
        ]);
        result = translator.translate('G91.1 G2 X9 Y20 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 0 80630 80630 80630',
        ]);
        result = translator.translate('G90.1 G2 X19 Y30 I19 J20');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 161260 80630 161260 161260',
        ]);
      });

      it('G53 applies machine coordinates for given line', () => {
        let result = translator.translate('G53 G90 G0 X1 Y1 Z1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 8063 8063 8063',
        ]);
        result = translator.translate('X1 Y1 Z1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 16126 -72567 48378',
        ]);
        result = translator.translate('G53 G1 X0 Y0 Z0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'set maxspeed 220',
          'line abs 0 0 0',
        ]);

        result = translator.translate('G53 G91.1 G2 X10 Y10 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 0 80630 80630 80630',
        ]);
        result = translator.translate('G2 X-1 Y10 J-10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 80630 0 0 0',
        ]);
        result = translator.translate('G53 G90.1 G2 X10 Y10 I0 J10');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'on a b arc abs cw 0 80630 80630 80630',
        ]);
      });

      it('uses last specified set coordinate system', () => {
        let result = translator.translate('G90 G0 X0 Y0 Z0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 8063 -80630 40315',
        ]);

        result = translator.translate('G55 X0 Y0 Z0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs -80630 8063 -40315',
        ]);
        result = translator.translate('X1 Y1 Z1');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs -72567 16126 -32252',
        ]);

        result = translator.translate('G56 X0 Y0 Z0');
        noWarnings(result);
        expect(result.commands).to.deep.eq([
          'line abs 0 0 0',
        ]);
      });

      describe('getAxisCoordinateSystemOffset', () => {
        it('returns offset for axis in given coordinate system', () => {
          expect(translator.getAxisCoordinateSystemOffset('G54', 'X', Length.mm)).to.eq(1);
          expect(translator.getAxisCoordinateSystemOffset('G54', 'Y', Length.cm)).to.eq(-1);
          expect(translator.getAxisCoordinateSystemOffset('G55', 'Z', Length.mm)).to.eq(-5);
        });
        it('throws error for invalid axis or coordinate system', () => {
          expect(() => translator.getAxisCoordinateSystemOffset('G54', 'R', Length.mm)).to.throw('Invalid axis R.');
          expect(() => translator.getAxisCoordinateSystemOffset('G53', 'X', Length.mm)).to.throw('Invalid coordinate system G53.');
          expect(() => translator.getAxisCoordinateSystemOffset('G54', 'X', Angle.DEGREES)).to.throw(
            'Dimensions of unit Length:millimetres and Angle:degrees are not compatible.');
        });
      });

      describe('coordinateSystem', () => {
        it('returns current coordinate system', () => {
          expect(translator.coordinateSystem).to.eq('G54');
          translator.translate('G55');
          expect(translator.coordinateSystem).to.eq('G55');
          translator.translate('G59.2');
          expect(translator.coordinateSystem).to.eq('G59.2');
        });
      });
    });

    describe('G64 trajectory smoothing', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G28 G90'));
        noWarnings(translator.translate('G64 P1'));
      });

      it('inserts arcs to smooth corners', () => {
        const commands = translateBatch(translator, [
          'G0 X50 Y0',
          'X50 Y50',
          'X0 Y50',
          'X0 Y0',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a b line abs 383684 0',
          'on a b arc abs ccw 383684 19466 403150 19466 join maxspeed 13210',
          'on a b line abs 403150 383684 join maxspeed 13210',
          'on a b arc abs ccw 383684 383684 383684 403150 join maxspeed 13210',
          'on a b line abs 19466 403150 join maxspeed 13210',
          'on a b arc abs ccw 19466 383684 0 383684 join maxspeed 13210',
          'on a b line abs 0 0 join maxspeed 13210',
        ]);
      });

      it('inserts arcs to smooth corners (ccw vs cw regression)', () => {
        const commands = translateBatch(translator, [
          'G1 Y100 F1000',
          'G1 X100',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 220173',
          'on b line abs 786833',
          'on a b arc abs cw 19466 786833 19466 806299 join maxspeed 220173',
          'on a line abs 806299 join maxspeed 220173',
        ]);
      });

      it('splits the arc when feed ratio differs', () => {
        const commands = translateBatch(translator, [
          'G1 X50 Y0 F100',
          'X50 Y50 F200',
          'X0 Y50 F300',
          'X0 Y0 F400',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 22017',
          'on a b line abs 383684 0',
          'on a b arc abs ccw 383684 19466 397448 5701 join maxspeed 22017',
          'set maxspeed 44035',
          'on a b arc abs ccw 383684 19466 403150 19466 join maxspeed 44035',
          'on a b line abs 403150 383684 join maxspeed 44035',
          'on a b arc abs ccw 383684 383684 397448 397448 join maxspeed 44035',
          'set maxspeed 66052',
          'on a b arc abs ccw 383684 383684 383684 403150 join maxspeed 66052',
          'on a b line abs 19466 403150 join maxspeed 66052',
          'on a b arc abs ccw 19466 383684 5701 397448 join maxspeed 66052',
          'set maxspeed 88069',
          'on a b arc abs ccw 19466 383684 0 383684 join maxspeed 88069',
          'on a b line abs 0 0 join maxspeed 88069',
        ]);
      });

      it('smooths segments with distinct axes', () => {
        const commands = translateBatch(translator, [
          'G0 X50',
          'Y50',
          'X0',
          'Y0',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 383684',
          'on a b arc abs ccw 383684 19466 403150 19466 join maxspeed 13210',
          'on b line abs 383684 join maxspeed 13210',
          'on a b arc abs ccw 383684 383684 383684 403150 join maxspeed 13210',
          'on a line abs 19466 join maxspeed 13210',
          'on a b arc abs ccw 19466 383684 0 383684 join maxspeed 13210',
          'on b line abs 0 join maxspeed 13210',
        ]);
      });

      it('smooths segments with incomplete axes', () => {
        const commands = translateBatch(translator, [
          'X10',
          'Y10X20',
          'Y20',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 40095',
          'on a b arc abs ccw 40095 97861 109293 28663 join maxspeed 13210',
          'on a b line abs 132753 52123 join maxspeed 13210',
          'on a b arc abs ccw 63931 120945 161260 120945 join maxspeed 13210',
          'on b line abs 161260 join maxspeed 13210',
        ]);
      });

      it('eliminates lines completely if it can', () => {
        const commands = translateBatch(translator, [
          'G64 P25',
          'G0 X50 Y0',
          'X50 Y50',
          'X0 Y50',
          'X0 Y0',
          'X10 Y0',
          'X10 Y10',
          'X0 Y10',
          'X0 Y0',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a b line abs 201575 0',
          'on a b arc abs ccw 201575 201575 403150 201575 join maxspeed 13210',
          'on a b arc abs ccw 201575 201575 201575 403150 join maxspeed 13210',
          'on a b arc abs ccw 201575 201575 0 201575 join maxspeed 13210',
          'on a b line abs 0 40315 join maxspeed 13210',
          'on a b arc abs ccw 40315 40315 40315 0 join maxspeed 13210',
          'on a b arc abs ccw 40315 40315 80630 40315 join maxspeed 13210',
          'on a b arc abs ccw 40315 40315 40315 80630 join maxspeed 13210',
          'on a b arc abs ccw 40315 40315 0 40315 join maxspeed 13210',
          'on a b line abs 0 0 join maxspeed 13210',
        ]);
      });

      it('flushes remaining command on flush call and M30, M2 codes', () => {
        let result = noWarnings(translator.translate('G0 X50'));
        expect(result.commands).to.deep.eq([]);
        expect(translator.flush()).to.deep.eq([
          'on a line abs 403150',
        ]);

        result = noWarnings(translator.translate('G0 Y50'));
        expect(result.commands).to.deep.eq([]);
        result = noWarnings(translator.translate('M30'));
        expect(result.commands).to.deep.eq([
          'on b line abs 403150',
        ]);

        result = noWarnings(translator.translate('G0 Z50'));
        expect(result.commands).to.deep.eq([]);
        result = noWarnings(translator.translate('M2'));
        expect(result.commands.slice(1)).to.deep.eq([
          'on c line abs 403150',
        ]);
      });

      it('throws error when trying to set position with segments queued', () => {
        noWarnings(translator.translate('G0 X50'));
        expect(() => translator.setAxisPosition('X', 10, Units.NATIVE)).to.throw(/Position cannot be set/);
      });

      it('does not alter subsequent movements of one axis', () => {
        const commands = translateBatch(translator, [
          'G0 X50',
          'X100',
          'X150',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 403150',
          'on a line abs 806299',
          'on a line abs 1209449',
        ]);
      });

      it('does smooth movements of incompatible axes', () => {
        const commands = translateBatch(translator, [
          'G0 X50 Y50',
          'Y50 Z50',
          'Z50 X50',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a b line abs 403150 403150',
          'on b c line abs 403150 403150',
          'on a c line abs 403150 403150',
        ]);
      });

      it('does not join arc if the omitted line was not joined', () => {
        const commands = translateBatch(translator, [
          'G64 P10',
          'G0 X10 G9',
          'X20',
          'Y20',
          'M30',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 80630',
          'wait 1',
          'on a b arc abs ccw 80630 80630 161260 80630', // arc not joined
          'on b line abs 161260 join maxspeed 13210',
        ]);
      });
    });

    describe('G61.1 exact stop mode', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G61.1 G90 G28 F1'));
      });

      it('it adds a little delay after every move', () => {
        const commands = translateBatch(translator, [
          'G0 X1',
          'G1 Y1',
          'G2 X0 Y0 I-1',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 8063',
          'wait 1',
          'set maxspeed 220',
          'on b line abs 8063',
          'wait 1',
          'on a b arc abs cw 0 8063 0 0',
          'wait 1',
        ]);
      });

      it('issues just one wait with G9', () => {
        expect(translateBatch(translator, ['G9 G1 X1'])).to.deep.eq([
          'set maxspeed 220',
          'on a line abs 8063',
          'wait 1',
        ]);
      });
    });

    describe('G9 exact stop', () => {
      beforeEach(() => {
        noWarnings(translator.translate('G90 F1'));
      });

      it('it adds a little delay after every move', () => {
        const commands = translateBatch(translator, [
          'G9 G28 Z1',
          'G9 G0 X1',
          'G9 G1 Y1',
          'G9 G2 X0 Y0 I-1',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 13210',
          'on c line abs 8063',
          'wait 1',
          'on c line abs 0',
          'wait 1',
          'on a line abs 8063',
          'wait 1',
          'set maxspeed 220',
          'on b line abs 8063',
          'wait 1',
          'on a b arc abs cw 0 8063 0 0',
          'wait 1',
        ]);
      });

      it('does not interfere with trajectory smoothing', () => {
        noWarnings(translator.translate('G64 G28'));
        const commands = translateBatch(translator, [
          'G0 X1',
          'G9 G0 Y1',
          'G9 G0 Z1',
        ]);
        expect(commands).to.deep.eq([
          'on a line abs 6116',
          'on a b arc abs ccw 6116 1947 8063 1947 join maxspeed 13210',
          'on b line abs 8063 join maxspeed 13210',
          'wait 1',
          'on c line abs 8063',
          'wait 1',
        ]);
      });
    });

    describe('M64/M65', () => {
      it('sets digital output on/off', () => {
        const commands = translateBatch(translator, [
          'M64 P1',
          'M65 P3',
        ]);
        expect(commands).to.deep.eq([
          'io set do 1 1',
          'io set do 3 0',
        ]);
      });

      it('throws error for invalid channels', () => {
        expect(() => translator.translate('M64 P0')).to.throw(/not a valid IO channel/);
        expect(() => translator.translate('M64 P1.1')).to.throw(/not a valid IO channel/);
        expect(() => translator.translate('M64 P-1')).to.throw(/not a valid IO channel/);
      });
    });

    describe('M66', () => {
      it('waits for digital input', () => {
        const commands = translateBatch(translator, [
          'M66 P1 L3',
          'M66 P3 L4',
        ]);
        expect(commands).to.deep.eq([
          'wait io di 1 == 1',
          'wait io di 3 == 0',
        ]);
      });
      it('throws error for invalid channels', () => {
        expect(() => translator.translate('M66 P0')).to.throw(/not a valid IO channel/);
      });
      it('does not support analog input', () => {
        expect(() => translator.translate('M66 E1 L3')).to.throw(/Waiting for analog input \(parameter E\) is not supported/);
      });
      it('does not support modes 0,1,2', () => {
        expect(() => translator.translate('M66 P1 L0')).to.throw(/Mode 0 is not supported/);
        expect(() => translator.translate('M66 P1 L1')).to.throw(/Mode 1 is not supported/);
        expect(() => translator.translate('M66 P1 L2')).to.throw(/Mode 2 is not supported/);
      });
      it('does not support timeout', () => {
        expect(() => translator.translate('M66 P1 L3 Q10')).to.throw(/Timeout \(parameter Q\) is not supported/);
      });
    });

    describe('M68', () => {
      it('sets analog output', () => {
        const commands = translateBatch(translator, [
          'M68 E1 Q1.45',
          'M68 E3 Q3.7',
        ]);
        expect(commands).to.deep.eq([
          'io set ao 1 1.45',
          'io set ao 3 3.7',
        ]);
      });

      it('throws error for invalid channels', () => {
        expect(() => translator.translate('M68 E0')).to.throw(/not a valid IO channel/);
      });
    });

    describe('M700 passthrough', () => {
      it('passes down commands in comments', () => {
        const commands = translateBatch(translator, ['M700 (command 1)(command 2)']);
        expect(commands).to.deep.eq([
          'command 1',
          'command 2',
        ]);
      });

      it('returns warnings when there is nothing to passthrough', () => {
        const result = translator.translate('M700');
        expect(result).to.deep.eq({
          warnings: [{
            fromBlock: 0,
            toBlock: 4,
            message: 'No commands to pass through.',
          }],
          commands: [],
        });
      });
    });

    describe('G92', () => {
      it('sets an offset to machine coordinates', () => {
        let commands = translateBatch(translator, [
          'G61 G90 G0 X1 Y2 Z3',
          'G92 X0 Y-1',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 13210',
          'line abs 8063 16126 24189',
        ]);

        expect(translator.getAxisPosition('X', Length.mm)).closeTo(0, 0.1);
        expect(translator.getAxisPosition('Y', Length.mm)).closeTo(-1, 0.1);
        expect(translator.getAxisPosition('Z', Length.mm)).closeTo(3, 0.1);

        expect(translator.getAxisPosition('X', Units.NATIVE)).closeTo(8063, 1);
        expect(translator.getAxisPosition('Y', Units.NATIVE)).closeTo(16126, 1);
        expect(translator.getAxisPosition('Z', Units.NATIVE)).closeTo(24189, 1);

        commands = translateBatch(translator, [
          'G91 X0 Y0 Z0',
          'G90 X0 Y-1',
          'G28',
        ]);
        expect(commands).to.deep.eq([
          'line abs 8063 16126 24189',
          'on a b line abs 8063 16126',
          'line abs 8063 24189 0',
        ]);
      });

      it('resets the offset with G92.1', () => {
        translateBatch(translator, [
          'G61 G90 G0 X1 Y2 Z3',
          'G92 X0 Y-1',
        ]);
        expect(translator.getAxisPosition('X', Length.mm)).closeTo(0, 0.1);
        expect(translator.getAxisPosition('Y', Length.mm)).closeTo(-1, 0.1);
        expect(translator.getAxisPosition('Z', Length.mm)).closeTo(3, 0.1);

        translateBatch(translator, ['G92.1']);

        expect(translator.getAxisPosition('X', Length.mm)).closeTo(1, 0.1);
        expect(translator.getAxisPosition('Y', Length.mm)).closeTo(2, 0.1);
        expect(translator.getAxisPosition('Z', Length.mm)).closeTo(3, 0.1);

        const commands = translateBatch(translator, [
          'G91 X0 Y0 Z0',
          'G90 X1 Y2',
          'G28',
        ]);
        expect(commands).to.deep.eq([
          'line abs 8063 16126 24189',
          'on a b line abs 8063 16126',
          'line abs 0 0 0',
        ]);
      });

      it('returns the offset with getAxisCoordinateSystemOffset', () => {
        translateBatch(translator, [
          'G61 G90 G0 X1 Y2 Z3',
          'G92 X2 Y-1',
        ]);

        expect(translator.getAxisCoordinateSystemOffset('G92', 'X', Length.mm)).closeTo(-1, 0.1);
        expect(translator.getAxisCoordinateSystemOffset('G92', 'Y', Length.mm)).closeTo(3, 0.1);
        expect(translator.getAxisCoordinateSystemOffset('G92', 'Z', Length.mm)).closeTo(0, 0.1);

        expect(translator.getAxisCoordinateSystemOffset('G92', 'X', Units.NATIVE)).closeTo(-8063, 1);
        expect(translator.getAxisCoordinateSystemOffset('G92', 'Y', Units.NATIVE)).closeTo(24189, 1);
        expect(translator.getAxisCoordinateSystemOffset('G92', 'Z', Units.NATIVE)).closeTo(0, 1);
      });

      it('properly chains G92 calls increasing the offset', () => {
        const commands = translateBatch(translator, [
          'G61 G90 G0 X1',
          'G92 X0',
          'G0 X1',
          'G92 X0',
          'G0 X1',
        ]);
        expect(commands).to.deep.eq([
          'set maxspeed 13210',
          'on a line abs 8063',
          'on a line abs 16126',
          'on a line abs 24189',
        ]);
      });

      it('respects G92 when calling other methods', () => {
        translateBatch(translator, [
          'G61 G90 G0 X1 Y2 Z3',
          'G92 X0 Y-1',
        ]);
        translator.setAxisHomePosition('X', 1, Length.mm);
        translator.setAxisHomePosition('Y', 6000, Units.NATIVE);

        const commands = translateBatch(translator, ['G28']);
        expect(commands).to.deep.eq([
          'line abs 16126 6000 0',
        ]);
      });
    });
  });

  describe('axis transformation', () => {
    beforeEach(async () => {
      translator = await OfflineTranslator.setup(MCC_XYZ, {
        axisTransformations: [{
          axisLetter: 'X',
          scaling: -2,
          translation: { value: 1, unit: Length.mm },
        }, {
          axisLetter: 'Y',
          scaling: 1,
          translation: { value: -63 },
        }],
      });

      const result = translator.translate('G61 G90 G28 F1');
      noWarnings(result);
    });

    it('transforms final device coordinates', () => {
      let result = translator.translate('X1 Y1 Z1');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'line abs -8063 8000 8063',
      ]);

      result = translator.translate('G1 G91 X-1 Y-1 Z-1');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'set maxspeed 220',
        'line abs 8063 -63 0',
      ]);

      result = translator.translate('G2 X1 Y1 I1');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'on a b arc abs cw -8062 -63 -8063 8000',
      ]);
    });

    it('transforms position and offsets returned in native units', () => {
      translateBatch(translator, [
        'G10 L2 P2 X1 Y1 Z1',
        'X1 Y1 Z1',
      ]);

      expect(translator.getAxisPosition('X', Units.NATIVE)).closeTo(-8063, 1);
      expect(translator.getAxisPosition('Y', Units.NATIVE)).closeTo(8000, 1);
      expect(translator.getAxisPosition('Z', Units.NATIVE)).closeTo(8063, 1);

      expect(translator.getAxisCoordinateSystemOffset('G55', 'X', Length.mm)).closeTo(1, 0.1);
      expect(translator.getAxisCoordinateSystemOffset('G55', 'Y', Length.mm)).closeTo(1, 0.1);
      expect(translator.getAxisCoordinateSystemOffset('G55', 'Z', Length.mm)).closeTo(1, 0.1);

      expect(translator.getAxisCoordinateSystemOffset('G55', 'X', Units.NATIVE)).closeTo(-16126, 1);
      expect(translator.getAxisCoordinateSystemOffset('G55', 'Y', Units.NATIVE)).closeTo(8063, 1);
      expect(translator.getAxisCoordinateSystemOffset('G55', 'Z', Units.NATIVE)).closeTo(8063, 1);
    });

    it('transforms back position set in native units', () => {
      translator.setAxisPosition('X', 10000, Units.NATIVE);
      translator.setAxisPosition('Y', 10000, Units.NATIVE);
      translator.setAxisPosition('Z', 10000, Units.NATIVE);

      expect(translator.getAxisPosition('X', Length.mm)).closeTo(-0.120, 0.001);
      expect(translator.getAxisPosition('Y', Length.mm)).closeTo(1.248, 0.001);
      expect(translator.getAxisPosition('Z', Length.mm)).closeTo(1.240, 0.001);

      let commands = translateBatch(translator, ['G91 G0 X0 Y0 Z0']);
      expect(commands[1]).to.eq('line abs 10000 10000 10000');

      translator.setAxisHomePosition('X', 100, Units.NATIVE);
      translator.setAxisHomePosition('Y', 100, Units.NATIVE);
      translator.setAxisHomePosition('Z', 100, Units.NATIVE);

      commands = translateBatch(translator, ['G28']);
      expect(commands[0]).to.eq('line abs 100 100 100');
    });

    it('works with G92', () => {
      let commands = translateBatch(translator, [
        'G61 G28 F1',
        'G90 X1 Y2 Z3',
        'G92 X0 Y-1',
      ]);
      expect(commands).to.deep.eq([
        'line abs 8063 -63 0',
        'line abs -8063 16063 24189',
      ]);

      expect(translator.getAxisPosition('X', Length.mm)).closeTo(0, 0.1);
      expect(translator.getAxisPosition('Y', Length.mm)).closeTo(-1, 0.1);
      expect(translator.getAxisPosition('Z', Length.mm)).closeTo(3, 0.1);

      expect(translator.getAxisPosition('X', Units.NATIVE)).closeTo(-8063, 1);
      expect(translator.getAxisPosition('Y', Units.NATIVE)).closeTo(16063, 1);
      expect(translator.getAxisPosition('Z', Units.NATIVE)).closeTo(24189, 1);

      commands = translateBatch(translator, [
        'G91 X0 Y0 Z0',
        'G91 X1 Y-1 Z-1',
        'G28',
      ]);
      expect(commands).to.deep.eq([
        'line abs -8063 16063 24189',
        'line abs -24189 8000 16126',
        'line abs -8063 24126 0',
      ]);
    });
  });

  describe('axis mapping', () => {
    it('allows to remap axis', async () => {
      translator = await OfflineTranslator.setup(MCC_XYZ, {
        axisMappings: [{
          axisLetter: 'X',
          axisIndex: 1,
        }, {
          axisLetter: 'Y',
          axisIndex: 0,
        }, {
          axisLetter: 'Z',
          axisIndex: 2,
        }]
      });
      noWarnings(translator.translate('G61 G28'));

      const result = translator.translate('G0X1Y2Z3');
      noWarnings(result);
      expect(result.commands).to.deep.eq([
        'on b a c line abs 8063 16126 24189',
      ]);
    });

    it('allows to ignore axes', async () => {
      translator = await OfflineTranslator.setup(MCC_XYZ, {
        axisMappings: [{
          axisLetter: 'Y',
          axisIndex: 0,
        }, {
          axisLetter: 'Z',
          axisIndex: 1,
        }]
      });
      noWarnings(translator.translate('G61 G28'));

      const result = translator.translate('G0X1Y2Z3');
      expect(result.warnings.map(w => w.message)).to.deep.eq(['Word unused X1.']);
      expect(result.commands).to.deep.eq([
        'line abs 16126 24189',
      ]);
    });
  });
});
