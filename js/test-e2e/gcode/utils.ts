import { expect } from 'chai';
import type { TranslateResult, OfflineTranslator } from '../../src/gcode';

export function noWarnings(result: TranslateResult) {
  expect(result.warnings).to.deep.eq([], 'warnings present');
  return result;
}

export function translateBatch(translator: OfflineTranslator, batch: string[]) {
  const commands: string[] = [];
  for (const code of batch) {
    const result = noWarnings(translator.translate(code));
    commands.push(...result.commands);
  }
  commands.push(...translator.flush());
  return commands;
}
