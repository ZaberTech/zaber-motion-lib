import { Velocity } from '../../src';

export const MCC_XYZ = {
  deviceId: 30331,
  axes: [{
    peripheralId: 43211,
    peripheralSerialNumber: 27011,
    microstepResolution: 64,
  }, {
    peripheralId: 43211,
    peripheralSerialNumber: 27017,
    microstepResolution: 64,
  }, {
    peripheralId: 43211,
    peripheralSerialNumber: 27000,
    microstepResolution: 64,
  }],
  maxSpeed: {
    value: 1,
    unit: Velocity['mm/s'],
  },
};

export const ADR_XY = {
  deviceId: 50952,
  axes: [{
    peripheralId: 70315,
    peripheralSerialNumber: 27111,
  }, {
    peripheralId: 70316,
    peripheralSerialNumber: 27103,
  }],
  maxSpeed: {
    value: 1,
    unit: Velocity['mm/s'],
  },
};
