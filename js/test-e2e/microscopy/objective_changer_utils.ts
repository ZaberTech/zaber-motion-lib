import { TestServer } from '../ascii/tcp';
import { eqAndPush } from '../test_util';


export async function objectiveChangerHome(
  server: TestServer,
  turret: number,
  focus: number,
  options: {
    homeOnly?: boolean;
    focusAxis?: number;
  } = {}) {
  const {
    homeOnly = false,
    focusAxis = 1,
  } = options;
  const eqAndPushX = eqAndPush.bind(null, server);
  await eqAndPushX(focus, focusAxis, 'get limit.home.offset', 'OK IDLE WR 10000');
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK IDLE WR 1 WR');
  await eqAndPushX(turret, 1, 'get motion.index.num', 'OK IDLE WR 0');
  await eqAndPushX(turret, 1, 'warnings clear', 'OK IDLE WR 1 WR');
  await eqAndPushX(focus, focusAxis, 'tools gotolimit hardstop neg 1 0', 'OK BUSY WR 0');
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK BUSY WR 1 WR');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE WR 0');
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK IDLE WR 1 WR');
  await eqAndPushX(turret, 1, 'home', 'OK BUSY WR 0');
  await eqAndPushX(turret, 1, 'warnings clear', 'OK BUSY -- 0');
  await eqAndPushX(turret, 1, '', 'OK IDLE -- 0');
  if (!homeOnly) {
    await eqAndPushX(turret, 1, 'move index 1', 'OK BUSY -- 0');
    await eqAndPushX(turret, 1, '', 'OK IDLE -- 0');
  }
  await eqAndPushX(focus, focusAxis, 'home', 'OK BUSY WR 0');
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK BUSY WR 1 WR');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
  if (!homeOnly) {
    await eqAndPushX(focus, focusAxis, 'move abs 10000', 'OK BUSY -- 0');
    await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
  }
}

interface ChangeArgs {
  index?: number;
  focusAxis?: number;
  focusOffset?: number;
}

export async function objectiveChangerChange(server: TestServer, turret: number, focus: number, args: ChangeArgs = {}) {
  const {
    index = 1,
    focusAxis = 1,
    focusOffset = 0,
  } = args;
  const eqAndPushX = eqAndPush.bind(null, server);
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK IDLE -- 0');
  await eqAndPushX(turret, 1, 'get motion.index.num', `OK BUSY -- ${index + 1}`);
  await eqAndPushX(focus, focusAxis, 'move min', 'OK BUSY -- 0');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
  await eqAndPushX(turret, 1, `move index ${index}`, 'OK BUSY -- 0');
  await eqAndPushX(turret, 1, '', 'OK IDLE -- 0');
  await eqAndPushX(focus, focusAxis, `move abs ${focusOffset}`, 'OK BUSY -- 0');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
}

export async function objectiveChangeHomeHasReference(
  server: TestServer,
  turret: number,
  focus: number,
  options: {
    forceHome?: boolean;
    focusAxis?: number;
  } = {}) {
  const {
    forceHome = false,
    focusAxis = 1,
  } = options;
  const eqAndPushX = eqAndPush.bind(null, server);
  await eqAndPushX(focus, focusAxis, 'get limit.home.offset', 'OK IDLE -- 10000');
  await eqAndPushX(focus, focusAxis, 'warnings clear', 'OK IDLE -- 0');
  await eqAndPushX(turret, 1, 'get motion.index.num', 'OK IDLE -- 0');
  if (!forceHome) {
    return;
  }

  await eqAndPushX(focus, focusAxis, 'move min', 'OK BUSY -- 0');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
  await eqAndPushX(turret, 1, 'home', 'OK BUSY -- 0');
  await eqAndPushX(turret, 1, '', 'OK IDLE -- 0');
  await eqAndPushX(focus, focusAxis, 'home', 'OK BUSY -- 0');
  await eqAndPushX(focus, focusAxis, '', 'OK IDLE -- 0');
}
