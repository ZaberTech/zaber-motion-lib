import { expect } from 'chai';
import { Connection } from '../../src/ascii';
import { FilterChanger } from '../../src/microscopy';
import { eqAndPush } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

describe('FilterChanger', () => {
  let connection: Connection;
  let changer: FilterChanger;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();

    changer = new FilterChanger(connection.getDevice(1));
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('change', () => {
    it('moves and waits', async () => {
      const promise = changer.change(2);
      await eqAndPushX(1, 1, 'move index 2', 'OK BUSY -- 0');
      await eqAndPushX(1, 1, '', 'OK IDLE -- 0');
      await promise;
    });
  });

  describe('getCurrentFilter', () => {
    it('gets position', async () => {
      const promise = changer.getCurrentFilter();
      await eqAndPushX(1, 1, 'get motion.index.num', 'OK IDLE -- 2');
      expect(await promise).to.eq(2);
    });
  });

  describe('getNumberOfFilters', () => {
    it('calculates indexed positions for cyclic device', async () => {
      const promise = changer.getNumberOfFilters();
      await eqAndPushX(1, 1, 'get motion.index.dist', 'OK IDLE -- 5');
      await eqAndPushX(1, 1, 'get limit.cycle.dist', 'OK IDLE -- 20');
      expect(await promise).to.eq(4);
    });
  });
});
