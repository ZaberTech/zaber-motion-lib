import { Library } from '../../src';
import { Connection } from '../../src/ascii';
import { Autofocus, WdiAutofocusProvider } from '../../src/microscopy';
import { eqAndPush } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { WDI_TEST_PORT, WdiProtocol } from './wdi/wdi_protocol';
import { BinaryTestServer } from './wdi/binary_test_server';
import { connectWdiAutofocus } from './wdi/utils';
import { DeviceXMOR } from '../ascii/devices/xmor';
import { DeviceXLDA } from '../ascii/devices/xlda';
import { parseCommand } from '../ascii/protocol';

const XMOR = 1;
const XLDA = 2;
const MAGNIFICATIONS = [1, 2, 5, 10, 20, 50, 100];
const CURRENT_OBJECTIVE_REG = 50;

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

let connection: Connection;

const wdiServer = new WdiProtocol(new BinaryTestServer());
let wdiClient: WdiAutofocusProvider;

let autofocus: Autofocus;

beforeAll(async () => {
  Library.setIdlePollingPeriod(50);
  await server.start();
  await wdiServer.server.start(WDI_TEST_PORT);
});

afterAll(async () => {
  await server.stop();
  await wdiServer.server.stop();
});

beforeEach(async () => {
  connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
  await server.waitForConnection();

  await DeviceXMOR.identify(connection.getDevice(XMOR), server);
  await DeviceXLDA.identify(connection.getDevice(XLDA), server);

  wdiClient = await connectWdiAutofocus(wdiServer);

  autofocus = new Autofocus(wdiClient.providerId, connection.getDevice(XLDA).getAxis(1), connection.getDevice(XMOR));
});

afterEach(async () => {
  await connection.close();
  server.closeSocket();

  await wdiClient.close();
  await wdiServer.server.closeSocket();
});

interface Objective {
  number: number;
  magnification: number;
  kp: number;
  ki: number;
  closestMagnification?: number;
  maxExposure?: number;
  snr?: number;
}

const OBJECTIVES: Objective[] = [
  { magnification: 5, kp: 0.1, ki: 500, maxExposure: 400, snr: 0.05 },
  { magnification: 20, kp: 0.05, ki: 200 },
  { magnification: 40, kp: 0.01, ki: 30, closestMagnification: 50 },
].map((params, i) => ({ number: i + 1, ...params }));

async function readObjectives(objectives = OBJECTIVES) {
  const cmd = parseCommand(await server.pop());
  const prefix = 'storage axis print prefix zaber.microscope.af.obj.';
  expect(cmd.data.substring(0, prefix.length)).toEqual(prefix);

  await server.push(`@${XLDA} 1 ${cmd.id} OK IDLE -- 0`);
  const objectiveMatch = cmd.strNoId.match(/\d+$/);

  for (const { number, kp, ki } of objectives) {
    if (objectiveMatch != null && +objectiveMatch[0] !== number) {
      continue;
    }
    const keyPrefix = `zaber.microscope.af.obj.${number}`;
    await server.push(`#${XLDA} 1 ${cmd.id} set ${keyPrefix}.kp ${kp}`);
    await server.push(`#${XLDA} 1 ${cmd.id} set ${keyPrefix}.ki ${ki}`);
  }
  await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0'); // multi-response end
}

async function readMagnifications() {
  const request = await wdiServer.read();
  expect(request).toMatchObject({
    register: 78,
    dataCount: 16,
  });
  await wdiServer.writeResponse(request,
    MAGNIFICATIONS.concat(new Array(request.dataCount - MAGNIFICATIONS.length).fill(0)));
}

async function getWdiStatus(inRange: boolean = true, laserOn = true) {
  const request = await wdiServer.read();
  expect(request).toMatchObject({
    register: 20,
    size: 'uint16',
  });
  let status = 0;
  if (laserOn) {
    status |= 1 << 4;
  }
  if (inRange) {
    status |= 1 << 12;
  }
  await wdiServer.writeResponse(request, [status]);
}

async function readMagnification(objective: typeof OBJECTIVES[0]) {
  const { number, magnification, closestMagnification } = objective;
  const request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: 54,
    offset: number - 1,
  });
  await wdiServer.writeResponse(request, [closestMagnification ?? magnification]);
}

async function synchronizeObjectiveParams(objective: typeof OBJECTIVES[0]) {
  const { number, magnification, closestMagnification } = objective;
  let request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: 54,
    offset: number - 1,
  });
  await wdiServer.writeResponse(request, [66]);

  request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'write',
    register: 54,
    offset: number - 1,
    data: [closestMagnification ?? magnification]
  });
  await wdiServer.write('ack');
}

async function synchronizeCurrentObjectiveParams(objective: typeof OBJECTIVES[0]) {
  const { maxExposure, snr } = objective;

  let request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'write',
    register: 230,
    data: [maxExposure ?? 600],
  });
  await wdiServer.write('ack');

  request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'write',
    register: 231,
    data: [expect.closeTo((snr ?? 0.02) * 1024, 0)],
  });
  await wdiServer.write('ack');
}

async function readsParameters(objective: typeof OBJECTIVES[0], currentObjective: number | null = null) {
  let request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: 54,
    offset: objective.number - 1,
  });
  await wdiServer.writeResponse(request, [objective.magnification]);

  const claimedObjective = currentObjective ?? objective.number;
  request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: CURRENT_OBJECTIVE_REG,
  });
  await wdiServer.writeResponse(request, [claimedObjective - 1]);

  if (claimedObjective !== objective.number) {
    request = await wdiServer.read();
    expect(request).toMatchObject({
      request: 'write',
      register: CURRENT_OBJECTIVE_REG,
      data: [objective.number - 1],
    });
    await wdiServer.write('ack');
  }

  request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: 230,
  });
  await wdiServer.writeResponse(request, [600]);

  request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: 231,
  });
  await wdiServer.writeResponse(request, [0.02 * 1024]);

  if (claimedObjective !== objective.number) {
    request = await wdiServer.read();
    expect(request).toMatchObject({
      request: 'write',
      register: CURRENT_OBJECTIVE_REG,
      data: [claimedObjective - 1],
    });
    await wdiServer.write('ack');
  }
}

async function synchronizeCurrentObjective(objective: typeof OBJECTIVES[0], syncProvider = true) {
  await eqAndPushX(XMOR, 1, 'get motion.index.num', `OK IDLE -- ${objective.number}`);

  if (syncProvider) {
    let request = await wdiServer.read();
    expect(request).toMatchObject({
      request: 'read',
      register: CURRENT_OBJECTIVE_REG,
    });
    await wdiServer.writeResponse(request, [0xFF]);

    request = await wdiServer.read();
    expect(request).toMatchObject({
      request: 'write',
      register: CURRENT_OBJECTIVE_REG,
      data: [objective.number - 1],
    });
    await wdiServer.write('ack');
  }

  await eqAndPushX(XLDA, 1, 'get motion.tracking.kp motion.tracking.ki', 'OK IDLE -- 0 ; 0');
  await eqAndPushX(XLDA, 1, `set motion.tracking.kp ${objective.kp}`, 'OK IDLE -- 0');
  await eqAndPushX(XLDA, 1, `set motion.tracking.ki ${objective.ki}`, 'OK IDLE -- 0');
}

interface PollUntilStopCtx {
  stallOnStop?: boolean;
  stopped?: boolean;
}

async function pollUntilStop(ctx: PollUntilStopCtx) {
  for (;;) {
    const command = parseCommand(await server.pop());
    const replyBase = `OK ${ctx.stopped ? 'IDLE' : 'BUSY'}`;
    let reply = '-- 0';
    switch (command.data) {
      case '':
        break;
      case 'stop':
        ctx.stopped = true;
        if (ctx.stallOnStop) {
          reply = 'FS 0';
        }
        break;
      case 'warnings clear':
        reply = 'FS 1 FS';
        break;
      default:
        throw new Error(`Unexpected command: ${command.data}`);
    }
    await server.push(`@${command.device} ${command.axis} ${command.id} ${replyBase} ${reply}`);
  }
}

describe('initialization', () => {
  test('synchronizes current objective', async () => {
    const promise = autofocus.synchronizeParameters(); // same as fresh initialization

    await readObjectives();

    await synchronizeCurrentObjective(OBJECTIVES[0]);

    await promise;
  });

  test('does not write anything if objectives are already synchronized', initialize);

  test('does not do sync objective if MOR is not homed', async () => {
    const promise = autofocus.synchronizeParameters();

    await readObjectives();

    await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK IDLE -- 0');

    await promise;
  });
});

async function initialize() {
  const promise = autofocus.synchronizeParameters();

  await readObjectives();

  const objective = OBJECTIVES[0];

  await eqAndPushX(XMOR, 1, 'get motion.index.num', `OK IDLE -- ${objective.number}`);

  const request = await wdiServer.read();
  expect(request).toMatchObject({
    request: 'read',
    register: CURRENT_OBJECTIVE_REG,
  });
  await wdiServer.writeResponse(request, [objective.number - 1]);

  await eqAndPushX(XLDA, 1, 'get motion.tracking.kp motion.tracking.ki',
    `OK IDLE -- ${objective.kp} ; ${objective.ki}`);

  await promise;
}

describe('initialized', () => {
  beforeEach(async () => {
    await initialize();
  });

  describe('setFocusZero', () => {
    test('sets zero position', async () => {
      const promise = autofocus.setFocusZero();

      await getWdiStatus();

      const request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: 62,
        size: null,
      });
      await wdiServer.write('ack');

      await promise;
    });

    test('throws error if not in range', async () => {
      const promise = autofocus.setFocusZero();
      await getWdiStatus(false);
      await expect(promise).rejects.toThrow('Autofocus is out of range.');
    });
  });

  describe('focusOnce', () => {
    test('focuses the axis', async () => {
      const promise = autofocus.focusOnce();

      await getWdiStatus();

      await eqAndPushX(XLDA, 1, 'move track once', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

      await promise;
    });

    test('throw error when on in range', async () => {
      const promise = autofocus.focusOnce();
      await getWdiStatus(false);
      await expect(promise).rejects.toThrow('Autofocus is out of range.');
    });

    test('throws error when laser is off', async () => {
      const promise = autofocus.focusOnce();
      await getWdiStatus(false, false);
      await expect(promise).rejects.toThrow('Laser is off.');
    });

    test('throws error when movement fails', async () => {
      const promise = autofocus.focusOnce();
      await getWdiStatus();

      await eqAndPushX(XLDA, 1, 'move track once', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE FE 0');
      await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE FE 1 FE');

      await expect(promise).rejects.toThrow(
        /Limit Error \(FE\)\. The autofocus loop has either reached the tracking limits or moved out of range/);
    });

    test('throws error with movement timeout', async () => {
      const promise = autofocus.focusOnce(false, 5);
      await getWdiStatus();

      await eqAndPushX(XLDA, 1, 'move track once', 'OK BUSY -- 0');
      const pollCtx: PollUntilStopCtx = {};
      pollUntilStop(pollCtx).catch(() => null);

      await expect(promise).rejects.toThrow('The autofocus operation timed out.');
      expect(pollCtx.stopped).toBe(true);
    });

    test('does not throw timeout error when focus completes in time', async () => {
      const promise = autofocus.focusOnce(false, 500);
      await getWdiStatus();

      await eqAndPushX(XLDA, 1, 'move track once', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

      await promise;
    });

    test('throws error when movement times out and stop fails', async () => {
      const promise = autofocus.focusOnce(false, 5);
      await getWdiStatus();

      await eqAndPushX(XLDA, 1, 'move track once', 'OK BUSY -- 0');
      const pollCtx: PollUntilStopCtx = {
        stallOnStop: true,
      };
      pollUntilStop(pollCtx).catch(() => null);

      await expect(promise).rejects.toThrow(
        /Movement may have failed because fault was observed: Stalled and Stopped \(FS\)\./);
    });

    test('scan ignores inRange status', async () => {
      const promise = autofocus.focusOnce(true);

      await getWdiStatus(false);

      await eqAndPushX(XLDA, 1, 'move scan track once', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

      await promise;
    });
  });

  describe('focus loop', () => {
    test('starts the loop', async () => {
      const promise = autofocus.startFocusLoop();
      await getWdiStatus();
      await eqAndPushX(XLDA, 1, 'move track', 'OK BUSY -- 0');
      await promise;
    });

    test('throw error when on in range', async () => {
      const promise = autofocus.startFocusLoop();
      await getWdiStatus(false);
      await expect(promise).rejects.toThrow('Autofocus is out of range.');
    });

    test('stops the loop', async () => {
      const promise = autofocus.stopFocusLoop();
      await eqAndPushX(XLDA, 1, '', 'OK BUSY -- 0');

      await eqAndPushX(XLDA, 1, 'stop', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
      await promise;
    });

    test('throws errors from the loop', async () => {
      const promise = autofocus.stopFocusLoop();
      await eqAndPushX(XLDA, 1, '', 'OK IDLE FE 0');

      await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE FE 1 FE');

      await expect(promise).rejects.toThrow(
        /Limit Error \(FE\)\. The autofocus loop has either reached the tracking limits or moved out of range/);
    });

    test('passes when there is nothing to stop', async () => {
      const promise = autofocus.stopFocusLoop();
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
      await promise;
    });
  });

  describe('limits', () => {
    test('sets tracking limits', async () => {
      let promise = autofocus.setLimitMin(50);
      await eqAndPushX(XLDA, 1, 'set motion.tracking.limit.min 50', 'OK IDLE -- 0');
      await promise;

      promise = autofocus.setLimitMax(100);
      await eqAndPushX(XLDA, 1, 'set motion.tracking.limit.max 100', 'OK IDLE -- 0');
      await promise;
    });
    test('gets tracking limits', async () => {
      let promise = autofocus.getLimitMin();
      await eqAndPushX(XLDA, 1, 'get motion.tracking.limit.min', 'OK IDLE -- 50');
      expect(await promise).toBe(50);

      promise = autofocus.getLimitMax();
      await eqAndPushX(XLDA, 1, 'get motion.tracking.limit.max', 'OK IDLE -- 100');
      expect(await promise).toBe(100);
    });
  });

  describe('getStatus', () => {
    test('combines status from WDI and the axis', async () => {
      for (const status of [false, true]) {
        const promise = autofocus.getStatus();

        await getWdiStatus(status);
        await eqAndPushX(XLDA, 1, 'get motion.tracking.settle.tolerance.met', `OK IDLE -- ${status ? 1 : 0}`);

        expect(await promise).toEqual({
          inRange: status,
          inFocus: status,
        });
      }
    });
  });

  describe('setObjectiveParameters', () => {
    test('sets parameters', async () => {
      const newObjective = {
        number: 1,
        magnification: 70,
        kp: 10,
        ki: 20,
        closestMagnification: 100,
        maxExposure: 400,
        snr: 0.05,
      };

      const promise = autofocus.setObjectiveParameters(1, [{
        name: 'control_loop_kp',
        value: newObjective.kp,
      }, {
        name: 'control_loop_ki',
        value: newObjective.ki,
      }, {
        name: 'magnification',
        value: newObjective.magnification,
      }, {
        name: 'max_exposure',
        value: newObjective.maxExposure,
      }, {
        name: 'signal_to_noise_ratio',
        value: newObjective.snr,
      }]);

      await eqAndPushX(XLDA, 1, 'storage axis set zaber.microscope.af.obj.1.kp 10', 'OK IDLE -- 0');
      await eqAndPushX(XLDA, 1, 'storage axis set zaber.microscope.af.obj.1.ki 20', 'OK IDLE -- 0');

      await readMagnifications();
      await synchronizeObjectiveParams(newObjective);

      await readMagnification(newObjective);

      const request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'read',
        register: CURRENT_OBJECTIVE_REG,
      });
      await wdiServer.writeResponse(request, [newObjective.number - 1]);

      await synchronizeCurrentObjectiveParams(newObjective);

      await readObjectives([newObjective]);

      await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK IDLE -- 2');

      await promise;
    });

    test('switches objectives when writing current objective settings', async () => {
      const CURRENT_OBJECTIVE = 3;
      const SET_OBJECTIVE = 1;

      const promise = autofocus.setObjectiveParameters(SET_OBJECTIVE, [{
        name: 'max_exposure',
        value: 350,
      }]);

      await readMagnification(OBJECTIVES[SET_OBJECTIVE - 1]);

      let request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'read',
        register: CURRENT_OBJECTIVE_REG,
      });
      await wdiServer.writeResponse(request, [CURRENT_OBJECTIVE - 1]);

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: CURRENT_OBJECTIVE_REG,
        data: [SET_OBJECTIVE - 1],
      });
      await wdiServer.write('ack');

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: 230,
        data: [350],
      });
      await wdiServer.write('ack');

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: CURRENT_OBJECTIVE_REG,
        data: [CURRENT_OBJECTIVE - 1],
      });
      await wdiServer.write('ack');

      await readObjectives([OBJECTIVES[SET_OBJECTIVE - 1]]);

      await eqAndPushX(XMOR, 1, 'get motion.index.num', `OK IDLE -- ${CURRENT_OBJECTIVE}`);

      await promise;
    });

    test('switched the current objective when writing current objective settings despite error', async () => {
      const CURRENT_OBJECTIVE = 3;
      const SET_OBJECTIVE = 1;

      const promise = autofocus.setObjectiveParameters(SET_OBJECTIVE, [{
        name: 'max_exposure',
        value: 350,
      }]);

      await readMagnification(OBJECTIVES[SET_OBJECTIVE - 1]);

      let request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'read',
        register: CURRENT_OBJECTIVE_REG,
      });
      await wdiServer.writeResponse(request, [CURRENT_OBJECTIVE - 1]);

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: CURRENT_OBJECTIVE_REG,
        data: [SET_OBJECTIVE - 1],
      });
      await wdiServer.write('ack');

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: 230,
        data: [350],
      });
      await wdiServer.write('nack');

      request = await wdiServer.read();
      expect(request).toMatchObject({
        request: 'write',
        register: CURRENT_OBJECTIVE_REG,
        data: [CURRENT_OBJECTIVE - 1],
      });
      await wdiServer.write('ack');

      await expect(promise).rejects.toThrow('Device responded with NAK: Write request to U230 (parameter: max_exposure)');
    });

    test('synchronizes parameters if set for the current objective', async () => {
      const newObjective = {
        number: 2,
        magnification: 70,
        kp: 10,
        ki: 20,
        closestMagnification: 100,
      };

      const promise = autofocus.setObjectiveParameters(2, [{
        name: 'control_loop_kp',
        value: newObjective.kp,
      }]);

      await eqAndPushX(XLDA, 1, 'storage axis set zaber.microscope.af.obj.2.kp 10', 'OK IDLE -- 0');

      await readObjectives([newObjective]);

      await synchronizeCurrentObjective(newObjective, false);

      await promise;
    });

    test('erases parameters', async () => {
      const promise = autofocus.setObjectiveParameters(2, [{
        name: 'control_loop_kp',
      }, {
        name: 'control_loop_ki',
      }]);

      await eqAndPushX(XLDA, 1, 'storage axis erase zaber.microscope.af.obj.2.kp', 'OK IDLE -- 0');
      await eqAndPushX(XLDA, 1, 'storage axis erase zaber.microscope.af.obj.2.ki', 'OK IDLE -- 0');
      await readObjectives([]);

      await promise;
    });

    test('throws error for invalid parameters', async () => {
      const promise = autofocus.setObjectiveParameters(1, [{
        name: 'control_loop_ko',
        value: 10,
      }]);

      await expect(promise).rejects.toThrow('Invalid parameter: control_loop_ko');
    });

    test('can disable objective on WDI', async () => {
      const newObjective = {
        number: 2,
        magnification: 0,
        kp: 0,
        ki: 0,
        closestMagnification: 0,
      };

      const promise = autofocus.setObjectiveParameters(2, [{
        name: 'magnification',
        value: 0,
      }]);

      await readMagnifications();
      await synchronizeObjectiveParams(newObjective);

      await readObjectives([newObjective]);
      await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK IDLE -- 0');

      await promise;
    });

    test('does not set parameters to disabled objective', async () => {
      const newObjective = {
        number: 2,
        magnification: 0,
        kp: 0,
        ki: 0,
        closestMagnification: 0,
      };

      const promise = autofocus.setObjectiveParameters(2, [{
        name: 'max_exposure',
        value: 500,
      }]);

      await readMagnification(newObjective);

      await readObjectives([newObjective]);
      await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK IDLE -- 0');

      await promise;
    });
  });

  describe('getObjectiveParameters', () => {
    test('gets parameters', async () => {
      const objective = OBJECTIVES[0];
      const promise = autofocus.getObjectiveParameters(objective.number);

      await readObjectives([objective]);
      await readsParameters(objective);

      expect(await promise).toEqual([
        { name: 'control_loop_kp', value: objective.kp },
        { name: 'control_loop_ki', value: objective.ki },
        { name: 'magnification', value: objective.magnification },
        { name: 'max_exposure', value: 600 },
        { name: 'signal_to_noise_ratio', value: expect.closeTo(0.02, 0.1 ** 3) },
      ]);
    });

    test('switches objective when reading current objective settings', async () => {
      const CURRENT_OBJECTIVE = 1;
      const objective = OBJECTIVES[1];
      const promise = autofocus.getObjectiveParameters(objective.number);

      await readObjectives([objective]);
      await readsParameters(objective, CURRENT_OBJECTIVE);

      await promise;
    });
  });

  describe('toString', () => {
    test('returns string representation', () => {
      expect(autofocus.toString()).toMatch(/^WDI autofocus 12345 at/);
    });
    it('returns representation for closed connection', async () => {
      await connection.close();
      await wdiClient.close();
      expect(autofocus.toString()).toMatch(/^Unknown provider \d+/);
    });
  });
});
