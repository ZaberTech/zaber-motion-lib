import { expect } from 'chai';
import { Connection } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { DeviceXLCA } from '../ascii/devices/xlca';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { Illuminator, IlluminatorChannel } from '../../src/microscopy';
import { BadCommandException, DeviceDetectionFailedException, OperationFailedException } from '../../src';
import { parseCommand } from '../ascii/protocol';
import { DeviceXLHM } from '../ascii/devices/xlhm';

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

describe('Illuminator', () => {
  let connection: Connection;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('device search', () => {
    it('throws error when no devices are detected', async () => {
      connection.defaultRequestTimeout = 1;
      const promise = Illuminator.find(connection);
      const err = expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');

      expect((await err).message).to.eq('Illuminator not found on the connection.');
    });

    it('throws error when devices are not present with provided address (timeout)', async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);

      connection.defaultRequestTimeout = 1;
      const promise = Illuminator.find(connection, { deviceAddress: 3 });
      const err = expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/3 0 id get lamp.status');

      expect((await err).message).to.eq('Illuminator with address 3 not found on the connection.');
    });

    it('throws error when correct devices are not present with provided address', async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);

      const promise = Illuminator.find(connection, { deviceAddress: 3 });

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/3 0 id get lamp.status');
      await server.push(`@03 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);

      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq('Device with address 3 does not have setting lamp.status and is not the expected Illuminator.');
    });

    it('throws error when there is a conflict amongst devices', async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);
      await DeviceXLCA.identify(connection.getDevice(2), server);

      const promise = Illuminator.find(connection);
      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq(
        'Multiple Illuminator devices found on the connection. Provide an address to specify one (found 1, 2).');
    });

    it('throws error when there is a conflict amongst devices (no identified)', async () => {
      const promise = Illuminator.find(connection);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 1`);
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 1`);

      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq(
        'Multiple Illuminator devices found on the connection. Provide an address to specify one (found 2, 3).');
    });

    it('detects the device even with conflict if addresses are provided', async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);
      await DeviceXLCA.identify(connection.getDevice(2), server);

      const promise = Illuminator.find(connection, { deviceAddress: 1 });
      for (let i = 1; i <= 4; i++) {
        await eqAndPushX(1, i, 'get lamp.flux.max', 'OK IDLE -- 100');
      }
      await promise;

      const promise2 = Illuminator.find(connection, { deviceAddress: 2 });
      for (let i = 1; i <= 4; i++) {
        await eqAndPushX(2, i, 'get lamp.flux.max', 'OK IDLE -- 100');
      }
      await promise2;
    });

    it('detects unidentified device', async () => {
      const promise = Illuminator.find(connection);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 1`);

      await eqAndPushX(1, 0, 'get system.axiscount', 'OK IDLE -- 1');
      await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 100');

      await promise;
    });

    it('detects unidentified devices with address hints', async () => {
      const promise = Illuminator.find(connection, { deviceAddress: 1 });
      await eqAndPushX(1, 0, 'get lamp.status', 'OK IDLE -- 1');
      await eqAndPushX(1, 0, 'get system.axiscount', 'OK IDLE -- 1');
      await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 100');
      await promise;
    });

    it('detects identified device', async () => {
      await DeviceXLCA.identify(connection.getDevice(2), server);

      const promise = Illuminator.find(connection);
      for (let i = 1; i <= 4; i++) {
        await eqAndPushX(2, i, 'get lamp.flux.max', 'OK IDLE -- 100');
      }
      const illuminator = await promise;
      expect(illuminator.device.deviceAddress).to.eq(2);
    });
  });

  describe('found', () => {
    let device: Illuminator;
    let channel: IlluminatorChannel;

    beforeEach(async () => {
      const promise = Illuminator.find(connection, { deviceAddress: 1 });

      await eqAndPushX(1, 0, 'get lamp.status', 'OK IDLE -- 1');
      await eqAndPushX(1, 0, 'get system.axiscount', 'OK IDLE -- 1');
      await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 200');

      device = await promise;
      channel = device.getChannel(1);
    });

    describe('find', () => {
      it('refreshes associated information', async () => {
        const promise = Illuminator.find(connection, { deviceAddress: 1 });

        await eqAndPushX(1, 0, 'get lamp.status', 'OK IDLE -- 1');
        await eqAndPushX(1, 0, 'get system.axiscount', 'OK IDLE -- 1');
        await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 113');

        device = await promise;
        channel = device.getChannel(1);

        const promise2 = channel.getIntensity();
        await eqAndPushX(1, 1, 'get lamp.flux', 'OK IDLE -- 113');
        expect(await promise2).to.eq(1.0);
      });
    });

    describe('on/off', () => {
      it('sends a proper request', async () => {
        let promise = channel.on();
        await eqAndPushX(1, 1, 'lamp on', 'OK IDLE -- 0');
        await promise;

        promise = channel.off();
        await eqAndPushX(1, 1, 'lamp off', 'OK IDLE -- 0');
        await promise;

        promise = channel.setOn(true);
        await eqAndPushX(1, 1, 'lamp on', 'OK IDLE -- 0');
        await promise;

        promise = channel.setOn(false);
        await eqAndPushX(1, 1, 'lamp off', 'OK IDLE -- 0');
        await promise;
      });

      it('handles warnings', async () => {
        const promise = channel.off();
        await eqAndPushX(1, 1, 'lamp off', 'OK IDLE FC 0');
        await eqAndPushX(1, 1, 'warnings clear', 'OK IDLE FC 1 FC');
        await expect(promise).to.rejectedWith(OperationFailedException);
        const err: OperationFailedException = await promise.catch(e => e);
        expect(err.message).to.eq(
          'Operation may have failed because fault was observed: Current Inrush Error (FC).');
        expect(err.details).to.deep.eq({
          axis: 1,
          device: 1,
          reason: 'Current Inrush Error (FC)',
          warnings: ['FC'],
        });
      });

      it('provide handles BADCOMMAND', async () => {
        const promise = channel.on();
        await eqAndPushX(1, 1, 'lamp on', 'RJ IDLE -- BADCOMMAND');
        await expect(promise).to.eventually.rejectedWith(/The channel is likely in fault, not connected, or not supported/);
      });
    });

    describe('isOn', () => {
      it('returns true for 2 and false everything else', async () => {
        let promise = channel.isOn();
        await eqAndPushX(1, 1, 'get lamp.status', 'OK IDLE -- 2');
        expect(await promise).to.eq(true);

        promise = channel.isOn();
        await eqAndPushX(1, 1, 'get lamp.status', 'OK IDLE -- 1');
        expect(await promise).to.eq(false);
      });
    });

    describe('intensity', () => {
      it('calculated value from max flux', async () => {
        let promise = channel.setIntensity(0.5);
        await eqAndPushX(1, 1, 'set lamp.flux 100', 'OK IDLE -- 0');
        await promise;

        promise = channel.setIntensity(1);
        await eqAndPushX(1, 1, 'set lamp.flux 200', 'OK IDLE -- 0');
        await promise;
      });

      it('calculates the value back', async () => {
        let promise = channel.getIntensity();
        await eqAndPushX(1, 1, 'get lamp.flux', 'OK IDLE -- 100');
        expect(await promise).to.be.closeTo(0.5, 1e-6);

        promise = channel.getIntensity();
        await eqAndPushX(1, 1, 'get lamp.flux', 'OK IDLE -- 200');
        expect(await promise).to.eq(1);
      });
    });
  });

  describe('constructor', () => {
    beforeEach(async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);
      await DeviceXLHM.identify(connection.getDevice(2), server);
    });

    it('throws error when device is not a illuminator', async () => {
      new Illuminator(connection.getDevice(1));
      new Illuminator(connection.getDevice(3));
      expect(() => new Illuminator(connection.getDevice(2))).to.throw('Device with address 2 is not an Illuminator.');
    });

    it('does not throw error on closed connection', async () => {
      await connection.close();
      expect(() => new Illuminator(connection.getDevice(1))).to.not.throw();
    });
  });

  describe('identified', () => {
    let device: Illuminator;
    let channel: IlluminatorChannel;

    beforeEach(async () => {
      await DeviceXLCA.identify(connection.getDevice(1), server);
      device = new Illuminator(connection.getDevice(1));
      channel = device.getChannel(1);
    });

    describe('formats value properly', () => {
      it('calculated value from max flux', async () => {
        const promise = channel.setIntensity(1 / 3);
        await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 100');
        await eqAndPushX(1, 1, 'set lamp.flux 33.333', 'OK IDLE -- 0');
        await promise;
      });
    });
  });

  describe('multi-axis', () => {
    let device: Illuminator;

    beforeEach(async () => {
      const promise = Illuminator.find(connection, { deviceAddress: 1 });

      await eqAndPushX(1, 0, 'get lamp.status', 'OK IDLE -- 1');
      await eqAndPushX(1, 0, 'get system.axiscount', 'OK IDLE -- 3');
      await eqAndPushX(1, 1, 'get lamp.flux.max', 'OK IDLE -- 100');
      await eqAndPushX(1, 2, 'get lamp.flux.max', 'OK IDLE -- 100');
      await eqAndPushX(1, 3, 'get lamp.flux.max', 'RJ IDLE -- BADCOMMAND');

      device = await promise;
    });

    it('fails predictively', async () => {
      const channel = device.getChannel(3);
      const promise = channel.setIntensity(0.5);
      await eqAndPushX(1, 3, 'get lamp.flux.max', 'RJ IDLE -- BADCOMMAND');
      await expect(promise).to.be.rejectedWith(BadCommandException);
    });
  });
});
