import { WdiAutofocusProvider } from '../../../src/microscopy';

import { BinaryTestServer } from './binary_test_server';
import { WdiProtocol } from './wdi_protocol';

export async function connectWdiAutofocus(server: WdiProtocol<BinaryTestServer>) {
  const connectPromise = WdiAutofocusProvider.openTcp(server.server.host, server.server.port);
  await server.server.waitForConnection();

  const serialRequest = await server.read();
  expect(serialRequest).toMatchObject({
    register: 82,
    offset: 3,
    size: 'uint32'
  });
  await server.writeResponse(serialRequest, [12345]);

  return await connectPromise;
}
