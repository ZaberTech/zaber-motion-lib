export const WDI_TEST_PORT = 28789 + Number(process.env.JEST_WORKER_ID) * 10;
export const WDI_TEST_HOST = '127.0.0.1';

interface WdiServer {
  read(size: number): Promise<Uint8Array>;
  write(data: Uint8Array): Promise<void>;
}

const DATA_TYPES = [null, 'uint8', 'uint16', 'uint32'] as const;

interface Request {
  request: 'read' | 'write';
  registerBank: string;
  register: number;
  size: null | 'uint8' | 'uint16' | 'uint32';
  offset: number;
  data: number[];
  dataCount: number;
}

interface Response {
  registerBank: string;
  register: number;
  size: null | 'uint8' | 'uint16' | 'uint32';
  data: number[];
}

export function fromRequest(request: Request): Omit<Response, 'data'> {
  return {
    registerBank: request.registerBank,
    register: request.register,
    size: request.size,
  };
}

export class WdiProtocol<TServer extends WdiServer> {
  constructor(readonly server: TServer) {}

  private async readTransport(): Promise<Uint8Array> {
    const header = await this.server.read(4);
    const dataView = new DataView(header.buffer);

    if (dataView.getUint8(0) !== 'l'.charCodeAt(0)) {
      throw new Error('Invalid endian');
    }
    const size = dataView.getUint16(1, true) - 1;
    // don't care about checksum
    return await this.server.read(size);
  }

  private async writeTransport(payload: Uint8Array | 'ack' | 'nack'): Promise<void> {
    if (typeof payload === 'string') {
      const ack = payload.toUpperCase().charCodeAt(0);
      await this.server.write(new Uint8Array([ack]));
      return;
    }

    const packet = new Uint8Array(5 + payload.length);
    const dataView = new DataView(packet.buffer);

    dataView.setUint8(0, 'A'.charCodeAt(0));
    dataView.setUint8(1, 'l'.charCodeAt(0));
    dataView.setUint16(2, payload.length + 1, true);
    packet.set(payload, 5);

    let checksum = 0;
    for (let i = 1; i < packet.length; i++) {
      checksum += packet[i];
    }
    checksum = 0x100 - (checksum & 0xff);
    dataView.setUint8(4, checksum);

    await this.server.write(packet);
  }

  public async read(): Promise<Request> {
    const payload = await this.readTransport();
    const dataView = new DataView(payload.buffer);

    const request: Partial<Request> = {};
    const type = String.fromCharCode(dataView.getUint8(0));
    switch (type) {
      case 'R':
        request.request = 'read';
        break;
      case 'W':
        request.request = 'write';
        break;
      default:
        throw new Error('Invalid request type');
    }
    request.registerBank = String.fromCharCode(dataView.getUint8(1));
    request.register = dataView.getUint8(2);

    const dataTags = dataView.getUint8(3);
    const dataType = (dataTags >> 4) & 0x3;
    request.size = DATA_TYPES[dataType];

    let offset = 4;

    let dataCount = 0;
    if (request.size != null) {
      dataCount = 1;
      if (dataTags & (1 << 6)) {
        dataCount = dataView.getUint16(offset, true);
        offset += 2;
      }
    }
    request.dataCount = dataCount;

    if (dataTags & (1 << 7)) {
      request.offset = dataView.getUint16(offset, true);
      offset += 2;
    } else {
      request.offset = 0;
    }

    if (request.request === 'write' && dataCount > 0) {
      const data = payload.slice(offset);
      switch (request.size) {
        case 'uint8':
          request.data = Array.from(data);
          break;
        case 'uint16':
          request.data = Array.from(new Uint16Array(data.buffer));
          break;
        case 'uint32':
          request.data = Array.from(new Uint32Array(data.buffer));
          break;
      }
    } else {
      if (payload.length > offset) {
        throw new Error('Unexpected data');
      }
      request.data = [];
    }

    return request as Request;
  }

  async writeResponse(request: Request, data: number[]): Promise<void> {
    await this.write({
      ...fromRequest(request),
      data,
    });
  }

  async write(response: Response | 'ack' | 'nack'): Promise<void> {
    if (typeof response === 'string') {
      await this.writeTransport(response);
      return;
    }

    const itemCount = response.data.length;

    const header = new Uint8Array(4 + (itemCount > 1 ? 2 : 0));
    const dataView = new DataView(header.buffer);
    dataView.setUint8(0, 'r'.charCodeAt(0));
    dataView.setUint8(1, response.registerBank.charCodeAt(0));
    dataView.setUint8(2, response.register);

    let dataTags = 0x07;
    if (response.size != null) {
      dataTags |= (DATA_TYPES.indexOf(response.size) << 4);
    }
    if (response.data.length > 1) {
      dataTags |= (1 << 6);
    }
    dataView.setUint8(3, dataTags);

    if (itemCount > 1) {
      dataView.setUint16(4, itemCount, true);
    }

    let itemSize = 0;
    switch (response.size) {
      case 'uint8':
        itemSize = 1;
        break;
      case 'uint16':
        itemSize = 2;
        break;
      case 'uint32':
        itemSize = 4;
        break;
    }
    const data: Uint8Array = new Uint8Array(itemSize * itemCount);

    switch (response.size) {
      case 'uint8':
        data.set(response.data);
        break;
      case 'uint16':
        new Uint16Array(data.buffer).set(response.data);
        break;
      case 'uint32':
        new Uint32Array(data.buffer).set(response.data);
        break;
    }

    await this.writeTransport(new Uint8Array([...header, ...data]));
  }
}
