import { createServer, Server, Socket } from 'net';
import Bluebird from 'bluebird';

export class BinaryTestServer {
  private tcpListener?: Server;
  private socket: Socket | null = null;
  public debug = false;
  private waitOnConnection: (() => void) | null = null;
  public port = 0;
  public host = '127.0.0.1';

  constructor() {
    this.handleSocket = this.handleSocket.bind(this);
  }

  public async start(port: number): Promise<void> {
    this.tcpListener = createServer(this.handleSocket);

    this.port = port;
    await new Promise<void>(resolve => this.tcpListener!.listen({
      host: this.host,
      port,
    }, resolve));
  }

  public async stop(): Promise<void> {
    const closePromise = Bluebird.fromCallback(cb => this.tcpListener!.close(cb));
    this.closeSocket();
    await closePromise;
  }

  public closeSocket(): void {
    if (!this.socket) {
      return;
    }
    this.socket.end();
    this.socket.destroy();
    this.socket = null;
  }

  public async waitForConnection(): Promise<void> {
    if (!this.socket) {
      if (this.waitOnConnection) {
        throw new Error('something else is already waiting');
      }
      await new Promise<void>(resolve => this.waitOnConnection = resolve);
    }
  }

  public async waitForClose(): Promise<void> {
    await new Promise(resolve => this.socket!.once('close', resolve));
  }

  public async read(size: number): Promise<Uint8Array> {
    const socket = this.socket;
    if (!socket) {
      throw new Error('no connection');
    }

    let buffer: Buffer | null = null;
    while (buffer == null) {
      buffer = socket.read(size);
      if (buffer == null) {
        await new Promise<void>(resolve => socket.once('readable', resolve));
      }
    }

    if (buffer.length !== size) {
      throw new Error('unexpected EOF');
    }

    return new Uint8Array(buffer);
  }

  public async write(data: Uint8Array): Promise<void> {
    await Bluebird.fromCallback(cb => this.socket!.write(data, cb));
  }

  private handleSocket(socket: Socket): void {
    this.closeSocket();
    this.socket = socket;
    socket.setNoDelay(true);

    this.waitOnConnection?.();
    this.waitOnConnection = null;
  }
}
