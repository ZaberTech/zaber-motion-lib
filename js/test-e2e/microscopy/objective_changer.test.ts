import { expect } from 'chai';
import { Axis, Connection, Device } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { DeviceXMOR } from '../ascii/devices/xmor';
import { DeviceXLDA } from '../ascii/devices/xlda';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { ObjectiveChanger } from '../../src/microscopy';
import { Length, MovementFailedException, DeviceDetectionFailedException } from '../../src';
import { parseCommand } from '../ascii/protocol';
import { objectiveChangerChange, objectiveChangerHome } from './objective_changer_utils';

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

const XLDA = 2;
const XMOR = 1;

describe('Objective Changer', () => {
  let connection: Connection;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('device search', () => {
    it('throws error when no devices are detected', async () => {
      connection.defaultRequestTimeout = 1;
      const promise = ObjectiveChanger.find(connection);
      const err = expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get device.id');

      expect((await err).message).to.eq('Device X-MOR not found on the connection.');
    });

    it('throws error when necessary devices are not present', async () => {
      await DeviceXLDA.identify(connection.getDevice(2), server);

      const promise = ObjectiveChanger.find(connection);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get device.id');
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 13718`);

      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq('Device X-MOR not found on the connection.');
    });

    it('throws error when devices are not present with provided address (timeout)', async () => {
      await DeviceXMOR.identify(connection.getDevice(1), server);
      await DeviceXLDA.identify(connection.getDevice(2), server);

      connection.defaultRequestTimeout = 1;
      const promise = ObjectiveChanger.find(connection, { turretAddress: 3 });
      const err = expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/3 0 id get device.id');

      expect((await err).message).to.eq('Device X-MOR with address 3 not found on the connection.');
    });

    it('throws error when correct devices are not present with provided address', async () => {
      await DeviceXMOR.identify(connection.getDevice(1), server);
      await DeviceXLDA.identify(connection.getDevice(2), server);

      const promise = ObjectiveChanger.find(connection, { turretAddress: 3 });

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/3 0 id get device.id');
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 666`);

      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq('Device X-MOR with address 3 does not match (found device with device.id 666).');
    });

    it('throws error when there is a conflict amongst devices', async () => {
      await DeviceXMOR.identify(connection.getDevice(1), server);
      await DeviceXLDA.identify(connection.getDevice(2), server);
      await DeviceXLDA.identify(connection.getDevice(3), server);

      const promise = ObjectiveChanger.find(connection);
      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq(
        'Multiple X-LDA devices found on the connection. Provide an address to specify one (found 2, 3).');
    });

    it('throws error when there is a conflict amongst devices (no identified)', async () => {
      const promise = ObjectiveChanger.find(connection);

      for (let i = 0; i < 2; i++) {
        const cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/0 0 id get device.id');
        await server.push(`@01 0 ${cmd.id} OK IDLE -- 53115`);
        await server.push(`@02 0 ${cmd.id} OK IDLE -- 50852`);
        await server.push(`@03 0 ${cmd.id} OK IDLE -- 50852`);
      }

      const err = await expect(promise).to.be.rejectedWith(DeviceDetectionFailedException);
      expect(err.message).to.eq(
        'Multiple X-LDA devices found on the connection. Provide an address to specify one (found 2, 3).');
    });

    it('detects the changer even with conflict if addresses are provided', async () => {
      await DeviceXMOR.identify(connection.getDevice(1), server);
      await DeviceXMOR.identify(connection.getDevice(2), server);
      await DeviceXLDA.identify(connection.getDevice(3), server);
      await DeviceXLDA.identify(connection.getDevice(4), server);

      const promise = ObjectiveChanger.find(connection, { turretAddress: 1, focusAddress: 3 });
      await eqAndPushX(3, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');
      await promise;

      const promise2 = ObjectiveChanger.find(connection, { turretAddress: 2, focusAddress: 4 });
      await eqAndPushX(4, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');
      await promise2;
    });

    it('detects unidentified devices', async () => {
      const promise = ObjectiveChanger.find(connection);

      for (let i = 0; i < 2; i++) {
        const cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/0 0 id get device.id');
        await server.push(`@01 0 ${cmd.id} OK IDLE -- 53115`);
        await server.push(`@02 0 ${cmd.id} OK IDLE -- 50852`);
      }

      await eqAndPushX(2, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');
      await promise;
    });

    it('detects unidentified devices with address hints', async () => {
      const promise = ObjectiveChanger.find(connection, { focusAddress: XLDA, turretAddress: XMOR });

      await eqAndPushX(XMOR, 0, 'get device.id', 'OK IDLE -- 53115');
      await eqAndPushX(XLDA, 0, 'get device.id', 'OK IDLE -- 50852');
      await eqAndPushX(XLDA, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');

      await promise;
    });
  });

  describe('constructor', () => {
    it('throws error when constructing with invalid device', async () => {
      await DeviceXLDA.identify(connection.getDevice(2), server);
      expect(() => new ObjectiveChanger(connection.getDevice(2), connection.getDevice(1).getAxis(1))).to.throw(
        'Device with address 2 is not an X-MOR.');
    });

    it('does not throw error on closed connection', async () => {
      await connection.close();
      expect(() => new ObjectiveChanger(connection.getDevice(1), connection.getDevice(2).getAxis(1))).to.not.throw();
    });
  });

  describe('found', () => {
    let changer: ObjectiveChanger;

    beforeEach(async () => {
      const promise = ObjectiveChanger.find(connection, { focusAddress: XLDA, turretAddress: XMOR });

      await eqAndPushX(XMOR, 0, 'get device.id', 'OK IDLE -- 53115');
      await eqAndPushX(XLDA, 0, 'get device.id', 'OK IDLE -- 50852');
      await eqAndPushX(XLDA, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');

      changer = await promise;
    });

    describe('find', () => {
      it('refreshes associated information', async () => {
        const promise = ObjectiveChanger.find(connection, { focusAddress: XLDA, turretAddress: XMOR });

        await eqAndPushX(XMOR, 0, 'get device.id', 'OK IDLE -- 53115');
        await eqAndPushX(XLDA, 0, 'get device.id', 'OK IDLE -- 50852');
        await eqAndPushX(XLDA, 1, 'get limit.home.offset', 'OK IDLE -- 10000000');

        changer = await promise;

        expect(await changer.getFocusDatum()).to.deep.eq(10000000);
      });
    });

    describe('change', () => {
      it('changes objective moving back to datum', async () => {
        const promise = changer.change(2);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
        await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'move index 2', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');
        await eqAndPushX(XLDA, 1, 'move abs 20000000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });

      it('moves to the provided offset', async () => {
        const promise = changer.change(2, { focusOffset: { value: 123 } });
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
        await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'move index 2', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');
        await eqAndPushX(XLDA, 1, 'move abs 20000123', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });

      it('only moves to the correct offset if the turret is already in the position', async () => {
        const promise = changer.change(2, { focusOffset: { value: 1 }  });
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 2');
        await eqAndPushX(XLDA, 1, 'move abs 20000001', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });

      it('throw error when LDA has an error flag already', async () => {
        const promise = changer.change(2);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE FR 1 FR');
        const err: MovementFailedException = await expect(promise).to.be.rejectedWith(MovementFailedException);
        expect(err.details).to.deep.eq({
          axis: 1,
          device: 2,
          reason: 'Overdrive Limit Exceeded (FR)',
          warnings: ['FR'],
        });
      });

      it('throws error on LDA fault', async () => {
        const promise = changer.change(2);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
        await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE FS 1');
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE FS 1 FS');
        const err: MovementFailedException = await expect(promise).to.be.rejectedWith(MovementFailedException);
        expect(err.details).to.deep.eq({
          axis: 1,
          device: 2,
          reason: 'Stalled and Stopped (FS)',
          warnings: ['FS'],
        });
      });

      it('throws error on MOR fault', async () => {
        const promise = changer.change(2);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
        await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'move index 2', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE FS 1');
        await eqAndPushX(XMOR, 1, 'warnings clear', 'OK IDLE FS 1 FS');
        const err: MovementFailedException = await expect(promise).to.be.rejectedWith(MovementFailedException);
        expect(err.details).to.deep.eq({
          axis: 1,
          device: 1,
          reason: 'Stalled and Stopped (FS)',
          warnings: ['FS'],
        });
      });
    });

    describe('homing', () => {
      it('homes the changer if needed', async () => {
        const promise = changer.change(1);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE WR 1 WR');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY WR 1');
        await eqAndPushX(XMOR, 1, 'warnings clear', 'OK IDLE WR 1 WR');

        await eqAndPushX(XLDA, 1, 'tools gotolimit hardstop neg 1 0', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XMOR, 1, 'home', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XMOR, 1, 'move index 1', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XLDA, 1, 'home', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XLDA, 1, 'move abs 20000000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });

      it('homes the changer if needed (move min variant)', async () => {
        const promise = changer.change(1);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE WR 1 WR');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY WR 1');
        await eqAndPushX(XMOR, 1, 'warnings clear', 'OK IDLE WR 1 WR');

        await eqAndPushX(XLDA, 1, 'tools gotolimit hardstop neg 1 0', 'RJ IDLE -- BADCOMMAND');
        await eqAndPushX(XLDA, 1, 'get limit.approach.maxspeed', 'OK IDLE -- 16384000');
        await eqAndPushX(XLDA, 1, 'move min 16384000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK BUSY FS 2 WS FS');
        await eqAndPushX(XLDA, 1, 'move rel 1000000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XMOR, 1, 'home', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XMOR, 1, 'move index 1', 'OK BUSY -- 0');
        await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XLDA, 1, 'home', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');

        await eqAndPushX(XLDA, 1, 'move abs 20000000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });

      it('fails on other errors during homing (move min variant)', async () => {
        const promise = changer.change(1);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE WR 1 WR');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 2');
        await eqAndPushX(XLDA, 1, 'tools gotolimit hardstop neg 1 0', 'RJ IDLE -- BADCOMMAND');
        await eqAndPushX(XLDA, 1, 'get limit.approach.maxspeed', 'OK IDLE -- 16384000');

        await eqAndPushX(XLDA, 1, 'move min 16384000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK BUSY FQ 1 FQ');
        await expect(promise).to.be.rejectedWith(MovementFailedException);
      });
    });

    describe('release', () => {
      it('moves the focus stage into minimum position', async () => {
        const promise = changer.release();
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
        await promise;
      });
    });

    describe('getCurrentObjective', () => {
      it('gets the current objective', async () => {
        const promise = changer.getCurrentObjective();
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK IDLE -- 3');
        expect(await promise).to.eq(3);
      });
    });

    describe('getNumberOfObjectives', () => {
      it('calculates indexed positions for cyclic device', async () => {
        const promise = changer.getNumberOfObjectives();
        await eqAndPushX(1, 1, 'get motion.index.dist', 'OK IDLE -- 5');
        await eqAndPushX(1, 1, 'get limit.cycle.dist', 'OK IDLE -- 20');
        expect(await promise).to.eq(4);
      });
    });

    describe('getFocusDatum', () => {
      it('gets the datum without request', async () => {
        expect(await changer.getFocusDatum()).to.deep.eq(20000000);
      });
    });

    describe('setFocusDatum', () => {
      it('sets the datum and uses it during changing', async () => {
        let promise = changer.setFocusDatum(10000000);
        await eqAndPushX(XLDA, 1, 'set limit.home.offset 10000000', 'OK IDLE -- 0');
        await promise;

        expect(await changer.getFocusDatum()).to.deep.eq(10000000);

        promise = changer.change(1);
        await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
        await eqAndPushX(XLDA, 1, 'move abs 10000000', 'OK BUSY -- 0');
        await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
      });
    });
  });

  describe('found - identified', () => {
    let changer: ObjectiveChanger;

    beforeEach(async () => {
      await DeviceXMOR.identify(connection.getDevice(XMOR), server);
      await DeviceXLDA.identify(connection.getDevice(XLDA), server);

      const promise = ObjectiveChanger.find(connection);
      await eqAndPushX(XLDA, 1, 'get limit.home.offset', 'OK IDLE -- 20000000');
      changer = await promise;
    });

    it('moves to the provided offset', async () => {
      const promise = changer.change(2, { focusOffset: { value: 0.123, unit: Length.MICROMETRES } });
      await eqAndPushX(XLDA, 1, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(XMOR, 1, 'get motion.index.num', 'OK BUSY -- 1');
      await eqAndPushX(XLDA, 1, 'move min', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
      await eqAndPushX(XMOR, 1, 'move index 2', 'OK BUSY -- 0');
      await eqAndPushX(XMOR, 1, '', 'OK IDLE -- 0');
      await eqAndPushX(XLDA, 1, 'move abs 20000123', 'OK BUSY -- 0');
      await eqAndPushX(XLDA, 1, '', 'OK IDLE -- 0');
      await promise;
    });

    describe('getFocusDatum', () => {
      it('gets the datum in the specified unit', async () => {
        expect(await changer.getFocusDatum(Length.mm)).to.deep.eq(20);
      });
    });

    describe('setFocusDatum', () => {
      it('sets the datum in the specified unit', async () => {
        const promise = changer.setFocusDatum(10, Length.mm);
        await eqAndPushX(XLDA, 1, 'set limit.home.offset 10000000', 'OK IDLE -- 0');
        await promise;

        expect(await changer.getFocusDatum(Length.mm)).to.deep.eq(10);
      });
    });
  });

  describe('constructed', () => {
    let changer: ObjectiveChanger;
    const FOCUS_ADDRESS = 2;
    const FOCUS_AXIS = 3;

    beforeEach(() => {
      changer = new ObjectiveChanger(
        new Device(connection, XMOR),
        new Axis(new Device(connection, FOCUS_ADDRESS), FOCUS_AXIS));
    });

    it('uses specified axis number', async () => {
      let promise = changer.change(1);
      await objectiveChangerHome(server, XMOR, FOCUS_ADDRESS, { focusAxis: FOCUS_AXIS });
      await promise;

      promise = changer.release();
      await eqAndPushX(FOCUS_ADDRESS, FOCUS_AXIS, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(FOCUS_ADDRESS, FOCUS_AXIS, 'move min', 'OK BUSY -- 0');
      await eqAndPushX(FOCUS_ADDRESS, FOCUS_AXIS, '', 'OK IDLE -- 0');
      await promise;
    });

    it('queries home offset again when focus axis changes', async () => {
      let promise = changer.change(1);
      await eqAndPushX(FOCUS_ADDRESS, FOCUS_AXIS, 'get limit.home.offset', 'OK IDLE -- 10000');
      await objectiveChangerChange(server, XMOR, FOCUS_ADDRESS, { focusAxis: FOCUS_AXIS, focusOffset: 10000 });
      await promise;
      promise = changer.change(2);
      await objectiveChangerChange(server, XMOR, FOCUS_ADDRESS, { focusAxis: FOCUS_AXIS, focusOffset: 10000, index: 2 });
      await promise;

      changer = new ObjectiveChanger(
        new Device(connection, XMOR),
        new Axis(new Device(connection, 6), 4));

      promise = changer.change(1);
      await eqAndPushX(6, 4, 'get limit.home.offset', 'OK IDLE -- 20000');
      await objectiveChangerChange(server, XMOR, 6, { focusAxis: 4, focusOffset: 20000 });
      await promise;
    });

    describe('getFocusDatum', () => {
      it('gets the datum', async () => {
        const promise = changer.getFocusDatum();
        await eqAndPushX(FOCUS_ADDRESS, FOCUS_AXIS, 'get limit.home.offset', 'OK IDLE -- 100000');
        expect(await promise).to.deep.eq(100000);
      });
    });
  });
});
