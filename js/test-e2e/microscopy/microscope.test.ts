import { expect } from 'chai';
import { Connection } from '../../src/ascii';
import { Microscope, MicroscopeConfig, WdiAutofocusProvider } from '../../src/microscopy';
import { eqAndPush } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { parseCommand } from '../ascii/protocol';
import { objectiveChangeHomeHasReference, objectiveChangerChange, objectiveChangerHome } from './objective_changer_utils';
import { WDI_TEST_PORT, WdiProtocol } from './wdi/wdi_protocol';
import { BinaryTestServer } from './wdi/binary_test_server';
import { connectWdiAutofocus } from './wdi/utils';
import { autofocusInitialize } from './autofocus_utils';

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

function getConfig(microscope: Microscope): MicroscopeConfig {
  return (microscope as unknown as { _config: MicroscopeConfig })._config;
}

const wdiServer = new WdiProtocol(new BinaryTestServer());

describe('Microscope', () => {
  let connection: Connection;

  beforeAll(async () => {
    await server.start();
    await wdiServer.server.start(WDI_TEST_PORT);
  });

  afterAll(async () => {
    await server.stop();
    await wdiServer.server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('find', () => {
    let wdiClient: WdiAutofocusProvider;

    beforeEach(async () => {
      wdiClient = await connectWdiAutofocus(wdiServer);
    });

    afterEach(async () => {
      await wdiClient.close();
      await wdiServer.server.closeSocket();
    });

    it('finds all the microscope parts', async () => {
      const promise = Microscope.find(connection, { autofocus: wdiClient.providerId });

      // lamp
      let cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@02 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@03 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@05 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@04 0 ${cmd.id} OK IDLE -- 1`);
      await eqAndPushX(4, 0, 'get system.axiscount', 'OK IDLE -- 1');
      await eqAndPushX(4, 1, 'get lamp.flux.max', 'OK IDLE -- 100');

      // storage identified parts
      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id storage all print prefix zaber.microscope.part');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#01 1 ${cmd.id} set zaber.microscope.part focus`);
      await server.push(`#01 0 ${cmd.id} set zaber.microscope.part.camera_trigger do=2`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#02 1 ${cmd.id} set zaber.microscope.part filter_changer`);
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@04 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@05 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#05 1 ${cmd.id} set zaber.microscope.part x`);
      await server.push(`#05 2 ${cmd.id} set zaber.microscope.part y`);

      // objective changer based on device.id 53115
      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get device.id');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 50852`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 50838`);
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 53115`);
      await server.push(`@04 0 ${cmd.id} OK IDLE -- 52005`);
      await eqAndPushX(1, 1, 'get limit.home.offset', 'OK IDLE -- 1000');

      // autofocus
      await eqAndPushX(1, 1, 'storage axis print prefix zaber.microscope.af.obj.', 'OK IDLE -- 0');
      await eqAndPushX(1, 1, '', 'OK IDLE -- 0'); // multi-response end
      await eqAndPushX(3, 1, 'get motion.index.num', 'OK IDLE -- 0');

      const microscope = await promise;
      expect(getConfig(microscope)).to.deep.eq({
        filterChanger: 2,
        focusAxis: {
          axis: 1,
          device: 1,
        },
        illuminator: 4,
        objectiveChanger: 3,
        xAxis: {
          axis: 1,
          device: 5,
        },
        yAxis: {
          axis: 2,
          device: 5,
        },
        autofocus: wdiClient.providerId,
        cameraTrigger: {
          device: 1,
          channel: 2,
        },
      });
      expect(microscope.filterChanger).to.not.be.null;
      expect(microscope.focusAxis).to.not.be.null;
      expect(microscope.xAxis).to.not.be.null;
      expect(microscope.yAxis).to.not.be.null;
      expect(microscope.objectiveChanger).to.not.be.null;
      expect(microscope.illuminator).to.not.be.null;
      expect(microscope.autofocus).to.not.be.null;
      expect(microscope.cameraTrigger).to.not.be.null;
    });

    it('finds none of the microscope parts', async () => {
      const promise = Microscope.find(connection);
      // lamp
      let cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@02 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@03 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);

      // storage identified parts
      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id storage all print prefix zaber.microscope.part');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@03 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);

      // no objective changer since there is no focus

      const microscope = await promise;
      expect(getConfig(microscope)).to.deep.eq({
        filterChanger: null,
        focusAxis: null,
        illuminator: null,
        objectiveChanger: null,
        xAxis: null,
        yAxis: null,
        autofocus: null,
        cameraTrigger: null,
      });
      expect(microscope.filterChanger).to.be.undefined;
      expect(microscope.focusAxis).to.be.undefined;
      expect(microscope.xAxis).to.be.undefined;
      expect(microscope.yAxis).to.be.undefined;
      expect(microscope.objectiveChanger).to.be.undefined;
      expect(microscope.illuminator).to.be.undefined;
      expect(microscope.cameraTrigger).to.be.undefined;
    });

    it('throws error when it finds multiple parts (focus axis)', async () => {
      const promise = Microscope.find(connection);

      let cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@02 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);

      // storage identified parts
      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id storage all print prefix zaber.microscope.part');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#01 1 ${cmd.id} set zaber.microscope.part focus`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#02 1 ${cmd.id} set zaber.microscope.part focus`);

      await expect(promise).to.be.rejectedWith('Multiple focus axes detected. Create the microscope configuration manually.');
    });

    it('throws error when it finds multiple parts (lamps)', async () => {
      const promise = Microscope.find(connection);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 0`);

      await expect(promise).to.be.rejectedWith('Multiple illuminators detected. Create the microscope configuration manually.');
    });

    it('throws error when it finds multiple parts (objective changers)', async () => {
      const promise = Microscope.find(connection);

      let cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get lamp.status');
      await server.push(`@01 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@02 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
      await server.push(`@03 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);

      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id storage all print prefix zaber.microscope.part');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 0`);
      await server.push(`#03 1 ${cmd.id} set zaber.microscope.part focus`);

      // objective changer based on device.id 53115
      cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/0 0 id get device.id');
      await server.push(`@01 0 ${cmd.id} OK IDLE -- 53115`);
      await server.push(`@02 0 ${cmd.id} OK IDLE -- 53115`);
      await server.push(`@03 0 ${cmd.id} OK IDLE -- 12345`);

      await expect(promise).to.be.rejectedWith('Multiple objective changers detected. Create the microscope configuration manually.');
    });
  });

  describe('full setup', () => {
    let microscope: Microscope;
    let wdiClient: WdiAutofocusProvider;
    const FOCUS = 1;
    const FILTER_CHANGER = 2;
    const OBJECTIVE_CHANGER = 3;
    const ILLUMINATOR = 4;
    const PLATE = 5;

    beforeEach(async () => {
      wdiClient = await connectWdiAutofocus(wdiServer);

      microscope = new Microscope(connection, {
        focusAxis: {
          device: FOCUS,
          axis: 1,
        },
        filterChanger: FILTER_CHANGER,
        objectiveChanger: OBJECTIVE_CHANGER,
        illuminator: ILLUMINATOR,
        xAxis: {
          device: PLATE,
          axis: 1,
        },
        yAxis: {
          device: PLATE,
          axis: 2,
        },
        autofocus: wdiClient.providerId,
        cameraTrigger: {
          device: PLATE,
          channel: 2,
        },
      });
    });

    afterEach(async () => {
      await wdiClient.close();
      await wdiServer.server.closeSocket();
    });

    describe('initialize', () => {
      it('initializes all the microscope parts', async () => {
        const promise = microscope.initialize();

        await objectiveChangerHome(server, OBJECTIVE_CHANGER, 1, { homeOnly: true });

        for (const axis of [1, 2]) {
          await eqAndPushX(PLATE, axis, 'warnings clear', 'OK IDLE WR 1 WR');
          await eqAndPushX(PLATE, axis, 'home', 'OK BUSY WR 0');
          await eqAndPushX(PLATE, axis, 'warnings clear', 'OK BUSY WR 1 WR');
          await eqAndPushX(PLATE, axis, '', 'OK IDLE -- 0');
        }

        await eqAndPushX(FILTER_CHANGER, 1, 'warnings clear', 'OK IDLE WR 1 WR');
        await eqAndPushX(FILTER_CHANGER, 1, 'home', 'OK BUSY WR 0');
        await eqAndPushX(FILTER_CHANGER, 1, 'warnings clear', 'OK BUSY WR 1 WR');
        await eqAndPushX(FILTER_CHANGER, 1, '', 'OK IDLE -- 0');

        await promise;
      });
      it('force initializes all the microscope parts', async () => {
        const promise = microscope.initialize({ force: true });

        await objectiveChangeHomeHasReference(server, OBJECTIVE_CHANGER, 1, { forceHome: true });

        for (const axis of [1, 2]) {
          await eqAndPushX(PLATE, axis, 'home', 'OK BUSY -- 0');
          await eqAndPushX(PLATE, axis, '', 'OK IDLE -- 0');
        }

        await eqAndPushX(FILTER_CHANGER, 1, 'home', 'OK BUSY -- 0');
        await eqAndPushX(FILTER_CHANGER, 1, '', 'OK IDLE -- 0');

        await promise;
      });
      it('does not do anything if homing is not required', async () => {
        const promise = microscope.initialize();

        await objectiveChangeHomeHasReference(server, OBJECTIVE_CHANGER, 1);

        for (const axis of [1, 2]) {
          await eqAndPushX(PLATE, axis, 'warnings clear', 'OK IDLE -- 0');
        }

        await eqAndPushX(FILTER_CHANGER, 1, 'warnings clear', 'OK IDLE -- 0');

        await promise;
      });
    });

    describe('objective changer', () => {
      it('synchronizes objectives with autofocus provider', async () => {
        const OBJECTIVE = 3;

        // The autofocus does not synchronize objectives unless it's been created by calling some autofocus method.
        // That's not generally a problem since it will be eventually created when e.g. asked to focus.
        const syncPromise = microscope.autofocus!.synchronizeParameters();
        await autofocusInitialize(server, OBJECTIVE_CHANGER, FOCUS);
        await syncPromise;

        const promise = microscope.objectiveChanger!.change(OBJECTIVE);

        await eqAndPushX(FOCUS, 1, 'get limit.home.offset', 'OK IDLE -- 0');
        await objectiveChangerChange(server, OBJECTIVE_CHANGER, FOCUS, { index: OBJECTIVE });

        await eqAndPushX(OBJECTIVE_CHANGER, 1, 'get motion.index.num', `OK IDLE -- ${OBJECTIVE}`);

        const CURRENT_OBJECTIVE_REGISTER = 50;
        let request = await wdiServer.read();
        expect(request.register).to.eq(CURRENT_OBJECTIVE_REGISTER);
        await wdiServer.writeResponse(request, [0]);

        request = await wdiServer.read();
        expect(request.register).to.eq(CURRENT_OBJECTIVE_REGISTER);
        expect(request.data).to.deep.eq([OBJECTIVE - 1]);
        await wdiServer.write('ack');

        await eqAndPushX(FOCUS, 1, 'get motion.tracking.kp motion.tracking.ki', 'OK IDLE -- 0; 0');
        await eqAndPushX(FOCUS, 1, 'set motion.tracking.ki 30', 'OK IDLE -- 0');

        await promise;
      });
    });

    describe('isInitialized', () => {
      it('returns true if no device has WR flag', async () => {
        const promise = microscope.isInitialized();

        await eqAndPushX(OBJECTIVE_CHANGER, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPushX(FOCUS, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPushX(PLATE, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPushX(PLATE, 2, 'warnings', 'OK IDLE -- 0');
        await eqAndPushX(FILTER_CHANGER, 1, 'warnings', 'OK IDLE -- 0');

        expect(await promise).to.eq(true);
      });

      it('returns false if any device has WR flag', async () => {
        const CLEAN = 'OK IDLE -- 0';
        const WR = 'OK IDLE WR 1 WR';

        for (let i = 0; i < 5; i++) {
          const promise = microscope.isInitialized();

          await eqAndPushX(OBJECTIVE_CHANGER, 1, 'warnings', i === 0 ? WR : CLEAN);
          if (i >= 1) {
            await eqAndPushX(FOCUS, 1, 'warnings', i === 1 ? WR : CLEAN);
          }
          if (i >= 2) {
            await eqAndPushX(PLATE, 1, 'warnings', i === 2 ? WR : CLEAN);
          }
          if (i >= 3) {
            await eqAndPushX(PLATE, 2, 'warnings', i === 3 ? WR : CLEAN);
          }
          if (i >= 4) {
            await eqAndPushX(FILTER_CHANGER, 1, 'warnings', i === 4 ? WR : CLEAN);
          }

          expect(await promise).to.eq(false);
        }
      });
    });

    describe('cameraTrigger', () => {
      it('creates instance correctly', async () => {
        const trigger = microscope.cameraTrigger!;
        expect(trigger.device.deviceAddress).to.eq(PLATE);
        expect(trigger.channel).to.eq(2);
      });
    });

    describe('toString', () => {
      it('returns a string representation of the microscope', () => {
        expect(microscope.toString()).to.eq([
          `Microscope -> Connection ${connection.interfaceId} (ASCII TCP: 127.0.0.1:${TEST_PORT})`,
          'Illuminator 4 (Unknown)',
          'Objective Changer 3 (Unknown)',
          'Filter Changer 2 (Unknown)',
          'Focus Axis 1 -> Device 1 (Unknown)',
          'X Axis 1 -> Device 5 (Unknown)',
          'Y Axis 2 -> Device 5 (Unknown)',
          `Autofocus -> WDI autofocus 12345 at 127.0.0.1:${WDI_TEST_PORT}`,
        ].join(', '));
      });

      it('returns representation for closed connection', async () => {
        await connection.close();
        expect(microscope.toString()).to.eq('Microscope -> Connection Closed');
      });
    });
  });

  describe('just focus axis', () => {
    let microscope: Microscope;

    beforeEach(() => {
      microscope = new Microscope(connection, {
        focusAxis: {
          axis: 1,
          device: 1,
        },
      });
    });

    describe('initialize', () => {
      it('initializes all the microscope parts', async () => {
        const promise = microscope.initialize();

        await eqAndPushX(1, 1, 'warnings clear', 'OK IDLE WR 1 WR');
        await eqAndPushX(1, 1, 'home', 'OK BUSY WR 0');
        await eqAndPushX(1, 1, 'warnings clear', 'OK BUSY WR 1 WR');
        await eqAndPushX(1, 1, '', 'OK IDLE -- 0');

        await promise;
      });
    });
  });

  describe('empty', () => {
    let microscope: Microscope;

    beforeEach(() => {
      microscope = new Microscope(connection, { });
    });

    describe('toString', () => {
      it('prints only connection', async () => {
        expect(microscope.toString()).to.eq([
          `Microscope -> Connection ${connection.interfaceId} (ASCII TCP: 127.0.0.1:${TEST_PORT})`,
        ].join(', '));
      });
    });
  });
});
