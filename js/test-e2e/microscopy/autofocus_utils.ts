import { parseCommand } from '../ascii/protocol';
import { TestServer } from '../ascii/tcp';
import { eqAndPush } from '../test_util';

const OBJECTIVES = [
  { ki: 500 },
  { ki: 200 },
  { ki: 30 },
].map((params, i) => ({ number: i + 1, ...params }));

export async function autofocusInitialize(
  server: TestServer,
  turret: number,
  focus: number) {
  const eqAndPushX = eqAndPush.bind(null, server);

  const cmd = parseCommand(await server.pop());
  const prefix = 'storage axis print prefix zaber.microscope.af.obj.';
  expect(cmd.data.substring(0, prefix.length)).toEqual(prefix);

  await server.push(`@${focus} 1 ${cmd.id} OK IDLE -- 0`);
  for (const { number, ki } of OBJECTIVES) {
    const keyPrefix = `zaber.microscope.af.obj.${number}`;
    await server.push(`#${focus} 1 ${cmd.id} set ${keyPrefix}.ki ${ki}`);
  }
  await eqAndPushX(focus, 1, '', 'OK IDLE -- 0'); // multi-response end

  await eqAndPushX(turret, 1, 'get motion.index.num', 'OK IDLE -- 0');
}
