import { InvalidArgumentException, InvalidDataException } from '../../src';
import { WdiAutofocusProvider } from '../../src/microscopy';
import { BinaryTestServer } from './wdi/binary_test_server';
import { connectWdiAutofocus } from './wdi/utils';
import { WDI_TEST_PORT, WdiProtocol } from './wdi/wdi_protocol';

const server = new WdiProtocol(new BinaryTestServer());

let client: WdiAutofocusProvider;

beforeAll(async () => {
  await server.server.start(WDI_TEST_PORT);
});

afterAll(async () => {
  await server.server.stop();
});

beforeEach(async () => {
  client = await connectWdiAutofocus(server);
});

afterEach(async () => {
  await client.close();
  await server.server.closeSocket();
});

describe('read', () => {
  test('reads a register (byte)', async () => {
    const promise = client.genericRead(50, 1);

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'read',
      registerBank: 'U',
      register: 50,
      size: 'uint8',
      offset: 0,
      dataCount: 1,
    });
    await server.writeResponse(request, [42]);

    const response = await promise;
    expect(response).toEqual([42]);
  });

  test('reads a register (size, offset, bank)', async () => {
    const promise = client.genericRead(50, 4, { offset: 11, registerBank: 'X' });

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'read',
      registerBank: 'X',
      register: 50,
      size: 'uint32',
      offset: 11,
      dataCount: 1,
    });
    const data = [0x12345678];
    await server.writeResponse(request, data);

    const response = await promise;
    expect(response).toEqual(data);
  });

  test('reads a register (array)', async () => {
    const promise = client.genericRead(50, 2, { count: 4 });

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'read',
      registerBank: 'U',
      register: 50,
      size: 'uint16',
      dataCount: 4,
    });
    const data = [0x1234, 0x5678, 0x1234, 0x5678];
    await server.writeResponse(request, data);

    const response = await promise;
    expect(response).toEqual(data);
  });

  test('handles highest uint32, uint16, uint8 as signed', async () => {
    for (const [highest, size] of [[0xFFFFFFFF, 4], [0xFFFF, 2], [0xFF, 1]]) {
      const promise = client.genericRead(50, size);
      const request = await server.read();
      await server.writeResponse(request, [highest]);
      expect(await promise).toEqual([-1]);
    }
  });

  test('throws errors invalid arguments', async () => {
    await expect(client.genericRead(50, 3)).rejects.toThrow(InvalidArgumentException);
    await expect(client.genericRead(50, 0)).rejects.toThrow(InvalidArgumentException);
  });

  test('throws error on NAK (read)', async () => {
    const promise = client.genericRead(50, 1);

    await server.read();
    await server.write('nack');

    await expect(promise).rejects.toThrow(InvalidDataException);
    const error = await promise.catch(e => e);
    expect(error.message).toEqual('Device responded with NAK: Read request from U50');
  });

  test('throws error on NAK (write)', async () => {
    const promise = client.genericWrite(42, 1, [123], { registerBank: 't' });

    await server.read();
    await server.write('nack');

    await expect(promise).rejects.toThrow(InvalidDataException);
    const error = await promise.catch(e => e);
    expect(error.message).toEqual('Device responded with NAK: Write request to t42');
  });
});


describe('write', () => {
  test('writes a register (no data)', async () => {
    const promise = client.genericWrite(50);

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'write',
      registerBank: 'U',
      register: 50,
      size: null,
      offset: 0,
      dataCount: 0,
      data: [],
    });
    await server.write('ack');

    await promise;
  });

  test('writes a register (byte)', async () => {
    const promise = client.genericWrite(50, 1, [123]);

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'write',
      size: 'uint8',
      offset: 0,
      dataCount: 1,
      data: [123],
    });
    await server.write('ack');

    await promise;
  });

  test('writes a register (size, offset, bank)', async () => {
    const promise = client.genericWrite(50, 2, [0x1234], { offset: 11, registerBank: 'X' });

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'write',
      registerBank: 'X',
      register: 50,
      size: 'uint16',
      offset: 11,
      dataCount: 1,
      data: [0x1234],
    });
    await server.write('ack');

    await promise;
  });

  test('writes a register (array)', async () => {
    const promise = client.genericWrite(50, 4, [0x12345678, 0x12345678]);

    const request = await server.read();
    expect(request).toMatchObject({
      request: 'write',
      size: 'uint32',
      dataCount: 2,
      data: [0x12345678, 0x12345678],
    });
    await server.write('ack');

    await promise;
  });

  test('throws errors invalid arguments', async () => {
    await expect(client.genericWrite(50, 0, [1])).rejects.toThrow(InvalidArgumentException);
  });
});

describe('getStatus', () => {
  test('gets the status', async () => {
    const onStatus = (1 << 4) | (1 << 12);

    for (const status of [onStatus, 0]) {
      const promise = client.getStatus();

      const request = await server.read();
      expect(request).toMatchObject({
        request: 'read',
        register: 20,
        size: 'uint16',
      });
      await server.writeResponse(request, [status]);

      const response = await promise;
      expect(response).toMatchObject({
        inRange: status > 0,
        laserOn: status > 0,
      });
    }
  });

  describe('toString', () => {
    test('returns string representation', () => {
      expect(client.toString()).toMatch(/^WDI autofocus 12345 at/);
    });
    it('returns representation for closed connection', async () => {
      await client.close();
      expect(client.toString()).toEqual('WDI autofocus (Connection closed)');
    });
  });
});
