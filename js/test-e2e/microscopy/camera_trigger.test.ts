import { expect } from 'chai';
import { Connection } from '../../src/ascii';
import { CameraTrigger } from '../../src/microscopy';
import { eqAndPush, measureTime } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from '../ascii/tcp';
import { Time } from '../../src';
import { DeviceXMCC4 } from '../ascii/devices/xmcc4';

const server = new TestServer();
const eqAndPushX = eqAndPush.bind(null, server);

describe('CameraTrigger', () => {
  let connection: Connection;
  let trigger: CameraTrigger;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();

    const device = connection.getDevice(1);
    trigger = new CameraTrigger(device, 3);
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('identified', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(connection.getDevice(1), server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_38);
    });

    it('schedules digital output', async () => {
      const promise = trigger.trigger(1500, Time.MICROSECONDS);
      await eqAndPushX(1, 0, 'io set do 3 1 schedule 1.5 0', 'OK IDLE -- 0');
      await promise;
    });

    it('waits for the duration of trigger', async () => {
      const duration = await measureTime(async () => {
        const promise = trigger.trigger(0.2, Time.SECONDS);
        await eqAndPushX(1, 0, /./, 'OK IDLE -- 0');
        await promise;
      });
      expect(duration).to.be.greaterThan(200);
    });

    it('does not wait', async () => {
      const promise = trigger.trigger(600, Time.SECONDS, { wait: false });
      await eqAndPushX(1, 0, /./, 'OK IDLE -- 0');
      await promise;
    });
  });

  it('works with native units; waits in milliseconds', async () => {
    const WAIT = 50;
    const duration = await measureTime(async () => {
      const promise = trigger.trigger(WAIT);
      await eqAndPushX(1, 0, `io set do 3 1 schedule ${WAIT} 0`, 'OK IDLE -- 0');
      await promise;
    });
    expect(duration).to.be.greaterThan(WAIT);
  });
});
