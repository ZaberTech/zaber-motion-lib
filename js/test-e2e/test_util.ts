import { expect, Assertion } from 'chai';
import { parseCommand } from './ascii//protocol';
import { TestServer } from './ascii//tcp';
import { TIMEOUT_ERROR_MARGIN } from './constants';

export async function eqAndPush(
  server: TestServer, device: number, axis: number, popStr: string | RegExp | null, pushStr: string
): Promise<ReturnType<typeof parseCommand>> {
  const command = parseCommand(await server.pop());

  try {
    expect(command.device).to.eq(device);
    expect(command.axis).to.eq(axis);

    if (typeof popStr === 'string') {
      expect(command.data).to.eq(popStr);
    } else if (popStr instanceof RegExp) {
      expect(command.data).to.match(popStr);
    }
  } catch (err) {
    throw new Error(`${command.strNoId} != ${device} ${axis} ${popStr}: ${String(err)}`);
  }
  await server.push(`@${device} ${axis} ${command.id} ${pushStr}`);

  return command;
}

export const itTime = process.env.ZML_SKIP_TIMED_TESTS ? it.skip : it;

// The margin to be early is 5ms, as when we switched to jest things strangely started to arrive a bit earlier.
const EARLY_MARGIN = 5;

Assertion.addMethod('onTime', function(expectedTime: number, marginToBeLate: number = TIMEOUT_ERROR_MARGIN): void {
  const actualTime = this._obj;
  new Assertion(Math.ceil(actualTime + EARLY_MARGIN)).to.be.gte(expectedTime, `Expected ${actualTime} to take at least ${expectedTime}.`);
  new Assertion(actualTime)
    .to.be.lte(expectedTime + marginToBeLate, `Expected ${actualTime} to take at most ${expectedTime} + ${marginToBeLate}`);
});

export async function expectStorage(
  server: TestServer,
  device: number,
  axis: number,
  key: string,
  appendsExpected: number
): Promise<string> {
  const storedData: string[] = [];

  for (let i = 0; i <= appendsExpected; i++) {
    let command: ReturnType<typeof parseCommand>;
    let keepReading = true;
    let firstReply = true;
    do {
      command = parseCommand(await server.pop());
      if (firstReply) {
        expect(command.strNoId).to.startWith(`/${device} ${axis} id storage ${axis ? 'axis ' : ''}${i === 0 ? 'set' : 'append'} ${key}`);

        const data = command.data.match(/(?:append|set) \S+ ([^\\]+)\\?$/)?.[1];
        if (data != null) {
          storedData.push(data);
        }
      } else {
        const data = command.data.match(/^cont \d+ ([^\\]+)\\?$/)![1];
        storedData.push(data);
      }

      firstReply = false;
      keepReading = command.data.endsWith('\\');
    } while (keepReading);

    await server.push(`@${device} ${axis} ${command.id} OK IDLE -- 0`);
  }

  return storedData.join(' ');
}

export async function measureTime(func: () => Promise<void>): Promise<number> {
  const start = performance.now();
  await func();
  return performance.now() - start;
}
