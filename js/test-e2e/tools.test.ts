import { expect } from 'chai';

import { Tools } from '../src';

describe('Tools', () => {
  describe('listSerialPorts', () => {
    it('returns string array without error', async () => {
      const ports = await Tools.listSerialPorts();
      expect(Array.isArray(ports)).to.eq(true);
    });
  });

  describe('getMessageRouterPipePath', () => {
    it('returns string that looks like a pipe on a given platform', async () => {
      const path = Tools.getMessageRouterPipePath();
      if (process.platform === 'win32') {
        expect(path).startWith('\\\\.\\pipe\\zaber');
      } else {
        expect(path).startsWith('/');
        expect(path).endsWith('.sock');
      }
    });
  });

  describe('getDeviceDbPipePath', () => {
    it('returns string that looks like a pipe on a given platform', async () => {
      const path = Tools.getDbServicePipePath();
      if (process.platform === 'win32') {
        expect(path).startWith('\\\\.\\pipe\\zaber');
      } else {
        expect(path).to.match(/^\/.*zaber-db-service.*sock$/);
      }
    });
  });
});
