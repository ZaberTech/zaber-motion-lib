import { expect } from 'chai';

import { AbsoluteTemperature, Acceleration, AngularAcceleration, Frequency, Length, Time, UnitTable, Units, Velocity } from '../src';

describe('UnitTable', () => {
  describe('lookup by symbol', () => {
    it('returns expected results', () => {
      expect(UnitTable.getUnit('mm')).to.be.eq(Length.MILLIMETRES);
      expect(UnitTable.getUnit('in/s')).to.be.eq(Velocity.INCHES_PER_SECOND);
      expect(UnitTable.getUnit('°/s²')).to.be.eq(AngularAcceleration.DEGREES_PER_SECOND_SQUARED);
      expect(UnitTable.getUnit('deg/s^2')).to.be.eq(AngularAcceleration.DEGREES_PER_SECOND_SQUARED);
      expect(UnitTable.getUnit('kHz')).to.be.eq(Frequency.KILOHERTZ);
    });

    it('throws for unsupported symbols', () => {
      expect(() => UnitTable.getUnit('°C')).to.throw(/No value/);
    });
  });

  describe('lookup by enum', () => {
    it('returns expected results', () => {
      expect(UnitTable.getSymbol(Length.MILLIMETRES)).to.be.eql('mm');
      expect(UnitTable.getSymbol(Velocity.METRES_PER_SECOND)).to.be.eql('m/s');
      expect(UnitTable.getSymbol(Acceleration.MICROMETRES_PER_SECOND_SQUARED)).to.be.eql('µm/s²');
      expect(UnitTable.getSymbol(Time.MILLISECONDS)).to.be.eql('ms');
      expect(UnitTable.getSymbol(AbsoluteTemperature.DEGREES_FAHRENHEIT)).to.be.eql('°F');
    });

    it('throws for units with no shortname', () => {
      expect(() => UnitTable.getSymbol(Units.NATIVE)).to.throw(/No value/);
    });
  });
});
