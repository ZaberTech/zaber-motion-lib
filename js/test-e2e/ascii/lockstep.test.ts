import { expect } from 'chai';

import { Connection, Device, Lockstep } from '../../src/ascii';
import {
  Length, Velocity, MovementFailedException, MovementInterruptedException,
  LockstepEnabledException, LockstepNotEnabledException, Units, InvalidArgumentException, Acceleration, Angle,
  Time,
  BadDataException,
} from '../../src';

import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { DeviceXMCB2 } from './devices/xmcb2';
import { eqAndPush } from '../test_util';
import { parseCommand } from './protocol';
import { DeviceXMCC4 } from './devices/xmcc4';

const MM_UNITS = { velocityUnit: Velocity['mm/s'] as const, accelerationUnit: Acceleration['mm/s²'] as const };

describe('2-Axis Lockstep', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let lockstep: Lockstep;
  let eqAndPushX: (axis: number, popStr: string | RegExp, pushStr: string) => Promise<unknown>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1);
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    lockstep = device.getLockstep(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('enable', () => {
    it('enables a lockstep group', async () => {
      const promise = lockstep.enable(1, 2, 4);
      await eqAndPushX(0, 'lockstep 1 setup enable 1 2 4', 'OK IDLE -- 0');
      await promise;
    });
    it('rejects whilst enabled', async () => {
      const promise = lockstep.enable(2, 1);
      await eqAndPushX(0, 'lockstep 1 setup enable 2 1', 'RJ IDLE -- LOCKSTEP');
      const error: LockstepEnabledException = await promise.catch(e => e);
      expect(error.message).to.startWith('Command "lockstep 1 setup enable 2 1" rejected: LOCKSTEP');
    });
    it('rejects invalid axis selection', async () => {
      const promise = lockstep.enable(0, 1);
      await eqAndPushX(0, 'lockstep 1 setup enable 0 1', 'RJ IDLE -- BADDATA');
      await expect(promise).to.be.rejectedWith(BadDataException);
    });
    it('rejects less than two axes', async () => {
      const promise = lockstep.enable(1);
      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      const error: InvalidArgumentException = await promise.catch(e => e);
      expect(error.message).to.eq('Lockstep must be enabled on at least two axes (provided: 1).');
    });
  });

  describe('disable', () => {
    it('disables a 1-2 lockstep group', async () => {
      const promise = lockstep.disable();
      await eqAndPushX(0, 'lockstep 1 setup disable', 'OK IDLE -- 0');
      await promise;
    });
    it('no op if not enabled', async () => {
      const promise = lockstep.disable();
      await eqAndPushX(0, 'lockstep 1 setup disable', 'RJ IDLE -- BADDATA');
      await promise;
    });
  });

  describe('stop', () => {
    it('sends stop request', async () => {
      const promise = lockstep.stop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 stop', 'OK BUSY -- 0');
      await promise;
    });
    it('stops ongoing lockstep movement', async () => {
      const promise = lockstep.stop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY NI 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 stop', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('caches lockstep group and stops ongoing lockstep movement using it', async () => {
      const firstStop = lockstep.stop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY NI 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 stop', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await firstStop;
      const secondStop = lockstep.stop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 stop', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await secondStop;
    });
    it('waits until idle', async () => {
      const promise = lockstep.stop();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 stop', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.stop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('home', () => {
    it('sends home request', async () => {
      const promise = lockstep.home({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 home', 'OK BUSY -- 0');
      await promise;
    });
    it('waits until idle', async () => {
      const promise = lockstep.home();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 home', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.home();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('moveMax', () => {
    it('sends proper move max request', async () => {
      const promise = lockstep.moveMax({ waitUntilIdle: false, velocity: 10, acceleration: 100 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move max 10 100', 'OK BUSY -- 0');
      await promise;
    });
    it('waits until idle', async () => {
      const promise = lockstep.moveMax();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move max', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.moveMax();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('moveMin', () => {
    it('sends proper move min request', async () => {
      const promise = lockstep.moveMin({ waitUntilIdle: false, velocity: 10, acceleration: 100 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move min 10 100', 'OK BUSY -- 0');
      await promise;
    });
  });

  describe('moveAbsolute', () => {
    describe('different units', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, undefined, DeviceXMCB2.FWType.FW7);
        connection.resetIds();
      });
      it('handles different units', async () => {
        const promise = lockstep.moveAbsolute(123, Length.cm, { waitUntilIdle: false, velocity: 10, acceleration: 100, ...MM_UNITS });
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, 'lockstep 1 move abs 9917480 132104 132', 'OK BUSY -- 0');
        await promise;
      });
      it('handles different units + wait until idle', async () => {
        const promise = lockstep.moveAbsolute(1, Length.m);
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 10 0');
        await eqAndPushX(0, /^lockstep 1 move abs \d+$/, 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK IDLE -- 0');
        await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
        await promise;
      });
      it('handles bad data + different units', async () => {
        const promise = lockstep.moveAbsolute(123, Length.cm);
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, /^lockstep 1 move abs \d+$/, 'RJ IDLE -- BADDATA');
        await expect(promise).to.be.rejectedWith(BadDataException);
      });
    });
    it('sends proper move request', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false, velocity: 10, acceleration: 100 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123 10 100', 'OK BUSY -- 0');
      await promise;
    });
    it('sends proper move request, with cached group', async () => {
      const firstMove = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await firstMove;
      const secondMove = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await secondMove;
    });
    it('handles bad data', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'RJ IDLE -- BADDATA');
      await expect(promise).to.be.rejectedWith(BadDataException);
    });
    it('handles bad data + wait until idle', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'RJ IDLE -- BADDATA');
      await expect(promise).to.be.rejectedWith(BadDataException);
    });
    it('waits until idle', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('waits until alarm comes as well', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');

      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await server.push('!01 1 IDLE --');

      let command = parseCommand(await server.pop());
      if (command.axis === 1) {
        // resolves race condition when alert does not make it in time
        expect(command.strNoId).to.eq('/1 1 id');
        await server.push(`@1 1 ${command.id} OK BUSY -- 0`);

        command = parseCommand(await server.pop());
      }
      expect(command.strNoId).to.eq('/1 2 id warnings clear');
      await server.push(`@1 2 ${command.id} OK IDLE -- 0`);

      await promise;
    });
    // case where 3 commands get layered, 3rd one interrupts 2nd ones waiting on 1st
    it('throws error when interrupted by other movement (NI flag)', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY NI 01 NI');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY NI 01 NI');
      await expect(promise).to.be.rejectedWith(MovementInterruptedException);
      const error: MovementInterruptedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement was interrupted by: New command.');
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'New command',
        warnings: ['NI'],
      });
      expect(error.details.reason).to.eq('New command');
      expect(error.details.warnings).to.include('NI');
    });
    it('ignores NI flag when present from very beginning', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY NI 01 NI');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY NI 01 NI');
      await promise;
    });
    it('throws error when interrupted by knob (NC flag)', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY NC 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY NC 02 NC NI');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY NC 02 NC NI');
      await expect(promise).to.be.rejectedWith(MovementInterruptedException);
      const error: MovementInterruptedException = await promise.catch(e => e);
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'Manual control',
        warnings: ['NC', 'NI'],
      });
    });
    it('throws error when fault flag is observed on both axes during waiting', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY FQ 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY FQ 01 FQ');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY FS 01 FS');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });
    it('throws error when fault flag is observed on axis 1 during waiting', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY FQ 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY FQ 01 FQ');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY -- 0');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });
    it('throws error when fault flag is observed on axis 2 after waiting', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY FQ 01 FQ');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(error.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });
    it('throws error when fault flag is observed in response', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY FS 0');
      await eqAndPushX(1, 'warnings clear', 'OK IDLE FS 01 FS');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Stalled and Stopped (FS).');
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'Stalled and Stopped (FS)',
        warnings: ['FS'],
      });
    });
    it('throws error when excessive twist', async () => {
      const promise = lockstep.moveAbsolute(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE FT 01 FT');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Excessive Twist (FT).');
      expect(error.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Excessive Twist (FT)',
        warnings: ['FT'],
      });
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.moveAbsolute(100, Length.MICROMETRES);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('moveSin', () => {
    describe('different units', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, undefined, DeviceXMCB2.FWType.FW7);
        connection.resetIds();
      });
      it('handles different units', async () => {
        const promise = lockstep.moveSin(100, Length.MILLIMETRES, 1, Time.SECONDS, { waitUntilIdle: false });
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, 'lockstep 1 move sin 806299 1000.0', 'OK BUSY -- 0');
        await promise;
      });
    });
    it('sends proper request', async () => {
      const promise = lockstep.moveSin(123, Units.NATIVE, 100, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin 123 100', 'OK BUSY -- 0');
      await promise;
    });
    it('sends proper move request, with cached group', async () => {
      const firstMove = lockstep.moveSin(12, Units.NATIVE, 123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin 12 123', 'OK BUSY -- 0');
      await firstMove;
      const secondMove = lockstep.moveSin(15, Units.NATIVE, 123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 move sin 15 123', 'OK BUSY -- 0');
      await secondMove;
    });
    it('handles bad data', async () => {
      const promise = lockstep.moveSin(100, Units.NATIVE, 123, Units.NATIVE, { waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin 100 123', 'RJ IDLE -- BADDATA');
      await expect(promise).to.be.rejectedWith(BadDataException);
    });
    it('waits until idle', async () => {
      const promise = lockstep.moveSin(100, Units.NATIVE, 123, Units.NATIVE);
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin 100 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
  });

  describe('moveSinStop', () => {
    it('Stop sine movement', async () => {
      const promise = lockstep.moveSinStop({ waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin stop', 'OK BUSY -- 1 2 2623 0');
      await promise;
    });

    it('waits until idle', async () => {
      const promise = lockstep.moveSinStop();
      await eqAndPushX(0, 'lockstep 1 info', 'OK BUSY -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move sin stop', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
  });

  describe('moveRelative', () => {
    describe('different units', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, undefined, DeviceXMCB2.FWType.FW7);
        connection.resetIds();
      });
      it('handles different units', async () => {
        const promise = lockstep.moveRelative(123, Length.cm, { waitUntilIdle: false, velocity: 10, acceleration: 100, ...MM_UNITS });
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, /^lockstep 1 move rel \d+ \d+ \d+$/, 'OK BUSY -- 0');
        await promise;
      });
      it('handles different units + wait until idle', async () => {
        const promise = lockstep.moveRelative(1, Length.m);
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 10 0');
        await eqAndPushX(0, /^lockstep 1 move rel \d+$/, 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK BUSY -- 0');
        await eqAndPushX(1, '', 'OK IDLE -- 0');
        await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
        await promise;
      });
    });
    it('sends proper move request', async () => {
      const promise = lockstep.moveRelative(123, Units.NATIVE, { waitUntilIdle: false, velocity: 10, acceleration: 100 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move rel 123 10 100', 'OK BUSY -- 0');
      await promise;
    });
    it('queries maxspeed when acceleration is provided without velocity', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false, velocity: 0, acceleration: 789 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 3 0 0');
      await eqAndPushX(0, 'get maxspeed', 'OK IDLE -- 999 10 456 NA');
      await eqAndPushX(0, 'lockstep 1 move abs 123 456 789', 'OK BUSY -- 0');
      await promise;
    });
    it('accepts positive infinity acceleration and translates it to 0', async () => {
      const promise = lockstep.moveAbsolute(123, Units.NATIVE, { acceleration: Infinity, velocity: 111, waitUntilIdle: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 0 0');
      await eqAndPushX(0, 'lockstep 1 move abs 123 111 0', 'OK BUSY -- 0');
      await promise;
    });
    it('waits until idle', async () => {
      const promise = lockstep.moveRelative(123);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move rel 123', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.moveRelative(100, Length.INCHES);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('moveVelocity', () => {
    describe('different units', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, undefined, DeviceXMCB2.FWType.FW7);
        connection.resetIds();
      });
      it('handles different units', async () => {
        const promise = lockstep.moveVelocity(123, Velocity['cm/s'], { acceleration: 1, accelerationUnit: Acceleration['m/s²'] });
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, /^lockstep 1 move vel \d+ \d+$/, 'OK BUSY -- 0');
        await promise;
      });
      it('handles bad data + different units', async () => {
        const promise = lockstep.moveVelocity(123, Velocity['cm/s']);
        await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
        await eqAndPushX(0, /^lockstep 1 move vel \d+$/, 'RJ IDLE -- BADDATA');
        await expect(promise).to.be.rejectedWith(BadDataException);
      });
    });
    it('sends proper move request', async () => {
      const promise = lockstep.moveVelocity(123, Units.NATIVE, { acceleration: 100 });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await eqAndPushX(0, 'lockstep 1 move vel 123 100', 'OK BUSY -- 0');
      await promise;
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.moveVelocity(10, Velocity.CENTIMETRES_PER_SECOND);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  // lockstep waitUntilIdle functions the same as axis.waitUntilIdle(), it just
  // waits on the first axis in the lockstep group
  describe('waitUntilIdle', () => {
    it('waits until device sends IDLE in status', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0 3 1500 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await eqAndPushX(3, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('waits until device sends alert', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      const loop = (async () => {
        for (;;) {
          const cmd = parseCommand(await server.pop());
          switch (cmd.strNoId) {
            case '/1 1 id':
              await server.push(`@01 1 ${cmd.id} OK BUSY -- 0`);
              break;
            case '/1 2 id warnings clear':
              await server.push(`@01 2 ${cmd.id} OK IDLE -- 0`);
              return;
            default:
              throw new Error(`Unexpected command: ${cmd.strNoId}`);
          }
        }
      })();

      await server.push('!01 1 IDLE --');

      await promise;
      await loop;
    });
    it('ignores alerts with busy status', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await server.push('!01 1 BUSY --');
      await eqAndPushX(1, '', 'OK BUSY -- 0');
      await server.push('!01 1 BUSY --');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('does not throw exception when NI or NC flag is observed', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK BUSY NI 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY NC 02 NC NI');
      await eqAndPushX(1, '', 'OK IDLE -- 0'); // keep waiting in this case
      await eqAndPushX(2, 'warnings clear', 'OK IDLE -- 0');
      await promise;
    });
    it('does not throw exception when fault flag is observed and throwErrorOnFault is false', async () => {
      const promise = lockstep.waitUntilIdle({ throwErrorOnFault: false });
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK BUSY FQ 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await promise;
    });
    it('throws exception when fault is observed on the first axis', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK BUSY FQ 0');
      await eqAndPushX(1, 'warnings clear', 'OK BUSY FQ 01 FQ');
      await eqAndPushX(2, 'warnings clear', 'OK BUSY -- 0');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(error.details).to.deep.eq({
        axis: 1,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });
    it('throws exception when fault is observed on the second axis', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 471 0');
      await eqAndPushX(1, '', 'OK IDLE -- 0');
      await eqAndPushX(2, 'warnings clear', 'OK IDLE FT 01 FT');
      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const error: MovementFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('Movement may have failed because fault was observed: Excessive Twist (FT).');
      expect(error.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Excessive Twist (FT)',
        warnings: ['FT'],
      });
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.waitUntilIdle();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('isBusy', () => {
    it('returns true when lockstep is busy', async () => {
      const promise = lockstep.isBusy();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 478 0');
      await eqAndPushX(2, '', 'OK BUSY -- 0');
      expect(await promise).to.eq(true);
    });
    it('returns false when device is not busy', async () => {
      const promise = lockstep.isBusy();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 478 0');
      await eqAndPushX(2, '', 'OK IDLE -- 0');
      expect(await promise).to.eq(false);
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.isBusy();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('getOffsets', () => {
    it('gets lockstep offsets, native units', async () => {
      const promise = lockstep.getOffsets();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 784781 0 3 5123 2223');
      expect(await promise).to.deep.eq([784781, 5123]);
    });
    it('gets lockstep offsets, mm', async () => {
      await DeviceXMCB2.identify(device, server);
      connection.resetIds();
      const promise = lockstep.getOffsets(Length.mm);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0 3 1000 0');
      expect(await promise).to.deep.eq([0.3253134765625, 0.1240234375]);
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.getOffsets();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
    });
  });

  describe('getTwists', () => {
    it('gets lockstep twists, native units', async () => {
      const promise = lockstep.getTwists();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 10 3 5000 20');
      expect(await promise).to.deep.eq([10, 20]);
    });
    it('gets lockstep twists, mm', async () => {
      await DeviceXMCB2.identify(device, server);
      connection.resetIds();
      const promise = lockstep.getTwists(Length.mm);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 -100 4000 3 0 5000');
      expect(await promise).to.deep.eq([0.49609375, 0.6201171875]);
    });
    it('throws not enabled if not enabled', async () => {
      const promise = lockstep.getTwists();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
    });
  });

  describe('isEnabled', () => {
    it('returns lockstep is enabled', async () => {
      const promise = lockstep.isEnabled();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      expect(await promise).to.eq(true);
    });
    it('returns lockstep is disabled', async () => {
      const promise = lockstep.isEnabled();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      expect(await promise).to.eq(false);
    });
    it('make sure it doesn\'t use cache', async () => {
      const enablePromise = lockstep.enable(1, 2);
      await eqAndPushX(0, 'lockstep 1 setup enable 1 2', 'OK IDLE -- 0');
      await enablePromise;

      const isEnabledPromise = lockstep.isEnabled();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2723 0');
      expect(await isEnabledPromise).to.eq(true);
    });
  });

  describe('getAxes', () => {
    it('returns lockstep axes', async () => {
      const promise = lockstep.getAxes();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 2623 0');
      const axes = await promise;
      expect(axes).to.deep.eq({ axis1: 2, axis2: 1, axis3: 0, axis4: 0 });
    });
    it('returns lockstep axes for more than 2 axes', async () => {
      const promise = lockstep.getAxes();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 2623 0 4 1500 0');
      const axes = await promise;
      expect(axes).to.deep.eq({ axis1: 2, axis2: 1, axis3: 4, axis4: 0 });
    });
    it('does not use cache, uses info command', async () => {
      const firstRequest = lockstep.getAxes();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      await firstRequest;

      const secondRequest = lockstep.getAxes();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      const axes = await secondRequest;
      expect(axes).to.deep.eq({ axis1: 1, axis2: 2, axis3: 0, axis4: 0 });
    });
    it('fails if lockstep not enabled', async () => {
      const promise = lockstep.getAxes();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('getAxisNumbers', () => {
    it('returns lockstep axes', async () => {
      const promise = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 2623 0');
      expect(await promise).to.deep.eq([2, 1]);
    });
    it('returns lockstep axes for more than 2 axes', async () => {
      const promise = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 2623 0 4 1500 0');
      expect(await promise).to.deep.eq([2, 1, 4]);
    });
    it('does not use cache, uses info command', async () => {
      const firstRequest = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 2 2623 0');
      expect(await firstRequest).to.deep.eq([1, 2]);

      const secondRequest = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 1 3 2623 0');
      expect(await secondRequest).to.deep.eq([1, 3]);
    });
    it('fails if lockstep not enabled', async () => {
      const promise = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- disabled');
      await expect(promise).to.be.rejectedWith(LockstepNotEnabledException);
      const error: LockstepNotEnabledException = await promise.catch(e => e);
      expect(error.message).to.eq('Command has failed because the lockstep group is not enabled.');
    });
  });

  describe('getPosition', () => {
    beforeEach(async () => {
      await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.TwoLinear, DeviceXMCB2.FWType.FW7);
    });

    it('returns position of the primary axis', async () => {
      const promise = lockstep.getPosition(Length.mm);
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 0 0');
      await eqAndPushX(2, 'get pos', 'OK IDLE -- 4800');
      expect(await promise).to.be.closeTo(20.25, 0.1 ** 3);
    });
  });

  describe('setTolerance', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary, DeviceXMCC4.FWVersion.FW7_30);
      const promise = lockstep.getAxisNumbers();
      await eqAndPushX(0, 'lockstep 1 info', 'OK IDLE -- 2 1 0 0 3 0 0');
      await promise;
    });

    it('sets tolerance to all axes', async () => {
      const promise = lockstep.setTolerance(0.01, Length.mm);
      await eqAndPushX(0, 'lockstep 1 set tolerance 81', 'OK IDLE -- 0');
      await promise;
    });

    it('sets tolerance to specific axes', async () => {
      const promise1 = lockstep.setTolerance(0.01, Length.mm, { axisIndex: 1 });
      await eqAndPushX(0, 'lockstep 1 set b tolerance 81', 'OK IDLE -- 0');
      await promise1;

      const promise2 = lockstep.setTolerance(0.01, Angle.DEGREES, { axisIndex: 2 });
      await eqAndPushX(0, 'lockstep 1 set c tolerance 64', 'OK IDLE -- 0');
      await promise2;
    });
  });

  describe('toString', () => {
    it('gets lockstep string', async () => {
      expect(lockstep.toString()).to.startWith('Lockstep 1 -> Device 1 (Unknown) -> Connection');
    });
    it('returns representation for closed connection', async () => {
      await connection.close();
      expect(lockstep.toString()).to.eq('Lockstep 1 -> Device 1 -> Connection Closed');
    });
  });
});
