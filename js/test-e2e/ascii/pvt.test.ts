import { expect } from 'chai';
import {
  Length, DeviceDbSourceType,
  Velocity, Library, Time, PvtExecutionException, PvtSetupFailedException, PvtModeException, PvtMovementFailedException,
  PvtMovementInterruptedException,
  PvtDiscontinuityException,
  InvalidArgumentException,
  InvalidOperationException,
  BadDataException,
} from '../../src';
import { Connection, Device, PvtSequence, PvtMode, StreamAxisType, DigitalOutputAction } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { MASTER_DEVICE_DB } from '../constants';

import { Command } from './protocol';
import { DeviceXMCC4 } from './devices/xmcc4';

describe('PVT', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let eqAndPushX: (popStr: string | RegExp, pushStr: string) => Promise<Command>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);

    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, MASTER_DEVICE_DB);
  });

  afterAll(async () => {
    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE);
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe ('tests for listing buffers', () => {
    it('lists PVT buffers empty', async () => {
      const promise = device.pvt.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 pvt buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.be.empty;
    });

    it('lists PVT buffer single', async () => {
      const promise = device.pvt.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 pvt buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 buffer 42');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.have.length(1);
      expect(result).to.contain(42);
    });

    it('lists PVT buffer multiple', async () => {
      const promise = device.pvt.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 pvt buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 buffer 42');
      await server.push('#01 0 00 buffer 1');
      await server.push('#01 0 00 buffer 21');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.have.length(3);
      expect(result).to.contain(42);
      expect(result).to.contain(1);
      expect(result).to.contain(21);
    });
  });

  describe('tests for setup streams', () => {
    let liveStream: PvtSequence;
    let storeStream: PvtSequence;
    let disabledStream: PvtSequence;

    const A = { value: 1090.19, unit: Length.mm };
    const B = { value: 10.18, unit: Length.mm };
    const AV = { value: 123, unit: Velocity['mm/s'] };
    const BV = { value: 45.67, unit: Velocity['mm/s'] };
    const T1 = { value: 234, unit: Time.MICROSECONDS };
    const T2 = { value: 5678, unit: Time.MICROSECONDS };

    async function setupStreams() {
      liveStream = device.pvt.getSequence(1);
      const setupLivePromise = liveStream.setupLive(1, 2);
      await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('pvt 1 setup live 1 2', 'OK IDLE -- 0');
      if (device.isIdentified) {
        await eqAndPushX('get pos', `OK IDLE -- ${[100, 200, 300, 400].slice(0, device.axisCount).join(' ')}`);
      }
      await setupLivePromise;

      storeStream = device.pvt.getSequence(2);
      const firstBuffer = device.pvt.getBuffer(1);
      const setupStorePromise = storeStream.setupStore(firstBuffer, 1, 2);
      await eqAndPushX('pvt 2 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('pvt 2 setup store 1 2', 'OK IDLE -- 0');
      await setupStorePromise;

      disabledStream = device.pvt.getSequence(3);
      const setupDisabledPromise = disabledStream.disable();
      await eqAndPushX('pvt 3 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('pvt 3 setup disable', 'OK IDLE -- 0');
      await setupDisabledPromise;
    }

    describe('stream setup', () => {
      it('sets up streams', async () => {
        await setupStreams();
      });

      it('throws correct error on failing setup', async () => {
        const promise = device.pvt.getSequence(1).setupLive(4, 1);
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('pvt 1 setup live 4 1', 'RJ IDLE -- BADDATA');
        await expect(promise).to.be.rejectedWith(PvtSetupFailedException);
        const error: PvtSetupFailedException = await promise.catch(e => e);
        expect(error.message).to.startWith('PVT Sequence setup failed.');
      });

      it('rejects if the stream is already setup', async () => {
        const stream = device.pvt.getSequence(1);
        const buffer = device.pvt.getBuffer(1);
        const setupStorePromise = stream.setupStore(buffer, 1, 3);
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('pvt 1 setup store 1 2', 'OK IDLE -- 0');
        await setupStorePromise;

        const setupLivePromise = stream.setupLive(1, 2);
        await expect(setupLivePromise).to.be.rejectedWith(PvtModeException);
        const error: PvtModeException = await setupLivePromise.catch(e => e);
        expect(error.message).to.eq('The PVT sequence is already setup. Disable the PVT sequence first.');
      });

      it('throws error if point is added when sequence is disabled', async () => {
        const stream = device.pvt.getSequence(1);
        const disablePromise = stream.disable();
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await disablePromise;

        const setupLivePromise = stream.point([], [], T1);
        await expect(setupLivePromise).to.be.rejectedWith(PvtModeException);
        const error: PvtModeException = await setupLivePromise.catch(e => e);
        expect(error.message).to.eq([
          'The PVT sequence is disabled.',
          'Set up the PVT sequence in one of the permissable mode(s) for this action, those being: [Live Store].',
        ].join(' '));
      });

      it('throws error if call is invoked when sequence is disabled', async () => {
        const stream = device.pvt.getSequence(1);
        const disablePromise = stream.disable();
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
        await disablePromise;

        const buffer = device.pvt.getBuffer(1);
        const callPromise = stream.call(buffer);
        await expect(callPromise).to.be.rejectedWith(PvtModeException);
        const error : InvalidOperationException = await callPromise.catch(e => e);
        expect(error.message).to.eq([
          'The PVT sequence is disabled.',
          'Set up the PVT sequence in one of the permissable mode(s) for this action, those being: [Live Store StoreArbitrary].',
        ].join(' '));
      });
    });

    describe('non-identified', () => {
      beforeEach(async () => {
        await setupStreams();
      });

      describe('point', () => {
        it('sends proper request', async () => {
          const promise = liveStream.point([
            { value: 123 },
            { value: 456 },
          ], [
            { value: 321 },
            { value: 654 },
          ], { value: 789 });
          await eqAndPushX('pvt 1 point abs p 123 456 v 321 654 t 789', 'OK BUSY -- 0');
          await promise;
        });

        it('throws error when velocity is missing', async () => {
          const promise = liveStream.point([A, B], [], T1);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const error: InvalidArgumentException = await promise.catch(e => e);
          expect(error.message).to.eq('Stream was setup on non-identified device. Velocity calculation is not available.');
        });
      });
    });

    describe('X-MCC4, two linear, same scale', () => {
      beforeEach(async () => {
        await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_38);
        await setupStreams();
      });

      describe('movement', () => {
        describe('point', () => {
          it('sends proper request', async () => {
            const promise = liveStream.point([A, B], [AV, BV], T1);
            await eqAndPushX('pvt 1 point abs p 8790193 82081 v 1624880 603319 t 0.2', 'OK BUSY -- 0');
            await promise;

            const promise2 = liveStream.pointRelative([B, A], [AV, BV], T2);
            await eqAndPushX('pvt 1 point rel p 82081 8790193 v 1624880 603319 t 5.7', 'OK BUSY -- 0');
            await promise2;
          });

          it('throws error when number of positions does not match axis', async () => {
            const promise = liveStream.point([A], [AV, BV], T1);
            await expect(promise).to.be.rejectedWith(InvalidArgumentException);
            const error: InvalidArgumentException = await promise.catch(e => e);
            expect(error.message).to.eq('The number of positions does not equal to number of axes.');
          });

          it('throws error when number of velocities does not match axis', async () => {
            const promise = liveStream.point([A, B], [AV], T1);
            await expect(promise).to.be.rejectedWith(InvalidArgumentException);
            const error: InvalidArgumentException = await promise.catch(e => e);
            expect(error.message).to.eq('The number of velocities does not equal to number of axes.');
          });

          it('throws command failed even if the sequence fails during the handling', async () => {
            const promise = liveStream.point([A, B], [AV, BV], T1);
            await eqAndPushX(/pvt 1 point abs/, 'RJ BUSY -- BADDATA');
            await eqAndPushX('pvt 1 info', 'OK IDLE FB live 2 10 endvel');
            await eqAndPushX('', 'OK IDLE -- 0');
            await expect(promise).to.be.rejectedWith(BadDataException);
            const error: BadDataException = await promise.catch(e => e);
            expect(error.message).to.match(/rejected: BADDATA/);
          });
        });
      });

      describe('PT calculation', () => {
        const A = { value: 100, unit: Length.mm };
        const B = { value: 20, unit: Length.mm };
        const T1 = { value: 4123, unit: Time.ms };
        const T2 = { value: 1234, unit: Time.ms };

        it('calculates velocity', async () => {
          let promise = liveStream.point([A, B], [{ value: 0 }, { value: 0 }], { value: 0 });
          await eqAndPushX('pvt 1 point abs p 806299 161260 v 0 0 t 0.0', 'OK BUSY -- 0');
          await promise;

          promise = liveStream.point([B, A], [], { value: 5123, unit: Time.ms });
          await promise;

          promise = liveStream.point([A, B], [{ value: 0 }, { value: 0 }], T1);
          await eqAndPushX('pvt 1 point abs p 161260 806299 v 25017 -25017 t 5123.0', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 point abs p 806299 161260 v 0 0 t 4123.0', 'OK BUSY -- 0');
          await promise;
        });

        it('calculates velocity for relative points', async () => {
          await liveStream.pointRelative([A, B], [], T2);

          const promise = liveStream.pointRelative([B, A], [{ value: 0 }, { value: 0 }], T1);
          await eqAndPushX('pvt 1 point rel p 806299 161260 v 567308 267257 t 1234.0', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 point rel p 161260 806299 v 0 0 t 4123.0', 'OK BUSY -- 0');
          await promise;
        });

        it('calculates velocity for one axis only', async () => {
          await liveStream.pointRelative([A, B], [{ value: 1234 }, null], T2);

          const promise = liveStream.pointRelative([B, A], [{ value: 0 }, { value: 0 }], T1);
          await eqAndPushX('pvt 1 point rel p 806299 161260 v 1234 267257 t 1234.0', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 point rel p 161260 806299 v 0 0 t 4123.0', 'OK BUSY -- 0');
          await promise;
        });

        it('throws error when first absolute point does not have velocity (store only)', async () => {
          const promise = storeStream.point([A, B], [], T1);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const error: InvalidArgumentException = await promise.catch(e => e);
          expect(error.message).to.startWith('The first submitted absolute point must have defined velocity.');
        });

        it('allows to submit abs point right from start (live only)', async () => {
          await liveStream.point([{ value: 300 }, { value: 300 }], [], { value: 4000 });

          const promise = liveStream.pointRelative(
            [{ value: 100 }, { value: 100 }],
            [{ value: 0 }, { value: 0 }],
            { value: 1000 });
          await eqAndPushX('pvt 1 point abs p 300 300 v 123 102 t 4000.0', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 point rel p 100 100 v 0 0 t 1000.0', 'OK BUSY -- 0');
          await promise;
        });

        it('throws error when combining relative and absolute points', async () => {
          await liveStream.pointRelative([A, B], [], T1);
          const promise = liveStream.point([A, B], [], T1);
          await expect(promise).to.be.rejectedWith(InvalidOperationException);
          const error: InvalidOperationException = await promise.catch(e => e);
          expect(error.message).to.startWith([
            'Cannot calculate velocity of the previous relative point because the current point is absolute.',
            'The error was partially or completely caused by previously', //...
          ].join(' '));
        });

        it('throws error when combining relative and absolute points 2', async () => {
          let promise = liveStream.pointRelative([A, B], [AV, BV], T1);
          await eqAndPushX(/^pvt 1 point/, 'OK BUSY -- 0');
          await promise;

          promise = liveStream.point([A, B], [], T1);
          await expect(promise).to.be.rejectedWith(InvalidOperationException);
          const error: InvalidOperationException = await promise.catch(e => e);
          expect(error.message).to.eq(
            'Cannot calculate velocity of an absolute point because the previous point is relative.');
        });

        it('throws error when time is 0', async () => {
          await liveStream.pointRelative([A, B], [], { value: 0 });
          const promise = liveStream.pointRelative([B, A], [], T1);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const error: InvalidArgumentException = await promise.catch(e => e);
          expect(error.message).to.startWith([
            'Cannot calculate velocity for points with zero time.',
            'The error was partially or completely caused by previously', //...
          ].join(' '));
        });

        it('interleaves io commands with the calculation', async () => {
          await liveStream.pointRelative([A, B], [], T2);

          await liveStream.io.setAnalogOutput(1, 10);
          await liveStream.io.setDigitalOutput(2, DigitalOutputAction.ON);

          let promise = liveStream.pointRelative([B, A], [], T2);
          await eqAndPushX(/^pvt 1 point rel/, 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 io set ao 1 10.000', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 io set do 2 1', 'OK BUSY -- 0');
          await promise;

          await liveStream.io.setAnalogOutput(1, 5);
          await liveStream.io.setDigitalOutput(2, DigitalOutputAction.OFF);

          promise = liveStream.pointRelative([B, A], [{ value: 0 }, { value: 0 }], T1);
          await eqAndPushX(/^pvt 1 point rel/, 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 io set ao 1 5.000', 'OK BUSY -- 0');
          await eqAndPushX('pvt 1 io set do 2 0', 'OK BUSY -- 0');
          await eqAndPushX(/^pvt 1 point rel/, 'OK BUSY -- 0');
          await promise;
        });

        describe('call', () => {
          it('throws error when points are queued for calculation', async () => {
            await liveStream.pointRelative([A, B], [], T2);

            const promise = liveStream.call(device.pvt.getBuffer(1));
            await expect(promise).to.be.rejectedWith(InvalidOperationException);
            const error: InvalidOperationException = await promise.catch(e => e);
            expect(error.message).to.startWith(
              'Cannot perform the action because the PVT sequence has queued points for calculation.');
          });

          it('resets the buffer preventing using the points before the buffer for calculation', async () => {
            let promise = liveStream.point([A, B], [AV, BV], T2);
            await eqAndPushX(/^pvt 1 point abs/, 'OK BUSY -- 0');
            await promise;

            promise = liveStream.call(device.pvt.getBuffer(1));
            await eqAndPushX('pvt 1 call 1', 'OK BUSY -- 0');
            await promise;

            promise = liveStream.point([A, B], [], T1);
            await expect(promise).to.be.rejectedWith(InvalidArgumentException);
            const error: InvalidArgumentException = await promise.catch(e => e);
            expect(error.message).to.startWith('The first submitted absolute point must have defined velocity.');
          });
        });

        describe('genericCommand', () => {
          it('throws error when points are queued for calculation', async () => {
            await liveStream.pointRelative([A, B], [], T2);

            let promise = liveStream.genericCommand('command');
            await expect(promise).to.be.rejectedWith(InvalidOperationException);

            promise = liveStream.genericCommandBatch(['command']);
            await expect(promise).to.be.rejectedWith(InvalidOperationException);
          });
        });

        describe('cork', () => {
          it('bypasses queued points', async () => {
            await liveStream.pointRelative([A, B], [], T2);

            let promise = liveStream.cork();
            await eqAndPushX('pvt 1 fifo cork', 'OK IDLE -- 0');
            await promise;

            promise = liveStream.uncork();
            await eqAndPushX('pvt 1 fifo uncork', 'OK IDLE -- 0');
            await promise;
          });
        });

        it('discards points if buffered point is rejected', async () => {
          await liveStream.pointRelative([A, B], [], T1);
          let promise = liveStream.pointRelative([B, A], [], T2);
          await eqAndPushX(/^pvt 1 point rel/, 'RJ IDLE -- BADDATA');
          await eqAndPushX('pvt 1 info', 'OK IDLE -- live 2 - -');
          await eqAndPushX('', 'OK IDLE -- 0');
          await expect(promise).to.be.rejectedWith(BadDataException);
          const error: BadDataException = await promise.catch(e => e);
          expect(error.message).to.eq([
            'Command "pvt 1 point rel p 806299 161260 v 267257 567308 t 4123.0" rejected: BADDATA:',
            'The command\'s data was incorrect or out of range.',
            'The error was partially or completely caused by previously submitted PVT action.',
            'The action itself and 1 actions submitted afterward were discarded.',
          ].join(' '));

          promise = liveStream.point([A, B], [{ value: 0 }, { value: 0 }], T1);
          await eqAndPushX('pvt 1 point abs p 806299 161260 v 0 0 t 4123.0', 'OK BUSY -- 0');
          await promise;
        });

        it('discards actions when action is rejected', async () => {
          await liveStream.pointRelative([A, B], [], T2);

          await liveStream.io.setAnalogOutput(1, 10);
          await liveStream.io.setDigitalOutput(2, DigitalOutputAction.ON);
          await liveStream.io.setDigitalOutput(3, DigitalOutputAction.OFF);

          const promise = liveStream.pointRelative([B, A], [], T2);

          await eqAndPushX(/^pvt 1 point rel/, 'OK IDLE -- 0');
          await eqAndPushX(/^pvt 1 io/, 'RJ IDLE -- BADDATA');
          await eqAndPushX('pvt 1 info', 'OK IDLE -- live 2 - -');
          await eqAndPushX('', 'OK IDLE -- 0');

          await expect(promise).to.be.rejectedWith(BadDataException);
          const error: BadDataException = await promise.catch(e => e);
          expect(error.message).to.startWith([
            'Command "pvt 1 io set ao 1 10.000" rejected: BADDATA:',
            'The command\'s data was incorrect or out of range.',
            'The error was partially or completely caused by previously submitted PVT action.',
            'The action itself and 3 actions submitted afterward were discarded.',
          ].join(' '));
        });

        describe('waitUntilIdle', () => {
          it('throws error when there are point pending the calculation', async () => {
            await liveStream.point([A, B], [], T2);

            const promise = liveStream.waitUntilIdle();
            await expect(promise).to.be.rejectedWith(InvalidOperationException);
            const error: InvalidOperationException = await promise.catch(e => e);
            expect(error.message).to.eq([
              'The PVT sequence contains points that were not yet sent to the device.',
              'Make sure that the last submitted point has defined velocity.',
            ].join(' '));
          });
        });
      });

      describe('FB flag', () => {
        it('handles specific error (e.g. decellimit)', async () => {
          const streamActionPromise = liveStream.genericCommand('cmd');
          await eqAndPushX('pvt 1 cmd', 'RJ IDLE FB BADDATA');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FB 01 FB');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FB 01 FB');
          const info = await eqAndPushX('pvt 1 info', 'OK BUSY FB live 3 10 decellimit');
          await server.push(`#1 0 ${info.id} point abs p 0 v -1000 t 1000 i 10`);
          await eqAndPushX('', 'OK BUSY FB 0');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FB 01 FB');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FB 01 FB');
          await eqAndPushX('pvt 1 info', 'OK IDLE -- live 2 - -');
          await eqAndPushX('', 'OK BUSY -- 0');

          await expect(streamActionPromise).to.be.rejectedWith(PvtExecutionException);
          const error: PvtExecutionException = await streamActionPromise.catch(e => e);
          expect(error.message).to.eq([
            'The device has indicated that a previously executed PVT action has failed.',
            'The reason for the failure is:',
            'A PVT segment tried to move an axis to a position at a velocity that would',
            'eventually cause the axis to hit a limit, even if decelerating at max deceleration.',
          ].join(' '));
          expect(error.details).to.deep.equal({
            errorFlag: 'decellimit',
            reason: [
              'A PVT segment tried to move an axis to a position at a velocity that would',
              'eventually cause the axis to hit a limit, even if decelerating at max deceleration.',
            ].join(' '),
            invalidPoints: [{
              index: 10,
              point: 'point abs p 0 v -1000 t 1000 i 10',
            }],
          });
        });
      });

      describe('movement errors', () => {
        it('throws correct error', async () => {
          const promise = liveStream.genericCommand('cmd');
          await eqAndPushX('pvt 1 cmd', 'OK IDLE FS 0');

          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FS 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FS 02 FS ND');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FS 01 ND');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FS 02 FS ND');

          await expect(promise).to.be.rejectedWith(PvtMovementFailedException);
          const error: PvtMovementFailedException = await promise.catch(e => e);
          expect(error.message).to.eq('The PVT movement may have failed because\
 a fault was observed: Stalled and Stopped (FS).');
          expect(error.details.reason).to.eq('Stalled and Stopped (FS)');
          expect(error.details.warnings).to.include.members(['FS', 'ND']);
        });

        it('throws exception when movement interrupted', async () => {
          const promise = liveStream.waitUntilIdle();
          await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
          await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
          await eqAndPush(server, 1, 1, '', 'OK IDLE NC 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE NC 02 NC NI');
          await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE NC 02 NC NI');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK IDLE NC 02 NC NI');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK IDLE NC 02 NC NI');
          await expect(promise).to.be.rejectedWith(PvtMovementInterruptedException);
          const error: PvtMovementInterruptedException = await promise.catch(e => e);
          expect(error.message).to.eq(
            'The PVT movement was interrupted due to a fault: Manual Control (NC), Movement Interrupted (NI).');
          expect(error.details.reason).to.eq('Manual Control (NC), Movement Interrupted (NI)');
          expect(error.details.warnings).to.include.members(['NC', 'NI']);
        });
      });

      describe('disable', () => {
        it('disables a stream', async () => {
          const promise = liveStream.disable();
          await eqAndPushX('pvt 1 setup disable', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('cork', () => {
        it('sends proper request', async () => {
          const promise = liveStream.cork();
          await eqAndPushX('pvt 1 fifo cork', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('uncork', () => {
        it('sends proper request', async () => {
          const promise = liveStream.uncork();
          await eqAndPushX('pvt 1 fifo uncork', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('toString', () => {
        it('gets stream string - Live', async () => {
          expect(liveStream.toString()).to.eq('PVT Sequence 1 (Live) -> Axes [1, 2] -> Device 1');
        });
        it('gets stream string - StoreTarget', async () => {
          expect(storeStream.toString()).to.eq('PVT Sequence 2 (Store) -> Buffer 1 (Axes [1, 2]) -> Device 1');
        });
        it('gets stream string - Disabled', async () => {
          expect(disabledStream.toString()).to.eq('PVT Sequence 3 (Disabled) -> Device 1');
        });
        it('gets stream string - Unknown', async () => {
          expect(device.pvt.getSequence(6).toString()).to.eq('PVT Sequence 6 (Unknown) -> Device 1');
        });
        it('returns representation for closed connection', async () => {
          await connection.close();
          expect(liveStream.toString()).to.eq('Stream 1 (Unknown) -> Device 1 -> Connection Closed');
        });
      });

      describe('call', () => {
        it('sends proper request', async () => {
          const buffer = device.pvt.getBuffer(1);
          const promise = liveStream.call(buffer);
          await eqAndPushX('pvt 1 call 1', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('io', () => {
        /* ZML-832 - Remove the following tests at a major release */
        describe('deprecated functions', () => {
          describe('setDigitalOutput', () => {
            it('sends proper request', async () => {
              const promise = liveStream.io.setDigitalOutput(2, DigitalOutputAction.OFF);
              await eqAndPushX('pvt 1 io set do 2 0', 'OK IDLE -- 0');
              await promise;
            });
          });

          describe('setAllDigitalOutputs', () => {
            it('sends proper request', async () => {
              const promise = liveStream.io.setAllDigitalOutputs([
                DigitalOutputAction.ON,
                DigitalOutputAction.OFF,
                DigitalOutputAction.TOGGLE,
                DigitalOutputAction.KEEP
              ]);
              await eqAndPushX('pvt 1 io set do port 1 0 t k', 'OK IDLE -- 0');
              await promise;
            });
          });

          describe('setAnalogOutput', () => {
            it('sends proper request', async () => {
              const promise = liveStream.setAnalogOutput(2, 1);
              await eqAndPushX('pvt 1 io set ao 2 1.000', 'OK IDLE -- 0');
              await promise;
            });
          });

          describe('setAllAnalogOutputs', () => {
            it('sends proper request', async () => {
              const promise = liveStream.setAllAnalogOutputs([2, 3.3, 0, 0]);
              await eqAndPushX('pvt 1 io set ao port 2.000 3.300 0.000 0.000', 'OK IDLE -- 0');
              await promise;
            });
          });
        });

        describe('setDigitalOutput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setDigitalOutput(2, DigitalOutputAction.OFF);
            await eqAndPushX('pvt 1 io set do 2 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllDigitalOutputs', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAllDigitalOutputs([
              DigitalOutputAction.ON,
              DigitalOutputAction.OFF,
              DigitalOutputAction.TOGGLE,
              DigitalOutputAction.KEEP
            ]);
            await eqAndPushX('pvt 1 io set do port 1 0 t k', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setDigitalOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setDigitalOutputSchedule(2, DigitalOutputAction.OFF, DigitalOutputAction.ON, 100);
            await eqAndPushX('pvt 1 io set do 2 0 schedule 100.0 1', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllDigitalOutputsSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAllDigitalOutputsSchedule(
              [DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP],
              [DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP],
              100,
              Time.MICROSECONDS
            );
            await eqAndPushX('pvt 1 io set do port k k k k schedule 0.1 1 1 k k', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAnalogOutput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAnalogOutput(2, 1);
            await eqAndPushX('pvt 1 io set ao 2 1.000', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllAnalogOutputs', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAllAnalogOutputs([2, 3.3, 0, 0]);
            await eqAndPushX('pvt 1 io set ao port 2.000 3.300 0.000 0.000', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAnalogOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAnalogOutputSchedule(1, 0, 3.3, 12, Time.SECONDS);
            await eqAndPushX('pvt 1 io set ao 1 0 schedule 12000.0 3.3', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllAnalogOutputsSchedule', () => {
          it('sets the values of all analog output channels with a schedule', async () => {
            const promise = liveStream.io.setAllAnalogOutputsSchedule([0, 3.3, 12, 0], [5, 0, 0, 0], 200, Time.MICROSECONDS);
            await eqAndPushX('pvt 1 io set ao port 0 3.3 12 0 schedule 0.2 5 0 0 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('cancelDigitalOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.cancelDigitalOutputSchedule(4);
            await eqAndPushX('pvt 1 io cancel schedule do 4', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('cancelAllDigitalOutputsSchedule', () => {
          it('Cancels all', async () => {
            const promise = liveStream.io.cancelAllDigitalOutputsSchedule();
            await eqAndPushX('pvt 1 io cancel schedule do port', 'OK IDLE -- 0');
            await promise;
          });

          it('Cancels some', async () => {
            const promise = liveStream.io.cancelAllDigitalOutputsSchedule([false, true, true, false]);
            await eqAndPushX('pvt 1 io cancel schedule do port 0 1 1 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('cancelAnalogOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.cancelAnalogOutputSchedule(1);
            await eqAndPushX('pvt 1 io cancel schedule ao 1', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('cancelAllAnalogOutputsSchedule', () => {
          it('Cancels all', async () => {
            const promise = liveStream.io.cancelAllAnalogOutputsSchedule();
            await eqAndPushX('pvt 1 io cancel schedule ao port', 'OK IDLE -- 0');
            await promise;
          });

          it('Cancels some', async () => {
            const promise = liveStream.io.cancelAllAnalogOutputsSchedule([false, true, false, true]);
            await eqAndPushX('pvt 1 io cancel schedule ao port 0 1 0 1', 'OK IDLE -- 0');
            await promise;
          });
        });
      });

      describe('settings', () => {
        describe('get', () => {
          describe('axes property', () => {
            it('returns a streams cached axes', async () => {
              expect(liveStream.axes).to.deep.eq([
                { axisNumber: 1, axisType: StreamAxisType.PHYSICAL },
                { axisNumber: 2, axisType: StreamAxisType.PHYSICAL },
              ]);
            });
          });

          describe('mode property', () => {
            it('returns stream mode', async () => {
              expect(liveStream.mode).to.eq(PvtMode.LIVE);
              expect(storeStream.mode).to.eq(PvtMode.STORE);
              expect(disabledStream.mode).to.eq(PvtMode.DISABLED);
            });
            it('new stream returns disabled', async () => {
              expect(device.streams.getStream(10).mode).to.eq(PvtMode.DISABLED);
            });
          });
        });

        describe('genericCommand', () => {
          it('sends proper request', async () => {
            const promise = liveStream.genericCommand('some command');
            await eqAndPushX('pvt 1 some command', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('genericCommandBatch', () => {
          it('sends proper request', async () => {
            const promise = liveStream.genericCommandBatch(['command1', 'command2', 'command3']);
            await eqAndPushX('pvt 1 command1', 'OK IDLE -- 0');
            await eqAndPushX('pvt 1 command2', 'OK IDLE -- 0');
            await eqAndPushX('pvt 1 command3', 'RJ IDLE -- AGAIN');
            await eqAndPushX('pvt 1 command3', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('checkDisabled', () => {
          it('returns true and disables the stream if the stream is disabled', async () => {
            const promise = liveStream.checkDisabled();
            await eqAndPushX('pvt 1 info', 'OK IDLE -- disabled - - -');
            await eqAndPush(server, 1, 0, '', 'OK BUSY -- 0');
            await expect(promise).eventually.eq(true);
            expect(liveStream.mode).to.eq(PvtMode.DISABLED);
          });
        });
      });

      describe('treatDiscontinuitiesAsError', () => {
        beforeEach(() => {
          liveStream.treatDiscontinuitiesAsError();
        });

        it('causes ND flag to throw an error', async () => {
          const promise = liveStream.genericCommand('command');
          await eqAndPushX('pvt 1 command', 'OK BUSY ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY ND 01 ND');
          await expect(promise).to.be.rejectedWith(PvtDiscontinuityException);
        });
      });
    });
  });
});
