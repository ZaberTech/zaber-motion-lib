import { expect } from 'chai';
import { createServer, Server, Socket } from 'net';
import { lastValueFrom } from 'rxjs';

import split2 from 'split2';

import JsonRpc from 'simple-jsonrpc-js';
import { Connection } from '../../src/ascii';
import Bluebird from 'bluebird';
import _ from 'lodash';
import { ConnectionFailedException, RequestTimeoutException, Tools } from '../../src';

interface RpcData {
  jsonrpc: '2.0';
  method: string;
  params: string[];
}

const SERIAL_PORT = 'usb-1';
const BROKEN_SERIAL_PORT = 'usb-fail';
const TCP_PORT = 11321;

const TRIGGER_FAIL_COMMAND = 'fail';
const TRIGGERED_FAIL_RESPONSE = 'test disconnected';

class MockMessageRouter {
  private server: Server;
  private rpc: any;
  private _rpcCalls: RpcData[] = [];
  private socket: Socket | null = null;
  public disconnected: Promise<void>;
  public nextMessageID = 0;

  public get rpcCalls() {
    return this._rpcCalls.map(comm => _.pick(comm, ['method', 'params']));
  }

  constructor() {
    this.rpc = new JsonRpc();
    this.rpc.on('status_getApiVersion', [], () => '1.2');
    this.rpc.on('client_setVersion', [], () => null);
    this.rpc.on('routing_warmup', ['connectionID'], this.handleWarmup.bind(this));
    this.rpc.on('routing_cooldown', () => null);
    this.rpc.on('routing_sendMessage', ['connectionID', 'message', 'id'], this.handleMessage.bind(this));
    this.rpc.on('routing_listConnections', [], () => [{ id: SERIAL_PORT }]);
    this.rpc.toStream = (message: string) => {
      if (!this.socket) {
        throw new Error('Cannot write response to a null socket');
      }
      this.socket.write(message);
    };

    this.server = createServer();
    this.disconnected = new Promise(resolve => {
      this.server.on('connection', newSocket => {
        this.socket = newSocket;
        this.socket.pipe(split2()).on('data', this.handleLineIn.bind(this));
        this.socket.on('close', () => {
          resolve();
          this.socket!.end();
        });
      });
    });
  }

  public async open(type: 'pipe' | 'tcp'): Promise<void> {
    await new Promise<void>(resolve => {
      if (type === 'pipe') {
        this.server.listen(Tools.getMessageRouterPipePath(), resolve);
      } else {
        this.server.listen(TCP_PORT, resolve);
      }
    });
  }

  public async close(): Promise<void> {
    await Bluebird.fromCallback<void>(cb => this.server.close(cb));
  }

  public async endSocket() {
    this.socket?.end();
  }

  private handleLineIn(line: string): void {
    if (!line) { return }
    const parsedData = JSON.parse(line) as RpcData;
    this._rpcCalls.push(parsedData);
    this.rpc.messageHandler(line);
  }

  private handleWarmup(connectionID: string): void {
    if (connectionID === BROKEN_SERIAL_PORT) {
      throw new Error('Could not warmup serial port');
    }
  }

  public replyCallback?: (message: string, id: string) => { reply: string; id?: number }[];

  private handleMessage(_connectionId: string, message: string): void {
    const regExp = /\d+\s\d+\s(?<id>\d+)\s?(?<command>\w*):\w*/;
    const match = regExp.exec(message);
    const id = match?.groups?.id;
    if (id == null) {
      throw new Error(`Could not find id in ${message}`);
    }
    const command = match?.groups?.command;
    if (command === TRIGGER_FAIL_COMMAND) {
      this.rpc.notification('routing_error', [SERIAL_PORT, TRIGGERED_FAIL_RESPONSE]);
    } else if (this.replyCallback != null) {
      const replies = this.replyCallback(message, id);
      for (const reply of replies) {
        this.rpc.notification('routing_reply', [SERIAL_PORT, [reply.reply], reply.id ?? this.nextMessageID++]);
      }
    } else {
      this.rpc.notification('routing_reply', [SERIAL_PORT, [`@01 0 ${id} OK IDLE -- 0`], this.nextMessageID++]);
    }
  }
}

const VERSION_EXCHANGE = [
  { method: 'status_getApiVersion' },
  { method: 'client_setVersion', params: ['2.0'] },
];

describe('Routed connection', () => {
  let router: MockMessageRouter;
  let connection: Connection;

  beforeEach(async () => {
    router = new MockMessageRouter();
    await router.open('pipe');
  });

  afterEach(async () => {
    if (connection) {
      await connection.close();
      connection = null!;
    }

    await router.close();
    router = null!;
  });

  it('connects to message router and disconnects on close', async () => {
    connection = await Connection.openSerialPort(SERIAL_PORT);
    await connection.close();

    await router.disconnected;
    expect(router.rpcCalls).to.deep.equal([
      ...VERSION_EXCHANGE,
      { method: 'routing_warmup', params: [SERIAL_PORT] },
      { method: 'routing_cooldown', params: [SERIAL_PORT] },
    ]);
  });

  it('reads and writes lines', async () => {
    connection = await Connection.openSerialPort(SERIAL_PORT);
    await connection.genericCommand('home', { device: 1 });
    await connection.genericCommand('stop', { device: 1 });
    await connection.close();

    expect(router.rpcCalls).to.deep.equal([
      ...VERSION_EXCHANGE,
      { method: 'routing_warmup', params: [SERIAL_PORT] },
      { method: 'routing_sendMessage', params: [SERIAL_PORT, '/1 0 00 home:36', 0] },
      { method: 'routing_sendMessage', params: [SERIAL_PORT, '/1 0 01 stop:18', 1] },
      { method: 'routing_cooldown', params: [SERIAL_PORT] },
    ]);
  });

  it('deals with out of order delivery and duplicate messages', async () => {
    router.replyCallback = (request, id) => [
      { reply: `#01 0 ${id} data2`, id: 2 },
      { reply: `#01 0 ${id} data1`, id: 1 },
      { reply: `#01 0 ${id} data2`, id: 2 },
      { reply: `@01 0 ${id} OK IDLE -- 0`, id: 0 },
      { reply: `@01 0 ${id} OK BUSY -- 0`, id: 3 },
    ];
    connection = await Connection.openSerialPort(SERIAL_PORT);
    const replies = await connection.getDevice(1).genericCommandMultiResponse('help');
    expect(replies.map(reply => `${reply.data} ${reply.status}`)).to.deep.equal([
      '0 IDLE',
      'data1 ',
      'data2 ',
    ]);
  });

  it('fails during warmup', async () => {
    try {
      connection = await Connection.openSerialPort(BROKEN_SERIAL_PORT);
      throw new Error('The mock router should have failed while warming up');
    } catch (e) {
      expect(e).to.be.instanceOf(ConnectionFailedException);
      expect((e as ConnectionFailedException).message).to.startWith('Cannot open serial port');
      expect(router.rpcCalls).to.deep.equal([
        ...VERSION_EXCHANGE,
        { method: 'routing_warmup', params: [BROKEN_SERIAL_PORT] },
      ]);
    }
  });

  it('respects option to not connect through router', async () => {
    try {
      connection = await Connection.openSerialPort(SERIAL_PORT, { direct: true });
      throw new Error('Should have avoided connecting to message router');
    } catch (e) {
      expect(e).to.be.instanceOf(ConnectionFailedException);
      expect((e as ConnectionFailedException).message).to.startWith('Cannot open serial port');
      expect(router.rpcCalls).to.deep.equal([]);
    }
  });

  it('disconnects connection when error is sent back from message router', async () => {
    connection = await Connection.openSerialPort(SERIAL_PORT);
    try {
      await connection.genericCommand(TRIGGER_FAIL_COMMAND, { checkErrors: false, timeout: 10 });
      throw new Error('Should have failed');
    } catch (err) {
      expect(err).to.be.instanceOf(RequestTimeoutException);
    }
    const disconnectReason = await lastValueFrom(connection.disconnected);
    expect(disconnectReason).to.be.instanceOf(ConnectionFailedException);
    expect(disconnectReason?.message).to.equal(TRIGGERED_FAIL_RESPONSE);
  });
});

describe('Routed connection - TCP', () => {
  let router: MockMessageRouter;
  let connection: Connection;

  beforeEach(async () => {
    router = new MockMessageRouter();
    await router.open('tcp');
  });

  afterEach(async () => {
    if (connection) {
      await connection.close();
      connection = null!;
    }

    await router.close();
    router = null!;
  });

  it('reads and writes lines', async () => {
    connection = await Connection.openNetworkShare('localhost', TCP_PORT, SERIAL_PORT);
    await connection.genericCommand('home', { device: 1 });
    await connection.genericCommand('stop', { device: 1 });
    await connection.close();

    expect(router.rpcCalls).to.deep.equal([
      ...VERSION_EXCHANGE,
      { method: 'routing_listConnections' },
      { method: 'routing_warmup', params: [SERIAL_PORT] },
      { method: 'routing_sendMessage', params: [SERIAL_PORT, '/1 0 00 home:36', 0] },
      { method: 'routing_sendMessage', params: [SERIAL_PORT, '/1 0 01 stop:18', 1] },
      { method: 'routing_cooldown', params: [SERIAL_PORT] },
    ]);
  });

  it('throws error on incorrect connection name', async () => {
    const promise = Connection.openNetworkShare('localhost', TCP_PORT, BROKEN_SERIAL_PORT);
    await expect(promise).to.be.rejectedWith(ConnectionFailedException);
    const error: ConnectionFailedException = await promise.catch(e => e);
    expect(error.message).to.eq(`Connection usb-fail not available. Choose one of [${SERIAL_PORT}]`);
  });

  it('throws error when connection breaks', async () => {
    connection = await Connection.openNetworkShare('localhost', TCP_PORT, SERIAL_PORT);

    const errPromise = lastValueFrom(connection.disconnected);
    await router.endSocket();

    const error: ConnectionFailedException = await errPromise;
    expect(error).to.be.an.instanceof(ConnectionFailedException);
    expect(error.message).to.eq('Connection closed by the remote host');
  });
});
