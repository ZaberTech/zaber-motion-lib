import mqtt from 'mqtt';
import got from 'got';
import { Observable, Subject } from 'rxjs';
import Bluebird from 'bluebird';
import { v4 as uuidv4 } from 'uuid';
import { filter } from 'rxjs/operators';

interface Message {
  topic: string;
  payload: string;
}

/** Establishes an MQTT connection to the staging environment to test against */
export class MqttTest {
  private client: mqtt.Client | null = null;
  private messages = new Subject<Message>();

  public async connect(): Promise<void> {
    const { body: { url } } = await got<{ url: string }>('https://api-staging.zaber.io/iot-token/websocket-url?token=unauthenticated', {
      responseType: 'json',
    });

    this.client = mqtt.connect(url, {
      clientId: `unauthenticated-client-${uuidv4()}`,
      clean: true,
      reconnectPeriod: -1,
    });

    this.client.on('message',
      (topic, payload) => {
        this.messages.next({ topic, payload: payload.toString('utf8') });
      }
    );

    await new Promise((resolve, reject) => {
      this.client!.once('connect', resolve);
      this.client!.once('error', () => {
        reject(new Error('Failed to connect to MQTT'));
      });
    });
  }

  public disconnect(): void {
    if (!this.client) {
      return;
    }
    this.client.end(true);
    this.client = null;

    this.messages.complete();
    this.messages = new Subject<Message>();
  }

  public async subscribe(topic: string): Promise<Observable<Message>> {
    const matchingRegexp = new RegExp(`^${topic}$`.replace('/', '\\/').replace('+', '(?<clientId>[0-9a-f\\-]+)').replace('#$', ''));
    await Bluebird.fromNode(cb => this.client!.subscribe(topic, { qos: 0 }, cb));
    return this.messages.pipe(filter(message => matchingRegexp.test(message.topic)));
  }

  public async publish(topic: string, message: string): Promise<void> {
    await Bluebird.fromNode(cb => this.client!.publish(topic, message, { qos: 0 }, cb));
  }
}
