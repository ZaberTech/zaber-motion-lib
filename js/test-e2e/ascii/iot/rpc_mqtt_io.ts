import { Observable, Subject } from 'rxjs';
import { MqttTest } from './mqtt_test';

export class RpcMqttIo {
  public linesSubject = new Subject<string>();
  public get lines(): Observable<string> {
    return this.linesSubject;
  }

  /** The sender id of the last messages to come in */
  private lastClientId: string | null = null;

  constructor(
    private readonly client: MqttTest,
    private readonly routerId: string,
    private readonly realm: string) {
  }

  public async init(): Promise<void> {
    const topic = `${this.realm}/message-router/${this.routerId}/+/rpc`;
    const messages = await this.client.subscribe(topic);
    messages.subscribe(message => {
      this.lastClientId = message.topic.split('/')[3];
      this.linesSubject.next(message.payload);
    });
  }

  public async pushLine(line: string): Promise<void> {
    if (!this.lastClientId) { throw new Error('Cannot reply to a message that has not come in yet') }
    const topic = `${this.realm}/message-router/${this.lastClientId}/${this.routerId}/rpc`;
    await this.client.publish(topic, line);
  }
}
