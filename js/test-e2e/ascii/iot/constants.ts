/* eslint-disable camelcase */
import { v4 as uuidv4 } from 'uuid';

export const TEST_ROUTER = uuidv4();
export const UNAUTHENTICATED_REALM = 'unauthenticated';
export const UNAUTHENTICATED_TOKEN = 'unauthenticated';
export const TEST_CONN_1 = 'COM1';
export const TEST_CONN_2 = 'COM2';

export interface ConnectionInfo {
	type: string;
	id: string;

	serial_port: string;
	baud_rate: number;
}

export function makeConnectionInfo(connectionNames: string[]): ConnectionInfo[] {
  return connectionNames.map(connectionName => ({
    type: 'serial',
    id: connectionName,

    serial_port: connectionName,
    baud_rate: 115200,
  }));
}
