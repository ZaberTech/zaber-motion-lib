import JsonRpc from 'simple-jsonrpc-js';
import { Observable } from 'rxjs';
import { expect } from 'chai';
import { ConnectionInfo } from './constants';

interface RcpClientIO {
  pushLine(line: string): Promise<void>;
  lines: Observable<string>;
}

export class RpcClient {
  private rpc: any;
  public failNextResponse = false;
  public nextMessageIDs: Record<string, number> = { };

  private warmConnections = new Set<string>();

  public availableConnections: ConnectionInfo[] = [];

  constructor(private readonly io: RcpClientIO) {
    this.rpc = new JsonRpc();
    this.rpc.toStream = (message: any) => this.io.pushLine(message);

    this.io.lines.subscribe(line => this.rpc.messageHandler(line));

    this.rpc.on('routing_sendMessage', ['connection', 'message', 'id'], this.onMessage.bind(this));
    this.rpc.on('routing_warmup', ['connection'], this.warmup.bind(this));
    this.rpc.on('routing_cooldown', ['connection'], this.coolDown.bind(this));
    this.rpc.on('routing_listConnections', [], () => this.availableConnections);
    this.rpc.on('status_ping', [], () => null);
    this.rpc.on('status_getApiVersion', [], () => '1.2');
    this.rpc.on('client_setVersion', [], () => null);
  }

  private warmup(connection: string) {
    this.nextMessageIDs[connection] = 0;
    this.warmConnections.add(connection);
  }

  private coolDown(connection: string) {
    this.warmConnections.delete(connection);
  }

  private onMessage(connection: string, message: string) {
    if (!this.warmConnections.has(connection)) {
      throw new Error(`Connection ${connection} has not been warmed up yet`);
    }
    const regExp = /\d+\s\d+\s(?<id>\d+)\s?(?<command>\w*):\w*/;
    const match = regExp.exec(message);
    const id = match?.groups?.id;
    if (id == null) {
      throw new Error(`Could not find id in ${message}`);
    }
    if (this.failNextResponse) {
      this.failNextResponse = false;
      this.rpc.notification('routing_error', [connection, 'disconnected']);
    } else {
      this.rpc.notification('routing_reply', [connection, [`@01 0 ${id} OK IDLE -- 0`], this.nextMessageIDs[connection]++]);
    }
  }

  public expectConnectionsClosed() {
    expect([...this.warmConnections.values()]).to.deep.equal([]);
  }
}
