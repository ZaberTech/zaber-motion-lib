import { expect } from 'chai';

import {
  DeviceDbFailedException, InvalidArgumentException, Length, DeviceNotIdentifiedException, CommandFailedException,
  RequestTimeoutException, ConnectionClosedException, ConnectionFailedException,
  RemoteModeException,
} from '../../src';
import { Connection, Device, AxisType } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { parseCommand, parseLines } from './protocol';
import { identifyDevice } from './devices/identify';
import { eqAndPush as eqAndPushBase } from '../test_util';

const server = new TestServer();
const eqAndPush = (pushStr: string, popStr: string, axis = 0) => eqAndPushBase(server, 1, axis, pushStr, popStr);

describe('Device', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('identify', () => {
    it('identifies simple one axis device', async () => {
      const promise = device.identify();

      await identifyDevice(1, server, {
        deviceid: 50081,
        serial: 12345,
        version: {
          version: '6.28',
          build: '1347',
        },
        axes: [{
          resolution: 64,
        }],
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        axisCount: 1,
        deviceId: 50081,
        firmwareVersion: {
          build: 1347,
          major: 6,
          minor: 28,
        },
        name: 'X-LHM100A',
        serialNumber: 12345,
        isModified: false,
        isIntegrated: true,
      });
      expect(device.getAxis(1).identity).to.deep.eq({
        axisType: AxisType.LINEAR,
        isPeripheral: false,
        peripheralName: '',
        peripheralSerialNumber: 0,
        peripheralId: 0,
        isModified: false,
      });
    });

    it('identifies a prototype device', async () => {
      const promise = device.identify();

      await identifyDevice(1, server, {
        deviceid: 50081,
        serial: 4294967295,
        version: {
          version: '6.28',
          build: '1347'
        },
        axes: [{
          resolution: 64
        }],
      });

      const identity = await promise;
      expect(identity).to.deep.include({
        serialNumber: 4294967295,
      });
    });

    it('throws exception when device cannot be identified', async () => {
      const promise = device.identify();
      await identifyDevice(1, server, {
        deviceid: 123,
        serial: 456,
        version: {
          version: '3.21',
          build: '123'
        },
        axes: [{
          resolution: 64
        }],
      });

      await expect(promise).to.be.rejectedWith(DeviceDbFailedException);
      const exception: DeviceDbFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Cannot find product with device ID 123 and firmware version 3.21.123');
      expect(exception.details).to.deep.eq({ code: 'not-found' });
    });

    it('identifies composite two axis device', async () => {
      const promise = device.identify();
      await identifyDevice(1, server, {
        deviceid: 30222,
        serial: 1111,
        version: {
          version: '6.29',
          build: '1432'
        },
        axes: [
          { peripheralid: 44122, peripheralSerialNumber: 13333, resolution: 64 },
          { peripheralid: 44112, peripheralSerialNumber: 13338, resolution: 64 },
        ]
      });

      const identity = await promise;
      expect(identity).to.deep.eq({
        axisCount: 2,
        deviceId: 30222,
        firmwareVersion: {
          build: 1432,
          major: 6,
          minor: 29,
        },
        name: 'X-MCB2',
        serialNumber: 1111,
        isModified: false,
        isIntegrated: false,
      });
      expect(device.getAxis(1).identity).to.deep.eq({
        axisType: AxisType.LINEAR,
        isPeripheral: true,
        peripheralName: 'ASR120B-E03T3 (Upper Axis)',
        peripheralSerialNumber: 13333,
        peripheralId: 44122,
        isModified: false,
      });
      expect(device.getAxis(2).identity).to.deep.eq({
        axisType: AxisType.LINEAR,
        isPeripheral: true,
        peripheralName: 'ASR100B-E03T3 (Lower Axis)',
        peripheralSerialNumber: 13338,
        peripheralId: 44112,
        isModified: false,
      });
    });

    it('assumes the provided version', async () => {
      const promise = device.identify({
        assumeVersion: { major: 7, minor: 8, build: 3904 },
      });

      await eqAndPush('get deviceid', 'OK IDLE -- 50081');
      await eqAndPush('get system.serial', 'OK IDLE -- 12345');
      await eqAndPush('get device.hw.modified', 'RJ IDLE -- BADCOMMAND');
      await eqAndPush('get comm.command.packets.max', 'RJ IDLE -- BADCOMMAND');
      await eqAndPush('get comm.word.size.max', 'RJ IDLE -- BADCOMMAND');
      await eqAndPush('get get.settings.max', 'RJ IDLE -- BADCOMMAND');
      await eqAndPush('get system.axiscount', 'OK IDLE -- 1');
      await eqAndPush('get resolution', 'OK IDLE -- 64', 1);
      await eqAndPush('get peripheralid', 'RJ IDLE -- BADCOMMAND', 1);

      expect(await promise).to.deep.contain({
        deviceId: 50081,
        firmwareVersion: {
          build: 3904,
          major: 7,
          minor: 8,
        },
      });

      expect(device.firmwareVersion).to.deep.eq({
        build: 3904,
        major: 7,
        minor: 8,
      });
    });
  });

  it('identifies an old device', async () => {
    const promise = device.identify();

    await eqAndPush('get deviceid', 'OK IDLE -- 22011');
    await eqAndPush('get system.serial', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get version', 'OK IDLE -- 6.14');
    await eqAndPush('get version.build', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get device.hw.modified', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get comm.command.packets.max', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get comm.word.size.max', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get get.settings.max', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush('get system.axiscount', 'OK IDLE -- 1');
    await eqAndPush('get resolution', 'OK IDLE -- 64', 1);
    await eqAndPush('get peripheralid', 'RJ IDLE -- BADCOMMAND', 1);

    const identity = await promise;
    expect(identity).to.deep.eq({
      axisCount: 1,
      deviceId: 22011,
      firmwareVersion: {
        major: 6,
        minor: 14,
        build: -1,
      },
      name: 'A-BLQ0070',
      serialNumber: 0,
      isModified: false,
      isIntegrated: true,
    });
  });

  it('failed identification erases previous identity', async () => {
    await DeviceXLHM.identify(device, server);
    connection.resetIds();

    expect(device.isIdentified).to.eq(true);

    const promise = device.identify();
    expect(await server.pop()).to.eq('/1 0 00 get deviceid');
    await server.push('@01 0 0 RJ IDLE -- NOPE');
    await expect(promise).to.rejectedWith(CommandFailedException);

    expect(device.isIdentified).to.eq(false);
  });

  describe('genericCommand', () => {
    it('sends correct command and passes response', async () => {
      const promise = device.genericCommand('home', { axis: 3, checkErrors: false });

      expect(await server.pop()).to.eq('/1 3 00 home');
      await server.push('@01 3 00 RJ BUSY -- BADCOMMAND');

      expect(await promise).to.deep.include({ data: 'BADCOMMAND' });
    });

    it('throws exception when command contains reserved characters', async () => {
      const promise = device.genericCommand('/1 home');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);

      const exception: InvalidArgumentException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Command argument contains ASCII protocol reserved characters /:@!#. ' +
        'Please note, that the library constructs the ASCII command for you.'
      );
    });
  });

  describe('toString', () => {
    it('returns representation for non identified device', () => {
      expect(device.toString()).to.startWith('Device 1 (Unknown) -> Connection');
    });

    it('returns representation for identified device', async () => {
      const identifyPromise = device.identify();
      await DeviceXLHM.identifyNumber(1, server);
      await identifyPromise;

      expect(device.toString()).to.startWith('Device 1 SN: 1234 (X-LHM100A) -> Connection');
    });

    it('returns representation for closed connection', async () => {
      await connection.close();
      expect(device.toString()).to.eq('Device 1 -> Connection Closed');
    });
  });

  describe('identity', () => {
    it('returns null before identification and identity after', async () => {
      expect(() => device.identity).to.throw(/Device number 1 is not identified\./);

      const identifyPromise = device.identify();
      await DeviceXLHM.identifyNumber(1, server);
      await identifyPromise;

      expect(device.identity.name).to.eq('X-LHM100A');
    });
  });

  describe('properties', () => {
    it('returns appropriate data', async () => {
      const identifyPromise = device.identify();
      await DeviceXLHM.identifyNumber(1, server);
      await identifyPromise;

      expect(device.deviceId).to.eq(50081);
      expect(device.serialNumber).to.eq(1234);
      expect(device.name).to.eq('X-LHM100A');
      expect(device.axisCount).to.eq(1);
      expect(device.firmwareVersion).to.deep.eq({
        build: 1347,
        major: 6,
        minor: 28,
      });
    });

    it('checks if isIdentified is false before identifying', async () => {
      expect(device.isIdentified).to.equal(false);
    });

    it('checks if isIdentified is true after identifying', async () => {
      const identifyPromise = device.identify();
      await DeviceXLHM.identifyNumber(1, server);
      await identifyPromise;
      expect(device.isIdentified).to.equal(true);
    });
  });

  describe('axis', () => {
    it('throws exception for invalid axis number', async () => {
      expect(() => device.getAxis(0)).to.throw('Invalid value; physical axes are numbered from 1.');
    });
  });

  describe('genericCommand', () => {
    it('handles EtherCAT rejection', async () => {
      const replyPromise = device.genericCommand('foo');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      await server.push(`@01 0 ${cmd.idStr} RJ IDLE -- REMOTE`);

      await expect(replyPromise).to.be.rejectedWith(RemoteModeException);
    });
  });

  describe('genericCommandMultiResponse', () => {
    it('sends correct command and returns replies', async () => {
      const replyPromise = device.genericCommandMultiResponse('fly');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id fly');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
      await server.push(`#01 0 ${cmd.idStr} bzzz...`);
      await server.push(`#01 0 ${cmd.idStr} ...zzz`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      const result = await replyPromise;

      expect(result.length).to.eq(3);
      expect(result[0]).to.deep.include({ data: '0' });
      expect(result[1]).to.deep.include({ data: 'bzzz...' });
      expect(result[2]).to.deep.include({ data: '...zzz' });
    });
  });

  describe('genericCommandNoResponse', () => {
    it('sends correct command', async () => {
      const replyPromise = device.genericCommandNoResponse('home');

      const line = await server.pop();
      expect(line).to.eq('/1 0 home');

      await replyPromise;
    });
  });

  describe('prepareCommand', () => {
    it('returns command without unit conversions when possible', async () => {
      const command = device.prepareCommand('move sin ? ? ?',
        { value: 32000 },
        { value: 1000.9 },
        { value: 3 },
      );

      expect(command).to.eq('move sin 32000 1000.9 3');
    });

    it('throws exception when device not identified and doing unit conversion', () => {
      try {
        device.prepareCommand('move sin ? ? ?',
          { value: 10, unit: Length.mm },
          { value: 1000 },
          { value: 3 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(DeviceNotIdentifiedException);
      }
    });
  });

  describe('waitToRespond', () => {
    beforeEach(() => {
      connection.defaultRequestTimeout = 1;
      server.ignoreWaitingLinesOnClose();
    });

    it('waits for the device to start responding', async () => {
      const promise = device.waitToRespond(10000);

      (async () => {
        for (let i = 0; i < 3; i++) {
          await server.pop();
        }
        connection.defaultRequestTimeout = 1000;
        for (;;) {
          await eqAndPush('', 'OK IDLE -- 0');
        }
      })().catch(() => null);

      await promise;
    });

    it('throws RequestTimeoutException when timeout passes', async () => {
      await expect(device.waitToRespond(20)).to.be.rejectedWith(RequestTimeoutException);
    });

    it('throws any other exceptions', async () => {
      const promise = device.waitToRespond(10000);
      for (let i = 0; i < 3; i++) {
        await server.pop();
      }
      setTimeout(() => server.closeSocket(), 0);
      const exception = await promise.catch(e => e);
      expect([
        ConnectionClosedException,
        ConnectionFailedException,
      ].some(Exc => exception instanceof Exc)).to.eq(true);
    });
  });

  describe('renumber device', () => {
    it('renumbers device', async () => {
      expect(device.deviceAddress).to.eq(1);
      const promise = device.renumber(4);

      expect(await server.pop()).to.eq('/1 0 00 renumber 4');
      await server.push('@04 0 00 OK IDLE -- 0');

      const newDevice = await promise;
      expect(newDevice.deviceAddress).to.eq(4);
    });

    it('fails to renumber device', async () => {
      connection.defaultRequestTimeout = 1;
      const promise = device.renumber(5);
      const err = expect(promise).to.be.rejectedWith(RequestTimeoutException);

      expect(await server.pop()).to.eq('/1 0 00 renumber 5');
      await err;
    });
  });

  describe('restore', () => {
    it('restores the device hard', async () => {
      const promise = device.restore(true);
      await eqAndPush('system factoryreset', 'OK IDLE -- 0');
      await promise;
    });

    it('restores the device soft, deleting all the zaber keys', async () => {
      const promise = device.restore();

      const command = parseCommand(await server.pop());
      expect(command.strNoId).to.eq('/1 0 id storage all print keys prefix zaber.');
      await server.push(`@01 0 ${command.id} OK IDLE -- 0`);
      await server.push(`#01 0 ${command.id} set zaber.foo`);
      await server.push(`#01 1 ${command.id} set zaber.foo`);
      await server.push(`#01 1 ${command.id} set zaber.bar`);
      await eqAndPush('', 'OK IDLE -- 0');

      await eqAndPush('storage erase zaber.foo', 'OK IDLE -- 0');
      await eqAndPush('storage axis erase zaber.foo', 'OK IDLE -- 0', 1);
      await eqAndPush('storage axis erase zaber.bar', 'OK IDLE -- 0', 1);

      await eqAndPush('system restore', 'OK IDLE -- 0');

      await promise;
    });

    it('restores axis, deleting all the zaber keys', async () => {
      const promise = device.getAxis(2).restore();

      const command = parseCommand(await server.pop());
      expect(command.strNoId).to.eq('/1 2 id storage axis print keys prefix zaber.');
      await server.push(`@01 2 ${command.id} OK IDLE -- 0`);
      await server.push(`#01 2 ${command.id} set zaber.foo`);
      await server.push(`#01 2 ${command.id} set zaber.bar`);
      await eqAndPush('', 'OK IDLE -- 0', 2);

      await eqAndPush('storage axis erase zaber.foo', 'OK IDLE -- 0', 2);
      await eqAndPush('storage axis erase zaber.bar', 'OK IDLE -- 0', 2);

      await eqAndPush('axis restore', 'OK IDLE -- 0', 2);

      await promise;
    });

    it('ignores storage not supported', async () => {
      const promise = device.restore();

      await eqAndPush('storage all print keys prefix zaber.', 'RJ IDLE -- BADCOMMAND');
      await eqAndPush('', 'OK IDLE -- 0');

      await eqAndPush('system restore', 'OK IDLE -- 0');

      await promise;
    });
  });
});
