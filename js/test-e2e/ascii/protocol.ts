export interface Command {
  device: number;
  axis: number;
  id: number;
  idStr: string;
  data: string;
  str: string;
  strNoId: string;
}

export const parseCommand = (command: string): Command => {
  const str = command.trim();
  // fairly simple and incomplete, but works for tests
  const match = command.trim().match(/^\/(\d+) (\d+) (\d+) ?(.*)$/);

  if (!match) {
    throw new Error(`Cannot parse: ${command}`);
  }
  const [, device, axis, id, data] = match;
  return {
    device: Number(device),
    axis: Number(axis),
    id: Number(id),
    idStr: id,
    data,
    str,
    strNoId: `/${device} ${axis} id ${data}`.trim(),
  };
};

export const parseLines = (lines: string[]): Command[] =>
  lines.map(parseCommand).sort((a, b) => a.strNoId.localeCompare(b.strNoId));

export const padId = (id: number | string) => String(id).padStart(2, '0');

export const signature = (cmd: Command) => `${cmd.device} ${cmd.axis} ${cmd.idStr}`;
