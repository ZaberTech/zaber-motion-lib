import { expect } from 'chai';

import { Connection, Device, MessageType } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import {
  Length, Time, DeviceDbSourceType, Library, Velocity, NotSupportedException, InvalidArgumentException, CommandTooLongException
} from '../../src';
import { DeviceXMCB2 } from './devices/xmcb2';
import { MASTER_DEVICE_DB } from '../constants';
import { DeviceXMCC4 } from './devices/xmcc4';
import { DeviceIdentificationInfo, identifyDevice } from './devices/identify';
import { parseCommand } from './protocol';
import { eqAndPush as eqAndPushBase } from '../test_util';

const server = new TestServer();
const eqAndPush = (pushStr: string, popStr: string, axis = 0) => eqAndPushBase(server, 1, axis, pushStr, popStr);

describe('Device - identified', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('integrated', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
      connection.resetIds();
    });

    describe('prepareCommand', () => {
      it('converts and formats device scope commands', () => {
        let command = device.prepareCommand('move sin ? ? ?',
          { value: 20.0, unit: Length.mm },
          { value: 2.0918 },
          { value: 1 },
        );

        expect(command).to.eq('move sin 161260 2.1 1');

        command = device.prepareCommand('trigger ? action a ? move abs ?',
          { value: 2 },
          { value: 1 },
          { value: 20.0, unit: Length.mm },
        );
        expect(command).to.eq('trigger 2 action a 1 move abs 161260');
      });

      it('converts and formats device scope commands where token type argument is a number', () => {
        let command = device.prepareCommand('trigger ? action b ? maxspeed += ?',
          { value: 2 },
          { value: 1 },
          { value: 10000 }, // token
        );
        expect(command).to.eq('trigger 2 action b 1 maxspeed += 10000');

        command = device.prepareCommand('trigger ? when ? pos >= ?',
          { value: 2 },
          { value: 1 },
          { value: 10000 }, // token
        );
        expect(command).to.eq('trigger 2 when 1 pos >= 10000');
      });

      it('throws error on device scope command where token type argument is a number with units', () => {
        // we cannot handle this conversion because the dimension depends on the previous token argument.
        expect(() => device.prepareCommand('trigger ? action b ? maxspeed += ?',
          { value: 2 },
          { value: 1 },
          { value: 10000, unit: Velocity.MILLIMETRES_PER_SECOND },
        )).to.throw('Command argument index 2 does not support unit conversion.');
      });
    });

    describe('restore', () => {
      it('clears devices identity', async () => {
        const promise = device.restore(true);
        await eqAndPush('system factoryreset', 'OK IDLE -- 0');
        await promise;

        expect(device.isIdentified).to.eq(false);
      });
    });
  });

  describe('multi axis', () => {
    beforeAll(() => {
      Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, MASTER_DEVICE_DB);
    });
    afterAll(() => {
      Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE);
    });

    beforeEach(async () => {
      await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.OneLinearOneRotary, DeviceXMCB2.FWType.FW7Proto);
    });

    describe('prepareCommand', () => {
      it('converts and formats device scope command', () => {
        const command = device.prepareCommand('stream ? wait ?',
          { value: 1 },
          { value: 1.23456, unit: Time.SECONDS },
        );

        expect(command).to.eq('stream 1 wait 1235');
      });
    });
  });

  describe('hardware modified product', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(
        device,
        server,
        DeviceXMCC4.Mode.FourLinear,
        DeviceXMCC4.FWVersion.FW7_21,
        { controller: true, axes: [1] }
      );
      connection.resetIds();
    });

    it('exposes whether the device and peripherals are modified', () => {
      expect(device.identity.isModified).to.eq(true);
      expect(device.getAxis(1).identity.isModified).to.eq(true);
      expect(device.getAxis(2).identity.isModified).to.eq(false);
    });

    it('exposes settings available to modified device', async () => {
      const modAxis = device.getAxis(1);
      const commonAxis = device.getAxis(2);

      const promise = modAxis.settings.set('cloop.displace.tolerance', 1, Length.mm);
      expect(await server.pop()).to.eq('/1 1 00 set cloop.displace.tolerance 8063');
      await server.push('@01 1 0 OK IDLE -- 0');
      await promise;

      const promise2 = commonAxis.settings.set('cloop.displace.tolerance', 1, Length.mm);
      await expect(promise2).to.be.rejectedWith(NotSupportedException);
    });

    it('exposes commands available to modified device', async () => {
      const modAxis = device.getAxis(1);
      const commonAxis = device.getAxis(2);

      const command = modAxis.prepareCommand('tools findrange');
      expect(command).to.eq('tools findrange');

      expect(() => commonAxis.prepareCommand('tools findrange')).to.throw(NotSupportedException);
    });
  });

  describe('Line continuations', () => {
    const identificationInfo: DeviceIdentificationInfo = {
      deviceid: 50081,
      serial: 1234,
      version: { version: '7.26', build: '11669' },
      protocol: { maxPackets: 2, wordLen: 50, maxSyncSettings: 12 },
      axes: [{ resolution: 64 }],
    };

    const word5 = 'abcde';
    const word10 = word5 + word5;
    const word30 = word10 + word10 + word10;
    const word31 = `${word30}g`;

    it('Pre-7.26 devices can write long words, but only one packet', async () => {
      const identifyPromise = device.identify();
      await identifyDevice(1, server, { ...identificationInfo, protocol: undefined });
      await identifyPromise;

      const word55 = word30 + word10 + word10 + word5;
      const replyPromise1 = device.genericCommand(`tools echo ${word55}`);

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq(`/1 0 id tools echo ${word55}`);
      await server.push(`@1 0 ${cmd.idStr} OK IDLE -- ${word55}`);

      await replyPromise1;

      const replyPromise2 = device.genericCommand(`tools echo ${word55} ${word55}`);

      await expect(replyPromise2).to.eventually.rejectedWith(CommandTooLongException);
    });

    it('can write line continuations', async () => {
      const identifyPromise = device.identify();
      await identifyDevice(1, server, identificationInfo);
      await identifyPromise;

      const replyPromise = device.genericCommand(`tools echo ${word30} ${word31}`);

      const cmd1 = parseCommand(await server.pop());
      expect(cmd1.strNoId).to.eq(`/1 0 id tools echo ${word30}\\`);
      const cmd2 = parseCommand(await server.pop());
      expect(cmd2.str).to.eq(`/1 0 ${cmd1.id} cont 1 ${word31}`);

      await server.push(`@01 0 ${cmd1.id} OK IDLE -- ${word30}\\`);
      await server.push(`#01 0 ${cmd1.id} cont ${word31}`);

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: `${word30} ${word31}`, deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
    });

    it('starts splitting exactly at the 80 length limit', async () => {
      const identifyPromise = device.identify();
      await identifyDevice(1, server, identificationInfo);
      await identifyPromise;

      connection.resetIds();

      const TEST_CMD = 'stream 1 on a b arc abs ccw 3254 9412 3054 10971 join maxspeed 19418';

      let replyPromise = device.genericCommand(TEST_CMD);

      let raw = await server.pop(true);
      expect(raw).to.have.length(80 - 1); // -1 for the line ending
      expect(raw).to.eq('/1 0 00 stream 1 on a b arc abs ccw 3254 9412 3054 10971 join maxspeed 19418:9F');

      await server.push('@01 0 0 OK IDLE -- 0');
      await replyPromise;

      replyPromise = device.genericCommand(`${TEST_CMD}0`);

      raw = await server.pop(true);
      expect(raw).to.eq('/1 0 01 stream 1 on a b arc abs ccw 3254 9412 3054 10971 join maxspeed\\:69');
      raw = await server.pop(true);
      expect(raw).to.eq('/1 0 01 cont 1 194180:82');

      await server.push('@01 0 1 OK IDLE -- 0');
      await replyPromise;
    });

    it('Cannot write a word that is longer than comm.word.size.max', async () => {
      const identifyPromise = device.identify();
      await identifyDevice(1, server, { ...identificationInfo, protocol: { maxPackets: 1, wordLen: 30 } });
      await identifyPromise;

      const replyPromise = device.genericCommand(`tools echo ${word30} ${word31}`);

      await expect(replyPromise).to.eventually.rejectedWith(
        InvalidArgumentException,
        `Could not write word ${word31} which is longer than the maximum allowed value 30 to the ASCII protocol`
      );
    });

    it('Identified devices check that they are sending the right number of packets', async () => {
      const identifyPromise = device.identify();
      await identifyDevice(1, server, { ...identificationInfo, protocol: { maxPackets: 2, wordLen: 50 } });
      await identifyPromise;

      const word45 = word30 + word10 + word5;
      const word50 = word45 + word5;

      // First a 2 line command.
      const echoPromise = device.genericCommand(`tools echo ${word45} ${word50}`);

      const cmd = parseCommand(await server.pop());
      expect(cmd.data).to.equal(`tools echo ${word45}\\`);
      expect(await server.pop()).to.equal(`/1 0 ${cmd.idStr} cont 1 ${word50}`);

      await server.push(`@1 0 ${cmd.idStr} OK IDLE -- ${word45}\\`);
      await server.push(`#1 0 ${cmd.idStr} cont ${word50}`);

      await echoPromise;

      // Next, one that will need to be split over 3 lines
      try {
        await device.genericCommand(`tools echo ${word45} ${word50} ${word50}`);
        throw new Error('Error expected');
      } catch (e) {
        if (e instanceof CommandTooLongException) {
          expect(e.details).to.deep.eq({
            fit: `tools echo ${word45} ${word50}`,
            remainder: word50,
            packetSize: 80,
            packetsMax: 2,
          });
        } else {
          throw e;
        }
      }
    });
  });
});
