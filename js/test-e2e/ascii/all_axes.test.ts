import { expect } from 'chai';

import { Connection, AllAxes, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { Library } from '../../src';
import { parseCommand } from './protocol';

const server = new TestServer();

describe('All Axes', () => {
  let connection: Connection;
  let device: Device;
  let allAxes: AllAxes;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    allAxes = device.allAxes;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('home', () => {
    it('sends proper home request', async () => {
      const promise = allAxes.home();

      expect(await server.pop()).to.eq('/1 0 00 home');
      await server.push('@01 0 00 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 0 01');
      await server.push('@01 0 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('stop', () => {
    it('sends proper stop request', async () => {
      const promise = allAxes.stop();

      expect(await server.pop()).to.eq('/1 0 00 stop');
      await server.push('@01 0 00 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 0 01');
      await server.push('@01 0 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('waitUntilIdle', () => {
    it('waits until device sends idle in status', async () => {
      const promise = allAxes.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 0 01');
      await server.push('@01 0 01 OK IDLE -- 0');

      await promise;
    });

    it('ignores alerts from axes', async () => {
      const promise = allAxes.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('!01 2 IDLE --');
      await server.push('@01 0 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 0 01');
      await server.push('!01 3 IDLE --');
      await server.push('@01 0 01 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 0 02');
      await server.push('@01 0 02 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 0 03');
      await server.push('@01 0 03 OK IDLE -- 0');

      await promise;
    });
  });

  describe('isBusy', () => {
    it('sends proper status request', async () => {
      const promise = allAxes.isBusy();

      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('isHomed', () => {
    it('sends proper warning request', async () => {
      const promise = allAxes.isHomed();

      expect(await server.pop()).to.eq('/1 0 00 warnings');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('park', () => {
    it('sends proper parking request', async () => {
      const promise = device.allAxes.park();

      expect(await server.pop()).to.eq('/1 0 00 tools parking park');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('unpark', () => {
    it('sends proper unparking request', async () => {
      const promise = device.allAxes.unpark();

      expect(await server.pop()).to.eq('/1 0 00 tools parking unpark');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('driver', () => {
    it('sends disable driver command correctly', async () => {
      const promise = device.allAxes.driverDisable();

      expect(await server.pop()).to.eq('/1 0 00 driver disable');
      await server.push('@01 0 00 OK IDLE FO 0');

      await promise;
    });

    it('tries to enable the driver', async () => {
      Library.setIdlePollingPeriod(0);
      const promise = device.allAxes.driverEnable();

      for (let i = 3; i >= 0; i--) {
        const cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/1 0 id driver enable');
        if (i > 0) {
          await server.push(`@01 0 ${cmd.id} RJ IDLE FO DRIVERDISABLED`);
        } else {
          await server.push(`@01 0 ${cmd.id} OK IDLE -- 0`);
        }
      }

      await promise;
    });
  });

  describe('toString', () => {
    it('returns representation', () => {
      expect(allAxes.toString()).to.startWith('All Axes -> Device 1 (Unknown) -> Connection');
    });
    it('returns representation for non-existing', async () => {
      await connection.close();
      expect(allAxes.toString()).to.eq('All Axes -> Device 1 -> Connection Closed');
    });
  });
});
