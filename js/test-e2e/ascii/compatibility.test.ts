import { expect } from 'chai';
import _ from 'lodash';
import { firstValueFrom } from 'rxjs';

import { Connection, Device, AxisType } from '../../src/ascii';
import { Time } from '../../src';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXJOY } from './devices/xjoy';
import { DeviceXLCA } from './devices/xlca';
import { DeviceXRDA } from './devices/xrda';
import { DeviceXSCA4 } from './devices/xsca4';

const server = new TestServer();

describe('Device', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('X-JOY', () => {
    it('works with the library', async () => {
      await DeviceXJOY.identify(device, server);

      expect(device.identity).to.deep.eq({
        deviceId: 51000,
        name: 'X-JOY3',
        serialNumber: 6666,
        axisCount: 0,
        firmwareVersion: {
          major: 6,
          minor: 31,
          build: 1541,
        },
        isModified: false,
        isIntegrated: true,
      });

      expect(device.toString()).to.startWith('Device 1 SN: 6666 (X-JOY3) ->');
    });

    it('supports special alerts', async () => {
      const alertPromise = firstValueFrom(connection.alert);

      await server.push('!02 0 key 1 1');

      const alert = await alertPromise;

      expect(alert).to.deep.eq({
        deviceAddress: 2,
        axisNumber: 0,
        status: '',
        warningFlag: '',
        data: 'key 1 1',
      });
    });
  });

  describe('X-LCA', () => {
    it('works with the library', async () => {
      await DeviceXLCA.identify(device, server);

      expect(device.identity).to.deep.eq({
        deviceId: 52005,
        name: 'X-LCA4 (rev 1)',
        serialNumber: 1234,
        axisCount: 4,
        firmwareVersion: {
          major: 7,
          minor: 9,
          build: 3906,
        },
        isModified: false,
        isIntegrated: false,
      });

      expect(device.toString()).to.startWith('Device 1 SN: 1234 (X-LCA4 (rev 1)) ->');

      const axisIdentities = _.range(device.axisCount).map(i => device.getAxis(i + 1).identity);
      expect(axisIdentities).to.deep.eq([{
        axisType: AxisType.LAMP,
        isPeripheral: true,
        peripheralId: 46663,
        peripheralName: 'LED385A-T13A',
        peripheralSerialNumber: 27011,
        isModified: false,
      }, {
        axisType: AxisType.LAMP,
        isPeripheral: true,
        peripheralId: 46664,
        peripheralName: 'LED473A-T13A',
        peripheralSerialNumber: 27013,
        isModified: false,
      }, {
        axisType: AxisType.LAMP,
        isPeripheral: true,
        peripheralId: 46665,
        peripheralName: 'LED568A-T13A',
        peripheralSerialNumber: 27017,
        isModified: false,
      }, {
        axisType: AxisType.LAMP,
        isPeripheral: true,
        peripheralId: 46666,
        peripheralName: 'LED625A-T13A',
        peripheralSerialNumber: 27019,
        isModified: false,
      }]);

      const cmd = device.getAxis(1).prepareCommand('lamp on ?', { value: 123.4, unit: Time.MILLISECONDS });
      expect(cmd).to.eq('lamp on 123.4');
    });
  });

  describe('X-RDA', () => {
    it('works with the library and has isPeripheral=false and isIntegrated=true on axes', async () => {
      await DeviceXRDA.identify(device, server);

      expect(device.identity).to.deep.eq({
        deviceId: 50952,
        name: 'X-ADR250B100B-AE53D12-ENG3216',
        serialNumber: 1234,
        axisCount: 2,
        firmwareVersion: {
          major: 7,
          minor: 15,
          build: 8617,
        },
        isModified: false,
        isIntegrated: true,
      });
      expect(device.getAxis(1).identity).to.deep.eq({
        axisType: AxisType.LINEAR,
        isPeripheral: false,
        peripheralId: 70315,
        peripheralName: 'ADR250B-AE53-ENG3216 (Lower Axis)',
        peripheralSerialNumber: 1111,
        isModified: false,
      });
      expect(device.getAxis(2).identity).to.deep.eq({
        axisType: AxisType.LINEAR,
        isPeripheral: false,
        peripheralId: 70316,
        peripheralName: 'ADR100B-AE53-ENG3216 (Upper Axis)',
        peripheralSerialNumber: 1119,
        isModified: false,
      });
    });
  });

  describe('X-SCA', () => {
    it('works with the library', async () => {
      await DeviceXSCA4.identify(device, server);

      expect(device.identity).to.deep.eq({
        deviceId: 52007,
        name: 'X-SCA4',
        serialNumber: 1234,
        axisCount: 4,
        firmwareVersion: {
          major: 7,
          minor: 35,
          build: 14985,
        },
        isModified: false,
        isIntegrated: true,
      });

      expect(device.toString()).to.startWith('Device 1 SN: 1234 (X-SCA4) ->');

      const axisIdentities = _.range(device.axisCount).map(i => device.getAxis(i + 1).identity);
      expect(axisIdentities).to.deep.eq([{
        axisType: AxisType.PROCESS,
        isPeripheral: false,
        peripheralId: 70386,
        peripheralName: 'Process',
        peripheralSerialNumber: 3003,
        isModified: false,
      }, {
        axisType: AxisType.PROCESS,
        isPeripheral: false,
        peripheralId: 70386,
        peripheralName: 'Process',
        peripheralSerialNumber: 3004,
        isModified: false,
      }, {
        axisType: AxisType.PROCESS,
        isPeripheral: false,
        peripheralId: 70386,
        peripheralName: 'Process',
        peripheralSerialNumber: 3005,
        isModified: false,
      }, {
        axisType: AxisType.PROCESS,
        isPeripheral: false,
        peripheralId: 70386,
        peripheralName: 'Process',
        peripheralSerialNumber: 3006,
        isModified: false,
      }]);

      const cmd = device.getAxis(1).prepareCommand('process on ?', { value: 123.4, unit: Time.MILLISECONDS });
      expect(cmd).to.eq('process on 123.4');
    });
  });
});
