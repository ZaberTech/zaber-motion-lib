import { expect } from 'chai';
import { Axis, Connection, Device } from '../../src/ascii';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { identifyDevice } from './devices/identify';
import { eqAndPush } from '../test_util';
import { DeviceXLHM } from './devices/xlhm';
import { InvalidOperationException } from '../../src';

const server = new TestServer();

function encode(value: string) {
  return Buffer.from(value).toString('base64');
}

describe('Naming', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('device with peripherals', () => {
    beforeEach(async () => {
      const idPromise = device.identify();
      await identifyDevice(device.deviceAddress, server, {
        deviceid: 30342,
        serial: 1234,
        version: { version: '7.26', build: '11669' },
        label: 'Yogi',
        axes: [{
          peripheralid: 70250,
          label: 'Boo-Boo',
        }],
      });

      await idPromise;
    });

    describe('label property', () => {
      it('returns label for device and axis', async () => {
        expect(device.label).to.eq('Yogi');
        expect(axis.label).to.eq('Boo-Boo');
      });
    });

    describe('toString', () => {
      it('prints label for device and axis', async () => {
        expect(device.toString()).to.startWith('Device 1 SN: 1234 Yogi (X-MCC4 (rev 2/3)) -> Connection');
        expect(axis.toString()).to.startWith('Axis 1 Boo-Boo (LDM110C-AE54T10A) -> Device');
      });
    });

    describe('SetLabel', () => {
      it('sets label for device and axis', async () => {
        let promise = device.setLabel('Ranger Smith');
        await eqAndPush(server, 1, 0, `storage set zaber.label ${encode('Ranger Smith')}`, 'OK IDLE -- 0');
        await promise;
        expect(device.label).to.eq('Ranger Smith');

        promise = axis.setLabel('Cindy Bear');
        await eqAndPush(server, 1, 1, `storage axis set zaber.label ${encode('Cindy Bear')}`, 'OK IDLE -- 0');
        await promise;
        expect(axis.label).to.eq('Cindy Bear');
      });

      it('erases the label', async () => {
        const promise = device.setLabel('');
        await eqAndPush(server, 1, 0, 'storage erase zaber.label', 'OK IDLE -- 0');
        await promise;
        expect(device.label).to.eq('');
      });
    });
  });

  describe('integrated device', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);
    });

    describe('SetLabel', () => {
      it('cannot set label to an axis', async () => {
        const promise = axis.setLabel('Cindy Bear');
        await expect(promise).to.eventually.be.rejectedWith(
          InvalidOperationException, 'Cannot set the name of an axis of integrated device'
        );
      });
    });
  });
});
