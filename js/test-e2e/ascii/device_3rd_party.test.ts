/* eslint-disable camelcase */
import { Axis, Connection, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXMCC4 } from './devices/xmcc4';
import { Acceleration, DCElectricCurrent, Length, Units, Velocity } from '../../src';
import { expect } from 'chai';
import { eqAndPush, expectStorage } from '../test_util';
import { DeviceIdentificationInfo, identifyDevice } from './devices/identify';

const server = new TestServer();

describe('Device - 3rd party peripheral', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('without unit conversions', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.One3rdParty, DeviceXMCC4.FWVersion.FW7_27);
    });

    it('throws error when custom unit conversion are not setup', () => {
      expect(() => axis.settings.convertFromNativeUnits('pos', 10, Length.mm)).throws(
        'Conversion not available for dimension: Length. Setup custom unit conversions.');
    });

    it('returns false when calling canConvertNativeUnits despite having a dimension', () => {
      expect(axis.settings.canConvertNativeUnits('pos')).to.eq(false);
    });
  });

  describe('setCustomUnitConversions', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.One3rdParty, DeviceXMCC4.FWVersion.FW7_27, { axes: [1] });
    });

    it('stores the custom scale factors', async () => {
      const promise = axis.settings.setCustomUnitConversions([{
        setting: 'pos', value: 0.01, unit: Length.mm,
      }, {
        setting: 'maxspeed', value: 0.01 / 0.125, unit: Velocity.MILLIMETRES_PER_SECOND,
      }, {
        setting: 'accel', value: 0.01 / Math.pow(0.125, 2), unit: Acceleration.MILLIMETRES_PER_SECOND_SQUARED,
      }, {
        setting: 'driver.current.max', value: 0.02, unit: DCElectricCurrent.A,
      }]);

      const storedData = await expectStorage(server, 1, 1, 'zaber.unit_conversions', 3);
      await promise;

      const conversions = JSON.parse(Buffer.from(storedData, 'base64').toString());
      expect(conversions).to.deep.eq({
        v: 1,
        rows: [{
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          function_name: 'linear-resolution',
          scale: 1562.4999999999998,
        }, {
          contextual_dimension_id: 2,
          dimension_name: 'Velocity',
          function_name: 'linear-resolution',
          scale: 195.31249999999997,
        }, {
          contextual_dimension_id: 3,
          dimension_name: 'Acceleration',
          function_name: 'linear-resolution',
          scale: 24.414062499999996,
        }, {
          contextual_dimension_id: 9,
          dimension_name: 'DC Electric Current',
          function_name: 'linear',
          scale: 50,
        }],
      });
    });

    it('throws errors on invalid arguments', async () => {
      await expect(axis.settings.setCustomUnitConversions([{
        setting: 'pos', value: 1, unit: Velocity.CENTIMETRES_PER_SECOND,
      }])).to.rejectedWith('Units Velocity:centimetres per second do not belong to dimension Length.');

      await expect(axis.settings.setCustomUnitConversions([{
        setting: 'knob.mode', value: 1, unit: Units.NATIVE,
      }])).to.rejectedWith('Setting knob.mode cannot be converted as it does not have a dimension.');

      await expect(axis.settings.setCustomUnitConversions([{
        setting: 'position', value: 1, unit: Units.NATIVE,
      }])).to.rejectedWith('Cannot find setting position.');

      await expect(axis.settings.setCustomUnitConversions([{
        setting: 'pos', value: 1, unit: Length.mm,
      }, {
        setting: 'limit.max', value: 1, unit: Length.mm,
      }])).to.rejectedWith('Dimension specified by multiple setting (pos, limit.max). Specify dimension only once.');
    });

    it('erases the storage if called with empty array', async () => {
      const promise = axis.settings.setCustomUnitConversions([]);
      await eqAndPush(server, 1, 1, 'storage axis erase zaber.unit_conversions', 'OK IDLE -- 0');
      await promise;
    });
  });

  describe('with custom unit conversions', () => {
    beforeEach(async () => {
      const info: DeviceIdentificationInfo = {
        deviceid: 30341,
        serial: 1234,
        version: {
          version: '7.27',
          build: '12025',
        },
        hwMod: true,
        axes: [{
          peripheralid: 88800,
          hwMod: true,
          resolution: 64,
          conversionTable: {
            v: 1,
            rows: [{
              contextual_dimension_id: 1,
              dimension_name: 'Length',
              function_name: 'linear-resolution',
              scale: 1562.5,
            }],
          },
        }],
      };
      const promise = device.identify();
      await identifyDevice(1, server, info);
      await promise;
    });

    it('converts using the new factor', () => {
      expect(axis.settings.convertToNativeUnits('pos', 1, Length.mm)).to.eq(100);
    });
  });
});
