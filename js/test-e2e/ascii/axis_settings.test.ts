import { expect } from 'chai';

import {
  Length, DeviceNotIdentifiedException, NotSupportedException, ConversionFailedException, Angle, Acceleration, Velocity,
  BadCommandException,
} from '../../src';
import { Connection, Axis, AxisSettings, Device, SettingConstants } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCC4 } from './devices/xmcc4';

const server = new TestServer();

describe('AxisSettings', () => {
  let connection: Connection;
  let axis: Axis;
  let device: Device;
  let settings: AxisSettings;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(2);
    axis = device.getAxis(1);
    settings = axis.settings;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('get', () => {
    it('sends the correct command and returns value', async () => {
      const promise = settings.get('foo');

      expect(await server.pop()).to.eq('/2 1 00 get foo');
      await server.push('@02 1 00 OK IDLE -- -123');

      const value = await promise;
      expect(value).to.deep.eq(-123);
    });

    it('sends the correct setting when using constants', async () => {
      const promise = settings.get(SettingConstants.POS);

      expect(await server.pop()).to.eq('/2 1 00 get pos');
      await server.push('@02 1 00 OK IDLE -- -123');

      const value = await promise;
      expect(value).to.deep.eq(-123);
    });

    it('throws exception when device does not support the setting', async () => {
      const promise = settings.get('foo');

      expect(await server.pop()).to.eq('/2 1 00 get foo');
      await server.push('@02 1 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
      const exception: BadCommandException = await promise.catch(e => e);
      expect(exception.message).to.startWith('Command "get foo" rejected: BADCOMMAND');
    });
  });

  describe('get - identified', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);
      connection.resetIds();
    });

    it('throws exception when setting is not supported by device settings table', async () => {
      const promise = settings.get('foo', Length.cm);

      expect(await server.pop()).to.eq('/2 1 00 get foo');
      await server.push('@02 1 00 OK IDLE -- 123');

      await expect(promise).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Conversion information not found for setting "foo".');
    });

    it('throws exception when wrong units are provided', async () => {
      const promise = settings.get(SettingConstants.POS, Angle.DEGREES);

      expect(await server.pop()).to.eq('/2 1 00 get pos');
      await server.push('@02 1 00 OK IDLE -- 123');

      await expect(promise).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
    });

    it('throws exception when setting has no dimension and units are provided', async () => {
      const promise = settings.get('limit.home.state', Length.mm);

      expect(await server.pop()).to.eq('/2 1 00 get limit.home.state');
      await server.push('@02 1 00 OK IDLE -- 123');

      await expect(promise).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Conversion not available for setting "limit.home.state".');
    });

    it('performs unit conversion', async () => {
      const promise = settings.get(SettingConstants.POS, Length.mm);

      expect(await server.pop()).to.eq('/2 1 00 get pos');
      await server.push('@02 1 00 OK IDLE -- 12345678');

      expect(await promise).to.be.closeTo(1531.1534, 3);
    });
  });

  describe('getDefault', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
      connection.resetIds();
    });

    it('performs unit conversion', async () => {
      expect(settings.getDefault(SettingConstants.MAXSPEED, Velocity.MILLIMETRES_PER_SECOND)).to.be.closeTo(5.8136, 3);
    });

    it('throws error on invalid units', async () => {
      expect(() => settings.getDefault(SettingConstants.MAXSPEED, Length.mm)).to.throw(/Invalid units/);
    });
  });

  describe('set', () => {
    it('sends the correct command and does not throw exception', async () => {
      const promise = settings.set('foo', -123);

      expect(await server.pop()).to.eq('/2 1 00 set foo -123');
      await server.push('@02 1 00 OK IDLE -- 0');

      await promise;
    });

    it('throws exception when device does not support the setting', async () => {
      const promise = settings.set('foo', -123);

      expect(await server.pop()).to.eq('/2 1 00 set foo -123');
      await server.push('@02 1 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
      const exception: BadCommandException = await promise.catch(e => e);
      expect(exception.message).to.startWith('Command "set foo -123" rejected: BADCOMMAND');
    });

    it('formats to the amount of decimal places specified', async () => {
      const promise = settings.set('foo', 17.21);
      expect(await server.pop()).to.eq('/2 1 00 set foo 17.21');
      await server.push('@02 1 00 OK IDLE -- 0');
      await promise;
    });

    it('throws exception when trying to do unit conversion', async () => {
      const promise = settings.set('foo', 0.1, Length.cm);
      await expect(promise).to.be.rejectedWith(DeviceNotIdentifiedException);
      const exception: DeviceNotIdentifiedException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot perform this operation. Device number 2 is not identified. Call device.identify() to enable this operation.');
    });
  });

  describe('set - identified', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
      connection.resetIds();
    });

    it('sends command when setting is not found and unit conversion is not needed', async () => {
      const promise = settings.set('foo', 2.11);
      expect(await server.pop()).to.eq('/2 1 00 set foo 2.11');
      await server.push('@02 1 00 OK IDLE -- 0');
      await promise;
    });

    it('throws exception when setting is not found and units are requested', async () => {
      const promise = settings.set('foo', 20, Length.cm);
      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Setting name "foo" not found.');
    });

    it('throws exception when wrong units are provided', async () => {
      const promise = settings.set(SettingConstants.LIMIT_MAX, 50, Angle.DEGREES);

      await expect(promise).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');
    });

    it('performs unit conversion', async () => {
      const promise = settings.set(SettingConstants.LIMIT_MAX, 1531.1534, Length.mm);

      expect(await server.pop()).to.eq('/2 1 00 set limit.max 12345678');
      await server.push('@02 1 00 OK IDLE -- 0');

      await promise;
    });

    it('checks for infinite acceleration conversion/rounding error', async () => {
      const promiseOk = settings.set('accel', 0.4, Acceleration.MILLIMETRES_PER_SECOND_SQUARED);
      const promiseError = settings.set('accel', 0.3, Acceleration.MILLIMETRES_PER_SECOND_SQUARED);

      await expect(promiseError).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promiseError.catch(e => e);
      expect(exception.message).to.eq([
        'The result of conversion and rounding is a forbidden value.',
        'Acceleration cannot be 0.',
        'Provided value: 0.300000 Acceleration:millimetres per second squared.',
      ].join(' '));

      expect(await server.pop()).to.eq('/2 1 00 set accel 1');
      await server.push('@02 1 00 OK IDLE -- 0');
      await promiseOk;
    });
  });

  describe('convert settings', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
      connection.resetIds();
    });

    it('throws exception when setting is not supported by device settings table', () => {
      expect(() => settings.convertFromNativeUnits('foo', 123, Length.cm))
        .to.throw('Conversion information not found for setting "foo".');
    });

    it('converts from native units', () => {
      expect(settings.convertFromNativeUnits(SettingConstants.POS, 806299, Length.cm)).to.be.closeTo(9.999, 1e-3);
    });

    it('converts to native units', () => {
      expect(settings.convertToNativeUnits(SettingConstants.POS, 10, Length.cm)).to.be.closeTo(806299.212, 1e-3);
    });
  });

  describe('getString', () => {
    it('sends the correct command and returns value', async () => {
      const promise = settings.getString('foo');

      expect(await server.pop()).to.eq('/2 1 00 get foo');
      await server.push('@02 1 00 OK IDLE -- bar');

      expect(await promise).to.eq('bar');
    });

    it('throws exception when the device does not support the setting', async () => {
      const promise = settings.getString('foo');

      expect(await server.pop()).to.eq('/2 1 00 get foo');
      await server.push('@02 1 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
    });
  });

  describe('setString', () => {
    it('sends the correct command and does not throw exception', async () => {
      const promise = settings.setString('foo', 'bar');

      expect(await server.pop()).to.eq('/2 1 00 set foo bar');
      await server.push('@02 1 00 OK IDLE -- 0');

      await promise;
    });

    it('throws exception when device does not support the setting', async () => {
      const promise = settings.setString('foo', 'bar');

      expect(await server.pop()).to.eq('/2 1 00 set foo bar');
      await server.push('@02 1 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
    });
  });

  describe('canConvertNativeUnits', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
    });

    it('returns true for settings with dimension', () => {
      expect(settings.canConvertNativeUnits('pos')).to.eq(true);
    });

    it('returns false for settings without dimension', () => {
      expect(settings.canConvertNativeUnits('knob.mode')).to.eq(false);
    });

    it('throws error on unknown setting', () => {
      expect(() => settings.canConvertNativeUnits('bla')).throws('Cannot find setting bla.');
    });
  });

  describe('multiget', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary, DeviceXMCC4.FWVersion.FW7_35);
      connection.resetIds();
    });

    it('gets many from axis', async () => {
      const promise = settings.getMany({ setting: 'pos', unit: Length.mm }, { setting: 'vel' }, { setting: 'accel' });

      expect(await server.pop()).to.eq('/2 0 00 get 1 accel pos vel');
      await server.push('@02 0 00 OK IDLE -- 9.8 ; 10000 ; 10');

      expect(await promise).to.deep.eq([
        { setting: 'pos', value: 1.2402343749999993, unit: Length.mm },
        { setting: 'vel', value: 10, unit: '' },
        { setting: 'accel', value: 9.8, unit: '' },
      ]);
    });

    it('gets synchronized from axis', async () => {
      const promise = settings.getSynchronized({ setting: 'pos', unit: Length.mm }, { setting: 'vel' }, { setting: 'accel' });

      expect(await server.pop()).to.eq('/2 0 00 get 1 accel pos vel');
      await server.push('@02 0 00 OK IDLE -- 9.8 ; 10000 ; 10');

      expect(await promise).to.deep.eq([
        { setting: 'pos', value: 1.2402343749999993, unit: Length.mm },
        { setting: 'vel', value: 10, unit: '' },
        { setting: 'accel', value: 9.8, unit: '' },
      ]);
    });
  });
});
