import { expect } from 'chai';

import {
  Acceleration, InvalidArgumentException, NotSupportedException, Time, Length, Velocity, DeviceNotIdentifiedException
} from '../../src';
import { Connection, Axis, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';

const server = new TestServer();

describe('Axis - identified', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(1);
    await server.waitForConnection();

    await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);
    connection.resetIds();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('exceptions', () => {
    it('throws exception when targeting invalid axis', async () => {
      const promise = device.getAxis(2).moveAbsolute(4.5, Length.mm);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      const exception: InvalidArgumentException = await promise.catch(e => e);
      expect(exception.message).to.eq('Target axis out of range: 2');
    });
  });

  describe('moveAbsolute', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveAbsolute(4.5, Length.mm, {
        waitUntilIdle: false, velocity: 1, velocityUnit: Velocity['mm/s'], acceleration: 100, accelerationUnit: Acceleration['mm/s²']
      });

      expect(await server.pop()).to.eq('/1 1 00 move abs 36283 13210 132');
      await server.push('@01 1 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('moveRelative', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveRelative(0.3, Length.cm, {
        waitUntilIdle: false, velocity: 1, velocityUnit: Velocity['mm/s'], acceleration: 100, accelerationUnit: Acceleration['mm/s²']
      });

      expect(await server.pop()).to.eq('/1 1 00 move rel 24189 13210 132');
      await server.push('@01 1 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('moveVelocity', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveVelocity(1.5, Velocity['mm/s'], { acceleration: 100, accelerationUnit: Acceleration['mm/s²'] });

      expect(await server.pop()).to.eq('/1 1 00 move vel 19816 132');
      await server.push('@01 1 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('moveMin/Max', () => {
    it('converts velocity and acceleration', async () => {
      let promise = axis.moveMax({
        waitUntilIdle: false, velocity: 1, velocityUnit: Velocity['mm/s'], acceleration: 100, accelerationUnit: Acceleration['mm/s²']
      });
      expect(await server.pop()).to.eq('/1 1 00 move max 13210 132');
      await server.push('@01 1 00 OK BUSY -- 0');
      await promise;

      promise = axis.moveMin({
        waitUntilIdle: false, velocity: 1, velocityUnit: Velocity['mm/s'], acceleration: 100, accelerationUnit: Acceleration['mm/s²']
      });
      expect(await server.pop()).to.eq('/1 1 01 move min 13210 132');
      await server.push('@01 1 01 OK BUSY -- 0');
      await promise;
    });
  });

  describe('moveSin', () => {
    it('Move sin with unit conversion', async () => {
      const promise = axis.moveSin(2, Length.MILLIMETRES, 2, Time.SECONDS, { waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 1 00 move sin 16126 2000.0');
      await server.push('@01 1 00 OK BUSY -- 0');

      await promise;
    });
  });


  describe('getPosition', () => {
    it('sends proper command and performs conversion', async () => {
      const promise = axis.getPosition(Length.mm);

      expect(await server.pop()).to.eq('/1 1 00 get pos');
      await server.push('@01 1 00 OK IDLE -- 80629');

      expect(await promise).to.be.closeTo(10, 0.001);
    });
  });

  describe('prepareCommand', () => {
    it('performs unit conversion where asked to do so and formats/rounds numbers', () => {
      const command = axis.prepareCommand('move sin ? ? ?',
        { value: 20, unit: Length.mm },
        { value: 4.16, unit: Time.MILLISECONDS },
        { value: 3 },
      );

      expect(command).to.eq('move sin 161260 4.2 3.0');
    });

    it('rounds up to the correct number of decimal places', () => {
      const command = axis.prepareCommand('move sin ? ? ?',
        { value: 20.2 },
        { value: 1.96 },
        { value: 3 },
      );

      expect(command).to.eq('move sin 20 2.0 3.0');
    });

    it('handles commands which do not end with leaf in command tree', () => {
      const command = axis.prepareCommand('move sin ? ?',
        { value: 10.0, unit: Length.mm },
        { value: 1.3, unit: Time.SECONDS },
      );

      expect(command).to.eq('move sin 80630 1300.0');
    });

    it('throws exception when command is not supported', () => {
      try {
        axis.prepareCommand('move cos ? ? ?',
          { value: 20, unit: Length.mm },
          { value: 1, unit: Time.SECONDS },
          { value: 3 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(NotSupportedException);
        expect((err as NotSupportedException).message).to.eq('Command with template "move cos ? ? ?" not supported.');
      }
    });

    it('throws exception when command is not complete', () => {
      try {
        axis.prepareCommand('move sin ?',
          { value: 20, unit: Length.mm },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(NotSupportedException);
        expect((err as NotSupportedException).message).to.eq('Command with template "move sin ?" not supported.');
      }
    });

    it('throws exception when invalid set command', () => {
      try {
        axis.prepareCommand('set scope.timebase maxspeed ?',
          { value: 20, unit: Length.mm },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(InvalidArgumentException);
        expect((err as InvalidArgumentException).message)
          .to.eq('Set command should be followed by a setting name and a value. Got [set scope.timebase maxspeed ?].');
      }
    });

    // this is not a valid command for this device, but it tests this branch in the library
    it('handles command with arity -1 on number argument', () => {
      const command = axis.prepareCommand('stream ? line abs ? ? ?',
        { value: 1 },
        { value: 10, unit: Length.mm },
        { value: 40, unit: Length.mm },
        { value: 20, unit: Length.mm },
      );
      expect(command).to.eq('stream 1 line abs 80630 322520 161260');
    });

    it('handles command with arity -1 on non number argument ', () => {
      const command = axis.prepareCommand('tools echo hello nice nice hello');
      expect(command).to.eq('tools echo hello nice nice hello');
    });

    it('handles command with no arguments', () => {
      const command = axis.prepareCommand('move max');
      expect(command).to.eq('move max');
    });

    it('handles command with numeric arguments in template', () => {
      const command = axis.prepareCommand('move sin ? 100 200',
        { value: 10.0, unit: Length.mm },
      );
      expect(command).to.eq('move sin 80630 100 200');
    });

    it('handles enum parameters', () => {
      const command = axis.prepareCommand('tools gotolimit home neg ? ?',
        { value: 1 },
        { value: 1 },
      );
      expect(command).to.eq('tools gotolimit home neg 1 1');
    });

    it('rejects invalid enum parameters', () => {
      try {
        axis.prepareCommand('tools gotolimit home neutral ? ?',
          { value: 1 },
          { value: 1 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(NotSupportedException);
        expect((err as NotSupportedException).message).to.eq('Command with template "tools gotolimit home neutral ? ?" not supported.');
      }
    });
  });

  describe('activate', () => {
    it('sends command correctly and removes axis identity', async () => {
      const promise = axis.activate();

      expect(await server.pop()).to.eq('/1 1 00 activate');
      await server.push('@01 1 00 OK IDLE FO 0');

      await promise;

      expect(() => axis.identity).to.throw(DeviceNotIdentifiedException);
      expect(device.isIdentified).to.eq(false);
    });
  });

  describe('toString', () => {
    it('returns representation for identified axis', () => {
      expect(axis.toString()).to.startWith('Axis 1 -> Device 1 SN: 1234 (X-LHM100A) -> Connection');
    });
  });
});
