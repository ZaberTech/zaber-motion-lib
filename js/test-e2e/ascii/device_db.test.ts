import { expect } from 'chai';
import path from 'path';
import rimraf from 'rimraf';
import fs from 'fs';
import bluebird from 'bluebird';

import { Library, DeviceDbSourceType, DeviceDbFailedException } from '../../src';
import { Connection, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { MASTER_DEVICE_DB } from '../constants';

beforeAll(() => {
  // disable the stores before running any tests (including in other files)
  Library.disableDeviceDbStore();
});

const { TEST_MASTER_DB_PATH } = process.env;

const server = new TestServer();

describe('DeviceDB', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
  });

  afterEach(async () => {
    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE);

    await connection.close();
    server.closeSocket();
  });

  describe('local file', () => {
    (TEST_MASTER_DB_PATH ? it : it.skip)('uses file when it is ordered to', async () => {
      Library.setDeviceDbSource(DeviceDbSourceType.FILE, TEST_MASTER_DB_PATH);

      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6Proto);

      expect(device.identity!.firmwareVersion).to.deep.eq({
        build: 0,
        major: 6,
        minor: 99,
      });
    }, 60 * 1000);
  });

  describe('master DB api', () => {
    it('uses different URL when it is ordered to', async () => {
      Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, MASTER_DEVICE_DB);

      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6Proto);

      expect(device.identity!.firmwareVersion).to.deep.eq({
        build: 0,
        major: 6,
        minor: 99,
      });
    });
  });

  describe('store', () => {
    const storeLocation = path.join(__dirname, '..', 'tmp');

    beforeEach(async () => {
      await bluebird.promisify<void, string>(rimraf)(storeLocation);
    });

    afterEach(() => {
      Library.disableDeviceDbStore();
    });

    it('uses store to save the identity data', async () => {
      Library.enableDeviceDbStore(storeLocation);

      await DeviceXLHM.identify(device, server);

      const files = await bluebird.promisify<string[], string>(fs.readdir)(storeLocation);
      expect(files).to.deep.eq(['cache1_api1.15_dev50081_fw6.28.1347.json']);
    });
  });

  it('fails with error if the database is not accessible', async () => {
    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'https://api.zaber.io/x');

    let promise = DeviceXLHM.identify(device, server);
    await expect(promise).to.be.rejectedWith(DeviceDbFailedException);
    let exception: DeviceDbFailedException = await promise.catch(e => e);
    expect(exception.message).to.startWith('Cannot access online source for the Device Database.');
    expect(exception.details).to.deep.eq({ code: 'no-access' });

    Library.setDeviceDbSource(DeviceDbSourceType.FILE, 'tmp/file.sql');

    promise = DeviceXLHM.identify(device, server);
    await expect(promise).to.be.rejectedWith(DeviceDbFailedException);
    exception = await promise.catch(e => e);
    expect(exception.message).to.startWith('Cannot access offline source for the Device Database');
    expect(exception.details).to.deep.eq({ code: 'no-access' });
  });
});
