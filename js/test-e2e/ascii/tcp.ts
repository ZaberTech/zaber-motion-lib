import { createServer, Server, Socket } from 'net';
import readline from 'readline';
import Bluebird from 'bluebird';

export const TEST_PORT = 22789 + Number(process.env.JEST_WORKER_ID) * 10;
export const TEST_HOST = '127.0.0.1';

export class ClosedError extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, ClosedError.prototype);
  }
}

export class TestServer {
  private tcpListener?: Server;
  private socket: Socket | null = null;
  private readline: readline.ReadLine | null = null;
  private lines: string[] = [];
  private _ignoreWaitingLinesOnClose = false;
  private waitOnLine: {
    resolve: (line: string) => void;
    reject: (err: unknown) => void;
  }[] = [];
  private waitOnConnection: (() => void) | null = null;

  public debug = false;

  constructor() {
    this.onLine = this.onLine.bind(this);
    this.handleSocket = this.handleSocket.bind(this);
  }

  public async start(port: number = TEST_PORT): Promise<void> {
    this.tcpListener = createServer(this.handleSocket);

    await new Promise<void>(resolve => this.tcpListener!.listen({
      host: TEST_HOST,
      port,
    }, resolve));
  }

  public async stop(): Promise<void> {
    const closePromise = Bluebird.fromCallback(cb => this.tcpListener!.close(cb));
    this.closeSocket();
    await closePromise;
  }

  public closeSocket(): void {
    if (!this.socket) {
      return;
    }
    this.socket.end();
    this.socket.destroy();
    this.socket = null;

    this.readline!.close();
    this.readline = null;

    const leftLines = this.lines;
    this.lines = [];
    this.waitOnLine.forEach(waiting => waiting.reject(new ClosedError('Socket closed')));
    this.waitOnLine = [];

    if (leftLines.length > 0 && !this._ignoreWaitingLinesOnClose) {
      throw new Error(`Unprocessed lines in the buffer: ${leftLines.join()}.`);
    }
  }

  public async waitForConnection(): Promise<void> {
    if (!this.socket) {
      if (this.waitOnConnection) {
        throw new Error('something else is already waiting');
      }
      await new Promise<void>(resolve => this.waitOnConnection = resolve);
    }
  }

  public async waitForClose(): Promise<void> {
    await new Promise(resolve => this.socket!.once('close', resolve));
  }

  public async pop(keepChecksum: boolean = false): Promise<string> {
    let line: string;
    if (this.lines.length) {
      line = this.lines.shift()!;
    } else {
      line = await new Promise<string>(
        (resolve, reject) => this.waitOnLine.push({ resolve, reject }));
    }

    if (keepChecksum) {
      return line;
    } else {
      return line.replace(/:.{2}$/, '');
    }
  }

  public async push(line: string): Promise<void> {
    if (this.debug) {
      // eslint-disable-next-line no-console
      console.log('TX:', line);
    }
    await Bluebird.fromCallback(cb => this.socket!.write(`${line}\n`, cb));
  }

  private handleSocket(socket: Socket): void {
    this.closeSocket();
    this.socket = socket;
    socket.setNoDelay(true);

    socket.setEncoding('utf8');
    this.readline = readline.createInterface({
      input: socket,
      output: socket,
    });
    this.readline.on('line', this.onLine);

    this._ignoreWaitingLinesOnClose = false;

    this.waitOnConnection?.();
    this.waitOnConnection = null;
  }

  private onLine(line: string): void {
    if (this.debug) {
      // eslint-disable-next-line no-console
      console.log('RX:', line);
    }
    const wait = this.waitOnLine.shift();
    if (wait) {
      wait.resolve(line);
    } else {
      this.lines.push(line);
    }
  }

  public ignoreWaitingLinesOnClose() {
    this._ignoreWaitingLinesOnClose = true;
  }

  public rejectAllWaiting() {
    this.waitOnLine.forEach(waiting => waiting.reject(new ClosedError('rejectAllWaiting called')));
    this.waitOnLine = [];
  }
}
