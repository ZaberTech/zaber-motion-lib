import { expect } from 'chai';

import { Connection, Device, IoPortType, TriggerAction, TriggerCondition, TriggerOperation, Triggers } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXJOY } from './devices/xjoy';
import {
  BadDataException, InvalidArgumentException, InvalidDataException, Length, NoValueForKeyException, NotSupportedException, Velocity
} from '../../src';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCC4 } from './devices/xmcc4';

const server = new TestServer();

describe('Triggers', () => {
  let connection: Connection;
  let device: Device;
  let triggers: Triggers;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    triggers = device.triggers;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('Triggers Properties', () => {
    it('will get correct device ID', () => {
      expect(triggers.device.deviceAddress).to.equal(1);
    });
  });

  describe('Triggers', () => {
    it('will get number of triggers on device', async () => {
      const promise = triggers.getNumberOfTriggers();

      expect(await server.pop()).to.eq('/1 0 00 get trigger.numtriggers');
      await server.push('@01 0 00 OK IDLE -- 6');

      const result = await promise;

      expect(result).to.eq(6);
    });

    it('will get number of triggers on device that does not support triggers', async () => {
      await DeviceXJOY.identify(device, server);

      const promise = triggers.getNumberOfTriggers();
      const result = await promise;
      expect(result).to.eq(0);
    });

    it('will get number of actions per trigger', async () => {
      const promise = triggers.getNumberOfActions();

      expect(await server.pop()).to.eq('/1 0 00 get trigger.numactions');
      await server.push('@01 0 00 OK IDLE -- 2');

      const result = await promise;

      expect(result).to.eq(2);
    });

    it('will get number of actions on device that does not support triggers', async () => {
      await DeviceXJOY.identify(device, server);

      const promise = triggers.getNumberOfActions();
      const result = await promise;
      expect(result).to.eq(0);
    });
  });

  describe('Get Trigger', () => {
    it('makes a trigger', async () => {
      const trigger = triggers.getTrigger(1);
      expect(trigger).to.have.property('triggerNumber');
      expect(trigger).to.have.property('device');
    });

    it('does not make a trigger with invalid number', async () => {
      expect(() => triggers.getTrigger(0)).to.throw(TypeError);
    });
  });

  describe('Get States of All Triggers', () => {
    it('will get state', async () => {
      const promise = triggers.getTriggerStates();

      expect(await server.pop()).to.eq('/1 0 00 trigger print');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 trigger 1');
      await server.push('#01 0 00 when none');
      await server.push('#01 0 00 action a none');
      await server.push('#01 0 00 action b none');
      await server.push('#01 0 00 disable');
      await server.push('#01 0 00 trigger 2');
      await server.push('#01 0 00 when time 42');
      await server.push('#01 0 00 action a home');
      await server.push('#01 0 00 action b move min');
      await server.push('#01 0 00 enable 12');
      await server.push('@01 0 00 OK IDLE -- 0');
      expect(await server.pop()).to.eq('/1 0 01 trigger show');
      await server.push('@01 0 01 OK IDLE -- d 11');

      const result = await promise;

      expect(result[0]).to.have.property('condition', 'when none');
      expect(result[0]).to.have.deep.property('actions', ['action a none', 'action b none']);
      expect(result[0]).to.have.property('enabled', false);
      expect(result[0]).to.have.property('firesTotal', 0);
      expect(result[0]).to.have.property('firesRemaining', 0);

      expect(result[1]).to.have.property('condition', 'when time 42');
      expect(result[1]).to.have.deep.property('actions', ['action a home', 'action b move min']);
      expect(result[1]).to.have.property('enabled', true);
      expect(result[1]).to.have.property('firesTotal', 12);
      expect(result[1]).to.have.property('firesRemaining', 11);
    });

    it('will get enabled state', async () => {
      const promise = triggers.getEnabledStates();

      expect(await server.pop()).to.eq('/1 0 00 trigger show');
      await server.push('@01 0 00 OK IDLE -- e d 24 d e d');

      const result = await promise;

      expect(result[0]).to.have.property('enabled', true);
      expect(result[0]).to.have.property('firesRemaining', -1);
      expect(result[1]).to.have.property('enabled', false);
      expect(result[1]).to.have.property('firesRemaining', 0);
      expect(result[2]).to.have.property('enabled', true);
      expect(result[2]).to.have.property('firesRemaining', 24);
      expect(result[3]).to.have.property('enabled', false);
      expect(result[3]).to.have.property('firesRemaining', 0);
      expect(result[4]).to.have.property('enabled', true);
      expect(result[4]).to.have.property('firesRemaining', -1);
      expect(result[5]).to.have.property('enabled', false);
      expect(result[5]).to.have.property('firesRemaining', 0);
    });
  });

  describe('Trigger Properties', () => {
    it('will get correct device ID', () => {
      const trigger = triggers.getTrigger(2);
      expect(trigger.device.deviceAddress).to.equal(1);
    });

    it('will get correct trigger number', () => {
      const trigger = triggers.getTrigger(2);
      expect(trigger.triggerNumber).to.equal(2);
    });
  });

  describe('Enable Trigger', () => {
    it('will enable trigger with unlimited count', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.enable();

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 enable');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('will enable trigger with specific count', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.enable(100);

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 enable 100');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('will return exception on device that does not support triggers', async () => {
      const trigger = triggers.getTrigger(2);
      const promise = trigger.enable();

      expect(await server.pop()).to.eq('/1 0 00 trigger 2 enable');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('Disable Trigger', () => {
    it('will disable trigger', async () => {
      const trigger = triggers.getTrigger(3);
      const promise = trigger.disable();

      expect(await server.pop()).to.eq('/1 0 00 trigger 3 disable');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Get State', () => {
    it('will get simple state', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.getState();

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 print');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 when none');
      await server.push('#01 0 00 action a none');
      await server.push('#01 0 00 action b none');
      await server.push('#01 0 00 disable');
      await server.push('@01 0 00 OK IDLE -- 0');
      expect(await server.pop()).to.eq('/1 0 01 trigger show');
      await server.push('@01 0 01 OK IDLE -- d d d d d d');

      const result = await promise;

      expect(result).to.have.property('condition', 'when none');
      expect(result).to.have.deep.property('actions', ['action a none', 'action b none']);
      expect(result).to.have.property('enabled', false);
      expect(result).to.have.property('firesTotal', 0);
      expect(result).to.have.property('firesRemaining', 0);
    });

    it('will get more complex state', async () => {
      const trigger = triggers.getTrigger(2);
      const promise = trigger.getState();

      expect(await server.pop()).to.eq('/1 0 00 trigger 2 print');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 when encoder dist 1000');
      await server.push('#01 0 00 action a home');
      await server.push('#01 0 00 action b move max');
      await server.push('#01 0 00 enable 100');
      await server.push('@01 0 00 OK IDLE -- 0');
      expect(await server.pop()).to.eq('/1 0 01 trigger show');
      await server.push('@01 0 01 OK IDLE -- d 22 d d d d');

      const result = await promise;

      expect(result).to.have.property('condition', 'when encoder dist 1000');
      expect(result).to.have.deep.property('actions', ['action a home', 'action b move max']);
      expect(result).to.have.property('enabled', true);
      expect(result).to.have.property('firesTotal', 100);
      expect(result).to.have.property('firesRemaining', 22);
    });

    it('will return exception on device that does not support triggers', async () => {
      const trigger = triggers.getTrigger(2);
      const promise = trigger.getState();

      expect(await server.pop()).to.eq('/1 0 00 trigger 2 print');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('Get Enabled State', () => {
    it('will get enabled', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.getEnabledState();

      expect(await server.pop()).to.eq('/1 0 00 trigger show');
      await server.push('@01 0 00 OK IDLE -- e d d d d d');

      const result = await promise;

      expect(result).to.have.property('enabled', true);
      expect(result).to.have.property('firesRemaining', -1);
    });

    it('will get disabled', async () => {
      const trigger = triggers.getTrigger(2);
      const promise = trigger.getEnabledState();

      expect(await server.pop()).to.eq('/1 0 00 trigger show');
      await server.push('@01 0 00 OK IDLE -- e d d d d d');

      const result = await promise;

      expect(result).to.have.property('enabled', false);
      expect(result).to.have.property('firesRemaining', 0);
    });

    it('will get enabled for specific count', async () => {
      const trigger = triggers.getTrigger(3);
      const promise = trigger.getEnabledState();

      expect(await server.pop()).to.eq('/1 0 00 trigger show');
      await server.push('@01 0 00 OK IDLE -- e d 25 d d d');

      const result = await promise;

      expect(result).to.have.property('enabled', true);
      expect(result).to.have.property('firesRemaining', 25);
    });

    it('will return exception on device that does not support triggers', async () => {
      const trigger = triggers.getTrigger(4);
      const promise = trigger.getEnabledState();

      expect(await server.pop()).to.eq('/1 0 00 trigger show');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('Fire When', () => {
    it('is valid condition', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhen('1 encoder dist 42');

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 when 1 encoder dist 42');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('is invalid condition', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhen('1 encoder 42');

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 when 1 encoder 42');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });

    it('is invalid argument', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhen('1 encoder dist -1');

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 when 1 encoder dist -1');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(BadDataException);
    });

    it('will return exception on device that does not support triggers', async () => {
      const trigger = triggers.getTrigger(4);
      const promise = trigger.fireWhen('time 1000');

      expect(await server.pop()).to.eq('/1 0 00 trigger 4 when time 1000');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('Fire When Encoder Dist', () => {
    it('is valid argument', async () => {
      const trigger = triggers.getTrigger(2);
      const promise = trigger.fireWhenEncoderDistanceTravelled(1, 2000);

      expect(await server.pop()).to.eq('/1 0 00 trigger 2 when 1 encoder dist 2000');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('is invalid axis', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(2);
      const promise = trigger.fireWhenEncoderDistanceTravelled(2, 2000);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is invalid argument', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenEncoderDistanceTravelled(1, -1);

      await expect(promise).to.be.rejectedWith(TypeError);
    });
  });

  describe('Fire When Dist', () => {
    it('is valid argument', async () => {
      const trigger = triggers.getTrigger(5);
      const promise = trigger.fireWhenDistanceTravelled(1, 42);

      expect(await server.pop()).to.eq('/1 0 00 trigger 5 when 1 dist 42');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Fire When Io', () => {
    it('is valid condition', async () => {
      const trigger = triggers.getTrigger(6);
      const promise = trigger.fireWhenIo(IoPortType.ANALOG_INPUT, 1, TriggerCondition.EQ, 0);

      expect(await server.pop()).to.eq('/1 0 00 trigger 6 when io ai 1 == 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('will return exception for devices that do not support io conditions', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenIo(IoPortType.ANALOG_INPUT, 1, TriggerCondition.EQ, 0);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 when io ai 1 == 0');
      await server.push('@01 0 11 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('Fire When Setting', () => {
    it('is valid condition', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenSetting(1, 'pos', TriggerCondition.GT, 1000);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 when 1 pos > 1000');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('is valid with unit conversion', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenSetting(1, 'maxspeed', TriggerCondition.GT, 10, Velocity.MILLIMETRES_PER_SECOND);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 when 1 maxspeed > 132104');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('is invalid setting', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenSetting(1, 'position', TriggerCondition.GT, 1000);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      await expect(promise).to.be.rejectedWith('The following setting is invalid for axis 1: \'position\'');
    });

    it('is device-scope setting with axis specified', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenSetting(1, 'comm.alert', TriggerCondition.GT, 1000);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      await expect(promise)
        .to.be.rejectedWith('The following is a device-scope setting: \'comm.alert\'. To use this setting, please specify an axis of 0.');
    });

    it('is axis-scope setting with no axis specified', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenSetting(0, 'pos', TriggerCondition.GT, 1000);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      await expect(promise)
        .to.be.rejectedWith('The following is an axis-scope setting: \'pos\'. To use this setting, please specify an axis > 0.');
    });

    it('is invalid axis', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(2);
      const promise = trigger.fireWhenSetting(2, 'maxspeed', TriggerCondition.GT, 10, Velocity.MILLIMETRES_PER_SECOND);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });
  });

  describe('Fire When Absolute Setting', () => {
    it('is valid condition', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenAbsoluteSetting(1, 'pos', TriggerCondition.GT, 1000);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 when 1 abs pos > 1000');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('is valid condition with unit conversion', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireWhenAbsoluteSetting(1, 'pos', TriggerCondition.GT, 12, Length.MILLIMETRES);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 when 1 abs pos > 96756');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Fire At Interval', () => {
    it('is valid condition', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.fireAtInterval(42);

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 when time 42');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('will return exception on device that does not support triggers', async () => {
      const trigger = triggers.getTrigger(4);
      const promise = trigger.fireAtInterval(1);

      expect(await server.pop()).to.eq('/1 0 00 trigger 4 when time 1');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('On Fire', () => {
    it('is valid command', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFire(TriggerAction.A, 1, 'move max');

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 action a 1 move max');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('is invalid command', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFire(TriggerAction.A, 1, 'moves max');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
    });
  });

  describe('On Fire Set', () => {
    it('is valid', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 1, 'maxspeed', TriggerOperation.INCREMENT_BY, 100);

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 action a 1 maxspeed += 100');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('is valid with unit conversion', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 1, 'maxspeed', TriggerOperation.INCREMENT_BY, 10, Velocity.MILLIMETRES_PER_SECOND);

      expect(await server.pop()).to.eq('/1 0 11 trigger 1 action a 1 maxspeed += 132104');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('is invalid setting', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 1, 'maxspeeds', TriggerOperation.INCREMENT_BY, 100);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is invalid axis', async () => {
      await DeviceXLHM.identify(device, server);
      const trigger = triggers.getTrigger(2);
      const promise = trigger.onFireSet(TriggerAction.A, 2, 'maxspeed', TriggerOperation.INCREMENT_BY, 100);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('sets all axes for axis-scope setting', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 0, 'accel', TriggerOperation.SET_TO, 100);

      expect(await server.pop()).to.eq('/1 0 25 trigger 1 action a 0 accel = 100');
      await server.push('@01 0 25 OK IDLE -- 0');

      await promise;
    });

    it('fails to set all axes for axis-scope setting due to unit conversion', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 0, 'accel', TriggerOperation.SET_TO, 10, Velocity.MILLIMETRES_PER_SECOND);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is device-scope setting with axis specified', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSet(TriggerAction.A, 1, 'comm.alert', TriggerOperation.SET_TO, 1);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      await expect(promise)
        .to.be.rejectedWith('The following is a device-scope setting: \'comm.alert\'. To use this setting, please specify an axis of 0.');
    });
  });

  describe('On Fire Set to Setting', () => {
    it('is valid', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.B, 1, 'maxspeed', TriggerOperation.SET_TO, 1, 'accel');

      expect(await server.pop()).to.eq('/1 0 00 trigger 1 action b 1 maxspeed = setting 1 accel');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('is invalid target setting', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.A, 0, 'maxspeeds', TriggerOperation.SET_TO, 1, 'accel');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is device-scope target setting with axis specified', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.A, 1, 'user.data.0', TriggerOperation.SET_TO, 1, 'accel');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is invalid source setting', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.A, 1, 'maxspeed', TriggerOperation.SET_TO, 0, 'accels');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('is device-scope source setting with axis specified', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.A, 1, 'accel', TriggerOperation.SET_TO, 1, 'user.data.1');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('targets all axes for axis-scope setting', async () => {
      await DeviceXMCC4.identify(device, server);
      const trigger = triggers.getTrigger(1);
      const promise = trigger.onFireSetToSetting(TriggerAction.A, 0, 'accel', TriggerOperation.SET_TO, 0, 'accel');

      expect(await server.pop()).to.eq('/1 0 25 trigger 1 action a 0 accel = setting 0 accel');
      await server.push('@01 0 25 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Clear Action', () => {
    it('clears a', async () => {
      const trigger = triggers.getTrigger(3);
      const promise = trigger.clearAction(TriggerAction.A);

      expect(await server.pop()).to.eq('/1 0 00 trigger 3 action a none');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('clears all', async () => {
      const trigger = triggers.getTrigger(4);
      const promise = trigger.clearAction(TriggerAction.ALL);

      expect(await server.pop()).to.eq('/1 0 00 trigger 4 action none');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Labels', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server);
    });

    it('sets label for a trigger', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.setLabel('Camera');

      expect(await server.pop()).to.eq('/1 0 11 storage set zaber.triggers.label.1 Q2FtZXJh');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('erases label for a trigger with empty string', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.setLabel('');

      expect(await server.pop()).to.eq('/1 0 11 storage erase zaber.triggers.label.1');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('erases label for a trigger with no arguments', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.setLabel();

      expect(await server.pop()).to.eq('/1 0 11 storage erase zaber.triggers.label.1');
      await server.push('@01 0 11 OK IDLE -- 0');

      await promise;
    });

    it('gets label for a trigger', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.getLabel();

      expect(await server.pop()).to.eq('/1 0 11 storage get zaber.triggers.label.1');
      await server.push('@01 0 11 OK IDLE -- Q2FtZXJh');

      expect(await promise).to.eq('Camera');
    });

    it('gets unencoded label for a trigger', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.getLabel();

      expect(await server.pop()).to.eq('/1 0 11 storage get zaber.triggers.label.1');
      await server.push('@01 0 11 OK IDLE -- Camera');

      await expect(promise).to.be.rejectedWith(InvalidDataException);
    });

    it('returns exception when getting missing label for a trigger', async () => {
      const trigger = triggers.getTrigger(1);
      const promise = trigger.getLabel();

      expect(await server.pop()).to.eq('/1 0 11 storage get zaber.triggers.label.1');
      await server.push('@01 0 11 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(NoValueForKeyException);
    });

    it('gets labels for all triggers', async () => {
      const promise = triggers.getAllLabels();

      expect(await server.pop()).to.eq('/1 0 11 get trigger.numtriggers');
      await server.push('@01 0 11 OK IDLE -- 6');
      expect(await server.pop()).to.eq('/1 0 12 storage print prefix zaber.triggers.label.');
      expect(await server.pop()).to.eq('/1 0 12');
      await server.push('@01 0 12 OK IDLE -- 0');
      await server.push('#01 0 12 set zaber.triggers.label.1 TXkgVHJpZ2dlcg==');
      await server.push('#01 0 12 set zaber.triggers.label.3 Q2FtZXJh');
      await server.push('@01 0 12 OK IDLE -- 0');

      expect(await promise).to.deep.eq(['My Trigger', '', 'Camera', '', '', '']);
    });

    it('gets labels for all triggers with all labels missing', async () => {
      const promise = triggers.getAllLabels();

      expect(await server.pop()).to.eq('/1 0 11 get trigger.numtriggers');
      await server.push('@01 0 11 OK IDLE -- 6');
      expect(await server.pop()).to.eq('/1 0 12 storage print prefix zaber.triggers.label.');
      expect(await server.pop()).to.eq('/1 0 12');
      await server.push('@01 0 12 OK IDLE -- 0');
      await server.push('@01 0 12 OK IDLE -- 0');

      expect(await promise).to.deep.eq(['', '', '', '', '', '']);
    });

    it('gets labels for all triggers with some labels not encoded', async () => {
      const promise = triggers.getAllLabels();

      expect(await server.pop()).to.eq('/1 0 11 get trigger.numtriggers');
      await server.push('@01 0 11 OK IDLE -- 6');
      expect(await server.pop()).to.eq('/1 0 12 storage print prefix zaber.triggers.label.');
      expect(await server.pop()).to.eq('/1 0 12');
      await server.push('@01 0 12 OK IDLE -- 0');
      await server.push('#01 0 12 set zaber.triggers.label.1 Camera');
      await server.push('@01 0 12 OK IDLE -- 0');

      await expect(promise).to.be.rejectedWith(InvalidDataException);
    });
  });
});
