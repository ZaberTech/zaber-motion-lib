import { expect } from 'chai';

import { Connection, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceDbFailedException } from '../../src';
import { ServoTuner } from '../../src/ascii/servo_tuner';
import { ServoTuningParamset } from '../../src/ascii/servo_tuning_paramset';
import { parseCommand } from './protocol';
import { ParamsetInfo } from '../../src/ascii/paramset_info';
import { ServoTuningParam } from '../../src/ascii/servo_tuning_param';
import { DeviceXMCC4 } from './devices/xmcc4';

import advancedTuning from './tunings/advanced-tuning.json';
import simpleTuning684 from './tunings/simple-tuning-684.json';
import expectedParams from './tunings/expected-params.json';

const server = new TestServer();

async function getTuning(server: TestServer, info: ParamsetInfo, paramset = 1) {
  const getAllCmd = parseCommand(await server.pop());
  expect(getAllCmd.data).to.eq(`servo ${paramset} get all`);
  const pre = `${getAllCmd.device} ${getAllCmd.axis} ${getAllCmd.id}`;
  // catch the empty command with the same id
  expect(await server.pop()).to.eq(`/${pre}`);

  await server.push(`@${pre} OK IDLE WR 0`);
  await server.push(`#${pre} type ${info.type}`);
  for (const { name, value } of info.params) {
    await server.push(`#${pre} parameter ${name} ${value}`);
  }
  await server.push(`@${pre} OK IDLE WR 0`);
}

async function expectTuning(server: TestServer, params: ServoTuningParam[]) {
  const paramCount = params.length;
  for (let i = 0; i < paramCount; i++) {
    const setCmd = parseCommand(await server.pop());
    const tokens = setCmd.data.split(' ');
    expect(tokens[0]).to.equal('servo');
    expect(tokens[1]).to.equal('1');
    expect(tokens[2]).to.equal('set');
    const paramName = tokens[3];
    const paramPair = params.find(p => p.name === paramName);
    expect(paramPair).to.not.be.null;
    expect(parseFloat(tokens[4])).to.eq(paramPair?.value);
    await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE WR 0`);
  }
}

const DEFAULT_PARAM_VALUE = 1.234979;
async function getDefaultParams(server: TestServer) {
  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('servo default get all');
  const pre = `${cmd.device} ${cmd.axis} ${cmd.id}`;
  // catch the empty command with the same id
  expect(await server.pop()).to.eq(`/${pre}`);

  await server.push(`@${pre} OK IDLE WR 0`);
  await server.push(`#${pre} type ffpid`);
  for (const param of expectedParams) {
    await server.push(`#${pre} parameter ${param} ${DEFAULT_PARAM_VALUE}`);
  }

  // respond to the empty command
  await server.push(`@${pre} OK IDLE WR 0`);
}

async function getExpectedParams(server: TestServer, paramset = 1) {
  const getTypeCmd = parseCommand(await server.pop());
  expect(getTypeCmd.data).to.equal(`servo ${paramset} get type`);
  await server.push(`@${getTypeCmd.device} ${getTypeCmd.axis} ${getTypeCmd.id} OK IDLE WR ffpid`);
  const printParamsCmd = parseCommand(await server.pop());
  expect(printParamsCmd.data).to.equal('servo print params ffpid');
  const pre = `${printParamsCmd.device} ${printParamsCmd.axis} ${printParamsCmd.id}`;
  // catch the empty command with the same id
  expect(await server.pop()).to.eq(`/${pre}`);

  await server.push(`@${pre} OK IDLE WR ffpid`);
  for (const param of expectedParams) {
    await server.push(`#${pre} ffpid ${param}`);
  }

  // respond to the empty command
  await server.push(`@${pre} OK IDLE WR 0`);
}

async function setTuning(server: TestServer): Promise<ServoTuningParam[]> {
  const params: ServoTuningParam[] = [];
  for (let i = 0; i < expectedParams.length; i++) {
    const setCmd = parseCommand(await server.pop());
    const tokens = setCmd.data.split(' ');
    expect(tokens[0]).to.equal('servo');
    expect(tokens[1]).to.equal('1');
    expect(tokens[2]).to.equal('set');
    params.push({ name: tokens[3], value: parseFloat(tokens[4]) });
    await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE WR 0`);
  }
  return params;
}

function expectedParamsToMatchOrBeDefault(received: ServoTuningParam[], expected: ServoTuningParam[]) {
  for (const receivedParam of received) {
    const expectedParam = expected.find(p => p.name === receivedParam.name);
    if (expectedParam != null) {
      expect(receivedParam.value).to.equal(receivedParam.value);
    } else {
      expect(receivedParam.value).to.equal(DEFAULT_PARAM_VALUE);
    }
  }
}

describe('Servo Tuning', () => {
  let connection: Connection;
  let device: Device;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    // axis = device.getAxis(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('Advanced tuning', () => {
    it('Cannot get tuning of non-tunable device', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningPromise = servoTuning.getTuning(ServoTuningParamset.P_1);

      await expect(tuningPromise).to.be.rejectedWith(
        DeviceDbFailedException, 'Product with product ID 1641095 does not exist or does not support servo tuning'
      );
    });

    it('Can get tuning', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningPromise = servoTuning.getTuning(ServoTuningParamset.P_1);
      await getTuning(server, advancedTuning);
      const tuningReceived = await tuningPromise;
      expect(tuningReceived).to.deep.equal(advancedTuning);
    });

    it('Can set tuning', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const params: ServoTuningParam[] = [
        { name: 'kd', value: 7430.8 },
        { name: 'kd.hs', value: 7386 },
      ];
      const tuningPromise = servoTuning.setTuning(ServoTuningParamset.P_1, params);
      await expectTuning(server, params);
      await tuningPromise;
    });
  });

  describe('PID tuning', () => {
    it('Will not PID tune devices that cannot be tuned', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningPromise = servoTuning.getPidTuning(ServoTuningParamset.P_1);

      await expect(tuningPromise).to.be.rejectedWith(
        DeviceDbFailedException, 'Product with product ID 1641095 does not exist or does not support servo tuning'
      );
    });

    it('Can get PID tuning', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningPromise = servoTuning.getPidTuning(ServoTuningParamset.P_1);
      await getTuning(server, advancedTuning);
      const pid = await tuningPromise;
      expect(pid.p).to.be.closeTo(67029.03590894632, 25);
      expect(pid.i).to.be.closeTo(5891421.9132792, 250);
      expect(pid.d).to.be.closeTo(646.4687647235994, 2.5);
      expect(pid.fc).to.be.closeTo(549.294178304, 2.5);
    });

    it('Can set PID tuning', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const P = 60000;
      const I = 5000000;
      const D = 600;
      const FC = 500;
      const tuningPromise = servoTuning.setPidTuning(ServoTuningParamset.P_1, P, I, D, FC);
      await getDefaultParams(server);
      const rawParamsSet = await setTuning(server);
      await getTuning(server, { type: 'ffpid', version: 4, params: rawParamsSet });
      const pid = await tuningPromise;
      expect(pid.p).to.be.closeTo(P, 10);
      expect(pid.i).to.be.closeTo(I, 100);
      expect(pid.d).to.be.closeTo(D, 1);
      expect(pid.fc).to.be.closeTo(FC, 1);
    });
  });

  describe('Simple tuning', () => {
    it('Will not simple tune devices that cannot be tuned', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningPromise = servoTuning.getSimpleTuningParamDefinitions();

      await expect(tuningPromise).to.be.rejectedWith(
        DeviceDbFailedException, 'Product with product ID 1641095 does not exist or does not support servo tuning'
      );
    });

    it('Can get simple tuning params', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningParamsPromise = servoTuning.getSimpleTuningParamDefinitions();
      const params = await tuningParamsPromise;
      expect(params).to.deep.equal([{
        name: 'Stiffness', minLabel: 'Smooth', maxLabel: 'Stiff', dataType: 'Interpolated', defaultValue: null,
      }]);
    });

    it('Can set simple tuning on pre-storage device', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuning: ServoTuningParam[] = [{ name: 'Stiffness', value: 0.684 }];
      const tuningPromise = servoTuning.setSimpleTuning(ServoTuningParamset.P_1, tuning, 0, { carriageMass: 2.29 });
      await getExpectedParams(server);
      await getDefaultParams(server);
      const rawParamsSet = await setTuning(server);
      await tuningPromise;
      expectedParamsToMatchOrBeDefault(rawParamsSet, simpleTuning684);
    });

    it('Can set simple tuning on device with storage', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_30);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuning: ServoTuningParam[] = [{ name: 'Stiffness', value: 0.684 }];
      const tuningPromise = servoTuning.setSimpleTuning(ServoTuningParamset.P_1, tuning, 0, { carriageMass: 2.29 });
      await getExpectedParams(server);
      await getDefaultParams(server);
      const rawParamsSet = await setTuning(server);

      const storeStiffnessKey = parseCommand(await server.pop());
      expect(storeStiffnessKey.data).to.equal('storage axis set zaber.servo-tune.1.stiffness\\');
      const storeStiffnessVal = parseCommand(await server.pop());
      expect(storeStiffnessVal.data).to.equal('cont 1 0.6840000000000000524025267623073887079954147');
      await server.push(`@${storeStiffnessKey.device} ${storeStiffnessKey.axis} ${storeStiffnessKey.id} OK IDLE -- 0`);

      const storeLoadMass = parseCommand(await server.pop());
      expect(storeLoadMass.data).to.equal('storage axis set zaber.servo-tune.1.load-mass 0');
      await server.push(`@${storeLoadMass.device} ${storeLoadMass.axis} ${storeLoadMass.id} OK IDLE -- 0`);

      const storeCarMassKey = parseCommand(await server.pop());
      expect(storeCarMassKey.data).to.equal('storage axis set zaber.servo-tune.1.car-mass\\');
      const storeCarMassVal = parseCommand(await server.pop());
      expect(storeCarMassVal.data).to.equal('cont 1 2.290000000000000035527136788005009293556213');
      await server.push(`@${storeCarMassKey.device} ${storeCarMassKey.axis} ${storeCarMassKey.id} OK IDLE -- 0`);

      await tuningPromise;
      expectedParamsToMatchOrBeDefault(rawParamsSet, simpleTuning684);
    });

    it('Can get simple tuning', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_30);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningParamsPromise = servoTuning.getSimpleTuning(ServoTuningParamset.P_1);

      const getLoadMass = parseCommand(await server.pop());
      expect(getLoadMass.data).to.equal('storage axis get zaber.servo-tune.1.load-mass');
      await server.push(`@${getLoadMass.device} ${getLoadMass.axis} ${getLoadMass.id} OK IDLE -- 0`);

      const getCarMass = parseCommand(await server.pop());
      expect(getCarMass.data).to.equal('storage axis get zaber.servo-tune.1.car-mass');
      await server.push(`@${getCarMass.device} ${getCarMass.axis} ${getCarMass.id} OK IDLE -- 2.29`);

      const getStiffness = parseCommand(await server.pop());
      expect(getStiffness.data).to.equal('storage axis get zaber.servo-tune.1.stiffness');
      await server.push(`@${getStiffness.device} ${getStiffness.axis} ${getStiffness.id} OK IDLE -- 0.684`);

      await getExpectedParams(server);
      await getDefaultParams(server);
      await getTuning(server, { type: 'ffpid', version: 4, params: simpleTuning684 });

      expect(await tuningParamsPromise).to.deep.equal({
        isUsed: true, carriageMass: 2.29, loadMass: 0, tuningParams: [{ name: 'Stiffness', value: 0.684 }]
      });
    });

    it('Returns a default tuning when none is saved', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_30);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningParamsPromise = servoTuning.getSimpleTuning(ServoTuningParamset.P_1);

      const getLoadMass = parseCommand(await server.pop());
      expect(getLoadMass.data).to.equal('storage axis get zaber.servo-tune.1.load-mass');
      await server.push(`@${getLoadMass.device} ${getLoadMass.axis} ${getLoadMass.id} RJ IDLE -- BADDATA`);

      expect(await tuningParamsPromise).to.deep.equal({
        isUsed: false, carriageMass: null, loadMass: 0, tuningParams: [{ name: 'Stiffness', value: 0.5 }]
      });
    });

    it('Returns undefined carriage mass when none is saved', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_30);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningParamsPromise = servoTuning.getSimpleTuning(ServoTuningParamset.P_1);

      const getLoadMass = parseCommand(await server.pop());
      expect(getLoadMass.data).to.equal('storage axis get zaber.servo-tune.1.load-mass');
      await server.push(`@${getLoadMass.device} ${getLoadMass.axis} ${getLoadMass.id} OK IDLE -- 4.1`);

      const getCarMass = parseCommand(await server.pop());
      expect(getCarMass.data).to.equal('storage axis get zaber.servo-tune.1.car-mass');
      await server.push(`@${getCarMass.device} ${getCarMass.axis} ${getCarMass.id} RJ IDLE -- BADDATA`);

      const getStiffness = parseCommand(await server.pop());
      expect(getStiffness.data).to.equal('storage axis get zaber.servo-tune.1.stiffness');
      await server.push(`@${getStiffness.device} ${getStiffness.axis} ${getStiffness.id} OK IDLE -- 0.9`);

      await getExpectedParams(server);
      await getDefaultParams(server);
      await getTuning(server, { type: 'ffpid', version: 4, params: simpleTuning684 });

      expect(await tuningParamsPromise).to.deep.equal({
        isUsed: false, carriageMass: null, loadMass: 4.1, tuningParams: [{ name: 'Stiffness', value: 0.9 }]
      });
    });

    it('Can detect when simple tuning is not in use anymore', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_30);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuningParamsPromise = servoTuning.getSimpleTuning(ServoTuningParamset.P_1);

      const getLoadMass = parseCommand(await server.pop());
      expect(getLoadMass.data).to.equal('storage axis get zaber.servo-tune.1.load-mass');
      await server.push(`@${getLoadMass.device} ${getLoadMass.axis} ${getLoadMass.id} OK IDLE -- 4.5`);

      const getCarMass = parseCommand(await server.pop());
      expect(getCarMass.data).to.equal('storage axis get zaber.servo-tune.1.car-mass');
      await server.push(`@${getCarMass.device} ${getCarMass.axis} ${getCarMass.id} OK IDLE -- 2.66`);

      const getStiffness = parseCommand(await server.pop());
      expect(getStiffness.data).to.equal('storage axis get zaber.servo-tune.1.stiffness');
      await server.push(`@${getStiffness.device} ${getStiffness.axis} ${getStiffness.id} OK IDLE -- 0.321`);

      await getExpectedParams(server);
      await getDefaultParams(server);
      await getTuning(server, advancedTuning);

      expect(await tuningParamsPromise).to.deep.equal({
        isUsed: false, carriageMass: 2.66, loadMass: 4.5, tuningParams: [{ name: 'Stiffness', value: 0.321 }]
      });
    });

    it('Simple tuning works with default carriage mass', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuning: ServoTuningParam[] = [{ name: 'Stiffness', value: 0.684 }];
      const tuningPromise = servoTuning.setSimpleTuning(ServoTuningParamset.P_1, tuning, 0);
      await getExpectedParams(server);
      await getDefaultParams(server);
      const rawParamsSet = await setTuning(server);
      await tuningPromise;
      expectedParamsToMatchOrBeDefault(rawParamsSet, simpleTuning684);
    });

    it('Can check that simple tuning is loaded to paramset', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuning: ServoTuningParam[] = [{ name: 'Stiffness', value: 0.684 }];
      const usingTuningPromise = servoTuning.isUsingSimpleTuning(ServoTuningParamset.P_1, tuning, 0, { carriageMass: 2.29 });
      await getExpectedParams(server);
      await getDefaultParams(server);
      await getTuning(server, { type: 'ffpid', version: 4, params: simpleTuning684 });
      expect(await usingTuningPromise).to.be.true;
    });

    it('Can see that the default is NOT the values tuned', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const tuning: ServoTuningParam[] = [{ name: 'Stiffness', value: 0.684 }];
      const usingTuningPromise = servoTuning.isUsingSimpleTuning(ServoTuningParamset.P_1, tuning, 0, { carriageMass: 2.29 });
      await getExpectedParams(server);
      await getDefaultParams(server);
      await getTuning(server, advancedTuning);
      expect(await usingTuningPromise).to.be.false;
    });
  });

  describe('Utility functions', () => {
    it('Can get startup paramset', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const startupPromise = servoTuning.getStartupParamset();

      const getTypeCmd = parseCommand(await server.pop());
      expect(getTypeCmd.data).to.equal('servo get startup');
      await server.push(`@${getTypeCmd.device} ${getTypeCmd.axis} ${getTypeCmd.id} OK IDLE WR 1`);

      const startup = await startupPromise;
      expect(startup).to.equal(ServoTuningParamset.P_1);
    });

    it('Can set startup paramset', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const setStartupPromise = servoTuning.setStartupParamset(ServoTuningParamset.P_1);

      const getTypeCmd = parseCommand(await server.pop());
      expect(getTypeCmd.data).to.equal('servo set startup 1');
      await server.push(`@${getTypeCmd.device} ${getTypeCmd.axis} ${getTypeCmd.id} OK IDLE WR 0`);

      await setStartupPromise;
    });

    it('Can load paramset', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const loadSetPromise = servoTuning.loadParamset(ServoTuningParamset.P_1, ServoTuningParamset.P_2);

      const getTypeCmd = parseCommand(await server.pop());
      expect(getTypeCmd.data).to.equal('servo 1 load 2');
      await server.push(`@${getTypeCmd.device} ${getTypeCmd.axis} ${getTypeCmd.id} OK IDLE WR 0`);

      await loadSetPromise;
    });

    it('Copies stored simple tuning info when loaded from a simple-tuned paramset', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_26);

      const servoTuning = new ServoTuner(device.getAxis(1));
      const loadSetPromise = servoTuning.loadParamset(ServoTuningParamset.P_1, ServoTuningParamset.P_2);

      const getTypeCmd = parseCommand(await server.pop());
      expect(getTypeCmd.data).to.equal('servo 1 load 2');
      await server.push(`@${getTypeCmd.device} ${getTypeCmd.axis} ${getTypeCmd.id} OK IDLE WR 0`);

      const getLoadMass = parseCommand(await server.pop());
      expect(getLoadMass.data).to.equal('storage axis get zaber.servo-tune.2.load-mass');
      await server.push(`@${getLoadMass.device} ${getLoadMass.axis} ${getLoadMass.id} OK IDLE -- 0`);

      const getCarMass = parseCommand(await server.pop());
      expect(getCarMass.data).to.equal('storage axis get zaber.servo-tune.2.car-mass');
      await server.push(`@${getCarMass.device} ${getCarMass.axis} ${getCarMass.id} OK IDLE -- 2.29`);

      const getStiffness = parseCommand(await server.pop());
      expect(getStiffness.data).to.equal('storage axis get zaber.servo-tune.2.stiffness');
      await server.push(`@${getStiffness.device} ${getStiffness.axis} ${getStiffness.id} OK IDLE -- 0.684`);

      await getExpectedParams(server, 2);
      await getDefaultParams(server);
      await getTuning(server, { type: 'ffpid', version: 4, params: simpleTuning684 }, 2);

      const storeStiffnessKey = parseCommand(await server.pop());
      expect(storeStiffnessKey.data).to.equal('storage axis set zaber.servo-tune.1.stiffness\\');
      const storeStiffnessVal = parseCommand(await server.pop());
      expect(storeStiffnessVal.data).to.equal('cont 1 0.6840000000000000524025267623073887079954147');
      await server.push(`@${storeStiffnessKey.device} ${storeStiffnessKey.axis} ${storeStiffnessKey.id} OK IDLE -- 0`);

      const storeLoadMass = parseCommand(await server.pop());
      expect(storeLoadMass.data).to.equal('storage axis set zaber.servo-tune.1.load-mass 0');
      await server.push(`@${storeLoadMass.device} ${storeLoadMass.axis} ${storeLoadMass.id} OK IDLE -- 0`);

      const storeCarMassKey = parseCommand(await server.pop());
      expect(storeCarMassKey.data).to.equal('storage axis set zaber.servo-tune.1.car-mass\\');
      const storeCarMassVal = parseCommand(await server.pop());
      expect(storeCarMassVal.data).to.equal('cont 1 2.290000000000000035527136788005009293556213');
      await server.push(`@${storeCarMassKey.device} ${storeCarMassKey.axis} ${storeCarMassKey.id} OK IDLE -- 0`);

      await loadSetPromise;
    });
  });
});
