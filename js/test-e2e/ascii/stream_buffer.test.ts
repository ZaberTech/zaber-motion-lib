import { expect } from 'chai';
import { Connection, Device, PvtBuffer, StreamBuffer } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { parseLines } from './protocol';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';

describe('Stream/PVT buffer', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let eqAndPushX: (pushStr: string, popStr: string) => Promise<unknown>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('Stream buffer', () => {
    let buffer: StreamBuffer;
    beforeEach(() => {
      buffer = device.streams.getBuffer(1);
    });

    describe('getContent', () => {
      it('returns a string of the buffer', async () => {
        const promise = buffer.getContent();
        const line = await server.pop();
        const cmd = parseLines([line])[0];
        expect(cmd.strNoId).to.equal('/1 0 id stream buffer 1 print');

        const lineTerminate = await server.pop();
        const cmdTerminate = parseLines([lineTerminate])[0];
        expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

        await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
        await server.push(`#01 0 ${cmd.idStr} setup store 1 1`);
        await server.push(`#01 0 ${cmd.idStr} line abs 1000`);
        await server.push(`#01 0 ${cmd.idStr} wait 1000`);
        await server.push(`#01 0 ${cmd.idStr} line abs 20000`);
        await server.push(`#01 0 ${cmd.idStr} setup disable`);
        await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

        const result = await promise;
        expect(result).to.deep.eq([
          'setup store 1 1',
          'line abs 1000',
          'wait 1000',
          'line abs 20000',
          'setup disable',
        ]);
      });

      it('empty array when buffer is unused (BADDATA)', async () => {
        const promise = buffer.getContent();

        const line = await server.pop();
        const cmd = parseLines([line])[0];
        expect(cmd.strNoId).to.equal('/1 0 id stream buffer 1 print');
        await server.pop();

        await server.push(`@01 0 ${cmd.idStr} RJ IDLE -- BADDATA`);
        await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

        const result = await promise;
        expect(result).to.deep.eq([]);
      });
    });

    describe('erase', () => {
      it('erases a buffer', async () => {
        const promise = buffer.erase();
        await eqAndPushX('stream buffer 1 erase', 'OK IDLE -- 0');
        await promise;
      });
      it('throws error when buffer is busy', async () => {
        const promise = buffer.erase();
        await eqAndPushX('stream buffer 1 erase', 'RJ IDLE -- STATUSBUSY');
        await expect(promise).to.rejectedWith('Stream buffer is busy. Disable the stream first.');
      });
    });
  });

  describe('PVT buffer', () => {
    let buffer: PvtBuffer;
    beforeEach(() => {
      buffer = device.pvt.getBuffer(1);
    });

    describe('getContent', () => {
      it('sends proper command', async () => {
        const promise = buffer.getContent();
        const line = await server.pop();
        const cmd = parseLines([line])[0];
        expect(cmd.strNoId).to.equal('/1 0 id pvt buffer 1 print');
        await server.pop();

        await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
        await server.push(`#01 0 ${cmd.idStr} setup store 1 1`);
        await server.push(`#01 0 ${cmd.idStr} point abs p 1000 v 1000 t 1000`);
        await server.push(`#01 0 ${cmd.idStr} point abs p 2000 v 500 t 1000`);
        await server.push(`#01 0 ${cmd.idStr} setup disable`);
        await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

        const result = await promise;
        expect(result).to.deep.eq([
          'setup store 1 1',
          'point abs p 1000 v 1000 t 1000',
          'point abs p 2000 v 500 t 1000',
          'setup disable',
        ]);
      });
    });

    describe('erase', () => {
      it('sends proper command', async () => {
        const promise = buffer.erase();
        await eqAndPushX('pvt buffer 1 erase', 'OK IDLE -- 0');
        await promise;
      });
    });
  });
});
