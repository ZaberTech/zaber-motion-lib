import { expect } from 'chai';

import {
  MovementFailedException, MovementInterruptedException, CommandFailedException,
  InvalidArgumentException, DeviceNotIdentifiedException, Length, Units, NotSupportedException, Library,
} from '../../src';
import { Connection, Axis, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST, ClosedError } from './tcp';
import { parseCommand, parseLines } from './protocol';
import { itTime, measureTime } from '../test_util';

const server = new TestServer();

describe('Axis', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    Library.setIdlePollingPeriod(0);
    await server.start();
  });

  afterAll(async () => {
    Library.setIdlePollingPeriod(-1);
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(2);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('waitUntilIdle', () => {
    it('waits until device sends idle in status', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK BUSY -- 0');
      expect(await server.pop()).to.eq('/1 2 03');
      await server.push('@01 2 03 OK IDLE -- 0');

      await promise;
    });

    it('waits until device sends alert', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY -- 0');

      const loop = (async () => {
        try {
          for (; ;) {
            const cmd = parseCommand(await server.pop());
            expect(cmd.strNoId).to.eq('/1 2 id');
            await server.push(`@01 2 ${cmd.id} OK BUSY -- 0`);
          }
        } catch (err) {
          if (err instanceof ClosedError) {
            return;
          }
          throw err;
        }
      })();

      await server.push('!01 2 IDLE --');

      await promise;

      server.rejectAllWaiting();
      await loop;
    });

    it('reacts to alerts containing warning flags', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY -- 0');

      const loop = (async () => {
        for (; ;) {
          const cmd = parseCommand(await server.pop());
          switch (cmd.strNoId) {
            case '/1 2 id':
              await server.push(`@01 2 ${cmd.id} OK BUSY -- 0`);
              break;
            case '/1 2 id warnings clear':
              await server.push(`@01 2 ${cmd.id} OK IDLE FS 1 FS`);
              return;
            default:
              throw new Error(`Unexpected command: ${cmd.strNoId}`);
          }
        }
      })();

      await server.push('!01 2 IDLE FS');
      await expect(promise).to.be.rejectedWith(MovementFailedException);

      await loop;
    });

    it('ignores alerts with busy status', async () => {
      // this is impossible according to the current specification
      // it is here to reflect behavior of the library
      // and future compatibility
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY -- 0');

      await server.push('!01 2 BUSY --');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');

      await server.push('!01 2 BUSY --');

      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK IDLE -- 0');

      await promise;
    });

    it('does not throw exception when NI or NC flag is observed', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY NI 0');
      expect(await server.pop()).to.eq('/1 2 01 warnings clear');
      await server.push('@01 2 01 OK BUSY NI 01 NI');
      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK BUSY NC 0');
      expect(await server.pop()).to.eq('/1 2 03 warnings clear');
      await server.push('@01 2 03 OK BUSY NC 02 NC NI');
      expect(await server.pop()).to.eq('/1 2 04');
      await server.push('@01 2 04 OK IDLE -- 0');

      await promise;
    });

    it('does not throw exception when fault flag is observed and throwExceptionOnFault is false', async () => {
      const promise = axis.waitUntilIdle({ throwErrorOnFault: false });

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY FQ 0');
      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY FD 0');
      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK IDLE -- 0');

      await promise;
    });

    it('throws exception when fault flag is observed', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY FQ 0');
      expect(await server.pop()).to.eq('/1 2 01 warnings clear');
      await server.push('@01 2 01 OK BUSY FQ 01 FQ');

      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const exception: MovementFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });

    it('throws exception when multiple fault flags are observed', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY FO 0');
      expect(await server.pop()).to.eq('/1 2 01 warnings clear');
      await server.push('@01 2 01 OK BUSY FO 02 FO FS');

      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const exception: MovementFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq([
        'Movement may have failed because fault was observed:',
        'Driver Disabled on Startup or by Command (FO), Stalled and Stopped (FS).',
      ].join(' '));
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Driver Disabled on Startup or by Command (FO), Stalled and Stopped (FS)',
        warnings: ['FO', 'FS'],
      });
    });
  });

  describe('isBusy', () => {
    it('returns true when device is busy', async () => {
      const promise = axis.isBusy();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await promise).to.eq(true);
    });

    it('returns false when device is not busy', async () => {
      const promise = axis.isBusy();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 OK IDLE -- 0');

      expect(await promise).to.eq(false);
    });
  });

  describe('isHomed', () => {
    it('returns false when device has WR', async () => {
      const promise = axis.isHomed();

      expect(await server.pop()).to.eq('/1 2 00 warnings');
      await server.push('@01 2 00 OK IDLE WR 1 WR');

      expect(await promise).to.eq(false);
    });

    it('returns false when device has WH', async () => {
      const promise = axis.isHomed();

      expect(await server.pop()).to.eq('/1 2 00 warnings');
      await server.push('@01 2 00 OK IDLE WH 1 WH');

      expect(await promise).to.eq(false);
    });

    it('returns true when WR and WH are not present', async () => {
      const promise = axis.isHomed();

      expect(await server.pop()).to.eq('/1 2 00 warnings');
      await server.push('@01 2 00 OK IDLE WS 1 WS');

      expect(await promise).to.eq(true);
    });
  });

  describe('general', () => {
    it('checks response for rejection', async () => {
      const promise = axis.waitUntilIdle();

      expect(await server.pop()).to.eq('/1 2 00');
      await server.push('@01 2 00 RJ BUSY -- SYSTEMERROR');

      await expect(promise).to.be.rejectedWith(CommandFailedException);
      const exception: CommandFailedException = await promise.catch(e => e);
      expect(exception.message).to.startWith('Command "" rejected: SYSTEMERROR');
    });
  });

  describe('home', () => {
    it('sends proper home request', async () => {
      const promise = axis.home({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 2 00 home');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.home();

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('stop', () => {
    it('sends proper stop request', async () => {
      const promise = axis.stop({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 2 00 stop');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.stop();

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('moveAbsolute', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 2 00 move abs 123');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });

    it('waits until device sends alert', async () => {
      const promise = axis.moveAbsolute(123);

      expect(await server.pop()).to.eq('/1 2 00 move abs 123');
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');

      const loop = (async () => {
        try {
          for (; ;) {
            const cmd = parseCommand(await server.pop());
            expect(cmd.strNoId).to.eq('/1 2 id');
            await server.push(`@01 2 ${cmd.id} OK BUSY -- 0`);
          }
        } catch (err) {
          if (err instanceof ClosedError) {
            return;
          }
          throw err;
        }
      })();

      await server.push('!01 2 IDLE --');

      await promise;

      server.rejectAllWaiting();
      await loop;
    });

    it('throw exception when interrupted by other movement (NI flag)', async () => {
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK BUSY NI 0');

      expect(await server.pop()).to.eq('/1 2 03 warnings clear');
      await server.push('@01 2 03 OK BUSY NI 01 NI');

      await expect(promise).to.be.rejectedWith(MovementInterruptedException);
      const exception: MovementInterruptedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Movement was interrupted by: New command.');
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'New command',
        warnings: ['NI'],
      });
    });

    it('ignores NI flag when present from very beginning', async () => {
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY NI 0');

      expect(await server.pop()).to.eq('/1 2 01 warnings clear');
      await server.push('@01 2 01 OK BUSY NI 01 NI');

      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 2 OK IDLE NI 0');

      expect(await server.pop()).to.eq('/1 2 03 warnings clear');
      await server.push('@01 2 03 OK BUSY NI 01 NI');

      await promise;
    });

    it('throw exception when interrupted by knob (NC flag)', async () => {
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK BUSY NC 0');

      expect(await server.pop()).to.eq('/1 2 03 warnings clear');
      await server.push('@01 2 03 OK BUSY NC 02 NC NI');

      await expect(promise).to.be.rejectedWith(MovementInterruptedException);
      const exception: MovementInterruptedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Movement was interrupted by: Manual control.');
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Manual control',
        warnings: ['NC', 'NI'],
      });
    });

    it('throw exception when fault flag is observed during waiting', async () => {
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 02');
      await server.push('@01 2 02 OK BUSY FQ 0');

      expect(await server.pop()).to.eq('/1 2 03 warnings clear');
      await server.push('@01 2 03 OK BUSY FQ 01 FQ');

      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const exception: MovementFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Movement may have failed because fault was observed: Encoder Error (FQ).');
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Encoder Error (FQ)',
        warnings: ['FQ'],
      });
    });

    it('throw exception when fault flag is observed in response', async () => {
      // This error may have been caused by some previous movement.
      // We still think that the error should be passed to the user.
      const promise = axis.moveAbsolute(123);

      await server.pop();
      await server.push('@01 2 00 OK IDLE FS 0');

      expect(await server.pop()).to.eq('/1 2 01 warnings clear');
      await server.push('@01 2 01 OK IDLE FS 01 FS');

      await expect(promise).to.be.rejectedWith(MovementFailedException);
      const exception: MovementFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Movement may have failed because fault was observed: Stalled and Stopped (FS).');
      expect(exception.details).to.deep.eq({
        axis: 2,
        device: 1,
        reason: 'Stalled and Stopped (FS)',
        warnings: ['FS'],
      });
    });

    it('allows providing of velocity and acceleration', async () => {
      let promise = axis.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false, velocity: 10 });
      expect(await server.pop()).to.eq('/1 2 00 move abs 123 10');
      await server.push('@01 2 00 OK BUSY -- 0');
      await promise;

      promise = axis.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false, velocity: 20, acceleration: 10 });
      expect(await server.pop()).to.eq('/1 2 01 move abs 123 20 10');
      await server.push('@01 2 01 OK BUSY -- 0');
      await promise;
    });

    it('queries maxspeed when acceleration is provided without velocity', async () => {
      const promise = axis.moveAbsolute(123, Units.NATIVE, { waitUntilIdle: false, velocity: 0, acceleration: 789 });
      expect(await server.pop()).to.eq('/1 2 00 get maxspeed');
      await server.push('@01 2 00 OK BUSY -- 456');
      expect(await server.pop()).to.eq('/1 2 01 move abs 123 456 789');
      await server.push('@01 2 01 OK BUSY -- 0');
      await promise;
    });

    it('accepts positive infinity acceleration and translates it to 0', async () => {
      const promise = axis.moveAbsolute(123, Units.NATIVE, { acceleration: Infinity, velocity: 111, waitUntilIdle: false });
      expect(await server.pop()).to.eq('/1 2 00 move abs 123 111 0');
      await server.push('@01 2 00 OK BUSY -- 0');
      await promise;
    });

    it('continues waiting if alert comes before response', async () => {
      // This is a statistical test for race condition. It may not always fail but if it ever does,
      // it means that the race condition is still present.
      for (let i = 0; i < 1000; i++) {
        const promise = axis.moveAbsolute(123);

        let cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/1 2 id move abs 123');
        await server.push('!01 2 IDLE --');
        await server.push(`@01 2 ${cmd.id} OK BUSY -- 0`);

        cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/1 2 id');
        await server.push(`@01 2 ${cmd.id} OK BUSY -- 0`);

        const rejectTimer = setTimeout(() => server.rejectAllWaiting(), 2000);
        cmd = parseCommand(await server.pop().catch(() => {
          throw new Error(`Command ${i} not received`);
        }));
        expect(cmd.strNoId).to.eq('/1 2 id');
        await server.push(`@01 2 ${cmd.id} OK IDLE -- 0`);
        clearTimeout(rejectTimer);

        await promise;
      }
    }, 30 * 1000);
  });

  describe('moveRelative', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveRelative(123, Units.NATIVE, { waitUntilIdle: false, velocity: 10, acceleration: 20 });

      expect(await server.pop()).to.eq('/1 2 00 move rel 123 10 20');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveRelative(123);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('moveSin', () => {
    it('Move sin with unlimited count', async () => {
      const promise = axis.moveSin(10000, Units.NATIVE, 2000, Units.NATIVE, { waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 2 00 move sin 10000 2000');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveSin(2000, Units.NATIVE, 10000, Units.NATIVE);

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });

    it('Move sin with count', async () => {
      const promise = axis.moveSin(2000, Units.NATIVE, 10000, Units.NATIVE, { waitUntilIdle: false, count: 2.5 });

      expect(await server.pop()).to.eq('/1 2 00 move sin 2000 10000 2.5');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('moveSinStop', () => {
    it('Stop sine movement', async () => {
      const promise = axis.moveSinStop({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/1 2 00 move sin stop');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveSinStop();

      await server.pop();
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('moveVelocity', () => {
    it('sends proper move request', async () => {
      const promise = axis.moveVelocity(123, Units.NATIVE, { acceleration: 10 });

      expect(await server.pop()).to.eq('/1 2 00 move vel 123 10');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('moveMax', () => {
    it('sends proper move max request', async () => {
      const promise = axis.moveMax({ waitUntilIdle: false, velocity: 10, acceleration: 20 });

      expect(await server.pop()).to.eq('/1 2 00 move max 10 20');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveMax();

      expect(await server.pop()).to.eq('/1 2 00 move max');
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('moveMin', () => {
    it('sends proper move min request', async () => {
      const promise = axis.moveMin({ waitUntilIdle: false, velocity: 10, acceleration: 20 });

      expect(await server.pop()).to.eq('/1 2 00 move min 10 20');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });

    it('waits until idle', async () => {
      const promise = axis.moveMin();

      expect(await server.pop()).to.eq('/1 2 00 move min');
      await server.push('@01 2 00 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/1 2 01');
      await server.push('@01 2 01 OK IDLE -- 0');

      await promise;
    });
  });

  describe('moveIndex', () => {
    it('sends proper request', async () => {
      const promise = axis.moveIndex(3, { waitUntilIdle: false, velocity: 10, acceleration: 20 });

      expect(await server.pop()).to.eq('/1 2 00 move index 3 10 20');
      await server.push('@01 2 00 OK BUSY -- 0');

      await promise;
    });
  });

  describe('getIndexPosition', () => {
    it('send proper request and returns response', async () => {
      const promise = axis.getIndexPosition();

      expect(await server.pop()).to.eq('/1 2 00 get motion.index.num');
      await server.push('@01 2 00 OK IDLE -- 13');

      expect(await promise).to.eq(13);
    });

    it('returns 0 if WR is present', async () => {
      const promise = axis.getIndexPosition();

      expect(await server.pop()).to.eq('/1 2 00 get motion.index.num');
      await server.push('@01 2 00 OK IDLE WR 1');

      expect(await server.pop()).to.eq('/1 2 01 warnings');
      await server.push('@01 2 01 OK IDLE WR 1 WR');

      expect(await promise).to.eq(0);
    });
  });

  describe('getNumberOfIndexPositions', () => {
    it('calculates indexed positions for cyclic device', async () => {
      const promise = axis.getNumberOfIndexPositions();

      let cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get motion.index.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 5`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.cycle.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 20`);

      expect(await promise).to.eq(4);
    });

    it('calculates indexed positions for cyclic device (fractional)', async () => {
      const promise = axis.getNumberOfIndexPositions();

      let cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get motion.index.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 5`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.cycle.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 16`);

      expect(await promise).to.eq(4);
    });

    it('calculates indexed positions for straight device', async () => {
      const promise = axis.getNumberOfIndexPositions();

      let cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get motion.index.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 5`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.cycle.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 0`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.min');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 0`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.max');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 20`);

      expect(await promise).to.eq(5);
    });

    it('calculates indexed positions for straight device (fractional)', async () => {
      const promise = axis.getNumberOfIndexPositions();

      let cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get motion.index.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 5`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.cycle.dist');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 0`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.min');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 0`);

      cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('get limit.max');
      await server.push(`@1 2 ${cmd.id} OK IDLE -- 19`);

      expect(await promise).to.eq(4);
    });
  });

  describe('getPosition', () => {
    it('sends proper command and returns response', async () => {
      const promise = axis.getPosition();

      expect(await server.pop()).to.eq('/1 2 00 get pos');
      await server.push('@01 2 00 OK IDLE -- 12345');

      expect(await promise).to.eq(12345);
    });
  });

  describe('genericCommand', () => {
    it('sends correct command and passes response', async () => {
      const promise = axis.genericCommand('home', { checkErrors: false });

      expect(await server.pop()).to.eq('/1 2 00 home');
      await server.push('@01 2 00 RJ BUSY -- BADCOMMAND');

      expect(await promise).to.deep.include({ data: 'BADCOMMAND' });
    });
  });

  describe('park', () => {
    it('sends proper parking request', async () => {
      const promise = axis.park();

      expect(await server.pop()).to.eq('/1 2 00 tools parking park');
      await server.push('@01 2 00 OK IDLE -- 0');

      await promise;
    });

    it('returns an exception with custom message when an axis on FW 6 device is parked', async () => {
      const promise = axis.park();

      expect(await server.pop()).to.eq('/1 2 00 tools parking park');
      await server.push('@01 2 0 RJ IDLE -- DEVICEONLY');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message)
        .to.eq('Firmware of a version 6 and lower do not support parking of a specific axis. Park the entire device using AllAxes.');
    });
  });

  describe('unpark', () => {
    it('sends proper unparking request', async () => {
      const promise = axis.unpark();

      expect(await server.pop()).to.eq('/1 2 00 tools parking unpark');
      await server.push('@01 2 00 OK IDLE -- 0');

      await promise;
    });

    it('returns an exception with custom message when an axis on FW 6 device is unparked', async () => {
      const promise = axis.unpark();

      expect(await server.pop()).to.eq('/1 2 00 tools parking unpark');
      await server.push('@01 2 0 RJ IDLE -- DEVICEONLY');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message)
        .to.eq('Firmware of a version 6 and lower do not support parking of a specific axis. Unpark the entire device using AllAxes.');
    });
  });

  describe('isParked', () => {
    it('returns true when axis is parked', async () => {
      const promise = axis.isParked();

      expect(await server.pop()).to.eq('/1 2 00 get parking.state');
      await server.push('@01 2 0 OK IDLE -- 1');

      expect(await promise).to.eq(true);
    });

    it('returns false when axis is not parked', async () => {
      const promise = axis.isParked();

      expect(await server.pop()).to.eq('/1 2 00 get parking.state');
      await server.push('@01 2 0 OK IDLE -- 0');

      expect(await promise).to.eq(false);
    });

    it('returns an exception with custom message when an axis rejects a parking query', async () => {
      const promise = axis.isParked();

      expect(await server.pop()).to.eq('/1 2 00 get parking.state');
      await server.push('@01 2 0 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message).to.eq('This device type does not support parking.');
    });
  });

  describe('prepareCommand', () => {
    it('returns command without unit conversions when possible', async () => {
      const command = axis.prepareCommand('move sin ? ? ?',
        { value: 32000 },
        { value: 1000.9 },
        { value: 3 },
      );

      expect(command).to.eq('move sin 32000 1000.9 3');
    });

    it('throws exception when device not identified and doing unit conversion', () => {
      try {
        axis.prepareCommand('move sin ? ? ?',
          { value: 10, unit: Length.mm },
          { value: 1000 },
          { value: 3 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(DeviceNotIdentifiedException);
      }
    });

    it('throws exception when arguments do not match the template (less arguments)', () => {
      try {
        axis.prepareCommand('move sin ? ? ?',
          { value: 3 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(InvalidArgumentException);
        expect((err as InvalidArgumentException).message).to.eq(
          'Number of arguments provided (1) does not match number of arguments in command template [move sin ? ? ?].');
      }
    });

    it('throws exception when arguments do not match the template (more arguments)', () => {
      try {
        axis.prepareCommand('move sin ? ?',
          { value: 3 },
          { value: 3 },
          { value: 3 },
        );
        throw new Error('No exception thrown');
      } catch (err) {
        expect(err).to.be.an.instanceof(InvalidArgumentException);
      }
    });
  });

  describe('toString', () => {
    it('returns representation for non identified/integrated device', () => {
      expect(axis.toString()).to.startWith('Axis 2 -> Device 1 (Unknown) -> Connection');
    });
    it('returns representation for closed connection', async () => {
      await connection.close();
      expect(axis.toString()).to.eq('Axis 2 -> Device 1 -> Connection Closed');
    });
  });

  describe('genericCommandMultiResponse', () => {
    it('sends correct command and returns replies', async () => {
      const replyPromise = axis.genericCommandMultiResponse('fly');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 2 id fly');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 2 ${cmd.idStr}`);

      await server.push(`@01 2 ${cmd.idStr} OK IDLE -- 0`);
      await server.push(`#01 2 ${cmd.idStr} bzzz...`);
      await server.push(`#01 2 ${cmd.idStr} ...zzz`);
      await server.push(`@01 2 ${cmd.idStr} OK IDLE -- 0`);

      const result = await replyPromise;

      expect(result.length).to.eq(3);
      expect(result[0]).to.deep.include({ data: '0' });
      expect(result[1]).to.deep.include({ data: 'bzzz...' });
      expect(result[2]).to.deep.include({ data: '...zzz' });
    });
  });

  describe('genericCommandNoResponse', () => {
    it('sends correct command', async () => {
      const replyPromise = axis.genericCommandNoResponse('home');

      const line = await server.pop();
      expect(line).to.eq('/1 2 home');

      await replyPromise;
    });
  });

  describe('driver', () => {
    it('sends disable driver command correctly', async () => {
      const promise = axis.driverDisable();

      expect(await server.pop()).to.eq('/1 2 00 driver disable');
      await server.push('@01 2 00 OK IDLE FO 0');

      await promise;
    });

    it('sends enable driver command correctly', async () => {
      const promise = axis.driverEnable();

      expect(await server.pop()).to.eq('/1 2 00 driver enable');
      await server.push('@01 2 00 OK IDLE -- 0');

      await promise;
    });

    it('handles regular rejection', async () => {
      const promise = axis.driverEnable();

      expect(await server.pop()).to.eq('/1 2 00 driver enable');
      await server.push('@01 2 00 RJ IDLE FZ INACTIVE');

      await expect(promise).to.be.rejectedWith(CommandFailedException);
    });

    it('keeps trying to enable the driver', async () => {
      Library.setIdlePollingPeriod(0);
      const promise = axis.driverEnable();

      for (let i = 3; i >= 0; i--) {
        const cmd = parseCommand(await server.pop());
        expect(cmd.strNoId).to.eq('/1 2 id driver enable');
        if (i > 0) {
          await server.push(`@01 2 ${cmd.id} RJ IDLE FO DRIVERDISABLED`);
        } else {
          await server.push(`@01 2 ${cmd.id} OK IDLE -- 0`);
        }
      }

      await promise;
    });

    itTime('fails after timeout if driver is not enabled in time', async () => {
      const TIMEOUT = 0.2; //s
      const elapsedTime = await measureTime(async () => {
        const promise = axis.driverEnable({ timeout: TIMEOUT });
        (async () => {
          for (;;) {
            const cmd = parseCommand(await server.pop());
            await server.push(`@01 2 ${cmd.id} RJ IDLE -- DRIVERDISABLED`);
          }
        })().catch(() => null);
        await expect(promise).to.be.rejectedWith(CommandFailedException);
      });
      expect(elapsedTime).to.be.onTime(TIMEOUT * 1e3);
    });
  });
});
