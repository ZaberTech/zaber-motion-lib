import { expect } from 'chai';
import { lastValueFrom } from 'rxjs';

import { ConnectionFailedException } from '../../src';
import { Connection } from '../../src/ascii';
import { RpcClient } from './iot/rpc_client';
import { MqttTest } from './iot/mqtt_test';
import { RpcMqttIo } from './iot/rpc_mqtt_io';
import { makeConnectionInfo, TEST_CONN_1, TEST_CONN_2, TEST_ROUTER, UNAUTHENTICATED_REALM, UNAUTHENTICATED_TOKEN } from './iot/constants';

const FAILS_MESSAGE = 'Should have failed command';

describe('IoT Connections', () => {
  const mqtt = new MqttTest();
  let rpcClientIot: RpcClient;
  let connection: Connection;

  beforeAll(async () => {
    await mqtt.connect();
    const mqttIo = new RpcMqttIo(mqtt, TEST_ROUTER, UNAUTHENTICATED_REALM);
    await mqttIo.init();
    rpcClientIot = new RpcClient(mqttIo);
  });
  afterAll(async () => {
    await mqtt.disconnect();
  });

  beforeEach(() => {
    rpcClientIot.availableConnections = makeConnectionInfo([TEST_CONN_1, TEST_CONN_2]);
  });

  afterEach(async () => {
    if (connection) {
      await connection.close();
      connection = null!;
    }
  });

  it('connects', async () => {
    connection = await Connection.openIot(TEST_ROUTER, { connectionName: TEST_CONN_1, api: 'https://api-staging.zaber.io' });
    await connection.genericCommand('home', { device: 1 });
    await connection.close();
    rpcClientIot.expectConnectionsClosed();
  });

  it('may fail', async () => {
    connection = await Connection.openIot(TEST_ROUTER, { connectionName: TEST_CONN_1, api: 'https://api-staging.zaber.io' });
    rpcClientIot.failNextResponse = true;
    try {
      await connection.genericCommand('home', { device: 1 });
      throw new Error(FAILS_MESSAGE);
    } catch (e) {
      expect((e as Error).message).to.not.equal(FAILS_MESSAGE);
      const disconnectReason = await lastValueFrom(connection.disconnected);
      expect(disconnectReason instanceof ConnectionFailedException);
      expect(disconnectReason?.message).to.equal('disconnected');
    }
  });

  it('will not connect when there are no connections', async () => {
    rpcClientIot.availableConnections = [];
    try {
      connection = await Connection.openIot(TEST_ROUTER, { connectionName: TEST_CONN_1, api: 'https://api-staging.zaber.io' });
      throw new Error(FAILS_MESSAGE);
    } catch (disconnectReason) {
      expect(disconnectReason instanceof ConnectionFailedException);
      expect((disconnectReason as ConnectionFailedException).message).to.equal('There are no connections available on this message router');
    }
  });

  it('will not connect to non-existent connection', async () => {
    const BAD_CONN = 'COM1.453';
    try {
      connection = await Connection.openIot(TEST_ROUTER, { connectionName: BAD_CONN, api: 'https://api-staging.zaber.io' });
      throw new Error(FAILS_MESSAGE);
    } catch (disconnectReason) {
      expect(disconnectReason instanceof ConnectionFailedException);
      expect((disconnectReason as ConnectionFailedException).message)
        .to.equal(`Connection ${BAD_CONN} not available. Choose one of [${TEST_CONN_1}, ${TEST_CONN_2}]`);
    }
  });

  it('will use default if there is only one connection', async () => {
    rpcClientIot.availableConnections = makeConnectionInfo([TEST_CONN_1]);
    connection = await Connection.openIot(TEST_ROUTER, { api: 'https://api-staging.zaber.io' });
    await connection.genericCommand('home', { device: 1 });
  });

  it('will use default realm', async () => {
    connection = await Connection
      .openIot(TEST_ROUTER, { token: UNAUTHENTICATED_TOKEN, connectionName: TEST_CONN_1, api: 'https://api-staging.zaber.io' });
    await connection.genericCommand('home', { device: 1 });
  });
});
