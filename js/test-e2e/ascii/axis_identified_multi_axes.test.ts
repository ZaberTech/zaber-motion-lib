import { expect } from 'chai';

import { AngularVelocity, ConversionFailedException, Length, Velocity, Angle } from '../../src';
import { Connection, Axis, Device, AxisType } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXMCB2 } from './devices/xmcb2';

const server = new TestServer();

describe('Axis - identified - multi axis', () => {
  let connection: Connection;
  let device: Device;
  let axis1: Axis;
  let axis2: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis1 = device.getAxis(1);
    axis2 = device.getAxis(2);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('one linear, one rotary', () => {
    beforeEach(async () => {
      await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.OneLinearOneRotary);
      connection.resetIds();
    });

    describe('axisType', () => {
      it('returns proper axisType for each of the peripheral', async () => {
        expect(axis1.identity.axisType).to.eq(AxisType.LINEAR);
        expect(axis2.identity.axisType).to.eq(AxisType.ROTARY);
      });
    });

    describe('exceptions', () => {
      it('throws exception when using wrong units', async () => {
        let promise = axis1.moveAbsolute(4.5, Angle.DEGREES);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception: ConversionFailedException = await promise.catch(e => e);
        expect(exception.message).to.eq('Invalid units: Angle:degrees. Provide units of dimension Length.');

        promise = axis2.moveAbsolute(4.5, Length.mm);

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
        const exception2: ConversionFailedException = await promise.catch(e => e);
        expect(exception2.message).to.eq('Invalid units: Length:millimetres. Provide units of dimension Angle.');
      });
    });

    describe('moveAbsolute', () => {
      it('sends proper move request to axis 1', async () => {
        const promise = axis1.moveAbsolute(4.5, Length.mm, { waitUntilIdle: false });

        expect(await server.pop()).to.eq('/1 1 00 move abs 36283');
        await server.push('@01 1 00 OK BUSY -- 0');

        await promise;
      });
      it('sends proper move request to axis 2', async () => {
        const promise = axis2.moveAbsolute(20, Angle.DEGREES, { waitUntilIdle: false });

        expect(await server.pop()).to.eq('/1 2 00 move abs 3556');
        await server.push('@01 2 00 OK BUSY -- 0');

        await promise;
      });
    });

    describe('moveVelocity', () => {
      it('sends proper move request to axis 1', async () => {
        const promise = axis1.moveVelocity(1.5, Velocity.MILLIMETRES_PER_SECOND);

        expect(await server.pop()).to.eq('/1 1 00 move vel 19816');
        await server.push('@01 1 00 OK BUSY -- 0');

        await promise;
      });
      it('sends proper move request to axis 2', async () => {
        const promise = axis2.moveVelocity(5, AngularVelocity.DEGREES_PER_SECOND);

        expect(await server.pop()).to.eq('/1 2 00 move vel 1456');
        await server.push('@01 2 00 OK BUSY -- 0');

        await promise;
      });
    });

    describe('toString', () => {
      it('returns representation for identified device with peripheral', () => {
        expect(axis1.toString()).to.startWith('Axis 1 (LHM025A-T3) -> Device 1 SN: 1234 (X-MCB2) -> Connection');
        expect(axis2.toString()).to.startWith('Axis 2 (RSB060AU-T3) -> Device 1 SN: 1234 (X-MCB2) -> Connection');
      });
    });

    describe('properties', () => {
      it('returns appropriate data', async () => {
        expect(axis1.isPeripheral).to.eq(true);
        expect(axis1.peripheralId).to.eq(43211);
        expect(axis1.peripheralName).to.eq('LHM025A-T3');
        expect(axis1.peripheralSerialNumber).to.eq(26000);
        expect(axis2.isPeripheral).to.eq(true);
        expect(axis2.peripheralId).to.eq(46653);
        expect(axis2.peripheralSerialNumber).to.eq(26001);
        expect(axis2.peripheralName).to.eq('RSB060AU-T3');
      });
    });
  });

  describe('one linear, one empty', () => {
    beforeEach(async () => {
      await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.OneLinearOneEmpty);
      connection.resetIds();
    });

    describe('identity', () => {
      it('returns proper identity for each of the peripherals', async () => {
        const linearAxis = axis1.identity;
        expect(linearAxis.axisType).to.eq(AxisType.LINEAR);

        const emptyAxis = axis2.identity;
        expect(emptyAxis.axisType).to.eq(AxisType.UNKNOWN);
        expect(emptyAxis.peripheralName).to.eq('Safe Mode');
        expect(emptyAxis.peripheralSerialNumber).to.eq(0);
        expect(emptyAxis.isPeripheral).to.eq(true);
      });
    });

    describe('toString', () => {
      it('returns representation for axis with empty peripheral', () => {
        expect(axis2.toString()).to.startWith('Axis 2 (Safe Mode) -> Device 1 SN: 1234 (X-MCB2) -> Connection');
      });
    });
  });
});
