import { expect } from 'chai';
import _ from 'lodash';
import { performance } from 'perf_hooks';
import { firstValueFrom, lastValueFrom } from 'rxjs';

import {
  ConnectionClosedException, ConnectionFailedException, RequestTimeoutException, CommandFailedException, DeviceFailedException,
  NoDeviceFoundException, InvalidResponseException, DeviceAddressConflictException, NotSupportedException, InvalidDataException,
  InvalidArgumentException,
  Library,
  BadDataException,
  BadCommandException,
} from '../../src';
import { Connection, MessageType } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { parseLines, parseCommand } from './protocol';
import { DeviceXLHM } from './devices/xlhm';
import { DeviceXMCB2 } from './devices/xmcb2';
import { itTime } from '../test_util';

const server = new TestServer();

type DeviceDetectInfo = number | { number: number; packetSize: number };

function getDevicePacketSize(deviceNumber: number, devices: DeviceDetectInfo[]): number | null {
  for (const device of devices) {
    if (typeof device !== 'number' && device.number === deviceNumber) {
      return device.packetSize;
    }
  }
  return null;
}

async function detect(server: TestServer, devices: DeviceDetectInfo[], identifiers?: () => Promise<void>) {
  const getDevicesCmd = parseCommand(await server.pop());
  expect(getDevicesCmd.strNoId).to.eq('/0 0 id');
  for (const device of devices) {
    await server.push(`@${typeof device === 'number' ? device : device.number} 0 ${getDevicesCmd.id} OK IDLE -- 0`);
  }

  for (let i = 0; i < devices.length; i++) {
    const getPacketSizeCmd = parseCommand(await server.pop());
    expect(getPacketSizeCmd.data).to.eq('get comm.packet.size.max');
    const deviceNumber = getPacketSizeCmd.device;
    const packetSize = getDevicePacketSize(deviceNumber, devices);
    if (packetSize == null) {
      await server.push(`@${deviceNumber} 0 ${getPacketSizeCmd.id} RJ IDLE -- BADCOMMAND`);
    } else {
      await server.push(`@${deviceNumber} 0 ${getPacketSizeCmd.id} OK IDLE -- ${packetSize}`);
    }
  }

  if (identifiers) { await identifiers() }
}

describe('Connection (ASCII)', () => {
  let connection: Connection;
  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  it('connects and sends command', async () => {
    await connection.genericCommandNoResponse('');

    const line = await server.pop();
    expect(line).to.eq('/0 0');
  });

  it('emits disconnect when closed by other side', async () => {
    server.closeSocket();

    const exception = await lastValueFrom(connection.disconnected);
    expect(exception).to.be.an.instanceof(ConnectionFailedException);
  });

  it('emits disconnect when closed by call', async () => {
    await connection.close();

    const exception = await lastValueFrom(connection.disconnected);
    expect(exception).to.be.an.instanceof(ConnectionClosedException);
  });

  it('closes the connection', async () => {
    const waitForClose = server.waitForClose();
    await connection.close();
    await waitForClose;
  });

  describe('checksum', () => {
    it('adds checksum to every message', async () => {
      await connection.genericCommandNoResponse('QA0000', { device: 1, axis: 2 });

      const line = await server.pop(true);
      expect(line).to.eq('/1 2 QA0000:0B');
    });
    it('allows to disable and enable checksum', async () => {
      connection.checksumEnabled = false;
      expect(connection.checksumEnabled).to.eq(false);

      await connection.genericCommandNoResponse('QA0000', { device: 1, axis: 2 });

      connection.checksumEnabled = true;
      expect(connection.checksumEnabled).to.eq(true);

      await connection.genericCommandNoResponse('QA0000', { device: 1, axis: 2 });

      const lines = await Promise.all([server.pop(true), server.pop(true)]);
      expect(lines).to.deep.eq([
        '/1 2 QA0000',
        '/1 2 QA0000:0B',
      ]);
    });
  });

  describe('unknownResponse', () => {
    it('emits unknownResponse event when cannot find request', async () => {
      const replyPromise = firstValueFrom(connection.unknownResponse);

      await server.push('@02 1 OK BUSY -- 0');

      const reply = await replyPromise;

      expect(reply).to.deep.include({
        deviceAddress: 2,
        axisNumber: 1,
        replyFlag: 'OK',
        status: 'BUSY',
        warningFlag: '--',
        data: '0',
        messageType: MessageType.REPLY,
      });
    });

    it('emits unknownResponse event when cannot find request for info message', async () => {
      const replyPromise = firstValueFrom(connection.unknownResponse);

      await server.push('#02 3 some info');

      const reply = await replyPromise;

      expect(reply).to.deep.include({
        deviceAddress: 2,
        axisNumber: 3,
        replyFlag: '',
        status: '',
        warningFlag: '',
        data: 'some info',
        messageType: MessageType.INFO,
      });
    });
  });

  describe('alert', () => {
    it('emits alert event when receives alert from device', async () => {
      const alertPromise = firstValueFrom(connection.alert);

      await server.push('!02 1 IDLE -- 0');

      const alert = await alertPromise;

      expect(alert).to.deep.include({
        deviceAddress: 2,
        axisNumber: 1,
        status: 'IDLE',
        warningFlag: '--',
        data: '0',
      });
    });
  });

  describe('genericCommandNoResponse', () => {
    it('sends empty command', async () => {
      await connection.genericCommandNoResponse('', { device: -1, axis: -1 });

      const line = await server.pop();
      expect(line).to.eq('/');
    });

    it('defaults device and axis to 0', async () => {
      await connection.genericCommandNoResponse('');

      const line = await server.pop();
      expect(line).to.eq('/0 0');
    });

    it('sends command with data, device, axis', async () => {
      await connection.genericCommandNoResponse('home', { device: 1, axis: 2 });

      const line = await server.pop();
      expect(line).to.eq('/1 2 home');
    });
  });

  describe('genericCommand', () => {
    it('matches responses using ids', async () => {
      const replyPromise1 = connection.genericCommand('');
      const replyPromise2 = connection.genericCommand('get version');
      const replyPromise3 = connection.genericCommand('move_abs 10000');

      const lines = await Promise.all(_.range(0, 3).map(() => server.pop()));
      const cmds = parseLines(lines);
      expect(cmds.map(cmd => cmd.strNoId)).to.deep.eq(['/0 0 id', '/0 0 id get version', '/0 0 id move_abs 10000']);

      await server.push(`@01 0 ${cmds[2].id} OK BUSY -- 0`);
      await server.push(`@01 0 ${cmds[1].id} OK IDLE -- 6.28`);
      await server.push(`@01 0 ${cmds[0].id} OK IDLE FQ 0`);

      expect(await replyPromise1).to.deep.eq({
        axisNumber: 0, data: '0', deviceAddress: 1, warningFlag: 'FQ',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
      expect(await replyPromise2).to.deep.eq({
        axisNumber: 0, data: '6.28', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
      expect(await replyPromise3).to.deep.eq({
        axisNumber: 0, data: '0', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'BUSY'
      });
    });

    it('times out when reply not received', async () => {
      const replyPromise1 = connection.genericCommand('');
      const replyPromise2 = connection.genericCommand('get version');

      const lines = await Promise.all(_.range(0, 2).map(() => server.pop()));
      const cmds = parseLines(lines);
      expect(cmds.map(cmd => cmd.strNoId)).to.deep.eq(['/0 0 id', '/0 0 id get version']);

      // send back only one
      await server.push(`@01 0 ${cmds[0].id} OK IDLE -- 0`);

      const reply1 = await replyPromise1;
      expect(reply1).to.deep.include({ replyFlag: 'OK' });

      await expect(replyPromise2).to.be.rejectedWith(RequestTimeoutException);
      const exception: RequestTimeoutException = await replyPromise2.catch(e => e);
      expect(exception.message).to.eq('Device has not responded in given timeout');
    });

    itTime('respects the timeout argument', async () => {
      for (const testedTimeout of [200, 400, 600]) {
        const startTime = performance.now();

        const replyPromise = connection.genericCommand('', { device: 1, checkErrors: false, timeout: testedTimeout });
        await server.pop();
        await expect(replyPromise).to.be.rejectedWith(RequestTimeoutException);

        const elapsedTime = performance.now() - startTime;
        expect(elapsedTime).to.be.onTime(testedTimeout);
      }
    });

    it('checks for the exception', async () => {
      const replyPromise = connection.genericCommand('cmd');

      const line = await server.pop();
      expect(line).to.eq('/0 0 00 cmd');

      await server.push('@01 0 00 RJ BUSY -- BADDATA');

      await expect(replyPromise).to.be.rejectedWith(BadDataException);
      const exception: BadDataException = await replyPromise.catch(e => e);
      expect(exception.message).to.startWith('Command "cmd" rejected: BADDATA');
      expect(exception.details).to.deep.equal({
        responseData: 'BADDATA',
        replyFlag: 'RJ',
        status: 'BUSY',
        warningFlag: '--',
        axisNumber: 0,
        command: 'cmd',
        deviceAddress: 1,
        id: 0,
      });
    });

    it('checks for the FF flag', async () => {
      const replyPromise = connection.genericCommand('');

      const line = await server.pop();
      expect(line).to.eq('/0 0 00');

      await server.push('@01 0 0 OK BUSY FF 0');

      await expect(replyPromise).to.be.rejectedWith(DeviceFailedException);
      const exception: DeviceFailedException = await replyPromise.catch(e => e);
      expect(exception.message).to.eq('Critical System Error (FF flag). Please contact Zaber support.');
    });

    it('do not check for exception when not ordered to', async () => {
      const replyPromise = connection.genericCommand('', { checkErrors: false });

      const line = await server.pop();
      expect(line).to.eq('/0 0 00');

      await server.push('@01 0 0 RJ BUSY -- BADDATA');

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: 'BADDATA',
        deviceAddress: 1, warningFlag: '--', messageType: MessageType.REPLY, replyFlag: 'RJ', status: 'BUSY'
      });
    });

    it('checks for the device number and axis of the reply', async () => {
      const replyPromise = connection.genericCommand('', { device: 2 });

      const line = await server.pop();
      expect(line).to.eq('/2 0 00');

      await server.push('@03 0 0 OK IDLE -- 0');

      await expect(replyPromise).to.be.rejectedWith(InvalidDataException);
      const exception: InvalidDataException = await replyPromise.catch(e => e);
      expect(exception.message).to.eq('Response device or axis does not match: 3 != 2 || 0 != 0');
    });

    it('does not check for the device number and axis of the reply when checkErrors is false', async () => {
      const replyPromise = connection.genericCommand('', { device: 2, checkErrors: false });

      const line = await server.pop();
      expect(line).to.eq('/2 0 00');

      await server.push('@03 0 0 OK IDLE -- 0');

      expect(await replyPromise).to.deep.include({ deviceAddress: 3, axisNumber: 0 });
    });
  });

  describe('command to all', () => {
    it('stops all and returns list', async () => {
      const stopAllReplyPromise = connection.stopAll({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/0 0 00 stop');

      await server.push('@02 0 0 OK BUSY -- 0');
      await server.push('@03 0 0 OK BUSY -- 0');

      expect(await stopAllReplyPromise).to.deep.eq([2, 3]);
    });

    it('homes all and returns list', async () => {
      const homeAllReplyPromise = connection.homeAll({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/0 0 00 home');

      await server.push('@02 0 0 OK BUSY -- 0');
      await server.push('@03 0 0 OK BUSY -- 0');

      expect(await homeAllReplyPromise).to.deep.eq([2, 3]);
    });

    it('waits until idle', async () => {
      const stopAllReplyPromise = connection.stopAll();

      expect(await server.pop()).to.eq('/0 0 00 stop');

      await server.push('@02 0 0 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/2 0 01');

      await server.push('@02 0 1 OK IDLE -- 0');

      expect(await stopAllReplyPromise).to.deep.eq([2]);
    });

    it('ignores rejection when command is not supported', async () => {
      const stopAllReplyPromise = connection.stopAll();

      expect(await server.pop()).to.eq('/0 0 00 stop');

      await server.push('@04 0 0 OK BUSY -- 0');
      await server.push('@03 0 0 RJ IDLE -- BADCOMMAND');

      expect(await server.pop()).to.eq('/4 0 01');

      await server.push('@04 0 1 OK IDLE -- 0');

      expect(await stopAllReplyPromise).to.deep.eq([4]);
    });

    it('fails on other rejections (other than command not supported)', async () => {
      const stopAllReplyPromise = connection.stopAll({ waitUntilIdle: false });

      expect(await server.pop()).to.eq('/0 0 00 stop');

      await server.push('@03 0 0 RJ IDLE -- BADAXIS');
      await server.push('@04 0 0 OK BUSY -- 0');

      await expect(stopAllReplyPromise).to.eventually.be.rejectedWith(CommandFailedException);
    });

    it('continues to wait after an error', async () => {
      const homeAllReplyPromise = connection.homeAll();

      expect(await server.pop()).to.eq('/0 0 00 home');

      await server.push('@06 0 0 RJ IDLE -- BADAXIS');
      await server.push('@03 0 0 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/3 0 01');
      await server.push('@03 0 1 OK BUSY -- 0');

      expect(await server.pop()).to.eq('/3 0 02');
      await server.push('@03 0 2 OK IDLE -- 0');

      await expect(homeAllReplyPromise).to.eventually.be.rejectedWith(CommandFailedException);
    });
  });

  describe('defaultRequestTimeout', () => {
    itTime('sets the default timeout for a requests', async () => {
      for (const testedTimeout of [200, 400, 600]) {
        connection.defaultRequestTimeout = testedTimeout;
        expect(connection.defaultRequestTimeout).to.eq(testedTimeout);

        const startTime = performance.now();

        const replyPromise = connection.genericCommand('', { device: 1 });
        await server.pop();
        await expect(replyPromise).to.be.rejectedWith(RequestTimeoutException);

        const elapsedTime = performance.now() - startTime;
        expect(elapsedTime).to.be.onTime(testedTimeout);
      }
    });
    it('throws an exception when set to an invalid value', () => {
      expect(() => connection.defaultRequestTimeout = 0).to.throw('The timeout must be greater than 0 (provided 0).');
    });
  });

  describe('continuations', () => {
    it('joins packets', async () => {
      const replyPromise = connection.genericCommand('get pos', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id get pos');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 1 2\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 3 4\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 5 6`);

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: '1 2 3 4 5 6', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
    });

    it('joins packets separately from info list', async () => {
      const replyPromise = connection.genericCommandMultiResponse('help commands', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id help commands');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseCommand(lineTerminate);
      expect(cmdTerminate.str).to.eq(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- help\\`);
      await server.push(`#01 0 ${cmd.idStr} cont commands`);
      await server.push(`#01 0 ${cmd.idStr} list`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      expect(await replyPromise).to.deep.eq([{
        axisNumber: 0, data: 'help commands', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      }, {
        axisNumber: 0, data: 'list', deviceAddress: 1, warningFlag: '',
        messageType: MessageType.INFO, replyFlag: '', status: ''
      }]);
    });

    it('handles interleaved packets', async () => {
      const replyPromise = connection.genericCommandMultiResponse('get pos');

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/0 0 id get pos');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 1 2\\`);
      await server.push(`@02 0 ${cmd.idStr} OK IDLE -- 2 3\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 3 4`);
      await server.push(`@03 0 ${cmd.idStr} OK IDLE -- 3 4\\`);
      await server.push(`#02 0 ${cmd.idStr} cont 4 5`);
      await server.push(`#03 0 ${cmd.idStr} cont 5 6`);

      expect(await replyPromise).to.deep.eq([{
        axisNumber: 0, data: '1 2 3 4', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      }, {
        axisNumber: 0, data: '2 3 4 5', deviceAddress: 2, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      }, {
        axisNumber: 0, data: '3 4 5 6', deviceAddress: 3, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      }]);
    });

    it('handles empty first packet', async () => {
      const replyPromise = connection.genericCommand('tools echo abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd', { device: 1 });

      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq('/1 0 id tools echo abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- \\`);
      await server.push(`#01 0 ${cmd.idStr} cont abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd`);

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: 'abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
    });

    it('handles multiple empty packets', async () => {
      const replyPromise = connection.genericCommand('tools echo 1 2', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id tools echo 1 2');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- \\`);
      await server.push(`#01 0 ${cmd.idStr} cont \\`);
      await server.push(`#01 0 ${cmd.idStr} cont 1\\`);
      await server.push(`#01 0 ${cmd.idStr} cont \\`);
      await server.push(`#01 0 ${cmd.idStr} cont 2`);

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: '1 2', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
    });

    it('times out if the first packet is missing', async () => {
      const replyPromise = connection.genericCommand('get pos', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id get pos');

      await server.push(`#01 0 ${cmd.idStr} cont 3 4`);

      // The error in this case is InvalidPacket, which is logged but
      // not visible through the public API.
      await expect(replyPromise).to.be.rejectedWith(RequestTimeoutException);
    });

    it('times out if the final packet is missing', async () => {
      const replyPromise = connection.genericCommand('get pos', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id get pos');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 1 2\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 3 4\\`);

      await expect(replyPromise).to.be.rejectedWith(RequestTimeoutException);
    });

    it('omits incomplete messages from info list responses', async () => {
      const replyPromise = connection.genericCommandMultiResponse('get pos', { device: 1 });

      const line = await server.pop();
      const cmd = parseCommand(line);
      expect(cmd.strNoId).to.eq('/1 0 id get pos');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseCommand(lineTerminate);
      expect(cmdTerminate.str).to.eq(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 1 2\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 3 4`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 5 6\\`);
      await server.push(`#01 0 ${cmd.idStr} cont 7 8\\`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      // The incomplete message is expected to trigger an invalid packet error
      // when the list termination message is received, but we cannot easily
      // test for invalid packet errors because they are not exposed in the API.

      expect(await replyPromise).to.deep.eq([{
        axisNumber: 0, data: '1 2 3 4', deviceAddress: 1, warningFlag: '--',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      }]);
    });

    it('throws error when word exceeds packet size (no device information)', async () => {
      const replyPromise = connection.genericCommandNoResponse(`tools echo ${_.repeat('a', 80)}`);
      const error = await expect(replyPromise).to.be.rejectedWith(InvalidArgumentException);
      expect(error.message).to.match(/Could not write word a+ which would exceed packet size limit \(80\) of ASCII protocol/);
    });
  });

  describe('renumberDevices', () => {
    it('returns correct number of devices that respond to the renumber', async () => {
      const renumberPromise = connection.renumberDevices();

      const line = await server.pop();
      expect(line).to.eq('/0 0 00 renumber 1');

      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@02 0 00 OK IDLE -- 0');
      await server.push('@03 0 00 OK IDLE -- 0');

      const devices = await renumberPromise;
      expect(devices).to.eq(3);
    });

    it('starts the renumber at the start value specified', async () => {
      const renumberPromise = connection.renumberDevices({ firstAddress: 5 });

      const line = await server.pop();
      expect(line).to.eq('/0 0 00 renumber 5');

      await server.push('@05 0 00 OK IDLE -- 0');

      const devices = await renumberPromise;
      expect(devices).to.eq(1);
    });

    it('resets device identities', async () => {
      const detectPromise = connection.detectDevices();

      await detect(server, [2], async () => await DeviceXLHM.identifyNumber(2, server));

      const devices = await detectPromise;
      expect(devices[0].identity!).to.not.eq(null);

      const renumberPromise = connection.renumberDevices();

      const renumberLine = await server.pop();
      const renumberCommand = parseCommand(renumberLine);
      expect(renumberCommand.strNoId).to.eq('/0 0 id renumber 1');

      await server.push(`@01 0 ${renumberCommand.id} OK IDLE -- 0`);

      const numberDevicesRenumbered = await renumberPromise;
      expect(numberDevicesRenumbered).to.eq(1);

      expect(() => devices[0].identity).to.throw(/Device number 2 is not identified\./);
    });

    it('throws exception when a device responds with an exception', async () => {
      const renumberPromise = connection.renumberDevices();

      const line = await server.pop();
      expect(line).to.eq('/0 0 00 renumber 1');

      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@02 0 00 OK IDLE -- 0');
      await server.push('@03 0 00 RJ IDLE -- FULL');

      await expect(renumberPromise).to.be.rejectedWith(CommandFailedException);
      const exception: CommandFailedException = await renumberPromise.catch(e => e);
      expect(exception.message).to.startWith('Command "renumber 1" rejected: FULL');
    });

    it('throws exception when no device responds', async () => {
      const renumberPromise = connection.renumberDevices();

      const line = await server.pop();
      expect(line).to.eq('/0 0 00 renumber 1');

      await expect(renumberPromise).to.be.rejectedWith(NoDeviceFoundException);
      const exception: NoDeviceFoundException = await renumberPromise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot find any device on the port. Make sure that you have opened correct port and devices are powered on.'
      );
    });

    it('throws exception if start value is invalid', async () => {
      await expect(connection.renumberDevices({ firstAddress: -5 })).to.be.rejectedWith(TypeError);
    });
  });

  describe('detectDevices', () => {
    it('returns devices which respond to broadcast', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      await detect(server, [7, 1, 3]);

      const devices = await detectPromise;
      expect(devices.map(device => device.deviceAddress)).to.deep.eq([1, 3, 7]);
    });

    it('throws exception when there is conflict in device numbers', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      const line = await server.pop();
      expect(line).to.eq('/0 0 00');

      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@03 0 00 OK IDLE -- 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await expect(detectPromise).to.be.rejectedWith(DeviceAddressConflictException);

      const error: DeviceAddressConflictException = await detectPromise.catch(e => e);
      expect(error.details.deviceAddresses).to.have.members([1, 1, 3]);
    });

    it('throws error on invalid address', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });

      await server.pop();
      await server.push('@00 0 00 OK IDLE -- 0');

      await expect(detectPromise).to.be.rejectedWith(InvalidDataException);
      const error: InvalidDataException = await detectPromise.catch(e => e);
      expect(error.message).to.eq('Invalid device address: 0');
    });

    it('identifies devices upon detection', async () => {
      const detectPromise = connection.detectDevices();

      await detect(server, [2, 5], async () => {
        await DeviceXLHM.identifyNumber(2, server);
        await DeviceXMCB2.identifyNumber(5, server);
      });

      const devices = await detectPromise;
      expect(devices.map(device => device.deviceAddress)).to.deep.eq([2, 5]);

      expect(devices[0].identity!.name).to.eq('X-LHM100A');
      expect(devices[1].identity!.name).to.eq('X-MCB2');
    });

    it('throws exception when no device responds', async () => {
      const detectPromise = connection.detectDevices();

      const line = await server.pop();
      expect(line).to.eq('/0 0 00');

      await expect(detectPromise).to.be.rejectedWith(NoDeviceFoundException);
      const exception: NoDeviceFoundException = await detectPromise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot find any device on the port. Make sure that you have opened correct port and devices are powered on.'
      );
    });

    it('throws an error when a device that does not support message IDs replies with RJ', async () => {
      const detectPromise = connection.detectDevices();

      const line = await server.pop();
      expect(line).to.eq('/0 0 00');

      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@03 0 RJ IDLE -- BADDATA');
      await server.push('@04 0 00 OK IDLE -- 0');

      await expect(detectPromise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await detectPromise.catch(e => e);
      expect(exception.message).to.eq(
        'Unsupported device with number 3 detected. Please contact Zaber support to upgrade your device.'
      );
    });
  });

  describe('forgetDevices', () => {
    it('forgets all devices', async () => {
      const detectPromise = connection.detectDevices();
      await detect(server, [2], async () => {
        await DeviceXLHM.identifyNumber(2, server);
      });
      await detectPromise;

      const device = connection.getDevice(2);
      expect(device.isIdentified).to.eq(true);
      connection.forgetDevices();
      expect(device.isIdentified).to.eq(false);
    });
    it('forgets all devices except specified', async () => {
      const detectPromise = connection.detectDevices();
      await detect(server, [2, 3], async () => {
        await DeviceXLHM.identifyNumber(2, server);
        await DeviceXLHM.identifyNumber(3, server);
      });
      await detectPromise;

      connection.forgetDevices([3]);
      expect(connection.getDevice(2).isIdentified).to.eq(false);
      expect(connection.getDevice(3).isIdentified).to.eq(true);
    });
  });

  describe('genericCommandMultiResponse', () => {
    it('handles broadcast responses', async () => {
      const replyPromise = connection.genericCommandMultiResponse('get version');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/0 0 id get version');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 6.28`);
      await server.push(`@03 0 ${cmd.idStr} OK IDLE FQ 7.06`);
      await server.push(`@02 0 ${cmd.idStr} OK IDLE NI 6.16`);
      await server.push(`@07 0 ${cmd.idStr} OK IDLE NB 6.30`);

      const result = await replyPromise;

      expect(result).to.deep.equal([
        {
          deviceAddress: 1, axisNumber: 0, data: '6.28', warningFlag: '--',
          messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 3, axisNumber: 0, data: '7.06', warningFlag: 'FQ',
          messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 2, axisNumber: 0, data: '6.16', warningFlag: 'NI',
          messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 7, axisNumber: 0, data: '6.30', warningFlag: 'NB',
          messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
      ]);
    });

    it('handles info list responses', async () => {
      const replyPromise = connection.genericCommandMultiResponse('help commands', { device: 1 });

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id help commands');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE WR 0`);
      await server.push(`#01 0 ${cmd.idStr}  help(...)              General Usage Information`);
      await server.push(`#01 0 ${cmd.idStr}  renumber(...)          Renumber All Devices`);
      await server.push(`#01 0 ${cmd.idStr}  home                   Move to Home Sensor`);
      await server.push(`#01 0 ${cmd.idStr}`);
      await server.push(`#01 0 ${cmd.idStr} Send '1 help CMD' for help on a specific command CMD.`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE WR 0`);

      const result = await replyPromise;

      expect(result).to.deep.equal([
        { deviceAddress: 1, axisNumber: 0, data: '0', warningFlag: 'WR', messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE' },
        {
          deviceAddress: 1, axisNumber: 0, data: ' help(...)              General Usage Information',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 1, axisNumber: 0, data: ' renumber(...)          Renumber All Devices',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 1, axisNumber: 0, data: ' home                   Move to Home Sensor',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 1, axisNumber: 0, data: '',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 1, axisNumber: 0, data: 'Send \'1 help CMD\' for help on a specific command CMD.',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
      ]);
    });

    itTime('stops waiting after terminating reply', async () => {
      const replyPromise = connection.genericCommandMultiResponse('scope print', { device: 1 });

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE WR 0`);
      await server.push(`#01 0 ${cmd.idStr} line1...`);
      await server.push(`#01 0 ${cmd.idStr} line2...`);

      const startTime = performance.now();
      await server.push(`@01 0 ${cmd.idStr} OK IDLE WR 0`);
      const result = await replyPromise;
      const endTime = performance.now();

      expect(endTime - startTime).to.be.onTime(0);
      expect(result.length).to.eq(3);
    });

    it('handles info list responses from broadcast commands', async () => {
      const replyPromise = connection.genericCommandMultiResponse('servo list paramsets');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/0 0 id servo list paramsets');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
      await server.push(`@02 0 ${cmd.idStr} OK IDLE WR 0`);
      await server.push(`@03 0 ${cmd.idStr} OK IDLE WR 0`);
      await server.push(`#03 0 ${cmd.idStr} paramset live`);
      await server.push(`#03 0 ${cmd.idStr} paramset staging`);
      await server.push(`#03 0 ${cmd.idStr} paramset default`);

      const result = await replyPromise;

      expect(result).to.deep.equal([
        {
          deviceAddress: 1, axisNumber: 0, data: '0',
          warningFlag: '--', messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 2, axisNumber: 0, data: '0',
          warningFlag: 'WR', messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 3, axisNumber: 0, data: '0',
          warningFlag: 'WR', messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
        },
        {
          deviceAddress: 3, axisNumber: 0, data: 'paramset live',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 3, axisNumber: 0, data: 'paramset staging',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
        {
          deviceAddress: 3, axisNumber: 0, data: 'paramset default',
          warningFlag: '', messageType: MessageType.INFO, replyFlag: '', status: ''
        },
      ]);
    });

    it('handles exceptions from broadcast and info commands', async () => {
      const replyPromise = connection.genericCommandMultiResponse('servo list paramsets');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/0 0 id servo list paramsets');

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
      await server.push(`#03 0 ${cmd.idStr} paramset live`);
      await server.push(`@02 0 ${cmd.idStr} RJ IDLE WR BADCOMMAND`);

      await expect(replyPromise).to.be.rejectedWith(BadCommandException);
      const exception: BadCommandException = await replyPromise.catch(e => e);
      expect(exception.message).to.startWith('Command "servo list paramsets" rejected: BADCOMMAND');
    });

    it('does not accept info messages as replies', async () => {
      const replyPromise = connection.genericCommandMultiResponse('help commands', { device: 1 });

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id help commands');

      await server.pop(); // terminator command

      await server.push(`#01 0 ${cmd.idStr}  help(...)              General Usage Information`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE WR 0`);

      await expect(replyPromise).to.be.rejectedWith(InvalidResponseException);
      const exception: InvalidResponseException = await replyPromise.catch(e => e);
      expect(exception.message).to.eq('Received a different message type (Info) when expecting a reply.');
      expect(exception.details).to.deep.equal({
        response: '&{Type:2 Device:1 Axis:0 ID:0 ReplyFlag: Status: WarningFlag: '
          + 'Data: help(...)              General Usage Information IsToBeContinued:false IsContinuation:false}',
      });
    });
  });

  describe('toString', () => {
    it('returns representation', () => {
      expect(connection.toString()).to.match(/^Connection \d+ \(ASCII TCP: 127\.0\.0\.1:\d+\)$/);
    });

    it('returns representation of closed connection', async () => {
      const waitForClose = connection.disconnected.toPromise();
      await connection.close();
      await waitForClose;

      expect(connection.toString()).to.match(/^Connection \d+ \(Closed\)$/);
    });
  });

  describe('Line continuations', () => {
    it('uses smallest comm.packet.size.max for continuations', async () => {
      const detectPromise = connection.detectDevices({ identifyDevices: false });
      await detect(server, [{ number: 1, packetSize: 60 }, { number: 2, packetSize: 30 }, 3]);
      await detectPromise;

      const promise = connection.genericCommandNoResponse(`tools echo ${_.range(10).map(() => 'a').join(' ')}`);
      const lines = await Promise.all([server.pop(true), server.pop(true)]);
      expect(lines[0].length).to.eq(29);
      expect(lines[1].length).to.eq(24);
      await promise;
    });

    it('resets the limit to default with when detectDevices called again', async () => {
      let detectPromise = connection.detectDevices({ identifyDevices: false });
      await detect(server, [{ number: 1, packetSize: 40 }]);
      await detectPromise;

      detectPromise = connection.detectDevices({ identifyDevices: false });
      await detect(server, [1]);
      await detectPromise;

      const promise = connection.genericCommandNoResponse(`tools echo ${_.range(30).map(() => 'a').join(' ')}`);
      const line = await server.pop(true);
      expect(line.length).to.eq(78);
      await promise;
    });
  });

  describe('internal mode', () => {
    beforeAll(() => Library.setInternalMode(true));
    afterAll(() => Library.setInternalMode(false));

    it('checks for the FF flag', async () => {
      const replyPromise = connection.genericCommand('get version', { device: 1 });

      const line = await server.pop();
      expect(line).to.eq('/1 0 00 get version');
      await server.push('@01 0 0 OK IDLE FF 7.22');

      expect(await replyPromise).to.deep.eq({
        axisNumber: 0, data: '7.22', deviceAddress: 1, warningFlag: 'FF',
        messageType: MessageType.REPLY, replyFlag: 'OK', status: 'IDLE'
      });
    });
  });
});
