import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice } from './identify';

enum Mode {
    OneLinearOneRotary,
    TwoLinear,
    TwoLinearDifferentScale,
    OneLinearOneEmpty,
}

enum FWType {
  FW6,
  FW7,
  FW7Proto,
}

export class DeviceXMCB2 {
  public static Mode = Mode;
  public static FWType = FWType;

  private static fwMap = {
    [FWType.FW6]: {
      version: '6.31',
      build: '1541',
    },
    [FWType.FW7]: {
      version: '7.32',
      build: '13718',
    },
    [FWType.FW7Proto]: {
      version: '7.99',
      build: '13754',
    },
  };

  public static async identify(
    device: Device, server: TestServer,
    mode: Mode = Mode.OneLinearOneRotary, fw: FWType = FWType.FW6
  ): Promise<void> {
    const promise = device.identify();

    await DeviceXMCB2.identifyNumber(device.deviceAddress, server, mode, fw);

    await promise;
  }

  public static async identifyNumber(
    device: number, server: TestServer,
    mode: Mode = Mode.OneLinearOneRotary, fw: FWType = FWType.FW6
  ): Promise<void> {
    const info: DeviceIdentificationInfo = {
      deviceid: 30222,
      serial: 1234,
      version: DeviceXMCB2.fwMap[fw],
      axes: DeviceXMCB2.peripherals[mode].map((id, idx) => ({
        peripheralid: id,
        peripheralSerialNumber: id > 0 ? 26000 + idx : 0,
        resolution: 64,
      })),
    };
    await identifyDevice(device, server, info);
  }

  private static peripherals = {
    [Mode.OneLinearOneRotary]: [43211, 46653],
    [Mode.TwoLinear]: [42122, 42112],
    [Mode.TwoLinearDifferentScale]: [43211, 40011],
    [Mode.OneLinearOneEmpty]: [43211, 0],
  };
}
