import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { identifyDevice } from './identify';

export class DeviceXJOY {
  public static async identify(device: Device, server: TestServer): Promise<void> {
    const promise = device.identify();
    await DeviceXJOY.identifyNumber(device.deviceAddress, server);
    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer): Promise<void> {
    await identifyDevice(device, server, {
      deviceid: 51000,
      serial: 6666,
      version: {
        version: '6.31',
        build: '1541'
      },
    });
  }
}
