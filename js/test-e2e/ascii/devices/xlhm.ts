import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice } from './identify';

enum FWType {
  FW6,
  FW6Proto,
  FW7,
  FW7_42,
  FW7_8,
}

export class DeviceXLHM {
  public static FWType = FWType;

  private static fwMap: Record<FWType, { version: string; build: string}> = {
    [FWType.FW6]: { version: '6.28', build: '1347' },
    [FWType.FW6Proto]: { version: '6.99', build: '0' },
    [FWType.FW7]: { version: '7.33', build: '14091' },
    [FWType.FW7_42]: { version: '7.42', build: '18298' },
    [FWType.FW7_8]: { version: '7.08', build: '3904' },
  };

  public static async identify(device: Device, server: TestServer, type: FWType = FWType.FW6): Promise<void> {
    const promise = device.identify();

    await DeviceXLHM.identifyNumber(device.deviceAddress, server, type);
    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer, type: FWType = FWType.FW6, useAssumedFwVersion = false): Promise<void> {
    const info: DeviceIdentificationInfo = {
      deviceid: 50081,
      serial: 1234,
      version: DeviceXLHM.fwMap[type],
      useAssumedFwVersion,
      axes: [{
        resolution: 64,
      }]
    };
    await identifyDevice(device, server, info);
  }
}
