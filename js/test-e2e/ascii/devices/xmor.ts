import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice } from './identify';

export class DeviceXMOR {
  public static async identify(device: Device, server: TestServer): Promise<void> {
    const promise = device.identify();
    await DeviceXMOR.identifyNumber(device.deviceAddress, server);
    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer): Promise<void> {
    const info: DeviceIdentificationInfo = {
      deviceid: 53115,
      serial: 1234,
      version: { version: '7.32', build: '13718' },
      axes: [{
        resolution: 64,
      }]
    };
    await identifyDevice(device, server, info);
  }
}
