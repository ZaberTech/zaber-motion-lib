import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice, PeripheralIdentificationInfo } from './identify';

export enum Mode {
  OneLinear,
  FourLinear,
  TwoLinearTwoRotary,
  One3rdParty,
}

export enum FWVersion {
  FW7Proto,
  FW7_21,
  FW7_26,
  FW7_27,
  FW7_30,
  FW7_33,
  FW7_35,
  FW7_37,
  FW7_38,
}

interface HardwareMod {
  controller?: boolean;
  axes?: number[];
}

export class DeviceXMCC4 {
  public static Mode = Mode;
  public static FWVersion = FWVersion;

  private static lineContinuationProtocol: DeviceIdentificationInfo['protocol'] = { maxPackets: 5, wordLen: 50 };
  private static getSyncProtocol: DeviceIdentificationInfo['protocol'] = { maxPackets: 5, wordLen: 50, maxSyncSettings: 12 };

  private static fwMap: Record<FWVersion, { version: string; build: string; protocol: DeviceIdentificationInfo['protocol'] }> = {
    [FWVersion.FW7Proto]: {
      version: '7.99',
      build: '13927',
      protocol: undefined,
    },
    [FWVersion.FW7_38]: {
      version: '7.38',
      build: '15708',
      protocol: this.getSyncProtocol,
    },
    [FWVersion.FW7_37]: {
      version: '7.37',
      build: '15516',
      protocol: this.getSyncProtocol,
    },
    [FWVersion.FW7_35]: {
      version: '7.35',
      build: '14985',
      protocol: this.getSyncProtocol,
    },
    [FWVersion.FW7_33]: {
      version: '7.33',
      build: '14091',
      protocol: this.lineContinuationProtocol,
    },
    [FWVersion.FW7_30]: {
      version: '7.30',
      build: '13144',
      protocol: this.lineContinuationProtocol,
    },
    [FWVersion.FW7_27]: {
      version: '7.27',
      build: '12025',
      protocol: this.lineContinuationProtocol,
    },
    [FWVersion.FW7_21]: {
      version: '7.21',
      build: '9827',
      protocol: undefined,
    },
    [FWVersion.FW7_26]: {
      version: '7.26',
      build: '11669',
      protocol: this.lineContinuationProtocol,
    },
  };

  public static async identify(
    device: Device, server: TestServer,
    mode: Mode = Mode.FourLinear, fw: FWVersion = FWVersion.FW7_21,
    hwMod: HardwareMod = {},
  ): Promise<void> {
    const promise = device.identify();

    await DeviceXMCC4.identifyNumber(device.deviceAddress, server, mode, fw, hwMod);

    await promise;
  }

  public static async identifyNumber(
    device: number, server: TestServer,
    mode: Mode = Mode.FourLinear, fw: FWVersion = FWVersion.FW7Proto,
    hwMod: HardwareMod = {},
  ): Promise<void> {
    const axes = DeviceXMCC4.peripherals[mode].map<PeripheralIdentificationInfo>((id, axis) => ({
      peripheralid: id,
      resolution: 64,
      hwMod: hwMod.axes?.includes(axis + 1)
    }));
    const info: DeviceIdentificationInfo = {
      deviceid: 30342,
      serial: 1234,
      version: DeviceXMCC4.fwMap[fw],
      hwMod: hwMod.controller,
      axes,
      protocol: this.fwMap[fw].protocol,
    };
    await identifyDevice(device, server, info);
  }

  private static peripherals = {
    [Mode.OneLinear]: [70250, 0, 0, 0],
    [Mode.FourLinear]: [43211, 43211, 43211, 43211],
    [Mode.TwoLinearTwoRotary]: [43211, 43211, 46657, 46657],
    [Mode.One3rdParty]: [88800, 0, 0, 0],
  };
}
