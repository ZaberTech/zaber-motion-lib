import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice } from './identify';

export class DeviceXSCA4 {
  public static async identify(device: Device, server: TestServer, serial = 1234): Promise<void> {
    const promise = device.identify();

    await DeviceXSCA4.identifyNumber(device.deviceAddress, server, serial);

    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer, serial: number): Promise<void> {
    const info: DeviceIdentificationInfo = {
      deviceid: 52007,
      serial,
      version: { version: '7.35', build: '14985' },
      axes: Array.from({ length: 4 }, (_, idx) => ({
        peripheralid: 70386,
        peripheralSerialNumber: 3003 + idx
      })),
    };
    await identifyDevice(device, server, info);
  }
}
