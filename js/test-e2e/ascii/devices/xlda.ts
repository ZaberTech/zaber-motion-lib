import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice } from './identify';

export class DeviceXLDA {
  public static async identify(device: Device, server: TestServer): Promise<void> {
    const promise = device.identify();
    await DeviceXLDA.identifyNumber(device.deviceAddress, server);
    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer): Promise<void> {
    const info: DeviceIdentificationInfo = {
      deviceid: 50852,
      serial: 1234,
      version: { version: '7.32', build: '13718' },
      axes: [{
        resolution: 1,
      }],
      protocol: {
        maxPackets: 5,
        wordLen: 50,
      },
    };
    await identifyDevice(device, server, info);
  }
}
