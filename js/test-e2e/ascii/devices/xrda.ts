import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { identifyDevice } from './identify';

export class DeviceXRDA {
  public static async identify(
    device: Device, server: TestServer
  ): Promise<void> {
    const promise = device.identify();

    await DeviceXRDA.identifyNumber(device.deviceAddress, server);

    await promise;
  }

  public static async identifyNumber(
    device: number, server: TestServer,
  ): Promise<void> {
    return identifyDevice(device, server, {
      deviceid: 50952,
      serial: 1234,
      version: {
        version: '7.15',
        build: '8617'
      },
      axes: [
        { peripheralid: 70315, peripheralSerialNumber: 1111, resolution: 1 },
        { peripheralid: 70316, peripheralSerialNumber: 1119, resolution: 1 },
      ]
    });
  }
}
