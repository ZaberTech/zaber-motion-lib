import { expect } from 'chai';
import { parseCommand } from '../protocol';
import { TestServer } from '../tcp';

export type PeripheralIdentificationInfo = {
  peripheralid?: number;
  peripheralSerialNumber?: number;
  resolution?: number;
  hwMod?: boolean;
  conversionTable?: unknown;
  label?: string;
};

export type DeviceIdentificationInfo = {
  deviceid: number;
  serial: number;
  version: {
    version: string;
    build: string;
  };
  useAssumedFwVersion?: boolean;
  label?: string;
  hwMod?: boolean;
  protocol?: {
    maxPackets: number;
    wordLen: number;
    maxSyncSettings?: number;
  };
  axes?: PeripheralIdentificationInfo[];
};

export async function identifyDevice(device: number, server: TestServer, info: DeviceIdentificationInfo): Promise<void> {
  let command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get deviceid`);
  await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.deviceid}`);

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get system.serial`);
  await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.serial}`);

  if (!info.useAssumedFwVersion) {
    command = parseCommand(await server.pop());
    expect(command.strNoId).to.eq(`/${device} 0 id get version`);
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.version.version}`);

    command = parseCommand(await server.pop());
    expect(command.strNoId).to.eq(`/${device} 0 id get version.build`);
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.version.build}`);
  }

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get device.hw.modified`);
  if (info.hwMod == null) {
    await server.push(`@${device} 0 ${command.id} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.hwMod ? 1 : 0}`);
  }

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get comm.command.packets.max`);
  if (!info.protocol) {
    await server.push(`@${device} 0 ${command.id} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.protocol.maxPackets}`);
  }

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get comm.word.size.max`);
  if (!info.protocol) {
    await server.push(`@${device} 0 ${command.id} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.protocol.wordLen}`);
  }

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get get.settings.max`);
  if (!info.protocol?.maxSyncSettings) {
    await server.push(`@${device} 0 ${command.id} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.protocol.maxSyncSettings}`);
  }

  command = parseCommand(await server.pop());
  expect(command.strNoId).to.eq(`/${device} 0 id get system.axiscount`);
  if (info.axes == null) {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- 0`);
  } else {
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${info.axes.length}`);

    for (let axis = 1; axis <= info.axes.length; axis++) {
      const axisInfo = info.axes[axis - 1];
      command = parseCommand(await server.pop());
      expect(command.strNoId).to.eq(`/${device} ${axis} id get resolution`);
      if (axisInfo.resolution == null) {
        await server.push(`@${device} ${axis} ${command.id} RJ IDLE -- BADCOMMAND`);
      } else {
        await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${axisInfo.resolution}`);
      }

      command = parseCommand(await server.pop());
      expect(command.strNoId).to.eq(`/${device} ${axis} id get peripheralid`);
      if (axisInfo.peripheralid == null) {
        await server.push(`@${device} 1 ${command.id} RJ IDLE -- BADCOMMAND`);
      } else {
        await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${axisInfo.peripheralid}`);

        command = parseCommand(await server.pop());
        expect(command.strNoId).to.eq(`/${device} ${axis} id get peripheral.serial`);
        if (axisInfo.peripheralSerialNumber == null) {
          await server.push(`@${device} ${axis} ${command.id} RJ IDLE -- BADCOMMAND`);
        } else {
          const serialNumber = axisInfo.peripheralid > 0 ? axisInfo.peripheralSerialNumber : 0;
          await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${serialNumber}`);
        }

        command = parseCommand(await server.pop());
        expect(command.strNoId).to.eq(`/${device} ${axis} id get peripheral.hw.modified`);
        if (axisInfo.hwMod == null) {
          await server.push(`@${device} ${axis} ${command.id} RJ IDLE -- BADCOMMAND`);
        } else {
          await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${axisInfo.hwMod ? 1 : 0}`);
        }
      }
    }

    for (let axis = 1; axis <= info.axes.length; axis++) {
      const axisInfo = info.axes[axis - 1];

      if (axisInfo.hwMod || (axisInfo.peripheralid == null && info.hwMod)) {
        command = parseCommand(await server.pop());
        expect(command.strNoId).to.eq(`/${device} ${axis} id storage axis get zaber.unit_conversions`);
        if (axisInfo.conversionTable) {
          const response = Buffer.from(JSON.stringify(axisInfo.conversionTable)).toString('base64');
          await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${response}`);
        } else {
          await server.push(`@${device} ${axis} ${command.id} RJ IDLE -- BADDATA`);
        }
      }
    }

    if (+info.version.build >= 11669) {
      command = parseCommand(await server.pop());
      expect(command.strNoId).to.eq(`/${device} ${command.axis} id storage get zaber.label`);
      if (info.label) {
        await server.push(`@${device} ${command.axis} ${command.id} OK IDLE -- ${Buffer.from(info.label).toString('base64')}`);
      } else  {
        await server.push(`@${device} ${command.axis} ${command.id} RJ IDLE -- BADDATA`);
      }

      for (const axis of info.axes.filter(axis => axis.peripheralid != null)) {
        command = parseCommand(await server.pop());
        expect(command.strNoId).to.eq(`/${device} ${command.axis} id storage axis get zaber.label`);
        if (axis.label) {
          await server.push(`@${device} ${command.axis} ${command.id} OK IDLE -- ${Buffer.from(axis.label).toString('base64')}`);
        } else  {
          await server.push(`@${device} ${command.axis} ${command.id} RJ IDLE -- BADDATA`);
        }
      }
    }
  }
}
