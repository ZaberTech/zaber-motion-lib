import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { DeviceIdentificationInfo, identifyDevice, PeripheralIdentificationInfo } from './identify';

export enum Mode {
  Linear,
  Rotary,
}

interface HardwareMod {
  controller?: boolean;
  axes?: number[];
}

export class DeviceXMCC1 {
  public static Mode = Mode;

  public static async identify(
    device: Device, server: TestServer,
    mode: Mode = Mode.Linear,
    hwMod: HardwareMod = {},
  ): Promise<void> {
    const promise = device.identify();

    await DeviceXMCC1.identifyNumber(device.deviceAddress, server, mode, hwMod);

    await promise;
  }

  public static async identifyNumber(
    device: number, server: TestServer,
    mode: Mode = Mode.Linear,
    hwMod: HardwareMod = {},
  ): Promise<void> {
    const axes = DeviceXMCC1.peripherals[mode].map<PeripheralIdentificationInfo>((id, axis) => ({
      peripheralid: id,
      resolution: 64,
      hwMod: hwMod.axes?.includes(axis + 1)
    }));
    const info: DeviceIdentificationInfo = {
      deviceid: 30342,
      serial: 1234,
      version: { version: '7.35', build: '13927' },
      hwMod: hwMod.controller,
      axes,
      protocol: { maxPackets: 5, wordLen: 50, maxSyncSettings: 12 },
    };
    await identifyDevice(device, server, info);
  }

  private static peripherals = {
    [Mode.Linear]: [70250],
    [Mode.Rotary]: [46657],
  };
}
