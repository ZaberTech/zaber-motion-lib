import { Device } from '../../../src/ascii';
import { TestServer } from '../tcp';
import { identifyDevice } from './identify';

export class DeviceXLCA {
  public static async identify(device: Device, server: TestServer): Promise<void> {
    const promise = device.identify();

    await DeviceXLCA.identifyNumber(device.deviceAddress, server);

    await promise;
  }

  public static async identifyNumber(device: number, server: TestServer): Promise<void> {
    await identifyDevice(device, server, {
      deviceid: 52005,
      serial: 1234,
      version: {
        version: '7.09',
        build: '3906'
      },
      axes: [
        { peripheralid: 46663, peripheralSerialNumber: 27011 },
        { peripheralid: 46664, peripheralSerialNumber: 27013 },
        { peripheralid: 46665, peripheralSerialNumber: 27017 },
        { peripheralid: 46666, peripheralSerialNumber: 27019 },
      ]
    });
  }
}
