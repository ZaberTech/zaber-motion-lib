import { expect } from 'chai';

import { Connection, DeviceIO, Device, DigitalOutputAction, IoPortType } from '../../src/ascii';
import { InvalidArgumentException, IoChannelOutOfRangeException, Time } from '../../src';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXMCC4 } from './devices/xmcc4';

const server = new TestServer();

describe('DeviceIO', () => {
  let connection: Connection;
  let device: Device;
  let deviceIO: DeviceIO;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    deviceIO = device.io;
    await server.waitForConnection();

    await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_38);
    connection.resetIds();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('gets all io correctly', () => {
    it('gets the values of all digital input channels', async () => {
      const promise = deviceIO.getAllDigitalInputs();

      expect(await server.pop()).to.eq('/1 0 00 io get di');
      await server.push('@01 0 00 OK IDLE -- 0 1 0 0');

      const values = await promise;
      expect(values).to.deep.eq([false, true, false, false]);
    });

    it('gets the values of all digital output channels', async () => {
      const promise = deviceIO.getAllDigitalOutputs();

      expect(await server.pop()).to.eq('/1 0 00 io get do');
      await server.push('@01 0 00 OK IDLE -- 0 1 1 0');

      const values = await promise;
      expect(values).to.deep.eq([false, true, true, false]);
    });

    it('gets the values of all analog input channels', async () => {
      const promise = deviceIO.getAllAnalogInputs();

      expect(await server.pop()).to.eq('/1 0 00 io get ai');
      await server.push('@01 0 00 OK IDLE -- 11.7125 5.7125 11.7000 1.7125');

      const values = await promise;
      expect(values).to.deep.eq([11.7125, 5.7125, 11.7000, 1.7125]);
    });

    it('gets the values of all analog output channels', async () => {
      const promise = deviceIO.getAllAnalogOutputs();

      expect(await server.pop()).to.eq('/1 0 00 io get ao');
      await server.push('@01 0 00 OK IDLE -- 0.0000 1.1111 2.2222 3.3333');

      const values = await promise;
      expect(values).to.deep.eq([0.0000, 1.1111, 2.2222, 3.3333]);
    });
  });

  describe('gets individual io channel correctly', () => {
    it('gets the value of a digital input channel', async () => {
      const promise = deviceIO.getDigitalInput(1);

      expect(await server.pop()).to.eq('/1 0 00 io get di 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      const value = await promise;
      expect(value).to.deep.eq(false);
    });

    it('gets the value of a digital output channel', async () => {
      const promise = deviceIO.getDigitalOutput(2);

      expect(await server.pop()).to.eq('/1 0 00 io get do 2');
      await server.push('@01 0 00 OK IDLE -- 1');

      const value = await promise;
      expect(value).to.deep.eq(true);
    });

    it('gets the value of an analog input channel', async () => {
      const promise = deviceIO.getAnalogInput(3);

      expect(await server.pop()).to.eq('/1 0 00 io get ai 3');
      await server.push('@01 0 00 OK IDLE -- 11.1111');

      const value = await promise;
      expect(value).to.deep.eq(11.1111);
    });

    it('gets the value of an analog output channel', async () => {
      const promise = deviceIO.getAnalogOutput(4);

      expect(await server.pop()).to.eq('/1 0 00 io get ao 4');
      await server.push('@01 0 00 OK IDLE -- 5.5555');

      const value = await promise;
      expect(value).to.deep.eq(5.5555);
    });
  });

  describe('throws exception for getting an out of range channel', () => {
    it('gets an out of range di channel and throws exception', async () => {
      const promise = deviceIO.getDigitalInput(5);

      expect(await server.pop()).to.eq('/1 0 00 io get di 5');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
      const exception: IoChannelOutOfRangeException = await promise.catch(e => e);
      expect(exception.message).to.
        eq('Channel number 5 is out of range for channel type \'di\'. Please check the number of io channels for this device.');
    });
  });

  describe('sets all output channels correctly', () => {
    it('sets the values of all digital output channels', async () => {
      const promise = deviceIO.setAllDigitalOutputs([
        DigitalOutputAction.ON,
        DigitalOutputAction.OFF,
        DigitalOutputAction.TOGGLE,
        DigitalOutputAction.KEEP
      ]);

      expect(await server.pop()).to.eq('/1 0 00 io set do port 1 0 t k');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets the values of all analog output channels', async () => {
      const promise = deviceIO.setAllAnalogOutputs([1.1, 2.2, 3.3, 4.4]);

      expect(await server.pop()).to.eq('/1 0 00 io set ao port 1.100 2.200 3.300 4.400');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('sets individual output channels correctly', () => {
    it('sets a digital output channel to be on', async () => {
      const promise = deviceIO.setDigitalOutput(2, DigitalOutputAction.ON);

      expect(await server.pop()).to.eq('/1 0 00 io set do 2 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a digital output channel to be off', async () => {
      const promise = deviceIO.setDigitalOutput(1, DigitalOutputAction.OFF);

      expect(await server.pop()).to.eq('/1 0 00 io set do 1 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a digital output channel to toggle', async () => {
      const promise = deviceIO.setDigitalOutput(1, DigitalOutputAction.TOGGLE);

      expect(await server.pop()).to.eq('/1 0 00 io set do 1 t');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a digital output channel to keep', async () => {
      const promise = deviceIO.setDigitalOutput(1, DigitalOutputAction.KEEP);

      expect(await server.pop()).to.eq('/1 0 00 io set do 1 k');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets the values of a analog output channel', async () => {
      const promise = deviceIO.setAnalogOutput(3, 1.234567);

      expect(await server.pop()).to.eq('/1 0 00 io set ao 3 1.235');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('Sets the low-pass filter for analog input channels', () => {
    it('sets the cutoff frequency', async () => {
      const setPromise = deviceIO.setAnalogInputLowpassFilter(1, 2500); // TODO: add Units when 7.40 comes out

      expect(await server.pop()).to.eq('/1 0 00 set io.ai.1.fc 2500');
      await server.push('@01 0 00 OK IDLE -- 0');

      await setPromise;
    });

    it('disables the low-pass filter', async () => {
      const promise = deviceIO.setAnalogInputLowpassFilter(2, 0);

      expect(await server.pop()).to.eq('/1 0 00 set io.ai.2.fc 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('throws exception for setting an out of range channel', () => {
    it('sets an out of range di channel and throws exception', async () => {
      const promise = deviceIO.setDigitalOutput(6, DigitalOutputAction.OFF);

      expect(await server.pop()).to.eq('/1 0 00 io set do 6 0');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
      const exception: IoChannelOutOfRangeException = await promise.catch(e => e);
      expect(exception.message).to.
        eq('Channel number 6 is out of range for channel type \'do\'. Please check the number of io channels for this device.');
    });
  });

  describe('gets info for device io', () => {
    it('gets info for device io', async () => {
      const promise = deviceIO.getChannelsInfo();

      expect(await server.pop()).to.eq('/1 0 00 io info');
      await server.push('@01 0 00 OK IDLE -- 1 2 3 4');

      expect(await promise).to.deep.eq({
        numberAnalogOutputs: 1,
        numberAnalogInputs: 2,
        numberDigitalOutputs: 3,
        numberDigitalInputs: 4,
      });
    });
    it('returns empty info for BADCOMMAND', async () => {
      const promise = deviceIO.getChannelsInfo();

      expect(await server.pop()).to.eq('/1 0 00 io info');
      await server.push('@01 0 00 RJ IDLE -- BADCOMMAND');

      expect(await promise).to.deep.eq({
        numberAnalogOutputs: 0,
        numberAnalogInputs: 0,
        numberDigitalOutputs: 0,
        numberDigitalInputs: 0,
      });
    });
  });

  describe('getting and setting channel labels', () => {
    it('sets labels for IO channels', async () => {
      const setAiLabel = deviceIO.setLabel(IoPortType.ANALOG_INPUT, 1, 'Pokemon Sensor');
      expect(await server.pop()).to.eq('/1 0 00 storage set zaber.label.io.ai.1 UG9rZW1vbiBTZW5zb3I=');
      await server.push('@01 0 00 OK IDLE -- 0');
      await setAiLabel;

      const setDoLabel = deviceIO.setLabel(IoPortType.DIGITAL_OUTPUT, 2, 'Digimon Simulator');
      expect(await server.pop()).to.eq('/1 0 01 storage set zaber.label.io.do.2 RGlnaW1vbiBTaW11bGF0b3I=');
      await server.push('@01 0 01 OK IDLE -- 0');
      await setDoLabel;
    });

    it('erases labels for IO channels', async () => {
      const setAiLabel = deviceIO.setLabel(IoPortType.ANALOG_INPUT, 1);
      expect(await server.pop()).to.eq('/1 0 00 storage erase zaber.label.io.ai.1');
      await server.push('@01 0 00 OK IDLE -- 0');
      await setAiLabel;

      const setDoLabel = deviceIO.setLabel(IoPortType.DIGITAL_OUTPUT, 2, '');
      expect(await server.pop()).to.eq('/1 0 01 storage erase zaber.label.io.do.2');
      await server.push('@01 0 01 OK IDLE -- 0');
      await setDoLabel;
    });

    it('gets labels for IO channels', async () => {
      const getAiLabel = deviceIO.getLabel(IoPortType.ANALOG_INPUT, 1);
      expect(await server.pop()).to.eq('/1 0 00 storage get zaber.label.io.ai.1');
      await server.push('@01 0 00 OK IDLE -- UG9rZW1vbiBTZW5zb3I=');
      expect(await getAiLabel).to.eq('Pokemon Sensor');

      const getDoLabel = deviceIO.getLabel(IoPortType.DIGITAL_OUTPUT, 2);
      expect(await server.pop()).to.eq('/1 0 01 storage get zaber.label.io.do.2');
      await server.push('@01 0 01 OK IDLE -- RGlnaW1vbiBTaW11bGF0b3I=');
      expect(await getDoLabel).to.eq('Digimon Simulator');
    });

    it('gets labels for all IO channels', async () => {
      const getLabels = deviceIO.getAllLabels();
      expect(await server.pop()).to.eq('/1 0 00 storage print prefix zaber.label.io');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 set zaber.label.io.do.2 RGlnaW1vbiBTaW11bGF0b3I=');
      await server.push('#01 0 00 set zaber.label.io.ai.1 UG9rZW1vbiBTZW5zb3I=');
      await server.push('@01 0 00 OK IDLE -- 0');
      const sortedLabels = (await getLabels).sort((a, b) => a.channelNumber - b.channelNumber);
      expect(sortedLabels).to.deep.eq([
        { portType: IoPortType.ANALOG_INPUT, channelNumber: 1, label: 'Pokemon Sensor' },
        { portType: IoPortType.DIGITAL_OUTPUT, channelNumber: 2, label: 'Digimon Simulator' },
      ]);
    });
  });

  describe('sets scheduled digital output channels', () => {
    it('sets a digital output channel to be on and then off', async () => {
      const promise = deviceIO.setDigitalOutputSchedule(1, DigitalOutputAction.ON, DigitalOutputAction.OFF, 50, Time.MILLISECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set do 1 1 schedule 50.0 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a digital output channel to be toggled in the future', async () => {
      const promise = deviceIO.setDigitalOutputSchedule(2, DigitalOutputAction.KEEP, DigitalOutputAction.TOGGLE, 2, Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set do 2 k schedule 2000.0 t');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a schedule for an invalid channel', async () => {
      const promise = deviceIO.setDigitalOutputSchedule(5, DigitalOutputAction.KEEP, DigitalOutputAction.ON, 1, Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set do 5 k schedule 1000.0 1');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
    });

    it('sets a schedule for all channels', async () => {
      const promise = deviceIO.setAllDigitalOutputsSchedule(
        [DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP],
        [DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.ON],
        1,
        Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set do port k k k k schedule 1000.0 1 1 1 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a schedule for unequal number of channels', async () => {
      const promise = deviceIO.setAllDigitalOutputsSchedule(
        [DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP],
        [DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.ON],
        20);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('sets a schedule for invalid number of channels', async () => {
      const promise = deviceIO.setAllDigitalOutputsSchedule(
        [DigitalOutputAction.KEEP, DigitalOutputAction.KEEP, DigitalOutputAction.KEEP],
        [DigitalOutputAction.ON, DigitalOutputAction.ON, DigitalOutputAction.ON],
        20);

      expect(await server.pop()).to.eq('/1 0 00 io set do port k k k schedule 20.0 1 1 1');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });
  });

  describe('cancels a schedule for digital channels', () => {
    it('Cancels a schedule for a single digital channel', async () => {
      const promise = deviceIO.cancelDigitalOutputSchedule(1);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule do 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels a schedule for a single invalid digital channel', async () => {
      const promise = deviceIO.cancelDigitalOutputSchedule(0);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule do 0');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
    });

    it('Cancels schedules for all digital channels', async () => {
      const promise = deviceIO.cancelAllDigitalOutputsSchedule();

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule do port');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels schedules for some digital channels', async () => {
      const promise = deviceIO.cancelAllDigitalOutputsSchedule([true, true, false, false]);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule do port 1 1 0 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels schedules for invalid number of digital channels', async () => {
      const promise = deviceIO.cancelAllDigitalOutputsSchedule([true, true]);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule do port 1 1');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });
  });

  describe('sets scheduled analog output channels', () => {
    it('sets an analog output channel to be high and then low', async () => {
      const promise = deviceIO.setAnalogOutputSchedule(1, 0, 5, 50, Time.MILLISECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set ao 1 0 schedule 50.0 5');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets an analog output channel to be the same in the future', async () => {
      const promise = deviceIO.setAnalogOutputSchedule(2, 3.33, 3.33, 2, Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set ao 2 3.33 schedule 2000.0 3.33');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a schedule for an invalid channel', async () => {
      const promise = deviceIO.setAnalogOutputSchedule(5, 1.25, 0, 1, Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set ao 5 1.25 schedule 1000.0 0');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
    });

    it('sets a schedule for all analog channels', async () => {
      const promise = deviceIO.setAllAnalogOutputsSchedule(
        [0, 1.11, 3.33, 5],
        [7.25, 2.5, 2, 0],
        1,
        Time.SECONDS);

      expect(await server.pop()).to.eq('/1 0 00 io set ao port 0 1.11 3.33 5 schedule 1000.0 7.25 2.5 2 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('sets a schedule for unequal number of channels', async () => {
      const promise = deviceIO.setAllAnalogOutputsSchedule(
        [1, 2, 3],
        [0, 0, 0, 0],
        20);

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });

    it('sets a schedule for invalid number of channels', async () => {
      const promise = deviceIO.setAllAnalogOutputsSchedule(
        [1, 2, 3],
        [0, 0, 0],
        20);

      expect(await server.pop()).to.eq('/1 0 00 io set ao port 1 2 3 schedule 20.0 0 0 0');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });
  });

  describe('cancels a schedule', () => {
    it('Cancels a schedule for a single analog channel', async () => {
      const promise = deviceIO.cancelAnalogOutputSchedule(1);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule ao 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels a schedule for a single invalid analog channel', async () => {
      const promise = deviceIO.cancelAnalogOutputSchedule(0);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule ao 0');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(IoChannelOutOfRangeException);
    });

    it('Cancels schedules for all analog channels', async () => {
      const promise = deviceIO.cancelAllAnalogOutputsSchedule();

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule ao port');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels schedules for some analog channels', async () => {
      const promise = deviceIO.cancelAllAnalogOutputsSchedule([false, true, false, true]);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule ao port 0 1 0 1');
      await server.push('@01 0 00 OK IDLE -- 0');

      await promise;
    });

    it('Cancels schedules for invalid number of analog channels', async () => {
      const promise = deviceIO.cancelAllAnalogOutputsSchedule([true, true]);

      expect(await server.pop()).to.eq('/1 0 00 io cancel schedule ao port 1 1');
      await server.push('@01 0 00 RJ IDLE -- BADDATA');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
    });
  });
});
