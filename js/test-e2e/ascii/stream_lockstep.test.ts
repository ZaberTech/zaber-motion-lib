import { expect } from 'chai';

import {
  Acceleration, Length, Velocity, Angle, AngularVelocity, Library, DeviceDbSourceType, StreamSetupFailedException,
} from '../../src';
import { Connection, Device, Stream, StreamAxisType } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { DeviceXMCB2 } from './devices/xmcb2';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { MASTER_DEVICE_DB } from '../constants';

describe('Stream -- lockstep', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let eqAndPushX: (pushStr: string, popStr: string) => Promise<unknown>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);

    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, MASTER_DEVICE_DB);
  });

  afterAll(async () => {
    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE);
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  it('allows to setup live stream with lockstep', async () => {
    const stream = device.streams.getStream(1);
    const setupPromise = stream.setupLiveComposite({ axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
    await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
    await eqAndPushX('lockstep 3 info', 'OK IDLE -- 1 2 500 0');
    await eqAndPushX('stream 1 setup live lockstep 3', 'OK IDLE -- 0');
    await setupPromise;
  });

  it('allows to setup store stream with lockstep', async () => {
    const stream = device.streams.getStream(1);
    const buffer = device.streams.getBuffer(1);
    const setupPromise = stream.setupStoreComposite(buffer, { axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
    await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
    await eqAndPushX('lockstep 3 info', 'OK IDLE -- 1 2 500 0');
    await eqAndPushX('stream 1 setup store 1 1', 'OK IDLE -- 0');
    await setupPromise;
  });

  it('throw error when lockstep does not exist', async () => {
    const stream = device.streams.getStream(1);
    const setupPromise = stream.setupLiveComposite({ axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
    await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
    await eqAndPushX('lockstep 3 info', 'OK IDLE -- disabled');
    await expect(setupPromise).to.be.rejectedWith(StreamSetupFailedException);
    const error: StreamSetupFailedException = await setupPromise.catch(e => e);
    expect(error.message).to.eq('Lockstep 3 is disabled.');
  });

  describe('unit conversions', () => {
    let stream: Stream;
    const A = { value: 1090.19, unit: Length.mm };

    describe('X-MCB2, one linear, one rotary', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.OneLinearOneRotary, DeviceXMCB2.FWType.FW7Proto);
        connection.resetIds();
      });

      describe('live stream', () => {
        beforeEach(async () => {
          stream = device.streams.getStream(1);
          const setupPromise = stream.setupLiveComposite({ axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
          await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
          await eqAndPushX('lockstep 3 info', 'OK IDLE -- 1 2 500 0');
          await eqAndPushX('stream 1 setup live lockstep 3', 'OK IDLE -- 0');
          await setupPromise;
        });

        it('performs unit conversions', async () => {
          const lineAbsPromise = stream.lineAbsolute(A);
          await eqAndPushX('stream 1 line abs 8790193', 'OK IDLE -- 0');
          await lineAbsPromise;

          const setMaxSpeedPromise = stream.setMaxSpeed(10, Velocity['in/s']);
          await eqAndPushX('stream 1 set maxspeed 3355443', 'OK IDLE -- 0');
          await setMaxSpeedPromise;

          const setAccelPromise = stream.setMaxCentripetalAcceleration(10, Acceleration['cm/s²']);
          await eqAndPushX('stream 1 set centripaccel 132', 'OK IDLE -- 0');
          await setAccelPromise;
        });

        describe('toString', () => {
          it('returns proper string', async () => {
            expect(stream.toString()).to.eq('Stream 1 (Live) -> Axes [Lockstep 3] -> Device 1');
          });
        });

        describe('axes property', () => {
          it('returns a streams cached axes', async () => {
            expect(stream.axes).to.deep.eq([
              { axisNumber: 3, axisType: StreamAxisType.LOCKSTEP },
            ]);
          });
        });

        describe('waitUntilIdle', () => {
          it('goes over physical axes to determine errors', async () => {
            const promise = stream.waitUntilIdle();
            await eqAndPush(server, 1, 1, '', 'OK IDLE -- 0');
            await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE -- 0');
            await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE -- 0');
            await promise;
          });
        });
      });

      describe('store stream', () => {
        beforeEach(async () => {
          stream = device.streams.getStream(1);
          const buffer = device.streams.getBuffer(1);
          const setupPromise = stream.setupStoreComposite(buffer, { axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
          await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
          await eqAndPushX('lockstep 3 info', 'OK IDLE -- 1 2 500 0');
          await eqAndPushX('stream 1 setup store 1 1', 'OK IDLE -- 0');
          await setupPromise;
        });

        it('performs unit conversions', async () => {
          const lineAbsPromise = stream.lineAbsolute(A);
          await eqAndPushX('stream 1 line abs 8790193', 'OK IDLE -- 0');
          await lineAbsPromise;
        });

        describe('toString', () => {
          it('returns proper string', async () => {
            expect(stream.toString()).to.eq('Stream 1 (Store) -> Buffer 1 (Axes [Lockstep 3]) -> Device 1');
          });
        });
      });

      describe('swapped lockstep axis', () => {
        beforeEach(async () => {
          stream = device.streams.getStream(1);
          const setupPromise = stream.setupLiveComposite({ axisNumber: 3, axisType: StreamAxisType.LOCKSTEP });
          await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
          await eqAndPushX('lockstep 3 info', 'OK IDLE -- 2 1 500 0');
          await eqAndPushX('stream 1 setup live lockstep 3', 'OK IDLE -- 0');
          await setupPromise;
        });

        it('performs unit conversions', async () => {
          const lineAbsPromise = stream.lineAbsolute({ value: 100, unit: Angle.DEGREES });
          await eqAndPushX('stream 1 line abs 17778', 'OK IDLE -- 0');
          await lineAbsPromise;

          const setMaxSpeedPromise = stream.setMaxSpeed(10, AngularVelocity.DEGREES_PER_SECOND);
          await eqAndPushX('stream 1 set maxspeed 2913', 'OK IDLE -- 0');
          await setMaxSpeedPromise;
        });
      });
    });
  });
});
