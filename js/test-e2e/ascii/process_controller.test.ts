import { expect } from 'chai';

import { Connection } from '../../src/ascii';
import { Process, ProcessController, ProcessControllerMode, ProcessControllerSourceSensor } from '../../src/product';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { parseCommand } from './protocol';
import { AbsoluteTemperature, InvalidArgumentException, Time, Voltage } from '../../src';
import { DeviceXMCC4 } from './devices/xmcc4';

import assert from 'assert';
import { DeviceXSCA4 } from './devices/xsca4';

const server = new TestServer();

describe('Process Controller', () => {
  let connection: Connection;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('Verification', () => {
    it('will not create a process controller from an identified motion controller', async () => {
      const device = connection.getDevice(1);
      await DeviceXMCC4.identify(device, server);
      assert.throws(() => { new ProcessController(device) }, InvalidArgumentException, 'This device is not a Process Controller');
    });

    it('can detect process controllers', async () => {
      const promiseOfControllers = ProcessController.detect(connection, false);
      expect(await server.pop()).to.eq('/0 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@02 0 00 OK IDLE -- 0');
      expect(await server.pop()).to.eq('/1 1 01 get process.control.mode');
      await server.push('@01 1 01 RJ IDLE -- BADCOMMAND');
      expect(await server.pop()).to.eq('/2 1 02 get process.control.mode');
      await server.push('@02 1 02 OK IDLE -- 0');
      expect(await server.pop()).to.eq('/2 0 03 get comm.packet.size.max');
      await server.push('@02 0 03 OK IDLE -- 80');
      expect((await promiseOfControllers).length).to.eq(1);
    });
  });

  describe('Processes', () => {
    let controller: ProcessController;
    let process: Process;

    beforeEach(async () => {
      const device = connection.getDevice(1);
      await DeviceXSCA4.identify(device, server);
      controller = new ProcessController(device);
      process = controller.getProcess(1);
    });

    afterEach(async () => {
      await connection.close();
      server.closeSocket();
    });

    it('enables', async () => {
      const promise = process.enable();
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('driver enable');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('disables', async () => {
      const promise = process.enable(false);
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('driver disable');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('turns on', async () => {
      const promise = process.on();
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('process on');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('turns on for time', async () => {
      const promise = process.on(4000);
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('process on 4000.0');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('turns on with unit', async () => {
      const promise = process.on(5, Time.SECONDS);
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('process on 5000.0');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('turns off', async () => {
      const promise = process.off();
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('process off');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('gets mode', async () => {
      const promise = process.getMode();
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('get process.control.mode');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      expect(await promise).to.equal(ProcessControllerMode.MANUAL);
    });

    it('sets mode', async () => {
      const promise = process.setMode(ProcessControllerMode.ON_OFF);
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('set process.control.mode 3');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('gets source', async () => {
      const promise = process.getSource();
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('get process.control.source');
      await server.push(`@1 1 ${command.id} OK IDLE -- 21`);
      expect(await promise).to.deep.equal({ sensor: ProcessControllerSourceSensor.ANALOG_INPUT, port: 1 });
    });

    it('sets source', async () => {
      const promise = process.setSource({ sensor: ProcessControllerSourceSensor.THERMISTOR, port: 2 });
      const command = parseCommand(await server.pop());
      expect(command.data).to.eq('set process.control.source 12');
      await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
      await promise;
    });

    it('gets thermistor input', async () => {
      const promise = process.getInput();
      const sourceCommand = parseCommand(await server.pop());
      expect(sourceCommand.data).to.eq('get process.control.source');
      await server.push(`@1 1 ${sourceCommand.id} OK IDLE -- 12`);
      const inputCommand = parseCommand(await server.pop());
      expect(inputCommand.data).to.eq('get sensor.temperature.2');
      await server.push(`@1 0 ${inputCommand.id} OK IDLE -- 12.3`);
      expect(await promise).to.deep.equal({ value: 12.3, unit: AbsoluteTemperature.DEGREES_CELSIUS });
    });

    it('gets analog input', async () => {
      const promise = process.getInput();
      const sourceCommand = parseCommand(await server.pop());
      expect(sourceCommand.data).to.eq('get process.control.source');
      await server.push(`@1 1 ${sourceCommand.id} OK IDLE -- 21`);
      const inputCommand = parseCommand(await server.pop());
      expect(inputCommand.data).to.eq('io get ai 1');
      await server.push(`@1 0 ${inputCommand.id} OK IDLE -- 12.3`);
      expect(await promise).to.deep.equal({ value: 12.3, unit: Voltage.V });
    });

    it('bridges', async () => {
      const promiseIsNotBridge = process.isBridge();
      const isNotBridgeCommand = parseCommand(await server.pop());
      expect(isNotBridgeCommand.data).to.eq('get driver.bipolar');
      await server.push(`@1 1 ${isNotBridgeCommand.id} OK IDLE -- 0`);
      expect(await promiseIsNotBridge).to.deep.equal(false);

      const promiseBridge = process.bridge();
      const bridgeCommand = parseCommand(await server.pop());
      expect(bridgeCommand.data).to.eq('set driver.bipolar 1');
      await server.push(`@1 1 ${bridgeCommand.id} OK IDLE -- 0`);
      await promiseBridge;

      const promiseIsBridge = process.isBridge();
      const isBridgeCommand = parseCommand(await server.pop());
      expect(isBridgeCommand.data).to.eq('get driver.bipolar');
      await server.push(`@1 1 ${isBridgeCommand.id} OK IDLE -- 1`);
      expect(await promiseIsBridge).to.deep.equal(true);
    });

    it('does not set or check bridge status of even-numbered processes', async () => {
      const process2 = controller.getProcess(2);

      const promiseIsNotBridge = process2.isBridge();
      const isNotBridgeCommand = parseCommand(await server.pop());
      expect(isNotBridgeCommand.axis).to.eq(1);
      expect(isNotBridgeCommand.data).to.eq('get driver.bipolar');
      await server.push(`@1 1 ${isNotBridgeCommand.id} OK IDLE -- 1`);
      expect(await promiseIsNotBridge).to.deep.equal(true);

      await expect(process2.bridge()).to.be.rejectedWith(
        InvalidArgumentException, 'Cannot create a bridge from process 2. Bridges must be created from odd-numbered processes'
      );
    });

    describe('toString', () => {
      it('returns a string representation of the process', () => {
        expect(process.toString()).to.startWith('Process 1 -> Device 1 SN: 1234 (X-SCA4) -> Connection');
      });
      it('returns a string representation of the controller', () => {
        expect(controller.toString()).to.startWith('Device 1 SN: 1234 (X-SCA4) -> Connection');
      });
      it('returns representation for closed connection', async () => {
        await connection.close();
        expect(process.toString()).to.eq('Axis 1 -> Device 1 -> Connection Closed');
        expect(controller.toString()).to.eq('Device 1 -> Connection Closed');
      });
    });
  });
});
