import { expect } from 'chai';
import _ from 'lodash';
import { lastValueFrom } from 'rxjs';

import { ConnectionClosedException, TransportAlreadyUsedException, ConnectionFailedException } from '../../src';
import { Connection, Transport } from '../../src/ascii';
import { parseCommand } from './protocol';

describe('Transport (ASCII)', () => {
  it('routes the communication to backend', async () => {
    const backend = Transport.open();
    const connection = await Connection.openCustom(backend);

    const promise = connection.genericCommand('home', { device: 1 });

    const message = await backend.read();
    expect(message).to.eq('/1 0 00 home:36');
    const nextRead = expect(backend.read()).to.be.rejectedWith(ConnectionClosedException);

    await backend.write('@01 0 0 OK BUSY -- 0');
    expect(await promise).to.deep.include({
      deviceAddress: 1,
      status: 'BUSY',
    });

    await connection.close();
    await backend.close();
    await nextRead;
  });

  it('closing from backend closes the connection and backend', async () => {
    const backend = Transport.open();
    const connection = await Connection.openCustom(backend);

    const closeEvent = lastValueFrom(connection.disconnected);

    const backendReadPromise = backend.read();
    const backendReadPromiseExpect = expect(backendReadPromise).to.be.rejectedWith(ConnectionClosedException);

    await backend.close();

    const backendWritePromise = backend.write('data');
    const connectionWritePromise = connection.genericCommandNoResponse('');

    await Promise.all([
      backendReadPromiseExpect,
      expect(backendWritePromise).to.be.rejectedWith(ConnectionClosedException),
      expect(connectionWritePromise).to.be.rejectedWith(ConnectionClosedException),
    ]);
    expect(await closeEvent).to.be.an.instanceof(ConnectionClosedException);
  });

  it('closing from connection closes the connection and backend', async () => {
    const backend = Transport.open();
    const connection = await Connection.openCustom(backend);

    const closeEvent = lastValueFrom(connection.disconnected);

    const backendReadPromise = backend.read();
    const backendReadPromiseExpect = expect(backendReadPromise).to.be.rejectedWith(ConnectionClosedException);

    await connection.close();

    const backendWritePromise = backend.write('data');
    const connectionWritePromise = connection.genericCommandNoResponse('');

    await Promise.all([
      backendReadPromiseExpect,
      expect(backendWritePromise).to.be.rejectedWith(ConnectionClosedException),
      expect(connectionWritePromise).to.be.rejectedWith(ConnectionClosedException),
    ]);
    expect(await closeEvent).to.be.an.instanceof(ConnectionClosedException);
  });

  it('propagates error when closed with error', async () => {
    const backend = Transport.open();
    const connection = await Connection.openCustom(backend);

    const closeEvent = lastValueFrom(connection.disconnected);

    const connectionWritePromise = connection.genericCommandNoResponse('');
    const connectionWritePromiseExpect = expect(connectionWritePromise).to.be.rejectedWith(ConnectionFailedException);
    expect(await backend.read()).to.eq('/0 0:80');

    await backend.closeWithError('Cannot continue');

    const backendReadPromise = backend.read();
    const backendWritePromise = backend.write('data');

    await Promise.all([
      connectionWritePromiseExpect,
      expect(backendReadPromise).to.be.rejectedWith(ConnectionClosedException),
      expect(backendWritePromise).to.be.rejectedWith(ConnectionClosedException),
    ]);

    const closeEventError = await closeEvent;
    expect(closeEventError).to.be.an.instanceof(ConnectionFailedException);
    expect(closeEventError?.message).to.eq('Cannot continue');
  });

  it('write does not finish on library side until new read is issued on transport', async () => {
    const backend = Transport.open();
    const connection = await Connection.openCustom(backend);

    let commandSentResolved = false;
    const commandSentPromise = connection.genericCommandNoResponse('home', { device: 1 }).then(() => commandSentResolved = true);

    const message = await backend.read();
    expect(message).to.eq('/1 0 home:B6');

    const WAIT_FOR_SEND_NOT_TO_RESOLVE = 100;
    await new Promise<void>(resolve => setTimeout(resolve, WAIT_FOR_SEND_NOT_TO_RESOLVE));
    expect(commandSentResolved).to.eq(false);

    const nextRead = expect(backend.read()).to.be.rejectedWith(ConnectionClosedException);

    await commandSentPromise;

    await connection.close();
    await backend.close();
    await nextRead;
  });

  it('holds in the smoke test', async () => {
    const CONNECTIONS = 10;
    const REQUESTS = 1000;

    const transports = _.range(CONNECTIONS).map(() => Transport.open());
    const connections = await Promise.all(transports.map(transport => Connection.openCustom(transport)));

    const replyLoops = transports.map(async transport => {
      try {
        for (;;) {
          const message = await transport.read();
          const command = parseCommand(message);
          await transport.write(`@${command.device} ${command.axis} ${command.id} OK IDLE -- 0`);
        }
      } catch (err) {
        expect(err).instanceOf(ConnectionClosedException);
      }
    });

    const fibers = connections.map(async connection => {
      for (let i = 0; i < REQUESTS; i++) {
        await connection.genericCommand('');
      }
    });

    await Promise.all(fibers);

    await Promise.all(connections.map(connection => connection.close()));

    await Promise.all(replyLoops);
  }, 20 * 1000);

  it('throws connection closed when trying to open closed transport', async () => {
    const backend = Transport.open();
    await backend.close();

    const connectionPromise = Connection.openCustom(backend);

    await expect(connectionPromise).to.be.rejectedWith(ConnectionClosedException);
  });

  it('throws exception when transport is used to open more than one connection', async () => {
    const backend = Transport.open();

    await Connection.openCustom(backend);
    const connectionPromise = Connection.openCustom(backend);

    await expect(connectionPromise).to.be.rejectedWith(TransportAlreadyUsedException);

    await backend.close();
  });
});
