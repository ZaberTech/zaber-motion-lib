import { expect } from 'chai';
import {
  ConversionFailedException, Acceleration, Length, RotationDirection, DeviceDbSourceType,
  StreamModeException, Velocity, InvalidArgumentException, Angle, Library,
  Time,
} from '../../src';
import { Connection, Device, DigitalOutputAction, Stream } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { DeviceXMCB2 } from './devices/xmcb2';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { MASTER_DEVICE_DB } from '../constants';

import { DeviceXMCC4 } from './devices/xmcc4';

describe('Stream -- identified', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let eqAndPushX: (pushStr: string, popStr: string) => Promise<unknown>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);

    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, MASTER_DEVICE_DB);
  });

  afterAll(async () => {
    Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE);
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('xlhm - integrated device', () => {
    beforeEach(async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);
      connection.resetIds();
    });

    it('runs integration test', async () => {
      const A = { value: 1090.19, unit: Length.mm };

      const liveStream = device.streams.getStream(1);
      const setupLivePromise = liveStream.setupLive(1);
      await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 1 setup live 1', 'OK IDLE -- 0');
      await setupLivePromise;

      const lineAbsPromise = liveStream.lineAbsolute(A);
      await eqAndPushX('stream 1 line abs 8790193', 'OK IDLE -- 0');
      await lineAbsPromise;

      const setMaxSpeedPromise = liveStream.setMaxSpeed(10, Velocity['in/s']);
      await eqAndPushX('stream 1 set maxspeed 3355443', 'OK IDLE -- 0');
      await setMaxSpeedPromise;

      const setAccelPromise = liveStream.setMaxCentripetalAcceleration(10, Acceleration['cm/s²']);
      await eqAndPushX('stream 1 set centripaccel 132', 'OK IDLE -- 0');
      await setAccelPromise;
    });
  });

  describe('tests for setup streams', () => {
    let liveStream: Stream;
    let storeStream: Stream;
    let storeArbitraryStream: Stream;

    // example measurements for moving
    const A = { value: 1090.19, unit: Length.mm };
    const B = { value: 10.18, unit: Length.mm };
    const C = { value: .19, unit: Length.cm };
    const nativeMeasurement = { value: 100 };

    async function setupStreams(liveAxes = [1, 2]) {
      liveStream = device.streams.getStream(1);
      const setupLivePromise = liveStream.setupLive(...liveAxes);
      await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
      await eqAndPushX(`stream 1 setup live ${liveAxes.join(' ')}`, 'OK IDLE -- 0');
      await setupLivePromise;

      storeStream = device.streams.getStream(2);
      const firstBuffer = device.streams.getBuffer(1);
      const setupStorePromise = storeStream.setupStore(firstBuffer, 1, 2);
      await eqAndPushX('stream 2 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 2 setup store 1 2', 'OK IDLE -- 0');
      await setupStorePromise;

      storeArbitraryStream = device.streams.getStream(3);
      const secondBuffer = device.streams.getBuffer(2);
      const setupStoreArbitraryPromise = storeArbitraryStream.setupStoreArbitraryAxes(secondBuffer, 2);
      await eqAndPushX('stream 3 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 3 setup store 2 2', 'OK IDLE -- 0');
      await setupStoreArbitraryPromise;
    }

    describe('X-MCB2, one linear, one rotary', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.OneLinearOneRotary, DeviceXMCB2.FWType.FW7Proto);
        await setupStreams();
      });
      it('does not allow unit conversion on rotary axis', async () => {
        const lineAbsPromise = storeStream.lineAbsolute(A, B);
        await expect(lineAbsPromise).to.be.rejectedWith(ConversionFailedException);
        const error: ConversionFailedException = await lineAbsPromise.catch(e => e);
        expect(error.message).to.eq('Invalid units: Length:millimetres. Provide units of dimension Angle.');
      });
      it('runs integration test', async () => {
        const lineAbsPromise = storeStream.lineAbsolute(A, nativeMeasurement);
        await eqAndPushX('stream 2 line abs 8790193 100', 'OK IDLE -- 0');
        await lineAbsPromise;

        const lineRelPromise = storeStream.lineRelativeOn([1], [{ value: 100, unit: Angle.RADIANS }]);
        await eqAndPushX('stream 2 on b line rel 1018592', 'OK IDLE -- 0');
        await lineRelPromise;

        const setMaxSpeedPromise = liveStream.setMaxSpeed(10, Velocity['in/s']);
        await eqAndPushX('stream 1 set maxspeed 3355443', 'OK IDLE -- 0');
        await setMaxSpeedPromise;

        const setAccelPromise = liveStream.setMaxCentripetalAcceleration(10, Acceleration['cm/s²']);
        await eqAndPushX('stream 1 set centripaccel 132', 'OK IDLE -- 0');
        await setAccelPromise;

        const circlePromise = liveStream.circleAbsolute(RotationDirection.CW, A, nativeMeasurement);
        await eqAndPushX('stream 1 circle abs cw 8790193 100', 'OK IDLE -- 0');
        await circlePromise;
      });
    });

    describe('X-MCB2, two linear, different scale', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.TwoLinearDifferentScale, DeviceXMCB2.FWType.FW7Proto);
        await setupStreams();
      });
      it('runs integration test', async () => {
        const arcPromise = storeStream.arcAbsolute(RotationDirection.CCW, A, A, B, B);
        await eqAndPushX('stream 2 arc abs ccw 8790193 10987742 82081 102602', 'OK IDLE -- 0');
        await arcPromise;

        const lineAbsPromise = storeStream.lineAbsolute(A, B);
        await eqAndPushX('stream 2 line abs 8790193 102602', 'OK IDLE -- 0');
        await lineAbsPromise;

        const lineRelPromise = storeStream.lineRelativeOn([0], [B]);
        await eqAndPushX('stream 2 on a line rel 82081', 'OK IDLE -- 0');
        await lineRelPromise;

        const setMaxSpeedPromise = liveStream.setMaxSpeed(10, Velocity['in/s']);
        await eqAndPushX('stream 1 set maxspeed 3355443', 'OK IDLE -- 0');
        await setMaxSpeedPromise;

        const setAccelPromise = liveStream.setMaxCentripetalAcceleration(10, Acceleration['cm/s²']);
        await eqAndPushX('stream 1 set centripaccel 132', 'OK IDLE -- 0');
        await setAccelPromise;

        const circlePromise = liveStream.circleAbsolute(RotationDirection.CW, A, B);
        await eqAndPushX('stream 1 circle abs cw 8790193 102602', 'OK IDLE -- 0');
        await circlePromise;
      });
    });

    describe('X-MCB2, two linear, same scale', () => {
      beforeEach(async () => {
        await DeviceXMCB2.identify(device, server, DeviceXMCB2.Mode.TwoLinear, DeviceXMCB2.FWType.FW7Proto);
        await setupStreams();
      });

      describe('setup store', () => {
        it('checks and rejects if target axes are out of range', async () => {
          const stream = device.streams.getStream(1);
          const buffer = device.streams.getBuffer(1);
          const promise = stream.setupStore(buffer, 1, 5);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const exception: InvalidArgumentException = await promise.catch(e => e);
          expect(exception.message).to.eq('Stream axis out of range: 5');
        });
      });

      it('does not allow unit conversion in StoreArbitrary mode', async () => {
        const setPromise = storeArbitraryStream.setMaxSpeed(2413, Velocity['cm/s']);
        const setError: StreamModeException = await setPromise.catch(e => e);
        expect(setError.message).to.eq('Command failed because it requires unit conversion, \
and the stream was setup with arbitrary axes. Setup stream with specific axes.');
      });

      describe('movement', () => {
        describe('arcAbsolute', () => {
          it('sends proper request', async () => {
            const promise = liveStream.arcAbsolute(RotationDirection.CW, A, A, B, B);
            await eqAndPushX('stream 1 arc abs cw 258415 258415 2413 2413', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('arcRelative', () => {
          it('sends proper request', async () => {
            const promise = storeStream.arcRelative(RotationDirection.CCW, A, A, B, B);
            await eqAndPushX('stream 2 arc rel ccw 258415 258415 2413 2413', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('circleAbsolute', () => {
          it('sends proper request', async () => {
            const promise = storeStream.circleAbsolute(RotationDirection.CCW, A, A);
            await eqAndPushX('stream 2 circle abs ccw 258415 258415', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('circleRelative', () => {
          it('sends proper request', async () => {
            const promise = liveStream.circleRelative(RotationDirection.CW, A, A);
            await eqAndPushX('stream 1 circle rel cw 258415 258415', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('lineAbsolute', () => {
          it('sends proper request', async () => {
            const promise = liveStream.lineAbsolute(A, B);
            await eqAndPushX('stream 1 line abs 258415 2413', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('lineAbsoluteOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.lineAbsoluteOn([1, 0], [C, A]);
            await eqAndPushX('stream 1 on b a line abs 450 258415', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('lineRelative', () => {
          it('sends proper request', async () => {
            const promise = liveStream.lineRelative(A, B);
            await eqAndPushX('stream 1 line rel 258415 2413', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('lineRelativeOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.lineRelativeOn([0, 1], [A, B]);
            await eqAndPushX('stream 1 on a b line rel 258415 2413', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('arcAbsoluteOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.arcAbsoluteOn([0, 1], RotationDirection.CCW, A, A, B, C);
            await eqAndPushX('stream 1 on a b arc abs ccw 258415 258415 2413 450', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('arcRelativeOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.arcRelativeOn([0, 1], RotationDirection.CW, A, A, B, C);
            await eqAndPushX('stream 1 on a b arc rel cw 258415 258415 2413 450', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('circleAbsoluteOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.circleAbsoluteOn([1, 0], RotationDirection.CW, A, A);
            await eqAndPushX('stream 1 on b a circle abs cw 258415 258415', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('circleRelativeOn', () => {
          it('sends proper request', async () => {
            const promise = liveStream.circleRelativeOn([0, 1], RotationDirection.CCW, A, A);
            await eqAndPushX('stream 1 on a b circle rel ccw 258415 258415', 'OK BUSY -- 0');
            await promise;
          });
        });
      });

      describe('settings', () => {
        describe('set', () => {
          describe('setMaxSpeed', () => {
            it('sets maxspeed', async () => {
              const promise = liveStream.setMaxSpeed(89.9, Velocity['cm/s']);
              await eqAndPushX('stream 1 set maxspeed 349137', 'OK IDLE -- 0');
              await promise;
            });
          });

          describe('setMaxTangentialAcceleration', () => {
            it('sets tanaccel', async () => {
              const promise = liveStream.setMaxTangentialAcceleration(2413, Acceleration['in/s²']);
              await eqAndPushX('stream 1 set tanaccel 2380', 'OK IDLE -- 0');
              await promise;
            });
          });

          describe('first axis to convert acceleration units', () => {
            it('sets centripaccel', async () => {
              const promise = liveStream.setMaxCentripetalAcceleration(192.99, Acceleration['cm/s²']);
              await eqAndPushX('stream 1 set centripaccel 75', 'OK IDLE -- 0');
              await promise;
            });
          });
        });

        describe('get', () => {
          describe('getMaxSpeed', () => {
            it('gets maxspeed', async () => {
              const promise = liveStream.getMaxSpeed(Velocity['cm/s']);
              await eqAndPushX('stream 1 info', 'OK IDLE -- live 1 205 24135 153600 -');
              expect(await promise).to.closeTo(39.551, 0.1 ** 3);
            });
          });

          describe('getMaxCentripetalAcceleration', () => {
            it('gets centripaccel', async () => {
              const promise = liveStream.getMaxCentripetalAcceleration(Acceleration['cm/s²']);
              await eqAndPushX('stream 1 info', 'OK IDLE -- live 1 205 24135 153600 -');
              expect(await promise).to.closeTo(527.859, 0.1 ** 3);
            });
          });

          describe('getMaxTangentialAcceleration', () => {
            it('gets tanaccel', async () => {
              const promise = liveStream.getMaxTangentialAcceleration(Acceleration['m/s²']);
              await eqAndPushX('stream 1 info', 'OK IDLE -- live 1 205 24135 153600 -');
              expect(await promise).to.closeTo(621.457, 0.1 ** 3);
            });
          });
        });
      });
    });

    describe('X-MCC', () => {
      beforeEach(async () => {
        await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_38);
        await setupStreams([1, 2, 3, 4]);
      });

      describe('io', () => {
        describe('setDigitalOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setDigitalOutputSchedule(2, DigitalOutputAction.OFF, DigitalOutputAction.ON, 5, Time.SECONDS);
            await eqAndPushX('stream 1 io set do 2 0 schedule 5000.0 1', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAnalogOutputSchedule', () => {
          it('sends proper request', async () => {
            const promise = liveStream.io.setAnalogOutputSchedule(1, 0, 3.3, 5, Time.SECONDS);
            await eqAndPushX('stream 1 io set ao 1 0 schedule 5000.0 3.3', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllAnalogOutputs', () => {
          it('sets the values of all analog output channels', async () => {
            const promise = liveStream.io.setAllAnalogOutputs([1.1, 2.2, 3.39999, 4.4321]);
            await eqAndPushX('stream 1 io set ao port 1.100 2.200 3.400 4.432', 'OK IDLE -- 0');
            await promise;
          });
        });
      });

      describe('helixAbsolute', () => {
        it('sends proper request', async () => {
          const promise = liveStream.helixAbsoluteOn([1, 2, 3], RotationDirection.CW, A, A, B, B, A);
          await eqAndPushX('stream 1 on b c d helix abs cw 8790193 8790193 82081 82081 8790193', 'OK BUSY -- 0');
          await promise;
        });

        it('throws error if the axes don\'t match endpoints', async () => {
          const promise = liveStream.helixAbsoluteOn([1, 2], RotationDirection.CW, A, A, B, B, A);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const exception: InvalidArgumentException = await promise.catch(e => e);
          expect(exception.message).to.eq('Invalid number of target axes 2 (expected 3).');
        });

        it('throws error if there are no endpoints', async () => {
          const promise = liveStream.helixAbsoluteOn([1, 2], RotationDirection.CW, A, A, B, B);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const exception: InvalidArgumentException = await promise.catch(e => e);
          expect(exception.message).to.eq('No endpoint provided for the line component.');
        });

        it('an empty target axes array executes on default axes', async () => {
          // This behavior exists only to be consistent with firmware.
          const promise = liveStream.helixAbsoluteOn([], RotationDirection.CW, A, A, B, B, A);
          await eqAndPushX('stream 1 helix abs cw 8790193 8790193 82081 82081 8790193', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('helixRelative', () => {
        it('sends proper request', async () => {
          const promise = liveStream.helixRelativeOn([3, 2, 1], RotationDirection.CCW, A, A, B, A, B);
          await eqAndPushX('stream 1 on d c b helix rel ccw 8790193 8790193 82081 8790193 82081', 'OK BUSY -- 0');
          await promise;
        });
      });
    });
  });
});
