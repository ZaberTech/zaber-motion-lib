import { expect } from 'chai';
import {
  Length, NotSupportedException, DeviceNotIdentifiedException,
  Time, ConversionFailedException, SettingNotFoundException, InvalidArgumentException, Angle, Voltage,
  BadCommandException,
} from '../../src';
import { Connection, Device, DeviceSettings, SettingConstants } from '../../src/ascii';
import { DeviceXMCC1 } from './devices/xmcc1';
import { DeviceXMCC4 } from './devices/xmcc4';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';

const server = new TestServer();

describe('DeviceSettings', () => {
  let connection: Connection;
  let device: Device;
  let settings: DeviceSettings;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(2);
    settings = device.settings;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('get', () => {
    it('sends the correct command and returns value', async () => {
      const promise = settings.get('foo');

      expect(await server.pop()).to.eq('/2 0 00 get foo');
      await server.push('@02 0 00 OK IDLE -- -123');

      const value = await promise;
      expect(value).to.deep.eq(-123);
    });

    it('sends the correct setting when using constants', async () => {
      const promise = settings.get(SettingConstants.VERSION_BUILD);

      expect(await server.pop()).to.eq('/2 0 00 get version.build');
      await server.push('@02 0 00 OK IDLE -- 123');

      const value = await promise;
      expect(value).to.deep.eq(123);
    });

    it('throws exception when the device does not support the setting', async () => {
      const promise = settings.get('foo');

      expect(await server.pop()).to.eq('/2 0 00 get foo');
      await server.push('@02 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
      const exception: BadCommandException = await promise.catch(e => e);
      expect(exception.message).to.startWith('Command "get foo" rejected: BADCOMMAND');
    });

    it('throws an exception when the device tries to get multiple axis', async () => {
      const promise = settings.get('foo');

      expect(await server.pop()).to.eq('/2 0 00 get foo');
      await server.push('@02 0 00 OK IDLE -- 1 1');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Multiple axis data returned; use axis level settings to get "foo".');
    });
  });

  describe('set', () => {
    it('sends the correct command and does not throw exception', async () => {
      const promise = settings.set('foo', -123);

      expect(await server.pop()).to.eq('/2 0 00 set foo -123');
      await server.push('@02 0 00 OK IDLE -- 0');

      await promise;
    });

    it('throws exception when device does not support the setting', async () => {
      const promise = settings.set('foo', -123);

      expect(await server.pop()).to.eq('/2 0 00 set foo -123');
      await server.push('@02 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
      const exception: BadCommandException = await promise.catch(e => e);
      expect(exception.message).to.startWith('Command "set foo -123" rejected: BADCOMMAND');
    });

    it('formats to the number of decimal places specified', async () => {
      const promise = settings.set('foo', 0.1);
      expect(await server.pop()).to.eq('/2 0 00 set foo 0.1');
      await server.push('@02 0 00 OK IDLE -- 0');
      await promise;
    });

    it('throws exception when trying to do unit conversion', async () => {
      const promise = settings.set('foo', 0.1, Length.cm);
      await expect(promise).to.be.rejectedWith(DeviceNotIdentifiedException);
      const exception: DeviceNotIdentifiedException = await promise.catch(e => e);
      expect(exception.message).to.eq(
        'Cannot perform this operation. Device number 2 is not identified. Call device.identify() to enable this operation.');
    });
  });

  describe('get - identified', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_21);
      connection.resetIds();
    });

    it('converts units', async () => {
      const promise = settings.get('scope.timebase', Time.MICROSECONDS);

      expect(await server.pop()).to.eq('/2 0 00 get scope.timebase');
      await server.push('@02 0 00 OK IDLE -- 3.5');

      expect(await promise).to.be.closeTo(3500, 1e-3);
    });

    it('throws exception when setting is not found', async () => {
      const promise = settings.get('schwifty', Time.MICROSECONDS);

      expect(await server.pop()).to.eq('/2 0 00 get schwifty');
      await server.push('@02 0 00 OK IDLE -- 1');

      await expect(promise).to.be.rejectedWith(ConversionFailedException);
      const exception: ConversionFailedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Conversion information not found for setting "schwifty".');
    });

    it('throws exception when reading a string setting', async () => {
      const promise = settings.get('comm.en.ipv4.address', Time.MICROSECONDS);

      expect(await server.pop()).to.eq('/2 0 00 get comm.en.ipv4.address');
      await server.push('@02 0 00 OK IDLE -- 192.168.0.13');

      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Settings is not parsable as number; use get string method to query "comm.en.ipv4.address".');
    });

    it('reads all axis settings', async () => {
      const promise = settings.getFromAllAxes('pos');

      expect(await server.pop()).to.eq('/2 0 00 get pos');
      await server.push('@02 0 00 OK IDLE -- 0 NA 20000 14');

      expect(await promise).to.be.deep.eq([0, Number.NaN, 20_000, 14]);
    });

    it('throws exception trying to read device scope setting', async () => {
      const promise = settings.getFromAllAxes('comm.address');

      expect(await server.pop()).to.eq('/2 0 00 get comm.address');
      await server.push('@02 0 00 OK IDLE -- 2');

      await expect(promise).to.be.rejectedWith(InvalidArgumentException);
      const exception: InvalidArgumentException = await promise.catch(e => e);
      expect(exception.message).to.eq('Setting \'comm.address\' is device scope, and cannot be fetched with GetFromAllAxes.');
    });
  });

  describe('getDefault', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_21);
      connection.resetIds();
    });

    it('gets the default value', () => {
      expect(settings.getDefault('comm.rs232.baud')).to.eq(115200);
    });

    it('throws error when setting is not found', () => {
      expect(() => settings.getDefault('hello')).to.throw(SettingNotFoundException, 'Setting hello not found.');
    });

    it('throws error when setting does not have a default value', () => {
      expect(() => settings.getDefault('system.temperature'))
        .to.throw(InvalidArgumentException, 'Setting system.temperature does not have a default value.');
    });

    it('returns the string value using getDefaultString', () => {
      expect(settings.getDefaultString('comm.rs232.baud')).to.eq('115200');
    });
  });

  describe('set - identified', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_21);
      connection.resetIds();
    });

    it('sends command when setting is not found and unit conversion is not needed', async () => {
      const promise = settings.set('schwifty', 2.11);
      expect(await server.pop()).to.eq('/2 0 00 set schwifty 2.11');
      await server.push('@02 0 00 OK IDLE -- 0');
      await promise;
    });

    it('throws exception when setting is not found and units are requested', async () => {
      const promise = settings.set('schwifty', 2.11, Length.mm);
      await expect(promise).to.be.rejectedWith(NotSupportedException);
      const exception: NotSupportedException = await promise.catch(e => e);
      expect(exception.message).to.eq('Setting name "schwifty" not found.');
    });

    it('formats setting to correct number of decimals', async () => {
      const promise = settings.set('scope.timebase', 1.19014);

      expect(await server.pop()).to.eq('/2 0 00 set scope.timebase 1.2');
      await server.push('@02 0 00 OK IDLE -- 0');

      await promise;
    });

    it('handles boolean parameter', async () => {
      const promise = settings.set('comm.checksum', 1);
      expect(await server.pop()).to.eq('/2 0 00 set comm.checksum 1');
      await server.push('@02 0 00 OK IDLE -- 0');
      await promise;
    });

    it('converts units', async () => {
      const promise = settings.set('scope.timebase', 3456, Time.MICROSECONDS);

      expect(await server.pop()).to.eq('/2 0 00 set scope.timebase 3.5');
      await server.push('@02 0 00 OK IDLE -- 0');

      await promise;
    });
  });

  describe('convert settings', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.FourLinear, DeviceXMCC4.FWVersion.FW7_21);
      connection.resetIds();
    });

    it('throws exception when setting is not supported by device settings table', () => {
      expect(() => settings.convertFromNativeUnits('foo', 123, Time.MICROSECONDS))
        .to.throw('Conversion information not found for setting "foo".');
    });

    it('converts from native units', () => {
      expect(settings.convertFromNativeUnits(SettingConstants.SCOPE_TIMEBASE, 3.5, Time.MICROSECONDS)).to.be.closeTo(3500, 1e-3);
    });

    it('converts to native units', () => {
      expect(settings.convertToNativeUnits(SettingConstants.SCOPE_TIMEBASE, 34567, Time.MICROSECONDS)).to.be.closeTo(34.567, 1e-3);
    });
  });

  describe('getString', () => {
    it('sends the correct command and returns value', async () => {
      const promise = settings.getString('foo');

      expect(await server.pop()).to.eq('/2 0 00 get foo');
      await server.push('@02 0 00 OK IDLE -- bar');

      expect(await promise).to.eq('bar');
    });

    it('throws exception when the device does not support the setting', async () => {
      const promise = settings.getString('foo');

      expect(await server.pop()).to.eq('/2 0 00 get foo');
      await server.push('@02 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
    });
  });

  describe('setString', () => {
    it('sends the correct command and does not throw exception', async () => {
      const promise = settings.setString('foo', 'bar');

      expect(await server.pop()).to.eq('/2 0 00 set foo bar');
      await server.push('@02 0 00 OK IDLE -- 0');

      await promise;
    });

    it('throws exception when device does not support the setting', async () => {
      const promise = settings.setString('foo', 'bar');

      expect(await server.pop()).to.eq('/2 0 00 set foo bar');
      await server.push('@02 0 00 RJ IDLE -- BADCOMMAND');

      await expect(promise).to.be.rejectedWith(BadCommandException);
    });
  });

  describe('multiget', () => {
    beforeEach(async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary, DeviceXMCC4.FWVersion.FW7_35);
      connection.resetIds();
    });

    describe('get many', () => {
      it('gets multiple values', async () => {
        const promise = settings.getMany({ setting: 'device.id' }, { setting: 'peripheral.id' });

        expect(await server.pop()).to.eq('/2 0 00 get device.id peripheral.id');
        await server.push('@02 0 00 OK IDLE -- 42 ; 43211 43211 46657 46657');

        expect(await promise).to.deep.eq([
          { setting: 'device.id', values: [42], unit: '' },
          { setting: 'peripheral.id', values: [43211, 43211, 46657, 46657], unit: '' }
        ]);
      });

      it('falls back to sequential gets on FW version 34 and below', async () => {
        await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary, DeviceXMCC4.FWVersion.FW7_33);
        connection.resetIds();

        const promise = settings.getMany({ setting: 'device.id' }, { setting: 'peripheral.id' });

        expect(await server.pop()).to.eq('/2 0 00 get device.id');
        await server.push('@02 0 00 OK IDLE -- 42');
        expect(await server.pop()).to.eq('/2 0 01 get peripheral.id');
        await server.push('@02 0 01 OK IDLE -- 43211 43211 46657 46657');

        expect(await promise).to.deep.eq([
          { setting: 'device.id', values: [42], unit: '' },
          { setting: 'peripheral.id', values: [43211, 43211, 46657, 46657], unit: '' }
        ]);
      });

      it('converts units even when ascii return is ambiguously device or axis scope', async () => {
        await DeviceXMCC1.identify(device, server);
        connection.resetIds();

        const promise = settings.getMany(
          { setting: 'system.voltage', unit: Voltage.mV },
          { setting: 'pos', axes: [1], unit: Length.mm },
        );

        expect(await server.pop()).to.eq('/2 0 00 get pos system.voltage');
        await server.push('@02 0 00 OK IDLE -- 10 ; 2.46');

        expect(await promise).to.deep.eq([
          { setting: 'system.voltage', values: [2_460], unit: Voltage.mV },
          { setting: 'pos', values: [0.00001], unit: Length.mm },
        ]);
      });

      it('groups gets if possible', async () => {
        const promise = settings.getMany(
          { setting: 'pos', axes: [3], unit: Angle['°'] },
          { setting: 'pos', axes: [1], unit: Length.mm },
          { setting: 'pos', axes: [1, 2, 3] },
        );

        expect(await server.pop()).to.eq('/2 0 00 get 1 2 3 pos');
        await server.push('@02 0 00 OK IDLE -- 10 99 360');

        expect(await promise).to.deep.eq([
          { setting: 'pos', values: [0.05625], unit: Angle['°'] },
          { setting: 'pos', values: [0.0012402343749999994], unit: Length.mm },
          { setting: 'pos', values: [10, 99, 360], unit: '' },
        ]);
      });

      it('groups settings with the same scope in the get call', async () => {
        const promise = settings.getMany(
          { setting: 'pos', axes: [1, 3] },
          { setting: 'device.id' },
          { setting: 'peripheral.id', axes: [1, 3] },
        );

        expect(await server.pop()).to.eq('/2 0 00 get device.id 1 3 peripheral.id pos');
        await server.push('@02 0 00 OK IDLE -- 42 ; 43211 46657 ; 99 360');

        expect(await promise).to.deep.eq([
          { setting: 'pos', values: [99, 360], unit: '' },
          { setting: 'device.id', values: [42], unit: '' },
          { setting: 'peripheral.id', values: [43211, 46657], unit: '' },
        ]);
      });

      it('passes ZL-915 regression test', async () => {
        const promise = settings.getMany(
          { setting: 'pos', axes: [2] },
          { setting: 'encoder.pos', axes: [2] },
          { setting: 'system.uptime' },
        );

        // Before the fix, system.uptime would have been added to the end of the axis 2 group
        // and a real device would have returned NA for it.
        expect(await server.pop()).to.eq('/2 0 00 get system.uptime 2 encoder.pos pos');
        await server.push('@02 0 00 OK IDLE -- 42 ; 43211 ; 46657');

        expect(await promise).to.deep.eq([
          { setting: 'pos', values: [46657], unit: '' },
          { setting: 'encoder.pos', values: [43211], unit: '' },
          { setting: 'system.uptime', values: [42], unit: '' },
        ]);
      });

      it('throws exception if asked to unit-convert axes with mixed motion types', async () => {
        const promise = settings.getMany({ setting: 'pos', axes: [1, 3], unit: Length.mm });

        expect(await server.pop()).to.eq('/2 0 00 get 1 3 pos');
        await server.push('@02 0 00 OK IDLE -- 99 360');

        await expect(promise).to.be.rejectedWith(ConversionFailedException);
      });

      it('returns NaN for unused axes', async () => {
        await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_35);
        connection.resetIds();

        const promise = settings.getMany({ setting: 'pos', axes: [1, 2] });

        expect(await server.pop()).to.eq('/2 0 00 get 1 2 pos');
        await server.push('@02 0 00 OK IDLE -- 99 NA');

        expect(await promise).to.deep.eq([
          { setting: 'pos', values: [99, NaN], unit: '' },
        ]);
      });
    });

    describe('get synchronized', () => {
      it('will never fall back', async () => {
        await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.TwoLinearTwoRotary, DeviceXMCC4.FWVersion.FW7_33);
        connection.resetIds();

        const promise = settings.getSynchronized({ setting: 'device.id' }, { setting: 'peripheral.id' });
        await expect(promise).to.be.rejectedWith(NotSupportedException);
      });

      it('command rejected if more than get.settings.max settings are requested (12 in FW 35)', async () => {
        const promise = settings.getSynchronized(
          { setting: 'user.data.0' }, { setting: 'user.data.2' }, { setting: 'user.data.3' }, { setting: 'user.data.4' },
          { setting: 'user.data.5' }, { setting: 'user.data.6' }, { setting: 'user.data.7' }, { setting: 'user.data.8' },
          { setting: 'user.data.9' }, { setting: 'user.data.10' }, { setting: 'user.data.11' }, { setting: 'user.data.12' },
          { setting: 'user.data.13' },
        );
        await expect(promise).to.be.rejectedWith(NotSupportedException);
      });


      it('multi-get will check protocol information of unidentified devices', async () => {
        const unidentifiedDevice = connection.getDevice(3);
        const promise = unidentifiedDevice.settings.getSynchronized({ setting: 'foo' }, { setting: 'bar', axes: [1, 3] });

        expect(await server.pop()).to.eq('/3 0 00 get comm.command.packets.max');
        await server.push('@03 0 00 OK IDLE -- 6');
        expect(await server.pop()).to.eq('/3 0 01 get comm.word.size.max');
        await server.push('@03 0 01 OK IDLE -- 40');
        expect(await server.pop()).to.eq('/3 0 02 get get.settings.max');
        await server.push('@03 0 02 OK IDLE -- 12');
        expect(await server.pop()).to.eq('/3 0 03 get foo 1 3 bar');
        await server.push('@03 0 03 OK IDLE -- 1 ; 2 3');

        expect(await promise).to.deep.eq([
          { setting: 'foo', values: [1], unit: '' },
          { setting: 'bar', values: [2, 3], unit: '' }
        ]);
      });
    });
  });
});
