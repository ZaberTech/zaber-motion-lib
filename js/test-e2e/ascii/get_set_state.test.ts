import { expect } from 'chai';

import { Axis, Connection, Device } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { DeviceXLHM } from './devices/xlhm';
import { parseCommand, signature } from './protocol';
import { NotSupportedException, SetDeviceStateFailedException } from '../../src';
import {
  AxisSerialization, SavedDeviceStateDefinition, ServoTuning, AxisDeviceCommonSerialization
} from './saved_states/saved_state_definition';
import { DeviceXMCC4 } from './devices/xmcc4';
import _ from 'lodash';

import xlhmState from './saved_states/xlhm.json';
import xlhmChangedBuffers from './saved_states/xlhm_changed_stream_buffers.json';
import xmcc4State from './saved_states/xmcc4.json';
import xmcc4WithStorageState from './saved_states/xmcc4-with-storage.json';
import axisState from './saved_states/axis1-of-xmcc4.json';
import axisHwModState from './saved_states/axis-with-hw-mod.json';
import noModAxisState from './saved_states/axis-no-hw-mod.json';
import noModDeviceState from './saved_states/no-mod-device.json';
import brokenKnobState from './saved_states/broken-knob.json';
import deviceWithIctrl from './saved_states/device-with-ictrl.json';

type StreamType = 'stream' | 'pvt';
const PVT_733_BUFFER_LIMIT = 100;

const server = new TestServer();

async function getDeviceSettings(server: TestServer, device: number, json: SavedDeviceStateDefinition) {
  const settingCount = Object.keys(json.settings).length;
  for (let i = 0; i < settingCount; i++) {
    const command = parseCommand(await server.pop());
    const val = json.settings[command.data.split(' ')[1]];
    if (val == null) {
      throw new Error(`Could not find value for setting ${command.data}`);
    }
    await server.push(`@${device} 0 ${command.id} OK IDLE -- ${val}`);
  }
}

async function getTriggers(
  server: TestServer,
  device: number,
  json: SavedDeviceStateDefinition,
  numtriggers: number
) {
  const numtriggersCommand = parseCommand(await server.pop());
  expect(numtriggersCommand.strNoId).to.eq(`/${device} 0 id get trigger.numtriggers`);
  await server.push(`@${device} 0 ${numtriggersCommand.id} OK IDLE -- ${numtriggers}`);

  for (let i = 1; i <= numtriggers; i++) {
    const command = parseCommand(await server.pop());
    expect(command.data).to.eq(`trigger ${i} print`);
    // catch the empty command with the same id
    expect(await server.pop()).to.eq(`/${device} 0 ${command.id}`);
    const trigger = json.triggers?.[`${i}`] ?? {
      when: 'none',
      a: 'none',
      b: 'none',
      enabled: false,
      count: 0,
    };
    const pre = `${device} 0 ${command.id}`;
    const enabledInfo = trigger.count > 0 ? `enable ${trigger.count}` : trigger.enabled ? 'enable' : 'disable';
    await server.push(`@${pre} OK IDLE -- 0`);
    await server.push(`#${pre} when ${trigger.when}`);
    await server.push(`#${pre} action a ${trigger.a}`);
    await server.push(`#${pre} action b ${trigger.b}`);
    await server.push(`#${pre} ${enabledInfo}`);
    await server.push(`@${pre} OK IDLE -- 0`);
  }
}

async function getStoredPositions(server: TestServer, storedPositions: SavedDeviceStateDefinition['storedPositions']) {
  if (storedPositions == null) {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq('tools storepos 1');
    await server.push(`@${cmd.device} ${cmd.axis} ${cmd.id} RJ IDLE -- BADCOMMAND`);
  } else {
    for (let i = 1; i <= 16; i++) {
      const cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq(`tools storepos ${i}`);
      await server.push(`@${cmd.device} ${cmd.axis} ${cmd.id} OK IDLE -- ${storedPositions[i]}`);
    }
  }
}

async function getStreamBuffer(server: TestServer, json: SavedDeviceStateDefinition['streamBuffers'], buf: number, type: StreamType) {
  const command = parseCommand(await server.pop());
  const device = command.device;
  expect(command.data).to.eq(`${type} buffer ${buf} print`);
  // catch the empty command with the same id
  expect(await server.pop()).to.eq(`/${device} 0 ${command.idStr}`);
  const pre = `${device} 0 ${command.id}`;
  const buffer = json?.[`${buf}`];
  if (!buffer) {
    await server.push(`@${pre} RJ IDLE -- BADDATA`);
  } else {
    await server.push(`@${pre} OK IDLE -- 0`);
    for (const cmd of buffer.lines) {
      await server.push(`#${pre} ${cmd}`);
    }
  }
  await server.push(`@${pre} OK IDLE -- 0`);
}

async function getStreamBuffers(
  server: TestServer,
  json: SavedDeviceStateDefinition,
  type: StreamType,
  listSupported = true,
) {
  const buffers = type === 'stream' ? json.streamBuffers : json.pvtBuffers;
  const numbufs = type === 'stream' ? 6 : PVT_733_BUFFER_LIMIT;

  let cmd = parseCommand(await server.pop());
  const device = cmd.device;
  if (type === 'stream') {
    expect(cmd.strNoId).to.eq(`/${device} 0 id get stream.numbufs`);
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- ${numbufs}`);
  } else {
    expect(cmd.strNoId).to.eq(`/${device} 0 id get pvt.numseqs`);
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 4`);
  }

  cmd = parseCommand(await server.pop());
  expect(cmd.strNoId).to.eq(`/${device} 0 id ${type} buffer list`);
  let bufferNums: string[];
  if (listSupported) {
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 0`);
    bufferNums = Object.keys(buffers ?? {}).sort();
    for (const bufferNum of bufferNums) {
      await server.push(`#${device} 0 ${cmd.id} buffer ${bufferNum}`);
    }
  } else {
    bufferNums = _.range(1, numbufs + 1).map(n => `${n}`);
    await server.push(`@${device} 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
  }
  await server.pop();
  await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 0`);

  for (const bufferNum of bufferNums) {
    await getStreamBuffer(server, buffers, +bufferNum, type);
  }
}

async function getStreamNotSupported(server: TestServer, type: StreamType) {
  const cmd = parseCommand(await server.pop());
  const device = cmd.device;
  expect(cmd.strNoId).to.eq(`/${device} 0 id get ${type === 'stream' ? 'stream.numbufs' : 'pvt.numseqs'}`);
  await server.push(`@${device} 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
}

async function getAxisSettings(server: TestServer, device: number, axis: number, json: AxisSerialization) {
  const settingCount = Object.keys(json.settings).length;
  for (let i = 0; i < settingCount; i++) {
    const command = parseCommand(await server.pop());
    const val = json.settings[command.data.split(' ')[1]];
    if (val == null) {
      throw new Error(`Could not find value for setting ${command.data}`);
    }
    await server.push(`@${device} ${axis} ${command.id} OK IDLE -- ${val}`);
  }
}

async function getServoTuning(server: TestServer, device: number, axis: number, tuning?: ServoTuning) {
  const startCmd = parseCommand(await server.pop());
  expect(startCmd.strNoId).to.eq(`/${device} ${axis} id servo get startup`);
  if (!tuning) {
    await server.push(`@${device} ${axis} ${startCmd.id} RJ IDLE -- BADCOMMAND`);
    return;
  }
  await server.push(`@${device} ${axis} ${startCmd.id} OK IDLE -- ${tuning.startup}`);

  for (let i = 1; i <= 9; i++) {
    const getCmd = parseCommand(await server.pop());
    expect(getCmd.strNoId).to.eq(`/${device} ${axis} id servo ${i} get all`);
    // catch the empty command with the same id
    expect(await server.pop()).to.eq(`/${device} ${axis} ${getCmd.id}`);
    const pre = `${device} ${axis} ${getCmd.id}`;
    const params = tuning.paramsets[`${i}`];
    if (!params) {
      await server.push(`@${pre} RJ IDLE -- BADDATA`);
    } else {
      await server.push(`@${pre} OK IDLE -- 0`);
      await server.push(`#${pre} type ${params.type}`);
      for (const paramId in params.parameters) {
        const val = params.parameters[paramId];
        await server.push(`#${pre} parameter ${paramId} ${val}`);
      }
    }
    await server.push(`@${pre} OK IDLE -- 0`);
  }
}

async function getStorage(server: TestServer, device: number, axis = 0, storage?: Record<string, string>) {
  const storageCmd = axis === 0 ? 'storage' : 'storage axis';
  const printCmd = parseCommand(await server.pop());
  expect(printCmd.strNoId).to.equal(`/${device} ${axis} id ${storageCmd} print`);
  const preamble = `${device} ${axis} ${printCmd.idStr}`;
  // catch the empty command with the same id
  expect(await server.pop()).to.eq(`/${preamble}`);
  if (storage == null) {
    await server.push(`@${preamble} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${preamble} OK IDLE -- 0`);
    for (const key in storage) {
      const value = storage[key];
      const words = value.split(' ');
      let i = 0;
      // Write 5 words at a time to test continuations.
      // This may not take up all the space it could, and could even overflow what ascii protocol *technically* allows
      // but this is OK for tests and will trigger continuations as needed
      while (i < words.length) {
        const iPlus5 = Math.min(i + 5, words.length);
        const nextLine = words.slice(i, iPlus5).join(' ');
        await server.push(`#${preamble} set ${key} ${nextLine}`);
        i = iPlus5;
      }
    }
  }
  await server.push(`@${preamble} OK IDLE -- 0`);
}

async function getAxisSerialization(server: TestServer, device: number, axis: number, json: AxisSerialization) {
  const idCommand = parseCommand(await server.pop());
  expect(idCommand.strNoId).to.eq(`/${device} ${axis} id get peripheral.id`);
  await server.push(`@${device} ${axis} ${idCommand.id} OK IDLE -- ${json.peripheralId}`);

  const serialCommand = parseCommand(await server.pop());
  expect(serialCommand.strNoId).to.eq(`/${device} ${axis} id get peripheral.serial`);
  if (json.serial === '') {
    await server.push(`@${device} ${axis} ${serialCommand.id} RJ IDLE -- BADCOMMAND`);
  } else {
    await server.push(`@${device} ${axis} ${serialCommand.id} OK IDLE -- ${json.serial}`);
  }

  await getAxisSettings(server, device, axis, json);
  await getServoTuning(server, device, axis, json.servoTuning);
  await getStoredPositions(server, json.storedPositions);
  await getStorage(server, device, axis, json.storage);
}

async function getAxes(server: TestServer, device: number, axisCount: number, json: SavedDeviceStateDefinition['axes']) {
  if (json == null) {
    return;
  }

  for (let i = 1; i <= axisCount; i++) {
    const axisJson = json[`${i}`];
    if (axisJson) {
      await getAxisSerialization(server, device, i, axisJson);
    } else {
      const cmd = parseCommand(await server.pop());
      expect(cmd.strNoId).to.eq(`/${device} ${i} id get peripheral.id`);
      await server.push(`@${device} ${i} ${cmd.idStr} OK IDLE -- 0`);
    }
  }
}

async function disableDriver(server: TestServer) {
  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('driver disable');
  await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE FO 0`);
}

async function enableDriver(server: TestServer) {
  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('driver enable');
  await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE -- 0`);
}

async function checkDevice(server: TestServer, deviceid: string) {
  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('get deviceid');
  await server.push(`@${cmd.device} 0 ${cmd.idStr} OK IDLE -- ${deviceid}`);
}

async function checkAxis(server: TestServer, peripheralid: string) {
  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('get peripheralid');
  await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE -- ${peripheralid}`);
}

async function restoreHwModProducts(server: TestServer, isAxis: boolean, fromHwMod: 0 | 1, toHwMod: 0 | 1) {
  if (toHwMod !== 1) {
    const productType = isAxis ? 'peripheral' : 'device';
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq(`get ${productType}.hw.modified`);
    await server.push(`@${signature(cmd)} OK IDLE -- ${fromHwMod}`);
    if (fromHwMod === 1) {
      const cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq(isAxis ? 'axis restore' : 'system restore');
      await server.push(`@${signature(cmd)} OK IDLE -- 0`);
    }
  }
}

async function setAccessLevelHigh(server: TestServer, device: number, isAxis: boolean) {
  if (isAxis) {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq('get system.access');
    await server.push(`@${device} 0 ${cmd.idStr} OK IDLE -- 1`);
  }

  const cmd = parseCommand(await server.pop());
  expect(cmd.data).to.eq('set system.access 2');
  await server.push(`@${device} 0 ${cmd.idStr} OK IDLE -- 0`);
}

async function setAccessLevelFinal(server: TestServer, device: number, level: string) {
  const setAccessCmd = parseCommand(await server.pop());
  expect(setAccessCmd.data).to.eq(`set system.access ${level}`);
  await server.push(`@${device} 0 ${setAccessCmd.id} OK IDLE -- 0`);
}

let testSetErrors = false;

/** A list of settings that are NOT set by the normal setting set step */
const SPECIAL_SETTINGS = ['system.access', 'device.hw.modified', 'peripheral.hw.modified'];
/**
 * Mocks the device responses as settings are set
 * @returns The settings that were set in the order they were set in
 */
async function setSettings(server: TestServer, device: number, axis: number, json: SavedDeviceStateDefinition | AxisSerialization) {
  const settingNames = Object.keys(json.settings);
  const writeSettings = settingNames.filter(setting => !SPECIAL_SETTINGS.includes(setting));
  const settingsSet: string[] = [];
  const settingCount = writeSettings.length;
  if (json.settings['peripheral.hw.modified'] === '1') {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq('set peripheral.hw.modified 1');
    await server.push(`@${device} ${axis} ${cmd.id} OK IDLE -- 0`);
  }
  if (json.settings['device.hw.modified'] === '1') {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq('set device.hw.modified 1');
    await server.push(`@${device} ${axis} ${cmd.id} OK IDLE -- 0`);
  }
  for (let i = 0; i < settingCount; i++) {
    const command = parseCommand(await server.pop());
    const setting = command.data.split(' ')[1];
    settingsSet.push(setting);
    const val = json.settings[setting];
    expect(command.data).to.eq(`set ${setting} ${val}`);
    if (testSetErrors && i === 0) {
      await server.push(`@${device} ${axis} ${command.id} RJ IDLE -- BADDATA`);
    } else {
      await server.push(`@${device} ${axis} ${command.id} OK IDLE -- 0`);
    }
  }
  return settingsSet;
}

interface KV {
  key: string;
  values: string[];
}
function getKeyValueOfStorageCmd(data: string, cmd: 'set' | 'append', isAxis: boolean): KV {
  const words = data.split(' ');
  let i = 0;
  expect(words[i++]).to.equal('storage');
  if (isAxis) {
    expect(words[i++]).to.equal('axis');
  }
  expect(words[i++]).to.equal(cmd);
  return {
    key: words[i],
    values: words.slice(i + 1),
  };
}

async function setStorage(server: TestServer, device: number, axis: number, storage: AxisDeviceCommonSerialization['storage']) {
  if (storage == null) { return }

  const keyCount = Object.keys(storage).length;
  for (let keyIndex = 0; keyIndex < keyCount; keyIndex++) {
    const setCmd = parseCommand(await server.pop());
    const { key, values } = getKeyValueOfStorageCmd(setCmd.data, 'set', axis !== 0);
    const expectedValues = storage[key].split(' ');
    expect(expectedValues).to.not.be.undefined;
    let i = 0;
    for (const value of values) {
      expect(value).to.equal(expectedValues[i++]);
    }
    if (testSetErrors) {
      await server.push(`@${device} ${axis} ${setCmd.id} RJ IDLE -- BADDATA`);
      continue;
    }
    await server.push(`@${device} ${axis} ${setCmd.id} OK IDLE -- 0`);
    while (i < expectedValues.length) {
      const appendCmd = parseCommand(await server.pop());
      const { key: aKey, values: aValues } = getKeyValueOfStorageCmd(appendCmd.data, 'append', axis !== 0);
      expect(aKey).to.equal(key);
      for (const value of aValues) {
        expect(value).to.equal(expectedValues[i++]);
      }
      await server.push(`@${device} ${axis} ${appendCmd.id} OK IDLE -- 0`);
    }
  }
}

async function setTriggers(
  server: TestServer,
  json: SavedDeviceStateDefinition,
  numtriggers: number
) {
  const numtriggersCommand = parseCommand(await server.pop());
  const device = numtriggersCommand.device;
  expect(numtriggersCommand.strNoId).to.eq(`/${device} 0 id get trigger.numtriggers`);
  await server.push(`@${device} 0 ${numtriggersCommand.id} OK IDLE -- ${numtriggers}`);

  for (let i = 1; i <= numtriggers; i++) {
    const trigger = json.triggers?.[`${i}`];
    if (trigger) {
      const whenCmd = parseCommand(await server.pop());
      expect(whenCmd.data).to.eq(`trigger ${i} when ${trigger.when}`);
      if (testSetErrors) {
        await server.push(`@${device} 0 ${whenCmd.id} RJ IDLE -- BADDATA`);
      } else {
        await server.push(`@${device} 0 ${whenCmd.id} OK IDLE -- 0`);
      }

      const aCmd = parseCommand(await server.pop());
      expect(aCmd.data).to.eq(`trigger ${i} action a ${trigger.a}`);
      if (testSetErrors) {
        await server.push(`@${device} 0 ${aCmd.id} RJ IDLE -- BADDATA`);
      } else {
        await server.push(`@${device} 0 ${aCmd.id} OK IDLE -- 0`);
      }

      const bCmd = parseCommand(await server.pop());
      expect(bCmd.data).to.eq(`trigger ${i} action b ${trigger.b}`);
      if (testSetErrors) {
        await server.push(`@${device} 0 ${bCmd.id} RJ IDLE -- BADDATA`);
      } else {
        await server.push(`@${device} 0 ${bCmd.id} OK IDLE -- 0`);
      }

      const enabledCmd = parseCommand(await server.pop());
      if (trigger.count > 0) {
        expect(enabledCmd.data).to.eq(`trigger ${i} enable ${trigger.count}`);
      } else if (trigger.enabled) {
        expect(enabledCmd.data).to.eq(`trigger ${i} enable`);
      } else {
        expect(enabledCmd.data).to.eq(`trigger ${i} disable`);
      }
      if (testSetErrors) {
        await server.push(`@${device} 0 ${enabledCmd.id} RJ IDLE -- BADCOMMAND`);
      } else {
        await server.push(`@${device} 0 ${enabledCmd.id} OK IDLE -- 0`);
      }
    } else {
      const aCmd = parseCommand(await server.pop());
      expect(aCmd.data).to.eq(`trigger ${i} action a none`);
      await server.push(`@${device} 0 ${aCmd.id} OK IDLE -- 0`);

      const bCmd = parseCommand(await server.pop());
      expect(bCmd.data).to.eq(`trigger ${i} action b none`);
      await server.push(`@${device} 0 ${bCmd.id} OK IDLE -- 0`);

      const enabledCmd = parseCommand(await server.pop());
      expect(enabledCmd.data).to.eq(`trigger ${i} disable`);
      if (testSetErrors) {
        await server.push(`@${device} 0 ${enabledCmd.id} RJ IDLE -- BADCOMMAND`);
      } else {
        await server.push(`@${device} 0 ${enabledCmd.id} OK IDLE -- 0`);
      }
    }
  }
}

async function setStreamBuffers(
  server: TestServer,
  json: SavedDeviceStateDefinition,
  deviceBuffers: NonNullable<SavedDeviceStateDefinition['streamBuffers']>,
  type: StreamType,
  listSupported = true,
) {
  const numStreams = 4;

  const numstreamsCmd = parseCommand(await server.pop());
  const device = numstreamsCmd.device;
  expect(numstreamsCmd.data).to.eq(`get ${type}.${type === 'stream' ? 'numstreams' : 'numseqs'}`);
  await server.push(`@${device} 0 ${numstreamsCmd.id} OK IDLE -- ${numStreams}`);

  for (let i = 1; i <= numStreams; i++) {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq(`${type} ${i} setup disable`);
    await server.push(`@${device} 0 ${cmd.idStr} OK IDLE -- 0`);
  }

  const buffers = (type === 'stream' ? json.streamBuffers : json.pvtBuffers);
  const numbufs = type === 'stream' ? 6 : PVT_733_BUFFER_LIMIT;

  let cmd = parseCommand(await server.pop());
  if (type === 'stream') {
    expect(cmd.strNoId).to.eq(`/${device} 0 id get stream.numbufs`);
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- ${numbufs}`);
  } else {
    expect(cmd.strNoId).to.eq(`/${device} 0 id get pvt.numseqs`);
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 4`);
  }

  cmd = parseCommand(await server.pop());
  expect(cmd.strNoId).to.eq(`/${device} 0 id ${type} buffer list`);
  let deviceBufferNums: string[];
  if (listSupported) {
    await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 0`);
    deviceBufferNums = Object.keys(deviceBuffers ?? {}).sort();
    for (const bufferNum of deviceBufferNums) {
      await server.push(`#${device} 0 ${cmd.id} buffer ${bufferNum}`);
    }
  } else {
    await server.push(`@${device} 0 ${cmd.id} RJ IDLE -- BADCOMMAND`);
    deviceBufferNums = _.range(1, numbufs + 1).map(n => `${n}`);
  }
  await server.pop();
  await server.push(`@${device} 0 ${cmd.id} OK IDLE -- 0`);

  for (const bufferNum of deviceBufferNums) {
    const stateBuffer = buffers?.[bufferNum];
    if (stateBuffer) {
      continue;
    }

    const eraseCmd = parseCommand(await server.pop());
    expect(eraseCmd.data).to.eq(`${type} buffer ${bufferNum} erase`);
    await server.push(`@${device} 0 ${eraseCmd.id} OK IDLE -- 0`);
  }

  for (const buffNum of Object.keys(buffers ?? {}).sort()) {
    const stateBuffer = buffers![buffNum];
    await getStreamBuffer(server, deviceBuffers, +buffNum, type);
    if (_.isEqual(stateBuffer, deviceBuffers[buffNum] ?? [])) {
      continue;
    }

    const eraseCmd = parseCommand(await server.pop());
    expect(eraseCmd.data).to.eq(`${type} buffer ${buffNum} erase`);
    await server.push(`@${device} 0 ${eraseCmd.id} OK IDLE -- 0`);

    for (const buffer of stateBuffer.lines) {
      const cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq(`${type} 1 ${buffer}`);
      if (testSetErrors) {
        await server.push(`@${cmd.device} 0 ${cmd.id} RJ IDLE -- BADDATA`);
        break;
      }
      await server.push(`@${device} 0 ${cmd.idStr} OK IDLE -- 0`);
    }
  }
}

async function setServoTuning(server: TestServer, tuning: ServoTuning) {
  for (let i = 1; i <= 9; i++) {
    const params = tuning?.paramsets[`${i}`]?.parameters ?? {};
    for (let j = 0; j < Object.keys(params).length; j++) {
      const cmd = parseCommand(await server.pop());
      const [, param, val] = /servo \w+ set ([\w.]+) ([\d.]+)/.exec(cmd.data)!;
      expect(val).to.eq(params[param]);
      if (testSetErrors && param === 'kd') {
        await server.push(`@${cmd.device} ${cmd.axis} ${cmd.id} RJ IDLE -- BADDATA`);
        return;
      }
      await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE -- 0`);
    }
  }

  if (tuning?.startup) {
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq(`servo set startup ${tuning.startup}`);
    await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE -- 0`);
  }
}

async function setStoredPositions(server: TestServer, storedPos: AxisDeviceCommonSerialization['storedPositions']) {
  if (storedPos == null) { return }

  for (let i = 1; i <= 16; i++) {
    const pos = storedPos[i];
    const cmd = parseCommand(await server.pop());
    expect(cmd.data).to.eq(`tools storepos ${i} ${pos}`);
    if (testSetErrors && i == 1) {
      await server.push(`@${cmd.device} ${cmd.axis} ${cmd.id} RJ IDLE -- BADDATA`);
      continue;
    }
    await server.push(`@${cmd.device} ${cmd.axis} ${cmd.idStr} OK IDLE -- 0`);
  }
}

beforeEach(() => {
  testSetErrors = false;
});

describe('Get and Set State', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('Get state', () => {
    it('Cannot get state of FW6 device', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW6);

      const statePromise = device.getState();
      await expect(statePromise).to.be.rejectedWith(NotSupportedException, 'Cannot get state of firmware 6 devices');
    });

    it('Reads state from single axis integrated device', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const statePromise = device.getState();
      await getDeviceSettings(server, 1, xlhmState);
      await getServoTuning(server, 1, 1, xlhmState.servoTuning);
      await getStoredPositions(server, xlhmState.storedPositions);
      await getStorage(server, device.deviceAddress, 1, xlhmState.axisStorage);
      await getTriggers(server, 1, xlhmState, 6);
      await getStreamBuffers(server, xlhmState, 'stream');
      await getStreamBuffers(server, xlhmState, 'pvt');
      await getStorage(server, device.deviceAddress, 0, xlhmState.storage);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(xlhmState);
    });

    it('Reads state from axis', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const statePromise = axis.getState();

      await getAxisSerialization(server, 1, 1, axisState.axis);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(axisState);
    });

    it('Reads state from controller with peripheral', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const statePromise = device.getState();

      await getDeviceSettings(server, 1, xmcc4State);
      await getAxes(server, 1, 4, xmcc4State.axes);
      await getTriggers(server, 1, xmcc4State, 6);
      await getStreamBuffers(server, xmcc4State, 'stream');
      await getStreamNotSupported(server, 'pvt');
      await getStorage(server, device.deviceAddress);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(xmcc4State);
    });

    it('Uses stream.numbufs if buffer list command is not available', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const statePromise = device.getState();
      await getDeviceSettings(server, 1, xlhmState);
      await getServoTuning(server, 1, 1, xlhmState.servoTuning);
      await getStoredPositions(server, xlhmState.storedPositions);
      await getStorage(server, device.deviceAddress, 1, xlhmState.axisStorage);
      await getTriggers(server, 1, xlhmState, 6);

      // stream buffer list command is not supported
      await getStreamBuffers(server, xlhmState, 'stream', false);
      await getStreamBuffers(server, xlhmState, 'pvt', false);

      await getStorage(server, device.deviceAddress, 0, xlhmState.storage);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(xlhmState);
    });
  });

  describe('Set state', () => {
    const BADDATA_REASON = 'BADDATA: The command\'s data was incorrect or out of range.';
    it('Will not set state from an invalid input', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);
      const msg = 'Invalid save state. This save state cannot be set to a device, are you sure it\'s for devices?';

      const check = device.canSetState(JSON.stringify(axisState));
      await checkDevice(server, '98876');
      await expect(check).to.eventually.deep.eq({
        error: msg,
        axisErrors: [],
      });

      const promise = device.setState(JSON.stringify(axisState));
      await checkDevice(server, '98876');
      await expect(promise).to.rejectedWith(NotSupportedException, msg);
    });

    it('will not set device state to an axis', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);
      const axis = device.getAxis(1);

      const promise = axis.canSetState(JSON.stringify(xmcc4State));
      await checkAxis(server, axis.peripheralId.toString());
      expect(await promise).to.startWith('Invalid save state.');
    });

    it('Will not set if device id do not match', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);
      const msg = 'Cannot set a state with device ID 50081 to a device with ID 98876';

      const check = device.canSetState(JSON.stringify(xlhmState));
      await checkDevice(server, '98876');
      await expect(check).to.eventually.deep.eq({
        error: msg,
        axisErrors: [],
      });

      const promise = device.setState(JSON.stringify(xlhmState));
      await checkDevice(server, '98876');
      await expect(promise).to.rejectedWith(NotSupportedException, msg);
    });

    it('Cannot set state with an unsupported version', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const check = device.canSetState(JSON.stringify({ ...xlhmState, context: { ...xlhmState.context, v: 5 } }));
      await expect(check).to.eventually.deep.eq({
        error: 'No support for save files with version 5. Try upgrading to the newest version of Zaber Motion Library.',
        axisErrors: [],
      });
    });

    it('indicates that can set state to device', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.canSetState(JSON.stringify(noModDeviceState));
      await checkDevice(server, noModDeviceState.deviceId);

      expect(await promise).to.deep.eq({
        error: null,
        axisErrors: [],
      });
    });

    it('indicates that can set state to axis', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = axis.canSetState(JSON.stringify(axisState));
      await checkAxis(server, axisState.axis.peripheralId);

      expect(await promise).to.eq(null);
    });

    it('will identify the device with a different firmware version to check future compatibility', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.canSetState(JSON.stringify(xlhmState), { firmwareVersion: { major: 7, minor: 42, build: 18298 } });
      await DeviceXLHM.identifyNumber(device.deviceAddress, server, DeviceXLHM.FWType.FW7_42, true);
      await checkDevice(server, xlhmState.deviceId);

      expect(await promise).to.deep.eq({ error: null, axisErrors: [] });
    });

    it('Will restore a hardware modified device to remove modifications', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.setState(JSON.stringify(noModDeviceState));
      await checkDevice(server, noModDeviceState.deviceId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 1, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, noModDeviceState);
      await setAccessLevelFinal(server, 1, '1');
      await enableDriver(server);

      await promise;
    });

    it('Will not apply a state with a new firmware', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7_8);

      const promise = device.setState(JSON.stringify(xlhmState));
      await expect(promise).to.rejectedWith(
        NotSupportedException,
        'Cannot set a version 7.33 state to a version 7.8 product. Use a saved state that targets the product\'s version or earlier.'
      );
    });

    it('Will set knob enable to 1 if the setting value is -1', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const set = device.setState(JSON.stringify(brokenKnobState));

      await checkDevice(server, brokenKnobState.deviceId);
      await disableDriver(server);
      await restoreHwModProducts(server, false, 1, 0);
      await setAccessLevelHigh(server, 1, false);

      const cmd = parseCommand(await server.pop());
      expect(cmd.data).to.eq('set knob.enable 1');
      await server.push(`@${cmd.device} 0 ${cmd.id} OK IDLE -- 0`);

      await setAccessLevelFinal(server, 1, '1');
      await enableDriver(server);
      await set;
    });

    it('checker will return controller and peripheral errors', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const check = device.canSetState(JSON.stringify(xmcc4State));
      await checkDevice(server, '1234');
      await checkAxis(server, '1234');

      await expect(check).to.eventually.deep.eq({
        error: 'Cannot set a state with device ID 30342 to a device with ID 1234',
        axisErrors: [{
          axisNumber: 1,
          error: 'Cannot set a state with peripheral ID 70250 to a peripheral with ID 1234.',
        }],
      });
    });

    it('Will apply state to an integrated device', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.setState(JSON.stringify(xlhmState));

      await checkDevice(server, xlhmState.deviceId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xlhmState);
      await setTriggers(server, xlhmState, 4);
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.stream, 'stream');
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.pvt, 'pvt');
      await setStorage(server, 1, 0, xlhmState.storage);
      await setServoTuning(server, xlhmState.servoTuning);
      await setStoredPositions(server, xlhmState.storedPositions);
      await setStorage(server, 1, 1, xlhmState.axisStorage);
      await setAccessLevelFinal(server, 1, xlhmState.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('handles errors', async () => {
      testSetErrors = true;
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.setState(JSON.stringify(xlhmState));

      await checkDevice(server, xlhmState.deviceId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xlhmState);
      await setTriggers(server, xlhmState, 4);
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.stream, 'stream');
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.pvt, 'pvt');
      await setStorage(server, 1, 0, xlhmState.storage);
      await setServoTuning(server, xlhmState.servoTuning);
      await setStoredPositions(server, xlhmState.storedPositions);
      await setStorage(server, 1, 1, xlhmState.axisStorage);
      await setAccessLevelFinal(server, 1, xlhmState.settings['system.access']);

      await expect(promise).rejectedWith(SetDeviceStateFailedException);
      const exception: SetDeviceStateFailedException = await promise.catch(e => e);
      expect(exception.details).to.deep.eq({
        settings: [
          'resolution'
        ],
        streamBuffers: [
          'stream 1 setup store 2 1'
        ],
        pvtBuffers: [
          'pvt 1 setup store 2 1'
        ],
        triggers: [
          'trigger 1 disable',
          'trigger 2 when 1 pos >= 300000',
          'trigger 2 action a 1 move min',
          'trigger 2 action b none',
          'trigger 2 enable',
          'trigger 3 disable',
          'trigger 4 disable'
        ],
        servoTuning: `Could not set kd to 5505.322319872: Command "servo 1 set kd 5505.322319872" rejected: ${BADDATA_REASON}`,
        storedPositions: [
          `Could not store position 0 at 1: Command "tools storepos 1 0" rejected: ${BADDATA_REASON}`
        ],
        storage: [
          `Could not store value with key foo: Command "storage set foo bar" rejected: ${BADDATA_REASON}`,
          `Could not store value with key afoo: Command "storage axis set afoo abar" rejected: ${BADDATA_REASON}`
        ],
        peripherals: [],
      });
    });

    it('Will apply stream buffers to device with no stream buffers', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.setState(JSON.stringify(xlhmState));

      await checkDevice(server, xlhmState.deviceId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xlhmState);
      await setTriggers(server, xlhmState, 4);
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.stream, 'stream');
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.pvt, 'pvt');
      await setStorage(server, 1, 0, xlhmState.storage);
      await setServoTuning(server, xlhmState.servoTuning);
      await setStoredPositions(server, xlhmState.storedPositions);
      await setStorage(server, 1, 1, xlhmState.axisStorage);
      await setAccessLevelFinal(server, 1, xlhmState.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('Will apply state to an axis without hw modification', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = axis.setState(JSON.stringify(axisState));

      await checkAxis(server, axisState.axis.peripheralId);
      await disableDriver(server);

      await restoreHwModProducts(server, true, 0, 0);
      await setAccessLevelHigh(server, 1, true);
      await setSettings(server, 1, 1, axisState.axis);
      await setServoTuning(server, axisState.axis.servoTuning);
      await setStoredPositions(server, axisState.axis.storedPositions);
      await setAccessLevelFinal(server, 1, '1');
      await enableDriver(server);
      await promise;
    });

    it('Will restore a hardware modified axis to remove modifications', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = axis.setState(JSON.stringify(noModAxisState));
      await checkAxis(server, noModAxisState.axis.peripheralId);
      await disableDriver(server);

      await restoreHwModProducts(server, true, 1, 0);
      await setAccessLevelHigh(server, 1, true);
      await setSettings(server, 1, 0, noModAxisState.axis);
      await setAccessLevelFinal(server, 1, noModAxisState.axis.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('Will apply state to an axis with hardware modification', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = axis.setState(JSON.stringify(axisHwModState));

      await checkAxis(server, axisHwModState.axis.peripheralId);
      await disableDriver(server);

      await setAccessLevelHigh(server, 1, true);
      const settingsSet = await setSettings(server, 1, 1, axisHwModState.axis);
      expect(settingsSet[0]).to.eq('resolution', 'Resolution should be set before other settings');
      expect(settingsSet.slice(1, 4)).to.deep.eq([
        'encoder.2.signal.min',
        'limit.max',
        'limit.min',
      ], 'Settings with min|max suffix should be set before the rest.');
      expect(settingsSet[settingsSet.length - 1]).to.eq('knob.enable', 'knob.enable is set last');
      const otherSettings = settingsSet.slice(5, -1);
      expect(otherSettings.sort()).to.deep.eq(otherSettings, 'The rest of the settings should be sorted alphabetically.');
      await setAccessLevelFinal(server, 1, '1');
      await enableDriver(server);
      await promise;
    });

    it('Will apply state to a controller with a peripheral', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = device.setState(JSON.stringify(xmcc4State));

      await checkDevice(server, xmcc4State.deviceId);
      await checkAxis(server, xmcc4State.axes[1].peripheralId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xmcc4State);

      await setSettings(server, 1, 1, xmcc4State.axes[1]);
      await setStoredPositions(server, xmcc4State.axes[1].storedPositions);
      await setAccessLevelFinal(server, 1, xmcc4State.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('Does not set axis state if device only param is true', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = device.setState(JSON.stringify(xmcc4State), { deviceOnly: true });

      await checkDevice(server, xmcc4State.deviceId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xmcc4State);

      await setAccessLevelFinal(server, 1, xmcc4State.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('handles errors', async () => {
      testSetErrors = true;
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = device.setState(JSON.stringify(xmcc4State));

      await checkDevice(server, xmcc4State.deviceId);
      await checkAxis(server, xmcc4State.axes[1].peripheralId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xmcc4State);

      await setSettings(server, 1, 1, xmcc4State.axes[1]);
      await setStoredPositions(server, xmcc4State.axes[1].storedPositions);
      await setAccessLevelFinal(server, 1, xmcc4State.settings['system.access']);

      await expect(promise).rejectedWith(SetDeviceStateFailedException);
      const exception: SetDeviceStateFailedException = await promise.catch(e => e);
      expect(exception.details).to.deep.eq({
        settings: [
          'comm.alert'
        ],
        streamBuffers: [],
        pvtBuffers: [],
        triggers: [],
        servoTuning: '',
        storedPositions: [],
        storage: [],
        peripherals: [
          {
            axisNumber: 1,
            settings: [
              'limit.max'
            ],
            servoTuning: '',
            storedPositions: [
              `Could not store position 5 at 1: Command "tools storepos 1 5" rejected: ${BADDATA_REASON}`
            ],
            storage: []
          }
        ]
      });
    });

    it('Will restore buffers to a device that does not support buffer list command', async () => {
      await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

      const promise = device.setState(JSON.stringify(xlhmState));

      await checkDevice(server, xlhmState.deviceId);
      await disableDriver(server);
      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xlhmState);
      await setTriggers(server, xlhmState, 4);

      // stream buffer list command is not supported
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.stream, 'stream', false);
      await setStreamBuffers(server, xlhmState, xlhmChangedBuffers.pvt, 'pvt', false);

      await setStorage(server, 1, 0, xlhmState.storage);
      await setServoTuning(server, xlhmState.servoTuning);
      await setStoredPositions(server, xlhmState.storedPositions);
      await setStorage(server, 1, 1, xlhmState.axisStorage);
      await setAccessLevelFinal(server, 1, xlhmState.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    describe('Current Control Settings', () => {
      it('Will set ictrl settings before others', async () => {
        await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

        const promise = device.setState(JSON.stringify(deviceWithIctrl));

        await checkDevice(server, deviceWithIctrl.deviceId);

        await disableDriver(server);
        await restoreHwModProducts(server, false, 0, 0);
        await setAccessLevelHigh(server, 1, false);

        const setIctrlType = parseCommand(await server.pop());
        expect(setIctrlType.data).to.eq('set ictrl.type 6');
        await server.push(`@1 0 ${setIctrlType.id} OK IDLE -- 0`);

        const setIctrlAdv = parseCommand(await server.pop());
        expect(setIctrlAdv.data).to.eq('set ictrl.advance.a 1.22');
        await server.push(`@1 0 ${setIctrlAdv.id} OK IDLE -- 0`);

        const setKnobMode = parseCommand(await server.pop());
        expect(setKnobMode.data).to.eq('set knob.mode 0');
        await server.push(`@1 0 ${setKnobMode.id} OK IDLE -- 0`);

        await setAccessLevelFinal(server, 1, '1');
        await enableDriver(server);

        await promise;
      });

      it('Will skip all ictrl settings if ictrl.type cannot be set', async () => {
        await DeviceXLHM.identify(device, server, DeviceXLHM.FWType.FW7);

        const promise = device.setState(JSON.stringify(deviceWithIctrl));

        await checkDevice(server, deviceWithIctrl.deviceId);

        await disableDriver(server);
        await restoreHwModProducts(server, false, 0, 0);
        await setAccessLevelHigh(server, 1, false);

        const setIctrlType = parseCommand(await server.pop());
        expect(setIctrlType.data).to.eq('set ictrl.type 6');
        await server.push(`@1 0 ${setIctrlType.id} RJ IDLE -- BADDATA`);

        const setKnobMode = parseCommand(await server.pop());
        expect(setKnobMode.data).to.eq('set knob.mode 0');
        await server.push(`@1 0 ${setKnobMode.id} OK IDLE -- 0`);

        await setAccessLevelFinal(server, 1, '1');
        await enableDriver(server);

        const response = await promise;
        expect(response.warnings).to.deep.eq(['Current controller type 6 is not supported. Default current tuning will be used.']);
      });
    });
  });

  describe('Storage commands', () => {
    it('Set storage', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const promise = device.setState(JSON.stringify(xmcc4WithStorageState));

      await checkDevice(server, xmcc4State.deviceId);
      await checkAxis(server, xmcc4WithStorageState.axes[1].peripheralId);
      await disableDriver(server);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, xmcc4WithStorageState);
      await setStorage(server, 1, 0, xmcc4WithStorageState.storage);

      await setSettings(server, 1, 1, xmcc4WithStorageState.axes[1]);
      await setStoredPositions(server, xmcc4WithStorageState.axes[1].storedPositions);
      await setStorage(server, 1, 1, xmcc4WithStorageState.axes[1].storage);

      await setAccessLevelFinal(server, 1, xmcc4WithStorageState.settings['system.access']);
      await enableDriver(server);

      await promise;
    });

    it('Get storage', async () => {
      await DeviceXMCC4.identify(device, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_21);

      const statePromise = device.getState();

      await getDeviceSettings(server, 1, xmcc4WithStorageState);
      await getAxes(server, 1, 4, xmcc4WithStorageState.axes);
      await getTriggers(server, 1, xmcc4State, 0);
      await getStreamBuffers(server, xmcc4State, 'stream');
      await getStreamNotSupported(server, 'pvt');
      await getStorage(server, device.deviceAddress, 0, xmcc4WithStorageState.storage);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(xmcc4WithStorageState);
    });
  });

  /* Disabled until firmware is settled
  describe('Process Controllers', () => {
    it('Sets state', async () => {
      await DeviceXSCA4.identify(device, server);

      const promise = device.setState(JSON.stringify(processControllerState));

      await checkDevice(server, processControllerState.deviceId);
      await checkAxis(server, processControllerState.axes[1].peripheralId);
      await checkAxis(server, processControllerState.axes[2].peripheralId);
      await checkAxis(server, processControllerState.axes[3].peripheralId);
      await checkAxis(server, processControllerState.axes[4].peripheralId);

      await restoreHwModProducts(server, false, 0, 0);
      await setAccessLevelHigh(server, 1, false);
      await setSettings(server, 1, 0, processControllerState);

      await setSettings(server, 1, 1, processControllerState.axes[1]);
      await setSettings(server, 1, 2, processControllerState.axes[2]);
      await setSettings(server, 1, 3, processControllerState.axes[3]);
      await setSettings(server, 1, 4, processControllerState.axes[4]);

      await setAccessLevelFinal(server, 1, processControllerState.settings['system.access']);

      await promise;
    });

    it('Gets state', async () => {
      await DeviceXSCA4.identify(device, server, Number.parseInt(processControllerState.serial, 10));

      const statePromise = device.getState();

      await getDeviceSettings(server, 1, processControllerState);
      await getAxes(server, 1, 4, processControllerState.axes);
      await getTriggers(server, 1, processControllerState, 0);
      await getStreamBuffers(server, processControllerState, 'stream');
      await getStreamNotSupported(server, 'pvt');
      await getStorage(server, device.deviceAddress);

      const state = JSON.parse(await statePromise);
      expect(state).to.deep.eq(processControllerState);
    });
  });
  */
});
