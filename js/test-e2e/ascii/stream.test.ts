import { expect } from 'chai';
import _ from 'lodash';
import {
  DeviceNotIdentifiedException,
  InvalidArgumentException,
  StreamExecutionException,
  StreamModeException,
  StreamMovementFailedException,
  StreamMovementInterruptedException,
  Time,
  DeviceBusyException,
  Length,
  RotationDirection,
  StreamDiscontinuityException,
  BadDataException,
} from '../../src';
import { Connection, Device, Stream, StreamMode, StreamAxisType, DigitalOutputAction } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { StreamSetupFailedException } from '../../src/exceptions/stream_setup_failed_exception';
import { parseCommand } from './protocol';

describe('Stream', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let eqAndPushX: (pushStr: string, popStr: string) => Promise<unknown>;

  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1, 0);
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe ('tests for listing buffers', () => {
    it('lists stream buffers empty', async () => {
      const promise = device.streams.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 stream buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.be.empty;
    });

    it('lists stream buffer single', async () => {
      const promise = device.streams.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 stream buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 buffer 42');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.have.length(1);
      expect(result).to.contain(42);
    });

    it('lists stream buffer multiple', async () => {
      const promise = device.streams.listBufferIds();

      expect(await server.pop()).to.eq('/1 0 00 stream buffer list');
      expect(await server.pop()).to.eq('/1 0 00');
      await server.push('@01 0 00 OK IDLE -- 0');
      await server.push('#01 0 00 buffer 42');
      await server.push('#01 0 00 buffer 1');
      await server.push('#01 0 00 buffer 21');
      await server.push('@01 0 00 OK IDLE -- 0');

      const result = await promise;

      expect(result).to.have.length(3);
      expect(result).to.contain(42);
      expect(result).to.contain(1);
      expect(result).to.contain(21);
    });
  });

  describe('setup commands', () => {
    let stream: Stream;
    beforeEach(() => {
      stream = device.streams.getStream(1);
    });

    describe('setup live', () => {
      it('disables the first time and then sets up', async () => {
        const promise = stream.setupLive(1, 2);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup live 1 2', 'OK IDLE -- 0');
        await promise;
      });
      it('rejects if the stream is already setup', async () => {
        const buffer = device.streams.getBuffer(1);
        const setupStorePromise = stream.setupStoreArbitraryAxes(buffer, 3);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup store 1 3', 'OK IDLE -- 0');
        await setupStorePromise;

        const setupLivePromise = stream.setupLive(1, 2);
        await expect(setupLivePromise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await setupLivePromise.catch(e => e);
        expect(error.message).to.eq('The stream is already setup. Disable the stream first.');
      });
      it('rejects invalid axis selection', async () => {
        const promise = stream.setupLive(4, 1);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup live 4 1', 'RJ IDLE -- BADDATA');
        await expect(promise).to.be.rejectedWith(StreamSetupFailedException);
        const error: StreamSetupFailedException = await promise.catch(e => e);
        expect(error.message).to.startWith('Stream setup failed.');
      });
    });

    describe('setup store', () => {
      it('disables the first time and then sets up', async () => {
        const buffer = device.streams.getBuffer(1);
        const promise = stream.setupStore(buffer, 1, 2);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup store 1 2', 'OK IDLE -- 0');
        await promise;
      });
      it('disables the first time and then sets up - arbitrary axes mode', async () => {
        const buffer = device.streams.getBuffer(1);
        const promise = stream.setupStoreArbitraryAxes(buffer, 3);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup store 1 3', 'OK IDLE -- 0');
        await promise;
      });
      it('rejects with StreamSetupFailedException in BADDATA case, and gives troubleshooting info', async () => {
        const buffer = device.streams.getBuffer(3);
        const promise = stream.setupStore(buffer, 1, 2, 3);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup store 3 3', 'RJ IDLE -- BADDATA');

        await expect(promise).to.be.rejectedWith(StreamSetupFailedException);
        const exception: StreamSetupFailedException = await promise.catch(e => e);
        expect(exception.message).to.startWith('Command "stream 1 setup store 3 3" rejected: BADDATA');
        expect(exception.message).to.endsWith('Make sure that the stream buffer exists on the device and is empty.');
      });
      it('rejects for duplicated axes', async () => {
        const buffer = device.streams.getBuffer(3);
        const promise = stream.setupStore(buffer, 1, 2, 1);
        const exception: InvalidArgumentException = await expect(promise).to.be.rejectedWith(InvalidArgumentException);
        expect(exception.message).to.eq('Axis with index 2 is already in the stream.');
      });
    });

    describe('disable', () => {
      it('disables a stream', async () => {
        const promise = stream.disable();
        // the first disable is for retrieving the stream for the first time
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await promise;
      });
      it('removes a cached stream in the zml instance', async () => {
        const setupPromise = stream.setupLive(1);
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await eqAndPushX('stream 1 setup live 1', 'OK IDLE -- 0');
        await setupPromise;

        const disablePromise = stream.disable();
        await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
        await disablePromise;

        const movePromise = stream.lineAbsolute({ value: 1074 });
        await expect(movePromise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await movePromise.catch(e => e);
        expect(error.message).to.eq('The stream is disabled. Set up the stream in one of the permissable mode(s) for this action, \
those being: [Live Store StoreArbitrary].');
      });
    });
  });

  describe('tests for setup streams', () => {
    let live3DStream: Stream;
    let liveStream: Stream;
    let storeStream: Stream;
    let storeArbitraryStream: Stream;
    let disabledStream: Stream;

    beforeEach(async () => {
      live3DStream = device.streams.getStream(1);
      const setupLive3DPromise = live3DStream.setupLive(1, 2, 3);
      await eqAndPushX('stream 1 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 1 setup live 1 2 3', 'OK IDLE -- 0');
      await setupLive3DPromise;

      liveStream = device.streams.getStream(2);
      const setupLivePromise = liveStream.setupLive(1, 2);
      await eqAndPushX('stream 2 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 2 setup live 1 2', 'OK IDLE -- 0');
      await setupLivePromise;

      storeStream = device.streams.getStream(3);
      const firstBuffer = device.streams.getBuffer(1);
      const setupStorePromise = storeStream.setupStore(firstBuffer, 1, 2);
      await eqAndPushX('stream 3 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 3 setup store 1 2', 'OK IDLE -- 0');
      await setupStorePromise;

      storeArbitraryStream = device.streams.getStream(4);
      const secondBuffer = device.streams.getBuffer(2);
      const setupStoreArbitraryPromise = storeArbitraryStream.setupStoreArbitraryAxes(secondBuffer, 2);
      await eqAndPushX('stream 4 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 4 setup store 2 2', 'OK IDLE -- 0');
      await setupStoreArbitraryPromise;

      disabledStream = device.streams.getStream(5);
      const disablePromise = disabledStream.disable();
      await eqAndPushX('stream 5 setup disable', 'OK IDLE -- 0');
      await eqAndPushX('stream 5 setup disable', 'OK IDLE -- 0');
      await disablePromise;
    });

    describe('StoreArbitraryAxes specifics', () => {
      it('does not allow unit conversion', async () => {
        const linePromise = storeArbitraryStream.lineAbsoluteOn([0], [{ value: 1074, unit: Length.mm }]);
        const error: StreamModeException = await linePromise.catch(e => e);
        expect(error.message).to.eq([
          'Command failed because it requires unit conversion, and the stream was setup with arbitrary axes.',
          'Setup stream with specific axes.'
        ].join(' '));
      });
      it('allows native units', async () => {
        const linePromise = storeArbitraryStream.lineAbsoluteOn([0], [{ value: 1074 }]);
        await eqAndPushX('stream 4 on a line abs 1074', 'OK IDLE -- 0');
        await linePromise;
      });
    });

    describe('error handling (all commands)', () => {
      describe('Live mode', () => {
        it('clears flags for each axis in stream if reply has warning flag concerning the stream,\
 and returns first relevant warning flag on streams axes', async () => {
          const promise = liveStream.genericCommand('cmd');
          await eqAndPushX('stream 2 cmd', 'OK IDLE FS 0');

          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FS 01 WT');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FS 02 FS NI');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FS 01 WT');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FS 02 FS NI');

          await expect(promise).to.be.rejectedWith(StreamMovementFailedException);
          const error: StreamMovementFailedException = await promise.catch(e => e);
          expect(error.message).to.eq('The stream movement may have failed because\
 a fault was observed: Stalled and Stopped (FS).');
          expect(error.details).to.deep.eq({
            reason: 'Stalled and Stopped (FS)',
            warnings: ['FS', 'NI'],
          });
        });

        it('prioritizes FB flags and returns special exception', async () => {
          const promise = liveStream.genericCommand('cmd');
          await eqAndPushX('stream 2 cmd', 'OK IDLE FS 0');

          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FS 02 FS FB');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FB 01 FB');
          await eqAndPushX('stream 2 info', 'OK BUSY FS live 3 205 205 153600 bufempty');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FS 02 FS FB');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FB 01 FB');

          await expect(promise).to.be.rejectedWith(StreamExecutionException);
          const error: StreamExecutionException = await promise.catch(e => e);
          expect(error.details).to.deep.equal({
            errorFlag: 'bufempty',
            reason: 'A stream call action called a buffer which is empty.',
          });
        });

        it('returns first relevant flag', async () => {
          const promise = liveStream.genericCommand('cmd');
          await eqAndPushX('stream 2 cmd', 'OK IDLE FS 0');

          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FS 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FS 02 FS ND');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FS 01 ND');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FS 02 FS ND');

          await expect(promise).to.be.rejectedWith(StreamMovementFailedException);
          const error: StreamMovementFailedException = await promise.catch(e => e);
          expect(error.message).to.eq('The stream movement may have failed because\
 a fault was observed: Stalled and Stopped (FS).');
          expect(error.details.reason).to.eq('Stalled and Stopped (FS)');
          expect(error.details.warnings).to.include.members(['FS', 'ND']);
        });

        describe('FB flag', () => {
          it('ignores FB flag if not present for stream axes', async () => {
            const streamActionPromise = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'OK BUSY FB 0');
            await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY -- 00');
            await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY -- 00');

            await streamActionPromise;
          });

          it('handles specific error (e.g. axislimit)', async () => {
            const streamActionPromise = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'OK BUSY FB 0');
            await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY FB 01 FB');
            await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY FB 01 FB');
            await eqAndPushX('stream 2 info', 'OK BUSY FB live 3 205 205 153600 axislimit');
            await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY FB 01 FB');
            await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY FB 01 FB');

            await expect(streamActionPromise).to.be.rejectedWith(StreamExecutionException);
            const error: StreamExecutionException = await streamActionPromise.catch(e => e);
            expect(error.message).to.eq('The device has indicated that a previously executed stream action has failed. \
The reason for the failure is: \
A path segment tried to move an axis to a position less than limit.min or greater than limit.max.');
            expect(error.details).to.deep.equal({
              errorFlag: 'axislimit',
              reason: 'A path segment tried to move an axis to a position less than limit.min or greater than limit.max.',
            });
          });
        });
      });

      describe('Store Mode and Store Arbitrary Mode', () => {
        it('does not check for warning flags, as no axis is being controlled', async () => {
          const promise = storeStream.genericCommand('cmd');
          await eqAndPushX('stream 3 cmd', 'OK IDLE FS 0');
          await promise;
        });
      });

      describe('all modes', () => {
        describe('AGAIN rejection reason', () => {
          it('handles AGAIN reason by repeating action until there is room', async () => {
            const streamAction = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'RJ BUSY -- AGAIN');
            await eqAndPushX('stream 2 cmd', 'RJ BUSY -- AGAIN');
            await eqAndPushX('stream 2 cmd', 'RJ BUSY -- AGAIN');
            await eqAndPushX('stream 2 cmd', 'RJ BUSY -- AGAIN');
            await eqAndPushX('stream 2 cmd', 'OK BUSY -- 0');
            await streamAction;
          });
        });
        describe('BADDATA rejection reason', () => {
          it('checks for stream mode when device replies BADDATA', async () => {
            // exists to handle disabled stream by certain actions e.g. knob or stop command
            const streamAction = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'RJ IDLE -- BADDATA');
            await eqAndPushX('stream 2 info', 'OK IDLE -- disabled - - - - -');

            await expect(streamAction).to.be.rejectedWith(StreamModeException);
            const error: StreamModeException = await streamAction.catch(e => e);
            expect(error.message).to.eq('The stream was disabled during operation. Setup the stream again.');
          });
          it('returns BADDATA if the stream mode is accurate', async () => {
            const streamAction = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'RJ IDLE -- BADDATA');
            await eqAndPushX('stream 2 info', 'OK IDLE -- live 2 20 30 15 -');

            await expect(streamAction).to.be.rejectedWith(BadDataException);
            const error: BadDataException = await streamAction.catch(e => e);
            expect(error.message).to.startWith('Command "stream 2 cmd" rejected: BADDATA');
          });
          it('checks for both warnings and disabled stream', async () => {
            // exists to handle disabled stream by certain faults
            const streamAction = liveStream.genericCommand('cmd');
            await eqAndPushX('stream 2 cmd', 'RJ IDLE FS BADDATA');
            await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE -- 0');
            await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE FS 01 FS');
            await eqAndPush(server, 1, 1, 'warnings clear', 'OK IDLE -- 0');
            await eqAndPush(server, 1, 2, 'warnings clear', 'OK IDLE FS 01 FS');
            await eqAndPushX('stream 2 info', 'OK IDLE -- disabled - - - - -');

            await expect(streamAction).to.be.rejectedWith(StreamMovementFailedException);
            const error: StreamMovementFailedException = await streamAction.catch(e => e);
            expect(error.message).to.eq('The stream movement may have failed because a fault was observed: Stalled and Stopped (FS).');

            expect(liveStream.mode).to.eq(StreamMode.DISABLED);
          });
        });
      });
    });

    describe('movement', () => {
      // some example measurements, abbreviated to be concise
      const A = { value: 1090 };
      const B = { value: 10 };
      const C = { value: 19 };

      it('rejects if target axes are out of bounds', async () => {
        const promise = live3DStream.lineAbsoluteOn([2, 3], [A, B]);
        await expect(promise).to.be.rejectedWith(InvalidArgumentException);
        const error: InvalidArgumentException = await promise.catch(e => e);
        expect(error.message).to.eq(
          'The stream has no axis with index 3. \
Axis indices must be greater than or equal to 0 and less than the number of axes in the stream (3).'
        );
      });

      it('rejects if command needs unit conversion and not identified', async () => {
        const streamActionPromise = liveStream.lineAbsolute({ value: 10, unit: Length.mm });
        await expect(streamActionPromise).to.be.rejectedWith(DeviceNotIdentifiedException);
        const error: DeviceNotIdentifiedException = await streamActionPromise.catch(e => e);
        expect(error.message).to.eq('Cannot perform this operation. Device number 1 is not identified. \
Call device.identify() to enable this operation.');
      });

      it('rejects if number of axes to target and number of arguments do not match', async () => {
        const promise = live3DStream.lineAbsoluteOn([0, 1], [A]);
        await expect(promise).to.be.rejectedWith(InvalidArgumentException);
        const error: InvalidArgumentException = await promise.catch(e => e);
        expect(error.message).to.eq('The number of axes and the number of line endpoints are not equal.');
      });

      describe('arcAbsolute', () => {
        it('sends proper request', async () => {
          const promise = liveStream.arcAbsolute(RotationDirection.CW, A, B, A, B);
          await eqAndPushX('stream 2 arc abs cw 1090 10 1090 10', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('arcRelative', () => {
        it('sends proper request', async () => {
          const promise = storeStream.arcRelative(RotationDirection.CCW, A, B, A, B);
          await eqAndPushX('stream 3 arc rel ccw 1090 10 1090 10', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('circleAbsolute', () => {
        it('sends proper request', async () => {
          const promise = storeStream.circleAbsolute(RotationDirection.CCW, A, B);
          await eqAndPushX('stream 3 circle abs ccw 1090 10', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('circleRelative', () => {
        it('sends proper request', async () => {
          const promise = liveStream.circleRelative(RotationDirection.CW, A, B);
          await eqAndPushX('stream 2 circle rel cw 1090 10', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('lineAbsolute', () => {
        it('sends proper request', async () => {
          const promise = live3DStream.lineAbsolute(A, B, C);
          await eqAndPushX('stream 1 line abs 1090 10 19', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('lineAbsoluteOn', () => {
        it('sends proper request', async () => {
          const promise = live3DStream.lineAbsoluteOn([1, 2], [C, A]);
          await eqAndPushX('stream 1 on b c line abs 19 1090', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('lineRelative', () => {
        it('sends proper request', async () => {
          const promise = live3DStream.lineRelative(A, B, C);
          await eqAndPushX('stream 1 line rel 1090 10 19', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('lineRelativeOn', () => {
        it('sends proper request', async () => {
          const promise = live3DStream.lineRelativeOn([0, 2], [A, B]);
          await eqAndPushX('stream 1 on a c line rel 1090 10', 'OK BUSY -- 0');
          await promise;
        });
      });

      describe('arcOn', () => {
        it('rejects if two axes are not specified', async () => {
          const promise = live3DStream.arcAbsoluteOn([0], RotationDirection.CCW, A, A, B, C);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const error: InvalidArgumentException = await promise.catch(e => e);
          expect(error.message).to.eq('Invalid number of target axes (expected 2).');
        });

        describe('arcAbsoluteOn', () => {
          it('sends proper request', async () => {
            const promise = live3DStream.arcAbsoluteOn([0, 2], RotationDirection.CCW, A, A, B, C);
            await eqAndPushX('stream 1 on a c arc abs ccw 1090 1090 10 19', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('arcRelativeOn', () => {
          it('sends proper request', async () => {
            const promise = live3DStream.arcRelativeOn([0, 1], RotationDirection.CW, A, A, B, C);
            await eqAndPushX('stream 1 on a b arc rel cw 1090 1090 10 19', 'OK BUSY -- 0');
            await promise;
          });
        });
      });

      describe('circleOn', () => {
        it('rejects if two axes are not specified', async () => {
          const promise = storeStream.circleAbsoluteOn([0], RotationDirection.CLOCKWISE, A, B);
          await expect(promise).to.be.rejectedWith(InvalidArgumentException);
          const error: InvalidArgumentException = await promise.catch(e => e);
          expect(error.message).to.eq('Invalid number of target axes (expected 2).');
        });
        describe('circleAbsoluteOn', () => {
          it('sends proper request', async () => {
            const promise = live3DStream.circleAbsoluteOn([1, 2], RotationDirection.CW, A, B);
            await eqAndPushX('stream 1 on b c circle abs cw 1090 10', 'OK BUSY -- 0');
            await promise;
          });
        });

        describe('circleRelativeOn', () => {
          it('sends proper request', async () => {
            const promise = live3DStream.circleRelativeOn([2, 1], RotationDirection.COUNTERCLOCKWISE, A, B);
            await eqAndPushX('stream 1 on c b circle rel ccw 1090 10', 'OK BUSY -- 0');
            await promise;
          });
        });
      });
    });

    describe('cork', () => {
      it('sends proper request', async () => {
        const promise = liveStream.cork();
        await eqAndPushX('stream 2 fifo cork', 'OK IDLE -- 0');
        await promise;
      });
      it('returns specific error if stream not in right mode after rejection', async () => {
        const corkPromise = storeStream.cork();
        await expect(corkPromise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await corkPromise.catch(e => e);
        expect(error.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live]. Current mode: Store.');
      });
      it('throws exception when stream is busy', async () => {
        const promise = liveStream.cork();
        await eqAndPushX('stream 2 fifo cork', 'RJ BUSY -- STATUSBUSY');

        await expect(promise).to.be.rejectedWith(DeviceBusyException);
        const error: DeviceBusyException = await promise.catch(e => e);
        expect(error.message).to.eq('Cannot cork busy stream. Wait for stream to become idle.');
      });
    });

    describe('uncork', () => {
      it('sends proper request', async () => {
        const promise = liveStream.uncork();
        await eqAndPushX('stream 2 fifo uncork', 'OK IDLE -- 0');
        await promise;
      });
      it('returns specific error if stream not in right mode after rejection', async () => {
        const corkPromise = storeStream.uncork();
        await expect(corkPromise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await corkPromise.catch(e => e);
        expect(error.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live]. Current mode: Store.');
      });
    });

    describe('setHold', () => {
      it('sends proper request', async () => {
        let promise = liveStream.setHold(true);
        await eqAndPushX('stream 2 hold on', 'OK IDLE -- 0');
        await promise;

        promise = liveStream.setHold(false);
        await eqAndPushX('stream 2 hold off', 'OK IDLE -- 0');
        await promise;
      });

      it('can be sent when other commands are being streamed', async () => {
        const linePromise = liveStream.lineAbsolute({ value: 0 }, { value: 0 });
        await eqAndPushX('stream 2 line abs 0 0', 'RJ BUSY -- AGAIN');

        const promise = liveStream.setHold(true);

        let keepRejecting = true;
        const replier = (async () => {
          while (keepRejecting) {
            const command = parseCommand(await server.pop());
            if (command.strNoId.includes('line abs')) {
              await server.push(`@1 0 ${command.id} RJ BUSY -- AGAIN`);
            } else {
              expect(command.strNoId).to.eq('/1 0 id stream 2 hold on');
              await server.push(`@1 0 ${command.id} OK BUSY -- 0`);
            }
          }

          await eqAndPushX('stream 2 line abs 0 0', 'OK BUSY -- 0');
        })();

        await promise;

        keepRejecting = false;
        await replier;
        await linePromise;
      });
    });

    describe('isBusy', () => {
      it('returns true if first axis in stream is busy', async () => {
        const promise = liveStream.isBusy();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        expect(await promise).to.eq(true);
      });
      it('returns false if first axis in stream is idle', async () => {
        const promise = liveStream.isBusy();
        await eqAndPush(server, 1, 1, '', 'OK IDLE -- 0');
        expect(await promise).to.eq(false);
      });
      it('rejects if not in Live mode', async () => {
        const promise = storeStream.isBusy();
        await expect(promise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await promise.catch(e => e);
        expect(error.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live]. Current mode: Store.');
      });
    });

    describe('waitUntilIdle', () => {
      it('waits until the first axis in stream is IDLE', async () => {
        const promise = liveStream.waitUntilIdle();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE -- 0');
        await promise;
      });
      it('does not throw error when throwErrorOnFault is false', async () => {
        const promise = liveStream.waitUntilIdle({ throwErrorOnFault: false });
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY FQ 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY FQ 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE FQ 0');
        await promise;
      });
      it('rejects if not in Live mode', async () => {
        const promise = storeStream.waitUntilIdle();
        await expect(promise).to.be.rejectedWith(StreamModeException);
        const error: StreamModeException = await promise.catch(e => e);
        expect(error.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live]. Current mode: Store.');
      });
      it('throws exception when fault flag is observed', async () => {
        const promise = liveStream.waitUntilIdle();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE FQ 01 FQ');
        await eqAndPush(server, 1, 1, 'warnings clear', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 2, 'warnings clear', 'OK IDLE FQ 01 FQ');
        await expect(promise).to.be.rejectedWith(StreamMovementFailedException);
        const error: StreamMovementFailedException = await promise.catch(e => e);
        expect(error.message).to.eq('The stream movement may have failed because a fault was observed: Encoder Error (FQ).');
        expect(error.details).to.deep.eq({
          reason: 'Encoder Error (FQ)',
          warnings: ['FQ'],
        });
      });
      it('throws exception when movement interrupted', async () => {
        const promise = liveStream.waitUntilIdle();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE NC 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE NC 02 NC NI');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE -- 0');
        await eqAndPush(server, 1, 1, 'warnings clear', 'OK IDLE NC 02 NC NI');
        await eqAndPush(server, 1, 2, 'warnings clear', 'OK IDLE -- 0');
        await expect(promise).to.be.rejectedWith(StreamMovementInterruptedException);
        const error: StreamMovementInterruptedException = await promise.catch(e => e);
        expect(error.message).to.eq(
          'The stream movement was interrupted due to a fault: Manual Control (NC), Movement Interrupted (NI).');
        expect(error.details).to.deep.eq({
          reason: 'Manual Control (NC), Movement Interrupted (NI)',
          warnings: ['NC', 'NI'],
        });
      });
      it('throws movement failed rather than movement interrupted', async () => {
        const promise = liveStream.waitUntilIdle();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE FS 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE NC 02 NC NI');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE FS 01 FS');
        await eqAndPush(server, 1, 1, 'warnings clear', 'OK IDLE NC 02 NC NI');
        await eqAndPush(server, 1, 2, 'warnings clear', 'OK IDLE FS 01 FS');
        await expect(promise).to.be.rejectedWith(StreamMovementFailedException);
      });
      it('does not throw stream discontinuity error', async () => {
        liveStream.treatDiscontinuitiesAsError();
        const promise = liveStream.waitUntilIdle();
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1, 1, '', 'OK IDLE ND 0');
        await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE ND 01 ND');
        await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE ND 01 ND');
        await promise;
      });
    });

    describe('wait', () => {
      it('sends proper request', async () => {
        const promise = liveStream.wait(5);
        await eqAndPushX('stream 2 wait 5', 'OK BUSY -- 0');
        await promise;
      });
      it('does unit conversion by using hard coded table, not DB', async () => {
        const promise = liveStream.wait(.5, Time.s);
        await eqAndPushX('stream 2 wait 500', 'OK BUSY -- 0');
        await promise;
      });
      it('works for microseconds too', async () => {
        const promise = liveStream.wait(5000, Time.MICROSECONDS);
        await eqAndPushX('stream 2 wait 5', 'OK BUSY -- 0');
        await promise;
      });
    });

    describe('toString', () => {
      it('gets stream string - 3D live', async () => {
        expect(live3DStream.toString()).to.eq('Stream 1 (Live) -> Axes [1, 2, 3] -> Device 1');
      });
      it('gets stream string - Live', async () => {
        expect(liveStream.toString()).to.eq('Stream 2 (Live) -> Axes [1, 2] -> Device 1');
      });
      it('gets stream string - StoreTarget', async () => {
        expect(storeStream.toString()).to.eq('Stream 3 (Store) -> Buffer 1 (Axes [1, 2]) -> Device 1');
      });
      it('gets stream string - StoreArbitrary', async () => {
        expect(storeArbitraryStream.toString()).to.eq('Stream 4 (StoreArbitrary) -> Buffer 2 (Axes 2) -> Device 1');
      });
      it('gets stream string - Disabled', async () => {
        expect(disabledStream.toString()).to.eq('Stream 5 (Disabled) -> Device 1');
      });
      it('gets stream string - Unknown', async () => {
        expect(device.streams.getStream(6).toString()).to.eq('Stream 6 (Unknown) -> Device 1');
      });
      it('returns representation for closed connection', async () => {
        await connection.close();
        expect(liveStream.toString()).to.eq('Stream 2 (Unknown) -> Device 1 -> Connection Closed');
      });
    });

    describe('call', () => {
      it('sends proper request', async () => {
        const buffer = device.streams.getBuffer(1);
        const promise = liveStream.call(buffer);
        await eqAndPushX('stream 2 call 1', 'OK IDLE -- 0');
        await promise;
      });
    });

    describe('io', () => {
      /* ZML-832 - Remove the following tests at a major release */
      describe('deprecated functions', () => {
        describe('waitDigitalInput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.waitDigitalInput(2, true);
            await eqAndPushX('stream 2 wait io di 2 == 1', 'OK IDLE -- 0');
            await promise;

            const promise2 = liveStream.waitDigitalInput(2, false);
            await eqAndPushX('stream 2 wait io di 2 == 0', 'OK IDLE -- 0');
            await promise2;
          });
        });

        describe('waitAnalogInput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.waitAnalogInput(1, '>=', 170);
            await eqAndPushX('stream 2 wait io ai 1 >= 170', 'OK IDLE -- 0');
            await promise;
          });
          it('changes != to <>', async () => {
            const promise = liveStream.waitAnalogInput(1, '!=', 50);
            await eqAndPushX('stream 2 wait io ai 1 <> 50', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setDigitalOutput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.setDigitalOutput(2, DigitalOutputAction.OFF);
            await eqAndPushX('stream 2 io set do 2 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllDigitalOutputs', () => {
          it('sets the values of all digital output channels', async () => {
            const promise = liveStream.setAllDigitalOutputs([
              DigitalOutputAction.ON,
              DigitalOutputAction.OFF,
              DigitalOutputAction.TOGGLE,
              DigitalOutputAction.KEEP
            ]);
            await eqAndPushX('stream 2 io set do port 1 0 t k', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAnalogOutput', () => {
          it('sends proper request', async () => {
            const promise = liveStream.setAnalogOutput(2, 1);
            await eqAndPushX('stream 2 io set ao 2 1', 'OK IDLE -- 0');
            await promise;
          });
          it('sends proper request, formats to number of decimal points specified', async () => {
            const promise = liveStream.setAnalogOutput(2, 2.12);
            await eqAndPushX('stream 2 io set ao 2 2.12', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setAllAnalogOutputs', () => {
          it('sends proper request', async () => {
            const promise = liveStream.setAllAnalogOutputs([0, 0, 5, 0]);
            await eqAndPushX('stream 2 io set ao port 0 0 5 0', 'OK IDLE -- 0');
            await promise;
          });
        });
      });

      describe('waitDigitalInput', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.waitDigitalInput(2, true);
          await eqAndPushX('stream 2 wait io di 2 == 1', 'OK IDLE -- 0');
          await promise;

          const promise2 = liveStream.io.waitDigitalInput(2, false);
          await eqAndPushX('stream 2 wait io di 2 == 0', 'OK IDLE -- 0');
          await promise2;
        });
      });

      describe('waitAnalogInput', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.waitAnalogInput(1, '>=', 170);
          await eqAndPushX('stream 2 wait io ai 1 >= 170', 'OK IDLE -- 0');
          await promise;
        });
        it('changes != to <>', async () => {
          const promise = liveStream.io.waitAnalogInput(1, '!=', 50);
          await eqAndPushX('stream 2 wait io ai 1 <> 50', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setDigitalOutput', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.setDigitalOutput(2, DigitalOutputAction.OFF);
          await eqAndPushX('stream 2 io set do 2 0', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setAllDigitalOutputs', () => {
        it('sets the values of all digital output channels', async () => {
          const promise = liveStream.io.setAllDigitalOutputs([
            DigitalOutputAction.ON,
            DigitalOutputAction.OFF,
            DigitalOutputAction.TOGGLE,
            DigitalOutputAction.KEEP
          ]);
          await eqAndPushX('stream 2 io set do port 1 0 t k', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setAllDigitalOutputsSchedule', () => {
        it('sets the values of all digital output channels with a schedule', async () => {
          const promise = liveStream.io.setAllDigitalOutputsSchedule(
            [DigitalOutputAction.ON, DigitalOutputAction.OFF],
            [DigitalOutputAction.TOGGLE, DigitalOutputAction.TOGGLE],
            200
          );
          await eqAndPushX('stream 2 io set do port 1 0 schedule 200 t t', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setAnalogOutput', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.setAnalogOutput(2, 1);
          await eqAndPushX('stream 2 io set ao 2 1', 'OK IDLE -- 0');
          await promise;
        });
        it('sends proper request, formats to number of decimal points specified', async () => {
          const promise = liveStream.io.setAnalogOutput(2, 2.12);
          await eqAndPushX('stream 2 io set ao 2 2.12', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setAllAnalogOutputs', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.setAllAnalogOutputs([0, 0, 5, 0]);
          await eqAndPushX('stream 2 io set ao port 0 0 5 0', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('setAllAnalogOutputsSchedule', () => {
        it('sets the values of all analog output channels with a schedule', async () => {
          const promise = liveStream.io.setAllAnalogOutputsSchedule([0, 3.3], [5, 0], 200);
          await eqAndPushX('stream 2 io set ao port 0 3.3 schedule 200 5 0', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('cancelDigitalOutputSchedule', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.cancelDigitalOutputSchedule(2);
          await eqAndPushX('stream 2 io cancel schedule do 2', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('cancelAllDigitalOutputsSchedule', () => {
        it('Cancels all', async () => {
          const promise = liveStream.io.cancelAllDigitalOutputsSchedule();
          await eqAndPushX('stream 2 io cancel schedule do port', 'OK IDLE -- 0');
          await promise;
        });

        it('Cancels some', async () => {
          const promise = liveStream.io.cancelAllDigitalOutputsSchedule([true, true, false, true]);
          await eqAndPushX('stream 2 io cancel schedule do port 1 1 0 1', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('cancelAnalogOutputSchedule', () => {
        it('sends proper request', async () => {
          const promise = liveStream.io.cancelAnalogOutputSchedule(4);
          await eqAndPushX('stream 2 io cancel schedule ao 4', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('cancelAllAnalogOutputsSchedule', () => {
        it('Cancels all', async () => {
          const promise = liveStream.io.cancelAllAnalogOutputsSchedule();
          await eqAndPushX('stream 2 io cancel schedule ao port', 'OK IDLE -- 0');
          await promise;
        });

        it('Cancels some', async () => {
          const promise = liveStream.io.cancelAllAnalogOutputsSchedule([false, true, false, true]);
          await eqAndPushX('stream 2 io cancel schedule ao port 0 1 0 1', 'OK IDLE -- 0');
          await promise;
        });
      });
    });

    describe('settings', () => {
      describe('set', () => {
        describe('setMaxTangentialAcceleration', () => {
          it('issues the command', async () => {
            const promise = liveStream.setMaxTangentialAcceleration(10);
            await eqAndPushX('stream 2 set tanaccel 10', 'OK IDLE -- 0');
            await promise;
          });
          it('accepts infinity and translates it to 0', async () => {
            const promise = liveStream.setMaxTangentialAcceleration(Infinity);
            await eqAndPushX('stream 2 set tanaccel 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setMaxCentripetalAcceleration', () => {
          it('issues the command', async () => {
            const promise = liveStream.setMaxCentripetalAcceleration(10);
            await eqAndPushX('stream 2 set centripaccel 10', 'OK IDLE -- 0');
            await promise;
          });
          it('accepts infinity and translates it to 0', async () => {
            const promise = liveStream.setMaxCentripetalAcceleration(Infinity);
            await eqAndPushX('stream 2 set centripaccel 0', 'OK IDLE -- 0');
            await promise;
          });
        });

        describe('setMaxSpeed', () => {
          it('issues the command', async () => {
            const promise = liveStream.setMaxSpeed(10);
            await eqAndPushX('stream 2 set maxspeed 10', 'OK IDLE -- 0');
            await promise;
          });
        });
      });

      describe('get', () => {
        describe('movement settings', () => {
          describe('getMaxCentripetalAcceleration', () => {
            it('sends proper request', async () => {
              const promise = liveStream.getMaxCentripetalAcceleration();
              await eqAndPushX('stream 2 info', 'OK IDLE -- live 2 20 30 15 -');
              await promise;
              expect(await promise).to.eq(20);
            });

            it('only allows in Live mode', async () => {
              const promise = storeStream.getMaxSpeed();
              await expect(promise).to.be.rejectedWith(StreamModeException);
              const error: StreamModeException = await promise.catch(e => e);
              expect(error.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live]. Current mode: Store.');
            });
          });

          describe('getMaxTangentialAcceleration', () => {
            it('sends proper request', async () => {
              const promise = liveStream.getMaxTangentialAcceleration();
              await eqAndPushX('stream 2 info', 'OK IDLE -- live 2 20 30 15 -');
              expect(await promise).to.eq(30);
            });
          });

          describe('getMaxSpeed', () => {
            it('sends proper request', async () => {
              const promise = liveStream.getMaxSpeed();
              await eqAndPushX('stream 2 info', 'OK IDLE -- live 1 20 30 15 -');
              expect(await promise).to.eq(15);
            });
          });
        });

        describe('axes property', () => {
          it('returns a streams cached axes', async () => {
            expect(live3DStream.axes).to.deep.eq([
              { axisNumber: 1, axisType: StreamAxisType.PHYSICAL },
              { axisNumber: 2, axisType: StreamAxisType.PHYSICAL },
              { axisNumber: 3, axisType: StreamAxisType.PHYSICAL },
            ]);
            expect(liveStream.axes).to.deep.eq([
              { axisNumber: 1, axisType: StreamAxisType.PHYSICAL },
              { axisNumber: 2, axisType: StreamAxisType.PHYSICAL },
            ]);
            expect(storeStream.axes).to.deep.eq([
              { axisNumber: 1, axisType: StreamAxisType.PHYSICAL },
              { axisNumber: 2, axisType: StreamAxisType.PHYSICAL },
            ]);
          });
          it('does not allow in StoreArbitrary mode', async () => {
            try {
              const axes = storeArbitraryStream.axes;
              throw new Error(`No exception thrown: ${JSON.stringify(axes)}`);
            } catch (ex) {
              const streamEx = ex as StreamModeException;
              expect(streamEx.message).to.eq('The stream is not setup in the right mode. \
Permissible mode(s) for action: [Live Store]. Current mode: StoreArbitrary.');
            }
          });
        });

        describe('mode property', () => {
          it('returns stream mode', async () => {
            expect(liveStream.mode).to.eq(StreamMode.LIVE);
            expect(storeStream.mode).to.eq(StreamMode.STORE);
            expect(storeArbitraryStream.mode).to.eq(StreamMode.STORE_ARBITRARY_AXES);
            expect(disabledStream.mode).to.eq(StreamMode.DISABLED);
          });
          it('new stream returns disabled', async () => {
            expect(device.streams.getStream(10).mode).to.eq(StreamMode.DISABLED);
          });
        });
      });

      describe('genericCommand', () => {
        it('sends proper request', async () => {
          const promise = liveStream.genericCommand('some command');
          await eqAndPushX('stream 2 some command', 'OK IDLE -- 0');
          await promise;
        });
      });

      describe('genericCommandBatch', () => {
        it('sends commands in an atomic batch', async () => {
          const REQUEST_COUNT = 5;

          const promise = Promise.all(_.range(REQUEST_COUNT).map(() =>
            liveStream.genericCommandBatch(['command1', 'command2', 'command3'])));

          for (let i = 0; i < REQUEST_COUNT; i++) {
            await eqAndPushX('stream 2 command1', 'OK IDLE -- 0');
            await eqAndPushX('stream 2 command2', 'OK IDLE -- 0');
            await eqAndPushX('stream 2 command3', 'RJ IDLE -- AGAIN');
            await eqAndPushX('stream 2 command3', 'OK IDLE -- 0');
          }
          await promise;
        });
      });

      describe('checkDisabled', () => {
        it('returns true and disables the stream if the stream is disabled', async () => {
          const promise = liveStream.checkDisabled();
          await eqAndPushX('stream 2 info', 'OK IDLE -- disabled - - - - -');
          await expect(promise).eventually.eq(true);
          expect(liveStream.mode).to.eq(StreamMode.DISABLED);
        });

        it('returns false if the stream is not disabled', async () => {
          const promise = liveStream.checkDisabled();
          await eqAndPushX('stream 2 info', 'OK IDLE -- live 2 20 30 15 -');
          await expect(promise).eventually.eq(false);
        });
      });

      describe('treatDiscontinuitiesAsError', () => {
        beforeEach(() => {
          liveStream.treatDiscontinuitiesAsError();
        });

        it('causes ND flag to throw an error', async () => {
          let promise = liveStream.genericCommand('command1');
          await eqAndPushX('stream 2 command1', 'OK BUSY -- 0');
          await promise;

          promise = liveStream.genericCommand('command2');
          await eqAndPushX('stream 2 command2', 'OK BUSY ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY ND 01 ND');
          await expect(promise).to.be.rejectedWith(StreamDiscontinuityException);
        });

        it('ignores first discontinuity after treatDiscontinuitiesAsError call', async () => {
          liveStream.ignoreCurrentDiscontinuity();

          let promise = liveStream.genericCommand('command1');
          await eqAndPushX('stream 2 command1', 'OK BUSY ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY ND 01 ND');
          await promise;

          promise = liveStream.genericCommand('command2');
          await eqAndPushX('stream 2 command2', 'OK BUSY ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 1, 'warnings clear', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings clear', 'OK BUSY ND 01 ND');
          await expect(promise).to.be.rejectedWith(StreamDiscontinuityException);

          liveStream.ignoreCurrentDiscontinuity();

          promise = liveStream.genericCommand('command3');
          await eqAndPushX('stream 2 command3', 'OK BUSY ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK BUSY ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK BUSY ND 01 ND');
          await promise;
        });

        it('discontinuities are ignored while the stream is IDLE', async () => {
          const promise = liveStream.genericCommand('some command');
          // Disco is ignored because the stream is IDLE.
          await eqAndPushX('stream 2 some command', 'OK IDLE ND 0');
          await eqAndPush(server, 1, 1, 'warnings', 'OK IDLE ND 01 ND');
          await eqAndPush(server, 1, 2, 'warnings', 'OK IDLE ND 01 ND');
          await promise;
        });

        it('throws when called on disabled stream', () => {
          expect(() => disabledStream.treatDiscontinuitiesAsError()).to.throw();
        });
      });
    });
  });
});
