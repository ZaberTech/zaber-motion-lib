import _ from 'lodash';
import { expect } from 'chai';

import { Connection, AxisGroup } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { eqAndPush } from '../test_util';
import { DeviceXMCC4 } from './devices/xmcc4';
import { Angle, Length } from '../../src';

const COUNT = 2;

const servers = _.range(COUNT).map(() => new TestServer());

describe('AxisGroup', () => {
  let connections: Connection[];
  let group: AxisGroup;
  let group2: AxisGroup;

  beforeAll(async () => {
    await Promise.all(servers.map((server, i) => server.start(TEST_PORT + i)));
  });

  afterAll(async () => {
    await Promise.all(servers.map(server => server.stop()));
  });

  beforeEach(async () => {
    connections = await Promise.all(_.range(COUNT).map(i => Connection.openTcp(TEST_HOST, TEST_PORT + i)));
    group = new AxisGroup([
      connections[0].getDevice(1).getAxis(2),
      connections[1].getDevice(2).getAxis(3),
    ]);
    group2 = new AxisGroup([
      connections[0].getDevice(1).getAxis(1),
      connections[1].getDevice(2).getAxis(1),
    ]);
    await Promise.all(servers.map(server => server.waitForConnection()));

    await DeviceXMCC4.identify(connections[0].getDevice(1), servers[0]);
    await DeviceXMCC4.identify(connections[1].getDevice(2), servers[1], DeviceXMCC4.Mode.TwoLinearTwoRotary);
  });

  afterEach(async () => {
    await Promise.all(connections.map(connection => connection.close()));
    servers.forEach(server => server.closeSocket());
  });

  ['home', 'stop', 'move max', 'move min'].map(method => {
    describe(method, () => {
      it('sends proper request', async () => {
        let promise: Promise<void>;
        switch (method) {
          case 'home':
            promise = group.home();
            break;
          case 'stop':
            promise = group.stop();
            break;
          case 'move max':
            promise = group.moveMax();
            break;
          case 'move min':
            promise = group.moveMin();
            break;
          default:
            throw new Error(`Unexpected method: ${method}`);
        }

        await Promise.all(servers.map(async (server, i) => {
          await eqAndPush(server, 1 + i, 2 + i, method, 'OK BUSY -- 0');
          await eqAndPush(server, 1 + i, 2 + i, '', 'OK BUSY -- 0');
          await eqAndPush(server, 1 + i, 2 + i, '', 'OK IDLE -- 0');
        }));

        await promise;
      });
    });
  });

  describe('moveAbsolute', () => {
    it('sends proper request', async () => {
      const promise = group.moveAbsolute(
        { value: 1, unit: Length.mm },
        { value: 2, unit: Angle.DEGREES },
      );

      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 2 + i, i ? 'move abs 12800' : 'move abs 8063', 'OK BUSY -- 0');
        await eqAndPush(server, 1 + i, 2 + i, '', 'OK IDLE -- 0');
      }));

      await promise;
    });
  });

  describe('moveRelative', () => {
    it('sends proper request', async () => {
      const promise = group.moveRelative(
        { value: 2, unit: Length.mm },
        { value: 1, unit: Angle.DEGREES },
      );

      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 2 + i, i ? 'move rel 6400' : 'move rel 16126', 'OK BUSY -- 0');
        await eqAndPush(server, 1 + i, 2 + i, '', 'OK IDLE -- 0');
      }));

      await promise;
    });
  });

  describe('waitUntilIdle', () => {
    it('waits until IDLE comes back', async () => {
      const promise = group.waitUntilIdle();

      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 2 + i, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1 + i, 2 + i, '', 'OK BUSY -- 0');
        await eqAndPush(server, 1 + i, 2 + i, '', 'OK IDLE -- 0');
      }));

      await promise;
    });
  });

  describe('isBusy', () => {
    it('returns true if any is busy', async () => {
      for (let combo = 0; combo < 2 ** COUNT; combo++) {
        const promise = group.isBusy();

        await Promise.all(servers.map(async (server, i) => {
          await eqAndPush(server, 1 + i, 2 + i, '',
            ((1 << i) & combo) ? 'OK BUSY -- 0' : 'OK IDLE -- 0');
        }));

        expect(await promise).to.be.eq(combo > 0);
      }
    });
  });

  describe('isHomed', () => {
    it('returns true if all are homed', async () => {
      for (let combo = 0; combo < 2 ** COUNT; combo++) {
        const promise = group.isHomed();

        await Promise.all(servers.map(async (server, i) => {
          await eqAndPush(server, 1 + i, 2 + i, 'warnings',
            ((1 << i) & combo) ? 'OK IDLE WR 1 WR' : 'OK IDLE -- 0');
        }));

        expect(await promise).to.be.eq(combo === 0);
      }
    });
  });

  describe('getPosition', () => {
    it('gets position in native units', async () => {
      const promise = group.getPosition();
      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 2 + i, 'get pos', `OK IDLE -- ${(i + 1) * 1000}`);
      }));
      expect(await promise).to.deep.eq([1000, 2000]);
    });

    it('gets position in specified units', async () => {
      const promise = group.getPosition(Length.mm, Angle.DEGREES);
      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 2 + i, 'get pos', 'OK IDLE -- 1000');
      }));
      const result = await promise;
      expect(result).to.have.lengthOf(2);
      expect(result[0]).to.be.closeTo(0.12402, 10 ** -3);
      expect(result[1]).to.be.closeTo(0.15625, 10 ** -3);
    });

    it('gets position in specified units (only one unit)', async () => {
      const promise = group2.getPosition(Length.mm);
      await Promise.all(servers.map(async (server, i) => {
        await eqAndPush(server, 1 + i, 1, 'get pos', `OK IDLE -- ${(i + 1) * 1000}`);
      }));
      const result = await promise;
      expect(result).to.have.lengthOf(2);
      expect(result[0]).to.be.closeTo(0.12402, 10 ** -3);
      expect(result[1]).to.be.closeTo(0.24804, 10 ** -3);
    });
  });

  describe('toString', () => {
    it('returns representation', () => {
      expect(group.toString()).to.eq([
        'AxisGroup ->',
        'Axis 2 (LHM025A-T3) -> Device 1 SN: 1234 (X-MCC4 (rev 2/3)) ->',
        `Connection ${connections[0].interfaceId} (ASCII TCP: 127.0.0.1:${TEST_PORT}),`,
        'Axis 3 (RST120AG-T3) -> Device 2 SN: 1234 (X-MCC4 (rev 2/3)) ->',
        `Connection ${connections[1].interfaceId} (ASCII TCP: 127.0.0.1:${TEST_PORT + 1})`,
      ].join(' '));
    });
    it('returns representation for closed connection', async () => {
      await Promise.all(connections.map(connection => connection.close()));
      expect(group.toString()).to.eq([
        'AxisGroup ->',
        'Axis 2 -> Device 1 -> Connection Closed,',
        'Axis 3 -> Device 2 -> Connection Closed',
      ].join(' '));
    });
  });
});
