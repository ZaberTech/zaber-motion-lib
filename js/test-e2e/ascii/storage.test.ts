import { expect } from 'chai';
import { BadCommandException, InvalidArgumentException, NotSupportedException, NoValueForKeyException } from '../../src';
import { Axis, Connection, Device } from '../../src/ascii';
import { eqAndPush } from '../test_util';
import { identifyDevice } from './devices/identify';
import { DeviceXMCC4 } from './devices/xmcc4';
import { parseCommand } from './protocol';
import { TestServer, TEST_HOST, TEST_PORT } from './tcp';

const server = new TestServer();

describe('Storage API', () => {
  let connection: Connection;
  let device: Device;
  let axis: Axis;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    axis = device.getAxis(1);
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  it('Rejects old devices with unsupported error', async () => {
    const setValue = device.storage.setString('some-key', 'some value');
    await eqAndPush(server, 1, 0, 'storage set some-key some value', 'RJ IDLE -- BADCOMMAND');
    await expect(setValue).to.eventually.be.rejectedWith(
      NotSupportedException, 'This device does not support key-value storage'
    );
  });

  it('Automatically fills in protocol info of unidentified devices the first time it is called', async () => {
    const setValue = device.storage.setString('some-key', 'some value', { encode: true });
    await eqAndPush(server, 1, 0, 'get comm.command.packets.max', 'OK IDLE -- 5');
    await eqAndPush(server, 1, 0, 'get comm.word.size.max', 'OK IDLE -- 50');
    await eqAndPush(server, 1, 0, 'get get.settings.max', 'RJ IDLE -- BADCOMMAND');
    await eqAndPush(server, 1, 0, 'storage set some-key c29tZSB2YWx1ZQ==', 'OK IDLE -- 0');
    await expect(setValue).to.eventually.be.fulfilled;

    const setAnotherValue = device.storage.setString('another-key', 'another value', { encode: true });
    await eqAndPush(server, 1, 0, 'storage set another-key YW5vdGhlciB2YWx1ZQ==', 'OK IDLE -- 0');
    await expect(setAnotherValue).to.eventually.be.fulfilled;
  });

  describe('Identified Device', () => {
    beforeEach(async () => {
      const idPromise = device.identify();
      await DeviceXMCC4.identifyNumber(1, server, DeviceXMCC4.Mode.OneLinear, DeviceXMCC4.FWVersion.FW7_26);
      await idPromise;
    });

    describe('Set storage', () => {
      it('Set short value on device', async () => {
        const setValue = device.storage.setString('some-key', 'some value');
        const setCmd = parseCommand(await server.pop());
        expect(setCmd.data).to.equal('storage set some-key some value');
        await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.be.fulfilled;
      });

      it('Set empty value returns InvalidArgumentException', async () => {
        const setValue = device.storage.setString('some-key', '');
        await expect(setValue).to.eventually.be.rejectedWith(
          InvalidArgumentException, 'Empty values are not supported.'
        );
      });

      it('Set short value on axis', async () => {
        const setValue = axis.storage.setString('some-key', 'some value');
        const setCmd = parseCommand(await server.pop());
        expect(setCmd.data).to.equal('storage axis set some-key some value');
        await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.fulfilled;
      });

      it('Long values get written to line continuations', async () => {
        const setValue = device.storage.setString('some-key', 'some value that is too long to fit on a single line');
        const line1 = parseCommand(await server.pop());
        expect(line1.data).to.equal('storage set some-key some value that is too long to fit on a single\\');
        const line2 = parseCommand(await server.pop());
        expect(line2.data).to.equal('cont 1 line');
        await server.push(`@${line1.device} ${line1.axis} ${line1.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.fulfilled;
      });

      it('Really long values get written to line continuations', async () => {
        const idPromise = device.identify();
        await identifyDevice(1, server, {
          deviceid: 50081,
          serial: 1234,
          version: { version: '7.26', build: '11669' },
          protocol: { maxPackets: 2, wordLen: 50 },
          axes: [{
            resolution: 64,
          }],
        });
        await idPromise;

        const value = [
          'This is a very very long value.',
          'So long in fact that it will no fit one one line or even two,',
          'thus requiring some gets appended.'
        ].join(' ');
        const setValue = device.storage.setString('some-key', value);

        const setLine1 = parseCommand(await server.pop());
        expect(setLine1.data).to.equal('storage set some-key This is a very very long value. So long in\\');
        const setLine2 = parseCommand(await server.pop());
        expect(setLine2.data).to.equal('cont 1 fact that it will no fit one one line or even two, thus');
        await server.push(`@${setLine1.device} ${setLine1.axis} ${setLine1.id} OK IDLE -- 0`);

        const appendLine1 = parseCommand(await server.pop());
        expect(appendLine1.data).to.equal('storage append some-key requiring some gets appended.');
        await server.push(`@${appendLine1.device} ${appendLine1.axis} ${appendLine1.id} OK IDLE -- 0`);

        await expect(setValue).to.eventually.fulfilled;
      });

      it('Encode values to base64', async () => {
        const key = 'some-key';
        const notAscii = '이 값은 여러 줄로 분할되어야 합니다';
        const base64 = Buffer.from(notAscii).toString('base64');
        const setValue = device.storage.setString(key, notAscii, { encode: true });

        const line1 = parseCommand(await server.pop());
        expect(line1.data).to.equal('storage set some-key\\');

        const contLineRegexp = /^(cont \d) ([\w=]+)\\?$/;

        const line2 = parseCommand(await server.pop());
        const line2Match = line2.data.match(contLineRegexp);
        expect(line2Match![1]).to.eq('cont 1');
        expect(base64).to.startWith(line2Match![2]);

        const line3 = parseCommand(await server.pop());
        const line3Match = line3.data.match(contLineRegexp);
        expect(line3Match![1]).to.eq('cont 2');
        expect(base64).to.endWith(line3Match![2]);

        await server.push(`@${line1.device} ${line1.axis} ${line1.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.fulfilled;
      });
    });

    describe('Get storage', () => {
      it('Throws a "No Value For Key" error if the key has not been set', async () => {
        const getValue = device.storage.getString('some-key');
        await eqAndPush(server, 1, 0, 'storage get some-key', 'RJ IDLE -- BADDATA');
        await expect(getValue).to.eventually.be.rejectedWith(
          NoValueForKeyException, 'No value has been stored with key \'some-key\''
        );
      });

      it('Get short value from device', async () => {
        const getValue = device.storage.getString('some-key');
        const getCmd = parseCommand(await server.pop());
        expect(getCmd.data).to.equal('storage get some-key');
        await server.push(`@${getCmd.device} ${getCmd.axis} ${getCmd.id} OK IDLE -- some value`);
        await expect(getValue).to.eventually.eq('some value');
      });

      it('Get short value from axis', async () => {
        const getValue = axis.storage.getString('some-key');
        const getCmd = parseCommand(await server.pop());
        expect(getCmd.data).to.equal('storage axis get some-key');
        await server.push(`@${getCmd.device} ${getCmd.axis} ${getCmd.id} OK IDLE -- some value`);
        await expect(getValue).to.eventually.eq('some value');
      });

      it('Get long values', async () => {
        const getValue = device.storage.getString('some-key');
        const getCmd = parseCommand(await server.pop());
        expect(getCmd.data).to.equal('storage get some-key');
        await server.push(`@${getCmd.device} ${getCmd.axis} ${getCmd.id} OK IDLE -- some value that is too long\\`);
        await server.push(`#${getCmd.device} ${getCmd.axis} ${getCmd.id} cont to fit on a single line`);
        await expect(getValue).to.eventually.eq('some value that is too long to fit on a single line');
      });

      it('Decode value from base64', async () => {
        const key = 'some-key';
        const notAscii = 'Çûr∫∑∂ √ållÚ';
        const base64 = Buffer.from(notAscii).toString('base64');
        const splitIndex = base64.length / 2;
        const getValue = device.storage.getString(key, { decode: true });
        // The get
        const getCmd = parseCommand(await server.pop());
        expect(getCmd.data).to.equal('storage get some-key');
        await server.push(`@${getCmd.device} ${getCmd.axis} ${getCmd.id} OK IDLE -- ${base64.slice(0, splitIndex)}\\`);
        await server.push(`#${getCmd.device} ${getCmd.axis} ${getCmd.id} cont ${base64.slice(splitIndex)}`);
        await expect(getValue).to.eventually.eq(notAscii);
      });
    });

    describe('Axis storage', () => {
      it('Sets storage on axis', async () => {
        const setValue = axis.storage.setString('some-key', 'some value');
        const setCmd = parseCommand(await server.pop());
        expect(setCmd.data).to.equal('storage axis set some-key some value');
        await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.be.fulfilled;
      });

      it('Gets storage on axis', async () => {
        const getValue = axis.storage.getString('some-key');
        const getCmd = parseCommand(await server.pop());
        expect(getCmd.data).to.equal('storage axis get some-key');
        await server.push(`@${getCmd.device} ${getCmd.axis} ${getCmd.id} OK IDLE -- some value`);
        await expect(getValue).to.eventually.eq('some value');
      });
    });

    describe('Numeric storage', () => {
      it('Set integer number', async () => {
        const setValue = device.storage.setNumber('some-key', 123);
        await eqAndPush(server, 1, 0, 'storage set some-key 123', 'OK IDLE -- 0');
        await expect(setValue).to.eventually.fulfilled;
      });

      it('Set floating point number', async () => {
        const val = 123.45678901234567;
        const setValue = device.storage.setNumber('some-key', val);
        const setCmd = parseCommand(await server.pop());
        expect(setCmd.data).to.eq('storage set some-key 123.4567890123456663786782883107662200927734');
        await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.fulfilled;
      });

      it('Set a big floating point number', async () => {
        const val = 123.456e102;
        const setValue = device.storage.setNumber('some-key', val);
        const setCmd = parseCommand(await server.pop());
        expect(setCmd.data).to.eq('storage set some-key\\');
        const setCmdValue = parseCommand(await server.pop());
        expect(setCmdValue.data).to.eq('cont 1 1.234559999999999927503943703552228964617391e+104');
        await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
        await expect(setValue).to.eventually.fulfilled;
      });

      it('Get an integer back', async () => {
        const int = 123;
        const getValue = device.storage.getNumber('some-key');
        await eqAndPush(server, 1, 0, 'storage get some-key', `OK IDLE -- ${int}`);
        expect(await getValue).to.eq(int);
      });

      it('getNumber does not lose precision at extremes', async () => {
        const testNumbers = [
          Number.MAX_VALUE,
          Number.MIN_VALUE,
          -Number.MAX_VALUE,
          -Number.MIN_VALUE,
        ];
        for (const n of testNumbers) {
          const setValue = device.storage.setNumber('some-key', n);
          const setCmd = parseCommand(await server.pop());
          expect(setCmd.data).to.eq('storage set some-key\\');
          const setCmdValue = parseCommand(await server.pop());
          const words = setCmdValue.data.split(' ');
          expect(words.slice(0, 2).join(' ')).to.eq('cont 1');
          const value = words[2];
          await server.push(`@${setCmd.device} ${setCmd.axis} ${setCmd.id} OK IDLE -- 0`);
          await expect(setValue).to.eventually.fulfilled;

          const getValue = device.storage.getNumber('some-key');
          await eqAndPush(server, 1, 0, 'storage get some-key', `OK IDLE -- ${value}`);
          expect(await getValue).to.eq(n);
        }
      });
    });

    describe('Boolean storage', () => {
      it('Set boolean', async () => {
        const setTrue = device.storage.setBool('some-key', true);
        await eqAndPush(server, 1, 0, 'storage set some-key 1', 'OK IDLE -- 0');
        await expect(setTrue).to.eventually.fulfilled;

        const setFalse = device.storage.setBool('some-key', false);
        await eqAndPush(server, 1, 0, 'storage set some-key 0', 'OK IDLE -- 0');
        await expect(setFalse).to.eventually.fulfilled;
      });

      it('Get a boolean back', async () => {
        const getTrue = device.storage.getBool('some-key');
        await eqAndPush(server, 1, 0, 'storage get some-key', 'OK IDLE -- 1');
        expect(await getTrue).to.be.true;

        const getFalse = device.storage.getBool('some-key');
        await eqAndPush(server, 1, 0, 'storage get some-key', 'OK IDLE -- 0');
        expect(await getFalse).to.be.false;
      });
    });

    describe('erase storage', () => {
      it('erases device storage', async () => {
        const promise = device.storage.eraseKey('some-key');
        await eqAndPush(server, 1, 0, 'storage erase some-key', 'OK IDLE -- 0');
        await expect(promise).to.eventually.fulfilled.to.eq(true);
      });
      it('erases axis storage', async () => {
        const promise = axis.storage.eraseKey('some-key');
        await eqAndPush(server, 1, 1, 'storage axis erase some-key', 'OK IDLE -- 0');
        await expect(promise).to.eventually.fulfilled.to.eq(true);
      });
      it('return false if the key does not exist', async () => {
        const promise = device.storage.eraseKey('some-key');
        await eqAndPush(server, 1, 0, 'storage erase some-key', 'RJ IDLE -- BADDATA');
        await expect(promise).to.eventually.fulfilled.to.eq(false);
      });
      it('propagates other errors', async () => {
        const promise = device.storage.eraseKey('some-key');
        await eqAndPush(server, 1, 0, 'storage erase some-key', 'RJ IDLE -- BADCOMMAND');
        await expect(promise).to.eventually.rejectedWith(BadCommandException);
      });
    });

    describe('listKeys', () => {
      it('lists all keys', async () => {
        const promise = device.storage.listKeys();

        const command = parseCommand(await server.pop());
        await server.pop();
        expect(command.strNoId).to.eq('/1 0 id storage print keys');
        await server.push(`@1 0 ${command.id} OK IDLE -- 0`);
        await server.push(`#1 0 ${command.id} set key1`);
        await server.push(`#1 0 ${command.id} set key2`);
        await server.push(`@1 0 ${command.id} OK IDLE -- 0`);

        expect(await promise).to.deep.eq(['key1', 'key2']);
      });
      it('lists keys with prefix', async () => {
        const promise = axis.storage.listKeys({ prefix: 'foo.bar' });

        const command = parseCommand(await server.pop());
        await server.pop();
        expect(command.strNoId).to.eq('/1 1 id storage axis print keys prefix foo.bar');
        await server.push(`@1 1 ${command.id} OK IDLE -- 0`);
        await server.push(`#1 1 ${command.id} set foo.bar.boo`);
        await server.push(`@1 1 ${command.id} OK IDLE -- 0`);

        expect(await promise).to.deep.eq(['foo.bar.boo']);
      });
    });

    describe('keyExists', () => {
      it('returns true if the key exists', async () => {
        const promise = device.storage.keyExists('foo.bar');
        await eqAndPush(server, 1, 0, 'storage exists foo.bar', 'OK IDLE -- 1');
        expect(await promise).to.eq(true);
      });
      it('returns false if the key does not exist', async () => {
        const promise = axis.storage.keyExists('foo.bar');
        await eqAndPush(server, 1, 1, 'storage axis exists foo.bar', 'OK IDLE -- 0');
        expect(await promise).to.eq(false);
      });
    });
  });
});
