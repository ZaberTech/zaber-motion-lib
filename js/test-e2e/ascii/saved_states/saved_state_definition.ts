export interface ServoTuning {
  startup: number;
  paramsets: Record<string, {
    type: string;
    parameters: Record<string, string>;
  }>;
}

export interface AxisDeviceCommonSerialization {
  settings: Record<string, string>;
  servoTuning?: ServoTuning;
  storedPositions?: Record<string, number>;
  storage?: Record<string, string>;
}

export interface AxisSerialization extends AxisDeviceCommonSerialization {
  peripheralId: string;
  serial: string;
}
export interface SavedDeviceStateDefinition extends AxisDeviceCommonSerialization {
  triggers?: Record<string, {
    when: string;
    a: string;
    b: string;
    enabled: boolean;
    count: number;
  }>;
  streamBuffers?: Record<string, {lines: string[]}>;
  pvtBuffers?: Record<string, {lines: string[]}>;
  axes?: Record<string, AxisSerialization>;
}

export interface SavedAxisStateDefinition {
  axis: AxisSerialization;
}
