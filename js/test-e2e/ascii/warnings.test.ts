import { expect } from 'chai';

import { Connection, Device, Warnings, WarningFlags } from '../../src/ascii';
import { TestServer, TEST_PORT, TEST_HOST } from './tcp';
import { eqAndPush } from '../test_util';
import { DeviceFailedException, TimeoutException } from '../../src';

const server = new TestServer();

describe('Warnings', () => {
  let connection: Connection;
  let device: Device;
  let warnings: Warnings;
  let axisWarnings: Warnings;

  beforeAll(async () => {
    await server.start();
  });

  afterAll(async () => {
    await server.stop();
  });

  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    warnings = device.warnings;
    axisWarnings = device.getAxis(2).warnings;
    await server.waitForConnection();
  });

  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });

  describe('get', () => {
    it('retrieves warning flags', async () => {
      const promise = warnings.getFlags();

      expect(await server.pop()).to.eq('/1 0 00 warnings');
      await server.push('@01 0 00 OK IDLE NC 02 NC NI');

      const flags = await promise;
      expect(Array.from(flags).sort()).to.deep.eq([WarningFlags.MANUAL_CONTROL, WarningFlags.MOVEMENT_INTERRUPTED]);
    });

    it('retrieves warning flags (empty)', async () => {
      const promise = warnings.getFlags();

      expect(await server.pop()).to.eq('/1 0 00 warnings');
      await server.push('@01 0 00 OK IDLE -- 00 ');

      const flags = await promise;
      expect(Array.from(flags).sort()).to.deep.eq([]);
    });
  });

  describe('clear', () => {
    it('clears and retrieves the flags', async () => {
      const promise = warnings.clearFlags();

      expect(await server.pop()).to.eq('/1 0 00 warnings clear');
      await server.push('@01 0 00 OK IDLE FQ 02 FQ NI');

      const flags = await promise;
      expect(Array.from(flags).sort()).to.deep.eq([WarningFlags.ENCODER_ERROR, WarningFlags.MOVEMENT_INTERRUPTED]);
    });
  });

  describe('axis', () => {
    describe('get', () => {
      it('sends correct command to axis and returns flags', async () => {
        const promise = axisWarnings.getFlags();

        expect(await server.pop()).to.eq('/1 2 00 warnings');
        await server.push('@01 2 00 OK IDLE NC 02 NC NI');

        const flags = await promise;
        expect(Array.from(flags).sort()).to.deep.eq([WarningFlags.MANUAL_CONTROL, WarningFlags.MOVEMENT_INTERRUPTED]);
      });
    });
  });

  describe('waitToClear', () => {
    it('waits for the flags to clear', async () => {
      const promise = warnings.waitToClear(10 * 1000, 'FO', 'FZ');

      await eqAndPush(server, 1, 0, 'warnings', 'OK IDLE FD 03 FD FO FZ');
      await eqAndPush(server, 1, 0, 'warnings', 'OK IDLE FD 02 FD FO');
      await eqAndPush(server, 1, 0, 'warnings', 'OK IDLE FD 01 FD');

      await promise;
    });

    it('throws TimeoutException if the flags don\'t clear in timeout', async () => {
      const promise = warnings.waitToClear(100, 'FZ');

      (async () => {
        for (;;) {
          await eqAndPush(server, 1, 0, 'warnings', 'OK IDLE FZ 01 FZ');
        }
      })().catch(() => null);

      await expect(promise).to.be.rejectedWith(TimeoutException);
    });

    it('checks the flags at least once even if timeout is 0', async () => {
      const promise = warnings.waitToClear(0, 'FU');
      await eqAndPush(server, 1, 0, 'warnings', 'OK IDLE FU 01 FU');
      await expect(promise).to.be.rejectedWith(TimeoutException);
    });
  });

  describe('FF handling', () => {
    it('throws exception on command', async () => {
      const promise = warnings.clearFlags();
      expect(await server.pop()).to.eq('/1 0 00 warnings clear');
      await server.push('@01 0 00 OK IDLE FF 1 FF');
      await expect(promise).to.be.rejectedWith(DeviceFailedException, 'Critical System Error (FF flag). Please contact Zaber support.');
    });

    it('does not throw exception on get or system command', async () => {
      const axis = device.getAxis(2);
      const posPromise = axis.settings.get('pos');
      expect(await server.pop()).to.eq('/1 2 00 get pos');
      await server.push('@01 2 00 OK IDLE FF 0');
      await posPromise;

      const systemPromise = device.genericCommandMultiResponse('system errors');
      expect(await server.pop()).to.eq('/1 0 01 system errors');
      await server.push('@01 0 01 OK IDLE FF 0');
      await server.push('#01 0 01 p0 user-io-invalid-config 0 0');

      await server.pop();
      await server.push('@01 0 01 OK IDLE FF 0');
      await systemPromise;
    });
  });
});
