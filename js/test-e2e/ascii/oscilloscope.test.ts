import { expect } from 'chai';

import { Connection, Device, Oscilloscope, OscilloscopeData, OscilloscopeDataSource } from '../../src/ascii';
import {
  BadDataException,
  CommandFailedException,
  Frequency,
  InvalidDataException,
  InvalidPacketException,
  Length,
  Time,
  Voltage,
} from '../../src';
import { eqAndPush } from '../test_util';

import { TestServer, TEST_HOST, TEST_PORT } from './tcp';
import { DeviceXMCC4, FWVersion, Mode } from './devices/xmcc4';
import { Command, parseLines } from './protocol';
import { IoPortType } from '../../src/ascii/io_port_type';

describe('Oscilloscope', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let oscilloscope: Oscilloscope;
  let eqAndPushX: (axis: number, pushStr: string, popStr: string) => Promise<unknown>;


  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1);
  });


  afterAll(async () => {
    await server.stop();
  });


  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    oscilloscope = device.oscilloscope;
    await server.waitForConnection();
    await DeviceXMCC4.identify(device, server, Mode.FourLinear, FWVersion.FW7_33);
    connection.resetIds();
  });


  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });


  describe('start and stop', () => {
    it('sends the start command', async () => {
      const promise = oscilloscope.start();
      await eqAndPushX(0, 'scope start', 'OK IDLE -- 0');
      await promise;
    });

    it('sends the stop command', async () => {
      const promise = oscilloscope.stop();
      await eqAndPushX(0, 'scope stop', 'OK IDLE -- 0');
      await promise;
    });

    it('handles invalid setup', async () => {
      const promise = oscilloscope.start();
      await eqAndPushX(0, 'scope start', 'RJ IDLE -- BADDATA');
      const error: BadDataException = await promise.catch(e => e);
      expect(error.message).to.startWith('The oscilloscope command was rejected; ensure');
    });

    it('handles busy', async () => {
      const promise = oscilloscope.start();
      await eqAndPushX(0, 'scope start', 'RJ IDLE -- STATUSBUSY');
      const error: CommandFailedException = await promise.catch(e => e);
      expect(error.message).to.eq('An oscilloscope capture is already in progress.');
    });
  });


  describe('settings', () => {
    it('gets timebase correctly', async () => {
      const promise = oscilloscope.getTimebase();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 1.2');
      const time = await promise;
      expect(time).to.eq(1.2);
    });

    it('gets timebase correctly with unit conversion', async () => {
      const promise = oscilloscope.getTimebase(Time.SECONDS);
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 1.2');
      const time = await promise;
      expect(time).to.eq(0.0012);
    });

    it('sets timebase correctly', async () => {
      const promise = oscilloscope.setTimebase(3.1);
      await eqAndPushX(0, 'set scope.timebase 3.1', 'OK IDLE -- 0');
      await promise;
    });

    it('sets timebase correctly with unit conversion', async () => {
      const promise = oscilloscope.setTimebase(0.1, Time.SECONDS);
      await eqAndPushX(0, 'set scope.timebase 100.0', 'OK IDLE -- 0');
      await promise;
    });

    it('gets delay correctly', async () => {
      const promise = oscilloscope.getDelay();
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 1.2');
      const time = await promise;
      expect(time).to.eq(1.2);
    });

    it('gets delay correctly with unit conversion', async () => {
      const promise = oscilloscope.getDelay(Time.SECONDS);
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 1.2');
      const time = await promise;
      expect(time).to.eq(0.0012);
    });

    it('sets delay correctly', async () => {
      const promise = oscilloscope.setDelay(3.1);
      await eqAndPushX(0, 'set scope.delay 3.1', 'OK IDLE -- 0');
      await promise;
    });

    it('sets delay correctly with unit conversion', async () => {
      const promise = oscilloscope.setDelay(0.1, Time.SECONDS);
      await eqAndPushX(0, 'set scope.delay 100.0', 'OK IDLE -- 0');
      await promise;
    });

    it('gets expected number of channels', async () => {
      const promise = oscilloscope.getMaxChannels();
      await eqAndPushX(0, 'get scope.numchannels', 'OK IDLE -- 4');
      const quantity = await promise;
      expect(quantity).to.eq(4);
    });

    it('gets expected max buffer size', async () => {
      const promise = oscilloscope.getMaxBufferSize();
      await eqAndPushX(0, 'get scope.channel.size.max', 'OK IDLE -- 6144');
      const quantity = await promise;
      expect(quantity).to.eq(6144);
    });

    it('gets expected buffer size', async () => {
      const promise = oscilloscope.getBufferSize();
      await eqAndPushX(0, 'get scope.channel.size', 'OK IDLE -- 512');
      const quantity = await promise;
      expect(quantity).to.eq(512);
    });

    it('gets frequency', async () => {
      const promise = oscilloscope.getFrequency();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 1.2');
      const freq = await promise;
      expect(freq).to.be.closeTo(0.833, 0.001);
    });

    it('gets frequency with unit conversion', async () => {
      const promise = oscilloscope.getFrequency(Frequency.MHz);
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 1.2');
      const freq = await promise;
      expect(freq).to.be.closeTo(8.333e-4, 1e-7);
    });

    it('sets frequency', async () => {
      const promise = oscilloscope.setFrequency(0.333);
      await eqAndPushX(0, 'set scope.timebase 3.0', 'OK IDLE -- 0');
      await promise;
    });

    it('sets frequency with unit conversion', async () => {
      const promise = oscilloscope.setFrequency(0.3, Frequency.kHz);
      await eqAndPushX(0, 'set scope.timebase 3.3', 'OK IDLE -- 0');
      await promise;
    });
  });


  describe('channels', () => {
    it('accepts legal input', async () => {
      const promise1 = oscilloscope.addChannel(0, 'system.voltage');
      await eqAndPushX(0, 'scope add system.voltage', 'OK IDLE -- 0');
      await promise1;

      const promise2 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise2;

      const promise3 = oscilloscope.addIoChannel(IoPortType.ANALOG_OUTPUT, 1);
      await eqAndPushX(0, 'scope add io ao 1', 'OK IDLE -- 0');
      await promise3;
    });

    it('clears channels', async () => {
      const promise1 = oscilloscope.clear();
      await eqAndPushX(0, 'scope clear', 'OK IDLE -- 0');
      await promise1;

      const promise2 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise2;

      const promise3 = oscilloscope.clear();
      await eqAndPushX(0, 'scope clear', 'OK IDLE -- 0');
      await promise3;
    });
  });


  describe('capture', () => {
    let data: OscilloscopeData[] = [];

    beforeEach(async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;
      const promise2 = oscilloscope.addChannel(0, 'system.temperature');
      await eqAndPushX(0, 'scope add system.temperature', 'OK IDLE -- 0');
      await promise2;
      const promise3 = oscilloscope.addIoChannel(IoPortType.DIGITAL_INPUT, 2);
      await eqAndPushX(0, 'scope add io di 2', 'OK IDLE -- 0');
      await promise3;
      const promise4 = oscilloscope.addIoChannel(IoPortType.ANALOG_INPUT, 1);
      await eqAndPushX(0, 'scope add io ai 1', 'OK IDLE -- 0');
      await promise4;

      const promise5 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 1.2');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0.5');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK BUSY -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 3 chan 4`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 8063`);
      await server.push(`#01 0 ${cmd.idStr} data 16126`);
      await server.push(`#01 0 ${cmd.idStr} data 24189`);
      await server.push(`#01 0 ${cmd.idStr} chan 2 system.temperature`);
      await server.push(`#01 0 ${cmd.idStr} data 31.0`);
      await server.push(`#01 0 ${cmd.idStr} data 32.0`);
      await server.push(`#01 0 ${cmd.idStr} data 33.0`);
      await server.push(`#01 0 ${cmd.idStr} chan 3 io di 2`);
      await server.push(`#01 0 ${cmd.idStr} data 0`);
      await server.push(`#01 0 ${cmd.idStr} data 1`);
      await server.push(`#01 0 ${cmd.idStr} data 0`);
      await server.push(`#01 0 ${cmd.idStr} chan 4 io ai 1`);
      await server.push(`#01 0 ${cmd.idStr} data 12.0`);
      await server.push(`#01 0 ${cmd.idStr} data 12.1`);
      await server.push(`#01 0 ${cmd.idStr} data 12.2`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      data = await promise5;
    });

    it('returns expected properties', async () => {
      expect(data).not.to.equal(undefined);
      expect(data.length).to.equal(4);
      expect(data[0].dataSource).to.equal(OscilloscopeDataSource.SETTING);
      expect(data[0].axisNumber).to.equal(1);
      expect(data[0].setting).to.equal('pos');
      expect(data[1].dataSource).to.equal(OscilloscopeDataSource.SETTING);
      expect(data[1].axisNumber).to.equal(0);
      expect(data[1].setting).to.equal('system.temperature');
      expect(data[2].dataSource).to.equal(OscilloscopeDataSource.IO);
      expect(data[2].ioType).to.equal(IoPortType.DIGITAL_INPUT);
      expect(data[2].ioChannel).to.equal(2);
      expect(data[3].dataSource).to.equal(OscilloscopeDataSource.IO);
      expect(data[3].ioType).to.equal(IoPortType.ANALOG_INPUT);
      expect(data[3].ioChannel).to.equal(1);
    });


    it('returns expected data', async () => {
      const data0 = data[0].getData();
      const data1 = data[1].getData();
      const data2 = data[2].getData();
      const data3 = data[3].getData();

      [0, 1, 2].forEach(i => {
        expect(data0[i]).to.be.approximately(8063 * (i + 1), 1);
        expect(data1[i]).to.be.approximately(31 + i, 0.0001);
        expect(data2[i]).to.equal(i == 1 ? 1 : 0);
        expect(data3[i]).to.be.approximately(12 + i / 10, 0.0001);
      });
    });


    it('converts units', async () => {
      const data0 = data[0].getData(Length.MILLIMETRES);
      // data1 - no unit conversions for temperature.
      // data2 - no unit conversions for digital inputs.
      const data3 = data[3].getData(Voltage.MILLIVOLTS);

      [0, 1, 2].forEach(i => {
        expect(data0[i]).to.be.approximately(i + 1, 0.0001);
        expect(data3[i]).to.be.approximately(12000 + 100 * i, 0.0001);
      });
    });


    it('throws when unit conversion is not supported', async () => {
      expect(() => data[2].getData(Voltage.MILLIVOLTS)).to.throw('Digital I/O channels do not support unit conversion.');
    });


    [0, 1, 2].forEach(i => {
      it('reports recorded timebase', () => {
        expect(data[i].getTimebase(Time.MILLISECONDS)).to.equal(1.2);
      });

      it('reports recorded timebase as frequency', () => {
        expect(data[i].getFrequency(Frequency.MHz)).to.be.closeTo(8.333e-4, 1e-7);
      });

      it('reports recorded delay', () => {
        expect(data[i].getDelay(Time.MILLISECONDS)).to.equal(0.5);
      });

      it('calculates sample time correctly', () => {
        expect(data[i].getSampleTime(0, Time.SECONDS)).to.equal(0.0005);
        expect(data[i].getSampleTime(1, Time.MICROSECONDS)).to.equal(1700);
        expect(data[i].getSampleTime(10, Time.MILLISECONDS)).to.equal(12.5);
        expect(data[i].getSampleTimes(Time.MICROSECONDS)).to.deep.eq([500, 1700, 2900]);
      });
    });
  });

  describe('capture (other)', () => {
    it('waits for oscilloscope to finish', async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;

      const promise4 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 0.2');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0.5');

      let cmd!: Command;
      for (let i = 3; i >= 0; i--) {
        const line = await server.pop();
        cmd = parseLines([line])[0];
        expect(cmd.strNoId).to.equal('/1 0 id scope print');

        const lineTerminate = await server.pop();
        expect(lineTerminate).to.equal(`/1 0 ${cmd.idStr}`);

        if (i > 0) {
          await server.push(`@01 0 ${cmd.idStr} RJ IDLE -- STATUSBUSY`);
          await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
        }
      }

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 3 chan 1`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 1`);
      await server.push(`#01 0 ${cmd.idStr} data 2`);
      await server.push(`#01 0 ${cmd.idStr} data 3`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      await promise4;
    });
  });

  describe('capture reports error', () => {
    it('extra data is received', async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;

      const promise2 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 0.1');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK BUSY -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 3 chan 1`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 1`);
      await server.push(`#01 0 ${cmd.idStr} data 2`);
      await server.push(`#01 0 ${cmd.idStr} data 3`);
      await server.push(`#01 0 ${cmd.idStr} data 4`);

      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      const error: InvalidDataException = await promise2.catch(e => e);
      expect(error.message).to.match(/Too much data received.*/);
    });


    it('data is missing', async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;

      const promise2 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 0.1');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK BUSY -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 4 chan 1`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 1`);
      await server.push(`#01 0 ${cmd.idStr} data 2`);
      await server.push(`#01 0 ${cmd.idStr} data 3`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      const error: InvalidDataException = await promise2.catch(e => e);
      expect(error.message).to.match(/Not enough data received.*/);
    });


    it('unexpected packet received', async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;

      const promise2 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 0.1');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK BUSY -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 2 chan 1`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 1`);
      await server.push(`#01 0 ${cmd.idStr} data Ⅱ`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      const error: InvalidPacketException = await promise2.catch(e => e);
      expect(error.message).to.match(/Unexpected message type.*/);
    });
  });
});


describe('Oscilloscope with older FW', () => {
  let server: TestServer;
  let connection: Connection;
  let device: Device;
  let oscilloscope: Oscilloscope;
  let eqAndPushX: (axis: number, pushStr: string, popStr: string) => Promise<unknown>;


  beforeAll(async () => {
    server = new TestServer();
    await server.start();
    eqAndPushX = eqAndPush.bind(null, server, 1);
  });


  afterAll(async () => {
    await server.stop();
  });


  beforeEach(async () => {
    connection = await Connection.openTcp(TEST_HOST, TEST_PORT);
    device = connection.getDevice(1);
    oscilloscope = device.oscilloscope;
    await server.waitForConnection();
    await DeviceXMCC4.identify(device, server, Mode.FourLinear, FWVersion.FW7_26);
    connection.resetIds();
  });


  afterEach(async () => {
    await connection.close();
    server.closeSocket();
  });


  describe('settings', () => {
    it('gets hardcoded number of channels', async () => {
      const quantity = await oscilloscope.getMaxChannels();
      expect(quantity).to.eq(6);
    });

    it('gets hardcoded max buffer size', async () => {
      const quantity = await oscilloscope.getMaxBufferSize();
      expect(quantity).to.eq(1024);
    });

    it('gets hardcoded buffer size', async () => {
      const quantity = await oscilloscope.getBufferSize();
      expect(quantity).to.eq(1024);
    });
  });

  describe('capture', () => {
    let data: OscilloscopeData[] = [];

    beforeEach(async () => {
      const promise1 = oscilloscope.addChannel(1, 'pos');
      await eqAndPushX(1, 'scope add pos', 'OK IDLE -- 0');
      await promise1;
      const promise2 = oscilloscope.addChannel(0, 'system.temperature');
      await eqAndPushX(0, 'scope add system.temperature', 'OK IDLE -- 0');
      await promise2;

      const promise3 = oscilloscope.read();
      await eqAndPushX(0, 'get scope.timebase', 'OK IDLE -- 0.2');
      await eqAndPushX(0, 'get scope.delay', 'OK IDLE -- 0.5');

      const line = await server.pop();
      const cmd = parseLines([line])[0];
      expect(cmd.strNoId).to.equal('/1 0 id scope print');

      const lineTerminate = await server.pop();
      const cmdTerminate = parseLines([lineTerminate])[0];
      expect(cmdTerminate.str).to.equal(`/1 0 ${cmd.idStr}`);

      await server.push(`@01 0 ${cmd.idStr} OK BUSY -- 0`);
      await server.push(`#01 0 ${cmd.idStr} count 3 chan 2`);
      await server.push(`#01 0 ${cmd.idStr} chan 1 pos axis 1`);
      await server.push(`#01 0 ${cmd.idStr} data 8063`);
      await server.push(`#01 0 ${cmd.idStr} data 16126`);
      await server.push(`#01 0 ${cmd.idStr} data 24189`);
      await server.push(`#01 0 ${cmd.idStr} chan 2 system.temperature`);
      await server.push(`#01 0 ${cmd.idStr} data 31.0`);
      await server.push(`#01 0 ${cmd.idStr} data 32.0`);
      await server.push(`#01 0 ${cmd.idStr} data 33.0`);
      await server.push(`@01 0 ${cmd.idStr} OK IDLE -- 0`);

      data = await promise3;
    });

    [0, 1].forEach(i => it('calculates sample time correctly', () => {
      expect(data[i].getSampleTime(0, Time.SECONDS)).to.equal(0.0007);
      expect(data[i].getSampleTime(1, Time.MICROSECONDS)).to.equal(900);
      expect(data[i].getSampleTime(10, Time.MILLISECONDS)).to.equal(2.7);
      expect(data[0].getSampleTimes(Time.MICROSECONDS)).to.deep.eq([700, 900, 1100]);
    }));
  });
});
