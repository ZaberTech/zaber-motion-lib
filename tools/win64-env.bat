@echo off
rem This batch file sets up environment variables for building the library on 64-bit Windows systems.
rem You must have the TDM-GCC-64 distribution installed - edit the paths below if it's a
rem different version or installed in a different location.

set GO111MODULE=on

rem Add MinGW to path.
set MINGW_BASE=C:\TDM-GCC-64
set MINGW_VARIANT=x86_64-w64-mingw32
set MINGW_VERSION=5.1.0
set PATH=%PATH%;%MINGW_BASE%\bin;%MINGW_BASE%\libexec\gcc\%MINGW_VARIANT%\%MINGW_VERSION%;%MINGW_BASE%\%MINGW_VARIANT%\bin

rem Add Go binaries to path.
set PATH=%PATH%;%GOPATH%\bin

rem Add local build tools to path.
set PATH=%PATH%;%~dp0\tools

