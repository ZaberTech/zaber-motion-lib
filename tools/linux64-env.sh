#!/bin/bash

# Don't run this script directly or the environment variables won't stick.
# Use 'source ./linux64-end.sh' instead.

export GO111MODULE=on

# Add Go binaries to path.
export PATH=$PATH:$GOPATH/bin

# Add local build tools to path.
export PATH=$PATH:`pwd`/tools

