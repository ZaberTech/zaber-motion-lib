//go:build wasm

package main

import (
	"zaber-motion-lib/internal/gateway"
	_ "zaber-motion-lib/internal/init"
)

func main() {
	gatewayManager := gateway.New()
	gatewayManager.Run()
}
