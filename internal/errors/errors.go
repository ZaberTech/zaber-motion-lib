package errors

import (
	"fmt"
	"strings"
	pb "zaber-motion-lib/internal/dto"
)

type SdkError interface {
	Type() pb.Errors
	Message() string
	AddToMessage(message string)
	AddToMessagef(format string, args ...interface{})
	String() string
	CustomData() pb.Message
}

type sdkError struct {
	errorType  pb.Errors
	message    string
	customData pb.Message
}

func (err *sdkError) Type() pb.Errors {
	return err.errorType
}
func (err *sdkError) TypeStr() string {
	return pb.Errors_name[err.errorType]
}
func (err *sdkError) Message() string {
	return err.message
}
func (err *sdkError) AddToMessage(message string) {
	err.message += " " + message
}
func (err *sdkError) AddToMessagef(format string, args ...interface{}) {
	err.AddToMessage(fmt.Sprintf(format, args...))
}
func (err *sdkError) String() string {
	return fmt.Sprintf("%s: %s", err.TypeStr(), err.message)
}
func (err *sdkError) CustomData() pb.Message {
	return err.customData
}

func ErrRequestTimeout() SdkError {
	return &sdkError{
		errorType: pb.Errors_REQUEST_TIMEOUT,
		message:   "Device has not responded in given timeout",
	}
}

func ErrConnectionClosed() SdkError {
	return &sdkError{
		errorType: pb.Errors_CONNECTION_CLOSED,
		message:   "Connection has been closed",
	}
}

func ErrInvalidArgument(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INVALID_ARGUMENT,
		message:   message,
	}
}

func ErrOutOfRequestIDs() SdkError {
	return &sdkError{
		errorType: pb.Errors_OUT_OF_REQUEST_IDS,
		message:   "Maximum number of simultaneous requests was reached",
	}
}

func ErrInvalidPacket(packet string, reason string) SdkError {
	customData := &pb.ExceptionsInvalidPacketExceptionData{Packet: packet, Reason: reason}
	return &sdkError{
		errorType:  pb.Errors_INVALID_PACKET,
		message:    fmt.Sprintf("Cannot parse incoming packet: \"%s\" (%s)", packet, reason),
		customData: customData,
	}
}

func ErrInvalidResponse(messageType string, responseAsText string) SdkError {
	customData := &pb.ExceptionsInvalidResponseExceptionData{Response: responseAsText}
	return &sdkError{
		errorType:  pb.Errors_INVALID_RESPONSE,
		message:    fmt.Sprintf("Received a different message type (%s) when expecting a reply.", messageType),
		customData: customData,
	}
}

func ErrConnectionFailed(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_CONNECTION_FAILED,
		message:   message,
	}
}

func ErrSerialPortBusy(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_SERIAL_PORT_BUSY,
		message:   message,
	}
}

func ErrUnknownRequest(request string) SdkError {
	return &sdkError{
		errorType: pb.Errors_UNKNOWN_REQUEST,
		message:   "UnknownRequest: " + request,
	}
}

func ErrInvalidRequestData(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INVALID_REQUEST_DATA,
		message:   "Invalid request data: " + message,
	}
}

func ErrInvalidData(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INVALID_DATA,
		message:   message,
	}
}

func ErrDeviceDbFailed(message string, code string) SdkError {
	return &sdkError{
		errorType: pb.Errors_DEVICE_DB_FAILED,
		message:   message,
		customData: &pb.ExceptionsDeviceDbFailedExceptionData{
			Code: code,
		},
	}
}

func ErrDeviceNotIdentified(deviceNumber int) SdkError {
	return &sdkError{
		errorType: pb.Errors_DEVICE_NOT_IDENTIFIED,
		message:   fmt.Sprintf("Cannot perform this operation. Device number %d is not identified. Call device.identify() to enable this operation.", deviceNumber),
	}
}

func ErrConversionFailed(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_CONVERSION_FAILED,
		message:   message,
	}
}

func ErrSettingNotFound(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_SETTING_NOT_FOUND,
		message:   message,
	}
}

func ErrDeviceAddressConflict(deviceAddresses []int32) SdkError {
	customData := &pb.ExceptionsDeviceAddressConflictExceptionData{DeviceAddresses: deviceAddresses}
	return &sdkError{
		errorType:  pb.Errors_DEVICE_ADDRESS_CONFLICT,
		message:    "There are multiple devices present with same device number. This prevents addressing each device separately. Please renumber the devices.",
		customData: customData,
	}
}

func ErrNoDeviceFound() SdkError {
	return &sdkError{
		errorType: pb.Errors_NO_DEVICE_FOUND,
		message:   "Cannot find any device on the port. Make sure that you have opened correct port and devices are powered on.",
	}
}

func ErrMovementInterrupted(reason string, warnings []string, device int32, axis int32) SdkError {
	customData := &pb.ExceptionsMovementInterruptedExceptionData{
		Warnings: warnings,
		Reason:   reason,
		Device:   device,
		Axis:     axis,
	}
	return &sdkError{
		errorType:  pb.Errors_MOVEMENT_INTERRUPTED,
		message:    fmt.Sprintf("Movement was interrupted by: %s.", reason),
		customData: customData,
	}
}

func ErrMovementFailed(reason string, warnings []string, device int32, axis int32) SdkError {
	customData := &pb.ExceptionsMovementFailedExceptionData{
		Warnings: warnings,
		Reason:   reason,
		Device:   device,
		Axis:     axis,
	}
	return &sdkError{
		errorType:  pb.Errors_MOVEMENT_FAILED,
		message:    fmt.Sprintf("Movement may have failed because fault was observed: %s.", reason),
		customData: customData,
	}
}

func BindErrMovementFailed(device int32, axis int32) func(reason string, warnings []string) SdkError {
	return func(reason string, warnings []string) SdkError {
		return ErrMovementFailed(reason, warnings, device, axis)
	}
}

func ErrOperationFailed(reason string, warnings []string, device int32, axis int32) SdkError {
	customData := &pb.ExceptionsOperationFailedExceptionData{
		Warnings: warnings,
		Reason:   reason,
		Device:   device,
		Axis:     axis,
	}
	return &sdkError{
		errorType:  pb.Errors_OPERATION_FAILED,
		message:    fmt.Sprintf("Operation may have failed because fault was observed: %s.", reason),
		customData: customData,
	}
}

func ErrIOError(operation string, file string, reason string) SdkError {
	return &sdkError{
		errorType: pb.Errors_IO_FAILED,
		message:   fmt.Sprintf("IO operation %s failed on \"%s\": %s.", operation, file, reason),
	}
}

func ErrNotSupported(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_NOT_SUPPORTED,
		message:   message,
	}
}

func ErrDeviceFailed(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_DEVICE_FAILED,
		message:   message,
	}
}

func ErrOSError(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_OS_FAILED,
		message:   message,
	}
}

func ErrInternal(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INTERNAL_ERROR,
		message:   message,
	}
}

func ErrCommandPreEmpted(command pb.BinaryCommandCode) SdkError {
	return &sdkError{
		errorType: pb.Errors_COMMAND_PREEMPTED,
		message:   fmt.Sprintf("Command %d was pre-empted by a subsequent command or interrupted by the device.", command),
	}
}

func ErrLockstepNotEnabled() SdkError {
	return &sdkError{
		errorType: pb.Errors_LOCKSTEP_NOT_ENABLED,
		message:   "Command has failed because the lockstep group is not enabled.",
	}
}

func ErrLockstepEnabled() SdkError {
	return &sdkError{
		errorType: pb.Errors_LOCKSTEP_ENABLED,
		message:   "Command has failed because the lockstep group is already enabled.",
	}
}

func ErrChannelOutOfRange(channelNumber int32, channelType string) SdkError {
	return &sdkError{
		errorType: pb.Errors_IO_CHANNEL_OUT_OF_RANGE,
		message: fmt.Sprintf(`Channel number %d is out of range for channel type '%s'. Please check the number of io channels for this device.`,
			channelNumber, channelType),
	}
}

func ErrDeviceNoAutoResponse(device int32) SdkError {
	return &sdkError{
		errorType: pb.Errors_NOT_SUPPORTED,
		message: fmt.Sprintf(`The library cannot communicate with device %d because it has Auto-Reply disabled. Please enable Auto-Reply on this device.`,
			device),
	}
}

func ErrStreamExecution(reason string, errorFlag string) SdkError {
	customData := &pb.ExceptionsStreamExecutionExceptionData{
		Reason:    reason,
		ErrorFlag: errorFlag,
	}
	return &sdkError{
		errorType:  pb.Errors_STREAM_EXECUTION,
		message:    "The device has indicated that a previously executed stream action has failed. The reason for the failure is: " + reason,
		customData: customData,
	}
}

func ErrPvtExecution(reason string, errorFlag string, invalidPoints []*pb.ExceptionsInvalidPvtPoint) SdkError {
	customData := &pb.ExceptionsPvtExecutionExceptionData{
		Reason:        reason,
		ErrorFlag:     errorFlag,
		InvalidPoints: invalidPoints,
	}
	return &sdkError{
		errorType:  pb.Errors_PVT_EXECUTION,
		message:    "The device has indicated that a previously executed PVT action has failed. The reason for the failure is: " + reason,
		customData: customData,
	}
}

func ErrStreamMode(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_STREAM_MODE,
		message:   message,
	}
}

func ErrPvtMode(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_PVT_MODE,
		message:   message,
	}
}

func ErrStreamMovementFailed(reason string, warnings []string) SdkError {
	customData := &pb.ExceptionsStreamMovementFailedExceptionData{
		Warnings: warnings,
		Reason:   reason,
	}
	return &sdkError{
		errorType:  pb.Errors_STREAM_MOVEMENT_FAILED,
		message:    fmt.Sprintf("The stream movement may have failed because a fault was observed: %s.", reason),
		customData: customData,
	}
}

func ErrPvtMovementFailed(reason string, warnings []string) SdkError {
	customData := &pb.ExceptionsPvtMovementFailedExceptionData{
		Warnings: warnings,
		Reason:   reason,
	}
	return &sdkError{
		errorType:  pb.Errors_PVT_MOVEMENT_FAILED,
		message:    fmt.Sprintf("The PVT movement may have failed because a fault was observed: %s.", reason),
		customData: customData,
	}
}

func ErrStreamMovementInterrupted(reason string, warnings []string) SdkError {
	customData := &pb.ExceptionsStreamMovementInterruptedExceptionData{
		Warnings: warnings,
		Reason:   reason,
	}
	return &sdkError{
		errorType:  pb.Errors_STREAM_MOVEMENT_INTERRUPTED,
		message:    fmt.Sprintf("The stream movement was interrupted due to a fault: %s.", reason),
		customData: customData,
	}
}

func ErrPvtMovementInterrupted(reason string, warnings []string) SdkError {
	customData := &pb.ExceptionsPvtMovementInterruptedExceptionData{
		Warnings: warnings,
		Reason:   reason,
	}
	return &sdkError{
		errorType:  pb.Errors_PVT_MOVEMENT_INTERRUPTED,
		message:    fmt.Sprintf("The PVT movement was interrupted due to a fault: %s.", reason),
		customData: customData,
	}
}

func ErrPvtDiscontinuity() SdkError {
	return &sdkError{
		errorType: pb.Errors_PVT_DISCONTINUITY,
		message: strings.Join([]string{
			"The sequence has encountered discontinuity and movement was interrupted.",
			"That may have been caused by exceeding communication bandwidth to the device",
			"or by the host program not sending actions fast enough.",
			"If the interruption is expected, call ignore_current_discontinuity before resuming the streaming.",
		}, " "),
	}
}

func ErrStreamDiscontinuity() SdkError {
	return &sdkError{
		errorType: pb.Errors_STREAM_DISCONTINUITY,
		message: strings.Join([]string{
			"The stream has encountered discontinuity and movement was interrupted.",
			"That may have been caused by exceeding communication bandwidth to the device",
			"or by the host program not sending actions fast enough.",
			"If the interruption is expected, call ignore_current_discontinuity before resuming the streaming.",
		}, " "),
	}
}

func ErrStreamSetupFailed(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_STREAM_SETUP_FAILED,
		message:   message,
	}
}

func ErrPvtSetupFailed(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_PVT_SETUP_FAILED,
		message:   message,
	}
}

func ErrDeviceBusy(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_DEVICE_BUSY,
		message:   message,
	}
}

func ErrInvalidParkState() SdkError {
	return &sdkError{
		errorType: pb.Errors_INVALID_PARK_STATE,
		message:   "The requested park state is invalid or the device cannot park/unpark because it is in motion.",
	}
}

func ErrTransportAlreadyUsed() SdkError {
	return &sdkError{
		errorType: pb.Errors_TRANSPORT_ALREADY_USED,
		message:   "The transport has already been used to open another connection. Create new transport.",
	}
}

func ErrCouldNotSetPeripheralState(data *pb.ExceptionsSetPeripheralStateExceptionData) SdkError {
	return &sdkError{
		errorType:  pb.Errors_SET_PERIPHERAL_STATE_FAILED,
		message:    "Could not set the axis to the requested state",
		customData: data,
	}
}

func ErrCouldNotSetDeviceState(data *pb.ExceptionsSetDeviceStateExceptionData) SdkError {
	return &sdkError{
		errorType:  pb.Errors_SET_DEVICE_STATE_FAILED,
		message:    "Could not set the device to the requested state",
		customData: data,
	}
}

func ErrGCodeSyntax(message string, from int, to int) SdkError {
	return &sdkError{
		errorType: pb.Errors_G_CODE_SYNTAX,
		message:   message,
		customData: &pb.ExceptionsGCodeSyntaxExceptionData{
			FromBlock: (int32)(from),
			ToBlock:   (int32)(to),
		},
	}
}

func ErrGCodeExecution(message string, from int, to int) SdkError {
	return &sdkError{
		errorType: pb.Errors_G_CODE_EXECUTION,
		message:   message,
		customData: &pb.ExceptionsGCodeExecutionExceptionData{
			FromBlock: (int32)(from),
			ToBlock:   (int32)(to),
		},
	}
}

func ErrInvalidOperation(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INVALID_OPERATION,
		message:   message,
	}
}

func ErrCommandTooLong(fit string, remainder string, packetSize int32, packetsMax int32) SdkError {
	return &sdkError{
		errorType: pb.Errors_COMMAND_TOO_LONG,
		message:   "Your command was too long to be written to this device",
		customData: &pb.ExceptionsCommandTooLongExceptionData{
			Fit:        fit,
			Remainder:  remainder,
			PacketSize: packetSize,
			PacketsMax: packetsMax,
		},
	}
}

func ErrNoValueForKey(key string) SdkError {
	return &sdkError{
		errorType: pb.Errors_NO_VALUE_FOR_KEY,
		message:   fmt.Sprintf("No value has been stored with key '%s'", key),
	}
}

type DeviceDetectionFailedError struct {
	sdkError
	errType DeviceDetectionFailedErrorType
}
type DeviceDetectionFailedErrorType int

//revive:disable:var-naming

const (
	DeviceDetectionFailed_NothingFound DeviceDetectionFailedErrorType = iota
	DeviceDetectionFailed_MultipleFound
	DeviceDetectionFailed_IncorrectFound
)

//revive:enable:var-naming

func (err *DeviceDetectionFailedError) ErrType() DeviceDetectionFailedErrorType {
	return err.errType
}

func ErrDeviceDetectionFailed(message string, errType DeviceDetectionFailedErrorType) SdkError {
	return &DeviceDetectionFailedError{
		sdkError: sdkError{
			errorType: pb.Errors_DEVICE_DETECTION_FAILED,
			message:   message,
		},
		errType: errType,
	}
}

func ErrTimeout(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_TIMEOUT,
		message:   message,
	}
}

func ErrIncompatibleSharedLibrary(message string) SdkError {
	return &sdkError{
		errorType: pb.Errors_INCOMPATIBLE_SHARED_LIBRARY,
		message:   message,
	}
}

func IsAnyOf(err SdkError, errors ...pb.Errors) bool {
	for _, e := range errors {
		if err.Type() == e {
			return true
		}
	}
	return false
}
