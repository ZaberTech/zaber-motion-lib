package customconnections

import (
	goerr "errors"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	c "zaber-motion-lib/internal/communication"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

type customConnections struct {
	gateway    ioc.GatewayManager
	transports *utils.TokenMap[*transport]
}

func NewCustomConnections(gateway ioc.GatewayManager) *customConnections {
	instance := &customConnections{
		gateway:    gateway,
		transports: utils.NewTokenMap[*transport](),
	}

	instance.register()

	return instance
}

func (instance *customConnections) register() {
	gateway := instance.gateway

	gateway.RegisterCallback("custom/interface/open", func() pb.Message {
		return &pb.EmptyRequest{}
	}, func(_ pb.Message) (pb.Message, errors.SdkError) {
		return instance.open()
	})
	gateway.RegisterCallback("custom/interface/close", func() pb.Message {
		return &pb.CustomInterfaceCloseRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.close(request.(*pb.CustomInterfaceCloseRequest))
	})
	gateway.RegisterCallback("custom/interface/read", func() pb.Message {
		return &pb.CustomInterfaceReadRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.read(request.(*pb.CustomInterfaceReadRequest))
	})
	gateway.RegisterCallback("custom/interface/write", func() pb.Message {
		return &pb.CustomInterfaceWriteRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.write(request.(*pb.CustomInterfaceWriteRequest))
	})
}

func (instance *customConnections) open() (*pb.CustomInterfaceOpenResponse, errors.SdkError) {
	transport := newTransport()
	transport.id = instance.transports.Store(transport)

	response := &pb.CustomInterfaceOpenResponse{
		TransportId: transport.id,
	}
	return response, nil
}

func (instance *customConnections) close(request *pb.CustomInterfaceCloseRequest) errors.SdkError {
	transport, removed := instance.transports.Delete(request.TransportId)
	if !removed {
		return nil // repeated close/error is noop
	}

	var closingError error
	if request.ErrorMessage != "" {
		closingError = goerr.New(request.ErrorMessage)
	}

	transport.closeFromTransport(closingError)

	return nil
}

func (instance *customConnections) GetAndBindTransport(transportID int32) (cBase.IO, errors.SdkError) {
	transport, err := instance.getTransport(transportID)
	if err != nil {
		return nil, err
	}

	if err := transport.bindToConnection(); err != nil {
		return nil, err
	}

	return transport, nil
}

func (instance *customConnections) getTransport(transportID int32) (*transport, errors.SdkError) {
	transport, errLoad := instance.transports.Load(transportID)
	if errLoad != nil {
		return nil, errors.ErrConnectionClosed()
	}
	return transport, nil
}

func (instance *customConnections) read(request *pb.CustomInterfaceReadRequest) (*pb.StringResponse, errors.SdkError) {
	transport, err := instance.getTransport(request.TransportId)
	if err != nil {
		return nil, err
	}

	message, commErr := transport.readFromTransport()
	if commErr != nil {
		return nil, c.ConvertCommErr(commErr)
	}

	return &pb.StringResponse{
		Value: message,
	}, nil
}

func (instance *customConnections) write(request *pb.CustomInterfaceWriteRequest) errors.SdkError {
	transport, err := instance.getTransport(request.TransportId)
	if err != nil {
		return err
	}
	commErr := transport.writeFromTransport(request.Message)
	return c.ConvertCommErr(commErr)
}
