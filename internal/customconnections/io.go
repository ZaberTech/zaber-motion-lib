package customconnections

import (
	"sync"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"

	c "zaber-motion-lib/internal/communication"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

type transport struct {
	id      int32
	lock    sync.Mutex
	isBound bool
	tx      *utils.Pipe
	rx      *utils.Pipe
}

func newTransport() *transport {
	connection := &transport{
		tx: utils.NewPipe(true),
		rx: utils.NewPipe(false),
	}

	return connection
}

func processPipeError(err error) cBase.CommErr {
	if err == utils.ErrPipeClosed {
		return cBase.NewErrConnectionClosed()
	} else {
		return cBase.NewErrConnectionFailed(err.Error())
	}
}

func (connection *transport) ReadLine() (string, cBase.CommErr) {
	if line, err := connection.rx.Read(); err != nil {
		return "", processPipeError(err)
	} else {
		return line.(string), nil
	}
}

func (connection *transport) Read(int) ([]byte, cBase.CommErr) {
	return nil, c.NewErrNotSupported("Not implemented")
}

func (connection *transport) WriteLines(lines []string) cBase.CommErr {
	for _, line := range lines {
		if err := connection.tx.Write(line); err != nil {
			return processPipeError(err)
		}
	}
	return nil
}

func (connection *transport) Write([]byte) cBase.CommErr {
	return c.NewErrNotSupported("Not implemented")
}

func (connection *transport) Close() cBase.CommErr {
	connection.rx.Close(nil)
	connection.tx.Close(nil)
	return nil
}

func (connection *transport) IsClosed() bool {
	return connection.rx.IsClosed()
}

func (connection *transport) closeFromTransport(closingErr error) {
	connection.tx.Close(closingErr)
	connection.rx.Close(closingErr)
}

func (connection *transport) writeFromTransport(message string) cBase.CommErr {
	if message == "" {
		return c.NewErrInvalidArgument("Message is an empty string")
	}

	if err := connection.rx.Write(message); err != nil {
		return processPipeError(err)
	}

	return nil
}

func (connection *transport) readFromTransport() (message string, err cBase.CommErr) {
	if line, err := connection.tx.Read(); err != nil {
		return "", processPipeError(err)
	} else {
		return line.(string), nil
	}
}

func (connection *transport) bindToConnection() errors.SdkError {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	if connection.isBound {
		return errors.ErrTransportAlreadyUsed()
	}
	connection.isBound = true

	return nil
}
