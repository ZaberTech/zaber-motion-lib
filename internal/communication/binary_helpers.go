package communication

import (
	pb "zaber-motion-lib/internal/dto"
)

var spontaneousErrors map[pb.BinaryErrorCode]bool
var errorToCommand map[pb.BinaryErrorCode]map[pb.BinaryCommandCode]bool

// There is no database information about which binary error codes can occur
// spontaneously, so this table is hand-coded and will need to be updated
// if firmware ever adds more spontaneous codes.
func init() {
	spontaneousErrors = map[pb.BinaryErrorCode]bool{
		pb.BinaryErrorCode_VOLTAGE_LOW:      true,
		pb.BinaryErrorCode_VOLTAGE_HIGH:     true,
		pb.BinaryErrorCode_TEMPERATURE_HIGH: true,
	}

	// This provides non-calculable mappings of error codes to command codes.
	// The calculable cases are handled in IsBinaryErrorInResponseToCommand().
	errorToCommand = map[pb.BinaryErrorCode]map[pb.BinaryCommandCode]bool{
		pb.BinaryErrorCode_DIGITAL_OUTPUT_PIN_INVALID: map[pb.BinaryCommandCode]bool{
			pb.BinaryCommandCode_READ_DIGITAL_OUTPUT:  true,
			pb.BinaryCommandCode_WRITE_DIGITAL_OUTPUT: true,
		},
	}
}

func isBinaryErrorCodeSpontaneous(err pb.BinaryErrorCode) bool {
	return spontaneousErrors[err]
}

func isBinaryErrorInResponseToCommand(err pb.BinaryErrorCode, cmd pb.BinaryCommandCode) bool {
	if isBinaryErrorCodeSpontaneous(err) {
		return false
	}

	if int(cmd) == int(err) {
		return true
	}

	// There is an imperfect pattern of error codes being 100 times
	// the corresponding command code, possibly with another small
	// number added.
	if (int(err) >= 256) && (int(err)/100 == int(cmd)) {
		return true
	}

	if cmds, found := errorToCommand[err]; found {
		return cmds[cmd]
	}

	return false
}

func doesCommandCausePreEmption(code pb.BinaryCommandCode) bool {
	// Commands that match this expression cancel all pending commands
	// received before them.
	return (code == pb.BinaryCommandCode_RESET) ||
		(code == pb.BinaryCommandCode_HOME) ||
		(code == pb.BinaryCommandCode(pb.BinaryReplyCode_UNEXPECTED_POSITION)) ||
		((code >= pb.BinaryCommandCode_MOVE_TO_STORED_POSITION) && (code <= pb.BinaryCommandCode_STOP))
}

func canBePreemptedByUnexpectedResponse(code pb.BinaryCommandCode) bool {
	// This is rather incomplete list but it serves to cover the most common cases
	// in already rare scenario of receiving unexpected response.
	return code != pb.BinaryCommandCode_RETURN_SETTING &&
		code != pb.BinaryCommandCode_RETURN_STATUS &&
		code != pb.BinaryCommandCode_RETURN_CURRENT_POSITION
}

func isBinaryReplyOnly(code pb.BinaryReplyCode) bool {
	_, ok := pb.BinaryReplyCode_name[code]
	return ok
}
