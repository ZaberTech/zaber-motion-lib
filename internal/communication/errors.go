package communication

import (
	"fmt"
	"zaber-motion-lib/internal/errors"

	c "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func ConvertCommErr(commErr c.CommErr) errors.SdkError {
	if commErr == nil {
		return nil
	} else if err, ok := commErr.(*c.ErrConnectionFailed); ok {
		return errors.ErrConnectionFailed(err.Error())
	} else if _, ok := commErr.(*c.ErrConnectionClosed); ok {
		return errors.ErrConnectionClosed()
	} else if err, ok := commErr.(*c.ErrSerialPortBusy); ok {
		return errors.ErrSerialPortBusy(err.Error())
	} else if err, ok := commErr.(*ErrInvalidArgument); ok {
		return errors.ErrInvalidArgument(err.Error())
	} else if err, ok := commErr.(*ErrNotSupported); ok {
		return errors.ErrNotSupported(err.Error())
	}
	panic(fmt.Sprintf("Unknown error %T: %s", commErr, commErr))
}

type ErrInvalidArgument struct {
	message string
}

func NewErrInvalidArgument(message string) c.CommErr {
	return &ErrInvalidArgument{
		message: message,
	}
}
func (err *ErrInvalidArgument) Error() string {
	return err.message
}
func (err *ErrInvalidArgument) IsCommErr() bool {
	return true
}

type ErrNotSupported struct {
	message string
}

func NewErrNotSupported(message string) c.CommErr {
	return &ErrNotSupported{
		message: message,
	}
}
func (err *ErrNotSupported) Error() string {
	return err.message
}
func (err *ErrNotSupported) IsCommErr() bool {
	return true
}
