package communication

import (
	"fmt"
	"log"
	"strings"
	"sync"
	"time"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

const maxID = 100 // exclusive
const reservedASCIIProtocolChars = "/:@!#"

var defaultOptions = &RequestOptions{
	Timeout: 1000 * time.Millisecond,
}

type request struct {
	id           int
	canTerminate bool
	responses    []*Response
	responded    chan bool
	terminate    chan bool
}

type RequestOptions struct {
	CollectMultiple bool
	Timeout         time.Duration
}

// Set of callbacks that can be used to handle events.
// Callbacks should be fast as to not block the read loop.
type RequestManagerEvents struct {
	UnknownResponse func(response *Response)
	Alert           func(response *Response)
	InvalidPacket   func(err errors.SdkError)
	Error           func(err errors.SdkError)
}

type RequestManager struct {
	protocol       *ProtocolCommunication
	lock           sync.Mutex
	requests       map[int]*request
	nextID         int
	events         RequestManagerEvents
	defaultOptions *RequestOptions
}

func NewRequestManager(
	protocol *ProtocolCommunication,
	events RequestManagerEvents,
) *RequestManager {
	manager := &RequestManager{
		protocol:       protocol,
		requests:       make(map[int]*request),
		events:         events,
		defaultOptions: &RequestOptions{},
	}
	*manager.defaultOptions = *defaultOptions

	go manager.readLoop()

	return manager
}

func (manager *RequestManager) ResetIDs() {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	manager.nextID = 0
}

func (manager *RequestManager) SetDefaultTimeout(timeout time.Duration) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	if timeout <= 0 {
		timeout = defaultOptions.Timeout
	}
	manager.defaultOptions.Timeout = timeout
}

func (manager *RequestManager) GetDefaultTimeout() time.Duration {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	return manager.defaultOptions.Timeout
}

func isCommunicationErr(err errors.SdkError) bool {
	if err == nil {
		return false
	}
	isFormattingErr := err.Type() == pb.Errors_COMMAND_TOO_LONG || err.Type() == pb.Errors_INVALID_ARGUMENT
	return !isFormattingErr
}

func (manager *RequestManager) RequestNoResponse(command Command) errors.SdkError {
	command.ID = -1

	err := manager.protocol.Send(&command)

	if isCommunicationErr(err) {
		manager.communicationError(err)
	}

	return err
}

func (manager *RequestManager) allocateRequest(canTerminate bool) (*request, errors.SdkError) {
	request := &request{
		canTerminate: canTerminate,
		responses:    make([]*Response, 0, 1),
		responded:    make(chan bool, 1),
		terminate:    make(chan bool, 1),
	}

	manager.lock.Lock()
	defer manager.lock.Unlock()

	found := false
	for i := 0; i < maxID; i++ {
		tryID := manager.nextID
		manager.nextID = (manager.nextID + 1) % maxID

		_, idUsed := manager.requests[tryID]
		if !idUsed {
			manager.requests[tryID] = request
			request.id = tryID
			found = true
			break
		}
	}

	if !found {
		return nil, errors.ErrOutOfRequestIDs()
	}

	return request, nil
}

func (manager *RequestManager) endRequest(request *request) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	close(request.responded)
	delete(manager.requests, request.id)
}

func (manager *RequestManager) waitForReplies(request *request, options *RequestOptions) errors.SdkError {
	timeout := options.Timeout
	if timeout <= 0 {
		timeout = manager.defaultOptions.Timeout
	}

	var responded bool

	for {
		select {
		case <-request.responded:
			responded = true
			if !options.CollectMultiple {
				return nil
			}

		case <-request.terminate:
			return nil

		case <-time.After(timeout):
			if responded {
				return nil
			}

			log.Printf("Request timeout (id: %d).", request.id)
			return errors.ErrRequestTimeout()
		}
	}
}

func (manager *RequestManager) Request(command Command, options *RequestOptions) ([]*Response, errors.SdkError) {
	if command.Device < 0 || command.Axis < 0 {
		return nil, errors.ErrInvalidArgument("Command must be designated to Device and Axis.")
	}

	if strings.ContainsAny(command.Command, reservedASCIIProtocolChars) {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf(
			"Command argument contains ASCII protocol reserved characters %s. "+
				"Please note, that the library constructs the ASCII command for you.",
			reservedASCIIProtocolChars))
	}

	if options == nil {
		options = manager.defaultOptions
	}

	canTerminate := options.CollectMultiple && !command.IsBroadcast()

	request, err := manager.allocateRequest(canTerminate)
	if err != nil {
		return nil, err
	}

	command.ID = request.id
	defer manager.endRequest(request)

	err = manager.protocol.Send(&command)

	if err == nil && canTerminate {
		// send request once more without command
		// as its reply will terminate waiting for more replies (info messages).
		terminateCommand := command
		terminateCommand.Command = ""
		err = manager.protocol.Send(&terminateCommand)
	}

	if err != nil {
		if isCommunicationErr(err) {
			manager.communicationError(err)
		}
		return nil, err
	}

	if err := manager.waitForReplies(request, options); err != nil {
		return nil, err
	}

	if request.responses[0].Type != ResponseTypeReply {
		return nil, errors.ErrInvalidResponse(
			request.responses[0].Type.ToString(),
			fmt.Sprintf("%+v", request.responses[0]),
		)
	}

	return request.responses, nil
}

func (manager *RequestManager) readLoop() {
	defer log.Print("Read loop exited")
	log.Print("Read loop started")

	for {
		response, err := manager.protocol.Receive()
		switch {
		case err == nil: // no error
		case err.Type() == pb.Errors_INVALID_PACKET:
			log.Print("Invalid packet: ", err)
			if callback := manager.events.InvalidPacket; callback != nil {
				callback(err)
			}
			continue
		default:
			manager.communicationError(err)
			return // end the loop
		}

		manager.handleResponse(response)
	}
}

func (manager *RequestManager) handleResponse(response *Response) {
	if response.Type == ResponseTypeAlert {
		if callback := manager.events.Alert; callback != nil {
			callback(response)
		}
	} else {
		if handled := manager.handleReply(response); !handled {
			log.Print("Unknown response: ", response)
			if callback := manager.events.UnknownResponse; callback != nil {
				callback(response)
			}
		}
	}
}

func (manager *RequestManager) handleReply(response *Response) (handled bool) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	request, found := manager.requests[response.ID]
	if !found {
		return false
	}

	if request.canTerminate && response.Type == ResponseTypeReply && len(request.responses) > 0 {
		select {
		case request.terminate <- true:
		default:
		}
	} else {
		request.responses = append(request.responses, response)
		select {
		case request.responded <- true:
		default:
		}
	}
	return true
}

func (manager *RequestManager) communicationError(err errors.SdkError) {
	if callback := manager.events.Error; callback != nil {
		go callback(err)
	}
	if err.Type() != pb.Errors_CONNECTION_CLOSED {
		log.Print("Connection error: ", err)
	}
}
