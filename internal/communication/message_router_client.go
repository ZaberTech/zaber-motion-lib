//go:build !wasm

package communication

import (
	"context"
	"fmt"
	"net"
	"time"
	"zaber-motion-lib/internal/errors"
	messagerouter "zaber-motion-lib/internal/message_router"
	router "zaber-motion-lib/internal/message_router"
	"zaber-motion-lib/internal/utils"

	rpc "github.com/ethereum/go-ethereum/rpc"
	uuid "github.com/nu7hatch/gouuid"
)

type messageRouterRoutedConnectionClient struct {
	routerAPI            *messagerouter.RouterAPI
	rpcClient            *rpc.Client
	isConnectionReliable bool
	closeChan            *utils.CloseChannel
}

func (client messageRouterRoutedConnectionClient) api() *messagerouter.RouterAPI {
	return client.routerAPI
}

func (client messageRouterRoutedConnectionClient) close() {
	client.rpcClient.Close()
}

func (client messageRouterRoutedConnectionClient) control() *utils.CloseChannel {
	return client.closeChan
}

func (client messageRouterRoutedConnectionClient) isReliable() bool {
	return client.isConnectionReliable
}

func dialRPC(connect func() (rpc.Conn, error)) (*rpc.Client, *utils.CloseChannel, error) {
	closeChan := utils.NewCloseChannel()

	rpcClient, err := rpc.DialCodec(context.Background(), func(_ context.Context) (rpc.ServerCodec, error) {
		if closeChan.IsClosed() {
			return nil, fmt.Errorf("Connection closed")
		}

		connection, err := connect()
		if err != nil {
			return nil, err
		}
		return rpc.NewCodec(messagerouter.NewConnectionSpy(connection, closeChan)), nil
	})
	if err != nil {
		return nil, nil, err
	}

	return rpcClient, closeChan, nil
}

func OpenSerialThroughMessageRouter(connectionName string) (*RoutedConnection, errors.SdkError) {
	rpcClient, closeChan, err := dialRPC(func() (rpc.Conn, error) {
		return dialLocalMessageRouter()
	})
	if err != nil {
		return nil, errors.ErrConnectionFailed("Cannot open: " + err.Error())
	}

	routerAPI := router.NewRouterAPI(rpcClient, 10*time.Second)
	client := &messageRouterRoutedConnectionClient{
		routerAPI:            routerAPI,
		rpcClient:            rpcClient,
		isConnectionReliable: true,
		closeChan:            closeChan,
	}

	return openRoutedConnection(client, connectionName, false)
}

func OpenNetworkShare(hostname string, port int, connectionName string) (*RoutedConnection, errors.SdkError) {
	dial := fmt.Sprintf("%s:%d", hostname, port)

	rpcClient, closeChan, err := dialRPC(func() (rpc.Conn, error) {
		return net.Dial("tcp", dial)
	})
	if err != nil {
		return nil, errors.ErrConnectionFailed("Cannot establish TCP connection: " + err.Error())
	}

	routerAPI := router.NewRouterAPI(rpcClient, 10*time.Second)
	routedClient := &messageRouterRoutedConnectionClient{
		routerAPI:            routerAPI,
		rpcClient:            rpcClient,
		isConnectionReliable: true,
		closeChan:            closeChan,
	}

	return openRoutedConnection(routedClient, connectionName, true)
}

func OpenIoTConnection(cloudID, connectionName, realm, token, api string) (*RoutedConnection, errors.SdkError) {
	connectionID, err := uuid.NewV4()
	if err != nil {
		return nil, errors.ErrConnectionFailed("Could not generate UUID for connection")
	}

	config := &router.CloudConfig{
		Token:  token,
		Realm:  realm,
		ID:     connectionID.String(),
		Router: cloudID,
		API:    api,
	}
	rpcClient, closeChan, err := dialRPC(func() (rpc.Conn, error) {
		return router.NewMqttIoClient(config)
	})

	if err != nil {
		return nil, errors.ErrConnectionFailed(fmt.Sprintf("Connecting to IoT failed: %s - Check Internet connection", err))
	}

	routerAPI := router.NewRouterAPI(rpcClient, 20*time.Second)
	routedClient := &messageRouterRoutedConnectionClient{
		routerAPI:            routerAPI,
		rpcClient:            rpcClient,
		isConnectionReliable: false,
		closeChan:            closeChan,
	}

	if err := routedClient.api().CheckConnection(); err != nil {
		return nil, errors.ErrConnectionFailed("Establishing connection failed - check Cloud ID")
	}

	return openRoutedConnection(routedClient, connectionName, true)
}
