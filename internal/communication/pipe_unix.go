//go:build darwin || linux

package communication

import (
	"context"
	"net"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func dialLocalMessageRouter() (net.Conn, error) {
	pipeName, err := communication.GetMessageRouterPipe()
	if err != nil {
		return nil, err
	}
	return new(net.Dialer).DialContext(context.Background(), "unix", pipeName)
}
