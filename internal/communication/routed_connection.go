//go:build !wasm

package communication

import (
	"fmt"
	"log"
	"sync"
	"time"
	"zaber-motion-lib/internal/errors"
	messagerouter "zaber-motion-lib/internal/message_router"
	"zaber-motion-lib/internal/utils"

	c "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
	baseUtils "gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
)

const checkUnreliableConnectionTime = 60 * time.Second

type routedConnectionListener struct {
	connectionName  string
	messageOrdering *baseUtils.MessageOrdering[[]string]
	control         *utils.CloseChannel
}

func (listener *routedConnectionListener) Reply(fromConnection string, messages []string, id uint32) {
	if listener.connectionName == fromConnection {
		if err := listener.messageOrdering.Write(messages, id); err != nil {
			listener.control.CloseWithErr(fmt.Errorf("Write failed: %s", err))
		}
	}
}

func (listener *routedConnectionListener) Error(fromConnection, errorMessage string) {
	if listener.connectionName == fromConnection {
		listener.control.CloseWithErr(fmt.Errorf("%s", errorMessage))
	}
}

type routedConnectionClient interface {
	api() *messagerouter.RouterAPI
	close()
	isReliable() bool
	control() *utils.CloseChannel
}
type RoutedConnection struct {
	client   routedConnectionClient
	isClosed bool
	listener *routedConnectionListener
	closing  chan interface{}
	lock     sync.Mutex

	readerLock     sync.Mutex
	lastBatch      []string
	lastBatchIndex int

	writerLock    sync.Mutex
	nextMessageID uint32
}

func openRoutedConnection(client routedConnectionClient, connectionName string, resolveConnectionName bool) (*RoutedConnection, errors.SdkError) {
	apiVersions, err := client.api().ExchangeVersions()
	if err != nil {
		client.close()
		return nil, errors.ErrConnectionFailed(err.Error())
	}

	if resolveConnectionName {
		if resolvedName, err := client.api().ResolveConnectionName(connectionName); err != nil {
			client.close()
			return nil, errors.ErrConnectionFailed(err.Error())
		} else {
			connectionName = resolvedName
		}
	}

	control := client.control()
	listener := &routedConnectionListener{
		connectionName: connectionName,
		control:        control,
		messageOrdering: baseUtils.NewMessageOrdering[[]string](10*time.Second, func() {
			control.CloseWithErr(fmt.Errorf("Missing packet caused connection interruption"))
		}),
	}
	routedConnection := &RoutedConnection{
		client:   client,
		isClosed: false,
		listener: listener,
		closing:  make(chan interface{}),
	}

	listener.messageOrdering.Reset()
	if baseUtils.VersionGte(apiVersions.Client, baseUtils.Version{Major: 2}) {
		client.api().RegisterReceiver("routing", listener)
	} else {
		client.api().RegisterReceiver("routing", &routedConnectionListenerV1Wrapper{listener})
	}

	log.Printf("Opening %s (message router)", connectionName)

	if err := client.api().Warmup(connectionName); err != nil {
		control.CloseWithErr(err)
		_ = routedConnection.Close()
		return nil, errors.ErrConnectionFailed(err.Error())
	}

	if !client.isReliable() {
		go routedConnection.checkConnectionLoop(control)
	}

	return routedConnection, nil
}

func (conn *RoutedConnection) ReadLine() (line string, err c.CommErr) {
	if conn.IsClosed() {
		return "", c.NewErrConnectionClosed()
	}

	conn.readerLock.Lock()
	defer conn.readerLock.Unlock()

	for conn.lastBatchIndex >= len(conn.lastBatch) {
		select {
		case batch := <-conn.listener.messageOrdering.OrderedData():
			conn.lastBatch = batch
			conn.lastBatchIndex = 0
		case err := <-conn.listener.control.Channel():
			if err != nil {
				return "", c.NewErrConnectionFailed(err.Error())
			}
			return "", c.NewErrConnectionClosed()
		}
	}

	line = conn.lastBatch[conn.lastBatchIndex]
	conn.lastBatchIndex++
	return line, nil
}

func (conn *RoutedConnection) Read(_ int) ([]byte, c.CommErr) {
	return nil, NewErrNotSupported("Read is not supported over routed connection. Use ReadLine instead.")
}

func (conn *RoutedConnection) WriteLines(lines []string) c.CommErr {
	if conn.IsClosed() {
		return c.NewErrConnectionClosed()
	}

	conn.writerLock.Lock()
	defer conn.writerLock.Unlock()

	for _, line := range lines {
		if err := conn.writeLine(line); err != nil {
			return err
		}
	}

	return nil
}

func (conn *RoutedConnection) writeLine(line string) c.CommErr {
	if line == "" {
		return NewErrInvalidArgument("Empty input is invalid")
	}

	id := conn.nextMessageID
	conn.nextMessageID++

	if err := conn.client.api().WriteLine(conn.listener.connectionName, line, id); err != nil {
		conn.listener.control.CloseWithErr(err)
		return c.NewErrConnectionFailed(err.Error())
	}
	return nil
}

func (conn *RoutedConnection) Write(_ []byte) c.CommErr {
	return NewErrNotSupported("Write is not supported over routed connection. Use WriteLine instead.")
}

func (conn *RoutedConnection) Close() c.CommErr {
	conn.lock.Lock()
	defer conn.lock.Unlock()

	if conn.isClosed {
		return nil
	}
	conn.isClosed = true

	close(conn.closing)

	var cooldownErr error
	isClosedForcefully := conn.listener.control.IsClosed()
	if !isClosedForcefully {
		conn.listener.control.Close()

		cooldownErr = conn.client.api().Cooldown(conn.listener.connectionName)
	}

	conn.client.close()
	conn.listener.messageOrdering.Stop()

	if cooldownErr != nil {
		return c.NewErrConnectionFailed(cooldownErr.Error())
	}
	return nil
}

func (conn *RoutedConnection) IsClosed() bool {
	conn.lock.Lock()
	defer conn.lock.Unlock()

	return conn.isClosed
}

func (conn *RoutedConnection) checkConnectionLoop(control *utils.CloseChannel) {
	var lastMessageID uint32
	for {
		select {
		case <-conn.closing:
			return
		case <-time.After(checkUnreliableConnectionTime):
			conn.writerLock.Lock()
			noTraffic := conn.nextMessageID == lastMessageID
			lastMessageID = conn.nextMessageID
			conn.writerLock.Unlock()

			if noTraffic {
				if err := conn.client.api().CheckConnection(); err != nil {
					control.CloseWithErr(err)
				}
			}
		}
	}
}

type routedConnectionListenerV1Wrapper struct {
	listener *routedConnectionListener
}

func (wrapper *routedConnectionListenerV1Wrapper) Reply(fromConnection string, message string, id uint32) {
	wrapper.listener.Reply(fromConnection, []string{message}, id)
}

func (wrapper *routedConnectionListenerV1Wrapper) Error(fromConnection, errorMessage string) {
	wrapper.listener.Error(fromConnection, errorMessage)
}
