package communication

import (
	"fmt"
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"zaber-motion-lib/internal/errors"

	c "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

var replyRegexp, _ = regexp.Compile(`^@(\d+)\s(\d+)\s((\d+)\s)?(\S+)\s(\S+)\s(\S+)\s([^\:\\]*)(\\)?(\:(\S+))?$`)
var alertRegexp, _ = regexp.Compile(`^!(\d+)\s(\d+)\s(\S+)\s(\S{2})(\s([^\:]*))?(\:(\S+))?$`)
var alertXJoyRegexp, _ = regexp.Compile(`^!(\d+)\s(\d+)\s((key)(\s([^\:]*)))?(\:(\S+))?$`)
var infoRegexp, _ = regexp.Compile(`^#(\d+)\s(\d+)(\s(\d+))?(\s([^\:\\]*))?(\\)?(\:(\S+))?$`)

const (
	reasonNumberFormat = "invalid number format"
	reasonPacketFormat = "unknown packet format"
	reasonLRC          = "invalid checksum"
)

type ResponseType int

const (
	ResponseTypeReply ResponseType = 0
	ResponseTypeAlert ResponseType = 1
	ResponseTypeInfo  ResponseType = 2
)

type Response struct {
	Type            ResponseType
	Device          int
	Axis            int
	ID              int // -1 means no ID
	ReplyFlag       string
	Status          string
	WarningFlag     string
	Data            string
	IsToBeContinued bool
	IsContinuation  bool
}
type Command struct {
	Device  int // -1 means no Device
	Axis    int // -1 means no Axis
	ID      int // -1 means no ID
	Command string
}

const DefaultPacketSize = 80

type DeviceProtocolInfo struct {
	// The maximum number of lines that can be joined by continuations for the device
	MaxPackets int
	// The maximum length of a single space separated word in a command to the device
	MaxWordLen int
	// The maximum number of settings that can be fetched in a single synchronized command
	MaxSyncSettings int
}

type protocolInfo struct {
	// The maximum number of characters allowed on a single line
	packetSize int
	devices    map[int]*DeviceProtocolInfo
}

func defaultProtocolInfo() protocolInfo {
	return protocolInfo{
		packetSize: DefaultPacketSize,
		devices:    make(map[int]*DeviceProtocolInfo),
	}
}

type ProtocolCommunication struct {
	port                c.IO
	options             ProtocolOptions
	incompleteResponses map[int]*Response
	lock                sync.Mutex
	info                protocolInfo
}

type ProtocolOptions struct {
	TxChecksum bool
}

func NewProtocol(port c.IO, options ProtocolOptions) *ProtocolCommunication {
	protocol := &ProtocolCommunication{port: port, options: options, incompleteResponses: make(map[int]*Response), info: defaultProtocolInfo()}
	return protocol
}

func (communication *ProtocolCommunication) Send(command *Command) errors.SdkError {
	limits, packetSize := communication.GetDeviceLimits(command.Device)
	var words []string
	if len(command.Command) > 0 {
		words = strings.Split(command.Command, " ")
	}
	wordCount := len(words)
	if limits != nil {
		for _, word := range words {
			if len(word) > limits.MaxWordLen {
				return errors.ErrInvalidArgument(
					fmt.Sprintf("Could not write word %s which is longer than the maximum allowed value %d to the ASCII protocol", word, limits.MaxWordLen),
				)
			}
		}
	}

	withChecksum := communication.GetTxChecksumEnabled()

	headBuilder := strings.Builder{}
	headBuilder.WriteString("/")

	if command.Device >= 0 {
		headBuilder.WriteString(strconv.Itoa(command.Device))

		if command.Axis >= 0 {
			headBuilder.WriteString(" ")
			headBuilder.WriteString(strconv.Itoa(command.Axis))

			if command.ID >= 0 {
				headBuilder.WriteString(" ")
				// pad with zeros in case the ID gets remapped later to a longer ID
				headBuilder.WriteString(fmt.Sprintf("%02d", command.ID))
			}
		}
	}
	head := headBuilder.String()

	freePacketSpace := packetSize - 2 // -1 for newline rune, -1 for line continuation rune
	if withChecksum {
		freePacketSpace -= 3 // For the checksum
	}

	var lines []string
	if wordCount == 0 {
		lines = []string{head}
	} else {
		wordIndex := 0
		for lineIndex := 0; limits == nil || lineIndex <= limits.MaxPackets; lineIndex++ {
			if limits != nil && lineIndex == limits.MaxPackets {
				return errors.ErrCommandTooLong(
					strings.Join(words[:wordIndex], " "),
					strings.Join(words[wordIndex:], " "),
					int32(packetSize),
					int32(limits.MaxPackets),
				)
			}
			separator := " "
			if head == "/" {
				separator = ""
			}
			lineBuilder := strings.Builder{}
			lineBuilder.WriteString(head)
			if lineIndex != 0 {
				lineBuilder.WriteString(separator)
				lineBuilder.WriteString("cont ")
				lineBuilder.WriteString(strconv.Itoa(lineIndex))
				separator = " "
			}
			firstWordIndex := wordIndex
			for wordIndex < wordCount {
				if wordIndex == wordCount-1 {
					freePacketSpace++ // If this is the last word, don't reserve space for a line continuation
				}
				word := words[wordIndex]
				if lineBuilder.Len()+len(separator)+len(word) > freePacketSpace {
					break
				}
				lineBuilder.WriteString(separator)
				lineBuilder.WriteString(word)
				wordIndex++
				separator = " "
			}
			if wordIndex == firstWordIndex {
				return errors.ErrInvalidArgument(
					fmt.Sprintf("Could not write word %s which would exceed packet size limit (%d) of ASCII protocol", words[wordIndex], packetSize),
				)
			}
			if wordIndex == wordCount {
				lines = append(lines, lineBuilder.String())
				break
			}

			lineBuilder.WriteString(`\`)
			lines = append(lines, lineBuilder.String())
		}
	}

	if withChecksum {
		for i, line := range lines {
			lines[i] += fmt.Sprintf(":%02X", c.ComputeLRC(line))
		}
	}

	for _, line := range lines {
		log.Print("TX Line: ", line)
	}

	commErr := communication.port.WriteLines(lines)
	return ConvertCommErr(commErr)
}

func (communication *ProtocolCommunication) Receive() (*Response, errors.SdkError) {
	for {
		parsedMessage, err := communication.readLine()
		if err != nil || parsedMessage != nil {
			return parsedMessage, err
		}
	}
}

func (communication *ProtocolCommunication) readLine() (*Response, errors.SdkError) {
	line, err := communication.port.ReadLine()
	if err != nil {
		return nil, ConvertCommErr(err)
	}

	log.Print("RX Line: ", line)

	if match := replyRegexp.FindStringSubmatch(line); match != nil {
		reply, err := parseResponse(line, match)
		if err != nil {
			return nil, err
		}

		combined, err := communication.handleContinuations(reply)
		if err != nil {
			return nil, err
		}

		return combined, nil
	}

	if match := infoRegexp.FindStringSubmatch(line); match != nil {
		infoMsg, err := parseInfoMessage(line, match)
		if err != nil {
			return nil, err
		}

		combined, err := communication.handleContinuations(infoMsg)
		if err != nil {
			return nil, err
		}

		return combined, nil
	}

	if match := alertRegexp.FindStringSubmatch(line); match != nil {
		return parseAlert(line, match)
	}

	if match := alertXJoyRegexp.FindStringSubmatch(line); match != nil {
		return parseAlertXJoy(line, match)
	}

	return nil, errors.ErrInvalidPacket(line, reasonPacketFormat)
}

func (communication *ProtocolCommunication) handleContinuations(reply *Response) (*Response, errors.SdkError) {
	if reply.IsContinuation {
		previous, prevExists := communication.incompleteResponses[reply.Device]
		if !prevExists {
			return nil, errors.ErrInvalidPacket(fmt.Sprintf("%+v", reply), "Received a continuation without a previous packet.")
		}

		previous.Data = strings.TrimSpace(fmt.Sprint(previous.Data, " ", reply.Data))
		previous.IsToBeContinued = reply.IsToBeContinued
		if !previous.IsToBeContinued {
			delete(communication.incompleteResponses, reply.Device)
			return previous, nil
		}

		return nil, nil
	}

	if reply.IsToBeContinued {
		previous, prevExists := communication.incompleteResponses[reply.Device]
		communication.incompleteResponses[reply.Device] = reply
		if prevExists {
			return nil, errors.ErrInvalidPacket(fmt.Sprintf("%+v", previous), "Incomplete message was replaced by a new incomplete message")
		}

		return nil, nil
	}

	return reply, nil
}

func parseResponse(line string, match []string) (*Response, errors.SdkError) {
	device, errParse := strconv.Atoi(match[1])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}
	axis, errParse := strconv.Atoi(match[2])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}
	id := -1
	if len(match[4]) > 0 {
		if idNum, errParse := strconv.Atoi(match[4]); errParse != nil {
			return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
		} else {
			id = idNum
		}
	}
	if len(match[11]) > 0 {
		lrc := match[11]
		if !c.VerifyLRC(line, lrc) {
			return nil, errors.ErrInvalidPacket(line, reasonLRC)
		}
	}

	response := Response{
		Type:            ResponseTypeReply,
		Device:          device,
		Axis:            axis,
		ID:              id,
		ReplyFlag:       match[5],
		Status:          match[6],
		WarningFlag:     match[7],
		Data:            match[8],
		IsToBeContinued: len(match[9]) > 0,
	}

	return &response, nil
}

func parseInfoMessage(line string, match []string) (*Response, errors.SdkError) {
	device, errParse := strconv.Atoi(match[1])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}

	axis, errParse := strconv.Atoi(match[2])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}

	id := -1
	if len(match[4]) > 0 {
		if idNum, errParse := strconv.Atoi(match[4]); errParse != nil {
			return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
		} else {
			id = idNum
		}
	}

	if len(match[9]) > 0 {
		lrc := match[9]
		if !c.VerifyLRC(line, lrc) {
			return nil, errors.ErrInvalidPacket(line, reasonLRC)
		}
	}

	data := match[6]
	isContinuation := strings.HasPrefix(data, "cont ")
	if isContinuation {
		data = data[len("cont "):]
	}

	response := Response{
		Type:            ResponseTypeInfo,
		Device:          device,
		Axis:            axis,
		ID:              id,
		Data:            data,
		IsToBeContinued: len(match[7]) > 0,
		IsContinuation:  isContinuation,
	}

	return &response, nil
}

func parseAlert(line string, match []string) (*Response, errors.SdkError) {
	device, errParse := strconv.Atoi(match[1])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}
	axis, errParse := strconv.Atoi(match[2])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}

	if len(match[8]) > 0 {
		lrc := match[8]
		if !c.VerifyLRC(line, lrc) {
			return nil, errors.ErrInvalidPacket(line, reasonLRC)
		}
	}

	response := Response{
		Type:        ResponseTypeAlert,
		Device:      device,
		Axis:        axis,
		ID:          -1,
		Status:      match[3],
		WarningFlag: match[4],
		Data:        match[6],
	}

	return &response, nil
}

func parseAlertXJoy(line string, match []string) (*Response, errors.SdkError) {
	device, errParse := strconv.Atoi(match[1])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}
	axis, errParse := strconv.Atoi(match[2])
	if errParse != nil {
		return nil, errors.ErrInvalidPacket(line, reasonNumberFormat)
	}

	if len(match[8]) > 0 {
		lrc := match[8]
		if !c.VerifyLRC(line, lrc) {
			return nil, errors.ErrInvalidPacket(line, reasonLRC)
		}
	}

	response := Response{
		Type:   ResponseTypeAlert,
		Device: device,
		Axis:   axis,
		ID:     -1,
		Data:   match[3],
	}

	return &response, nil
}

func (response *Response) DataAsInt() (int, errors.SdkError) {
	number, err := strconv.Atoi(response.Data)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf(`Cannot parse response data "%s" as integer`, response.Data))
	}
	return number, nil
}

func (response *Response) DataAsInt64() (int64, errors.SdkError) {
	number, err := strconv.ParseInt(response.Data, 10, 64)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf(`Cannot parse response data "%s" as integer`, response.Data))
	}
	return number, nil
}

func (response *Response) DataAsIntArray(allowNA bool) ([]int64, errors.SdkError) {
	parts := strings.Split(response.Data, " ")
	values := make([]int64, len(parts))

	for i, part := range parts {
		if part == "NA" && allowNA {
			continue
		}
		number, err := strconv.ParseInt(part, 10, 64)
		if err != nil {
			return nil, errors.ErrInvalidData(fmt.Sprintf(`Cannot parse response data "%s" as int array`, response.Data))
		}
		values[i] = number
	}

	return values, nil
}

func (response *Response) DataAsFloat() (float64, errors.SdkError) {
	number, err := strconv.ParseFloat(response.Data, 64)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf(`Cannot parse response data "%s" as float`, response.Data))
	}
	return number, nil
}

func DataStringAsFloatArray(data string, allowNA bool) ([]float64, errors.SdkError) {
	parts := strings.Split(data, " ")
	values := make([]float64, len(parts))

	for i, part := range parts {
		if part == "NA" && allowNA {
			values[i] = math.NaN()
			continue
		}
		number, err := strconv.ParseFloat(part, 64)
		if err != nil {
			return nil, errors.ErrInvalidData(fmt.Sprintf(`Cannot parse response data "%s" as float array. %s`, data, err.Error()))
		}
		values[i] = number
	}

	return values, nil
}

func (response *Response) DataAsFloatArray(allowNA bool) ([]float64, errors.SdkError) {
	return DataStringAsFloatArray(response.Data, allowNA)
}

func (response *Response) DataAsFloatArrayArray(allowNA bool) ([][]float64, errors.SdkError) {
	parts := strings.Split(response.Data, ";")
	values := make([][]float64, len(parts))

	for i, part := range parts {
		innerValues, err := DataStringAsFloatArray(strings.Trim(part, " "), allowNA)
		if err != nil {
			return nil, err
		}
		values[i] = innerValues
	}

	return values, nil
}

func (response *Response) DataAsStrArray() []string {
	return strings.Split(response.Data, " ")
}

func (responseType ResponseType) ToString() string {
	switch responseType {
	case ResponseTypeReply:
		return "Reply"
	case ResponseTypeAlert:
		return "Alert"
	case ResponseTypeInfo:
		return "Info"
	}

	return "Undefined"
}

func (cmd Command) IsBroadcast() bool {
	return cmd.Device == 0
}

func (communication *ProtocolCommunication) SetTxChecksumEnabled(isEnabled bool) {
	communication.lock.Lock()
	defer communication.lock.Unlock()

	communication.options.TxChecksum = isEnabled
}

func (communication *ProtocolCommunication) GetTxChecksumEnabled() bool {
	communication.lock.Lock()
	defer communication.lock.Unlock()

	return communication.options.TxChecksum
}

func (communication *ProtocolCommunication) SetPacketSize(packetSize int) {
	communication.lock.Lock()
	defer communication.lock.Unlock()

	communication.info.packetSize = packetSize
}

func (communication *ProtocolCommunication) SetDeviceProtocolInfo(deviceNumber int, info *DeviceProtocolInfo) {
	communication.lock.Lock()
	defer communication.lock.Unlock()

	communication.info.devices[deviceNumber] = info
}

// Returns the device's protocol info, and the packet length for the chain it's on
func (communication *ProtocolCommunication) GetDeviceLimits(deviceNumber int) (*DeviceProtocolInfo, int) {
	communication.lock.Lock()
	defer communication.lock.Unlock()

	deviceInfo, ok := communication.info.devices[deviceNumber]
	if !ok {
		return nil, communication.info.packetSize
	} else {
		return deviceInfo, communication.info.packetSize
	}
}
