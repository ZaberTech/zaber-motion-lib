//go:build windows

package communication

import (
	"net"
	"time"

	io "github.com/Microsoft/go-winio"
	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func dialLocalMessageRouter() (net.Conn, error) {
	pipeName, err := communication.GetMessageRouterPipe()
	if err != nil {
		return nil, err
	}
	timeout := time.Second
	return io.DialPipe(pipeName, &timeout)
}
