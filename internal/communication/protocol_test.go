package communication

import (
	"testing"

	"gotest.tools/assert"
)

var responseCases = []struct {
	line     string
	response *Response
}{
	{
		line: "@01 2 45 OK IDLE -- 6.28:64",
		response: &Response{
			Type:        ResponseTypeReply,
			Device:      1,
			Axis:        2,
			ID:          45,
			ReplyFlag:   "OK",
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "6.28",
		},
	},
	{
		line: "@01 2 45 OK IDLE -- 6.28",
		response: &Response{
			Type:        ResponseTypeReply,
			Device:      1,
			Axis:        2,
			ID:          45,
			ReplyFlag:   "OK",
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "6.28",
		},
	},
	{
		line: "@01 2 OK IDLE -- 6.28:ED",
		response: &Response{
			Type:        ResponseTypeReply,
			Device:      1,
			Axis:        2,
			ID:          -1,
			ReplyFlag:   "OK",
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "6.28",
		},
	},
	{
		line: "@01 2 OK IDLE -- 6.28",
		response: &Response{
			Type:        ResponseTypeReply,
			Device:      1,
			Axis:        2,
			ID:          -1,
			ReplyFlag:   "OK",
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "6.28",
		},
	},
	{
		line: "@01 2 OK IDLE -- 3 4\\:D8",
		response: &Response{
			Type:            ResponseTypeReply,
			Device:          1,
			Axis:            2,
			ID:              -1,
			ReplyFlag:       "OK",
			Status:          "IDLE",
			WarningFlag:     "--",
			Data:            "3 4",
			IsContinuation:  false,
			IsToBeContinued: true,
		},
	},
	{
		line: "@01 2 OK IDLE -- \\",
		response: &Response{
			Type:            ResponseTypeReply,
			Device:          1,
			Axis:            2,
			ID:              -1,
			ReplyFlag:       "OK",
			Status:          "IDLE",
			WarningFlag:     "--",
			Data:            "",
			IsContinuation:  false,
			IsToBeContinued: true,
		},
	},
}

func TestProtocol_parseResponse(t *testing.T) {
	for _, testCase := range responseCases {
		line := testCase.line
		response, err := parseResponse(line, replyRegexp.FindStringSubmatch(line))
		if err != nil {
			t.Error(err)
		}
		assert.DeepEqual(t, response, testCase.response)
	}
}

var infoCases = []struct {
	line     string
	response *Response
}{
	{
		line: "#02 3 45 some info:22",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     45,
			Data:   "some info",
		},
	},
	{
		line: "#02 3 45 some info",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     45,
			Data:   "some info",
		},
	},
	{
		line: "#02 3 some info:AB",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "some info",
		},
	},
	{
		line: "#02 3 some info",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "some info",
		},
	},
	{
		line: "#02 3 45some info",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "45some info",
		},
	},
	{
		line: "#02 3 45some info:42",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "45some info",
		},
	},
	{
		line: "#02 3 45",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     45,
			Data:   "",
		},
	},
	{
		line: "#02 3 45:C2",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     45,
			Data:   "",
		},
	},
	{
		line: "#02 3",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "",
		},
	},
	{
		line: "#02 3:4B",
		response: &Response{
			Type:   ResponseTypeInfo,
			Device: 2,
			Axis:   3,
			ID:     -1,
			Data:   "",
		},
	},
	{
		line: "#02 3 some info\\",
		response: &Response{
			Type:            ResponseTypeInfo,
			Device:          2,
			Axis:            3,
			ID:              -1,
			Data:            "some info",
			IsContinuation:  false,
			IsToBeContinued: true,
		},
	},
	{
		line: "#02 3 cont some info",
		response: &Response{
			Type:            ResponseTypeInfo,
			Device:          2,
			Axis:            3,
			ID:              -1,
			Data:            "some info",
			IsContinuation:  true,
			IsToBeContinued: false,
		},
	},
	{
		line: "#02 3 45 cont some info\\",
		response: &Response{
			Type:            ResponseTypeInfo,
			Device:          2,
			Axis:            3,
			ID:              45,
			Data:            "some info",
			IsContinuation:  true,
			IsToBeContinued: true,
		},
	},
	{
		line: "#02 3 45 cont some info\\:F2",
		response: &Response{
			Type:            ResponseTypeInfo,
			Device:          2,
			Axis:            3,
			ID:              45,
			Data:            "some info",
			IsContinuation:  true,
			IsToBeContinued: true,
		},
	},
}

func TestProtocol_parseInfoMessage(t *testing.T) {
	for _, testCase := range infoCases {
		line := testCase.line
		info, err := parseInfoMessage(line, infoRegexp.FindStringSubmatch(line))
		if err != nil {
			t.Error(err)
		}
		assert.DeepEqual(t, info, testCase.response)
	}
}

var alertCases = []struct {
	line     string
	response *Response
}{
	{
		line: "!01 2 IDLE --",
		response: &Response{
			Type:        ResponseTypeAlert,
			Device:      1,
			Axis:        2,
			ID:          -1,
			Status:      "IDLE",
			WarningFlag: "--",
		},
	},
	{
		line: "!01 2 IDLE --:95",
		response: &Response{
			Type:        ResponseTypeAlert,
			Device:      1,
			Axis:        2,
			ID:          -1,
			Status:      "IDLE",
			WarningFlag: "--",
		},
	},
	{
		line: "!01 2 IDLE -- data",
		response: &Response{
			Type:        ResponseTypeAlert,
			Device:      1,
			Axis:        2,
			ID:          -1,
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "data",
		},
	},
	{
		line: "!01 2 IDLE -- data:DB",
		response: &Response{
			Type:        ResponseTypeAlert,
			Device:      1,
			Axis:        2,
			ID:          -1,
			Status:      "IDLE",
			WarningFlag: "--",
			Data:        "data",
		},
	},
}

func TestProtocol_parseAlert(t *testing.T) {
	for _, testCase := range alertCases {
		line := testCase.line
		info, err := parseAlert(line, alertRegexp.FindStringSubmatch(line))
		if err != nil {
			t.Error(err)
		}
		assert.DeepEqual(t, info, testCase.response)
	}
}

var alertXJoyCases = []struct {
	line     string
	response *Response
}{
	{
		line: "!01 0 key 1 1",
		response: &Response{
			Type:   ResponseTypeAlert,
			Device: 1,
			ID:     -1,
			Data:   "key 1 1",
		},
	},
	{
		line: "!01 0 key 1 1:44",
		response: &Response{
			Type:   ResponseTypeAlert,
			Device: 1,
			ID:     -1,
			Data:   "key 1 1",
		},
	},
}

func TestProtocol_parseAlertXJoy(t *testing.T) {
	for _, testCase := range alertXJoyCases {
		line := testCase.line
		info, err := parseAlertXJoy(line, alertXJoyRegexp.FindStringSubmatch(line))
		if err != nil {
			t.Error(err)
		}
		assert.DeepEqual(t, info, testCase.response)
	}
}
