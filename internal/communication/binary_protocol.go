package communication

import (
	binary "encoding/binary"
	"fmt"
	"log"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	c "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

const datawithIDMask int32 = 0x00FFFFFF

const binaryPacketSize int = 6

type BinaryMessage struct {
	Device  int
	Command pb.BinaryCommandCode
	ID      int
	Data    int32
}

type BinaryProtocolOptions struct {
	UseMessageIDs bool
}

func NewBinaryMessage(device int, command pb.BinaryCommandCode, data int32) *BinaryMessage {
	msg := &BinaryMessage{
		Device:  device,
		Command: command,
		ID:      0,
		Data:    data,
	}
	return msg
}

type BinaryProtocolCommunication struct {
	port    c.IO
	options BinaryProtocolOptions
}

func NewBinaryProtocol(port c.IO, options BinaryProtocolOptions) *BinaryProtocolCommunication {
	protocol := &BinaryProtocolCommunication{port: port, options: options}
	return protocol
}

func (message *BinaryMessage) Encode(withID bool) ([]byte, errors.SdkError) {
	var packet [binaryPacketSize]byte
	packet[0] = (byte)(message.Device)
	packet[1] = (byte)(message.Command)

	// Add message ID and make sure no data is lost.
	var data = message.Data
	if withID {
		if (message.ID < 0) || (message.ID > 255) {
			return nil, errors.ErrInvalidArgument("Message ID is out of range.")
		}

		if data < (-datawithIDMask>>1) || (datawithIDMask>>1) < data {
			return nil, errors.ErrInvalidArgument("Data value is too large for the Binary Protocol with IDs enabled.")
		}

		data = (int32(message.ID) << 24) | (data & datawithIDMask)
	}

	binary.LittleEndian.PutUint32(packet[2:], uint32(data))

	return packet[:], nil
}

func DecodeBinaryMessage(packet []byte, withID bool) (*BinaryMessage, errors.SdkError) {
	if len(packet) < binaryPacketSize {
		return nil, errors.ErrInvalidPacket(fmt.Sprint(packet), "Binary packet is too small.")
	}

	message := &BinaryMessage{}
	message.Device = (int)(packet[0])
	message.Command = pb.BinaryCommandCode(packet[1])

	var data = int32(binary.LittleEndian.Uint32(packet[2:]))

	if withID {
		message.ID = (int)(packet[5])
		data = (data << 8) >> 8
	}

	message.Data = data

	return message, nil
}

func (communication *BinaryProtocolCommunication) Send(command *BinaryMessage) errors.SdkError {
	packet, err := command.Encode(communication.options.UseMessageIDs)
	if err != nil {
		return err
	}

	log.Print("TX Line: ", packet)

	commErr := communication.port.Write(packet)
	return ConvertCommErr(commErr)
}

func (communication *BinaryProtocolCommunication) Receive() (*BinaryMessage, errors.SdkError) {
	packet, err := communication.port.Read(binaryPacketSize)
	if err != nil {
		return nil, ConvertCommErr(err)
	}

	log.Print("RX Line: ", packet)

	var arr [6]byte
	copy(arr[:], packet[:6])
	return DecodeBinaryMessage(packet, communication.options.UseMessageIDs)
}
