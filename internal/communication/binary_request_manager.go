package communication

import (
	"log"
	"sync"
	"time"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

// We avoid using ID 0 as it is being used by the device in unsolicited communication.
const resetBinaryID = 1
const maxBinaryID = 255
const noReplyReservedBinaryID = 0

var defaultBinaryOptions = &RequestOptions{
	Timeout: 500 * time.Millisecond,
}

type binaryRequest struct {
	command   *BinaryMessage
	responses []*BinaryMessage
	responded chan bool
	preempted chan bool
	id        int
}

// Set of callbacks that can be used to handle events.
// Callbacks should be fast as to not block the read loop.
type BinaryRequestManagerEvents struct {
	UnknownResponse func(response *BinaryMessage)
	Alert           func(response *BinaryMessage)
	InvalidPacket   func(err errors.SdkError)
	Error           func(err errors.SdkError)
}

type BinaryRequestManager struct {
	protocol     *BinaryProtocolCommunication
	lock         sync.Mutex
	requests     []*binaryRequest
	requestsByID map[int]*binaryRequest
	nextID       int
	events       BinaryRequestManagerEvents
}

func NewBinaryRequestManager(protocol *BinaryProtocolCommunication, events BinaryRequestManagerEvents) *BinaryRequestManager {
	manager := &BinaryRequestManager{
		protocol:     protocol,
		requests:     make([]*binaryRequest, 0, 1),
		requestsByID: make(map[int]*binaryRequest),
		events:       events,
		nextID:       resetBinaryID,
	}

	go manager.readLoop()

	return manager
}

func (manager *BinaryRequestManager) ResetIDs() {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	manager.nextID = resetBinaryID
}

func (manager *BinaryRequestManager) allocateMessageID() (int, errors.SdkError) {
	for i := 0; i < maxBinaryID; i++ {
		tryID := manager.nextID

		manager.nextID = manager.nextID + 1
		if manager.nextID > maxBinaryID {
			manager.nextID = resetBinaryID
		}

		if _, isUsed := manager.requestsByID[tryID]; !isUsed {
			return tryID, nil
		}
	}

	return -1, errors.ErrOutOfRequestIDs()
}

func (manager *BinaryRequestManager) allocateRequest() (*binaryRequest, errors.SdkError) {
	request := &binaryRequest{
		responses: make([]*BinaryMessage, 0, 1),
		responded: make(chan bool, 1),
		preempted: make(chan bool, 1),
	}

	manager.lock.Lock()
	defer manager.lock.Unlock()

	if manager.protocol.options.UseMessageIDs {
		id, err := manager.allocateMessageID()
		if err != nil {
			return nil, err
		}

		request.id = id
		manager.requestsByID[id] = request
	}

	// Even with message IDs, requests are always put in the chronological
	// list so that pre-emptions will cancel the right ones.
	manager.requests = append(manager.requests, request)

	return request, nil
}

func (manager *BinaryRequestManager) endRequest(request *binaryRequest) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	close(request.responded)
	close(request.preempted)

	if manager.protocol.options.UseMessageIDs {
		delete(manager.requestsByID, request.id)
	}

	for i, r := range manager.requests {
		if r == request {
			manager.requests = append(manager.requests[:i], manager.requests[i+1:]...)
			break
		}
	}
}

func (manager *BinaryRequestManager) waitForReplies(request *binaryRequest, options *RequestOptions) errors.SdkError {
	timeout := options.Timeout
	if timeout <= 0 {
		timeout = defaultBinaryOptions.Timeout
	}

	var responded bool

	for {
		select {
		case <-request.preempted:
			// Arbitrary decision: Pre-empted broadcast commands will
			// behave the same as addressed commands - that is, they will
			// raise the pre-empted exception right away, meaning any
			// remaining responses to the broadcast will become unexpected.
			log.Printf("Command %d pre-empted.", request.command.Command)
			return errors.ErrCommandPreEmpted(request.command.Command)

		case <-request.responded:
			responded = true
			if !options.CollectMultiple {
				return nil
			}

		case <-time.After(timeout):
			if responded {
				return nil
			}

			log.Printf("Request timeout (command: %d).", request.command.Command)
			return errors.ErrRequestTimeout()
		}
	}
}

func (manager *BinaryRequestManager) RequestNoResponse(command BinaryMessage) errors.SdkError {
	if (command.Device < 0) || (command.Device > 255) {
		return errors.ErrInvalidArgument("Device number is out of range. Legal device addresses are from 0 to 255.")
	}

	if manager.protocol.options.UseMessageIDs {
		command.ID = noReplyReservedBinaryID
	}

	return manager.protocol.Send(&command)
}

func (manager *BinaryRequestManager) Request(command BinaryMessage, options *RequestOptions) ([]*BinaryMessage, errors.SdkError) {
	if (command.Device < 0) || (command.Device > 255) {
		return nil, errors.ErrInvalidArgument("Device number is out of range. Legal device addresses are from 0 to 255.")
	}

	if options == nil {
		options = defaultBinaryOptions
	}

	request, err := manager.allocateRequest()
	if err != nil {
		return nil, err
	}

	request.command = &command
	request.command.ID = request.id
	defer manager.endRequest(request)

	err = manager.protocol.Send(request.command)
	if err != nil {
		manager.communicationError(err)
		return nil, err
	}

	if err := manager.waitForReplies(request, options); err != nil {
		return nil, err
	}

	return request.responses, nil
}

func (manager *BinaryRequestManager) readLoop() {
	defer log.Print("Read loop exited")
	log.Print("Read loop started")

	for {
		response, err := manager.protocol.Receive()
		switch {
		case err == nil: // no error
		case err.Type() == pb.Errors_INVALID_PACKET:
			if callback := manager.events.InvalidPacket; callback != nil {
				callback(err)
			}
			log.Print("Invalid packet: ", err)
			continue
		default:
			manager.communicationError(err)
			return // end the loop
		}

		manager.handleResponse(response)
	}
}

func (manager *BinaryRequestManager) handleResponse(response *BinaryMessage) {
	if pb.BinaryReplyCode(response.Command) == pb.BinaryReplyCode_ERROR {
		if handled := manager.handleErrorResponse(response); !handled {
			log.Print("Unmatched error: ", *response)
			if callback := manager.events.UnknownResponse; callback != nil {
				callback(response)
			}
		}
	} else if isBinaryReplyOnly(pb.BinaryReplyCode(response.Command)) {
		if callback := manager.events.Alert; callback != nil {
			callback(response)
		}
	} else {
		if handled := manager.handleNormalResponse(response); !handled {
			log.Print("Unmatched response: ", *response)
			if callback := manager.events.UnknownResponse; callback != nil {
				callback(response)
			}
		}
	}
}

func (manager *BinaryRequestManager) handleErrorResponse(response *BinaryMessage) (handled bool) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	request := manager.findRequestForError(response)
	if request == nil {
		return false
	}

	log.Print("Error ", *response, " matches command: ", *request.command)
	request.responses = append(request.responses, response)
	select {
	case request.responded <- true:
	default:
	}
	return true
}

func (manager *BinaryRequestManager) findRequestForError(response *BinaryMessage) *binaryRequest {
	errorCode := pb.BinaryErrorCode(response.Data)
	if manager.protocol.options.UseMessageIDs {
		return manager.requestsByID[response.ID]
	} else if (errorCode == pb.BinaryErrorCode_BUSY) ||
		(errorCode == pb.BinaryErrorCode_STORAGE_FULL) ||
		(errorCode == pb.BinaryErrorCode_COMMAND_INVALID) {
		// Busy, storage full or command invalid errors should always be returned immediately, so
		// we assume they result from the most recent command sent to the device.
		for i := len(manager.requests) - 1; i >= 0; i-- {
			req := manager.requests[i]
			if (req.command.Device == 0) || (req.command.Device == response.Device) {
				return req
			}
		}
	} else {
		for i := len(manager.requests) - 1; i >= 0; i-- {
			req := manager.requests[i]
			if (req.command.Device == 0) || (req.command.Device == response.Device) {
				if isBinaryErrorInResponseToCommand(errorCode, req.command.Command) {
					return req
				}
			}
		}
	}
	return nil
}

func (manager *BinaryRequestManager) handleNormalResponse(response *BinaryMessage) (handled bool) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	request := manager.findRequest(response)
	isExpectedResponse := request != nil

	// Some commands pre-empt all outstanding commands before them, meaning we
	// no longer expect a reply to the earlier commands.
	// We only perform pre-emption when we get a success response, because firmware
	// does not cancel the previous command if the new one causes an error.
	if doesCommandCausePreEmption(response.Command) {
		for _, r := range manager.requests {
			if r == request {
				// stop on the matched request if there is such
				break
			}

			matchesDevice := (r.command.Device == 0) || (r.command.Device == response.Device)
			// Unexpected response preempts all requests but we only receive such when e.g.
			// - user broadcasts something without waiting for response
			// - when device hits home sensor unexpectedly
			canBePreempted := isExpectedResponse || canBePreemptedByUnexpectedResponse(r.command.Command)
			if matchesDevice && canBePreempted {
				log.Print("Pre-empting command ", *r.command)
				select {
				case r.preempted <- true:
				default:
				}
			}
		}
	}

	if !isExpectedResponse {
		return false
	}

	// Add the response to the request that caused it.
	log.Print("Response ", *response, " matches command: ", *request.command)
	request.responses = append(request.responses, response)
	select {
	case request.responded <- true:
	default:
	}
	return true
}

func (manager *BinaryRequestManager) findRequest(response *BinaryMessage) *binaryRequest {
	if manager.protocol.options.UseMessageIDs {
		return manager.requestsByID[response.ID]
	} else {
		// Non-error response with message IDs turned off.
		// Look for the most recent command that the response command number matches.
		for i := len(manager.requests) - 1; i >= 0; i-- {
			req := manager.requests[i]
			if (req.command.Device == 0) || (req.command.Device == response.Device) {
				if response.Command == req.command.Command {
					return req
				} else if (req.command.Command == pb.BinaryCommandCode_RETURN_SETTING) && (req.command.Data == int32(response.Command)) {
					return req
				}
			}
		}
	}
	return nil
}

func (manager *BinaryRequestManager) communicationError(err errors.SdkError) {
	if callback := manager.events.Error; callback != nil {
		go callback(err)
	}

	if err.Type() != pb.Errors_CONNECTION_CLOSED {
		log.Print("Connection error: ", err)
	}
}
