package commands

import (
	"fmt"
	"regexp"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/errors"
)

var allowedFFCommands = regexp.MustCompile(`^(get|system)\s`)
var checkFF = true
var replies map[string]string

func init() {
	replies = map[string]string{
		"AGAIN":          "The device cannot process the command right now. Send the command again.",
		"AXISONLY":       "The command must be addressed to a particular axis, but the axis number was 0.",
		"BADAXIS":        "The command's axis number was greater than the number of axes available.",
		"BADCOMMAND":     "The command or setting was incorrect or invalid; a firmware update may be required.",
		"BADDATA":        "The command's data was incorrect or out of range.",
		"BADMESSAGEID":   "The command included a message ID, but the ID was not either -- or a number from 0 to 99.",
		"BADSPLIT":       "The command was split into multiple packets in an incorrect way.",
		"BRAKECLOSED":    "One of the specified axes has a closed brake so cannot move. Open the brake using the brake.mode setting.",
		"DEVICEONLY":     "An axis number was provided to a device-scope command.",
		"DRIVERDISABLED": "One of the drivers is disabled because the driver is still in an over-temperature, over-voltage, or over-current condition or because the user disabled it with the command.",
		"FULL":           "The device cannot allocate more memory for information related to the command. See the documentation for the command that prompted this response for details of this rejection.",
		"INACTIVE":       "The command failed to execute because the axis is inactive.",
		"LOCKSTEP":       "The command has failed because the device currently has an enabled lockstep group.",
		"LONGWORD":       "A word in a message was longer than comm.word.size.max characters.",
		"NOACCESS":       "The command or setting is not available at the current access level.",
		"NOTSIN":         "The axis is not currently moving in response to a move sin command, so it cannot be stopped with the move sin stop command.",
		"NOTSUPPORTED":   "The axis cannot be activated because the connected peripheral is not supported by this version of firmware.",
		"OVERDRIVELIMIT": "The axis cannot execute motion requests because it exceeded its overdrive energy limit and has entered the recovery state.",
		"PARKED":         "The device cannot move because it is currently parked.",
		"PROCESSON":      "The setting value cannot be changed because the process output is currently turned on.",
		"REMOTE":         "The command was rejected because the device is in EtherCAT Control (remote) mode.",
		"STATUSBUSY":     "The device cannot execute the command because it is currently busy.",
		"SYSTEMERROR":    "The device failed to execute the command due to a system failure. Use the command to review errors.",
		"TWISTED":        "The device failed to execute a stream command on a lockstep group because the axes in the group are twisted beyond the allowable threshold and stream commands cannot correct for the twist.",
	}
}

func CheckOk(reply *c.Response, command *c.Command) errors.SdkError {
	if reply.ReplyFlag != "OK" {
		replyMessage := replies[reply.Data]
		if replyMessage == "" {
			replyMessage = "Unknown reply. Please see the documentation."
		}

		return NewCommandFailedErr(
			fmt.Sprintf("Command \"%s\" rejected: %s: %s", command.Command, reply.Data, replyMessage),
			reply, command)
	}

	// There is no potential for harming FF devices with get or system commands
	if checkFF && reply.WarningFlag == "FF" && !allowedFFCommands.MatchString(command.Command) {
		return errors.ErrDeviceFailed("Critical System Error (FF flag). Please contact Zaber support.")
	}

	return nil
}

func CheckDeviceAndAxis(cmd *c.Command, reply *c.Response) errors.SdkError {
	if reply.Device != cmd.Device || reply.Axis != cmd.Axis {
		return errors.ErrInvalidData(
			fmt.Sprintf("Response device or axis does not match: %d != %d || %d != %d", reply.Device, cmd.Device, reply.Axis, cmd.Axis),
		)
	}
	return nil
}

func SetCheckFF(check bool) {
	checkFF = check
}
