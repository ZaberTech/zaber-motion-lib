package commands

import (
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/errors"

	"github.com/elliotchance/pie/v2"
)

func SingleCommand(requests *c.RequestManager, cmd c.Command) (*c.Response, errors.SdkError) {
	replies, err := requests.Request(cmd, nil)
	if err != nil {
		return nil, err
	}
	reply := replies[0]
	if err := CheckDeviceAndAxis(&cmd, reply); err != nil {
		return reply, err
	}
	if err := CheckOk(reply, &cmd); err != nil {
		return reply, err
	}
	return reply, nil
}

func SingleCommandMultiResponse(requests *c.RequestManager, cmd c.Command) ([]*c.Response, errors.SdkError) {
	options := &c.RequestOptions{
		CollectMultiple: true,
	}
	replies, err := requests.Request(cmd, options)
	if err != nil {
		return nil, err
	}
	reply := replies[0]
	if err := CheckDeviceAndAxis(&cmd, reply); err != nil {
		return replies, err
	}
	if err := CheckOk(reply, &cmd); err != nil {
		return replies, err
	}
	return replies, nil
}

func BroadcastCommand(requests *c.RequestManager, cmd c.Command) ([]*c.Response, errors.SdkError) {
	options := &c.RequestOptions{
		CollectMultiple: true,
	}
	replies, err := requests.Request(cmd, options)
	if err != nil {
		return nil, err
	}
	replies = pie.Filter(replies, func(reply *c.Response) bool {
		return reply.ReplyFlag == "OK"
	})
	return replies, nil
}
