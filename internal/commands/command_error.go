package commands

import (
	"fmt"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

type commandFailedErr struct {
	errorType  pb.Errors
	reply      *c.Response
	command    *c.Command
	message    string
	customData pb.Message
}

type CommandFailedErr interface {
	errors.SdkError
	Command() *c.Command
	Response() *c.Response
	Data() string
}

func (err *commandFailedErr) Type() pb.Errors {
	return err.errorType
}
func (err *commandFailedErr) TypeStr() string {
	return pb.Errors_name[err.Type()]
}
func (err *commandFailedErr) Response() *c.Response {
	return err.reply
}
func (err *commandFailedErr) Command() *c.Command {
	return err.command
}
func (err *commandFailedErr) Data() string {
	return err.reply.Data
}
func (err *commandFailedErr) Message() string {
	return err.message
}
func (err *commandFailedErr) AddToMessage(message string) {
	err.message += " " + message
}
func (err *commandFailedErr) AddToMessagef(format string, args ...interface{}) {
	err.AddToMessage(fmt.Sprintf(format, args...))
}
func (err *commandFailedErr) String() string {
	return fmt.Sprintf("%s: %s", err.TypeStr(), err.message)
}
func (err *commandFailedErr) CustomData() pb.Message {
	return err.customData
}

func IsBadDataErr(err errors.SdkError) bool {
	return IsCommandErr(err, "BADDATA")
}

func IsBadCommandErr(err errors.SdkError) bool {
	return IsCommandErr(err, "BADCOMMAND")
}

func IsAgainErr(err errors.SdkError) bool {
	return IsCommandErr(err, "AGAIN")
}

func IsBusyErr(err errors.SdkError) bool {
	return IsCommandErr(err, "STATUSBUSY")
}

func IsParkedErr(err errors.SdkError) bool {
	return IsCommandErr(err, "PARKED")
}

func IsDeviceOnlyErr(err errors.SdkError) bool {
	return IsCommandErr(err, "DEVICEONLY")
}

func IsInvalidLimitsErr(err errors.SdkError) bool {
	return IsCommandErr(err, "INVALIDLIMITS")
}

func IsCommandErr(err errors.SdkError, reason string) bool {
	deviceErr, ok := err.(*commandFailedErr)
	return ok && deviceErr.Data() == reason
}

func NewCommandFailedErr(message string, reply *c.Response, command *c.Command) CommandFailedErr {
	customData := &pb.ExceptionsCommandFailedExceptionData{
		ResponseData:  reply.Data,
		ReplyFlag:     reply.ReplyFlag,
		Status:        reply.Status,
		WarningFlag:   reply.WarningFlag,
		Command:       command.Command,
		DeviceAddress: int32(reply.Device),
		AxisNumber:    int32(reply.Axis),
		Id:            int32(reply.ID),
	}

	var errorType pb.Errors = pb.Errors_COMMAND_FAILED
	switch reply.Data {
	case "BADCOMMAND":
		errorType = pb.Errors_BAD_COMMAND
	case "BADDATA":
		errorType = pb.Errors_BAD_DATA
	case "DRIVERDISABLED":
		errorType = pb.Errors_DRIVER_DISABLED
	case "REMOTE":
		errorType = pb.Errors_REMOTE_MODE
	}

	return &commandFailedErr{
		errorType:  errorType,
		reply:      reply,
		command:    command,
		message:    message,
		customData: customData,
	}
}

func NewErrFromExisting(message string, err CommandFailedErr) CommandFailedErr {
	return NewCommandFailedErr(message, err.Response(), err.Command())
}
