package commands

import (
	"strings"
	c "zaber-motion-lib/internal/communication"
	g "zaber-motion-lib/internal/generated"
)

func GetWarningText(warning string) string {
	text, hasText := g.WarningFlags[warning]
	if hasText {
		return text
	}

	if strings.HasPrefix(warning, "F") {
		return "Unknown fault"
	}
	if strings.HasPrefix(warning, "W") {
		return "Unknown warning"
	}
	return "Unknown warning flag"
}

func HasWarnings(reply *c.Response) bool {
	return reply.WarningFlag != "--"
}

func HasFaultOrWarning(reply *c.Response) bool {
	return HasWarnings(reply) && !strings.HasPrefix(reply.WarningFlag, "N")
}
