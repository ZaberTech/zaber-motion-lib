package commands

import (
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func SingleCommandDevice(requests *c.RequestManager, request ioc.DeviceTarget, command string) (*c.Response, errors.SdkError) {
	return SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: command,
	})
}

func SingleRequestDevice(devices ioc.DeviceManager, request ioc.DeviceTarget, command string) (*c.Response, errors.SdkError) {
	requests, err := devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	return SingleCommandDevice(requests, request, command)
}

func SingleCommandAxis(requests *c.RequestManager, request ioc.AxisTarget, command string) (*c.Response, errors.SdkError) {
	return SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: command,
	})
}

func SingleRequestAxis(devices ioc.DeviceManager, request ioc.AxisTarget, command string) (*c.Response, errors.SdkError) {
	requests, err := devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	return SingleCommandAxis(requests, request, command)
}
