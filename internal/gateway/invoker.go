//go:build !windows && !wasm

package gateway

/*
#include <stdlib.h>
#include <stdint.h>

typedef void (*callbackType) (const void*, int64_t);

void invoke(const void* data, int64_t tag, void* callback)
{
	((callbackType)callback)(data, tag);
}
*/
import "C"

import (
	"unsafe"
)

/*
Only sync mode of callback invoke is provided. See invoker_windows.go.
*/

func invokeCallback(data unsafe.Pointer, tag C.int64_t, callback unsafe.Pointer, _ bool) {
	C.invoke(data, tag, callback)
	C.free(data)
}
