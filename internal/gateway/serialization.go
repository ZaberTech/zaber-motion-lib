package gateway

import (
	"encoding/binary"

	pb "zaber-motion-lib/internal/dto"
)

const sizeTypeSize = 4 // uint32

func deserialize(data []byte) [][]byte {
	bufferSlice := data[sizeTypeSize:] // skip total length
	var buffers [][]byte

	for len(bufferSlice) > 0 {
		size := binary.LittleEndian.Uint32(bufferSlice)
		buffer := bufferSlice[sizeTypeSize : sizeTypeSize+size]

		buffers = append(buffers, buffer)
		bufferSlice = bufferSlice[sizeTypeSize+size:]
	}

	return buffers
}

func serialize(messages []pb.Message) ([]byte, error) {
	buffer := make([]byte, sizeTypeSize)

	for _, message := range messages {
		messageBytes, err := message.ToBytes()
		if err != nil {
			return nil, err
		}

		sizeBytes := make([]byte, sizeTypeSize)
		binary.LittleEndian.PutUint32(sizeBytes, uint32(len(messageBytes)))
		buffer = append(buffer, sizeBytes...)

		buffer = append(buffer, messageBytes...)
	}

	binary.LittleEndian.PutUint32(buffer, uint32(len(buffer)))

	return buffer, nil
}
