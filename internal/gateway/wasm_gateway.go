//go:build wasm

package gateway

import (
	"syscall/js"
)

type wasmGateway struct {
	requestCb         js.Func
	setEventHandlerCb js.Func
	shutdownCb        js.Func
	openCustomCb      js.Func

	done chan bool
}

func New() *wasmGateway {
	return &wasmGateway{
		done: make(chan bool, 1),
	}
}

func (g *wasmGateway) Run() {
	g.setupCallbacks()
	defer g.releaseCallbacks()

	js.Global().Set("__zmlShutdown", g.shutdownCb)
	defer js.Global().Set("__zmlShutdown", js.Undefined())

	js.Global().Set("__zmlRequest", g.requestCb)
	defer js.Global().Set("__zmlRequest", js.Undefined())

	js.Global().Set("__zmlSetEventHandler", g.setEventHandlerCb)
	defer js.Global().Set("__zmlSetEventHandler", js.Undefined())

	js.Global().Get("__zmlStarted").Invoke()

	<-g.done // from shutdown cb
}
