package gateway

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/ioc"
)

func processCall(data []byte) []byte {
	buffers := deserialize(data)

	request := &pb.GatewayRequest{}
	if err := request.FromBytes(buffers[0]); err != nil {
		panic(err)
	}

	var requestData []byte
	if len(buffers) > 1 {
		requestData = buffers[1]
	}

	response, responseData := ioc.Instance.GetGateway().Request(request, requestData)

	responseMessages := []pb.Message{response}
	if responseData != nil {
		responseMessages = append(responseMessages, responseData)
	}

	responseBytes, err := serialize(responseMessages)
	if err != nil {
		panic(err)
	}

	return responseBytes
}
