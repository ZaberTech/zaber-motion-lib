package gateway

import (
	"sync"

	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type callbackData struct {
	callback      ioc.Callback
	createMessage ioc.CreateMessage
}

type callManager struct {
	callbacks sync.Map
}

func (manager *callManager) Request(request *pb.GatewayRequest, requestData []byte) (response *pb.GatewayResponse, responseData pb.Message) {
	var returnedErr errors.SdkError

	if callback, requestKnown := manager.callbacks.Load(request.GetRequest()); requestKnown {
		callbackData := callback.(*callbackData)

		var requestDataProto pb.Message
		if requestData != nil {
			requestDataProto = callbackData.createMessage()
			if err := requestDataProto.FromBytes(requestData); err != nil {
				returnedErr = errors.ErrInvalidRequestData(err.Error())
			}
		}

		if returnedErr == nil {
			responseData, returnedErr = callbackData.callback(requestDataProto)
		}
	} else {
		returnedErr = errors.ErrUnknownRequest(request.GetRequest())
	}

	if returnedErr != nil {
		response = &pb.GatewayResponse{
			Response:     pb.ResponseType_ERROR,
			ErrorType:    returnedErr.Type(),
			ErrorMessage: returnedErr.Message(),
		}
		responseData = returnedErr.CustomData()
	} else {
		response = &pb.GatewayResponse{
			Response: pb.ResponseType_OK,
		}
	}

	return response, responseData
}

func (manager *callManager) RegisterCallback(request string, createMessage ioc.CreateMessage, callback ioc.Callback) {
	callbackData := &callbackData{
		createMessage: createMessage,
		callback:      callback,
	}

	manager.callbacks.Store(request, callbackData)
}

func (manager *callManager) InvokeEvent(event string, eventData pb.Message) {
	eventProto := &pb.GatewayEvent{
		Event: event,
	}
	invokeEvent(eventProto, eventData)
}

func NewGatewayManager() *callManager {
	manager := &callManager{}

	return manager
}
