//go:build !wasm

package gateway

/*
#include <stdint.h>

uint32_t get_size(const void* data) {
	return *((uint32_t*)data);
}
*/
import "C"
import (
	"unsafe"
)

type Tag int64

func CallInternal(data unsafe.Pointer, tag Tag, callback unsafe.Pointer, async bool) int32 {
	totalLength := C.get_size(data)
	dataBytes := C.GoBytes(data, C.int(totalLength))

	if async {
		go processCallAndInvokeCallback(dataBytes, tag, callback, async)
	} else {
		processCallAndInvokeCallback(dataBytes, tag, callback, async)
	}

	return 0
}

func processCallAndInvokeCallback(dataBytes []byte, tag Tag, callback unsafe.Pointer, async bool) {
	responseBytes := processCall(dataBytes)

	pointer := C.CBytes(responseBytes)
	// responsible for freeing the data
	invokeCallback(pointer, (C.int64_t)(tag), callback, async)
}
