#ifndef CALLBACK_INTERNAL_H_
#define CALLBACK_INTERNAL_H_

#include <stdint.h>

#ifdef _WIN32
#define ZML_DLL_EXPORT __declspec(dllexport)
#else
#define ZML_DLL_EXPORT
#endif

ZML_DLL_EXPORT void zml_setEventHandler(int64_t tag, void* callback);

#endif
