//go:build !wasm

package gateway

/*
#include "callback.h"

extern void (*eventHandler) (const void*, int64_t);
extern int64_t eventHandlerTag;
*/
import "C"

import (
	"unsafe"
	pb "zaber-motion-lib/internal/dto"
)

func invokeEvent(event *pb.GatewayEvent, eventData pb.Message) {
	eventMessages := []pb.Message{event}
	if eventData != nil {
		eventMessages = append(eventMessages, eventData)
	}

	eventBytes, err := serialize(eventMessages)
	if err != nil {
		panic(err)
	}

	pointer := C.CBytes(eventBytes)

	eventHandler := unsafe.Pointer(C.eventHandler)
	if eventHandler == nil {
		panic("Events are not initialized (call setEventHandler)")
	}

	// responsible for freeing the data
	invokeCallback(pointer, C.eventHandlerTag, eventHandler, true)
}
