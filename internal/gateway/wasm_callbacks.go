//go:build wasm

package gateway

import (
	b64 "encoding/base64"
	"syscall/js"
)

func (g *wasmGateway) onRequest(this js.Value, args []js.Value) interface{} {
	dataBytes, err := b64.StdEncoding.DecodeString(args[0].String())
	if err != nil {
		panic(err)
	}

	switch len(args) {
	case 2:
		go func() {
			responseBytes := processCall(dataBytes)
			responseString := b64.StdEncoding.EncodeToString(responseBytes)
			callback := args[1]
			callback.Invoke(responseString)
		}()
	case 1:
		responseBytes := processCall(dataBytes)
		responseString := b64.StdEncoding.EncodeToString(responseBytes)
		return responseString
	default:
		panic("Incorrect number of arguments for __zmlRequest")
	}

	return nil
}

func (g *wasmGateway) onSetEventHandler(this js.Value, args []js.Value) interface{} {
	callback := args[0]
	setEventHandler(callback)
	return nil
}

func (g *wasmGateway) onShutdown(this js.Value, args []js.Value) interface{} {
	g.done <- true
	return nil
}

func (g *wasmGateway) setupCallbacks() {
	g.requestCb = js.FuncOf(g.onRequest)
	g.setEventHandlerCb = js.FuncOf(g.onSetEventHandler)
	g.shutdownCb = js.FuncOf(g.onShutdown)
}

func (g *wasmGateway) releaseCallbacks() {
	g.requestCb.Release()
	g.setEventHandlerCb.Release()
	g.shutdownCb.Release()
}
