//go:build windows && !wasm

package gateway

/*
#include <windows.h>
#include <stdint.h>

typedef void (*callbackType) (const void*, int64_t);

typedef struct Link {
	void* data;
	int64_t tag;
	void* callback;

	struct Link* next;
} Link;

Link* queueBeginning = NULL;
Link* queueEnd = NULL;

CRITICAL_SECTION mutex;
HANDLE event;

DWORD WINAPI mainLoop(LPVOID lpParameter) {
	while(TRUE) {
		Link* link = NULL;

		EnterCriticalSection(&mutex);

		if (queueBeginning != NULL) {
			link = queueBeginning;
			queueBeginning = queueBeginning->next;

			if (queueBeginning == NULL) {
				queueEnd = NULL;
			}
		}

		LeaveCriticalSection(&mutex);

		if (link != NULL) {
			((callbackType)link->callback)(link->data, link->tag);

			free(link->data);
			free(link);
		} else {
			WaitForSingleObject(event, INFINITE);
		}
	}
}

int init() {
	InitializeCriticalSection(&mutex);

	event = CreateEventA(NULL, FALSE, FALSE, NULL);
	if (event == NULL) {
		return 1;
	}

	HANDLE thread = CreateThread(NULL, 0, mainLoop, NULL, 0, NULL);
	if (thread == NULL) {
		return 1;
	}

	CloseHandle(thread);

	return 0;
}

int invoke(void* data, int64_t tag, void* callback) {
	Link* link = malloc(sizeof(Link));
	if (!link) {
		return 1;
	}

	link->data = data;
	link->tag = tag;
	link->callback = callback;
	link->next = NULL;

	EnterCriticalSection(&mutex);

	if (queueBeginning == NULL) {
		queueBeginning = link;
		queueEnd = link;
	} else {
		queueEnd->next = link;
		queueEnd = link;
	}

	LeaveCriticalSection(&mutex);

	SetEvent(event);

	return 0;
}

void invokeSync(const void* data, int64_t tag, void* callback)
{
	((callbackType)callback)(data, tag);
}

*/
import "C"

import (
	"unsafe"
)

/*
Code above starts WinAPI thread which is used to invoke all async callbacks.
Otherwise .NET debugger always corrupts Go stack which renders .NET app non-debuggable.
It seems that .NET debugger does not like functions invoked on top of Go stack.
*/

func init() {
	initResult := C.init()
	if initResult > 0 {
		panic("cannot init windows objects")
	}
}

func invokeCallback(data unsafe.Pointer, tag C.int64_t, callback unsafe.Pointer, async bool) {
	if async {
		invokeResult := C.invoke(data, tag, callback)
		if invokeResult > 0 {
			panic("out of memory")
		}
	} else {
		C.invokeSync(data, tag, callback)
		C.free(data)
	}
}
