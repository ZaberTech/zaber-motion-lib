#include "callback.h"


void (*eventHandler) (const void*, int64_t) = 0;
int64_t eventHandlerTag = 0;

void zml_setEventHandler(int64_t tag, void* callback) {
    eventHandler = callback;
    eventHandlerTag = tag;
}
