//go:build wasm

package gateway

import (
	b64 "encoding/base64"
	pb "zaber-motion-lib/internal/dto"

	"syscall/js"
)

var eventHandler js.Value
var initialized bool = false

func setEventHandler(callback js.Value) {
	eventHandler = callback
	initialized = true
}

func invokeEvent(event *pb.GatewayEvent, eventData pb.Message) {
	if initialized == false {
		panic("Events are not initialized (call setEventHandler)")
	}
	eventMessages := []pb.Message{event}
	if eventData != nil {
		eventMessages = append(eventMessages, eventData)
	}

	eventBytes, err := serialize(eventMessages)
	if err != nil {
		panic(err)
	}
	responseString := b64.StdEncoding.EncodeToString(eventBytes)
	eventHandler.Invoke(responseString)
}
