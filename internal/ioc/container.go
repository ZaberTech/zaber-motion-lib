package ioc

type Container interface {
	GetGateway() GatewayManager
	GetInterfaceManager() InterfaceManager
}

var Instance Container
