package ioc

import (
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	g "zaber-motion-lib/internal/generated"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"

	"zaber-motion-lib/internal/errors"

	deviceDbApi "gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	deviceDbDto "gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

// Gateway
type Callback func(pb.Message) (pb.Message, errors.SdkError)
type CreateMessage func() pb.Message

type GatewayManager interface {
	RegisterCallback(request string, createMessage CreateMessage, callback Callback)
	Request(request *pb.GatewayRequest, requestData []byte) (response *pb.GatewayResponse, responseData pb.Message)
	InvokeEvent(event string, eventData pb.Message)
}

// Interfaces
type OnInterfaceRemovedCallback func(CommunicationInterface)

type ResponseFilter func(*c.Response) bool
type BinaryAlertFilter func(*c.BinaryMessage) bool

type ResponseSubscription interface {
	Cancel()
	Channel() chan *c.Response
}

type BinaryAlertSubscription interface {
	Cancel()
	Channel() chan *c.BinaryMessage
}

type CommunicationInterface interface {
	ID() int32
	ToString() string
}

type ASCIICommunicationInterface interface {
	CommunicationInterface
	Requests() *c.RequestManager
	// Subscribes for a single alert that conforms to the filter.
	SubscribeForAlert(filter ResponseFilter) ResponseSubscription
	// Subscribes for a single unknown response that conforms to the filter.
	SubscribeForUnknownResponse(filter ResponseFilter) ResponseSubscription
	SetPacketSize(packetSize int)
	SetDeviceProtocolInfo(deviceNumber int, info *c.DeviceProtocolInfo)
	GetDeviceLimits(deviceNumber int) (*c.DeviceProtocolInfo, int)
}

type BinaryCommunicationInterface interface {
	CommunicationInterface
	BinaryRequests() *c.BinaryRequestManager
	// Subscribes for a single alert that conforms to the filter.
	SubscribeForBinaryAlert(filter BinaryAlertFilter) BinaryAlertSubscription
}

type InterfaceManager interface {
	GetInterface(id int32) (CommunicationInterface, errors.SdkError)
	OnInterfaceRemoved(callback OnInterfaceRemovedCallback)
	AddCommunication(cBase.IO, string) int32
}

// Device DB
type ContextualDimensionID int
type ParamType string

type ParamInfo struct {
	ContextualDimensionID ContextualDimensionID
	ParamType             ParamType
}

type BinaryCommandDimensions struct {
	ParamContextualDimension  ContextualDimensionID
	ReturnContextualDimension ContextualDimensionID
}

type DeviceDb interface {
	GetProductInformation(query *deviceDbApi.GetProductInformationQuery) (*deviceDbDto.DeviceInfo, errors.SdkError)
	GetBinaryProductInformation(deviceID string, fw *deviceDbDto.FirmwareVersion, peripheralID string) (*deviceDbDto.BinaryDeviceInfo, errors.SdkError)
	GetCommandTree(tree *deviceDbDto.CommandNode) CommandTree
	GetBinaryCommandTable(table *deviceDbDto.BinaryCommandsTable) BinaryCommandTable
	GetServoTuningInfo(productID int, fw *deviceDbDto.FirmwareVersion) (*deviceDbDto.ServoTuningInfo, errors.SdkError)
	GetSettingsTable(table *deviceDbDto.SettingsTable) SettingsTable
	GetBinarySettingsTable(table *deviceDbDto.BinarySettingsTable) BinarySettingsTable
}

type CommandTree interface {
	GetParamsInfo(command []string) ([]*ParamInfo, bool)
}
type BinaryCommandTable interface {
	GetContextualDimensions(commandCode pb.BinaryCommandCode) (*BinaryCommandDimensions, bool)
}
type SettingsTable interface {
	GetParamInfo(setting string) (*ParamInfo, bool)
	GetDefaultValue(setting string) (valueOrNil *string, settingFound bool)
}
type BinarySettingsTable interface {
	GetContextualDimension(setting *g.BinarySetting) (ContextualDimensionID, bool)
}

// Units
type Units interface {
	GetConversionTable(tableData *deviceDbDto.ConversionTable, resolution int) ConversionTable
	ConvertStaticUnitToDimensionBase(value float64, unit string) (float64, errors.SdkError)
	ConvertStaticUnitFromDimensionBase(value float64, unit string) (float64, errors.SdkError)
	ConvertStaticUnit(value float64, fromUnit string, toUnit string) (float64, errors.SdkError)
	AreUnitsCompatible(unit1 string, unit2 string) errors.SdkError
	CheckDimension(unit string, dimension string) errors.SdkError
	GetBaseUnits(dimension string) (string, errors.SdkError)
	GetDimensionName(units string) (string, errors.SdkError)
}

type ConversionTable interface {
	GetDimensionName(ctxDimensionID ContextualDimensionID) (string, bool)
	GetContextualDimensionName(ctxDimensionID ContextualDimensionID) (string, bool)
	GetContextualDimensionIDs() []ContextualDimensionID
	ConvertSettingValue(settingsTable SettingsTable, setting string, value float64, unit string) (float64, errors.SdkError)
	Convert(value float64, unit string, ctxDimensionID ContextualDimensionID) (float64, errors.SdkError)
	ConvertBack(value float64, unit string, ctxDimensionID ContextualDimensionID) (float64, errors.SdkError)
	CanConvert(ctxDimensionID ContextualDimensionID) bool
	GetSourceData() *dto.ConversionTable
	GetScale(functionName string, value float64) (float64, errors.SdkError)
	GetBaseUnits(ctxDimensionID ContextualDimensionID) (string, errors.SdkError)
	GetUnits() Units
}

// Library
type Library interface {
	GetVersionString() string
}

type CustomConnections interface {
	GetAndBindTransport(transportID int32) (cBase.IO, errors.SdkError)
}

type DeviceManager interface {
	SetStorageManager(manager StorageManager) // solves circular dependency
	GetStreamCommandIssuingInfo(stream StreamTarget) ([]*CommandIssuingInfo, errors.SdkError)
	StreamCommands(stream StreamTarget, commands []string) errors.SdkError
	StreamWaitUntilIdle(stream StreamTarget) errors.SdkError
	GetStreamMode(stream StreamTarget) (pb.AsciiStreamMode, errors.SdkError)
	GetStreamAxisSetting(stream StreamTarget, setting string, checkIdle bool) ([]float64, errors.SdkError)
	GetDeviceInfo(request DeviceTarget) (DeviceInfo, errors.SdkError)
	GetUnidentifiedDeviceInfo(request DeviceTarget) (UnidentifiedDeviceInfo, errors.SdkError)
	GetIdentifiedDeviceInfo(request DeviceTarget) (DeviceInfo, errors.SdkError)
	GetAllIdentifiedDevices(target InterfaceTarget) ([]DeviceInfo, errors.SdkError)
	GetProtocolInfo(target DeviceTarget) (*c.DeviceProtocolInfo, int, errors.SdkError)
	GetRequests(target InterfaceTarget) (*c.RequestManager, errors.SdkError)
	GetInterface(target InterfaceTarget) (ASCIICommunicationInterface, errors.SdkError)
	ConvertUnitSetting(request AxisTarget, setting string, value float64, unit string, convertBack bool) (float64, errors.SdkError)
	BuildCommand(request AxisTarget, commandTemplate []string, arguments []CommandArg) (string, errors.SdkError)
	SetDigitalOutputSchedule(request *pb.DeviceSetDigitalOutputScheduleRequest) errors.SdkError
}

type StorageManager interface {
	GetNumber(request AxisTarget, key string) (float64, errors.SdkError)
	SetNumber(request AxisTarget, key string, value float64) errors.SdkError
	Erase(request AxisTarget, key string) (bool, errors.SdkError)
	SetStorage(request AxisTarget, key string, value string, encoded bool) errors.SdkError
	GetStorage(request AxisTarget, key string, encoded bool) (string, errors.SdkError)
	ListKeyValuePairs(request AxisTarget, prefix string, encoded bool) (map[string]string, errors.SdkError)
	SetStorageStructure(request AxisTarget, key string, dataStruct interface{}) errors.SdkError
	GetStoredStructure(request AxisTarget, key string, dataStruct interface{}) (bool, errors.SdkError)
	EraseWithPrefix(request AxisTarget, prefix string, allAxes bool) errors.SdkError
}

type AxisInfo interface {
	GetProduct() *dto.Device
	GetCommandIssuingInfo() *CommandIssuingInfo
}

type UnidentifiedDeviceInfo interface {
	GetAddress() int
	AssociateStruct(key string, data interface{})
	GetAssociatedStruct(key string) (interface{}, bool)
	// Returns nil if not identified.
	GetIdentified() DeviceInfo
	ToString(typeName string) string
	ToStringAxis(axis AxisTarget, typeName string) (string, errors.SdkError)
}

type DeviceInfo interface {
	UnidentifiedDeviceInfo
	IsIdentified() bool
	VerifyIdentified() errors.SdkError
	GetProduct() *dto.Device
	GetFirmwareVersion() *dto.FirmwareVersion
	GetAxisInfo(request AxisTarget) (AxisInfo, errors.SdkError)
	GetAxes() []AxisInfo
	GetLabel(request AxisTarget) (string, errors.SdkError)
	SetLabel(request AxisTarget, label string) errors.SdkError
	GetCommandIssuingInfo(request AxisTarget) (*CommandIssuingInfo, errors.SdkError)
	IsPeripheralLikeAxis(axis int) bool
	GetServoTuningDefaultParams(read func() ([]*pb.AsciiServoTuningParam, errors.SdkError)) ([]*pb.AsciiServoTuningParam, errors.SdkError)
}

type AutofocusProvider interface {
	IsReadyToFocus(options IsReadyToFocusOptions) errors.SdkError
	SetFocusZero() errors.SdkError
	GetStatus() (AutofocusProviderStatus, errors.SdkError)
	GetCurrentObjective() (int, errors.SdkError)
	SetCurrentObjective(objective int) errors.SdkError
	SetObjectiveParameters(objective int, parameters []*pb.NamedParameter) errors.SdkError
	GetObjectiveParameters(objective int) ([]*pb.NamedParameter, errors.SdkError)
	IsValidParameter(name string) bool
	ToString() string
}

type Microscopy interface {
	RegisterAutofocusProvider(provider AutofocusProvider) int32
	GetAutofocusProvider(id int32) (provider AutofocusProvider, ok bool)
	RemoveAutofocusProvider(id int32) (provider AutofocusProvider, ok bool)
}
