package ioc

type CommandIssuingInfo struct {
	ConversionTable ConversionTable
	CommandTree     CommandTree
	SettingsTable   SettingsTable
}

type InterfaceTarget interface {
	GetInterfaceId() int32
}

type DeviceTarget interface {
	InterfaceTarget
	GetDevice() int32
}

type AxisTarget interface {
	DeviceTarget
	GetAxis() int32
}

type WaitUntilIdleTarget interface {
	GetWaitUntilIdle() bool
	AxisTarget
}

type StreamTarget interface {
	DeviceTarget
	GetStreamId() int32
	GetPvt() bool
}

type CommandArg interface {
	GetValue() float64
	GetUnit() string
}

type AutofocusProviderStatus interface {
	GetInRange() bool
}

type IsReadyToFocusOptions struct {
	IgnoreInRange bool
}
