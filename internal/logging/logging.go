package logging

import (
	"io"
	"log"
	"os"
	"strings"
	"sync"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

type logManager struct {
	gateway ioc.GatewayManager
	lock    sync.Mutex
	logFile *os.File
}

func NewLogging(gateway ioc.GatewayManager) *logManager {
	manager := &logManager{
		gateway: gateway,
	}

	manager.register()
	manager.setFromEnv()

	return manager
}

func (manager *logManager) setFromEnv() {
	logEnv := os.Getenv("ZABER_MOTION_LIB_LOG")

	var err errors.SdkError
	if strings.HasPrefix(logEnv, "file:") {
		filename := strings.TrimPrefix(logEnv, "file:")
		err = manager.setLogOutput(pb.LogOutputMode_FILE, filename)
	} else if logEnv == "stderr" {
		err = manager.setLogOutput(pb.LogOutputMode_STDERR, "")
	} else if logEnv == "stdout" {
		err = manager.setLogOutput(pb.LogOutputMode_STDOUT, "")
	} else {
		err = manager.setLogOutput(pb.LogOutputMode_OFF, "")
	}
	if err != nil {
		panic(err)
	}
}

func (manager *logManager) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("logging/set_output", func() pb.Message {
		return &pb.SetLogOutputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setLogOutputRequest(request.(*pb.SetLogOutputRequest))
	})
}

func (manager *logManager) setLogOutputRequest(request *pb.SetLogOutputRequest) errors.SdkError {
	return manager.setLogOutput((pb.LogOutputMode)(request.Mode), utils.PtrDefault(request.FilePath, ""))
}

func (manager *logManager) setLogOutput(logOutputMode pb.LogOutputMode, filePath string) errors.SdkError {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	oldfile := manager.logFile

	if logOutputMode == pb.LogOutputMode_OFF {
		log.SetFlags(0)
	} else {
		log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	}

	switch logOutputMode {
	case pb.LogOutputMode_OFF:
		log.SetOutput(io.Discard)
	case pb.LogOutputMode_STDOUT:
		log.SetOutput(os.Stdout)
	case pb.LogOutputMode_STDERR:
		log.SetOutput(os.Stderr)
	case pb.LogOutputMode_FILE:
		file, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		if err != nil {
			return errors.ErrIOError("open", filePath, err.Error())
		}
		manager.logFile = file
		log.SetOutput(file)
	}

	if oldfile != nil {
		if manager.logFile == oldfile {
			manager.logFile = nil
		}

		if err := oldfile.Close(); err != nil {
			return errors.ErrIOError("close", oldfile.Name(), err.Error())
		}
	}

	return nil
}
