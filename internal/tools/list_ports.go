//go:build !wasm

package tools

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	serial "github.com/zabertech/go-serial"
)

func (instance *tools) listSerialPorts() (*pb.ToolsListSerialPortsResponse, errors.SdkError) {
	ports, err := serial.GetPortsList()
	if err != nil {
		return nil, errors.ErrOSError("Cannot list ports: " + err.Error())
	}

	response := &pb.ToolsListSerialPortsResponse{
		Ports: ports,
	}
	return response, nil
}
