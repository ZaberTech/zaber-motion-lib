//go:build wasm

package tools

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (instance *tools) listSerialPorts() (*pb.ToolsListSerialPortsResponse, errors.SdkError) {
	return &pb.ToolsListSerialPortsResponse{}, nil
}
