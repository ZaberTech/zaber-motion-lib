package tools

import (
	"fmt"
	"time"
	"zaber-motion-lib/internal/commands"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

type tools struct {
	gateway ioc.GatewayManager
	library ioc.Library
}

func NewTools(gateway ioc.GatewayManager, library ioc.Library) *tools {
	instance := &tools{
		gateway: gateway,
		library: library,
	}

	instance.register()

	return instance
}

func (instance *tools) register() {
	gateway := instance.gateway

	gateway.RegisterCallback("tools/list_serial_ports", func() pb.Message {
		return &pb.EmptyRequest{}
	}, func(_ pb.Message) (pb.Message, errors.SdkError) {
		return instance.listSerialPorts()
	})
	gateway.RegisterCallback("tools/get_message_router_pipe", func() pb.Message {
		return &pb.EmptyRequest{}
	}, func(_ pb.Message) (pb.Message, errors.SdkError) {
		return instance.getMessageRouterPipe()
	})
	gateway.RegisterCallback("tools/get_db_service_pipe", func() pb.Message {
		return &pb.EmptyRequest{}
	}, func(_ pb.Message) (pb.Message, errors.SdkError) {
		return instance.getDbServicePipe()
	})
	gateway.RegisterCallback("library/set_internal_mode", func() pb.Message {
		return &pb.SetInternalModeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setInternalMode(request.(*pb.SetInternalModeRequest))
	})
	gateway.RegisterCallback("library/set_idle_polling_period", func() pb.Message {
		return &pb.IntRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setIdlePollingPeriod(request.(*pb.IntRequest))
	})
	gateway.RegisterCallback("library/check_version", func() pb.Message {
		return &pb.CheckVersionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.checkVersion(request.(*pb.CheckVersionRequest))
	})
}

func (instance *tools) getMessageRouterPipe() (*pb.StringResponse, errors.SdkError) {
	path, err := communication.GetMessageRouterPipe()
	if err != nil {
		return nil, errors.ErrOSError("Cannot get message router pipe: " + err.Error())
	}

	return &pb.StringResponse{
		Value: path,
	}, nil
}

func (instance *tools) getDbServicePipe() (*pb.StringResponse, errors.SdkError) {
	path, err := communication.GetDbServicePipe()
	if err != nil {
		return nil, errors.ErrOSError("Cannot get database service pipe: " + err.Error())
	}

	return &pb.StringResponse{
		Value: path,
	}, nil
}

func (instance *tools) setInternalMode(request *pb.SetInternalModeRequest) errors.SdkError {
	commands.SetCheckFF(!request.Mode)
	return nil
}

func (instance *tools) setIdlePollingPeriod(request *pb.IntRequest) errors.SdkError {
	constants.SetIdlePollingPeriod(time.Duration(request.Value) * time.Millisecond)
	return nil
}

func (instance *tools) checkVersion(request *pb.CheckVersionRequest) errors.SdkError {
	version := instance.library.GetVersionString()
	if version != "DEV" && request.Version != version {
		message := fmt.Sprintf(
			"The loaded Zaber Motion shared library version %s does not match expected version %s. ",
			version, request.Version)
		message += "The installation of the library is corrupt. "
		switch request.Host {
		case "py":
			message += "Reset your virtual environment and reinstall the library."
		case "cpp":
			message += "Reinstall the library into your system and rebuild your project."
		case "cs":
			message += "Reinstall the NuGet package and rebuild your project."
		case "java":
			message += "Reinstall the Maven package and rebuild your project."
		case "js":
			message += "Delete the library from node_modules and install it again."
		}
		return errors.ErrIncompatibleSharedLibrary(message)
	}

	return nil
}
