package commandbuilding

import (
	"fmt"
	"regexp"
	"strings"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/generated"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

var isAccelerationUnitRegex, _ = regexp.Compile("^(Angular Acceleration|Acceleration):")

func replaceParametersWithArguments(commandTemplate []string, arguments []string) []string {
	argumentsIndex := 0
	result := make([]string, len(commandTemplate))
	for i, cmdPart := range commandTemplate {
		if cmdPart == constants.CommandArgument {
			result[i] = arguments[argumentsIndex]
			argumentsIndex++
		} else {
			result[i] = commandTemplate[i]
		}
	}

	return result
}

func checkInvalidConversion(arg CommandArg, convertedRoundedValue float64) errors.SdkError {
	if isAccelerationUnitRegex.MatchString(arg.GetUnit()) && convertedRoundedValue == 0 && arg.GetValue() != 0 {
		errorMsg := fmt.Sprintf("The result of conversion and rounding is a forbidden value. Acceleration cannot be 0. Provided value: %f %s.", arg.GetValue(), arg.GetUnit())
		return errors.ErrConversionFailed(errorMsg)
	}
	return nil
}

func getParamsInfo(commandTemplate []string, arguments []CommandArg, info *ioc.CommandIssuingInfo) ([]*ioc.ParamInfo, errors.SdkError) {
	var paramsInfo []*ioc.ParamInfo
	if commandTemplate[0] == "set" {
		if len(commandTemplate) != 3 || len(arguments) != 1 {
			errorMessage := fmt.Sprintf("Set command should be followed by a setting name and a value. Got %v.", commandTemplate)
			return nil, errors.ErrInvalidArgument(errorMessage)
		}

		paramsInfo = make([]*ioc.ParamInfo, 1)
		if info, found := info.SettingsTable.GetParamInfo(commandTemplate[1]); found {
			paramsInfo[0] = info
		} else if arguments[0].GetUnit() != constants.NativeUnit {
			errorMessage := fmt.Sprintf("Setting name \"%s\" not found.", commandTemplate[1])
			return nil, errors.ErrNotSupported(errorMessage)
		}
	} else {
		var found bool
		paramsInfo, found = info.CommandTree.GetParamsInfo(commandTemplate)
		if !found {
			errorMessage := fmt.Sprintf("Command with template \"%s\" not supported.", strings.Join(commandTemplate, " "))
			return nil, errors.ErrNotSupported(errorMessage)
		}
	}

	return paramsInfo, nil
}

func convertArgument(argument CommandArg, paramInfo *ioc.ParamInfo, paramIndex int, conversionTable ioc.ConversionTable, convertBack bool) (float64, errors.SdkError) {
	var convertedValue float64
	if argument.GetUnit() != constants.NativeUnit {
		if paramInfo == nil {
			errorMessage := "Command or setting not found while requesting unit conversion."
			return 0, errors.ErrNotSupported(errorMessage)
		} else if paramInfo.ContextualDimensionID == 0 {
			errorMessage := fmt.Sprintf("Command argument index %d does not support unit conversion.", paramIndex)
			return 0, errors.ErrConversionFailed(errorMessage)
		}

		var value float64
		var err errors.SdkError
		if convertBack {
			value, err = conversionTable.ConvertBack(argument.GetValue(), argument.GetUnit(), paramInfo.ContextualDimensionID)
		} else {
			value, err = conversionTable.Convert(argument.GetValue(), argument.GetUnit(), paramInfo.ContextualDimensionID)
		}

		if err != nil {
			return 0, err
		}
		convertedValue = value
	} else {
		convertedValue = argument.GetValue()
	}

	return convertedValue, nil
}

func convertAndFormatArgument(argument CommandArg, paramInfo *ioc.ParamInfo, paramIndex int, conversionTable ioc.ConversionTable) (string, errors.SdkError) {
	convertedValue, err := convertArgument(argument, paramInfo, paramIndex, conversionTable, false)
	if err != nil {
		return "", err
	}

	if paramInfo == nil {
		return FormatValueUsingNumberDecimalPlaces(convertedValue), nil
	}

	paramTypeInfo, paramTypeInfoFound := generated.GetParamTypeInfo(string(paramInfo.ParamType))
	if !paramTypeInfoFound {
		return "", errors.ErrInternal(fmt.Sprintf("On formatting argument, unknown argument type '%s' received from database. Please update the library.", paramInfo.ParamType))
	}

	roundedValue := utils.RoundNumber(convertedValue, paramTypeInfo.DecimalPlaces)

	if err := checkInvalidConversion(argument, roundedValue); err != nil {
		return "", err
	}

	formatString := fmt.Sprintf("%%.%df", paramTypeInfo.DecimalPlaces)
	return fmt.Sprintf(formatString, roundedValue), nil
}

func checkHasEqualNumberArgs(command []string, arguments []CommandArg) errors.SdkError {
	argumentCount := 0
	for _, cmdPart := range command {
		if cmdPart == constants.CommandArgument {
			argumentCount++
		}
	}

	if argumentCount != len(arguments) {
		errorMessage := fmt.Sprintf(
			"Number of arguments provided (%d) does not match number of arguments in command template %v.",
			len(arguments), command,
		)

		return errors.ErrInvalidArgument(errorMessage)
	}

	return nil
}

func AreAllArgumentsNative(arguments []CommandArg) bool {
	for _, argument := range arguments {
		if argument.GetUnit() != constants.NativeUnit {
			return false
		}
	}
	return true
}

func FormatValueUsingNumberDecimalPlaces(value float64) string {
	valueString := fmt.Sprintf("%.12f", value)
	valueString = strings.TrimRight(valueString, "0")
	valueString = strings.TrimRight(valueString, ".")
	return valueString
}

func formatArgumentsUsingNumberDecimalPlaces(arguments []CommandArg) []string {
	formattedArguments := make([]string, len(arguments))
	for i, argument := range arguments {
		formattedArguments[i] = FormatValueUsingNumberDecimalPlaces(argument.GetValue())
	}
	return formattedArguments
}

func BuildCommand(commandTemplate []string, arguments []CommandArg, optionalCommandIssuingInfo *ioc.CommandIssuingInfo) (string, errors.SdkError) {
	if err := checkHasEqualNumberArgs(commandTemplate, arguments); err != nil {
		return "", err
	}

	var formattedArguments []string
	if optionalCommandIssuingInfo != nil {
		paramsInfo, err := getParamsInfo(commandTemplate, arguments, optionalCommandIssuingInfo)
		if err != nil {
			return "", err
		}

		formattedArguments = make([]string, len(arguments))
		for i, argument := range arguments {
			if formattedArgument, err := convertAndFormatArgument(argument, paramsInfo[i], i, optionalCommandIssuingInfo.ConversionTable); err != nil {
				return "", err
			} else {
				formattedArguments[i] = formattedArgument
			}
		}
	} else {
		if !AreAllArgumentsNative(arguments) {
			return "", errors.ErrNotSupported("Unit conversion information is not available")
		}

		formattedArguments = formatArgumentsUsingNumberDecimalPlaces(arguments)
	}

	builtCommandParts := replaceParametersWithArguments(commandTemplate, formattedArguments)
	builtCommand := strings.Join(builtCommandParts, " ")

	return builtCommand, nil
}

func ConvertUnitsForCommandArg(commandTemplate []string, commandIssuingInfo *ioc.CommandIssuingInfo, argIndex int, value float64, unit string, convertBack bool) (float64, errors.SdkError) {
	paramsInfo, err := getParamsInfo(commandTemplate, nil, commandIssuingInfo)
	if err != nil {
		return 0, err
	}

	if argIndex >= len(paramsInfo) {
		return 0, errors.ErrInvalidArgument(fmt.Sprintf("Argument index %d is out of range for command template %v.", argIndex, commandTemplate))
	}

	var arg CommandArg = CommandArgImpl{
		Value: value,
		Unit:  unit,
	}

	return convertArgument(arg, paramsInfo[argIndex], argIndex, commandIssuingInfo.ConversionTable, convertBack)
}

func ConvertUnitSetting(commandIssuingInfo *ioc.CommandIssuingInfo, setting string, value float64, unit string, convertBack bool) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	}

	settingInfo, err := GetSettingInfo(commandIssuingInfo, setting)
	if err != nil {
		return 0, err
	}

	contextualDimensionID := settingInfo.ContextualDimensionID
	if convertBack {
		return commandIssuingInfo.ConversionTable.ConvertBack(value, unit, contextualDimensionID)
	}
	return commandIssuingInfo.ConversionTable.Convert(value, unit, contextualDimensionID)
}

func GetSettingInfo(commandIssuingInfo *ioc.CommandIssuingInfo, setting string) (*ioc.ParamInfo, errors.SdkError) {
	settingInfo, found := commandIssuingInfo.SettingsTable.GetParamInfo(setting)
	if !found {
		return nil, errors.ErrConversionFailed(fmt.Sprintf("Conversion information not found for setting \"%s\".", setting))
	} else if settingInfo.ContextualDimensionID == 0 {
		return nil, errors.ErrConversionFailed(fmt.Sprintf("Conversion not available for setting \"%s\".", setting))
	}
	return settingInfo, nil
}

func ConvertToBaseUnits(
	commandIssuingInfo *ioc.CommandIssuingInfo, measurement CommandArg, dimension ioc.ContextualDimensionID,
) (*CommandArgImpl, errors.SdkError) {
	table := commandIssuingInfo.ConversionTable
	unitsService := table.GetUnits()

	baseUnits, err := table.GetBaseUnits(dimension)
	if err != nil {
		return nil, err
	}

	var value float64
	if measurement.GetUnit() == constants.NativeUnit {
		baseValue, err := table.ConvertBack(measurement.GetValue(), baseUnits, dimension)
		if err != nil {
			return nil, err
		}
		value = baseValue
	} else if measurement.GetUnit() == baseUnits {
		value = measurement.GetValue()
	} else {
		if err := unitsService.AreUnitsCompatible(measurement.GetUnit(), baseUnits); err != nil {
			return nil, err
		}
		baseValue, err := unitsService.ConvertStaticUnitToDimensionBase(measurement.GetValue(), measurement.GetUnit())
		if err != nil {
			return nil, err
		}
		value = baseValue
	}

	return &CommandArgImpl{
		Value: value,
		Unit:  baseUnits,
	}, nil
}
