package commandbuilding

type CommandArg interface {
	GetValue() float64
	GetUnit() string
}
type CommandArgImpl struct {
	Value float64
	Unit  string
}

func (commandArg CommandArgImpl) GetValue() float64 {
	return commandArg.Value
}
func (commandArg CommandArgImpl) GetUnit() string {
	return commandArg.Unit
}
