package devices

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *streamManager) registerStreamBufferCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_buffer_get_content", func() pb.Message {
		return &pb.StreamBufferGetContentRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.bufferGetContent(request.(*pb.StreamBufferGetContentRequest))
	})
	gateway.RegisterCallback("device/stream_buffer_erase", func() pb.Message {
		return &pb.StreamBufferEraseRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.bufferErase(request.(*pb.StreamBufferEraseRequest))
	})
	gateway.RegisterCallback("device/stream_buffer_list", func() pb.Message {
		return &pb.StreamBufferList{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.bufferList(request.(*pb.StreamBufferList))
	})
}

func (manager *streamManager) bufferGetContent(request *pb.StreamBufferGetContentRequest) (*pb.StreamBufferGetContentResponse, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	commandString := fmt.Sprintf("%s buffer %d print", getStreamPrefix(request), request.GetBufferId())
	command := c.Command{
		Device:  int(request.GetDevice()),
		Axis:    0,
		Command: commandString,
	}
	replies, err := commands.SingleCommandMultiResponse(requests, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return &pb.StreamBufferGetContentResponse{}, nil
		}
		return nil, err
	}

	streamBufferLines := []string{}
	for _, infoResponse := range replies[1:] {
		streamBufferLines = append(streamBufferLines, infoResponse.Data)
	}

	return &pb.StreamBufferGetContentResponse{
		BufferLines: streamBufferLines,
	}, nil
}

func (manager *streamManager) bufferErase(request *pb.StreamBufferEraseRequest) errors.SdkError {
	command := fmt.Sprintf("%s buffer %d erase", getStreamPrefix(request), request.GetBufferId())
	_, err := manager.deviceManager.singleRequestDevice(request, command)
	if cmdErr, ok := err.(commands.CommandFailedErr); ok && commands.IsBusyErr(err) {
		return commands.NewErrFromExisting("Stream buffer is busy. Disable the stream first.", cmdErr)
	}
	return err
}

func (manager *streamManager) bufferList(request *pb.StreamBufferList) (*pb.IntArrayResponse, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	var command string
	if request.Pvt {
		command = "pvt buffer list"
	} else {
		command = "stream buffer list"
	}
	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: command,
	})
	if err != nil {
		return nil, err
	}

	regexBuffer := regexp.MustCompile(`(\d+)$`)
	var ids []int32
	for _, reply := range replies {
		if strings.HasPrefix(reply.Data, "buffer") {
			id, _ := strconv.Atoi(regexBuffer.FindString(reply.Data))
			ids = append(ids, int32(id))
		}
	}

	return &pb.IntArrayResponse{Values: ids}, nil
}
