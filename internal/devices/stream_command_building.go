package devices

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *streamManager) convertAxisStreamArgsToNative(
	request streamTarget, stream *stream, arguments []streamArg,
) ([]commandbuilding.CommandArg, errors.SdkError) {
	deviceManager := manager.deviceManager

	var convertedArgs []commandbuilding.CommandArg
	for _, arg := range arguments {
		if isDevice := arg.GetAxisIndex() < 0; isDevice {
			// device arguments are not converted
			convertedArgs = append(convertedArgs, arg)
			continue
		}

		conversionAxisNumber, err := stream.getUnitConversionAxis(arg.GetAxisIndex())
		if err != nil {
			return nil, err
		}

		axisToUseForConversion := &axisTargetImpl{
			interfaceID: request.GetInterfaceId(),
			device:      request.GetDevice(),
			axis:        conversionAxisNumber,
		}

		// setting table is used because there is no conversion
		// information in the command tree since streams are device
		// scope command and conversion info exists on peripherals
		nativeValue, err := deviceManager.ConvertUnitSetting(axisToUseForConversion, arg.GetSettingName(), arg.GetValue(), arg.GetUnit(), false)
		if err != nil {
			return nil, err
		}

		convertedArgs = append(convertedArgs, commandbuilding.CommandArgImpl{
			Value: nativeValue,
			Unit:  constants.NativeUnit,
		})
	}

	return convertedArgs, nil
}

func checkIfContainsNonNativeUnits(args []streamArg) bool {
	for _, arg := range args {
		if arg.GetUnit() != constants.NativeUnit {
			return true
		}
	}

	return false
}

func (manager *streamManager) buildStreamCommand(
	request streamTarget, stream *stream, commandTemplate []string, arguments []streamArg,
) (streamCommand string, err errors.SdkError) {
	var commandArguments []commandbuilding.CommandArg
	if needsConvert := checkIfContainsNonNativeUnits(arguments); needsConvert {
		if stream.mode == StreamModeStoreArbitrary {
			message := strings.Join([]string{
				fmt.Sprintf("Command failed because it requires unit conversion, and the %s was setup with arbitrary axes.", getStreamTextWord(request)),
				fmt.Sprintf("Setup %s with specific axes.", getStreamTextWord(request)),
			}, " ")
			if request.GetPvt() {
				return "", errors.ErrPvtMode(message)
			} else {
				return "", errors.ErrStreamMode(message)
			}
		}

		commandArguments, err = manager.convertAxisStreamArgsToNative(request, stream, arguments)
		if err != nil {
			return "", err
		}
	} else {
		for _, arg := range arguments {
			commandArguments = append(commandArguments, arg)
		}
	}

	streamPrefix := []string{getStreamPrefix(request), "1"}
	commandTemplate = append(streamPrefix[:], commandTemplate...)

	deviceTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
	}

	deviceCommand, err := manager.deviceManager.buildCommand(deviceTarget, commandTemplate, commandArguments)
	if err != nil {
		return "", err
	}

	streamCommand = strings.TrimPrefix(deviceCommand, strings.Join(streamPrefix, " ")+" ")
	return streamCommand, nil
}

func (manager *streamManager) getStreamCommandIssuingInfo(request streamTarget) ([]*ioc.CommandIssuingInfo, errors.SdkError) {
	deviceManager := manager.deviceManager

	stream, err := manager.getStreamSync(request)
	if err != nil {
		return nil, err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err = stream.assureMode(StreamModeLive, StreamModeStore); err != nil {
		return nil, err
	}

	var infos []*ioc.CommandIssuingInfo
	for _, axis := range stream.axes {
		axisToUseForConversion := &axisTargetImpl{
			interfaceID: request.GetInterfaceId(),
			device:      request.GetDevice(),
			axis:        axis.primaryAxisNumber,
		}

		device, err := deviceManager.getDeviceInfo(request)
		if err != nil {
			return nil, err
		}

		info, err := device.GetCommandIssuingInfo(axisToUseForConversion)
		if err != nil {
			return nil, err
		}

		infos = append(infos, info)
	}

	return infos, nil
}
