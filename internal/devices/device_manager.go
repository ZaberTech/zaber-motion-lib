package devices

import (
	"sync"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

type interfaceInfo struct {
	communication ioc.ASCIICommunicationInterface
	devices       map[int]*deviceInfo
	lock          sync.Mutex
}

type deviceManager struct {
	gateway                ioc.GatewayManager
	interfaces             ioc.InterfaceManager
	lock                   sync.Mutex
	interfaceInfo          map[ioc.ASCIICommunicationInterface]*interfaceInfo
	units                  ioc.Units
	deviceDb               ioc.DeviceDb
	streamManager          *streamManager
	saveStateManager       *saveStateManager
	ipcManager             *processControllerManager
	storage                ioc.StorageManager
	oscilloscope           *oscilloscopeManager
	independentAxesManager *independentAxesManager
	triggers               *triggersManager
}

func NewDeviceManager(gateway ioc.GatewayManager, interfaces ioc.InterfaceManager, deviceDb ioc.DeviceDb, units ioc.Units) ioc.DeviceManager {
	manager := &deviceManager{
		gateway:       gateway,
		interfaces:    interfaces,
		interfaceInfo: make(map[ioc.ASCIICommunicationInterface]*interfaceInfo),
		units:         units,
		deviceDb:      deviceDb,
	}

	manager.streamManager = newStreamManager(manager)
	manager.saveStateManager = newSaveStateManager(manager)
	manager.ipcManager = newProcessControllerManager(manager)
	manager.oscilloscope = newOscilloscopeManager(manager)
	manager.independentAxesManager = newIndependentAxesManager(manager)
	manager.triggers = newTriggersManager(manager)

	interfaces.OnInterfaceRemoved(func(communication ioc.CommunicationInterface) {
		manager.onInterfaceRemoved(communication)
	})
	manager.register()
	return manager
}

func (manager *deviceManager) SetStorageManager(storage ioc.StorageManager) {
	manager.storage = storage
}

func (manager *deviceManager) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("device/get_all_digital_io", func() pb.Message {
		return &pb.DeviceGetAllDigitalIORequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAllDigitalIO(request.(*pb.DeviceGetAllDigitalIORequest))
	})
	gateway.RegisterCallback("device/get_all_analog_io", func() pb.Message {
		return &pb.DeviceGetAllAnalogIORequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAllAnalogIO(request.(*pb.DeviceGetAllAnalogIORequest))
	})
	gateway.RegisterCallback("device/get_digital_io", func() pb.Message {
		return &pb.DeviceGetDigitalIORequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getDigitalIO(request.(*pb.DeviceGetDigitalIORequest))
	})
	gateway.RegisterCallback("device/get_analog_io", func() pb.Message {
		return &pb.DeviceGetAnalogIORequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAnalogIO(request.(*pb.DeviceGetAnalogIORequest))
	})
	gateway.RegisterCallback("device/set_all_digital_outputs", func() pb.Message {
		return &pb.DeviceSetAllDigitalOutputsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllDigitalOutputs(request.(*pb.DeviceSetAllDigitalOutputsRequest))
	})
	gateway.RegisterCallback("device/set_all_digital_outputs_schedule", func() pb.Message {
		return &pb.DeviceSetAllDigitalOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllDigitalOutputsSchedule(request.(*pb.DeviceSetAllDigitalOutputsScheduleRequest))
	})
	gateway.RegisterCallback("device/cancel_all_outputs_schedule", func() pb.Message {
		return &pb.DeviceCancelAllOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.cancelAllOutputsSchedule(request.(*pb.DeviceCancelAllOutputsScheduleRequest))
	})
	gateway.RegisterCallback("device/set_all_analog_outputs", func() pb.Message {
		return &pb.DeviceSetAllAnalogOutputsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllAnalogOutputs(request.(*pb.DeviceSetAllAnalogOutputsRequest))
	})
	gateway.RegisterCallback("device/set_all_analog_outputs_schedule", func() pb.Message {
		return &pb.DeviceSetAllAnalogOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllAnalogOutputsSchedule(request.(*pb.DeviceSetAllAnalogOutputsScheduleRequest))
	})
	gateway.RegisterCallback("device/set_digital_output", func() pb.Message {
		return &pb.DeviceSetDigitalOutputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setDigitalOutput(request.(*pb.DeviceSetDigitalOutputRequest))
	})
	gateway.RegisterCallback("device/set_digital_output_schedule", func() pb.Message {
		return &pb.DeviceSetDigitalOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.SetDigitalOutputSchedule(request.(*pb.DeviceSetDigitalOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/cancel_output_schedule", func() pb.Message {
		return &pb.DeviceCancelOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.cancelOutputSchedule(request.(*pb.DeviceCancelOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/set_analog_output", func() pb.Message {
		return &pb.DeviceSetAnalogOutputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAnalogOutput(request.(*pb.DeviceSetAnalogOutputRequest))
	})
	gateway.RegisterCallback("device/set_analog_output_schedule", func() pb.Message {
		return &pb.DeviceSetAnalogOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAnalogOutputSchedule(request.(*pb.DeviceSetAnalogOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/get_io_info", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getNumberOfChannels(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("device/set_io_label", func() pb.Message {
		return &pb.SetIoPortLabelRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setIoPortLabel(request.(*pb.SetIoPortLabelRequest))
	})
	gateway.RegisterCallback("device/get_io_label", func() pb.Message {
		return &pb.GetIoPortLabelRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIoPortLabel(request.(*pb.GetIoPortLabelRequest))
	})
	gateway.RegisterCallback("device/get_all_io_labels", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAllIoPortLabels(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("device/set_lowpass_filter", func() pb.Message {
		return &pb.DeviceSetLowpassFilterRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setLowpassFilter(request.(*pb.DeviceSetLowpassFilterRequest))
	})
	gateway.RegisterCallback("device/renumber_all", func() pb.Message {
		return &pb.RenumberRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.renumberAll(request.(*pb.RenumberRequest))
	})
	gateway.RegisterCallback("device/detect", func() pb.Message {
		return &pb.DeviceDetectRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.detect(request.(*pb.DeviceDetectRequest))
	})
	gateway.RegisterCallback("device/forget", func() pb.Message {
		return &pb.ForgetDevicesRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.forgetDevices(request.(*pb.ForgetDevicesRequest))
	})
	gateway.RegisterCallback("device/identify", func() pb.Message {
		return &pb.DeviceIdentifyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.identifyDevice(request.(*pb.DeviceIdentifyRequest))
	})
	gateway.RegisterCallback("device/get_identity", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIdentity(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("device/get_is_identified", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIsIdentified(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("device/get_axis_identity", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAxisIdentity(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/driver_disable", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.driverDisable(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/driver_enable", func() pb.Message {
		return &pb.DriverEnableRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.driverEnableRequest(request.(*pb.DriverEnableRequest))
	})
	gateway.RegisterCallback("device/home", func() pb.Message {
		return &pb.DeviceHomeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.home(request.(*pb.DeviceHomeRequest))
	})
	gateway.RegisterCallback("device/home_all", func() pb.Message {
		return &pb.DeviceOnAllRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.homeAll(request.(*pb.DeviceOnAllRequest))
	})
	gateway.RegisterCallback("device/move", func() pb.Message {
		return &pb.DeviceMoveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.move(request.(*pb.DeviceMoveRequest))
	})
	gateway.RegisterCallback("device/move_sin", func() pb.Message {
		return &pb.DeviceMoveSinRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveSin(request.(*pb.DeviceMoveSinRequest))
	})
	gateway.RegisterCallback("device/move_sin_stop", func() pb.Message {
		return &pb.DeviceStopRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveSinStop(request.(*pb.DeviceStopRequest))
	})
	gateway.RegisterCallback("device/wait_until_idle", func() pb.Message {
		return &pb.DeviceWaitUntilIdleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitUntilIdleRequest(request.(*pb.DeviceWaitUntilIdleRequest))
	})
	gateway.RegisterCallback("device/is_busy", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isBusy(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/is_homed", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isHomed(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/stop", func() pb.Message {
		return &pb.DeviceStopRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.stop(request.(*pb.DeviceStopRequest))
	})
	gateway.RegisterCallback("device/stop_all", func() pb.Message {
		return &pb.DeviceOnAllRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.stopAll(request.(*pb.DeviceOnAllRequest))
	})
	gateway.RegisterCallback("device/get_index_position", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIndexPosition(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/get_index_count", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getNumberOfIndices(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/get_warnings", func() pb.Message {
		return &pb.DeviceGetWarningsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getWarningsRequest(request.(*pb.DeviceGetWarningsRequest))
	})
	gateway.RegisterCallback("device/wait_to_clear_warnings", func() pb.Message {
		return &pb.WaitToClearWarningsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitToClearWarnings(request.(*pb.WaitToClearWarningsRequest))
	})
	gateway.RegisterCallback("device/get_setting", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSetting(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/set_setting", func() pb.Message {
		return &pb.DeviceSetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setSetting(request.(*pb.DeviceSetSettingRequest))
	})
	gateway.RegisterCallback("device/get_setting_str", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSettingStr(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/set_setting_str", func() pb.Message {
		return &pb.DeviceSetSettingStrRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setSettingStr(request.(*pb.DeviceSetSettingStrRequest))
	})
	gateway.RegisterCallback("device/get_setting_default", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSettingDefault(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/get_setting_default_str", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSettingDefaultStr(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/get_setting_from_all_axes", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSettingFromAllAxes(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/get_many_settings", func() pb.Message {
		return &pb.DeviceMultiGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getManySettings(request.(*pb.DeviceMultiGetSettingRequest), false)
	})
	gateway.RegisterCallback("device/get_sync_settings", func() pb.Message {
		return &pb.DeviceMultiGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getManySettings(request.(*pb.DeviceMultiGetSettingRequest), true)
	})
	gateway.RegisterCallback("device/can_convert_setting", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.canConvertSetting(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("device/set_unit_conversions", func() pb.Message {
		return &pb.DeviceSetUnitConversionsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setUnitConversions(request.(*pb.DeviceSetUnitConversionsRequest))
	})
	gateway.RegisterCallback("device/device_to_string", func() pb.Message {
		return &pb.AxisToStringRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.deviceToString(request.(*pb.AxisToStringRequest))
	})
	gateway.RegisterCallback("device/all_axes_to_string", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.allAxesToString(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/axis_to_string", func() pb.Message {
		return &pb.AxisToStringRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.axisToString(request.(*pb.AxisToStringRequest))
	})
	gateway.RegisterCallback("device/prepare_command", func() pb.Message {
		return &pb.PrepareCommandRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.prepareCommand(request.(*pb.PrepareCommandRequest))
	})
	gateway.RegisterCallback("device/convert_setting", func() pb.Message {
		return &pb.DeviceConvertSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.convertSetting(request.(*pb.DeviceConvertSettingRequest))
	})
	gateway.RegisterCallback("device/is_parked", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isParked(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/park", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.park(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/unpark", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.unpark(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/wait_to_respond", func() pb.Message {
		return &pb.WaitToRespondRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitToRespond(request.(*pb.WaitToRespondRequest))
	})
	gateway.RegisterCallback("device/get_state", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.saveStateManager.getState(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/set_device_state", func() pb.Message {
		return &pb.SetStateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.saveStateManager.setDeviceState(request.(*pb.SetStateRequest))
	})
	gateway.RegisterCallback("device/set_axis_state", func() pb.Message {
		return &pb.SetStateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.saveStateManager.setPeripheralState(request.(*pb.SetStateRequest))
	})
	gateway.RegisterCallback("device/can_set_state", func() pb.Message {
		return &pb.CanSetStateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		canSetStateRequest := request.(*pb.CanSetStateRequest)
		return manager.saveStateManager.canSetState(canSetStateRequest, canSetStateRequest.FirmwareVersion), nil
	})
	gateway.RegisterCallback("device/can_set_axis_state", func() pb.Message {
		return &pb.CanSetStateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		canSetStateRequest := request.(*pb.CanSetStateRequest)
		return manager.saveStateManager.canSetAxisState(canSetStateRequest, canSetStateRequest.FirmwareVersion), nil
	})
	gateway.RegisterCallback("device/activate", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.activate(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/renumber", func() pb.Message {
		return &pb.RenumberRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.renumberDevice(request.(*pb.RenumberRequest))
	})
	gateway.RegisterCallback("process_controller/verify", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.ipcManager.errIfNotDeviceProcessController(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("process-controller/enable", func() pb.Message {
		return &pb.ProcessOn{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.ipcManager.enableProcess(request.(*pb.ProcessOn))
	})
	gateway.RegisterCallback("process-controller/on", func() pb.Message {
		return &pb.ProcessOn{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.ipcManager.powerProcess(request.(*pb.ProcessOn))
	})
	gateway.RegisterCallback("process_controller/get_input", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.ipcManager.getInput(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("process_controller/get_source", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.ipcManager.getSource(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("process_controller/set_source", func() pb.Message {
		return &pb.SetProcessControllerSource{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.ipcManager.setSource(request.(*pb.SetProcessControllerSource))
	})
	gateway.RegisterCallback("process_controller/bridge", func() pb.Message {
		return &pb.ProcessOn{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.ipcManager.bridge(request.(*pb.ProcessOn))
	})
	gateway.RegisterCallback("process_controller/is_bridge", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.ipcManager.isBridge(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("device/restore", func() pb.Message {
		return &pb.DeviceRestoreRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.restoreDeviceAxis(request.(*pb.DeviceRestoreRequest))
	})

	manager.registerLockstepCallbacks()
	manager.oscilloscope.registerCallbacks(gateway)
	manager.streamManager.registerStreamCallbacks(gateway)
	manager.independentAxesManager.registerCallbacks(gateway)
	manager.triggers.registerCallbacks(gateway)
}

func (manager *deviceManager) getInterface(request interfaceTarget) (ioc.ASCIICommunicationInterface, errors.SdkError) {
	communication, err := manager.interfaces.GetInterface(request.GetInterfaceId())
	if err != nil {
		return nil, err
	}

	asciiComm, castOk := communication.(ioc.ASCIICommunicationInterface)
	if !castOk {
		return nil, errors.ErrInternal("The ASCII protocol device manager was given a Binary interface. This is a bug.")
	}

	return asciiComm, nil
}

func (manager *deviceManager) getRequests(request interfaceTarget) (*c.RequestManager, errors.SdkError) {
	communication, err := manager.getInterface(request)
	if err != nil {
		return nil, err
	}

	return communication.Requests(), nil
}

func (manager *deviceManager) GetRequests(target ioc.InterfaceTarget) (*c.RequestManager, errors.SdkError) {
	return manager.getRequests(target)
}

func (manager *deviceManager) GetInterface(target ioc.InterfaceTarget) (ioc.ASCIICommunicationInterface, errors.SdkError) {
	return manager.getInterface(target)
}

func (manager *deviceManager) getInterfaceInfo(request interfaceTarget) (*interfaceInfo, errors.SdkError) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	communication, err := manager.getInterface(request)
	if err != nil {
		return nil, err
	}

	info, ok := manager.interfaceInfo[communication]
	if !ok {
		info = &interfaceInfo{
			communication: communication,
			devices:       make(map[int]*deviceInfo),
		}
		manager.interfaceInfo[communication] = info
	}

	return info, nil
}

func (manager *deviceManager) getDeviceInfoBase(request deviceTarget, fresh bool) (*deviceInfo, errors.SdkError) {
	info, err := manager.getInterfaceInfo(request)
	if err != nil {
		return nil, err
	}

	return info.getDeviceInfo((int)(request.GetDevice()), fresh), nil
}

func (info *interfaceInfo) getDeviceInfo(address int, fresh bool) *deviceInfo {
	info.lock.Lock()
	defer info.lock.Unlock()

	device, ok := info.devices[address]
	if !ok || fresh {
		device = &deviceInfo{
			number:        address,
			interfaceInfo: info,
		}
		info.devices[address] = device
	}

	return device
}

func (info *interfaceInfo) removeDeviceInfo(address int) {
	info.lock.Lock()
	defer info.lock.Unlock()

	delete(info.devices, address)
}

func (manager *deviceManager) getAllDeviceInfos(request interfaceTarget) ([]*deviceInfo, errors.SdkError) {
	info, err := manager.getInterfaceInfo(request)
	if err != nil {
		return nil, err
	}

	info.lock.Lock()
	defer info.lock.Unlock()

	return pie.Values(info.devices), nil
}

func (manager *deviceManager) GetDeviceInfo(request deviceTarget) (ioc.DeviceInfo, errors.SdkError) {
	return manager.getDeviceInfo(request)
}

func (manager *deviceManager) getDeviceInfo(request deviceTarget) (*deviceInfo, errors.SdkError) {
	return manager.getDeviceInfoBase(request, false)
}

func (manager *deviceManager) GetUnidentifiedDeviceInfo(request deviceTarget) (ioc.UnidentifiedDeviceInfo, errors.SdkError) {
	info, err := manager.getDeviceInfoBase(request, false)
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (manager *deviceManager) GetIdentifiedDeviceInfo(request deviceTarget) (ioc.DeviceInfo, errors.SdkError) {
	info, err := manager.getDeviceInfoBase(request, false)
	if err != nil {
		return nil, err
	}
	if err = info.VerifyIdentified(); err != nil {
		return nil, err
	}

	return info, nil
}

func (manager *deviceManager) GetAllIdentifiedDevices(request interfaceTarget) ([]ioc.DeviceInfo, errors.SdkError) {
	devices, err := manager.getAllDeviceInfos(request)
	if err != nil {
		return nil, err
	}
	devices = pie.Filter(devices, func(device *deviceInfo) bool {
		return device.isIdentified()
	})

	return pie.Map(devices, func(device *deviceInfo) ioc.DeviceInfo {
		return device
	}), nil
}

func (manager *deviceManager) clearAllDeviceInfo(request interfaceTarget, exceptDevices []int32) errors.SdkError {
	info, err := manager.getInterfaceInfo(request)
	if err != nil {
		return err
	}

	info.lock.Lock()
	defer info.lock.Unlock()

	clearedDevices := info.devices
	info.devices = make(map[int]*deviceInfo)

	for _, address := range exceptDevices {
		if device, ok := clearedDevices[(int)(address)]; ok {
			info.devices[device.number] = device
		}
	}

	return nil
}

func (manager *deviceManager) onInterfaceRemoved(communication ioc.CommunicationInterface) {
	asciiCommunication, ok := communication.(ioc.ASCIICommunicationInterface)
	if !ok {
		return
	}

	manager.lock.Lock()
	defer manager.lock.Unlock()

	delete(manager.interfaceInfo, asciiCommunication)
}

func (manager *deviceManager) GetStreamCommandIssuingInfo(request streamTarget) ([]*ioc.CommandIssuingInfo, errors.SdkError) {
	return manager.streamManager.getStreamCommandIssuingInfo(request)
}

func (manager *deviceManager) StreamCommands(stream streamTarget, commands []string) errors.SdkError {
	return manager.streamManager.streamActionsBasic(stream, commands, streamActionOptions{
		resetBuffer: true,
	})
}

func (manager *deviceManager) StreamWaitUntilIdle(stream streamTarget) errors.SdkError {
	return manager.streamManager.waitUntilIdle(&pb.StreamWaitUntilIdleRequest{
		InterfaceId:       stream.GetInterfaceId(),
		Device:            stream.GetDevice(),
		StreamId:          stream.GetStreamId(),
		ThrowErrorOnFault: true,
	})
}

func (manager *deviceManager) GetStreamAxisSetting(target streamTarget, setting string, checkIdle bool) ([]float64, errors.SdkError) {
	stream, err := manager.streamManager.getStream(target, true)
	if err != nil {
		return nil, err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	return manager.streamManager.getStreamAxisSetting(stream, target, setting, checkIdle)
}

func (manager *deviceManager) GetStreamMode(target streamTarget) (pb.AsciiStreamMode, errors.SdkError) {
	mode, err := manager.streamManager.getMode(target)
	return mode.DtoMode(), err
}
