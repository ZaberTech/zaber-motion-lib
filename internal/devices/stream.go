package devices

import (
	"fmt"
	"sync"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"
)

type StreamMode int

const (
	StreamModeDisabled StreamMode = iota
	StreamModeStore
	StreamModeStoreArbitrary
	StreamModeLive
)

func (s StreamMode) String() string {
	return [...]string{"Disabled", "Store", "StoreArbitrary", "Live"}[s]
}

var asciiStreamModeMap = map[StreamMode]pb.AsciiStreamMode{
	StreamModeDisabled:       pb.AsciiStreamMode_DISABLED,
	StreamModeLive:           pb.AsciiStreamMode_LIVE,
	StreamModeStore:          pb.AsciiStreamMode_STORE,
	StreamModeStoreArbitrary: pb.AsciiStreamMode_STORE_ARBITRARY_AXES,
}

func (s StreamMode) DtoMode() pb.AsciiStreamMode {
	return asciiStreamModeMap[s]
}

var pvtStreamModeMap = map[StreamMode]pb.AsciiPvtMode{
	StreamModeDisabled:       pb.AsciiPvtMode_DISABLED,
	StreamModeLive:           pb.AsciiPvtMode_LIVE,
	StreamModeStore:          pb.AsciiPvtMode_STORE,
	StreamModeStoreArbitrary: pb.AsciiPvtMode_DISABLED,
}

func (s StreamMode) DtoPvtMode() pb.AsciiPvtMode {
	return pvtStreamModeMap[s]
}

type streamAxis struct {
	streamAxisNumber  int32
	primaryAxisNumber int32
	axisType          pb.AsciiStreamAxisType
}

type streamData struct {
	mode                StreamMode
	axesCount           int
	axes                []*streamAxis
	physicalAxesNumbers []int32
	bufferID            int
}

type streamRuntimeData struct {
	discontinuitiesAsError     bool
	ignoreCurrentDiscontinuity bool
}

type stream struct {
	lock     sync.Mutex
	dataLock sync.Mutex
	streamID int
	pvt      bool
	// setup data
	streamData
	// data that changes when the stream is active
	runtime streamRuntimeData
	// data specific to PVT streams
	pvtData *pvtStream
}

func (stream *stream) checkAxisIndex(streamAxisIndex int32) errors.SdkError {
	if streamAxisIndex < 0 || int(streamAxisIndex) >= stream.axesCount {
		errorStr := fmt.Sprintf("The stream has no axis with index %d. Axis indices must be greater than or equal to 0 and less than the number of axes in the stream (%d).", streamAxisIndex, stream.axesCount)
		return errors.ErrInvalidArgument(errorStr)
	}

	return nil
}

func (stream *stream) getUnitConversionAxis(streamAxisIndex int32) (int32, errors.SdkError) {
	if err := stream.checkAxisIndex(streamAxisIndex); err != nil {
		return 0, err
	}
	return stream.axes[streamAxisIndex].primaryAxisNumber, nil
}

func (stream *stream) assureMode(permissableModes ...StreamMode) errors.SdkError {
	for _, permissableMode := range permissableModes {
		if stream.mode == permissableMode {
			return nil
		}
	}

	streamWord := getStreamTextWord(stream)
	var message string
	if stream.mode == StreamModeDisabled {
		message = fmt.Sprintf("The %s is disabled. Set up the %s in one of the permissable mode(s) for this action, those being: %v.", streamWord, streamWord, permissableModes)
	} else if len(permissableModes) == 1 && permissableModes[0] == StreamModeDisabled {
		message = fmt.Sprintf("The %s is already setup. Disable the %s first.", streamWord, streamWord)
	} else {
		message = fmt.Sprintf("The %s is not setup in the right mode. Permissible mode(s) for action: %v. Current mode: %v.", streamWord, permissableModes, StreamMode(stream.mode))
	}

	if stream.pvt {
		return errors.ErrPvtMode(message)
	} else {
		return errors.ErrStreamMode(message)
	}
}

func (stream *stream) GetPvt() bool {
	return stream.pvt
}

type pvtTarget interface {
	GetPvt() bool
}

func getStreamPrefix(target pvtTarget) string {
	return getStreamPrefixBase(target.GetPvt())
}

func getStreamPrefixBase(pvt bool) string {
	if pvt {
		return "pvt"
	} else {
		return "stream"
	}
}

func getStreamTextWordEx(target pvtTarget, upper bool) string {
	if target.GetPvt() {
		if upper {
			return "PVT Sequence"
		} else {
			return "PVT sequence"
		}
	} else {
		if upper {
			return "Stream"
		} else {
			return "stream"
		}
	}
}
func getStreamTextWord(target pvtTarget) string {
	return getStreamTextWordEx(target, false)
}

type pvtBufferTarget interface {
	GetPvt() bool
	GetPvtBuffer() int32
	GetStreamBuffer() int32
}

func getBufferID(target pvtBufferTarget) int {
	if target.GetPvt() {
		return int(target.GetPvtBuffer())
	} else {
		return int(target.GetStreamBuffer())
	}
}

func streamAxisDefinitionToPvt(axis *pb.AsciiStreamAxisDefinition) *pb.AsciiPvtAxisDefinition {
	pvtAxis := &pb.AsciiPvtAxisDefinition{
		AxisNumber: axis.AxisNumber,
		AxisType:   utils.CastEnumPointer[pb.AsciiStreamAxisType, pb.AsciiPvtAxisType](axis.AxisType),
	}
	return pvtAxis
}

func pvtAxisDefinitionToStream(axis *pb.AsciiPvtAxisDefinition) *pb.AsciiStreamAxisDefinition {
	streamAxis := &pb.AsciiStreamAxisDefinition{
		AxisNumber: axis.AxisNumber,
		AxisType:   utils.CastEnumPointer[pb.AsciiPvtAxisType, pb.AsciiStreamAxisType](axis.AxisType),
	}
	return streamAxis
}
