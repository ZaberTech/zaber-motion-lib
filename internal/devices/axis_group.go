package devices

import (
	"fmt"
	"strings"
	"sync"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type independentAxesManager struct {
	deviceManager *deviceManager
}

func newIndependentAxesManager(deviceManager *deviceManager) *independentAxesManager {
	return &independentAxesManager{deviceManager}
}

func (manager *independentAxesManager) registerCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("axes/home", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveCommand(request.(*pb.AxesEmptyRequest), "home")
	})
	gateway.RegisterCallback("axes/stop", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveCommand(request.(*pb.AxesEmptyRequest), "stop")
	})
	gateway.RegisterCallback("axes/move_absolute", func() pb.Message {
		return &pb.AxesMoveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.move(request.(*pb.AxesMoveRequest), pb.AxisMoveType_ABS)
	})
	gateway.RegisterCallback("axes/move_relative", func() pb.Message {
		return &pb.AxesMoveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.move(request.(*pb.AxesMoveRequest), pb.AxisMoveType_REL)
	})
	gateway.RegisterCallback("axes/move_min", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveCommand(request.(*pb.AxesEmptyRequest), "move min")
	})
	gateway.RegisterCallback("axes/move_max", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.moveCommand(request.(*pb.AxesEmptyRequest), "move max")
	})
	gateway.RegisterCallback("axes/wait_until_idle", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitUntilIdle(request.(*pb.AxesEmptyRequest))
	})
	gateway.RegisterCallback("axes/is_homed", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isHomed(request.(*pb.AxesEmptyRequest))
	})
	gateway.RegisterCallback("axes/is_busy", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isBusy(request.(*pb.AxesEmptyRequest))
	})
	gateway.RegisterCallback("axes/get_setting", func() pb.Message {
		return &pb.AxesGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getSetting(request.(*pb.AxesGetSettingRequest))
	})
	gateway.RegisterCallback("axes/to_string", func() pb.Message {
		return &pb.AxesEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.toString(request.(*pb.AxesEmptyRequest))
	})
}

func (manager *independentAxesManager) moveCommand(request *pb.AxesEmptyRequest, move string) errors.SdkError {
	return manager.request(request, func(_ int, interfaceId, device, axis int32) errors.SdkError {
		return manager.deviceManager.moveCommand(&pb.DeviceMoveRequest{
			InterfaceId:   interfaceId,
			Device:        device,
			Axis:          axis,
			WaitUntilIdle: true,
		}, move)
	})
}

func (manager *independentAxesManager) move(request *pb.AxesMoveRequest, move pb.AxisMoveType) errors.SdkError {
	if len(request.Interfaces) != len(request.Position) {
		return errors.ErrInvalidArgument("You must specify a position for each axis in the group.")
	}

	return manager.request(request, func(i int, interfaceId, device, axis int32) errors.SdkError {
		return manager.deviceManager.move(&pb.DeviceMoveRequest{
			InterfaceId:   interfaceId,
			Device:        device,
			Axis:          axis,
			WaitUntilIdle: true,
			Type:          move,
			Arg:           request.Position[i].Value,
			Unit:          request.Position[i].Unit,
		})
	})
}

type AxisGroupTarget interface {
	GetInterfaces() []int32
	GetDevices() []int32
	GetAxes() []int32
}

func (manager *independentAxesManager) request(target AxisGroupTarget, requestFunc func(index int, interfaceId, device, axis int32) errors.SdkError) errors.SdkError {
	count := len(target.GetInterfaces())
	results := make(chan errors.SdkError, count)

	for i := range target.GetInterfaces() {
		i := i
		go func() {
			results <- requestFunc(i, target.GetInterfaces()[i], target.GetDevices()[i], target.GetAxes()[i])
		}()
	}

	for i := 0; i < count; i++ {
		err := <-results
		if err != nil {
			return err
		}
	}

	return nil
}

func (manager *independentAxesManager) waitUntilIdle(request *pb.AxesEmptyRequest) errors.SdkError {
	return manager.request(request, func(_ int, interfaceId, device, axis int32) errors.SdkError {
		return manager.deviceManager.waitUntilIdleRequest(&pb.DeviceWaitUntilIdleRequest{
			InterfaceId:       interfaceId,
			Device:            device,
			Axis:              axis,
			ThrowErrorOnFault: true,
		})
	})
}

func (manager *independentAxesManager) getSetting(request *pb.AxesGetSettingRequest) (*pb.DoubleArrayResponse, errors.SdkError) {
	count := len(request.Interfaces)
	if len(request.Unit) > 1 && len(request.Unit) != count {
		return nil, errors.ErrInvalidArgument(
			fmt.Sprintf("You must specify a units once or for each axis in the group (expected %d).", count))
	}

	result := &pb.DoubleArrayResponse{}
	for i := 0; i < count; i++ {
		unit := constants.NativeUnit
		if len(request.Unit) == 1 {
			unit = request.Unit[0]
		} else if len(request.Unit) > 1 {
			unit = request.Unit[i]
		}

		resp, err := manager.deviceManager.getSetting(&pb.DeviceGetSettingRequest{
			InterfaceId: request.GetInterfaces()[i],
			Device:      request.GetDevices()[i],
			Axis:        request.GetAxes()[i],
			Setting:     request.Setting,
			Unit:        unit,
		})
		if err != nil {
			return nil, err
		}

		result.Values = append(result.Values, resp.Value)
	}

	return result, nil
}

func (manager *independentAxesManager) toString(request *pb.AxesEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	count := len(request.Interfaces)

	var axesStr []string

	for i := 0; i < count; i++ {
		axisStr, err := manager.deviceManager.axisToString(&pb.AxisToStringRequest{
			InterfaceId: request.Interfaces[i],
			Device:      request.Devices[i],
			Axis:        request.Axes[i],
		})
		if err != nil {
			return nil, err
		}
		axesStr = append(axesStr, axisStr.Value)
	}

	return &pb.StringResponse{
		Value: "AxisGroup -> " + strings.Join(axesStr, ", "),
	}, nil
}

func (manager *independentAxesManager) isHomed(request *pb.AxesEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	isHomed := true
	var lock sync.Mutex

	err := manager.request(request, func(_ int, interfaceId, device, axis int32) errors.SdkError {
		result, err := manager.deviceManager.isHomed(&pb.AxisEmptyRequest{
			InterfaceId: interfaceId,
			Device:      device,
			Axis:        axis,
		})
		if err != nil {
			return err
		}

		lock.Lock()
		defer lock.Unlock()
		isHomed = isHomed && result.Value
		return nil
	})
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: isHomed,
	}, nil
}

func (manager *independentAxesManager) isBusy(request *pb.AxesEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	isBusy := false
	var lock sync.Mutex

	err := manager.request(request, func(_ int, interfaceId, device, axis int32) errors.SdkError {
		result, err := manager.deviceManager.isBusy(&pb.AxisEmptyRequest{
			InterfaceId: interfaceId,
			Device:      device,
			Axis:        axis,
		})
		if err != nil {
			return err
		}

		lock.Lock()
		defer lock.Unlock()
		isBusy = isBusy || result.Value
		return nil
	})
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: isBusy,
	}, nil
}
