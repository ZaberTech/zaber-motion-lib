package devices

import (
	"fmt"
	"math"
	"regexp"
	"strings"
	"time"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	"zaber-motion-lib/internal/communication"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

var MovementErrorWarningFlags, _ = regexp.Compile(`^(F\w|WL)$`)
var MovementInterruptedWarningFlags, _ = regexp.Compile(`^(NI|NC)$`)

func (manager *deviceManager) moveCommand(request waitUntilIdleTarget, move string) errors.SdkError {
	communication, err := manager.getInterface(request)
	if err != nil {
		return err
	}

	return MoveCommand(communication, request, move)
}

func MoveCommand(communication ioc.ASCIICommunicationInterface, request waitUntilIdleTarget, move string) errors.SdkError {
	moveReply, err := singleCommandAxis(communication.Requests(), request, move)
	if err != nil {
		return err
	}
	warnings, err := checkMoveReplyErrors(communication.Requests(), moveReply)
	if err != nil {
		return err
	}

	if !request.GetWaitUntilIdle() {
		return nil
	}

	return WaitForMovementToComplete(communication, request, warnings)
}

// MoveCommandTimeout is a version of MoveCommand that will stop the movement if it takes longer than the specified timeout.
func MoveCommandTimeout(
	communication ioc.ASCIICommunicationInterface, request waitUntilIdleTarget, move string, timeout time.Duration,
) (err errors.SdkError, timedOut bool) {
	moveReply, err := singleCommandAxis(communication.Requests(), request, move)
	if err != nil {
		return err, false
	}
	warnings, err := checkMoveReplyErrors(communication.Requests(), moveReply)
	if err != nil {
		return err, false
	}

	if !request.GetWaitUntilIdle() {
		return nil, false
	}

	result := make(chan errors.SdkError, 1)
	go func() {
		result <- WaitForMovementToComplete(communication, request, warnings)
	}()

	select {
	case err := <-result:
		return err, false
	case <-time.After(timeout):
		err := MoveCommand(communication, request, "stop")
		// We should not return until the polling finishes.
		if waitErr := <-result; waitErr != nil && waitErr.Type() != pb.Errors_MOVEMENT_INTERRUPTED {
			err = waitErr
		}
		return err, true
	}
}

// processReplyWithMobileAxes takes a reply, and checks it for errors, waiting until idle if specified
// returns true if it is a mobile axis, false if not
func processReplyWithMobileAxes(
	communication ioc.ASCIICommunicationInterface,
	command *c.Command,
	reply *c.Response,
	request *pb.DeviceOnAllRequest,
) (isMobile bool, err errors.SdkError) {
	if err := commands.CheckOk(reply, command); err != nil {
		if commands.IsBadCommandErr(err) {
			// This is a device with no mobile axes. Don't wait on it or add it to the list of moved devices.
			return false, nil
		}
		return true, err
	}

	warnings, err := checkMoveReplyErrors(communication.Requests(), reply)
	if err != nil {
		return true, err
	}

	if request.GetWaitUntilIdle() {
		axisTarget := &axisTargetImpl{axis: 0, device: int32(reply.Device), interfaceID: request.InterfaceId}
		if err := WaitForMovementToComplete(communication, axisTarget, warnings); err != nil {
			return true, err
		}
	}

	return true, nil
}

func (manager *deviceManager) moveAllCommand(request *pb.DeviceOnAllRequest, command string) (*pb.DeviceOnAllResponse, errors.SdkError) {
	communication, interfaceErr := manager.getInterface(request)
	if interfaceErr != nil {
		return nil, interfaceErr
	}
	requests := communication.Requests()

	commandEx := c.Command{Command: command}
	replies, commandErr := requests.Request(commandEx, &c.RequestOptions{CollectMultiple: true})
	if commandErr != nil {
		return nil, commandErr
	}

	deviceList := []int32{}
	var returnError errors.SdkError
	for _, reply := range replies {
		isMobile, checkErr := processReplyWithMobileAxes(communication, &commandEx, reply, request)
		if checkErr != nil {
			returnError = checkErr
		}
		if isMobile {
			deviceList = append(deviceList, int32(reply.Device))
		}
	}

	if returnError != nil {
		return nil, returnError
	}

	return &pb.DeviceOnAllResponse{DeviceAddresses: deviceList}, nil
}

func WaitForMovementToComplete(
	communication ioc.ASCIICommunicationInterface,
	target axisTarget,
	warnings *WarningsFlags,
) errors.SdkError {
	options := WaitUntilIdleOptions{
		CheckErrors:        true,
		CheckInterruption:  warnings.HasNot("NI"),
		CheckManualControl: true,
	}
	return WaitUntilIdle(communication, target, options)
}

func checkMoveReplyErrors(requests *communication.RequestManager, basedOn *communication.Response) (*WarningsFlags, errors.SdkError) {
	warnings, err := GetResponseWarnings(requests, basedOn, true)
	if err != nil {
		return warnings, err
	}
	movementErr := CheckErrorFlags(warnings, MovementErrorWarningFlags, errors.BindErrMovementFailed(int32(basedOn.Device), int32(basedOn.Axis)))
	return warnings, movementErr
}

func (manager *deviceManager) home(request *pb.DeviceHomeRequest) errors.SdkError {
	return manager.moveCommand(request, "home")
}

func (manager *deviceManager) move(request *pb.DeviceMoveRequest) errors.SdkError {
	moveType := strings.ToLower(pb.AxisMoveType_name[(request.Type)])
	var commandTemplate []string
	var args []commandbuilding.CommandArg

	requiresArgument := request.Type != pb.AxisMoveType_MIN && request.Type != pb.AxisMoveType_MAX
	if requiresArgument {
		commandTemplate = []string{"move", moveType, constants.CommandArgument}
		arg := request.Arg
		if request.Type == pb.AxisMoveType_INDEX {
			arg = float64(request.ArgInt)
		}
		args = append(args, commandbuilding.CommandArgImpl{Value: arg, Unit: request.Unit})
	} else {
		commandTemplate = []string{"move", moveType}
	}

	if request.Acceleration != 0 && request.Velocity == 0 && request.Type != pb.AxisMoveType_VEL {
		if maxspeedReply, err := manager.singleRequestAxis(request, "get maxspeed"); err != nil {
			return err
		} else if maxspeed, err := maxspeedReply.DataAsFloat(); err != nil {
			return err
		} else {
			request.Velocity = maxspeed
			request.VelocityUnit = constants.NativeUnit
		}
	}

	if request.Velocity != 0 {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		args = append(args, commandbuilding.CommandArgImpl{Value: request.Velocity, Unit: request.VelocityUnit})
	}
	if request.Acceleration != 0 {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		value := request.Acceleration
		if math.IsInf(value, 1) {
			value = 0
		}
		args = append(args, commandbuilding.CommandArgImpl{Value: value, Unit: request.AccelerationUnit})
	}

	command, err := manager.buildCommand(request, commandTemplate, args)
	if err != nil {
		return err
	}

	return manager.moveCommand(request, command)
}

func (manager *deviceManager) moveSin(request *pb.DeviceMoveSinRequest) errors.SdkError {
	var commandTemplate []string

	commandTemplate = []string{"move", "sin", constants.CommandArgument, constants.CommandArgument}
	if request.Count != 0 {
		commandTemplate = append(commandTemplate, fmt.Sprint(request.Count))
	}

	args := []commandbuilding.CommandArg{
		commandbuilding.CommandArgImpl{Value: float64(request.GetAmplitude()), Unit: request.GetAmplitudeUnits()},
		commandbuilding.CommandArgImpl{Value: float64(request.GetPeriod()), Unit: request.GetPeriodUnits()},
	}

	command, err := manager.buildCommand(request, commandTemplate, args)
	if err != nil {
		return err
	}

	return manager.moveCommand(request, command)
}

func (manager *deviceManager) moveSinStop(request *pb.DeviceStopRequest) errors.SdkError {
	return manager.moveCommand(request, "move sin stop")
}

func (manager *deviceManager) stop(request *pb.DeviceStopRequest) errors.SdkError {
	return manager.moveCommand(request, "stop")
}

func (manager *deviceManager) stopAll(request *pb.DeviceOnAllRequest) (*pb.DeviceOnAllResponse, errors.SdkError) {
	return manager.moveAllCommand(request, "stop")
}

func (manager *deviceManager) homeAll(request *pb.DeviceOnAllRequest) (*pb.DeviceOnAllResponse, errors.SdkError) {
	return manager.moveAllCommand(request, "home")
}

func (manager *deviceManager) isBusy(request *pb.AxisEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	reply, err := manager.singleRequestAxis(request, "")
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: reply.Status != "IDLE",
	}, nil
}

func (manager *deviceManager) isHomed(request *pb.AxisEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	warnings, _, err := GetAxisWarnings(requests, request.Device, request.Axis, false)
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: !warnings.Has("WR") && !warnings.Has("WH"),
	}, nil
}

func (manager *deviceManager) getNumberOfIndices(request *pb.AxisEmptyRequest) (*pb.IntResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	resp, err := singleCommandAxis(requests, request, "get motion.index.dist")
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return nil, errors.ErrNotSupported("Device does not support motion indices.")
		}
		return nil, err
	}
	indexDist, err := resp.DataAsInt()
	if err != nil {
		return nil, err
	}

	if indexDist == 0 {
		return &pb.IntResponse{
			Value: 0,
		}, nil
	}

	cycleDist := 0
	if resp, err := singleCommandAxis(requests, request, "get limit.cycle.dist"); err == nil {
		cycleDist, err = resp.DataAsInt()
		if err != nil {
			return nil, err
		}
	} else if !commands.IsBadCommandErr(err) {
		return nil, err
	}

	numberOfIndices := 0
	if cycleDist == 0 {
		resp, err = singleCommandAxis(requests, request, "get limit.min")
		if err != nil {
			return nil, err
		}
		limitMin, err := resp.DataAsInt()
		if err != nil {
			return nil, err
		}

		resp, err = singleCommandAxis(requests, request, "get limit.max")
		if err != nil {
			return nil, err
		}
		limitMax, err := resp.DataAsInt()
		if err != nil {
			return nil, err
		}

		numberOfIndices = (limitMax-limitMin)/indexDist + 1
	} else {
		numberOfIndices = (cycleDist + indexDist - 1) / indexDist
	}

	return &pb.IntResponse{
		Value: int32(numberOfIndices),
	}, nil
}

func (manager *deviceManager) getIndexPosition(request *pb.AxisEmptyRequest) (*pb.IntResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	resp, err := singleCommandAxis(requests, request, "get motion.index.num")
	if err != nil {
		return nil, err
	}
	indexPos, err := resp.DataAsInt()
	if err != nil {
		return nil, err
	}

	if commands.HasWarnings(resp) {
		warnings, _, err := GetAxisWarnings(requests, request.Device, request.Axis, false)
		if err != nil {
			return nil, err
		}

		if warnings.Has("WR") || warnings.Has("WH") {
			indexPos = 0
		}
	}

	return &pb.IntResponse{
		Value: int32(indexPos),
	}, nil
}
