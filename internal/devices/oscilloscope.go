package devices

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	cmd "zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	unitsp "zaber-motion-lib/internal/units"
	"zaber-motion-lib/internal/utils"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

const (
	scopePollingTimeMs = 50
	reasonNumberFormat = "Invalid number format"
)

var dataCountRE, _ = regexp.Compile(`^count\s(\d+)\schan\s\d+$`)
var settingHeaderRE, _ = regexp.Compile(`^chan\s\d+\s(\S+)(?:\saxis\s(\d+))?$`)
var ioHeaderRE, _ = regexp.Compile(`^chan\s\d+\sio\s(\S+)\s(\d+)$`)
var dataRE, _ = regexp.Compile(`^data\s(-?\d+(?:\.\d+)?)$`)

type oscilloscopeChannelData struct {
	target    *axisTargetImpl
	source    pb.AsciiOscilloscopeDataSource
	setting   string
	ioPort    pb.AsciiIoPortType
	ioChannel int
	timebase  float64
	delay     float64
	data      []float64
}

type channelDataTarget interface {
	GetDataId() int32
}

type oscilloscopeManager struct {
	devices *deviceManager

	data *utils.TokenMap[*oscilloscopeChannelData]
}

type scopeDataUnitConverter func(float64) (float64, errors.SdkError)

func newOscilloscopeManager(deviceManager *deviceManager) *oscilloscopeManager {
	return &oscilloscopeManager{
		devices: deviceManager,
		data:    utils.NewTokenMap[*oscilloscopeChannelData](),
	}
}

func (manager *oscilloscopeManager) registerCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("oscilloscope/add_setting_channel", func() pb.Message {
		return &pb.OscilloscopeAddSettingChannelRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeAddSettingChannel(request.(*pb.OscilloscopeAddSettingChannelRequest))
	})

	gateway.RegisterCallback("oscilloscope/add_io_channel", func() pb.Message {
		return &pb.OscilloscopeAddIoChannelRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeAddIoChannel(request.(*pb.OscilloscopeAddIoChannelRequest))
	})

	gateway.RegisterCallback("oscilloscope/clear_channels", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeClearChannels(request.(*pb.DeviceEmptyRequest))
	})

	gateway.RegisterCallback("oscilloscope/start", func() pb.Message {
		return &pb.OscilloscopeStartRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeStart(request.(*pb.OscilloscopeStartRequest))
	})

	gateway.RegisterCallback("oscilloscope/stop", func() pb.Message {
		return &pb.OscilloscopeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeStop(request.(*pb.OscilloscopeRequest))
	})

	gateway.RegisterCallback("oscilloscope/get_setting", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeGetSetting(request.(*pb.DeviceGetSettingRequest))
	})

	gateway.RegisterCallback("oscilloscope/get_frequency", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeGetFrequency(request.(*pb.DeviceGetSettingRequest))
	})

	gateway.RegisterCallback("oscilloscope/set_frequency", func() pb.Message {
		return &pb.DeviceSetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.oscilloscopeSetFrequency(request.(*pb.DeviceSetSettingRequest))
	})

	gateway.RegisterCallback("oscilloscope/read", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeRead(request.(*pb.DeviceEmptyRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_properties", func() pb.Message {
		return &pb.OscilloscopeDataIdentifier{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetProperties(request.(*pb.OscilloscopeDataIdentifier))
	})

	gateway.RegisterCallback("oscilloscopedata/get_timebase", func() pb.Message {
		return &pb.OscilloscopeDataGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetTimebase(request.(*pb.OscilloscopeDataGetRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_frequency", func() pb.Message {
		return &pb.OscilloscopeDataGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetFrequency(request.(*pb.OscilloscopeDataGetRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_delay", func() pb.Message {
		return &pb.OscilloscopeDataGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetDelay(request.(*pb.OscilloscopeDataGetRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_sample_time", func() pb.Message {
		return &pb.OscilloscopeDataGetSampleTimeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetSampleTime(request.(*pb.OscilloscopeDataGetSampleTimeRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_sample_times", func() pb.Message {
		return &pb.OscilloscopeDataGetSampleTimeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetSampleTimes(request.(*pb.OscilloscopeDataGetSampleTimeRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/get_samples", func() pb.Message {
		return &pb.OscilloscopeDataGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.oscilloscopeDataGetSamples(request.(*pb.OscilloscopeDataGetRequest))
	})

	gateway.RegisterCallback("oscilloscopedata/free", func() pb.Message {
		return &pb.OscilloscopeDataIdentifier{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		manager.oscilloscopeDataFree(request.(*pb.OscilloscopeDataIdentifier))
		return nil, nil
	})
}

func (manager *oscilloscopeManager) getOscilloscopeChannelData(request channelDataTarget) (*oscilloscopeChannelData, errors.SdkError) {
	data, err := manager.data.Load(request.GetDataId())
	if err != nil {
		return nil, errors.ErrInvalidOperation("The requested oscilloscope data is no longer in memory.")
	}

	return data, nil
}

func (manager *oscilloscopeManager) oscilloscopeAddSettingChannel(request *pb.OscilloscopeAddSettingChannelRequest) errors.SdkError {
	_, err := manager.devices.singleRequestAxis(request, "scope add "+request.Setting)
	return err
}

func (manager *oscilloscopeManager) oscilloscopeAddIoChannel(request *pb.OscilloscopeAddIoChannelRequest) errors.SdkError {
	port, err := utils.IoPortTypeToFirmwarePort(pb.AsciiIoPortType(request.IoType))
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf("scope add io %s %d", port, request.IoChannel))
	return err
}

func (manager *oscilloscopeManager) oscilloscopeClearChannels(request *pb.DeviceEmptyRequest) errors.SdkError {
	_, err := manager.devices.singleRequestDevice(request, "scope clear")
	return err
}

func (manager *oscilloscopeManager) oscilloscopeGetSetting(request *pb.DeviceGetSettingRequest) (*pb.IntResponse, errors.SdkError) {
	device, err := manager.devices.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if device.isIdentified() {
		if _, found := device.commandIssuingInfo.SettingsTable.GetParamInfo(request.Setting); !found {
			switch request.Setting {
			case "scope.numchannels":
				return &pb.IntResponse{Value: 6}, nil
			case "scope.channel.size":
				return &pb.IntResponse{Value: 1024}, nil
			case "scope.channel.size.max":
				return &pb.IntResponse{Value: 1024}, nil
			}
		}
	}

	reply, err := manager.devices.singleRequestAxis(request, "get "+request.Setting)
	if err != nil {
		return nil, err
	}

	value, err := reply.DataAsInt()
	if err != nil {
		return nil, err
	}

	return &pb.IntResponse{Value: int32(value)}, nil
}

func (manager *oscilloscopeManager) oscilloscopeGetFrequency(request *pb.DeviceGetSettingRequest) (*pb.DoubleResponse, errors.SdkError) {
	var err errors.SdkError
	units := manager.devices.units

	frequencyUnits := request.Unit
	if frequencyUnits != constants.NativeUnit {
		if err := units.CheckDimension(frequencyUnits, unitsp.Dimension_Frequency); err != nil {
			return nil, err
		}

		request.Unit, err = units.GetBaseUnits(unitsp.Dimension_Time)
		if err != nil {
			return nil, errors.ErrInternal(err.String())
		}
	}

	result, err := manager.devices.getSetting(request)
	if err != nil {
		return nil, err
	}

	result.Value = 1 / result.Value

	if frequencyUnits != constants.NativeUnit {
		result.Value, err = units.ConvertStaticUnitFromDimensionBase(result.Value, frequencyUnits)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func (manager *oscilloscopeManager) oscilloscopeSetFrequency(request *pb.DeviceSetSettingRequest) errors.SdkError {
	var err errors.SdkError
	units := manager.devices.units

	frequencyUnits := request.Unit
	if frequencyUnits != constants.NativeUnit {
		if err := units.CheckDimension(frequencyUnits, unitsp.Dimension_Frequency); err != nil {
			return err
		}

		request.Value, err = units.ConvertStaticUnitToDimensionBase(request.Value, frequencyUnits)
		if err != nil {
			return err
		}

		request.Unit, err = units.GetBaseUnits(unitsp.Dimension_Time)
		if err != nil {
			return errors.ErrInternal(err.String())
		}
	}

	request.Value = 1 / request.Value

	return manager.devices.setSetting(request)
}

func (manager *oscilloscopeManager) oscilloscopeStart(request *pb.OscilloscopeStartRequest) errors.SdkError {
	var command string
	if request.CaptureLength > 0 {
		device, err := manager.devices.getDeviceInfo(request)
		if err != nil {
			return err
		}

		if device.isIdentified() {
			if _, found := device.commandIssuingInfo.CommandTree.GetParamsInfo(
				[]string{"scope", "start", constants.CommandArgument}); !found {
				return errors.ErrNotSupported("The Capture Length parameter is not supported by this device's firmware version.")
			}
		}

		command = fmt.Sprintf("scope start %d", request.CaptureLength)
	} else {
		command = "scope start"
	}

	_, err := manager.devices.singleRequestDevice(request, command)
	if err != nil {
		if cmd.IsBadDataErr(err) {
			return errors.ErrInvalidOperation(
				"The oscilloscope command was rejected; ensure at least one channel was added and that you did not " +
					"request more data than will fit in the buffer.")
		}

		if cmd.IsBusyErr(err) {
			return errors.ErrInvalidOperation("An oscilloscope capture is already in progress.")
		}
	}

	return err
}

func (manager *oscilloscopeManager) oscilloscopeStop(request *pb.OscilloscopeRequest) errors.SdkError {
	_, err := manager.devices.singleRequestDevice(request, "scope stop")
	return err
}

func (manager *oscilloscopeManager) oscilloscopeRead(request *pb.DeviceEmptyRequest) (*pb.OscilloscopeReadResponse, errors.SdkError) {
	requests, err := manager.devices.getRequests(request)
	if err != nil {
		return nil, err
	}

	reply, err := singleCommandDevice(requests, request, "get scope.timebase")
	if err != nil {
		return nil, err
	}

	timebase, err := reply.DataAsFloat()
	if err != nil {
		return nil, err
	}

	reply, err = singleCommandDevice(requests, request, "get scope.delay")
	if err != nil {
		return nil, err
	}

	delay, err := reply.DataAsFloat()
	if err != nil {
		return nil, err
	}

	var scopeMessages []*c.Response

	for {
		scopeMessages, err = cmd.SingleCommandMultiResponse(requests, c.Command{
			Device:  int(request.GetDevice()),
			Command: "scope print",
		})

		if err == nil {
			break
		} else if !cmd.IsBusyErr(err) {
			return nil, err
		}

		time.Sleep(scopePollingTimeMs * time.Millisecond)
	}

	var channels []*oscilloscopeChannelData
	var dataIndex int
	var channelLength int
	var dataSource pb.AsciiOscilloscopeDataSource
	var settingName string
	var ioPort pb.AsciiIoPortType
	var ioChannel int
	var axis int32
	var data []float64

	// Local function to save a completed channel data block.
	storeChannelData := func() errors.SdkError {
		if dataIndex < channelLength {
			return errors.ErrInvalidData(
				fmt.Sprintf("Not enough data received for channel %s; expected %d items but got %d.",
					settingName, channelLength, dataIndex))
		}

		var channelData = &oscilloscopeChannelData{
			target: &axisTargetImpl{
				interfaceID: request.InterfaceId,
				device:      request.Device,
				axis:        axis,
			},
			source:    dataSource,
			setting:   settingName,
			ioPort:    ioPort,
			ioChannel: ioChannel,
			timebase:  timebase,
			delay:     delay,
			data:      data,
		}

		channels = append(channels, channelData)
		return nil
	}

	for _, message := range scopeMessages[1:] {
		// Check for most frequent message type first for speed.
		// Messages will actually be encountered in the opposite order of these RE matches.

		if match := dataRE.FindStringSubmatch(message.Data); match != nil {
			// Data sample received.
			dataValue, errParse := strconv.ParseFloat(match[1], 64)
			if errParse != nil {
				return nil, errors.ErrInvalidData(reasonNumberFormat + " in oscilloscope data value")
			}

			if dataIndex >= channelLength {
				return nil, errors.ErrInvalidData(fmt.Sprintf("Too much data received for channel %s; expected only %d items", settingName, channelLength))
			}

			data[dataIndex] = dataValue
			dataIndex++

		} else if match := settingHeaderRE.FindStringSubmatch(message.Data); match != nil {
			// Beginning of a new data block for a setting channel.
			// Package up the last channel's data if any.
			if data != nil {
				if err := storeChannelData(); err != nil {
					return nil, err
				}
			}

			// Set up next channel's data
			dataSource = pb.AsciiOscilloscopeDataSource_SETTING
			ioPort = pb.AsciiIoPortType_NONE
			ioChannel = -1
			settingName = match[1]
			axis = 0
			if len(match[2]) > 0 {
				axisNum, errParse := strconv.Atoi(match[2])
				if errParse != nil {
					return nil, errors.ErrInvalidData(reasonNumberFormat + " in oscilloscope channel axis number")
				}

				axis = int32(axisNum)
			}

			data = make([]float64, channelLength)
			dataIndex = 0

		} else if match := ioHeaderRE.FindStringSubmatch(message.Data); match != nil {
			// Beginning of a new data block for an I/O channel.
			// Package up the last channel's data if any.
			if data != nil {
				if err := storeChannelData(); err != nil {
					return nil, err
				}
			}

			// Set up next channel's data
			dataSource = pb.AsciiOscilloscopeDataSource_IO
			settingName = ""
			axis = -1
			ioPort, err = utils.FirmwarePortToIoPortType(match[1])
			if err != nil {
				return nil, err
			}

			if channel, errParse := strconv.Atoi(match[2]); errParse != nil {
				return nil, errors.ErrInvalidData(reasonNumberFormat + " in oscilloscope channel axis number")
			} else {
				ioChannel = channel
			}

			data = make([]float64, channelLength)
			dataIndex = 0

		} else if match := dataCountRE.FindStringSubmatch(message.Data); match != nil {
			// Overall capture header.
			length, errParse := strconv.Atoi(match[1])
			if errParse != nil {
				return nil, errors.ErrInvalidData(reasonNumberFormat + " in oscilloscope data count")
			}

			channelLength = length

		} else {
			return nil, errors.ErrInvalidPacket(message.Data, "Unexpected message type in oscilloscope print output.")
		}
	}

	// Package up the final channel's data if any.
	if data != nil {
		if err := storeChannelData(); err != nil {
			return nil, err
		}
	}

	var channelIDs []int32

	for _, channelData := range channels {
		id := manager.data.Store(channelData)
		channelIDs = append(channelIDs, id)
	}

	return &pb.OscilloscopeReadResponse{
		DataIds: channelIDs,
	}, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetProperties(request *pb.OscilloscopeDataIdentifier) (*pb.AsciiOscilloscopeCaptureProperties, errors.SdkError) {
	data, err := manager.getOscilloscopeChannelData(request)
	if err != nil {
		return nil, err
	}

	return &pb.AsciiOscilloscopeCaptureProperties{
		DataSource: data.source,
		Setting:    data.setting,
		AxisNumber: data.target.axis,
		IoType:     data.ioPort,
		IoChannel:  int32(data.ioChannel),
	}, nil
}

func (manager *oscilloscopeManager) oscilloscopeConvertSavedSetting(request *pb.OscilloscopeDataGetRequest, setting string, selector func(data *oscilloscopeChannelData) float64) (*pb.DoubleResponse, errors.SdkError) {
	data, err := manager.getOscilloscopeChannelData(request)
	if err != nil {
		return nil, err
	}

	deviceTarget := &axisTargetImpl{
		interfaceID: data.target.interfaceID,
		device:      data.target.device,
	}

	valueConverted, err := manager.devices.ConvertUnitSetting(deviceTarget, setting, selector(data), request.Unit, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: valueConverted,
	}, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetTimebase(request *pb.OscilloscopeDataGetRequest) (*pb.DoubleResponse, errors.SdkError) {
	return manager.oscilloscopeConvertSavedSetting(request, "scope.timebase",
		func(data *oscilloscopeChannelData) float64 { return data.timebase })
}

func (manager *oscilloscopeManager) oscilloscopeDataGetFrequency(request *pb.OscilloscopeDataGetRequest) (*pb.DoubleResponse, errors.SdkError) {
	var err errors.SdkError
	units := manager.devices.units

	frequencyUnits := request.Unit
	if frequencyUnits != constants.NativeUnit {
		if err := units.CheckDimension(frequencyUnits, unitsp.Dimension_Frequency); err != nil {
			return nil, err
		}

		request.Unit, err = units.GetBaseUnits(unitsp.Dimension_Time)
		if err != nil {
			return nil, errors.ErrInternal(err.String())
		}
	}

	result, err := manager.oscilloscopeDataGetTimebase(request)
	if err != nil {
		return nil, err
	}

	result.Value = 1 / result.Value

	if frequencyUnits != constants.NativeUnit {
		result.Value, err = units.ConvertStaticUnitFromDimensionBase(result.Value, frequencyUnits)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetDelay(request *pb.OscilloscopeDataGetRequest) (*pb.DoubleResponse, errors.SdkError) {
	return manager.oscilloscopeConvertSavedSetting(request, "scope.delay",
		func(data *oscilloscopeChannelData) float64 { return data.delay })
}

func (manager *oscilloscopeManager) oscilloscopeDataGetSampleTimesBase(request *pb.OscilloscopeDataGetSampleTimeRequest, allTimes bool) ([]float64, errors.SdkError) {
	data, err := manager.getOscilloscopeChannelData(request)
	if err != nil {
		return nil, err
	}

	deviceTarget := &axisTargetImpl{
		interfaceID: data.target.interfaceID,
		device:      data.target.device,
	}

	deviceInfo, err := manager.devices.getDeviceInfo(deviceTarget)
	if err != nil {
		return nil, err
	}

	// FW versions before 7.33 delayed one timebase interval before recording the first sample.
	var indexOffset int
	if deviceInfo.isIdentified() && !deviceInfo.fwVersion.IsHigherOrSame(dto.FirmwareVersion{Major: 7, Minor: 33}) {
		indexOffset = 1
	}

	var sampleIndices []int
	if allTimes {
		sampleIndices = utils.Range(0, len(data.data))
	} else {
		sampleIndices = append(sampleIndices, int(request.Index))
	}

	times := make([]float64, len(sampleIndices))
	for i, sampleIndex := range sampleIndices {
		// Note this assumes scope.timebase and scope.delay have the same native units.
		time := data.delay + float64(sampleIndex+indexOffset)*data.timebase

		valueConverted, err := manager.devices.ConvertUnitSetting(deviceTarget, "scope.delay", time, request.Unit, true)
		if err != nil {
			return nil, err
		}

		times[i] = valueConverted
	}

	return times, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetSampleTime(request *pb.OscilloscopeDataGetSampleTimeRequest) (*pb.DoubleResponse, errors.SdkError) {
	times, err := manager.oscilloscopeDataGetSampleTimesBase(request, false)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: times[0],
	}, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetSampleTimes(request *pb.OscilloscopeDataGetSampleTimeRequest) (*pb.DoubleArrayResponse, errors.SdkError) {
	times, err := manager.oscilloscopeDataGetSampleTimesBase(request, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleArrayResponse{
		Values: times,
	}, nil
}

func (manager *oscilloscopeManager) oscilloscopeDataGetSamples(request *pb.OscilloscopeDataGetRequest) (*pb.OscilloscopeDataGetSamplesResponse, errors.SdkError) {
	data, err := manager.getOscilloscopeChannelData(request)
	if err != nil {
		return nil, err
	}

	var result = data.data
	if request.Unit != constants.NativeUnit {
		result = make([]float64, len(data.data))

		var converter, err = manager.makeUnitConverter(data, request.Unit)
		if err != nil {
			return nil, err
		}

		for i := 0; i < len(data.data); i++ {
			result[i], err = converter(data.data[i])
			if err != nil {
				return nil, err
			}
		}
	}

	return &pb.OscilloscopeDataGetSamplesResponse{
		Data: result,
	}, nil
}

func (manager *oscilloscopeManager) makeUnitConverter(data *oscilloscopeChannelData, unit string) (scopeDataUnitConverter, errors.SdkError) {
	if data.source == pb.AsciiOscilloscopeDataSource_IO {
		var target = data.target
		target.axis = 0 // I/Os are always device scope but we set the target axis to -1 in in the saved data.

		switch data.ioPort {
		case pb.AsciiIoPortType_DIGITAL_INPUT, pb.AsciiIoPortType_DIGITAL_OUTPUT:
			return nil, errors.ErrNotSupported("Digital I/O channels do not support unit conversion.")

		case pb.AsciiIoPortType_ANALOG_OUTPUT:
			return func(value float64) (float64, errors.SdkError) {
				// Assuming here that the scope captures analog output voltages in the same native units as the set analog output command.
				return manager.devices.ConvertUnitForCommandArg(target, []string{"io", "set", "ao", "?", "?"}, 1, value, unit, true)
			}, nil

		case pb.AsciiIoPortType_ANALOG_INPUT:
			// There is no data-driven way to determine the native unit of analog inputs, so we just assume volts.
			// We can't use the same approach as for analog outputs because there might not be any on the device,
			// meaning the "io set ao" command wouldn't exist.
			device, err := manager.devices.getDeviceInfo(target)
			if err != nil {
				return nil, err
			}

			if !device.isIdentified() {
				return nil, errors.ErrNotSupported("Cannot perform unit conversion on unidentified devices.")
			}

			commandIssuingInfo, err := device.GetCommandIssuingInfo(target)
			if err != nil {
				return nil, err
			}

			var contextualDimensionID ioc.ContextualDimensionID = -1
			for _, id := range commandIssuingInfo.ConversionTable.GetContextualDimensionIDs() {
				name, found := commandIssuingInfo.ConversionTable.GetContextualDimensionName(id)
				// Assuming the oscilloscope reads analog inputs in the units of the
				// Voltage contextual dimension.
				if found && name == "Voltage" {
					contextualDimensionID = id
					break
				}
			}

			if contextualDimensionID == -1 {
				return nil, errors.ErrNotSupported("No unit conversion info is available for analog inputs.")
			}

			return func(value float64) (float64, errors.SdkError) {
				return commandIssuingInfo.ConversionTable.ConvertBack(value, unit, contextualDimensionID)
			}, nil

		default:
			return nil, errors.ErrNotSupported("The oscilloscope I/O channel type is invalid.")
		}

	} else if data.source == pb.AsciiOscilloscopeDataSource_SETTING {
		return func(value float64) (float64, errors.SdkError) {
			return manager.devices.ConvertUnitSetting(data.target, data.setting, value, unit, true)
		}, nil

	} else {
		return nil, errors.ErrNotSupported("The oscilloscope data source is neither I/O nor setting.")
	}
}

func (manager *oscilloscopeManager) oscilloscopeDataFree(request *pb.OscilloscopeDataIdentifier) {
	manager.data.Delete(request.DataId)
}
