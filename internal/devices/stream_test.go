package devices

import (
	"testing"
	pb "zaber-motion-lib/internal/dto"
)

func TestStreams_AxisTypeDefinitionsMatch(t *testing.T) {
	if len(pb.AsciiStreamAxisType_name) != len(pb.AsciiPvtAxisType_name) {
		t.Fatal("Stream axis type name count mismatch")
	}
	for key, name := range pb.AsciiStreamAxisType_name {
		name2 := pb.AsciiPvtAxisType_name[pb.AsciiPvtAxisType(key)]
		if name != name2 {
			t.Fatalf("Stream axis type name mismatch: %s != %s", name, name2)
		}
	}
}
