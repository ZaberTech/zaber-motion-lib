package devices

import (
	"time"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type WaitUntilIdleOptions struct {
	CheckErrors        bool
	CheckInterruption  bool
	CheckManualControl bool
}

func WaitUntilIdle(communication ioc.ASCIICommunicationInterface, request axisTarget, options WaitUntilIdleOptions) errors.SdkError {
	axis := (int)(request.GetAxis())
	device := (int)(request.GetDevice())
	isParticularAxis := axis > 0

	filter := func(alert *c.Response) bool {
		return device == alert.Device && (axis == alert.Axis || !isParticularAxis) && alert.Status == "IDLE"
	}
	alertSubscription := communication.SubscribeForAlert(filter)
	defer func() {
		// this call is wrapped in a closure to call cancel on the last subscription
		alertSubscription.Cancel()
	}()

	var idleReply *c.Response

	for idleReply == nil {
		reply, err := singleCommandAxis(communication.Requests(), request, "")
		if err != nil {
			return err
		}
		if reply.Status == "IDLE" {
			idleReply = reply
			break
		}

		if err := CheckWarningsForWaitUntilIdle(communication.Requests(), reply, options); err != nil {
			return err
		}

		select {
		case alert := <-alertSubscription.Channel():
			if isParticularAxis {
				if alert.Status == "IDLE" {
					idleReply = alert
				} else {
					// BUSY alerts should not be possible.
					// This else branch exists only to address if the assumption ever changes.
					alertSubscription = communication.SubscribeForAlert(filter)
				}
			} else {
				// Resubscribe for another alert.
				// Alerts here serve only to skip the polling time.
				alertSubscription = communication.SubscribeForAlert(filter)
			}
		case <-time.After(constants.GetIdlePollingPeriod()):
		}
	}

	return CheckWarningsForWaitUntilIdle(communication.Requests(), idleReply, options)
}

func CheckWarningsForWaitUntilIdle(requests *c.RequestManager, reply *c.Response, options WaitUntilIdleOptions) errors.SdkError {
	checkWarnings := options.CheckErrors || options.CheckInterruption || options.CheckManualControl
	if !checkWarnings {
		return nil
	}

	warnings, err := GetResponseWarnings(requests, reply, options.CheckErrors)
	if err != nil {
		return err
	}
	if options.CheckErrors {
		if err := CheckErrorFlags(warnings, MovementErrorWarningFlags, errors.BindErrMovementFailed(int32(reply.Device), int32(reply.Axis))); err != nil {
			return err
		}
	}
	if options.CheckManualControl && warnings.Has("NC") {
		return errors.ErrMovementInterrupted("Manual control", warnings.Flags(), int32(reply.Device), int32(reply.Axis))
	}
	if options.CheckInterruption && warnings.Has("NI") {
		return errors.ErrMovementInterrupted("New command", warnings.Flags(), int32(reply.Device), int32(reply.Axis))
	}
	return nil
}

func (manager *deviceManager) waitUntilIdleRequest(request *pb.DeviceWaitUntilIdleRequest) errors.SdkError {
	communication, err := manager.getInterface(request)
	if err != nil {
		return err
	}
	return WaitUntilIdle(communication, request, WaitUntilIdleOptions{
		CheckErrors: request.ThrowErrorOnFault,
	})
}
