package devices

import (
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

// The maximum save state version that this build can handle. Increment if a new feature is added.
const MaxSaveStateVersion = 4

const MaxStoredPositions = 16

type saveStateManager struct {
	deviceManager *deviceManager
}

func newSaveStateManager(deviceManager *deviceManager) *saveStateManager {
	return &saveStateManager{deviceManager}
}

type setStateTarget interface {
	GetState() string
	axisTarget
}

type Paramset struct {
	Type       string            `json:"type"`
	Parameters map[string]string `json:"parameters"`
}

type ServoTuning struct {
	Startup   int                 `json:"startup"`
	Paramsets map[string]Paramset `json:"paramsets"`
}

type Trigger struct {
	When    string `json:"when"`
	A       string `json:"a"`
	B       string `json:"b"`
	Count   uint64 `json:"count"`   // Number of times to fire this trigger
	Enabled bool   `json:"enabled"` // Whether this trigger is enabled or disabled. Ignored if count > 0
}

type StreamBuffer struct {
	Lines []string `json:"lines"` // The commands stored in this stream buffer
}

func (sb1 StreamBuffer) equals(sb2 *StreamBuffer) bool {
	if len(sb1.Lines) != len(sb2.Lines) {
		return false
	}
	for i := 0; i < len(sb1.Lines); i++ {
		if sb1.Lines[i] != sb2.Lines[i] {
			return false
		}
	}
	return true
}

type SaveStateContext struct {
	Version         int                 `json:"v"`    // The version of the save state
	Type            string              `json:"type"` // "controller", "device", or "peripheral"
	FirmwareVersion dto.FirmwareVersion `json:"fw"`   // FW version on the controller
}

type DeviceState struct {
	Context         *SaveStateContext           `json:"context"`
	DeviceID        string                      `json:"deviceId"`
	SerialNumber    string                      `json:"serial"`
	Settings        map[string]string           `json:"settings"`
	Storage         map[string]string           `json:"storage,omitempty"`
	Triggers        map[string]*Trigger         `json:"triggers,omitempty"`
	StreamBuffers   map[string]*StreamBuffer    `json:"streamBuffers,omitempty"`
	PvtBuffers      map[string]*StreamBuffer    `json:"pvtBuffers,omitempty"`
	ServoTuning     *ServoTuning                `json:"servoTuning,omitempty"`     // Only for integrated devices
	StoredPositions map[string]int              `json:"storedPositions,omitempty"` // Only for integrated devices
	AxisStorage     map[string]string           `json:"axisStorage,omitempty"`     // Only for integrated devices
	Axes            map[string]*PeripheralState `json:"axes,omitempty"`            // Only for controllers
}

type PeripheralState struct {
	PeripheralID    string            `json:"peripheralId"`
	SerialNumber    string            `json:"serial"`
	Settings        map[string]string `json:"settings"`
	Storage         map[string]string `json:"storage,omitempty"`
	ServoTuning     *ServoTuning      `json:"servoTuning,omitempty"`
	StoredPositions map[string]int    `json:"storedPositions,omitempty"`
}

type AxisState struct {
	Context *SaveStateContext `json:"context"`
	Axis    *PeripheralState  `json:"axis"`
}
