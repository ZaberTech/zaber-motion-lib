package devices

import (
	"fmt"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *streamManager) registerStreamIOCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_wait_digital_input", func() pb.Message {
		return &pb.StreamWaitDigitalInputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitDigitalInput(request.(*pb.StreamWaitDigitalInputRequest))
	})
	gateway.RegisterCallback("device/stream_wait_analog_input", func() pb.Message {
		return &pb.StreamWaitAnalogInputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitAnalogInput(request.(*pb.StreamWaitAnalogInputRequest))
	})
	gateway.RegisterCallback("device/stream_set_digital_output", func() pb.Message {
		return &pb.StreamSetDigitalOutputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setDigitalOutput(request.(*pb.StreamSetDigitalOutputRequest))
	})
	gateway.RegisterCallback("device/stream_set_all_digital_outputs", func() pb.Message {
		return &pb.StreamSetAllDigitalOutputsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllDigitalOutputs(request.(*pb.StreamSetAllDigitalOutputsRequest))
	})
	gateway.RegisterCallback("device/stream_set_digital_output_schedule", func() pb.Message {
		return &pb.StreamSetDigitalOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setDigitalOutputSchedule(request.(*pb.StreamSetDigitalOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/stream_set_all_digital_outputs_schedule", func() pb.Message {
		return &pb.StreamSetAllDigitalOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllDigitalOutputsSchedule(request.(*pb.StreamSetAllDigitalOutputsScheduleRequest))
	})
	gateway.RegisterCallback("device/stream_set_analog_output", func() pb.Message {
		return &pb.StreamSetAnalogOutputRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAnalogOutput(request.(*pb.StreamSetAnalogOutputRequest))
	})
	gateway.RegisterCallback("device/stream_set_all_analog_outputs", func() pb.Message {
		return &pb.StreamSetAllAnalogOutputsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllAnalogOutputs(request.(*pb.StreamSetAllAnalogOutputsRequest))
	})
	gateway.RegisterCallback("device/stream_set_analog_output_schedule", func() pb.Message {
		return &pb.StreamSetAnalogOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAnalogOutputSchedule(request.(*pb.StreamSetAnalogOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/stream_set_all_analog_outputs_schedule", func() pb.Message {
		return &pb.StreamSetAllAnalogOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setAllAnalogOutputsSchedule(request.(*pb.StreamSetAllAnalogOutputsScheduleRequest))
	})
	gateway.RegisterCallback("device/stream_cancel_output_schedule", func() pb.Message {
		return &pb.StreamCancelOutputScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.cancelOutputSchedule(request.(*pb.StreamCancelOutputScheduleRequest))
	})
	gateway.RegisterCallback("device/stream_cancel_all_outputs_schedule", func() pb.Message {
		return &pb.StreamCancelAllOutputsScheduleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.cancelAllOutputsSchedule(request.(*pb.StreamCancelAllOutputsScheduleRequest))
	})
}

func (manager *streamManager) waitDigitalInput(request *pb.StreamWaitDigitalInputRequest) errors.SdkError {
	digitalValue := 0
	if request.GetValue() {
		digitalValue = 1
	}

	command := fmt.Sprintf("wait io di %d == %d", request.GetChannelNumber(), digitalValue)
	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) waitAnalogInput(request *pb.StreamWaitAnalogInputRequest) errors.SdkError {
	condition := request.GetCondition()
	if condition == "!=" {
		condition = "<>"
	}

	channelNumber := fmt.Sprintf("%d", request.GetChannelNumber())
	commandTemplate := []string{"wait", "io", "ai", channelNumber, condition, constants.CommandArgument}
	arguments := []streamArg{streamArgImpl{
		CommandArgImpl: commandbuilding.CommandArgImpl{
			Value: request.GetValue(),
		},
	}}

	return manager.streamAction(request, commandTemplate, arguments, streamActionOptions{})
}

func (manager *streamManager) setDigitalOutput(request *pb.StreamSetDigitalOutputRequest) errors.SdkError {
	command := manager.deviceManager.buildSetDigitalOutputCommand(&pb.DeviceSetDigitalOutputRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Value:         request.Value,
	})

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAllDigitalOutputs(request *pb.StreamSetAllDigitalOutputsRequest) errors.SdkError {
	command := manager.deviceManager.buildSetAllDigitalOutputsCommand(&pb.DeviceSetAllDigitalOutputsRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		Values:      request.Values,
	})

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setDigitalOutputSchedule(request *pb.StreamSetDigitalOutputScheduleRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetDigitalOutputScheduleCommand(&pb.DeviceSetDigitalOutputScheduleRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Value:         request.Value,
		FutureValue:   request.FutureValue,
		Delay:         request.Delay,
		Unit:          request.Unit,
	})
	if err != nil {
		return err
	}

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAllDigitalOutputsSchedule(request *pb.StreamSetAllDigitalOutputsScheduleRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetAllDigitalOutputsScheduleCommand(&pb.DeviceSetAllDigitalOutputsScheduleRequest{
		InterfaceId:  request.InterfaceId,
		Device:       request.Device,
		Values:       request.Values,
		FutureValues: request.FutureValues,
		Delay:        request.Delay,
		Unit:         request.Unit,
	})
	if err != nil {
		return err
	}

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAnalogOutput(request *pb.StreamSetAnalogOutputRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetAnalogOutputCommand(&pb.DeviceSetAnalogOutputRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Value:         request.Value,
	})
	if err != nil {
		return err
	}

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAllAnalogOutputs(request *pb.StreamSetAllAnalogOutputsRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetAllAnalogOutputsCommand(&pb.DeviceSetAllAnalogOutputsRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		Values:      request.Values,
	})
	if err != nil {
		return err
	}

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAnalogOutputSchedule(request *pb.StreamSetAnalogOutputScheduleRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetAnalogOutputScheduleCommand(&pb.DeviceSetAnalogOutputScheduleRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Value:         request.Value,
		FutureValue:   request.FutureValue,
		Delay:         request.Delay,
		Unit:          request.Unit,
	})
	if err != nil {
		return err
	}
	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) setAllAnalogOutputsSchedule(request *pb.StreamSetAllAnalogOutputsScheduleRequest) errors.SdkError {
	command, err := manager.deviceManager.buildSetAllAnalogOutputsScheduleCommand(&pb.DeviceSetAllAnalogOutputsScheduleRequest{
		InterfaceId:  request.InterfaceId,
		Device:       request.Device,
		Values:       request.Values,
		FutureValues: request.FutureValues,
		Delay:        request.Delay,
		Unit:         request.Unit,
	})
	if err != nil {
		return err
	}
	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) cancelOutputSchedule(request *pb.StreamCancelOutputScheduleRequest) errors.SdkError {
	command := manager.deviceManager.buildCancelOutputScheduleCommand(&pb.DeviceCancelOutputScheduleRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Analog:        request.Analog,
	})

	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) cancelAllOutputsSchedule(request *pb.StreamCancelAllOutputsScheduleRequest) errors.SdkError {
	command := manager.deviceManager.buildCancelAllOutputsScheduleCommand(&pb.DeviceCancelAllOutputsScheduleRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		Analog:      request.Analog,
		Channels:    request.Channels,
	})

	return manager.streamActionBasic(request, command, streamActionOptions{})
}
