package devices

import (
	"fmt"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (manager *deviceManager) getSetting(request *pb.DeviceGetSettingRequest) (*pb.DoubleResponse, errors.SdkError) {
	reply, err := manager.singleRequestAxis(request, "get "+request.Setting)
	if err != nil {
		return nil, err
	}

	value, err := reply.DataAsFloat()
	if err != nil {
		if request.Axis == 0 && err.Type() == pb.Errors_INVALID_DATA {
			if len(strings.Split(reply.Data, " ")) > 1 {
				return nil, errors.ErrNotSupported(fmt.Sprintf("Multiple axis data returned; use axis level settings to get \"%s\".", request.Setting))
			} else {
				return nil, errors.ErrNotSupported(fmt.Sprintf("Settings is not parsable as number; use get string method to query \"%s\".", request.Setting))
			}
		}
		return nil, err
	}

	valueConverted, err := manager.ConvertUnitSetting(request, request.Setting, value, request.Unit, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: valueConverted,
	}, nil
}

func (manager *deviceManager) getSettingStr(request *pb.DeviceGetSettingRequest) (*pb.StringResponse, errors.SdkError) {
	reply, err := manager.singleRequestAxis(request, "get "+request.Setting)
	if err != nil {
		return nil, err
	}

	return &pb.StringResponse{
		Value: reply.Data,
	}, nil
}

func (manager *deviceManager) setSetting(request *pb.DeviceSetSettingRequest) errors.SdkError {
	commandTemplate := []string{"set", request.Setting, constants.CommandArgument}
	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Value, Unit: request.Unit}}

	command, err := manager.buildCommand(request, commandTemplate, args)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestAxis(request, command)
	if err != nil {
		return err
	}

	return nil
}

func (manager *deviceManager) setSettingStr(request *pb.DeviceSetSettingStrRequest) errors.SdkError {
	_, err := manager.singleRequestAxis(request, fmt.Sprintf("set %s %s", request.Setting, request.Value))
	return err
}

func (manager *deviceManager) convertSetting(request *pb.DeviceConvertSettingRequest) (*pb.DoubleResponse, errors.SdkError) {
	valueConverted, err := manager.ConvertUnitSetting(request, request.Setting, request.Value, request.Unit, request.FromNative)
	if err != nil {
		return nil, err
	}
	return &pb.DoubleResponse{
		Value: valueConverted,
	}, nil
}

func (manager *deviceManager) getSettingDefaultStr(request *pb.DeviceGetSettingRequest) (*pb.StringResponse, errors.SdkError) {
	info, err := manager.GetIdentifiedDeviceInfo(request)
	if err != nil {
		return nil, err
	}
	commandIssuingInfo, err := info.GetCommandIssuingInfo(request)
	if err != nil {
		return nil, err
	}

	defaultValue, found := commandIssuingInfo.SettingsTable.GetDefaultValue(request.Setting)
	if !found {
		return nil, errors.ErrSettingNotFound(fmt.Sprintf("Setting %s not found.", request.Setting))
	} else if defaultValue == nil {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Setting %s does not have a default value.", request.Setting))
	}

	return &pb.StringResponse{
		Value: *defaultValue,
	}, nil
}

func (manager *deviceManager) getSettingFromAllAxes(request *pb.DeviceGetSettingRequest) (*pb.DoubleArrayResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, "get "+request.Setting)
	if err != nil {
		return nil, err
	}
	values, err := reply.DataAsFloatArray(true)
	if err != nil {
		return nil, err
	}

	info, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if info.isIdentified() {
		if len(values) != len(info.axes) {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Setting '%s' is device scope, and cannot be fetched with GetFromAllAxes.", request.Setting))
		}
	}

	return &pb.DoubleArrayResponse{
		Values: values,
	}, nil
}

func (manager *deviceManager) getSettingDefault(request *pb.DeviceGetSettingRequest) (*pb.DoubleResponse, errors.SdkError) {
	responseStr, err := manager.getSettingDefaultStr(request)
	if err != nil {
		return nil, err
	}

	numericValue, parseErr := strconv.ParseFloat(responseStr.Value, 64)
	if parseErr != nil {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Default value %s of setting %s cannot be converted to number.", responseStr.Value, request.Setting))
	}

	valueConverted, err := manager.ConvertUnitSetting(request, request.Setting, numericValue, request.Unit, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: valueConverted,
	}, nil
}

func (manager *deviceManager) canConvertSetting(request *pb.DeviceGetSettingRequest) (*pb.BoolResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	commandIssuingInfo, err := device.GetCommandIssuingInfo(request)
	if err != nil {
		return nil, err
	}

	paramInfo, ok := commandIssuingInfo.SettingsTable.GetParamInfo(request.Setting)
	if !ok {
		return nil, errors.ErrSettingNotFound(fmt.Sprintf("Cannot find setting %s.", request.Setting))
	}

	return &pb.BoolResponse{
		Value: commandIssuingInfo.ConversionTable.CanConvert(paramInfo.ContextualDimensionID),
	}, nil
}

type storedConversionTableRow struct {
	ContextualDimensionID int      `json:"contextual_dimension_id"`
	DimensionName         string   `json:"dimension_name"`
	FunctionName          string   `json:"function_name"`
	Scale                 *float64 `json:"scale"`
}
type storedConversionTable struct {
	Version int                         `json:"v"`
	Rows    []*storedConversionTableRow `json:"rows"`
}

func (manager *deviceManager) setUnitConversions(request *pb.DeviceSetUnitConversionsRequest) errors.SdkError {
	if len(request.Conversions) == 0 {
		_, err := manager.storage.Erase(request, constants.UnitConversionTableKey)
		return err
	}

	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return err
	}

	axisInfo, err := device.getAxisInfo(request)
	if err != nil {
		return err
	}

	if axisInfo.isPeripheralLike && !axisInfo.identity.IsModified {
		return errors.ErrInvalidOperation("Unit conversions can only be set on modified peripherals.")
	} else if !axisInfo.isPeripheralLike && !device.identity.IsModified {
		return errors.ErrInvalidOperation("Unit conversions can only be set on modified devices.")
	}

	commandIssuingInfo, err := device.GetCommandIssuingInfo(request)
	if err != nil {
		return err
	}

	sourceData := commandIssuingInfo.ConversionTable.GetSourceData()
	tableToStore := &storedConversionTable{
		Version: constants.UnitConversionTableVersion,
	}

	uniqueCheck := make(map[int]string)

	for _, conversion := range request.Conversions {
		paramInfo, ok := commandIssuingInfo.SettingsTable.GetParamInfo(conversion.Setting)
		if !ok {
			return errors.ErrSettingNotFound(fmt.Sprintf("Cannot find setting %s.", conversion.Setting))
		} else if paramInfo.ContextualDimensionID == 0 {
			return errors.ErrSettingNotFound(fmt.Sprintf("Setting %s cannot be converted as it does not have a dimension.", conversion.Setting))
		}

		contextualDimensionID := (int)(paramInfo.ContextualDimensionID)
		if otherSetting, isDuplicate := uniqueCheck[contextualDimensionID]; isDuplicate {
			return errors.ErrInvalidArgument(fmt.Sprintf(
				"Dimension specified by multiple setting (%s, %s). Specify dimension only once.",
				otherSetting, conversion.Setting))
		} else {
			uniqueCheck[contextualDimensionID] = conversion.Setting
		}

		dimensionRowIndex := utils.SliceIndex(len(sourceData.Rows), func(i int) bool {
			return sourceData.Rows[i].ContextualDimensionID == contextualDimensionID
		})
		if dimensionRowIndex < 0 {
			return errors.ErrInternal(fmt.Sprintf("Cannot find dimension row %d.", contextualDimensionID))
		}
		dimension := sourceData.Rows[dimensionRowIndex]

		if err := manager.units.CheckDimension(conversion.Unit, dimension.DimensionName); err != nil {
			return err
		}
		valueBaseUnits, err := manager.units.ConvertStaticUnitToDimensionBase(conversion.Value, conversion.Unit)
		if err != nil {
			return err
		}
		scale, err := commandIssuingInfo.ConversionTable.GetScale(dimension.FunctionName, valueBaseUnits)
		if err != nil {
			return err
		}

		row := &storedConversionTableRow{
			ContextualDimensionID: contextualDimensionID,
			DimensionName:         dimension.DimensionName,
			FunctionName:          dimension.FunctionName,
			Scale:                 &scale,
		}

		tableToStore.Rows = append(tableToStore.Rows, row)
	}

	return manager.storage.SetStorageStructure(request, constants.UnitConversionTableKey, tableToStore)
}

func (manager *deviceManager) loadCustomConversions(target ioc.DeviceTarget, axis int, table *dto.ConversionTable) errors.SdkError {
	var storedTable storedConversionTable
	if stored, err := manager.storage.GetStoredStructure(&axisTargetImpl{
		interfaceID: target.GetInterfaceId(),
		device:      target.GetDevice(),
		axis:        int32(axis),
	}, constants.UnitConversionTableKey, &storedTable); err != nil {
		return err
	} else if stored {
		if storedTable.Version != constants.UnitConversionTableVersion {
			return errors.ErrNotSupported("Conversion table in device's storage not compatible with this version of the library.")
		}

		for _, row := range table.Rows {
			for _, storedRow := range storedTable.Rows {
				if storedRow.ContextualDimensionID == row.ContextualDimensionID {
					row.Scale = storedRow.Scale
					row.FunctionName = storedRow.FunctionName
					break
				}
			}
		}
	}
	return nil
}
