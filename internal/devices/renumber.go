package devices

import (
	"fmt"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) renumberAll(request *pb.RenumberRequest) (*pb.IntResponse, errors.SdkError) {
	// Since the devices are renumbered, device infos are now invalid and should be reset
	err := manager.clearAllDeviceInfo(request, nil)
	if err != nil {
		return nil, err
	}

	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	cmd := c.Command{Command: fmt.Sprintf("renumber %d", int(request.GetAddress()))}
	replies, err := requests.Request(cmd, &c.RequestOptions{
		CollectMultiple: true,
	})
	if err != nil {
		if err.Type() == pb.Errors_REQUEST_TIMEOUT {
			err = errors.ErrNoDeviceFound()
		}
		return nil, err
	}

	for _, response := range replies {
		if err := commands.CheckOk(response, &cmd); err != nil {
			return nil, err
		}
	}

	response := &pb.IntResponse{
		Value: int32(len(replies)),
	}

	return response, nil
}

func (manager *deviceManager) renumberDevice(request *pb.RenumberRequest) (*pb.IntResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, fmt.Sprintf("renumber %d", int(request.GetAddress())))
	if err != nil && err.Type() != pb.Errors_INVALID_DATA {
		return nil, err
	}

	response := &pb.IntResponse{
		Value: int32(reply.Device),
	}

	return response, nil
}
