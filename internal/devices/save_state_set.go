package devices

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"

	"github.com/elliotchance/pie/v2"
)

func setServoTuning(request axisTarget, requests *c.RequestManager, tunings *ServoTuning) string {
	if tunings == nil {
		return ""
	}

	cmd := c.Command{
		Device: int(request.GetDevice()),
		Axis:   int(request.GetAxis()),
	}

	for paramsetNum := 1; paramsetNum <= 9; paramsetNum++ {
		paramset := tunings.Paramsets[fmt.Sprint(paramsetNum)]
		for paramName, paramVal := range paramset.Parameters {
			cmd.Command = fmt.Sprintf("servo %d set %s %s", paramsetNum, paramName, paramVal)
			if _, err := commands.SingleCommand(requests, cmd); err != nil {
				return fmt.Sprintf("Could not set %s to %s: %s", paramName, paramVal, err.Message())
			}
		}
	}

	cmd.Command = fmt.Sprintf("servo set startup %d", tunings.Startup)
	if _, err := commands.SingleCommand(requests, cmd); err != nil {
		return fmt.Sprintf("Could not set startup paramset to %d: %s", tunings.Startup, err.Message())
	}

	return ""
}

func setStoredPositions(request axisTarget, requests *c.RequestManager, storedPositions map[string]int) []string {
	var problems []string
	if len(storedPositions) == 0 {
		return problems
	}

	cmd := c.Command{
		Device: int(request.GetDevice()),
		Axis:   int(request.GetAxis()),
	}

	for i := 1; i <= MaxStoredPositions; i++ {
		pos := storedPositions[fmt.Sprint(i)]
		cmd.Command = fmt.Sprintf("tools storepos %d %d", i, pos)
		if _, err := commands.SingleCommand(requests, cmd); err != nil {
			problems = append(problems, fmt.Sprintf("Could not store position %d at %d: %s", pos, i, err.Message()))
		}
	}

	return problems
}

func (manager *saveStateManager) setTriggers(request *pb.SetStateRequest, triggers map[string]*Trigger) []string {
	var errs []string
	if len(triggers) == 0 {
		return errs
	}

	var numTriggers int
	getNumTriggersCmd := "get trigger.numtriggers"
	if numTriggersReply, err := manager.deviceManager.singleRequestDevice(request, getNumTriggersCmd); err != nil {
		errs = append(errs, getNumTriggersCmd)
	} else if numTriggers, err = numTriggersReply.DataAsInt(); err != nil {
		errs = append(errs, getNumTriggersCmd)
	}

	for i := 1; i <= numTriggers; i++ {
		if savedTrigger, savedTriggerExists := triggers[fmt.Sprint(i)]; savedTriggerExists {
			if savedTrigger.When != "" {
				cmd := fmt.Sprintf("trigger %d when %s", i, savedTrigger.When)
				_, err := manager.deviceManager.singleRequestDevice(request, cmd)
				if err != nil {
					errs = append(errs, cmd)
				}
			}
			if savedTrigger.A != "" {
				cmd := fmt.Sprintf("trigger %d action a %s", i, savedTrigger.A)
				_, err := manager.deviceManager.singleRequestDevice(request, cmd)
				if err != nil {
					errs = append(errs, cmd)
				}
			}
			if savedTrigger.B != "" {
				cmd := fmt.Sprintf("trigger %d action b %s", i, savedTrigger.B)
				_, err := manager.deviceManager.singleRequestDevice(request, cmd)
				if err != nil {
					errs = append(errs, cmd)
				}
			}
			if savedTrigger.Count > 0 {
				cmd := fmt.Sprintf("trigger %d enable %d", i, savedTrigger.Count)
				_, err := manager.deviceManager.singleRequestDevice(request, cmd)
				if err != nil {
					errs = append(errs, cmd)
				}
			} else if savedTrigger.Enabled {
				cmd := fmt.Sprintf("trigger %d enable", i)
				_, err := manager.deviceManager.singleRequestDevice(request, cmd)
				if err != nil {
					errs = append(errs, cmd)
				}
			}
		} else {
			manager.deviceManager.singleRequestDevice(request, fmt.Sprintf("trigger %d action a none", i))
			manager.deviceManager.singleRequestDevice(request, fmt.Sprintf("trigger %d action b none", i))
			cmd := fmt.Sprintf("trigger %d disable", i)
			_, err := manager.deviceManager.singleRequestDevice(request, cmd)
			if err != nil {
				errs = append(errs, cmd)
			}
		}
	}

	return errs
}

func setStreamBuffers(request *pb.SetStateRequest, requests *c.RequestManager, streamBuffers map[string]*StreamBuffer, pvt bool) []string {
	var errs []string
	if len(streamBuffers) == 0 {
		return errs
	}

	var numSetting string
	if pvt {
		numSetting = "pvt.numseqs"
	} else {
		numSetting = "stream.numstreams"
	}
	cmd := c.Command{
		Device:  int(request.Device),
		Command: "get " + numSetting,
	}

	if numStreamsReply, err := commands.SingleCommand(requests, cmd); err != nil {
		errs = append(errs, cmd.Command)
	} else if numStreams, err := numStreamsReply.DataAsInt(); err != nil {
		errs = append(errs, cmd.Command)
	} else {
		for i := 1; i <= numStreams; i++ {
			cmd.Command = fmt.Sprintf("%s %d setup disable", getStreamPrefixBase(pvt), i)
			_, err := commands.SingleCommand(requests, cmd)
			if err != nil {
				errs = append(errs, cmd.Command)
			}
		}
	}

	bufferList, err, errCmd := listStreamBuffers(requests, request, pvt)
	if err != nil {
		errs = append(errs, errCmd)
		return errs
	}

	for _, buffNum := range bufferList {
		if _, isSavedBuf := streamBuffers[fmt.Sprint(buffNum)]; isSavedBuf {
			continue
		}

		cmd.Command = fmt.Sprintf("%s buffer %d erase", getStreamPrefixBase(pvt), buffNum)
		_, err := commands.SingleCommand(requests, cmd)
		if err != nil {
			errs = append(errs, cmd.Command)
			continue
		}
	}

	for _, buffNumStr := range pie.Sort(pie.Keys(streamBuffers)) {
		buffer := streamBuffers[buffNumStr]
		buffNum, _ := strconv.Atoi(buffNumStr)

		if oldBuffer, err, errCmd := getStreamBuffer(request, requests, buffNum, pvt); err != nil {
			errs = append(errs, errCmd)
			continue
		} else if oldBuffer != nil {
			if oldBuffer.equals(buffer) {
				continue
			}

			cmd.Command = fmt.Sprintf("%s buffer %d erase", getStreamPrefixBase(pvt), buffNum)
			_, err := commands.SingleCommand(requests, cmd)
			if err != nil {
				errs = append(errs, cmd.Command)
				continue
			}
		}

		for _, bufferCmd := range buffer.Lines {
			cmd.Command = getStreamPrefixBase(pvt) + " 1 " + bufferCmd
			_, err = commands.SingleCommand(requests, cmd)
			if err != nil {
				errs = append(errs, cmd.Command)
				break
			}
		}
	}

	return errs
}

// These settings have to be handled specially and should be skipped in the general set loop
var SkipSettings = map[string]struct{}{
	// Settings that require special handling
	"peripheral.hw.modified": {}, "device.hw.modified": {}, "system.access": {}, "knob.enable": {},
}

// These settings should be set early since setting them can influence other settings
// either by overwritting them, or by altering their valid range
var EarlySetSetting = map[string][]struct{}{
	// This updates anything in native distance units see: https://www.zaber.com/protocol-manual?protocol=ASCII#topic_setting_resolution
	"resolution": {},
	// Must be set before `motor.current.max`
	"motor.current.overdrive.max": {},
	// This limits the legal values of driver.current.run, driver.current.hold, driver.current.servo, and force.max so should be set before them.
	"motor.current.max": {},
}

const CurrentControlSettingPrefix = "ictrl"
const CurrentControlTypeSetting = "ictrl.type"

func writeListOfSettings(requests *c.RequestManager, cmd c.Command, settings []string, values map[string]string) []string {
	var errors []string
	for _, setting := range settings {
		value := values[setting]
		cmd.Command = fmt.Sprintf("set %s %s", setting, value)
		_, err := commands.SingleCommand(requests, cmd)
		if err != nil {
			errors = append(errors, setting)
		}
	}
	return errors
}

func setSettings(request *pb.SetStateRequest, requests *c.RequestManager, settings map[string]string) ([]string, []string) {
	var setWarnings []string
	var setErrors []string

	cmd := c.Command{
		Device: int(request.Device),
	}
	productType := "device"
	if request.Axis > 0 {
		cmd.Axis = int(request.Axis)
		productType = "peripheral"
	}

	// Set hardware modified to true first since other settings may require it to be 1
	hwModSetting := productType + ".hw.modified"
	if hwMod, ok := settings[hwModSetting]; ok && hwMod == "1" {
		cmd.Command = fmt.Sprintf("set %s 1", hwModSetting)
		_, err := commands.SingleCommand(requests, cmd)
		if err != nil {
			setErrors = append(setErrors, hwModSetting)
		}
	}

	var earlySetSettings []string
	var currentControlSettings []string
	var generalSettings []string
	for setting := range settings {
		_, skipped := SkipSettings[setting]
		_, nonSaved := NonsavedSettings[setting]
		if _, isEarly := EarlySetSetting[setting]; isEarly {
			earlySetSettings = append(earlySetSettings, setting)
		} else if isCurrentControlSetting := strings.HasPrefix(setting, CurrentControlSettingPrefix); isCurrentControlSetting {
			if setting != CurrentControlTypeSetting {
				currentControlSettings = append(currentControlSettings, setting)
			}
		} else if !skipped && !nonSaved {
			generalSettings = append(generalSettings, setting)
		}
	}
	sort.Slice(earlySetSettings, func(i, j int) bool {
		return settingOrderComparator(earlySetSettings[i], earlySetSettings[j])
	})
	sort.Slice(currentControlSettings, func(i, j int) bool {
		return settingOrderComparator(currentControlSettings[i], currentControlSettings[j])
	})
	sort.Slice(generalSettings, func(i, j int) bool {
		return settingOrderComparator(generalSettings[i], generalSettings[j])
	})

	earlySetErrors := writeListOfSettings(requests, cmd, earlySetSettings, settings)
	setErrors = append(setErrors, earlySetErrors...)
	if controllerType, hasCurrentControl := settings[CurrentControlTypeSetting]; hasCurrentControl {
		cmd.Command = fmt.Sprintf("set %s %s", CurrentControlTypeSetting, controllerType)
		_, err := commands.SingleCommand(requests, cmd)
		if err != nil {
			if commands.IsBadDataErr(err) {
				setWarnings = append(setWarnings, fmt.Sprintf("Current controller type %s is not supported. Default current tuning will be used.", controllerType))
			} else {
				setErrors = append(setErrors, "ictrl.*")
			}
		} else {
			currentControlSetErrors := writeListOfSettings(requests, cmd, currentControlSettings, settings)
			setErrors = append(setErrors, currentControlSetErrors...)
		}
	}

	generalSetErrors := writeListOfSettings(requests, cmd, generalSettings, settings)
	setErrors = append(setErrors, generalSetErrors...)

	// Cannot set `knob.enable` to -1, so set to 1 instead
	knobEnableValue, hasKnobEnabled := settings["knob.enable"]
	if hasKnobEnabled {
		if knobEnableValue == "-1" {
			knobEnableValue = "1"
		}
		cmd.Command = fmt.Sprintf("set knob.enable %s", knobEnableValue)
		_, err := commands.SingleCommand(requests, cmd)
		if err != nil {
			setErrors = append(setErrors, "knob.enable")
		}
	}

	return setWarnings, setErrors
}

var regexEndWithMinMax = regexp.MustCompile(`\.(min|max)$`)

// Puts settings that set a min|max value first, then everything else aphabetically
func settingOrderComparator(setting1, setting2 string) bool {
	isMinMax1 := regexEndWithMinMax.MatchString(setting1)
	isMinMax2 := regexEndWithMinMax.MatchString(setting2)
	if isMinMax1 != isMinMax2 {
		return isMinMax1
	}

	return setting1 < setting2
}

func (manager *saveStateManager) setStorage(request axisTarget, storage map[string]string) []string {
	errs := []string{}
	if len(storage) < 1 {
		return errs
	}

	for key, value := range storage {
		err := manager.deviceManager.storage.SetStorage(request, key, value, false)
		if err != nil {
			errs = append(errs, fmt.Sprintf("Could not store value with key %s: %s", key, err.Message()))
			continue
		}
	}

	return errs
}

func isSetPeripheralStateClean(data *pb.ExceptionsSetPeripheralStateExceptionData) bool {
	if numField := len(utils.GetExportedFields(data)); numField != 5 {
		// update the condition below
		panic(fmt.Errorf("isSetPeripheralStateClean: unexpected number of fields (%d)", numField))
	}
	return len(data.Settings) == 0 && data.ServoTuning == "" &&
		len(data.StoredPositions) == 0 && len(data.Storage) == 0
}

func (manager *saveStateManager) setStateToPeripheral(request *pb.SetStateRequest, requests *c.RequestManager, state *PeripheralState) ([]string, *pb.ExceptionsSetPeripheralStateExceptionData) {
	settingWarnings, settingErrors := setSettings(request, requests, state.Settings)

	problems := &pb.ExceptionsSetPeripheralStateExceptionData{
		// don't forget to add condition to isSetPeripheralStateClean above
		AxisNumber:      request.Axis,
		Settings:        settingErrors,
		ServoTuning:     setServoTuning(request, requests, state.ServoTuning),
		StoredPositions: setStoredPositions(request, requests, state.StoredPositions),
		Storage:         manager.setStorage(request, state.Storage),
	}

	if !isSetPeripheralStateClean(problems) {
		return settingWarnings, problems
	}

	return settingWarnings, nil
}

func (manager *saveStateManager) setPeripheralState(request *pb.SetStateRequest) (*pb.AsciiSetStateAxisResponse, errors.SdkError) {
	check, state := manager.canSetToPeripheral(request, nil)
	if check.Error != nil {
		return nil, errors.ErrNotSupported(*check.Error)
	}

	err := manager.deviceManager.driverDisable(request)
	if err != nil {
		return nil, err
	}
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	if state.Axis.Settings["peripheral.hw.modified"] != "1" {
		hwModLive, err := manager.deviceManager.singleRequestAxis(request, "get peripheral.hw.modified")
		if err != nil {
			if !commands.IsBadCommandErr(err) {
				return nil, err
			}
		} else if hwModLive.Data == "1" {
			// Must restore this axis to revert peripheral.hw.modified to 0
			if _, err = manager.deviceManager.singleRequestAxis(request, "axis restore"); err != nil {
				return nil, err
			}
		}
	}

	if accessLevelReply, err := manager.deviceManager.singleRequestDevice(request, "get system.access"); err != nil {
		return nil, err
	} else if _, err = manager.deviceManager.singleRequestDevice(request, "set system.access 2"); err != nil {
		return nil, err
	} else {
		warnings, problems := manager.setStateToPeripheral(request, requests, state.Axis)
		manager.deviceManager.singleRequestDevice(request, fmt.Sprintf("set system.access %s", accessLevelReply.Data))
		if problems != nil {
			return nil, errors.ErrCouldNotSetPeripheralState(problems)
		}

		err := manager.deviceManager.driverEnable(request, constants.DriverEnableTimeout)
		return &pb.AsciiSetStateAxisResponse{
			AxisNumber: request.Axis,
			Warnings:   warnings,
		}, err
	}
}

func isSetDeviceStateClean(data *pb.ExceptionsSetDeviceStateExceptionData) bool {
	if numField := len(utils.GetExportedFields(data)); numField != 8 {
		// update the condition below
		panic(fmt.Errorf("isSetDeviceStateClean: unexpected number of fields (%d)", numField))
	}
	return len(data.Peripherals) == 0 && len(data.Settings) == 0 &&
		len(data.Triggers) == 0 && len(data.StreamBuffers) == 0 &&
		data.ServoTuning == "" && len(data.StoredPositions) == 0 &&
		len(data.Storage) == 0 && len(data.PvtBuffers) == 0
}

func (manager *saveStateManager) setDeviceState(request *pb.SetStateRequest) (*pb.AsciiSetStateDeviceResponse, errors.SdkError) {
	check, state, axes := manager.canSetToDevice(request, nil, request.DeviceOnly)
	if check.Error != nil {
		return nil, errors.ErrNotSupported(*check.Error)
	} else if !request.DeviceOnly {
		for _, axisCheck := range check.AxisErrors {
			if axisCheck.Error != nil {
				return nil, errors.ErrNotSupported(*axisCheck.Error)
			}
		}
	}
	err := manager.deviceManager.driverDisable(request)
	if err != nil {
		return nil, err
	}
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	if state.Settings["device.hw.modified"] != "1" {
		hwModLive, err := manager.deviceManager.singleRequestDevice(request, "get device.hw.modified")
		if err != nil {
			if !commands.IsBadCommandErr(err) {
				return nil, err
			}
		} else if hwModLive.Data == "1" {
			// Must restore this device to revert device.hw.modified to 0
			if _, err = manager.deviceManager.singleRequestDevice(request, "system restore"); err != nil {
				return nil, err
			}
		}
	}

	if _, err = manager.deviceManager.singleRequestDevice(request, "set system.access 2"); err != nil {
		return nil, err
	} else {
		settingsWarnings, settingsProblems := setSettings(request, requests, state.Settings)

		problems := &pb.ExceptionsSetDeviceStateExceptionData{
			// don't forget to add condition to isSetDeviceStateClean above
			Settings:      settingsProblems,
			Triggers:      manager.setTriggers(request, state.Triggers),
			StreamBuffers: setStreamBuffers(request, requests, state.StreamBuffers, false),
			PvtBuffers:    setStreamBuffers(request, requests, state.PvtBuffers, true),
			Storage:       manager.setStorage(request, state.Storage),
		}

		warnings := &pb.AsciiSetStateDeviceResponse{
			Warnings: settingsWarnings,
		}

		if state.Context.Type == "device" {
			axisRequest := &pb.AxisEmptyRequest{
				InterfaceId: request.InterfaceId,
				Device:      request.Device,
				Axis:        1,
			}
			problems.ServoTuning = setServoTuning(axisRequest, requests, state.ServoTuning)
			problems.StoredPositions = setStoredPositions(axisRequest, requests, state.StoredPositions)
			problems.Storage = append(problems.Storage, manager.setStorage(axisRequest, state.AxisStorage)...)
		}

		if state.Context.Type == "controller" && !request.DeviceOnly {
			warnings.AxisResponses = make([]*pb.AsciiSetStateAxisResponse, len(axes))
			for i, address := range pie.Sort(pie.Keys(axes)) {
				axis := axes[address]
				axisRequest := &pb.SetStateRequest{InterfaceId: request.InterfaceId, Device: request.Device, Axis: address}
				axisWarnings, axisProblems := manager.setStateToPeripheral(axisRequest, requests, axis)
				if axisProblems != nil {
					problems.Peripherals = append(problems.Peripherals, axisProblems)
				}
				warnings.AxisResponses[i] = &pb.AsciiSetStateAxisResponse{
					AxisNumber: address,
					Warnings:   axisWarnings,
				}
			}
		}

		if accessLevel, ok := state.Settings["system.access"]; ok {
			manager.deviceManager.singleRequestDevice(request, fmt.Sprintf("set system.access %s", accessLevel))
		} else {
			manager.deviceManager.singleRequestDevice(request, "set system.access 1")
		}

		if !isSetDeviceStateClean(problems) {
			return nil, errors.ErrCouldNotSetDeviceState(problems)
		}

		err := manager.deviceManager.driverEnable(request, constants.DriverEnableTimeout)
		return warnings, err
	}
}
