package devices

import (
	"fmt"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type streamManager struct {
	deviceManager *deviceManager
}

func newStreamManager(deviceManager *deviceManager) *streamManager {
	return &streamManager{deviceManager}
}

func (manager *streamManager) registerStreamCallbacks(gateway ioc.GatewayManager) {
	manager.registerStreamSettingsCallbacks(gateway)
	manager.registerStreamSetupCallbacks(gateway)
	manager.registerStreamMovementCallbacks(gateway)
	manager.registerStreamBufferCallbacks(gateway)
	manager.registerStreamIOCallbacks(gateway)
	manager.registerStreamUtilCallbacks(gateway)
}

// Gets the stream.
// Disables the stream on the device when it's created for the first time.
// Use getStreamSync instead in synchronous methods.
func (manager *streamManager) getStream(request streamTarget, createIfDoesNotExist bool) (*stream, errors.SdkError) {
	deviceInfo, err := manager.deviceManager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	streamID := fmt.Sprintf("%s%d", getStreamPrefix(request), request.GetStreamId())

	stream, created := func() (*stream, bool) {
		deviceInfo.streamsLock.Lock()
		defer deviceInfo.streamsLock.Unlock()

		retrievedStream, exists := deviceInfo.streams[streamID]
		if exists || !createIfDoesNotExist {
			return retrievedStream, false
		}

		retrievedStream = &stream{
			streamID: int(request.GetStreamId()),
			pvt:      request.GetPvt(),
		}
		if deviceInfo.streams == nil {
			deviceInfo.streams = make(map[string]*stream)
		}
		deviceInfo.streams[streamID] = retrievedStream

		return retrievedStream, true
	}()

	if created {
		// disabling the stream ensures that library is in sync with the device
		_, err = manager.streamRequest(request, "setup disable")
		if err != nil {
			return nil, err
		}
	}

	return stream, nil
}

// Gets setup stream or returns ErrStreamMode if it's disabled (or not yet created).
// Use in synchronous methods that should not send anything to the device.
func (manager *streamManager) getStreamSync(request streamTarget) (*stream, errors.SdkError) {
	stream, err := manager.getStream(request, false)
	if err != nil {
		return nil, err
	} else if stream == nil {
		message := fmt.Sprintf("The %s is disabled. Setup the %s first.", getStreamTextWord(request), getStreamTextWord(request))
		if request.GetPvt() {
			return nil, errors.ErrPvtMode(message)
		} else {
			return nil, errors.ErrStreamMode(message)
		}
	}

	return stream, nil
}
