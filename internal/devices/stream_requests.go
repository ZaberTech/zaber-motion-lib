package devices

import (
	"fmt"
	"strings"
	"time"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	"github.com/elliotchance/pie/v2"
)

var flagFBExplanations = map[string]string{
	"axislimit":  "A path segment tried to move an axis to a position less than limit.min or greater than limit.max.",
	"setting":    "A stream set action had an out-of-range value.",
	"stackdepth": "The stream's action queue overflowed from too many nested stream call actions (i.e. a stream buffer contained a stream call action, which called another buffer containing a stream call action, etc.)",
	"bufempty":   "A stream call action called a buffer which is empty.",
	"bufaxes":    "A stream call action called a buffer which was recorded for a different number of axes than are driven by the stream.",
	"arcradius":  "A stream arc path segment specified coordinates that are not congruent to a circle (i.e. the arc’s radius is significantly different between the start and end of the arc).",
	"decellimit": "A PVT segment tried to move an axis to a position at a velocity that would eventually cause the axis to hit a limit, even if decelerating at max deceleration.",
	"endvel":     "A PVT sequence reached its final point with nonzero velocity, causing the affected axes to overshoot the final position point and deviate from the prescribed path. This is likely caused by exceeding communication bandwidth.",
}

func (manager *streamManager) processFBError(request streamTarget) errors.SdkError {
	streamInfo, err := manager.requestStreamInfo(request)
	if err != nil {
		return err
	}

	if streamInfo.streamError == "-" {
		return nil
	}

	explanation, ok := flagFBExplanations[streamInfo.streamError]
	if !ok {
		explanation = fmt.Sprintf("Unknown stream error: %s.", streamInfo.streamError)
	}

	return errors.ErrStreamExecution(explanation, streamInfo.streamError)
}

func (manager *streamManager) processPvtFBError(request streamTarget) errors.SdkError {
	pvtInfo, err := manager.requestPvtInfo(request)
	if err != nil {
		return err
	}

	if pvtInfo.streamError == "-" {
		return nil
	}

	var invalidPoints []*pb.ExceptionsInvalidPvtPoint
	for _, point := range pvtInfo.invalidPoints {
		invalidPoints = append(invalidPoints, &pb.ExceptionsInvalidPvtPoint{
			Index: int32(point.index),
			Point: point.data,
		})
	}

	explanation, ok := flagFBExplanations[pvtInfo.streamError]
	if !ok {
		explanation = fmt.Sprintf("Unknown stream error: %s.", pvtInfo.streamError)
	}

	return errors.ErrPvtExecution(explanation, pvtInfo.streamError, invalidPoints)
}

type checkStreamWarningsOptions struct {
	checkDiscontinuities bool
}

func (manager *streamManager) checkAndClearStreamWarnings(
	requests *c.RequestManager, request streamTarget, stream *stream, options checkStreamWarningsOptions,
) errors.SdkError {
	if stream.mode != StreamModeLive {
		return nil
	}

	hasFB := false
	warningFlagsForAxes := make([]*WarningsFlags, 0, len(stream.physicalAxesNumbers))

	for _, axisNumber := range stream.physicalAxesNumbers {
		warningFlagsForAxis, _, err := GetAxisWarnings(requests, request.GetDevice(), axisNumber, false)
		if err != nil {
			return err
		}

		warningFlagsForAxes = append(warningFlagsForAxes, warningFlagsForAxis)
		hasFB = hasFB || warningFlagsForAxis.Has("FB")
	}

	var errToReturn errors.SdkError

	if hasFB && errToReturn == nil {
		// FB error must be processed before clearing of the warnings
		// because it is cleared by the FB warning.
		if request.GetPvt() {
			errToReturn = manager.processPvtFBError(request)
		} else {
			errToReturn = manager.processFBError(request)
		}
	}

	for _, warnings := range warningFlagsForAxes {
		if errToReturn == nil {
			errCtor := errors.ErrStreamMovementFailed
			if request.GetPvt() {
				errCtor = errors.ErrPvtMovementFailed
			}
			errToReturn = CheckErrorFlags(warnings, MovementErrorWarningFlags, errCtor)
		}
	}
	for _, warnings := range warningFlagsForAxes {
		if errToReturn == nil {
			errCtor := errors.ErrStreamMovementInterrupted
			if request.GetPvt() {
				errCtor = errors.ErrPvtMovementInterrupted
			}
			errToReturn = CheckErrorFlags(warnings, MovementInterruptedWarningFlags, errCtor)
		}
	}
	if options.checkDiscontinuities {
		for _, warnings := range warningFlagsForAxes {
			if errToReturn == nil && warnings.Has("ND") {
				if stream.pvt {
					errToReturn = errors.ErrPvtDiscontinuity()
				} else {
					errToReturn = errors.ErrStreamDiscontinuity()
				}
			}
		}
	}

	hasError := errToReturn != nil
	if hasError {
		for _, axisNumber := range stream.physicalAxesNumbers {
			if _, _, err := GetAxisWarnings(requests, request.GetDevice(), axisNumber, true); err != nil {
				return err
			}
		}
	}

	return errToReturn
}

func (manager *streamManager) streamRequest(target streamTarget, streamCommand string) (*c.Response, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(target)
	if err != nil {
		return nil, err
	}

	deviceCommand := fmt.Sprintf("%s %d %s", getStreamPrefix(target), target.GetStreamId(), streamCommand)

	reply, err := singleCommandDevice(requests, target, deviceCommand)
	if err != nil {
		return reply, err
	}

	return reply, nil
}

func (manager *streamManager) streamRequestMulti(target streamTarget, streamCommand string) ([]*c.Response, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(target)
	if err != nil {
		return nil, err
	}

	deviceCommand := fmt.Sprintf("%s %d %s", getStreamPrefix(target), target.GetStreamId(), streamCommand)

	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(target.GetDevice()),
		Command: deviceCommand,
	})
	if err != nil {
		return replies, err
	}

	return replies, nil
}

func (manager *streamManager) streamActionRequest(target streamTarget, stream *stream, streamCommand string) (*c.Response, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(target)
	if err != nil {
		return nil, err
	}

	deviceCommand := fmt.Sprintf("%s %d %s", getStreamPrefix(target), target.GetStreamId(), streamCommand)

	reply, errRequest := singleCommandDevice(requests, target, deviceCommand)
	for commands.IsAgainErr(errRequest) {
		time.Sleep(0)
		reply, errRequest = singleCommandDevice(requests, target, deviceCommand)
	}

	isBusy := reply != nil && reply.Status == "BUSY"

	checkDiscontinuities := false
	// Only check for discontinuity when the recent command made the device BUSY.
	if isBusy && stream.runtime.discontinuitiesAsError {
		checkDiscontinuities = !stream.runtime.ignoreCurrentDiscontinuity
		stream.runtime.ignoreCurrentDiscontinuity = false
	}

	var errWarnings errors.SdkError
	if reply != nil && commands.HasWarnings(reply) {
		errWarnings = manager.checkAndClearStreamWarnings(requests, target, stream, checkStreamWarningsOptions{
			checkDiscontinuities: checkDiscontinuities,
		})
	}

	var errStreamDisabled errors.SdkError
	if commands.IsBadDataErr(errRequest) {
		if isDisabled, err := manager.checkIfStreamDisabled(target, stream); err != nil {
			errStreamDisabled = err
		} else if isDisabled {
			message := fmt.Sprintf("The %s was disabled during operation. Setup the %s again.", getStreamTextWord(target), getStreamTextWord(target))
			if target.GetPvt() {
				errStreamDisabled = errors.ErrPvtMode(message)
			} else {
				errStreamDisabled = errors.ErrStreamMode(message)
			}
		}
	}

	if errWarnings != nil {
		return reply, errWarnings
	} else if errStreamDisabled != nil {
		return reply, errStreamDisabled
	} else if errRequest != nil {
		return reply, errRequest
	}

	return reply, nil
}

func (manager *streamManager) checkIfStreamDisabled(target streamTarget, stream *stream) (bool, errors.SdkError) {
	var isDisabled bool
	if target.GetPvt() {
		streamInfo, err := manager.requestPvtInfo(target)
		if err != nil {
			return false, err
		}
		isDisabled = streamInfo.mode == "disabled"
	} else {
		streamInfo, err := manager.requestStreamInfo(target)
		if err != nil {
			return false, err
		}
		isDisabled = streamInfo.mode == "disabled"
	}
	if isDisabled && stream.mode != StreamModeDisabled {
		stream.setModeDisabled()
	}
	return isDisabled, nil
}

type streamAction struct {
	// fully formed command
	command string

	// template and arguments if the command above is empty
	template  []string
	arguments []streamArg
}

type streamActionOptions struct {
	// checks that stream is in one of those modes
	modeCheck []StreamMode
	// causes PVT buffer to get reset after it's sent
	resetBuffer bool
}

func (manager *streamManager) streamActions(target streamTarget, commands []streamAction, options streamActionOptions) errors.SdkError {
	stream, err := manager.getStream(target, true)
	if err != nil {
		return err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	if len(options.modeCheck) > 0 {
		if err := stream.assureMode(options.modeCheck...); err != nil {
			return err
		}
	}

	for i, command := range commands {
		if command.command == "" {
			builtCommand, err := manager.buildStreamCommand(target, stream, command.template, command.arguments)
			if err != nil {
				return err
			}
			commands[i].command = builtCommand
		}
	}

	if stream.pvt {
		pvtData := stream.pvtData

		if options.resetBuffer && pvtData.hasUnsentActions() {
			return errors.ErrInvalidOperation(strings.Join([]string{
				"Cannot perform the action because the PVT sequence has queued points for calculation.",
				"The action could potentially result in an incorrect result of the calculation.",
			}, " "))
		}

		for _, command := range commands {
			pvtData.actionBuffer = append(pvtData.actionBuffer, &pvtBufferedAction{
				resetBuffer: options.resetBuffer,
				command:     command.command,
			})
		}
		return manager.flushPvtBuffer(target, stream)
	}

	for _, command := range commands {
		if _, err := manager.streamActionRequest(target, stream, command.command); err != nil {
			return err
		}
	}
	return nil
}

func (manager *streamManager) streamAction(
	target streamTarget,
	commandTemplate []string,
	arguments []streamArg,
	options streamActionOptions,
) errors.SdkError {
	return manager.streamActions(target, []streamAction{{
		template:  commandTemplate,
		arguments: arguments,
	}}, options)
}

func (manager *streamManager) streamActionBasic(
	target streamTarget,
	streamCommand string,
	options streamActionOptions,
) errors.SdkError {
	return manager.streamActions(target, []streamAction{{
		command: streamCommand,
	}}, options)
}

func (manager *streamManager) streamActionsBasic(
	target streamTarget,
	streamCommands []string,
	options streamActionOptions,
) errors.SdkError {
	actions := pie.Map(streamCommands, func(command string) streamAction {
		return streamAction{
			command: command,
		}
	})
	return manager.streamActions(target, actions, options)
}

type streamDirectCommandOptions struct {
	// checks that stream is in one of those modes
	modeCheck []StreamMode
}

func checkDirectCommandOptions(stream *stream, options streamDirectCommandOptions) errors.SdkError {
	stream.dataLock.Lock()
	defer stream.dataLock.Unlock()

	if len(options.modeCheck) > 0 {
		if err := stream.assureMode(options.modeCheck...); err != nil {
			return err
		}
	}

	return nil
}

// Sends a stream command directly only doing the mode check if possible and not locking the main stream lock.
// Use for commands that can be sent any time, don't queue up segments, and don't require unit conversions.
func (manager *streamManager) streamDirectCommand(target streamTarget, command string, options streamDirectCommandOptions) errors.SdkError {
	stream, err := manager.getStream(target, false)
	if err != nil {
		return err
	}

	if stream != nil {
		if err := checkDirectCommandOptions(stream, options); err != nil {
			return err
		}
	}

	requests, err := manager.deviceManager.getRequests(target)
	if err != nil {
		return err
	}

	deviceCommand := fmt.Sprintf("%s %d %s", getStreamPrefix(target), target.GetStreamId(), command)

	_, err = singleCommandDevice(requests, target, deviceCommand)
	return err
}
