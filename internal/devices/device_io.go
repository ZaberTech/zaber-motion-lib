package devices

import (
	"fmt"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/dto"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"
)

var toBoolMap = map[int]bool{
	0: false,
	1: true,
}

var fromBoolMap = map[bool]string{
	false: "0",
	true:  "1",
}

var fromEnumMap = map[pb.AsciiDigitalOutputAction]string{
	0: "0",
	1: "1",
	2: "t",
	3: "k",
}

func (manager *deviceManager) getDigitalIO(request *pb.DeviceGetDigitalIORequest) (*pb.BoolResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, fmt.Sprintf(`io get %s %d`, request.GetChannelType(), request.GetChannelNumber()))
	if err != nil {
		if commands.IsBadDataErr(err) {
			return nil, errors.ErrChannelOutOfRange(request.GetChannelNumber(), request.GetChannelType())
		}
		return nil, err
	}

	intValue, err := reply.DataAsInt()
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: toBoolMap[intValue],
	}, nil
}

func (manager *deviceManager) getAllDigitalIO(request *pb.DeviceGetAllDigitalIORequest) (*pb.DeviceGetAllDigitalIOResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, fmt.Sprintf(`io get %s`, request.GetChannelType()))
	if err != nil {
		return nil, err
	}

	intValues, err := reply.DataAsIntArray(false)
	if err != nil {
		return nil, err
	}

	var boolValues = make([]bool, len(intValues))
	for i, value := range intValues {
		boolValues[i] = toBoolMap[int(value)]
	}

	return &pb.DeviceGetAllDigitalIOResponse{
		Values: boolValues,
	}, nil
}

func (manager *deviceManager) getAnalogIO(request *pb.DeviceGetAnalogIORequest) (*pb.DoubleResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, fmt.Sprintf(`io get %s %d`, request.GetChannelType(), request.GetChannelNumber()))
	if err != nil {
		if commands.IsBadDataErr(err) {
			return nil, errors.ErrChannelOutOfRange(request.GetChannelNumber(), request.GetChannelType())
		}
		return nil, err
	}

	value, err := reply.DataAsFloat()
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (manager *deviceManager) getAllAnalogIO(request *pb.DeviceGetAllAnalogIORequest) (*pb.DeviceGetAllAnalogIOResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, fmt.Sprintf(`io get %s`, request.GetChannelType()))
	if err != nil {
		return nil, err
	}

	values, err := reply.DataAsFloatArray(false)
	if err != nil {
		return nil, err
	}

	return &pb.DeviceGetAllAnalogIOResponse{
		Values: values,
	}, nil
}

func (manager *deviceManager) buildSetDigitalOutputCommand(request *pb.DeviceSetDigitalOutputRequest) string {
	value := fromEnumMap[request.GetValue()]

	return fmt.Sprintf("io set do %d %s", request.GetChannelNumber(), value)
}

func (manager *deviceManager) setDigitalOutput(request *pb.DeviceSetDigitalOutputRequest) errors.SdkError {
	command := manager.buildSetDigitalOutputCommand(request)

	_, err := manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrChannelOutOfRange(request.GetChannelNumber(), "do")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) buildSetAllDigitalOutputsCommand(request *pb.DeviceSetAllDigitalOutputsRequest) string {
	rawValues := request.GetValues()
	values := make([]string, len(rawValues))
	for i, value := range rawValues {
		values[i] = fromEnumMap[value]
	}

	params := strings.Join(values, " ")

	return fmt.Sprintf("io set do port %s", params)
}

func (manager *deviceManager) setAllDigitalOutputs(request *pb.DeviceSetAllDigitalOutputsRequest) errors.SdkError {
	command := manager.buildSetAllDigitalOutputsCommand(request)

	_, err := manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidArgument("Values array length must be equal to the number of digital output channels on this device.")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) buildSetDigitalOutputScheduleCommand(request *pb.DeviceSetDigitalOutputScheduleRequest) (string, errors.SdkError) {
	commandTemplate := []string{
		"io",
		"set",
		"do",
		fmt.Sprint(request.ChannelNumber),
		fromEnumMap[request.Value],
		"schedule",
		constants.CommandArgument,
		fromEnumMap[request.FutureValue],
	}

	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Delay, Unit: request.Unit}}

	command, err := manager.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) SetDigitalOutputSchedule(request *pb.DeviceSetDigitalOutputScheduleRequest) errors.SdkError {
	command, err := manager.buildSetDigitalOutputScheduleCommand(request)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrChannelOutOfRange(request.GetChannelNumber(), "do")
		}
		return err
	}

	return nil
}

func (manager *deviceManager) buildSetAllDigitalOutputsScheduleCommand(request *pb.DeviceSetAllDigitalOutputsScheduleRequest) (string, errors.SdkError) {
	if len(request.Values) != len(request.FutureValues) {
		return "", errors.ErrInvalidArgument("Values and FutureValues array length must be equal.")
	}

	rawValues := request.Values
	values := make([]string, len(rawValues))
	for i, value := range rawValues {
		values[i] = fromEnumMap[value]
	}
	rawFutureValues := request.FutureValues
	futureValues := make([]string, len(rawFutureValues))
	for i, futureValue := range rawFutureValues {
		futureValues[i] = fromEnumMap[futureValue]
	}

	commandTemplate := []string{
		"io",
		"set",
		"do",
		"port",
	}
	commandTemplate = append(commandTemplate, values...)
	commandTemplate = append(commandTemplate, "schedule", constants.CommandArgument)
	commandTemplate = append(commandTemplate, futureValues...)

	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Delay, Unit: request.Unit}}

	command, err := manager.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) setAllDigitalOutputsSchedule(request *pb.DeviceSetAllDigitalOutputsScheduleRequest) errors.SdkError {
	command, err := manager.buildSetAllDigitalOutputsScheduleCommand(request)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidArgument("Values and FutureValues array length must be equal to the number of digital output channels on this device.")
		}
		return err
	}

	return nil
}

func (manager *deviceManager) buildSetAnalogOutputCommand(request *pb.DeviceSetAnalogOutputRequest) (string, errors.SdkError) {
	commandTemplate := []string{"io", "set", "ao", constants.CommandArgument, constants.CommandArgument}
	args := []commandbuilding.CommandArg{
		commandbuilding.CommandArgImpl{Value: float64(request.GetChannelNumber()), Unit: constants.NativeUnit},
		commandbuilding.CommandArgImpl{Value: request.GetValue(), Unit: constants.NativeUnit},
	}
	target := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
	}

	command, err := manager.buildCommand(target, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) setAnalogOutput(request *pb.DeviceSetAnalogOutputRequest) errors.SdkError {
	command, err := manager.buildSetAnalogOutputCommand(request)
	if err != nil {
		return err
	}

	if _, err := manager.singleRequestDevice(request, command); err != nil {
		return err
	}
	return nil
}

func (manager *deviceManager) buildSetAllAnalogOutputsCommand(request *pb.DeviceSetAllAnalogOutputsRequest) (string, errors.SdkError) {
	values := request.GetValues()

	commandTemplate := []string{"io", "set", "ao", "port"}
	args := make([]commandbuilding.CommandArg, len(values))
	for i, value := range values {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		args[i] = commandbuilding.CommandArgImpl{
			Value: value,
			Unit:  constants.NativeUnit,
		}
	}
	target := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
	}

	command, err := manager.buildCommand(target, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) setAllAnalogOutputs(request *pb.DeviceSetAllAnalogOutputsRequest) errors.SdkError {
	command, err := manager.buildSetAllAnalogOutputsCommand(request)
	if err != nil {
		return err
	}

	if _, err := manager.singleRequestDevice(request, command); err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidArgument("Values array length must be equal to the number of analog output channels on this device.")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) buildSetAnalogOutputScheduleCommand(request *pb.DeviceSetAnalogOutputScheduleRequest) (string, errors.SdkError) {
	commandTemplate := []string{
		"io",
		"set",
		"ao",
		fmt.Sprint(request.ChannelNumber),
		fmt.Sprint(request.Value),
		"schedule",
		constants.CommandArgument,
		fmt.Sprint(request.FutureValue),
	}

	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Delay, Unit: request.Unit}}

	command, err := manager.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) setAnalogOutputSchedule(request *pb.DeviceSetAnalogOutputScheduleRequest) errors.SdkError {
	command, err := manager.buildSetAnalogOutputScheduleCommand(request)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrChannelOutOfRange(request.GetChannelNumber(), "ao")
		}
		return err
	}

	return nil
}

func (manager *deviceManager) buildSetAllAnalogOutputsScheduleCommand(request *pb.DeviceSetAllAnalogOutputsScheduleRequest) (string, errors.SdkError) {
	if len(request.Values) != len(request.FutureValues) {
		return "", errors.ErrInvalidArgument("Values and FutureValues array length must be equal.")
	}

	rawValues := request.Values
	values := make([]string, len(rawValues))
	for i, value := range rawValues {
		values[i] = fmt.Sprint(value)
	}
	rawFutureValues := request.FutureValues
	futureValues := make([]string, len(rawFutureValues))
	for i, futureValue := range rawFutureValues {
		futureValues[i] = fmt.Sprint(futureValue)
	}

	commandTemplate := []string{
		"io",
		"set",
		"ao",
		"port",
	}
	commandTemplate = append(commandTemplate, values...)
	commandTemplate = append(commandTemplate, "schedule", constants.CommandArgument)
	commandTemplate = append(commandTemplate, futureValues...)

	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Delay, Unit: request.Unit}}

	command, err := manager.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return "", err
	}

	return command, nil
}

func (manager *deviceManager) setAllAnalogOutputsSchedule(request *pb.DeviceSetAllAnalogOutputsScheduleRequest) errors.SdkError {
	command, err := manager.buildSetAllAnalogOutputsScheduleCommand(request)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidArgument("Values and FutureValues array length must be equal to the number of analog output channels on this device.")
		}
		return err
	}

	return nil
}

func (manager *deviceManager) setLowpassFilter(request *pb.DeviceSetLowpassFilterRequest) errors.SdkError {
	commandTemplate := []string{
		"set",
		fmt.Sprintf("io.ai.%d.fc", request.ChannelNumber),
		constants.CommandArgument,
	}
	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.CutoffFrequency, Unit: request.Unit}}

	command, err := manager.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	return err
}

func (manager *deviceManager) buildCancelOutputScheduleCommand(request *pb.DeviceCancelOutputScheduleRequest) string {
	ioType := "do"
	if request.Analog {
		ioType = "ao"
	}

	return fmt.Sprintf("io cancel schedule %s %d", ioType, request.ChannelNumber)
}

func (manager *deviceManager) cancelOutputSchedule(request *pb.DeviceCancelOutputScheduleRequest) errors.SdkError {
	command := manager.buildCancelOutputScheduleCommand(request)

	_, err := manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrChannelOutOfRange(request.GetChannelNumber(), "do")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) buildCancelAllOutputsScheduleCommand(request *pb.DeviceCancelAllOutputsScheduleRequest) string {
	ioType := "do"

	if request.Analog {
		ioType = "ao"
	}

	channels := make([]string, len(request.Channels))
	for i, boolChannel := range request.Channels {
		channels[i] = fromBoolMap[boolChannel]
	}

	params := ""
	if len(channels) > 0 {
		params = " " + strings.Join(channels, " ")
	}

	return fmt.Sprintf("io cancel schedule %s port%s", ioType, params)
}

func (manager *deviceManager) cancelAllOutputsSchedule(request *pb.DeviceCancelAllOutputsScheduleRequest) errors.SdkError {
	command := manager.buildCancelAllOutputsScheduleCommand(request)

	_, err := manager.singleRequestDevice(request, command)
	if err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidArgument("Channel array length must be 0 or equal to the number of digital output channels on this device.")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) getNumberOfChannels(request *pb.DeviceEmptyRequest) (*pb.AsciiDeviceIOInfo, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, "io info")
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return &pb.AsciiDeviceIOInfo{}, nil
		}
		return nil, err
	}

	values, err := reply.DataAsIntArray(false)
	if err != nil {
		return nil, err
	}

	return &pb.AsciiDeviceIOInfo{
		NumberAnalogOutputs:  int32(values[0]),
		NumberAnalogInputs:   int32(values[1]),
		NumberDigitalOutputs: int32(values[2]),
		NumberDigitalInputs:  int32(values[3]),
	}, nil
}

const ioLabelStorageKeyPrefix = "zaber.label.io"

func ioLabelKey(portType dto.AsciiIoPortType, channelNumber int32) (string, errors.SdkError) {
	firmwarePort, err := utils.IoPortTypeToFirmwarePort(portType)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s.%s.%d", ioLabelStorageKeyPrefix, firmwarePort, channelNumber), nil
}

func (manager *deviceManager) setIoPortLabel(request *pb.SetIoPortLabelRequest) errors.SdkError {
	key, err := ioLabelKey(request.PortType, request.ChannelNumber)
	if err != nil {
		return err
	}
	if utils.PtrDefault(request.Label, "") == "" {
		_, err := manager.storage.Erase(targetAxis(request, 0), key)
		return err
	} else {
		return manager.storage.SetStorage(targetAxis(request, 0), key, *request.Label, true)
	}
}

func (manager *deviceManager) getIoPortLabel(request *pb.GetIoPortLabelRequest) (*pb.StringResponse, errors.SdkError) {
	key, err := ioLabelKey(request.PortType, request.ChannelNumber)
	if err != nil {
		return nil, err
	}
	value, err := manager.storage.GetStorage(targetAxis(request, 0), key, true)
	if err != nil {
		return nil, err
	}
	return &pb.StringResponse{
		Value: value,
	}, nil
}

func (manager *deviceManager) getAllIoPortLabels(request *pb.DeviceEmptyRequest) (*pb.GetAllIoPortLabelsResponse, errors.SdkError) {
	keyedLabels, err := manager.storage.ListKeyValuePairs(&axisTargetImpl{
		interfaceID: request.InterfaceId,
		device:      request.Device,
	}, ioLabelStorageKeyPrefix, true)
	if err != nil {
		return nil, err
	}
	labels := make([]*dto.AsciiIoPortLabel, 0, len(keyedLabels))
	for key, label := range keyedLabels {
		parts := strings.Split(key, ".")
		portType, err := utils.FirmwarePortToIoPortType(parts[3])
		if err != nil {
			continue
		}
		channelNumber, convErr := strconv.Atoi((parts[4]))
		if convErr != nil {
			continue
		}
		labels = append(labels, &dto.AsciiIoPortLabel{
			PortType:      portType,
			ChannelNumber: int32(channelNumber),
			Label:         string(label),
		})
	}
	return &pb.GetAllIoPortLabelsResponse{
		Labels: labels,
	}, nil
}
