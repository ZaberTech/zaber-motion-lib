package devices

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/go-gl/mathgl/mgl64"

	"github.com/elliotchance/pie/v2"
)

type pvtStream struct {
	// whether the stream was setup on identified device
	identified bool

	axisCIIs  []*ioc.CommandIssuingInfo
	deviceCII *ioc.CommandIssuingInfo

	position []*dimensionAndUnit
	velocity []*dimensionAndUnit
	time     *dimensionAndUnit

	actionBuffer []*pvtBufferedAction
}

type pvtBufferedAction struct {
	// whether the point/command was already sent to the device
	sent bool
	// resets the entire buffer after this action
	resetBuffer bool

	// either
	point *pvtPoint
	// or
	command string
}

type pvtPoint struct {
	abs      bool
	position []*streamArgImpl
	velocity []*streamArgImpl
	time     *streamArgImpl
}

type dimensionAndUnit struct {
	dimension ioc.ContextualDimensionID
	baseUnit  string
}

func (manager *streamManager) initPvtStream(device *deviceInfo, stream *streamData, request streamTarget) (*pvtStream, errors.SdkError) {
	pvtData := &pvtStream{}

	if !device.isIdentified() {
		return pvtData, nil
	}
	pvtData.identified = true

	for _, axis := range stream.axes {
		axisTarget := &axisTargetImpl{
			interfaceID: request.GetInterfaceId(),
			device:      request.GetDevice(),
			axis:        axis.primaryAxisNumber,
		}

		commandIssuingInfo, err := device.GetCommandIssuingInfo(axisTarget)
		if err != nil {
			return nil, err
		}
		pvtData.axisCIIs = append(pvtData.axisCIIs, commandIssuingInfo)

		posInfo, err := commandbuilding.GetSettingInfo(commandIssuingInfo, "pos")
		if err != nil {
			return nil, err
		}
		positionUnits, err := commandIssuingInfo.ConversionTable.GetBaseUnits(posInfo.ContextualDimensionID)
		if err != nil {
			return nil, err
		}
		pvtData.position = append(pvtData.position, &dimensionAndUnit{
			dimension: posInfo.ContextualDimensionID,
			baseUnit:  positionUnits,
		})

		velInfo, err := commandbuilding.GetSettingInfo(commandIssuingInfo, "maxspeed")
		if err != nil {
			return nil, err
		}
		velocityUnits, err := commandIssuingInfo.ConversionTable.GetBaseUnits(velInfo.ContextualDimensionID)
		if err != nil {
			return nil, err
		}
		pvtData.velocity = append(pvtData.velocity, &dimensionAndUnit{
			dimension: velInfo.ContextualDimensionID,
			baseUnit:  velocityUnits,
		})
	}

	{
		target := &axisTargetImpl{
			interfaceID: request.GetInterfaceId(),
			device:      request.GetDevice(),
		}

		commandIssuingInfo, err := device.GetCommandIssuingInfo(target)
		if err != nil {
			return nil, err
		}
		pvtData.deviceCII = commandIssuingInfo

		paramsInfo, found := commandIssuingInfo.CommandTree.GetParamsInfo([]string{
			"pvt", constants.CommandArgument,
			"point", "abs",
			"p", constants.CommandArgument,
			"v", constants.CommandArgument,
			"t", constants.CommandArgument,
		})
		if !found {
			return nil, errors.ErrNotSupported("PVT is not supported on this device.")
		}

		timeDimension := paramsInfo[3].ContextualDimensionID
		timeUnits, err := commandIssuingInfo.ConversionTable.GetBaseUnits(timeDimension)
		if err != nil {
			return nil, err
		}

		pvtData.time = &dimensionAndUnit{
			dimension: timeDimension,
			baseUnit:  timeUnits,
		}
	}

	return pvtData, nil
}

func (manager *streamManager) point(request *pb.PvtPointRequest) errors.SdkError {
	stream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err = stream.assureMode(StreamModeLive, StreamModeStore); err != nil {
		return err
	}

	pvtData := stream.pvtData

	if len(request.Positions) != stream.axesCount {
		return errors.ErrInvalidArgument("The number of positions does not equal to number of axes.")
	}

	if !pvtData.identified {
		point, err := manager.newPvtPoint(request, stream)
		if err != nil {
			return err
		}
		return manager.sendPvtPoint(request, stream, point)
	}

	point, err := manager.newPvtPointBaseUnits(request, stream)
	if err != nil {
		return err
	}
	pvtData.actionBuffer = append(pvtData.actionBuffer, &pvtBufferedAction{
		point: point,
	})

	return manager.flushPvtBuffer(request, stream)
}

func (manager *streamManager) newPvtPointBaseUnits(request *pb.PvtPointRequest, stream *stream) (*pvtPoint, errors.SdkError) {
	pvtData := stream.pvtData
	positions := request.Positions
	velocities := request.Velocities

	if len(velocities) != 0 && len(velocities) != len(stream.axes) {
		return nil, errors.ErrInvalidArgument("The number of velocities does not equal to number of axes.")
	}

	point := &pvtPoint{
		abs: request.Type == pb.StreamSegmentType_ABS,
	}

	for axisIndex := range stream.axes {
		position, err := commandbuilding.ConvertToBaseUnits(pvtData.axisCIIs[axisIndex], positions[axisIndex], pvtData.position[axisIndex].dimension)
		if err != nil {
			return nil, err
		}

		point.position = append(point.position, &streamArgImpl{
			axisIndex:      int32(axisIndex),
			settingName:    "pos",
			CommandArgImpl: *position,
		})

		if len(velocities) != 0 && velocities[axisIndex] != nil {
			velocity, err := commandbuilding.ConvertToBaseUnits(pvtData.axisCIIs[axisIndex], velocities[axisIndex], pvtData.velocity[axisIndex].dimension)
			if err != nil {
				return nil, err
			}

			point.velocity = append(point.velocity, &streamArgImpl{
				axisIndex:      int32(axisIndex),
				settingName:    "maxspeed",
				CommandArgImpl: *velocity,
			})
		} else {
			point.velocity = append(point.velocity, nil)
		}
	}

	time, err := commandbuilding.ConvertToBaseUnits(pvtData.deviceCII, request.Time, pvtData.time.dimension)
	if err != nil {
		return nil, err
	}

	point.time = &streamArgImpl{
		axisIndex:      -1,
		CommandArgImpl: *time,
	}

	return point, nil
}

func (manager *streamManager) newPvtPoint(request *pb.PvtPointRequest, stream *stream) (*pvtPoint, errors.SdkError) {
	if len(request.Velocities) > 0 && len(request.Velocities) != len(stream.axes) {
		return nil, errors.ErrInvalidArgument("The number of velocities does not equal to number of axes.")
	} else if len(request.Velocities) == 0 || pie.Any(request.Velocities, func(value *pb.Measurement) bool {
		return value == nil
	}) {
		return nil, errors.ErrInvalidArgument("Stream was setup on non-identified device. Velocity calculation is not available.")
	}

	point := &pvtPoint{
		abs: request.Type == pb.StreamSegmentType_ABS,
	}

	for axisIndex := range stream.axes {
		position := request.Positions[axisIndex]
		velocity := request.Velocities[axisIndex]

		point.position = append(point.position, &streamArgImpl{
			axisIndex:   int32(axisIndex),
			settingName: "pos",
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: position.Value,
				Unit:  position.Unit,
			},
		})

		point.velocity = append(point.velocity, &streamArgImpl{
			axisIndex:   int32(axisIndex),
			settingName: "maxspeed",
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: velocity.Value,
				Unit:  velocity.Unit,
			},
		})
	}

	point.time = &streamArgImpl{
		axisIndex: -1,
		CommandArgImpl: commandbuilding.CommandArgImpl{
			Value: request.Time.Value,
			Unit:  request.Time.Unit,
		},
	}

	return point, nil
}

func (manager *streamManager) flushPvtBuffer(request streamTarget, stream *stream) errors.SdkError {
	pvtData := stream.pvtData

	for bufferIndex := 0; bufferIndex < len(pvtData.actionBuffer); bufferIndex++ {
		action := pvtData.actionBuffer[bufferIndex]
		if action.sent {
			continue
		}

		if action.point != nil {
			point := action.point

			if pie.Any(point.velocity, func(arg *streamArgImpl) bool {
				return arg == nil
			}) {
				if done, err := pvtData.calculatePointVelocities(bufferIndex); err != nil {
					return pvtData.handlePvtError(bufferIndex, err)
				} else if !done {
					break
				}
			}

			if err := manager.sendPvtPoint(request, stream, point); err != nil {
				return pvtData.handlePvtError(bufferIndex, err)
			}
		} else {
			if _, err := manager.streamActionRequest(request, stream, action.command); err != nil {
				return pvtData.handlePvtError(bufferIndex, err)
			}
		}

		action.sent = true
		if action.resetBuffer {
			pvtData.actionBuffer = pvtData.actionBuffer[bufferIndex+1:]
			bufferIndex = -1
		}
	}

	pvtData.flushSentActions()

	return nil
}

func (pvtData *pvtStream) handlePvtError(bufferIndex int, err errors.SdkError) errors.SdkError {
	if canRecover := pie.Contains([]pb.Errors{
		pb.Errors_BAD_COMMAND,
		pb.Errors_BAD_DATA,
		pb.Errors_COMMAND_FAILED,
		pb.Errors_DRIVER_DISABLED,
		pb.Errors_INVALID_ARGUMENT,
		pb.Errors_INVALID_OPERATION,
		pb.Errors_REMOTE_MODE,
	}, err.Type()); canRecover {
		discardedActions := len(pvtData.actionBuffer) - bufferIndex - 1
		pvtData.actionBuffer = pvtData.actionBuffer[:bufferIndex]

		if discardedActions > 0 {
			err.AddToMessage(strings.Join([]string{
				"The error was partially or completely caused by previously submitted PVT action.",
				fmt.Sprintf("The action itself and %d actions submitted afterward were discarded.", discardedActions),
			}, " "))
		}
	} else {
		pvtData.actionBuffer = nil
	}

	return err
}

func (pvtData *pvtStream) flushSentActions() {
	for len(pie.Filter(pvtData.actionBuffer, func(action *pvtBufferedAction) bool {
		return action.point != nil && action.sent
	})) > 1 {
		pvtData.actionBuffer = pvtData.actionBuffer[1:]
	}
}

func (pvtData *pvtStream) findNextPoint(currentIndex int) *pvtPoint {
	index := pie.FindFirstUsing(pvtData.actionBuffer[currentIndex+1:], func(action *pvtBufferedAction) bool {
		return action.point != nil
	})
	if index < 0 {
		return nil
	}
	return pvtData.actionBuffer[currentIndex+1+index].point
}

func (pvtData *pvtStream) findPreviousPoint(currentIndex int) *pvtPoint {
	pointsOnly := pie.Filter(pvtData.actionBuffer[:currentIndex], func(action *pvtBufferedAction) bool {
		return action.point != nil
	})
	if len(pointsOnly) == 0 {
		return nil
	}
	return pie.Last(pointsOnly).point
}

func (pvtData *pvtStream) calculatePointVelocities(i int) (done bool, err errors.SdkError) {
	action := pvtData.actionBuffer[i]
	point := action.point

	var diffPrevious *mgl64.VecN
	var diffNext *mgl64.VecN

	if point.abs {
		previousPoint := pvtData.findPreviousPoint(i)
		if previousPoint == nil {
			return false, errors.ErrInvalidArgument("The first submitted absolute point must have defined velocity.")
		} else if !previousPoint.abs {
			return false, errors.ErrInvalidOperation("Cannot calculate velocity of an absolute point because the previous point is relative.")
		}

		prevPos := mgl64.NewVecNFromData(streamArgsToVector(previousPoint.position))
		currentPos := mgl64.NewVecNFromData(streamArgsToVector(point.position))
		diffPrevious = currentPos.Sub(nil, prevPos)
	} else {
		diffPrevious = mgl64.NewVecNFromData(streamArgsToVector(point.position))
	}

	nextPoint := pvtData.findNextPoint(i)
	if nextPoint == nil {
		return false, nil
	}

	if nextPoint.abs {
		if !point.abs {
			return false, errors.ErrInvalidOperation("Cannot calculate velocity of the previous relative point because the current point is absolute.")
		}
		currentPos := mgl64.NewVecNFromData(streamArgsToVector(point.position))
		nextPos := mgl64.NewVecNFromData(streamArgsToVector(nextPoint.position))
		diffNext = nextPos.Sub(nil, currentPos)
	} else {
		diffNext = mgl64.NewVecNFromData(streamArgsToVector(nextPoint.position))
	}

	if point.time.GetValue() == 0 || nextPoint.time.GetValue() == 0 {
		return false, errors.ErrInvalidArgument("Cannot calculate velocity for points with zero time.")
	}

	diffPrevious.Mul(diffPrevious, 1.0/point.time.GetValue())
	diffNext.Mul(diffNext, 1.0/nextPoint.time.GetValue())

	calculatedVelocity := diffPrevious.Add(nil, diffNext).Mul(nil, 0.5)

	for axisIndex, arg := range point.velocity {
		if arg != nil {
			continue
		}
		point.velocity[axisIndex] = &streamArgImpl{
			axisIndex:   int32(axisIndex),
			settingName: "maxspeed",
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: calculatedVelocity.Get(axisIndex),
				Unit:  pvtData.velocity[axisIndex].baseUnit,
			},
		}
	}

	return true, nil
}

func (pvtData *pvtStream) hasUnsentActions() bool {
	return pie.Any(pvtData.actionBuffer, func(action *pvtBufferedAction) bool {
		return !action.sent
	})
}

func (manager *streamManager) sendPvtPoint(request streamTarget, stream *stream, point *pvtPoint) errors.SdkError {
	var moveArgs []streamArg

	commandTemplate := []string{"point"}
	if point.abs {
		commandTemplate = append(commandTemplate, "abs")
	} else {
		commandTemplate = append(commandTemplate, "rel")
	}

	commandTemplate = append(commandTemplate, "p")
	for _, position := range point.position {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		moveArgs = append(moveArgs, position)
	}

	commandTemplate = append(commandTemplate, "v")
	for _, velocity := range point.velocity {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		moveArgs = append(moveArgs, velocity)
	}

	commandTemplate = append(commandTemplate, "t")
	commandTemplate = append(commandTemplate, constants.CommandArgument)
	moveArgs = append(moveArgs, point.time)

	command, err := manager.buildStreamCommand(request, stream, commandTemplate, moveArgs)
	if err != nil {
		return err
	}

	_, err = manager.streamActionRequest(request, stream, command)
	return err
}

func (manager *streamManager) populatePvtActionBufferFromPosition(stream *stream, target streamTarget) errors.SdkError {
	positions, err := manager.getStreamAxisSetting(stream, target, "pos", true)
	if err != nil {
		return err
	}

	point, err := manager.newPvtPointBaseUnits(&pb.PvtPointRequest{
		Type: pb.StreamSegmentType_ABS,
		Positions: pie.Map(positions, func(position float64) *pb.Measurement {
			return &pb.Measurement{Value: position}
		}),
		Velocities: pie.Map(positions, func(_ float64) *pb.Measurement {
			return &pb.Measurement{}
		}),
		Time: &pb.Measurement{},
	}, stream)
	if err != nil {
		return err
	}

	stream.pvtData.actionBuffer = append(stream.pvtData.actionBuffer, &pvtBufferedAction{
		sent:  true,
		point: point,
	})

	return nil
}

func streamArgsToVector(args []*streamArgImpl) []float64 {
	return pie.Map(args, func(arg *streamArgImpl) float64 { return arg.Value })
}
