package devices

import (
	"fmt"
	"regexp"
	"strings"
	"time"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	pie "github.com/elliotchance/pie/v2"
)

type WarningsFlags struct {
	Ordered []string
	Check   map[string]bool
}

func GetAxisWarnings(requests *c.RequestManager, device int32, axis int32, clearWarnings bool) (*WarningsFlags, *c.Response, errors.SdkError) {
	cmd := c.Command{
		Device:  int(device),
		Axis:    int(axis),
		Command: "warnings",
	}
	if clearWarnings {
		cmd.Command += " clear"
	}

	reply, err := commands.SingleCommand(requests, cmd)
	if err != nil {
		return nil, nil, err
	}
	warningsReplyParts := strings.Split(reply.Data, " ")
	warningCount := len(warningsReplyParts) - 1 // first is count
	if warningCount == 0 {
		return nil, reply, nil
	}

	flags := &WarningsFlags{
		Check:   make(map[string]bool),
		Ordered: make([]string, warningCount),
	}
	for i := 0; i < warningCount; i++ {
		flags.Ordered[i] = warningsReplyParts[i+1]
		flags.Check[warningsReplyParts[i+1]] = true
	}

	return flags, reply, nil
}

func GetResponseWarnings(requests *c.RequestManager, basedOn *c.Response, clearWarnings bool) (*WarningsFlags, errors.SdkError) {
	if commands.HasWarnings(basedOn) {
		warnings, _, err := GetAxisWarnings(requests, int32(basedOn.Device), int32(basedOn.Axis), clearWarnings)
		return warnings, err
	}
	return nil, nil
}

func CheckErrorFlags(flags *WarningsFlags, flagSet *regexp.Regexp, newError func(string, []string) errors.SdkError) errors.SdkError {
	matching := pie.Filter(flags.Flags(), func(flag string) bool {
		return flagSet.MatchString(flag)
	})
	if len(matching) > 0 {
		errorText := strings.Join(pie.Map(matching, func(flag string) string {
			return fmt.Sprintf("%s (%s)", commands.GetWarningText(flag), flag)
		}), ", ")
		return newError(errorText, flags.Flags())
	}
	return nil
}

func (flags *WarningsFlags) Has(flag string) bool {
	if flags == nil {
		return false
	}
	return flags.Check[flag]
}
func (flags *WarningsFlags) HasNot(flag string) bool {
	if flags == nil {
		return true
	}
	return !flags.Check[flag]
}
func (flags *WarningsFlags) HasAny() bool {
	if flags == nil {
		return false
	}
	return len(flags.Ordered) > 0
}
func (flags *WarningsFlags) Flags() []string {
	if flags == nil {
		return nil
	}
	return flags.Ordered
}
func (flags *WarningsFlags) Remove(flag string) (wasRemoved bool) {
	if !flags.Has(flag) {
		return false
	}

	flags.Check[flag] = false
	flags.Ordered = pie.Filter(flags.Ordered, func(other string) bool {
		return flag != other
	})

	return true
}

func (manager *deviceManager) getWarningsRequest(request *pb.DeviceGetWarningsRequest) (*pb.DeviceGetWarningsResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	warnings, _, err := GetAxisWarnings(requests, (request.Device), (request.Axis), request.GetClear())
	if err != nil {
		return nil, err
	}

	return &pb.DeviceGetWarningsResponse{
		Flags: warnings.Flags(),
	}, nil
}

func (manager *deviceManager) waitToClearWarnings(request *pb.WaitToClearWarningsRequest) errors.SdkError {
	requests, err := manager.getRequests(request)
	if err != nil {
		return err
	}

	timeout := time.Duration(request.Timeout * float64(time.Millisecond))
	startTime := time.Now()

	for {
		warnings, _, err := GetAxisWarnings(requests, (request.Device), (request.Axis), false)
		if err != nil {
			return err
		}

		remainingFlags := pie.Filter(request.WarningFlags, func(flag string) bool {
			return warnings.Has(flag)
		})
		if len(remainingFlags) == 0 {
			return nil
		} else if time.Since(startTime) >= timeout {
			return errors.ErrTimeout(fmt.Sprintf("The following flags have not cleared in time: %s", strings.Join(remainingFlags, ", ")))
		}

		<-time.After(constants.GetIdlePollingPeriod())
	}
}
