package devices

import (
	"fmt"
	"strings"
	"time"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func singleCommandDevice(requests *c.RequestManager, request deviceTarget, command string) (*c.Response, errors.SdkError) {
	return commands.SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: command,
	})
}

func (manager *deviceManager) singleRequestDevice(request deviceTarget, command string) (*c.Response, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}
	return singleCommandDevice(requests, request, command)
}

func singleCommandAxis(requests *c.RequestManager, request axisTarget, command string) (*c.Response, errors.SdkError) {
	return commands.SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: command,
	})
}

func (manager *deviceManager) singleRequestAxis(request axisTarget, command string) (*c.Response, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}
	return singleCommandAxis(requests, request, command)
}

func (manager *deviceManager) deviceToString(request *pb.AxisToStringRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("Device %d -> Connection Closed", request.Device),
			}, nil
		}
		return nil, err
	}
	return &pb.StringResponse{
		Value: fmt.Sprintf("%s -> %s", device.ToString(request.TypeOverride), device.interfaceInfo.communication.ToString()),
	}, nil
}

func (manager *deviceManager) allAxesToString(request *pb.AxisEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("All Axes -> Device %d -> Connection Closed", request.Device),
			}, nil
		}
		return nil, err
	}
	return &pb.StringResponse{
		Value: fmt.Sprintf("All Axes -> %s -> %s", device.ToString(""), device.interfaceInfo.communication.ToString()),
	}, nil
}

func (manager *deviceManager) axisToString(request *pb.AxisToStringRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("Axis %d -> Device %d -> Connection Closed", request.Axis, request.Device),
			}, nil
		}
		return nil, err
	}

	str, err := device.ToStringAxis(request, request.TypeOverride)
	if err != nil {
		return nil, err
	}

	return &pb.StringResponse{
		Value: fmt.Sprintf("%s -> %s -> %s", str, device.ToString(""), device.interfaceInfo.communication.ToString()),
	}, nil
}

func (manager *deviceManager) prepareCommand(request *pb.PrepareCommandRequest) (*pb.StringResponse, errors.SdkError) {
	args := make([]commandbuilding.CommandArg, len(request.Parameters))
	for i, param := range request.Parameters {
		args[i] = commandbuilding.CommandArgImpl{Value: param.Value, Unit: param.Unit}
	}

	commandTemplate := strings.Split(request.CommandTemplate, " ")
	command, err := manager.buildCommand(request, commandTemplate, args)
	if err != nil {
		return nil, err
	}

	return &pb.StringResponse{
		Value: command,
	}, nil
}

func (manager *deviceManager) driverDisable(request axisTarget) errors.SdkError {
	_, err := manager.singleRequestAxis(request, "driver disable")
	return err
}

func (manager *deviceManager) driverEnableRequest(request *pb.DriverEnableRequest) errors.SdkError {
	timeout := time.Duration(request.Timeout * float64(time.Second))
	return manager.driverEnable(request, timeout)
}

func (manager *deviceManager) driverEnable(request axisTarget, timeout time.Duration) errors.SdkError {
	start := time.Now()
	for {
		if _, err := manager.singleRequestAxis(request, "driver enable"); err == nil {
			return nil
		} else if commands.IsCommandErr(err, "DRIVERDISABLED") && time.Since(start) < timeout {
			<-time.After(constants.GetIdlePollingPeriod())
			continue
		} else {
			return err
		}
	}
}

func (manager *deviceManager) activate(request *pb.AxisEmptyRequest) errors.SdkError {
	interfaceInfo, err := manager.getInterfaceInfo(request)
	if err != nil {
		return err
	}
	requests := interfaceInfo.communication.Requests()

	if _, err := singleCommandAxis(requests, request, "activate"); err != nil {
		return err
	}

	interfaceInfo.removeDeviceInfo(int(request.Device))
	return nil
}

func (manager *deviceManager) restoreDeviceAxis(request *pb.DeviceRestoreRequest) errors.SdkError {
	interfaceInfo, err := manager.getInterfaceInfo(request)
	if err != nil {
		return err
	}
	requests := interfaceInfo.communication.Requests()

	if request.Hard {
		if _, err := singleCommandAxis(requests, request, "system factoryreset"); err != nil {
			return err
		}
	} else {
		eraseAll := request.Axis == 0
		if err := manager.storage.EraseWithPrefix(request, "zaber.", eraseAll); err != nil && !commands.IsBadCommandErr(err) {
			return err
		}

		var cmd string
		if request.Axis == 0 {
			cmd = "system restore"
		} else {
			cmd = "axis restore"
		}
		if _, err := singleCommandAxis(requests, request, cmd); err != nil {
			return err
		}
	}

	interfaceInfo.removeDeviceInfo(int(request.Device))
	return nil
}
