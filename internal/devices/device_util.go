package devices

import (
	"time"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) waitToRespond(request *pb.WaitToRespondRequest) errors.SdkError {
	timeout := time.Duration(request.Timeout * float64(time.Millisecond))
	startTime := time.Now()

	for {
		_, err := manager.singleRequestDevice(request, "")
		if err == nil || err.Type() != pb.Errors_REQUEST_TIMEOUT || time.Since(startTime) >= timeout {
			return err
		}
	}
}
