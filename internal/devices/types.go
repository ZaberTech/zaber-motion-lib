package devices

import (
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/ioc"
)

type streamArg interface {
	commandbuilding.CommandArg
	GetAxisIndex() int32
	GetSettingName() string
}
type streamArgImpl struct {
	commandbuilding.CommandArgImpl
	axisIndex   int32
	settingName string
}

func (arg streamArgImpl) GetAxisIndex() int32 {
	return arg.axisIndex
}
func (arg streamArgImpl) GetSettingName() string {
	return arg.settingName
}

type interfaceTarget = ioc.InterfaceTarget
type deviceTarget = ioc.DeviceTarget
type axisTarget = ioc.AxisTarget
type waitUntilIdleTarget = ioc.WaitUntilIdleTarget

type lockstepTarget interface {
	GetLockstepGroupId() int32
	deviceTarget
}
type lockstepWaitUntilIdleTarget interface {
	GetWaitUntilIdle() bool
	lockstepTarget
}

type streamTarget = ioc.StreamTarget

type axisTargetImpl struct {
	interfaceID int32
	device      int32
	axis        int32
}

func (target *axisTargetImpl) GetInterfaceId() int32 { //revive:disable-line:var-naming
	return target.interfaceID
}

func (target *axisTargetImpl) GetDevice() int32 {
	return target.device
}

func (target *axisTargetImpl) GetAxis() int32 {
	return target.axis
}

func targetAxis(device deviceTarget, axis int32) axisTarget {
	return &axisTargetImpl{
		interfaceID: device.GetInterfaceId(),
		device:      device.GetDevice(),
		axis:        axis,
	}
}

type lockstepTargetImpl struct {
	interfaceID     int32
	device          int32
	lockstepGroupID int32
}

func (target *lockstepTargetImpl) GetInterfaceId() int32 { //revive:disable-line:var-naming
	return target.interfaceID
}

func (target *lockstepTargetImpl) GetDevice() int32 {
	return target.device
}

func (target *lockstepTargetImpl) GetLockstepGroupId() int32 { //revive:disable-line:var-naming
	return target.lockstepGroupID
}
