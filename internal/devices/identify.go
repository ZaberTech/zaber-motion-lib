package devices

import (
	"fmt"
	"math"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	deviceDbApi "gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

var axisTypeDimensionMap map[string]pb.AsciiAxisType

func init() {
	axisTypeDimensionMap = map[string]pb.AsciiAxisType{
		"Length": pb.AsciiAxisType_LINEAR,
		"Angle":  pb.AsciiAxisType_ROTARY,
	}
}

func (manager *deviceManager) getIdentity(request deviceTarget) (*pb.AsciiDeviceIdentity, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if err := device.verifyIdentified(); err != nil {
		return nil, err
	}

	return device.identity, nil
}

func (manager *deviceManager) getIsIdentified(request deviceTarget) (*pb.BoolResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: device.isIdentified(),
	}, nil
}

func (manager *deviceManager) getAxisIdentity(request ioc.AxisTarget) (*pb.AsciiAxisIdentity, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	axis, err := device.getAxisInfo(request)
	if err != nil {
		return nil, err
	}

	return axis.identity, nil
}

func readDeviceProtocolInfo(device *deviceInfo, request deviceTarget, requests *c.RequestManager) (*c.DeviceProtocolInfo, errors.SdkError) {
	// By default, identified devices can send one packet and have no word length cap
	info := &c.DeviceProtocolInfo{
		MaxPackets:      1,
		MaxWordLen:      math.MaxInt32,
		MaxSyncSettings: 0,
	}

	if maxPacketsReply, err := singleCommandDevice(requests, request, "get comm.command.packets.max"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return nil, err
		}
	} else if info.MaxPackets, err = maxPacketsReply.DataAsInt(); err != nil {
		return nil, err
	}

	if wordLenReply, err := singleCommandDevice(requests, request, "get comm.word.size.max"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return nil, err
		}
	} else if info.MaxWordLen, err = wordLenReply.DataAsInt(); err != nil {
		return nil, err
	}

	if getSettingsMax, err := singleCommandDevice(requests, request, "get get.settings.max"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return nil, err
		}
	} else if info.MaxSyncSettings, err = getSettingsMax.DataAsInt(); err != nil {
		return nil, err
	}

	device.interfaceInfo.communication.SetDeviceProtocolInfo(int(request.GetDevice()), info)
	return info, nil
}

func (manager *deviceManager) getUsedFirmwareVersion(
	firmwareVersion *pb.FirmwareVersion,
	request ioc.DeviceTarget,
	requests *c.RequestManager,
) (*dto.FirmwareVersion, errors.SdkError) {
	if !utils.IsEmptyFirmwareVersion(firmwareVersion) {
		return &dto.FirmwareVersion{
			Major: int(firmwareVersion.Major),
			Minor: int(firmwareVersion.Minor),
			Build: int(firmwareVersion.Build),
		}, nil
	}

	if reply, err := singleCommandDevice(requests, request, "get version"); err != nil {
		return nil, err
	} else if fwVersion, err := dto.ParseFWVersion(reply.Data); err != nil {
		return nil, errors.ErrInvalidData(fmt.Sprintf("Cannot parse fw: %s", reply.Data))
	} else if reply, err := singleCommandDevice(requests, request, "get version.build"); err != nil {
		if commands.IsBadCommandErr(err) {
			return &fwVersion, nil
		} else {
			return nil, err
		}
	} else if buildNumber, err := reply.DataAsInt(); err != nil {
		return nil, err
	} else {
		fwVersion.Build = buildNumber
		return &fwVersion, nil
	}
}

// Gets protocol info and packet size, or reads these from the device
func (manager *deviceManager) GetProtocolInfo(target ioc.DeviceTarget) (*c.DeviceProtocolInfo, int, errors.SdkError) {
	requests, err := manager.getRequests(target)
	if err != nil {
		return nil, 0, err
	}

	info, err := manager.getDeviceInfo(target)
	if err != nil {
		return nil, 0, err
	}

	limits, packetSize := info.interfaceInfo.communication.GetDeviceLimits(int(target.GetDevice()))
	if limits == nil {
		limits, err = readDeviceProtocolInfo(info, target, requests)
	}
	return limits, packetSize, err
}

// TODO: this method is too long ZML-400

// Populates the passed *deviceInfo with information from the database and the device itself
func (manager *deviceManager) identifyDeviceInfo(request ioc.DeviceTarget, withFirmwareVersion *pb.FirmwareVersion, device *deviceInfo) errors.SdkError {
	requests := device.interfaceInfo.communication.Requests()
	query := &deviceDbApi.GetProductInformationQuery{}

	if reply, err := singleCommandDevice(requests, request, "get deviceid"); err != nil {
		return err
	} else {
		query.DeviceID = reply.Data
	}

	var serialNumber uint32
	if reply, err := singleCommandDevice(requests, request, "get system.serial"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return err
		}
	} else if parsed, err := reply.DataAsInt64(); err != nil {
		return errors.ErrInvalidData(fmt.Sprintf("Cannot parse serial number: %s", reply.Data))
	} else {
		serialNumber = (uint32)(parsed)
	}

	version, err := manager.getUsedFirmwareVersion(withFirmwareVersion, request, requests)
	if err != nil {
		return err
	}
	query.FW = version

	if reply, err := singleCommandDevice(requests, request, "get device.hw.modified"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return err
		}
	} else {
		query.Modified = reply.Data != "0"
	}

	_, err = readDeviceProtocolInfo(device, request, requests)
	if err != nil {
		return err
	}

	var axisCount int
	if reply, err := singleCommandDevice(requests, request, "get system.axiscount"); err != nil {
		return err
	} else if count, err := reply.DataAsInt(); err != nil {
		return err
	} else {
		axisCount = count
	}

	axes := make([]*axisInfo, axisCount)

	for i := 0; i < axisCount; i++ {
		resolution := 1
		if reply, err := commands.SingleCommand(requests, c.Command{
			Device:  int(request.GetDevice()),
			Axis:    i + 1,
			Command: "get resolution",
		}); err != nil {
			if !commands.IsBadCommandErr(err) {
				return err
			}
		} else if parsed, err := reply.DataAsInt(); err != nil {
			return err
		} else {
			resolution = parsed
		}

		isPeripheralLike := false
		var peripheralID string

		if reply, err := commands.SingleCommand(requests, c.Command{
			Device:  int(request.GetDevice()),
			Axis:    i + 1,
			Command: "get peripheralid",
		}); err != nil {
			if !commands.IsBadCommandErr(err) {
				return err
			}
		} else {
			isPeripheralLike = true
			peripheralID = reply.Data
			query.Peripherals = append(query.Peripherals, deviceDbApi.Peripheral{
				PeripheralID: peripheralID,
			})
		}

		var peripheralSerialNumber uint32
		if isPeripheralLike {
			if reply, err := commands.SingleCommand(requests, c.Command{
				Device:  int(request.GetDevice()),
				Axis:    i + 1,
				Command: "get peripheral.serial",
			}); err != nil {
				if !commands.IsBadCommandErr(err) {
					return err
				}
			} else {
				if serialNumber, err := reply.DataAsInt64(); err != nil {
					return err
				} else {
					peripheralSerialNumber = uint32(serialNumber)
				}
			}
		}

		axes[i] = &axisInfo{
			isPeripheralLike:       isPeripheralLike,
			peripheralID:           peripheralID,
			peripheralSerialNumber: peripheralSerialNumber,
			resolution:             resolution,
		}

		if isPeripheralLike {
			if reply, err := commands.SingleCommand(requests, c.Command{
				Device:  int(request.GetDevice()),
				Axis:    i + 1,
				Command: "get peripheral.hw.modified",
			}); err != nil {
				if !commands.IsBadCommandErr(err) {
					return err
				}
			} else {
				query.Peripherals[i].Modified = reply.Data != "0"
			}
		}
	}

	productInfo, err := manager.deviceDb.GetProductInformation(query)
	if err != nil {
		return err
	}

	isController := utils.IsStringInSlice("controller", productInfo.Capabilities)

	deviceIdentity := &pb.AsciiDeviceIdentity{
		DeviceId:     (int32)(productInfo.Device.DeviceID),
		SerialNumber: serialNumber,
		Name:         productInfo.Device.Name,
		AxisCount:    (int32)(axisCount),
		FirmwareVersion: &pb.FirmwareVersion{
			Major: (int32)(query.FW.Major),
			Minor: (int32)(query.FW.Minor),
			Build: (int32)(query.FW.Build),
		},
		IsModified:   query.Modified,
		IsIntegrated: !isController,
	}

	peripheralIndex := 0
	for axisIndex, axis := range axes {
		table := productInfo.ConversionTable
		commandTree := productInfo.CommandTree
		settingsTable := productInfo.Settings

		axisIdentity := &pb.AsciiAxisIdentity{}
		if axis.isPeripheralLike {
			peripheral := productInfo.Peripherals[peripheralIndex]
			addDeviceDimensionsToPeripheralTable(peripheral.ConversionTable, productInfo.ConversionTable)

			axis.product = peripheral.Device
			table = peripheral.ConversionTable
			commandTree = peripheral.CommandTree
			settingsTable = peripheral.Settings

			axisIdentity.PeripheralId = (int32)(axis.product.DeviceID)
			axisIdentity.PeripheralName = axis.product.Name
			axisIdentity.PeripheralSerialNumber = uint32(axis.peripheralSerialNumber)
			axisIdentity.IsPeripheral = isController
			axisIdentity.IsModified = query.Peripherals[peripheralIndex].Modified

			peripheralIndex++
		}

		if axisIdentity.IsModified || (!axis.isPeripheralLike && deviceIdentity.IsModified) {
			if err := manager.loadCustomConversions(request, axisIndex+1, table); err != nil {
				return err
			}
		}

		cmdIssuingInfo := &ioc.CommandIssuingInfo{
			ConversionTable: manager.units.GetConversionTable(table, axis.resolution),
			CommandTree:     manager.deviceDb.GetCommandTree(commandTree),
			SettingsTable:   manager.deviceDb.GetSettingsTable(settingsTable),
		}
		axis.commandIssuingInfo = cmdIssuingInfo

		if paramInfo, found := cmdIssuingInfo.SettingsTable.GetParamInfo("pos"); found {
			dimensionName, found := cmdIssuingInfo.ConversionTable.GetDimensionName(paramInfo.ContextualDimensionID)
			if found {
				axisType, found := axisTypeDimensionMap[dimensionName]
				if found {
					axis.axisType = axisType
				}
			}
		} else if _, foundProcess := cmdIssuingInfo.CommandTree.GetParamsInfo([]string{"process", "on"}); foundProcess {
			axis.axisType = pb.AsciiAxisType_PROCESS
		} else if _, foundLamp := cmdIssuingInfo.CommandTree.GetParamsInfo([]string{"lamp", "on"}); foundLamp {
			axis.axisType = pb.AsciiAxisType_LAMP
		}

		axisIdentity.AxisType = axis.axisType
		axis.identity = axisIdentity
	}

	var deviceCommandIssuingInfo *ioc.CommandIssuingInfo
	if len(query.Peripherals) > 0 || len(axes) == 0 {
		deviceCommandIssuingInfo = &ioc.CommandIssuingInfo{
			ConversionTable: manager.units.GetConversionTable(productInfo.ConversionTable, 1),
			CommandTree:     manager.deviceDb.GetCommandTree(productInfo.CommandTree),
			SettingsTable:   manager.deviceDb.GetSettingsTable(productInfo.Settings),
		}
	} else {
		deviceCommandIssuingInfo = axes[0].commandIssuingInfo
	}

	labels, err := getDeviceLabels(manager.storage, request, deviceCommandIssuingInfo, axes)
	if err != nil {
		return err
	}

	device.lock.Lock()
	defer device.lock.Unlock()

	device.fwVersion = *query.FW
	device.deviceID = query.DeviceID
	device.serialNumber = serialNumber
	device.product = productInfo.Device
	device.axes = axes
	device.identity = deviceIdentity
	device.commandIssuingInfo = deviceCommandIssuingInfo
	device.productInfo = productInfo
	for axis, label := range labels {
		if axis == 0 {
			device.label = label
		} else {
			device.axes[axis-1].label = label
		}
	}
	device._isIdentified = true

	return nil
}

// Creates a new deviceInfo object and identifies it without effecting any cached device info
func (manager *deviceManager) getIdentifiedDevice(request ioc.DeviceTarget, withFirmwareVersion *pb.FirmwareVersion) (*deviceInfo, errors.SdkError) {
	interfaceInfo, err := manager.getInterfaceInfo(request)
	if err != nil {
		return nil, err
	}

	device := &deviceInfo{interfaceInfo: interfaceInfo}
	err = manager.identifyDeviceInfo(request, withFirmwareVersion, device)
	if err != nil {
		return nil, err
	}

	return device, nil
}

// Gets a cached device info or creates one if it doesn't exist and identifies it, adding this information to the cache
func (manager *deviceManager) identifyDevice(request *pb.DeviceIdentifyRequest) (*pb.AsciiDeviceIdentity, errors.SdkError) {
	device, err := manager.getDeviceInfoBase(request, true)
	if err != nil {
		return nil, err
	}

	err = manager.identifyDeviceInfo(request, request.AssumeVersion, device)
	if err != nil {
		return nil, err
	}

	return device.identity, nil
}

// Gets labels for device and all peripherals.
func getDeviceLabels(
	storage ioc.StorageManager,
	request deviceTarget,
	cmdInfo *ioc.CommandIssuingInfo,
	axes []*axisInfo,
) (labels map[int]string, err errors.SdkError) {
	if _, hasStorage := cmdInfo.CommandTree.GetParamsInfo([]string{"storage", "get", constants.CommandArgument}); !hasStorage {
		return nil, nil
	}
	labels = make(map[int]string)

	for axis := range utils.Range(0, len(axes)+1) {
		if axis > 0 && !axes[axis-1].isPeripheralLike {
			continue
		}

		label, err := storage.GetStorage(targetAxis(request, int32(axis)), constants.LabelStorageKey, true)
		if err != nil {
			if !errors.IsAnyOf(err, pb.Errors_NOT_SUPPORTED, pb.Errors_NO_VALUE_FOR_KEY) {
				return nil, err
			}
			continue
		}

		labels[axis] = label
	}

	return labels, nil
}

func addDeviceDimensionsToPeripheralTable(peripheralTable *dto.ConversionTable, deviceTable *dto.ConversionTable) {
	peripheralTable.Rows = append(peripheralTable.Rows, deviceTable.Rows...)
}
