package devices

import (
	"fmt"
	"sync"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

/*
Structure to hold information about an axis.
The instance of this structure only exists when the device is identified.
All the fields are read-only. If any mutable fields are added later, they must be protected by a mutex.
Refer to deviceInfo struct for more information.
*/
type axisInfo struct {
	isPeripheralLike       bool
	peripheralID           string
	peripheralSerialNumber uint32
	product                *dto.Device
	label                  string
	resolution             int
	axisType               pb.AsciiAxisType
	identity               *pb.AsciiAxisIdentity
	commandIssuingInfo     *ioc.CommandIssuingInfo
}

func (info *axisInfo) GetProduct() *dto.Device {
	return info.product
}

func (info *axisInfo) GetCommandIssuingInfo() *ioc.CommandIssuingInfo {
	return info.commandIssuingInfo
}

/*
Structure to hold information about a device.
It's shared across multiple threads so the access to mutable fields are protected by corresponding mutexes.
The identity information is only filled once and then it's read-only, so it's not protected by a mutex.
Use isIdentified or verifyIdentified to check if the device is identified before accessing the identity fields.
If the device is identified again, the entire structure is discarded and a new one is created.
*/
type deviceInfo struct {
	_isIdentified bool
	number        int
	fwVersion     dto.FirmwareVersion
	deviceID      string
	serialNumber  uint32
	label         string
	product       *dto.Device
	lock          sync.Mutex
	axes          []*axisInfo
	interfaceInfo *interfaceInfo
	identity      *pb.AsciiDeviceIdentity

	commandIssuingInfo *ioc.CommandIssuingInfo
	productInfo        *dto.DeviceInfo

	// need to know a groups axes when we clear warnings, wait until
	// idle, or do unit conversions, so just save instead of polling each time
	lockstepGroups map[int]*lockstepGroup
	lockstepLock   sync.Mutex

	streams     map[string]*stream
	streamsLock sync.Mutex

	associatedData     map[string]interface{}
	associatedDataLock sync.Mutex

	servoTuningDefaultParams     []*pb.AsciiServoTuningParam
	servoTuningDefaultParamsLock sync.Mutex
}

func (info *deviceInfo) GetAddress() int {
	return info.number
}

func (info *deviceInfo) GetProduct() *dto.Device {
	return info.product
}

func (info *deviceInfo) GetFirmwareVersion() *dto.FirmwareVersion {
	return &info.fwVersion
}

func (info *deviceInfo) GetAxes() []ioc.AxisInfo {
	axes := make([]ioc.AxisInfo, len(info.axes))
	for i := range info.axes {
		axes[i] = info.axes[i]
	}
	return axes
}

func (info *deviceInfo) GetLabel(request axisTarget) (string, errors.SdkError) {
	if request.GetAxis() == 0 {
		if err := info.verifyIdentified(); err != nil {
			return "", err
		}
		info.lock.Lock()
		defer info.lock.Unlock()
		return info.label, nil
	} else {
		axis, err := info.getAxisInfo(request)
		if err != nil {
			return "", err
		}
		info.lock.Lock()
		defer info.lock.Unlock()
		return axis.label, nil
	}
}

func (info *deviceInfo) SetLabel(request axisTarget, label string) errors.SdkError {
	info.lock.Lock()
	defer info.lock.Unlock()

	axisNumber := int(request.GetAxis())
	if axisNumber == 0 {
		info.label = label
	} else if axisNumber <= len(info.axes) {
		info.axes[axisNumber-1].label = label
	} else {
		return errors.ErrInvalidArgument(fmt.Sprintf("Axis number %d out of range", axisNumber))
	}
	return nil
}

func (info *deviceInfo) IsIdentified() bool {
	return info.isIdentified()
}

func (info *deviceInfo) isIdentified() bool {
	info.lock.Lock()
	defer info.lock.Unlock()

	return info._isIdentified
}

func (info *deviceInfo) VerifyIdentified() errors.SdkError {
	return info.verifyIdentified()
}

func (info *deviceInfo) verifyIdentified() errors.SdkError {
	if !info.isIdentified() {
		return errors.ErrDeviceNotIdentified(info.number)
	}
	return nil
}

func (info *deviceInfo) getAxisInfo(request axisTarget) (*axisInfo, errors.SdkError) {
	if err := info.VerifyIdentified(); err != nil {
		return nil, err
	}

	targetAxis := (int)(request.GetAxis())
	axisIndex := targetAxis - 1
	if axisIndex < 0 || len(info.axes) <= axisIndex {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Target axis out of range: %d", targetAxis))
	}

	return info.axes[axisIndex], nil
}

func (info *deviceInfo) GetAxisInfo(request axisTarget) (ioc.AxisInfo, errors.SdkError) {
	return info.getAxisInfo(request)
}

func (info *deviceInfo) GetCommandIssuingInfo(request axisTarget) (*ioc.CommandIssuingInfo, errors.SdkError) {
	if request.GetAxis() == 0 {
		if err := info.VerifyIdentified(); err != nil {
			return nil, err
		}
		return info.commandIssuingInfo, nil
	} else {
		axisInfo, err := info.getAxisInfo(request)
		if err != nil {
			return nil, err
		}
		return axisInfo.commandIssuingInfo, nil
	}
}

func (info *deviceInfo) ToStringAxis(axis ioc.AxisTarget, typeName string) (string, errors.SdkError) {
	if typeName == "" {
		typeName = "Axis"
	}

	axisString := fmt.Sprintf("%s %d", typeName, axis.GetAxis())

	if info.IsIdentified() {
		axis, err := info.getAxisInfo(axis)
		if err != nil {
			return "", err
		}
		if axis.isPeripheralLike && axis.axisType != pb.AsciiAxisType_PROCESS {
			if axis.label != "" {
				axisString += fmt.Sprintf(" %s", axis.label)
			}
			axisString += fmt.Sprintf(" (%s)", axis.product.Name)
		}
	}

	return axisString, nil
}

func (info *deviceInfo) ToString(typeName string) string {
	var deviceIdentity string

	info.lock.Lock()
	if info._isIdentified {
		deviceIdentity = fmt.Sprintf("SN: %d", info.serialNumber)
		if info.label != "" {
			deviceIdentity += fmt.Sprintf(" %s", info.label)
		}
		deviceIdentity += fmt.Sprintf(" (%s)", info.product.Name)
	} else {
		deviceIdentity = "(Unknown)"
	}
	info.lock.Unlock()

	if typeName == "" {
		typeName = "Device"
	}

	str := fmt.Sprintf("%s %d %s", typeName, info.number, deviceIdentity)
	return str
}

func (info *deviceInfo) AssociateStruct(key string, data interface{}) {
	info.associatedDataLock.Lock()
	defer info.associatedDataLock.Unlock()

	if info.associatedData == nil {
		info.associatedData = make(map[string]interface{})
	}
	info.associatedData[key] = data
}

func (info *deviceInfo) GetAssociatedStruct(key string) (interface{}, bool) {
	info.associatedDataLock.Lock()
	defer info.associatedDataLock.Unlock()

	data, found := info.associatedData[key]
	return data, found
}

func (info *deviceInfo) GetIdentified() ioc.DeviceInfo {
	if !info.IsIdentified() {
		return nil
	}
	return info
}

func (info *deviceInfo) IsPeripheralLikeAxis(axis int) bool {
	return axis <= len(info.axes) && info.axes[axis-1].isPeripheralLike
}

func (info *deviceInfo) GetServoTuningDefaultParams(read func() ([]*pb.AsciiServoTuningParam, errors.SdkError)) ([]*pb.AsciiServoTuningParam, errors.SdkError) {
	info.servoTuningDefaultParamsLock.Lock()
	defer info.servoTuningDefaultParamsLock.Unlock()

	if info.servoTuningDefaultParams != nil {
		return info.servoTuningDefaultParams, nil
	}

	params, err := read()
	if err != nil {
		return nil, err
	}
	info.servoTuningDefaultParams = params
	return params, nil
}
