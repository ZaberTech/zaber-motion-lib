package devices

import (
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

func (manager *deviceManager) BuildCommand(request axisTarget, commandTemplate []string, arguments []ioc.CommandArg) (string, errors.SdkError) {
	return manager.buildCommand(request, commandTemplate, pie.Map(arguments, func(arg ioc.CommandArg) commandbuilding.CommandArg {
		return arg
	}))
}

func (manager *deviceManager) buildCommand(request axisTarget, commandTemplate []string, arguments []commandbuilding.CommandArg) (string, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return "", err
	}

	var commandIssuingInfo *ioc.CommandIssuingInfo
	if device.isIdentified() {
		commandIssuingInfo, err = device.GetCommandIssuingInfo(request)
		if err != nil {
			return "", err
		}
	} else if !commandbuilding.AreAllArgumentsNative(arguments) {
		return "", errors.ErrDeviceNotIdentified(device.number)
	}
	return commandbuilding.BuildCommand(commandTemplate, arguments, commandIssuingInfo)
}

func (manager *deviceManager) ConvertUnitSetting(request axisTarget, setting string, value float64, unit string, convertBack bool) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	}

	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return 0, err
	}

	commandIssuingInfo, err := device.GetCommandIssuingInfo(request)
	if err != nil {
		return 0, err
	}

	return commandbuilding.ConvertUnitSetting(commandIssuingInfo, setting, value, unit, convertBack)
}

func (manager *deviceManager) ConvertUnitForCommandArg(request axisTarget, commandTemplate []string, argIndex int, value float64, unit string, convertBack bool) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	}

	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return 0, err
	}

	commandIssuingInfo, err := device.GetCommandIssuingInfo(request)
	if err != nil {
		return 0, err
	}

	return commandbuilding.ConvertUnitsForCommandArg(commandTemplate, commandIssuingInfo, argIndex, value, unit, convertBack)
}
