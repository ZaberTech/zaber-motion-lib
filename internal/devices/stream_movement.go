package devices

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *streamManager) registerStreamMovementCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_line", func() pb.Message {
		return &pb.StreamLineRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.line(request.(*pb.StreamLineRequest))
	})
	gateway.RegisterCallback("device/stream_arc", func() pb.Message {
		return &pb.StreamArcRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.arc(request.(*pb.StreamArcRequest), false)
	})
	gateway.RegisterCallback("device/stream_helix", func() pb.Message {
		return &pb.StreamArcRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.arc(request.(*pb.StreamArcRequest), true)
	})
	gateway.RegisterCallback("device/stream_circle", func() pb.Message {
		return &pb.StreamCircleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.circle(request.(*pb.StreamCircleRequest))
	})
	gateway.RegisterCallback("device/stream_point", func() pb.Message {
		return &pb.PvtPointRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.point(request.(*pb.PvtPointRequest))
	})
}

var rotationDirectionToString = map[pb.RotationDirection]string{
	pb.RotationDirection_CW:  "cw",
	pb.RotationDirection_CCW: "ccw",
}

func convertStreamAxisToLetter(i int32) string {
	return string(rune(int(i) + int('a')))
}

func (manager *streamManager) move(request streamTarget, targetAxes []int32, move string, moveType string, rotationDirection string, moveArgs []streamArg) errors.SdkError {
	stream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err = stream.assureMode(StreamModeLive, StreamModeStore, StreamModeStoreArbitrary); err != nil {
		return err
	}

	var commandTemplate []string
	if len(targetAxes) > 0 {
		commandTemplate = append(commandTemplate, "on")
		for _, streamAxisIndex := range targetAxes {
			if err = stream.checkAxisIndex(streamAxisIndex); err != nil {
				return err
			}

			axisLetter := convertStreamAxisToLetter(streamAxisIndex)
			commandTemplate = append(commandTemplate, axisLetter)
		}
	}

	commandTemplate = append(commandTemplate, move, moveType)

	if rotationDirection != "" {
		commandTemplate = append(commandTemplate, rotationDirection)
	}

	for i := 0; i < len(moveArgs); i++ {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
	}

	command, err := manager.buildStreamCommand(request, stream, commandTemplate, moveArgs)
	if err != nil {
		return err
	}

	_, err = manager.streamActionRequest(request, stream, command)
	return err
}

func (manager *streamManager) line(request *pb.StreamLineRequest) errors.SdkError {
	targetAxes := request.GetTargetAxesIndices()
	var commandOnAxes = false
	if len(targetAxes) > 0 {
		commandOnAxes = true
	}

	move := "line"
	moveType := strings.ToLower(pb.StreamSegmentType_name[request.Type])
	rotationDirection := ""

	endpoint := request.GetEndpoint()

	if commandOnAxes && len(targetAxes) != len(endpoint) {
		return errors.ErrInvalidArgument("The number of axes and the number of line endpoints are not equal.")
	}

	var moveArgs []streamArg
	for i, measurement := range endpoint {
		axisIndexForArgument := int32(i)
		if commandOnAxes {
			axisIndexForArgument = targetAxes[i]
		}

		moveArgs = append(moveArgs, streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: measurement.GetValue(),
				Unit:  measurement.GetUnit(),
			},
			axisIndex:   axisIndexForArgument,
			settingName: "pos",
		})
	}

	return manager.move(request, targetAxes, move, moveType, rotationDirection, moveArgs)
}

func (manager *streamManager) arc(request *pb.StreamArcRequest, isHelix bool) errors.SdkError {
	targetAxes := request.GetTargetAxesIndices()
	commandOnAxes := len(targetAxes) > 0

	move := "arc"
	if isHelix {
		move = "helix"
	}
	moveType := strings.ToLower(pb.StreamSegmentType_name[(request.Type)])
	rotationDir := rotationDirectionToString[request.GetRotationDirection()]

	const numberOfAxesForArcCommand = 2
	if isHelix {
		if len(request.Endpoint) == 0 {
			return errors.ErrInvalidArgument("No endpoint provided for the line component.")
		}
		expected := numberOfAxesForArcCommand + len(request.Endpoint)
		if commandOnAxes && len(targetAxes) != expected {
			return errors.ErrInvalidArgument(
				fmt.Sprintf("Invalid number of target axes %d (expected %d).", len(targetAxes), expected))
		}
	} else {
		if commandOnAxes && len(targetAxes) != numberOfAxesForArcCommand {
			return errors.ErrInvalidArgument("Invalid number of target axes (expected 2).")
		}
	}

	var axisOneIndex int32
	var axisTwoIndex int32 = 1
	if commandOnAxes {
		axisOneIndex = targetAxes[0]
		axisTwoIndex = targetAxes[1]
	}

	moveArgs := []streamArg{
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetCenterX().GetValue(),
				Unit:  request.GetCenterX().GetUnit(),
			},
			axisIndex:   axisOneIndex,
			settingName: "pos",
		},
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetCenterY().GetValue(),
				Unit:  request.GetCenterY().GetUnit(),
			},
			axisIndex:   axisTwoIndex,
			settingName: "pos",
		},
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetEndX().GetValue(),
				Unit:  request.GetEndX().GetUnit(),
			},
			axisIndex:   axisOneIndex,
			settingName: "pos",
		},
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetEndY().GetValue(),
				Unit:  request.GetEndY().GetUnit(),
			},
			axisIndex:   axisTwoIndex,
			settingName: "pos",
		},
	}

	for i, endpoint := range request.Endpoint {
		var axisIndex int32
		if commandOnAxes {
			axisIndex = targetAxes[numberOfAxesForArcCommand+i]
		} else {
			axisIndex = (int32)(numberOfAxesForArcCommand + i)
		}

		moveArgs = append(moveArgs, streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: endpoint.Value,
				Unit:  endpoint.Unit,
			},
			axisIndex:   axisIndex,
			settingName: "pos",
		})
	}

	return manager.move(request, targetAxes, move, moveType, rotationDir, moveArgs)
}

func (manager *streamManager) circle(request *pb.StreamCircleRequest) errors.SdkError {
	targetAxes := request.GetTargetAxesIndices()
	var commandOnAxes = false
	if len(targetAxes) > 0 {
		commandOnAxes = true
	}

	move := "circle"
	moveType := strings.ToLower(pb.StreamSegmentType_name[(request.Type)])
	rotationDir := rotationDirectionToString[request.GetRotationDirection()]

	const numberOfAxesForCircleCommand = 2
	if commandOnAxes && len(targetAxes) != numberOfAxesForCircleCommand {
		return errors.ErrInvalidArgument("Invalid number of target axes (expected 2).")
	}

	var axisOneIndex int32
	var axisTwoIndex int32 = 1
	if commandOnAxes {
		axisOneIndex = targetAxes[0]
		axisTwoIndex = targetAxes[1]
	}

	moveArgs := []streamArg{
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetCenterX().GetValue(),
				Unit:  request.GetCenterX().GetUnit(),
			},
			axisIndex:   axisOneIndex,
			settingName: "pos",
		},
		streamArgImpl{
			CommandArgImpl: commandbuilding.CommandArgImpl{
				Value: request.GetCenterY().GetValue(),
				Unit:  request.GetCenterY().GetUnit(),
			},
			axisIndex:   axisTwoIndex,
			settingName: "pos",
		},
	}

	return manager.move(request, targetAxes, move, moveType, rotationDir, moveArgs)
}
