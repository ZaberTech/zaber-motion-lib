package devices

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

type triggersManager struct {
	devices *deviceManager
}

type deviceRequest struct {
	ioc.DeviceTarget
}

func (dr deviceRequest) GetAxis() int32 {
	return 0
}

var toActionLabel = map[pb.AsciiTriggerAction]string{
	1: "a",
	2: "b",
}

var toTriggerOperator = map[pb.AsciiTriggerCondition]string{
	0: "==",
	1: "<>",
	2: ">",
	3: ">=",
	4: "<",
	5: "<=",
}

var toTriggerOperation = map[pb.AsciiTriggerOperation]string{
	0: "=",
	1: "+=",
	2: "-=",
}

func newTriggersManager(deviceManager *deviceManager) *triggersManager {
	return &triggersManager{
		devices: deviceManager,
	}
}

func (manager *triggersManager) checkTriggersSupported(err errors.SdkError) errors.SdkError {
	if err == nil {
		return nil
	} else if commands.IsBadCommandErr(err) || err.Type() == pb.Errors_NOT_SUPPORTED {
		return errors.ErrNotSupported("The specified device does not support this trigger functionality: " + err.Message())
	} else {
		return err
	}
}

func (manager *triggersManager) registerCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("triggers/get_setting", func() pb.Message {
		return &pb.DeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggersGetSetting(request.(*pb.DeviceGetSettingRequest))
	})
	gateway.RegisterCallback("trigger/enable", func() pb.Message {
		return &pb.TriggerEnableRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerEnable(request.(*pb.TriggerEnableRequest))
	})
	gateway.RegisterCallback("trigger/disable", func() pb.Message {
		return &pb.TriggerEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerDisable(request.(*pb.TriggerEmptyRequest))
	})
	gateway.RegisterCallback("trigger/get_state", func() pb.Message {
		return &pb.TriggerEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggerGetState(request.(*pb.TriggerEmptyRequest))
	})
	gateway.RegisterCallback("trigger/get_enabled_state", func() pb.Message {
		return &pb.TriggerEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggerGetEnabledState(request.(*pb.TriggerEmptyRequest))
	})
	gateway.RegisterCallback("trigger/fire_when", func() pb.Message {
		return &pb.TriggerFireWhenRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhen(request.(*pb.TriggerFireWhenRequest))
	})
	gateway.RegisterCallback("trigger/fire_when_encoder_distance_travelled", func() pb.Message {
		return &pb.TriggerFireWhenDistanceTravelledRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhenEncoderDistanceTravelled(request.(*pb.TriggerFireWhenDistanceTravelledRequest))
	})
	gateway.RegisterCallback("trigger/fire_when_distance_travelled", func() pb.Message {
		return &pb.TriggerFireWhenDistanceTravelledRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhenDistanceTravelled(request.(*pb.TriggerFireWhenDistanceTravelledRequest))
	})
	gateway.RegisterCallback("trigger/fire_when_io", func() pb.Message {
		return &pb.TriggerFireWhenIoRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhenIo(request.(*pb.TriggerFireWhenIoRequest))
	})
	gateway.RegisterCallback("trigger/fire_when_setting", func() pb.Message {
		return &pb.TriggerFireWhenSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhenSetting(request.(*pb.TriggerFireWhenSettingRequest))
	})
	gateway.RegisterCallback("trigger/fire_when_setting_absolute", func() pb.Message {
		return &pb.TriggerFireWhenSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireWhenAbsoluteSetting(request.(*pb.TriggerFireWhenSettingRequest))
	})
	gateway.RegisterCallback("trigger/fire_at_interval", func() pb.Message {
		return &pb.TriggerFireAtIntervalRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerFireAtInterval(request.(*pb.TriggerFireAtIntervalRequest))
	})
	gateway.RegisterCallback("trigger/on_fire", func() pb.Message {
		return &pb.TriggerOnFireRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerOnFire(request.(*pb.TriggerOnFireRequest))
	})
	gateway.RegisterCallback("trigger/on_fire_set", func() pb.Message {
		return &pb.TriggerOnFireSetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerOnFireSet(request.(*pb.TriggerOnFireSetRequest))
	})
	gateway.RegisterCallback("trigger/on_fire_set_to_setting", func() pb.Message {
		return &pb.TriggerOnFireSetToSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerOnFireSetToSetting(request.(*pb.TriggerOnFireSetToSettingRequest))
	})
	gateway.RegisterCallback("trigger/clear_action", func() pb.Message {
		return &pb.TriggerClearActionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerClearAction(request.(*pb.TriggerClearActionRequest))
	})
	gateway.RegisterCallback("trigger/get_label", func() pb.Message {
		return &pb.TriggerEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggerGetLabel(request.(*pb.TriggerEmptyRequest))
	})
	gateway.RegisterCallback("trigger/set_label", func() pb.Message {
		return &pb.TriggerSetLabelRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.triggerSetLabel(request.(*pb.TriggerSetLabelRequest))
	})
	gateway.RegisterCallback("triggers/get_trigger_states", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggersGetStates(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("triggers/get_enabled_states", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggersGetEnabledStates(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("triggers/get_all_labels", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.triggersGetAllLabels(request.(*pb.DeviceEmptyRequest))
	})
}

func (manager *triggersManager) triggersGetSetting(request *pb.DeviceGetSettingRequest) (*pb.IntResponse, errors.SdkError) {
	device, err := manager.devices.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if device.isIdentified() {
		if _, found := device.commandIssuingInfo.SettingsTable.GetParamInfo(request.Setting); !found {
			switch request.Setting {
			case "trigger.numtriggers":
				return &pb.IntResponse{Value: 0}, nil

			case "trigger.numactions":
				return &pb.IntResponse{Value: 0}, nil
			}
		}
	}

	reply, err := manager.devices.singleRequestAxis(request, "get "+request.Setting)
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}

	value, err := reply.DataAsInt()
	if err != nil {
		return nil, err
	}

	return &pb.IntResponse{Value: int32(value)}, nil
}

func (manager *triggersManager) triggerEnable(request *pb.TriggerEnableRequest) errors.SdkError {
	var command string
	if request.Count == 0 {
		command = fmt.Sprintf("trigger %d enable", request.TriggerNumber)
	} else {
		command = fmt.Sprintf("trigger %d enable %d", request.TriggerNumber, request.Count)
	}

	_, err := manager.devices.singleRequestDevice(request, command)

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerDisable(request *pb.TriggerEmptyRequest) errors.SdkError {
	_, err := manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d disable",
		request.TriggerNumber,
	))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) parseTriggerState(replies []string) *pb.AsciiTriggerState {
	var triggerState pb.AsciiTriggerState

	triggerState.FiresTotal = 0

	for _, reply := range replies {
		if strings.HasPrefix(reply, "when") {
			triggerState.Condition = reply
		} else if strings.HasPrefix(reply, "action") {
			triggerState.Actions = append(triggerState.Actions, reply)
		} else if strings.HasPrefix(reply, "enable") {
			match := regexp.MustCompile(`(\d+)$`).FindString(reply)
			if match == "" {
				triggerState.FiresTotal = -1
			} else {
				firesTotal, _ := strconv.Atoi(match)
				triggerState.FiresTotal = int32(firesTotal)
			}
		}
	}

	return &triggerState
}

func (manager *triggersManager) parseEnabledState(reply string, trigger int) (*pb.AsciiTriggerEnabledState, errors.SdkError) {
	enabledStatesRaw := strings.Split(reply, " ")
	if trigger > len(enabledStatesRaw) {
		return nil, errors.ErrInvalidData(fmt.Sprintf("Data not available for trigger %d", trigger))
	}

	firesRemaining := 0
	if enabledStatesRaw[trigger-1] == "e" {
		firesRemaining = -1
	} else if enabledStatesRaw[trigger-1] != "d" {
		enabledState, err := strconv.Atoi(enabledStatesRaw[trigger-1])
		if err != nil {
			return nil, errors.ErrInvalidData(fmt.Sprintf("Cannot parse trigger state %s", enabledStatesRaw[trigger-1]))
		} else {
			firesRemaining = enabledState
		}
	}

	return &pb.AsciiTriggerEnabledState{
		Enabled:        firesRemaining != 0,
		FiresRemaining: int32(firesRemaining),
	}, nil
}

func (manager *triggersManager) triggerGetState(request *pb.TriggerEmptyRequest) (*pb.AsciiTriggerState, errors.SdkError) {
	requests, err := manager.devices.getRequests(request)
	if err != nil {
		return nil, err
	}

	printReplies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: fmt.Sprintf("trigger %d print", request.TriggerNumber),
	})
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}
	var printRepliesData []string
	for _, reply := range printReplies {
		printRepliesData = append(printRepliesData, reply.Data)
	}
	triggerState := manager.parseTriggerState(printRepliesData)

	showReply, err := manager.devices.singleRequestDevice(request, "trigger show")
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}
	enabledState, err := manager.parseEnabledState(showReply.Data, int(request.TriggerNumber))
	if err != nil {
		return nil, err
	}

	triggerState.Enabled = enabledState.Enabled
	triggerState.FiresRemaining = enabledState.FiresRemaining

	return triggerState, nil
}

func (manager *triggersManager) triggerGetEnabledState(request *pb.TriggerEmptyRequest) (*pb.AsciiTriggerEnabledState, errors.SdkError) {
	reply, err := manager.devices.singleRequestDevice(request, "trigger show")
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}
	enabledState, err := manager.parseEnabledState(reply.Data, int(request.TriggerNumber))
	if err != nil {
		return nil, err
	}

	return enabledState, nil
}

func (manager *triggersManager) triggerFireWhen(request *pb.TriggerFireWhenRequest) errors.SdkError {
	commandTemplate := []string{
		"trigger",
		fmt.Sprint(request.TriggerNumber),
		"when",
	}

	conditionParts := strings.Split(request.Condition, " ")
	commandTemplate = append(commandTemplate, conditionParts...)

	command, err := manager.devices.buildCommand(deviceRequest{request}, commandTemplate, []commandbuilding.CommandArg{})
	if err != nil {
		if err.Type() == pb.Errors_NOT_SUPPORTED {
			return errors.ErrNotSupported("The specified device does not support Triggers, or the following condition is invalid for specified device/axis: '" + request.Condition + "'")
		} else {
			return err
		}
	}

	_, err = manager.devices.singleRequestDevice(request, command)

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) convertSettingValue(request ioc.AxisTarget, setting string, value float64, unit string) (float64, errors.SdkError) {
	device, err := manager.devices.getDeviceInfo(request)
	if err != nil {
		return 0, err
	}

	if device.isIdentified() {
		if request.GetAxis() == 0 {
			info := device.commandIssuingInfo
			if _, found := info.SettingsTable.GetParamInfo(setting); !found {
				for _, axis := range device.axes {
					if _, foundAxis := axis.commandIssuingInfo.SettingsTable.GetParamInfo(setting); foundAxis {
						return 0, errors.ErrInvalidArgument("The following is an axis-scope setting: '" + setting + "'. To use this setting, please specify an axis > 0.")
					}
				}
				return 0, errors.ErrInvalidArgument("The following setting is invalid for this device: '" + setting + "'")
			}

			return info.ConversionTable.ConvertSettingValue(info.SettingsTable, setting, value, unit)
		} else if int(request.GetAxis()) <= len(device.axes) {
			info := device.axes[request.GetAxis()-1].commandIssuingInfo
			if _, found := info.SettingsTable.GetParamInfo(setting); !found {
				if _, foundDevice := device.commandIssuingInfo.SettingsTable.GetParamInfo(setting); foundDevice {
					return 0, errors.ErrInvalidArgument("The following is a device-scope setting: '" + setting + "'. To use this setting, please specify an axis of 0.")
				}
				return 0, errors.ErrInvalidArgument(fmt.Sprintf("The following setting is invalid for axis %d: '%s'", request.GetAxis(), setting))
			}

			return info.ConversionTable.ConvertSettingValue(info.SettingsTable, setting, value, unit)
		} else {
			return 0, errors.ErrInvalidArgument("Invalid axis")
		}
	} else if unit == constants.NativeUnit {
		return value, nil
	} else {
		return 0, errors.ErrInvalidArgument("Unit conversion is not supported for unidentified devices")
	}
}

func (manager *triggersManager) triggerFireWhenEncoderDistanceTravelled(request *pb.TriggerFireWhenDistanceTravelledRequest) errors.SdkError {
	dist, err := manager.convertSettingValue(request, "pos", request.Distance, request.Unit)
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d when %d encoder dist %d",
		request.TriggerNumber,
		request.Axis,
		int(dist)))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerFireWhenDistanceTravelled(request *pb.TriggerFireWhenDistanceTravelledRequest) errors.SdkError {
	dist, err := manager.convertSettingValue(request, "pos", request.Distance, request.Unit)
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d when %d dist %d",
		request.TriggerNumber,
		request.Axis,
		int(dist)))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerFireWhenIo(request *pb.TriggerFireWhenIoRequest) errors.SdkError {
	ioType, err := utils.IoPortTypeToFirmwarePort(pb.AsciiIoPortType(request.PortType))
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d when io %s %d %s %s",
		request.TriggerNumber,
		ioType,
		request.Channel,
		toTriggerOperator[request.TriggerCondition],
		commandbuilding.FormatValueUsingNumberDecimalPlaces(request.Value)))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerFireWhenSetting(request *pb.TriggerFireWhenSettingRequest) errors.SdkError {
	value, err := manager.convertSettingValue(request, request.Setting, request.Value, request.Unit)
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d when %d %s %s %s",
		request.TriggerNumber,
		request.Axis,
		request.Setting,
		toTriggerOperator[request.TriggerCondition],
		commandbuilding.FormatValueUsingNumberDecimalPlaces(value)))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerFireWhenAbsoluteSetting(request *pb.TriggerFireWhenSettingRequest) errors.SdkError {
	value, err := manager.convertSettingValue(request, request.Setting, request.Value, request.Unit)
	if err != nil {
		return err
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d when %d abs %s %s %s",
		request.TriggerNumber,
		request.Axis,
		request.Setting,
		toTriggerOperator[request.TriggerCondition],
		commandbuilding.FormatValueUsingNumberDecimalPlaces(value)))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerFireAtInterval(request *pb.TriggerFireAtIntervalRequest) errors.SdkError {

	commandTemplate := []string{
		"trigger",
		fmt.Sprint(request.TriggerNumber),
		"when",
		"time",
		constants.CommandArgument,
	}

	args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Interval, Unit: request.Unit}}

	command, err := manager.devices.buildCommand(deviceRequest{request}, commandTemplate, args)
	if err != nil {
		return manager.checkTriggersSupported(err)
	}

	_, err = manager.devices.singleRequestDevice(request, command)

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerOnFire(request *pb.TriggerOnFireRequest) errors.SdkError {
	if request.Action == 0 {
		return errors.ErrInvalidArgument("Trigger Action cannot be 'ALL'")
	}

	commandTemplate := []string{
		"trigger",
		fmt.Sprint(request.TriggerNumber),
		"action",
		toActionLabel[request.Action],
		fmt.Sprint(request.Axis),
	}

	commandParts := strings.Split(request.Command, " ")
	commandTemplate = append(commandTemplate, commandParts...)

	command, err := manager.devices.buildCommand(deviceRequest{request}, commandTemplate, []commandbuilding.CommandArg{})
	if err != nil {
		if err.Type() == pb.Errors_NOT_SUPPORTED {
			return errors.ErrNotSupported("The specified device does not support Triggers, or the following command is invalid for specified device/axis: '" + request.Command + "'")
		} else {
			return err
		}
	}

	_, err = manager.devices.singleRequestDevice(request, command)

	return manager.checkTriggersSupported(err)
}

func hasSettingDevice(device *deviceInfo, setting string) bool {
	_, found := device.commandIssuingInfo.SettingsTable.GetParamInfo(setting)
	return found
}

func (manager *triggersManager) triggerOnFireSet(request *pb.TriggerOnFireSetRequest) errors.SdkError {
	if request.Action == 0 {
		return errors.ErrInvalidArgument("Trigger Action cannot be 'ALL'")
	}

	device, err := manager.devices.getDeviceInfo(request)
	if err != nil {
		return err
	}

	value := request.Value
	if request.Axis > 0 || hasSettingDevice(device, request.Setting) {
		value, err = manager.convertSettingValue(request, request.Setting, request.Value, request.Unit)
		if err != nil {
			return err
		}
	} else {
		if request.Unit != constants.NativeUnit {
			return errors.ErrInvalidArgument("Unit conversion is not supported without axis selected for axis-scope setting: '" + request.Setting + "'")
		}
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d action %s %d %s %s %s",
		request.TriggerNumber,
		toActionLabel[request.Action],
		request.Axis,
		request.Setting,
		toTriggerOperation[request.Operation],
		commandbuilding.FormatValueUsingNumberDecimalPlaces(value)))

	return manager.checkTriggersSupported(err)
}

func hasSettingAxis(device *deviceInfo, axis int, setting string) bool {
	if axis > 0 && axis <= len(device.axes) {
		axisInfo := device.axes[axis-1]
		_, found := axisInfo.commandIssuingInfo.SettingsTable.GetParamInfo(setting)
		return found
	} else {
		return false
	}
}

func hasSettingDeviceOrAxes(device *deviceInfo, setting string) bool {
	if hasSettingDevice(device, setting) {
		return true
	}
	for _, axis := range device.axes {
		if _, found := axis.commandIssuingInfo.SettingsTable.GetParamInfo(setting); found {
			return true
		}
	}
	return false
}

func (manager *triggersManager) triggerOnFireSetToSetting(request *pb.TriggerOnFireSetToSettingRequest) errors.SdkError {
	if request.Action == 0 {
		return errors.ErrInvalidArgument("Trigger Action cannot be 'ALL'")
	}

	device, err := manager.devices.getDeviceInfo(request)
	if err != nil {
		return err
	}

	if device.isIdentified() {
		if request.Axis == 0 {
			if !hasSettingDeviceOrAxes(device, request.Setting) {
				return errors.ErrInvalidArgument("The following setting is invalid for device: '" + request.Setting + "'")
			}
		} else {
			if !hasSettingAxis(device, int(request.Axis), request.Setting) {
				return errors.ErrInvalidArgument(fmt.Sprintf("The following setting is invalid for axis %d: '%s'", request.Axis, request.Setting))
			}
		}
		if request.FromAxis == 0 {
			if !hasSettingDeviceOrAxes(device, request.FromSetting) {
				return errors.ErrInvalidArgument("The following setting is invalid for device: '" + request.Setting + "'")
			}
		} else {
			if !hasSettingAxis(device, int(request.FromAxis), request.FromSetting) {
				return errors.ErrInvalidArgument(fmt.Sprintf("The following setting is invalid for axis %d: '%s'", request.FromAxis, request.FromSetting))
			}
		}
	}

	_, err = manager.devices.singleRequestDevice(request, fmt.Sprintf(
		"trigger %d action %s %d %s %s setting %d %s",
		request.TriggerNumber,
		toActionLabel[request.Action],
		request.Axis,
		request.Setting,
		toTriggerOperation[request.Operation],
		request.FromAxis,
		request.FromSetting,
	))

	return manager.checkTriggersSupported(err)
}

func (manager *triggersManager) triggerClearAction(request *pb.TriggerClearActionRequest) errors.SdkError {
	if request.Action == 0 {
		_, err := manager.devices.singleRequestDevice(request, fmt.Sprintf("trigger %d action none", request.TriggerNumber))
		return manager.checkTriggersSupported(err)
	} else {
		_, err := manager.devices.singleRequestDevice(request, fmt.Sprintf("trigger %d action %s none", request.TriggerNumber, toActionLabel[request.Action]))
		return manager.checkTriggersSupported(err)
	}
}

const triggersLabelStorageKeyPrefix = "zaber.triggers.label."

func (manager *triggersManager) triggerGetLabel(request *pb.TriggerEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	key := fmt.Sprintf("%s%d", triggersLabelStorageKeyPrefix, request.TriggerNumber)
	label, err := manager.devices.storage.GetStorage(targetAxis(request, 0), key, true)
	if err != nil {
		return nil, err
	}
	return &pb.StringResponse{Value: label}, nil
}

func (manager *triggersManager) triggerSetLabel(request *pb.TriggerSetLabelRequest) errors.SdkError {
	key := fmt.Sprintf("%s%d", triggersLabelStorageKeyPrefix, request.TriggerNumber)

	if utils.PtrDefault(request.Label, "") == "" {
		_, err := manager.devices.storage.Erase(targetAxis(request, 0), key)
		return err
	} else {
		return manager.devices.storage.SetStorage(targetAxis(request, 0), key, *request.Label, true)
	}
}

func (manager *triggersManager) triggersGetStates(request *pb.DeviceEmptyRequest) (*pb.TriggerStates, errors.SdkError) {
	requests, errRequests := manager.devices.getRequests(request)

	if errRequests != nil {
		return nil, errRequests
	}

	printReplies, errCommand := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: "trigger print",
	})
	if errCommand != nil {
		return nil, manager.checkTriggersSupported(errCommand)
	}

	var printRepliesData [][]string
	currentTrigger := 0
	for _, reply := range printReplies {
		if strings.HasPrefix(reply.Data, "trigger") {
			currentTrigger++
			printRepliesData = append(printRepliesData, make([]string, 0))
		} else if currentTrigger >= 1 {
			printRepliesData[currentTrigger-1] = append(printRepliesData[currentTrigger-1], reply.Data)
		}
	}

	var triggerStates []*pb.AsciiTriggerState
	for _, triggerData := range printRepliesData {
		triggerStates = append(triggerStates, manager.parseTriggerState(triggerData))
	}

	showReply, errShow := manager.devices.singleRequestDevice(request, "trigger show")
	if errShow != nil {
		return nil, manager.checkTriggersSupported(errShow)
	}

	for i, triggerState := range triggerStates {
		enabledState, err := manager.parseEnabledState(showReply.Data, i+1)
		if err != nil {
			return nil, err
		}
		triggerState.Enabled = enabledState.Enabled
		triggerState.FiresRemaining = enabledState.FiresRemaining
	}

	return &pb.TriggerStates{States: triggerStates}, nil
}

func (manager *triggersManager) triggersGetEnabledStates(request *pb.DeviceEmptyRequest) (*pb.TriggerEnabledStates, errors.SdkError) {
	reply, err := manager.devices.singleRequestDevice(request, "trigger show")
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}

	numTriggers := len(strings.Split(reply.Data, " "))

	var enabledStates []*pb.AsciiTriggerEnabledState
	for i := 0; i < numTriggers; i++ {
		enabledState, err := manager.parseEnabledState(reply.Data, i+1)
		if err != nil {
			return nil, err
		}
		enabledStates = append(enabledStates, enabledState)
	}

	return &pb.TriggerEnabledStates{States: enabledStates}, nil
}

func (manager *triggersManager) triggersGetAllLabels(request *pb.DeviceEmptyRequest) (*pb.StringArrayResponse, errors.SdkError) {
	numTriggersReply, err := manager.devices.singleRequestDevice(request, "get trigger.numtriggers")
	if err != nil {
		return nil, manager.checkTriggersSupported(err)
	}
	numTriggers, err := numTriggersReply.DataAsInt()
	if err != nil {
		return nil, err
	}

	keyedLabels, err := manager.devices.storage.ListKeyValuePairs(&axisTargetImpl{
		interfaceID: request.InterfaceId,
		device:      request.Device,
	}, triggersLabelStorageKeyPrefix, true)
	if err != nil {
		return nil, err
	}

	labels := &pb.StringArrayResponse{
		Values: make([]string, numTriggers),
	}

	for i := 0; i < numTriggers; i++ {
		key := fmt.Sprintf("%s%d", triggersLabelStorageKeyPrefix, i+1)
		if label, found := keyedLabels[key]; found {
			labels.Values[i] = string(label)
		}
	}

	return labels, nil
}
