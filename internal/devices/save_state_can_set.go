package devices

import (
	"encoding/json"
	"fmt"
	"strconv"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"
)

func (manager *saveStateManager) checkContext(request setStateTarget, withFirmwareVersion *pb.FirmwareVersion, context *SaveStateContext) errors.SdkError {
	if context.Version > MaxSaveStateVersion {
		return errors.ErrNotSupported(
			fmt.Sprintf("No support for save files with version %d. Try upgrading to the newest version of Zaber Motion Library.", context.Version))
	}
	var device *deviceInfo
	var err errors.SdkError
	if withFirmwareVersion != nil {
		device, err = manager.deviceManager.getIdentifiedDevice(request, withFirmwareVersion)
	} else {
		device, err = manager.deviceManager.getDeviceInfo(request)
	}
	if err != nil {
		return err
	}
	if err := device.verifyIdentified(); err != nil {
		return err
	}

	if context.FirmwareVersion.Major != 7 {
		return errors.ErrNotSupported("No support for setting state with major firmware versions other than 7.")
	}

	deviceSaveStateVersion := device.productInfo.SaveStateVersion
	if deviceSaveStateVersion > MaxSaveStateVersion {
		return errors.ErrNotSupported(
			fmt.Sprintf("No support for setting state with save state version %d. Upgrade to the newest version of ZML.", deviceSaveStateVersion))
	}

	deviceVersionSpecial := device.fwVersion.Minor == 98 || device.fwVersion.Minor == 99
	stateVersionSpecial := context.FirmwareVersion.Minor == 98 || context.FirmwareVersion.Minor == 99
	if (!deviceVersionSpecial && !stateVersionSpecial) && device.fwVersion.Minor < context.FirmwareVersion.Minor {
		msg := fmt.Sprintf("Cannot set a version %d.%d state to a version %d.%d product.", context.FirmwareVersion.Major, context.FirmwareVersion.Minor, device.fwVersion.Major, device.fwVersion.Minor)
		tip := " Use a saved state that targets the product's version or earlier."
		return errors.ErrNotSupported(msg + tip)
	}

	if !stateVersionSpecial && context.Version > MaxSaveStateVersion {
		return errors.ErrNotSupported(
			fmt.Sprintf("No support for setting state with firmware version %d.%d. Upgrade to the newest version of ZML.",
				device.fwVersion.Major, device.fwVersion.Minor))
	}

	return nil
}

func (manager *saveStateManager) checkPeripheral(request axisTarget, state *PeripheralState) errors.SdkError {
	peripheralIDReply, err := manager.deviceManager.singleRequestAxis(request, "get peripheralid")
	if err != nil {
		return errors.ErrNotSupported("Cannot set state to an axis with no peripheral ID.")
	}
	if state == nil || state.PeripheralID == "" {
		return errors.ErrInvalidArgument("Invalid save state. This save state cannot be set to an axis, are you sure it's for axes?")
	}
	if state.PeripheralID != peripheralIDReply.Data {
		return errors.ErrInvalidArgument(
			fmt.Sprintf("Cannot set a state with peripheral ID %s to a peripheral with ID %s.",
				state.PeripheralID, peripheralIDReply.Data))
	}

	return nil
}

func (manager *saveStateManager) checkDevice(request setStateTarget, state *DeviceState) errors.SdkError {
	deviceIDReply, err := manager.deviceManager.singleRequestDevice(request, "get deviceid")
	if err != nil {
		return errors.ErrNotSupported("Cannot set state to a device with no ID.")
	}
	if state.DeviceID == "" {
		return errors.ErrInvalidArgument("Invalid save state. This save state cannot be set to a device, are you sure it's for devices?")
	}
	if state.DeviceID != deviceIDReply.Data {
		return errors.ErrInvalidArgument(
			fmt.Sprintf("Cannot set a state with device ID %s to a device with ID %s",
				state.DeviceID, deviceIDReply.Data))
	}

	return nil
}

func (manager *saveStateManager) canSetToPeripheral(request setStateTarget, withFirmwareVersion *pb.FirmwareVersion) (*pb.CanSetStateAxisResponse, *AxisState) {
	response := &pb.CanSetStateAxisResponse{
		AxisNumber: request.GetAxis(),
	}

	var state *AxisState
	if err := json.Unmarshal([]byte(request.GetState()), &state); err != nil {
		response.Error = utils.ToPointer("Could not deserialize the provided state: " + err.Error())
		return response, nil
	}

	if err := manager.checkContext(request, withFirmwareVersion, state.Context); err != nil {
		response.Error = utils.ToPointer(err.Message())
		return response, state
	}

	if err := manager.checkPeripheral(request, state.Axis); err != nil {
		response.Error = utils.ToPointer(err.Message())
	}
	return response, state
}

func (manager *saveStateManager) canSetToDevice(request setStateTarget, withFirmwareVersion *pb.FirmwareVersion, deviceOnly bool) (*pb.AsciiCanSetStateDeviceResponse, *DeviceState, map[int32]*PeripheralState) {
	var state *DeviceState
	if err := json.Unmarshal([]byte(request.GetState()), &state); err != nil {
		return &pb.AsciiCanSetStateDeviceResponse{
			Error: utils.ToPointer("Could not deserialize the provided state: " + err.Error()),
		}, nil, nil
	}

	if err := manager.checkContext(request, withFirmwareVersion, state.Context); err != nil {
		return &pb.AsciiCanSetStateDeviceResponse{
			Error: utils.ToPointer(err.Message()),
		}, state, nil
	}

	response := &pb.AsciiCanSetStateDeviceResponse{}

	if err := manager.checkDevice(request, state); err != nil {
		response.Error = utils.ToPointer(err.Message())
	}

	if deviceOnly {
		return response, state, nil
	}

	axes := make(map[int32]*PeripheralState)
	for address, peripheral := range state.Axes {
		addressInt64, err := strconv.ParseInt(address, 10, 32)
		if err != nil {
			return &pb.AsciiCanSetStateDeviceResponse{
				Error: utils.ToPointer(fmt.Sprintf("Could not parse peripheral address %s as an integer", address)),
			}, state, nil
		}

		addressInt32 := int32(addressInt64)
		axisTarget := &pb.SetStateRequest{InterfaceId: request.GetInterfaceId(), Device: request.GetDevice(), Axis: addressInt32}
		axes[addressInt32] = peripheral

		var axisResponse = &pb.AsciiCanSetStateAxisResponse{AxisNumber: addressInt32}
		response.AxisErrors = append(response.AxisErrors, axisResponse)

		if err := manager.checkPeripheral(axisTarget, peripheral); err != nil {
			axisResponse.Error = utils.ToPointer(err.Message())
		}
	}

	return response, state, axes
}

func (manager *saveStateManager) canSetState(request setStateTarget, withFirmwareVersion *pb.FirmwareVersion) *pb.AsciiCanSetStateDeviceResponse {
	check, _, _ := manager.canSetToDevice(request, withFirmwareVersion, false)
	return check
}

func (manager *saveStateManager) canSetAxisState(request setStateTarget, withFirmwareVersion *pb.FirmwareVersion) *pb.CanSetStateAxisResponse {
	check, _ := manager.canSetToPeripheral(request, withFirmwareVersion)
	return check
}
