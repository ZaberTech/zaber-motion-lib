package devices

import (
	"fmt"
	"math"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

func (manager *deviceManager) determinePacketSize(communication ioc.ASCIICommunicationInterface, devices []int32) errors.SdkError {
	packetSize := math.MaxInt32
	for _, deviceNumber := range devices {
		reply, err := commands.SingleCommand(communication.Requests(), c.Command{
			Device:  int(deviceNumber),
			Command: "get comm.packet.size.max",
		})
		if err != nil {
			if commands.IsBadCommandErr(err) {
				continue
			}
			return err
		}

		devicePacketSize, err := reply.DataAsInt()
		if err != nil {
			return err
		}

		if devicePacketSize < packetSize {
			packetSize = devicePacketSize
		}
	}
	if packetSize == math.MaxInt32 {
		packetSize = c.DefaultPacketSize
	}
	communication.SetPacketSize(packetSize)
	return nil
}

func (manager *deviceManager) detect(request *pb.DeviceDetectRequest) (*pb.DeviceDetectResponse, errors.SdkError) {
	communication, err := manager.getInterface(request)
	if err != nil {
		return nil, err
	}
	requests := communication.Requests()

	unknownResponseSubscription := communication.SubscribeForUnknownResponse(func(response *c.Response) bool {
		return response.ReplyFlag == "RJ" && response.ID < 0
	})

	replies, err := requests.Request(c.Command{}, &c.RequestOptions{
		CollectMultiple: true,
	})

	select {
	case unknownResponse := <-unknownResponseSubscription.Channel():
		return nil, errors.ErrNotSupported(
			fmt.Sprintf("Unsupported device with number %d detected. Please contact Zaber support to upgrade your device.", unknownResponse.Device))
	default:
		unknownResponseSubscription.Cancel()
	}

	if err != nil {
		if err.Type() == pb.Errors_REQUEST_TIMEOUT {
			err = errors.ErrNoDeviceFound()
		}
		return nil, err
	}

	devices := pie.Map(replies, func(reply *c.Response) int32 { return int32(reply.Device) })
	devices = pie.Sort(devices)
	if !pie.AreUnique(devices) {
		return nil, errors.ErrDeviceAddressConflict(devices)
	}
	if pie.Any(devices, func(address int32) bool { return address == 0 }) {
		return nil, errors.ErrInvalidData("Invalid device address: 0")
	}

	switch request.Type {
	case pb.DeviceType_PROCESS_CONTROLLER:
		var devicesFiltered []int32
		for _, address := range devices {
			isProcessController, err := manager.ipcManager.isUnidentifiedDeviceProcessController(requests, address)
			if err != nil {
				return nil, err
			} else if isProcessController {
				devicesFiltered = append(devicesFiltered, address)
			}
		}
		devices = devicesFiltered
	}

	response := &pb.DeviceDetectResponse{
		Devices: devices,
	}

	if err := manager.determinePacketSize(communication, devices); err != nil {
		return nil, err
	}

	if request.IdentifyDevices {
		for _, device := range devices {
			target := &pb.DeviceIdentifyRequest{
				InterfaceId: request.InterfaceId,
				Device:      device,
			}

			_, err := manager.identifyDevice(target)
			if err != nil {
				return nil, err
			}
		}
	}

	return response, nil
}

func (manager *deviceManager) forgetDevices(request *pb.ForgetDevicesRequest) errors.SdkError {
	return manager.clearAllDeviceInfo(request, request.ExceptDevices)
}
