package devices

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"

	"github.com/elliotchance/pie/v2"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

var bufferListReplyRegexp = regexp.MustCompile(`^buffer\s+(\d+)`)

var NonsavedSettings = map[string]struct{}{
	// Skip these settings because they are volatile
	"io.do.port": {}, "lamp.current": {}, "lamp.flux": {}, "lamp.pwm": {},
	"pos": {}, "scope.delay": {}, "scope.timebase": {},
	"user.vdata.0": {}, "user.vdata.1": {}, "user.vdata.2": {}, "user.vdata.3": {},
	// These are saved elsewhere
	"peripheral.id": {}, "device.id": {},
	// This is a calculated setting based on motion.accelonly and motion.decelonly. Since they are saved, it can be excluded
	"accel": {},
	// These should be handled by the user
	"comm.address": {}, "comm.protocol": {}, "comm.rs232.baud": {}, "comm.rs232.protocol": {}, "comm.usb.protocol": {},
}

func makeSaveStateContext(device *deviceInfo, isPeripheral bool) *SaveStateContext {
	saveStateType := "device"
	if isPeripheral {
		saveStateType = "peripheral"
	} else {
		for _, axis := range device.axes {
			if axis.isPeripheralLike {
				saveStateType = "controller"
			}
		}
	}
	return &SaveStateContext{
		Version:         device.productInfo.SaveStateVersion,
		Type:            saveStateType,
		FirmwareVersion: device.fwVersion,
	}
}

// Takes a table of settings and removes those that are:
// volatile (no point saving these),
// read only (nothing to do about them),
// are hardware_modification visibility (if the device is not modified)
func getSettingsToSave(allSettings []*dto.SettingsTableRow, isHwMod bool) []string {
	settingsToWrite := make([]string, 0)
	for _, row := range allSettings {
		isEndUserWritable := row.WriteAccessLevel == 1 || row.WriteAccessLevel == 2
		if !isEndUserWritable {
			continue
		}
		if !isHwMod && row.Visibility == "hardware_modification" {
			continue
		}
		if _, skip := NonsavedSettings[row.Name]; skip {
			continue
		}
		settingsToWrite = append(settingsToWrite, row.Name)
	}
	return settingsToWrite
}

func (manager *saveStateManager) getTriggers(request *pb.AxisEmptyRequest) (map[string]*Trigger, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	var numTriggers int
	if numTriggersReply, err := manager.deviceManager.singleRequestDevice(request, "get trigger.numtriggers"); err != nil {
		return nil, err
	} else if numTriggers, err = numTriggersReply.DataAsInt(); err != nil {
		return nil, err
	}

	triggers := make(map[string]*Trigger)

	for triggerNum := 1; triggerNum <= numTriggers; triggerNum++ {
		triggerInfos, err := commands.SingleCommandMultiResponse(requests, c.Command{
			Device:  int(request.GetDevice()),
			Axis:    int(request.GetAxis()),
			Command: fmt.Sprintf("trigger %d print", triggerNum),
		})
		if err != nil {
			if commands.IsBadDataErr(err) {
				break
			}
			return nil, err
		}
		trigger := &Trigger{}
		for _, triggerInfo := range triggerInfos {
			tokens := strings.Fields(triggerInfo.Data)
			switch tokens[0] {
			case "when":
				trigger.When = strings.Join(tokens[1:], " ")
			case "action":
				switch tokens[1] {
				case "a":
					trigger.A = strings.Join(tokens[2:], " ")
				case "b":
					trigger.B = strings.Join(tokens[2:], " ")
				}
			case "enable":
				if len(tokens) > 1 {
					trigger.Enabled = true
					count, err := strconv.ParseUint(tokens[1], 10, 64)
					if err != nil {
						return nil, errors.ErrInvalidData(fmt.Sprintf("Could not convert %s to uint for trigger %d enabled count", tokens[1], triggerNum))
					}
					trigger.Count = count
				} else {
					trigger.Enabled = true
					trigger.Count = 0
				}
			case "disable":
				trigger.Enabled = false
				trigger.Count = 0
			}
		}
		if trigger.When != "none" || trigger.A != "none" || trigger.B != "none" || trigger.Enabled {
			triggers[fmt.Sprint(triggerNum)] = trigger
		}
	}

	if len(triggers) > 0 {
		return triggers, nil
	} else {
		return nil, nil
	}
}

func getStreamBuffer(request deviceTarget, requests *c.RequestManager, bufNum int, pvt bool) (buffer *StreamBuffer, err errors.SdkError, errCmd string) {
	cmd := fmt.Sprintf("%s buffer %d print", getStreamPrefixBase(pvt), bufNum)
	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: cmd,
	})
	if err != nil {
		if commands.IsBadDataErr(err) {
			return nil, nil, ""
		}
		return nil, err, cmd
	}

	bufferedCommands := pie.Map(replies[1:], func(reply *c.Response) string { return reply.Data })
	return &StreamBuffer{bufferedCommands}, nil, ""
}

func listStreamBuffers(requests *c.RequestManager, request deviceTarget, pvt bool) (bufferList []int, err errors.SdkError, errCmd string) {
	var numBuffs int
	if pvt {
		cmd := "get pvt.numseqs"
		if _, err := singleCommandDevice(requests, request, cmd); err != nil {
			if commands.IsBadCommandErr(err) { // PVT not supported
				return nil, nil, ""
			}
			return nil, err, cmd
		}

		numBuffs = pvt733BufferLimit
	} else {
		cmd := "get stream.numbufs"
		if reply, err := singleCommandDevice(requests, request, cmd); err != nil {
			if commands.IsBadCommandErr(err) { // streams not supported
				return nil, nil, ""
			}
			return nil, err, cmd
		} else if numBuffs, err = reply.DataAsInt(); err != nil {
			return nil, err, cmd
		}
	}

	cmd := fmt.Sprintf("%s buffer list", getStreamPrefixBase(pvt))
	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: cmd,
	})
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return utils.Range(1, numBuffs+1), nil, ""
		}
		return nil, err, cmd
	}

	for _, reply := range replies[1:] {
		match := bufferListReplyRegexp.FindStringSubmatch(reply.Data)
		if match == nil {
			return nil, errors.ErrInvalidData(fmt.Sprintf("Could not parse buffer list reply: %s", reply.Data)), cmd
		}
		buffNum, _ := strconv.Atoi(match[1])
		bufferList = append(bufferList, buffNum)
	}
	return bufferList, nil, ""
}

func (manager *saveStateManager) getStreamBuffers(request *pb.AxisEmptyRequest, pvt bool) (map[string]*StreamBuffer, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	bufferList, err, _ := listStreamBuffers(requests, request, pvt)
	if err != nil || bufferList == nil {
		return nil, err
	}

	streamBuffers := make(map[string]*StreamBuffer)
	for _, buffNum := range bufferList {
		streamBuffer, err, _ := getStreamBuffer(request, requests, buffNum, pvt)
		if err != nil {
			return nil, err
		} else if streamBuffer != nil {
			streamBuffers[fmt.Sprint(buffNum)] = streamBuffer
		}
	}

	if len(streamBuffers) > 0 {
		return streamBuffers, nil
	} else {
		return nil, nil
	}
}

func (manager *saveStateManager) getStorage(request *pb.AxisEmptyRequest) (map[string]string, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	printCmd := "storage print"
	if request.Axis != 0 {
		printCmd = "storage axis print"
	}

	storage := make(map[string]string)

	storedValues, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: printCmd,
	})
	if err != nil {
		if commands.IsBadCommandErr(err) { // Returned by devices that don't support storage
			return storage, nil
		}
		return nil, err
	}

	for _, row := range storedValues {
		tokens := strings.SplitN(row.Data, " ", 3)
		if tokens[0] == "set" {
			key := tokens[1]
			value := tokens[2]
			currentValueStored, shouldAppend := storage[key]
			if shouldAppend {
				storage[key] = currentValueStored + " " + value
			} else {
				storage[key] = value
			}
		}
	}

	return storage, nil
}

func (manager *saveStateManager) getServoTunings(request *pb.AxisEmptyRequest) (*ServoTuning, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(request)
	if err != nil {
		return nil, err
	}

	var startupSet int
	if startupSetReply, err := manager.deviceManager.singleRequestAxis(request, "servo get startup"); err != nil {
		if commands.IsBadCommandErr(err) {
			return nil, nil
		}
		return nil, err
	} else if startupSet, err = startupSetReply.DataAsInt(); err != nil {
		return nil, err
	}

	tuning := &ServoTuning{
		Startup:   startupSet,
		Paramsets: make(map[string]Paramset),
	}

	for paramsetNum := 1; paramsetNum <= 9; paramsetNum++ {
		paramInfos, err := commands.SingleCommandMultiResponse(requests, c.Command{
			Device:  int(request.GetDevice()),
			Axis:    int(request.GetAxis()),
			Command: fmt.Sprintf("servo %d get all", paramsetNum),
		})
		if err != nil {
			if commands.IsBadDataErr(err) {
				break
			}
			return nil, err
		}
		paramset := Paramset{
			Parameters: make(map[string]string),
		}
		for _, paramInfo := range paramInfos {
			tokens := strings.Fields(paramInfo.Data)
			switch tokens[0] {
			case "type":
				paramset.Type = tokens[1]
			case "parameter":
				paramVal := tokens[2]
				paramset.Parameters[tokens[1]] = paramVal
			}
		}
		tuning.Paramsets[fmt.Sprint(paramsetNum)] = paramset
	}

	return tuning, nil
}

func (manager *saveStateManager) getStoredPositions(request *pb.AxisEmptyRequest) (map[string]int, errors.SdkError) {
	storedPositions := make(map[string]int, MaxStoredPositions)

	for i := 1; i <= MaxStoredPositions; i++ {
		if posResponse, err := manager.deviceManager.singleRequestAxis(request, fmt.Sprintf("tools storepos %d", i)); err != nil {
			if commands.IsBadCommandErr(err) {
				return nil, nil
			}
			return nil, err
		} else if pos, err := posResponse.DataAsInt(); err != nil {
			return nil, err
		} else {
			storedPositions[fmt.Sprint(i)] = pos
		}
	}

	return storedPositions, nil
}

func (manager *saveStateManager) getControllerState(request *pb.AxisEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.deviceManager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}
	if err := device.verifyIdentified(); err != nil {
		return nil, err
	}

	if device.fwVersion.Major == 7 {
		if device.productInfo.SaveStateVersion > MaxSaveStateVersion {
			return nil, errors.ErrNotSupported(fmt.Sprintf("No support for getting state of devices with firmware version %d.%d. Upgrade to the newest version of ZML.", device.fwVersion.Major, device.fwVersion.Minor))
		}
	} else {
		return nil, errors.ErrNotSupported(fmt.Sprintf("Cannot get state of firmware %d devices", device.fwVersion.Major))
	}

	productInfo := device.productInfo

	settingMap := make(map[string]string)
	for _, setting := range getSettingsToSave(productInfo.Settings.Rows, device.identity.IsModified) {
		settingVal, getErr := manager.deviceManager.singleRequestDevice(request, "get "+setting)
		if getErr != nil {
			return nil, errors.ErrNotSupported(fmt.Sprintf("Cannot save the current value of %s: %s", setting, getErr.Message()))
		}
		settingMap[setting] = settingVal.Data
	}

	ctx := makeSaveStateContext(device, false)

	axisStates := make(map[string]*PeripheralState)
	var servoTuning *ServoTuning
	var storedPositions map[string]int
	var axisStorage map[string]string
	if ctx.Type == "controller" {
		for i, peripheralInfo := range productInfo.Peripherals {
			axisAddress := i + 1
			axisRequest := &pb.AxisEmptyRequest{
				InterfaceId: request.InterfaceId,
				Device:      request.Device,
				Axis:        int32(axisAddress),
			}
			axisInfo, err := device.getAxisInfo(axisRequest)
			if err != nil {
				return nil, err
			}

			axis, err := manager.getAxis(axisRequest, peripheralInfo, axisInfo)
			if err != nil {
				return nil, err
			}
			if axis != nil {
				axisStates[fmt.Sprint(axisAddress)] = axis
			}
		}
	} else if ctx.Type == "device" {
		axisRequest := &pb.AxisEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      request.Device,
			Axis:        1,
		}
		if servoTuning, err = manager.getServoTunings(axisRequest); err != nil {
			return nil, err
		}
		if storedPositions, err = manager.getStoredPositions(axisRequest); err != nil {
			return nil, err
		}
		if axisStorage, err = manager.getStorage(axisRequest); err != nil {
			return nil, err
		}
	}

	triggers, err := manager.getTriggers(request)
	if err != nil {
		return nil, err
	}

	streamBuffers, err := manager.getStreamBuffers(request, false)
	if err != nil {
		return nil, err
	}

	pvtBuffers, err := manager.getStreamBuffers(request, true)
	if err != nil {
		return nil, err
	}

	storage, err := manager.getStorage(request)
	if err != nil {
		return nil, err
	}

	state := DeviceState{
		Context:         ctx,
		DeviceID:        fmt.Sprintf("%d", device.identity.DeviceId), // TODO: make them number
		SerialNumber:    fmt.Sprintf("%d", device.identity.SerialNumber),
		Settings:        settingMap,
		Axes:            axisStates,
		Triggers:        triggers,
		StreamBuffers:   streamBuffers,
		PvtBuffers:      pvtBuffers,
		ServoTuning:     servoTuning,
		StoredPositions: storedPositions,
		Storage:         storage,
		AxisStorage:     axisStorage,
	}

	serializedState, serErr := json.Marshal(state)
	if serErr != nil {
		return nil, errors.ErrInternal("Could not serialize the device state.")
	}

	return &pb.StringResponse{
		Value: string(serializedState),
	}, nil
}

// Gets the state of an axis or peripheral, returning nil if the axis is unidentified
func (manager *saveStateManager) getAxis(request *pb.AxisEmptyRequest, productInfo *dto.Peripheral, axisInfo *axisInfo) (*PeripheralState, errors.SdkError) {
	peripheralIDReply, err := manager.deviceManager.singleRequestAxis(request, "get peripheral.id")
	if err != nil {
		return nil, err
	}
	peripheralID, err := peripheralIDReply.DataAsInt()
	if err != nil {
		return nil, err
	} else if peripheralID == 0 {
		return nil, nil
	}

	serialNumber := ""
	if serialNumberReply, err := manager.deviceManager.singleRequestAxis(request, "get peripheral.serial"); err != nil {
		if !commands.IsBadCommandErr(err) {
			return nil, err
		}
	} else {
		serialNumber = serialNumberReply.Data
	}

	settingMap := make(map[string]string)
	for _, setting := range getSettingsToSave(productInfo.Settings.Rows, axisInfo.identity.IsModified) {
		settingVal, getErr := manager.deviceManager.singleRequestAxis(request, "get "+setting)
		if getErr != nil {
			return nil, errors.ErrNotSupported(fmt.Sprintf("Cannot save the current value of %s: %s", setting, getErr.Message()))
		}
		settingMap[setting] = settingVal.Data
	}

	tuning, err := manager.getServoTunings(request)
	if err != nil {
		return nil, err
	}

	storedPositions, err := manager.getStoredPositions(request)
	if err != nil {
		return nil, err
	}

	storage, err := manager.getStorage(request)
	if err != nil {
		return nil, err
	}

	return &PeripheralState{
		PeripheralID:    fmt.Sprintf("%d", axisInfo.identity.PeripheralId),
		SerialNumber:    serialNumber,
		Settings:        settingMap,
		ServoTuning:     tuning,
		StoredPositions: storedPositions,
		Storage:         storage,
	}, nil
}

func (manager *saveStateManager) getAxisState(request *pb.AxisEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.deviceManager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}
	if err := device.verifyIdentified(); err != nil {
		return nil, err
	}
	axisInfo, err := device.getAxisInfo(request)
	if err != nil {
		return nil, err
	}

	if device.fwVersion.Major == 7 {
		if device.productInfo.SaveStateVersion > MaxSaveStateVersion {
			return nil, errors.ErrNotSupported(fmt.Sprintf("No support for getting state of peripherals on controllers with firmware version %d.%d. Upgrade to the newest version of ZML.", device.fwVersion.Major, device.fwVersion.Minor))
		}
	} else {
		return nil, errors.ErrNotSupported(fmt.Sprintf("Cannot get state of peripherals on firmware %d devices", device.fwVersion.Major))
	}

	productInfo := device.productInfo

	if len(productInfo.Peripherals) < int(request.Axis) {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("No peripheral at address %d", request.Axis))
	}
	peripheralInfo := productInfo.Peripherals[request.GetAxis()-1]
	if peripheralInfo == nil {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Could not get product information for axis %d", request.GetAxis()))
	}

	axis, err := manager.getAxis(request, peripheralInfo, axisInfo)
	if err != nil {
		return nil, err
	}
	if axis == nil {
		return nil, errors.ErrNotSupported("Could not get this axis' state")
	}
	state := AxisState{
		Context: makeSaveStateContext(device, true),
		Axis:    axis,
	}

	serializedState, serErr := json.Marshal(state)
	if serErr != nil {
		return nil, errors.ErrInternal("Could not serialize the device state.")
	}

	return &pb.StringResponse{
		Value: string(serializedState),
	}, nil
}

func (manager *saveStateManager) getState(request *pb.AxisEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	if request.Axis == 0 {
		return manager.getControllerState(request)
	} else {
		return manager.getAxisState(request)
	}
}
