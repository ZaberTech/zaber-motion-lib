package devices

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

type lockstepGroup struct {
	axes []int32
}

type lockstepSecondaryAxisInfo struct {
	axis   int32
	offset float64
	twist  float64
}

type lockstepInfo struct {
	enabled       bool
	primaryAxis   int32
	secondaryAxes []lockstepSecondaryAxisInfo
}

func singleCommandLockstep(requests *c.RequestManager, request lockstepTarget, lockstepCommand string) (*c.Response, errors.SdkError) {
	command := fmt.Sprintf("lockstep %d %s", request.GetLockstepGroupId(), lockstepCommand)
	return commands.SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Command: command,
	})
}

func lockstepGetInfo(requests *c.RequestManager, target lockstepTarget) (*lockstepInfo, errors.SdkError) {
	reply, err := singleCommandLockstep(requests, target, "info")
	if err != nil {
		return nil, err
	}

	lockstepInfoResponseParts := strings.Split(reply.Data, " ")
	if len(lockstepInfoResponseParts) == 0 {
		return nil, errors.ErrInvalidData("Cannot parse lockstep info reply: " + reply.Data)
	}

	if lockstepInfoResponseParts[0] == "disabled" {
		return &lockstepInfo{
			enabled: false,
		}, nil
	}

	primaryAxis, convErr := strconv.Atoi(lockstepInfoResponseParts[0])
	if convErr != nil {
		return nil, errors.ErrInvalidData("Failed to parse lockstepInfoResponseParts: " + reply.Data)
	}

	info := &lockstepInfo{
		enabled:     true,
		primaryAxis: int32(primaryAxis),
	}

	secondaryAxisIndex := 1
	for secondaryAxisIndex+2 < len(lockstepInfoResponseParts) {
		axis, convErr := strconv.Atoi(lockstepInfoResponseParts[secondaryAxisIndex])
		if convErr != nil {
			return nil, errors.ErrInvalidData("Failed to parse lockstepInfoResponseParts: " + reply.Data)
		}

		offset, convErr := strconv.ParseFloat(lockstepInfoResponseParts[secondaryAxisIndex+1], 64)
		if convErr != nil {
			return nil, errors.ErrInvalidData("Failed to parse lockstepInfoResponseParts: " + reply.Data)
		}

		twist, convErr := strconv.ParseFloat(lockstepInfoResponseParts[secondaryAxisIndex+2], 64)
		if convErr != nil {
			return nil, errors.ErrInvalidData("Failed to parse lockstepInfoResponseParts: " + reply.Data)
		}

		info.secondaryAxes = append(info.secondaryAxes, lockstepSecondaryAxisInfo{
			axis:   int32(axis),
			offset: offset,
			twist:  twist,
		})
		secondaryAxisIndex += 3
	}

	return info, nil
}

func (manager *deviceManager) registerLockstepCallbacks() {
	gateway := manager.gateway
	gateway.RegisterCallback("device/lockstep_enable", func() pb.Message {
		return &pb.LockstepEnableRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepEnable(request.(*pb.LockstepEnableRequest))
	})
	gateway.RegisterCallback("device/lockstep_disable", func() pb.Message {
		return &pb.LockstepDisableRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepDisable(request.(*pb.LockstepDisableRequest))
	})
	gateway.RegisterCallback("device/lockstep_stop", func() pb.Message {
		return &pb.LockstepStopRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepStop(request.(*pb.LockstepStopRequest))
	})
	gateway.RegisterCallback("device/lockstep_home", func() pb.Message {
		return &pb.LockstepHomeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepHome(request.(*pb.LockstepHomeRequest))
	})
	gateway.RegisterCallback("device/lockstep_move", func() pb.Message {
		return &pb.LockstepMoveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepMove(request.(*pb.LockstepMoveRequest))
	})
	gateway.RegisterCallback("device/lockstep_move_sin", func() pb.Message {
		return &pb.LockstepMoveSinRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepMoveSin(request.(*pb.LockstepMoveSinRequest))
	})
	gateway.RegisterCallback("device/lockstep_move_sin_stop", func() pb.Message {
		return &pb.LockstepStopRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepMoveSinStop(request.(*pb.LockstepStopRequest))
	})
	gateway.RegisterCallback("device/lockstep_is_busy", func() pb.Message {
		return &pb.LockstepEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepIsBusy(request.(*pb.LockstepEmptyRequest))
	})
	gateway.RegisterCallback("device/lockstep_wait_until_idle", func() pb.Message {
		return &pb.LockstepWaitUntilIdleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepWaitUntilIdle(request.(*pb.LockstepWaitUntilIdleRequest))
	})
	gateway.RegisterCallback("device/lockstep_get_axes", func() pb.Message {
		return &pb.LockstepEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepGetAxes(request.(*pb.LockstepEmptyRequest))
	})
	gateway.RegisterCallback("device/lockstep_get_axis_numbers", func() pb.Message {
		return &pb.LockstepEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepGetAxisNumbers(request.(*pb.LockstepEmptyRequest))
	})
	gateway.RegisterCallback("device/lockstep_get_offsets", func() pb.Message {
		return &pb.LockstepGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepGetOffsets(request.(*pb.LockstepGetRequest))
	})
	gateway.RegisterCallback("device/lockstep_get_twists", func() pb.Message {
		return &pb.LockstepGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepGetTwists(request.(*pb.LockstepGetRequest))
	})
	gateway.RegisterCallback("device/lockstep_get_pos", func() pb.Message {
		return &pb.LockstepGetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepGetPosition(request.(*pb.LockstepGetRequest))
	})
	gateway.RegisterCallback("device/lockstep_set_tolerance", func() pb.Message {
		return &pb.LockstepSetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.lockstepSetTolerance(request.(*pb.LockstepSetRequest))
	})
	gateway.RegisterCallback("device/lockstep_is_enabled", func() pb.Message {
		return &pb.LockstepEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepIsEnabled(request.(*pb.LockstepEmptyRequest))
	})
	gateway.RegisterCallback("device/lockstep_to_string", func() pb.Message {
		return &pb.LockstepEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.lockstepToString(request.(*pb.LockstepEmptyRequest))
	})
}

func (manager *deviceManager) lockstepMoveCommand(request lockstepWaitUntilIdleTarget, move string) errors.SdkError {
	communication, err := manager.getInterface(request)
	if err != nil {
		return err
	}

	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	reply, err := singleCommandLockstep(communication.Requests(), request, move)
	if err != nil {
		return err
	}

	// lockstep warnings occur on an axis level, thus we need to process
	// warnings that may have resulted from a move command by checking and
	// clearing each axis of the group

	var firstAxisWarnings *WarningsFlags
	if commands.HasWarnings(reply) {
		allWarnings := make(map[int32]*WarningsFlags)
		device := request.GetDevice()

		for i, axis := range group.axes {
			warnings, _, err := GetAxisWarnings(communication.Requests(), device, axis, true)
			if err != nil {
				return err
			}
			allWarnings[axis] = warnings
			if i == 0 {
				firstAxisWarnings = warnings
			}
		}

		for axis, warnings := range allWarnings {
			if err := CheckErrorFlags(warnings, MovementErrorWarningFlags, errors.BindErrMovementFailed(device, axis)); err != nil {
				return err
			}
		}
	}

	if !request.GetWaitUntilIdle() {
		return nil
	}

	var axesErrs []errors.SdkError

	firstAxis := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        group.axes[0],
	}
	if err := WaitForMovementToComplete(communication, firstAxis, firstAxisWarnings); err != nil {
		axesErrs = append(axesErrs, err)
	}

	if movementErrs, err := lockstepGetSecondaryAxesErrors(communication.Requests(), group, request); err != nil {
		return err
	} else {
		axesErrs = append(axesErrs, movementErrs...)
	}

	if len(axesErrs) > 0 {
		return axesErrs[0]
	}
	return nil
}

func lockstepGetSecondaryAxesErrors(requests *c.RequestManager, group *lockstepGroup, request lockstepTarget) (movementErrors []errors.SdkError, err errors.SdkError) {
	device := request.GetDevice()
	for _, axis := range group.axes[1:] {
		warnings, _, err := GetAxisWarnings(requests, device, axis, true)
		if err != nil {
			return movementErrors, err
		}
		if err := CheckErrorFlags(warnings, MovementErrorWarningFlags, errors.BindErrMovementFailed(device, axis)); err != nil {
			movementErrors = append(movementErrors, err)
		}
	}
	return movementErrors, err
}

func (manager *deviceManager) getCachedOrQueryLockstepInfo(target lockstepTarget, forceQuery bool) (*lockstepGroup, errors.SdkError) {
	devInfo, err := manager.getDeviceInfo(target)
	if err != nil {
		return nil, err
	}

	devInfo.lockstepLock.Lock()
	defer devInfo.lockstepLock.Unlock()

	if devInfo.lockstepGroups == nil {
		devInfo.lockstepGroups = make(map[int]*lockstepGroup)
	}

	id := int(target.GetLockstepGroupId())
	if group, ok := devInfo.lockstepGroups[id]; ok && !forceQuery {
		return group, nil
	} else {
		requests, err := manager.getRequests(target)
		if err != nil {
			return nil, err
		}

		lockstepInfo, err := lockstepGetInfo(requests, target)
		if err != nil {
			return nil, err
		}

		if !lockstepInfo.enabled {
			delete(devInfo.lockstepGroups, id)
			return nil, nil
		}

		newGroup := lockstepGroup{}
		newGroup.axes = []int32{lockstepInfo.primaryAxis}
		for _, axisInfo := range lockstepInfo.secondaryAxes {
			newGroup.axes = append(newGroup.axes, axisInfo.axis)
		}

		devInfo.lockstepGroups[id] = &newGroup
		return &newGroup, nil
	}
}

func (manager *deviceManager) lockstepEnable(request *pb.LockstepEnableRequest) errors.SdkError {
	if len(request.Axes) < 2 {
		return errors.ErrInvalidArgument(
			fmt.Sprintf("Lockstep must be enabled on at least two axes (provided: %d).", len(request.Axes)))
	}
	requests, err := manager.getRequests(request)
	if err != nil {
		return err
	}

	devInfo, err := manager.getDeviceInfo(request)
	if err != nil {
		return err
	}

	devInfo.lockstepLock.Lock()
	defer devInfo.lockstepLock.Unlock()

	cmdParts := []string{"setup", "enable"}
	axes := make([]int32, 0, len(request.Axes))

	for _, axis := range request.Axes {
		cmdParts = append(cmdParts, fmt.Sprintf("%d", axis))
		axes = append(axes, axis)
	}

	_, err = singleCommandLockstep(requests, request, strings.Join(cmdParts, " "))
	if err != nil {
		return err
	}

	if devInfo.lockstepGroups == nil {
		devInfo.lockstepGroups = make(map[int]*lockstepGroup)
	}

	id := int(request.GetLockstepGroupId())
	devInfo.lockstepGroups[id] = &lockstepGroup{
		axes: axes,
	}
	return nil
}

func (manager *deviceManager) lockstepDisable(request *pb.LockstepDisableRequest) errors.SdkError {
	requests, err := manager.getRequests(request)
	if err != nil {
		return err
	}

	devInfo, err := manager.getDeviceInfo(request)
	if err != nil {
		return err
	}

	devInfo.lockstepLock.Lock()
	defer devInfo.lockstepLock.Unlock()

	id := int(request.GetLockstepGroupId())
	// this is assumed to no op if it does not exist
	delete(devInfo.lockstepGroups, id)

	_, err = singleCommandLockstep(requests, request, "setup disable")
	if err != nil && !commands.IsBadDataErr(err) {
		return err
	}

	return nil
}

func (manager *deviceManager) lockstepStop(request *pb.LockstepStopRequest) errors.SdkError {
	return manager.lockstepMoveCommand(request, "stop")
}

func (manager *deviceManager) lockstepHome(request *pb.LockstepHomeRequest) errors.SdkError {
	return manager.lockstepMoveCommand(request, "home")
}

func (manager *deviceManager) lockstepMove(request *pb.LockstepMoveRequest) errors.SdkError {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	moveType := strings.ToLower(pb.AxisMoveType_name[(request.Type)])

	var commandTemplate []string
	var args []commandbuilding.CommandArg
	requiresArgument := request.Type != pb.AxisMoveType_MIN && request.Type != pb.AxisMoveType_MAX
	if requiresArgument {
		commandTemplate = []string{"move", moveType, constants.CommandArgument}
		args = []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Arg, Unit: request.Unit}}
	} else {
		commandTemplate = []string{"move", moveType}
	}

	if request.Acceleration != 0 && request.Velocity == 0 && request.Type != pb.AxisMoveType_VEL {
		if maxspeedReply, err := manager.singleRequestDevice(request, "get maxspeed"); err != nil {
			return err
		} else if maxspeeds, err := maxspeedReply.DataAsFloatArray(true); err != nil {
			return err
		} else {
			minSpeed := math.Inf(1)
			for _, axis := range group.axes {
				if speed := maxspeeds[axis-1]; speed < minSpeed {
					minSpeed = speed
				}
			}
			request.Velocity = minSpeed
			request.VelocityUnit = constants.NativeUnit
		}
	}

	if request.Velocity != 0 {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		args = append(args, commandbuilding.CommandArgImpl{Value: request.Velocity, Unit: request.VelocityUnit})
	}
	if request.Acceleration != 0 {
		commandTemplate = append(commandTemplate, constants.CommandArgument)
		value := request.Acceleration
		if math.IsInf(value, 1) {
			value = 0
		}
		args = append(args, commandbuilding.CommandArgImpl{Value: value, Unit: request.AccelerationUnit})
	}

	firstAxisInGroup := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        group.axes[0],
	}

	command, err := manager.buildCommand(firstAxisInGroup, commandTemplate, args)
	if err != nil {
		return err
	}

	return manager.lockstepMoveCommand(request, command)
}

func (manager *deviceManager) lockstepMoveSin(request *pb.LockstepMoveSinRequest) errors.SdkError {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	var commandTemplate []string

	commandTemplate = []string{"move", "sin", constants.CommandArgument, constants.CommandArgument}
	if request.Count != 0 {
		commandTemplate = append(commandTemplate, fmt.Sprint(request.Count))
	}

	args := []commandbuilding.CommandArg{
		commandbuilding.CommandArgImpl{Value: float64(request.GetAmplitude()), Unit: request.GetAmplitudeUnits()},
		commandbuilding.CommandArgImpl{Value: float64(request.GetPeriod()), Unit: request.GetPeriodUnits()},
	}

	firstAxisInGroup := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        group.axes[0],
	}

	command, err := manager.buildCommand(firstAxisInGroup, commandTemplate, args)
	if err != nil {
		return err
	}

	return manager.lockstepMoveCommand(request, command)
}

func (manager *deviceManager) lockstepMoveSinStop(request *pb.LockstepStopRequest) errors.SdkError {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	return manager.lockstepMoveCommand(request, "move sin stop")
}

func (manager *deviceManager) lockstepIsBusy(request *pb.LockstepEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return nil, err
	}
	if group == nil {
		return nil, errors.ErrLockstepNotEnabled()
	}

	axisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        group.axes[0],
	}

	reply, err := manager.singleRequestAxis(axisTarget, "")
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: reply.Status != "IDLE",
	}, nil
}

func (manager *deviceManager) lockstepWaitUntilIdle(request *pb.LockstepWaitUntilIdleRequest) errors.SdkError {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	communication, err := manager.getInterface(request)
	if err != nil {
		return err
	}

	var axesErrs []errors.SdkError

	firstAxis := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        group.axes[0],
	}
	options := WaitUntilIdleOptions{
		CheckErrors: request.ThrowErrorOnFault,
	}
	if err := WaitUntilIdle(communication, firstAxis, options); err != nil {
		axesErrs = append(axesErrs, err)
	}

	if request.ThrowErrorOnFault {
		if movementErrs, err := lockstepGetSecondaryAxesErrors(communication.Requests(), group, request); err != nil {
			return err
		} else {
			axesErrs = append(axesErrs, movementErrs...)
		}
	}

	if len(axesErrs) > 0 {
		return axesErrs[0]
	}
	return nil
}

func (manager *deviceManager) lockstepGetAxes(request *pb.LockstepEmptyRequest) (*pb.AsciiLockstepAxes, errors.SdkError) {
	group, err := manager.getCachedOrQueryLockstepInfo(request, true)
	if err != nil {
		return nil, err
	}
	if group == nil {
		return nil, errors.ErrLockstepNotEnabled()
	}

	axes := &pb.AsciiLockstepAxes{
		Axis1: group.axes[0],
		Axis2: group.axes[1],
	}
	if len(group.axes) > 2 {
		axes.Axis3 = group.axes[2]
	}
	if len(group.axes) > 3 {
		axes.Axis4 = group.axes[3]
	}

	return axes, nil
}

func (manager *deviceManager) lockstepGetAxisNumbers(request *pb.LockstepEmptyRequest) (*pb.LockstepGetAxisNumbersResponse, errors.SdkError) {
	group, err := manager.getCachedOrQueryLockstepInfo(request, true)
	if err != nil {
		return nil, err
	}
	if group == nil {
		return nil, errors.ErrLockstepNotEnabled()
	}
	return &pb.LockstepGetAxisNumbersResponse{
		Axes: group.axes,
	}, nil
}

func (manager *deviceManager) lockstepGetOffsets(request *pb.LockstepGetRequest) (*pb.DoubleArrayResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	lockstepInfo, err := lockstepGetInfo(requests, request)
	if err != nil {
		return nil, err
	}

	if !lockstepInfo.enabled {
		return nil, errors.ErrLockstepNotEnabled()
	}

	axisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        lockstepInfo.primaryAxis,
	}

	offsets := make([]float64, len(lockstepInfo.secondaryAxes))
	for i, axisInfo := range lockstepInfo.secondaryAxes {
		convertedValue, err := manager.ConvertUnitSetting(axisTarget, "pos", axisInfo.offset, request.GetUnit(), true)
		if err != nil {
			return nil, err
		}
		offsets[i] = convertedValue
	}

	return &pb.DoubleArrayResponse{
		Values: offsets,
	}, nil
}

func (manager *deviceManager) lockstepGetTwists(request *pb.LockstepGetRequest) (*pb.DoubleArrayResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	lockstepInfo, err := lockstepGetInfo(requests, request)
	if err != nil {
		return nil, err
	}

	if !lockstepInfo.enabled {
		return nil, errors.ErrLockstepNotEnabled()
	}

	axisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        lockstepInfo.primaryAxis,
	}

	twists := make([]float64, len(lockstepInfo.secondaryAxes))
	for i, axisInfo := range lockstepInfo.secondaryAxes {
		convertedValue, err := manager.ConvertUnitSetting(axisTarget, "pos", axisInfo.twist, request.GetUnit(), true)
		if err != nil {
			return nil, err
		}
		twists[i] = convertedValue
	}

	return &pb.DoubleArrayResponse{
		Values: twists,
	}, nil
}

func (manager *deviceManager) lockstepGetPosition(request *pb.LockstepGetRequest) (*pb.DoubleResponse, errors.SdkError) {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return nil, err
	}
	if group == nil {
		return nil, errors.ErrLockstepNotEnabled()
	}

	return manager.getSetting(&pb.DeviceGetSettingRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		Axis:        group.axes[0],
		Setting:     "pos",
		Unit:        request.Unit,
	})
}

func (manager *deviceManager) lockstepSetTolerance(request *pb.LockstepSetRequest) errors.SdkError {
	group, err := manager.getCachedOrQueryLockstepInfo(request, false)
	if err != nil {
		return err
	}
	if group == nil {
		return errors.ErrLockstepNotEnabled()
	}

	commandTemplate := []string{"lockstep", constants.CommandArgument, "set"}

	axisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
	}
	if request.AxisIndex > 0 {
		if int(request.AxisIndex) >= len(group.axes) {
			return errors.ErrInvalidArgument("Axis index is out of range.")
		}
		axisTarget.axis = group.axes[request.AxisIndex]
		commandTemplate = append(commandTemplate, convertStreamAxisToLetter(request.AxisIndex))
	} else {
		axisTarget.axis = group.axes[0]
	}

	commandTemplate = append(commandTemplate, "tolerance", constants.CommandArgument)
	toleranceNative, err := manager.ConvertUnitSetting(axisTarget, "pos", request.Value, request.GetUnit(), false)
	if err != nil {
		return err
	}

	deviceTarget := &axisTargetImpl{
		interfaceID: axisTarget.interfaceID,
		device:      axisTarget.device,
	}
	command, err := manager.buildCommand(deviceTarget, commandTemplate, []commandbuilding.CommandArg{
		commandbuilding.CommandArgImpl{Value: float64(request.LockstepGroupId)},
		commandbuilding.CommandArgImpl{Value: toleranceNative},
	})
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, command)
	return err
}

func (manager *deviceManager) lockstepIsEnabled(request *pb.LockstepEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	group, err := manager.getCachedOrQueryLockstepInfo(request, true)
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: group != nil,
	}, nil
}

func (manager *deviceManager) lockstepToString(request *pb.LockstepEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("Lockstep %d -> Device %d -> Connection Closed", request.LockstepGroupId, request.Device),
			}, nil
		}
		return nil, err
	}

	return &pb.StringResponse{
		Value: fmt.Sprintf("Lockstep %d -> %s -> %s",
			request.GetLockstepGroupId(),
			device.ToString(""),
			device.interfaceInfo.communication.ToString()),
	}, nil
}
