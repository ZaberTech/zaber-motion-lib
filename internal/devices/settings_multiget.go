package devices

import (
	"fmt"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	"github.com/elliotchance/pie/v2"
)

// The Axes that this setting should be read for
type settingScope struct {
	readDeviceScope bool
	axes            map[int]struct{}
}

func (scope *settingScope) equals(otherScope *settingScope) bool {
	if scope.readDeviceScope && otherScope.readDeviceScope {
		return true
	}

	for axis := range scope.axes {
		if _, scopeIncludesAxis := otherScope.axes[axis]; !scopeIncludesAxis {
			return false
		}
	}

	return true
}

// return a unique value for any scope
func (scope settingScope) sortingValue() int {
	if scope.readDeviceScope {
		return 1
	}
	value := 0
	for axis := range scope.axes {
		value += 1 << axis
	}
	return value
}

func (scope settingScope) sortedAxes() []int {
	if scope.readDeviceScope {
		return []int{0}
	}
	return pie.Sort(pie.Keys[int](scope.axes))
}

func (scope settingScope) print() string {
	return pie.Join(scope.sortedAxes(), " ")
}

func (scope *settingScope) hasAllAxes(axisCount int) bool {
	if axisCount == 0 {
		return false
	}
	for i := 1; i <= axisCount; i++ {
		if _, hasAxisI := scope.axes[i]; !hasAxisI {

			return false
		}
	}

	return true
}

func (scope *settingScope) add(axes []int32, axisCount int) {
	if len(axes) == 0 || axes[0] == 0 {
		scope.readDeviceScope = true
	}
	if scope.readDeviceScope {
		return
	}
	for _, axis := range axes {
		scope.axes[int(axis)] = struct{}{}
	}
	scope.readDeviceScope = scope.hasAllAxes(axisCount)
}

func newSettingScope(axes []int32, axisCount int) settingScope {
	scope := settingScope{axes: make(map[int]struct{}, 0)}
	scope.add(axes, axisCount)
	return scope
}

func validateAxes(axes []int32, axisCount int) errors.SdkError {
	if len(axes) == 0 || (len(axes) == 1 && axes[0] == 0) {
		return nil
	}
	for _, axis := range axes {
		if axis < 0 {
			return errors.ErrInvalidArgument(fmt.Sprintf("Invalid axis number %d", axis))
		}
		if axis == 0 {
			return errors.ErrInvalidArgument("Invalid 0 in list of axis numbers. To read device scope settings, pass [] or [0]")
		}
		if axisCount > 0 && int(axis) > axisCount {
			return errors.ErrInvalidArgument(fmt.Sprintf("Invalid axis number %d on a device with %d axes", axis, axisCount))
		}
	}
	return nil
}

func multiGetConvertUnit(manager *deviceManager, request *pb.DeviceMultiGetSettingRequest, info *deviceInfo, value float64, unit string, setting string, axis int32) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	}
	if !info.isIdentified() {
		return 0, errors.ErrDeviceNotIdentified(int(request.Device))
	}
	settingDescription := fmt.Sprintf("setting \"%s\" of axis %d", setting, axis)
	axisRequest := &pb.AxisEmptyRequest{InterfaceId: request.InterfaceId, Device: request.Device, Axis: axis}
	if axis == 1 {
		_, isDeviceScopedSetting := info.commandIssuingInfo.SettingsTable.GetParamInfo(setting)
		if isDeviceScopedSetting {
			settingDescription = fmt.Sprintf("setting \"%s\" of device", setting)
			axisRequest.Axis = 0
		}
	}
	valueConverted, err := manager.ConvertUnitSetting(axisRequest, setting, value, unit, true)
	if err != nil {
		return 0, errors.ErrConversionFailed(fmt.Sprintf("Could not convert %s to \"%s\"", settingDescription, unit))
	}
	return valueConverted, nil
}

func (manager *deviceManager) getManySettings(request *pb.DeviceMultiGetSettingRequest, synchronized bool) (pb.Message, errors.SdkError) {
	isAxis := request.Axis != 0
	for _, setting := range request.AxisSettings {
		request.Settings = append(request.Settings, &pb.AsciiGetSetting{
			Setting: setting.Setting,
			Axes:    []int32{request.Axis},
			Unit:    setting.Unit,
		})
	}

	info, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}
	axisCount := 0
	if info.isIdentified() {
		axisCount = len(info.axes)
	}

	settingScopes := map[string]settingScope{}
	for _, getRequest := range request.Settings {
		validateErr := validateAxes(getRequest.Axes, axisCount)
		if validateErr != nil {
			return nil, validateErr
		}
		if scope, hasSetting := settingScopes[getRequest.Setting]; hasSetting {
			scope.add(getRequest.Axes, axisCount)
		} else {
			settingScopes[getRequest.Setting] = newSettingScope(getRequest.Axes, axisCount)
		}
	}

	limits, _, err := manager.GetProtocolInfo(request)
	if err != nil {
		return nil, err
	}

	if synchronized {
		if limits.MaxSyncSettings == 0 {
			return nil, errors.ErrNotSupported("This device does not support synchronized getting.")
		}
		if limits.MaxSyncSettings < len(settingScopes) {
			return nil, errors.ErrNotSupported(fmt.Sprintf("This device does not support getting %d synchronized settings. Max synchronized settings: %d.", len(settingScopes), limits.MaxSyncSettings))
		}
	}

	settingsToRead := pie.Keys(settingScopes)
	sortedSettings := pie.SortUsing[string](settingsToRead, func(i, j string) bool {
		iScopeSortingValue := settingScopes[i].sortingValue()
		jScopeSortingValue := settingScopes[j].sortingValue()
		if iScopeSortingValue != jScopeSortingValue {
			return iScopeSortingValue < jScopeSortingValue
		}
		return i < j
	})

	nativeResults := map[string]map[int]float64{}
	if limits.MaxSyncSettings == 0 {
		for _, setting := range sortedSettings {
			nativeResults[setting] = map[int]float64{}
			reply, err := manager.singleRequestDevice(request, "get "+setting)
			if err != nil {
				return nil, err
			}
			results, err := reply.DataAsFloatArray(true)
			if err != nil {
				return nil, err
			}
			for i, result := range results {
				nativeResults[setting][i+1] = result
			}
		}
	} else {
		for readFrom := 0; readFrom < len(sortedSettings); readFrom += limits.MaxSyncSettings {
			prevScope := settingScope{readDeviceScope: true}
			command := ""
			readTo := pie.Min[int]([]int{readFrom + limits.MaxSyncSettings, len(sortedSettings)})
			limitedSettings := pie.SubSlice[string](sortedSettings, readFrom, readTo)
			for _, setting := range limitedSettings {
				scope := settingScopes[setting]
				if !scope.equals(&prevScope) {
					command += " " + scope.print()
				}
				prevScope = scope
				command += " " + setting
			}

			reply, err := manager.singleRequestDevice(request, "get"+command)
			if err != nil {
				return nil, err
			}
			results, err := reply.DataAsFloatArrayArray(true)
			if err != nil {
				return nil, err
			}

			for i, setting := range limitedSettings {
				scope := settingScopes[setting]
				nativeResults[setting] = map[int]float64{}
				if scope.readDeviceScope {
					for j, result := range results[i] {
						nativeResults[setting][j+1] = result
					}
				} else {
					for j, axis := range scope.sortedAxes() {
						nativeResults[setting][axis] = results[i][j]
					}
				}
			}
		}
	}

	results := make([]*pb.AsciiGetSettingResult, len(request.Settings))
	for i, getRequest := range request.Settings {
		var values []float64
		if isDevice := len(getRequest.Axes) == 0 || getRequest.Axes[0] == 0; isDevice {
			values = make([]float64, len(nativeResults[getRequest.Setting]))
			for axis, value := range nativeResults[getRequest.Setting] {
				valueConverted, err := multiGetConvertUnit(manager, request, info, value, getRequest.Unit, getRequest.Setting, int32(axis))
				if err != nil {
					return nil, err
				}
				values[axis-1] = valueConverted
			}
		} else {
			values = make([]float64, len(getRequest.Axes))
			for j, axis := range getRequest.Axes {
				value := nativeResults[getRequest.Setting][int(axis)]
				valueConverted, err := multiGetConvertUnit(manager, request, info, value, getRequest.Unit, getRequest.Setting, axis)
				if err != nil {
					return nil, err
				}
				values[j] = valueConverted
			}
		}
		results[i] = &pb.AsciiGetSettingResult{
			Setting: getRequest.Setting,
			Values:  values,
			Unit:    getRequest.Unit,
		}
	}

	if isAxis {
		axisResults := make([]*pb.AsciiGetAxisSettingResult, len(results))
		for i, result := range results {
			axisResults[i] = &pb.AsciiGetAxisSettingResult{
				Setting: result.Setting,
				Value:   result.Values[0],
				Unit:    result.Unit,
			}
		}
		return &pb.GetAxisSettingResults{Results: axisResults}, nil
	} else {
		return &pb.GetSettingResults{Results: results}, nil
	}
}
