package devices

import (
	"zaber-motion-lib/internal/commands"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) unpark(request *pb.AxisEmptyRequest) errors.SdkError {
	_, err := manager.singleRequestAxis(request, "tools parking unpark")
	if err != nil {
		if commands.IsDeviceOnlyErr(err) {
			return errors.ErrNotSupported(constants.FwSixParkingAxisError + " Unpark the entire device using AllAxes.")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) park(request *pb.AxisEmptyRequest) errors.SdkError {
	_, err := manager.singleRequestAxis(request, "tools parking park")
	if err != nil {
		if commands.IsDeviceOnlyErr(err) {
			return errors.ErrNotSupported(constants.FwSixParkingAxisError + " Park the entire device using AllAxes.")
		}
		return err
	}
	return nil
}

func (manager *deviceManager) isParked(request *pb.AxisEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	reply, err := manager.singleRequestAxis(request, "get parking.state")
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return nil, errors.ErrNotSupported(constants.ParkingRejectedAxisError)
		}
		return nil, err
	}
	parked, err := reply.DataAsInt()
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: parked == 1,
	}, nil
}
