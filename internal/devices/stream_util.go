package devices

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commands"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *streamManager) registerStreamUtilCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_wait", func() pb.Message {
		return &pb.StreamWaitRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.wait(request.(*pb.StreamWaitRequest))
	})
	gateway.RegisterCallback("device/stream_wait_until_idle", func() pb.Message {
		return &pb.StreamWaitUntilIdleRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitUntilIdle(request.(*pb.StreamWaitUntilIdleRequest))
	})
	gateway.RegisterCallback("device/stream_cork", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.cork(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_uncork", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.uncork(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_is_busy", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isBusy(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_to_string", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.toString(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_call", func() pb.Message {
		return &pb.StreamCallRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.call(request.(*pb.StreamCallRequest))
	})
	gateway.RegisterCallback("device/stream_generic_command", func() pb.Message {
		return &pb.StreamGenericCommandRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.genericCommand(request.(*pb.StreamGenericCommandRequest))
	})
	gateway.RegisterCallback("device/stream_generic_command_batch", func() pb.Message {
		return &pb.StreamGenericCommandBatchRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.genericCommandBatch(request.(*pb.StreamGenericCommandBatchRequest))
	})
	gateway.RegisterCallback("device/stream_treat_discontinuities", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.treatDiscontinuitiesAsError(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_ignore_discontinuity", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.treatIgnoreDiscontinuity(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_set_hold", func() pb.Message {
		return &pb.StreamSetHoldRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setHold(request.(*pb.StreamSetHoldRequest))
	})
}

func (manager *streamManager) wait(request *pb.StreamWaitRequest) errors.SdkError {
	// we cannot use command building because FW6 controllers don't have unit conversion tables
	time := request.GetTime()
	if unit := request.GetUnit(); unit != constants.NativeUnit {
		if convertedTime, err := manager.deviceManager.units.ConvertStaticUnitToDimensionBase(time, unit); err != nil {
			return err
		} else {
			time = convertedTime * 1000
		}
	}

	command := fmt.Sprintf("wait %d", int(time))
	return manager.streamActionBasic(request, command, streamActionOptions{})
}

func (manager *streamManager) waitUntilIdle(request *pb.StreamWaitUntilIdleRequest) errors.SdkError {
	communication, err := manager.deviceManager.getInterface(request)
	if err != nil {
		return err
	}

	stream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err := stream.assureMode(StreamModeLive); err != nil {
		return err
	}

	if stream.pvt && stream.pvtData.hasUnsentActions() {
		return errors.ErrInvalidOperation(strings.Join([]string{
			"The PVT sequence contains points that were not yet sent to the device.",
			"Make sure that the last submitted point has defined velocity.",
		}, " "))
	}

	oneAxisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        stream.physicalAxesNumbers[0],
	}

	if err := WaitUntilIdle(communication, oneAxisTarget, WaitUntilIdleOptions{}); err != nil {
		return err
	}

	if request.ThrowErrorOnFault {
		if err := manager.checkAndClearStreamWarnings(communication.Requests(), request, stream, checkStreamWarningsOptions{}); err != nil {
			return err
		}
	}
	return nil
}

func (manager *streamManager) isBusy(request *pb.StreamEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	stream, err := manager.getStream(request, true)
	if err != nil {
		return nil, err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err := stream.assureMode(StreamModeLive); err != nil {
		return nil, err
	}

	axisTarget := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        stream.physicalAxesNumbers[0],
	}

	reply, err := manager.deviceManager.singleRequestAxis(axisTarget, "")
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: reply.Status != "IDLE",
	}, nil
}

func (manager *streamManager) cork(request *pb.StreamEmptyRequest) errors.SdkError {
	err := manager.streamDirectCommand(request, "fifo cork", streamDirectCommandOptions{
		modeCheck: []StreamMode{StreamModeLive},
	})
	if err != nil {
		if commands.IsBusyErr(err) {
			return errors.ErrDeviceBusy("Cannot cork busy stream. Wait for stream to become idle.")
		}
		return err
	}

	return nil
}

func (manager *streamManager) uncork(request *pb.StreamEmptyRequest) errors.SdkError {
	err := manager.streamDirectCommand(request, "fifo uncork", streamDirectCommandOptions{
		modeCheck: []StreamMode{StreamModeLive},
	})
	if err != nil {
		if commands.IsBusyErr(err) {
			return errors.ErrDeviceBusy("Cannot uncork busy stream. Wait for stream to become idle.")
		}
		return err
	}

	return err
}

func (manager *streamManager) call(request *pb.StreamCallRequest) errors.SdkError {
	command := fmt.Sprintf("call %d", getBufferID(request))
	return manager.streamActionBasic(request, command, streamActionOptions{
		modeCheck:   []StreamMode{StreamModeLive, StreamModeStore, StreamModeStoreArbitrary},
		resetBuffer: true,
	})
}

func (manager *streamManager) toString(request *pb.StreamEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	stream, err := manager.getStream(request, false)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("Stream %d (Unknown) -> Device %d -> Connection Closed", request.StreamId, request.Device),
			}, nil
		}
		return nil, err
	}

	streamString := fmt.Sprintf("%s %d", getStreamTextWordEx(request, true), request.GetStreamId())
	if stream != nil {
		stream.dataLock.Lock()
		defer stream.dataLock.Unlock()

		streamString += fmt.Sprintf(" (%s)", StreamMode(stream.mode))
		switch stream.mode {
		case StreamModeStoreArbitrary:
			streamString += fmt.Sprintf(" -> Buffer %d (Axes %d)", stream.bufferID, stream.axesCount)
		case StreamModeStore:
			streamString += fmt.Sprintf(" -> Buffer %d (Axes ", stream.bufferID)
		case StreamModeLive:
			streamString += " -> Axes "
		}
		switch stream.mode {
		case StreamModeStore, StreamModeLive:
			streamString += "["
			for i, axis := range stream.axes {
				if axis.axisType == pb.AsciiStreamAxisType_LOCKSTEP {
					streamString += "Lockstep "
				}
				streamString += fmt.Sprintf("%d", axis.streamAxisNumber)
				if i != stream.axesCount-1 {
					streamString += ", "
				}
			}
			streamString += "]"
		}
		if stream.mode == StreamModeStore {
			streamString += ")"
		}

	} else {
		streamString += " (Unknown)"
	}

	return &pb.StringResponse{
		Value: fmt.Sprintf("%s -> Device %d", streamString, request.GetDevice()),
	}, nil
}

func (manager *streamManager) genericCommand(request *pb.StreamGenericCommandRequest) errors.SdkError {
	return manager.streamActionBasic(request, request.Command, streamActionOptions{
		resetBuffer: true,
	})
}

func (manager *streamManager) genericCommandBatch(request *pb.StreamGenericCommandBatchRequest) errors.SdkError {
	return manager.streamActionsBasic(request, request.Batch, streamActionOptions{
		resetBuffer: true,
	})
}

func (manager *streamManager) getStreamAxisSetting(stream *stream, target streamTarget, setting string, checkIdle bool) ([]float64, errors.SdkError) {
	requests, err := manager.deviceManager.getRequests(target)
	if err != nil {
		return nil, err
	}

	reply, err := singleCommandDevice(requests, target, "get "+setting)
	if err != nil {
		return nil, err
	}
	replyData, err := reply.DataAsFloatArray(true)
	if err != nil {
		return nil, err
	}
	isDeviceIdle := reply.Status == "IDLE"

	if err := stream.assureMode(StreamModeLive, StreamModeStore); err != nil {
		return nil, err
	}

	values := make([]float64, len(stream.axes))
	for i, axis := range stream.axes {
		valueIndex := int(axis.primaryAxisNumber - 1)
		if valueIndex >= len(replyData) {
			return nil, errors.ErrInvalidData(fmt.Sprintf(
				"Device sent position data that does not match stream information (%s, axis %d).",
				reply.Data, axis.primaryAxisNumber))
		}
		values[i] = replyData[valueIndex]

		if checkIdle && !isDeviceIdle {
			reply, err := singleCommandAxis(requests, &axisTargetImpl{
				interfaceID: target.GetInterfaceId(),
				device:      target.GetDevice(),
				axis:        axis.primaryAxisNumber,
			}, "get "+setting)
			if err != nil {
				return nil, err
			}
			dataAgain, err := reply.DataAsFloat()
			if err != nil {
				return nil, err
			}
			if reply.Status != "IDLE" || values[i] != dataAgain {
				return nil, errors.ErrInvalidOperation(fmt.Sprintf("%s cannot be reliably determined as the device is moving.", setting))
			}
		}
	}

	return values, nil
}

func (manager *streamManager) treatDiscontinuitiesAsError(request *pb.StreamEmptyRequest) errors.SdkError {
	stream, err := manager.getStreamSync(request)
	if err != nil {
		return err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err := stream.assureMode(StreamModeLive); err != nil {
		return err
	}

	stream.runtime.discontinuitiesAsError = true
	return nil
}

func (manager *streamManager) treatIgnoreDiscontinuity(request *pb.StreamEmptyRequest) errors.SdkError {
	stream, err := manager.getStreamSync(request)
	if err != nil {
		return err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	if !stream.runtime.discontinuitiesAsError {
		return errors.ErrInvalidOperation("The stream does not treat discontinuities as errors.")
	}

	stream.runtime.ignoreCurrentDiscontinuity = true
	return nil
}

func (manager *streamManager) setHold(request *pb.StreamSetHoldRequest) errors.SdkError {
	command := "hold "
	if request.GetHold() {
		command += "on"
	} else {
		command += "off"
	}

	return manager.streamDirectCommand(request, command, streamDirectCommandOptions{
		modeCheck: []StreamMode{StreamModeLive},
	})
}
