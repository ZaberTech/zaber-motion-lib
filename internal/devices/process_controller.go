package devices

import (
	"fmt"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/units"
)

type processControllerManager struct {
	device *deviceManager
}

func newProcessControllerManager(deviceManager *deviceManager) *processControllerManager {
	return &processControllerManager{deviceManager}
}

// Checks if an identified device is a process controller, or returns true if the device is unidentified
func (manager *processControllerManager) isDeviceProcessController(request *pb.DeviceEmptyRequest) (bool, errors.SdkError) {
	deviceInfo, err := manager.device.getDeviceInfo(request)
	if err != nil {
		return false, err
	}
	if !deviceInfo.isIdentified() {
		return true, nil
	}
	if deviceInfo.identity.AxisCount < 1 {
		return false, nil
	}
	axisInfo, err := deviceInfo.GetAxisInfo(&axisTargetImpl{interfaceID: request.InterfaceId, device: request.Device, axis: 1})
	if err != nil {
		return false, err
	}
	_, exists := axisInfo.GetCommandIssuingInfo().CommandTree.GetParamsInfo([]string{"process", "on"})
	if !exists {
		return false, nil
	}
	return true, nil
}

func (manager *processControllerManager) errIfNotDeviceProcessController(request *pb.DeviceEmptyRequest) errors.SdkError {
	isIPC, err := manager.isDeviceProcessController(request)
	if err != nil {
		return err
	}
	if !isIPC {
		return errors.ErrInvalidArgument("This device is not a Process Controller")
	}
	return nil
}

// Checks if a device is a processes controller regardless of identification. Can only be called in an async context.
func (manager *processControllerManager) isUnidentifiedDeviceProcessController(requests *c.RequestManager, address int32) (bool, errors.SdkError) {
	axisTarget := &axisTargetImpl{device: address, axis: 1}
	_, err := singleCommandAxis(requests, axisTarget, "get process.control.mode")
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (manager *processControllerManager) enableProcess(request *pb.ProcessOn) errors.SdkError {
	command := "driver enable"
	if !request.On {
		command = "driver disable"
	}
	_, err := manager.device.singleRequestAxis(request, command)
	return err
}

func (manager *processControllerManager) powerProcess(request *pb.ProcessOn) errors.SdkError {
	command := "process off"

	if request.On {
		if request.Duration == 0 {
			command = "process on"
		} else {
			commandTemplate := []string{
				"process",
				"on",
				constants.CommandArgument,
			}
			args := []commandbuilding.CommandArg{commandbuilding.CommandArgImpl{Value: request.Duration, Unit: request.Unit}}
			builtCommand, err := manager.device.buildCommand(request, commandTemplate, args)
			if err != nil {
				return err
			}
			command = builtCommand
		}
	}

	_, err := manager.device.singleRequestAxis(request, command)
	if commands.IsInvalidLimitsErr(err) {
		return errors.ErrNotSupported("You cannot turn this process on. Ensure 'process.control.voltage.min' is less than 'process.control.voltage.max'.")
	}
	return err
}

func (manager *processControllerManager) setSource(request *pb.SetProcessControllerSource) errors.SdkError {
	source := int32(request.Source.Sensor) + request.Source.Port
	getSourceCommand := fmt.Sprintf("set process.control.source %d", source)
	_, err := manager.device.singleRequestAxis(request, getSourceCommand)
	return err
}

func (manager *processControllerManager) getSource(request ioc.AxisTarget) (*pb.ProductProcessControllerSource, errors.SdkError) {
	getSourceCommand := "get process.control.source"
	sourceResp, err := manager.device.singleRequestAxis(request, getSourceCommand)
	if err != nil {
		return nil, err
	}
	source, err := sourceResp.DataAsInt()
	if err != nil {
		return nil, err
	}

	sourcePort := source % 10
	sourceSensor := (pb.ProductProcessControllerSourceSensor)(source - sourcePort)
	if _, ok := pb.ProductProcessControllerSourceSensor_name[sourceSensor]; ok {
		return &pb.ProductProcessControllerSource{
			Sensor: sourceSensor,
			Port:   int32(sourcePort),
		}, nil
	} else {
		return nil, errors.ErrNotSupported(fmt.Sprintf("Unexpected process control source %d. You may need a newer version of ZML to support this product.", source))
	}
}

func (manager *processControllerManager) getInput(request *pb.AxisEmptyRequest) (*pb.Measurement, errors.SdkError) {
	source, err := manager.getSource(request)
	if err != nil {
		return nil, err
	}

	if source.Sensor == 10 { // thermistor
		command := fmt.Sprintf("get sensor.temperature.%d", source.Port)
		resp, err := manager.device.singleRequestDevice(request, command)
		if err != nil {
			return nil, err
		}
		temperature, err := resp.DataAsFloat()
		if err != nil {
			return nil, err
		}
		return &pb.Measurement{Value: temperature, Unit: units.Units_Celsius}, nil
	} else { // analog input
		command := fmt.Sprintf("io get ai %d", source.Port)
		resp, err := manager.device.singleRequestDevice(request, command)
		if err != nil {
			return nil, err
		}
		analogIn, err := resp.DataAsFloat()
		if err != nil {
			return nil, err
		}
		return &pb.Measurement{Value: analogIn, Unit: units.Units_Volts}, nil
	}
}

func (manager *processControllerManager) bridge(request *pb.ProcessOn) errors.SdkError {
	if request.Axis%2 == 0 {
		return errors.ErrInvalidArgument(fmt.Sprintf("Cannot create a bridge from process %d. Bridges must be created from odd-numbered processes", request.Axis))
	}
	mode := 0
	if request.On {
		mode = 1
	}
	_, err := manager.device.singleRequestAxis(request, fmt.Sprintf("set driver.bipolar %d", mode))
	if err != nil {
		return err
	}
	return nil
}

func (manager *processControllerManager) isBridge(request *pb.AxisEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	axisTarget := &axisTargetImpl{
		interfaceID: request.InterfaceId,
		device:      request.Device,
		axis:        request.Axis,
	}
	if request.Axis%2 == 0 {
		axisTarget.axis = axisTarget.axis - 1
	}
	resp, err := manager.device.singleRequestAxis(axisTarget, "get driver.bipolar")
	if err != nil {
		return nil, err
	}
	mode, err := resp.DataAsInt()
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: mode == 1,
	}, nil
}
