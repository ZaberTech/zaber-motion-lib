package devices

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

type streamInfo struct {
	mode            string
	maxTanAccel     string
	maxCentripAccel string
	maxSpeed        string
	streamError     string
}

type invalidPoint struct {
	data  string
	index int
}

type pvtInfo struct {
	mode              string
	numberOfAxes      int
	invalidPointIndex int
	streamError       string
	invalidPoints     []invalidPoint
}

func (manager *streamManager) registerStreamSettingsCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_get_axes", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getAxes(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_get_mode", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getModeRequest(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_check_disabled", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.checkDisabledRequest(request.(*pb.StreamEmptyRequest))
	})
	gateway.RegisterCallback("device/stream_get_max_speed", func() pb.Message {
		return &pb.StreamGetMaxSpeedRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getMaxSpeed(request.(*pb.StreamGetMaxSpeedRequest))
	})
	gateway.RegisterCallback("device/stream_get_max_tangential_acceleration", func() pb.Message {
		return &pb.StreamGetMaxTangentialAccelerationRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getMaxTangentialAcceleration(request.(*pb.StreamGetMaxTangentialAccelerationRequest))
	})
	gateway.RegisterCallback("device/stream_get_max_centripetal_acceleration", func() pb.Message {
		return &pb.StreamGetMaxCentripetalAccelerationRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getMaxCentripetalAcceleration(request.(*pb.StreamGetMaxCentripetalAccelerationRequest))
	})
	gateway.RegisterCallback("device/stream_set_max_speed", func() pb.Message {
		return &pb.StreamSetMaxSpeedRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setMaxSpeed(request.(*pb.StreamSetMaxSpeedRequest))
	})
	gateway.RegisterCallback("device/stream_set_max_tangential_acceleration", func() pb.Message {
		return &pb.StreamSetMaxTangentialAccelerationRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setMaxTangentialAcceleration(request.(*pb.StreamSetMaxTangentialAccelerationRequest))
	})
	gateway.RegisterCallback("device/stream_set_max_centripetal_acceleration", func() pb.Message {
		return &pb.StreamSetMaxCentripetalAccelerationRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setMaxCentripetalAcceleration(request.(*pb.StreamSetMaxCentripetalAccelerationRequest))
	})
}

func (manager *streamManager) requestStreamInfo(request streamTarget) (*streamInfo, errors.SdkError) {
	if request.GetPvt() {
		panic("requestStreamInfo not available for PVT")
	}
	reply, err := manager.streamRequest(request, "info")
	if err != nil {
		return nil, err
	}

	infoParts := strings.Split(reply.Data, " ")
	if len(infoParts) < 6 {
		return nil, errors.ErrInvalidData("Cannot parse stream info: " + reply.Data)
	}

	return &streamInfo{
		mode:            infoParts[0],
		maxCentripAccel: infoParts[2],
		maxTanAccel:     infoParts[3],
		maxSpeed:        infoParts[4],
		streamError:     infoParts[5],
	}, nil
}

var pvtErrorPointRegexp = regexp.MustCompile(`i\s(\d+)`)

func (manager *streamManager) requestPvtInfo(request streamTarget) (*pvtInfo, errors.SdkError) {
	if !request.GetPvt() {
		panic("requestPvtInfo not available for stream")
	}
	replies, err := manager.streamRequestMulti(request, "info")
	if err != nil {
		return nil, err
	}

	makeError := func() errors.SdkError {
		return errors.ErrInvalidData("Cannot parse PVT info: " + replies[0].Data)
	}

	infoParts := strings.Split(replies[0].Data, " ")
	if len(infoParts) < 4 {
		return nil, makeError()
	}

	info := &pvtInfo{
		mode:              infoParts[0],
		numberOfAxes:      -1,
		invalidPointIndex: -1,
		streamError:       infoParts[3],
	}

	if info.mode != "disabled" {
		if number, parseErr := strconv.ParseInt(infoParts[1], 10, 32); parseErr != nil {
			return nil, makeError()
		} else {
			info.numberOfAxes = int(number)
		}

		if infoParts[2] != "-" {
			if number, parseErr := strconv.ParseInt(infoParts[2], 10, 32); parseErr != nil {
				return nil, makeError()
			} else {
				info.invalidPointIndex = int(number)
			}
		}
	}

	if len(replies) > 1 {
		for _, point := range replies[1:] {
			if match := pvtErrorPointRegexp.FindStringSubmatch(point.Data); match == nil {
				return nil, errors.ErrInvalidData("Cannot parse PVT info point: " + point.Data)
			} else {
				index, _ := strconv.ParseInt(match[1], 10, 32)
				info.invalidPoints = append(info.invalidPoints, invalidPoint{
					data:  point.Data,
					index: int(index),
				})
			}
		}
	}

	return info, nil
}

func (manager *streamManager) getAxes(request *pb.StreamEmptyRequest) (*pb.StreamGetAxesResponse, errors.SdkError) {
	stream, err := manager.getStream(request, true)
	if err != nil {
		return nil, err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	err = stream.assureMode(StreamModeLive, StreamModeStore)
	if err != nil {
		return nil, err
	}

	response := &pb.StreamGetAxesResponse{}
	for _, axis := range stream.axes {

		definition := &pb.AsciiStreamAxisDefinition{
			AxisNumber: axis.streamAxisNumber,
			AxisType:   utils.ToPointer(axis.axisType),
		}
		if stream.pvt {
			response.PvtAxes = append(response.PvtAxes, streamAxisDefinitionToPvt(definition))
		} else {
			response.Axes = append(response.Axes, definition)
		}
	}
	return response, nil
}

func (manager *streamManager) getMode(request streamTarget) (StreamMode, errors.SdkError) {
	stream, err := manager.getStream(request, false)
	if err != nil {
		return StreamModeDisabled, err
	}

	var mode StreamMode
	if stream != nil {
		stream.dataLock.Lock()
		mode = stream.mode
		stream.dataLock.Unlock()
	}

	return mode, nil
}

func (manager *streamManager) getModeRequest(request *pb.StreamEmptyRequest) (*pb.StreamModeResponse, errors.SdkError) {
	if mode, err := manager.getMode(request); err != nil {
		return nil, err
	} else {
		return &pb.StreamModeResponse{
			StreamMode: mode.DtoMode(),
			PvtMode:    mode.DtoPvtMode(),
		}, nil
	}
}

func (manager *streamManager) checkDisabledRequest(request *pb.StreamEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	stream, err := manager.getStream(request, true)
	if err != nil {
		return nil, err
	}

	stream.lock.Lock()
	defer stream.lock.Unlock()

	isDisabled, err := manager.checkIfStreamDisabled(request, stream)
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: isDisabled,
	}, nil
}

func (manager *streamManager) convertValueUsingFirstAxisInStream(request streamTarget, stream *stream, nativeValue float64, settingNameForUnitConversion string, unit string) (convertedValue float64, err errors.SdkError) {
	conversionAxis, err := stream.getUnitConversionAxis(0)
	if err != nil {
		return 0, err
	}

	axisForConversion := &axisTargetImpl{
		interfaceID: request.GetInterfaceId(),
		device:      request.GetDevice(),
		axis:        conversionAxis,
	}

	convertedValue, err = manager.deviceManager.ConvertUnitSetting(
		axisForConversion, settingNameForUnitConversion, nativeValue, unit, true,
	)
	if err != nil {
		return 0, err
	}

	return convertedValue, nil
}

type pickStreamSetting func(*streamInfo) string

func (manager *streamManager) getStreamSetting(request streamTarget, pick pickStreamSetting, conversionSetting string, unit string) (float64, errors.SdkError) {
	if request.GetPvt() {
		panic("getStreamSetting not supported for PVT")
	}

	stream, err := manager.getStream(request, true)
	if err != nil {
		return 0, err
	}
	stream.lock.Lock()
	defer stream.lock.Unlock()

	if err := stream.assureMode(StreamModeLive); err != nil {
		return 0, err
	}

	streamInfo, err := manager.requestStreamInfo(request)
	if err != nil {
		return 0, err
	}

	settingStr := pick(streamInfo)
	settingNativeValue, convErr := strconv.ParseFloat(settingStr, 64)
	if convErr != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf("Could not parse stream info values: %+v.", streamInfo))
	}

	convertedValue, err := manager.convertValueUsingFirstAxisInStream(request, stream, settingNativeValue, conversionSetting, unit)
	if err != nil {
		return 0, err
	}

	return convertedValue, nil
}

func (manager *streamManager) getMaxSpeed(request *pb.StreamGetMaxSpeedRequest) (*pb.DoubleResponse, errors.SdkError) {
	value, err := manager.getStreamSetting(request, func(info *streamInfo) string { return info.maxSpeed }, "maxspeed", request.GetUnit())
	if err != nil {
		return nil, err
	}
	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (manager *streamManager) getMaxTangentialAcceleration(request *pb.StreamGetMaxTangentialAccelerationRequest) (*pb.DoubleResponse, errors.SdkError) {
	value, err := manager.getStreamSetting(request, func(info *streamInfo) string { return info.maxTanAccel }, "accel", request.GetUnit())
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (manager *streamManager) getMaxCentripetalAcceleration(request *pb.StreamGetMaxCentripetalAccelerationRequest) (*pb.DoubleResponse, errors.SdkError) {
	value, err := manager.getStreamSetting(request, func(info *streamInfo) string { return info.maxCentripAccel }, "accel", request.GetUnit())
	if err != nil {
		return nil, err
	}
	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (manager *streamManager) setMaxSpeed(request *pb.StreamSetMaxSpeedRequest) errors.SdkError {
	commandTemplate := []string{"set", "maxspeed", constants.CommandArgument}
	arguments := []streamArg{streamArgImpl{
		CommandArgImpl: commandbuilding.CommandArgImpl{
			Value: request.GetMaxSpeed(),
			Unit:  request.GetUnit(),
		},
		axisIndex:   0,
		settingName: "maxspeed",
	}}

	return manager.streamAction(request, commandTemplate, arguments, streamActionOptions{})
}

func (manager *streamManager) setMaxTangentialAcceleration(request *pb.StreamSetMaxTangentialAccelerationRequest) errors.SdkError {
	commandTemplate := []string{"set", "tanaccel", constants.CommandArgument}
	value := request.GetMaxTangentialAcceleration()
	if math.IsInf(value, 1) {
		value = 0
	}
	arguments := []streamArg{streamArgImpl{
		CommandArgImpl: commandbuilding.CommandArgImpl{
			Value: value,
			Unit:  request.GetUnit(),
		},
		axisIndex:   0,
		settingName: "accel",
	}}

	return manager.streamAction(request, commandTemplate, arguments, streamActionOptions{})
}

func (manager *streamManager) setMaxCentripetalAcceleration(request *pb.StreamSetMaxCentripetalAccelerationRequest) errors.SdkError {
	commandTemplate := []string{"set", "centripaccel", constants.CommandArgument}
	value := request.GetMaxCentripetalAcceleration()
	if math.IsInf(value, 1) {
		value = 0
	}
	arguments := []streamArg{streamArgImpl{
		CommandArgImpl: commandbuilding.CommandArgImpl{
			Value: value,
			Unit:  request.GetUnit(),
		},
		axisIndex:   0,
		settingName: "accel",
	}}

	return manager.streamAction(request, commandTemplate, arguments, streamActionOptions{})
}
