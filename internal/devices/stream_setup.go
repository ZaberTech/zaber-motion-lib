package devices

import (
	"fmt"
	"strconv"
	"zaber-motion-lib/internal/commands"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"strings"

	"github.com/elliotchance/pie/v2"
)

// This constant artificially limits number of PVT buffers to 7.33 because we cannot iterate over them.
const pvt733BufferLimit = 100

func (manager *streamManager) registerStreamSetupCallbacks(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("device/stream_setup_live", func() pb.Message {
		return &pb.StreamSetupLiveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setupLive(request.(*pb.StreamSetupLiveRequest))
	})
	gateway.RegisterCallback("device/stream_setup_store", func() pb.Message {
		return &pb.StreamSetupStoreRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setupStore(request.(*pb.StreamSetupStoreRequest))
	})
	gateway.RegisterCallback("device/stream_setup_live_composite", func() pb.Message {
		return &pb.StreamSetupLiveCompositeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setupLiveComposite(request.(*pb.StreamSetupLiveCompositeRequest))
	})
	gateway.RegisterCallback("device/stream_setup_store_composite", func() pb.Message {
		return &pb.StreamSetupStoreCompositeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setupStoreComposite(request.(*pb.StreamSetupStoreCompositeRequest))
	})
	gateway.RegisterCallback("device/stream_setup_store_arbitrary_axes", func() pb.Message {
		return &pb.StreamSetupStoreArbitraryAxesRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setupStoreArbitraryAxes(request.(*pb.StreamSetupStoreArbitraryAxesRequest))
	})
	gateway.RegisterCallback("device/stream_disable", func() pb.Message {
		return &pb.StreamEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.disable(request.(*pb.StreamEmptyRequest))
	})
}

func (manager *streamManager) determinePhysicalAxes(stream *streamData, device streamTarget) errors.SdkError {
	for _, axis := range stream.axes {
		switch axis.axisType {
		case pb.AsciiStreamAxisType_PHYSICAL:
			axis.primaryAxisNumber = axis.streamAxisNumber
			stream.physicalAxesNumbers = append(stream.physicalAxesNumbers, axis.streamAxisNumber)
		case pb.AsciiStreamAxisType_LOCKSTEP:
			lockstepTarget := lockstepTargetImpl{
				interfaceID:     device.GetInterfaceId(),
				device:          device.GetDevice(),
				lockstepGroupID: axis.streamAxisNumber,
			}
			lockstep, err := manager.deviceManager.getCachedOrQueryLockstepInfo(&lockstepTarget, false)
			if err != nil {
				return err
			} else if lockstep == nil {
				message := fmt.Sprintf("Lockstep %d is disabled.", lockstepTarget.lockstepGroupID)
				if device.GetPvt() {
					return errors.ErrPvtSetupFailed(message)
				} else {
					return errors.ErrStreamSetupFailed(message)
				}
			}

			axis.primaryAxisNumber = lockstep.axes[0]
			stream.physicalAxesNumbers = append(stream.physicalAxesNumbers, lockstep.axes...)
		}
	}
	return nil
}

type streamSetupRequest interface {
	GetAxes() []*pb.AsciiStreamAxisDefinition
	GetPvtAxes() []*pb.AsciiPvtAxisDefinition
	GetPvt() bool
}

func processStreamAxes(request streamSetupRequest, device *deviceInfo) ([]*streamAxis, errors.SdkError) {
	axisDefinitions := request.GetAxes()[:]
	axisDefinitions = append(axisDefinitions, pie.Map(request.GetPvtAxes(), pvtAxisDefinitionToStream)...)
	if len(axisDefinitions) == 0 {
		return nil, errors.ErrInvalidArgument("No axes provided.")
	}

	axes := make([]*streamAxis, 0, len(axisDefinitions))
	for i, axisDefinition := range axisDefinitions {
		axis := &streamAxis{
			streamAxisNumber: axisDefinition.AxisNumber,
			axisType:         utils.EnumDefault(axisDefinition.AxisType, pb.AsciiStreamAxisType_PHYSICAL),
		}

		if device.isIdentified() {
			if axis.axisType == pb.AsciiStreamAxisType_PHYSICAL &&
				(axis.streamAxisNumber <= 0 || len(device.axes) < int(axis.streamAxisNumber)) {
				return nil, errors.ErrInvalidArgument(fmt.Sprintf("Stream axis out of range: %d", axis.streamAxisNumber))
			}
		}

		for _, existing := range axes {
			if existing.axisType == axis.axisType && existing.streamAxisNumber == axis.streamAxisNumber {
				return nil, errors.ErrInvalidArgument(fmt.Sprintf("Axis with index %d is already in the stream.", i))
			}
		}

		axes = append(axes, axis)
	}
	return axes, nil
}

func (manager *streamManager) setupLiveComposite(request *pb.StreamSetupLiveCompositeRequest) errors.SdkError {
	device, err := manager.deviceManager.getDeviceInfo(request)
	if err != nil {
		return err
	}

	axes, err := processStreamAxes(request, device)
	if err != nil {
		return err
	}

	targetStream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}

	targetStream.lock.Lock()
	defer targetStream.lock.Unlock()

	if err := targetStream.assureMode(StreamModeDisabled); err != nil {
		return err
	}

	commandParts := []string{"setup", "live"}
	for _, axis := range axes {
		if axis.axisType == pb.AsciiStreamAxisType_LOCKSTEP {
			commandParts = append(commandParts, "lockstep")
		}
		commandParts = append(commandParts, strconv.Itoa(int(axis.streamAxisNumber)))
	}
	setupLiveCommand := strings.Join(commandParts, " ")

	streamData := &streamData{
		mode:      StreamModeLive,
		axes:      axes,
		axesCount: len(axes),
	}
	if err := manager.determinePhysicalAxes(streamData, request); err != nil {
		return err
	}

	var pvtData *pvtStream
	if targetStream.pvt {
		if pvtData, err = manager.initPvtStream(device, streamData, request); err != nil {
			return err
		}
	}

	_, err = manager.streamRequest(request, setupLiveCommand)
	if err != nil {
		if commands.IsBadDataErr(err) {
			message := strings.Join([]string{
				fmt.Sprintf("%s setup failed.", getStreamTextWordEx(request, true)),
				fmt.Sprintf("Ensure that the axes and the %s number are correct, that the axes are homed,", getStreamTextWord(request)),
				"and that none of the axes are part of another live stream or PVT sequence.",
			}, " ")
			if request.GetPvt() {
				return errors.ErrPvtSetupFailed(message)
			} else {
				return errors.ErrStreamSetupFailed(message)
			}
		}
		return err
	}

	targetStream.setupWithData(streamData)
	if targetStream.pvt {
		targetStream.pvtData = pvtData

		if pvtData.identified {
			if err := manager.populatePvtActionBufferFromPosition(targetStream, request); err != nil {
				return err
			}
		}
	}
	return nil
}

func (manager *streamManager) setupStoreComposite(request *pb.StreamSetupStoreCompositeRequest) errors.SdkError {
	device, err := manager.deviceManager.getDeviceInfo(request)
	if err != nil {
		return err
	}

	axes, err := processStreamAxes(request, device)
	if err != nil {
		return err
	}

	targetStream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	targetStream.lock.Lock()
	defer targetStream.lock.Unlock()

	if err := targetStream.assureMode(StreamModeDisabled); err != nil {
		return err
	}

	bufferID := getBufferID(request)
	axesCount := len(axes)
	setupStoreCommand := fmt.Sprintf("setup store %d %d", bufferID, axesCount)

	if request.Pvt && device.isIdentified() {
		fw := device.identity.FirmwareVersion
		if fw.Major == 7 && fw.Minor == 33 && bufferID > pvt733BufferLimit {
			return errors.ErrInvalidArgument(fmt.Sprintf("For firmware version 7.33 the number of buffers is limited to %d.", pvt733BufferLimit))
		}
	}

	streamData := &streamData{
		mode:      StreamModeStore,
		axes:      axes,
		axesCount: axesCount,
		bufferID:  bufferID,
	}
	if err := manager.determinePhysicalAxes(streamData, request); err != nil {
		return err
	}

	var pvtData *pvtStream
	if targetStream.pvt {
		if pvtData, err = manager.initPvtStream(device, streamData, request); err != nil {
			return err
		}
	}

	if _, err := manager.streamRequest(request, setupStoreCommand); err != nil {
		if commands.IsBadDataErr(err) {
			message := fmt.Sprintf("%s Make sure that the %s buffer exists on the device and is empty.", err.Message(), getStreamTextWord(request))
			if request.GetPvt() {
				return errors.ErrPvtSetupFailed(message)
			} else {
				return errors.ErrStreamSetupFailed(message)
			}
		}

		return err
	}

	targetStream.setupWithData(streamData)
	if targetStream.pvt {
		targetStream.pvtData = pvtData
	}
	return nil
}

func convertPhysicalNumbersToAxisDefinition(axesIndices []int32) []*pb.AsciiStreamAxisDefinition {
	axes := make([]*pb.AsciiStreamAxisDefinition, len(axesIndices))
	for i, axis := range axesIndices {
		axes[i] = &pb.AsciiStreamAxisDefinition{
			AxisNumber: axis,
			AxisType:   utils.ToPointer(pb.AsciiStreamAxisType_PHYSICAL),
		}
	}
	return axes
}

func (manager *streamManager) setupLive(request *pb.StreamSetupLiveRequest) errors.SdkError {
	requestProxy := &pb.StreamSetupLiveCompositeRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		StreamId:    request.StreamId,
		Axes:        convertPhysicalNumbersToAxisDefinition(request.Axes),
		Pvt:         request.Pvt,
	}
	return manager.setupLiveComposite(requestProxy)
}

func (manager *streamManager) setupStore(request *pb.StreamSetupStoreRequest) errors.SdkError {
	requestProxy := &pb.StreamSetupStoreCompositeRequest{
		InterfaceId:  request.InterfaceId,
		Device:       request.Device,
		StreamId:     request.StreamId,
		StreamBuffer: request.StreamBuffer,
		PvtBuffer:    request.PvtBuffer,
		Axes:         convertPhysicalNumbersToAxisDefinition(request.Axes),
		Pvt:          request.Pvt,
	}
	return manager.setupStoreComposite(requestProxy)
}

func (manager *streamManager) setupStoreArbitraryAxes(request *pb.StreamSetupStoreArbitraryAxesRequest) errors.SdkError {
	if request.Pvt {
		panic("setupStoreArbitraryAxes not supported for PVT")
	}
	targetStream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	targetStream.lock.Lock()
	defer targetStream.lock.Unlock()

	if err := targetStream.assureMode(StreamModeDisabled); err != nil {
		return err
	}

	bufferID := getBufferID(request)
	axesCount := int(request.GetAxesCount())

	setupStoreCommand := fmt.Sprintf("setup store %d %d", bufferID, axesCount)

	if _, err := manager.streamRequest(request, setupStoreCommand); err != nil {
		if commands.IsBadDataErr(err) {
			return errors.ErrInvalidData(fmt.Sprintf("%s %s", err.Message(), "Make sure that the stream buffer exists on the device and is empty."))
		}

		return err
	}

	targetStream.setupWithData(&streamData{
		mode:      StreamModeStoreArbitrary,
		axesCount: axesCount,
		bufferID:  bufferID,
	})

	return nil
}

func (manager *streamManager) disable(request *pb.StreamEmptyRequest) errors.SdkError {
	targetStream, err := manager.getStream(request, true)
	if err != nil {
		return err
	}
	targetStream.lock.Lock()
	defer targetStream.lock.Unlock()

	_, err = manager.streamRequest(request, "setup disable")
	if err != nil {
		return err
	}

	targetStream.setModeDisabled()
	return nil
}

func (targetStream *stream) setModeDisabled() {
	targetStream.setupWithData(&streamData{})
}

func (targetStream *stream) setupWithData(data *streamData) {
	// stream.lock must already be locked by the caller.
	targetStream.dataLock.Lock()
	defer targetStream.dataLock.Unlock()

	targetStream.mode = data.mode
	targetStream.axes = data.axes
	targetStream.axesCount = data.axesCount
	targetStream.physicalAxesNumbers = data.physicalAxesNumbers
	targetStream.bufferID = data.bufferID

	targetStream.runtime = streamRuntimeData{}
	if targetStream.pvt {
		targetStream.pvtData = nil
	}
}
