package devicedb

import (
	"zaber-motion-lib/internal/ioc"

	pb "zaber-motion-lib/internal/dto"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (db *deviceDb) GetBinaryCommandTable(table *dto.BinaryCommandsTable) ioc.BinaryCommandTable {
	return &commandTable{
		table: table,
	}
}

type commandTable struct {
	table *dto.BinaryCommandsTable
}

func (commands *commandTable) GetContextualDimensions(commandCode pb.BinaryCommandCode) (*ioc.BinaryCommandDimensions, bool) {
	commandRows := commands.table.Rows
	for _, row := range commandRows {
		if row.Command == (int)(commandCode) {
			return &ioc.BinaryCommandDimensions{
				ParamContextualDimension:  (ioc.ContextualDimensionID)(row.ParamContextualDimensionID),
				ReturnContextualDimension: (ioc.ContextualDimensionID)(row.ReturnContextualDimensionID),
			}, true
		}
	}
	return nil, false
}
