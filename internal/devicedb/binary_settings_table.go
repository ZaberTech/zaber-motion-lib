package devicedb

import (
	g "zaber-motion-lib/internal/generated"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (db *deviceDb) GetBinarySettingsTable(table *dto.BinarySettingsTable) ioc.BinarySettingsTable {
	return &binarySettingsTable{
		table: table,
	}
}

type binarySettingsTable struct {
	table *dto.BinarySettingsTable
}

func (table *binarySettingsTable) GetContextualDimension(setting *g.BinarySetting) (ioc.ContextualDimensionID, bool) {
	for _, row := range table.table.Rows {
		if (int32)(row.GetCommand) == setting.GetCommand && (int32)(row.SetCommand) == setting.SetCommand {
			return (ioc.ContextualDimensionID)(row.ContextualDimensionID), true
		}
	}
	return 0, false
}
