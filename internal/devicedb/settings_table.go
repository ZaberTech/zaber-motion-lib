package devicedb

import (
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (db *deviceDb) GetSettingsTable(table *dto.SettingsTable) ioc.SettingsTable {
	return &settingsTable{
		table: table,
	}
}

type settingsTable struct {
	table *dto.SettingsTable
}

func (table *settingsTable) findSetting(setting string) (*dto.SettingsTableRow, bool) {
	for _, row := range table.table.Rows {
		if row.Name == setting {
			return row, true
		}
	}

	return nil, false
}

func (table *settingsTable) GetParamInfo(setting string) (*ioc.ParamInfo, bool) {
	row, found := table.findSetting(setting)
	if !found {
		return nil, false
	}
	return &ioc.ParamInfo{
		ContextualDimensionID: ioc.ContextualDimensionID(row.ContextualDimensionID),
		ParamType:             ioc.ParamType(row.ParamType),
	}, true
}

func (table *settingsTable) GetDefaultValue(setting string) (*string, bool) {
	row, found := table.findSetting(setting)
	if !found {
		return nil, false
	}
	return row.DefaultValue, true
}
