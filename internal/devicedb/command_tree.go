package devicedb

import (
	"strconv"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (db *deviceDb) GetCommandTree(tree *dto.CommandNode) ioc.CommandTree {
	return &commandTree{
		root: tree,
	}
}

type commandTree struct {
	root *dto.CommandNode
}

func contains(array []string, toSearch string) bool {
	for _, item := range array {
		if item == toSearch {
			return true
		}
	}
	return false
}

func isNumericParamNode(node *dto.CommandNode) bool {
	return node.IsParam && node.ParamType != "Token" && node.ParamType != "Enum"
}

func getNextNodeInCommand(currentNode *dto.CommandNode, nextCmdPart string) *dto.CommandNode {
	selectionPool := currentNode.Nodes
	if currentNode.IsParam && currentNode.ParamArity == constants.InfiniteArity {
		selectionPool = append(selectionPool, currentNode)
	}

	if nextCmdPart == constants.CommandArgument {
		for _, node := range selectionPool {
			if isNumericParamNode(node) {
				return node
			}
		}

		for _, node := range selectionPool {
			if node.ParamType == "Token" {
				return node
			}
		}
	} else {
		for _, node := range selectionPool {
			if node.Command == nextCmdPart {
				return node
			}
		}

		for _, node := range selectionPool {
			if isNumericParamNode(node) {
				if _, err := strconv.ParseFloat(nextCmdPart, 64); err == nil {
					return node
				}
			}
		}

		for _, node := range selectionPool {
			if node.ParamType == "Enum" && contains(node.Values, nextCmdPart) {
				return node
			}
		}

		for _, node := range selectionPool {
			if node.ParamType == "Token" {
				return node
			}
		}
	}

	return nil
}

func (tree *commandTree) getNodesForCommand(command []string) []*dto.CommandNode {
	var nodesSoFar []*dto.CommandNode

	currentNode := tree.root
	restOfCmd := command
	for len(restOfCmd) > 0 {
		nextCmdPart := restOfCmd[0]
		restOfCmd = restOfCmd[1:]

		nextNode := getNextNodeInCommand(currentNode, nextCmdPart)
		if nextNode == nil {
			return nil
		}

		nodesSoFar = append(nodesSoFar, nextNode)
		currentNode = nextNode
	}

	if !currentNode.IsCommand {
		return nil
	}

	return nodesSoFar
}

func (tree *commandTree) GetParamsInfo(command []string) ([]*ioc.ParamInfo, bool) {
	nodes := tree.getNodesForCommand(command)
	if nodes == nil {
		return nil, false
	}

	var infos []*ioc.ParamInfo
	for i, node := range nodes {
		if command[i] == constants.CommandArgument && node.IsParam {
			paramInfo := &ioc.ParamInfo{
				ContextualDimensionID: (ioc.ContextualDimensionID)(node.ContextualDimensionID),
				ParamType:             (ioc.ParamType)(node.ParamType),
			}

			infos = append(infos, paramInfo)
		}
	}

	return infos, true
}
