package devicedb

import (
	"fmt"
	"log"
	"path"
	"sync"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/client"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
	dbErrors "gitlab.com/ZaberTech/zaber-device-db-service/pkg/errors"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/service"
	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/store"

	pb "zaber-motion-lib/internal/dto"
)

const defaultServiceURL = "https://api.zaber.io/device-db/public"
const defaultStoreDirName = "device-db-store"
const devFwMinorVersion = 99

type deviceDb struct {
	service      api.BaseServiceBasic
	serviceStore store.ServiceStore
	sourceType   pb.DeviceDbSourceType
	lock         sync.RWMutex
	gateway      ioc.GatewayManager
	library      ioc.Library
}

func NewDeviceDb(gateway ioc.GatewayManager, library ioc.Library) *deviceDb {
	db := &deviceDb{
		gateway: gateway,
		library: library,
	}

	db.register()

	if err := db.setSource(pb.DeviceDbSourceType_WEB_SERVICE, ""); err != nil {
		panic(err)
	}

	_ = db.toggleStore(true, "")
	// In the unlikely case of an error the store stays disabled.

	return db
}

func (db *deviceDb) register() {
	gateway := db.gateway

	gateway.RegisterCallback("device_db/set_source", func() pb.Message {
		return &pb.SetDeviceDbSourceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, db.setSourceRequest(request.(*pb.SetDeviceDbSourceRequest))
	})
	gateway.RegisterCallback("device_db/toggle_store", func() pb.Message {
		return &pb.ToggleDeviceDbStoreRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, db.toggleStoreRequest(request.(*pb.ToggleDeviceDbStoreRequest))
	})
}

func (db *deviceDb) toggleStoreRequest(request *pb.ToggleDeviceDbStoreRequest) errors.SdkError {
	return db.toggleStore(request.ToggleOn, utils.PtrDefault(request.StoreLocation, ""))
}

func (db *deviceDb) toggleStore(on bool, location string) errors.SdkError {
	db.lock.Lock()
	defer db.lock.Unlock()

	if on {
		if location == "" {
			err, dataDir := utils.GetLibraryDataDir()
			if err != nil {
				return err
			}
			location = path.Join(dataDir, defaultStoreDirName)
		}
		db.serviceStore = store.NewServiceStore(db.service, location)
	} else {
		db.serviceStore = nil
	}

	return nil
}

func (db *deviceDb) setSourceRequest(request *pb.SetDeviceDbSourceRequest) errors.SdkError {
	return db.setSource((pb.DeviceDbSourceType)(request.SourceType), utils.PtrDefault(request.UrlOrFilePath, ""))
}

func (db *deviceDb) setSource(sourceType pb.DeviceDbSourceType, urlOrFilePath string) errors.SdkError {
	var newService api.BaseServiceBasic

	switch sourceType {
	case pb.DeviceDbSourceType_WEB_SERVICE:
		url := urlOrFilePath
		if url == "" {
			url = defaultServiceURL
		}
		userAgent := fmt.Sprintf("ZaberMotionLibrary v%s", db.library.GetVersionString())
		if service, err := client.NewClient(url, userAgent); err != nil {
			return errors.ErrDeviceDbFailed(err.Error(), "no-access")
		} else {
			newService = service
		}

	case pb.DeviceDbSourceType_FILE:
		newService = service.NewService(urlOrFilePath)
	}

	db.lock.Lock()
	oldService := db.service
	db.service = newService
	db.sourceType = sourceType
	if db.serviceStore != nil {
		db.serviceStore.ExchangeUnderlayingService(newService)
	}
	db.lock.Unlock()

	if instance, isService := oldService.(service.FileService); isService {
		go func() {
			if err := instance.Close(); err != nil {
				log.Print("Error closing Device DB: ", err.Error())
			}
		}()
	}

	return nil
}

func (db *deviceDb) getService() api.BaseServiceBasic {
	if db.serviceStore != nil {
		return db.serviceStore
	}
	return db.service
}

func (db *deviceDb) GetProductInformation(query *api.GetProductInformationQuery) (*dto.DeviceInfo, errors.SdkError) {
	db.lock.RLock()
	defer db.lock.RUnlock()

	info, err := db.getService().GetProductInformation(query)
	if err != nil {
		return nil, db.handleError(err, query.FW)
	}
	return info, nil
}

func (db *deviceDb) GetBinaryProductInformation(deviceID string, fw *dto.FirmwareVersion, peripheralID string) (*dto.BinaryDeviceInfo, errors.SdkError) {
	db.lock.RLock()
	defer db.lock.RUnlock()

	info, err := db.getService().GetBinaryProductInformation(deviceID, fw, peripheralID)
	if err != nil {
		return nil, db.handleError(err, fw)
	}
	return info, nil
}

func (db *deviceDb) GetServoTuningInfo(productID int, fw *dto.FirmwareVersion) (*dto.ServoTuningInfo, errors.SdkError) {
	db.lock.RLock()
	defer db.lock.RUnlock()

	info, err := db.getService().GetServoTuningInfo(productID)
	if err != nil {
		return nil, db.handleError(err, fw)
	}
	return info, nil
}

func (db *deviceDb) handleError(err dbErrors.Error, fw *dto.FirmwareVersion) errors.SdkError {
	switch err.ErrorType() {
	case dbErrors.ErrorGeneric:
		switch db.sourceType {
		case pb.DeviceDbSourceType_WEB_SERVICE:
			return errors.ErrDeviceDbFailed(fmt.Sprintf("Cannot access online source for the Device Database. Restore your internet connection or setup an offline source for the Device Database. Error: %s", err.Error()), "no-access")
		case pb.DeviceDbSourceType_FILE:
			return errors.ErrDeviceDbFailed(fmt.Sprintf("Cannot access offline source for the Device Database: %s", err.Error()), "no-access")
		}
	case dbErrors.ErrorIncompatibleDatabase:
		return errors.ErrDeviceDbFailed(fmt.Sprintf("The library and the database are not compatible. Make sure to update both library and database to the latest version. Error: %s", err.Error()), "incompatible")
	case dbErrors.ErrorNoRecordFound:
		if fw != nil && fw.Minor == devFwMinorVersion {
			return errors.ErrDeviceDbFailed(fmt.Sprintf("You are using a development device. You need a master device database source. Go to: https://iwiki.izaber.com/FWEE/ZML. Error: %s", err.Error()), "not-found")
		}
		return errors.ErrDeviceDbFailed(err.Error(), "not-found")
	}
	return errors.ErrDeviceDbFailed(err.Error(), "unknown")
}
