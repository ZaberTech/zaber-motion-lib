package interfaces

import (
	"sync"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/ioc"
)

type responseSubscription struct {
	manager         *responseManager
	responseType    c.ResponseType
	filter          ioc.ResponseFilter
	responseChannel chan *c.Response
}

type responseManager struct {
	lock          sync.Mutex
	subscriptions map[*responseSubscription]bool
}

func newResponseManager() *responseManager {
	return &responseManager{
		subscriptions: make(map[*responseSubscription]bool),
	}
}

func (manager *responseManager) subscribe(responseType c.ResponseType, filter ioc.ResponseFilter) ioc.ResponseSubscription {
	subscription := &responseSubscription{
		manager:         manager,
		responseType:    responseType,
		filter:          filter,
		responseChannel: make(chan *c.Response, 1),
	}

	manager.lock.Lock()
	manager.subscriptions[subscription] = false
	manager.lock.Unlock()

	return subscription
}

func (manager *responseManager) cancelSubscription(subscription *responseSubscription) {
	manager.lock.Lock()
	_, isPresent := manager.subscriptions[subscription]
	if isPresent {
		delete(manager.subscriptions, subscription)
	}
	manager.lock.Unlock()

	if isPresent {
		close(subscription.responseChannel)
	}
}

func (manager *responseManager) responseReceived(response *c.Response) {
	var toInvoke []*responseSubscription

	manager.lock.Lock()
	for subscription := range manager.subscriptions {
		shouldInvoke := subscription.responseType == response.Type && (subscription.filter == nil || subscription.filter(response))
		if shouldInvoke {
			toInvoke = append(toInvoke, subscription)
			delete(manager.subscriptions, subscription)
		}
	}
	manager.lock.Unlock()

	for _, subscription := range toInvoke {
		subscription.responseChannel <- response
		close(subscription.responseChannel)
	}
}

func (subscription *responseSubscription) Cancel() {
	subscription.manager.cancelSubscription(subscription)
}

func (subscription *responseSubscription) Channel() chan *c.Response {
	return subscription.responseChannel
}
