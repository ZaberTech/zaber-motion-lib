//go:build wasm

package interfaces

import (
	"fmt"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func (manager *interfaceManager) openIO(request *pb.OpenInterfaceRequest) (io cBase.IO, infoString string, err errors.SdkError) {
	if request.InterfaceType == pb.InterfaceType_CUSTOM {
		io, err = manager.customConnections.GetAndBindTransport(request.Transport)
		infoString = fmt.Sprintf("ASCII Custom: %d", request.Transport)
	} else {
		return nil, "", errors.ErrUnknownRequest("OpenInterfaceRequest. Interface not implemented. Only 'openCustom' is available.")
	}

	if err != nil {
		return nil, "", err
	}
	return io, infoString, nil
}

func (manager *interfaceManager) openBinaryIO(request *pb.OpenBinaryInterfaceRequest) (io cBase.IO, infoString string, err errors.SdkError) {
	return nil, "", errors.ErrUnknownRequest("OpenBinaryInterfaceRequest. Interface not implemented for binary protocol")
}
