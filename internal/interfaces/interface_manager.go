package interfaces

import (
	"fmt"
	"time"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

type communicationInterface struct {
	id                   int32
	infoString           string
	requests             *c.RequestManager
	binaryRequests       *c.BinaryRequestManager
	protocol             *c.ProtocolCommunication
	io                   cBase.IO
	spontaneousResponses *responseManager
	binaryAlerts         *binaryAlertManager
}

type interfaceManager struct {
	communications               *utils.TokenMap[*communicationInterface]
	gateway                      ioc.GatewayManager
	customConnections            ioc.CustomConnections
	removeCommunicationCallbacks []ioc.OnInterfaceRemovedCallback
	protocolOptions              c.ProtocolOptions
}

var messageTypeMap map[c.ResponseType]pb.AsciiMessageType

func init() {
	messageTypeMap = map[c.ResponseType]pb.AsciiMessageType{
		c.ResponseTypeReply: pb.AsciiMessageType_REPLY,
		c.ResponseTypeAlert: pb.AsciiMessageType_ALERT,
		c.ResponseTypeInfo:  pb.AsciiMessageType_INFO,
	}
}

func NewInterfaceManager(gateway ioc.GatewayManager, customConnections ioc.CustomConnections) *interfaceManager {
	manager := &interfaceManager{
		communications: utils.NewTokenMap[*communicationInterface](),
		gateway:        gateway,
		protocolOptions: c.ProtocolOptions{
			TxChecksum: true,
		},
		customConnections: customConnections,
	}
	manager.register()
	return manager
}

func (manager *interfaceManager) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("interface/open", func() pb.Message {
		return &pb.OpenInterfaceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.openInterface(request.(*pb.OpenInterfaceRequest))
	})

	gateway.RegisterCallback("binary/interface/open", func() pb.Message {
		return &pb.OpenBinaryInterfaceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.openBinaryInterface(request.(*pb.OpenBinaryInterfaceRequest))
	})

	gateway.RegisterCallback("interface/close", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.closeInterface(request.(*pb.InterfaceEmptyRequest))
	})

	gateway.RegisterCallback("interface/reset_ids", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.resetIDs(request.(*pb.InterfaceEmptyRequest))
	})

	gateway.RegisterCallback("interface/get_timeout", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getTimeout(request.(*pb.InterfaceEmptyRequest))
	})

	gateway.RegisterCallback("interface/set_timeout", func() pb.Message {
		return &pb.SetInterfaceTimeoutRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setTimeout(request.(*pb.SetInterfaceTimeoutRequest))
	})

	gateway.RegisterCallback("interface/get_checksum_enabled", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getChecksumEnabled(request.(*pb.InterfaceEmptyRequest))
	})

	gateway.RegisterCallback("interface/set_checksum_enabled", func() pb.Message {
		return &pb.SetInterfaceChecksumEnabledRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setChecksumEnabled(request.(*pb.SetInterfaceChecksumEnabledRequest))
	})

	gateway.RegisterCallback("interface/generic_command", func() pb.Message {
		return &pb.GenericCommandRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericCommand(request.(*pb.GenericCommandRequest))
	})

	gateway.RegisterCallback("interface/generic_command_no_response", func() pb.Message {
		return &pb.GenericCommandRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.genericCommandNoResponse(request.(*pb.GenericCommandRequest))
	})

	gateway.RegisterCallback("interface/generic_command_multi_response", func() pb.Message {
		return &pb.GenericCommandRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericCommandMultiResponse(request.(*pb.GenericCommandRequest))
	})

	gateway.RegisterCallback("binary/interface/generic_command", func() pb.Message {
		return &pb.GenericBinaryRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericBinaryCommand(request.(*pb.GenericBinaryRequest))
	})

	gateway.RegisterCallback("binary/interface/generic_command_no_response", func() pb.Message {
		return &pb.GenericBinaryRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.genericBinaryCommandNoResponse(request.(*pb.GenericBinaryRequest))
	})

	gateway.RegisterCallback("binary/interface/generic_command_multi_response", func() pb.Message {
		return &pb.GenericBinaryRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericBinaryCommandMultiResponse(request.(*pb.GenericBinaryRequest))
	})

	gateway.RegisterCallback("interface/to_string", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.toString(request.(*pb.InterfaceEmptyRequest))
	})
}

func (manager *interfaceManager) getInterface(id int32) (*communicationInterface, errors.SdkError) {
	value, errLoad := manager.communications.Load(id)
	if errLoad != nil {
		return nil, errors.ErrConnectionClosed()
	}
	return value, nil
}

func (manager *interfaceManager) GetInterface(id int32) (ioc.CommunicationInterface, errors.SdkError) {
	return manager.getInterface(id)
}

func (manager *interfaceManager) AddCommunication(io cBase.IO, infoString string) int32 {
	communication := &communicationInterface{
		io:                   io,
		infoString:           infoString,
		spontaneousResponses: newResponseManager(),
	}
	communication.id = manager.communications.Store(communication)

	communication.protocol = c.NewProtocol(io, manager.protocolOptions)
	events := c.RequestManagerEvents{
		UnknownResponse: func(response *c.Response) { manager.unknownResponse(communication, response) },
		Alert:           func(response *c.Response) { manager.alert(communication, response) },
		Error:           func(err errors.SdkError) { manager.communicationError(communication, err) },
	}
	communication.requests = c.NewRequestManager(
		communication.protocol,
		events,
	)

	return communication.id
}

func (manager *interfaceManager) openInterface(request *pb.OpenInterfaceRequest) (response *pb.OpenInterfaceResponse, err errors.SdkError) {
	io, infoString, err := manager.openIO(request)
	if err != nil {
		return nil, err
	}

	id := manager.AddCommunication(io, infoString)
	return &pb.OpenInterfaceResponse{
		InterfaceId: id,
	}, nil
}

func (manager *interfaceManager) closeInterface(request *pb.InterfaceEmptyRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return nil // repeated close is noop
	}

	commErr := communication.io.Close()
	return c.ConvertCommErr(commErr)
}

func (manager *interfaceManager) resetIDs(request *pb.InterfaceEmptyRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return errLoad
	}

	communication.requests.ResetIDs()
	return nil
}

func (manager *interfaceManager) getTimeout(request *pb.InterfaceEmptyRequest) (*pb.IntResponse, errors.SdkError) {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return nil, errLoad
	}

	return &pb.IntResponse{
		Value: (int32)(communication.requests.GetDefaultTimeout() / time.Millisecond),
	}, nil
}

func (manager *interfaceManager) setTimeout(request *pb.SetInterfaceTimeoutRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return errLoad
	}

	if request.Timeout <= 0 {
		return errors.ErrInvalidArgument(fmt.Sprintf("The timeout must be greater than 0 (provided %d).", request.Timeout))
	}

	communication.requests.SetDefaultTimeout(time.Duration(request.Timeout) * time.Millisecond)
	return nil
}

func (manager *interfaceManager) getChecksumEnabled(request *pb.InterfaceEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return nil, errLoad
	}

	return &pb.BoolResponse{
		Value: communication.protocol.GetTxChecksumEnabled(),
	}, nil
}

func (manager *interfaceManager) setChecksumEnabled(request *pb.SetInterfaceChecksumEnabledRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return errLoad
	}

	communication.protocol.SetTxChecksumEnabled(request.IsEnabled)
	return nil
}

func (manager *interfaceManager) toString(request *pb.InterfaceEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	var toString string
	if communication, err := manager.getInterface(request.InterfaceId); err != nil {
		if err.Type() != pb.Errors_CONNECTION_CLOSED {
			return nil, err
		}
		toString = fmt.Sprintf("Connection %d (Closed)", request.InterfaceId)
	} else {
		toString = communication.ToString()
	}

	return &pb.StringResponse{
		Value: toString,
	}, nil
}

func (manager *interfaceManager) genericCommand(request *pb.GenericCommandRequest) (*pb.AsciiResponse, errors.SdkError) {
	options := &c.RequestOptions{
		Timeout: time.Duration(request.Timeout) * time.Millisecond,
	}

	responses, err := manager.genericCommandImpl(request, options)
	if err != nil {
		return nil, err
	}

	return responses.Responses[0], nil
}

func (manager *interfaceManager) genericCommandMultiResponse(request *pb.GenericCommandRequest) (*pb.GenericCommandResponseCollection, errors.SdkError) {
	options := &c.RequestOptions{
		CollectMultiple: true,
		Timeout:         time.Duration(request.Timeout) * time.Millisecond,
	}

	return manager.genericCommandImpl(request, options)
}

func (manager *interfaceManager) genericCommandImpl(request *pb.GenericCommandRequest, options *c.RequestOptions) (*pb.GenericCommandResponseCollection, errors.SdkError) {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return nil, errLoad
	}

	cmd := c.Command{
		Device:  int(request.Device),
		Axis:    int(request.Axis),
		Command: request.Command,
	}

	replies, err := communication.requests.Request(cmd, options)
	if err != nil {
		return nil, err
	}

	if request.CheckErrors {
		for _, response := range replies {
			if response.Type == c.ResponseTypeReply {
				if cmd.Device != 0 {
					if err := commands.CheckDeviceAndAxis(&cmd, response); err != nil {
						return nil, err
					}
				}

				if err := commands.CheckOk(response, &cmd); err != nil {
					return nil, err
				}
			}
		}
	}

	results := make([]*pb.AsciiResponse, len(replies))
	for i, response := range replies {
		results[i] = &pb.AsciiResponse{
			MessageType:   messageTypeMap[response.Type],
			DeviceAddress: int32(response.Device),
			AxisNumber:    int32(response.Axis),
			ReplyFlag:     response.ReplyFlag,
			Status:        response.Status,
			WarningFlag:   response.WarningFlag,
			Data:          response.Data,
		}
	}

	resultCollection := &pb.GenericCommandResponseCollection{
		Responses: results,
	}

	return resultCollection, nil
}

func (manager *interfaceManager) genericCommandNoResponse(request *pb.GenericCommandRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return errLoad
	}

	cmd := c.Command{
		Device:  int(request.Device),
		Axis:    int(request.Axis),
		Command: request.Command,
	}

	return communication.requests.RequestNoResponse(cmd)
}

func (manager *interfaceManager) unknownResponse(communication *communicationInterface, response *c.Response) {
	communication.spontaneousResponses.responseReceived(response)

	event := &pb.UnknownResponseEventWrapper{
		InterfaceId: communication.id,
		UnknownResponse: &pb.AsciiUnknownResponseEvent{
			DeviceAddress: int32(response.Device),
			AxisNumber:    int32(response.Axis),
			ReplyFlag:     response.ReplyFlag,
			Status:        response.Status,
			WarningFlag:   response.WarningFlag,
			Data:          response.Data,
			MessageType:   messageTypeMap[response.Type],
		},
	}
	go manager.gateway.InvokeEvent("interface/unknown_response", event)
}

func (manager *interfaceManager) alert(communication *communicationInterface, alert *c.Response) {
	communication.spontaneousResponses.responseReceived(alert)

	event := &pb.AlertEventWrapper{
		InterfaceId: communication.id,
		Alert: &pb.AsciiAlertEvent{
			DeviceAddress: int32(alert.Device),
			AxisNumber:    int32(alert.Axis),
			Status:        alert.Status,
			WarningFlag:   alert.WarningFlag,
			Data:          alert.Data,
		},
	}
	go manager.gateway.InvokeEvent("interface/alert", event)
}

func (manager *interfaceManager) communicationError(communication *communicationInterface, err errors.SdkError) {
	if _, wasRemoved := manager.communications.Delete(communication.id); !wasRemoved {
		return
	}

	// close just in case it was unexpected error
	_ = communication.io.Close()

	event := &pb.DisconnectedEvent{
		InterfaceId:  communication.id,
		ErrorType:    err.Type(),
		ErrorMessage: err.Message(),
	}
	manager.gateway.InvokeEvent("interface/disconnected", event)

	for _, callback := range manager.removeCommunicationCallbacks {
		callback(communication)
	}
}

func (manager *interfaceManager) OnInterfaceRemoved(callback ioc.OnInterfaceRemovedCallback) {
	manager.removeCommunicationCallbacks = append(manager.removeCommunicationCallbacks, callback)
}

func (communication *communicationInterface) Requests() *c.RequestManager {
	return communication.requests
}

func (communication *communicationInterface) ID() int32 {
	return communication.id
}

func (communication *communicationInterface) SubscribeForAlert(filter ioc.ResponseFilter) ioc.ResponseSubscription {
	return communication.spontaneousResponses.subscribe(c.ResponseTypeAlert, filter)
}

func (communication *communicationInterface) SubscribeForUnknownResponse(filter ioc.ResponseFilter) ioc.ResponseSubscription {
	return communication.spontaneousResponses.subscribe(c.ResponseTypeReply, filter)
}

func (communication *communicationInterface) ToString() string {
	return fmt.Sprintf("Connection %d (%s)", communication.id, communication.infoString)
}

func (communication *communicationInterface) SetPacketSize(packetSize int) {
	communication.protocol.SetPacketSize(packetSize)
}

func (communication *communicationInterface) SetDeviceProtocolInfo(deviceNumber int, info *c.DeviceProtocolInfo) {
	communication.protocol.SetDeviceProtocolInfo(deviceNumber, info)
}

func (communication *communicationInterface) GetDeviceLimits(deviceNumber int) (*c.DeviceProtocolInfo, int) {
	return communication.protocol.GetDeviceLimits(deviceNumber)
}
