//go:build !wasm

package interfaces

import (
	"fmt"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"

	cBase "gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

func getRoutedConnectionFromRequest(request *pb.OpenInterfaceRequest) *c.RoutedConnection {
	if request.RejectRoutedConnection {
		return nil
	}
	if connection, err := c.OpenSerialThroughMessageRouter(request.PortName); err != nil {
		return nil
	} else {
		return connection
	}
}

func (manager *interfaceManager) openIO(request *pb.OpenInterfaceRequest) (io cBase.IO, infoString string, err errors.SdkError) {
	var commErr cBase.CommErr
	if request.InterfaceType == pb.InterfaceType_SERIAL_PORT {
		if routedIo := getRoutedConnectionFromRequest(request); routedIo != nil {
			io = routedIo
			infoString = fmt.Sprintf("ASCII Serial port (message router): %s", request.PortName)
		} else {
			io, commErr = cBase.OpenSerial(request.PortName, int(request.BaudRate))
			infoString = fmt.Sprintf("ASCII Serial port: %s", request.PortName)
		}
	} else if request.InterfaceType == pb.InterfaceType_TCP {
		io, commErr = cBase.OpenTCP(request.HostName, int(request.Port))
		infoString = fmt.Sprintf("ASCII TCP: %s:%d", request.HostName, request.Port)
	} else if request.InterfaceType == pb.InterfaceType_CUSTOM {
		io, err = manager.customConnections.GetAndBindTransport(request.Transport)
		infoString = fmt.Sprintf("ASCII Custom: %d", request.Transport)
	} else if request.InterfaceType == pb.InterfaceType_IOT {
		io, err = c.OpenIoTConnection(request.CloudId, utils.PtrDefault(request.ConnectionName, ""), utils.PtrDefault(request.Realm, ""), request.Token, request.Api)
		infoString = fmt.Sprintf("ASCII IoT: %s", request.CloudId)
	} else if request.InterfaceType == pb.InterfaceType_NETWORK_SHARE {
		io, err = c.OpenNetworkShare(request.HostName, int(request.Port), utils.PtrDefault(request.ConnectionName, ""))
		infoString = fmt.Sprintf("ASCII Network Share: %s:%d", request.HostName, request.Port)
	}
	if commErr != nil {
		return nil, "", c.ConvertCommErr(commErr)
	}
	if err != nil {
		return nil, "", err
	}
	return io, infoString, nil
}

func (manager *interfaceManager) openBinaryIO(request *pb.OpenBinaryInterfaceRequest) (io cBase.IO, infoString string, err errors.SdkError) {
	var commErr cBase.CommErr
	if request.InterfaceType == pb.InterfaceType_SERIAL_PORT {
		io, commErr = cBase.OpenSerial(request.PortName, int(request.BaudRate))
		infoString = fmt.Sprintf("Binary Serial port: %s", request.PortName)
	} else if request.InterfaceType == pb.InterfaceType_TCP {
		io, commErr = cBase.OpenTCP(request.HostName, int(request.Port))
		infoString = fmt.Sprintf("Binary TCP: %s", request.HostName)
	}
	if commErr != nil {
		return nil, "", c.ConvertCommErr(commErr)
	}
	if err != nil {
		return nil, "", err
	}
	return io, infoString, nil
}
