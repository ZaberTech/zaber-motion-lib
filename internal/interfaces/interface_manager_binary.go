package interfaces

import (
	"time"
	bc "zaber-motion-lib/internal/binarycommands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

func (manager *interfaceManager) openBinaryInterface(request *pb.OpenBinaryInterfaceRequest) (response *pb.OpenInterfaceResponse, err errors.SdkError) {
	io, infoString, err := manager.openBinaryIO(request)
	if err != nil {
		return nil, err
	}

	communication := &communicationInterface{
		io:           io,
		infoString:   infoString,
		binaryAlerts: newBinaryAlertManager(),
	}
	communication.id = manager.communications.Store(communication)

	options := c.BinaryProtocolOptions{
		UseMessageIDs: request.UseMessageIds,
	}

	protocol := c.NewBinaryProtocol(io, options)
	events := c.BinaryRequestManagerEvents{
		UnknownResponse: func(response *c.BinaryMessage) { manager.unknownBinaryResponse(communication, response) },
		Alert:           func(response *c.BinaryMessage) { manager.binaryAlert(communication, response) },
		Error:           func(err errors.SdkError) { manager.communicationError(communication, err) },
	}
	communication.binaryRequests = c.NewBinaryRequestManager(
		protocol,
		events,
	)

	return &pb.OpenInterfaceResponse{
		InterfaceId: communication.id,
	}, nil
}

func (manager *interfaceManager) genericBinaryCommand(request *pb.GenericBinaryRequest) (*pb.BinaryMessage, errors.SdkError) {
	options := &c.RequestOptions{}
	responses, err := manager.genericBinaryCommandImpl(request, options)
	if err != nil {
		return nil, err
	}

	return responses.Messages[0], nil
}

func (manager *interfaceManager) genericBinaryCommandMultiResponse(request *pb.GenericBinaryRequest) (*pb.BinaryMessageCollection, errors.SdkError) {
	options := &c.RequestOptions{
		CollectMultiple: true,
	}

	return manager.genericBinaryCommandImpl(request, options)
}

func (manager *interfaceManager) genericBinaryCommandImpl(request *pb.GenericBinaryRequest, options *c.RequestOptions) (*pb.BinaryMessageCollection, errors.SdkError) {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return nil, errLoad
	}

	cmd := c.BinaryMessage{
		Device:  int(request.Device),
		Command: pb.BinaryCommandCode(request.Command),
		Data:    request.Data,
	}

	if request.Timeout > 0.0 {
		options.Timeout = time.Duration(request.Timeout*1000.0) * time.Millisecond
	}

	replies, err := communication.binaryRequests.Request(cmd, options)
	if err != nil {
		return nil, err
	}

	if request.CheckErrors {
		for _, reply := range replies {
			if err := bc.CheckOk(&cmd, reply); err != nil {
				return nil, err
			}
		}
	}

	results := make([]*pb.BinaryMessage, len(replies))
	for i, response := range replies {
		results[i] = &pb.BinaryMessage{
			DeviceAddress: int32(response.Device),
			Command:       int32(response.Command),
			Data:          response.Data,
		}
	}

	resultCollection := &pb.BinaryMessageCollection{
		Messages: results,
	}

	return resultCollection, nil
}

func (manager *interfaceManager) genericBinaryCommandNoResponse(request *pb.GenericBinaryRequest) errors.SdkError {
	communication, errLoad := manager.getInterface(request.InterfaceId)
	if errLoad != nil {
		return errLoad
	}

	cmd := c.BinaryMessage{
		Device:  int(request.Device),
		Command: pb.BinaryCommandCode(request.Command),
		Data:    request.Data,
	}

	return communication.binaryRequests.RequestNoResponse(cmd)
}

func (manager *interfaceManager) unknownBinaryResponse(communication *communicationInterface, response *c.BinaryMessage) {
	event := &pb.UnknownBinaryResponseEventWrapper{
		InterfaceId: communication.id,
		UnknownResponse: &pb.BinaryUnknownResponseEvent{
			DeviceAddress: int32(response.Device),
			Command:       int32(response.Command),
			Data:          response.Data,
		},
	}
	go manager.gateway.InvokeEvent("binary/interface/unknown_response", event)
}

func (manager *interfaceManager) binaryAlert(communication *communicationInterface, alert *c.BinaryMessage) {
	communication.binaryAlerts.alertReceived(alert)

	event := &pb.BinaryReplyOnlyEventWrapper{
		InterfaceId: communication.id,
		Reply: &pb.BinaryReplyOnlyEvent{
			DeviceAddress: int32(alert.Device),
			Command:       int32(alert.Command),
			Data:          alert.Data,
		},
	}
	go manager.gateway.InvokeEvent("binary/interface/reply_only", event)
}

func (communication *communicationInterface) BinaryRequests() *c.BinaryRequestManager {
	return communication.binaryRequests
}

func (communication *communicationInterface) SubscribeForBinaryAlert(filter ioc.BinaryAlertFilter) ioc.BinaryAlertSubscription {
	return communication.binaryAlerts.subscribe(filter)
}
