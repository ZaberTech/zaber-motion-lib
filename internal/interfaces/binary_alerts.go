package interfaces

import (
	"sync"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/ioc"
)

// The Binary protocol has "Reply-Only Commands" instead of alerts, but they are handled
// similarly to ASCII alerts in the Go part of the library. These objects and functions
// have "alert" in the name instead of "reply only" to associate them with the very
// similar ASCII implementation. In the public API these messages are named "reply only".

type binaryAlertSubscription struct {
	manager      *binaryAlertManager
	filter       ioc.BinaryAlertFilter
	alertChannel chan *c.BinaryMessage
}

type binaryAlertManager struct {
	lock          sync.Mutex
	subscriptions map[*binaryAlertSubscription]bool
}

func newBinaryAlertManager() *binaryAlertManager {
	return &binaryAlertManager{
		subscriptions: make(map[*binaryAlertSubscription]bool),
	}
}

func (manager *binaryAlertManager) subscribe(filter ioc.BinaryAlertFilter) ioc.BinaryAlertSubscription {
	subscription := &binaryAlertSubscription{
		manager:      manager,
		filter:       filter,
		alertChannel: make(chan *c.BinaryMessage, 1),
	}

	manager.lock.Lock()
	manager.subscriptions[subscription] = false
	manager.lock.Unlock()

	return subscription
}

func (manager *binaryAlertManager) cancel(subscription *binaryAlertSubscription) {
	manager.lock.Lock()
	_, isPresent := manager.subscriptions[subscription]
	if isPresent {
		delete(manager.subscriptions, subscription)
	}
	manager.lock.Unlock()

	if isPresent {
		close(subscription.alertChannel)
	}
}

func (manager *binaryAlertManager) alertReceived(alert *c.BinaryMessage) {
	var toInvoke []*binaryAlertSubscription

	manager.lock.Lock()
	for subscription := range manager.subscriptions {
		if subscription.filter(alert) {
			toInvoke = append(toInvoke, subscription)
			delete(manager.subscriptions, subscription)
		}
	}
	manager.lock.Unlock()

	for _, subscription := range toInvoke {
		subscription.alertChannel <- alert
		close(subscription.alertChannel)
	}
}

func (subscription *binaryAlertSubscription) Cancel() {
	subscription.manager.cancel(subscription)
}

func (subscription *binaryAlertSubscription) Channel() chan *c.BinaryMessage {
	return subscription.alertChannel
}
