package init

import (
	"zaber-motion-lib/internal/customconnections"
	"zaber-motion-lib/internal/devicedb"
	"zaber-motion-lib/internal/devices"
	"zaber-motion-lib/internal/devicesbinary"
	"zaber-motion-lib/internal/gateway"
	"zaber-motion-lib/internal/gcode"
	"zaber-motion-lib/internal/interfaces"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/library"
	"zaber-motion-lib/internal/logging"
	"zaber-motion-lib/internal/microscopy"
	"zaber-motion-lib/internal/servotuning"
	"zaber-motion-lib/internal/storage"
	"zaber-motion-lib/internal/test"
	"zaber-motion-lib/internal/thirdparty/wdi"
	"zaber-motion-lib/internal/tools"
	"zaber-motion-lib/internal/units"
)

type Container struct {
	gatewayManager      ioc.GatewayManager
	testRequests        interface{}
	interfaceManager    ioc.InterfaceManager
	deviceManager       ioc.DeviceManager
	binaryDeviceManager interface{}
	deviceDb            ioc.DeviceDb
	units               ioc.Units
	log                 interface{}
	library             ioc.Library
	tools               interface{}
	customConnections   ioc.CustomConnections
	gcode               interface{}
	servotuning         interface{}
	microscopy          ioc.Microscopy
	wdi                 interface{}
	storage             ioc.StorageManager
}

func init() {
	container := &Container{}

	container.gatewayManager = gateway.NewGatewayManager()
	container.log = logging.NewLogging(container.gatewayManager)
	container.library = library.NewLibrary()
	container.tools = tools.NewTools(container.gatewayManager, container.library)
	container.testRequests = test.NewTestRequests(container.gatewayManager)
	container.deviceDb = devicedb.NewDeviceDb(container.gatewayManager, container.library)
	container.units = units.NewUnits(container.gatewayManager)
	container.customConnections = customconnections.NewCustomConnections(container.gatewayManager)
	container.interfaceManager = interfaces.NewInterfaceManager(container.gatewayManager, container.customConnections)
	container.deviceManager = devices.NewDeviceManager(container.gatewayManager, container.interfaceManager, container.deviceDb, container.units)
	container.storage = storage.NewStorageManager(container.gatewayManager, container.deviceManager)
	container.binaryDeviceManager = devicesbinary.NewDeviceManager(container.gatewayManager, container.interfaceManager, container.deviceDb, container.units)
	container.gcode = gcode.NewGCode(container.gatewayManager, container.deviceDb, container.units, container.deviceManager)
	container.servotuning = servotuning.NewServoTuning(container.gatewayManager, container.deviceDb, container.deviceManager, container.storage)
	container.microscopy = microscopy.NewMicroscopy(container.gatewayManager, container.units, container.deviceManager, container.storage)
	container.wdi = wdi.NewInterfaceManager(container.gatewayManager, container.microscopy)

	ioc.Instance = container
}

func (container *Container) GetGateway() ioc.GatewayManager {
	return container.gatewayManager
}

func (container *Container) GetInterfaceManager() ioc.InterfaceManager {
	return container.interfaceManager
}
