package devicesbinary

type interfaceTarget interface {
	GetInterfaceId() int32
}
type deviceTarget interface {
	GetDevice() int32
	interfaceTarget
}
type movementTarget interface {
	GetTimeout() float64
	GetUnit() string
	deviceTarget
}
