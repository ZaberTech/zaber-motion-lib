package devicesbinary

import (
	"sort"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) detect(request *pb.BinaryDeviceDetectRequest) (*pb.BinaryDeviceDetectResponse, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}
	replies, err := requests.Request(c.BinaryMessage{Command: pb.BinaryCommandCode_RETURN_DEVICE_ID}, &c.RequestOptions{
		CollectMultiple: true,
	})
	if err != nil {
		if err.Type() == pb.Errors_REQUEST_TIMEOUT {
			err = errors.ErrNoDeviceFound()
		}
		return nil, err
	}

	// sort by device
	sort.Slice(replies, func(i, j int) bool { return replies[i].Device < replies[j].Device })

	devices := make([]int32, len(replies))
	deviceAddressHasConflict := false

	for i, reply := range replies {
		deviceNumber := int32(reply.Device)
		devices[i] = deviceNumber

		if deviceNumber == 0 {
			return nil, errors.ErrInvalidData("Invalid device address: 0")
		}

		if i > 0 && devices[i-1] == deviceNumber {
			deviceAddressHasConflict = true
		}
	}
	if deviceAddressHasConflict {
		return nil, errors.ErrDeviceAddressConflict(devices)
	}

	response := &pb.BinaryDeviceDetectResponse{
		Devices: devices,
	}

	if request.IdentifyDevices {
		for _, device := range devices {
			target := &pb.DeviceIdentifyRequest{
				InterfaceId: request.InterfaceId,
				Device:      device,
			}

			_, err := manager.identifyDevice(target)
			if err != nil {
				return nil, err
			}
		}
	}
	return response, nil
}
