package devicesbinary

import (
	"fmt"
	"math"
	"sync"
	"time"
	bc "zaber-motion-lib/internal/binarycommands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	g "zaber-motion-lib/internal/generated"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

type deviceType int

const (
	deviceTypeUnknown deviceType = 0
	deviceTypeLinear  deviceType = 1
	deviceTypeRotary  deviceType = 2
)

type deviceInfo struct {
	_isIdentified     bool
	number            int
	fwVersion         dto.FirmwareVersion
	deviceID          int
	serialNumber      uint32
	product           *dto.Device
	lock              sync.Mutex
	identity          *pb.BinaryDeviceIdentity
	interfaceInfo     *interfaceInfo
	isPeripheral      bool
	peripheralID      int
	peripheralProduct *dto.Device
	resolution        int
	conversionTable   ioc.ConversionTable
	commands          ioc.BinaryCommandTable
	settingsTable     ioc.BinarySettingsTable
}

func singleCommand(requests *c.BinaryRequestManager, cmd c.BinaryMessage, options *c.RequestOptions) (*c.BinaryMessage, errors.SdkError) {
	replies, err := requests.Request(cmd, options)
	if err != nil {
		return nil, err
	}

	reply := replies[0]
	if reply.Device != cmd.Device {
		return nil, errors.ErrInvalidData(
			fmt.Sprintf("Response device does not match: %d != %d", reply.Device, cmd.Device),
		)
	}
	if err := bc.CheckOk(&cmd, reply); err != nil {
		return nil, err
	}
	return reply, nil
}

func singleCommandDevice(requests *c.BinaryRequestManager, request deviceTarget, command pb.BinaryCommandCode, data int32, options *c.RequestOptions) (*c.BinaryMessage, errors.SdkError) {
	return singleCommand(requests, c.BinaryMessage{
		Device:  int(request.GetDevice()),
		Command: command,
		Data:    data,
	}, options)
}

func (device *deviceInfo) isIdentified() bool {
	device.lock.Lock()
	defer device.lock.Unlock()

	return device._isIdentified
}

func (device *deviceInfo) verifyIdentified() errors.SdkError {
	if !device.isIdentified() {
		return errors.ErrDeviceNotIdentified(device.number)
	}
	return nil
}

func (manager *deviceManager) singleRequestDevice(request deviceTarget, command pb.BinaryCommandCode, data int32, options *c.RequestOptions) (*c.BinaryMessage, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}
	return singleCommandDevice(requests, request, command, data, options)
}

func (device *deviceInfo) toString() string {
	var deviceIdentity string

	device.lock.Lock()
	if device._isIdentified {
		if device.isPeripheral {
			deviceIdentity = fmt.Sprintf("SN: %d (%s -> %s)", device.serialNumber, device.peripheralProduct.Name, device.product.Name)
		} else {
			deviceIdentity = fmt.Sprintf("SN: %d (%s)", device.serialNumber, device.product.Name)
		}
	} else {
		deviceIdentity = "(Unknown)"
	}
	device.lock.Unlock()

	return fmt.Sprintf("Device %d %s -> %s", device.number, deviceIdentity, device.interfaceInfo.communication.ToString())
}

func (manager *deviceManager) deviceToString(request *pb.DeviceEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return &pb.StringResponse{
				Value: fmt.Sprintf("Device %d -> Connection Closed", request.Device),
			}, nil
		}
		return nil, err
	}
	return &pb.StringResponse{
		Value: device.toString(),
	}, nil
}

func getSettingInfo(request *pb.BinaryGenericWithUnitsRequest) (*g.BinarySetting, bool) {
	if request.Command == pb.BinaryCommandCode_RETURN_SETTING {
		return g.GetSettingByCommand((int32)(request.Data))
	}
	return g.GetSettingByCommand((int32)(request.Command))
}

func (manager *deviceManager) genericCommandWithUnits(request *pb.BinaryGenericWithUnitsRequest) (*pb.DoubleResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if err := device.verifyIdentified(); err != nil {
		return nil, err
	}

	settingInfo, isSetting := getSettingInfo(request)

	commandCode := (pb.BinaryCommandCode)(request.Command)

	var argConverted float64
	if isSetting && settingInfo.SetCommand == (int32)(request.Command) {
		argConverted, err = manager.convertUnitSetting(request, settingInfo, request.Data, request.FromUnit, false)
	} else {
		args := commandArg{request.Data, request.FromUnit}
		argConverted, err = manager.convertUnitCommand(request, commandCode, args, false)
	}
	if err != nil {
		return nil, err
	}

	options := &c.RequestOptions{
		Timeout: time.Duration(request.Timeout*1000.0) * time.Millisecond,
	}
	reply, err := manager.singleRequestDevice(request, commandCode, (int32)(math.Round(argConverted)), options)
	if err != nil {
		return nil, err
	}

	var resultConverted float64
	if isSetting && (int32(request.Command) == settingInfo.GetCommand || request.Command == pb.BinaryCommandCode_RETURN_SETTING) {
		resultConverted, err = manager.convertUnitSetting(request, settingInfo, (float64)(reply.Data), request.ToUnit, true)
	} else {
		result := commandArg{(float64)(reply.Data), request.ToUnit}
		resultConverted, err = manager.convertUnitCommand(request, commandCode, result, true)
	}
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: resultConverted,
	}, nil
}
