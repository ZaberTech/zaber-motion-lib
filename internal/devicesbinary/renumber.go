package devicesbinary

import (
	"time"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) renumber(request *pb.InterfaceEmptyRequest) (*pb.IntResponse, errors.SdkError) {
	// Since the devices are renumbered, device infos are now invalid and should be reset
	err := manager.clearAllDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	requests, err := manager.getRequests(request)
	if err != nil {
		return nil, err
	}

	replies, err := requests.Request(c.BinaryMessage{Command: pb.BinaryCommandCode_RENUMBER}, &c.RequestOptions{
		CollectMultiple: true,
		Timeout:         1000 * time.Millisecond, // Needs more than the default 500ms
	})
	if err != nil {
		if err.Type() == pb.Errors_REQUEST_TIMEOUT {
			err = errors.ErrNoDeviceFound()
		}
		return nil, err
	}

	response := &pb.IntResponse{
		Value: int32(len(replies)),
	}
	return response, nil
}
