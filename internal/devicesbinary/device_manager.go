package devicesbinary

import (
	"sync"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type interfaceInfo struct {
	communication ioc.BinaryCommunicationInterface
	devices       map[int]*deviceInfo
	lock          sync.Mutex
}

type deviceManager struct {
	gateway       ioc.GatewayManager
	interfaces    ioc.InterfaceManager
	lock          sync.Mutex
	interfaceInfo map[ioc.BinaryCommunicationInterface]*interfaceInfo
	units         ioc.Units
	deviceDb      ioc.DeviceDb
}

func NewDeviceManager(gateway ioc.GatewayManager, interfaces ioc.InterfaceManager, deviceDb ioc.DeviceDb, units ioc.Units) *deviceManager {
	manager := &deviceManager{
		gateway:       gateway,
		interfaces:    interfaces,
		interfaceInfo: make(map[ioc.BinaryCommunicationInterface]*interfaceInfo),
		units:         units,
		deviceDb:      deviceDb,
	}
	interfaces.OnInterfaceRemoved(func(communication ioc.CommunicationInterface) {
		manager.onInterfaceRemoved(communication)
	})
	manager.register()
	return manager
}

func (manager *deviceManager) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("binary/device/detect", func() pb.Message {
		return &pb.BinaryDeviceDetectRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.detect(request.(*pb.BinaryDeviceDetectRequest))
	})
	gateway.RegisterCallback("binary/device/renumber", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.renumber(request.(*pb.InterfaceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/identify", func() pb.Message {
		return &pb.DeviceIdentifyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.identify(request.(*pb.DeviceIdentifyRequest))
	})
	gateway.RegisterCallback("binary/device/get_identity", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIdentity(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/get_is_identified", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getIsIdentified(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/generic_command_with_units", func() pb.Message {
		return &pb.BinaryGenericWithUnitsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericCommandWithUnits(request.(*pb.BinaryGenericWithUnitsRequest))
	})
	gateway.RegisterCallback("binary/device/home", func() pb.Message {
		return &pb.BinaryDeviceHomeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.home(request.(*pb.BinaryDeviceHomeRequest))
	})
	gateway.RegisterCallback("binary/device/move", func() pb.Message {
		return &pb.BinaryDeviceMoveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.move(request.(*pb.BinaryDeviceMoveRequest))
	})
	gateway.RegisterCallback("binary/device/stop", func() pb.Message {
		return &pb.BinaryDeviceStopRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.stop(request.(*pb.BinaryDeviceStopRequest))
	})
	gateway.RegisterCallback("binary/device/wait_until_idle", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.waitUntilIdle(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/is_busy", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isBusy(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/device_to_string", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.deviceToString(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/get_setting", func() pb.Message {
		return &pb.BinaryDeviceGetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getDeviceSetting(request.(*pb.BinaryDeviceGetSettingRequest))
	})
	gateway.RegisterCallback("binary/device/set_setting", func() pb.Message {
		return &pb.BinaryDeviceSetSettingRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setDeviceSetting(request.(*pb.BinaryDeviceSetSettingRequest))
	})
	gateway.RegisterCallback("binary/device/is_parked", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.isParked(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/park", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.park(request.(*pb.DeviceEmptyRequest))
	})
	gateway.RegisterCallback("binary/device/unpark", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.unpark(request.(*pb.DeviceEmptyRequest))
	})
}

func (manager *deviceManager) getInterface(request interfaceTarget) (ioc.BinaryCommunicationInterface, errors.SdkError) {
	communication, err := manager.interfaces.GetInterface(request.GetInterfaceId())
	if err != nil {
		return nil, err
	}

	binaryComm, castOk := communication.(ioc.BinaryCommunicationInterface)
	if !castOk {
		return nil, errors.ErrInternal("The Binary protocol device manager was given a ASCII interface. This is a bug.")
	}

	return binaryComm, nil
}

func (manager *deviceManager) getRequests(request interfaceTarget) (*c.BinaryRequestManager, errors.SdkError) {
	communication, err := manager.getInterface(request)
	if err != nil {
		return nil, err
	}

	return communication.BinaryRequests(), nil
}

func (manager *deviceManager) getInterfaceInfo(request interfaceTarget) (*interfaceInfo, errors.SdkError) {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	communication, err := manager.getInterface(request)
	if err != nil {
		return nil, err
	}

	info, ok := manager.interfaceInfo[communication]
	if !ok {
		info = &interfaceInfo{
			communication: communication,
			devices:       make(map[int]*deviceInfo),
		}
		manager.interfaceInfo[communication] = info
	}

	return info, nil
}

func (manager *deviceManager) getDeviceInfoBase(request deviceTarget, fresh bool) (*deviceInfo, errors.SdkError) {
	info, err := manager.getInterfaceInfo(request)
	if err != nil {
		return nil, err
	}
	info.lock.Lock()
	defer info.lock.Unlock()

	number := (int)(request.GetDevice())

	device, ok := info.devices[number]
	if !ok || fresh {
		device = &deviceInfo{
			number:        number,
			interfaceInfo: info,
		}
		info.devices[number] = device
	}

	return device, nil
}

func (manager *deviceManager) getDeviceInfo(request deviceTarget) (*deviceInfo, errors.SdkError) {
	return manager.getDeviceInfoBase(request, false)
}

func (manager *deviceManager) clearAllDeviceInfo(request interfaceTarget) errors.SdkError {
	info, err := manager.getInterfaceInfo(request)
	if err != nil {
		return err
	}

	info.lock.Lock()
	defer info.lock.Unlock()

	info.devices = make(map[int]*deviceInfo)

	return nil
}

func (manager *deviceManager) onInterfaceRemoved(communication ioc.CommunicationInterface) {
	binaryCommunication, ok := communication.(ioc.BinaryCommunicationInterface)
	if !ok {
		return
	}

	manager.lock.Lock()
	defer manager.lock.Unlock()

	delete(manager.interfaceInfo, binaryCommunication)
}
