package devicesbinary

import (
	"fmt"
	"math"
	"strings"
	"time"
	com "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

const (
	BinaryReplyCodeIdle           pb.BinaryReplyCode = 0
	BinaryReplyCodeParked         pb.BinaryReplyCode = 65
	BinaryReplyCodeDriverDisabled pb.BinaryReplyCode = 90
)

type commandArg struct {
	value float64
	unit  string
}

var moveNameToCommandCode = map[string]pb.BinaryCommandCode{
	"abs": pb.BinaryCommandCode_MOVE_ABSOLUTE,
	"rel": pb.BinaryCommandCode_MOVE_RELATIVE,
	"vel": pb.BinaryCommandCode_MOVE_AT_CONSTANT_SPEED,
}

func (manager *deviceManager) convertUnitCommand(request deviceTarget, commandCode pb.BinaryCommandCode, argument commandArg, convertBack bool) (float64, errors.SdkError) {
	if argument.unit == constants.NativeUnit {
		return argument.value, nil
	}

	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return 0, err
	}

	if err := device.verifyIdentified(); err != nil {
		return 0, err
	}

	contextDimensions, found := device.commands.GetContextualDimensions(commandCode)
	if !found {
		return 0, errors.ErrNotSupported(fmt.Sprintf("Command code %d not supported.", commandCode))
	}

	if convertBack {
		if contextDimensions.ReturnContextualDimension == 0 {
			return 0, errors.ErrConversionFailed("Command reply does not support unit conversion.")
		}
		return device.conversionTable.ConvertBack(argument.value, argument.unit, contextDimensions.ReturnContextualDimension)
	} else {
		if contextDimensions.ParamContextualDimension == 0 {
			return 0, errors.ErrConversionFailed("Command argument does not support unit conversion.")
		}
		return device.conversionTable.Convert(argument.value, argument.unit, contextDimensions.ParamContextualDimension)
	}
}

func (manager *deviceManager) moveCommandWithConversion(request movementTarget, commandCode pb.BinaryCommandCode, data int32) (float64, errors.SdkError) {
	requests, err := manager.getRequests(request)
	if err != nil {
		return 0, err
	}

	requestOptions := &com.RequestOptions{
		Timeout: time.Duration(request.GetTimeout()*1000.0) * time.Millisecond,
	}

	reply, err := singleCommandDevice(requests, request, commandCode, data, requestOptions)
	if err != nil {
		return 0, err
	}

	result := commandArg{(float64)(reply.Data), request.GetUnit()}
	resultConverted, err := manager.convertUnitCommand(request, commandCode, result, true)
	if err != nil {
		return 0, err
	}
	return resultConverted, nil
}

func (manager *deviceManager) home(request *pb.BinaryDeviceHomeRequest) (*pb.DoubleResponse, errors.SdkError) {
	resultConverted, err := manager.moveCommandWithConversion(request, pb.BinaryCommandCode_HOME, 0)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: resultConverted,
	}, nil
}

func (manager *deviceManager) move(request *pb.BinaryDeviceMoveRequest) (*pb.DoubleResponse, errors.SdkError) {
	moveType := strings.ToLower(pb.AxisMoveType_name[request.Type])
	commandCode := moveNameToCommandCode[moveType]
	args := commandArg{request.Arg, request.Unit}

	argConverted, err := manager.convertUnitCommand(request, commandCode, args, false)
	if err != nil {
		return nil, err
	}

	resultConverted, err := manager.moveCommandWithConversion(request, commandCode, (int32)(math.Round(argConverted)))
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: resultConverted,
	}, nil
}

func (manager *deviceManager) stop(request *pb.BinaryDeviceStopRequest) (*pb.DoubleResponse, errors.SdkError) {
	resultConverted, err := manager.moveCommandWithConversion(request, pb.BinaryCommandCode_STOP, 0)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: resultConverted,
	}, nil
}

func isDeviceIdle(reply *com.BinaryMessage) bool {
	return reply.Data == (int32)(BinaryReplyCodeIdle) || reply.Data == (int32)(BinaryReplyCodeDriverDisabled) || reply.Data == (int32)(BinaryReplyCodeParked)
}

func (manager *deviceManager) waitUntilIdle(request *pb.DeviceEmptyRequest) errors.SdkError {
	requests, err := manager.getRequests(request)
	if err != nil {
		return err
	}

	for {
		reply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_STATUS, 0, nil)
		if err != nil {
			return err
		}
		if isDeviceIdle(reply) {
			return nil
		}
		time.Sleep(constants.GetIdlePollingPeriod())
	}
}

func (manager *deviceManager) isBusy(request *pb.DeviceEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, pb.BinaryCommandCode_RETURN_STATUS, 0, nil)
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: !isDeviceIdle(reply),
	}, nil
}
