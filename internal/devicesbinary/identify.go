package devicesbinary

import (
	"strconv"
	bc "zaber-motion-lib/internal/binarycommands"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

var deviceTypeDimensionMap map[string]deviceType
var deviceTypeToPbMap map[deviceType]pb.BinaryDeviceType

func init() {
	deviceTypeDimensionMap = map[string]deviceType{
		"Length": deviceTypeLinear,
		"Angle":  deviceTypeRotary,
	}
	deviceTypeToPbMap = map[deviceType]pb.BinaryDeviceType{
		deviceTypeLinear:  pb.BinaryDeviceType_LINEAR,
		deviceTypeRotary:  pb.BinaryDeviceType_ROTARY,
		deviceTypeUnknown: pb.BinaryDeviceType_UNKNOWN,
	}
}

func (manager *deviceManager) getIdentity(request *pb.DeviceEmptyRequest) (*pb.BinaryDeviceIdentity, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	if err := device.verifyIdentified(); err != nil {
		return nil, err
	}

	return device.identity, nil
}

func (manager *deviceManager) getIsIdentified(request *pb.DeviceEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: device.isIdentified(),
	}, nil
}

func (manager *deviceManager) identify(request *pb.DeviceIdentifyRequest) (*pb.BinaryDeviceIdentity, errors.SdkError) {
	return manager.identifyDevice(request)
}

func (manager *deviceManager) identifyDevice(request *pb.DeviceIdentifyRequest) (*pb.BinaryDeviceIdentity, errors.SdkError) {
	device, err := manager.getDeviceInfoBase(request, true)
	if err != nil {
		return nil, err
	}

	requests := device.interfaceInfo.communication.BinaryRequests()

	autoReplyReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_SETTING, (int32)(pb.BinaryCommandCode_SET_AUTO_REPLY_DISABLED_MODE), nil)
	if err != nil {
		if !bc.IsInvalidCommandOrSettingErr(err) {
			return nil, err
		}
	} else {
		if autoReplyReply.Data == 1 {
			return nil, errors.ErrDeviceNoAutoResponse((int32)(device.number))
		}
	}

	idReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_DEVICE_ID, 0, nil)
	if err != nil {
		return nil, err
	}
	deviceID := idReply.Data

	var serialNumber uint32
	serialNumberReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_SERIAL_NUMBER, 0, nil)
	if err != nil {
		if !bc.IsInvalidCommandOrSettingErr(err) {
			return nil, err
		}
	} else {
		serialNumber = (uint32)(serialNumberReply.Data)
	}

	var fwVersion dto.FirmwareVersion
	if utils.IsEmptyFirmwareVersion(request.AssumeVersion) {
		versionReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_FIRMWARE_VERSION, 0, nil)
		if err != nil {
			return nil, err
		}
		fwVersion = dto.FirmwareVersion{
			Major: (int)(versionReply.Data / 100),
			Minor: (int)(versionReply.Data % 100),
		}

		if fwVersion.Major >= 6 {
			buildNumberReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_FIRMWARE_BUILD, 0, nil)
			if err != nil {
				if !bc.IsInvalidCommandOrSettingErr(err) {
					return nil, err
				}
			} else {
				fwVersion.Build = (int)(buildNumberReply.Data)
			}
		}
	} else {
		version := request.AssumeVersion
		fwVersion = dto.FirmwareVersion{
			Major: int(version.Major),
			Minor: int(version.Minor),
			Build: int(version.Build),
		}
	}

	resolution := 1
	resolutionReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_SETTING, (int32)(pb.BinaryCommandCode_SET_MICROSTEP_RESOLUTION), nil)
	if err != nil {
		if !bc.IsInvalidCommandOrSettingErr(err) {
			return nil, err
		}
	} else {
		resolution = (int)(resolutionReply.Data)
	}

	isPeripheral := false
	var peripheralID int32
	peripheralReply, err := singleCommandDevice(requests, request, pb.BinaryCommandCode_RETURN_SETTING, (int32)(pb.BinaryCommandCode_SET_PERIPHERAL_ID), nil)
	if err != nil {
		if !bc.IsInvalidCommandOrSettingErr(err) {
			return nil, err
		}
		// is not peripheral
	} else {
		peripheralID = peripheralReply.Data
		isPeripheral = true

		// firmware <6.00,6.04> has bug of returning peripheral ID for integrated products
		if fwVersion.Major == 6 && fwVersion.Minor <= 4 && deviceID != 30111 /*A-MCA*/ {
			isPeripheral = false
		}
	}

	var peripheralIDStr string
	if isPeripheral {
		peripheralIDStr = strconv.Itoa(int(peripheralID))
	}
	productInfo, err := manager.deviceDb.GetBinaryProductInformation(strconv.Itoa(int(deviceID)), &fwVersion, peripheralIDStr)
	if err != nil {
		return nil, err
	}

	var conversionTable ioc.ConversionTable
	var commands ioc.BinaryCommandTable
	var settingsTable ioc.BinarySettingsTable
	if isPeripheral {
		conversionTable = manager.units.GetConversionTable(productInfo.Peripheral.ConversionTable, resolution)
		commands = manager.deviceDb.GetBinaryCommandTable(productInfo.Peripheral.Commands)
		settingsTable = manager.deviceDb.GetBinarySettingsTable(productInfo.Peripheral.Settings)
	} else {
		conversionTable = manager.units.GetConversionTable(productInfo.ConversionTable, resolution)
		commands = manager.deviceDb.GetBinaryCommandTable(productInfo.Commands)
		settingsTable = manager.deviceDb.GetBinarySettingsTable(productInfo.Settings)
	}

	var deviceType deviceType
	moveAbsCtxDimensions, found := commands.GetContextualDimensions(pb.BinaryCommandCode_MOVE_ABSOLUTE)
	if found {
		dimensionName, found := conversionTable.GetDimensionName(moveAbsCtxDimensions.ParamContextualDimension)
		if found {
			deviceType = deviceTypeDimensionMap[dimensionName]
		}
	}
	identity := &pb.BinaryDeviceIdentity{
		DeviceId:     deviceID,
		SerialNumber: serialNumber,
		Name:         productInfo.Device.Name,
		FirmwareVersion: &pb.FirmwareVersion{
			Major: (int32)(fwVersion.Major),
			Minor: (int32)(fwVersion.Minor),
			Build: (int32)(fwVersion.Build),
		},
		IsPeripheral: isPeripheral,
		PeripheralId: peripheralID,
		DeviceType:   deviceTypeToPbMap[deviceType],
	}

	if isPeripheral {
		identity.PeripheralName = productInfo.Peripheral.Device.Name
	}

	device.lock.Lock()
	defer device.lock.Unlock()

	device.fwVersion = fwVersion
	device.deviceID = (int)(deviceID)
	device.serialNumber = serialNumber
	device.peripheralID = (int)(peripheralID)
	device.isPeripheral = isPeripheral
	device.product = productInfo.Device
	if isPeripheral {
		device.peripheralProduct = productInfo.Peripheral.Device
	}
	device.conversionTable = conversionTable
	device.commands = commands
	device.settingsTable = settingsTable
	device.identity = identity
	device.resolution = resolution
	device._isIdentified = true

	return identity, nil
}
