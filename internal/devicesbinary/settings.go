package devicesbinary

import (
	"fmt"
	"math"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	g "zaber-motion-lib/internal/generated"
)

func (manager *deviceManager) convertUnitSetting(request deviceTarget, setting *g.BinarySetting, value float64, unit string, convertBack bool) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	}

	device, err := manager.getDeviceInfo(request)
	if err != nil {
		return 0, err
	}

	if err := device.verifyIdentified(); err != nil {
		return 0, err
	}

	contextDimension, found := device.settingsTable.GetContextualDimension(setting)
	if !found {
		return 0, errors.ErrConversionFailed(fmt.Sprintf("Conversion information not found for setting \"%s\".", setting.Name))
	}

	if convertBack {
		return device.conversionTable.ConvertBack(value, unit, contextDimension)
	}
	return device.conversionTable.Convert(value, unit, contextDimension)

}

func (manager *deviceManager) getDeviceSetting(request *pb.BinaryDeviceGetSettingRequest) (*pb.DoubleResponse, errors.SdkError) {
	settingInfo := g.GetSetting(int32(request.Setting))

	var commandCode pb.BinaryCommandCode
	var commandData int32
	if settingInfo.GetCommand != 0 {
		commandCode = (pb.BinaryCommandCode)(settingInfo.GetCommand)
		commandData = 0
	} else if settingInfo.SetCommand != 0 {
		commandCode = pb.BinaryCommandCode_RETURN_SETTING
		commandData = (int32)(settingInfo.SetCommand)
	} else {
		return nil, errors.ErrSettingNotFound(fmt.Sprintf("Get setting command not found for setting \"%s\".", settingInfo.Name))
	}

	reply, err := manager.singleRequestDevice(request, commandCode, commandData, nil)
	if err != nil {
		return nil, err
	}

	valueConverted, err := manager.convertUnitSetting(request, settingInfo, (float64)(reply.Data), request.Unit, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: valueConverted,
	}, nil
}

func (manager *deviceManager) setDeviceSetting(request *pb.BinaryDeviceSetSettingRequest) errors.SdkError {
	settingInfo := g.GetSetting(int32(request.Setting))
	if settingInfo.SetCommand == 0 {
		return errors.ErrSettingNotFound(fmt.Sprintf("Set setting command not found for setting \"%s\".", settingInfo.Name))
	}

	valueConverted, err := manager.convertUnitSetting(request, settingInfo, request.Value, request.Unit, false)
	if err != nil {
		return err
	}

	_, err = manager.singleRequestDevice(request, (pb.BinaryCommandCode)(settingInfo.SetCommand), (int32)(math.Round(valueConverted)), nil)
	return err
}
