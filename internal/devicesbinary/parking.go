package devicesbinary

import (
	"zaber-motion-lib/internal/binarycommands"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *deviceManager) park(request *pb.DeviceEmptyRequest) errors.SdkError {
	_, err := manager.singleRequestDevice(request, pb.BinaryCommandCode_SET_PARK_STATE, 1, nil)
	if err != nil {
		if binarycommands.IsErrorWithCode(err, pb.BinaryErrorCode_PARK_STATE_INVALID) {
			return errors.ErrInvalidParkState()
		}
		return err
	}
	return nil
}

func (manager *deviceManager) unpark(request *pb.DeviceEmptyRequest) errors.SdkError {
	_, err := manager.singleRequestDevice(request, pb.BinaryCommandCode_SET_PARK_STATE, 0, nil)
	if err != nil {
		if binarycommands.IsErrorWithCode(err, pb.BinaryErrorCode_PARK_STATE_INVALID) {
			return errors.ErrInvalidParkState()
		}
		return err
	}
	return nil
}

func (manager *deviceManager) isParked(request *pb.DeviceEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	reply, err := manager.singleRequestDevice(request, pb.BinaryCommandCode_RETURN_STATUS, 0, nil)
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: reply.Data == (int32)(pb.BinaryCommandCode_SET_PARK_STATE),
	}, nil
}
