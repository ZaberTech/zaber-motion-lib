// This file is generated from the Zaber device database. Do not manually edit this file.

package units

var staticUnitsJSON = `
[
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "metres",
    "ShortName": "m",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "centimetres",
    "ShortName": "cm",
    "Scale": 100,
    "Offset": 0
  },
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "millimetres",
    "ShortName": "mm",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "micrometres",
    "ShortName": "µm",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "nanometres",
    "ShortName": "nm",
    "Scale": 1000000000,
    "Offset": 0
  },
  {
    "DimensionName": "Length",
    "DimensionId": 1,
    "LongName": "inches",
    "ShortName": "in",
    "Scale": 39.37007874015748,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "metres per second",
    "ShortName": "m/s",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "centimetres per second",
    "ShortName": "cm/s",
    "Scale": 100,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "millimetres per second",
    "ShortName": "mm/s",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "micrometres per second",
    "ShortName": "µm/s",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "nanometres per second",
    "ShortName": "nm/s",
    "Scale": 1000000000,
    "Offset": 0
  },
  {
    "DimensionName": "Velocity",
    "DimensionId": 2,
    "LongName": "inches per second",
    "ShortName": "in/s",
    "Scale": 39.37007874015748,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "metres per second squared",
    "ShortName": "m/s²",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "centimetres per second squared",
    "ShortName": "cm/s²",
    "Scale": 100,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "millimetres per second squared",
    "ShortName": "mm/s²",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "micrometres per second squared",
    "ShortName": "µm/s²",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "nanometres per second squared",
    "ShortName": "nm/s²",
    "Scale": 1000000000,
    "Offset": 0
  },
  {
    "DimensionName": "Acceleration",
    "DimensionId": 3,
    "LongName": "inches per second squared",
    "ShortName": "in/s²",
    "Scale": 39.37007874015748,
    "Offset": 0
  },
  {
    "DimensionName": "Angle",
    "DimensionId": 4,
    "LongName": "degrees",
    "ShortName": "°",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Angle",
    "DimensionId": 4,
    "LongName": "radians",
    "ShortName": "rad",
    "Scale": 0.017453292519943295,
    "Offset": 0
  },
  {
    "DimensionName": "Angular Velocity",
    "DimensionId": 5,
    "LongName": "degrees per second",
    "ShortName": "°/s",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Angular Velocity",
    "DimensionId": 5,
    "LongName": "radians per second",
    "ShortName": "rad/s",
    "Scale": 0.017453292519943295,
    "Offset": 0
  },
  {
    "DimensionName": "Angular Acceleration",
    "DimensionId": 6,
    "LongName": "degrees per second squared",
    "ShortName": "°/s²",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Angular Acceleration",
    "DimensionId": 6,
    "LongName": "radians per second squared",
    "ShortName": "rad/s²",
    "Scale": 0.017453292519943295,
    "Offset": 0
  },
  {
    "DimensionName": "AC Electric Current",
    "DimensionId": 7,
    "LongName": "amperes peak",
    "ShortName": "A(peak)",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "AC Electric Current",
    "DimensionId": 7,
    "LongName": "amperes RMS",
    "ShortName": "A(RMS)",
    "Scale": 0.7071067811865476,
    "Offset": 0
  },
  {
    "DimensionName": "Percent",
    "DimensionId": 8,
    "LongName": "percent",
    "ShortName": "%",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "DC Electric Current",
    "DimensionId": 9,
    "LongName": "amperes",
    "ShortName": "A",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Force",
    "DimensionId": 10,
    "LongName": "newtons",
    "ShortName": "N",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Force",
    "DimensionId": 10,
    "LongName": "millinewtons",
    "ShortName": "mN",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Force",
    "DimensionId": 10,
    "LongName": "pounds-force",
    "ShortName": "lbf",
    "Scale": 0.2248089430997105,
    "Offset": 0
  },
  {
    "DimensionName": "Force",
    "DimensionId": 10,
    "LongName": "kilonewtons",
    "ShortName": "kN",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Time",
    "DimensionId": 11,
    "LongName": "seconds",
    "ShortName": "s",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Time",
    "DimensionId": 11,
    "LongName": "milliseconds",
    "ShortName": "ms",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Time",
    "DimensionId": 11,
    "LongName": "microseconds",
    "ShortName": "µs",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Torque",
    "DimensionId": 12,
    "LongName": "newton metres",
    "ShortName": "N⋅m",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Torque",
    "DimensionId": 12,
    "LongName": "newton centimetres",
    "ShortName": "N⋅cm",
    "Scale": 100,
    "Offset": 0
  },
  {
    "DimensionName": "Torque",
    "DimensionId": 12,
    "LongName": "pound-force-feet",
    "ShortName": "lbf⋅ft",
    "Scale": 0.7375621211696556,
    "Offset": 0
  },
  {
    "DimensionName": "Torque",
    "DimensionId": 12,
    "LongName": "ounce-force-inches",
    "ShortName": "ozf⋅in",
    "Scale": 141.61192726457386,
    "Offset": 0
  },
  {
    "DimensionName": "Inertia",
    "DimensionId": 13,
    "LongName": "grams",
    "ShortName": "g",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Inertia",
    "DimensionId": 13,
    "LongName": "kilograms",
    "ShortName": "kg",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Inertia",
    "DimensionId": 13,
    "LongName": "milligrams",
    "ShortName": "mg",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Inertia",
    "DimensionId": 13,
    "LongName": "pounds",
    "ShortName": "lb",
    "Scale": 0.002204622621848776,
    "Offset": 0
  },
  {
    "DimensionName": "Inertia",
    "DimensionId": 13,
    "LongName": "ounces",
    "ShortName": "oz",
    "Scale": 0.035273961949580414,
    "Offset": 0
  },
  {
    "DimensionName": "Rotational Inertia",
    "DimensionId": 14,
    "LongName": "gram-square metre",
    "ShortName": "g⋅m²",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Rotational Inertia",
    "DimensionId": 14,
    "LongName": "kilogram-square metre",
    "ShortName": "kg⋅m²",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Rotational Inertia",
    "DimensionId": 14,
    "LongName": "pound-square-feet",
    "ShortName": "lb⋅ft²",
    "Scale": 0.02373036,
    "Offset": 0
  },
  {
    "DimensionName": "Force Constant",
    "DimensionId": 15,
    "LongName": "newtons per amp",
    "ShortName": "N/A",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Force Constant",
    "DimensionId": 15,
    "LongName": "millinewtons per amp",
    "ShortName": "mN/A",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Force Constant",
    "DimensionId": 15,
    "LongName": "kilonewtons per amp",
    "ShortName": "kN/A",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Force Constant",
    "DimensionId": 15,
    "LongName": "pounds-force per amp",
    "ShortName": "lbf/A",
    "Scale": 0.2248089430997105,
    "Offset": 0
  },
  {
    "DimensionName": "Torque Constant",
    "DimensionId": 16,
    "LongName": "newton metres per amp",
    "ShortName": "N⋅m/A",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Torque Constant",
    "DimensionId": 16,
    "LongName": "millinewton metres per amp",
    "ShortName": "mN⋅m/A",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Torque Constant",
    "DimensionId": 16,
    "LongName": "kilonewton metres per amp",
    "ShortName": "kN⋅m/A",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Torque Constant",
    "DimensionId": 16,
    "LongName": "pound-force-feet per amp",
    "ShortName": "lbf⋅ft/A",
    "Scale": 0.7375621211696556,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage",
    "DimensionId": 17,
    "LongName": "volts",
    "ShortName": "V",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage",
    "DimensionId": 17,
    "LongName": "millivolts",
    "ShortName": "mV",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage",
    "DimensionId": 17,
    "LongName": "microvolts",
    "ShortName": "µV",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Proportional Gain",
    "DimensionId": 18,
    "LongName": "volts per amp",
    "ShortName": "V/A",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Proportional Gain",
    "DimensionId": 18,
    "LongName": "millivolts per amp",
    "ShortName": "mV/A",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Proportional Gain",
    "DimensionId": 18,
    "LongName": "microvolts per amp",
    "ShortName": "µV/A",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Integral Gain",
    "DimensionId": 19,
    "LongName": "volts per amp per second",
    "ShortName": "V/(A⋅s)",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Integral Gain",
    "DimensionId": 19,
    "LongName": "millivolts per amp per second",
    "ShortName": "mV/(A⋅s)",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Integral Gain",
    "DimensionId": 19,
    "LongName": "microvolts per amp per second",
    "ShortName": "µV/(A⋅s)",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Derivative Gain",
    "DimensionId": 20,
    "LongName": "volts second per amp",
    "ShortName": "V⋅s/A",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Derivative Gain",
    "DimensionId": 20,
    "LongName": "millivolts second per amp",
    "ShortName": "mV⋅s/A",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Current Controller Derivative Gain",
    "DimensionId": 20,
    "LongName": "microvolts second per amp",
    "ShortName": "µV⋅s/A",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Resistance",
    "DimensionId": 21,
    "LongName": "kiloohms",
    "ShortName": "kΩ",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Resistance",
    "DimensionId": 21,
    "LongName": "ohms",
    "ShortName": "Ω",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Resistance",
    "DimensionId": 21,
    "LongName": "milliohms",
    "ShortName": "mΩ",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Resistance",
    "DimensionId": 21,
    "LongName": "microohms",
    "ShortName": "µΩ",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Resistance",
    "DimensionId": 21,
    "LongName": "nanoohms",
    "ShortName": "nΩ",
    "Scale": 1000000000,
    "Offset": 0
  },
  {
    "DimensionName": "Inductance",
    "DimensionId": 22,
    "LongName": "henries",
    "ShortName": "H",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Inductance",
    "DimensionId": 22,
    "LongName": "millihenries",
    "ShortName": "mH",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Inductance",
    "DimensionId": 22,
    "LongName": "microhenries",
    "ShortName": "µH",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Inductance",
    "DimensionId": 22,
    "LongName": "nanohenries",
    "ShortName": "nH",
    "Scale": 1000000000,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage Constant",
    "DimensionId": 23,
    "LongName": "volt seconds per radian",
    "ShortName": "V·s/rad",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage Constant",
    "DimensionId": 23,
    "LongName": "millivolt seconds per radian",
    "ShortName": "mV·s/rad",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Voltage Constant",
    "DimensionId": 23,
    "LongName": "microvolt seconds per radian",
    "ShortName": "µV·s/rad",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Absolute Temperature",
    "DimensionId": 24,
    "LongName": "degrees Celsius",
    "ShortName": "°C",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Absolute Temperature",
    "DimensionId": 24,
    "LongName": "kelvins",
    "ShortName": "K",
    "Scale": 1,
    "Offset": 273.15
  },
  {
    "DimensionName": "Absolute Temperature",
    "DimensionId": 24,
    "LongName": "degrees Fahrenheit",
    "ShortName": "°F",
    "Scale": 1.8,
    "Offset": 32
  },
  {
    "DimensionName": "Absolute Temperature",
    "DimensionId": 24,
    "LongName": "degrees Rankine",
    "ShortName": "°R",
    "Scale": 1.8,
    "Offset": 491.67
  },
  {
    "DimensionName": "Relative Temperature",
    "DimensionId": 25,
    "LongName": "degrees Celsius",
    "ShortName": "°C",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Relative Temperature",
    "DimensionId": 25,
    "LongName": "kelvins",
    "ShortName": "K",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Relative Temperature",
    "DimensionId": 25,
    "LongName": "degrees Fahrenheit",
    "ShortName": "°F",
    "Scale": 1.8,
    "Offset": 0
  },
  {
    "DimensionName": "Relative Temperature",
    "DimensionId": 25,
    "LongName": "degrees Rankine",
    "ShortName": "°R",
    "Scale": 1.8,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "gigahertz",
    "ShortName": "GHz",
    "Scale": 1e-9,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "megahertz",
    "ShortName": "MHz",
    "Scale": 0.000001,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "kilohertz",
    "ShortName": "kHz",
    "Scale": 0.001,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "hertz",
    "ShortName": "Hz",
    "Scale": 1,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "millihertz",
    "ShortName": "mHz",
    "Scale": 1000,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "microhertz",
    "ShortName": "µHz",
    "Scale": 1000000,
    "Offset": 0
  },
  {
    "DimensionName": "Frequency",
    "DimensionId": 26,
    "LongName": "nanohertz",
    "ShortName": "nHz",
    "Scale": 1000000000,
    "Offset": 0
  }
]
`
