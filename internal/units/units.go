package units

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"regexp"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/generated"
	"zaber-motion-lib/internal/utils"

	"zaber-motion-lib/internal/ioc"

	pb "zaber-motion-lib/internal/dto"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

type staticUnit struct {
	DimensionName string  `json:"DimensionName"`
	UnitLongName  string  `json:"LongName"`
	UnitShortName string  `json:"ShortName"`
	Scale         float64 `json:"Scale"`
	Offset        float64 `json:"Offset"`
}

type conversionFunction func(value float64) float64

type units struct {
	staticConversions  map[string]*staticUnit
	dimensionBaseUnits map[string]*staticUnit
	shortNames         map[string]string
}

type conversionTableEntry struct {
	convert                 conversionFunction
	dimensionName           string
	contextualDimensionName string
}

type dimensionMap = map[ioc.ContextualDimensionID]*conversionTableEntry

type conversionTable struct {
	resolution                  int
	units                       *units
	dimensionConversions        dimensionMap
	dimensionConversionsInverse dimensionMap
	source                      *dto.ConversionTable
}

var lookupDimensions = map[string]bool{
	"Length":                               true,
	"Velocity":                             true,
	"Acceleration":                         true,
	"Angle":                                true,
	"Angular Velocity":                     true,
	"Angular Acceleration":                 true,
	"AC Electric Current":                  true,
	"Percent":                              true,
	"DC Electric Current":                  true,
	"Force":                                true,
	"Time":                                 true,
	"Torque":                               true,
	"Inertia":                              true,
	"Rotational Inertia":                   true,
	"Force Constant":                       true,
	"Torque Constant":                      true,
	"Voltage":                              true,
	"Current Controller Proportional Gain": true,
	"Current Controller Integral Gain":     true,
	"Current Controller Derivative Gain":   true,
	"Resistance":                           true,
	"Inductance":                           true,
	"Voltage Constant":                     true,
	"Frequency":                            true,
}

func NewUnits(gateway ioc.GatewayManager) *units {
	units := &units{}

	units.loadStaticConversions()
	units.register(gateway)

	return units
}

func (units *units) register(gateway ioc.GatewayManager) {
	gateway.RegisterCallback("units/get_symbol", func() pb.Message {
		return &pb.UnitGetSymbolRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return units.getSymbolRequest(request.(*pb.UnitGetSymbolRequest))
	})

	gateway.RegisterCallback("units/get_enum", func() pb.Message {
		return &pb.UnitGetEnumRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return units.getEnumRequest(request.(*pb.UnitGetEnumRequest))
	})
}

func (units *units) getSymbolRequest(request *pb.UnitGetSymbolRequest) (*pb.UnitGetSymbolResponse, errors.SdkError) {
	unit, err := units.getUnitData(request.Unit)
	if err != nil || len(unit.UnitShortName) == 0 {
		return nil, errors.ErrNoValueForKey(request.Unit)
	}

	return &pb.UnitGetSymbolResponse{Symbol: unit.UnitShortName}, nil
}

func (units *units) getShortNameVariations(shortName string) []string {
	var names []string
	names = append(names, shortName)

	replacements := map[string]string{
		"²": "^2",
		"°": "deg",
		"µ": "u",
	}

	re := regexp.MustCompile("[µ°²]")
	altName := re.ReplaceAllStringFunc(shortName, func(s string) string { return replacements[s] })
	if altName != shortName {
		names = append(names, altName)
	}

	return names
}

func (units *units) getEnumRequest(request *pb.UnitGetEnumRequest) (*pb.UnitGetEnumResponse, errors.SdkError) {
	longName, err := units.getByShortName(request.Symbol)
	if err != nil {
		return nil, err
	}

	return &pb.UnitGetEnumResponse{Unit: longName}, nil
}

func (units *units) loadStaticConversions() {
	var rows []*staticUnit
	err := json.Unmarshal(([]byte)(staticUnitsJSON), &rows)
	if err != nil {
		panic(err)
	}

	units.staticConversions = make(map[string]*staticUnit)
	units.dimensionBaseUnits = map[string]*staticUnit{}
	units.shortNames = make(map[string]string)

	for _, unit := range rows {
		key := unit.getKey()
		units.staticConversions[key] = unit

		if unit.Scale == 1 && unit.Offset == 0 {
			units.dimensionBaseUnits[unit.DimensionName] = unit
		}

		if len(unit.UnitShortName) > 0 {
			if _, ok := lookupDimensions[unit.DimensionName]; ok {
				nameVariations := units.getShortNameVariations(unit.UnitShortName)
				for _, name := range nameVariations {
					units.shortNames[name] = key
				}
			}
		}
	}

	if units.dimensionBaseUnits[Dimension_Time].UnitLongName != "seconds" ||
		units.dimensionBaseUnits[Dimension_Frequency].UnitLongName != "hertz" {
		panic("Unexpected base units for Time and Frequency. Some calculations may be incorrect.")
	}
}

func (units *units) getUnitData(unit string) (*staticUnit, errors.SdkError) {
	unitData, ok := units.staticConversions[unit]
	if !ok {
		return nil, errors.ErrConversionFailed("Unknown unit: " + unit)
	}
	return unitData, nil
}

func (units *units) getByShortName(shortName string) (string, errors.SdkError) {
	longName, ok := units.shortNames[shortName]
	if !ok {
		return "", errors.ErrNoValueForKey(shortName)
	}

	return longName, nil
}

func (unit *staticUnit) convertToDimensionBase(value float64) float64 {
	return (value - unit.Offset) / unit.Scale
}

func (unit *staticUnit) convertFromDimensionBase(valueBase float64) float64 {
	return (valueBase * unit.Scale) + unit.Offset
}

func (units *units) GetConversionTable(tableData *dto.ConversionTable, resolution int) ioc.ConversionTable {
	table := &conversionTable{
		units:      units,
		resolution: resolution,
	}

	table.loadTable(tableData)

	return table
}

// Returns scale for given function such as convert(value)=1.
func (table *conversionTable) GetScale(functionName string, value float64) (float64, errors.SdkError) {
	resolution := (float64)(table.resolution)
	switch functionName {
	case "linear-resolution":
		return 1 / (value * resolution), nil
	case "linear":
		return 1 / value, nil
	case "reciprocal":
		return value, nil
	case "tangential-resolution":
		return 1 / (math.Tan(value*math.Pi/180) * resolution), nil
	default:
		return 0, errors.ErrInternal(fmt.Sprintf("Unknown conversion function: %s", functionName))
	}
}

func (table *conversionTable) loadTable(tableData *dto.ConversionTable) {
	table.source = tableData
	table.dimensionConversions = make(dimensionMap)
	table.dimensionConversionsInverse = make(dimensionMap)
	resolution := (float64)(table.resolution)

	for _, row := range tableData.Rows {
		var conversionFunc conversionFunction
		var conversionFuncInverse conversionFunction
		if row.Scale != nil {
			scale := *row.Scale

			switch row.FunctionName {
			case "linear-resolution":
				conversionFunc = func(value float64) float64 {
					return value * resolution * scale
				}
				conversionFuncInverse = func(value float64) float64 {
					return value / (resolution * scale)
				}
			case "linear":
				conversionFunc = func(value float64) float64 {
					return value * scale
				}
				conversionFuncInverse = func(value float64) float64 {
					return value / scale
				}
			case "reciprocal":
				conversionFunc = func(value float64) float64 {
					if value == 0 {
						return 0
					}
					return scale / value
				}
				conversionFuncInverse = func(value float64) float64 {
					if value == 0 {
						return 0
					}
					return scale / value
				}
			case "tangential-resolution":
				conversionFunc = func(value float64) float64 {
					return math.Tan(value*math.Pi/180) * resolution * scale
				}
				conversionFuncInverse = func(value float64) float64 {
					return math.Atan2(value, scale*resolution) * 180 / math.Pi
				}
			default:
				log.Print("Unknown function: ", row.FunctionName)
				continue
			}
		}

		ctxDimensionID := (ioc.ContextualDimensionID)(row.ContextualDimensionID)
		table.dimensionConversions[ctxDimensionID] = &conversionTableEntry{
			convert:                 conversionFunc,
			dimensionName:           row.DimensionName,
			contextualDimensionName: row.ContextualDimensionName,
		}
		table.dimensionConversionsInverse[ctxDimensionID] = &conversionTableEntry{
			convert:                 conversionFuncInverse,
			dimensionName:           row.DimensionName,
			contextualDimensionName: row.ContextualDimensionName,
		}
	}
}

func getDimensionMapEntry(dimensionMap dimensionMap, ctxDimensionID ioc.ContextualDimensionID, unitData *staticUnit) (*conversionTableEntry, errors.SdkError) {
	tableEntry, ok := dimensionMap[ctxDimensionID]
	if !ok {
		return nil, errors.ErrConversionFailed(fmt.Sprintf("Invalid dimension ID: %d.", ctxDimensionID))
	} else if unitData.DimensionName != tableEntry.dimensionName {
		return nil, errors.ErrConversionFailed(fmt.Sprintf("Invalid units: %s:%s. Provide units of dimension %s.",
			unitData.DimensionName, unitData.UnitLongName, tableEntry.dimensionName))
	} else if tableEntry.convert == nil {
		return nil, errors.ErrConversionFailed(fmt.Sprintf("Conversion not available for dimension: %s. Setup custom unit conversions.", tableEntry.dimensionName))
	}
	return tableEntry, nil
}

func (table *conversionTable) ConvertSettingValue(settingsTable ioc.SettingsTable, setting string, value float64, unit string) (float64, errors.SdkError) {
	if unit == constants.NativeUnit {
		return value, nil
	} else if paramInfo, found := settingsTable.GetParamInfo(setting); found {
		convertedValue, err := table.Convert(value, unit, paramInfo.ContextualDimensionID)
		if err != nil {
			return 0, err
		}

		paramTypeInfo, paramTypeInfoFound := generated.GetParamTypeInfo(string(paramInfo.ParamType))
		if !paramTypeInfoFound {
			return 0, errors.ErrInternal("Unit Conversion Error")
		}

		return utils.RoundNumber(convertedValue, paramTypeInfo.DecimalPlaces), nil

	}

	return 0, errors.ErrInternal("Unit Conversion Error")
}

func (table *conversionTable) Convert(value float64, unit string, ctxDimensionID ioc.ContextualDimensionID) (float64, errors.SdkError) {
	unitData, err := table.units.getUnitData(unit)
	if err != nil {
		return 0, err
	}
	tableEntry, err := getDimensionMapEntry(table.dimensionConversions, ctxDimensionID, unitData)
	if err != nil {
		return 0, err
	}
	valueBase := unitData.convertToDimensionBase(value)
	convertedValue := tableEntry.convert(valueBase)
	return convertedValue, nil
}

func (table *conversionTable) ConvertBack(value float64, unit string, ctxDimensionID ioc.ContextualDimensionID) (float64, errors.SdkError) {
	unitData, err := table.units.getUnitData(unit)
	if err != nil {
		return 0, err
	}
	tableEntry, err := getDimensionMapEntry(table.dimensionConversionsInverse, ctxDimensionID, unitData)
	if err != nil {
		return 0, err
	}
	valueBase := tableEntry.convert(value)
	convertedValue := unitData.convertFromDimensionBase(valueBase)
	return convertedValue, nil
}

func (table *conversionTable) GetDimensionName(ctxDimensionID ioc.ContextualDimensionID) (string, bool) {
	tableEntry, found := table.dimensionConversionsInverse[ctxDimensionID]
	if !found {
		return "", false
	}
	return tableEntry.dimensionName, true
}

func (table *conversionTable) GetContextualDimensionName(ctxDimensionID ioc.ContextualDimensionID) (string, bool) {
	tableEntry, found := table.dimensionConversionsInverse[ctxDimensionID]
	if !found {
		return "", false
	}
	return tableEntry.contextualDimensionName, true
}

func (table *conversionTable) GetContextualDimensionIDs() []ioc.ContextualDimensionID {
	keys := make([]ioc.ContextualDimensionID, 0, len(table.dimensionConversionsInverse))
	for k := range table.dimensionConversionsInverse {
		keys = append(keys, k)
	}
	return keys
}

func (unit *staticUnit) getKey() string {
	return fmt.Sprintf("%s:%s", unit.DimensionName, unit.UnitLongName)
}

func (units *units) ConvertStaticUnitToDimensionBase(value float64, unit string) (float64, errors.SdkError) {
	unitData, err := units.getUnitData(unit)
	if err != nil {
		return 0, err
	}
	valueBase := unitData.convertToDimensionBase(value)
	return valueBase, nil
}

func (units *units) ConvertStaticUnitFromDimensionBase(valueBase float64, unit string) (float64, errors.SdkError) {
	unitData, err := units.getUnitData(unit)
	if err != nil {
		return 0, err
	}
	value := unitData.convertFromDimensionBase(valueBase)
	return value, nil
}

func (units *units) ConvertStaticUnit(value float64, fromUnit string, toUnit string) (float64, errors.SdkError) {
	fromUnitData, err := units.getUnitData(fromUnit)
	if err != nil {
		return 0, err
	}
	toUnitData, err := units.getUnitData(toUnit)
	if err != nil {
		return 0, err
	}
	if fromUnitData.DimensionName != toUnitData.DimensionName {
		return 0, errors.ErrConversionFailed(fmt.Sprintf("Dimensions of unit %s and %s are not compatible.", fromUnit, toUnit))
	}
	newValue := toUnitData.convertFromDimensionBase(fromUnitData.convertToDimensionBase(value))
	return newValue, nil
}

func (units *units) AreUnitsCompatible(unit1 string, unit2 string) errors.SdkError {
	unitData1, err := units.getUnitData(unit1)
	if err != nil {
		return err
	}
	unitData2, err := units.getUnitData(unit2)
	if err != nil {
		return err
	}
	if unitData1.DimensionName != unitData2.DimensionName {
		return errors.ErrConversionFailed(fmt.Sprintf("Dimensions of unit %s and %s are not compatible.", unit1, unit2))
	}
	return nil
}

func (units *units) CheckDimension(unit string, dimension string) errors.SdkError {
	unitData, err := units.getUnitData(unit)
	if err != nil {
		return err
	}
	if unitData.DimensionName != dimension {
		return errors.ErrConversionFailed(fmt.Sprintf("Units %s do not belong to dimension %s.", unit, dimension))
	}
	return nil
}

func (units *units) GetBaseUnits(dimension string) (string, errors.SdkError) {
	baseUnits, found := units.dimensionBaseUnits[dimension]
	if !found {
		return "", errors.ErrConversionFailed(fmt.Sprintf("Cannot find base units for dimension %s.", dimension))
	}
	return baseUnits.getKey(), nil
}

func (units *units) GetDimensionName(unitsName string) (string, errors.SdkError) {
	unitData, err := units.getUnitData(unitsName)
	if err != nil {
		return "", err
	}
	return unitData.DimensionName, nil
}

func (table *conversionTable) GetBaseUnits(ctxDimensionID ioc.ContextualDimensionID) (string, errors.SdkError) {
	dimensionName, _ := table.GetDimensionName(ctxDimensionID)
	return table.units.GetBaseUnits(dimensionName)
}

func (table *conversionTable) CanConvert(ctxDimensionID ioc.ContextualDimensionID) bool {
	tableEntry1, ok1 := table.dimensionConversions[ctxDimensionID]
	tableEntry2, ok2 := table.dimensionConversionsInverse[ctxDimensionID]
	return ok1 && tableEntry1.convert != nil && ok2 && tableEntry2.convert != nil
}

func (table *conversionTable) GetSourceData() *dto.ConversionTable {
	return table.source
}
func (table *conversionTable) GetUnits() ioc.Units {
	return table.units
}
