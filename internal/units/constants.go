package units

//revive:disable:var-naming

const Units_lengthMm = "Length:millimetres"
const Units_lengthInch = "Length:inches"
const Units_angleDegrees = "Angle:degrees"
const Units_VelocityMmPerS = "Velocity:millimetres per second"
const Units_VelocityInchPerS = "Velocity:inches per second"
const Units_AngularVelocityDegreePerS = "Angular Velocity:degrees per second"
const Units_Volts = "Voltage:volts"
const Units_Celsius = "Absolute Temperature:degrees Celsius"
const Units_TimeMs = "Time:milliseconds"

const Dimension_Time = "Time"
const Dimension_Frequency = "Frequency"

//revive:enable:var-naming
