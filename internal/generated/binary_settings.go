package generated

type BinarySetting struct {
	Name       string
	GetCommand int32
	SetCommand int32
}

var settings []*BinarySetting

func init() {
	settings = []*BinarySetting{
		&BinarySetting{ Name: "Acceleration", GetCommand: 0, SetCommand: 43 },
		&BinarySetting{ Name: "Acceleration Only", GetCommand: 0, SetCommand: 113 },
		&BinarySetting{ Name: "Active Axis", GetCommand: 0, SetCommand: 25 },
		&BinarySetting{ Name: "Alias Number", GetCommand: 0, SetCommand: 48 },
		&BinarySetting{ Name: "Analog Input Count", GetCommand: 75, SetCommand: 0 },
		&BinarySetting{ Name: "Analog Output Count", GetCommand: 77, SetCommand: 0 },
		&BinarySetting{ Name: "Auto-Home Disabled Mode", GetCommand: 0, SetCommand: 105 },
		&BinarySetting{ Name: "Auto-Reply Disabled Mode", GetCommand: 0, SetCommand: 101 },
		&BinarySetting{ Name: "Axis Device Number", GetCommand: 0, SetCommand: 26 },
		&BinarySetting{ Name: "Axis Inversion", GetCommand: 0, SetCommand: 27 },
		&BinarySetting{ Name: "Axis Velocity Profile", GetCommand: 0, SetCommand: 28 },
		&BinarySetting{ Name: "Axis Velocity Scale", GetCommand: 0, SetCommand: 29 },
		&BinarySetting{ Name: "Baud Rate", GetCommand: 0, SetCommand: 122 },
		&BinarySetting{ Name: "Calibrated Encoder Count", GetCommand: 83, SetCommand: 0 },
		&BinarySetting{ Name: "Calibration Error", GetCommand: 85, SetCommand: 0 },
		&BinarySetting{ Name: "Calibration Type", GetCommand: 84, SetCommand: 0 },
		&BinarySetting{ Name: "Closed-Loop Mode", GetCommand: 0, SetCommand: 118 },
		&BinarySetting{ Name: "Current Position", GetCommand: 60, SetCommand: 45 },
		&BinarySetting{ Name: "Cycle Distance", GetCommand: 0, SetCommand: 80 },
		&BinarySetting{ Name: "Deceleration Only", GetCommand: 0, SetCommand: 114 },
		&BinarySetting{ Name: "Device Direction", GetCommand: 0, SetCommand: 121 },
		&BinarySetting{ Name: "Device ID", GetCommand: 50, SetCommand: 0 },
		&BinarySetting{ Name: "Device Mode", GetCommand: 0, SetCommand: 40 },
		&BinarySetting{ Name: "Digital Input Count", GetCommand: 67, SetCommand: 0 },
		&BinarySetting{ Name: "Digital Output Count", GetCommand: 70, SetCommand: 0 },
		&BinarySetting{ Name: "Encoder Count", GetCommand: 82, SetCommand: 0 },
		&BinarySetting{ Name: "Encoder Position", GetCommand: 89, SetCommand: 0 },
		&BinarySetting{ Name: "Filter Holder ID", GetCommand: 0, SetCommand: 81 },
		&BinarySetting{ Name: "Firmware Build", GetCommand: 56, SetCommand: 0 },
		&BinarySetting{ Name: "Firmware Version", GetCommand: 51, SetCommand: 0 },
		&BinarySetting{ Name: "Hold Current", GetCommand: 0, SetCommand: 39 },
		&BinarySetting{ Name: "Home Offset", GetCommand: 0, SetCommand: 47 },
		&BinarySetting{ Name: "Home Sensor Type", GetCommand: 0, SetCommand: 104 },
		&BinarySetting{ Name: "Home Speed", GetCommand: 0, SetCommand: 41 },
		&BinarySetting{ Name: "Home Status", GetCommand: 0, SetCommand: 103 },
		&BinarySetting{ Name: "Index Distance", GetCommand: 0, SetCommand: 79 },
		&BinarySetting{ Name: "Joystick Calibration Mode", GetCommand: 0, SetCommand: 33 },
		&BinarySetting{ Name: "Knob Direction", GetCommand: 0, SetCommand: 108 },
		&BinarySetting{ Name: "Knob Disabled Mode", GetCommand: 0, SetCommand: 107 },
		&BinarySetting{ Name: "Knob Jog Size", GetCommand: 0, SetCommand: 110 },
		&BinarySetting{ Name: "Knob Movement Mode", GetCommand: 0, SetCommand: 109 },
		&BinarySetting{ Name: "Knob Velocity Profile", GetCommand: 0, SetCommand: 112 },
		&BinarySetting{ Name: "Knob Velocity Scale", GetCommand: 0, SetCommand: 111 },
		&BinarySetting{ Name: "Lock State", GetCommand: 0, SetCommand: 49 },
		&BinarySetting{ Name: "Manual Move Tracking Disabled Mode", GetCommand: 0, SetCommand: 116 },
		&BinarySetting{ Name: "Maximum Position", GetCommand: 0, SetCommand: 44 },
		&BinarySetting{ Name: "Maximum Relative Move", GetCommand: 0, SetCommand: 46 },
		&BinarySetting{ Name: "Message ID Mode", GetCommand: 0, SetCommand: 102 },
		&BinarySetting{ Name: "Microstep Resolution", GetCommand: 0, SetCommand: 37 },
		&BinarySetting{ Name: "Minimum Position", GetCommand: 0, SetCommand: 106 },
		&BinarySetting{ Name: "Move Tracking Mode", GetCommand: 0, SetCommand: 115 },
		&BinarySetting{ Name: "Move Tracking Period", GetCommand: 0, SetCommand: 117 },
		&BinarySetting{ Name: "Park State", GetCommand: 0, SetCommand: 65 },
		&BinarySetting{ Name: "Peripheral ID", GetCommand: 0, SetCommand: 66 },
		&BinarySetting{ Name: "Peripheral ID Pending", GetCommand: 91, SetCommand: 0 },
		&BinarySetting{ Name: "Peripheral Serial Number", GetCommand: 86, SetCommand: 86 },
		&BinarySetting{ Name: "Peripheral Serial Pending", GetCommand: 92, SetCommand: 0 },
		&BinarySetting{ Name: "Power Supply Voltage", GetCommand: 52, SetCommand: 0 },
		&BinarySetting{ Name: "Protocol", GetCommand: 0, SetCommand: 123 },
		&BinarySetting{ Name: "Running Current", GetCommand: 0, SetCommand: 38 },
		&BinarySetting{ Name: "Serial Number", GetCommand: 63, SetCommand: 0 },
		&BinarySetting{ Name: "Slip Tracking Period", GetCommand: 0, SetCommand: 119 },
		&BinarySetting{ Name: "Stall Timeout", GetCommand: 0, SetCommand: 120 },
		&BinarySetting{ Name: "Status", GetCommand: 54, SetCommand: 0 },
		&BinarySetting{ Name: "Target Speed", GetCommand: 0, SetCommand: 42 },
	}
}

func GetSetting(index int32) *BinarySetting {
	return settings[index]
}

func GetSettingByCommand(command int32) (*BinarySetting, bool) {
	for _, setting := range settings {
		if command == setting.GetCommand || command == setting.SetCommand {
			return setting, true
		}
	}
	return nil, false
}
