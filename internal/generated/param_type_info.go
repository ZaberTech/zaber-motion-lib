package generated

import (
	"regexp"
	"strconv"
)

type ParamTypeInfo struct {
	Name          string
	DecimalPlaces int
}

var intRegex, _ = regexp.Compile(`^(Ui|I)nt(8|16|32|64|96)?DP(\d+)$`)

var paramTypesInfo = map[string]ParamTypeInfo{
	"Bool":        ParamTypeInfo{"Bool", 0},
	"Enum":        ParamTypeInfo{"Enum", 0},
	"IPv4Address": ParamTypeInfo{"IPv4Address", 0},
	"IPv4Mask":    ParamTypeInfo{"IPv4Mask", 0},
	"Int":         ParamTypeInfo{"Int", 0},
	"Int16":       ParamTypeInfo{"Int16", 0},
	"Int16DP1":    ParamTypeInfo{"Int16DP1", 1},
	"Int16DP3":    ParamTypeInfo{"Int16DP3", 3},
	"Int32":       ParamTypeInfo{"Int32", 0},
	"Int32DP2":    ParamTypeInfo{"Int32DP2", 2},
	"Int32DP3":    ParamTypeInfo{"Int32DP3", 3},
	"Int32DP6":    ParamTypeInfo{"Int32DP6", 6},
	"Int64":       ParamTypeInfo{"Int64", 0},
	"Int64DP9":    ParamTypeInfo{"Int64DP9", 9},
	"Int8":        ParamTypeInfo{"Int8", 0},
	"IntDP1":      ParamTypeInfo{"IntDP1", 1},
	"MACAddress":  ParamTypeInfo{"MACAddress", 0},
	"Tenths":      ParamTypeInfo{"Tenths", 1},
	"Token":       ParamTypeInfo{"Token", 0},
	"UTenths":     ParamTypeInfo{"UTenths", 1},
	"Uhalves32":   ParamTypeInfo{"Uhalves32", 1},
	"Uint":        ParamTypeInfo{"Uint", 0},
	"Uint16":      ParamTypeInfo{"Uint16", 0},
	"Uint16DP1":   ParamTypeInfo{"Uint16DP1", 1},
	"Uint16DP2":   ParamTypeInfo{"Uint16DP2", 2},
	"Uint16DP3":   ParamTypeInfo{"Uint16DP3", 3},
	"Uint32":      ParamTypeInfo{"Uint32", 0},
	"Uint32DP1":   ParamTypeInfo{"Uint32DP1", 1},
	"Uint32DP3":   ParamTypeInfo{"Uint32DP3", 3},
	"Uint32DP6":   ParamTypeInfo{"Uint32DP6", 6},
	"Uint64":      ParamTypeInfo{"Uint64", 0},
	"Uint64DP1":   ParamTypeInfo{"Uint64DP1", 1},
	"Uint64DP6":   ParamTypeInfo{"Uint64DP6", 6},
	"Uint64DP9":   ParamTypeInfo{"Uint64DP9", 9},
	"Uint8":       ParamTypeInfo{"Uint8", 0},
	"UintDP1":     ParamTypeInfo{"UintDP1", 1},
	"Utenths":     ParamTypeInfo{"Utenths", 1},
}

func GetParamTypeInfo(paramName string) (ParamTypeInfo, bool) {
	info, ok := paramTypesInfo[paramName]
	if !ok {
		if match := intRegex.FindStringSubmatch(paramName); match != nil {
			decimalPlaces, _ := strconv.Atoi(match[3])
			info := ParamTypeInfo{paramName, decimalPlaces}
			paramTypesInfo[paramName] = info
			return info, true
		}
	}
	return info, ok
}
