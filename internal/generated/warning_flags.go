﻿package generated

var WarningFlags map[string]string

func init() {
	WarningFlags = map[string]string{
		"FF": "Critical System Error",
		"FN": "Peripheral Not Supported",
		"FZ": "Peripheral Inactive",
		"FH": "Hardware Emergency Stop Driver Disabled",
		"FV": "Overvoltage or Undervoltage Driver Disabled",
		"FO": "Driver Disabled on Startup or by Command",
		"FC": "Current Inrush Error",
		"FM": "Motor Temperature Error",
		"FD": "Driver Disabled",
		"FQ": "Encoder Error",
		"FI": "Index Error",
		"FA": "Analog Encoder Sync Error",
		"FR": "Overdrive Limit Exceeded",
		"FS": "Stalled and Stopped",
		"FB": "Stream Bounds Error",
		"FP": "Interpolated Path Deviation",
		"FE": "Limit Error",
		"FT": "Excessive Twist",
		"WL": "Unexpected Limit Trigger",
		"WV": "Voltage Out of Range",
		"WT": "Controller Temperature High",
		"WS": "Stalled with Recovery",
		"WM": "Displaced When Stationary",
		"WP": "Invalid Calibration Type",
		"WR": "No Reference Position",
		"WH": "Device Not Homed",
		"NC": "Manual Control",
		"NI": "Movement Interrupted",
		"ND": "Stream Discontinuity",
		"NR": "Value Rounded",
		"NT": "Value Truncated",
		"NU": "Setting Update Pending",
		"NJ": "Joystick Calibrating",
		"NB": "Device in Firmware Update Mode",
	}
}
