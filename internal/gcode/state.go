package gcode

type axisState struct {
	position                 float64
	homePosition             float64
	secondaryHomePosition    float64
	unit                     string
	positionInitialized      bool
	coordinateSystemsOffsets [coordinateSystemsCount]float64
}

type translatorState struct {
	modes                    [ModalGroupEnumSize]Word
	feedRateNative           float64
	feedRateOverride         float64
	traverseRateNative       int64
	currentSpeedNative       int64
	segmentBuffer            []interface{}
	smoothingDeviationNative float64
	helixEnabled             bool
}

func newState() *translatorState {
	state := &translatorState{
		feedRateOverride: 1,
		helixEnabled:     true,
	}
	for _, group := range modalGroups {
		if group.DefaultValue != emptyWord {
			state.modes[group.Group] = group.DefaultValue
		}
	}
	return state
}

func (state *translatorState) copyTo(to *translatorState) {
	segmentBuffer := to.segmentBuffer
	*to = *state
	to.segmentBuffer = append(segmentBuffer[:0], state.segmentBuffer...)
}

func (state *translatorState) changeMode(word Word) {
	groups := wordToModal[word]
	for _, group := range groups {
		state.modes[group] = word
	}
}

func (state *translatorState) getModalValue(group ModalGroupEnum) Word {
	return state.modes[group]
}

func (state *translatorState) isRelativeDistanceMode() bool {
	mode := state.modes[ModalGroup_DistanceMode]
	return mode == Word{'G', 91}
}

func (state *translatorState) isRelativeArcMode() bool {
	mode := state.modes[ModalGroup_ArcDistanceMode]
	return mode == Word{'G', 91.1}
}

func newAxisState(unit string) *axisState {
	return &axisState{
		unit: unit,
	}
}

func (state *axisState) copyTo(to *axisState) {
	*to = *state
}
