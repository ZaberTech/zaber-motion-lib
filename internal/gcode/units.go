package gcode

import (
	"fmt"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/units"
)

func getDefaultUnits(dimension string) (string, errors.SdkError) {
	switch dimension {
	case "":
		return constants.NativeUnit, nil
	case "Length":
		return units.Units_lengthMm, nil
	case "Angle":
		return units.Units_angleDegrees, nil
	}
	return "", errors.ErrInternal(fmt.Sprintf("Missing default units for dimension %s", dimension))
}
func getVelocityUnits(travelUnits string) (string, errors.SdkError) {
	switch travelUnits {
	case "":
		return constants.NativeUnit, nil
	case units.Units_lengthMm:
		return units.Units_VelocityMmPerS, nil
	case units.Units_lengthInch:
		return units.Units_VelocityInchPerS, nil
	case units.Units_angleDegrees:
		return units.Units_AngularVelocityDegreePerS, nil
	}
	return "", errors.ErrInternal(fmt.Sprintf("Missing velocity units for %s", travelUnits))
}
