package gcode

import (
	"fmt"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"
)

var emptyWord = Word{}

const smallestDistance = 0.01

type WordSet = map[Word]struct{}

type ModalGroupEnum = int

//revive:disable:var-naming

const (
	ModalGroup_None ModalGroupEnum = iota
	ModalGroup_Motion
	ModalGroup_PlaneSelection
	ModalGroup_DistanceMode
	ModalGroup_ArcDistanceMode
	ModalGroup_FeedRateMode
	ModalGroup_Units
	ModalGroup_CutterRadiusCompensation
	ModalGroup_ToolLengthOffset
	ModalGroup_ReturnModeInCannedCycles
	ModalGroup_CoordinateSystemSelection
	ModalGroup_PathControlMode
	ModalGroup_Stopping
	ModalGroup_ToolChange
	ModalGroup_SpindleTurning
	ModalGroup_FloodCoolant
	ModalGroup_MistCoolant
	ModalGroup_FeedAndSpeedOverrideSwitches
	ModalGroup_CodesWithCoordinates
	ModalGroupEnumSize
)

//revive:enable:var-naming

type ModalGroup struct {
	Group        ModalGroupEnum
	Name         string
	DefaultValue Word
	Words        WordSet
}

// From: https://www.nist.gov/publications/nist-rs274ngc-interpreter-version-3?pub_id=823374

var modalGroups = map[ModalGroupEnum]*ModalGroup{
	ModalGroup_Motion: {
		Name:         "Motion",
		Group:        ModalGroup_Motion,
		DefaultValue: Word{'G', 0},
		Words: WordSet{
			Word{'G', 0}:    {},
			Word{'G', 1}:    {},
			Word{'G', 2}:    {},
			Word{'G', 3}:    {},
			Word{'G', 38.2}: {},
			Word{'G', 80}:   {},
			Word{'G', 81}:   {},
			Word{'G', 82}:   {},
			Word{'G', 83}:   {},
			Word{'G', 84}:   {},
			Word{'G', 85}:   {},
			Word{'G', 86}:   {},
			Word{'G', 87}:   {},
			Word{'G', 88}:   {},
			Word{'G', 88}:   {},
			Word{'G', 89}:   {},
		},
	},
	ModalGroup_PlaneSelection: {
		Name:         "Plane Selection",
		Group:        ModalGroup_PlaneSelection,
		DefaultValue: Word{'G', 17},
		Words: WordSet{
			Word{'G', 17}: {},
			Word{'G', 18}: {},
			Word{'G', 19}: {},
		},
	},
	ModalGroup_DistanceMode: {
		Name:         "Distance Mode",
		Group:        ModalGroup_DistanceMode,
		DefaultValue: Word{'G', 91},
		Words: WordSet{
			Word{'G', 90}: {},
			Word{'G', 91}: {},
		},
	},
	ModalGroup_ArcDistanceMode: {
		Name:         "Arc Distance Mode",
		Group:        ModalGroup_ArcDistanceMode,
		DefaultValue: Word{'G', 91.1},
		Words: WordSet{
			Word{'G', 90.1}: {},
			Word{'G', 91.1}: {},
		},
	},
	ModalGroup_FeedRateMode: {
		Name:         "Feed Rate Mode",
		Group:        ModalGroup_FeedRateMode,
		DefaultValue: Word{'G', 94},
		Words: WordSet{
			Word{'G', 93}: {},
			Word{'G', 94}: {},
			Word{'G', 95}: {},
		},
	},
	ModalGroup_Units: {
		Name:         "Units",
		Group:        ModalGroup_Units,
		DefaultValue: Word{'G', 21},
		Words: WordSet{
			Word{'G', 20}: {},
			Word{'G', 21}: {},
		},
	},
	ModalGroup_CutterRadiusCompensation: {
		Name:         "Curret Radius Compensation",
		Group:        ModalGroup_CutterRadiusCompensation,
		DefaultValue: Word{'G', 40},
		Words: WordSet{
			Word{'G', 40}: {},
			Word{'G', 41}: {},
			Word{'G', 42}: {},
		},
	},
	ModalGroup_ToolLengthOffset: {
		Name:         "Tool Length Offset",
		Group:        ModalGroup_ToolLengthOffset,
		DefaultValue: Word{'G', 49},
		Words: WordSet{
			Word{'G', 43}: {},
			Word{'G', 44}: {},
			Word{'G', 49}: {},
		},
	},
	ModalGroup_ReturnModeInCannedCycles: {
		Name:         "Return Mode in Canned Cycles",
		Group:        ModalGroup_ReturnModeInCannedCycles,
		DefaultValue: Word{'G', 99},
		Words: WordSet{
			Word{'G', 98}: {},
			Word{'G', 99}: {},
		},
	},
	ModalGroup_CoordinateSystemSelection: {
		Name:         "Coordinate System Selection",
		Group:        ModalGroup_CoordinateSystemSelection,
		DefaultValue: Word{'G', 54},
		Words: WordSet{
			Word{'G', 54}:   {},
			Word{'G', 55}:   {},
			Word{'G', 56}:   {},
			Word{'G', 57}:   {},
			Word{'G', 58}:   {},
			Word{'G', 59}:   {},
			Word{'G', 59.1}: {},
			Word{'G', 59.2}: {},
			Word{'G', 59.3}: {},
			Word{'G', 54.1}: {},
		},
	},
	ModalGroup_PathControlMode: {
		Name:         "Path Control Mode",
		Group:        ModalGroup_PathControlMode,
		DefaultValue: Word{'G', 64},
		Words: WordSet{
			Word{'G', 61}:   {},
			Word{'G', 61.1}: {},
			Word{'G', 64}:   {},
		},
	},
	ModalGroup_Stopping: {
		Name:  "Stopping",
		Group: ModalGroup_Stopping,
		Words: WordSet{
			Word{'M', 0}:  {},
			Word{'M', 1}:  {},
			Word{'M', 2}:  {},
			Word{'M', 30}: {},
			Word{'M', 60}: {},
		},
	},
	ModalGroup_ToolChange: {
		Name:  "Tool Change",
		Group: ModalGroup_ToolChange,
		Words: WordSet{
			Word{'M', 6}: {},
		},
	},
	ModalGroup_SpindleTurning: {
		Name:         "Spindle Turning",
		Group:        ModalGroup_SpindleTurning,
		DefaultValue: Word{'M', 5},
		Words: WordSet{
			Word{'M', 3}: {},
			Word{'M', 4}: {},
			Word{'M', 5}: {},
		},
	},
	ModalGroup_FloodCoolant: {
		Name:         "Flood Coolant",
		Group:        ModalGroup_FloodCoolant,
		DefaultValue: Word{'M', 9},
		Words: WordSet{
			Word{'M', 7}: {},
			Word{'M', 9}: {},
		},
	},
	ModalGroup_MistCoolant: {
		Name:         "Mist Coolant",
		Group:        ModalGroup_MistCoolant,
		DefaultValue: Word{'M', 9},
		Words: WordSet{
			Word{'M', 8}: {},
			Word{'M', 9}: {},
		},
	},
	ModalGroup_FeedAndSpeedOverrideSwitches: {
		Name:         "Feed and Speed Override Switches",
		Group:        ModalGroup_FeedAndSpeedOverrideSwitches,
		DefaultValue: Word{'M', 49},
		Words: WordSet{
			Word{'M', 48}: {},
			Word{'M', 49}: {},
		},
	},
	ModalGroup_CodesWithCoordinates: {
		Name:  "Non-Modal Codes with Coordinates",
		Group: ModalGroup_CodesWithCoordinates,
		Words: WordSet{
			Word{'G', 10}:   {},
			Word{'G', 28}:   {},
			Word{'G', 30}:   {},
			Word{'G', 92}:   {},
			Word{'G', 92.1}: {},
			Word{'G', 92.2}: {},
			Word{'G', 92.3}: {},
		},
	},
}

var wordToModal map[Word][]ModalGroupEnum

func init() {
	wordToModal = make(map[Word][]ModalGroupEnum)
	for _, group := range modalGroups {
		for word := range group.Words {
			wordToModal[word] = append(wordToModal[word], group.Group)
		}
	}
}

var movementTriggers = []rune{
	'X', 'Y', 'Z',
	'A', 'B', 'C',
	'I', 'J', 'K',
}

func triggersMovement(word Word) bool {
	return utils.SliceIndex(len(movementTriggers), func(i int) bool { return movementTriggers[i] == word.Letter }) >= 0
}

const coordinateSystemsCount = 10
const g92MachineCoordOffsetIndex = 9

func getCoordinateSystemIndex(word Word) (int, errors.SdkError) {
	if 54 <= word.Value && word.Value <= 59 {
		return (int)(word.Value - 54), nil
	} else if word.Value == 59.1 {
		return 6, nil
	} else if word.Value == 59.2 {
		return 7, nil
	} else if word.Value == 59.3 {
		return 8, nil
	} else if word.Value == 92 {
		return g92MachineCoordOffsetIndex, nil
	}
	return 0, errors.ErrInvalidArgument(fmt.Sprintf("Invalid coordinate system %s.", word))
}
