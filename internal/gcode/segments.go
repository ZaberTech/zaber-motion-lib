package gcode

import (
	"fmt"
	"math"
	"strings"
	"zaber-motion-lib/internal/utils"

	"github.com/go-gl/mathgl/mgl64"
)

type lineSegment struct {
	axes        []int
	start       []float64
	end         []float64
	maxspeed    int64
	noSmoothing bool
	stop        bool
	join        bool
}

type arcSegment struct {
	axes      []int
	center    []float64
	end       []float64
	clockwise bool
	maxspeed  int64
	stop      bool
	join      bool
}

type waitSegment struct {
	time float64
}

type passthroughSegment struct {
	command string
}

// TODO: decrease or drop the limits once FW fixes the tolerance
const minArcRadius = 10.0

var minOptimizableAngleCos = math.Cos(math.Pi * 0.001) // 0.18°

type twoLineInfo struct {
	axes     []int
	start    mgl64.Vec2
	midpoint mgl64.Vec2
	end      mgl64.Vec2
}

func getUnifiedAxisInfo(line1, line2 *lineSegment) *twoLineInfo {
	// axes are guaranteed to be in fixed order
	info := &twoLineInfo{
		axes: make([]int, 2),
	}

	if len(line1.axes) == 2 && len(line2.axes) == 2 {
		copy(info.axes, line1.axes)
		copy(info.start[:], line1.start)
		copy(info.midpoint[:], line1.end)
		copy(info.end[:], line2.end)
	} else if len(line1.axes) == 2 {
		if line1.axes[0] == line2.axes[0] {
			info.end[0] = line2.end[0]
			info.end[1] = line1.end[1]
		} else {
			info.end[0] = line1.end[0]
			info.end[1] = line2.end[0]
		}
		copy(info.axes, line1.axes)
		copy(info.start[:], line1.start)
		copy(info.midpoint[:], line1.end)
	} else if len(line2.axes) == 2 {
		if line1.axes[0] == line2.axes[0] {
			info.start[0] = line1.start[0]
			info.start[1] = line2.start[1]
		} else {
			info.start[0] = line2.start[0]
			info.start[1] = line1.start[0]
		}
		copy(info.axes, line2.axes)
		copy(info.midpoint[:], line2.start)
		copy(info.end[:], line2.end)
	} else {
		info.axes[0] = line1.axes[0]
		info.axes[1] = line2.axes[0]
		info.start[0] = line1.start[0]
		info.start[1] = line2.start[0]
		info.midpoint[0] = line1.end[0]
		info.midpoint[1] = line2.start[0]
		info.end[0] = line1.end[0]
		info.end[1] = line2.end[0]
	}

	// the axes must be in order for cross product
	if (info.axes[0] > info.axes[1]) {
		info.axes[0], info.axes[1] = info.axes[1], info.axes[0]
		info.start[0], info.start[1] = info.start[1], info.start[0]
		info.midpoint[0], info.midpoint[1] = info.midpoint[1], info.midpoint[0]
		info.end[0], info.end[1] = info.end[1], info.end[0]
	}

	return info
}

func smoothLines(line1, line2 *lineSegment, smoothingDeviation float64) ([]interface{}, bool) {
	maxAxesCount := utils.MaxInt(len(line1.axes), len(line2.axes))

	if maxAxesCount > 2 ||
		line1.stop || line1.noSmoothing || line2.noSmoothing ||
		len(line1.start) == 0 || len(line2.start) == 0 {
		return nil, false
	}

	if axesCompatible := (maxAxesCount == 2 &&
		(utils.IsSubsetIntSlice(line1.axes, line2.axes) || utils.IsSubsetIntSlice(line2.axes, line1.axes))) ||
		(maxAxesCount == 1 && line1.axes[0] != line2.axes[0]); !axesCompatible {
		return nil, false
	}

	info := getUnifiedAxisInfo(line1, line2)

	dir1 := info.midpoint.Sub(info.start)
	line1Len := dir1.Len()
	dir1 = dir1.Normalize()
	dir1Reversed := dir1.Mul(-1)

	dir2 := info.end.Sub(info.midpoint)
	line2Len := dir2.Len()
	dir2 = dir2.Normalize()

	if isBelowDeviceResolutionVec2(info.start, info.midpoint) ||
		isBelowDeviceResolutionVec2(info.midpoint, info.end) {
		return nil, false
	}

	if areColinear := math.Abs(dir1.Dot(dir2)) > minOptimizableAngleCos; areColinear {
		return nil, false
	}

	cornerAngle := math.Acos(dir2.Dot(dir1Reversed))
	cornerAngleHalf := cornerAngle / 2

	orthoDistanceAtDeviation := math.Sin(cornerAngleHalf) * smoothingDeviation
	cornerRadius := orthoDistanceAtDeviation / (1 - math.Sin(cornerAngleHalf))
	if cornerRadius < minArcRadius {
		return nil, false
	}

	centerDistance := cornerRadius + smoothingDeviation
	orthoDistanceAtCenter := math.Cos(cornerAngleHalf) * centerDistance
	maxOrthoDistanceAtCenter := math.Min(line1Len, line2Len/2)

	if orthoDistanceAtCenter > maxOrthoDistanceAtCenter {
		orthoDistanceAtCenter = maxOrthoDistanceAtCenter
		centerDistance = orthoDistanceAtCenter / math.Cos(cornerAngleHalf)
		cornerRadius = math.Sin(cornerAngleHalf) * centerDistance

		if cornerRadius < minArcRadius {
			return nil, false
		}
	}

	centripetalDir := dir2.Sub(dir1).Normalize()
	center := info.midpoint.Add(centripetalDir.Mul(centerDistance))

	midArc := center.Add(centripetalDir.Mul(-cornerRadius))
	splitArc := line1.maxspeed != line2.maxspeed

	line1NewEnd := info.midpoint.Add(dir1Reversed.Mul(orthoDistanceAtCenter))
	line2NewStart := info.midpoint.Add(dir2.Mul(orthoDistanceAtCenter))

	if splitArc {
		if !isRoundedArcInTolerance(center, line1NewEnd, midArc) {
			return nil, false
		}
		if !isRoundedArcInTolerance(center, midArc, line2NewStart) {
			return nil, false
		}
		if isBelowDeviceResolutionVec2(line1NewEnd, midArc) {
			return nil, false
		}
		if isBelowDeviceResolutionVec2(midArc, line2NewStart) {
			return nil, false
		}
	} else {
		if !isRoundedArcInTolerance(center, line1NewEnd, line2NewStart) {
			return nil, false
		}
		if isBelowDeviceResolutionVec2(line1NewEnd, line2NewStart) {
			return nil, false
		}
	}

	line1.setFrom(info.axes, line1NewEnd[:], false)
	line2.setFrom(info.axes, line2NewStart[:], true)
	line2.join = true

	angleSign := math.Signbit(dir1Reversed[0]*dir2[1] - dir1Reversed[1]*dir2[0])

	newSegments := make([]interface{}, 0, 4)
	if addLine1 := isAboveDeviceResolutionVec2(info.start, line1NewEnd); addLine1 {
		newSegments = append(newSegments, line1)
	}
	if splitArc {
		newSegments = append(newSegments, &arcSegment{
			axes:      info.axes,
			center:    center[:],
			end:       midArc[:],
			maxspeed:  line1.maxspeed,
			clockwise: !angleSign,
			join:      true,
		})
	}
	newSegments = append(newSegments, &arcSegment{
		axes:      info.axes,
		center:    center[:],
		end:       line2NewStart[:],
		maxspeed:  line2.maxspeed,
		clockwise: !angleSign,
		join:      true,
	})
	newSegments = append(newSegments, line2)

	if arc, line1Omitted := newSegments[0].(*arcSegment); line1Omitted {
		arc.join = line1.join // if line1 is omitted the first arc inherits the joining
	}

	return newSegments, true
}

const fwArcTolerance = 0.01

func isRoundedArcInTolerance(center, p1, p2 mgl64.Vec2) bool {
	p1 = roundVec2(p1)
	p2 = roundVec2(p2)
	center = roundVec2(center)
	dist1 := p1.Sub(center).Len()
	dist2 := p2.Sub(center).Len()
	return math.Abs(dist1-dist2)/math.Min(dist1, dist2) < fwArcTolerance
}

func (instance *translator) processSegments(flush bool) []string {
	state := instance.state

	var commands []string
	for len(state.segmentBuffer) > 0 {
		if wait, ok := state.segmentBuffer[0].(*waitSegment); ok {
			commands = append(commands, fmt.Sprintf("wait %s", formatIntValue(wait.time)))
			state.segmentBuffer = state.segmentBuffer[1:]
		} else if arc, ok := state.segmentBuffer[0].(*arcSegment); ok {
			if state.currentSpeedNative != arc.maxspeed {
				state.currentSpeedNative = arc.maxspeed
				commands = append(commands, fmt.Sprintf("set maxspeed %d", state.currentSpeedNative))
			}

			axesStr := makeAxisString(arc.axes, len(instance.axes))
			centerAndPosition := []string{formatIntValue(arc.center[0]), formatIntValue(arc.center[1])}
			for i := range arc.axes {
				centerAndPosition = append(centerAndPosition, formatIntValue(arc.end[i]))
			}

			arcWord := "arc"
			if len(arc.axes) > 2 {
				arcWord = "helix"
			}

			direction := "cw"
			if !arc.clockwise {
				direction = "ccw"
			}

			if arc.join {
				centerAndPosition = append(centerAndPosition, fmt.Sprintf("join maxspeed %d", state.currentSpeedNative))
			}

			commands = append(commands, strings.Join(utils.FilterEmpty(
				append([]string{axesStr, arcWord, "abs", direction}, centerAndPosition...),
			), " "))
			state.segmentBuffer = state.segmentBuffer[1:]

			if arc.stop {
				commands = append(commands, "wait 1")
			}
		} else if line, ok := state.segmentBuffer[0].(*lineSegment); ok {
			if (state.modes[ModalGroup_PathControlMode] == Word{'G', 64}) {
				if shouldBuffer := len(state.segmentBuffer) == 1 && !line.noSmoothing && !line.stop && !flush; shouldBuffer {
					break
				} else if len(state.segmentBuffer) > 1 {
					if nextLine, ok := state.segmentBuffer[1].(*lineSegment); ok {
						if newSegments, success := smoothLines(line, nextLine, state.smoothingDeviationNative); success {
							state.segmentBuffer = append(newSegments, state.segmentBuffer[2:]...)
							continue
						}
					}
				}
			}

			if state.currentSpeedNative != line.maxspeed {
				state.currentSpeedNative = line.maxspeed
				commands = append(commands, fmt.Sprintf("set maxspeed %d", state.currentSpeedNative))
			}

			axesStr := makeAxisString(line.axes, len(instance.axes))
			position := make([]string, len(line.axes))
			for i := range line.axes {
				position[i] = formatIntValue(line.end[i])
			}

			if line.join {
				position = append(position, fmt.Sprintf("join maxspeed %d", state.currentSpeedNative))
			}

			commands = append(commands, strings.Join(utils.FilterEmpty(
				append([]string{axesStr, "line", "abs"}, position...),
			), " "))
			state.segmentBuffer = state.segmentBuffer[1:]

			if line.stop {
				commands = append(commands, "wait 1")
			}
		} else if passthrough, ok := state.segmentBuffer[0].(*passthroughSegment); ok {
			commands = append(commands, passthrough.command)
			state.segmentBuffer = state.segmentBuffer[1:]
		}
	}

	return commands
}

func makeAxisString(axes []int, streamAxisCount int) string {
	if len(axes) == streamAxisCount {
		isInOrder := true
		for i, axis := range axes {
			if i == 0 {
				isInOrder = isInOrder && axis == 0
			} else {
				isInOrder = isInOrder && axis == axes[i-1]+1
			}
		}
		if isInOrder {
			return ""
		}
	}

	axesStr := []string{"on"}
	for _, axis := range axes {
		axesStr = append(axesStr, streamAxisLetters[axis])
	}
	return strings.Join(axesStr, " ")
}

func (segment *lineSegment) setFrom(axes []int, data []float64, startOrEnd bool) {
	for dataIndex, axis := range axes {
		if axisIndex := utils.SliceIndex(len(segment.axes), func(i int) bool {
			return segment.axes[i] == axis
		}); axisIndex >= 0 {
			if startOrEnd {
				segment.start[axisIndex] = data[dataIndex]
			} else {
				segment.end[axisIndex] = data[dataIndex]
			}
		}
	}
}

func roundVec2(v mgl64.Vec2) mgl64.Vec2 {
	return mgl64.Vec2{math.Round(v[0]), math.Round(v[1])}
}

func isBelowDeviceResolutionVec2(from mgl64.Vec2, to mgl64.Vec2) bool {
	return isBelowDeviceResolution(from[:], to[:])
}
func isAboveDeviceResolutionVec2(from mgl64.Vec2, to mgl64.Vec2) bool {
	return !isBelowDeviceResolution(from[:], to[:])
}

func isBelowDeviceResolution(from []float64, to []float64) bool {
	for i := 0; i < len(from); i++ {
		if math.Round(from[i]) != math.Round(to[i]) {
			return false
		}
	}
	return true
}
