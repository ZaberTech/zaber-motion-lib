package gcode

import (
	"fmt"
	"math"
	"zaber-motion-lib/internal/commandbuilding"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/utils"

	"github.com/go-gl/mathgl/mgl64"
)

// TODO: limit "on a b c" to only when needed

const minDwellTimeMs = 1
const defaultSmoothingDeviation = 0.1

var streamAxisLetters = []string{"a", "b", "c", "d", "e", "f"}

type commandFunc = func(word *parsedWord, block *block, translator *translator) errors.SdkError

var commandsByWord = map[Word]commandFunc{
	{'G', 0}: G0,
	{'G', 1}: G1,
	{'G', 2}: G2,
	{'G', 3}: G3,
	{'G', 4}: G4,

	{'G', 9}: noop,

	{'G', 10}: G10,

	{'G', 17}: addModalStateChangeCallback(ModalGroup_PlaneSelection),
	{'G', 18}: addModalStateChangeCallback(ModalGroup_PlaneSelection),
	{'G', 19}: addModalStateChangeCallback(ModalGroup_PlaneSelection),

	{'G', 20}: addErrCallback("Inches are not supported."),
	{'G', 21}: addModalStateChangeCallback(ModalGroup_Units),

	{'G', 28}:   G28G30,
	{'G', 28.1}: G281G301,
	{'G', 30}:   G28G30,
	{'G', 30.1}: G281G301,

	{'G', 40}: addWarningCallback("Tools radius compensation is not supported."),
	{'G', 41}: addErrCallback("Tools radius compensation is not supported."),
	{'G', 42}: addErrCallback("Tools radius compensation is not supported."),

	{'G', 43}: addWarningCallback("Tool length compensation is not supported."),
	{'G', 44}: addWarningCallback("Tool length compensation is not supported."),
	{'G', 49}: addWarningCallback("Tool length compensation is not supported."),

	{'G', 53}:   noop,
	{'G', 54}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 54.1}: addErrCallback("Only 9 coordinate systems are currently supported."),
	{'G', 55}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 56}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 57}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 58}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 59}:   addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 59.1}: addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 59.2}: addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),
	{'G', 59.3}: addModalStateChangeCallback(ModalGroup_CoordinateSystemSelection),

	{'G', 61}:   addModalStateChangeCallback(ModalGroup_PathControlMode),
	{'G', 61.1}: addModalStateChangeCallback(ModalGroup_PathControlMode),
	{'G', 64}:   G64,

	{'G', 90}:   addModalStateChangeCallback(ModalGroup_DistanceMode),
	{'G', 90.1}: addModalStateChangeCallback(ModalGroup_ArcDistanceMode),
	{'G', 91}:   addModalStateChangeCallback(ModalGroup_DistanceMode),
	{'G', 91.1}: addModalStateChangeCallback(ModalGroup_ArcDistanceMode),

	{'G', 92}:   G92,
	{'G', 92.1}: G92,
	{'G', 92.2}: G92,

	{'G', 93}: addErrCallback("Inverse time feed rate mode is not supported."),
	{'G', 95}: addErrCallback("Feed rate per revolution is not supported."),

	{'M', 2}:  M2M30,
	{'M', 30}: M2M30,

	{'M', 64}: M64M65,
	{'M', 65}: M64M65,
	{'M', 66}: M66,
	{'M', 68}: M68,

	// custom
	{'M', 700}: M700,
	{'M', 701}: M701,
}
var commandsByLetter = map[rune]commandFunc{
	'N': N,
	'T': addWarningCallback("Tool change is not supported."),
	'S': addWarningCallback("Spindle speed setting is not supported."),
	'F': F,
}

func G0(word *parsedWord, block *block, translator *translator) errors.SdkError {
	state := translator.state
	state.changeMode(word.Word)

	_, hasG53 := findWord(block, Word{'G', 53})

	return translator.line(word, block, lineArgs{
		relativeMode:    state.isRelativeDistanceMode(),
		useMachineCoord: hasG53,
		maxspeedNative:  state.traverseRateNative,
	})
}

func G1(word *parsedWord, block *block, translator *translator) errors.SdkError {
	state := translator.state
	state.changeMode(word.Word)

	feedRateNative, err := state.getCurrentFeedrateNative(word, block)
	if err != nil {
		return err
	}

	_, hasG53 := findWord(block, Word{'G', 53})

	return translator.line(word, block, lineArgs{
		relativeMode:    state.isRelativeDistanceMode(),
		useMachineCoord: hasG53,
		maxspeedNative:  feedRateNative,
	})
}

func G2(word *parsedWord, block *block, translator *translator) errors.SdkError {
	state := translator.state
	state.changeMode(word.Word)

	feedRateNative, err := state.getCurrentFeedrateNative(word, block)
	if err != nil {
		return err
	}

	_, hasG53 := findWord(block, Word{'G', 53})

	return translator.arc(word, block, arcArgs{
		clockwise:       true,
		useMachineCoord: hasG53,
		maxspeedNative:  feedRateNative,
	})
}

func G3(word *parsedWord, block *block, translator *translator) errors.SdkError {
	state := translator.state
	state.changeMode(word.Word)

	feedRateNative, err := state.getCurrentFeedrateNative(word, block)
	if err != nil {
		return err
	}

	_, hasG53 := findWord(block, Word{'G', 53})

	return translator.arc(word, block, arcArgs{
		clockwise:       false,
		useMachineCoord: hasG53,
		maxspeedNative:  feedRateNative,
	})
}

func G4(word *parsedWord, block *block, translator *translator) errors.SdkError {
	var dwellTime float64
	if p, hasP := useParameter(block, 'P'); hasP {
		if p.Value < 0 {
			return errExec(word, fmt.Sprintf("Invalid dwell time %s.", p))
		}
		dwellTime = p.Value * 1000
	}
	dwellTime = math.Max(dwellTime, minDwellTimeMs)

	translator.state.segmentBuffer = append(translator.state.segmentBuffer, &waitSegment{dwellTime})
	return nil
}

const lParamSetCoordinateSystem = 2

func G10(word *parsedWord, block *block, translator *translator) errors.SdkError {
	if l, hasL := useParameter(block, 'L'); hasL {
		if l.Value != lParamSetCoordinateSystem {
			return errExec(l, fmt.Sprintf("L parameter value %s not supported.", l))
		}
	} else {
		return errExec(word, "L parameter not provided")
	}

	p, hasP := useParameter(block, 'P')
	if !hasP {
		return errExec(word, "P parameter not provided")
	} else if p.Value != math.Floor(p.Value) {
		return errExec(p, fmt.Sprintf("P parameter %s is not an integer.", p))
	}

	coordinateSystemIndex := int(p.Value) - 1
	if coordinateSystemIndex == -1 {
		currentSystem := translator.state.modes[ModalGroup_CoordinateSystemSelection]
		coordinateSystemIndex, _ = getCoordinateSystemIndex(currentSystem)
	}

	if coordinateSystemIndex < 0 || coordinateSystemIndex >= coordinateSystemsCount {
		return errExec(p, fmt.Sprintf("P parameter %s out of range.", p))
	}

	for letter, axis := range translator.axes {
		param, hasParam := useParameter(block, letter)
		if !hasParam {
			continue
		}

		axis.state.coordinateSystemsOffsets[coordinateSystemIndex] = param.Value
	}

	return nil
}

func G28G30(word *parsedWord, block *block, translator *translator) errors.SdkError {
	state := translator.state
	var axes []int
	var positions []float64

	hasCoordinates := false
	for letter := range translator.axes {
		if _, found := useParameter(block, letter); found {
			hasCoordinates = true
			break
		}
	}

	if hasCoordinates {
		if err := translator.line(word, block, lineArgs{
			relativeMode:    state.isRelativeDistanceMode(),
			useMachineCoord: true,
			maxspeedNative:  state.traverseRateNative,
			noSmoothing:     true,
		}); err != nil {
			return err
		}
	}

	usePrimary := word.Value == 28

	for _, letter := range translator.axisLettersInOrder {
		if hasCoordinates {
			if _, found := useParameter(block, letter); !found {
				continue
			}
		}

		axis := translator.axes[letter]
		if usePrimary {
			axis.state.position = axis.state.homePosition
		} else {
			axis.state.position = axis.state.secondaryHomePosition
		}
		axis.state.positionInitialized = true

		converted, err := translator.convertPosition(word, axis, axis.state.position)
		if err != nil {
			return err
		}

		axes = append(axes, axis.streamAxis)
		positions = append(positions, converted)
	}

	state.segmentBuffer = append(state.segmentBuffer, &lineSegment{
		axes:        axes,
		end:         positions,
		maxspeed:    state.traverseRateNative,
		noSmoothing: true,
		stop:        translator.hasExactStop(block),
	})
	return nil
}

func G281G301(word *parsedWord, _ *block, translator *translator) errors.SdkError {
	usePrimary := math.Round(word.Value) == 28

	for _, letter := range translator.axisLettersInOrder {
		axis := translator.axes[letter]

		if usePrimary {
			axis.state.homePosition = axis.state.position
		} else {
			axis.state.secondaryHomePosition = axis.state.position
		}
	}
	return nil
}

func G64(_ *parsedWord, block *block, translator *translator) errors.SdkError {
	deviation := defaultSmoothingDeviation
	if param, found := useParameter(block, 'P'); found {
		deviation = param.Value
		if deviation <= 0 {
			return errExec(param, "Invalid deviation value.")
		}
	}

	if err := translator.setSmoothingDeviation(deviation); err != nil {
		return err
	}

	translator.state.modes[ModalGroup_PathControlMode] = Word{'G', 64}
	return nil
}

func G92(word *parsedWord, block *block, translator *translator) errors.SdkError {
	resetOffset := word.Value > 92

	for _, letter := range translator.axisLettersInOrder {
		axis := translator.axes[letter]

		if resetOffset {
			axis.state.position += axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex]
			axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex] = 0
		} else {
			position, found := useParameter(block, letter)
			if !found {
				continue
			}
			axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex] += axis.state.position - position.Value
			axis.state.position = position.Value
		}
	}
	return nil
}

func F(word *parsedWord, _ *block, translator *translator) errors.SdkError {
	value := word.Value
	state := translator.state

	if state.getModalValue(ModalGroup_FeedRateMode) != (Word{'G', 94}) {
		return errExec(word, "Inverse time feed rate mode is not supported.")
	}

	velocityAxis := translator.axes[translator.velocityAxis]
	velocityUnits, err := getVelocityUnits(velocityAxis.state.unit)
	if err != nil {
		return err
	}

	value = value / 60.0 // values are in units per minute but we use seconds
	converted, err := commandbuilding.ConvertUnitSetting(velocityAxis.commandIssuingInfo, "maxspeed", value, velocityUnits, false)
	if err != nil {
		return errConvert(word, velocityAxis.letter, err)
	}

	if converted <= 0 {
		return errExec(word, fmt.Sprintf("Invalid feed rate %f.", word.Value))
	}

	state.feedRateNative = converted
	return nil
}

func N(word *parsedWord, block *block, _ *translator) errors.SdkError {
	if word.Index != 0 {
		block.warnings = append(block.warnings, warningFromWord(word, "Line number should be the first word."))
	}
	return nil
}

func M2M30(_ *parsedWord, block *block, _ *translator) errors.SdkError {
	block.isProgramEnd = true
	return nil
}

func verifyChannelNumber(parameter *parsedWord) errors.SdkError {
	if parameter.Value == 0 {
		return errExec(parameter, "0 is not a valid IO channel number. The channel numbers start with 1.")
	} else if math.Round(parameter.Value) != parameter.Value || parameter.Value < 0 {
		return errExec(parameter, fmt.Sprintf("%s is not a valid IO channel number.", formatFloatValue(parameter.Value)))
	}
	return nil
}

func M64M65(word *parsedWord, block *block, translator *translator) errors.SdkError {
	channelNo, found := useParameter(block, 'P')
	if !found {
		return errExec(word, "P parameter (digital output number) is missing.")
	}
	if err := verifyChannelNumber(channelNo); err != nil {
		return err
	}

	value := 1
	if word.Value == 65 {
		value = 0
	}
	translator.state.segmentBuffer = append(translator.state.segmentBuffer, &passthroughSegment{
		command: fmt.Sprintf("io set do %d %d", int(channelNo.Value), value),
	})
	return nil
}

const useStreamCommandPassthough = "Use stream command passthough (M700) to achieve this functionality."

func M66(word *parsedWord, block *block, translator *translator) errors.SdkError {
	if _, found := useParameter(block, 'E'); found {
		return errExec(word, "Waiting for analog input (parameter E) is not supported by the translator. "+useStreamCommandPassthough)
	}
	if _, found := useParameter(block, 'Q'); found {
		return errExec(word, "Timeout (parameter Q) is not supported. Only indefinite waiting is supported.")
	}

	channelNo, found := useParameter(block, 'P')
	if !found {
		return errExec(word, "P parameter (digital input number) is missing.")
	}
	if err := verifyChannelNumber(channelNo); err != nil {
		return err
	}

	modeWord, found := useParameter(block, 'L')
	if !found {
		return errExec(word, "L parameter (mode) is missing.")
	}
	mode := int(modeWord.Value)

	var waitValue int
	switch mode {
	case 0:
		return errExec(word, "Mode 0 is not supported.")
	case 3:
		waitValue = 1
	case 4:
		waitValue = 0
	default:
		return errExec(word, fmt.Sprintf("Mode %d is not supported. Only modes 3 and 4 are supported. "+useStreamCommandPassthough, mode))
	}

	translator.state.segmentBuffer = append(translator.state.segmentBuffer, &passthroughSegment{
		command: fmt.Sprintf("wait io di %d == %d", int(channelNo.Value), waitValue),
	})
	return nil
}

func M68(word *parsedWord, block *block, translator *translator) errors.SdkError {
	channelNo, found := useParameter(block, 'E')
	if !found {
		return errExec(word, "E parameter (output number) is missing.")
	}
	if err := verifyChannelNumber(channelNo); err != nil {
		return err
	}

	voltage, found := useParameter(block, 'Q')
	if !found {
		return errExec(word, "Q parameter (voltage) is missing.")
	}
	translator.state.segmentBuffer = append(translator.state.segmentBuffer, &passthroughSegment{
		command: fmt.Sprintf("io set ao %d %s", int(channelNo.Value), formatFloatValue(voltage.Value)),
	})
	return nil
}

func M700(word *parsedWord, block *block, instance *translator) errors.SdkError {
	if len(block.comments) == 0 {
		block.warnings = append(block.warnings, warningFromWord(word, "No commands to pass through."))
	}
	for _, comment := range block.comments {
		instance.state.segmentBuffer = append(instance.state.segmentBuffer, &passthroughSegment{
			command: comment.text,
		})
	}
	return nil
}

func M701(word *parsedWord, block *block, instance *translator) errors.SdkError {
	param, found := useParameter(block, 'P')
	if !found {
		return errExec(word, "P parameter is missing.")
	}
	instance.state.helixEnabled = param.Value > 0
	return nil
}

type lineArgs struct {
	relativeMode    bool
	useMachineCoord bool
	noSmoothing     bool
	maxspeedNative  int64
}

func (instance *translator) line(word *parsedWord, block *block, args lineArgs) errors.SdkError {
	state := instance.state

	var axes []int
	var end []float64
	var start []float64

	coordinateSystemIndex, _ := getCoordinateSystemIndex(state.modes[ModalGroup_CoordinateSystemSelection])

	for _, letter := range instance.axisLettersInOrder {
		axis := instance.axes[letter]
		word, hasAxis := useParameter(block, axis.letter)
		if !hasAxis {
			continue
		}

		if axis.state.positionInitialized {
			position, err := instance.convertPosition(word, axis, axis.state.position)
			if err != nil {
				return err
			}
			start = append(start, position)
		}

		if args.relativeMode {
			if !axis.state.positionInitialized {
				return axisNotInitialized(word, axis.letter)
			}
			axis.state.position = axis.state.position + word.Value
		} else {
			axis.state.positionInitialized = true
			if args.useMachineCoord {
				axis.state.position = word.Value
			} else {
				axis.state.position = word.Value + axis.state.coordinateSystemsOffsets[coordinateSystemIndex]
			}
		}

		converted, err := instance.convertPosition(word, axis, axis.state.position)
		if err != nil {
			return err
		}

		axes = append(axes, axis.streamAxis)
		end = append(end, converted)
	}

	if len(axes) == 0 {
		block.warnings = append(block.warnings, warningFromWord(word, "Axes not defined, movement ignored."))
		return nil
	}
	if startIncomplete := len(start) != len(axes); startIncomplete {
		start = nil
	}

	if len(start) != 0 && isBelowDeviceResolution(start, end) && !utils.CompareSlice(start, end) {
		block.warnings = append(block.warnings, warningFromWord(word, "Line is below device resolution."))
	}

	state.segmentBuffer = append(state.segmentBuffer, &lineSegment{
		axes:        axes,
		end:         end,
		start:       start,
		maxspeed:    args.maxspeedNative,
		noSmoothing: args.noSmoothing,
		stop:        instance.hasExactStop(block),
	})
	return nil
}

func (instance *translator) hasExactStop(block *block) bool {
	if instance.state.modes[ModalGroup_PathControlMode] == (Word{'G', 61.1}) {
		return true
	}
	_, hasG9 := findWord(block, Word{'G', 9})
	return hasG9
}

type arcArgs struct {
	clockwise       bool
	useMachineCoord bool
	maxspeedNative  int64
}

func (instance *translator) arc(word *parsedWord, block *block, args arcArgs) errors.SdkError {
	state := instance.state

	coordinateSystemIndex, _ := getCoordinateSystemIndex(state.modes[ModalGroup_CoordinateSystemSelection])

	var arcAxes []int
	var arcCurrent mgl64.Vec2
	var arcCenter mgl64.Vec2
	var arcEnd mgl64.Vec2

	var helixLinearAxes []int
	var helixLinearEnd []float64

	radius, hasRadius := useParameter(block, 'R')
	if hasRadius {
		return errExec(radius, "Radius arc not supported.")
	} else {
		plane := state.getModalValue(ModalGroup_PlaneSelection)

		var param1Letter rune
		var param2Letter rune
		var pos1Letter rune
		var pos2Letter rune

		if plane == (Word{'G', 17}) {
			param1Letter = 'I'
			param2Letter = 'J'
			pos1Letter = 'X'
			pos2Letter = 'Y'
		} else if plane == (Word{'G', 18}) {
			param1Letter = 'I'
			param2Letter = 'K'
			pos1Letter = 'X'
			pos2Letter = 'Z'
		} else if plane == (Word{'G', 19}) {
			param1Letter = 'J'
			param2Letter = 'K'
			pos1Letter = 'Y'
			pos2Letter = 'Z'
		} else {
			panic(fmt.Sprintf("Unknown plane %s", plane))
		}

		param1, param1Found := useParameterValue(block, param1Letter)
		param2, param2Found := useParameterValue(block, param2Letter)
		if state.isRelativeArcMode() {
			if math.Abs(param1) < smallestDistance && math.Abs(param2) < smallestDistance {
				return errExec(word, fmt.Sprintf("At least one of parameter %s or %s must be specified as non-zero number.", string(param1Letter), string(param2Letter)))
			}
		} else {
			if !param1Found || !param2Found {
				return errExec(word, fmt.Sprintf("Both parameters %s and %s must be specified.", string(param1Letter), string(param2Letter)))
			}
		}

		pos1Axis := instance.axes[pos1Letter]
		pos2Axis := instance.axes[pos2Letter]
		if pos2Axis == nil && pos1Axis == nil {
			block.warnings = append(block.warnings, warningFromWord(word, "Axes not defined, movement ignored."))
			return nil
		}
		if pos1Axis == nil {
			return errExec(word, fmt.Sprintf("Axis %s is not defined.", string(pos1Letter)))
		}
		if pos2Axis == nil {
			return errExec(word, fmt.Sprintf("Axis %s is not defined.", string(pos2Letter)))
		}
		if !pos1Axis.state.positionInitialized {
			return axisNotInitialized(word, pos1Axis.letter)
		}
		if !pos2Axis.state.positionInitialized {
			return axisNotInitialized(word, pos2Axis.letter)
		}

		cordinateSystemOffset := mgl64.Vec2{
			pos1Axis.state.coordinateSystemsOffsets[coordinateSystemIndex],
			pos2Axis.state.coordinateSystemsOffsets[coordinateSystemIndex],
		}

		current := mgl64.Vec2{
			pos1Axis.state.position,
			pos2Axis.state.position,
		}

		for _, letter := range instance.axisLettersInOrder {
			axis := instance.axes[letter]
			word, hasAxis := useParameter(block, axis.letter)
			if !hasAxis {
				continue
			}

			if args.useMachineCoord {
				axis.state.position = word.Value
			} else {
				axis.state.position = word.Value + axis.state.coordinateSystemsOffsets[coordinateSystemIndex]
			}

			if axis.letter != pos1Letter && axis.letter != pos2Letter {
				converted, err := instance.convertPosition(word, axis, axis.state.position)
				if err != nil {
					return err
				}

				helixLinearAxes = append(helixLinearAxes, axis.streamAxis)
				helixLinearEnd = append(helixLinearEnd, converted)
			}
		}

		end := (mgl64.Vec2{
			pos1Axis.state.position,
			pos2Axis.state.position,
		})

		center := (mgl64.Vec2{param1, param2})
		if state.isRelativeArcMode() {
			center = center.Add(current)
		} else if !args.useMachineCoord {
			center = center.Add(cordinateSystemOffset)
		}

		radiusDiff := distanceFromCenterDifference(center, current, end)
		if radiusDiff > smallestDistance {
			return errExec(word, fmt.Sprintf("Specified coordinates do not form an arc (radius difference %f).", radiusDiff))
		}

		var centerConverted mgl64.Vec2

		if v, err := instance.convertPosition(word, pos1Axis, current[0]); err != nil {
			return err
		} else {
			arcCurrent[0] = v
		}
		if v, err := instance.convertPosition(word, pos2Axis, current[1]); err != nil {
			return err
		} else {
			arcCurrent[1] = v
		}

		if v, err := instance.convertPosition(word, pos1Axis, end[0]); err != nil {
			return err
		} else {
			arcEnd[0] = v
		}
		if v, err := instance.convertPosition(word, pos2Axis, end[1]); err != nil {
			return err
		} else {
			arcEnd[1] = v
		}

		if v, err := instance.convertPosition(word, pos1Axis, center[0]); err != nil {
			return err
		} else {
			centerConverted[0] = v
		}
		if v, err := instance.convertPosition(word, pos2Axis, center[1]); err != nil {
			return err
		} else {
			centerConverted[1] = v
		}

		arcCenter = roundCenterToBestCircle(centerConverted, arcCurrent, arcEnd)
		arcAxes = []int{pos1Axis.streamAxis, pos2Axis.streamAxis}
	}

	allAxes := arcAxes
	allEnds := arcEnd[:]

	if len(helixLinearAxes) > 0 {
		if state.helixEnabled {
			allAxes = append(allAxes, helixLinearAxes...)
			allEnds = append(allEnds, helixLinearEnd...)
		} else {
			state.segmentBuffer = append(state.segmentBuffer, &lineSegment{
				axes:        helixLinearAxes,
				end:         helixLinearEnd,
				maxspeed:    args.maxspeedNative,
				noSmoothing: true,
			})
		}
	}

	isCircle := arcCurrent == arcEnd
	if isAboveDeviceResolutionVec2(arcCurrent, arcEnd) || isCircle {
		state.segmentBuffer = append(state.segmentBuffer, &arcSegment{
			axes:      allAxes,
			center:    arcCenter[:],
			end:       allEnds,
			clockwise: args.clockwise,
			maxspeed:  args.maxspeedNative,
			stop:      instance.hasExactStop(block),
		})
	} else {
		block.warnings = append(block.warnings, warningFromWord(word, "Arc is below device resolution."))
		state.segmentBuffer = append(state.segmentBuffer, &lineSegment{
			axes:        allAxes,
			end:         allEnds,
			maxspeed:    args.maxspeedNative,
			noSmoothing: true,
			stop:        instance.hasExactStop(block),
		})
	}

	return nil
}

func (instance *translator) convertPosition(word *parsedWord, axis *translatorAxis, position float64) (float64, errors.SdkError) {
	position += axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex]

	converted, err := commandbuilding.ConvertUnitSetting(axis.commandIssuingInfo, "pos", position, axis.state.unit, false)
	if err != nil {
		return 0, errConvert(word, axis.letter, err)
	}
	if axis.transformation != nil {
		converted = axis.transformation.scaling*converted + axis.transformation.translationNative
	}
	return converted, nil
}

func useParameter(block *block, parameter rune) (word *parsedWord, found bool) {
	if i := utils.SliceIndex(len(block.words), func(i int) bool { return block.words[i].Letter == parameter }); i < 0 {
		return nil, false
	} else {
		word := block.words[i]
		block.used[word.Index] = true
		return word, true
	}
}
func useParameterValue(block *block, parameter rune) (value float64, found bool) {
	if word, has := useParameter(block, parameter); has {
		return word.Value, true
	} else {
		return 0, false
	}
}
func findWord(block *block, w Word) (word *parsedWord, found bool) {
	if i := utils.SliceIndex(len(block.words), func(i int) bool { return block.words[i].Word == w }); i < 0 {
		return nil, false
	} else {
		return block.words[i], true
	}
}

func formatIntValue(value float64) string {
	return fmt.Sprintf("%.0f", math.Round(value))
}

func formatFloatValue(value float64) string {
	return commandbuilding.FormatValueUsingNumberDecimalPlaces(value)
}

func distanceFromCenterDifference(center, p1, p2 mgl64.Vec2) float64 {
	return math.Abs(p1.Sub(center).Len() - p2.Sub(center).Len())
}

// TODO: remove in favor of chaning 1% condition of FW
func roundCenterToBestCircle(center, p1, p2 mgl64.Vec2) mgl64.Vec2 {
	p1 = roundVec2(p1)
	p2 = roundVec2(p2)

	options := []mgl64.Vec2{
		{
			math.Floor(center[0]),
			math.Floor(center[1]),
		},
		{
			math.Floor(center[0]),
			math.Ceil(center[1]),
		},
		{
			math.Ceil(center[0]),
			math.Floor(center[1]),
		},
		{
			math.Ceil(center[0]),
			math.Ceil(center[1]),
		},
	}

	smallestDistance := mgl64.InfPos
	bestIndex := 0
	for i, option := range options {
		if distance := distanceFromCenterDifference(option, p1, p2); distance < smallestDistance {
			smallestDistance = distance
			bestIndex = i
		}
	}

	return options[bestIndex]
}

func warningFromWord(word *parsedWord, message string) *pb.GcodeTranslateMessage {
	return &pb.GcodeTranslateMessage{
		Message:   message,
		FromBlock: int32(word.From),
		ToBlock:   int32(word.To),
	}
}

func isWordCommand(word Word) bool {
	return word.Letter == 'G' || word.Letter == 'M' || commandsByLetter[word.Letter] != nil
}

func errExec(word *parsedWord, message string) errors.SdkError {
	return errors.ErrGCodeExecution(
		message,
		word.blockPosition.From, word.blockPosition.To)
}

func axisNotInitialized(word *parsedWord, axisLetter rune) errors.SdkError {
	return errExec(word, fmt.Sprintf("Axis %s is not initialized. Use G28, G90 or setAxisPosition method.", string(axisLetter)))
}

func errConvert(word *parsedWord, letter rune, err errors.SdkError) errors.SdkError {
	return errExec(
		word,
		fmt.Sprintf("Cannot convert units for axis %s: %s.", string(letter), err))
}

func addWarningCallback(message string) commandFunc {
	return func(word *parsedWord, block *block, _ *translator) errors.SdkError {
		block.warnings = append(block.warnings, warningFromWord(word, message))
		return nil
	}
}

func addErrCallback(message string) commandFunc {
	return func(word *parsedWord, _ *block, _ *translator) errors.SdkError {
		return errExec(word, message)
	}
}

func addModalStateChangeCallback(modalGroup ModalGroupEnum) commandFunc {
	return func(word *parsedWord, _ *block, translator *translator) errors.SdkError {
		applicableWords := modalGroups[modalGroup].Words
		if _, exists := applicableWords[word.Word]; !exists {
			panic(fmt.Sprintf("Attemp to set modal state %d to %s which impossible.", modalGroup, word.Word))
		}
		translator.state.modes[modalGroup] = word.Word
		return nil
	}
}

func noop(_ *parsedWord, _ *block, _ *translator) errors.SdkError {
	return nil
}

func (state *translatorState) getCurrentFeedrateNative(word *parsedWord, block *block) (int64, errors.SdkError) {
	if state.feedRateNative == 0 {
		return 0, errExec(word, "Feed rate is not set.")
	}

	feedrateFinal := (int64)(math.Round(state.feedRateOverride * state.feedRateNative))
	if feedrateFinal == 0 {
		feedrateFinal = 1
		block.warnings = append(block.warnings, warningFromWord(word, "Feed rate rounded to minimum speed."))
	}

	return feedrateFinal, nil
}
