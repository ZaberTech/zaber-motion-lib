package gcode

import (
	"sort"
	"zaber-motion-lib/internal/utils"
)

/* Categories is our concept of sorting instructions for execution of our engine. */

type category struct {
	Letter     rune
	ModalGroup ModalGroupEnum
	Value      float64
	hasValue   bool
	Index      int
}

var noCategory = &category{}

var categories = []*category{
	{Letter: 'G', ModalGroup: ModalGroup_FeedRateMode},
	{Letter: 'F'},
	{Letter: 'S'},
	{Letter: 'T'},
	{Letter: 'M', ModalGroup: ModalGroup_ToolChange},
	{Letter: 'M', ModalGroup: ModalGroup_SpindleTurning},
	{Letter: 'M', ModalGroup: ModalGroup_FloodCoolant},
	{Letter: 'M', ModalGroup: ModalGroup_MistCoolant},
	{Letter: 'M', ModalGroup: ModalGroup_FeedAndSpeedOverrideSwitches},
	{Letter: 'G', Value: 4, hasValue: true},
	{Letter: 'G', ModalGroup: ModalGroup_PlaneSelection},
	{Letter: 'G', ModalGroup: ModalGroup_Units},
	{Letter: 'G', ModalGroup: ModalGroup_CutterRadiusCompensation},
	{Letter: 'G', ModalGroup: ModalGroup_ToolLengthOffset},
	{Letter: 'G', ModalGroup: ModalGroup_CoordinateSystemSelection},
	{Letter: 'G', ModalGroup: ModalGroup_PathControlMode},
	{Letter: 'G', ModalGroup: ModalGroup_DistanceMode},
	{Letter: 'G', ModalGroup: ModalGroup_ArcDistanceMode},
	{Letter: 'G', ModalGroup: ModalGroup_ReturnModeInCannedCycles},
	{Letter: 'G', ModalGroup: ModalGroup_CodesWithCoordinates},
	{Letter: 'G', ModalGroup: ModalGroup_Motion},
	{Letter: 'G', ModalGroup: ModalGroup_Stopping},
	noCategory,
}

func init() {
	for i, category := range categories {
		category.Index = i
	}
}

func getCategoryForWord(word Word) *category {
	groups := wordToModal[word]

	for _, category := range categories {
		if category.Letter == word.Letter && category.hasValue && category.Value == word.Value {
			return category
		}
	}
	for _, category := range categories {
		if category.Letter == word.Letter && utils.SliceIndex(len(groups), func(i int) bool { return groups[i] == category.ModalGroup }) >= 0 {
			return category
		}
	}
	for _, category := range categories {
		if category.Letter == word.Letter {
			return category
		}
	}
	return noCategory
}

type WordsWithCategories struct {
	categories []*category
	words      []*parsedWord
}

func (wwc *WordsWithCategories) Less(i, j int) bool {
	return wwc.categories[i].Index < wwc.categories[j].Index
}

func (wwc *WordsWithCategories) Len() int {
	return len(wwc.words)
}

func (wwc *WordsWithCategories) Swap(i, j int) {
	c := wwc.categories[i]
	wwc.categories[i] = wwc.categories[j]
	wwc.categories[j] = c

	w := wwc.words[i]
	wwc.words[i] = wwc.words[j]
	wwc.words[j] = w
}

func sortWordsForExecution(words []*parsedWord) {
	toSort := &WordsWithCategories{
		categories: make([]*category, len(words)),
		words:      words,
	}
	for i, word := range words {
		toSort.categories[i] = getCategoryForWord(word.Word)
	}
	sort.Sort(toSort)
}
