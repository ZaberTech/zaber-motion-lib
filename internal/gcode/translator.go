package gcode

import (
	"fmt"
	"math"
	"sync"
	"zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/api"
	apiDto "gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

var axisLetters = []rune{'X', 'Y', 'Z', 'A', 'B', 'C', 'E'}

type translatorAxis struct {
	letter             rune
	streamAxis         int
	commandIssuingInfo *ioc.CommandIssuingInfo
	transformation     *axisTransformation
	state              *axisState
	previousState      *axisState
}

type axisTransformation struct {
	scaling           float64
	translationNative float64
}

type translator struct {
	ID                 int32
	lock               sync.Mutex
	liveLock           sync.Mutex
	axisLettersInOrder []rune
	axes               map[rune]*translatorAxis
	velocityAxis       rune
	streamTarget       ioc.StreamTarget
	state              *translatorState
	previousState      *translatorState
	units              ioc.Units
	deviceManager      ioc.DeviceManager
}

func newTranslatorOffline(request *pb.TranslatorCreateRequest, deviceDb ioc.DeviceDb, units ioc.Units) (*translator, errors.SdkError) {
	definition := request.Definition

	if len(definition.Axes) == 0 {
		return nil, errors.ErrInvalidArgument("No axes defined.")
	}

	peripherals := make([]api.Peripheral, 0, len(definition.Axes))
	for _, axis := range definition.Axes {
		if axis.PeripheralId > 0 {
			peripherals = append(peripherals, api.Peripheral{PeripheralID: fmt.Sprintf("%d", axis.PeripheralId)})
		}
	}
	info, err := deviceDb.GetProductInformation(&api.GetProductInformationQuery{
		DeviceID:    fmt.Sprintf("%d", definition.DeviceId),
		Peripherals: peripherals,
	})
	if err != nil {
		return nil, err
	}

	cmdInfos := make([]*ioc.CommandIssuingInfo, len(definition.Axes))
	peripheralIndex := 0

	for i, axisDefinition := range definition.Axes {
		axis := definition.Axes[i]
		var cmdInfo *ioc.CommandIssuingInfo

		if axisDefinition.PeripheralId > 0 {
			peripheral := info.Peripherals[peripheralIndex]
			resolution, err := determineAxisResolution(axis, peripheral.Settings)
			if err != nil {
				return nil, err
			}

			cmdInfo = &ioc.CommandIssuingInfo{
				CommandTree:     deviceDb.GetCommandTree(peripheral.CommandTree),
				SettingsTable:   deviceDb.GetSettingsTable(peripheral.Settings),
				ConversionTable: units.GetConversionTable(peripheral.ConversionTable, resolution),
			}
			peripheralIndex++
		} else {
			resolution, err := determineAxisResolution(axis, info.Settings)
			if err != nil {
				return nil, err
			}

			cmdInfo = &ioc.CommandIssuingInfo{
				CommandTree:     deviceDb.GetCommandTree(info.CommandTree),
				SettingsTable:   deviceDb.GetSettingsTable(info.Settings),
				ConversionTable: units.GetConversionTable(info.ConversionTable, resolution),
			}
		}

		cmdInfos[i] = cmdInfo
	}

	t, err := newTranslator(request.Config, cmdInfos, units)
	if err != nil {
		return nil, err
	}

	if err := t.setTraverseRate(request.Definition.MaxSpeed); err != nil {
		return nil, err
	}

	return t, nil
}

func newTranslatorOfflineFromDevice(
	request *pb.TranslatorCreateFromDeviceRequest,
	deviceManager ioc.DeviceManager, units ioc.Units,
) (*translator, errors.SdkError) {
	if len(request.Axes) == 0 {
		return nil, errors.ErrInvalidArgument("Axes not specified")
	}

	device, err := deviceManager.GetIdentifiedDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	var cmdInfos []*ioc.CommandIssuingInfo
	for _, axis := range request.Axes {
		axisInfo, err := device.GetAxisInfo(&pb.AxisEmptyRequest{
			Axis: axis,
		})
		if err != nil {
			return nil, err
		}
		cmdInfos = append(cmdInfos, axisInfo.GetCommandIssuingInfo())
	}

	t, err := newTranslator(request.Config, cmdInfos, units)
	if err != nil {
		return nil, err
	}

	maxspeed, err := getMinMaxspeed(deviceManager, request)
	if err != nil {
		return nil, err
	}

	if err := t.setTraverseRate(&pb.Measurement{Value: maxspeed}); err != nil {
		return nil, err
	}

	return t, nil
}

func getMinMaxspeed(deviceManager ioc.DeviceManager, request *pb.TranslatorCreateFromDeviceRequest) (float64, errors.SdkError) {
	requests, err := deviceManager.GetRequests(request)
	if err != nil {
		return 0, err
	}

	reply, err := commands.SingleCommand(requests, c.Command{
		Device:  int(request.Device),
		Command: "get maxspeed",
	})
	if err != nil {
		return 0, err
	}

	data, err := reply.DataAsFloatArray(true)
	if err != nil {
		return 0, err
	}

	minSpeed := math.Inf(1)
	for _, axisNumber := range request.Axes {
		axisIndex := (int)(axisNumber - 1)
		if axisIndex >= len(data) {
			return 0, errors.ErrInvalidData("Device sent too short data array")
		}
		if data[axisIndex] < minSpeed {
			minSpeed = data[axisIndex]
		}
	}
	return minSpeed, nil
}

func determineAxisResolution(axis *pb.GcodeAxisDefinition, settings *apiDto.SettingsTable) (int, errors.SdkError) {
	hasResolution := utils.SliceIndex(len(settings.Rows), func(i int) bool {
		return settings.Rows[i].Name == "resolution"
	}) >= 0

	if !hasResolution {
		return 1, nil
	}

	resolution := axis.MicrostepResolution
	if resolution == nil || *resolution == 0 {
		return 0, errors.ErrInvalidArgument(fmt.Sprintf("Resolution not provided for axis with peripheral %d.", axis.PeripheralId))
	}
	return int(*resolution), nil
}

func newTranslatorLive(request *pb.TranslatorCreateLiveRequest, deviceManager ioc.DeviceManager, units ioc.Units) (*translator, errors.SdkError) {
	streamTarget := &pb.StreamEmptyRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		StreamId:    request.StreamId,
	}

	cmdInfos, err := deviceManager.GetStreamCommandIssuingInfo(streamTarget)
	if err != nil {
		return nil, err
	}
	streamMode, err := deviceManager.GetStreamMode(streamTarget)
	if err != nil {
		return nil, err
	}

	t, err := newTranslator(request.Config, cmdInfos, units)
	if err != nil {
		return nil, err
	}

	t.streamTarget = streamTarget
	t.deviceManager = deviceManager

	if streamMode == pb.AsciiStreamMode_LIVE {
		if err := t.resetPositionFromStream(); err != nil {
			return nil, err
		}
	}

	maxSpeeds, err := deviceManager.GetStreamAxisSetting(t.streamTarget, "maxspeed", false)
	if err != nil {
		return nil, err
	}
	if err := t.setTraverseRate(&pb.Measurement{Value: utils.MinFloatArray(maxSpeeds)}); err != nil {
		return nil, err
	}

	return t, nil
}

func newTranslator(config *pb.GcodeTranslatorConfig, cmdInfos []*ioc.CommandIssuingInfo, units ioc.Units) (*translator, errors.SdkError) {
	axes := make(map[rune]*translatorAxis)
	var firstAxis rune

	if config == nil {
		config = &pb.GcodeTranslatorConfig{}
	}

	if len(config.AxisMappings) == 0 {
		firstAxis = axisLetters[0]
		for i, info := range cmdInfos {
			if i >= len(axisLetters) {
				return nil, errors.ErrInvalidArgument("Too many axes.")
			}

			letter := axisLetters[i]
			axes[letter] = &translatorAxis{
				letter:             letter,
				streamAxis:         i,
				commandIssuingInfo: info,
			}
		}
	} else {
		for i, axis := range config.AxisMappings {
			if int(axis.AxisIndex) >= len(cmdInfos) || axis.AxisIndex < 0 {
				return nil, errors.ErrInvalidArgument(
					fmt.Sprintf("Invalid axis index %d, only %d axes are available.", axis.AxisIndex, len(cmdInfos)))
			}

			letter := firstRune(axis.AxisLetter)
			if _, alreadyExists := axes[letter]; alreadyExists {
				return nil, errors.ErrInvalidArgument(fmt.Sprintf("Duplicate axis: %s.", string(letter)))
			}

			axes[letter] = &translatorAxis{
				letter:             letter,
				streamAxis:         int(axis.AxisIndex),
				commandIssuingInfo: cmdInfos[axis.AxisIndex],
			}

			if i == 0 {
				firstAxis = letter
			}
		}
	}

	instance := &translator{
		axes:          axes,
		state:         newState(),
		previousState: newState(),
		velocityAxis:  firstAxis,
		units:         units,
	}

	for letter := range instance.axes {
		if utils.SliceIndex(len(axisLetters), func(i int) bool { return axisLetters[i] == letter }) < 0 {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Invalid axis letter: %s.", string(letter)))
		}
	}
	for _, letter := range axisLetters {
		if _, exists := instance.axes[letter]; exists {
			instance.axisLettersInOrder = append(instance.axisLettersInOrder, letter)
		}
	}

	duplicate := make(map[int]bool)
	for _, axis := range instance.axes {
		if duplicate[axis.streamAxis] {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Duplicate axis index: %d.", axis.streamAxis))
		}
		duplicate[axis.streamAxis] = true
	}

	for _, letter := range instance.axisLettersInOrder {
		axis := instance.axes[letter]

		positionInfo, exists := axis.commandIssuingInfo.SettingsTable.GetParamInfo("pos")
		if !exists {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Axis %s appears to be immobile.", string(letter)))
		}

		var dimension string
		if positionInfo.ContextualDimensionID > 0 {
			dimension, exists = axis.commandIssuingInfo.ConversionTable.GetDimensionName(positionInfo.ContextualDimensionID)
			if !exists {
				return nil, errors.ErrInternal(fmt.Sprintf("Unknown dimension %d.", positionInfo.ContextualDimensionID))
			}
		}

		units, err := getDefaultUnits(dimension)
		if err != nil {
			return nil, err
		}

		axis.state = newAxisState(units)
		axis.previousState = newAxisState(units)
	}

	for _, transform := range config.AxisTransformations {
		axis, exists := instance.axes[firstRune(transform.AxisLetter)]
		if !exists {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Transformation specified for unknown axis: %s.", transform.AxisLetter))
		}
		if axis.transformation != nil {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Transformation already specified for axis: %s.", transform.AxisLetter))
		}

		scaling := utils.NumberDefault(transform.Scaling, 1.0)
		if scaling == 0 { // backwards compatibility
			scaling = 1
		}

		translation := 0.0
		if transform.Translation != nil {
			transNative, err := commandbuilding.ConvertUnitSetting(axis.commandIssuingInfo, "pos", transform.Translation.Value, transform.Translation.Unit, false)
			if err != nil {
				return nil, err
			}
			translation = transNative
		}

		axis.transformation = &axisTransformation{
			scaling:           scaling,
			translationNative: translation,
		}
	}

	if err := instance.setSmoothingDeviation(defaultSmoothingDeviation); err != nil {
		return nil, err
	}

	return instance, nil
}

func (instance *translator) setTraverseRate(rate *pb.Measurement) errors.SdkError {
	if rate == nil {
		return errors.ErrInvalidArgument("Traverse rate not specified.")
	}

	instance.lock.Lock()
	defer instance.lock.Unlock()

	velocityInfo := instance.axes[instance.velocityAxis].commandIssuingInfo
	converted, err := commandbuilding.ConvertUnitSetting(velocityInfo, "maxspeed", rate.Value, rate.Unit, false)
	if err != nil {
		return errors.ErrInvalidArgument(fmt.Sprintf("Cannot convert traverse rate using axis %s: %s", string(instance.velocityAxis), err))
	}
	traverseRateNative := int64(math.Round(converted))

	if traverseRateNative <= 0 {
		return errors.ErrInvalidArgument(fmt.Sprintf("Invalid traverse rate %g.", rate.Value))
	}

	instance.state.traverseRateNative = traverseRateNative
	return nil
}

func (instance *translator) setSmoothingDeviation(deviation float64) errors.SdkError {
	velocityAxis := instance.axes[instance.velocityAxis]
	deviationNative, err := commandbuilding.ConvertUnitSetting(velocityAxis.commandIssuingInfo, "pos", deviation, velocityAxis.state.unit, false)
	if err != nil {
		return err
	}

	instance.state.smoothingDeviationNative = deviationNative
	return nil
}

func (instance *translator) translateRequest(block string) (*pb.GcodeTranslateResult, errors.SdkError) {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	return instance.translate(block)
}

func (instance *translator) flushRequest() (*pb.TranslatorFlushResponse, errors.SdkError) {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	return instance.flush(), nil
}

func (instance *translator) translateLiveRequest(block string) (*pb.GcodeTranslateResult, errors.SdkError) {
	instance.liveLock.Lock()
	defer instance.liveLock.Unlock()

	instance.lock.Lock()
	result, err := instance.translate(block)
	instance.lock.Unlock()
	if err != nil {
		return nil, err
	}

	if err := instance.streamCommands(result.Commands); err != nil {
		return nil, err
	}

	return result, nil
}

func (instance *translator) flushLiveRequest(request *pb.TranslatorFlushLiveRequest) (*pb.TranslatorFlushResponse, errors.SdkError) {
	instance.liveLock.Lock()
	defer instance.liveLock.Unlock()

	instance.lock.Lock()
	result := instance.flush()
	instance.lock.Unlock()

	streamMode, err := instance.deviceManager.GetStreamMode(instance.streamTarget)
	if err != nil {
		return nil, err
	}

	if err := instance.streamCommands(result.Commands); err != nil {
		return nil, err
	}

	if request.WaitUntilIdle && streamMode == pb.AsciiStreamMode_LIVE {
		if err := instance.deviceManager.StreamWaitUntilIdle(instance.streamTarget); err != nil {
			return nil, err
		}
	}

	return result, nil
}

func (instance *translator) streamCommands(commands []string) errors.SdkError {
	if instance.streamTarget == nil {
		return errors.ErrInternal("Translator is not a live translator.")
	}
	if len(commands) == 0 {
		return nil
	}

	if err := instance.deviceManager.StreamCommands(instance.streamTarget, commands); err != nil {
		instance.resetAfterStreamError()
		return err
	}

	return nil
}

func (instance *translator) translate(blockStr string) (*TranslateResult, errors.SdkError) {
	words, comments, err := parseBlock(blockStr)
	if err != nil {
		return nil, err
	}

	block := &block{
		words:    words,
		comments: comments,
		used:     make([]bool, len(words)),
	}

	instance.saveState()
	if err := instance.executeBlock(block); err != nil {
		instance.restoreStateAfterError()
		return nil, err
	}

	commands := instance.processSegments(block.isProgramEnd)

	if block.isProgramEnd {
		instance.resetTransientState()
	}

	return &TranslateResult{
		Commands: commands,
		Warnings: block.warnings,
	}, nil
}

func (instance *translator) flush() *FlushResult {
	commands := instance.processSegments(true)

	return &FlushResult{
		Commands: commands,
	}
}

type TranslateResult = pb.GcodeTranslateResult
type FlushResult = pb.TranslatorFlushResponse

type block struct {
	words        []*parsedWord
	comments     []*parsedComment
	used         []bool
	warnings     []*pb.GcodeTranslateMessage
	isProgramEnd bool
}

func (instance *translator) executeBlock(block *block) errors.SdkError {
	sortWordsForExecution(block.words)

	var triggeredMovement *parsedWord

	modalGroups := make(map[ModalGroupEnum]*parsedWord)
	for _, word := range block.words {
		if triggersMovement(word.Word) {
			triggeredMovement = word
		}

		groups, found := wordToModal[word.Word]
		if !found {
			continue
		}
		for _, group := range groups {
			conflict, found := modalGroups[group]
			if found {
				if group == ModalGroup_CodesWithCoordinates {
					return errExec(word, fmt.Sprintf("Conflict between %s and %s, they both use coordinates.", conflict, word))
				} else {
					return errExec(word, fmt.Sprintf("Conflict between %s and %s, they both belong the same modal group.", conflict, word))
				}
			}
			modalGroups[group] = word
		}
	}

	movementWord := modalGroups[ModalGroup_Motion]
	codeWithCoordinates := modalGroups[ModalGroup_CodesWithCoordinates]
	if codeWithCoordinates != nil && movementWord != nil {
		return errExec(codeWithCoordinates, fmt.Sprintf("Conflict between %s and %s, they both use coordinates.", codeWithCoordinates, movementWord))
	}

	for _, word := range block.words {
		if isWordCommand(word.Word) {
			if callback, found := commandsByWord[word.Word]; found {
				if err := callback(word, block, instance); err != nil {
					return err
				}
			} else if callback, found := commandsByLetter[word.Letter]; found {
				if err := callback(word, block, instance); err != nil {
					return err
				}
			} else {
				block.warnings = append(block.warnings, warningFromWord(word, fmt.Sprintf("Unknown command %s.", word.Word)))
			}
			block.used[word.Index] = true
		}
	}

	if triggeredMovement != nil && movementWord == nil && codeWithCoordinates == nil {
		currentMotion := instance.state.modes[ModalGroup_Motion]

		if callback, found := commandsByWord[currentMotion]; found {
			if err := callback(&parsedWord{
				Word:          currentMotion,
				Index:         triggeredMovement.Index,
				blockPosition: triggeredMovement.blockPosition,
			}, block, instance); err != nil {
				return err
			}
		}
	}

	for _, word := range block.words {
		if !block.used[word.Index] {
			block.warnings = append(block.warnings, warningFromWord(word, fmt.Sprintf("Word unused %s.", word.Word)))
		}
	}

	return nil
}

func (instance *translator) getAxis(axisLetter string) (*translatorAxis, errors.SdkError) {
	axis, found := instance.axes[firstRune(axisLetter)]
	if !found {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Invalid axis %s.", axisLetter))
	}
	return axis, nil
}

func (instance *translator) checkSegmentBufferEmpty() errors.SdkError {
	if len(instance.state.segmentBuffer) != 0 {
		return errors.ErrInvalidOperation("Position cannot be set because optimization buffer is not empty; call flush method first.")
	}
	return nil
}

func (instance *translator) setAxisPosition(request *pb.TranslatorSetAxisPositionRequest) errors.SdkError {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	if err := instance.checkSegmentBufferEmpty(); err != nil {
		return err
	}

	axis, err := instance.getAxis(request.Axis)
	if err != nil {
		return err
	}

	value, err := instance.convertAxisUnits(axis, request.Position, request.Unit, true, true)
	if err != nil {
		return err
	}

	axis.state.position = value
	axis.state.positionInitialized = true

	// if position has changed we assume that speed may have changed too
	instance.state.currentSpeedNative = 0

	return nil
}

func (instance *translator) getAxisPosition(request *pb.TranslatorGetAxisPositionRequest) (*pb.DoubleResponse, errors.SdkError) {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	axis, err := instance.getAxis(request.Axis)
	if err != nil {
		return nil, err
	}

	if !axis.state.positionInitialized {
		return nil, errors.ErrInvalidOperation(fmt.Sprintf("Position of axis %s is not initialized.", request.Axis))
	}

	value, err := instance.convertAxisUnits(axis, axis.state.position, request.Unit, false, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (instance *translator) setAxisHomePosition(request *pb.TranslatorSetAxisPositionRequest, isPrimary bool) errors.SdkError {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	axis, err := instance.getAxis(request.Axis)
	if err != nil {
		return err
	}

	value, err := instance.convertAxisUnits(axis, request.Position, request.Unit, true, true)
	if err != nil {
		return err
	}

	if isPrimary {
		axis.state.homePosition = value
	} else {
		axis.state.secondaryHomePosition = value
	}
	return nil
}

func (instance *translator) convertAxisUnits(axis *translatorAxis, valueToConvert float64, unit string, toAxisUnit bool, absolute bool) (float64, errors.SdkError) {
	units := instance.units

	var value float64
	if unit == constants.NativeUnit {
		if absolute && !toAxisUnit {
			valueToConvert += axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex]
		}

		if axis.transformation != nil && toAxisUnit {
			if absolute {
				valueToConvert -= axis.transformation.translationNative
			}
			valueToConvert /= axis.transformation.scaling
		}

		var err errors.SdkError
		value, err = commandbuilding.ConvertUnitSetting(axis.commandIssuingInfo, "pos", valueToConvert, axis.state.unit, toAxisUnit)
		if err != nil {
			return 0, err
		}

		if axis.transformation != nil && !toAxisUnit {
			value *= axis.transformation.scaling
			if absolute {
				value += axis.transformation.translationNative
			}
		}

		if absolute && toAxisUnit {
			value -= axis.state.coordinateSystemsOffsets[g92MachineCoordOffsetIndex]
		}
	} else {
		if err := units.AreUnitsCompatible(axis.state.unit, unit); err != nil {
			return 0, err
		}

		var unitFrom string
		var unitTo string
		if toAxisUnit {
			unitFrom = unit
			unitTo = axis.state.unit
		} else {
			unitFrom = axis.state.unit
			unitTo = unit
		}
		baseValue, err := units.ConvertStaticUnitToDimensionBase(valueToConvert, unitFrom)
		if err != nil {
			return 0, err
		}
		value, err = units.ConvertStaticUnitFromDimensionBase(baseValue, unitTo)
		if err != nil {
			return 0, err
		}
	}

	return value, nil
}

func (instance *translator) saveState() {
	instance.state.copyTo(instance.previousState)

	for _, axis := range instance.axes {
		axis.state.copyTo(axis.previousState)
	}
}

func (instance *translator) restoreStateAfterError() {
	instance.previousState.copyTo(instance.state)

	for _, axis := range instance.axes {
		axis.previousState.copyTo(axis.state)
	}
}

// resets state related to device state (position, speed)
func (instance *translator) resetTransientState() {
	instance.state.segmentBuffer = nil
	instance.state.currentSpeedNative = 0
	for _, axis := range instance.axes {
		axis.state.positionInitialized = false
		axis.state.position = 0
	}
}

func (instance *translator) getAxisOffset(request *pb.TranslatorGetAxisOffsetRequest) (*pb.DoubleResponse, errors.SdkError) {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	axis, err := instance.getAxis(request.Axis)
	if err != nil {
		return nil, err
	}

	word, err := parseWord([]rune(request.CoordinateSystem), blockPosition{}, 0)
	if err != nil {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Invalid coordinate system %s.", request.CoordinateSystem))
	}

	coordinateSystemIndex, err := getCoordinateSystemIndex(word.Word)
	if err != nil {
		return nil, err
	}

	offset := axis.state.coordinateSystemsOffsets[coordinateSystemIndex]
	value, err := instance.convertAxisUnits(axis, offset, request.Unit, false, false)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (instance *translator) getCurrentCoordinateSystem() *pb.StringResponse {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	return &pb.StringResponse{
		Value: instance.state.modes[ModalGroup_CoordinateSystemSelection].String(),
	}
}

func (instance *translator) resetAfterStreamError() {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	instance.resetTransientState()
}

func (instance *translator) setFeedRateOverride(coefficient float64) errors.SdkError {
	if coefficient < 0 {
		return errors.ErrInvalidArgument(fmt.Sprintf("Invalid feed rate override %f", coefficient))
	}

	instance.lock.Lock()
	defer instance.lock.Unlock()

	instance.state.feedRateOverride = coefficient
	return nil
}

func (instance *translator) resetPositionFromStream() errors.SdkError {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	if err := instance.checkSegmentBufferEmpty(); err != nil {
		return err
	}

	streamPosition, err := instance.deviceManager.GetStreamAxisSetting(instance.streamTarget, "pos", true)
	if err != nil {
		return err
	}

	for _, axis := range instance.axes {
		if axis.streamAxis >= len(streamPosition) {
			return errors.ErrInvalidOperation("Translator axes no longer match stream axes. Setup the stream again.")
		}

		position := streamPosition[axis.streamAxis]
		converted, err := instance.convertAxisUnits(axis, position, constants.NativeUnit, true, true)
		if err != nil {
			return err
		}

		axis.state.position = converted
		axis.state.positionInitialized = true
	}
	return nil
}
