package gcode

import (
	"fmt"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

type gcodeManager struct {
	gateway       ioc.GatewayManager
	deviceDb      ioc.DeviceDb
	units         ioc.Units
	deviceManager ioc.DeviceManager
	translators   *utils.TokenMap[*translator]
}

func NewGCode(gateway ioc.GatewayManager, deviceDb ioc.DeviceDb, units ioc.Units, deviceManager ioc.DeviceManager) *gcodeManager {
	instance := &gcodeManager{
		gateway:       gateway,
		deviceDb:      deviceDb,
		units:         units,
		deviceManager: deviceManager,
		translators:   utils.NewTokenMap[*translator](),
	}

	instance.register()

	return instance
}

func (instance *gcodeManager) register() {
	gateway := instance.gateway

	gateway.RegisterCallback("gcode/create", func() pb.Message {
		return &pb.TranslatorCreateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.create(request.(*pb.TranslatorCreateRequest))
	})
	gateway.RegisterCallback("gcode/create_from_device", func() pb.Message {
		return &pb.TranslatorCreateFromDeviceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.createFromDevice(request.(*pb.TranslatorCreateFromDeviceRequest))
	})
	gateway.RegisterCallback("gcode/free", func() pb.Message {
		return &pb.TranslatorEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.free(request.(*pb.TranslatorEmptyRequest))
	})
	gateway.RegisterCallback("gcode/translate", func() pb.Message {
		return &pb.TranslatorTranslateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.translate(request.(*pb.TranslatorTranslateRequest))
	})
	gateway.RegisterCallback("gcode/translate_live", func() pb.Message {
		return &pb.TranslatorTranslateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.translateLive(request.(*pb.TranslatorTranslateRequest))
	})
	gateway.RegisterCallback("gcode/flush", func() pb.Message {
		return &pb.TranslatorEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.flush(request.(*pb.TranslatorEmptyRequest))
	})
	gateway.RegisterCallback("gcode/flush_live", func() pb.Message {
		return &pb.TranslatorFlushLiveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.flushLive(request.(*pb.TranslatorFlushLiveRequest))
	})
	gateway.RegisterCallback("gcode/create_live", func() pb.Message {
		return &pb.TranslatorCreateLiveRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.createLive(request.(*pb.TranslatorCreateLiveRequest))
	})
	gateway.RegisterCallback("gcode/set_traverse_rate", func() pb.Message {
		return &pb.TranslatorSetTraverseRateRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setTraverseRate(request.(*pb.TranslatorSetTraverseRateRequest))
	})
	gateway.RegisterCallback("gcode/set_axis_position", func() pb.Message {
		return &pb.TranslatorSetAxisPositionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setAxisPosition(request.(*pb.TranslatorSetAxisPositionRequest))
	})
	gateway.RegisterCallback("gcode/get_axis_position", func() pb.Message {
		return &pb.TranslatorGetAxisPositionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getAxisPosition(request.(*pb.TranslatorGetAxisPositionRequest))
	})
	gateway.RegisterCallback("gcode/set_axis_home", func() pb.Message {
		return &pb.TranslatorSetAxisPositionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setAxisHomePosition(request.(*pb.TranslatorSetAxisPositionRequest), true)
	})
	gateway.RegisterCallback("gcode/set_axis_secondary_home", func() pb.Message {
		return &pb.TranslatorSetAxisPositionRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setAxisHomePosition(request.(*pb.TranslatorSetAxisPositionRequest), false)
	})
	gateway.RegisterCallback("gcode/get_axis_offset", func() pb.Message {
		return &pb.TranslatorGetAxisOffsetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getAxisOffset(request.(*pb.TranslatorGetAxisOffsetRequest))
	})
	gateway.RegisterCallback("gcode/get_current_coordinate_system", func() pb.Message {
		return &pb.TranslatorEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getCurrentCoordinateSystem(request.(*pb.TranslatorEmptyRequest))
	})
	gateway.RegisterCallback("gcode/reset_after_stream_error", func() pb.Message {
		return &pb.TranslatorEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.resetAfterStreamError(request.(*pb.TranslatorEmptyRequest))
	})
	gateway.RegisterCallback("gcode/set_feed_rate_override", func() pb.Message {
		return &pb.TranslatorSetFeedRateOverrideRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setFeedRateOverride(request.(*pb.TranslatorSetFeedRateOverrideRequest))
	})
	gateway.RegisterCallback("gcode/reset_position_from_stream", func() pb.Message {
		return &pb.TranslatorEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.resetPositionFromStream(request.(*pb.TranslatorEmptyRequest))
	})
}

func (instance *gcodeManager) create(request *pb.TranslatorCreateRequest) (*pb.TranslatorCreateResponse, errors.SdkError) {
	translator, err := newTranslatorOffline(request, instance.deviceDb, instance.units)
	if err != nil {
		return nil, err
	}
	translator.ID = instance.translators.Store(translator)

	return &pb.TranslatorCreateResponse{
		TranslatorId: translator.ID,
	}, nil
}

func (instance *gcodeManager) createFromDevice(request *pb.TranslatorCreateFromDeviceRequest) (*pb.TranslatorCreateResponse, errors.SdkError) {
	translator, err := newTranslatorOfflineFromDevice(request, instance.deviceManager, instance.units)
	if err != nil {
		return nil, err
	}
	translator.ID = instance.translators.Store(translator)

	return &pb.TranslatorCreateResponse{
		TranslatorId: translator.ID,
	}, nil
}

func (instance *gcodeManager) createLive(request *pb.TranslatorCreateLiveRequest) (*pb.TranslatorCreateResponse, errors.SdkError) {
	translator, err := newTranslatorLive(request, instance.deviceManager, instance.units)
	if err != nil {
		return nil, err
	}
	translator.ID = instance.translators.Store(translator)

	return &pb.TranslatorCreateResponse{
		TranslatorId: translator.ID,
	}, nil
}

func (instance *gcodeManager) free(request *pb.TranslatorEmptyRequest) errors.SdkError {
	instance.translators.Delete(request.TranslatorId)
	return nil
}

func (instance *gcodeManager) getTranslator(id int32) (*translator, errors.SdkError) {
	translator, errLoad := instance.translators.Load(id)
	if errLoad != nil {
		return nil, errors.ErrInvalidArgument(fmt.Sprintf("Translator %d is no longer available", id))
	}
	return translator, nil
}

func (instance *gcodeManager) translate(request *pb.TranslatorTranslateRequest) (*pb.GcodeTranslateResult, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.translateRequest(request.Block)
}

func (instance *gcodeManager) translateLive(request *pb.TranslatorTranslateRequest) (*pb.GcodeTranslateResult, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.translateLiveRequest(request.Block)
}

func (instance *gcodeManager) flush(request *pb.TranslatorEmptyRequest) (*pb.TranslatorFlushResponse, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.flushRequest()
}

func (instance *gcodeManager) flushLive(request *pb.TranslatorFlushLiveRequest) (*pb.TranslatorFlushResponse, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.flushLiveRequest(request)
}

func (instance *gcodeManager) setTraverseRate(request *pb.TranslatorSetTraverseRateRequest) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	return translator.setTraverseRate(&pb.Measurement{
		Value: request.TraverseRate,
		Unit:  request.Unit,
	})
}

func (instance *gcodeManager) setAxisPosition(request *pb.TranslatorSetAxisPositionRequest) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	return translator.setAxisPosition(request)
}

func (instance *gcodeManager) getAxisPosition(request *pb.TranslatorGetAxisPositionRequest) (*pb.DoubleResponse, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.getAxisPosition(request)
}

func (instance *gcodeManager) setAxisHomePosition(request *pb.TranslatorSetAxisPositionRequest, isPrimary bool) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	return translator.setAxisHomePosition(request, isPrimary)
}

func (instance *gcodeManager) getAxisOffset(request *pb.TranslatorGetAxisOffsetRequest) (*pb.DoubleResponse, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.getAxisOffset(request)
}

func (instance *gcodeManager) getCurrentCoordinateSystem(request *pb.TranslatorEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return nil, err
	}
	return translator.getCurrentCoordinateSystem(), nil
}

func (instance *gcodeManager) resetAfterStreamError(request *pb.TranslatorEmptyRequest) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	translator.resetAfterStreamError()
	return nil
}

func (instance *gcodeManager) setFeedRateOverride(request *pb.TranslatorSetFeedRateOverrideRequest) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	return translator.setFeedRateOverride(request.Coefficient)
}

func (instance *gcodeManager) resetPositionFromStream(request *pb.TranslatorEmptyRequest) errors.SdkError {
	translator, err := instance.getTranslator(request.TranslatorId)
	if err != nil {
		return err
	}
	return translator.resetPositionFromStream()
}
