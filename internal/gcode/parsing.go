package gcode

import (
	"fmt"
	"strconv"
	"unicode"
	"zaber-motion-lib/internal/errors"
)

type blockPosition struct {
	From int
	To   int
}

type Word struct {
	Letter rune
	Value  float64
}

func (w Word) String() string {
	return fmt.Sprintf("%s%g", string(w.Letter), w.Value)
}

type parsedWord struct {
	Word
	blockPosition
	Index int
}

type parsedComment struct {
	text string
	blockPosition
}

const commentStart = '('
const commentEnd = ')'
const expressionStart = '['
const parameterAssignmentOrValue = '#'
const optionalBlock = '/'
const blockEnd = ';'

func parseWord(word []rune, position blockPosition, index int) (*parsedWord, errors.SdkError) {
	if len(word) == 1 {
		return nil, errors.ErrGCodeSyntax("Word is missing a value.", position.From, position.To)
	}
	value, err := strconv.ParseFloat(string(word[1:]), 64)
	if err != nil {
		return nil, errors.ErrGCodeSyntax(fmt.Sprintf("Cannot parse word value: %s.", err), position.From, position.To)
	}
	return &parsedWord{
		Word: Word{
			Letter: word[0],
			Value:  value,
		},
		blockPosition: position,
		Index:         index,
	}, nil
}

func parseBlock(block string) ([]*parsedWord, []*parsedComment, errors.SdkError) {
	if len(block) == 0 || block[0] == optionalBlock {
		return nil, nil, nil
	}

	var words []*parsedWord
	var comments []*parsedComment
	var currentWord []rune
	currentWordStart := 0
	inComment := false
	currentCommentStart := 0

	runes := []rune(block)
	i := 0
	for ; i < len(runes); i++ {
		c := unicode.ToUpper(runes[i])

		if unicode.IsSpace(c) {
			continue
		} else if c == commentStart {
			if inComment {
				return nil, nil, errors.ErrGCodeSyntax("Nested comment is forbidden.", i, i+1)
			}
			inComment = true
			currentCommentStart = i
		} else if c == commentEnd {
			if !inComment {
				return nil, nil, errors.ErrGCodeSyntax("Unexpected comment end.", i, i+1)
			}
			comments = append(comments, &parsedComment{
				text:          block[currentCommentStart+1 : i],
				blockPosition: blockPosition{From: currentCommentStart, To: i + 1},
			})
			inComment = false
		} else if inComment {
			continue
		} else if c == expressionStart {
			return nil, nil, errors.ErrGCodeSyntax("Expressions are not supported.", i, i+1)
		} else if c == parameterAssignmentOrValue {
			return nil, nil, errors.ErrGCodeSyntax("Parameters are not supported.", i, i+1)
		} else if unicode.IsLetter(c) {
			if len(currentWord) > 0 {
				if parsed, err := parseWord(currentWord, blockPosition{From: currentWordStart, To: i}, len(words)); err != nil {
					return nil, nil, err
				} else {
					words = append(words, parsed)
				}
				currentWord = nil
			}

			currentWordStart = i
			currentWord = append(currentWord, c)
		} else if c == blockEnd {
			break
		} else {
			currentWord = append(currentWord, c)
		}
	}

	if inComment {
		return nil, nil, errors.ErrGCodeSyntax("Comment is missing closing bracket.", currentCommentStart, i)
	}

	if len(currentWord) > 0 {
		if parsed, err := parseWord(currentWord, blockPosition{From: currentWordStart, To: i}, len(words)); err != nil {
			return nil, nil, err
		} else {
			words = append(words, parsed)
		}
		//nolint:ineffassign
		currentWord = nil
	}

	for _, word := range words {
		for unicode.IsSpace(runes[word.To-1]) {
			word.To--
		}
	}

	return words, comments, nil
}

func firstRune(str string) rune {
	return ([]rune(str))[0]
}
