package test

import (
	"fmt"
	"time"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type testGateway struct {
	gateway ioc.GatewayManager
}

func NewTestRequests(gateway ioc.GatewayManager) *testGateway {
	manager := &testGateway{
		gateway: gateway,
	}
	manager.register()
	return manager
}

func (manager *testGateway) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("test/request", func() pb.Message {
		return &pb.TestRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.testRequest(request.(*pb.TestRequest))
	})
	gateway.RegisterCallback("test/request_long", func() pb.Message {
		return &pb.TestRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.testRequestLong(request.(*pb.TestRequest))
	})
	gateway.RegisterCallback("test/request_no_response", nil,
		func(_ pb.Message) (pb.Message, errors.SdkError) {
			return nil, nil
		})
	gateway.RegisterCallback("test/emit_event", nil,
		func(_ pb.Message) (pb.Message, errors.SdkError) {
			go manager.testEmitEvent()
			return nil, nil
		})
}

func (manager *testGateway) testRequest(request *pb.TestRequest) (*pb.TestResponse, errors.SdkError) {
	time.Sleep(20 * time.Millisecond)

	if request.ReturnError {
		return nil, errors.ErrRequestTimeout()
	}

	if request.ReturnErrorWithData {
		return nil, errors.ErrInvalidPacket("123", "For test")
	}

	return &pb.TestResponse{
		DataPong: request.DataPing + "...",
	}, nil
}

func (manager *testGateway) testRequestLong(request *pb.TestRequest) (*pb.TestResponseLong, errors.SdkError) {
	time.Sleep(20 * time.Millisecond)

	if request.ReturnError {
		return nil, errors.ErrRequestTimeout()
	}

	if request.ReturnErrorWithData {
		return nil, errors.ErrInvalidPacket("123", "For test")
	}

	response := make([]string, 10000)
	for i := 0; i < len(response); i++ {
		response[i] = fmt.Sprintf("%s...%d", request.DataPing, i)
	}

	return &pb.TestResponseLong{
		DataPong: response,
	}, nil
}

func (manager *testGateway) testEmitEvent() {
	time.Sleep(20 * time.Millisecond)

	event := &pb.TestEvent{
		Data: "testing event data",
	}
	manager.gateway.InvokeEvent("test/event", event)
}
