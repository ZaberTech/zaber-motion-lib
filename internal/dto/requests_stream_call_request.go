/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamCallRequest struct {
	InterfaceId  int32 `bson:"interfaceId"`
	Device       int32 `bson:"device"`
	StreamId     int32 `bson:"streamId"`
	Pvt          bool  `bson:"pvt"`
	StreamBuffer int32 `bson:"streamBuffer"`
	PvtBuffer    int32 `bson:"pvtBuffer"`
}

func (c *StreamCallRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamCallRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamCallRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamCallRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamCallRequest) GetStreamBuffer() int32 {
	return c.StreamBuffer
}

func (c *StreamCallRequest) GetPvtBuffer() int32 {
	return c.PvtBuffer
}

func (c *StreamCallRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamCallRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamCallRequest) Clone() (*StreamCallRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamCallRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamCallRequest) Sanitize() error {
	return nil
}
