/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type UnknownBinaryResponseEventWrapper struct {
	InterfaceId     int32                       `bson:"interfaceId"`
	UnknownResponse *BinaryUnknownResponseEvent `bson:"unknownResponse"`
}

func (c *UnknownBinaryResponseEventWrapper) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *UnknownBinaryResponseEventWrapper) GetUnknownResponse() *BinaryUnknownResponseEvent {
	return c.UnknownResponse
}

func (c *UnknownBinaryResponseEventWrapper) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *UnknownBinaryResponseEventWrapper) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *UnknownBinaryResponseEventWrapper) Clone() (*UnknownBinaryResponseEventWrapper, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &UnknownBinaryResponseEventWrapper{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *UnknownBinaryResponseEventWrapper) Sanitize() error {
	if c.UnknownResponse != nil {
		if err := c.UnknownResponse.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property UnknownResponse of UnknownBinaryResponseEventWrapper is nil")
	}

	return nil
}
