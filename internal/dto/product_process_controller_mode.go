/* This file is generated. Do not modify by hand. */
package dto

// Servo Tuning Parameter Set to target.
type ProductProcessControllerMode int32

const (
	ProductProcessControllerMode_MANUAL     ProductProcessControllerMode = 0
	ProductProcessControllerMode_PID        ProductProcessControllerMode = 1
	ProductProcessControllerMode_PID_HEATER ProductProcessControllerMode = 2
	ProductProcessControllerMode_ON_OFF     ProductProcessControllerMode = 3
)

var ProductProcessControllerMode_name = map[ProductProcessControllerMode]string{
	ProductProcessControllerMode_MANUAL:     "Manual",
	ProductProcessControllerMode_PID:        "Pid",
	ProductProcessControllerMode_PID_HEATER: "PidHeater",
	ProductProcessControllerMode_ON_OFF:     "OnOff",
}
