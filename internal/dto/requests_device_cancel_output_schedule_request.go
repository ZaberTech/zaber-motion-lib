/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceCancelOutputScheduleRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	Analog        bool  `bson:"analog"`
	ChannelNumber int32 `bson:"channelNumber"`
}

func (c *DeviceCancelOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceCancelOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceCancelOutputScheduleRequest) GetAnalog() bool {
	return c.Analog
}

func (c *DeviceCancelOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceCancelOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceCancelOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceCancelOutputScheduleRequest) Clone() (*DeviceCancelOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceCancelOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceCancelOutputScheduleRequest) Sanitize() error {
	return nil
}
