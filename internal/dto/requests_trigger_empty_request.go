/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerEmptyRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	TriggerNumber int32 `bson:"triggerNumber"`
}

func (c *TriggerEmptyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerEmptyRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerEmptyRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerEmptyRequest) Clone() (*TriggerEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerEmptyRequest) Sanitize() error {
	return nil
}
