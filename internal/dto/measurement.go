/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Represents a numerical value with optional units specified.
type Measurement struct {
	// Value of the measurement.
	Value float64 `bson:"value"`
	// Optional units of the measurement.
	Unit string `bson:"unit"`
}

// Value of the measurement.
func (c *Measurement) GetValue() float64 {
	return c.Value
}

// Optional units of the measurement.
func (c *Measurement) GetUnit() string {
	return c.Unit
}

func (c *Measurement) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *Measurement) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *Measurement) Clone() (*Measurement, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &Measurement{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *Measurement) Sanitize() error {
	return nil
}
