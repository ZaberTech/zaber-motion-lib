/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Defines an axis of the PVT sequence.
type AsciiPvtAxisDefinition struct {
	// Number of a physical axis or a lockstep group.
	AxisNumber int32 `bson:"axisNumber"`
	// Defines the type of the axis.
	AxisType *AsciiPvtAxisType `bson:"axisType"`
}

// Number of a physical axis or a lockstep group.
func (c *AsciiPvtAxisDefinition) GetAxisNumber() int32 {
	return c.AxisNumber
}

// Defines the type of the axis.
func (c *AsciiPvtAxisDefinition) GetAxisType() *AsciiPvtAxisType {
	return c.AxisType
}

func (c *AsciiPvtAxisDefinition) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiPvtAxisDefinition) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiPvtAxisDefinition) Clone() (*AsciiPvtAxisDefinition, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiPvtAxisDefinition{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiPvtAxisDefinition) Sanitize() error {
	return nil
}
