/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for PvtExecutionException.
type ExceptionsPvtExecutionExceptionData struct {
	// The error flag that caused the exception.
	ErrorFlag string `bson:"errorFlag"`
	// The reason for the exception.
	Reason string `bson:"reason"`
	// A list of points that cause the error (if applicable).
	InvalidPoints []*ExceptionsInvalidPvtPoint `bson:"invalidPoints"`
}

// The error flag that caused the exception.
func (c *ExceptionsPvtExecutionExceptionData) GetErrorFlag() string {
	return c.ErrorFlag
}

// The reason for the exception.
func (c *ExceptionsPvtExecutionExceptionData) GetReason() string {
	return c.Reason
}

// A list of points that cause the error (if applicable).
func (c *ExceptionsPvtExecutionExceptionData) GetInvalidPoints() []*ExceptionsInvalidPvtPoint {
	return c.InvalidPoints
}

func (c *ExceptionsPvtExecutionExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsPvtExecutionExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsPvtExecutionExceptionData) Clone() (*ExceptionsPvtExecutionExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsPvtExecutionExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsPvtExecutionExceptionData) Sanitize() error {
	if c.InvalidPoints == nil {
		c.InvalidPoints = make([]*ExceptionsInvalidPvtPoint, 0)
	}

	for _, item := range c.InvalidPoints {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property InvalidPoints of PvtExecutionExceptionData is nil")
		}
	}

	return nil
}
