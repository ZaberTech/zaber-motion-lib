/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceMultiGetSettingRequest struct {
	InterfaceId  int32                  `bson:"interfaceId"`
	Device       int32                  `bson:"device"`
	Axis         int32                  `bson:"axis"`
	Settings     []*AsciiGetSetting     `bson:"settings"`
	AxisSettings []*AsciiGetAxisSetting `bson:"axisSettings"`
}

func (c *DeviceMultiGetSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceMultiGetSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceMultiGetSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceMultiGetSettingRequest) GetSettings() []*AsciiGetSetting {
	return c.Settings
}

func (c *DeviceMultiGetSettingRequest) GetAxisSettings() []*AsciiGetAxisSetting {
	return c.AxisSettings
}

func (c *DeviceMultiGetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceMultiGetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceMultiGetSettingRequest) Clone() (*DeviceMultiGetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceMultiGetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceMultiGetSettingRequest) Sanitize() error {
	if c.Settings == nil {
		c.Settings = make([]*AsciiGetSetting, 0)
	}

	for _, item := range c.Settings {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Settings of DeviceMultiGetSettingRequest is nil")
		}
	}

	if c.AxisSettings == nil {
		c.AxisSettings = make([]*AsciiGetAxisSetting, 0)
	}

	for _, item := range c.AxisSettings {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property AxisSettings of DeviceMultiGetSettingRequest is nil")
		}
	}

	return nil
}
