/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The complete state of a trigger.
type AsciiTriggerState struct {
	// The firing condition for a trigger.
	Condition string `bson:"condition"`
	// The actions for a trigger.
	Actions []string `bson:"actions"`
	// The enabled state for a trigger.
	Enabled bool `bson:"enabled"`
	// The number of total fires for this trigger.
	// A value of -1 indicates unlimited fires.
	FiresTotal int32 `bson:"firesTotal"`
	// The number of remaining fires for this trigger.
	// A value of -1 indicates unlimited fires remaining.
	FiresRemaining int32 `bson:"firesRemaining"`
}

// The firing condition for a trigger.
func (c *AsciiTriggerState) GetCondition() string {
	return c.Condition
}

// The actions for a trigger.
func (c *AsciiTriggerState) GetActions() []string {
	return c.Actions
}

// The enabled state for a trigger.
func (c *AsciiTriggerState) GetEnabled() bool {
	return c.Enabled
}

// The number of total fires for this trigger.
// A value of -1 indicates unlimited fires.
func (c *AsciiTriggerState) GetFiresTotal() int32 {
	return c.FiresTotal
}

// The number of remaining fires for this trigger.
// A value of -1 indicates unlimited fires remaining.
func (c *AsciiTriggerState) GetFiresRemaining() int32 {
	return c.FiresRemaining
}

func (c *AsciiTriggerState) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiTriggerState) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiTriggerState) Clone() (*AsciiTriggerState, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiTriggerState{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiTriggerState) Sanitize() error {
	if c.Actions == nil {
		c.Actions = make([]string, 0)
	}

	return nil
}
