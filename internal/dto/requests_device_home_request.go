/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceHomeRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	Axis          int32 `bson:"axis"`
	WaitUntilIdle bool  `bson:"waitUntilIdle"`
}

func (c *DeviceHomeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceHomeRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceHomeRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceHomeRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *DeviceHomeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceHomeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceHomeRequest) Clone() (*DeviceHomeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceHomeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceHomeRequest) Sanitize() error {
	return nil
}
