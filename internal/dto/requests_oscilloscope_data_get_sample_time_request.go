/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeDataGetSampleTimeRequest struct {
	DataId int32  `bson:"dataId"`
	Unit   string `bson:"unit"`
	Index  int32  `bson:"index"`
}

func (c *OscilloscopeDataGetSampleTimeRequest) GetDataId() int32 {
	return c.DataId
}

func (c *OscilloscopeDataGetSampleTimeRequest) GetUnit() string {
	return c.Unit
}

func (c *OscilloscopeDataGetSampleTimeRequest) GetIndex() int32 {
	return c.Index
}

func (c *OscilloscopeDataGetSampleTimeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeDataGetSampleTimeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeDataGetSampleTimeRequest) Clone() (*OscilloscopeDataGetSampleTimeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeDataGetSampleTimeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeDataGetSampleTimeRequest) Sanitize() error {
	return nil
}
