/* This file is generated. Do not modify by hand. */
package dto

// Kind of I/O pin to use.
type AsciiIoPortType int32

const (
	AsciiIoPortType_NONE           AsciiIoPortType = 0
	AsciiIoPortType_ANALOG_INPUT   AsciiIoPortType = 1
	AsciiIoPortType_ANALOG_OUTPUT  AsciiIoPortType = 2
	AsciiIoPortType_DIGITAL_INPUT  AsciiIoPortType = 3
	AsciiIoPortType_DIGITAL_OUTPUT AsciiIoPortType = 4
)

var AsciiIoPortType_name = map[AsciiIoPortType]string{
	AsciiIoPortType_NONE:           "None",
	AsciiIoPortType_ANALOG_INPUT:   "AnalogInput",
	AsciiIoPortType_ANALOG_OUTPUT:  "AnalogOutput",
	AsciiIoPortType_DIGITAL_INPUT:  "DigitalInput",
	AsciiIoPortType_DIGITAL_OUTPUT: "DigitalOutput",
}
