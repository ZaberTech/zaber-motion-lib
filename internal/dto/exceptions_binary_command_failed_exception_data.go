/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for BinaryCommandFailedException.
type ExceptionsBinaryCommandFailedExceptionData struct {
	// The response data.
	ResponseData int32 `bson:"responseData"`
}

// The response data.
func (c *ExceptionsBinaryCommandFailedExceptionData) GetResponseData() int32 {
	return c.ResponseData
}

func (c *ExceptionsBinaryCommandFailedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsBinaryCommandFailedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsBinaryCommandFailedExceptionData) Clone() (*ExceptionsBinaryCommandFailedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsBinaryCommandFailedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsBinaryCommandFailedExceptionData) Sanitize() error {
	return nil
}
