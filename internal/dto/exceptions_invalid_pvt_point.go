/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains invalid PVT points for PvtExecutionException.
type ExceptionsInvalidPvtPoint struct {
	// Index of the point numbered from the last submitted point.
	Index int32 `bson:"index"`
	// The textual representation of the point.
	Point string `bson:"point"`
}

// Index of the point numbered from the last submitted point.
func (c *ExceptionsInvalidPvtPoint) GetIndex() int32 {
	return c.Index
}

// The textual representation of the point.
func (c *ExceptionsInvalidPvtPoint) GetPoint() string {
	return c.Point
}

func (c *ExceptionsInvalidPvtPoint) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsInvalidPvtPoint) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsInvalidPvtPoint) Clone() (*ExceptionsInvalidPvtPoint, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsInvalidPvtPoint{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsInvalidPvtPoint) Sanitize() error {
	return nil
}
