/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The raw parameters currently saved to a given paramset.
type AsciiParamsetInfo struct {
	// The tuning algorithm used for this axis.
	Type string `bson:"type"`
	// The version of the tuning algorithm used for this axis.
	Version int32 `bson:"version"`
	// The raw tuning parameters of this device.
	Params []*AsciiServoTuningParam `bson:"params"`
}

// The tuning algorithm used for this axis.
func (c *AsciiParamsetInfo) GetType() string {
	return c.Type
}

// The version of the tuning algorithm used for this axis.
func (c *AsciiParamsetInfo) GetVersion() int32 {
	return c.Version
}

// The raw tuning parameters of this device.
func (c *AsciiParamsetInfo) GetParams() []*AsciiServoTuningParam {
	return c.Params
}

func (c *AsciiParamsetInfo) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiParamsetInfo) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiParamsetInfo) Clone() (*AsciiParamsetInfo, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiParamsetInfo{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiParamsetInfo) Sanitize() error {
	if c.Params == nil {
		c.Params = make([]*AsciiServoTuningParam, 0)
	}

	for _, item := range c.Params {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Params of ParamsetInfo is nil")
		}
	}

	return nil
}
