/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetDigitalOutputScheduleRequest struct {
	InterfaceId   int32                    `bson:"interfaceId"`
	Device        int32                    `bson:"device"`
	ChannelNumber int32                    `bson:"channelNumber"`
	Value         AsciiDigitalOutputAction `bson:"value"`
	FutureValue   AsciiDigitalOutputAction `bson:"futureValue"`
	Delay         float64                  `bson:"delay"`
	Unit          string                   `bson:"unit"`
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetValue() AsciiDigitalOutputAction {
	return c.Value
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetFutureValue() AsciiDigitalOutputAction {
	return c.FutureValue
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *DeviceSetDigitalOutputScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetDigitalOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetDigitalOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetDigitalOutputScheduleRequest) Clone() (*DeviceSetDigitalOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetDigitalOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetDigitalOutputScheduleRequest) Sanitize() error {
	return nil
}
