/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepStopRequest struct {
	InterfaceId     int32 `bson:"interfaceId"`
	Device          int32 `bson:"device"`
	LockstepGroupId int32 `bson:"lockstepGroupId"`
	WaitUntilIdle   bool  `bson:"waitUntilIdle"`
}

func (c *LockstepStopRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepStopRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepStopRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepStopRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *LockstepStopRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepStopRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepStopRequest) Clone() (*LockstepStopRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepStopRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepStopRequest) Sanitize() error {
	return nil
}
