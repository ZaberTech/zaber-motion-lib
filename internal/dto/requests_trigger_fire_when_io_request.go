/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerFireWhenIoRequest struct {
	InterfaceId      int32                 `bson:"interfaceId"`
	Device           int32                 `bson:"device"`
	TriggerNumber    int32                 `bson:"triggerNumber"`
	PortType         AsciiIoPortType       `bson:"portType"`
	Channel          int32                 `bson:"channel"`
	TriggerCondition AsciiTriggerCondition `bson:"triggerCondition"`
	Value            float64               `bson:"value"`
}

func (c *TriggerFireWhenIoRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerFireWhenIoRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerFireWhenIoRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerFireWhenIoRequest) GetPortType() AsciiIoPortType {
	return c.PortType
}

func (c *TriggerFireWhenIoRequest) GetChannel() int32 {
	return c.Channel
}

func (c *TriggerFireWhenIoRequest) GetTriggerCondition() AsciiTriggerCondition {
	return c.TriggerCondition
}

func (c *TriggerFireWhenIoRequest) GetValue() float64 {
	return c.Value
}

func (c *TriggerFireWhenIoRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerFireWhenIoRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerFireWhenIoRequest) Clone() (*TriggerFireWhenIoRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerFireWhenIoRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerFireWhenIoRequest) Sanitize() error {
	return nil
}
