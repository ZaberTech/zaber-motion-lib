/* This file is generated. Do not modify by hand. */
package dto

// Named constants for all Zaber Binary protocol error codes.
type BinaryErrorCode int32

const (
	BinaryErrorCode_CANNOT_HOME                                BinaryErrorCode = 1
	BinaryErrorCode_DEVICE_NUMBER_INVALID                      BinaryErrorCode = 2
	BinaryErrorCode_ADDRESS_INVALID                            BinaryErrorCode = 5
	BinaryErrorCode_VOLTAGE_LOW                                BinaryErrorCode = 14
	BinaryErrorCode_VOLTAGE_HIGH                               BinaryErrorCode = 15
	BinaryErrorCode_STORED_POSITION_INVALID                    BinaryErrorCode = 18
	BinaryErrorCode_ABSOLUTE_POSITION_INVALID                  BinaryErrorCode = 20
	BinaryErrorCode_RELATIVE_POSITION_INVALID                  BinaryErrorCode = 21
	BinaryErrorCode_VELOCITY_INVALID                           BinaryErrorCode = 22
	BinaryErrorCode_AXIS_INVALID                               BinaryErrorCode = 25
	BinaryErrorCode_AXIS_DEVICE_NUMBER_INVALID                 BinaryErrorCode = 26
	BinaryErrorCode_INVERSION_INVALID                          BinaryErrorCode = 27
	BinaryErrorCode_VELOCITY_PROFILE_INVALID                   BinaryErrorCode = 28
	BinaryErrorCode_VELOCITY_SCALE_INVALID                     BinaryErrorCode = 29
	BinaryErrorCode_LOAD_EVENT_INVALID                         BinaryErrorCode = 30
	BinaryErrorCode_RETURN_EVENT_INVALID                       BinaryErrorCode = 31
	BinaryErrorCode_JOYSTICK_CALIBRATION_MODE_INVALID          BinaryErrorCode = 33
	BinaryErrorCode_PERIPHERAL_ID_INVALID                      BinaryErrorCode = 36
	BinaryErrorCode_RESOLUTION_INVALID                         BinaryErrorCode = 37
	BinaryErrorCode_RUN_CURRENT_INVALID                        BinaryErrorCode = 38
	BinaryErrorCode_HOLD_CURRENT_INVALID                       BinaryErrorCode = 39
	BinaryErrorCode_MODE_INVALID                               BinaryErrorCode = 40
	BinaryErrorCode_HOME_SPEED_INVALID                         BinaryErrorCode = 41
	BinaryErrorCode_SPEED_INVALID                              BinaryErrorCode = 42
	BinaryErrorCode_ACCELERATION_INVALID                       BinaryErrorCode = 43
	BinaryErrorCode_MAXIMUM_POSITION_INVALID                   BinaryErrorCode = 44
	BinaryErrorCode_CURRENT_POSITION_INVALID                   BinaryErrorCode = 45
	BinaryErrorCode_MAXIMUM_RELATIVE_MOVE_INVALID              BinaryErrorCode = 46
	BinaryErrorCode_OFFSET_INVALID                             BinaryErrorCode = 47
	BinaryErrorCode_ALIAS_INVALID                              BinaryErrorCode = 48
	BinaryErrorCode_LOCK_STATE_INVALID                         BinaryErrorCode = 49
	BinaryErrorCode_DEVICE_ID_UNKNOWN                          BinaryErrorCode = 50
	BinaryErrorCode_SETTING_INVALID                            BinaryErrorCode = 53
	BinaryErrorCode_COMMAND_INVALID                            BinaryErrorCode = 64
	BinaryErrorCode_PARK_STATE_INVALID                         BinaryErrorCode = 65
	BinaryErrorCode_TEMPERATURE_HIGH                           BinaryErrorCode = 67
	BinaryErrorCode_DIGITAL_INPUT_PIN_INVALID                  BinaryErrorCode = 68
	BinaryErrorCode_DIGITAL_OUTPUT_PIN_INVALID                 BinaryErrorCode = 71
	BinaryErrorCode_DIGITAL_OUTPUT_MASK_INVALID                BinaryErrorCode = 74
	BinaryErrorCode_ANALOG_INPUT_PIN_INVALID                   BinaryErrorCode = 76
	BinaryErrorCode_MOVE_INDEX_NUMBER_INVALID                  BinaryErrorCode = 78
	BinaryErrorCode_INDEX_DISTANCE_INVALID                     BinaryErrorCode = 79
	BinaryErrorCode_CYCLE_DISTANCE_INVALID                     BinaryErrorCode = 80
	BinaryErrorCode_FILTER_HOLDER_ID_INVALID                   BinaryErrorCode = 81
	BinaryErrorCode_ABSOLUTE_FORCE_INVALID                     BinaryErrorCode = 87
	BinaryErrorCode_AUTO_REPLY_DISABLED_MODE_INVALID           BinaryErrorCode = 101
	BinaryErrorCode_MESSAGE_ID_MODE_INVALID                    BinaryErrorCode = 102
	BinaryErrorCode_HOME_STATUS_INVALID                        BinaryErrorCode = 103
	BinaryErrorCode_HOME_SENSOR_TYPE_INVALID                   BinaryErrorCode = 104
	BinaryErrorCode_AUTO_HOME_DISABLED_MODE_INVALID            BinaryErrorCode = 105
	BinaryErrorCode_MINIMUM_POSITION_INVALID                   BinaryErrorCode = 106
	BinaryErrorCode_KNOB_DISABLED_MODE_INVALID                 BinaryErrorCode = 107
	BinaryErrorCode_KNOB_DIRECTION_INVALID                     BinaryErrorCode = 108
	BinaryErrorCode_KNOB_MOVEMENT_MODE_INVALID                 BinaryErrorCode = 109
	BinaryErrorCode_KNOB_JOG_SIZE_INVALID                      BinaryErrorCode = 110
	BinaryErrorCode_KNOB_VELOCITY_SCALE_INVALID                BinaryErrorCode = 111
	BinaryErrorCode_KNOB_VELOCITY_PROFILE_INVALID              BinaryErrorCode = 112
	BinaryErrorCode_ACCELERATION_ONLY_INVALID                  BinaryErrorCode = 113
	BinaryErrorCode_DECELERATION_ONLY_INVALID                  BinaryErrorCode = 114
	BinaryErrorCode_MOVE_TRACKING_MODE_INVALID                 BinaryErrorCode = 115
	BinaryErrorCode_MANUAL_MOVE_TRACKING_DISABLED_MODE_INVALID BinaryErrorCode = 116
	BinaryErrorCode_MOVE_TRACKING_PERIOD_INVALID               BinaryErrorCode = 117
	BinaryErrorCode_CLOSED_LOOP_MODE_INVALID                   BinaryErrorCode = 118
	BinaryErrorCode_SLIP_TRACKING_PERIOD_INVALID               BinaryErrorCode = 119
	BinaryErrorCode_STALL_TIMEOUT_INVALID                      BinaryErrorCode = 120
	BinaryErrorCode_DEVICE_DIRECTION_INVALID                   BinaryErrorCode = 121
	BinaryErrorCode_BAUD_RATE_INVALID                          BinaryErrorCode = 122
	BinaryErrorCode_PROTOCOL_INVALID                           BinaryErrorCode = 123
	BinaryErrorCode_BAUD_RATE_OR_PROTOCOL_INVALID              BinaryErrorCode = 124
	BinaryErrorCode_BUSY                                       BinaryErrorCode = 255
	BinaryErrorCode_SYSTEM_ERROR                               BinaryErrorCode = 257
	BinaryErrorCode_STORAGE_FULL                               BinaryErrorCode = 401
	BinaryErrorCode_REGISTER_ADDRESS_INVALID                   BinaryErrorCode = 701
	BinaryErrorCode_REGISTER_VALUE_INVALID                     BinaryErrorCode = 702
	BinaryErrorCode_SAVE_POSITION_INVALID                      BinaryErrorCode = 1600
	BinaryErrorCode_SAVE_POSITION_NOT_HOMED                    BinaryErrorCode = 1601
	BinaryErrorCode_RETURN_POSITION_INVALID                    BinaryErrorCode = 1700
	BinaryErrorCode_MOVE_POSITION_INVALID                      BinaryErrorCode = 1800
	BinaryErrorCode_MOVE_POSITION_NOT_HOMED                    BinaryErrorCode = 1801
	BinaryErrorCode_RELATIVE_POSITION_LIMITED                  BinaryErrorCode = 2146
	BinaryErrorCode_SETTINGS_LOCKED                            BinaryErrorCode = 3600
	BinaryErrorCode_BIT_1_INVALID                              BinaryErrorCode = 4001
	BinaryErrorCode_BIT_2_INVALID                              BinaryErrorCode = 4002
	BinaryErrorCode_DISABLE_AUTO_HOME_INVALID                  BinaryErrorCode = 4008
	BinaryErrorCode_BIT_10_INVALID                             BinaryErrorCode = 4010
	BinaryErrorCode_BIT_11_INVALID                             BinaryErrorCode = 4011
	BinaryErrorCode_HOME_SWITCH_INVALID                        BinaryErrorCode = 4012
	BinaryErrorCode_BIT_13_INVALID                             BinaryErrorCode = 4013
	BinaryErrorCode_BIT_14_INVALID                             BinaryErrorCode = 4014
	BinaryErrorCode_BIT_15_INVALID                             BinaryErrorCode = 4015
	BinaryErrorCode_DEVICE_PARKED                              BinaryErrorCode = 6501
	BinaryErrorCode_DRIVER_DISABLED                            BinaryErrorCode = 9001
)

var BinaryErrorCode_name = map[BinaryErrorCode]string{
	BinaryErrorCode_CANNOT_HOME:                                "CannotHome",
	BinaryErrorCode_DEVICE_NUMBER_INVALID:                      "DeviceNumberInvalid",
	BinaryErrorCode_ADDRESS_INVALID:                            "AddressInvalid",
	BinaryErrorCode_VOLTAGE_LOW:                                "VoltageLow",
	BinaryErrorCode_VOLTAGE_HIGH:                               "VoltageHigh",
	BinaryErrorCode_STORED_POSITION_INVALID:                    "StoredPositionInvalid",
	BinaryErrorCode_ABSOLUTE_POSITION_INVALID:                  "AbsolutePositionInvalid",
	BinaryErrorCode_RELATIVE_POSITION_INVALID:                  "RelativePositionInvalid",
	BinaryErrorCode_VELOCITY_INVALID:                           "VelocityInvalid",
	BinaryErrorCode_AXIS_INVALID:                               "AxisInvalid",
	BinaryErrorCode_AXIS_DEVICE_NUMBER_INVALID:                 "AxisDeviceNumberInvalid",
	BinaryErrorCode_INVERSION_INVALID:                          "InversionInvalid",
	BinaryErrorCode_VELOCITY_PROFILE_INVALID:                   "VelocityProfileInvalid",
	BinaryErrorCode_VELOCITY_SCALE_INVALID:                     "VelocityScaleInvalid",
	BinaryErrorCode_LOAD_EVENT_INVALID:                         "LoadEventInvalid",
	BinaryErrorCode_RETURN_EVENT_INVALID:                       "ReturnEventInvalid",
	BinaryErrorCode_JOYSTICK_CALIBRATION_MODE_INVALID:          "JoystickCalibrationModeInvalid",
	BinaryErrorCode_PERIPHERAL_ID_INVALID:                      "PeripheralIDInvalid",
	BinaryErrorCode_RESOLUTION_INVALID:                         "ResolutionInvalid",
	BinaryErrorCode_RUN_CURRENT_INVALID:                        "RunCurrentInvalid",
	BinaryErrorCode_HOLD_CURRENT_INVALID:                       "HoldCurrentInvalid",
	BinaryErrorCode_MODE_INVALID:                               "ModeInvalid",
	BinaryErrorCode_HOME_SPEED_INVALID:                         "HomeSpeedInvalid",
	BinaryErrorCode_SPEED_INVALID:                              "SpeedInvalid",
	BinaryErrorCode_ACCELERATION_INVALID:                       "AccelerationInvalid",
	BinaryErrorCode_MAXIMUM_POSITION_INVALID:                   "MaximumPositionInvalid",
	BinaryErrorCode_CURRENT_POSITION_INVALID:                   "CurrentPositionInvalid",
	BinaryErrorCode_MAXIMUM_RELATIVE_MOVE_INVALID:              "MaximumRelativeMoveInvalid",
	BinaryErrorCode_OFFSET_INVALID:                             "OffsetInvalid",
	BinaryErrorCode_ALIAS_INVALID:                              "AliasInvalid",
	BinaryErrorCode_LOCK_STATE_INVALID:                         "LockStateInvalid",
	BinaryErrorCode_DEVICE_ID_UNKNOWN:                          "DeviceIDUnknown",
	BinaryErrorCode_SETTING_INVALID:                            "SettingInvalid",
	BinaryErrorCode_COMMAND_INVALID:                            "CommandInvalid",
	BinaryErrorCode_PARK_STATE_INVALID:                         "ParkStateInvalid",
	BinaryErrorCode_TEMPERATURE_HIGH:                           "TemperatureHigh",
	BinaryErrorCode_DIGITAL_INPUT_PIN_INVALID:                  "DigitalInputPinInvalid",
	BinaryErrorCode_DIGITAL_OUTPUT_PIN_INVALID:                 "DigitalOutputPinInvalid",
	BinaryErrorCode_DIGITAL_OUTPUT_MASK_INVALID:                "DigitalOutputMaskInvalid",
	BinaryErrorCode_ANALOG_INPUT_PIN_INVALID:                   "AnalogInputPinInvalid",
	BinaryErrorCode_MOVE_INDEX_NUMBER_INVALID:                  "MoveIndexNumberInvalid",
	BinaryErrorCode_INDEX_DISTANCE_INVALID:                     "IndexDistanceInvalid",
	BinaryErrorCode_CYCLE_DISTANCE_INVALID:                     "CycleDistanceInvalid",
	BinaryErrorCode_FILTER_HOLDER_ID_INVALID:                   "FilterHolderIDInvalid",
	BinaryErrorCode_ABSOLUTE_FORCE_INVALID:                     "AbsoluteForceInvalid",
	BinaryErrorCode_AUTO_REPLY_DISABLED_MODE_INVALID:           "AutoReplyDisabledModeInvalid",
	BinaryErrorCode_MESSAGE_ID_MODE_INVALID:                    "MessageIDModeInvalid",
	BinaryErrorCode_HOME_STATUS_INVALID:                        "HomeStatusInvalid",
	BinaryErrorCode_HOME_SENSOR_TYPE_INVALID:                   "HomeSensorTypeInvalid",
	BinaryErrorCode_AUTO_HOME_DISABLED_MODE_INVALID:            "AutoHomeDisabledModeInvalid",
	BinaryErrorCode_MINIMUM_POSITION_INVALID:                   "MinimumPositionInvalid",
	BinaryErrorCode_KNOB_DISABLED_MODE_INVALID:                 "KnobDisabledModeInvalid",
	BinaryErrorCode_KNOB_DIRECTION_INVALID:                     "KnobDirectionInvalid",
	BinaryErrorCode_KNOB_MOVEMENT_MODE_INVALID:                 "KnobMovementModeInvalid",
	BinaryErrorCode_KNOB_JOG_SIZE_INVALID:                      "KnobJogSizeInvalid",
	BinaryErrorCode_KNOB_VELOCITY_SCALE_INVALID:                "KnobVelocityScaleInvalid",
	BinaryErrorCode_KNOB_VELOCITY_PROFILE_INVALID:              "KnobVelocityProfileInvalid",
	BinaryErrorCode_ACCELERATION_ONLY_INVALID:                  "AccelerationOnlyInvalid",
	BinaryErrorCode_DECELERATION_ONLY_INVALID:                  "DecelerationOnlyInvalid",
	BinaryErrorCode_MOVE_TRACKING_MODE_INVALID:                 "MoveTrackingModeInvalid",
	BinaryErrorCode_MANUAL_MOVE_TRACKING_DISABLED_MODE_INVALID: "ManualMoveTrackingDisabledModeInvalid",
	BinaryErrorCode_MOVE_TRACKING_PERIOD_INVALID:               "MoveTrackingPeriodInvalid",
	BinaryErrorCode_CLOSED_LOOP_MODE_INVALID:                   "ClosedLoopModeInvalid",
	BinaryErrorCode_SLIP_TRACKING_PERIOD_INVALID:               "SlipTrackingPeriodInvalid",
	BinaryErrorCode_STALL_TIMEOUT_INVALID:                      "StallTimeoutInvalid",
	BinaryErrorCode_DEVICE_DIRECTION_INVALID:                   "DeviceDirectionInvalid",
	BinaryErrorCode_BAUD_RATE_INVALID:                          "BaudRateInvalid",
	BinaryErrorCode_PROTOCOL_INVALID:                           "ProtocolInvalid",
	BinaryErrorCode_BAUD_RATE_OR_PROTOCOL_INVALID:              "BaudRateOrProtocolInvalid",
	BinaryErrorCode_BUSY:                                       "Busy",
	BinaryErrorCode_SYSTEM_ERROR:                               "SystemError",
	BinaryErrorCode_STORAGE_FULL:                               "StorageFull",
	BinaryErrorCode_REGISTER_ADDRESS_INVALID:                   "RegisterAddressInvalid",
	BinaryErrorCode_REGISTER_VALUE_INVALID:                     "RegisterValueInvalid",
	BinaryErrorCode_SAVE_POSITION_INVALID:                      "SavePositionInvalid",
	BinaryErrorCode_SAVE_POSITION_NOT_HOMED:                    "SavePositionNotHomed",
	BinaryErrorCode_RETURN_POSITION_INVALID:                    "ReturnPositionInvalid",
	BinaryErrorCode_MOVE_POSITION_INVALID:                      "MovePositionInvalid",
	BinaryErrorCode_MOVE_POSITION_NOT_HOMED:                    "MovePositionNotHomed",
	BinaryErrorCode_RELATIVE_POSITION_LIMITED:                  "RelativePositionLimited",
	BinaryErrorCode_SETTINGS_LOCKED:                            "SettingsLocked",
	BinaryErrorCode_BIT_1_INVALID:                              "Bit1Invalid",
	BinaryErrorCode_BIT_2_INVALID:                              "Bit2Invalid",
	BinaryErrorCode_DISABLE_AUTO_HOME_INVALID:                  "DisableAutoHomeInvalid",
	BinaryErrorCode_BIT_10_INVALID:                             "Bit10Invalid",
	BinaryErrorCode_BIT_11_INVALID:                             "Bit11Invalid",
	BinaryErrorCode_HOME_SWITCH_INVALID:                        "HomeSwitchInvalid",
	BinaryErrorCode_BIT_13_INVALID:                             "Bit13Invalid",
	BinaryErrorCode_BIT_14_INVALID:                             "Bit14Invalid",
	BinaryErrorCode_BIT_15_INVALID:                             "Bit15Invalid",
	BinaryErrorCode_DEVICE_PARKED:                              "DeviceParked",
	BinaryErrorCode_DRIVER_DISABLED:                            "DriverDisabled",
}
