/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type MicroscopeEmptyRequest struct {
	InterfaceId int32                       `bson:"interfaceId"`
	Config      *MicroscopyMicroscopeConfig `bson:"config"`
}

func (c *MicroscopeEmptyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *MicroscopeEmptyRequest) GetConfig() *MicroscopyMicroscopeConfig {
	return c.Config
}

func (c *MicroscopeEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopeEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopeEmptyRequest) Clone() (*MicroscopeEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopeEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopeEmptyRequest) Sanitize() error {
	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Config of MicroscopeEmptyRequest is nil")
	}

	return nil
}
