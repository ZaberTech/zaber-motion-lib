/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetStorageRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Key         string `bson:"key"`
	Value       string `bson:"value"`
	Encode      bool   `bson:"encode"`
}

func (c *DeviceSetStorageRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetStorageRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetStorageRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetStorageRequest) GetKey() string {
	return c.Key
}

func (c *DeviceSetStorageRequest) GetValue() string {
	return c.Value
}

func (c *DeviceSetStorageRequest) GetEncode() bool {
	return c.Encode
}

func (c *DeviceSetStorageRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetStorageRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetStorageRequest) Clone() (*DeviceSetStorageRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetStorageRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetStorageRequest) Sanitize() error {
	return nil
}
