/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The enabled state of a single trigger.
// Returns whether the given trigger is enabled and the number of times it will fire.
// This is a subset of the complete state, and is faster to query.
type AsciiTriggerEnabledState struct {
	// The enabled state for a trigger.
	Enabled bool `bson:"enabled"`
	// The number of remaining fires for this trigger.
	// A value of -1 indicates unlimited fires remaining.
	FiresRemaining int32 `bson:"firesRemaining"`
}

// The enabled state for a trigger.
func (c *AsciiTriggerEnabledState) GetEnabled() bool {
	return c.Enabled
}

// The number of remaining fires for this trigger.
// A value of -1 indicates unlimited fires remaining.
func (c *AsciiTriggerEnabledState) GetFiresRemaining() int32 {
	return c.FiresRemaining
}

func (c *AsciiTriggerEnabledState) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiTriggerEnabledState) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiTriggerEnabledState) Clone() (*AsciiTriggerEnabledState, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiTriggerEnabledState{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiTriggerEnabledState) Sanitize() error {
	return nil
}
