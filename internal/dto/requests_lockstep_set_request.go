/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepSetRequest struct {
	InterfaceId     int32   `bson:"interfaceId"`
	Device          int32   `bson:"device"`
	LockstepGroupId int32   `bson:"lockstepGroupId"`
	Value           float64 `bson:"value"`
	Unit            string  `bson:"unit"`
	AxisIndex       int32   `bson:"axisIndex"`
}

func (c *LockstepSetRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepSetRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepSetRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepSetRequest) GetValue() float64 {
	return c.Value
}

func (c *LockstepSetRequest) GetUnit() string {
	return c.Unit
}

func (c *LockstepSetRequest) GetAxisIndex() int32 {
	return c.AxisIndex
}

func (c *LockstepSetRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepSetRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepSetRequest) Clone() (*LockstepSetRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepSetRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepSetRequest) Sanitize() error {
	return nil
}
