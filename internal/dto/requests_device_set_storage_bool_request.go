/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetStorageBoolRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Key         string `bson:"key"`
	Value       bool   `bson:"value"`
}

func (c *DeviceSetStorageBoolRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetStorageBoolRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetStorageBoolRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetStorageBoolRequest) GetKey() string {
	return c.Key
}

func (c *DeviceSetStorageBoolRequest) GetValue() bool {
	return c.Value
}

func (c *DeviceSetStorageBoolRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetStorageBoolRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetStorageBoolRequest) Clone() (*DeviceSetStorageBoolRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetStorageBoolRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetStorageBoolRequest) Sanitize() error {
	return nil
}
