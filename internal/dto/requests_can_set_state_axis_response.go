/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CanSetStateAxisResponse struct {
	Error      *string `bson:"error"`
	AxisNumber int32   `bson:"axisNumber"`
}

func (c *CanSetStateAxisResponse) GetError() *string {
	return c.Error
}

func (c *CanSetStateAxisResponse) GetAxisNumber() int32 {
	return c.AxisNumber
}

func (c *CanSetStateAxisResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CanSetStateAxisResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CanSetStateAxisResponse) Clone() (*CanSetStateAxisResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CanSetStateAxisResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CanSetStateAxisResponse) Sanitize() error {
	return nil
}
