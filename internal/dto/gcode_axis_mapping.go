/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Maps a translator axis to a Zaber stream axis.
type GcodeAxisMapping struct {
	// Letter of the translator axis (X,Y,Z,A,B,C,E).
	AxisLetter string `bson:"axisLetter"`
	// Index of the stream axis.
	AxisIndex int32 `bson:"axisIndex"`
}

// Letter of the translator axis (X,Y,Z,A,B,C,E).
func (c *GcodeAxisMapping) GetAxisLetter() string {
	return c.AxisLetter
}

// Index of the stream axis.
func (c *GcodeAxisMapping) GetAxisIndex() int32 {
	return c.AxisIndex
}

func (c *GcodeAxisMapping) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeAxisMapping) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeAxisMapping) Clone() (*GcodeAxisMapping, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeAxisMapping{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeAxisMapping) Sanitize() error {
	return nil
}
