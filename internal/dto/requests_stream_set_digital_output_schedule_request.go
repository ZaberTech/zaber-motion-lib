/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetDigitalOutputScheduleRequest struct {
	InterfaceId   int32                    `bson:"interfaceId"`
	Device        int32                    `bson:"device"`
	StreamId      int32                    `bson:"streamId"`
	Pvt           bool                     `bson:"pvt"`
	ChannelNumber int32                    `bson:"channelNumber"`
	Value         AsciiDigitalOutputAction `bson:"value"`
	FutureValue   AsciiDigitalOutputAction `bson:"futureValue"`
	Delay         float64                  `bson:"delay"`
	Unit          string                   `bson:"unit"`
}

func (c *StreamSetDigitalOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetDigitalOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetDigitalOutputScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetDigitalOutputScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetDigitalOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamSetDigitalOutputScheduleRequest) GetValue() AsciiDigitalOutputAction {
	return c.Value
}

func (c *StreamSetDigitalOutputScheduleRequest) GetFutureValue() AsciiDigitalOutputAction {
	return c.FutureValue
}

func (c *StreamSetDigitalOutputScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *StreamSetDigitalOutputScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetDigitalOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetDigitalOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetDigitalOutputScheduleRequest) Clone() (*StreamSetDigitalOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetDigitalOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetDigitalOutputScheduleRequest) Sanitize() error {
	return nil
}
