/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for PvtMovementInterruptedException.
type ExceptionsPvtMovementInterruptedExceptionData struct {
	// The full list of warnings.
	Warnings []string `bson:"warnings"`
	// The reason for the Exception.
	Reason string `bson:"reason"`
}

// The full list of warnings.
func (c *ExceptionsPvtMovementInterruptedExceptionData) GetWarnings() []string {
	return c.Warnings
}

// The reason for the Exception.
func (c *ExceptionsPvtMovementInterruptedExceptionData) GetReason() string {
	return c.Reason
}

func (c *ExceptionsPvtMovementInterruptedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsPvtMovementInterruptedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsPvtMovementInterruptedExceptionData) Clone() (*ExceptionsPvtMovementInterruptedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsPvtMovementInterruptedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsPvtMovementInterruptedExceptionData) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	return nil
}
