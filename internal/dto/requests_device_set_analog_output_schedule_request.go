/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAnalogOutputScheduleRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	ChannelNumber int32   `bson:"channelNumber"`
	Value         float64 `bson:"value"`
	FutureValue   float64 `bson:"futureValue"`
	Delay         float64 `bson:"delay"`
	Unit          string  `bson:"unit"`
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetValue() float64 {
	return c.Value
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetFutureValue() float64 {
	return c.FutureValue
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *DeviceSetAnalogOutputScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetAnalogOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAnalogOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAnalogOutputScheduleRequest) Clone() (*DeviceSetAnalogOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAnalogOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAnalogOutputScheduleRequest) Sanitize() error {
	return nil
}
