/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamCircleRequest struct {
	InterfaceId       int32             `bson:"interfaceId"`
	Device            int32             `bson:"device"`
	StreamId          int32             `bson:"streamId"`
	Pvt               bool              `bson:"pvt"`
	Type              StreamSegmentType `bson:"type"`
	RotationDirection RotationDirection `bson:"rotationDirection"`
	CenterX           *Measurement      `bson:"centerX"`
	CenterY           *Measurement      `bson:"centerY"`
	TargetAxesIndices []int32           `bson:"targetAxesIndices"`
}

func (c *StreamCircleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamCircleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamCircleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamCircleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamCircleRequest) GetType() StreamSegmentType {
	return c.Type
}

func (c *StreamCircleRequest) GetRotationDirection() RotationDirection {
	return c.RotationDirection
}

func (c *StreamCircleRequest) GetCenterX() *Measurement {
	return c.CenterX
}

func (c *StreamCircleRequest) GetCenterY() *Measurement {
	return c.CenterY
}

func (c *StreamCircleRequest) GetTargetAxesIndices() []int32 {
	return c.TargetAxesIndices
}

func (c *StreamCircleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamCircleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamCircleRequest) Clone() (*StreamCircleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamCircleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamCircleRequest) Sanitize() error {
	if c.CenterX != nil {
		if err := c.CenterX.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property CenterX of StreamCircleRequest is nil")
	}

	if c.CenterY != nil {
		if err := c.CenterY.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property CenterY of StreamCircleRequest is nil")
	}

	if c.TargetAxesIndices == nil {
		c.TargetAxesIndices = make([]int32, 0)
	}

	return nil
}
