/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetDigitalOutputRequest struct {
	InterfaceId   int32                    `bson:"interfaceId"`
	Device        int32                    `bson:"device"`
	ChannelNumber int32                    `bson:"channelNumber"`
	Value         AsciiDigitalOutputAction `bson:"value"`
}

func (c *DeviceSetDigitalOutputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetDigitalOutputRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetDigitalOutputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceSetDigitalOutputRequest) GetValue() AsciiDigitalOutputAction {
	return c.Value
}

func (c *DeviceSetDigitalOutputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetDigitalOutputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetDigitalOutputRequest) Clone() (*DeviceSetDigitalOutputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetDigitalOutputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetDigitalOutputRequest) Sanitize() error {
	return nil
}
