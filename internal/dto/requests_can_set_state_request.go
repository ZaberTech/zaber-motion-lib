/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CanSetStateRequest struct {
	InterfaceId     int32            `bson:"interfaceId"`
	Device          int32            `bson:"device"`
	Axis            int32            `bson:"axis"`
	State           string           `bson:"state"`
	FirmwareVersion *FirmwareVersion `bson:"firmwareVersion"`
}

func (c *CanSetStateRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *CanSetStateRequest) GetDevice() int32 {
	return c.Device
}

func (c *CanSetStateRequest) GetAxis() int32 {
	return c.Axis
}

func (c *CanSetStateRequest) GetState() string {
	return c.State
}

func (c *CanSetStateRequest) GetFirmwareVersion() *FirmwareVersion {
	return c.FirmwareVersion
}

func (c *CanSetStateRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CanSetStateRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CanSetStateRequest) Clone() (*CanSetStateRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CanSetStateRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CanSetStateRequest) Sanitize() error {
	if c.FirmwareVersion != nil {
		if err := c.FirmwareVersion.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
