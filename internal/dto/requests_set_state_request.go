/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetStateRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	State       string `bson:"state"`
	DeviceOnly  bool   `bson:"deviceOnly"`
}

func (c *SetStateRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetStateRequest) GetDevice() int32 {
	return c.Device
}

func (c *SetStateRequest) GetAxis() int32 {
	return c.Axis
}

func (c *SetStateRequest) GetState() string {
	return c.State
}

func (c *SetStateRequest) GetDeviceOnly() bool {
	return c.DeviceOnly
}

func (c *SetStateRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetStateRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetStateRequest) Clone() (*SetStateRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetStateRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetStateRequest) Sanitize() error {
	return nil
}
