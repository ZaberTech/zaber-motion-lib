/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The response from a multi-get command.
type AsciiGetSettingResult struct {
	// The setting read.
	Setting string `bson:"setting"`
	// The list of values returned.
	Values []float64 `bson:"values"`
	// The unit of the values.
	Unit string `bson:"unit"`
}

// The setting read.
func (c *AsciiGetSettingResult) GetSetting() string {
	return c.Setting
}

// The list of values returned.
func (c *AsciiGetSettingResult) GetValues() []float64 {
	return c.Values
}

// The unit of the values.
func (c *AsciiGetSettingResult) GetUnit() string {
	return c.Unit
}

func (c *AsciiGetSettingResult) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiGetSettingResult) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiGetSettingResult) Clone() (*AsciiGetSettingResult, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiGetSettingResult{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiGetSettingResult) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	return nil
}
