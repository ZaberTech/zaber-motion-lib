/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ProcessOn struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	On          bool    `bson:"on"`
	Duration    float64 `bson:"duration"`
	Unit        string  `bson:"unit"`
}

func (c *ProcessOn) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ProcessOn) GetDevice() int32 {
	return c.Device
}

func (c *ProcessOn) GetAxis() int32 {
	return c.Axis
}

func (c *ProcessOn) GetOn() bool {
	return c.On
}

func (c *ProcessOn) GetDuration() float64 {
	return c.Duration
}

func (c *ProcessOn) GetUnit() string {
	return c.Unit
}

func (c *ProcessOn) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ProcessOn) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ProcessOn) Clone() (*ProcessOn, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ProcessOn{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ProcessOn) Sanitize() error {
	return nil
}
