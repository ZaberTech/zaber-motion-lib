/* This file is generated. Do not modify by hand. */
package dto

// Comparison operator for trigger condition.
type AsciiTriggerCondition int32

const (
	AsciiTriggerCondition_EQ AsciiTriggerCondition = 0
	AsciiTriggerCondition_NE AsciiTriggerCondition = 1
	AsciiTriggerCondition_GT AsciiTriggerCondition = 2
	AsciiTriggerCondition_GE AsciiTriggerCondition = 3
	AsciiTriggerCondition_LT AsciiTriggerCondition = 4
	AsciiTriggerCondition_LE AsciiTriggerCondition = 5
)

var AsciiTriggerCondition_name = map[AsciiTriggerCondition]string{
	AsciiTriggerCondition_EQ: "EQ",
	AsciiTriggerCondition_NE: "NE",
	AsciiTriggerCondition_GT: "GT",
	AsciiTriggerCondition_GE: "GE",
	AsciiTriggerCondition_LT: "LT",
	AsciiTriggerCondition_LE: "LE",
}
