/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ToolsListSerialPortsResponse struct {
	Ports []string `bson:"ports"`
}

func (c *ToolsListSerialPortsResponse) GetPorts() []string {
	return c.Ports
}

func (c *ToolsListSerialPortsResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ToolsListSerialPortsResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ToolsListSerialPortsResponse) Clone() (*ToolsListSerialPortsResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ToolsListSerialPortsResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ToolsListSerialPortsResponse) Sanitize() error {
	if c.Ports == nil {
		c.Ports = make([]string, 0)
	}

	return nil
}
