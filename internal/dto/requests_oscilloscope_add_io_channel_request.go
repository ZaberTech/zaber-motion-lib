/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeAddIoChannelRequest struct {
	InterfaceId int32           `bson:"interfaceId"`
	Device      int32           `bson:"device"`
	IoType      AsciiIoPortType `bson:"ioType"`
	IoChannel   int32           `bson:"ioChannel"`
}

func (c *OscilloscopeAddIoChannelRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *OscilloscopeAddIoChannelRequest) GetDevice() int32 {
	return c.Device
}

func (c *OscilloscopeAddIoChannelRequest) GetIoType() AsciiIoPortType {
	return c.IoType
}

func (c *OscilloscopeAddIoChannelRequest) GetIoChannel() int32 {
	return c.IoChannel
}

func (c *OscilloscopeAddIoChannelRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeAddIoChannelRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeAddIoChannelRequest) Clone() (*OscilloscopeAddIoChannelRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeAddIoChannelRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeAddIoChannelRequest) Sanitize() error {
	return nil
}
