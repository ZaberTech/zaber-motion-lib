/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for CommandFailedException.
type ExceptionsCommandFailedExceptionData struct {
	// The command that got rejected.
	Command string `bson:"command"`
	// The data from the reply containing the rejection reason.
	ResponseData string `bson:"responseData"`
	// The flag indicating that the command was rejected.
	ReplyFlag string `bson:"replyFlag"`
	// The current device or axis status.
	Status string `bson:"status"`
	// The highest priority warning flag on the device or axis.
	WarningFlag string `bson:"warningFlag"`
	// The address of the device that rejected the command.
	DeviceAddress int32 `bson:"deviceAddress"`
	// The number of the axis which the rejection relates to.
	AxisNumber int32 `bson:"axisNumber"`
	// The message ID of the reply.
	Id int32 `bson:"id"`
}

// The command that got rejected.
func (c *ExceptionsCommandFailedExceptionData) GetCommand() string {
	return c.Command
}

// The data from the reply containing the rejection reason.
func (c *ExceptionsCommandFailedExceptionData) GetResponseData() string {
	return c.ResponseData
}

// The flag indicating that the command was rejected.
func (c *ExceptionsCommandFailedExceptionData) GetReplyFlag() string {
	return c.ReplyFlag
}

// The current device or axis status.
func (c *ExceptionsCommandFailedExceptionData) GetStatus() string {
	return c.Status
}

// The highest priority warning flag on the device or axis.
func (c *ExceptionsCommandFailedExceptionData) GetWarningFlag() string {
	return c.WarningFlag
}

// The address of the device that rejected the command.
func (c *ExceptionsCommandFailedExceptionData) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

// The number of the axis which the rejection relates to.
func (c *ExceptionsCommandFailedExceptionData) GetAxisNumber() int32 {
	return c.AxisNumber
}

// The message ID of the reply.
func (c *ExceptionsCommandFailedExceptionData) GetId() int32 {
	return c.Id
}

func (c *ExceptionsCommandFailedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsCommandFailedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsCommandFailedExceptionData) Clone() (*ExceptionsCommandFailedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsCommandFailedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsCommandFailedExceptionData) Sanitize() error {
	return nil
}
