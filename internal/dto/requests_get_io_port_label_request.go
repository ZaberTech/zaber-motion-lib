/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GetIoPortLabelRequest struct {
	InterfaceId   int32           `bson:"interfaceId"`
	Device        int32           `bson:"device"`
	PortType      AsciiIoPortType `bson:"portType"`
	ChannelNumber int32           `bson:"channelNumber"`
}

func (c *GetIoPortLabelRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *GetIoPortLabelRequest) GetDevice() int32 {
	return c.Device
}

func (c *GetIoPortLabelRequest) GetPortType() AsciiIoPortType {
	return c.PortType
}

func (c *GetIoPortLabelRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *GetIoPortLabelRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GetIoPortLabelRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GetIoPortLabelRequest) Clone() (*GetIoPortLabelRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GetIoPortLabelRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GetIoPortLabelRequest) Sanitize() error {
	return nil
}
