/* This file is generated. Do not modify by hand. */
package dto

// Named constants for all Zaber Binary protocol commands.
type BinaryCommandCode int32

const (
	BinaryCommandCode_RESET                                  BinaryCommandCode = 0
	BinaryCommandCode_HOME                                   BinaryCommandCode = 1
	BinaryCommandCode_RENUMBER                               BinaryCommandCode = 2
	BinaryCommandCode_STORE_CURRENT_POSITION                 BinaryCommandCode = 16
	BinaryCommandCode_RETURN_STORED_POSITION                 BinaryCommandCode = 17
	BinaryCommandCode_MOVE_TO_STORED_POSITION                BinaryCommandCode = 18
	BinaryCommandCode_MOVE_ABSOLUTE                          BinaryCommandCode = 20
	BinaryCommandCode_MOVE_RELATIVE                          BinaryCommandCode = 21
	BinaryCommandCode_MOVE_AT_CONSTANT_SPEED                 BinaryCommandCode = 22
	BinaryCommandCode_STOP                                   BinaryCommandCode = 23
	BinaryCommandCode_SET_ACTIVE_AXIS                        BinaryCommandCode = 25
	BinaryCommandCode_SET_AXIS_DEVICE_NUMBER                 BinaryCommandCode = 26
	BinaryCommandCode_SET_AXIS_INVERSION                     BinaryCommandCode = 27
	BinaryCommandCode_SET_AXIS_VELOCITY_PROFILE              BinaryCommandCode = 28
	BinaryCommandCode_SET_AXIS_VELOCITY_SCALE                BinaryCommandCode = 29
	BinaryCommandCode_LOAD_EVENT_INSTRUCTION                 BinaryCommandCode = 30
	BinaryCommandCode_RETURN_EVENT_INSTRUCTION               BinaryCommandCode = 31
	BinaryCommandCode_SET_CALIBRATION_MODE                   BinaryCommandCode = 33
	BinaryCommandCode_SET_JOYSTICK_CALIBRATION_MODE          BinaryCommandCode = 33
	BinaryCommandCode_READ_OR_WRITE_MEMORY                   BinaryCommandCode = 35
	BinaryCommandCode_RESTORE_SETTINGS                       BinaryCommandCode = 36
	BinaryCommandCode_SET_MICROSTEP_RESOLUTION               BinaryCommandCode = 37
	BinaryCommandCode_SET_RUNNING_CURRENT                    BinaryCommandCode = 38
	BinaryCommandCode_SET_HOLD_CURRENT                       BinaryCommandCode = 39
	BinaryCommandCode_SET_DEVICE_MODE                        BinaryCommandCode = 40
	BinaryCommandCode_SET_HOME_SPEED                         BinaryCommandCode = 41
	BinaryCommandCode_SET_START_SPEED                        BinaryCommandCode = 41
	BinaryCommandCode_SET_TARGET_SPEED                       BinaryCommandCode = 42
	BinaryCommandCode_SET_ACCELERATION                       BinaryCommandCode = 43
	BinaryCommandCode_SET_MAXIMUM_POSITION                   BinaryCommandCode = 44
	BinaryCommandCode_SET_CURRENT_POSITION                   BinaryCommandCode = 45
	BinaryCommandCode_SET_MAXIMUM_RELATIVE_MOVE              BinaryCommandCode = 46
	BinaryCommandCode_SET_HOME_OFFSET                        BinaryCommandCode = 47
	BinaryCommandCode_SET_ALIAS_NUMBER                       BinaryCommandCode = 48
	BinaryCommandCode_SET_LOCK_STATE                         BinaryCommandCode = 49
	BinaryCommandCode_RETURN_DEVICE_ID                       BinaryCommandCode = 50
	BinaryCommandCode_RETURN_FIRMWARE_VERSION                BinaryCommandCode = 51
	BinaryCommandCode_RETURN_POWER_SUPPLY_VOLTAGE            BinaryCommandCode = 52
	BinaryCommandCode_RETURN_SETTING                         BinaryCommandCode = 53
	BinaryCommandCode_RETURN_STATUS                          BinaryCommandCode = 54
	BinaryCommandCode_ECHO_DATA                              BinaryCommandCode = 55
	BinaryCommandCode_RETURN_FIRMWARE_BUILD                  BinaryCommandCode = 56
	BinaryCommandCode_RETURN_CURRENT_POSITION                BinaryCommandCode = 60
	BinaryCommandCode_RETURN_SERIAL_NUMBER                   BinaryCommandCode = 63
	BinaryCommandCode_SET_PARK_STATE                         BinaryCommandCode = 65
	BinaryCommandCode_SET_PERIPHERAL_ID                      BinaryCommandCode = 66
	BinaryCommandCode_RETURN_DIGITAL_INPUT_COUNT             BinaryCommandCode = 67
	BinaryCommandCode_READ_DIGITAL_INPUT                     BinaryCommandCode = 68
	BinaryCommandCode_READ_ALL_DIGITAL_INPUTS                BinaryCommandCode = 69
	BinaryCommandCode_RETURN_DIGITAL_OUTPUT_COUNT            BinaryCommandCode = 70
	BinaryCommandCode_READ_DIGITAL_OUTPUT                    BinaryCommandCode = 71
	BinaryCommandCode_READ_ALL_DIGITAL_OUTPUTS               BinaryCommandCode = 72
	BinaryCommandCode_WRITE_DIGITAL_OUTPUT                   BinaryCommandCode = 73
	BinaryCommandCode_WRITE_ALL_DIGITAL_OUTPUTS              BinaryCommandCode = 74
	BinaryCommandCode_RETURN_ANALOG_INPUT_COUNT              BinaryCommandCode = 75
	BinaryCommandCode_READ_ANALOG_INPUT                      BinaryCommandCode = 76
	BinaryCommandCode_RETURN_ANALOG_OUTPUT_COUNT             BinaryCommandCode = 77
	BinaryCommandCode_MOVE_INDEX                             BinaryCommandCode = 78
	BinaryCommandCode_SET_INDEX_DISTANCE                     BinaryCommandCode = 79
	BinaryCommandCode_SET_CYCLE_DISTANCE                     BinaryCommandCode = 80
	BinaryCommandCode_SET_FILTER_HOLDER_ID                   BinaryCommandCode = 81
	BinaryCommandCode_RETURN_ENCODER_COUNT                   BinaryCommandCode = 82
	BinaryCommandCode_RETURN_CALIBRATED_ENCODER_COUNT        BinaryCommandCode = 83
	BinaryCommandCode_RETURN_CALIBRATION_TYPE                BinaryCommandCode = 84
	BinaryCommandCode_RETURN_CALIBRATION_ERROR               BinaryCommandCode = 85
	BinaryCommandCode_RETURN_PERIPHERAL_SERIAL_NUMBER        BinaryCommandCode = 86
	BinaryCommandCode_SET_PERIPHERAL_SERIAL_NUMBER           BinaryCommandCode = 86
	BinaryCommandCode_FORCE_ABSOLUTE                         BinaryCommandCode = 87
	BinaryCommandCode_FORCE_OFF                              BinaryCommandCode = 88
	BinaryCommandCode_RETURN_ENCODER_POSITION                BinaryCommandCode = 89
	BinaryCommandCode_RETURN_PERIPHERAL_ID_PENDING           BinaryCommandCode = 91
	BinaryCommandCode_RETURN_PERIPHERAL_SERIAL_PENDING       BinaryCommandCode = 92
	BinaryCommandCode_ACTIVATE                               BinaryCommandCode = 93
	BinaryCommandCode_SET_AUTO_REPLY_DISABLED_MODE           BinaryCommandCode = 101
	BinaryCommandCode_SET_MESSAGE_ID_MODE                    BinaryCommandCode = 102
	BinaryCommandCode_SET_HOME_STATUS                        BinaryCommandCode = 103
	BinaryCommandCode_SET_HOME_SENSOR_TYPE                   BinaryCommandCode = 104
	BinaryCommandCode_SET_AUTO_HOME_DISABLED_MODE            BinaryCommandCode = 105
	BinaryCommandCode_SET_MINIMUM_POSITION                   BinaryCommandCode = 106
	BinaryCommandCode_SET_KNOB_DISABLED_MODE                 BinaryCommandCode = 107
	BinaryCommandCode_SET_KNOB_DIRECTION                     BinaryCommandCode = 108
	BinaryCommandCode_SET_KNOB_MOVEMENT_MODE                 BinaryCommandCode = 109
	BinaryCommandCode_SET_KNOB_JOG_SIZE                      BinaryCommandCode = 110
	BinaryCommandCode_SET_KNOB_VELOCITY_SCALE                BinaryCommandCode = 111
	BinaryCommandCode_SET_KNOB_VELOCITY_PROFILE              BinaryCommandCode = 112
	BinaryCommandCode_SET_ACCELERATION_ONLY                  BinaryCommandCode = 113
	BinaryCommandCode_SET_DECELERATION_ONLY                  BinaryCommandCode = 114
	BinaryCommandCode_SET_MOVE_TRACKING_MODE                 BinaryCommandCode = 115
	BinaryCommandCode_SET_MANUAL_MOVE_TRACKING_DISABLED_MODE BinaryCommandCode = 116
	BinaryCommandCode_SET_MOVE_TRACKING_PERIOD               BinaryCommandCode = 117
	BinaryCommandCode_SET_CLOSED_LOOP_MODE                   BinaryCommandCode = 118
	BinaryCommandCode_SET_SLIP_TRACKING_PERIOD               BinaryCommandCode = 119
	BinaryCommandCode_SET_STALL_TIMEOUT                      BinaryCommandCode = 120
	BinaryCommandCode_SET_DEVICE_DIRECTION                   BinaryCommandCode = 121
	BinaryCommandCode_SET_BAUD_RATE                          BinaryCommandCode = 122
	BinaryCommandCode_SET_PROTOCOL                           BinaryCommandCode = 123
	BinaryCommandCode_CONVERT_TO_ASCII                       BinaryCommandCode = 124
)

var BinaryCommandCode_name = map[BinaryCommandCode]string{
	BinaryCommandCode_RESET:                                  "Reset",
	BinaryCommandCode_HOME:                                   "Home",
	BinaryCommandCode_RENUMBER:                               "Renumber",
	BinaryCommandCode_STORE_CURRENT_POSITION:                 "StoreCurrentPosition",
	BinaryCommandCode_RETURN_STORED_POSITION:                 "ReturnStoredPosition",
	BinaryCommandCode_MOVE_TO_STORED_POSITION:                "MoveToStoredPosition",
	BinaryCommandCode_MOVE_ABSOLUTE:                          "MoveAbsolute",
	BinaryCommandCode_MOVE_RELATIVE:                          "MoveRelative",
	BinaryCommandCode_MOVE_AT_CONSTANT_SPEED:                 "MoveAtConstantSpeed",
	BinaryCommandCode_STOP:                                   "Stop",
	BinaryCommandCode_SET_ACTIVE_AXIS:                        "SetActiveAxis",
	BinaryCommandCode_SET_AXIS_DEVICE_NUMBER:                 "SetAxisDeviceNumber",
	BinaryCommandCode_SET_AXIS_INVERSION:                     "SetAxisInversion",
	BinaryCommandCode_SET_AXIS_VELOCITY_PROFILE:              "SetAxisVelocityProfile",
	BinaryCommandCode_SET_AXIS_VELOCITY_SCALE:                "SetAxisVelocityScale",
	BinaryCommandCode_LOAD_EVENT_INSTRUCTION:                 "LoadEventInstruction",
	BinaryCommandCode_RETURN_EVENT_INSTRUCTION:               "ReturnEventInstruction",
	BinaryCommandCode_SET_CALIBRATION_MODE:                   "SetCalibrationMode",
	BinaryCommandCode_READ_OR_WRITE_MEMORY:                   "ReadOrWriteMemory",
	BinaryCommandCode_RESTORE_SETTINGS:                       "RestoreSettings",
	BinaryCommandCode_SET_MICROSTEP_RESOLUTION:               "SetMicrostepResolution",
	BinaryCommandCode_SET_RUNNING_CURRENT:                    "SetRunningCurrent",
	BinaryCommandCode_SET_HOLD_CURRENT:                       "SetHoldCurrent",
	BinaryCommandCode_SET_DEVICE_MODE:                        "SetDeviceMode",
	BinaryCommandCode_SET_HOME_SPEED:                         "SetHomeSpeed",
	BinaryCommandCode_SET_TARGET_SPEED:                       "SetTargetSpeed",
	BinaryCommandCode_SET_ACCELERATION:                       "SetAcceleration",
	BinaryCommandCode_SET_MAXIMUM_POSITION:                   "SetMaximumPosition",
	BinaryCommandCode_SET_CURRENT_POSITION:                   "SetCurrentPosition",
	BinaryCommandCode_SET_MAXIMUM_RELATIVE_MOVE:              "SetMaximumRelativeMove",
	BinaryCommandCode_SET_HOME_OFFSET:                        "SetHomeOffset",
	BinaryCommandCode_SET_ALIAS_NUMBER:                       "SetAliasNumber",
	BinaryCommandCode_SET_LOCK_STATE:                         "SetLockState",
	BinaryCommandCode_RETURN_DEVICE_ID:                       "ReturnDeviceID",
	BinaryCommandCode_RETURN_FIRMWARE_VERSION:                "ReturnFirmwareVersion",
	BinaryCommandCode_RETURN_POWER_SUPPLY_VOLTAGE:            "ReturnPowerSupplyVoltage",
	BinaryCommandCode_RETURN_SETTING:                         "ReturnSetting",
	BinaryCommandCode_RETURN_STATUS:                          "ReturnStatus",
	BinaryCommandCode_ECHO_DATA:                              "EchoData",
	BinaryCommandCode_RETURN_FIRMWARE_BUILD:                  "ReturnFirmwareBuild",
	BinaryCommandCode_RETURN_CURRENT_POSITION:                "ReturnCurrentPosition",
	BinaryCommandCode_RETURN_SERIAL_NUMBER:                   "ReturnSerialNumber",
	BinaryCommandCode_SET_PARK_STATE:                         "SetParkState",
	BinaryCommandCode_SET_PERIPHERAL_ID:                      "SetPeripheralID",
	BinaryCommandCode_RETURN_DIGITAL_INPUT_COUNT:             "ReturnDigitalInputCount",
	BinaryCommandCode_READ_DIGITAL_INPUT:                     "ReadDigitalInput",
	BinaryCommandCode_READ_ALL_DIGITAL_INPUTS:                "ReadAllDigitalInputs",
	BinaryCommandCode_RETURN_DIGITAL_OUTPUT_COUNT:            "ReturnDigitalOutputCount",
	BinaryCommandCode_READ_DIGITAL_OUTPUT:                    "ReadDigitalOutput",
	BinaryCommandCode_READ_ALL_DIGITAL_OUTPUTS:               "ReadAllDigitalOutputs",
	BinaryCommandCode_WRITE_DIGITAL_OUTPUT:                   "WriteDigitalOutput",
	BinaryCommandCode_WRITE_ALL_DIGITAL_OUTPUTS:              "WriteAllDigitalOutputs",
	BinaryCommandCode_RETURN_ANALOG_INPUT_COUNT:              "ReturnAnalogInputCount",
	BinaryCommandCode_READ_ANALOG_INPUT:                      "ReadAnalogInput",
	BinaryCommandCode_RETURN_ANALOG_OUTPUT_COUNT:             "ReturnAnalogOutputCount",
	BinaryCommandCode_MOVE_INDEX:                             "MoveIndex",
	BinaryCommandCode_SET_INDEX_DISTANCE:                     "SetIndexDistance",
	BinaryCommandCode_SET_CYCLE_DISTANCE:                     "SetCycleDistance",
	BinaryCommandCode_SET_FILTER_HOLDER_ID:                   "SetFilterHolderID",
	BinaryCommandCode_RETURN_ENCODER_COUNT:                   "ReturnEncoderCount",
	BinaryCommandCode_RETURN_CALIBRATED_ENCODER_COUNT:        "ReturnCalibratedEncoderCount",
	BinaryCommandCode_RETURN_CALIBRATION_TYPE:                "ReturnCalibrationType",
	BinaryCommandCode_RETURN_CALIBRATION_ERROR:               "ReturnCalibrationError",
	BinaryCommandCode_RETURN_PERIPHERAL_SERIAL_NUMBER:        "ReturnPeripheralSerialNumber",
	BinaryCommandCode_FORCE_ABSOLUTE:                         "ForceAbsolute",
	BinaryCommandCode_FORCE_OFF:                              "ForceOff",
	BinaryCommandCode_RETURN_ENCODER_POSITION:                "ReturnEncoderPosition",
	BinaryCommandCode_RETURN_PERIPHERAL_ID_PENDING:           "ReturnPeripheralIDPending",
	BinaryCommandCode_RETURN_PERIPHERAL_SERIAL_PENDING:       "ReturnPeripheralSerialPending",
	BinaryCommandCode_ACTIVATE:                               "Activate",
	BinaryCommandCode_SET_AUTO_REPLY_DISABLED_MODE:           "SetAutoReplyDisabledMode",
	BinaryCommandCode_SET_MESSAGE_ID_MODE:                    "SetMessageIDMode",
	BinaryCommandCode_SET_HOME_STATUS:                        "SetHomeStatus",
	BinaryCommandCode_SET_HOME_SENSOR_TYPE:                   "SetHomeSensorType",
	BinaryCommandCode_SET_AUTO_HOME_DISABLED_MODE:            "SetAutoHomeDisabledMode",
	BinaryCommandCode_SET_MINIMUM_POSITION:                   "SetMinimumPosition",
	BinaryCommandCode_SET_KNOB_DISABLED_MODE:                 "SetKnobDisabledMode",
	BinaryCommandCode_SET_KNOB_DIRECTION:                     "SetKnobDirection",
	BinaryCommandCode_SET_KNOB_MOVEMENT_MODE:                 "SetKnobMovementMode",
	BinaryCommandCode_SET_KNOB_JOG_SIZE:                      "SetKnobJogSize",
	BinaryCommandCode_SET_KNOB_VELOCITY_SCALE:                "SetKnobVelocityScale",
	BinaryCommandCode_SET_KNOB_VELOCITY_PROFILE:              "SetKnobVelocityProfile",
	BinaryCommandCode_SET_ACCELERATION_ONLY:                  "SetAccelerationOnly",
	BinaryCommandCode_SET_DECELERATION_ONLY:                  "SetDecelerationOnly",
	BinaryCommandCode_SET_MOVE_TRACKING_MODE:                 "SetMoveTrackingMode",
	BinaryCommandCode_SET_MANUAL_MOVE_TRACKING_DISABLED_MODE: "SetManualMoveTrackingDisabledMode",
	BinaryCommandCode_SET_MOVE_TRACKING_PERIOD:               "SetMoveTrackingPeriod",
	BinaryCommandCode_SET_CLOSED_LOOP_MODE:                   "SetClosedLoopMode",
	BinaryCommandCode_SET_SLIP_TRACKING_PERIOD:               "SetSlipTrackingPeriod",
	BinaryCommandCode_SET_STALL_TIMEOUT:                      "SetStallTimeout",
	BinaryCommandCode_SET_DEVICE_DIRECTION:                   "SetDeviceDirection",
	BinaryCommandCode_SET_BAUD_RATE:                          "SetBaudRate",
	BinaryCommandCode_SET_PROTOCOL:                           "SetProtocol",
	BinaryCommandCode_CONVERT_TO_ASCII:                       "ConvertToASCII",
}
