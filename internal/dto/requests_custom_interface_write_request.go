/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CustomInterfaceWriteRequest struct {
	TransportId int32  `bson:"transportId"`
	Message     string `bson:"message"`
}

func (c *CustomInterfaceWriteRequest) GetTransportId() int32 {
	return c.TransportId
}

func (c *CustomInterfaceWriteRequest) GetMessage() string {
	return c.Message
}

func (c *CustomInterfaceWriteRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CustomInterfaceWriteRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CustomInterfaceWriteRequest) Clone() (*CustomInterfaceWriteRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CustomInterfaceWriteRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CustomInterfaceWriteRequest) Sanitize() error {
	return nil
}
