/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ToggleDeviceDbStoreRequest struct {
	ToggleOn      bool    `bson:"toggleOn"`
	StoreLocation *string `bson:"storeLocation"`
}

func (c *ToggleDeviceDbStoreRequest) GetToggleOn() bool {
	return c.ToggleOn
}

func (c *ToggleDeviceDbStoreRequest) GetStoreLocation() *string {
	return c.StoreLocation
}

func (c *ToggleDeviceDbStoreRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ToggleDeviceDbStoreRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ToggleDeviceDbStoreRequest) Clone() (*ToggleDeviceDbStoreRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ToggleDeviceDbStoreRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ToggleDeviceDbStoreRequest) Sanitize() error {
	return nil
}
