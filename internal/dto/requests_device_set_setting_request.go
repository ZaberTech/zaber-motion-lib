/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetSettingRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Setting     string  `bson:"setting"`
	Value       float64 `bson:"value"`
	Unit        string  `bson:"unit"`
}

func (c *DeviceSetSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *DeviceSetSettingRequest) GetValue() float64 {
	return c.Value
}

func (c *DeviceSetSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetSettingRequest) Clone() (*DeviceSetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetSettingRequest) Sanitize() error {
	return nil
}
