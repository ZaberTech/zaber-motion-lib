/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorCreateLiveRequest struct {
	InterfaceId int32                  `bson:"interfaceId"`
	Device      int32                  `bson:"device"`
	StreamId    int32                  `bson:"streamId"`
	Config      *GcodeTranslatorConfig `bson:"config"`
}

func (c *TranslatorCreateLiveRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TranslatorCreateLiveRequest) GetDevice() int32 {
	return c.Device
}

func (c *TranslatorCreateLiveRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *TranslatorCreateLiveRequest) GetConfig() *GcodeTranslatorConfig {
	return c.Config
}

func (c *TranslatorCreateLiveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorCreateLiveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorCreateLiveRequest) Clone() (*TranslatorCreateLiveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorCreateLiveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorCreateLiveRequest) Sanitize() error {
	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
