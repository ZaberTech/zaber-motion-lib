/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OpenBinaryInterfaceRequest struct {
	InterfaceType InterfaceType `bson:"interfaceType"`
	PortName      string        `bson:"portName"`
	BaudRate      int32         `bson:"baudRate"`
	HostName      string        `bson:"hostName"`
	Port          int32         `bson:"port"`
	UseMessageIds bool          `bson:"useMessageIds"`
}

func (c *OpenBinaryInterfaceRequest) GetInterfaceType() InterfaceType {
	return c.InterfaceType
}

func (c *OpenBinaryInterfaceRequest) GetPortName() string {
	return c.PortName
}

func (c *OpenBinaryInterfaceRequest) GetBaudRate() int32 {
	return c.BaudRate
}

func (c *OpenBinaryInterfaceRequest) GetHostName() string {
	return c.HostName
}

func (c *OpenBinaryInterfaceRequest) GetPort() int32 {
	return c.Port
}

func (c *OpenBinaryInterfaceRequest) GetUseMessageIds() bool {
	return c.UseMessageIds
}

func (c *OpenBinaryInterfaceRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OpenBinaryInterfaceRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OpenBinaryInterfaceRequest) Clone() (*OpenBinaryInterfaceRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OpenBinaryInterfaceRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OpenBinaryInterfaceRequest) Sanitize() error {
	return nil
}
