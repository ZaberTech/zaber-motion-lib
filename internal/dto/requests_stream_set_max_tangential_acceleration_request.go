/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetMaxTangentialAccelerationRequest struct {
	InterfaceId               int32   `bson:"interfaceId"`
	Device                    int32   `bson:"device"`
	StreamId                  int32   `bson:"streamId"`
	Pvt                       bool    `bson:"pvt"`
	MaxTangentialAcceleration float64 `bson:"maxTangentialAcceleration"`
	Unit                      string  `bson:"unit"`
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetMaxTangentialAcceleration() float64 {
	return c.MaxTangentialAcceleration
}

func (c *StreamSetMaxTangentialAccelerationRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetMaxTangentialAccelerationRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetMaxTangentialAccelerationRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetMaxTangentialAccelerationRequest) Clone() (*StreamSetMaxTangentialAccelerationRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetMaxTangentialAccelerationRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetMaxTangentialAccelerationRequest) Sanitize() error {
	return nil
}
