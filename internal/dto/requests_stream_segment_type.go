/* This file is generated. Do not modify by hand. */
package dto

type StreamSegmentType int32

const (
	StreamSegmentType_ABS StreamSegmentType = 0
	StreamSegmentType_REL StreamSegmentType = 1
)

var StreamSegmentType_name = map[StreamSegmentType]string{
	StreamSegmentType_ABS: "Abs",
	StreamSegmentType_REL: "Rel",
}
