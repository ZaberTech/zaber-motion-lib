/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetServoTuningRequest struct {
	InterfaceId             int32                    `bson:"interfaceId"`
	Device                  int32                    `bson:"device"`
	Axis                    int32                    `bson:"axis"`
	Paramset                AsciiServoTuningParamset `bson:"paramset"`
	TuningParams            []*AsciiServoTuningParam `bson:"tuningParams"`
	SetUnspecifiedToDefault bool                     `bson:"setUnspecifiedToDefault"`
}

func (c *SetServoTuningRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetServoTuningRequest) GetDevice() int32 {
	return c.Device
}

func (c *SetServoTuningRequest) GetAxis() int32 {
	return c.Axis
}

func (c *SetServoTuningRequest) GetParamset() AsciiServoTuningParamset {
	return c.Paramset
}

func (c *SetServoTuningRequest) GetTuningParams() []*AsciiServoTuningParam {
	return c.TuningParams
}

func (c *SetServoTuningRequest) GetSetUnspecifiedToDefault() bool {
	return c.SetUnspecifiedToDefault
}

func (c *SetServoTuningRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetServoTuningRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetServoTuningRequest) Clone() (*SetServoTuningRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetServoTuningRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetServoTuningRequest) Sanitize() error {
	if c.TuningParams == nil {
		c.TuningParams = make([]*AsciiServoTuningParam, 0)
	}

	for _, item := range c.TuningParams {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property TuningParams of SetServoTuningRequest is nil")
		}
	}

	return nil
}
