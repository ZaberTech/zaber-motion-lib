/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAllAnalogOutputsRequest struct {
	InterfaceId int32     `bson:"interfaceId"`
	Device      int32     `bson:"device"`
	StreamId    int32     `bson:"streamId"`
	Pvt         bool      `bson:"pvt"`
	Values      []float64 `bson:"values"`
}

func (c *StreamSetAllAnalogOutputsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAllAnalogOutputsRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAllAnalogOutputsRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAllAnalogOutputsRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAllAnalogOutputsRequest) GetValues() []float64 {
	return c.Values
}

func (c *StreamSetAllAnalogOutputsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAllAnalogOutputsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAllAnalogOutputsRequest) Clone() (*StreamSetAllAnalogOutputsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAllAnalogOutputsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAllAnalogOutputsRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	return nil
}
