/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Event that is sent when a connection is lost.
type DisconnectedEvent struct {
	// The id of the interface that was disconnected.
	InterfaceId int32 `bson:"interfaceId"`
	// The type of error that caused the disconnection.
	ErrorType Errors `bson:"errorType"`
	// The message describing the error.
	ErrorMessage string `bson:"errorMessage"`
}

// The id of the interface that was disconnected.
func (c *DisconnectedEvent) GetInterfaceId() int32 {
	return c.InterfaceId
}

// The type of error that caused the disconnection.
func (c *DisconnectedEvent) GetErrorType() Errors {
	return c.ErrorType
}

// The message describing the error.
func (c *DisconnectedEvent) GetErrorMessage() string {
	return c.ErrorMessage
}

func (c *DisconnectedEvent) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DisconnectedEvent) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DisconnectedEvent) Clone() (*DisconnectedEvent, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DisconnectedEvent{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DisconnectedEvent) Sanitize() error {
	return nil
}
