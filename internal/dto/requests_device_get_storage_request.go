/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetStorageRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Key         string `bson:"key"`
	Decode      bool   `bson:"decode"`
}

func (c *DeviceGetStorageRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetStorageRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetStorageRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceGetStorageRequest) GetKey() string {
	return c.Key
}

func (c *DeviceGetStorageRequest) GetDecode() bool {
	return c.Decode
}

func (c *DeviceGetStorageRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetStorageRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetStorageRequest) Clone() (*DeviceGetStorageRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetStorageRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetStorageRequest) Sanitize() error {
	return nil
}
