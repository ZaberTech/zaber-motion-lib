/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamArcRequest struct {
	InterfaceId       int32             `bson:"interfaceId"`
	Device            int32             `bson:"device"`
	StreamId          int32             `bson:"streamId"`
	Pvt               bool              `bson:"pvt"`
	Type              StreamSegmentType `bson:"type"`
	RotationDirection RotationDirection `bson:"rotationDirection"`
	CenterX           *Measurement      `bson:"centerX"`
	CenterY           *Measurement      `bson:"centerY"`
	EndX              *Measurement      `bson:"endX"`
	EndY              *Measurement      `bson:"endY"`
	TargetAxesIndices []int32           `bson:"targetAxesIndices"`
	Endpoint          []*Measurement    `bson:"endpoint"`
}

func (c *StreamArcRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamArcRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamArcRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamArcRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamArcRequest) GetType() StreamSegmentType {
	return c.Type
}

func (c *StreamArcRequest) GetRotationDirection() RotationDirection {
	return c.RotationDirection
}

func (c *StreamArcRequest) GetCenterX() *Measurement {
	return c.CenterX
}

func (c *StreamArcRequest) GetCenterY() *Measurement {
	return c.CenterY
}

func (c *StreamArcRequest) GetEndX() *Measurement {
	return c.EndX
}

func (c *StreamArcRequest) GetEndY() *Measurement {
	return c.EndY
}

func (c *StreamArcRequest) GetTargetAxesIndices() []int32 {
	return c.TargetAxesIndices
}

func (c *StreamArcRequest) GetEndpoint() []*Measurement {
	return c.Endpoint
}

func (c *StreamArcRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamArcRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamArcRequest) Clone() (*StreamArcRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamArcRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamArcRequest) Sanitize() error {
	if c.CenterX != nil {
		if err := c.CenterX.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property CenterX of StreamArcRequest is nil")
	}

	if c.CenterY != nil {
		if err := c.CenterY.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property CenterY of StreamArcRequest is nil")
	}

	if c.EndX != nil {
		if err := c.EndX.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property EndX of StreamArcRequest is nil")
	}

	if c.EndY != nil {
		if err := c.EndY.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property EndY of StreamArcRequest is nil")
	}

	if c.TargetAxesIndices == nil {
		c.TargetAxesIndices = make([]int32, 0)
	}

	if c.Endpoint == nil {
		c.Endpoint = make([]*Measurement, 0)
	}

	for _, item := range c.Endpoint {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Endpoint of StreamArcRequest is nil")
		}
	}

	return nil
}
