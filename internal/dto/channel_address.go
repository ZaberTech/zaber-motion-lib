/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Holds device address and IO channel number.
type ChannelAddress struct {
	// Device address.
	Device int32 `bson:"device"`
	// IO channel number.
	Channel int32 `bson:"channel"`
}

// Device address.
func (c *ChannelAddress) GetDevice() int32 {
	return c.Device
}

// IO channel number.
func (c *ChannelAddress) GetChannel() int32 {
	return c.Channel
}

func (c *ChannelAddress) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ChannelAddress) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ChannelAddress) Clone() (*ChannelAddress, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ChannelAddress{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ChannelAddress) Sanitize() error {
	return nil
}
