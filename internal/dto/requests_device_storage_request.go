/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceStorageRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Key         string `bson:"key"`
}

func (c *DeviceStorageRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceStorageRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceStorageRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceStorageRequest) GetKey() string {
	return c.Key
}

func (c *DeviceStorageRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceStorageRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceStorageRequest) Clone() (*DeviceStorageRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceStorageRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceStorageRequest) Sanitize() error {
	return nil
}
