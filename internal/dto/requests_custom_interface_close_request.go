/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CustomInterfaceCloseRequest struct {
	TransportId  int32  `bson:"transportId"`
	ErrorMessage string `bson:"errorMessage"`
}

func (c *CustomInterfaceCloseRequest) GetTransportId() int32 {
	return c.TransportId
}

func (c *CustomInterfaceCloseRequest) GetErrorMessage() string {
	return c.ErrorMessage
}

func (c *CustomInterfaceCloseRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CustomInterfaceCloseRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CustomInterfaceCloseRequest) Clone() (*CustomInterfaceCloseRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CustomInterfaceCloseRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CustomInterfaceCloseRequest) Sanitize() error {
	return nil
}
