/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DoubleArrayResponse struct {
	Values []float64 `bson:"values"`
}

func (c *DoubleArrayResponse) GetValues() []float64 {
	return c.Values
}

func (c *DoubleArrayResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DoubleArrayResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DoubleArrayResponse) Clone() (*DoubleArrayResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DoubleArrayResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DoubleArrayResponse) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	return nil
}
