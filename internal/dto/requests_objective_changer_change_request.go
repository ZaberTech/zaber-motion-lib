/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ObjectiveChangerChangeRequest struct {
	InterfaceId   int32        `bson:"interfaceId"`
	TurretAddress int32        `bson:"turretAddress"`
	FocusAddress  int32        `bson:"focusAddress"`
	FocusAxis     int32        `bson:"focusAxis"`
	Objective     int32        `bson:"objective"`
	FocusOffset   *Measurement `bson:"focusOffset"`
}

func (c *ObjectiveChangerChangeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ObjectiveChangerChangeRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *ObjectiveChangerChangeRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *ObjectiveChangerChangeRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *ObjectiveChangerChangeRequest) GetObjective() int32 {
	return c.Objective
}

func (c *ObjectiveChangerChangeRequest) GetFocusOffset() *Measurement {
	return c.FocusOffset
}

func (c *ObjectiveChangerChangeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ObjectiveChangerChangeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ObjectiveChangerChangeRequest) Clone() (*ObjectiveChangerChangeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ObjectiveChangerChangeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ObjectiveChangerChangeRequest) Sanitize() error {
	if c.FocusOffset != nil {
		if err := c.FocusOffset.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
