/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceHomeRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Timeout     float64 `bson:"timeout"`
	Unit        string  `bson:"unit"`
}

func (c *BinaryDeviceHomeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceHomeRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryDeviceHomeRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *BinaryDeviceHomeRequest) GetUnit() string {
	return c.Unit
}

func (c *BinaryDeviceHomeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceHomeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceHomeRequest) Clone() (*BinaryDeviceHomeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceHomeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceHomeRequest) Sanitize() error {
	return nil
}
