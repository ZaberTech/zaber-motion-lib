/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GetSettingResults struct {
	Results []*AsciiGetSettingResult `bson:"results"`
}

func (c *GetSettingResults) GetResults() []*AsciiGetSettingResult {
	return c.Results
}

func (c *GetSettingResults) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GetSettingResults) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GetSettingResults) Clone() (*GetSettingResults, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GetSettingResults{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GetSettingResults) Sanitize() error {
	if c.Results == nil {
		c.Results = make([]*AsciiGetSettingResult, 0)
	}

	for _, item := range c.Results {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Results of GetSettingResults is nil")
		}
	}

	return nil
}
