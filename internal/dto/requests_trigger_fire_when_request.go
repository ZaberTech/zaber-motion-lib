/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerFireWhenRequest struct {
	InterfaceId   int32  `bson:"interfaceId"`
	Device        int32  `bson:"device"`
	TriggerNumber int32  `bson:"triggerNumber"`
	Condition     string `bson:"condition"`
}

func (c *TriggerFireWhenRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerFireWhenRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerFireWhenRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerFireWhenRequest) GetCondition() string {
	return c.Condition
}

func (c *TriggerFireWhenRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerFireWhenRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerFireWhenRequest) Clone() (*TriggerFireWhenRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerFireWhenRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerFireWhenRequest) Sanitize() error {
	return nil
}
