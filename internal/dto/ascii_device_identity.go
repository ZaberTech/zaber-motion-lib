/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Representation of data gathered during device identification.
type AsciiDeviceIdentity struct {
	// Unique ID of the device hardware.
	DeviceId int32 `bson:"deviceId"`
	// Serial number of the device.
	SerialNumber uint32 `bson:"serialNumber"`
	// Name of the product.
	Name string `bson:"name"`
	// Number of axes this device has.
	AxisCount int32 `bson:"axisCount"`
	// Version of the firmware.
	FirmwareVersion *FirmwareVersion `bson:"firmwareVersion"`
	// The device has hardware modifications.
	IsModified bool `bson:"isModified"`
	// The device is an integrated product.
	IsIntegrated bool `bson:"isIntegrated"`
}

// Unique ID of the device hardware.
func (c *AsciiDeviceIdentity) GetDeviceId() int32 {
	return c.DeviceId
}

// Serial number of the device.
func (c *AsciiDeviceIdentity) GetSerialNumber() uint32 {
	return c.SerialNumber
}

// Name of the product.
func (c *AsciiDeviceIdentity) GetName() string {
	return c.Name
}

// Number of axes this device has.
func (c *AsciiDeviceIdentity) GetAxisCount() int32 {
	return c.AxisCount
}

// Version of the firmware.
func (c *AsciiDeviceIdentity) GetFirmwareVersion() *FirmwareVersion {
	return c.FirmwareVersion
}

// The device has hardware modifications.
func (c *AsciiDeviceIdentity) GetIsModified() bool {
	return c.IsModified
}

// The device is an integrated product.
func (c *AsciiDeviceIdentity) GetIsIntegrated() bool {
	return c.IsIntegrated
}

func (c *AsciiDeviceIdentity) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiDeviceIdentity) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiDeviceIdentity) Clone() (*AsciiDeviceIdentity, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiDeviceIdentity{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiDeviceIdentity) Sanitize() error {
	if c.FirmwareVersion != nil {
		if err := c.FirmwareVersion.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property FirmwareVersion of DeviceIdentity is nil")
	}

	return nil
}
