/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ObjectiveChangerGetCurrentResponse struct {
	Value int32 `bson:"value"`
}

func (c *ObjectiveChangerGetCurrentResponse) GetValue() int32 {
	return c.Value
}

func (c *ObjectiveChangerGetCurrentResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ObjectiveChangerGetCurrentResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ObjectiveChangerGetCurrentResponse) Clone() (*ObjectiveChangerGetCurrentResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ObjectiveChangerGetCurrentResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ObjectiveChangerGetCurrentResponse) Sanitize() error {
	return nil
}
