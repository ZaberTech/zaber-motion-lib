/* This file is generated. Do not modify by hand. */
package dto

// Kind of channel to record in the Oscilloscope.
type AsciiOscilloscopeDataSource int32

const (
	AsciiOscilloscopeDataSource_SETTING AsciiOscilloscopeDataSource = 0
	AsciiOscilloscopeDataSource_IO      AsciiOscilloscopeDataSource = 1
)

var AsciiOscilloscopeDataSource_name = map[AsciiOscilloscopeDataSource]string{
	AsciiOscilloscopeDataSource_SETTING: "Setting",
	AsciiOscilloscopeDataSource_IO:      "Io",
}
