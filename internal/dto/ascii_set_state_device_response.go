/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// An object containing any non-blocking issues encountered when loading a saved state to a device.
type AsciiSetStateDeviceResponse struct {
	// The warnings encountered when applying this state to the given device.
	Warnings []string `bson:"warnings"`
	// A list of warnings encountered when applying this state to the device's axes.
	AxisResponses []*AsciiSetStateAxisResponse `bson:"axisResponses"`
}

// The warnings encountered when applying this state to the given device.
func (c *AsciiSetStateDeviceResponse) GetWarnings() []string {
	return c.Warnings
}

// A list of warnings encountered when applying this state to the device's axes.
func (c *AsciiSetStateDeviceResponse) GetAxisResponses() []*AsciiSetStateAxisResponse {
	return c.AxisResponses
}

func (c *AsciiSetStateDeviceResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiSetStateDeviceResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiSetStateDeviceResponse) Clone() (*AsciiSetStateDeviceResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiSetStateDeviceResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiSetStateDeviceResponse) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	if c.AxisResponses == nil {
		c.AxisResponses = make([]*AsciiSetStateAxisResponse, 0)
	}

	for _, item := range c.AxisResponses {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property AxisResponses of SetStateDeviceResponse is nil")
		}
	}

	return nil
}
