/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Defines an axis of the translator.
type GcodeAxisDefinition struct {
	// ID of the peripheral.
	PeripheralId int32 `bson:"peripheralId"`
	// Microstep resolution of the axis.
	// Can be obtained by reading the resolution setting.
	// Leave empty if the axis does not have the setting.
	MicrostepResolution *int32 `bson:"microstepResolution"`
}

// ID of the peripheral.
func (c *GcodeAxisDefinition) GetPeripheralId() int32 {
	return c.PeripheralId
}

// Microstep resolution of the axis.
// Can be obtained by reading the resolution setting.
// Leave empty if the axis does not have the setting.
func (c *GcodeAxisDefinition) GetMicrostepResolution() *int32 {
	return c.MicrostepResolution
}

func (c *GcodeAxisDefinition) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeAxisDefinition) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeAxisDefinition) Clone() (*GcodeAxisDefinition, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeAxisDefinition{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeAxisDefinition) Sanitize() error {
	return nil
}
