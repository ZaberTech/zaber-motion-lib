/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamGetAxesResponse struct {
	Axes    []*AsciiStreamAxisDefinition `bson:"axes"`
	PvtAxes []*AsciiPvtAxisDefinition    `bson:"pvtAxes"`
}

func (c *StreamGetAxesResponse) GetAxes() []*AsciiStreamAxisDefinition {
	return c.Axes
}

func (c *StreamGetAxesResponse) GetPvtAxes() []*AsciiPvtAxisDefinition {
	return c.PvtAxes
}

func (c *StreamGetAxesResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamGetAxesResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamGetAxesResponse) Clone() (*StreamGetAxesResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamGetAxesResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamGetAxesResponse) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]*AsciiStreamAxisDefinition, 0)
	}

	for _, item := range c.Axes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Axes of StreamGetAxesResponse is nil")
		}
	}

	if c.PvtAxes == nil {
		c.PvtAxes = make([]*AsciiPvtAxisDefinition, 0)
	}

	for _, item := range c.PvtAxes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property PvtAxes of StreamGetAxesResponse is nil")
		}
	}

	return nil
}
