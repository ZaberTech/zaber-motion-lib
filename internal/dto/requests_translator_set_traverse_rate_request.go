/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorSetTraverseRateRequest struct {
	TranslatorId int32   `bson:"translatorId"`
	TraverseRate float64 `bson:"traverseRate"`
	Unit         string  `bson:"unit"`
}

func (c *TranslatorSetTraverseRateRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorSetTraverseRateRequest) GetTraverseRate() float64 {
	return c.TraverseRate
}

func (c *TranslatorSetTraverseRateRequest) GetUnit() string {
	return c.Unit
}

func (c *TranslatorSetTraverseRateRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorSetTraverseRateRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorSetTraverseRateRequest) Clone() (*TranslatorSetTraverseRateRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorSetTraverseRateRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorSetTraverseRateRequest) Sanitize() error {
	return nil
}
