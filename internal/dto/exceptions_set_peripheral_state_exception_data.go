/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for a SetPeripheralStateFailedException.
type ExceptionsSetPeripheralStateExceptionData struct {
	// The number of axis where the exception originated.
	AxisNumber int32 `bson:"axisNumber"`
	// A list of settings which could not be set.
	Settings []string `bson:"settings"`
	// The reason servo tuning could not be set.
	ServoTuning string `bson:"servoTuning"`
	// The reasons stored positions could not be set.
	StoredPositions []string `bson:"storedPositions"`
	// The reasons storage could not be set.
	Storage []string `bson:"storage"`
}

// The number of axis where the exception originated.
func (c *ExceptionsSetPeripheralStateExceptionData) GetAxisNumber() int32 {
	return c.AxisNumber
}

// A list of settings which could not be set.
func (c *ExceptionsSetPeripheralStateExceptionData) GetSettings() []string {
	return c.Settings
}

// The reason servo tuning could not be set.
func (c *ExceptionsSetPeripheralStateExceptionData) GetServoTuning() string {
	return c.ServoTuning
}

// The reasons stored positions could not be set.
func (c *ExceptionsSetPeripheralStateExceptionData) GetStoredPositions() []string {
	return c.StoredPositions
}

// The reasons storage could not be set.
func (c *ExceptionsSetPeripheralStateExceptionData) GetStorage() []string {
	return c.Storage
}

func (c *ExceptionsSetPeripheralStateExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsSetPeripheralStateExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsSetPeripheralStateExceptionData) Clone() (*ExceptionsSetPeripheralStateExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsSetPeripheralStateExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsSetPeripheralStateExceptionData) Sanitize() error {
	if c.Settings == nil {
		c.Settings = make([]string, 0)
	}

	if c.StoredPositions == nil {
		c.StoredPositions = make([]string, 0)
	}

	if c.Storage == nil {
		c.Storage = make([]string, 0)
	}

	return nil
}
