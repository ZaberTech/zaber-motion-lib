/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceStopRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	Axis          int32 `bson:"axis"`
	WaitUntilIdle bool  `bson:"waitUntilIdle"`
}

func (c *DeviceStopRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceStopRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceStopRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceStopRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *DeviceStopRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceStopRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceStopRequest) Clone() (*DeviceStopRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceStopRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceStopRequest) Sanitize() error {
	return nil
}
