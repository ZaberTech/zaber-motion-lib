/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetIoPortLabelRequest struct {
	InterfaceId   int32           `bson:"interfaceId"`
	Device        int32           `bson:"device"`
	PortType      AsciiIoPortType `bson:"portType"`
	ChannelNumber int32           `bson:"channelNumber"`
	Label         *string         `bson:"label"`
}

func (c *SetIoPortLabelRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetIoPortLabelRequest) GetDevice() int32 {
	return c.Device
}

func (c *SetIoPortLabelRequest) GetPortType() AsciiIoPortType {
	return c.PortType
}

func (c *SetIoPortLabelRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *SetIoPortLabelRequest) GetLabel() *string {
	return c.Label
}

func (c *SetIoPortLabelRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetIoPortLabelRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetIoPortLabelRequest) Clone() (*SetIoPortLabelRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetIoPortLabelRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetIoPortLabelRequest) Sanitize() error {
	return nil
}
