/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerEnableRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	TriggerNumber int32 `bson:"triggerNumber"`
	Count         int32 `bson:"count"`
}

func (c *TriggerEnableRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerEnableRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerEnableRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerEnableRequest) GetCount() int32 {
	return c.Count
}

func (c *TriggerEnableRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerEnableRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerEnableRequest) Clone() (*TriggerEnableRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerEnableRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerEnableRequest) Sanitize() error {
	return nil
}
