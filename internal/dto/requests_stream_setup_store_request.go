/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetupStoreRequest struct {
	InterfaceId  int32   `bson:"interfaceId"`
	Device       int32   `bson:"device"`
	StreamId     int32   `bson:"streamId"`
	Pvt          bool    `bson:"pvt"`
	StreamBuffer int32   `bson:"streamBuffer"`
	PvtBuffer    int32   `bson:"pvtBuffer"`
	Axes         []int32 `bson:"axes"`
}

func (c *StreamSetupStoreRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetupStoreRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetupStoreRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetupStoreRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetupStoreRequest) GetStreamBuffer() int32 {
	return c.StreamBuffer
}

func (c *StreamSetupStoreRequest) GetPvtBuffer() int32 {
	return c.PvtBuffer
}

func (c *StreamSetupStoreRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *StreamSetupStoreRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetupStoreRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetupStoreRequest) Clone() (*StreamSetupStoreRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetupStoreRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetupStoreRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	return nil
}
