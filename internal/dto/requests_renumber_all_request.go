/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type RenumberAllRequest struct {
	InterfaceId  int32 `bson:"interfaceId"`
	FirstAddress int32 `bson:"firstAddress"`
}

func (c *RenumberAllRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *RenumberAllRequest) GetFirstAddress() int32 {
	return c.FirstAddress
}

func (c *RenumberAllRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *RenumberAllRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *RenumberAllRequest) Clone() (*RenumberAllRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &RenumberAllRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *RenumberAllRequest) Sanitize() error {
	return nil
}
