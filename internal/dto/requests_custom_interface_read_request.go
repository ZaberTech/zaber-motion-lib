/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CustomInterfaceReadRequest struct {
	TransportId int32 `bson:"transportId"`
}

func (c *CustomInterfaceReadRequest) GetTransportId() int32 {
	return c.TransportId
}

func (c *CustomInterfaceReadRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CustomInterfaceReadRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CustomInterfaceReadRequest) Clone() (*CustomInterfaceReadRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CustomInterfaceReadRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CustomInterfaceReadRequest) Sanitize() error {
	return nil
}
