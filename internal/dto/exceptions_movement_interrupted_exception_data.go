/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for MovementInterruptedException.
type ExceptionsMovementInterruptedExceptionData struct {
	// The full list of warnings.
	Warnings []string `bson:"warnings"`
	// The reason for the Exception.
	Reason string `bson:"reason"`
	// The address of the device that caused the interruption.
	Device int32 `bson:"device"`
	// The number of the axis that caused the interruption.
	Axis int32 `bson:"axis"`
}

// The full list of warnings.
func (c *ExceptionsMovementInterruptedExceptionData) GetWarnings() []string {
	return c.Warnings
}

// The reason for the Exception.
func (c *ExceptionsMovementInterruptedExceptionData) GetReason() string {
	return c.Reason
}

// The address of the device that caused the interruption.
func (c *ExceptionsMovementInterruptedExceptionData) GetDevice() int32 {
	return c.Device
}

// The number of the axis that caused the interruption.
func (c *ExceptionsMovementInterruptedExceptionData) GetAxis() int32 {
	return c.Axis
}

func (c *ExceptionsMovementInterruptedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsMovementInterruptedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsMovementInterruptedExceptionData) Clone() (*ExceptionsMovementInterruptedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsMovementInterruptedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsMovementInterruptedExceptionData) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	return nil
}
