/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepEmptyRequest struct {
	InterfaceId     int32 `bson:"interfaceId"`
	Device          int32 `bson:"device"`
	LockstepGroupId int32 `bson:"lockstepGroupId"`
}

func (c *LockstepEmptyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepEmptyRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepEmptyRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepEmptyRequest) Clone() (*LockstepEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepEmptyRequest) Sanitize() error {
	return nil
}
