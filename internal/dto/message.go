package dto

import "errors"

type Message interface {
	ToBytes() ([]byte, error)
	FromBytes(b []byte) error
}

func makeError(err string) error {
	return errors.New(err)
}
