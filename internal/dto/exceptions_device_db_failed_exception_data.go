/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for a DeviceDbFailedException.
type ExceptionsDeviceDbFailedExceptionData struct {
	// Code describing type of the error.
	Code string `bson:"code"`
}

// Code describing type of the error.
func (c *ExceptionsDeviceDbFailedExceptionData) GetCode() string {
	return c.Code
}

func (c *ExceptionsDeviceDbFailedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsDeviceDbFailedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsDeviceDbFailedExceptionData) Clone() (*ExceptionsDeviceDbFailedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsDeviceDbFailedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsDeviceDbFailedExceptionData) Sanitize() error {
	return nil
}
