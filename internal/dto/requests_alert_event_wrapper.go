/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AlertEventWrapper struct {
	InterfaceId int32            `bson:"interfaceId"`
	Alert       *AsciiAlertEvent `bson:"alert"`
}

func (c *AlertEventWrapper) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AlertEventWrapper) GetAlert() *AsciiAlertEvent {
	return c.Alert
}

func (c *AlertEventWrapper) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AlertEventWrapper) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AlertEventWrapper) Clone() (*AlertEventWrapper, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AlertEventWrapper{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AlertEventWrapper) Sanitize() error {
	if c.Alert != nil {
		if err := c.Alert.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Alert of AlertEventWrapper is nil")
	}

	return nil
}
