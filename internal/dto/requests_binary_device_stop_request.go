/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceStopRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Timeout     float64 `bson:"timeout"`
	Unit        string  `bson:"unit"`
}

func (c *BinaryDeviceStopRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceStopRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryDeviceStopRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *BinaryDeviceStopRequest) GetUnit() string {
	return c.Unit
}

func (c *BinaryDeviceStopRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceStopRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceStopRequest) Clone() (*BinaryDeviceStopRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceStopRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceStopRequest) Sanitize() error {
	return nil
}
