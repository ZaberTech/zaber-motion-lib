/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type MicroscopeTriggerCameraRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	ChannelNumber int32   `bson:"channelNumber"`
	Delay         float64 `bson:"delay"`
	Unit          string  `bson:"unit"`
	Wait          bool    `bson:"wait"`
}

func (c *MicroscopeTriggerCameraRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *MicroscopeTriggerCameraRequest) GetDevice() int32 {
	return c.Device
}

func (c *MicroscopeTriggerCameraRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *MicroscopeTriggerCameraRequest) GetDelay() float64 {
	return c.Delay
}

func (c *MicroscopeTriggerCameraRequest) GetUnit() string {
	return c.Unit
}

func (c *MicroscopeTriggerCameraRequest) GetWait() bool {
	return c.Wait
}

func (c *MicroscopeTriggerCameraRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopeTriggerCameraRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopeTriggerCameraRequest) Clone() (*MicroscopeTriggerCameraRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopeTriggerCameraRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopeTriggerCameraRequest) Sanitize() error {
	return nil
}
