/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamCancelAllOutputsScheduleRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	StreamId    int32  `bson:"streamId"`
	Pvt         bool   `bson:"pvt"`
	Analog      bool   `bson:"analog"`
	Channels    []bool `bson:"channels"`
}

func (c *StreamCancelAllOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamCancelAllOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamCancelAllOutputsScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamCancelAllOutputsScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamCancelAllOutputsScheduleRequest) GetAnalog() bool {
	return c.Analog
}

func (c *StreamCancelAllOutputsScheduleRequest) GetChannels() []bool {
	return c.Channels
}

func (c *StreamCancelAllOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamCancelAllOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamCancelAllOutputsScheduleRequest) Clone() (*StreamCancelAllOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamCancelAllOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamCancelAllOutputsScheduleRequest) Sanitize() error {
	if c.Channels == nil {
		c.Channels = make([]bool, 0)
	}

	return nil
}
