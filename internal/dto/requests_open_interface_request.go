/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OpenInterfaceRequest struct {
	InterfaceType          InterfaceType `bson:"interfaceType"`
	PortName               string        `bson:"portName"`
	BaudRate               int32         `bson:"baudRate"`
	HostName               string        `bson:"hostName"`
	Port                   int32         `bson:"port"`
	Transport              int32         `bson:"transport"`
	RejectRoutedConnection bool          `bson:"rejectRoutedConnection"`
	CloudId                string        `bson:"cloudId"`
	ConnectionName         *string       `bson:"connectionName"`
	Realm                  *string       `bson:"realm"`
	Token                  string        `bson:"token"`
	Api                    string        `bson:"api"`
}

func (c *OpenInterfaceRequest) GetInterfaceType() InterfaceType {
	return c.InterfaceType
}

func (c *OpenInterfaceRequest) GetPortName() string {
	return c.PortName
}

func (c *OpenInterfaceRequest) GetBaudRate() int32 {
	return c.BaudRate
}

func (c *OpenInterfaceRequest) GetHostName() string {
	return c.HostName
}

func (c *OpenInterfaceRequest) GetPort() int32 {
	return c.Port
}

func (c *OpenInterfaceRequest) GetTransport() int32 {
	return c.Transport
}

func (c *OpenInterfaceRequest) GetRejectRoutedConnection() bool {
	return c.RejectRoutedConnection
}

func (c *OpenInterfaceRequest) GetCloudId() string {
	return c.CloudId
}

func (c *OpenInterfaceRequest) GetConnectionName() *string {
	return c.ConnectionName
}

func (c *OpenInterfaceRequest) GetRealm() *string {
	return c.Realm
}

func (c *OpenInterfaceRequest) GetToken() string {
	return c.Token
}

func (c *OpenInterfaceRequest) GetApi() string {
	return c.Api
}

func (c *OpenInterfaceRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OpenInterfaceRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OpenInterfaceRequest) Clone() (*OpenInterfaceRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OpenInterfaceRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OpenInterfaceRequest) Sanitize() error {
	return nil
}
