/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorCreateResponse struct {
	TranslatorId int32 `bson:"translatorId"`
}

func (c *TranslatorCreateResponse) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorCreateResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorCreateResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorCreateResponse) Clone() (*TranslatorCreateResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorCreateResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorCreateResponse) Sanitize() error {
	return nil
}
