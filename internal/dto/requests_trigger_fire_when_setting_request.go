/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerFireWhenSettingRequest struct {
	InterfaceId      int32                 `bson:"interfaceId"`
	Device           int32                 `bson:"device"`
	TriggerNumber    int32                 `bson:"triggerNumber"`
	Axis             int32                 `bson:"axis"`
	Setting          string                `bson:"setting"`
	TriggerCondition AsciiTriggerCondition `bson:"triggerCondition"`
	Value            float64               `bson:"value"`
	Unit             string                `bson:"unit"`
}

func (c *TriggerFireWhenSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerFireWhenSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerFireWhenSettingRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerFireWhenSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *TriggerFireWhenSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *TriggerFireWhenSettingRequest) GetTriggerCondition() AsciiTriggerCondition {
	return c.TriggerCondition
}

func (c *TriggerFireWhenSettingRequest) GetValue() float64 {
	return c.Value
}

func (c *TriggerFireWhenSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *TriggerFireWhenSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerFireWhenSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerFireWhenSettingRequest) Clone() (*TriggerFireWhenSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerFireWhenSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerFireWhenSettingRequest) Sanitize() error {
	return nil
}
