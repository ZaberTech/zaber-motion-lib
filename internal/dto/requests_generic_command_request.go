/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GenericCommandRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Command     string `bson:"command"`
	CheckErrors bool   `bson:"checkErrors"`
	Timeout     int32  `bson:"timeout"`
}

func (c *GenericCommandRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *GenericCommandRequest) GetDevice() int32 {
	return c.Device
}

func (c *GenericCommandRequest) GetAxis() int32 {
	return c.Axis
}

func (c *GenericCommandRequest) GetCommand() string {
	return c.Command
}

func (c *GenericCommandRequest) GetCheckErrors() bool {
	return c.CheckErrors
}

func (c *GenericCommandRequest) GetTimeout() int32 {
	return c.Timeout
}

func (c *GenericCommandRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GenericCommandRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GenericCommandRequest) Clone() (*GenericCommandRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GenericCommandRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GenericCommandRequest) Sanitize() error {
	return nil
}
