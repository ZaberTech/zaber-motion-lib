/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceOnAllRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	WaitUntilIdle bool  `bson:"waitUntilIdle"`
}

func (c *DeviceOnAllRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceOnAllRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *DeviceOnAllRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceOnAllRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceOnAllRequest) Clone() (*DeviceOnAllRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceOnAllRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceOnAllRequest) Sanitize() error {
	return nil
}
