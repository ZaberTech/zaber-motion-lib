/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamWaitDigitalInputRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	StreamId      int32 `bson:"streamId"`
	Pvt           bool  `bson:"pvt"`
	ChannelNumber int32 `bson:"channelNumber"`
	Value         bool  `bson:"value"`
}

func (c *StreamWaitDigitalInputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamWaitDigitalInputRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamWaitDigitalInputRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamWaitDigitalInputRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamWaitDigitalInputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamWaitDigitalInputRequest) GetValue() bool {
	return c.Value
}

func (c *StreamWaitDigitalInputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamWaitDigitalInputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamWaitDigitalInputRequest) Clone() (*StreamWaitDigitalInputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamWaitDigitalInputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamWaitDigitalInputRequest) Sanitize() error {
	return nil
}
