/* This file is generated. Do not modify by hand. */
package dto

// Denotes type of an axis and units it accepts.
type AsciiAxisType int32

const (
	AsciiAxisType_UNKNOWN AsciiAxisType = 0
	AsciiAxisType_LINEAR  AsciiAxisType = 1
	AsciiAxisType_ROTARY  AsciiAxisType = 2
	AsciiAxisType_PROCESS AsciiAxisType = 3
	AsciiAxisType_LAMP    AsciiAxisType = 4
)

var AsciiAxisType_name = map[AsciiAxisType]string{
	AsciiAxisType_UNKNOWN: "Unknown",
	AsciiAxisType_LINEAR:  "Linear",
	AsciiAxisType_ROTARY:  "Rotary",
	AsciiAxisType_PROCESS: "Process",
	AsciiAxisType_LAMP:    "Lamp",
}
