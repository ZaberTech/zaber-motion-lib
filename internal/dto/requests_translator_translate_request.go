/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorTranslateRequest struct {
	TranslatorId int32  `bson:"translatorId"`
	Block        string `bson:"block"`
}

func (c *TranslatorTranslateRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorTranslateRequest) GetBlock() string {
	return c.Block
}

func (c *TranslatorTranslateRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorTranslateRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorTranslateRequest) Clone() (*TranslatorTranslateRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorTranslateRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorTranslateRequest) Sanitize() error {
	return nil
}
