/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerStates struct {
	States []*AsciiTriggerState `bson:"states"`
}

func (c *TriggerStates) GetStates() []*AsciiTriggerState {
	return c.States
}

func (c *TriggerStates) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerStates) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerStates) Clone() (*TriggerStates, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerStates{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerStates) Sanitize() error {
	if c.States == nil {
		c.States = make([]*AsciiTriggerState, 0)
	}

	for _, item := range c.States {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property States of TriggerStates is nil")
		}
	}

	return nil
}
