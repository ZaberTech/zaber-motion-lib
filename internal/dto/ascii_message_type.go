/* This file is generated. Do not modify by hand. */
package dto

// Denotes type of the response message.
// For more information refer to:
// [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format).
type AsciiMessageType int32

const (
	AsciiMessageType_REPLY AsciiMessageType = 0
	AsciiMessageType_INFO  AsciiMessageType = 1
	AsciiMessageType_ALERT AsciiMessageType = 2
)

var AsciiMessageType_name = map[AsciiMessageType]string{
	AsciiMessageType_REPLY: "Reply",
	AsciiMessageType_INFO:  "Info",
	AsciiMessageType_ALERT: "Alert",
}
