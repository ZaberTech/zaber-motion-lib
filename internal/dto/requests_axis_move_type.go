/* This file is generated. Do not modify by hand. */
package dto

type AxisMoveType int32

const (
	AxisMoveType_ABS   AxisMoveType = 0
	AxisMoveType_REL   AxisMoveType = 1
	AxisMoveType_VEL   AxisMoveType = 2
	AxisMoveType_MAX   AxisMoveType = 3
	AxisMoveType_MIN   AxisMoveType = 4
	AxisMoveType_INDEX AxisMoveType = 5
)

var AxisMoveType_name = map[AxisMoveType]string{
	AxisMoveType_ABS:   "Abs",
	AxisMoveType_REL:   "Rel",
	AxisMoveType_VEL:   "Vel",
	AxisMoveType_MAX:   "Max",
	AxisMoveType_MIN:   "Min",
	AxisMoveType_INDEX: "Index",
}
