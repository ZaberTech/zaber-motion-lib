/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AutofocusGetObjectiveParamsResponse struct {
	Parameters []*NamedParameter `bson:"parameters"`
}

func (c *AutofocusGetObjectiveParamsResponse) GetParameters() []*NamedParameter {
	return c.Parameters
}

func (c *AutofocusGetObjectiveParamsResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AutofocusGetObjectiveParamsResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AutofocusGetObjectiveParamsResponse) Clone() (*AutofocusGetObjectiveParamsResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AutofocusGetObjectiveParamsResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AutofocusGetObjectiveParamsResponse) Sanitize() error {
	if c.Parameters == nil {
		c.Parameters = make([]*NamedParameter, 0)
	}

	for _, item := range c.Parameters {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Parameters of AutofocusGetObjectiveParamsResponse is nil")
		}
	}

	return nil
}
