/* This file is generated. Do not modify by hand. */
package dto

// Trigger action identifier.
type AsciiTriggerAction int32

const (
	AsciiTriggerAction_ALL AsciiTriggerAction = 0
	AsciiTriggerAction_A   AsciiTriggerAction = 1
	AsciiTriggerAction_B   AsciiTriggerAction = 2
)

var AsciiTriggerAction_name = map[AsciiTriggerAction]string{
	AsciiTriggerAction_ALL: "All",
	AsciiTriggerAction_A:   "A",
	AsciiTriggerAction_B:   "B",
}
