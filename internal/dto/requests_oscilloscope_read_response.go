/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeReadResponse struct {
	DataIds []int32 `bson:"dataIds"`
}

func (c *OscilloscopeReadResponse) GetDataIds() []int32 {
	return c.DataIds
}

func (c *OscilloscopeReadResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeReadResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeReadResponse) Clone() (*OscilloscopeReadResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeReadResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeReadResponse) Sanitize() error {
	if c.DataIds == nil {
		c.DataIds = make([]int32, 0)
	}

	return nil
}
