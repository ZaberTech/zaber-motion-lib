/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetStorageNumberRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Key         string  `bson:"key"`
	Value       float64 `bson:"value"`
}

func (c *DeviceSetStorageNumberRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetStorageNumberRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetStorageNumberRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetStorageNumberRequest) GetKey() string {
	return c.Key
}

func (c *DeviceSetStorageNumberRequest) GetValue() float64 {
	return c.Value
}

func (c *DeviceSetStorageNumberRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetStorageNumberRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetStorageNumberRequest) Clone() (*DeviceSetStorageNumberRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetStorageNumberRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetStorageNumberRequest) Sanitize() error {
	return nil
}
