/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetAnalogIORequest struct {
	InterfaceId   int32  `bson:"interfaceId"`
	Device        int32  `bson:"device"`
	ChannelType   string `bson:"channelType"`
	ChannelNumber int32  `bson:"channelNumber"`
}

func (c *DeviceGetAnalogIORequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetAnalogIORequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetAnalogIORequest) GetChannelType() string {
	return c.ChannelType
}

func (c *DeviceGetAnalogIORequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceGetAnalogIORequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetAnalogIORequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetAnalogIORequest) Clone() (*DeviceGetAnalogIORequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetAnalogIORequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetAnalogIORequest) Sanitize() error {
	return nil
}
