/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetLogOutputRequest struct {
	Mode     LogOutputMode `bson:"mode"`
	FilePath *string       `bson:"filePath"`
}

func (c *SetLogOutputRequest) GetMode() LogOutputMode {
	return c.Mode
}

func (c *SetLogOutputRequest) GetFilePath() *string {
	return c.FilePath
}

func (c *SetLogOutputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetLogOutputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetLogOutputRequest) Clone() (*SetLogOutputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetLogOutputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetLogOutputRequest) Sanitize() error {
	return nil
}
