/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for OperationFailedException.
type ExceptionsOperationFailedExceptionData struct {
	// The full list of warnings.
	Warnings []string `bson:"warnings"`
	// The reason for the Exception.
	Reason string `bson:"reason"`
	// The address of the device that attempted the failed operation.
	Device int32 `bson:"device"`
	// The number of the axis that attempted the failed operation.
	Axis int32 `bson:"axis"`
}

// The full list of warnings.
func (c *ExceptionsOperationFailedExceptionData) GetWarnings() []string {
	return c.Warnings
}

// The reason for the Exception.
func (c *ExceptionsOperationFailedExceptionData) GetReason() string {
	return c.Reason
}

// The address of the device that attempted the failed operation.
func (c *ExceptionsOperationFailedExceptionData) GetDevice() int32 {
	return c.Device
}

// The number of the axis that attempted the failed operation.
func (c *ExceptionsOperationFailedExceptionData) GetAxis() int32 {
	return c.Axis
}

func (c *ExceptionsOperationFailedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsOperationFailedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsOperationFailedExceptionData) Clone() (*ExceptionsOperationFailedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsOperationFailedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsOperationFailedExceptionData) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	return nil
}
