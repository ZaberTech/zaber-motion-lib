/* This file is generated. Do not modify by hand. */
package dto

// Named constants for all Zaber Binary protocol reply-only command codes.
type BinaryReplyCode int32

const (
	BinaryReplyCode_MOVE_TRACKING        BinaryReplyCode = 8
	BinaryReplyCode_LIMIT_ACTIVE         BinaryReplyCode = 9
	BinaryReplyCode_MANUAL_MOVE_TRACKING BinaryReplyCode = 10
	BinaryReplyCode_MANUAL_MOVE          BinaryReplyCode = 11
	BinaryReplyCode_SLIP_TRACKING        BinaryReplyCode = 12
	BinaryReplyCode_UNEXPECTED_POSITION  BinaryReplyCode = 13
	BinaryReplyCode_ERROR                BinaryReplyCode = 255
)

var BinaryReplyCode_name = map[BinaryReplyCode]string{
	BinaryReplyCode_MOVE_TRACKING:        "MoveTracking",
	BinaryReplyCode_LIMIT_ACTIVE:         "LimitActive",
	BinaryReplyCode_MANUAL_MOVE_TRACKING: "ManualMoveTracking",
	BinaryReplyCode_MANUAL_MOVE:          "ManualMove",
	BinaryReplyCode_SLIP_TRACKING:        "SlipTracking",
	BinaryReplyCode_UNEXPECTED_POSITION:  "UnexpectedPosition",
	BinaryReplyCode_ERROR:                "Error",
}
