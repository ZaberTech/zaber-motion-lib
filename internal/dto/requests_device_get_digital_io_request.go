/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetDigitalIORequest struct {
	InterfaceId   int32  `bson:"interfaceId"`
	Device        int32  `bson:"device"`
	ChannelType   string `bson:"channelType"`
	ChannelNumber int32  `bson:"channelNumber"`
}

func (c *DeviceGetDigitalIORequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetDigitalIORequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetDigitalIORequest) GetChannelType() string {
	return c.ChannelType
}

func (c *DeviceGetDigitalIORequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceGetDigitalIORequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetDigitalIORequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetDigitalIORequest) Clone() (*DeviceGetDigitalIORequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetDigitalIORequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetDigitalIORequest) Sanitize() error {
	return nil
}
