/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AutofocusGetStatusResponse struct {
	Status *MicroscopyAutofocusStatus `bson:"status"`
}

func (c *AutofocusGetStatusResponse) GetStatus() *MicroscopyAutofocusStatus {
	return c.Status
}

func (c *AutofocusGetStatusResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AutofocusGetStatusResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AutofocusGetStatusResponse) Clone() (*AutofocusGetStatusResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AutofocusGetStatusResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AutofocusGetStatusResponse) Sanitize() error {
	if c.Status != nil {
		if err := c.Status.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Status of AutofocusGetStatusResponse is nil")
	}

	return nil
}
