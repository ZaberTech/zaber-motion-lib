/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AutofocusFocusRequest struct {
	ProviderId    int32 `bson:"providerId"`
	InterfaceId   int32 `bson:"interfaceId"`
	FocusAddress  int32 `bson:"focusAddress"`
	FocusAxis     int32 `bson:"focusAxis"`
	TurretAddress int32 `bson:"turretAddress"`
	Scan          bool  `bson:"scan"`
	Once          bool  `bson:"once"`
	Timeout       int32 `bson:"timeout"`
}

func (c *AutofocusFocusRequest) GetProviderId() int32 {
	return c.ProviderId
}

func (c *AutofocusFocusRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AutofocusFocusRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *AutofocusFocusRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *AutofocusFocusRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *AutofocusFocusRequest) GetScan() bool {
	return c.Scan
}

func (c *AutofocusFocusRequest) GetOnce() bool {
	return c.Once
}

func (c *AutofocusFocusRequest) GetTimeout() int32 {
	return c.Timeout
}

func (c *AutofocusFocusRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AutofocusFocusRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AutofocusFocusRequest) Clone() (*AutofocusFocusRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AutofocusFocusRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AutofocusFocusRequest) Sanitize() error {
	return nil
}
