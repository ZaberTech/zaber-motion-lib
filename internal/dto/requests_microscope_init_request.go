/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type MicroscopeInitRequest struct {
	InterfaceId int32                       `bson:"interfaceId"`
	Config      *MicroscopyMicroscopeConfig `bson:"config"`
	Force       bool                        `bson:"force"`
}

func (c *MicroscopeInitRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *MicroscopeInitRequest) GetConfig() *MicroscopyMicroscopeConfig {
	return c.Config
}

func (c *MicroscopeInitRequest) GetForce() bool {
	return c.Force
}

func (c *MicroscopeInitRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopeInitRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopeInitRequest) Clone() (*MicroscopeInitRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopeInitRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopeInitRequest) Sanitize() error {
	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Config of MicroscopeInitRequest is nil")
	}

	return nil
}
