/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type WaitToRespondRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Timeout     float64 `bson:"timeout"`
}

func (c *WaitToRespondRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *WaitToRespondRequest) GetDevice() int32 {
	return c.Device
}

func (c *WaitToRespondRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *WaitToRespondRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *WaitToRespondRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *WaitToRespondRequest) Clone() (*WaitToRespondRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &WaitToRespondRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *WaitToRespondRequest) Sanitize() error {
	return nil
}
