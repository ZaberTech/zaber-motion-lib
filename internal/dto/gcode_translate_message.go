/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Represents a message from translator regarding a block translation.
type GcodeTranslateMessage struct {
	// The message describing an occurrence.
	Message string `bson:"message"`
	// The index in the block string that the message regards to.
	FromBlock int32 `bson:"fromBlock"`
	// The end index in the block string that the message regards to.
	// The end index is exclusive.
	ToBlock int32 `bson:"toBlock"`
}

// The message describing an occurrence.
func (c *GcodeTranslateMessage) GetMessage() string {
	return c.Message
}

// The index in the block string that the message regards to.
func (c *GcodeTranslateMessage) GetFromBlock() int32 {
	return c.FromBlock
}

// The end index in the block string that the message regards to.
// The end index is exclusive.
func (c *GcodeTranslateMessage) GetToBlock() int32 {
	return c.ToBlock
}

func (c *GcodeTranslateMessage) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeTranslateMessage) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeTranslateMessage) Clone() (*GcodeTranslateMessage, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeTranslateMessage{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeTranslateMessage) Sanitize() error {
	return nil
}
