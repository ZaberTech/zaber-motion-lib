/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Configuration representing a microscope setup.
// Device address of value 0 means that the part is not present.
type MicroscopyMicroscopeConfig struct {
	// Focus axis of the microscope.
	FocusAxis *AxisAddress `bson:"focusAxis"`
	// X axis of the microscope.
	XAxis *AxisAddress `bson:"xAxis"`
	// Y axis of the microscope.
	YAxis *AxisAddress `bson:"yAxis"`
	// Illuminator device address.
	Illuminator *int32 `bson:"illuminator"`
	// Filter changer device address.
	FilterChanger *int32 `bson:"filterChanger"`
	// Objective changer device address.
	ObjectiveChanger *int32 `bson:"objectiveChanger"`
	// Autofocus identifier.
	Autofocus *int32 `bson:"autofocus"`
	// Camera trigger digital output address.
	CameraTrigger *ChannelAddress `bson:"cameraTrigger"`
}

// Focus axis of the microscope.
func (c *MicroscopyMicroscopeConfig) GetFocusAxis() *AxisAddress {
	return c.FocusAxis
}

// X axis of the microscope.
func (c *MicroscopyMicroscopeConfig) GetXAxis() *AxisAddress {
	return c.XAxis
}

// Y axis of the microscope.
func (c *MicroscopyMicroscopeConfig) GetYAxis() *AxisAddress {
	return c.YAxis
}

// Illuminator device address.
func (c *MicroscopyMicroscopeConfig) GetIlluminator() *int32 {
	return c.Illuminator
}

// Filter changer device address.
func (c *MicroscopyMicroscopeConfig) GetFilterChanger() *int32 {
	return c.FilterChanger
}

// Objective changer device address.
func (c *MicroscopyMicroscopeConfig) GetObjectiveChanger() *int32 {
	return c.ObjectiveChanger
}

// Autofocus identifier.
func (c *MicroscopyMicroscopeConfig) GetAutofocus() *int32 {
	return c.Autofocus
}

// Camera trigger digital output address.
func (c *MicroscopyMicroscopeConfig) GetCameraTrigger() *ChannelAddress {
	return c.CameraTrigger
}

func (c *MicroscopyMicroscopeConfig) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopyMicroscopeConfig) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopyMicroscopeConfig) Clone() (*MicroscopyMicroscopeConfig, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopyMicroscopeConfig{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopyMicroscopeConfig) Sanitize() error {
	if c.FocusAxis != nil {
		if err := c.FocusAxis.Sanitize(); err != nil {
			return err
		}
	}

	if c.XAxis != nil {
		if err := c.XAxis.Sanitize(); err != nil {
			return err
		}
	}

	if c.YAxis != nil {
		if err := c.YAxis.Sanitize(); err != nil {
			return err
		}
	}

	if c.CameraTrigger != nil {
		if err := c.CameraTrigger.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
