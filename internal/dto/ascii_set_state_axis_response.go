/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// An object containing any non-blocking issues encountered when loading a saved state to an axis.
type AsciiSetStateAxisResponse struct {
	// The warnings encountered when applying this state to the given axis.
	Warnings []string `bson:"warnings"`
	// The number of the axis that was set.
	AxisNumber int32 `bson:"axisNumber"`
}

// The warnings encountered when applying this state to the given axis.
func (c *AsciiSetStateAxisResponse) GetWarnings() []string {
	return c.Warnings
}

// The number of the axis that was set.
func (c *AsciiSetStateAxisResponse) GetAxisNumber() int32 {
	return c.AxisNumber
}

func (c *AsciiSetStateAxisResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiSetStateAxisResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiSetStateAxisResponse) Clone() (*AsciiSetStateAxisResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiSetStateAxisResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiSetStateAxisResponse) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	return nil
}
