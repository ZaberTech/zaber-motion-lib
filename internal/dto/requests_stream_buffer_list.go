/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamBufferList struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Pvt         bool  `bson:"pvt"`
}

func (c *StreamBufferList) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamBufferList) GetDevice() int32 {
	return c.Device
}

func (c *StreamBufferList) GetPvt() bool {
	return c.Pvt
}

func (c *StreamBufferList) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamBufferList) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamBufferList) Clone() (*StreamBufferList, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamBufferList{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamBufferList) Sanitize() error {
	return nil
}
