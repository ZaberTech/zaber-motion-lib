/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CheckVersionRequest struct {
	Version string `bson:"version"`
	Host    string `bson:"host"`
}

func (c *CheckVersionRequest) GetVersion() string {
	return c.Version
}

func (c *CheckVersionRequest) GetHost() string {
	return c.Host
}

func (c *CheckVersionRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CheckVersionRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CheckVersionRequest) Clone() (*CheckVersionRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CheckVersionRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CheckVersionRequest) Sanitize() error {
	return nil
}
