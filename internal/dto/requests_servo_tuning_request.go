/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ServoTuningRequest struct {
	InterfaceId int32                    `bson:"interfaceId"`
	Device      int32                    `bson:"device"`
	Axis        int32                    `bson:"axis"`
	Paramset    AsciiServoTuningParamset `bson:"paramset"`
}

func (c *ServoTuningRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ServoTuningRequest) GetDevice() int32 {
	return c.Device
}

func (c *ServoTuningRequest) GetAxis() int32 {
	return c.Axis
}

func (c *ServoTuningRequest) GetParamset() AsciiServoTuningParamset {
	return c.Paramset
}

func (c *ServoTuningRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ServoTuningRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ServoTuningRequest) Clone() (*ServoTuningRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ServoTuningRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ServoTuningRequest) Sanitize() error {
	return nil
}
