/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The response from a multi-get axis command.
type AsciiGetAxisSettingResult struct {
	// The setting read.
	Setting string `bson:"setting"`
	// The value read.
	Value float64 `bson:"value"`
	// The unit of the values.
	Unit string `bson:"unit"`
}

// The setting read.
func (c *AsciiGetAxisSettingResult) GetSetting() string {
	return c.Setting
}

// The value read.
func (c *AsciiGetAxisSettingResult) GetValue() float64 {
	return c.Value
}

// The unit of the values.
func (c *AsciiGetAxisSettingResult) GetUnit() string {
	return c.Unit
}

func (c *AsciiGetAxisSettingResult) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiGetAxisSettingResult) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiGetAxisSettingResult) Clone() (*AsciiGetAxisSettingResult, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiGetAxisSettingResult{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiGetAxisSettingResult) Sanitize() error {
	return nil
}
