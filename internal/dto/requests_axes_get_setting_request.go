/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AxesGetSettingRequest struct {
	Interfaces []int32  `bson:"interfaces"`
	Devices    []int32  `bson:"devices"`
	Axes       []int32  `bson:"axes"`
	Unit       []string `bson:"unit"`
	Setting    string   `bson:"setting"`
}

func (c *AxesGetSettingRequest) GetInterfaces() []int32 {
	return c.Interfaces
}

func (c *AxesGetSettingRequest) GetDevices() []int32 {
	return c.Devices
}

func (c *AxesGetSettingRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *AxesGetSettingRequest) GetUnit() []string {
	return c.Unit
}

func (c *AxesGetSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *AxesGetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxesGetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxesGetSettingRequest) Clone() (*AxesGetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxesGetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxesGetSettingRequest) Sanitize() error {
	if c.Interfaces == nil {
		c.Interfaces = make([]int32, 0)
	}

	if c.Devices == nil {
		c.Devices = make([]int32, 0)
	}

	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	if c.Unit == nil {
		c.Unit = make([]string, 0)
	}

	return nil
}
