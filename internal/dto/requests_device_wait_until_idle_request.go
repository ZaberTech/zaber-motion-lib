/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceWaitUntilIdleRequest struct {
	InterfaceId       int32 `bson:"interfaceId"`
	Device            int32 `bson:"device"`
	Axis              int32 `bson:"axis"`
	ThrowErrorOnFault bool  `bson:"throwErrorOnFault"`
}

func (c *DeviceWaitUntilIdleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceWaitUntilIdleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceWaitUntilIdleRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceWaitUntilIdleRequest) GetThrowErrorOnFault() bool {
	return c.ThrowErrorOnFault
}

func (c *DeviceWaitUntilIdleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceWaitUntilIdleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceWaitUntilIdleRequest) Clone() (*DeviceWaitUntilIdleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceWaitUntilIdleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceWaitUntilIdleRequest) Sanitize() error {
	return nil
}
