/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetAllDigitalIOResponse struct {
	Values []bool `bson:"values"`
}

func (c *DeviceGetAllDigitalIOResponse) GetValues() []bool {
	return c.Values
}

func (c *DeviceGetAllDigitalIOResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetAllDigitalIOResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetAllDigitalIOResponse) Clone() (*DeviceGetAllDigitalIOResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetAllDigitalIOResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetAllDigitalIOResponse) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]bool, 0)
	}

	return nil
}
