/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetSettingRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Setting     string `bson:"setting"`
	Unit        string `bson:"unit"`
}

func (c *DeviceGetSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceGetSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *DeviceGetSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceGetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetSettingRequest) Clone() (*DeviceGetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetSettingRequest) Sanitize() error {
	return nil
}
