/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type EmptyAutofocusRequest struct {
	ProviderId    int32 `bson:"providerId"`
	InterfaceId   int32 `bson:"interfaceId"`
	FocusAddress  int32 `bson:"focusAddress"`
	FocusAxis     int32 `bson:"focusAxis"`
	TurretAddress int32 `bson:"turretAddress"`
}

func (c *EmptyAutofocusRequest) GetProviderId() int32 {
	return c.ProviderId
}

func (c *EmptyAutofocusRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *EmptyAutofocusRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *EmptyAutofocusRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *EmptyAutofocusRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *EmptyAutofocusRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *EmptyAutofocusRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *EmptyAutofocusRequest) Clone() (*EmptyAutofocusRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &EmptyAutofocusRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *EmptyAutofocusRequest) Sanitize() error {
	return nil
}
