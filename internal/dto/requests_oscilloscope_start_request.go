/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeStartRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	CaptureLength int32 `bson:"captureLength"`
}

func (c *OscilloscopeStartRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *OscilloscopeStartRequest) GetDevice() int32 {
	return c.Device
}

func (c *OscilloscopeStartRequest) GetCaptureLength() int32 {
	return c.CaptureLength
}

func (c *OscilloscopeStartRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeStartRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeStartRequest) Clone() (*OscilloscopeStartRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeStartRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeStartRequest) Sanitize() error {
	return nil
}
