/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Representation of data gathered during device identification.
type BinaryDeviceIdentity struct {
	// Unique ID of the device hardware.
	DeviceId int32 `bson:"deviceId"`
	// Serial number of the device.
	// Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
	SerialNumber uint32 `bson:"serialNumber"`
	// Name of the product.
	Name string `bson:"name"`
	// Version of the firmware.
	FirmwareVersion *FirmwareVersion `bson:"firmwareVersion"`
	// Indicates whether the device is a peripheral or part of an integrated device.
	IsPeripheral bool `bson:"isPeripheral"`
	// Unique ID of the peripheral hardware.
	PeripheralId int32 `bson:"peripheralId"`
	// Name of the peripheral hardware.
	PeripheralName string `bson:"peripheralName"`
	// Determines the type of an device and units it accepts.
	DeviceType BinaryDeviceType `bson:"deviceType"`
}

// Unique ID of the device hardware.
func (c *BinaryDeviceIdentity) GetDeviceId() int32 {
	return c.DeviceId
}

// Serial number of the device.
// Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
func (c *BinaryDeviceIdentity) GetSerialNumber() uint32 {
	return c.SerialNumber
}

// Name of the product.
func (c *BinaryDeviceIdentity) GetName() string {
	return c.Name
}

// Version of the firmware.
func (c *BinaryDeviceIdentity) GetFirmwareVersion() *FirmwareVersion {
	return c.FirmwareVersion
}

// Indicates whether the device is a peripheral or part of an integrated device.
func (c *BinaryDeviceIdentity) GetIsPeripheral() bool {
	return c.IsPeripheral
}

// Unique ID of the peripheral hardware.
func (c *BinaryDeviceIdentity) GetPeripheralId() int32 {
	return c.PeripheralId
}

// Name of the peripheral hardware.
func (c *BinaryDeviceIdentity) GetPeripheralName() string {
	return c.PeripheralName
}

// Determines the type of an device and units it accepts.
func (c *BinaryDeviceIdentity) GetDeviceType() BinaryDeviceType {
	return c.DeviceType
}

func (c *BinaryDeviceIdentity) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceIdentity) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceIdentity) Clone() (*BinaryDeviceIdentity, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceIdentity{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceIdentity) Sanitize() error {
	if c.FirmwareVersion != nil {
		if err := c.FirmwareVersion.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property FirmwareVersion of DeviceIdentity is nil")
	}

	return nil
}
