/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetLowpassFilterRequest struct {
	InterfaceId     int32   `bson:"interfaceId"`
	Device          int32   `bson:"device"`
	ChannelNumber   int32   `bson:"channelNumber"`
	CutoffFrequency float64 `bson:"cutoffFrequency"`
	Unit            string  `bson:"unit"`
}

func (c *DeviceSetLowpassFilterRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetLowpassFilterRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetLowpassFilterRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceSetLowpassFilterRequest) GetCutoffFrequency() float64 {
	return c.CutoffFrequency
}

func (c *DeviceSetLowpassFilterRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetLowpassFilterRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetLowpassFilterRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetLowpassFilterRequest) Clone() (*DeviceSetLowpassFilterRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetLowpassFilterRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetLowpassFilterRequest) Sanitize() error {
	return nil
}
