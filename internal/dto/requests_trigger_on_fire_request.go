/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerOnFireRequest struct {
	InterfaceId   int32              `bson:"interfaceId"`
	Device        int32              `bson:"device"`
	TriggerNumber int32              `bson:"triggerNumber"`
	Action        AsciiTriggerAction `bson:"action"`
	Axis          int32              `bson:"axis"`
	Command       string             `bson:"command"`
}

func (c *TriggerOnFireRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerOnFireRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerOnFireRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerOnFireRequest) GetAction() AsciiTriggerAction {
	return c.Action
}

func (c *TriggerOnFireRequest) GetAxis() int32 {
	return c.Axis
}

func (c *TriggerOnFireRequest) GetCommand() string {
	return c.Command
}

func (c *TriggerOnFireRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerOnFireRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerOnFireRequest) Clone() (*TriggerOnFireRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerOnFireRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerOnFireRequest) Sanitize() error {
	return nil
}
