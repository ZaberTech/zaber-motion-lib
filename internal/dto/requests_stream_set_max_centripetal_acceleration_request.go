/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetMaxCentripetalAccelerationRequest struct {
	InterfaceId                int32   `bson:"interfaceId"`
	Device                     int32   `bson:"device"`
	StreamId                   int32   `bson:"streamId"`
	Pvt                        bool    `bson:"pvt"`
	MaxCentripetalAcceleration float64 `bson:"maxCentripetalAcceleration"`
	Unit                       string  `bson:"unit"`
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetMaxCentripetalAcceleration() float64 {
	return c.MaxCentripetalAcceleration
}

func (c *StreamSetMaxCentripetalAccelerationRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetMaxCentripetalAccelerationRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetMaxCentripetalAccelerationRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetMaxCentripetalAccelerationRequest) Clone() (*StreamSetMaxCentripetalAccelerationRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetMaxCentripetalAccelerationRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetMaxCentripetalAccelerationRequest) Sanitize() error {
	return nil
}
