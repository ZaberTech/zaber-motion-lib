/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GetAllIoPortLabelsResponse struct {
	Labels []*AsciiIoPortLabel `bson:"labels"`
}

func (c *GetAllIoPortLabelsResponse) GetLabels() []*AsciiIoPortLabel {
	return c.Labels
}

func (c *GetAllIoPortLabelsResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GetAllIoPortLabelsResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GetAllIoPortLabelsResponse) Clone() (*GetAllIoPortLabelsResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GetAllIoPortLabelsResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GetAllIoPortLabelsResponse) Sanitize() error {
	if c.Labels == nil {
		c.Labels = make([]*AsciiIoPortLabel, 0)
	}

	for _, item := range c.Labels {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Labels of GetAllIoPortLabelsResponse is nil")
		}
	}

	return nil
}
