/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerFireWhenDistanceTravelledRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	TriggerNumber int32   `bson:"triggerNumber"`
	Axis          int32   `bson:"axis"`
	Distance      float64 `bson:"distance"`
	Unit          string  `bson:"unit"`
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetAxis() int32 {
	return c.Axis
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetDistance() float64 {
	return c.Distance
}

func (c *TriggerFireWhenDistanceTravelledRequest) GetUnit() string {
	return c.Unit
}

func (c *TriggerFireWhenDistanceTravelledRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerFireWhenDistanceTravelledRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerFireWhenDistanceTravelledRequest) Clone() (*TriggerFireWhenDistanceTravelledRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerFireWhenDistanceTravelledRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerFireWhenDistanceTravelledRequest) Sanitize() error {
	return nil
}
