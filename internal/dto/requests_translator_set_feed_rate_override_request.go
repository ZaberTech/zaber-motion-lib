/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorSetFeedRateOverrideRequest struct {
	TranslatorId int32   `bson:"translatorId"`
	Coefficient  float64 `bson:"coefficient"`
}

func (c *TranslatorSetFeedRateOverrideRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorSetFeedRateOverrideRequest) GetCoefficient() float64 {
	return c.Coefficient
}

func (c *TranslatorSetFeedRateOverrideRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorSetFeedRateOverrideRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorSetFeedRateOverrideRequest) Clone() (*TranslatorSetFeedRateOverrideRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorSetFeedRateOverrideRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorSetFeedRateOverrideRequest) Sanitize() error {
	return nil
}
