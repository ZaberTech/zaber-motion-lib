/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamGetMaxCentripetalAccelerationRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	StreamId    int32  `bson:"streamId"`
	Pvt         bool   `bson:"pvt"`
	Unit        string `bson:"unit"`
}

func (c *StreamGetMaxCentripetalAccelerationRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamGetMaxCentripetalAccelerationRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamGetMaxCentripetalAccelerationRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamGetMaxCentripetalAccelerationRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamGetMaxCentripetalAccelerationRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamGetMaxCentripetalAccelerationRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamGetMaxCentripetalAccelerationRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamGetMaxCentripetalAccelerationRequest) Clone() (*StreamGetMaxCentripetalAccelerationRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamGetMaxCentripetalAccelerationRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamGetMaxCentripetalAccelerationRequest) Sanitize() error {
	return nil
}
