/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AutofocusSetObjectiveParamsRequest struct {
	ProviderId    int32             `bson:"providerId"`
	InterfaceId   int32             `bson:"interfaceId"`
	FocusAddress  int32             `bson:"focusAddress"`
	FocusAxis     int32             `bson:"focusAxis"`
	TurretAddress int32             `bson:"turretAddress"`
	Objective     int32             `bson:"objective"`
	Parameters    []*NamedParameter `bson:"parameters"`
}

func (c *AutofocusSetObjectiveParamsRequest) GetProviderId() int32 {
	return c.ProviderId
}

func (c *AutofocusSetObjectiveParamsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AutofocusSetObjectiveParamsRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *AutofocusSetObjectiveParamsRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *AutofocusSetObjectiveParamsRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *AutofocusSetObjectiveParamsRequest) GetObjective() int32 {
	return c.Objective
}

func (c *AutofocusSetObjectiveParamsRequest) GetParameters() []*NamedParameter {
	return c.Parameters
}

func (c *AutofocusSetObjectiveParamsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AutofocusSetObjectiveParamsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AutofocusSetObjectiveParamsRequest) Clone() (*AutofocusSetObjectiveParamsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AutofocusSetObjectiveParamsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AutofocusSetObjectiveParamsRequest) Sanitize() error {
	if c.Parameters == nil {
		c.Parameters = make([]*NamedParameter, 0)
	}

	for _, item := range c.Parameters {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Parameters of AutofocusSetObjectiveParamsRequest is nil")
		}
	}

	return nil
}
