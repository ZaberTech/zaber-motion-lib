/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GatewayResponse struct {
	Response     ResponseType `bson:"response"`
	ErrorType    Errors       `bson:"errorType"`
	ErrorMessage string       `bson:"errorMessage"`
}

func (c *GatewayResponse) GetResponse() ResponseType {
	return c.Response
}

func (c *GatewayResponse) GetErrorType() Errors {
	return c.ErrorType
}

func (c *GatewayResponse) GetErrorMessage() string {
	return c.ErrorMessage
}

func (c *GatewayResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GatewayResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GatewayResponse) Clone() (*GatewayResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GatewayResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GatewayResponse) Sanitize() error {
	return nil
}
