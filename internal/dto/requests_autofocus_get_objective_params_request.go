/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AutofocusGetObjectiveParamsRequest struct {
	ProviderId    int32 `bson:"providerId"`
	InterfaceId   int32 `bson:"interfaceId"`
	FocusAddress  int32 `bson:"focusAddress"`
	FocusAxis     int32 `bson:"focusAxis"`
	TurretAddress int32 `bson:"turretAddress"`
	Objective     int32 `bson:"objective"`
}

func (c *AutofocusGetObjectiveParamsRequest) GetProviderId() int32 {
	return c.ProviderId
}

func (c *AutofocusGetObjectiveParamsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AutofocusGetObjectiveParamsRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *AutofocusGetObjectiveParamsRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *AutofocusGetObjectiveParamsRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *AutofocusGetObjectiveParamsRequest) GetObjective() int32 {
	return c.Objective
}

func (c *AutofocusGetObjectiveParamsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AutofocusGetObjectiveParamsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AutofocusGetObjectiveParamsRequest) Clone() (*AutofocusGetObjectiveParamsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AutofocusGetObjectiveParamsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AutofocusGetObjectiveParamsRequest) Sanitize() error {
	return nil
}
