/* This file is generated. Do not modify by hand. */
package dto

// Denotes type of the stream axis.
type AsciiStreamAxisType int32

const (
	AsciiStreamAxisType_PHYSICAL AsciiStreamAxisType = 0
	AsciiStreamAxisType_LOCKSTEP AsciiStreamAxisType = 1
)

var AsciiStreamAxisType_name = map[AsciiStreamAxisType]string{
	AsciiStreamAxisType_PHYSICAL: "Physical",
	AsciiStreamAxisType_LOCKSTEP: "Lockstep",
}
