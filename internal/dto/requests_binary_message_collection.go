/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryMessageCollection struct {
	Messages []*BinaryMessage `bson:"messages"`
}

func (c *BinaryMessageCollection) GetMessages() []*BinaryMessage {
	return c.Messages
}

func (c *BinaryMessageCollection) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryMessageCollection) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryMessageCollection) Clone() (*BinaryMessageCollection, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryMessageCollection{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryMessageCollection) Sanitize() error {
	if c.Messages == nil {
		c.Messages = make([]*BinaryMessage, 0)
	}

	for _, item := range c.Messages {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Messages of BinaryMessageCollection is nil")
		}
	}

	return nil
}
