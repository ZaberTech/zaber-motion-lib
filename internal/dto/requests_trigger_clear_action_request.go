/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerClearActionRequest struct {
	InterfaceId   int32              `bson:"interfaceId"`
	Device        int32              `bson:"device"`
	TriggerNumber int32              `bson:"triggerNumber"`
	Action        AsciiTriggerAction `bson:"action"`
}

func (c *TriggerClearActionRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerClearActionRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerClearActionRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerClearActionRequest) GetAction() AsciiTriggerAction {
	return c.Action
}

func (c *TriggerClearActionRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerClearActionRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerClearActionRequest) Clone() (*TriggerClearActionRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerClearActionRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerClearActionRequest) Sanitize() error {
	return nil
}
