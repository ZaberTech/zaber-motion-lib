/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ChannelOn struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Axis        int32 `bson:"axis"`
	On          bool  `bson:"on"`
}

func (c *ChannelOn) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ChannelOn) GetDevice() int32 {
	return c.Device
}

func (c *ChannelOn) GetAxis() int32 {
	return c.Axis
}

func (c *ChannelOn) GetOn() bool {
	return c.On
}

func (c *ChannelOn) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ChannelOn) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ChannelOn) Clone() (*ChannelOn, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ChannelOn{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ChannelOn) Sanitize() error {
	return nil
}
