/* This file is generated. Do not modify by hand. */
package dto

// Mode of a stream.
type AsciiStreamMode int32

const (
	AsciiStreamMode_DISABLED             AsciiStreamMode = 0
	AsciiStreamMode_STORE                AsciiStreamMode = 1
	AsciiStreamMode_STORE_ARBITRARY_AXES AsciiStreamMode = 2
	AsciiStreamMode_LIVE                 AsciiStreamMode = 3
)

var AsciiStreamMode_name = map[AsciiStreamMode]string{
	AsciiStreamMode_DISABLED:             "Disabled",
	AsciiStreamMode_STORE:                "Store",
	AsciiStreamMode_STORE_ARBITRARY_AXES: "StoreArbitraryAxes",
	AsciiStreamMode_LIVE:                 "Live",
}
