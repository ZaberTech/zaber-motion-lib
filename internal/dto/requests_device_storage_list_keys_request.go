/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceStorageListKeysRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Prefix      *string `bson:"prefix"`
}

func (c *DeviceStorageListKeysRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceStorageListKeysRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceStorageListKeysRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceStorageListKeysRequest) GetPrefix() *string {
	return c.Prefix
}

func (c *DeviceStorageListKeysRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceStorageListKeysRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceStorageListKeysRequest) Clone() (*DeviceStorageListKeysRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceStorageListKeysRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceStorageListKeysRequest) Sanitize() error {
	return nil
}
