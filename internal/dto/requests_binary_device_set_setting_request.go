/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceSetSettingRequest struct {
	InterfaceId int32                `bson:"interfaceId"`
	Device      int32                `bson:"device"`
	Setting     BinaryBinarySettings `bson:"setting"`
	Value       float64              `bson:"value"`
	Unit        string               `bson:"unit"`
}

func (c *BinaryDeviceSetSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceSetSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryDeviceSetSettingRequest) GetSetting() BinaryBinarySettings {
	return c.Setting
}

func (c *BinaryDeviceSetSettingRequest) GetValue() float64 {
	return c.Value
}

func (c *BinaryDeviceSetSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *BinaryDeviceSetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceSetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceSetSettingRequest) Clone() (*BinaryDeviceSetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceSetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceSetSettingRequest) Sanitize() error {
	return nil
}
