/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The label of an IO port.
type AsciiIoPortLabel struct {
	// The type of the port.
	PortType AsciiIoPortType `bson:"portType"`
	// The number of the port.
	ChannelNumber int32 `bson:"channelNumber"`
	// The label of the port.
	Label string `bson:"label"`
}

// The type of the port.
func (c *AsciiIoPortLabel) GetPortType() AsciiIoPortType {
	return c.PortType
}

// The number of the port.
func (c *AsciiIoPortLabel) GetChannelNumber() int32 {
	return c.ChannelNumber
}

// The label of the port.
func (c *AsciiIoPortLabel) GetLabel() string {
	return c.Label
}

func (c *AsciiIoPortLabel) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiIoPortLabel) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiIoPortLabel) Clone() (*AsciiIoPortLabel, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiIoPortLabel{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiIoPortLabel) Sanitize() error {
	return nil
}
