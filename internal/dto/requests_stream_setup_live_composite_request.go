/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetupLiveCompositeRequest struct {
	InterfaceId int32                        `bson:"interfaceId"`
	Device      int32                        `bson:"device"`
	StreamId    int32                        `bson:"streamId"`
	Pvt         bool                         `bson:"pvt"`
	Axes        []*AsciiStreamAxisDefinition `bson:"axes"`
	PvtAxes     []*AsciiPvtAxisDefinition    `bson:"pvtAxes"`
}

func (c *StreamSetupLiveCompositeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetupLiveCompositeRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetupLiveCompositeRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetupLiveCompositeRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetupLiveCompositeRequest) GetAxes() []*AsciiStreamAxisDefinition {
	return c.Axes
}

func (c *StreamSetupLiveCompositeRequest) GetPvtAxes() []*AsciiPvtAxisDefinition {
	return c.PvtAxes
}

func (c *StreamSetupLiveCompositeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetupLiveCompositeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetupLiveCompositeRequest) Clone() (*StreamSetupLiveCompositeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetupLiveCompositeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetupLiveCompositeRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]*AsciiStreamAxisDefinition, 0)
	}

	for _, item := range c.Axes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Axes of StreamSetupLiveCompositeRequest is nil")
		}
	}

	if c.PvtAxes == nil {
		c.PvtAxes = make([]*AsciiPvtAxisDefinition, 0)
	}

	for _, item := range c.PvtAxes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property PvtAxes of StreamSetupLiveCompositeRequest is nil")
		}
	}

	return nil
}
