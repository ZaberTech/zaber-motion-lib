/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAllAnalogOutputsScheduleRequest struct {
	InterfaceId  int32     `bson:"interfaceId"`
	Device       int32     `bson:"device"`
	StreamId     int32     `bson:"streamId"`
	Pvt          bool      `bson:"pvt"`
	Values       []float64 `bson:"values"`
	FutureValues []float64 `bson:"futureValues"`
	Delay        float64   `bson:"delay"`
	Unit         string    `bson:"unit"`
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetValues() []float64 {
	return c.Values
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetFutureValues() []float64 {
	return c.FutureValues
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAllAnalogOutputsScheduleRequest) Clone() (*StreamSetAllAnalogOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAllAnalogOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAllAnalogOutputsScheduleRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	if c.FutureValues == nil {
		c.FutureValues = make([]float64, 0)
	}

	return nil
}
