/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AxesEmptyRequest struct {
	Interfaces []int32 `bson:"interfaces"`
	Devices    []int32 `bson:"devices"`
	Axes       []int32 `bson:"axes"`
}

func (c *AxesEmptyRequest) GetInterfaces() []int32 {
	return c.Interfaces
}

func (c *AxesEmptyRequest) GetDevices() []int32 {
	return c.Devices
}

func (c *AxesEmptyRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *AxesEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxesEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxesEmptyRequest) Clone() (*AxesEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxesEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxesEmptyRequest) Sanitize() error {
	if c.Interfaces == nil {
		c.Interfaces = make([]int32, 0)
	}

	if c.Devices == nil {
		c.Devices = make([]int32, 0)
	}

	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	return nil
}
