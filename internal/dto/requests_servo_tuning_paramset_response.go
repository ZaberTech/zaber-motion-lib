/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ServoTuningParamsetResponse struct {
	Paramset AsciiServoTuningParamset `bson:"paramset"`
}

func (c *ServoTuningParamsetResponse) GetParamset() AsciiServoTuningParamset {
	return c.Paramset
}

func (c *ServoTuningParamsetResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ServoTuningParamsetResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ServoTuningParamsetResponse) Clone() (*ServoTuningParamsetResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ServoTuningParamsetResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ServoTuningParamsetResponse) Sanitize() error {
	return nil
}
