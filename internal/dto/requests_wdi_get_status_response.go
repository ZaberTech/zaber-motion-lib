/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type WdiGetStatusResponse struct {
	Status *MicroscopyWdiAutofocusProviderStatus `bson:"status"`
}

func (c *WdiGetStatusResponse) GetStatus() *MicroscopyWdiAutofocusProviderStatus {
	return c.Status
}

func (c *WdiGetStatusResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *WdiGetStatusResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *WdiGetStatusResponse) Clone() (*WdiGetStatusResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &WdiGetStatusResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *WdiGetStatusResponse) Sanitize() error {
	if c.Status != nil {
		if err := c.Status.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Status of WdiGetStatusResponse is nil")
	}

	return nil
}
