/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepGetRequest struct {
	InterfaceId     int32  `bson:"interfaceId"`
	Device          int32  `bson:"device"`
	LockstepGroupId int32  `bson:"lockstepGroupId"`
	Unit            string `bson:"unit"`
}

func (c *LockstepGetRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepGetRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepGetRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepGetRequest) GetUnit() string {
	return c.Unit
}

func (c *LockstepGetRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepGetRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepGetRequest) Clone() (*LockstepGetRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepGetRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepGetRequest) Sanitize() error {
	return nil
}
