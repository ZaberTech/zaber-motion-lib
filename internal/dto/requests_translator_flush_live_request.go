/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorFlushLiveRequest struct {
	TranslatorId  int32 `bson:"translatorId"`
	WaitUntilIdle bool  `bson:"waitUntilIdle"`
}

func (c *TranslatorFlushLiveRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorFlushLiveRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *TranslatorFlushLiveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorFlushLiveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorFlushLiveRequest) Clone() (*TranslatorFlushLiveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorFlushLiveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorFlushLiveRequest) Sanitize() error {
	return nil
}
