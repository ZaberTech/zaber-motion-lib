/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type UnknownResponseEventWrapper struct {
	InterfaceId     int32                      `bson:"interfaceId"`
	UnknownResponse *AsciiUnknownResponseEvent `bson:"unknownResponse"`
}

func (c *UnknownResponseEventWrapper) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *UnknownResponseEventWrapper) GetUnknownResponse() *AsciiUnknownResponseEvent {
	return c.UnknownResponse
}

func (c *UnknownResponseEventWrapper) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *UnknownResponseEventWrapper) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *UnknownResponseEventWrapper) Clone() (*UnknownResponseEventWrapper, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &UnknownResponseEventWrapper{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *UnknownResponseEventWrapper) Sanitize() error {
	if c.UnknownResponse != nil {
		if err := c.UnknownResponse.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property UnknownResponse of UnknownResponseEventWrapper is nil")
	}

	return nil
}
