/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetAllAnalogIORequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	ChannelType string `bson:"channelType"`
}

func (c *DeviceGetAllAnalogIORequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetAllAnalogIORequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetAllAnalogIORequest) GetChannelType() string {
	return c.ChannelType
}

func (c *DeviceGetAllAnalogIORequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetAllAnalogIORequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetAllAnalogIORequest) Clone() (*DeviceGetAllAnalogIORequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetAllAnalogIORequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetAllAnalogIORequest) Sanitize() error {
	return nil
}
