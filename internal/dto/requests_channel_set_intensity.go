/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ChannelSetIntensity struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Intensity   float64 `bson:"intensity"`
}

func (c *ChannelSetIntensity) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ChannelSetIntensity) GetDevice() int32 {
	return c.Device
}

func (c *ChannelSetIntensity) GetAxis() int32 {
	return c.Axis
}

func (c *ChannelSetIntensity) GetIntensity() float64 {
	return c.Intensity
}

func (c *ChannelSetIntensity) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ChannelSetIntensity) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ChannelSetIntensity) Clone() (*ChannelSetIntensity, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ChannelSetIntensity{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ChannelSetIntensity) Sanitize() error {
	return nil
}
