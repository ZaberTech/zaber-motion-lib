/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetSettingStrRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Setting     string `bson:"setting"`
	Value       string `bson:"value"`
}

func (c *DeviceSetSettingStrRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetSettingStrRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetSettingStrRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetSettingStrRequest) GetSetting() string {
	return c.Setting
}

func (c *DeviceSetSettingStrRequest) GetValue() string {
	return c.Value
}

func (c *DeviceSetSettingStrRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetSettingStrRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetSettingStrRequest) Clone() (*DeviceSetSettingStrRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetSettingStrRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetSettingStrRequest) Sanitize() error {
	return nil
}
