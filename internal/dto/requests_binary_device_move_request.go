/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceMoveRequest struct {
	InterfaceId int32        `bson:"interfaceId"`
	Device      int32        `bson:"device"`
	Timeout     float64      `bson:"timeout"`
	Type        AxisMoveType `bson:"type"`
	Arg         float64      `bson:"arg"`
	Unit        string       `bson:"unit"`
}

func (c *BinaryDeviceMoveRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceMoveRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryDeviceMoveRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *BinaryDeviceMoveRequest) GetType() AxisMoveType {
	return c.Type
}

func (c *BinaryDeviceMoveRequest) GetArg() float64 {
	return c.Arg
}

func (c *BinaryDeviceMoveRequest) GetUnit() string {
	return c.Unit
}

func (c *BinaryDeviceMoveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceMoveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceMoveRequest) Clone() (*BinaryDeviceMoveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceMoveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceMoveRequest) Sanitize() error {
	return nil
}
