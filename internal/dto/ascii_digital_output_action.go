/* This file is generated. Do not modify by hand. */
package dto

// Action type for digital output.
type AsciiDigitalOutputAction int32

const (
	AsciiDigitalOutputAction_OFF    AsciiDigitalOutputAction = 0
	AsciiDigitalOutputAction_ON     AsciiDigitalOutputAction = 1
	AsciiDigitalOutputAction_TOGGLE AsciiDigitalOutputAction = 2
	AsciiDigitalOutputAction_KEEP   AsciiDigitalOutputAction = 3
)

var AsciiDigitalOutputAction_name = map[AsciiDigitalOutputAction]string{
	AsciiDigitalOutputAction_OFF:    "Off",
	AsciiDigitalOutputAction_ON:     "On",
	AsciiDigitalOutputAction_TOGGLE: "Toggle",
	AsciiDigitalOutputAction_KEEP:   "Keep",
}
