/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorCreateFromDeviceRequest struct {
	InterfaceId int32                  `bson:"interfaceId"`
	Device      int32                  `bson:"device"`
	Axes        []int32                `bson:"axes"`
	Config      *GcodeTranslatorConfig `bson:"config"`
}

func (c *TranslatorCreateFromDeviceRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TranslatorCreateFromDeviceRequest) GetDevice() int32 {
	return c.Device
}

func (c *TranslatorCreateFromDeviceRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *TranslatorCreateFromDeviceRequest) GetConfig() *GcodeTranslatorConfig {
	return c.Config
}

func (c *TranslatorCreateFromDeviceRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorCreateFromDeviceRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorCreateFromDeviceRequest) Clone() (*TranslatorCreateFromDeviceRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorCreateFromDeviceRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorCreateFromDeviceRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
