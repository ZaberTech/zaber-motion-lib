/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OpenInterfaceResponse struct {
	InterfaceId int32 `bson:"interfaceId"`
}

func (c *OpenInterfaceResponse) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *OpenInterfaceResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OpenInterfaceResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OpenInterfaceResponse) Clone() (*OpenInterfaceResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OpenInterfaceResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OpenInterfaceResponse) Sanitize() error {
	return nil
}
