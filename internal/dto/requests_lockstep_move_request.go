/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepMoveRequest struct {
	InterfaceId      int32        `bson:"interfaceId"`
	Device           int32        `bson:"device"`
	LockstepGroupId  int32        `bson:"lockstepGroupId"`
	WaitUntilIdle    bool         `bson:"waitUntilIdle"`
	Type             AxisMoveType `bson:"type"`
	Arg              float64      `bson:"arg"`
	Unit             string       `bson:"unit"`
	Velocity         float64      `bson:"velocity"`
	VelocityUnit     string       `bson:"velocityUnit"`
	Acceleration     float64      `bson:"acceleration"`
	AccelerationUnit string       `bson:"accelerationUnit"`
}

func (c *LockstepMoveRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepMoveRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepMoveRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepMoveRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *LockstepMoveRequest) GetType() AxisMoveType {
	return c.Type
}

func (c *LockstepMoveRequest) GetArg() float64 {
	return c.Arg
}

func (c *LockstepMoveRequest) GetUnit() string {
	return c.Unit
}

func (c *LockstepMoveRequest) GetVelocity() float64 {
	return c.Velocity
}

func (c *LockstepMoveRequest) GetVelocityUnit() string {
	return c.VelocityUnit
}

func (c *LockstepMoveRequest) GetAcceleration() float64 {
	return c.Acceleration
}

func (c *LockstepMoveRequest) GetAccelerationUnit() string {
	return c.AccelerationUnit
}

func (c *LockstepMoveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepMoveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepMoveRequest) Clone() (*LockstepMoveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepMoveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepMoveRequest) Sanitize() error {
	return nil
}
