/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Status of the autofocus.
type MicroscopyAutofocusStatus struct {
	// Indicates whether the autofocus is in focus.
	InFocus bool `bson:"inFocus"`
	// Indicates whether the autofocus is in range.
	InRange bool `bson:"inRange"`
}

// Indicates whether the autofocus is in focus.
func (c *MicroscopyAutofocusStatus) GetInFocus() bool {
	return c.InFocus
}

// Indicates whether the autofocus is in range.
func (c *MicroscopyAutofocusStatus) GetInRange() bool {
	return c.InRange
}

func (c *MicroscopyAutofocusStatus) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopyAutofocusStatus) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopyAutofocusStatus) Clone() (*MicroscopyAutofocusStatus, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopyAutofocusStatus{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopyAutofocusStatus) Sanitize() error {
	return nil
}
