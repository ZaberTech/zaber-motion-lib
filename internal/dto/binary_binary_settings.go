/* This file is generated. Do not modify by hand. */
package dto

// Named constants for all Zaber Binary protocol settings.
type BinaryBinarySettings int32

const (
	BinaryBinarySettings_ACCELERATION                       BinaryBinarySettings = 0
	BinaryBinarySettings_ACCELERATION_ONLY                  BinaryBinarySettings = 1
	BinaryBinarySettings_ACTIVE_AXIS                        BinaryBinarySettings = 2
	BinaryBinarySettings_ALIAS_NUMBER                       BinaryBinarySettings = 3
	BinaryBinarySettings_ANALOG_INPUT_COUNT                 BinaryBinarySettings = 4
	BinaryBinarySettings_ANALOG_OUTPUT_COUNT                BinaryBinarySettings = 5
	BinaryBinarySettings_AUTO_HOME_DISABLED_MODE            BinaryBinarySettings = 6
	BinaryBinarySettings_AUTO_REPLY_DISABLED_MODE           BinaryBinarySettings = 7
	BinaryBinarySettings_AXIS_DEVICE_NUMBER                 BinaryBinarySettings = 8
	BinaryBinarySettings_AXIS_INVERSION                     BinaryBinarySettings = 9
	BinaryBinarySettings_AXIS_VELOCITY_PROFILE              BinaryBinarySettings = 10
	BinaryBinarySettings_AXIS_VELOCITY_SCALE                BinaryBinarySettings = 11
	BinaryBinarySettings_BAUD_RATE                          BinaryBinarySettings = 12
	BinaryBinarySettings_CALIBRATED_ENCODER_COUNT           BinaryBinarySettings = 13
	BinaryBinarySettings_CALIBRATION_ERROR                  BinaryBinarySettings = 14
	BinaryBinarySettings_CALIBRATION_TYPE                   BinaryBinarySettings = 15
	BinaryBinarySettings_CLOSED_LOOP_MODE                   BinaryBinarySettings = 16
	BinaryBinarySettings_CURRENT_POSITION                   BinaryBinarySettings = 17
	BinaryBinarySettings_CYCLE_DISTANCE                     BinaryBinarySettings = 18
	BinaryBinarySettings_DECELERATION_ONLY                  BinaryBinarySettings = 19
	BinaryBinarySettings_DEVICE_DIRECTION                   BinaryBinarySettings = 20
	BinaryBinarySettings_DEVICE_ID                          BinaryBinarySettings = 21
	BinaryBinarySettings_DEVICE_MODE                        BinaryBinarySettings = 22
	BinaryBinarySettings_DIGITAL_INPUT_COUNT                BinaryBinarySettings = 23
	BinaryBinarySettings_DIGITAL_OUTPUT_COUNT               BinaryBinarySettings = 24
	BinaryBinarySettings_ENCODER_COUNT                      BinaryBinarySettings = 25
	BinaryBinarySettings_ENCODER_POSITION                   BinaryBinarySettings = 26
	BinaryBinarySettings_FILTER_HOLDER_ID                   BinaryBinarySettings = 27
	BinaryBinarySettings_FIRMWARE_BUILD                     BinaryBinarySettings = 28
	BinaryBinarySettings_FIRMWARE_VERSION                   BinaryBinarySettings = 29
	BinaryBinarySettings_HOLD_CURRENT                       BinaryBinarySettings = 30
	BinaryBinarySettings_HOME_OFFSET                        BinaryBinarySettings = 31
	BinaryBinarySettings_HOME_SENSOR_TYPE                   BinaryBinarySettings = 32
	BinaryBinarySettings_HOME_SPEED                         BinaryBinarySettings = 33
	BinaryBinarySettings_HOME_STATUS                        BinaryBinarySettings = 34
	BinaryBinarySettings_INDEX_DISTANCE                     BinaryBinarySettings = 35
	BinaryBinarySettings_JOYSTICK_CALIBRATION_MODE          BinaryBinarySettings = 36
	BinaryBinarySettings_KNOB_DIRECTION                     BinaryBinarySettings = 37
	BinaryBinarySettings_KNOB_DISABLED_MODE                 BinaryBinarySettings = 38
	BinaryBinarySettings_KNOB_JOG_SIZE                      BinaryBinarySettings = 39
	BinaryBinarySettings_KNOB_MOVEMENT_MODE                 BinaryBinarySettings = 40
	BinaryBinarySettings_KNOB_VELOCITY_PROFILE              BinaryBinarySettings = 41
	BinaryBinarySettings_KNOB_VELOCITY_SCALE                BinaryBinarySettings = 42
	BinaryBinarySettings_LOCK_STATE                         BinaryBinarySettings = 43
	BinaryBinarySettings_MANUAL_MOVE_TRACKING_DISABLED_MODE BinaryBinarySettings = 44
	BinaryBinarySettings_MAXIMUM_POSITION                   BinaryBinarySettings = 45
	BinaryBinarySettings_MAXIMUM_RELATIVE_MOVE              BinaryBinarySettings = 46
	BinaryBinarySettings_MESSAGE_ID_MODE                    BinaryBinarySettings = 47
	BinaryBinarySettings_MICROSTEP_RESOLUTION               BinaryBinarySettings = 48
	BinaryBinarySettings_MINIMUM_POSITION                   BinaryBinarySettings = 49
	BinaryBinarySettings_MOVE_TRACKING_MODE                 BinaryBinarySettings = 50
	BinaryBinarySettings_MOVE_TRACKING_PERIOD               BinaryBinarySettings = 51
	BinaryBinarySettings_PARK_STATE                         BinaryBinarySettings = 52
	BinaryBinarySettings_PERIPHERAL_ID                      BinaryBinarySettings = 53
	BinaryBinarySettings_PERIPHERAL_ID_PENDING              BinaryBinarySettings = 54
	BinaryBinarySettings_PERIPHERAL_SERIAL_NUMBER           BinaryBinarySettings = 55
	BinaryBinarySettings_PERIPHERAL_SERIAL_PENDING          BinaryBinarySettings = 56
	BinaryBinarySettings_POWER_SUPPLY_VOLTAGE               BinaryBinarySettings = 57
	BinaryBinarySettings_PROTOCOL                           BinaryBinarySettings = 58
	BinaryBinarySettings_RUNNING_CURRENT                    BinaryBinarySettings = 59
	BinaryBinarySettings_SERIAL_NUMBER                      BinaryBinarySettings = 60
	BinaryBinarySettings_SLIP_TRACKING_PERIOD               BinaryBinarySettings = 61
	BinaryBinarySettings_STALL_TIMEOUT                      BinaryBinarySettings = 62
	BinaryBinarySettings_STATUS                             BinaryBinarySettings = 63
	BinaryBinarySettings_TARGET_SPEED                       BinaryBinarySettings = 64
)

var BinaryBinarySettings_name = map[BinaryBinarySettings]string{
	BinaryBinarySettings_ACCELERATION:                       "Acceleration",
	BinaryBinarySettings_ACCELERATION_ONLY:                  "AccelerationOnly",
	BinaryBinarySettings_ACTIVE_AXIS:                        "ActiveAxis",
	BinaryBinarySettings_ALIAS_NUMBER:                       "AliasNumber",
	BinaryBinarySettings_ANALOG_INPUT_COUNT:                 "AnalogInputCount",
	BinaryBinarySettings_ANALOG_OUTPUT_COUNT:                "AnalogOutputCount",
	BinaryBinarySettings_AUTO_HOME_DISABLED_MODE:            "AutoHomeDisabledMode",
	BinaryBinarySettings_AUTO_REPLY_DISABLED_MODE:           "AutoReplyDisabledMode",
	BinaryBinarySettings_AXIS_DEVICE_NUMBER:                 "AxisDeviceNumber",
	BinaryBinarySettings_AXIS_INVERSION:                     "AxisInversion",
	BinaryBinarySettings_AXIS_VELOCITY_PROFILE:              "AxisVelocityProfile",
	BinaryBinarySettings_AXIS_VELOCITY_SCALE:                "AxisVelocityScale",
	BinaryBinarySettings_BAUD_RATE:                          "BaudRate",
	BinaryBinarySettings_CALIBRATED_ENCODER_COUNT:           "CalibratedEncoderCount",
	BinaryBinarySettings_CALIBRATION_ERROR:                  "CalibrationError",
	BinaryBinarySettings_CALIBRATION_TYPE:                   "CalibrationType",
	BinaryBinarySettings_CLOSED_LOOP_MODE:                   "ClosedLoopMode",
	BinaryBinarySettings_CURRENT_POSITION:                   "CurrentPosition",
	BinaryBinarySettings_CYCLE_DISTANCE:                     "CycleDistance",
	BinaryBinarySettings_DECELERATION_ONLY:                  "DecelerationOnly",
	BinaryBinarySettings_DEVICE_DIRECTION:                   "DeviceDirection",
	BinaryBinarySettings_DEVICE_ID:                          "DeviceID",
	BinaryBinarySettings_DEVICE_MODE:                        "DeviceMode",
	BinaryBinarySettings_DIGITAL_INPUT_COUNT:                "DigitalInputCount",
	BinaryBinarySettings_DIGITAL_OUTPUT_COUNT:               "DigitalOutputCount",
	BinaryBinarySettings_ENCODER_COUNT:                      "EncoderCount",
	BinaryBinarySettings_ENCODER_POSITION:                   "EncoderPosition",
	BinaryBinarySettings_FILTER_HOLDER_ID:                   "FilterHolderID",
	BinaryBinarySettings_FIRMWARE_BUILD:                     "FirmwareBuild",
	BinaryBinarySettings_FIRMWARE_VERSION:                   "FirmwareVersion",
	BinaryBinarySettings_HOLD_CURRENT:                       "HoldCurrent",
	BinaryBinarySettings_HOME_OFFSET:                        "HomeOffset",
	BinaryBinarySettings_HOME_SENSOR_TYPE:                   "HomeSensorType",
	BinaryBinarySettings_HOME_SPEED:                         "HomeSpeed",
	BinaryBinarySettings_HOME_STATUS:                        "HomeStatus",
	BinaryBinarySettings_INDEX_DISTANCE:                     "IndexDistance",
	BinaryBinarySettings_JOYSTICK_CALIBRATION_MODE:          "JoystickCalibrationMode",
	BinaryBinarySettings_KNOB_DIRECTION:                     "KnobDirection",
	BinaryBinarySettings_KNOB_DISABLED_MODE:                 "KnobDisabledMode",
	BinaryBinarySettings_KNOB_JOG_SIZE:                      "KnobJogSize",
	BinaryBinarySettings_KNOB_MOVEMENT_MODE:                 "KnobMovementMode",
	BinaryBinarySettings_KNOB_VELOCITY_PROFILE:              "KnobVelocityProfile",
	BinaryBinarySettings_KNOB_VELOCITY_SCALE:                "KnobVelocityScale",
	BinaryBinarySettings_LOCK_STATE:                         "LockState",
	BinaryBinarySettings_MANUAL_MOVE_TRACKING_DISABLED_MODE: "ManualMoveTrackingDisabledMode",
	BinaryBinarySettings_MAXIMUM_POSITION:                   "MaximumPosition",
	BinaryBinarySettings_MAXIMUM_RELATIVE_MOVE:              "MaximumRelativeMove",
	BinaryBinarySettings_MESSAGE_ID_MODE:                    "MessageIDMode",
	BinaryBinarySettings_MICROSTEP_RESOLUTION:               "MicrostepResolution",
	BinaryBinarySettings_MINIMUM_POSITION:                   "MinimumPosition",
	BinaryBinarySettings_MOVE_TRACKING_MODE:                 "MoveTrackingMode",
	BinaryBinarySettings_MOVE_TRACKING_PERIOD:               "MoveTrackingPeriod",
	BinaryBinarySettings_PARK_STATE:                         "ParkState",
	BinaryBinarySettings_PERIPHERAL_ID:                      "PeripheralID",
	BinaryBinarySettings_PERIPHERAL_ID_PENDING:              "PeripheralIDPending",
	BinaryBinarySettings_PERIPHERAL_SERIAL_NUMBER:           "PeripheralSerialNumber",
	BinaryBinarySettings_PERIPHERAL_SERIAL_PENDING:          "PeripheralSerialPending",
	BinaryBinarySettings_POWER_SUPPLY_VOLTAGE:               "PowerSupplyVoltage",
	BinaryBinarySettings_PROTOCOL:                           "Protocol",
	BinaryBinarySettings_RUNNING_CURRENT:                    "RunningCurrent",
	BinaryBinarySettings_SERIAL_NUMBER:                      "SerialNumber",
	BinaryBinarySettings_SLIP_TRACKING_PERIOD:               "SlipTrackingPeriod",
	BinaryBinarySettings_STALL_TIMEOUT:                      "StallTimeout",
	BinaryBinarySettings_STATUS:                             "Status",
	BinaryBinarySettings_TARGET_SPEED:                       "TargetSpeed",
}
