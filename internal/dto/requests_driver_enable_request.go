/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DriverEnableRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Timeout     float64 `bson:"timeout"`
}

func (c *DriverEnableRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DriverEnableRequest) GetDevice() int32 {
	return c.Device
}

func (c *DriverEnableRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DriverEnableRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *DriverEnableRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DriverEnableRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DriverEnableRequest) Clone() (*DriverEnableRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DriverEnableRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DriverEnableRequest) Sanitize() error {
	return nil
}
