/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Represents unit conversion factor for a single dimension.
type AsciiConversionFactor struct {
	// Setting representing the dimension.
	Setting string `bson:"setting"`
	// Value representing 1 native device unit in specified real-word units.
	Value float64 `bson:"value"`
	// Units of the value.
	Unit string `bson:"unit"`
}

// Setting representing the dimension.
func (c *AsciiConversionFactor) GetSetting() string {
	return c.Setting
}

// Value representing 1 native device unit in specified real-word units.
func (c *AsciiConversionFactor) GetValue() float64 {
	return c.Value
}

// Units of the value.
func (c *AsciiConversionFactor) GetUnit() string {
	return c.Unit
}

func (c *AsciiConversionFactor) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiConversionFactor) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiConversionFactor) Clone() (*AsciiConversionFactor, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiConversionFactor{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiConversionFactor) Sanitize() error {
	return nil
}
