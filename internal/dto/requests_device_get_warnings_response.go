/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetWarningsResponse struct {
	Flags []string `bson:"flags"`
}

func (c *DeviceGetWarningsResponse) GetFlags() []string {
	return c.Flags
}

func (c *DeviceGetWarningsResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetWarningsResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetWarningsResponse) Clone() (*DeviceGetWarningsResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetWarningsResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetWarningsResponse) Sanitize() error {
	if c.Flags == nil {
		c.Flags = make([]string, 0)
	}

	return nil
}
