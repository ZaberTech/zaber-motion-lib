/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamGenericCommandBatchRequest struct {
	InterfaceId int32    `bson:"interfaceId"`
	Device      int32    `bson:"device"`
	StreamId    int32    `bson:"streamId"`
	Pvt         bool     `bson:"pvt"`
	Batch       []string `bson:"batch"`
}

func (c *StreamGenericCommandBatchRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamGenericCommandBatchRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamGenericCommandBatchRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamGenericCommandBatchRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamGenericCommandBatchRequest) GetBatch() []string {
	return c.Batch
}

func (c *StreamGenericCommandBatchRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamGenericCommandBatchRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamGenericCommandBatchRequest) Clone() (*StreamGenericCommandBatchRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamGenericCommandBatchRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamGenericCommandBatchRequest) Sanitize() error {
	if c.Batch == nil {
		c.Batch = make([]string, 0)
	}

	return nil
}
