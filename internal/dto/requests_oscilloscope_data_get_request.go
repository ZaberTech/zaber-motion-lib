/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeDataGetRequest struct {
	DataId int32  `bson:"dataId"`
	Unit   string `bson:"unit"`
}

func (c *OscilloscopeDataGetRequest) GetDataId() int32 {
	return c.DataId
}

func (c *OscilloscopeDataGetRequest) GetUnit() string {
	return c.Unit
}

func (c *OscilloscopeDataGetRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeDataGetRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeDataGetRequest) Clone() (*OscilloscopeDataGetRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeDataGetRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeDataGetRequest) Sanitize() error {
	return nil
}
