/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetInterfaceChecksumEnabledRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	IsEnabled   bool  `bson:"isEnabled"`
}

func (c *SetInterfaceChecksumEnabledRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetInterfaceChecksumEnabledRequest) GetIsEnabled() bool {
	return c.IsEnabled
}

func (c *SetInterfaceChecksumEnabledRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetInterfaceChecksumEnabledRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetInterfaceChecksumEnabledRequest) Clone() (*SetInterfaceChecksumEnabledRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetInterfaceChecksumEnabledRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetInterfaceChecksumEnabledRequest) Sanitize() error {
	return nil
}
