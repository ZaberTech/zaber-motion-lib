/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ObjectiveChangerRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	TurretAddress int32 `bson:"turretAddress"`
	FocusAddress  int32 `bson:"focusAddress"`
	FocusAxis     int32 `bson:"focusAxis"`
}

func (c *ObjectiveChangerRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ObjectiveChangerRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *ObjectiveChangerRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *ObjectiveChangerRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *ObjectiveChangerRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ObjectiveChangerRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ObjectiveChangerRequest) Clone() (*ObjectiveChangerRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ObjectiveChangerRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ObjectiveChangerRequest) Sanitize() error {
	return nil
}
