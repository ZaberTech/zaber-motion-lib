/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceMoveRequest struct {
	InterfaceId      int32        `bson:"interfaceId"`
	Device           int32        `bson:"device"`
	Axis             int32        `bson:"axis"`
	WaitUntilIdle    bool         `bson:"waitUntilIdle"`
	Type             AxisMoveType `bson:"type"`
	Arg              float64      `bson:"arg"`
	ArgInt           int32        `bson:"argInt"`
	Unit             string       `bson:"unit"`
	Velocity         float64      `bson:"velocity"`
	VelocityUnit     string       `bson:"velocityUnit"`
	Acceleration     float64      `bson:"acceleration"`
	AccelerationUnit string       `bson:"accelerationUnit"`
}

func (c *DeviceMoveRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceMoveRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceMoveRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceMoveRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *DeviceMoveRequest) GetType() AxisMoveType {
	return c.Type
}

func (c *DeviceMoveRequest) GetArg() float64 {
	return c.Arg
}

func (c *DeviceMoveRequest) GetArgInt() int32 {
	return c.ArgInt
}

func (c *DeviceMoveRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceMoveRequest) GetVelocity() float64 {
	return c.Velocity
}

func (c *DeviceMoveRequest) GetVelocityUnit() string {
	return c.VelocityUnit
}

func (c *DeviceMoveRequest) GetAcceleration() float64 {
	return c.Acceleration
}

func (c *DeviceMoveRequest) GetAccelerationUnit() string {
	return c.AccelerationUnit
}

func (c *DeviceMoveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceMoveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceMoveRequest) Clone() (*DeviceMoveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceMoveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceMoveRequest) Sanitize() error {
	return nil
}
