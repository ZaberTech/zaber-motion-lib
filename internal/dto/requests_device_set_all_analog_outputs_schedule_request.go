/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAllAnalogOutputsScheduleRequest struct {
	InterfaceId  int32     `bson:"interfaceId"`
	Device       int32     `bson:"device"`
	Values       []float64 `bson:"values"`
	FutureValues []float64 `bson:"futureValues"`
	Delay        float64   `bson:"delay"`
	Unit         string    `bson:"unit"`
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetValues() []float64 {
	return c.Values
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetFutureValues() []float64 {
	return c.FutureValues
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAllAnalogOutputsScheduleRequest) Clone() (*DeviceSetAllAnalogOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAllAnalogOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAllAnalogOutputsScheduleRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	if c.FutureValues == nil {
		c.FutureValues = make([]float64, 0)
	}

	return nil
}
