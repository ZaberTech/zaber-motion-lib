/* This file is generated. Do not modify by hand. */
package dto

// Denotes type of an device and units it accepts.
type BinaryDeviceType int32

const (
	BinaryDeviceType_UNKNOWN BinaryDeviceType = 0
	BinaryDeviceType_LINEAR  BinaryDeviceType = 1
	BinaryDeviceType_ROTARY  BinaryDeviceType = 2
)

var BinaryDeviceType_name = map[BinaryDeviceType]string{
	BinaryDeviceType_UNKNOWN: "Unknown",
	BinaryDeviceType_LINEAR:  "Linear",
	BinaryDeviceType_ROTARY:  "Rotary",
}
