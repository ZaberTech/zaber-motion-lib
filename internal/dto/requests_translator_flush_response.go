/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorFlushResponse struct {
	Commands []string `bson:"commands"`
}

func (c *TranslatorFlushResponse) GetCommands() []string {
	return c.Commands
}

func (c *TranslatorFlushResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorFlushResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorFlushResponse) Clone() (*TranslatorFlushResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorFlushResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorFlushResponse) Sanitize() error {
	if c.Commands == nil {
		c.Commands = make([]string, 0)
	}

	return nil
}
