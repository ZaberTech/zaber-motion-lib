/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Class representing version of firmware in the controller.
type FirmwareVersion struct {
	// Major version number.
	Major int32 `bson:"major"`
	// Minor version number.
	Minor int32 `bson:"minor"`
	// Build version number.
	Build int32 `bson:"build"`
}

// Major version number.
func (c *FirmwareVersion) GetMajor() int32 {
	return c.Major
}

// Minor version number.
func (c *FirmwareVersion) GetMinor() int32 {
	return c.Minor
}

// Build version number.
func (c *FirmwareVersion) GetBuild() int32 {
	return c.Build
}

func (c *FirmwareVersion) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *FirmwareVersion) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *FirmwareVersion) Clone() (*FirmwareVersion, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &FirmwareVersion{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *FirmwareVersion) Sanitize() error {
	return nil
}
