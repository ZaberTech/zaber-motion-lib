/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAnalogOutputScheduleRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	StreamId      int32   `bson:"streamId"`
	Pvt           bool    `bson:"pvt"`
	ChannelNumber int32   `bson:"channelNumber"`
	Value         float64 `bson:"value"`
	FutureValue   float64 `bson:"futureValue"`
	Delay         float64 `bson:"delay"`
	Unit          string  `bson:"unit"`
}

func (c *StreamSetAnalogOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAnalogOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAnalogOutputScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAnalogOutputScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAnalogOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamSetAnalogOutputScheduleRequest) GetValue() float64 {
	return c.Value
}

func (c *StreamSetAnalogOutputScheduleRequest) GetFutureValue() float64 {
	return c.FutureValue
}

func (c *StreamSetAnalogOutputScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *StreamSetAnalogOutputScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetAnalogOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAnalogOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAnalogOutputScheduleRequest) Clone() (*StreamSetAnalogOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAnalogOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAnalogOutputScheduleRequest) Sanitize() error {
	return nil
}
