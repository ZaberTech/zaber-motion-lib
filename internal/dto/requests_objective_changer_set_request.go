/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ObjectiveChangerSetRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	TurretAddress int32   `bson:"turretAddress"`
	FocusAddress  int32   `bson:"focusAddress"`
	FocusAxis     int32   `bson:"focusAxis"`
	Value         float64 `bson:"value"`
	Unit          string  `bson:"unit"`
}

func (c *ObjectiveChangerSetRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ObjectiveChangerSetRequest) GetTurretAddress() int32 {
	return c.TurretAddress
}

func (c *ObjectiveChangerSetRequest) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *ObjectiveChangerSetRequest) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *ObjectiveChangerSetRequest) GetValue() float64 {
	return c.Value
}

func (c *ObjectiveChangerSetRequest) GetUnit() string {
	return c.Unit
}

func (c *ObjectiveChangerSetRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ObjectiveChangerSetRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ObjectiveChangerSetRequest) Clone() (*ObjectiveChangerSetRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ObjectiveChangerSetRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ObjectiveChangerSetRequest) Sanitize() error {
	return nil
}
