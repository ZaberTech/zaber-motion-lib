/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetDigitalOutputRequest struct {
	InterfaceId   int32                    `bson:"interfaceId"`
	Device        int32                    `bson:"device"`
	StreamId      int32                    `bson:"streamId"`
	Pvt           bool                     `bson:"pvt"`
	ChannelNumber int32                    `bson:"channelNumber"`
	Value         AsciiDigitalOutputAction `bson:"value"`
}

func (c *StreamSetDigitalOutputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetDigitalOutputRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetDigitalOutputRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetDigitalOutputRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetDigitalOutputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamSetDigitalOutputRequest) GetValue() AsciiDigitalOutputAction {
	return c.Value
}

func (c *StreamSetDigitalOutputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetDigitalOutputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetDigitalOutputRequest) Clone() (*StreamSetDigitalOutputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetDigitalOutputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetDigitalOutputRequest) Sanitize() error {
	return nil
}
