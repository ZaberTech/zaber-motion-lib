/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Holds information about device and its axes for purpose of a translator.
type GcodeDeviceDefinition struct {
	// Device ID of the controller.
	// Can be obtained from device settings.
	DeviceId int32 `bson:"deviceId"`
	// Applicable axes of the device.
	Axes []*GcodeAxisDefinition `bson:"axes"`
	// The smallest of each axis' maxspeed setting value.
	// This value becomes the traverse rate of the translator.
	MaxSpeed *Measurement `bson:"maxSpeed"`
}

// Device ID of the controller.
// Can be obtained from device settings.
func (c *GcodeDeviceDefinition) GetDeviceId() int32 {
	return c.DeviceId
}

// Applicable axes of the device.
func (c *GcodeDeviceDefinition) GetAxes() []*GcodeAxisDefinition {
	return c.Axes
}

// The smallest of each axis' maxspeed setting value.
// This value becomes the traverse rate of the translator.
func (c *GcodeDeviceDefinition) GetMaxSpeed() *Measurement {
	return c.MaxSpeed
}

func (c *GcodeDeviceDefinition) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeDeviceDefinition) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeDeviceDefinition) Clone() (*GcodeDeviceDefinition, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeDeviceDefinition{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeDeviceDefinition) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]*GcodeAxisDefinition, 0)
	}

	for _, item := range c.Axes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Axes of DeviceDefinition is nil")
		}
	}

	if c.MaxSpeed != nil {
		if err := c.MaxSpeed.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property MaxSpeed of DeviceDefinition is nil")
	}

	return nil
}
