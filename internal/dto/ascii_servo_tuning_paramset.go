/* This file is generated. Do not modify by hand. */
package dto

// Servo Tuning Parameter Set to target.
type AsciiServoTuningParamset int32

const (
	AsciiServoTuningParamset_LIVE    AsciiServoTuningParamset = 0
	AsciiServoTuningParamset_P_1     AsciiServoTuningParamset = 1
	AsciiServoTuningParamset_P_2     AsciiServoTuningParamset = 2
	AsciiServoTuningParamset_P_3     AsciiServoTuningParamset = 3
	AsciiServoTuningParamset_P_4     AsciiServoTuningParamset = 4
	AsciiServoTuningParamset_P_5     AsciiServoTuningParamset = 5
	AsciiServoTuningParamset_P_6     AsciiServoTuningParamset = 6
	AsciiServoTuningParamset_P_7     AsciiServoTuningParamset = 7
	AsciiServoTuningParamset_P_8     AsciiServoTuningParamset = 8
	AsciiServoTuningParamset_P_9     AsciiServoTuningParamset = 9
	AsciiServoTuningParamset_STAGING AsciiServoTuningParamset = 10
	AsciiServoTuningParamset_DEFAULT AsciiServoTuningParamset = 11
)

var AsciiServoTuningParamset_name = map[AsciiServoTuningParamset]string{
	AsciiServoTuningParamset_LIVE:    "Live",
	AsciiServoTuningParamset_P_1:     "P1",
	AsciiServoTuningParamset_P_2:     "P2",
	AsciiServoTuningParamset_P_3:     "P3",
	AsciiServoTuningParamset_P_4:     "P4",
	AsciiServoTuningParamset_P_5:     "P5",
	AsciiServoTuningParamset_P_6:     "P6",
	AsciiServoTuningParamset_P_7:     "P7",
	AsciiServoTuningParamset_P_8:     "P8",
	AsciiServoTuningParamset_P_9:     "P9",
	AsciiServoTuningParamset_STAGING: "Staging",
	AsciiServoTuningParamset_DEFAULT: "Default",
}
