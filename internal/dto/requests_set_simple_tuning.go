/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetSimpleTuning struct {
	InterfaceId  int32                    `bson:"interfaceId"`
	Device       int32                    `bson:"device"`
	Axis         int32                    `bson:"axis"`
	Paramset     AsciiServoTuningParamset `bson:"paramset"`
	CarriageMass *float64                 `bson:"carriageMass"`
	LoadMass     float64                  `bson:"loadMass"`
	TuningParams []*AsciiServoTuningParam `bson:"tuningParams"`
}

func (c *SetSimpleTuning) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetSimpleTuning) GetDevice() int32 {
	return c.Device
}

func (c *SetSimpleTuning) GetAxis() int32 {
	return c.Axis
}

func (c *SetSimpleTuning) GetParamset() AsciiServoTuningParamset {
	return c.Paramset
}

func (c *SetSimpleTuning) GetCarriageMass() *float64 {
	return c.CarriageMass
}

func (c *SetSimpleTuning) GetLoadMass() float64 {
	return c.LoadMass
}

func (c *SetSimpleTuning) GetTuningParams() []*AsciiServoTuningParam {
	return c.TuningParams
}

func (c *SetSimpleTuning) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetSimpleTuning) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetSimpleTuning) Clone() (*SetSimpleTuning, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetSimpleTuning{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetSimpleTuning) Sanitize() error {
	if c.TuningParams == nil {
		c.TuningParams = make([]*AsciiServoTuningParam, 0)
	}

	for _, item := range c.TuningParams {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property TuningParams of SetSimpleTuning is nil")
		}
	}

	return nil
}
