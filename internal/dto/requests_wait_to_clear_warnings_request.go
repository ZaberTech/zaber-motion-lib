/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type WaitToClearWarningsRequest struct {
	InterfaceId  int32    `bson:"interfaceId"`
	Device       int32    `bson:"device"`
	Axis         int32    `bson:"axis"`
	Timeout      float64  `bson:"timeout"`
	WarningFlags []string `bson:"warningFlags"`
}

func (c *WaitToClearWarningsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *WaitToClearWarningsRequest) GetDevice() int32 {
	return c.Device
}

func (c *WaitToClearWarningsRequest) GetAxis() int32 {
	return c.Axis
}

func (c *WaitToClearWarningsRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *WaitToClearWarningsRequest) GetWarningFlags() []string {
	return c.WarningFlags
}

func (c *WaitToClearWarningsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *WaitToClearWarningsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *WaitToClearWarningsRequest) Clone() (*WaitToClearWarningsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &WaitToClearWarningsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *WaitToClearWarningsRequest) Sanitize() error {
	if c.WarningFlags == nil {
		c.WarningFlags = make([]string, 0)
	}

	return nil
}
