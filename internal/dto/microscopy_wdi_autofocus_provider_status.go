/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Status of the WDI autofocus.
type MicroscopyWdiAutofocusProviderStatus struct {
	// Indicates whether the autofocus is in range.
	InRange bool `bson:"inRange"`
	// Indicates whether the laser is turned on.
	LaserOn bool `bson:"laserOn"`
}

// Indicates whether the autofocus is in range.
func (c *MicroscopyWdiAutofocusProviderStatus) GetInRange() bool {
	return c.InRange
}

// Indicates whether the laser is turned on.
func (c *MicroscopyWdiAutofocusProviderStatus) GetLaserOn() bool {
	return c.LaserOn
}

func (c *MicroscopyWdiAutofocusProviderStatus) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopyWdiAutofocusProviderStatus) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopyWdiAutofocusProviderStatus) Clone() (*MicroscopyWdiAutofocusProviderStatus, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopyWdiAutofocusProviderStatus{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopyWdiAutofocusProviderStatus) Sanitize() error {
	return nil
}
