/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for DeviceAddressConflictException.
type ExceptionsDeviceAddressConflictExceptionData struct {
	// The full list of detected device addresses.
	DeviceAddresses []int32 `bson:"deviceAddresses"`
}

// The full list of detected device addresses.
func (c *ExceptionsDeviceAddressConflictExceptionData) GetDeviceAddresses() []int32 {
	return c.DeviceAddresses
}

func (c *ExceptionsDeviceAddressConflictExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsDeviceAddressConflictExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsDeviceAddressConflictExceptionData) Clone() (*ExceptionsDeviceAddressConflictExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsDeviceAddressConflictExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsDeviceAddressConflictExceptionData) Sanitize() error {
	if c.DeviceAddresses == nil {
		c.DeviceAddresses = make([]int32, 0)
	}

	return nil
}
