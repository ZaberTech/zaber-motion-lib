/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryGenericWithUnitsRequest struct {
	InterfaceId int32             `bson:"interfaceId"`
	Device      int32             `bson:"device"`
	Command     BinaryCommandCode `bson:"command"`
	Data        float64           `bson:"data"`
	FromUnit    string            `bson:"fromUnit"`
	ToUnit      string            `bson:"toUnit"`
	Timeout     float64           `bson:"timeout"`
}

func (c *BinaryGenericWithUnitsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryGenericWithUnitsRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryGenericWithUnitsRequest) GetCommand() BinaryCommandCode {
	return c.Command
}

func (c *BinaryGenericWithUnitsRequest) GetData() float64 {
	return c.Data
}

func (c *BinaryGenericWithUnitsRequest) GetFromUnit() string {
	return c.FromUnit
}

func (c *BinaryGenericWithUnitsRequest) GetToUnit() string {
	return c.ToUnit
}

func (c *BinaryGenericWithUnitsRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *BinaryGenericWithUnitsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryGenericWithUnitsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryGenericWithUnitsRequest) Clone() (*BinaryGenericWithUnitsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryGenericWithUnitsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryGenericWithUnitsRequest) Sanitize() error {
	return nil
}
