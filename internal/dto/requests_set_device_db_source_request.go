/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetDeviceDbSourceRequest struct {
	SourceType    DeviceDbSourceType `bson:"sourceType"`
	UrlOrFilePath *string            `bson:"urlOrFilePath"`
}

func (c *SetDeviceDbSourceRequest) GetSourceType() DeviceDbSourceType {
	return c.SourceType
}

func (c *SetDeviceDbSourceRequest) GetUrlOrFilePath() *string {
	return c.UrlOrFilePath
}

func (c *SetDeviceDbSourceRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetDeviceDbSourceRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetDeviceDbSourceRequest) Clone() (*SetDeviceDbSourceRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetDeviceDbSourceRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetDeviceDbSourceRequest) Sanitize() error {
	return nil
}
