/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type WdiGenericRequest struct {
	InterfaceId  int32   `bson:"interfaceId"`
	RegisterId   int32   `bson:"registerId"`
	Size         int32   `bson:"size"`
	Count        int32   `bson:"count"`
	Offset       int32   `bson:"offset"`
	RegisterBank string  `bson:"registerBank"`
	Data         []int32 `bson:"data"`
}

func (c *WdiGenericRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *WdiGenericRequest) GetRegisterId() int32 {
	return c.RegisterId
}

func (c *WdiGenericRequest) GetSize() int32 {
	return c.Size
}

func (c *WdiGenericRequest) GetCount() int32 {
	return c.Count
}

func (c *WdiGenericRequest) GetOffset() int32 {
	return c.Offset
}

func (c *WdiGenericRequest) GetRegisterBank() string {
	return c.RegisterBank
}

func (c *WdiGenericRequest) GetData() []int32 {
	return c.Data
}

func (c *WdiGenericRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *WdiGenericRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *WdiGenericRequest) Clone() (*WdiGenericRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &WdiGenericRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *WdiGenericRequest) Sanitize() error {
	if c.Data == nil {
		c.Data = make([]int32, 0)
	}

	return nil
}
