/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The public properties of one channel of recorded oscilloscope data.
type AsciiOscilloscopeCaptureProperties struct {
	// Indicates whether the data came from a setting or an I/O pin.
	DataSource AsciiOscilloscopeDataSource `bson:"dataSource"`
	// The name of the recorded setting.
	Setting string `bson:"setting"`
	// The number of the axis the data was recorded from, or 0 for the controller.
	AxisNumber int32 `bson:"axisNumber"`
	// Which kind of I/O port data was recorded from.
	IoType AsciiIoPortType `bson:"ioType"`
	// Which I/O pin within the port was recorded.
	IoChannel int32 `bson:"ioChannel"`
}

// Indicates whether the data came from a setting or an I/O pin.
func (c *AsciiOscilloscopeCaptureProperties) GetDataSource() AsciiOscilloscopeDataSource {
	return c.DataSource
}

// The name of the recorded setting.
func (c *AsciiOscilloscopeCaptureProperties) GetSetting() string {
	return c.Setting
}

// The number of the axis the data was recorded from, or 0 for the controller.
func (c *AsciiOscilloscopeCaptureProperties) GetAxisNumber() int32 {
	return c.AxisNumber
}

// Which kind of I/O port data was recorded from.
func (c *AsciiOscilloscopeCaptureProperties) GetIoType() AsciiIoPortType {
	return c.IoType
}

// Which I/O pin within the port was recorded.
func (c *AsciiOscilloscopeCaptureProperties) GetIoChannel() int32 {
	return c.IoChannel
}

func (c *AsciiOscilloscopeCaptureProperties) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiOscilloscopeCaptureProperties) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiOscilloscopeCaptureProperties) Clone() (*AsciiOscilloscopeCaptureProperties, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiOscilloscopeCaptureProperties{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiOscilloscopeCaptureProperties) Sanitize() error {
	return nil
}
