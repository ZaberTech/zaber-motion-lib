/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type PvtPointRequest struct {
	InterfaceId int32             `bson:"interfaceId"`
	Device      int32             `bson:"device"`
	StreamId    int32             `bson:"streamId"`
	Pvt         bool              `bson:"pvt"`
	Type        StreamSegmentType `bson:"type"`
	Positions   []*Measurement    `bson:"positions"`
	Velocities  []*Measurement    `bson:"velocities"`
	Time        *Measurement      `bson:"time"`
}

func (c *PvtPointRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *PvtPointRequest) GetDevice() int32 {
	return c.Device
}

func (c *PvtPointRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *PvtPointRequest) GetPvt() bool {
	return c.Pvt
}

func (c *PvtPointRequest) GetType() StreamSegmentType {
	return c.Type
}

func (c *PvtPointRequest) GetPositions() []*Measurement {
	return c.Positions
}

func (c *PvtPointRequest) GetVelocities() []*Measurement {
	return c.Velocities
}

func (c *PvtPointRequest) GetTime() *Measurement {
	return c.Time
}

func (c *PvtPointRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *PvtPointRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *PvtPointRequest) Clone() (*PvtPointRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &PvtPointRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *PvtPointRequest) Sanitize() error {
	if c.Positions == nil {
		c.Positions = make([]*Measurement, 0)
	}

	for _, item := range c.Positions {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Positions of PvtPointRequest is nil")
		}
	}

	if c.Velocities == nil {
		c.Velocities = make([]*Measurement, 0)
	}

	for _, item := range c.Velocities {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		}
	}

	if c.Time != nil {
		if err := c.Time.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Time of PvtPointRequest is nil")
	}

	return nil
}
