/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamGenericCommandRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	StreamId    int32  `bson:"streamId"`
	Pvt         bool   `bson:"pvt"`
	Command     string `bson:"command"`
}

func (c *StreamGenericCommandRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamGenericCommandRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamGenericCommandRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamGenericCommandRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamGenericCommandRequest) GetCommand() string {
	return c.Command
}

func (c *StreamGenericCommandRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamGenericCommandRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamGenericCommandRequest) Clone() (*StreamGenericCommandRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamGenericCommandRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamGenericCommandRequest) Sanitize() error {
	return nil
}
