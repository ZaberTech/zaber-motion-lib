/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Representation of data gathered during axis identification.
type AsciiAxisIdentity struct {
	// Unique ID of the peripheral hardware.
	PeripheralId int32 `bson:"peripheralId"`
	// Name of the peripheral.
	PeripheralName string `bson:"peripheralName"`
	// Serial number of the peripheral, or 0 when not applicable.
	PeripheralSerialNumber uint32 `bson:"peripheralSerialNumber"`
	// Indicates whether the axis is a peripheral or part of an integrated device.
	IsPeripheral bool `bson:"isPeripheral"`
	// Determines the type of an axis and units it accepts.
	AxisType AsciiAxisType `bson:"axisType"`
	// The peripheral has hardware modifications.
	IsModified bool `bson:"isModified"`
}

// Unique ID of the peripheral hardware.
func (c *AsciiAxisIdentity) GetPeripheralId() int32 {
	return c.PeripheralId
}

// Name of the peripheral.
func (c *AsciiAxisIdentity) GetPeripheralName() string {
	return c.PeripheralName
}

// Serial number of the peripheral, or 0 when not applicable.
func (c *AsciiAxisIdentity) GetPeripheralSerialNumber() uint32 {
	return c.PeripheralSerialNumber
}

// Indicates whether the axis is a peripheral or part of an integrated device.
func (c *AsciiAxisIdentity) GetIsPeripheral() bool {
	return c.IsPeripheral
}

// Determines the type of an axis and units it accepts.
func (c *AsciiAxisIdentity) GetAxisType() AsciiAxisType {
	return c.AxisType
}

// The peripheral has hardware modifications.
func (c *AsciiAxisIdentity) GetIsModified() bool {
	return c.IsModified
}

func (c *AsciiAxisIdentity) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiAxisIdentity) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiAxisIdentity) Clone() (*AsciiAxisIdentity, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiAxisIdentity{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiAxisIdentity) Sanitize() error {
	return nil
}
