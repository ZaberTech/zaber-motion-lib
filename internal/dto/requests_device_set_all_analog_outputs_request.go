/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAllAnalogOutputsRequest struct {
	InterfaceId int32     `bson:"interfaceId"`
	Device      int32     `bson:"device"`
	Values      []float64 `bson:"values"`
}

func (c *DeviceSetAllAnalogOutputsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAllAnalogOutputsRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAllAnalogOutputsRequest) GetValues() []float64 {
	return c.Values
}

func (c *DeviceSetAllAnalogOutputsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAllAnalogOutputsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAllAnalogOutputsRequest) Clone() (*DeviceSetAllAnalogOutputsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAllAnalogOutputsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAllAnalogOutputsRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]float64, 0)
	}

	return nil
}
