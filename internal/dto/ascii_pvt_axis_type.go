/* This file is generated. Do not modify by hand. */
package dto

// Denotes type of the PVT sequence axis.
type AsciiPvtAxisType int32

const (
	AsciiPvtAxisType_PHYSICAL AsciiPvtAxisType = 0
	AsciiPvtAxisType_LOCKSTEP AsciiPvtAxisType = 1
)

var AsciiPvtAxisType_name = map[AsciiPvtAxisType]string{
	AsciiPvtAxisType_PHYSICAL: "Physical",
	AsciiPvtAxisType_LOCKSTEP: "Lockstep",
}
