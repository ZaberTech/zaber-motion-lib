/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// An object containing any setup issues that will prevent setting a state to a given device.
type AsciiCanSetStateDeviceResponse struct {
	// The error blocking applying this state to the given device.
	Error *string `bson:"error"`
	// A list of errors that block setting state of device's axes.
	AxisErrors []*AsciiCanSetStateAxisResponse `bson:"axisErrors"`
}

// The error blocking applying this state to the given device.
func (c *AsciiCanSetStateDeviceResponse) GetError() *string {
	return c.Error
}

// A list of errors that block setting state of device's axes.
func (c *AsciiCanSetStateDeviceResponse) GetAxisErrors() []*AsciiCanSetStateAxisResponse {
	return c.AxisErrors
}

func (c *AsciiCanSetStateDeviceResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiCanSetStateDeviceResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiCanSetStateDeviceResponse) Clone() (*AsciiCanSetStateDeviceResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiCanSetStateDeviceResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiCanSetStateDeviceResponse) Sanitize() error {
	if c.AxisErrors == nil {
		c.AxisErrors = make([]*AsciiCanSetStateAxisResponse, 0)
	}

	for _, item := range c.AxisErrors {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property AxisErrors of CanSetStateDeviceResponse is nil")
		}
	}

	return nil
}
