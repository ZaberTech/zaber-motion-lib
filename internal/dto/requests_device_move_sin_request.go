/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceMoveSinRequest struct {
	InterfaceId    int32   `bson:"interfaceId"`
	Device         int32   `bson:"device"`
	Axis           int32   `bson:"axis"`
	Amplitude      float64 `bson:"amplitude"`
	AmplitudeUnits string  `bson:"amplitudeUnits"`
	Period         float64 `bson:"period"`
	PeriodUnits    string  `bson:"periodUnits"`
	Count          float64 `bson:"count"`
	WaitUntilIdle  bool    `bson:"waitUntilIdle"`
}

func (c *DeviceMoveSinRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceMoveSinRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceMoveSinRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceMoveSinRequest) GetAmplitude() float64 {
	return c.Amplitude
}

func (c *DeviceMoveSinRequest) GetAmplitudeUnits() string {
	return c.AmplitudeUnits
}

func (c *DeviceMoveSinRequest) GetPeriod() float64 {
	return c.Period
}

func (c *DeviceMoveSinRequest) GetPeriodUnits() string {
	return c.PeriodUnits
}

func (c *DeviceMoveSinRequest) GetCount() float64 {
	return c.Count
}

func (c *DeviceMoveSinRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *DeviceMoveSinRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceMoveSinRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceMoveSinRequest) Clone() (*DeviceMoveSinRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceMoveSinRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceMoveSinRequest) Sanitize() error {
	return nil
}
