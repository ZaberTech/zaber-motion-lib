/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for StreamExecutionException.
type ExceptionsStreamExecutionExceptionData struct {
	// The error flag that caused the exception.
	ErrorFlag string `bson:"errorFlag"`
	// The reason for the exception.
	Reason string `bson:"reason"`
}

// The error flag that caused the exception.
func (c *ExceptionsStreamExecutionExceptionData) GetErrorFlag() string {
	return c.ErrorFlag
}

// The reason for the exception.
func (c *ExceptionsStreamExecutionExceptionData) GetReason() string {
	return c.Reason
}

func (c *ExceptionsStreamExecutionExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsStreamExecutionExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsStreamExecutionExceptionData) Clone() (*ExceptionsStreamExecutionExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsStreamExecutionExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsStreamExecutionExceptionData) Sanitize() error {
	return nil
}
