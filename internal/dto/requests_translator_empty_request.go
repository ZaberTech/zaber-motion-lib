/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorEmptyRequest struct {
	TranslatorId int32 `bson:"translatorId"`
}

func (c *TranslatorEmptyRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorEmptyRequest) Clone() (*TranslatorEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorEmptyRequest) Sanitize() error {
	return nil
}
