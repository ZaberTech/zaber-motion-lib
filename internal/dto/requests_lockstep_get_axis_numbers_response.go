/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepGetAxisNumbersResponse struct {
	Axes []int32 `bson:"axes"`
}

func (c *LockstepGetAxisNumbersResponse) GetAxes() []int32 {
	return c.Axes
}

func (c *LockstepGetAxisNumbersResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepGetAxisNumbersResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepGetAxisNumbersResponse) Clone() (*LockstepGetAxisNumbersResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepGetAxisNumbersResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepGetAxisNumbersResponse) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	return nil
}
