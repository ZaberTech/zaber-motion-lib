/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StringArrayResponse struct {
	Values []string `bson:"values"`
}

func (c *StringArrayResponse) GetValues() []string {
	return c.Values
}

func (c *StringArrayResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StringArrayResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StringArrayResponse) Clone() (*StringArrayResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StringArrayResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StringArrayResponse) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]string, 0)
	}

	return nil
}
