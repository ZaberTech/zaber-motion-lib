/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamCancelOutputScheduleRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	Device        int32 `bson:"device"`
	StreamId      int32 `bson:"streamId"`
	Pvt           bool  `bson:"pvt"`
	Analog        bool  `bson:"analog"`
	ChannelNumber int32 `bson:"channelNumber"`
}

func (c *StreamCancelOutputScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamCancelOutputScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamCancelOutputScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamCancelOutputScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamCancelOutputScheduleRequest) GetAnalog() bool {
	return c.Analog
}

func (c *StreamCancelOutputScheduleRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamCancelOutputScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamCancelOutputScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamCancelOutputScheduleRequest) Clone() (*StreamCancelOutputScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamCancelOutputScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamCancelOutputScheduleRequest) Sanitize() error {
	return nil
}
