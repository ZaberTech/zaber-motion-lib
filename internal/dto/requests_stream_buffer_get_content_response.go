/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamBufferGetContentResponse struct {
	BufferLines []string `bson:"bufferLines"`
}

func (c *StreamBufferGetContentResponse) GetBufferLines() []string {
	return c.BufferLines
}

func (c *StreamBufferGetContentResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamBufferGetContentResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamBufferGetContentResponse) Clone() (*StreamBufferGetContentResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamBufferGetContentResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamBufferGetContentResponse) Sanitize() error {
	if c.BufferLines == nil {
		c.BufferLines = make([]string, 0)
	}

	return nil
}
