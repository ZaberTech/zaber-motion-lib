/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeDataIdentifier struct {
	DataId int32 `bson:"dataId"`
}

func (c *OscilloscopeDataIdentifier) GetDataId() int32 {
	return c.DataId
}

func (c *OscilloscopeDataIdentifier) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeDataIdentifier) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeDataIdentifier) Clone() (*OscilloscopeDataIdentifier, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeDataIdentifier{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeDataIdentifier) Sanitize() error {
	return nil
}
