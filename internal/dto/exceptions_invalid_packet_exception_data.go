/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for the InvalidPacketException.
type ExceptionsInvalidPacketExceptionData struct {
	// The invalid packet that caused the exception.
	Packet string `bson:"packet"`
	// The reason for the exception.
	Reason string `bson:"reason"`
}

// The invalid packet that caused the exception.
func (c *ExceptionsInvalidPacketExceptionData) GetPacket() string {
	return c.Packet
}

// The reason for the exception.
func (c *ExceptionsInvalidPacketExceptionData) GetReason() string {
	return c.Reason
}

func (c *ExceptionsInvalidPacketExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsInvalidPacketExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsInvalidPacketExceptionData) Clone() (*ExceptionsInvalidPacketExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsInvalidPacketExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsInvalidPacketExceptionData) Sanitize() error {
	return nil
}
