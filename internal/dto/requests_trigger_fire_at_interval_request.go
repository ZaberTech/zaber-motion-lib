/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerFireAtIntervalRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	TriggerNumber int32   `bson:"triggerNumber"`
	Interval      float64 `bson:"interval"`
	Unit          string  `bson:"unit"`
}

func (c *TriggerFireAtIntervalRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerFireAtIntervalRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerFireAtIntervalRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerFireAtIntervalRequest) GetInterval() float64 {
	return c.Interval
}

func (c *TriggerFireAtIntervalRequest) GetUnit() string {
	return c.Unit
}

func (c *TriggerFireAtIntervalRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerFireAtIntervalRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerFireAtIntervalRequest) Clone() (*TriggerFireAtIntervalRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerFireAtIntervalRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerFireAtIntervalRequest) Sanitize() error {
	return nil
}
