/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AxesMoveRequest struct {
	Interfaces []int32        `bson:"interfaces"`
	Devices    []int32        `bson:"devices"`
	Axes       []int32        `bson:"axes"`
	Position   []*Measurement `bson:"position"`
}

func (c *AxesMoveRequest) GetInterfaces() []int32 {
	return c.Interfaces
}

func (c *AxesMoveRequest) GetDevices() []int32 {
	return c.Devices
}

func (c *AxesMoveRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *AxesMoveRequest) GetPosition() []*Measurement {
	return c.Position
}

func (c *AxesMoveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxesMoveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxesMoveRequest) Clone() (*AxesMoveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxesMoveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxesMoveRequest) Sanitize() error {
	if c.Interfaces == nil {
		c.Interfaces = make([]int32, 0)
	}

	if c.Devices == nil {
		c.Devices = make([]int32, 0)
	}

	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	if c.Position == nil {
		c.Position = make([]*Measurement, 0)
	}

	for _, item := range c.Position {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Position of AxesMoveRequest is nil")
		}
	}

	return nil
}
