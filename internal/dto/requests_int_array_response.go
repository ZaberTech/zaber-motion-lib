/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type IntArrayResponse struct {
	Values []int32 `bson:"values"`
}

func (c *IntArrayResponse) GetValues() []int32 {
	return c.Values
}

func (c *IntArrayResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *IntArrayResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *IntArrayResponse) Clone() (*IntArrayResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &IntArrayResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *IntArrayResponse) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]int32, 0)
	}

	return nil
}
