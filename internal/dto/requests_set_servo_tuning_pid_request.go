/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetServoTuningPIDRequest struct {
	InterfaceId int32                    `bson:"interfaceId"`
	Device      int32                    `bson:"device"`
	Axis        int32                    `bson:"axis"`
	Paramset    AsciiServoTuningParamset `bson:"paramset"`
	P           float64                  `bson:"p"`
	I           float64                  `bson:"i"`
	D           float64                  `bson:"d"`
	Fc          float64                  `bson:"fc"`
}

func (c *SetServoTuningPIDRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *SetServoTuningPIDRequest) GetDevice() int32 {
	return c.Device
}

func (c *SetServoTuningPIDRequest) GetAxis() int32 {
	return c.Axis
}

func (c *SetServoTuningPIDRequest) GetParamset() AsciiServoTuningParamset {
	return c.Paramset
}

func (c *SetServoTuningPIDRequest) GetP() float64 {
	return c.P
}

func (c *SetServoTuningPIDRequest) GetI() float64 {
	return c.I
}

func (c *SetServoTuningPIDRequest) GetD() float64 {
	return c.D
}

func (c *SetServoTuningPIDRequest) GetFc() float64 {
	return c.Fc
}

func (c *SetServoTuningPIDRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetServoTuningPIDRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetServoTuningPIDRequest) Clone() (*SetServoTuningPIDRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetServoTuningPIDRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetServoTuningPIDRequest) Sanitize() error {
	return nil
}
