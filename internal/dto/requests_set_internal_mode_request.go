/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type SetInternalModeRequest struct {
	Mode bool `bson:"mode"`
}

func (c *SetInternalModeRequest) GetMode() bool {
	return c.Mode
}

func (c *SetInternalModeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *SetInternalModeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *SetInternalModeRequest) Clone() (*SetInternalModeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &SetInternalModeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *SetInternalModeRequest) Sanitize() error {
	return nil
}
