/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Alert message received from the device.
type AsciiAlertEvent struct {
	// Number of the device that sent the message.
	DeviceAddress int32 `bson:"deviceAddress"`
	// Number of the axis which the response applies to. Zero denotes device scope.
	AxisNumber int32 `bson:"axisNumber"`
	// The device status contains BUSY when the axis is moving and IDLE otherwise.
	Status string `bson:"status"`
	// The warning flag contains the highest priority warning currently active for the device or axis.
	WarningFlag string `bson:"warningFlag"`
	// Response data which varies depending on the request.
	Data string `bson:"data"`
}

// Number of the device that sent the message.
func (c *AsciiAlertEvent) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

// Number of the axis which the response applies to. Zero denotes device scope.
func (c *AsciiAlertEvent) GetAxisNumber() int32 {
	return c.AxisNumber
}

// The device status contains BUSY when the axis is moving and IDLE otherwise.
func (c *AsciiAlertEvent) GetStatus() string {
	return c.Status
}

// The warning flag contains the highest priority warning currently active for the device or axis.
func (c *AsciiAlertEvent) GetWarningFlag() string {
	return c.WarningFlag
}

// Response data which varies depending on the request.
func (c *AsciiAlertEvent) GetData() string {
	return c.Data
}

func (c *AsciiAlertEvent) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiAlertEvent) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiAlertEvent) Clone() (*AsciiAlertEvent, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiAlertEvent{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiAlertEvent) Sanitize() error {
	return nil
}
