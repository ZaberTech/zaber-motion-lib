/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetupLiveRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	StreamId    int32   `bson:"streamId"`
	Pvt         bool    `bson:"pvt"`
	Axes        []int32 `bson:"axes"`
}

func (c *StreamSetupLiveRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetupLiveRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetupLiveRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetupLiveRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetupLiveRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *StreamSetupLiveRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetupLiveRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetupLiveRequest) Clone() (*StreamSetupLiveRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetupLiveRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetupLiveRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	return nil
}
