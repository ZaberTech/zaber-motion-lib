/* This file is generated. Do not modify by hand. */
package dto

// Mode of logging output of the library.
type LogOutputMode int32

const (
	LogOutputMode_OFF    LogOutputMode = 0
	LogOutputMode_STDOUT LogOutputMode = 1
	LogOutputMode_STDERR LogOutputMode = 2
	LogOutputMode_FILE   LogOutputMode = 3
)

var LogOutputMode_name = map[LogOutputMode]string{
	LogOutputMode_OFF:    "Off",
	LogOutputMode_STDOUT: "Stdout",
	LogOutputMode_STDERR: "Stderr",
	LogOutputMode_FILE:   "File",
}
