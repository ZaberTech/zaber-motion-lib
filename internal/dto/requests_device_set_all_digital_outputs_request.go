/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAllDigitalOutputsRequest struct {
	InterfaceId int32                      `bson:"interfaceId"`
	Device      int32                      `bson:"device"`
	Values      []AsciiDigitalOutputAction `bson:"values"`
}

func (c *DeviceSetAllDigitalOutputsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAllDigitalOutputsRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAllDigitalOutputsRequest) GetValues() []AsciiDigitalOutputAction {
	return c.Values
}

func (c *DeviceSetAllDigitalOutputsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAllDigitalOutputsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAllDigitalOutputsRequest) Clone() (*DeviceSetAllDigitalOutputsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAllDigitalOutputsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAllDigitalOutputsRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]AsciiDigitalOutputAction, 0)
	}

	return nil
}
