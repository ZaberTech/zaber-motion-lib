/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamModeResponse struct {
	StreamMode AsciiStreamMode `bson:"streamMode"`
	PvtMode    AsciiPvtMode    `bson:"pvtMode"`
}

func (c *StreamModeResponse) GetStreamMode() AsciiStreamMode {
	return c.StreamMode
}

func (c *StreamModeResponse) GetPvtMode() AsciiPvtMode {
	return c.PvtMode
}

func (c *StreamModeResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamModeResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamModeResponse) Clone() (*StreamModeResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamModeResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamModeResponse) Sanitize() error {
	return nil
}
