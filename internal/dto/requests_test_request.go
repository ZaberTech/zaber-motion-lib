/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TestRequest struct {
	ReturnError         bool   `bson:"returnError"`
	DataPing            string `bson:"dataPing"`
	ReturnErrorWithData bool   `bson:"returnErrorWithData"`
}

func (c *TestRequest) GetReturnError() bool {
	return c.ReturnError
}

func (c *TestRequest) GetDataPing() string {
	return c.DataPing
}

func (c *TestRequest) GetReturnErrorWithData() bool {
	return c.ReturnErrorWithData
}

func (c *TestRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TestRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TestRequest) Clone() (*TestRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TestRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TestRequest) Sanitize() error {
	return nil
}
