/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerEnabledStates struct {
	States []*AsciiTriggerEnabledState `bson:"states"`
}

func (c *TriggerEnabledStates) GetStates() []*AsciiTriggerEnabledState {
	return c.States
}

func (c *TriggerEnabledStates) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerEnabledStates) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerEnabledStates) Clone() (*TriggerEnabledStates, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerEnabledStates{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerEnabledStates) Sanitize() error {
	if c.States == nil {
		c.States = make([]*AsciiTriggerEnabledState, 0)
	}

	for _, item := range c.States {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property States of TriggerEnabledStates is nil")
		}
	}

	return nil
}
