/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The axis numbers of a lockstep group.
type AsciiLockstepAxes struct {
	// The axis number used to set the first axis.
	Axis1 int32 `bson:"axis1"`
	// The axis number used to set the second axis.
	Axis2 int32 `bson:"axis2"`
	// The axis number used to set the third axis.
	Axis3 int32 `bson:"axis3"`
	// The axis number used to set the fourth axis.
	Axis4 int32 `bson:"axis4"`
}

// The axis number used to set the first axis.
func (c *AsciiLockstepAxes) GetAxis1() int32 {
	return c.Axis1
}

// The axis number used to set the second axis.
func (c *AsciiLockstepAxes) GetAxis2() int32 {
	return c.Axis2
}

// The axis number used to set the third axis.
func (c *AsciiLockstepAxes) GetAxis3() int32 {
	return c.Axis3
}

// The axis number used to set the fourth axis.
func (c *AsciiLockstepAxes) GetAxis4() int32 {
	return c.Axis4
}

func (c *AsciiLockstepAxes) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiLockstepAxes) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiLockstepAxes) Clone() (*AsciiLockstepAxes, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiLockstepAxes{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiLockstepAxes) Sanitize() error {
	return nil
}
