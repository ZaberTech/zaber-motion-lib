/* This file is generated. Do not modify by hand. */
package dto

// Operation for trigger action.
type AsciiTriggerOperation int32

const (
	AsciiTriggerOperation_SET_TO       AsciiTriggerOperation = 0
	AsciiTriggerOperation_INCREMENT_BY AsciiTriggerOperation = 1
	AsciiTriggerOperation_DECREMENT_BY AsciiTriggerOperation = 2
)

var AsciiTriggerOperation_name = map[AsciiTriggerOperation]string{
	AsciiTriggerOperation_SET_TO:       "SetTo",
	AsciiTriggerOperation_INCREMENT_BY: "IncrementBy",
	AsciiTriggerOperation_DECREMENT_BY: "DecrementBy",
}
