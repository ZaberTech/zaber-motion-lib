/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceGetSettingRequest struct {
	InterfaceId int32                `bson:"interfaceId"`
	Device      int32                `bson:"device"`
	Setting     BinaryBinarySettings `bson:"setting"`
	Unit        string               `bson:"unit"`
}

func (c *BinaryDeviceGetSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceGetSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *BinaryDeviceGetSettingRequest) GetSetting() BinaryBinarySettings {
	return c.Setting
}

func (c *BinaryDeviceGetSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *BinaryDeviceGetSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceGetSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceGetSettingRequest) Clone() (*BinaryDeviceGetSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceGetSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceGetSettingRequest) Sanitize() error {
	return nil
}
