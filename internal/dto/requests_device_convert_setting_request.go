/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceConvertSettingRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	Axis        int32   `bson:"axis"`
	Setting     string  `bson:"setting"`
	Unit        string  `bson:"unit"`
	Value       float64 `bson:"value"`
	FromNative  bool    `bson:"fromNative"`
}

func (c *DeviceConvertSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceConvertSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceConvertSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceConvertSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *DeviceConvertSettingRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceConvertSettingRequest) GetValue() float64 {
	return c.Value
}

func (c *DeviceConvertSettingRequest) GetFromNative() bool {
	return c.FromNative
}

func (c *DeviceConvertSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceConvertSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceConvertSettingRequest) Clone() (*DeviceConvertSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceConvertSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceConvertSettingRequest) Sanitize() error {
	return nil
}
