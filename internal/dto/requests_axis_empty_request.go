/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AxisEmptyRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Axis        int32 `bson:"axis"`
}

func (c *AxisEmptyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AxisEmptyRequest) GetDevice() int32 {
	return c.Device
}

func (c *AxisEmptyRequest) GetAxis() int32 {
	return c.Axis
}

func (c *AxisEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxisEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxisEmptyRequest) Clone() (*AxisEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxisEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxisEmptyRequest) Sanitize() error {
	return nil
}
