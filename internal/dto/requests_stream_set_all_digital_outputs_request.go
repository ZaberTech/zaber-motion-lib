/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAllDigitalOutputsRequest struct {
	InterfaceId int32                      `bson:"interfaceId"`
	Device      int32                      `bson:"device"`
	StreamId    int32                      `bson:"streamId"`
	Pvt         bool                       `bson:"pvt"`
	Values      []AsciiDigitalOutputAction `bson:"values"`
}

func (c *StreamSetAllDigitalOutputsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAllDigitalOutputsRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAllDigitalOutputsRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAllDigitalOutputsRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAllDigitalOutputsRequest) GetValues() []AsciiDigitalOutputAction {
	return c.Values
}

func (c *StreamSetAllDigitalOutputsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAllDigitalOutputsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAllDigitalOutputsRequest) Clone() (*StreamSetAllDigitalOutputsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAllDigitalOutputsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAllDigitalOutputsRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]AsciiDigitalOutputAction, 0)
	}

	return nil
}
