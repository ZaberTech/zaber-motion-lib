/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetUnitConversionsRequest struct {
	InterfaceId int32                    `bson:"interfaceId"`
	Device      int32                    `bson:"device"`
	Axis        int32                    `bson:"axis"`
	Key         string                   `bson:"key"`
	Conversions []*AsciiConversionFactor `bson:"conversions"`
}

func (c *DeviceSetUnitConversionsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetUnitConversionsRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetUnitConversionsRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceSetUnitConversionsRequest) GetKey() string {
	return c.Key
}

func (c *DeviceSetUnitConversionsRequest) GetConversions() []*AsciiConversionFactor {
	return c.Conversions
}

func (c *DeviceSetUnitConversionsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetUnitConversionsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetUnitConversionsRequest) Clone() (*DeviceSetUnitConversionsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetUnitConversionsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetUnitConversionsRequest) Sanitize() error {
	if c.Conversions == nil {
		c.Conversions = make([]*AsciiConversionFactor, 0)
	}

	for _, item := range c.Conversions {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Conversions of DeviceSetUnitConversionsRequest is nil")
		}
	}

	return nil
}
