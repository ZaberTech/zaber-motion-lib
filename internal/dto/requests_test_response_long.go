/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TestResponseLong struct {
	DataPong []string `bson:"dataPong"`
}

func (c *TestResponseLong) GetDataPong() []string {
	return c.DataPong
}

func (c *TestResponseLong) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TestResponseLong) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TestResponseLong) Clone() (*TestResponseLong, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TestResponseLong{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TestResponseLong) Sanitize() error {
	if c.DataPong == nil {
		c.DataPong = make([]string, 0)
	}

	return nil
}
