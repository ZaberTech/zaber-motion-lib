/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GenericCommandResponseCollection struct {
	Responses []*AsciiResponse `bson:"responses"`
}

func (c *GenericCommandResponseCollection) GetResponses() []*AsciiResponse {
	return c.Responses
}

func (c *GenericCommandResponseCollection) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GenericCommandResponseCollection) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GenericCommandResponseCollection) Clone() (*GenericCommandResponseCollection, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GenericCommandResponseCollection{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GenericCommandResponseCollection) Sanitize() error {
	if c.Responses == nil {
		c.Responses = make([]*AsciiResponse, 0)
	}

	for _, item := range c.Responses {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Responses of GenericCommandResponseCollection is nil")
		}
	}

	return nil
}
