/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetupStoreArbitraryAxesRequest struct {
	InterfaceId  int32 `bson:"interfaceId"`
	Device       int32 `bson:"device"`
	StreamId     int32 `bson:"streamId"`
	Pvt          bool  `bson:"pvt"`
	StreamBuffer int32 `bson:"streamBuffer"`
	PvtBuffer    int32 `bson:"pvtBuffer"`
	AxesCount    int32 `bson:"axesCount"`
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetStreamBuffer() int32 {
	return c.StreamBuffer
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetPvtBuffer() int32 {
	return c.PvtBuffer
}

func (c *StreamSetupStoreArbitraryAxesRequest) GetAxesCount() int32 {
	return c.AxesCount
}

func (c *StreamSetupStoreArbitraryAxesRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetupStoreArbitraryAxesRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetupStoreArbitraryAxesRequest) Clone() (*StreamSetupStoreArbitraryAxesRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetupStoreArbitraryAxesRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetupStoreArbitraryAxesRequest) Sanitize() error {
	return nil
}
