/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LoadParamset struct {
	InterfaceId  int32                    `bson:"interfaceId"`
	Device       int32                    `bson:"device"`
	Axis         int32                    `bson:"axis"`
	ToParamset   AsciiServoTuningParamset `bson:"toParamset"`
	FromParamset AsciiServoTuningParamset `bson:"fromParamset"`
}

func (c *LoadParamset) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LoadParamset) GetDevice() int32 {
	return c.Device
}

func (c *LoadParamset) GetAxis() int32 {
	return c.Axis
}

func (c *LoadParamset) GetToParamset() AsciiServoTuningParamset {
	return c.ToParamset
}

func (c *LoadParamset) GetFromParamset() AsciiServoTuningParamset {
	return c.FromParamset
}

func (c *LoadParamset) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LoadParamset) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LoadParamset) Clone() (*LoadParamset, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LoadParamset{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LoadParamset) Sanitize() error {
	return nil
}
