/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The masses and parameters last used by simple tuning.
type AsciiSimpleTuning struct {
	// Whether the tuning returned is currently in use by this paramset,
	// or if it has been overwritten by a later change.
	IsUsed bool `bson:"isUsed"`
	// The mass of the carriage in kg.
	CarriageMass *float64 `bson:"carriageMass"`
	// The mass of the load in kg, excluding the mass of the carriage.
	LoadMass float64 `bson:"loadMass"`
	// The parameters used by simple tuning.
	TuningParams []*AsciiServoTuningParam `bson:"tuningParams"`
}

// Whether the tuning returned is currently in use by this paramset,
// or if it has been overwritten by a later change.
func (c *AsciiSimpleTuning) GetIsUsed() bool {
	return c.IsUsed
}

// The mass of the carriage in kg.
func (c *AsciiSimpleTuning) GetCarriageMass() *float64 {
	return c.CarriageMass
}

// The mass of the load in kg, excluding the mass of the carriage.
func (c *AsciiSimpleTuning) GetLoadMass() float64 {
	return c.LoadMass
}

// The parameters used by simple tuning.
func (c *AsciiSimpleTuning) GetTuningParams() []*AsciiServoTuningParam {
	return c.TuningParams
}

func (c *AsciiSimpleTuning) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiSimpleTuning) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiSimpleTuning) Clone() (*AsciiSimpleTuning, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiSimpleTuning{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiSimpleTuning) Sanitize() error {
	if c.TuningParams == nil {
		c.TuningParams = make([]*AsciiServoTuningParam, 0)
	}

	for _, item := range c.TuningParams {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property TuningParams of SimpleTuning is nil")
		}
	}

	return nil
}
