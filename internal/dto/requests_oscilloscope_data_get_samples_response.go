/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeDataGetSamplesResponse struct {
	Data []float64 `bson:"data"`
}

func (c *OscilloscopeDataGetSamplesResponse) GetData() []float64 {
	return c.Data
}

func (c *OscilloscopeDataGetSamplesResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeDataGetSamplesResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeDataGetSamplesResponse) Clone() (*OscilloscopeDataGetSamplesResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeDataGetSamplesResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeDataGetSamplesResponse) Sanitize() error {
	if c.Data == nil {
		c.Data = make([]float64, 0)
	}

	return nil
}
