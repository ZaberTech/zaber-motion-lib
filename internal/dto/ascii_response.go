/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Response message from the device.
type AsciiResponse struct {
	// Number of the device that sent the message.
	DeviceAddress int32 `bson:"deviceAddress"`
	// Number of the axis which the response applies to. Zero denotes device scope.
	AxisNumber int32 `bson:"axisNumber"`
	// The reply flag indicates if the request was accepted (OK) or rejected (RJ).
	ReplyFlag string `bson:"replyFlag"`
	// The device status contains BUSY when the axis is moving and IDLE otherwise.
	Status string `bson:"status"`
	// The warning flag contains the highest priority warning currently active for the device or axis.
	WarningFlag string `bson:"warningFlag"`
	// Response data which varies depending on the request.
	Data string `bson:"data"`
	// Type of the reply received.
	MessageType AsciiMessageType `bson:"messageType"`
}

// Number of the device that sent the message.
func (c *AsciiResponse) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

// Number of the axis which the response applies to. Zero denotes device scope.
func (c *AsciiResponse) GetAxisNumber() int32 {
	return c.AxisNumber
}

// The reply flag indicates if the request was accepted (OK) or rejected (RJ).
func (c *AsciiResponse) GetReplyFlag() string {
	return c.ReplyFlag
}

// The device status contains BUSY when the axis is moving and IDLE otherwise.
func (c *AsciiResponse) GetStatus() string {
	return c.Status
}

// The warning flag contains the highest priority warning currently active for the device or axis.
func (c *AsciiResponse) GetWarningFlag() string {
	return c.WarningFlag
}

// Response data which varies depending on the request.
func (c *AsciiResponse) GetData() string {
	return c.Data
}

// Type of the reply received.
func (c *AsciiResponse) GetMessageType() AsciiMessageType {
	return c.MessageType
}

func (c *AsciiResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiResponse) Clone() (*AsciiResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiResponse) Sanitize() error {
	return nil
}
