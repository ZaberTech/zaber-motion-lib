/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamLineRequest struct {
	InterfaceId       int32             `bson:"interfaceId"`
	Device            int32             `bson:"device"`
	StreamId          int32             `bson:"streamId"`
	Pvt               bool              `bson:"pvt"`
	Type              StreamSegmentType `bson:"type"`
	Endpoint          []*Measurement    `bson:"endpoint"`
	TargetAxesIndices []int32           `bson:"targetAxesIndices"`
}

func (c *StreamLineRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamLineRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamLineRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamLineRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamLineRequest) GetType() StreamSegmentType {
	return c.Type
}

func (c *StreamLineRequest) GetEndpoint() []*Measurement {
	return c.Endpoint
}

func (c *StreamLineRequest) GetTargetAxesIndices() []int32 {
	return c.TargetAxesIndices
}

func (c *StreamLineRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamLineRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamLineRequest) Clone() (*StreamLineRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamLineRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamLineRequest) Sanitize() error {
	if c.Endpoint == nil {
		c.Endpoint = make([]*Measurement, 0)
	}

	for _, item := range c.Endpoint {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Endpoint of StreamLineRequest is nil")
		}
	}

	if c.TargetAxesIndices == nil {
		c.TargetAxesIndices = make([]int32, 0)
	}

	return nil
}
