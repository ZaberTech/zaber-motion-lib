/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// A parameter used to establish the servo tuning of an axis.
type AsciiServoTuningParam struct {
	// The name of the parameter to set.
	Name string `bson:"name"`
	// The value to use for this parameter.
	Value float64 `bson:"value"`
}

// The name of the parameter to set.
func (c *AsciiServoTuningParam) GetName() string {
	return c.Name
}

// The value to use for this parameter.
func (c *AsciiServoTuningParam) GetValue() float64 {
	return c.Value
}

func (c *AsciiServoTuningParam) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiServoTuningParam) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiServoTuningParam) Clone() (*AsciiServoTuningParam, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiServoTuningParam{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiServoTuningParam) Sanitize() error {
	return nil
}
