/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamEmptyRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	StreamId    int32 `bson:"streamId"`
	Pvt         bool  `bson:"pvt"`
}

func (c *StreamEmptyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamEmptyRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamEmptyRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamEmptyRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamEmptyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamEmptyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamEmptyRequest) Clone() (*StreamEmptyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamEmptyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamEmptyRequest) Sanitize() error {
	return nil
}
