/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepDisableRequest struct {
	InterfaceId     int32 `bson:"interfaceId"`
	Device          int32 `bson:"device"`
	LockstepGroupId int32 `bson:"lockstepGroupId"`
	WaitUntilIdle   bool  `bson:"waitUntilIdle"`
}

func (c *LockstepDisableRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepDisableRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepDisableRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepDisableRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *LockstepDisableRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepDisableRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepDisableRequest) Clone() (*LockstepDisableRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepDisableRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepDisableRequest) Sanitize() error {
	return nil
}
