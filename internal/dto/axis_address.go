/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Holds device address and axis number.
type AxisAddress struct {
	// Device address.
	Device int32 `bson:"device"`
	// Axis number.
	Axis int32 `bson:"axis"`
}

// Device address.
func (c *AxisAddress) GetDevice() int32 {
	return c.Device
}

// Axis number.
func (c *AxisAddress) GetAxis() int32 {
	return c.Axis
}

func (c *AxisAddress) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxisAddress) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxisAddress) Clone() (*AxisAddress, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxisAddress{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxisAddress) Sanitize() error {
	return nil
}
