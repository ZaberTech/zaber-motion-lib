/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceDetectRequest struct {
	InterfaceId     int32      `bson:"interfaceId"`
	IdentifyDevices bool       `bson:"identifyDevices"`
	Type            DeviceType `bson:"type"`
}

func (c *DeviceDetectRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceDetectRequest) GetIdentifyDevices() bool {
	return c.IdentifyDevices
}

func (c *DeviceDetectRequest) GetType() DeviceType {
	return c.Type
}

func (c *DeviceDetectRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceDetectRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceDetectRequest) Clone() (*DeviceDetectRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceDetectRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceDetectRequest) Sanitize() error {
	return nil
}
