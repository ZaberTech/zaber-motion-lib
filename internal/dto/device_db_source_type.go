/* This file is generated. Do not modify by hand. */
package dto

// Type of source of Device DB data.
type DeviceDbSourceType int32

const (
	DeviceDbSourceType_WEB_SERVICE DeviceDbSourceType = 0
	DeviceDbSourceType_FILE        DeviceDbSourceType = 1
)

var DeviceDbSourceType_name = map[DeviceDbSourceType]string{
	DeviceDbSourceType_WEB_SERVICE: "WebService",
	DeviceDbSourceType_FILE:        "File",
}
