/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAnalogOutputRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	ChannelNumber int32   `bson:"channelNumber"`
	Value         float64 `bson:"value"`
}

func (c *DeviceSetAnalogOutputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAnalogOutputRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAnalogOutputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *DeviceSetAnalogOutputRequest) GetValue() float64 {
	return c.Value
}

func (c *DeviceSetAnalogOutputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAnalogOutputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAnalogOutputRequest) Clone() (*DeviceSetAnalogOutputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAnalogOutputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAnalogOutputRequest) Sanitize() error {
	return nil
}
