/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceDetectResponse struct {
	Devices []int32 `bson:"devices"`
}

func (c *BinaryDeviceDetectResponse) GetDevices() []int32 {
	return c.Devices
}

func (c *BinaryDeviceDetectResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceDetectResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceDetectResponse) Clone() (*BinaryDeviceDetectResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceDetectResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceDetectResponse) Sanitize() error {
	if c.Devices == nil {
		c.Devices = make([]int32, 0)
	}

	return nil
}
