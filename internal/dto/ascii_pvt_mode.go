/* This file is generated. Do not modify by hand. */
package dto

// Mode of a PVT sequence.
type AsciiPvtMode int32

const (
	AsciiPvtMode_DISABLED AsciiPvtMode = 0
	AsciiPvtMode_STORE    AsciiPvtMode = 1
	AsciiPvtMode_LIVE     AsciiPvtMode = 2
)

var AsciiPvtMode_name = map[AsciiPvtMode]string{
	AsciiPvtMode_DISABLED: "Disabled",
	AsciiPvtMode_STORE:    "Store",
	AsciiPvtMode_LIVE:     "Live",
}
