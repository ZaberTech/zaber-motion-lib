/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceOnAllResponse struct {
	DeviceAddresses []int32 `bson:"deviceAddresses"`
}

func (c *DeviceOnAllResponse) GetDeviceAddresses() []int32 {
	return c.DeviceAddresses
}

func (c *DeviceOnAllResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceOnAllResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceOnAllResponse) Clone() (*DeviceOnAllResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceOnAllResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceOnAllResponse) Sanitize() error {
	if c.DeviceAddresses == nil {
		c.DeviceAddresses = make([]int32, 0)
	}

	return nil
}
