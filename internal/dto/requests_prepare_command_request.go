/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type PrepareCommandRequest struct {
	InterfaceId     int32          `bson:"interfaceId"`
	Device          int32          `bson:"device"`
	Axis            int32          `bson:"axis"`
	CommandTemplate string         `bson:"commandTemplate"`
	Parameters      []*Measurement `bson:"parameters"`
}

func (c *PrepareCommandRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *PrepareCommandRequest) GetDevice() int32 {
	return c.Device
}

func (c *PrepareCommandRequest) GetAxis() int32 {
	return c.Axis
}

func (c *PrepareCommandRequest) GetCommandTemplate() string {
	return c.CommandTemplate
}

func (c *PrepareCommandRequest) GetParameters() []*Measurement {
	return c.Parameters
}

func (c *PrepareCommandRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *PrepareCommandRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *PrepareCommandRequest) Clone() (*PrepareCommandRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &PrepareCommandRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *PrepareCommandRequest) Sanitize() error {
	if c.Parameters == nil {
		c.Parameters = make([]*Measurement, 0)
	}

	for _, item := range c.Parameters {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Parameters of PrepareCommandRequest is nil")
		}
	}

	return nil
}
