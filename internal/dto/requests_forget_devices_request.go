/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ForgetDevicesRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	ExceptDevices []int32 `bson:"exceptDevices"`
}

func (c *ForgetDevicesRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *ForgetDevicesRequest) GetExceptDevices() []int32 {
	return c.ExceptDevices
}

func (c *ForgetDevicesRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ForgetDevicesRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ForgetDevicesRequest) Clone() (*ForgetDevicesRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ForgetDevicesRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ForgetDevicesRequest) Sanitize() error {
	if c.ExceptDevices == nil {
		c.ExceptDevices = make([]int32, 0)
	}

	return nil
}
