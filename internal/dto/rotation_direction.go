/* This file is generated. Do not modify by hand. */
package dto

// Direction of rotation.
type RotationDirection int32

const (
	RotationDirection_CLOCKWISE        RotationDirection = 0
	RotationDirection_COUNTERCLOCKWISE RotationDirection = 1
	RotationDirection_CW               RotationDirection = 0
	RotationDirection_CCW              RotationDirection = 1
)

var RotationDirection_name = map[RotationDirection]string{
	RotationDirection_CLOCKWISE:        "Clockwise",
	RotationDirection_COUNTERCLOCKWISE: "Counterclockwise",
}
