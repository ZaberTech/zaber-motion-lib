/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Class representing information on the I/O channels of the device.
type AsciiDeviceIOInfo struct {
	// Number of analog output channels.
	NumberAnalogOutputs int32 `bson:"numberAnalogOutputs"`
	// Number of analog input channels.
	NumberAnalogInputs int32 `bson:"numberAnalogInputs"`
	// Number of digital output channels.
	NumberDigitalOutputs int32 `bson:"numberDigitalOutputs"`
	// Number of digital input channels.
	NumberDigitalInputs int32 `bson:"numberDigitalInputs"`
}

// Number of analog output channels.
func (c *AsciiDeviceIOInfo) GetNumberAnalogOutputs() int32 {
	return c.NumberAnalogOutputs
}

// Number of analog input channels.
func (c *AsciiDeviceIOInfo) GetNumberAnalogInputs() int32 {
	return c.NumberAnalogInputs
}

// Number of digital output channels.
func (c *AsciiDeviceIOInfo) GetNumberDigitalOutputs() int32 {
	return c.NumberDigitalOutputs
}

// Number of digital input channels.
func (c *AsciiDeviceIOInfo) GetNumberDigitalInputs() int32 {
	return c.NumberDigitalInputs
}

func (c *AsciiDeviceIOInfo) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiDeviceIOInfo) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiDeviceIOInfo) Clone() (*AsciiDeviceIOInfo, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiDeviceIOInfo{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiDeviceIOInfo) Sanitize() error {
	return nil
}
