/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Represents a result of a G-code block translation.
type GcodeTranslateResult struct {
	// Stream commands resulting from the block.
	Commands []string `bson:"commands"`
	// Messages informing about unsupported codes and features.
	Warnings []*GcodeTranslateMessage `bson:"warnings"`
}

// Stream commands resulting from the block.
func (c *GcodeTranslateResult) GetCommands() []string {
	return c.Commands
}

// Messages informing about unsupported codes and features.
func (c *GcodeTranslateResult) GetWarnings() []*GcodeTranslateMessage {
	return c.Warnings
}

func (c *GcodeTranslateResult) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeTranslateResult) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeTranslateResult) Clone() (*GcodeTranslateResult, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeTranslateResult{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeTranslateResult) Sanitize() error {
	if c.Commands == nil {
		c.Commands = make([]string, 0)
	}

	if c.Warnings == nil {
		c.Warnings = make([]*GcodeTranslateMessage, 0)
	}

	for _, item := range c.Warnings {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Warnings of TranslateResult is nil")
		}
	}

	return nil
}
