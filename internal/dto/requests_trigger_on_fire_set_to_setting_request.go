/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerOnFireSetToSettingRequest struct {
	InterfaceId   int32                 `bson:"interfaceId"`
	Device        int32                 `bson:"device"`
	TriggerNumber int32                 `bson:"triggerNumber"`
	Action        AsciiTriggerAction    `bson:"action"`
	Axis          int32                 `bson:"axis"`
	Setting       string                `bson:"setting"`
	Operation     AsciiTriggerOperation `bson:"operation"`
	FromAxis      int32                 `bson:"fromAxis"`
	FromSetting   string                `bson:"fromSetting"`
}

func (c *TriggerOnFireSetToSettingRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerOnFireSetToSettingRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerOnFireSetToSettingRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerOnFireSetToSettingRequest) GetAction() AsciiTriggerAction {
	return c.Action
}

func (c *TriggerOnFireSetToSettingRequest) GetAxis() int32 {
	return c.Axis
}

func (c *TriggerOnFireSetToSettingRequest) GetSetting() string {
	return c.Setting
}

func (c *TriggerOnFireSetToSettingRequest) GetOperation() AsciiTriggerOperation {
	return c.Operation
}

func (c *TriggerOnFireSetToSettingRequest) GetFromAxis() int32 {
	return c.FromAxis
}

func (c *TriggerOnFireSetToSettingRequest) GetFromSetting() string {
	return c.FromSetting
}

func (c *TriggerOnFireSetToSettingRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerOnFireSetToSettingRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerOnFireSetToSettingRequest) Clone() (*TriggerOnFireSetToSettingRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerOnFireSetToSettingRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerOnFireSetToSettingRequest) Sanitize() error {
	return nil
}
