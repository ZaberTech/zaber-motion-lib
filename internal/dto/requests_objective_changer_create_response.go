/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type ObjectiveChangerCreateResponse struct {
	Turret       int32 `bson:"turret"`
	FocusAddress int32 `bson:"focusAddress"`
	FocusAxis    int32 `bson:"focusAxis"`
}

func (c *ObjectiveChangerCreateResponse) GetTurret() int32 {
	return c.Turret
}

func (c *ObjectiveChangerCreateResponse) GetFocusAddress() int32 {
	return c.FocusAddress
}

func (c *ObjectiveChangerCreateResponse) GetFocusAxis() int32 {
	return c.FocusAxis
}

func (c *ObjectiveChangerCreateResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ObjectiveChangerCreateResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ObjectiveChangerCreateResponse) Clone() (*ObjectiveChangerCreateResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ObjectiveChangerCreateResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ObjectiveChangerCreateResponse) Sanitize() error {
	return nil
}
