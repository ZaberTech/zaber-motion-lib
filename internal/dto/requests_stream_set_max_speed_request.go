/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetMaxSpeedRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	StreamId    int32   `bson:"streamId"`
	Pvt         bool    `bson:"pvt"`
	MaxSpeed    float64 `bson:"maxSpeed"`
	Unit        string  `bson:"unit"`
}

func (c *StreamSetMaxSpeedRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetMaxSpeedRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetMaxSpeedRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetMaxSpeedRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetMaxSpeedRequest) GetMaxSpeed() float64 {
	return c.MaxSpeed
}

func (c *StreamSetMaxSpeedRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetMaxSpeedRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetMaxSpeedRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetMaxSpeedRequest) Clone() (*StreamSetMaxSpeedRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetMaxSpeedRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetMaxSpeedRequest) Sanitize() error {
	return nil
}
