/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for MovementFailedException.
type ExceptionsMovementFailedExceptionData struct {
	// The full list of warnings.
	Warnings []string `bson:"warnings"`
	// The reason for the Exception.
	Reason string `bson:"reason"`
	// The address of the device that performed the failed movement.
	Device int32 `bson:"device"`
	// The number of the axis that performed the failed movement.
	Axis int32 `bson:"axis"`
}

// The full list of warnings.
func (c *ExceptionsMovementFailedExceptionData) GetWarnings() []string {
	return c.Warnings
}

// The reason for the Exception.
func (c *ExceptionsMovementFailedExceptionData) GetReason() string {
	return c.Reason
}

// The address of the device that performed the failed movement.
func (c *ExceptionsMovementFailedExceptionData) GetDevice() int32 {
	return c.Device
}

// The number of the axis that performed the failed movement.
func (c *ExceptionsMovementFailedExceptionData) GetAxis() int32 {
	return c.Axis
}

func (c *ExceptionsMovementFailedExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsMovementFailedExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsMovementFailedExceptionData) Clone() (*ExceptionsMovementFailedExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsMovementFailedExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsMovementFailedExceptionData) Sanitize() error {
	if c.Warnings == nil {
		c.Warnings = make([]string, 0)
	}

	return nil
}
