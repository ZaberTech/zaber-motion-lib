/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Reply that could not be matched to a request.
type AsciiUnknownResponseEvent struct {
	// Number of the device that sent the message.
	DeviceAddress int32 `bson:"deviceAddress"`
	// Number of the axis which the response applies to. Zero denotes device scope.
	AxisNumber int32 `bson:"axisNumber"`
	// The reply flag indicates if the request was accepted (OK) or rejected (RJ).
	ReplyFlag string `bson:"replyFlag"`
	// The device status contains BUSY when the axis is moving and IDLE otherwise.
	Status string `bson:"status"`
	// The warning flag contains the highest priority warning currently active for the device or axis.
	WarningFlag string `bson:"warningFlag"`
	// Response data which varies depending on the request.
	Data string `bson:"data"`
	// Type of the reply received.
	MessageType AsciiMessageType `bson:"messageType"`
}

// Number of the device that sent the message.
func (c *AsciiUnknownResponseEvent) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

// Number of the axis which the response applies to. Zero denotes device scope.
func (c *AsciiUnknownResponseEvent) GetAxisNumber() int32 {
	return c.AxisNumber
}

// The reply flag indicates if the request was accepted (OK) or rejected (RJ).
func (c *AsciiUnknownResponseEvent) GetReplyFlag() string {
	return c.ReplyFlag
}

// The device status contains BUSY when the axis is moving and IDLE otherwise.
func (c *AsciiUnknownResponseEvent) GetStatus() string {
	return c.Status
}

// The warning flag contains the highest priority warning currently active for the device or axis.
func (c *AsciiUnknownResponseEvent) GetWarningFlag() string {
	return c.WarningFlag
}

// Response data which varies depending on the request.
func (c *AsciiUnknownResponseEvent) GetData() string {
	return c.Data
}

// Type of the reply received.
func (c *AsciiUnknownResponseEvent) GetMessageType() AsciiMessageType {
	return c.MessageType
}

func (c *AsciiUnknownResponseEvent) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiUnknownResponseEvent) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiUnknownResponseEvent) Clone() (*AsciiUnknownResponseEvent, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiUnknownResponseEvent{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiUnknownResponseEvent) Sanitize() error {
	return nil
}
