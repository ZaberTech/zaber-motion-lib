/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAllDigitalOutputsScheduleRequest struct {
	InterfaceId  int32                      `bson:"interfaceId"`
	Device       int32                      `bson:"device"`
	StreamId     int32                      `bson:"streamId"`
	Pvt          bool                       `bson:"pvt"`
	Values       []AsciiDigitalOutputAction `bson:"values"`
	FutureValues []AsciiDigitalOutputAction `bson:"futureValues"`
	Delay        float64                    `bson:"delay"`
	Unit         string                     `bson:"unit"`
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetValues() []AsciiDigitalOutputAction {
	return c.Values
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetFutureValues() []AsciiDigitalOutputAction {
	return c.FutureValues
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAllDigitalOutputsScheduleRequest) Clone() (*StreamSetAllDigitalOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAllDigitalOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAllDigitalOutputsScheduleRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]AsciiDigitalOutputAction, 0)
	}

	if c.FutureValues == nil {
		c.FutureValues = make([]AsciiDigitalOutputAction, 0)
	}

	return nil
}
