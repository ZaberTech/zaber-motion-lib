/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepMoveSinRequest struct {
	InterfaceId     int32   `bson:"interfaceId"`
	Device          int32   `bson:"device"`
	LockstepGroupId int32   `bson:"lockstepGroupId"`
	Amplitude       float64 `bson:"amplitude"`
	AmplitudeUnits  string  `bson:"amplitudeUnits"`
	Period          float64 `bson:"period"`
	PeriodUnits     string  `bson:"periodUnits"`
	Count           float64 `bson:"count"`
	WaitUntilIdle   bool    `bson:"waitUntilIdle"`
}

func (c *LockstepMoveSinRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepMoveSinRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepMoveSinRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepMoveSinRequest) GetAmplitude() float64 {
	return c.Amplitude
}

func (c *LockstepMoveSinRequest) GetAmplitudeUnits() string {
	return c.AmplitudeUnits
}

func (c *LockstepMoveSinRequest) GetPeriod() float64 {
	return c.Period
}

func (c *LockstepMoveSinRequest) GetPeriodUnits() string {
	return c.PeriodUnits
}

func (c *LockstepMoveSinRequest) GetCount() float64 {
	return c.Count
}

func (c *LockstepMoveSinRequest) GetWaitUntilIdle() bool {
	return c.WaitUntilIdle
}

func (c *LockstepMoveSinRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepMoveSinRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepMoveSinRequest) Clone() (*LockstepMoveSinRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepMoveSinRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepMoveSinRequest) Sanitize() error {
	return nil
}
