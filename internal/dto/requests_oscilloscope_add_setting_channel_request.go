/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeAddSettingChannelRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Axis        int32  `bson:"axis"`
	Setting     string `bson:"setting"`
}

func (c *OscilloscopeAddSettingChannelRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *OscilloscopeAddSettingChannelRequest) GetDevice() int32 {
	return c.Device
}

func (c *OscilloscopeAddSettingChannelRequest) GetAxis() int32 {
	return c.Axis
}

func (c *OscilloscopeAddSettingChannelRequest) GetSetting() string {
	return c.Setting
}

func (c *OscilloscopeAddSettingChannelRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeAddSettingChannelRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeAddSettingChannelRequest) Clone() (*OscilloscopeAddSettingChannelRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeAddSettingChannelRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeAddSettingChannelRequest) Sanitize() error {
	return nil
}
