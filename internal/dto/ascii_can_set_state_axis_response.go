/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// An object containing any setup issues that will prevent setting a state to a given axis.
type AsciiCanSetStateAxisResponse struct {
	// The error blocking applying this state to the given axis.
	Error *string `bson:"error"`
	// The number of the axis that cannot be set.
	AxisNumber int32 `bson:"axisNumber"`
}

// The error blocking applying this state to the given axis.
func (c *AsciiCanSetStateAxisResponse) GetError() *string {
	return c.Error
}

// The number of the axis that cannot be set.
func (c *AsciiCanSetStateAxisResponse) GetAxisNumber() int32 {
	return c.AxisNumber
}

func (c *AsciiCanSetStateAxisResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiCanSetStateAxisResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiCanSetStateAxisResponse) Clone() (*AsciiCanSetStateAxisResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiCanSetStateAxisResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiCanSetStateAxisResponse) Sanitize() error {
	return nil
}
