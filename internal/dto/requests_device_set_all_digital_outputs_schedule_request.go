/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceSetAllDigitalOutputsScheduleRequest struct {
	InterfaceId  int32                      `bson:"interfaceId"`
	Device       int32                      `bson:"device"`
	Values       []AsciiDigitalOutputAction `bson:"values"`
	FutureValues []AsciiDigitalOutputAction `bson:"futureValues"`
	Delay        float64                    `bson:"delay"`
	Unit         string                     `bson:"unit"`
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetValues() []AsciiDigitalOutputAction {
	return c.Values
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetFutureValues() []AsciiDigitalOutputAction {
	return c.FutureValues
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetDelay() float64 {
	return c.Delay
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) GetUnit() string {
	return c.Unit
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceSetAllDigitalOutputsScheduleRequest) Clone() (*DeviceSetAllDigitalOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceSetAllDigitalOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceSetAllDigitalOutputsScheduleRequest) Sanitize() error {
	if c.Values == nil {
		c.Values = make([]AsciiDigitalOutputAction, 0)
	}

	if c.FutureValues == nil {
		c.FutureValues = make([]AsciiDigitalOutputAction, 0)
	}

	return nil
}
