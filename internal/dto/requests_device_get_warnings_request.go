/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceGetWarningsRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Axis        int32 `bson:"axis"`
	Clear       bool  `bson:"clear"`
}

func (c *DeviceGetWarningsRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceGetWarningsRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceGetWarningsRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceGetWarningsRequest) GetClear() bool {
	return c.Clear
}

func (c *DeviceGetWarningsRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceGetWarningsRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceGetWarningsRequest) Clone() (*DeviceGetWarningsRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceGetWarningsRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceGetWarningsRequest) Sanitize() error {
	return nil
}
