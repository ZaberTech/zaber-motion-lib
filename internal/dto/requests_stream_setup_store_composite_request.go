/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetupStoreCompositeRequest struct {
	InterfaceId  int32                        `bson:"interfaceId"`
	Device       int32                        `bson:"device"`
	StreamId     int32                        `bson:"streamId"`
	Pvt          bool                         `bson:"pvt"`
	StreamBuffer int32                        `bson:"streamBuffer"`
	PvtBuffer    int32                        `bson:"pvtBuffer"`
	Axes         []*AsciiStreamAxisDefinition `bson:"axes"`
	PvtAxes      []*AsciiPvtAxisDefinition    `bson:"pvtAxes"`
}

func (c *StreamSetupStoreCompositeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetupStoreCompositeRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetupStoreCompositeRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetupStoreCompositeRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetupStoreCompositeRequest) GetStreamBuffer() int32 {
	return c.StreamBuffer
}

func (c *StreamSetupStoreCompositeRequest) GetPvtBuffer() int32 {
	return c.PvtBuffer
}

func (c *StreamSetupStoreCompositeRequest) GetAxes() []*AsciiStreamAxisDefinition {
	return c.Axes
}

func (c *StreamSetupStoreCompositeRequest) GetPvtAxes() []*AsciiPvtAxisDefinition {
	return c.PvtAxes
}

func (c *StreamSetupStoreCompositeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetupStoreCompositeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetupStoreCompositeRequest) Clone() (*StreamSetupStoreCompositeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetupStoreCompositeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetupStoreCompositeRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]*AsciiStreamAxisDefinition, 0)
	}

	for _, item := range c.Axes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Axes of StreamSetupStoreCompositeRequest is nil")
		}
	}

	if c.PvtAxes == nil {
		c.PvtAxes = make([]*AsciiPvtAxisDefinition, 0)
	}

	for _, item := range c.PvtAxes {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property PvtAxes of StreamSetupStoreCompositeRequest is nil")
		}
	}

	return nil
}
