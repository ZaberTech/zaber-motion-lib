/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The source used by a process in a closed-loop mode.
type ProductProcessControllerSource struct {
	// The type of input sensor.
	Sensor ProductProcessControllerSourceSensor `bson:"sensor"`
	// The specific input to use.
	Port int32 `bson:"port"`
}

// The type of input sensor.
func (c *ProductProcessControllerSource) GetSensor() ProductProcessControllerSourceSensor {
	return c.Sensor
}

// The specific input to use.
func (c *ProductProcessControllerSource) GetPort() int32 {
	return c.Port
}

func (c *ProductProcessControllerSource) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ProductProcessControllerSource) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ProductProcessControllerSource) Clone() (*ProductProcessControllerSource, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ProductProcessControllerSource{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ProductProcessControllerSource) Sanitize() error {
	return nil
}
