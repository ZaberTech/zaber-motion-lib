/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type RenumberRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Address     int32 `bson:"address"`
}

func (c *RenumberRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *RenumberRequest) GetDevice() int32 {
	return c.Device
}

func (c *RenumberRequest) GetAddress() int32 {
	return c.Address
}

func (c *RenumberRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *RenumberRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *RenumberRequest) Clone() (*RenumberRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &RenumberRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *RenumberRequest) Sanitize() error {
	return nil
}
