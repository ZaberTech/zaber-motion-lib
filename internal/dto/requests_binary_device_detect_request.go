/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryDeviceDetectRequest struct {
	InterfaceId     int32 `bson:"interfaceId"`
	IdentifyDevices bool  `bson:"identifyDevices"`
}

func (c *BinaryDeviceDetectRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryDeviceDetectRequest) GetIdentifyDevices() bool {
	return c.IdentifyDevices
}

func (c *BinaryDeviceDetectRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryDeviceDetectRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryDeviceDetectRequest) Clone() (*BinaryDeviceDetectRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryDeviceDetectRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryDeviceDetectRequest) Sanitize() error {
	return nil
}
