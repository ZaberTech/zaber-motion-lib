/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GenericBinaryRequest struct {
	InterfaceId int32             `bson:"interfaceId"`
	Device      int32             `bson:"device"`
	Command     BinaryCommandCode `bson:"command"`
	Data        int32             `bson:"data"`
	CheckErrors bool              `bson:"checkErrors"`
	Timeout     float64           `bson:"timeout"`
}

func (c *GenericBinaryRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *GenericBinaryRequest) GetDevice() int32 {
	return c.Device
}

func (c *GenericBinaryRequest) GetCommand() BinaryCommandCode {
	return c.Command
}

func (c *GenericBinaryRequest) GetData() int32 {
	return c.Data
}

func (c *GenericBinaryRequest) GetCheckErrors() bool {
	return c.CheckErrors
}

func (c *GenericBinaryRequest) GetTimeout() float64 {
	return c.Timeout
}

func (c *GenericBinaryRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GenericBinaryRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GenericBinaryRequest) Clone() (*GenericBinaryRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GenericBinaryRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GenericBinaryRequest) Sanitize() error {
	return nil
}
