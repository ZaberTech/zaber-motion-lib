/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Specifies a setting to get with one of the multi-get commands.
type AsciiGetAxisSetting struct {
	// The setting to read.
	Setting string `bson:"setting"`
	// The unit to convert the read setting to.
	Unit string `bson:"unit"`
}

// The setting to read.
func (c *AsciiGetAxisSetting) GetSetting() string {
	return c.Setting
}

// The unit to convert the read setting to.
func (c *AsciiGetAxisSetting) GetUnit() string {
	return c.Unit
}

func (c *AsciiGetAxisSetting) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiGetAxisSetting) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiGetAxisSetting) Clone() (*AsciiGetAxisSetting, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiGetAxisSetting{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiGetAxisSetting) Sanitize() error {
	return nil
}
