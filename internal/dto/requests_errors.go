/* This file is generated. Do not modify by hand. */
package dto

type Errors int32

const (
	Errors_BAD_COMMAND                 Errors = 0
	Errors_BAD_DATA                    Errors = 1
	Errors_BINARY_COMMAND_FAILED       Errors = 2
	Errors_COMMAND_FAILED              Errors = 3
	Errors_COMMAND_PREEMPTED           Errors = 4
	Errors_COMMAND_TOO_LONG            Errors = 5
	Errors_CONNECTION_CLOSED           Errors = 6
	Errors_CONNECTION_FAILED           Errors = 7
	Errors_CONVERSION_FAILED           Errors = 8
	Errors_DEVICE_ADDRESS_CONFLICT     Errors = 9
	Errors_DEVICE_BUSY                 Errors = 10
	Errors_DEVICE_DB_FAILED            Errors = 11
	Errors_DEVICE_DETECTION_FAILED     Errors = 12
	Errors_DEVICE_FAILED               Errors = 13
	Errors_DEVICE_NOT_IDENTIFIED       Errors = 14
	Errors_DRIVER_DISABLED             Errors = 15
	Errors_G_CODE_EXECUTION            Errors = 16
	Errors_G_CODE_SYNTAX               Errors = 17
	Errors_INCOMPATIBLE_SHARED_LIBRARY Errors = 18
	Errors_INTERNAL_ERROR              Errors = 19
	Errors_INVALID_ARGUMENT            Errors = 20
	Errors_INVALID_DATA                Errors = 21
	Errors_INVALID_OPERATION           Errors = 22
	Errors_INVALID_PACKET              Errors = 23
	Errors_INVALID_PARK_STATE          Errors = 24
	Errors_INVALID_REQUEST_DATA        Errors = 25
	Errors_INVALID_RESPONSE            Errors = 26
	Errors_IO_CHANNEL_OUT_OF_RANGE     Errors = 27
	Errors_IO_FAILED                   Errors = 28
	Errors_LOCKSTEP_ENABLED            Errors = 29
	Errors_LOCKSTEP_NOT_ENABLED        Errors = 30
	Errors_MOVEMENT_FAILED             Errors = 31
	Errors_MOVEMENT_INTERRUPTED        Errors = 32
	Errors_NO_DEVICE_FOUND             Errors = 33
	Errors_NO_VALUE_FOR_KEY            Errors = 34
	Errors_NOT_SUPPORTED               Errors = 35
	Errors_OPERATION_FAILED            Errors = 36
	Errors_OS_FAILED                   Errors = 37
	Errors_OUT_OF_REQUEST_IDS          Errors = 38
	Errors_PVT_DISCONTINUITY           Errors = 39
	Errors_PVT_EXECUTION               Errors = 40
	Errors_PVT_MODE                    Errors = 41
	Errors_PVT_MOVEMENT_FAILED         Errors = 42
	Errors_PVT_MOVEMENT_INTERRUPTED    Errors = 43
	Errors_PVT_SETUP_FAILED            Errors = 44
	Errors_REMOTE_MODE                 Errors = 45
	Errors_REQUEST_TIMEOUT             Errors = 46
	Errors_SERIAL_PORT_BUSY            Errors = 47
	Errors_SET_DEVICE_STATE_FAILED     Errors = 48
	Errors_SET_PERIPHERAL_STATE_FAILED Errors = 49
	Errors_SETTING_NOT_FOUND           Errors = 50
	Errors_STREAM_DISCONTINUITY        Errors = 51
	Errors_STREAM_EXECUTION            Errors = 52
	Errors_STREAM_MODE                 Errors = 53
	Errors_STREAM_MOVEMENT_FAILED      Errors = 54
	Errors_STREAM_MOVEMENT_INTERRUPTED Errors = 55
	Errors_STREAM_SETUP_FAILED         Errors = 56
	Errors_TIMEOUT                     Errors = 57
	Errors_TRANSPORT_ALREADY_USED      Errors = 58
	Errors_UNKNOWN_REQUEST             Errors = 59
)

var Errors_name = map[Errors]string{
	Errors_BAD_COMMAND:                 "BadCommand",
	Errors_BAD_DATA:                    "BadData",
	Errors_BINARY_COMMAND_FAILED:       "BinaryCommandFailed",
	Errors_COMMAND_FAILED:              "CommandFailed",
	Errors_COMMAND_PREEMPTED:           "CommandPreempted",
	Errors_COMMAND_TOO_LONG:            "CommandTooLong",
	Errors_CONNECTION_CLOSED:           "ConnectionClosed",
	Errors_CONNECTION_FAILED:           "ConnectionFailed",
	Errors_CONVERSION_FAILED:           "ConversionFailed",
	Errors_DEVICE_ADDRESS_CONFLICT:     "DeviceAddressConflict",
	Errors_DEVICE_BUSY:                 "DeviceBusy",
	Errors_DEVICE_DB_FAILED:            "DeviceDbFailed",
	Errors_DEVICE_DETECTION_FAILED:     "DeviceDetectionFailed",
	Errors_DEVICE_FAILED:               "DeviceFailed",
	Errors_DEVICE_NOT_IDENTIFIED:       "DeviceNotIdentified",
	Errors_DRIVER_DISABLED:             "DriverDisabled",
	Errors_G_CODE_EXECUTION:            "GCodeExecution",
	Errors_G_CODE_SYNTAX:               "GCodeSyntax",
	Errors_INCOMPATIBLE_SHARED_LIBRARY: "IncompatibleSharedLibrary",
	Errors_INTERNAL_ERROR:              "InternalError",
	Errors_INVALID_ARGUMENT:            "InvalidArgument",
	Errors_INVALID_DATA:                "InvalidData",
	Errors_INVALID_OPERATION:           "InvalidOperation",
	Errors_INVALID_PACKET:              "InvalidPacket",
	Errors_INVALID_PARK_STATE:          "InvalidParkState",
	Errors_INVALID_REQUEST_DATA:        "InvalidRequestData",
	Errors_INVALID_RESPONSE:            "InvalidResponse",
	Errors_IO_CHANNEL_OUT_OF_RANGE:     "IoChannelOutOfRange",
	Errors_IO_FAILED:                   "IoFailed",
	Errors_LOCKSTEP_ENABLED:            "LockstepEnabled",
	Errors_LOCKSTEP_NOT_ENABLED:        "LockstepNotEnabled",
	Errors_MOVEMENT_FAILED:             "MovementFailed",
	Errors_MOVEMENT_INTERRUPTED:        "MovementInterrupted",
	Errors_NO_DEVICE_FOUND:             "NoDeviceFound",
	Errors_NO_VALUE_FOR_KEY:            "NoValueForKey",
	Errors_NOT_SUPPORTED:               "NotSupported",
	Errors_OPERATION_FAILED:            "OperationFailed",
	Errors_OS_FAILED:                   "OsFailed",
	Errors_OUT_OF_REQUEST_IDS:          "OutOfRequestIds",
	Errors_PVT_DISCONTINUITY:           "PvtDiscontinuity",
	Errors_PVT_EXECUTION:               "PvtExecution",
	Errors_PVT_MODE:                    "PvtMode",
	Errors_PVT_MOVEMENT_FAILED:         "PvtMovementFailed",
	Errors_PVT_MOVEMENT_INTERRUPTED:    "PvtMovementInterrupted",
	Errors_PVT_SETUP_FAILED:            "PvtSetupFailed",
	Errors_REMOTE_MODE:                 "RemoteMode",
	Errors_REQUEST_TIMEOUT:             "RequestTimeout",
	Errors_SERIAL_PORT_BUSY:            "SerialPortBusy",
	Errors_SET_DEVICE_STATE_FAILED:     "SetDeviceStateFailed",
	Errors_SET_PERIPHERAL_STATE_FAILED: "SetPeripheralStateFailed",
	Errors_SETTING_NOT_FOUND:           "SettingNotFound",
	Errors_STREAM_DISCONTINUITY:        "StreamDiscontinuity",
	Errors_STREAM_EXECUTION:            "StreamExecution",
	Errors_STREAM_MODE:                 "StreamMode",
	Errors_STREAM_MOVEMENT_FAILED:      "StreamMovementFailed",
	Errors_STREAM_MOVEMENT_INTERRUPTED: "StreamMovementInterrupted",
	Errors_STREAM_SETUP_FAILED:         "StreamSetupFailed",
	Errors_TIMEOUT:                     "Timeout",
	Errors_TRANSPORT_ALREADY_USED:      "TransportAlreadyUsed",
	Errors_UNKNOWN_REQUEST:             "UnknownRequest",
}
