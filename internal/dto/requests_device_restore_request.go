/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceRestoreRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	Axis        int32 `bson:"axis"`
	Hard        bool  `bson:"hard"`
}

func (c *DeviceRestoreRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceRestoreRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceRestoreRequest) GetAxis() int32 {
	return c.Axis
}

func (c *DeviceRestoreRequest) GetHard() bool {
	return c.Hard
}

func (c *DeviceRestoreRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceRestoreRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceRestoreRequest) Clone() (*DeviceRestoreRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceRestoreRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceRestoreRequest) Sanitize() error {
	return nil
}
