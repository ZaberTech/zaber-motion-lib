/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Information about a parameter used for the simple tuning method.
type AsciiSimpleTuningParamDefinition struct {
	// The name of the parameter.
	Name string `bson:"name"`
	// The human readable description of the effect of a lower value on this setting.
	MinLabel string `bson:"minLabel"`
	// The human readable description of the effect of a higher value on this setting.
	MaxLabel string `bson:"maxLabel"`
	// How this parameter will be parsed by the tuner.
	DataType string `bson:"dataType"`
	// The default value of this parameter.
	DefaultValue *float64 `bson:"defaultValue"`
}

// The name of the parameter.
func (c *AsciiSimpleTuningParamDefinition) GetName() string {
	return c.Name
}

// The human readable description of the effect of a lower value on this setting.
func (c *AsciiSimpleTuningParamDefinition) GetMinLabel() string {
	return c.MinLabel
}

// The human readable description of the effect of a higher value on this setting.
func (c *AsciiSimpleTuningParamDefinition) GetMaxLabel() string {
	return c.MaxLabel
}

// How this parameter will be parsed by the tuner.
func (c *AsciiSimpleTuningParamDefinition) GetDataType() string {
	return c.DataType
}

// The default value of this parameter.
func (c *AsciiSimpleTuningParamDefinition) GetDefaultValue() *float64 {
	return c.DefaultValue
}

func (c *AsciiSimpleTuningParamDefinition) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiSimpleTuningParamDefinition) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiSimpleTuningParamDefinition) Clone() (*AsciiSimpleTuningParamDefinition, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiSimpleTuningParamDefinition{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiSimpleTuningParamDefinition) Sanitize() error {
	return nil
}
