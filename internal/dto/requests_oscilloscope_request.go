/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type OscilloscopeRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
}

func (c *OscilloscopeRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *OscilloscopeRequest) GetDevice() int32 {
	return c.Device
}

func (c *OscilloscopeRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *OscilloscopeRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *OscilloscopeRequest) Clone() (*OscilloscopeRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &OscilloscopeRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *OscilloscopeRequest) Sanitize() error {
	return nil
}
