/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamWaitRequest struct {
	InterfaceId int32   `bson:"interfaceId"`
	Device      int32   `bson:"device"`
	StreamId    int32   `bson:"streamId"`
	Pvt         bool    `bson:"pvt"`
	Time        float64 `bson:"time"`
	Unit        string  `bson:"unit"`
}

func (c *StreamWaitRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamWaitRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamWaitRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamWaitRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamWaitRequest) GetTime() float64 {
	return c.Time
}

func (c *StreamWaitRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamWaitRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamWaitRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamWaitRequest) Clone() (*StreamWaitRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamWaitRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamWaitRequest) Sanitize() error {
	return nil
}
