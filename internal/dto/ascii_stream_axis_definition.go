/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Defines an axis of the stream.
type AsciiStreamAxisDefinition struct {
	// Number of a physical axis or a lockstep group.
	AxisNumber int32 `bson:"axisNumber"`
	// Defines the type of the axis.
	AxisType *AsciiStreamAxisType `bson:"axisType"`
}

// Number of a physical axis or a lockstep group.
func (c *AsciiStreamAxisDefinition) GetAxisNumber() int32 {
	return c.AxisNumber
}

// Defines the type of the axis.
func (c *AsciiStreamAxisDefinition) GetAxisType() *AsciiStreamAxisType {
	return c.AxisType
}

func (c *AsciiStreamAxisDefinition) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiStreamAxisDefinition) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiStreamAxisDefinition) Clone() (*AsciiStreamAxisDefinition, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiStreamAxisDefinition{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiStreamAxisDefinition) Sanitize() error {
	return nil
}
