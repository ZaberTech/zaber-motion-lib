/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for GCodeSyntaxException.
type ExceptionsGCodeSyntaxExceptionData struct {
	// The index in the block string that caused the exception.
	FromBlock int32 `bson:"fromBlock"`
	// The end index in the block string that caused the exception.
	// The end index is exclusive.
	ToBlock int32 `bson:"toBlock"`
}

// The index in the block string that caused the exception.
func (c *ExceptionsGCodeSyntaxExceptionData) GetFromBlock() int32 {
	return c.FromBlock
}

// The end index in the block string that caused the exception.
// The end index is exclusive.
func (c *ExceptionsGCodeSyntaxExceptionData) GetToBlock() int32 {
	return c.ToBlock
}

func (c *ExceptionsGCodeSyntaxExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsGCodeSyntaxExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsGCodeSyntaxExceptionData) Clone() (*ExceptionsGCodeSyntaxExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsGCodeSyntaxExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsGCodeSyntaxExceptionData) Sanitize() error {
	return nil
}
