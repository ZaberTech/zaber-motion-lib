/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamBufferEraseRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	BufferId    int32 `bson:"bufferId"`
	Pvt         bool  `bson:"pvt"`
}

func (c *StreamBufferEraseRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamBufferEraseRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamBufferEraseRequest) GetBufferId() int32 {
	return c.BufferId
}

func (c *StreamBufferEraseRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamBufferEraseRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamBufferEraseRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamBufferEraseRequest) Clone() (*StreamBufferEraseRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamBufferEraseRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamBufferEraseRequest) Sanitize() error {
	return nil
}
