/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Configuration of a translator.
type GcodeTranslatorConfig struct {
	// Optional custom mapping of translator axes to stream axes.
	AxisMappings []*GcodeAxisMapping `bson:"axisMappings"`
	// Optional transformation of axes.
	AxisTransformations []*GcodeAxisTransformation `bson:"axisTransformations"`
}

// Optional custom mapping of translator axes to stream axes.
func (c *GcodeTranslatorConfig) GetAxisMappings() []*GcodeAxisMapping {
	return c.AxisMappings
}

// Optional transformation of axes.
func (c *GcodeTranslatorConfig) GetAxisTransformations() []*GcodeAxisTransformation {
	return c.AxisTransformations
}

func (c *GcodeTranslatorConfig) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeTranslatorConfig) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeTranslatorConfig) Clone() (*GcodeTranslatorConfig, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeTranslatorConfig{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeTranslatorConfig) Sanitize() error {
	for _, item := range c.AxisMappings {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property AxisMappings of TranslatorConfig is nil")
		}
	}

	for _, item := range c.AxisTransformations {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property AxisTransformations of TranslatorConfig is nil")
		}
	}

	return nil
}
