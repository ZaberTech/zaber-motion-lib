/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceCancelAllOutputsScheduleRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	Analog      bool   `bson:"analog"`
	Channels    []bool `bson:"channels"`
}

func (c *DeviceCancelAllOutputsScheduleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceCancelAllOutputsScheduleRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceCancelAllOutputsScheduleRequest) GetAnalog() bool {
	return c.Analog
}

func (c *DeviceCancelAllOutputsScheduleRequest) GetChannels() []bool {
	return c.Channels
}

func (c *DeviceCancelAllOutputsScheduleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceCancelAllOutputsScheduleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceCancelAllOutputsScheduleRequest) Clone() (*DeviceCancelAllOutputsScheduleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceCancelAllOutputsScheduleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceCancelAllOutputsScheduleRequest) Sanitize() error {
	if c.Channels == nil {
		c.Channels = make([]bool, 0)
	}

	return nil
}
