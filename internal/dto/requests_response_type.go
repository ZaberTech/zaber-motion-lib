/* This file is generated. Do not modify by hand. */
package dto

type ResponseType int32

const (
	ResponseType_OK    ResponseType = 0
	ResponseType_ERROR ResponseType = 1
)

var ResponseType_name = map[ResponseType]string{
	ResponseType_OK:    "Ok",
	ResponseType_ERROR: "Error",
}
