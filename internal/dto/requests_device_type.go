/* This file is generated. Do not modify by hand. */
package dto

type DeviceType int32

const (
	DeviceType_ANY                DeviceType = 0
	DeviceType_PROCESS_CONTROLLER DeviceType = 1
)

var DeviceType_name = map[DeviceType]string{
	DeviceType_ANY:                "Any",
	DeviceType_PROCESS_CONTROLLER: "ProcessController",
}
