/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceIdentifyRequest struct {
	InterfaceId   int32            `bson:"interfaceId"`
	Device        int32            `bson:"device"`
	AssumeVersion *FirmwareVersion `bson:"assumeVersion"`
}

func (c *DeviceIdentifyRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *DeviceIdentifyRequest) GetDevice() int32 {
	return c.Device
}

func (c *DeviceIdentifyRequest) GetAssumeVersion() *FirmwareVersion {
	return c.AssumeVersion
}

func (c *DeviceIdentifyRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceIdentifyRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceIdentifyRequest) Clone() (*DeviceIdentifyRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceIdentifyRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceIdentifyRequest) Sanitize() error {
	if c.AssumeVersion != nil {
		if err := c.AssumeVersion.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
