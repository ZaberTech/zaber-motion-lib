/* This file is generated. Do not modify by hand. */
package dto

// Servo Tuning Parameter Set to target.
type ProductProcessControllerSourceSensor int32

const (
	ProductProcessControllerSourceSensor_THERMISTOR   ProductProcessControllerSourceSensor = 10
	ProductProcessControllerSourceSensor_ANALOG_INPUT ProductProcessControllerSourceSensor = 20
)

var ProductProcessControllerSourceSensor_name = map[ProductProcessControllerSourceSensor]string{
	ProductProcessControllerSourceSensor_THERMISTOR:   "Thermistor",
	ProductProcessControllerSourceSensor_ANALOG_INPUT: "AnalogInput",
}
