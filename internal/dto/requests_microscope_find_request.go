/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type MicroscopeFindRequest struct {
	InterfaceId int32                           `bson:"interfaceId"`
	ThirdParty  *MicroscopyThirdPartyComponents `bson:"thirdParty"`
}

func (c *MicroscopeFindRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *MicroscopeFindRequest) GetThirdParty() *MicroscopyThirdPartyComponents {
	return c.ThirdParty
}

func (c *MicroscopeFindRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopeFindRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopeFindRequest) Clone() (*MicroscopeFindRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopeFindRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopeFindRequest) Sanitize() error {
	if c.ThirdParty != nil {
		if err := c.ThirdParty.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
