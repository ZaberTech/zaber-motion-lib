/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Represents a transformation of a translator axis.
type GcodeAxisTransformation struct {
	// Letter of the translator axis (X,Y,Z,A,B,C,E).
	AxisLetter string `bson:"axisLetter"`
	// Scaling factor.
	Scaling *float64 `bson:"scaling"`
	// Translation distance.
	Translation *Measurement `bson:"translation"`
}

// Letter of the translator axis (X,Y,Z,A,B,C,E).
func (c *GcodeAxisTransformation) GetAxisLetter() string {
	return c.AxisLetter
}

// Scaling factor.
func (c *GcodeAxisTransformation) GetScaling() *float64 {
	return c.Scaling
}

// Translation distance.
func (c *GcodeAxisTransformation) GetTranslation() *Measurement {
	return c.Translation
}

func (c *GcodeAxisTransformation) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GcodeAxisTransformation) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GcodeAxisTransformation) Clone() (*GcodeAxisTransformation, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GcodeAxisTransformation{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GcodeAxisTransformation) Sanitize() error {
	if c.Translation != nil {
		if err := c.Translation.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
