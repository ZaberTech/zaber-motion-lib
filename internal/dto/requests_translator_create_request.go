/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorCreateRequest struct {
	Definition *GcodeDeviceDefinition `bson:"definition"`
	Config     *GcodeTranslatorConfig `bson:"config"`
}

func (c *TranslatorCreateRequest) GetDefinition() *GcodeDeviceDefinition {
	return c.Definition
}

func (c *TranslatorCreateRequest) GetConfig() *GcodeTranslatorConfig {
	return c.Config
}

func (c *TranslatorCreateRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorCreateRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorCreateRequest) Clone() (*TranslatorCreateRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorCreateRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorCreateRequest) Sanitize() error {
	if c.Definition != nil {
		if err := c.Definition.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Definition of TranslatorCreateRequest is nil")
	}

	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	}

	return nil
}
