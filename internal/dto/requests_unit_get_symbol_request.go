/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type UnitGetSymbolRequest struct {
	Unit string `bson:"unit"`
}

func (c *UnitGetSymbolRequest) GetUnit() string {
	return c.Unit
}

func (c *UnitGetSymbolRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *UnitGetSymbolRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *UnitGetSymbolRequest) Clone() (*UnitGetSymbolRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &UnitGetSymbolRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *UnitGetSymbolRequest) Sanitize() error {
	return nil
}
