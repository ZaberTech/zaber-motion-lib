/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Information describing why the command could not fit.
type ExceptionsCommandTooLongExceptionData struct {
	// The part of the command that could be successfully fit in the space provided by the protocol.
	Fit string `bson:"fit"`
	// The part of the command that could not fit within the space provided.
	Remainder string `bson:"remainder"`
	// The length of the ascii string that can be written to a single line.
	PacketSize int32 `bson:"packetSize"`
	// The number of lines a command can be split over using continuations.
	PacketsMax int32 `bson:"packetsMax"`
}

// The part of the command that could be successfully fit in the space provided by the protocol.
func (c *ExceptionsCommandTooLongExceptionData) GetFit() string {
	return c.Fit
}

// The part of the command that could not fit within the space provided.
func (c *ExceptionsCommandTooLongExceptionData) GetRemainder() string {
	return c.Remainder
}

// The length of the ascii string that can be written to a single line.
func (c *ExceptionsCommandTooLongExceptionData) GetPacketSize() int32 {
	return c.PacketSize
}

// The number of lines a command can be split over using continuations.
func (c *ExceptionsCommandTooLongExceptionData) GetPacketsMax() int32 {
	return c.PacketsMax
}

func (c *ExceptionsCommandTooLongExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsCommandTooLongExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsCommandTooLongExceptionData) Clone() (*ExceptionsCommandTooLongExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsCommandTooLongExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsCommandTooLongExceptionData) Sanitize() error {
	return nil
}
