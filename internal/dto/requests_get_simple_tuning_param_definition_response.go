/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GetSimpleTuningParamDefinitionResponse struct {
	Params []*AsciiSimpleTuningParamDefinition `bson:"params"`
}

func (c *GetSimpleTuningParamDefinitionResponse) GetParams() []*AsciiSimpleTuningParamDefinition {
	return c.Params
}

func (c *GetSimpleTuningParamDefinitionResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GetSimpleTuningParamDefinitionResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GetSimpleTuningParamDefinitionResponse) Clone() (*GetSimpleTuningParamDefinitionResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GetSimpleTuningParamDefinitionResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GetSimpleTuningParamDefinitionResponse) Sanitize() error {
	if c.Params == nil {
		c.Params = make([]*AsciiSimpleTuningParamDefinition, 0)
	}

	for _, item := range c.Params {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Params of GetSimpleTuningParamDefinitionResponse is nil")
		}
	}

	return nil
}
