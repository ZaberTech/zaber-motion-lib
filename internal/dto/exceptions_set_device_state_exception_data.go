/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Contains additional data for a SetDeviceStateFailedException.
type ExceptionsSetDeviceStateExceptionData struct {
	// A list of settings which could not be set.
	Settings []string `bson:"settings"`
	// The reason the stream buffers could not be set.
	StreamBuffers []string `bson:"streamBuffers"`
	// The reason the pvt buffers could not be set.
	PvtBuffers []string `bson:"pvtBuffers"`
	// The reason the triggers could not be set.
	Triggers []string `bson:"triggers"`
	// The reason servo tuning could not be set.
	ServoTuning string `bson:"servoTuning"`
	// The reasons stored positions could not be set.
	StoredPositions []string `bson:"storedPositions"`
	// The reasons storage could not be set.
	Storage []string `bson:"storage"`
	// Errors for any peripherals that could not be set.
	Peripherals []*ExceptionsSetPeripheralStateExceptionData `bson:"peripherals"`
}

// A list of settings which could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetSettings() []string {
	return c.Settings
}

// The reason the stream buffers could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetStreamBuffers() []string {
	return c.StreamBuffers
}

// The reason the pvt buffers could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetPvtBuffers() []string {
	return c.PvtBuffers
}

// The reason the triggers could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetTriggers() []string {
	return c.Triggers
}

// The reason servo tuning could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetServoTuning() string {
	return c.ServoTuning
}

// The reasons stored positions could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetStoredPositions() []string {
	return c.StoredPositions
}

// The reasons storage could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetStorage() []string {
	return c.Storage
}

// Errors for any peripherals that could not be set.
func (c *ExceptionsSetDeviceStateExceptionData) GetPeripherals() []*ExceptionsSetPeripheralStateExceptionData {
	return c.Peripherals
}

func (c *ExceptionsSetDeviceStateExceptionData) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *ExceptionsSetDeviceStateExceptionData) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *ExceptionsSetDeviceStateExceptionData) Clone() (*ExceptionsSetDeviceStateExceptionData, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &ExceptionsSetDeviceStateExceptionData{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *ExceptionsSetDeviceStateExceptionData) Sanitize() error {
	if c.Settings == nil {
		c.Settings = make([]string, 0)
	}

	if c.StreamBuffers == nil {
		c.StreamBuffers = make([]string, 0)
	}

	if c.PvtBuffers == nil {
		c.PvtBuffers = make([]string, 0)
	}

	if c.Triggers == nil {
		c.Triggers = make([]string, 0)
	}

	if c.StoredPositions == nil {
		c.StoredPositions = make([]string, 0)
	}

	if c.Storage == nil {
		c.Storage = make([]string, 0)
	}

	if c.Peripherals == nil {
		c.Peripherals = make([]*ExceptionsSetPeripheralStateExceptionData, 0)
	}

	for _, item := range c.Peripherals {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Peripherals of SetDeviceStateExceptionData is nil")
		}
	}

	return nil
}
