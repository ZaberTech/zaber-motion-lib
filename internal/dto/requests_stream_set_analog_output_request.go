/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetAnalogOutputRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	StreamId      int32   `bson:"streamId"`
	Pvt           bool    `bson:"pvt"`
	ChannelNumber int32   `bson:"channelNumber"`
	Value         float64 `bson:"value"`
}

func (c *StreamSetAnalogOutputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetAnalogOutputRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetAnalogOutputRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetAnalogOutputRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetAnalogOutputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamSetAnalogOutputRequest) GetValue() float64 {
	return c.Value
}

func (c *StreamSetAnalogOutputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetAnalogOutputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetAnalogOutputRequest) Clone() (*StreamSetAnalogOutputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetAnalogOutputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetAnalogOutputRequest) Sanitize() error {
	return nil
}
