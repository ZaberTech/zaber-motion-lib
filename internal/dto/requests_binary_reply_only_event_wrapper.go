/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type BinaryReplyOnlyEventWrapper struct {
	InterfaceId int32                 `bson:"interfaceId"`
	Reply       *BinaryReplyOnlyEvent `bson:"reply"`
}

func (c *BinaryReplyOnlyEventWrapper) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *BinaryReplyOnlyEventWrapper) GetReply() *BinaryReplyOnlyEvent {
	return c.Reply
}

func (c *BinaryReplyOnlyEventWrapper) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryReplyOnlyEventWrapper) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryReplyOnlyEventWrapper) Clone() (*BinaryReplyOnlyEventWrapper, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryReplyOnlyEventWrapper{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryReplyOnlyEventWrapper) Sanitize() error {
	if c.Reply != nil {
		if err := c.Reply.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Reply of BinaryReplyOnlyEventWrapper is nil")
	}

	return nil
}
