/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamWaitAnalogInputRequest struct {
	InterfaceId   int32   `bson:"interfaceId"`
	Device        int32   `bson:"device"`
	StreamId      int32   `bson:"streamId"`
	Pvt           bool    `bson:"pvt"`
	ChannelNumber int32   `bson:"channelNumber"`
	Condition     string  `bson:"condition"`
	Value         float64 `bson:"value"`
}

func (c *StreamWaitAnalogInputRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamWaitAnalogInputRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamWaitAnalogInputRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamWaitAnalogInputRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamWaitAnalogInputRequest) GetChannelNumber() int32 {
	return c.ChannelNumber
}

func (c *StreamWaitAnalogInputRequest) GetCondition() string {
	return c.Condition
}

func (c *StreamWaitAnalogInputRequest) GetValue() float64 {
	return c.Value
}

func (c *StreamWaitAnalogInputRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamWaitAnalogInputRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamWaitAnalogInputRequest) Clone() (*StreamWaitAnalogInputRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamWaitAnalogInputRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamWaitAnalogInputRequest) Sanitize() error {
	return nil
}
