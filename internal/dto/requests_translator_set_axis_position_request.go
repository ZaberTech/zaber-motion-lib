/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TranslatorSetAxisPositionRequest struct {
	TranslatorId int32   `bson:"translatorId"`
	Axis         string  `bson:"axis"`
	Position     float64 `bson:"position"`
	Unit         string  `bson:"unit"`
}

func (c *TranslatorSetAxisPositionRequest) GetTranslatorId() int32 {
	return c.TranslatorId
}

func (c *TranslatorSetAxisPositionRequest) GetAxis() string {
	return c.Axis
}

func (c *TranslatorSetAxisPositionRequest) GetPosition() float64 {
	return c.Position
}

func (c *TranslatorSetAxisPositionRequest) GetUnit() string {
	return c.Unit
}

func (c *TranslatorSetAxisPositionRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TranslatorSetAxisPositionRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TranslatorSetAxisPositionRequest) Clone() (*TranslatorSetAxisPositionRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TranslatorSetAxisPositionRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TranslatorSetAxisPositionRequest) Sanitize() error {
	return nil
}
