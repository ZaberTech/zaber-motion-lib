/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepEnableRequest struct {
	InterfaceId     int32   `bson:"interfaceId"`
	Device          int32   `bson:"device"`
	LockstepGroupId int32   `bson:"lockstepGroupId"`
	Axes            []int32 `bson:"axes"`
}

func (c *LockstepEnableRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepEnableRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepEnableRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepEnableRequest) GetAxes() []int32 {
	return c.Axes
}

func (c *LockstepEnableRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepEnableRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepEnableRequest) Clone() (*LockstepEnableRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepEnableRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepEnableRequest) Sanitize() error {
	if c.Axes == nil {
		c.Axes = make([]int32, 0)
	}

	return nil
}
