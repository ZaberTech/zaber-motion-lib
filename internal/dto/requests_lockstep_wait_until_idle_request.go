/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type LockstepWaitUntilIdleRequest struct {
	InterfaceId       int32 `bson:"interfaceId"`
	Device            int32 `bson:"device"`
	LockstepGroupId   int32 `bson:"lockstepGroupId"`
	ThrowErrorOnFault bool  `bson:"throwErrorOnFault"`
}

func (c *LockstepWaitUntilIdleRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *LockstepWaitUntilIdleRequest) GetDevice() int32 {
	return c.Device
}

func (c *LockstepWaitUntilIdleRequest) GetLockstepGroupId() int32 {
	return c.LockstepGroupId
}

func (c *LockstepWaitUntilIdleRequest) GetThrowErrorOnFault() bool {
	return c.ThrowErrorOnFault
}

func (c *LockstepWaitUntilIdleRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *LockstepWaitUntilIdleRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *LockstepWaitUntilIdleRequest) Clone() (*LockstepWaitUntilIdleRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &LockstepWaitUntilIdleRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *LockstepWaitUntilIdleRequest) Sanitize() error {
	return nil
}
