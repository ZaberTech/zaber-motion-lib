/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type MicroscopeConfigResponse struct {
	Config *MicroscopyMicroscopeConfig `bson:"config"`
}

func (c *MicroscopeConfigResponse) GetConfig() *MicroscopyMicroscopeConfig {
	return c.Config
}

func (c *MicroscopeConfigResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *MicroscopeConfigResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *MicroscopeConfigResponse) Clone() (*MicroscopeConfigResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &MicroscopeConfigResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *MicroscopeConfigResponse) Sanitize() error {
	if c.Config != nil {
		if err := c.Config.Sanitize(); err != nil {
			return err
		}
	} else {
		return makeError("Required property Config of MicroscopeConfigResponse is nil")
	}

	return nil
}
