/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type DeviceDetectResponse struct {
	Devices []int32 `bson:"devices"`
}

func (c *DeviceDetectResponse) GetDevices() []int32 {
	return c.Devices
}

func (c *DeviceDetectResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *DeviceDetectResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *DeviceDetectResponse) Clone() (*DeviceDetectResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &DeviceDetectResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *DeviceDetectResponse) Sanitize() error {
	if c.Devices == nil {
		c.Devices = make([]int32, 0)
	}

	return nil
}
