/* This file is generated. Do not modify by hand. */
package dto

type InterfaceType int32

const (
	InterfaceType_SERIAL_PORT   InterfaceType = 0
	InterfaceType_TCP           InterfaceType = 1
	InterfaceType_CUSTOM        InterfaceType = 2
	InterfaceType_IOT           InterfaceType = 3
	InterfaceType_NETWORK_SHARE InterfaceType = 4
)

var InterfaceType_name = map[InterfaceType]string{
	InterfaceType_SERIAL_PORT:   "SerialPort",
	InterfaceType_TCP:           "Tcp",
	InterfaceType_CUSTOM:        "Custom",
	InterfaceType_IOT:           "Iot",
	InterfaceType_NETWORK_SHARE: "NetworkShare",
}
