/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Specifies a setting to get with one of the multi-get commands.
type AsciiGetSetting struct {
	// The setting to read.
	Setting string `bson:"setting"`
	// The list of axes to read.
	Axes []int32 `bson:"axes"`
	// The unit to convert the read settings to.
	Unit string `bson:"unit"`
}

// The setting to read.
func (c *AsciiGetSetting) GetSetting() string {
	return c.Setting
}

// The list of axes to read.
func (c *AsciiGetSetting) GetAxes() []int32 {
	return c.Axes
}

// The unit to convert the read settings to.
func (c *AsciiGetSetting) GetUnit() string {
	return c.Unit
}

func (c *AsciiGetSetting) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiGetSetting) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiGetSetting) Clone() (*AsciiGetSetting, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiGetSetting{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiGetSetting) Sanitize() error {
	return nil
}
