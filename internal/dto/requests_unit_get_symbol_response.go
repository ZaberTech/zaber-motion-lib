/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type UnitGetSymbolResponse struct {
	Symbol string `bson:"symbol"`
}

func (c *UnitGetSymbolResponse) GetSymbol() string {
	return c.Symbol
}

func (c *UnitGetSymbolResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *UnitGetSymbolResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *UnitGetSymbolResponse) Clone() (*UnitGetSymbolResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &UnitGetSymbolResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *UnitGetSymbolResponse) Sanitize() error {
	return nil
}
