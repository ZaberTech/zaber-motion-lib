/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Named parameter with optional value.
type NamedParameter struct {
	// Name of the parameter.
	Name string `bson:"name"`
	// Optional value of the parameter.
	Value *float64 `bson:"value"`
}

// Name of the parameter.
func (c *NamedParameter) GetName() string {
	return c.Name
}

// Optional value of the parameter.
func (c *NamedParameter) GetValue() *float64 {
	return c.Value
}

func (c *NamedParameter) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *NamedParameter) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *NamedParameter) Clone() (*NamedParameter, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &NamedParameter{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *NamedParameter) Sanitize() error {
	return nil
}
