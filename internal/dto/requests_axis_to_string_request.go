/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type AxisToStringRequest struct {
	InterfaceId  int32  `bson:"interfaceId"`
	Device       int32  `bson:"device"`
	Axis         int32  `bson:"axis"`
	TypeOverride string `bson:"typeOverride"`
}

func (c *AxisToStringRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *AxisToStringRequest) GetDevice() int32 {
	return c.Device
}

func (c *AxisToStringRequest) GetAxis() int32 {
	return c.Axis
}

func (c *AxisToStringRequest) GetTypeOverride() string {
	return c.TypeOverride
}

func (c *AxisToStringRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AxisToStringRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AxisToStringRequest) Clone() (*AxisToStringRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AxisToStringRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AxisToStringRequest) Sanitize() error {
	return nil
}
