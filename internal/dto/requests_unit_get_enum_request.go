/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type UnitGetEnumRequest struct {
	Symbol string `bson:"symbol"`
}

func (c *UnitGetEnumRequest) GetSymbol() string {
	return c.Symbol
}

func (c *UnitGetEnumRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *UnitGetEnumRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *UnitGetEnumRequest) Clone() (*UnitGetEnumRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &UnitGetEnumRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *UnitGetEnumRequest) Sanitize() error {
	return nil
}
