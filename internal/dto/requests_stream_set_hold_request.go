/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamSetHoldRequest struct {
	InterfaceId int32 `bson:"interfaceId"`
	Device      int32 `bson:"device"`
	StreamId    int32 `bson:"streamId"`
	Pvt         bool  `bson:"pvt"`
	Hold        bool  `bson:"hold"`
}

func (c *StreamSetHoldRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamSetHoldRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamSetHoldRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamSetHoldRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamSetHoldRequest) GetHold() bool {
	return c.Hold
}

func (c *StreamSetHoldRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamSetHoldRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamSetHoldRequest) Clone() (*StreamSetHoldRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamSetHoldRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamSetHoldRequest) Sanitize() error {
	return nil
}
