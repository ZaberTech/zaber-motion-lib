/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type CustomInterfaceOpenResponse struct {
	TransportId int32 `bson:"transportId"`
}

func (c *CustomInterfaceOpenResponse) GetTransportId() int32 {
	return c.TransportId
}

func (c *CustomInterfaceOpenResponse) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *CustomInterfaceOpenResponse) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *CustomInterfaceOpenResponse) Clone() (*CustomInterfaceOpenResponse, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &CustomInterfaceOpenResponse{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *CustomInterfaceOpenResponse) Sanitize() error {
	return nil
}
