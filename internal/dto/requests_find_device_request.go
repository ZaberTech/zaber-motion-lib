/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type FindDeviceRequest struct {
	InterfaceId   int32 `bson:"interfaceId"`
	DeviceAddress int32 `bson:"deviceAddress"`
}

func (c *FindDeviceRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *FindDeviceRequest) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

func (c *FindDeviceRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *FindDeviceRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *FindDeviceRequest) Clone() (*FindDeviceRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &FindDeviceRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *FindDeviceRequest) Sanitize() error {
	return nil
}
