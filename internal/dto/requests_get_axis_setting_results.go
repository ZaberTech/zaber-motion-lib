/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type GetAxisSettingResults struct {
	Results []*AsciiGetAxisSettingResult `bson:"results"`
}

func (c *GetAxisSettingResults) GetResults() []*AsciiGetAxisSettingResult {
	return c.Results
}

func (c *GetAxisSettingResults) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *GetAxisSettingResults) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *GetAxisSettingResults) Clone() (*GetAxisSettingResults, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &GetAxisSettingResults{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *GetAxisSettingResults) Sanitize() error {
	if c.Results == nil {
		c.Results = make([]*AsciiGetAxisSettingResult, 0)
	}

	for _, item := range c.Results {
		if item != nil {
			if err := item.Sanitize(); err != nil {
				return err
			}
		} else {
			return makeError("Required item in property Results of GetAxisSettingResults is nil")
		}
	}

	return nil
}
