/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// The tuning of this axis represented by PID parameters.
type AsciiPidTuning struct {
	// The tuning algorithm used to tune this axis.
	Type string `bson:"type"`
	// The version of the tuning algorithm used to tune this axis.
	Version int32 `bson:"version"`
	// The positional tuning argument.
	P float64 `bson:"p"`
	// The integral tuning argument.
	I float64 `bson:"i"`
	// The derivative tuning argument.
	D float64 `bson:"d"`
	// The frequency cutoff for the tuning.
	Fc float64 `bson:"fc"`
}

// The tuning algorithm used to tune this axis.
func (c *AsciiPidTuning) GetType() string {
	return c.Type
}

// The version of the tuning algorithm used to tune this axis.
func (c *AsciiPidTuning) GetVersion() int32 {
	return c.Version
}

// The positional tuning argument.
func (c *AsciiPidTuning) GetP() float64 {
	return c.P
}

// The integral tuning argument.
func (c *AsciiPidTuning) GetI() float64 {
	return c.I
}

// The derivative tuning argument.
func (c *AsciiPidTuning) GetD() float64 {
	return c.D
}

// The frequency cutoff for the tuning.
func (c *AsciiPidTuning) GetFc() float64 {
	return c.Fc
}

func (c *AsciiPidTuning) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *AsciiPidTuning) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *AsciiPidTuning) Clone() (*AsciiPidTuning, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &AsciiPidTuning{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *AsciiPidTuning) Sanitize() error {
	return nil
}
