/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type TriggerOnFireSetRequest struct {
	InterfaceId   int32                 `bson:"interfaceId"`
	Device        int32                 `bson:"device"`
	TriggerNumber int32                 `bson:"triggerNumber"`
	Action        AsciiTriggerAction    `bson:"action"`
	Axis          int32                 `bson:"axis"`
	Setting       string                `bson:"setting"`
	Operation     AsciiTriggerOperation `bson:"operation"`
	Value         float64               `bson:"value"`
	Unit          string                `bson:"unit"`
}

func (c *TriggerOnFireSetRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *TriggerOnFireSetRequest) GetDevice() int32 {
	return c.Device
}

func (c *TriggerOnFireSetRequest) GetTriggerNumber() int32 {
	return c.TriggerNumber
}

func (c *TriggerOnFireSetRequest) GetAction() AsciiTriggerAction {
	return c.Action
}

func (c *TriggerOnFireSetRequest) GetAxis() int32 {
	return c.Axis
}

func (c *TriggerOnFireSetRequest) GetSetting() string {
	return c.Setting
}

func (c *TriggerOnFireSetRequest) GetOperation() AsciiTriggerOperation {
	return c.Operation
}

func (c *TriggerOnFireSetRequest) GetValue() float64 {
	return c.Value
}

func (c *TriggerOnFireSetRequest) GetUnit() string {
	return c.Unit
}

func (c *TriggerOnFireSetRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *TriggerOnFireSetRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *TriggerOnFireSetRequest) Clone() (*TriggerOnFireSetRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &TriggerOnFireSetRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *TriggerOnFireSetRequest) Sanitize() error {
	return nil
}
