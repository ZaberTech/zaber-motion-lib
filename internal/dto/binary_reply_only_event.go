/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

// Spontaneous message received from the device.
type BinaryReplyOnlyEvent struct {
	// Number of the device that sent or should receive the message.
	DeviceAddress int32 `bson:"deviceAddress"`
	// The warning flag contains the highest priority warning currently active for the device or axis.
	Command int32 `bson:"command"`
	// Data payload of the message, if applicable, or zero otherwise.
	Data int32 `bson:"data"`
}

// Number of the device that sent or should receive the message.
func (c *BinaryReplyOnlyEvent) GetDeviceAddress() int32 {
	return c.DeviceAddress
}

// The warning flag contains the highest priority warning currently active for the device or axis.
func (c *BinaryReplyOnlyEvent) GetCommand() int32 {
	return c.Command
}

// Data payload of the message, if applicable, or zero otherwise.
func (c *BinaryReplyOnlyEvent) GetData() int32 {
	return c.Data
}

func (c *BinaryReplyOnlyEvent) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *BinaryReplyOnlyEvent) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *BinaryReplyOnlyEvent) Clone() (*BinaryReplyOnlyEvent, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &BinaryReplyOnlyEvent{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *BinaryReplyOnlyEvent) Sanitize() error {
	return nil
}
