/* This file is generated. Do not modify by hand. */
package dto

import (
	"go.mongodb.org/mongo-driver/bson"
)

type StreamGetMaxTangentialAccelerationRequest struct {
	InterfaceId int32  `bson:"interfaceId"`
	Device      int32  `bson:"device"`
	StreamId    int32  `bson:"streamId"`
	Pvt         bool   `bson:"pvt"`
	Unit        string `bson:"unit"`
}

func (c *StreamGetMaxTangentialAccelerationRequest) GetInterfaceId() int32 {
	return c.InterfaceId
}

func (c *StreamGetMaxTangentialAccelerationRequest) GetDevice() int32 {
	return c.Device
}

func (c *StreamGetMaxTangentialAccelerationRequest) GetStreamId() int32 {
	return c.StreamId
}

func (c *StreamGetMaxTangentialAccelerationRequest) GetPvt() bool {
	return c.Pvt
}

func (c *StreamGetMaxTangentialAccelerationRequest) GetUnit() string {
	return c.Unit
}

func (c *StreamGetMaxTangentialAccelerationRequest) ToBytes() ([]byte, error) {
	if err := c.Sanitize(); err != nil {
		return nil, err
	}
	return bson.Marshal(c)
}

func (c *StreamGetMaxTangentialAccelerationRequest) FromBytes(b []byte) error {
	if err := bson.Unmarshal(b, c); err != nil {
		return err
	}
	return c.Sanitize()
}

func (c *StreamGetMaxTangentialAccelerationRequest) Clone() (*StreamGetMaxTangentialAccelerationRequest, error) {
	bytes, err := c.ToBytes()
	if err != nil {
		return nil, err
	}
	cloned := &StreamGetMaxTangentialAccelerationRequest{}
	if err := cloned.FromBytes(bytes); err != nil {
		return nil, err
	}
	return cloned, nil
}

// Checks for nil values on required properties.
// Initializes required nil slices to zero length.
func (c *StreamGetMaxTangentialAccelerationRequest) Sanitize() error {
	return nil
}
