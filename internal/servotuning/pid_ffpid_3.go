package servotuning

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

const pidFfpid3AsciiScaling = 1000000000.0

func (manager *servoTuningManager) pidFfpid3CalcGain(
	request paramsetTarget,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (float64, errors.SdkError) {
	posStdPerMotorUnit, err := getStepsPerMotorUnit(request, deviceInfo)
	if err != nil {
		return 0, err
	}

	gainAmps := 1 / (posStdPerMotorUnit * tuningInfo.Forque)

	return pidFfpid3AsciiScaling * gainAmps * servoAmpToMicroamp * servoGainScaling, nil
}

func (manager *servoTuningManager) pidFfpid3GetConversions() ffpidConversions {
	return ffpidConversions{
		pToKp:   pidFfpid3AsciiScaling / servoGainScaling,
		iToKi:   pidFfpid3AsciiScaling / servoGainScaling,
		dToKd:   pidFfpid3AsciiScaling / servoGainScaling,
		fcToFc1: pidFfpid3AsciiScaling,
	}
}

func (manager *servoTuningManager) pidFfpid3ReverseCalculation(
	request paramsetTarget,
	tuningInfo *dto.ServoTuningInfo,
) (*pb.AsciiPidTuning, errors.SdkError) {
	deviceRawParameters, err := manager.getTuning(request)
	if err != nil {
		return nil, err
	}

	conversions := manager.pidFfpid3GetConversions()

	kp, gotKp := getParam(deviceRawParameters.Params, "kp")
	if !gotKp {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for kp available on the device's current tuning.")
	}
	ki, gotKi := getParam(deviceRawParameters.Params, "ki")
	if !gotKi {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for ki available on the device's current tuning.")
	}
	kd, gotKd := getParam(deviceRawParameters.Params, "kd")
	if !gotKd {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for kd available on the device's current tuning.")
	}
	fc1, gotFc1 := getParam(deviceRawParameters.Params, "fc1")
	if !gotFc1 {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for fc1 available on the device's current tuning.")
	}

	return &pb.AsciiPidTuning{
		Type:    tuningInfo.Algorithm,
		Version: int32(tuningInfo.AlgorithmVersion),
		P:       kp / conversions.pToKp,
		I:       ki / conversions.iToKi,
		D:       kd / conversions.dToKd,
		Fc:      fc1 / conversions.fcToFc1,
	}, nil
}

func (manager *servoTuningManager) pidFfpid3SetTuningPid(
	request *pb.SetServoTuningPIDRequest,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (*pb.AsciiPidTuning, errors.SdkError) {
	conversions := manager.pidFfpid3GetConversions()

	gain, err := manager.pidFfpid3CalcGain(request, deviceInfo, tuningInfo)
	if err != nil {
		return nil, err
	}

	kd := request.D * conversions.dToKd
	ki := request.I * conversions.iToKi
	kp := request.P * conversions.pToKp

	fc := request.Fc * conversions.fcToFc1

	fcla := 0.0
	fclg := 0.0

	fcHsNonascii := request.Fc / pidFfpidFilterRatioHz
	if fc < pidFfpidHsFilterMinHz {
		fcHsNonascii = request.Fc
	} else if fcHsNonascii < pidFfpidHsFilterMinHz {
		fcHsNonascii = pidFfpidHsFilterMinHz
	}

	var tuning [17]*pb.AsciiServoTuningParam

	tuning[0] = &pb.AsciiServoTuningParam{Name: "gain", Value: gain}
	tuning[1] = &pb.AsciiServoTuningParam{Name: "finv1", Value: 0}
	tuning[2] = &pb.AsciiServoTuningParam{Name: "finv2", Value: 0}
	tuning[3] = &pb.AsciiServoTuningParam{Name: "kd", Value: kd}
	tuning[4] = &pb.AsciiServoTuningParam{Name: "ki", Value: ki}
	tuning[5] = &pb.AsciiServoTuningParam{Name: "kp", Value: kp}
	tuning[6] = &pb.AsciiServoTuningParam{Name: "fc1", Value: fc}
	tuning[7] = &pb.AsciiServoTuningParam{Name: "fc2", Value: fc}
	tuning[8] = &pb.AsciiServoTuningParam{Name: "fcla", Value: fcla}
	tuning[9] = &pb.AsciiServoTuningParam{Name: "fclg", Value: fclg}
	tuning[10] = &pb.AsciiServoTuningParam{Name: "kd.hs", Value: kd}
	tuning[11] = &pb.AsciiServoTuningParam{Name: "ki.hs", Value: ki}
	tuning[12] = &pb.AsciiServoTuningParam{Name: "kp.hs", Value: kp}
	tuning[13] = &pb.AsciiServoTuningParam{Name: "fc1.hs", Value: fcHsNonascii * pidFfpid3AsciiScaling}
	tuning[14] = &pb.AsciiServoTuningParam{Name: "fc2.hs", Value: fcHsNonascii * pidFfpid3AsciiScaling}
	tuning[15] = &pb.AsciiServoTuningParam{Name: "fcla.hs", Value: fcla}
	tuning[16] = &pb.AsciiServoTuningParam{Name: "fclg.hs", Value: fclg}

	setErr := manager.setTuning(&pb.SetServoTuningRequest{
		InterfaceId:             request.InterfaceId,
		Device:                  request.Device,
		Axis:                    request.Axis,
		Paramset:                request.Paramset,
		TuningParams:            tuning[:],
		SetUnspecifiedToDefault: true,
	})
	if setErr != nil {
		return nil, setErr
	}

	return manager.pidFfpid3ReverseCalculation(request, tuningInfo)
}
