package servotuning

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

const servoGainScaling = 1000.0
const servoAmpToMicroamp = 1000000.0

// This is always 10,000Hz for existing devices
const servoLoopFrequency = 10000.0

type paramsetTarget interface {
	ioc.AxisTarget
	GetParamset() pb.AsciiServoTuningParamset
}

func getParam(params []*pb.AsciiServoTuningParam, paramName string) (float64, bool) {
	for _, param := range params {
		if param.Name == paramName {
			return param.Value, true
		}
	}
	return 0, false
}

func addParam(params []*pb.AsciiServoTuningParam, paramName string, value float64) []*pb.AsciiServoTuningParam {
	paramCurrentIndex := -1
	for i, param := range params {
		if param.Name == paramName {
			paramCurrentIndex = i
			break
		}
	}

	if paramCurrentIndex == -1 {
		return append(params, &pb.AsciiServoTuningParam{Name: paramName, Value: value})
	} else {
		params[paramCurrentIndex].Value = value
		return params
	}
}

func createParamsetCommand(target paramsetTarget, commandCompletion string) (*c.Command, errors.SdkError) {
	paramset, err := getParamsetName(target)
	if err != nil {
		return nil, err
	}

	command := &c.Command{
		Device:  int(target.GetDevice()),
		Axis:    int(target.GetAxis()),
		Command: fmt.Sprintf("servo %s %s", paramset, commandCompletion),
	}

	return command, nil
}

func (manager *servoTuningManager) getParamNames(target paramsetTarget) ([]string, errors.SdkError) {
	requests, err := manager.deviceManager.GetRequests(target)
	if err != nil {
		return nil, err
	}

	command, err := createParamsetCommand(target, "get type")
	if err != nil {
		return nil, err
	}

	typeResponse, err := commands.SingleCommand(requests, *command)
	if err != nil {
		return nil, err
	}

	tuningType := typeResponse.Data
	paramNameResponses, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(target.GetDevice()),
		Axis:    int(target.GetAxis()),
		Command: fmt.Sprintf("servo print params %s", tuningType),
	})
	if err != nil {
		return nil, err
	}

	paramNames := make([]string, 0, len(paramNameResponses)-1)

	for _, resp := range paramNameResponses[1:] {
		tokens := strings.Split(resp.Data, " ")
		if len(tokens) != 2 && tokens[0] != tuningType {
			return nil, errors.ErrInvalidData(fmt.Sprintf("Unexpected response '%s' to print params", resp.Data))
		}
		paramNames = append(paramNames, tokens[1])
	}

	return paramNames, nil
}

func (manager *servoTuningManager) getInfo(request ioc.AxisTarget) (ioc.DeviceInfo, *dto.ServoTuningInfo, errors.SdkError) {
	deviceInfo, err := manager.deviceManager.GetIdentifiedDeviceInfo(request)
	if err != nil {
		return nil, nil, err
	}
	axisInfo, err := deviceInfo.GetAxisInfo(request)
	if err != nil {
		return nil, nil, err
	}
	var productID int
	productInfo := axisInfo.GetProduct()
	if productInfo == nil {
		// For integrated devices, get the product ID from the device itself
		productID = deviceInfo.GetProduct().ProductID
	} else {
		productID = productInfo.ProductID
	}

	tuningInfo, err := manager.deviceDb.GetServoTuningInfo(productID, deviceInfo.GetFirmwareVersion())
	if err != nil {
		return nil, nil, err
	}

	return deviceInfo, tuningInfo, nil
}
