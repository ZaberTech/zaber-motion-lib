package servotuning

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func (manager *servoTuningManager) pidFfpid4CalcGain(
	request paramsetTarget,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (float64, errors.SdkError) {
	posStdPerMotorUnit, err := getStepsPerMotorUnit(request, deviceInfo)
	if err != nil {
		return 0, err
	}

	gainAmps := 1 / (posStdPerMotorUnit * tuningInfo.Forque)

	return gainAmps * servoAmpToMicroamp * servoGainScaling, nil
}

func (manager *servoTuningManager) pidFfpid4GetConversions() ffpidConversions {
	return ffpidConversions{
		pToKp: 1 / servoGainScaling,
		iToKi: 1 / (2 * servoGainScaling * servoLoopFrequency),
		dToKd: (2 * servoLoopFrequency) / servoGainScaling,
	}
}

func (manager *servoTuningManager) pidFfpid4ReverseCalculation(
	request paramsetTarget,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (*pb.AsciiPidTuning, errors.SdkError) {
	deviceRawParameters, err := manager.getTuning(request)
	if err != nil {
		return nil, err
	}

	gain, err := manager.pidFfpid4CalcGain(request, deviceInfo, tuningInfo)
	if err != nil {
		return nil, err
	}

	conversions := manager.pidFfpid4GetConversions()

	kp, gotKp := getParam(deviceRawParameters.Params, "kp")
	if !gotKp {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for kp available on the device's current tuning.")
	}
	ki, gotKi := getParam(deviceRawParameters.Params, "ki")
	if !gotKi {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for ki available on the device's current tuning.")
	}
	kd, gotKd := getParam(deviceRawParameters.Params, "kd")
	if !gotKd {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for kd available on the device's current tuning.")
	}
	fc1, gotFc1 := getParam(deviceRawParameters.Params, "fc1")
	if !gotFc1 {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for fc1 available on the device's current tuning.")
	}

	prevGain, gotGain := getParam(deviceRawParameters.Params, "gain")
	if !gotGain {
		return nil, errors.ErrInternal("Cannot calculate device's PID values. No value for gain available on the device's current tuning.")
	}
	// This is the ratio between the gain as of now, and the gain when this device was last tuned
	// Use this to convert old parameters into their new representation when gain changes
	gainRatio := prevGain / gain

	return &pb.AsciiPidTuning{
		Type:    tuningInfo.Algorithm,
		Version: int32(tuningInfo.AlgorithmVersion),
		P:       kp * gainRatio / conversions.pToKp,
		I:       ki * gainRatio / conversions.iToKi,
		D:       kd * gainRatio / conversions.dToKd,
		Fc:      fc1,
	}, nil
}

func (manager *servoTuningManager) pidFfpid4SetTuningPid(
	request *pb.SetServoTuningPIDRequest,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (*pb.AsciiPidTuning, errors.SdkError) {
	gain, err := manager.pidFfpid4CalcGain(request, deviceInfo, tuningInfo)
	if err != nil {
		return nil, err
	}
	conversions := manager.pidFfpid4GetConversions()

	kd := request.D * conversions.dToKd
	ki := request.I * conversions.iToKi
	kp := request.P * conversions.pToKp

	fc := request.Fc

	fcla := 0.0
	fclg := 0.0

	fcHs := fc / pidFfpidFilterRatioHz
	if fc < pidFfpidHsFilterMinHz {
		fcHs = fc
	} else if fcHs < pidFfpidHsFilterMinHz {
		fcHs = pidFfpidHsFilterMinHz
	}

	var tuning [17]*pb.AsciiServoTuningParam

	tuning[0] = &pb.AsciiServoTuningParam{Name: "gain", Value: gain}
	tuning[1] = &pb.AsciiServoTuningParam{Name: "finv1", Value: 0}
	tuning[2] = &pb.AsciiServoTuningParam{Name: "finv2", Value: 0}
	tuning[3] = &pb.AsciiServoTuningParam{Name: "kd", Value: kd}
	tuning[4] = &pb.AsciiServoTuningParam{Name: "ki", Value: ki}
	tuning[5] = &pb.AsciiServoTuningParam{Name: "kp", Value: kp}
	tuning[6] = &pb.AsciiServoTuningParam{Name: "fc1", Value: fc}
	tuning[7] = &pb.AsciiServoTuningParam{Name: "fc2", Value: fc}
	tuning[8] = &pb.AsciiServoTuningParam{Name: "fcla", Value: fcla}
	tuning[9] = &pb.AsciiServoTuningParam{Name: "fclg", Value: fclg}
	tuning[10] = &pb.AsciiServoTuningParam{Name: "kd.hs", Value: kd}
	tuning[11] = &pb.AsciiServoTuningParam{Name: "ki.hs", Value: ki}
	tuning[12] = &pb.AsciiServoTuningParam{Name: "kp.hs", Value: kp}
	tuning[13] = &pb.AsciiServoTuningParam{Name: "fc1.hs", Value: fcHs}
	tuning[14] = &pb.AsciiServoTuningParam{Name: "fc2.hs", Value: fcHs}
	tuning[15] = &pb.AsciiServoTuningParam{Name: "fcla.hs", Value: fcla}
	tuning[16] = &pb.AsciiServoTuningParam{Name: "fclg.hs", Value: fclg}

	setErr := manager.setTuning(&pb.SetServoTuningRequest{
		InterfaceId:             request.InterfaceId,
		Device:                  request.Device,
		Axis:                    request.Axis,
		Paramset:                request.Paramset,
		TuningParams:            tuning[:],
		SetUnspecifiedToDefault: true,
	})
	if setErr != nil {
		return nil, setErr
	}

	return manager.pidFfpid4ReverseCalculation(request, deviceInfo, tuningInfo)
}
