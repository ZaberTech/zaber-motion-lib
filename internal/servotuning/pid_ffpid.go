package servotuning

const pidFfpidFilterRatioHz = 4.0
const pidFfpidHsFilterMinHz = 100.0

type ffpidConversions struct {
	pToKp   float64
	iToKi   float64
	dToKd   float64
	fcToFc1 float64
}
