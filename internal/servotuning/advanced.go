package servotuning

import (
	"fmt"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commands"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"

	"github.com/elliotchance/pie/v2"
)

func (manager *servoTuningManager) getTuning(request paramsetTarget) (*pb.AsciiParamsetInfo, errors.SdkError) {
	_, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return nil, err
	}

	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return nil, err
	}

	command, err := createParamsetCommand(request, "get all")
	if err != nil {
		return nil, err
	}

	responses, err := commands.SingleCommandMultiResponse(requests, *command)
	if err != nil {
		return nil, err
	}

	if responses[1].Data != "type ffpid" {
		return nil, errors.ErrNotSupported(fmt.Sprintf("No support for '%s' servo tuning. Please update ZML.", responses[1].Data))
	}

	paramsetInfo := pb.AsciiParamsetInfo{
		Type:    tuningInfo.Algorithm,
		Version: int32(tuningInfo.AlgorithmVersion),
		Params:  make([]*pb.AsciiServoTuningParam, 0, len(responses)-2),
	}

	for _, resp := range responses[2:] {
		tokens := strings.Split(resp.Data, " ")
		if len(tokens) != 3 || tokens[0] != "parameter" {
			return nil, errors.ErrInvalidData(fmt.Sprintf("Unexpected response '%s'", resp.Data))
		}
		if tokens[0] == "parameter" {
			value, err := strconv.ParseFloat(tokens[2], 64)
			if err != nil {
				return nil, errors.ErrInvalidData(fmt.Sprintf("Cannot parse parameter value of '%s'", resp.Data))
			}
			paramsetInfo.Params = append(paramsetInfo.Params, &pb.AsciiServoTuningParam{
				Name:  tokens[1],
				Value: value,
			})
		}
	}

	return &paramsetInfo, nil
}

func (manager *servoTuningManager) addDefaultsToParams(request paramsetTarget, params []*pb.AsciiServoTuningParam) ([]*pb.AsciiServoTuningParam, errors.SdkError) {
	defaultTunings, err := manager.getServoTuningDefaultParams(request)
	if err != nil {
		return nil, err
	}

	for _, defaultTuning := range defaultTunings {
		indexInExistingList := pie.FindFirstUsing(params, func(param *pb.AsciiServoTuningParam) bool {
			return param.Name == defaultTuning.Name
		})
		if indexInExistingList == -1 {
			params = append(params, defaultTuning)
		}
	}
	return params, nil
}

func (manager *servoTuningManager) setTuning(request *pb.SetServoTuningRequest) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	tuningParams := request.TuningParams
	if request.SetUnspecifiedToDefault {
		tuningParams, err = manager.addDefaultsToParams(request, tuningParams)
		if err != nil {
			return err
		}
	}

	for _, param := range tuningParams {
		command, err := createParamsetCommand(request, fmt.Sprintf("set %s %.9f", param.Name, param.Value))
		if err != nil {
			return err
		}
		_, err = commands.SingleCommand(requests, *command)
		if err != nil {
			return err
		}
	}

	return nil
}
