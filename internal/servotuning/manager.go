package servotuning

import (
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type servoTuningManager struct {
	gateway        ioc.GatewayManager
	deviceDb       ioc.DeviceDb
	deviceManager  ioc.DeviceManager
	storageManager ioc.StorageManager
}

func NewServoTuning(gateway ioc.GatewayManager, deviceDb ioc.DeviceDb, deviceManager ioc.DeviceManager, storage ioc.StorageManager) *servoTuningManager {
	instance := &servoTuningManager{
		gateway:        gateway,
		deviceDb:       deviceDb,
		deviceManager:  deviceManager,
		storageManager: storage,
	}

	instance.register()

	return instance
}

func (instance *servoTuningManager) register() {
	gateway := instance.gateway

	gateway.RegisterCallback("servotuning/get_raw", func() pb.Message {
		return &pb.ServoTuningRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getTuning(request.(*pb.ServoTuningRequest))
	})
	gateway.RegisterCallback("servotuning/set_raw", func() pb.Message {
		return &pb.SetServoTuningRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setTuning(request.(*pb.SetServoTuningRequest))
	})
	gateway.RegisterCallback("servotuning/get_pid", func() pb.Message {
		return &pb.ServoTuningRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getPIDTuning(request.(*pb.ServoTuningRequest))
	})
	gateway.RegisterCallback("servotuning/set_pid", func() pb.Message {
		return &pb.SetServoTuningPIDRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.setPIDTuning(request.(*pb.SetServoTuningPIDRequest))
	})
	gateway.RegisterCallback("servotuning/get_simple_params_definition", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getSimpleTuningParams(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("servotuning/set_simple_tuning", func() pb.Message {
		return &pb.SetSimpleTuning{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setSimpleTuningParams(request.(*pb.SetSimpleTuning))
	})
	gateway.RegisterCallback("servotuning/get_simple_tuning", func() pb.Message {
		return &pb.ServoTuningRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getSimpleTuning(request.(*pb.ServoTuningRequest))
	})
	gateway.RegisterCallback("servotuning/is_using_simple_tuning", func() pb.Message {
		return &pb.SetSimpleTuning{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.isUsingProvidedSimpleTuning(request.(*pb.SetSimpleTuning))
	})
	gateway.RegisterCallback("servotuning/get_startup_set", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.getStartupParamset(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("servotuning/set_startup_set", func() pb.Message {
		return &pb.ServoTuningRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.setStartupParamset(request.(*pb.ServoTuningRequest))
	})
	gateway.RegisterCallback("servotuning/load_paramset", func() pb.Message {
		return &pb.LoadParamset{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.loadParamset(request.(*pb.LoadParamset))
	})
}
