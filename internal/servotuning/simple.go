package servotuning

import (
	"fmt"
	"strings"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

type simpleTuningInterface interface {
	GetCarriageMass() *float64
	GetLoadMass() float64
	GetTuningParams() []*pb.AsciiServoTuningParam
}

func (manager *servoTuningManager) getSimpleTuningParams(request ioc.AxisTarget) (*pb.GetSimpleTuningParamDefinitionResponse, errors.SdkError) {
	_, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return nil, err
	}

	guidance := tuningInfo.Guidance[0]

	var params []*pb.AsciiSimpleTuningParamDefinition
	for _, guidanceParam := range guidance.UserParameters {
		param := &pb.AsciiSimpleTuningParamDefinition{
			Name:         guidanceParam.ParameterName,
			MinLabel:     guidanceParam.MinValueLabel,
			MaxLabel:     guidanceParam.MaxValueLabel,
			DataType:     guidanceParam.DataType,
			DefaultValue: guidanceParam.DefaultValue,
		}
		params = append(params, param)
	}

	return &pb.GetSimpleTuningParamDefinitionResponse{
		Params: params,
	}, nil
}

func (manager *servoTuningManager) storageCommandsSupported(request ioc.AxisTarget) (bool, errors.SdkError) {
	deviceInfo, err := manager.deviceManager.GetIdentifiedDeviceInfo(request)
	if err != nil {
		return false, err
	}

	cmd, err := deviceInfo.GetCommandIssuingInfo(&pb.AxisEmptyRequest{InterfaceId: request.GetInterfaceId(), Device: request.GetDevice()})
	if err != nil {
		return false, err
	}
	_, hasStorage := cmd.CommandTree.GetParamsInfo([]string{"storage", "print"})
	return hasStorage, nil
}

func (manager *servoTuningManager) setSimpleTuningParams(request *pb.SetSimpleTuning) errors.SdkError {
	request.CarriageMass = negativeToNull(request.CarriageMass) // backwards compatibility for -1.0 being default value before

	deviceInfo, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return err
	}

	if len(tuningInfo.Guidance) == 0 {
		return errors.ErrNotSupported("This device has no guidance data to use with the simple tuning method")
	}

	unsupportedGuidanceAlgorithms := make([]string, 0)
	for _, guidance := range tuningInfo.Guidance {
		if guidance.Algorithm == "servoPIDFFv1" && tuningInfo.AlgorithmVersion == 4 {
			err := manager.simple4Tune(request, guidance.UserParameters, deviceInfo, tuningInfo)
			if err != nil {
				return err
			}

			storageCommandsSupported, err := manager.storageCommandsSupported(request)
			if err == nil && storageCommandsSupported {
				err = manager.writeSimpleTuningToStorage(request, &pb.AsciiSimpleTuning{
					CarriageMass: request.CarriageMass,
					LoadMass:     request.LoadMass,
					TuningParams: request.TuningParams,
				})
			}
			return err
		} else {
			unsupportedGuidanceAlgorithms = append(unsupportedGuidanceAlgorithms, fmt.Sprintf("%s %d", guidance.Algorithm, tuningInfo.AlgorithmVersion))
		}
	}

	return errors.ErrNotSupported(fmt.Sprintf("Guidance data included only unsupported algorithms: %s", strings.Join(unsupportedGuidanceAlgorithms, ", ")))
}

func (manager *servoTuningManager) writeSimpleTuningToStorage(request paramsetTarget, tuning simpleTuningInterface) errors.SdkError {
	paramset := request.GetParamset()
	for _, param := range tuning.GetTuningParams() {
		err := manager.storageManager.SetNumber(request, simpleTuneParamKey(paramset, param.Name), param.Value)
		if err != nil {
			return err
		}
	}

	err := manager.storageManager.SetNumber(request, simpleTuneLoadMassKey(paramset), tuning.GetLoadMass())
	if err != nil {
		return err
	}

	carriageMass := tuning.GetCarriageMass()
	if carriageMass != nil {
		err = manager.storageManager.SetNumber(request, simpleTuneCarriageMassKey(paramset), *carriageMass)
		if err != nil {
			return err
		}
	} else {
		_, err = manager.storageManager.Erase(request, simpleTuneCarriageMassKey(paramset))
		if err != nil {
			return err
		}
	}

	return nil
}

func (manager *servoTuningManager) getSimpleTuning(request *pb.ServoTuningRequest) (*pb.AsciiSimpleTuning, errors.SdkError) {
	storageCommandsSupported, err := manager.storageCommandsSupported(request)
	if err != nil {
		return nil, err
	} else if !storageCommandsSupported {
		return nil, errors.ErrNotSupported("Simple tuning cannot be fetched from a device that does not support storage commands")
	}

	tuning, err := manager.getSimpleTuningParams(request)
	if err != nil {
		return nil, err
	}
	params := tuning.GetParams()

	loadMass, err := manager.storageManager.GetNumber(request, simpleTuneLoadMassKey(request.Paramset))
	if err != nil {
		if err.Type() == pb.Errors_NO_VALUE_FOR_KEY {
			return manager.defaultSimpleTuning(params)
		} else {
			return nil, err
		}
	}

	var carriageMass *float64
	if mass, err := manager.storageManager.GetNumber(request, simpleTuneCarriageMassKey(request.Paramset)); err != nil {
		if err.Type() != pb.Errors_NO_VALUE_FOR_KEY {
			return nil, err
		}
	} else {
		carriageMass = &mass
	}

	tuningParams := make([]*pb.AsciiServoTuningParam, len(params))
	for i, param := range params {
		currentValue, err := manager.storageManager.GetNumber(request, simpleTuneParamKey(request.Paramset, param.Name))
		if err != nil {
			return nil, err
		}
		tuningParams[i] = &pb.AsciiServoTuningParam{Name: param.Name, Value: currentValue}
	}

	isUsed, err := manager.isUsingProvidedSimpleTuning(&pb.SetSimpleTuning{
		InterfaceId:  request.InterfaceId,
		Device:       request.Device,
		Axis:         request.Axis,
		Paramset:     request.Paramset,
		LoadMass:     loadMass,
		CarriageMass: carriageMass,
		TuningParams: tuningParams,
	})
	if err != nil {
		return nil, err
	}

	return &pb.AsciiSimpleTuning{
		IsUsed:       isUsed.Value,
		LoadMass:     loadMass,
		CarriageMass: carriageMass,
		TuningParams: tuningParams,
	}, nil
}

const DefaultSliderPosition = 0.5

func (manager *servoTuningManager) defaultSimpleTuning(params []*pb.AsciiSimpleTuningParamDefinition) (*pb.AsciiSimpleTuning, errors.SdkError) {
	tuningParams := pie.Map(params, func(param *pb.AsciiSimpleTuningParamDefinition) *pb.AsciiServoTuningParam {
		var defaultValue float64
		if param.DefaultValue != nil {
			defaultValue = *param.DefaultValue
		} else {
			defaultValue = DefaultSliderPosition
		}
		return &pb.AsciiServoTuningParam{Name: param.Name, Value: defaultValue}
	})

	return &pb.AsciiSimpleTuning{
		IsUsed:       false,
		LoadMass:     0,
		CarriageMass: nil,
		TuningParams: tuningParams,
	}, nil
}

func (manager *servoTuningManager) isUsingProvidedSimpleTuning(request *pb.SetSimpleTuning) (*pb.BoolResponse, errors.SdkError) {
	deviceInfo, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return nil, err
	}

	if len(tuningInfo.Guidance) == 0 {
		return nil, errors.ErrNotSupported("This device has no guidance data to use with the simple tuning method")
	}

	unsupportedGuidanceAlgorithms := make([]string, 0)
	for _, guidance := range tuningInfo.Guidance {
		if guidance.Algorithm == "servoPIDFFv1" && tuningInfo.AlgorithmVersion == 4 {
			return manager.simple4Verify(request, guidance.UserParameters, deviceInfo, tuningInfo)
		} else {
			unsupportedGuidanceAlgorithms = append(unsupportedGuidanceAlgorithms, fmt.Sprintf("%s %d", guidance.Algorithm, tuningInfo.AlgorithmVersion))
		}
	}

	return nil, errors.ErrNotSupported(fmt.Sprintf("Guidance data included only unsupported algorithms: %s", strings.Join(unsupportedGuidanceAlgorithms, ", ")))
}

func negativeToNull(value *float64) *float64 {
	if value != nil && *value < 0.0 {
		return nil
	}
	return value
}
