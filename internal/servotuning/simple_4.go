package servotuning

import (
	"fmt"
	"math"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"gitlab.com/ZaberTech/zaber-device-db-service/pkg/dto"
)

func interpolateValue(tuningValue float64, interpolationValues []float64) float64 {
	length := len(interpolationValues)
	position := tuningValue * float64(length-1)
	whole := math.Floor(position)
	remainder := position - whole
	index := int(whole)

	if index == (length - 1) {
		return interpolationValues[index]
	}
	var low = interpolationValues[index]
	var high = interpolationValues[index+1]
	var result = low + (remainder * (high - low))
	return result
}

func getRawParamsOfSimpleParam(guidanceParam dto.ServoTuningSimpleParam, tuningParam *pb.AsciiServoTuningParam) ([]*pb.AsciiServoTuningParam, errors.SdkError) {
	if guidanceParam.DataType != "Interpolated" {
		return nil, errors.ErrNotSupported("No support for parameters of data type " + guidanceParam.DataType)
	}

	params := make([]*pb.AsciiServoTuningParam, 0)

	for paramName, interpolationValues := range guidanceParam.Values {
		if len(interpolationValues) < 1 {
			return nil, errors.ErrNotSupported("No interpolation values provided for parameter" + paramName)
		} else if len(interpolationValues) == 1 {
			params = append(params, &pb.AsciiServoTuningParam{Name: paramName, Value: interpolationValues[0]})
		} else {
			params = append(params, &pb.AsciiServoTuningParam{Name: paramName, Value: interpolateValue(tuningParam.Value, interpolationValues)})
		}
	}

	return params, nil
}

func getRawParamsOfSimpleParams(guidanceParams []dto.ServoTuningSimpleParam, tuningParams []*pb.AsciiServoTuningParam) ([]*pb.AsciiServoTuningParam, errors.SdkError) {
	// Run some sanity checks on the tuning params
	for _, tuningParam := range tuningParams {
		if tuningParam.Value < 0.0 || tuningParam.Value > 1.0 {
			return nil, errors.ErrInvalidArgument(fmt.Sprintf("Invalid value %f for passed parameter %s. These values must be between 0 and 1.", tuningParam.Value, tuningParam.Name))
		}
		hasGuidance := false
		for _, guidanceParam := range guidanceParams {
			if guidanceParam.ParameterName == tuningParam.Name {
				hasGuidance = true
				break
			}
		}
		if !hasGuidance {
			return nil, errors.ErrInvalidArgument("No tuning guidance for passed parameter: " + tuningParam.Name)
		}
	}

	params := make([]*pb.AsciiServoTuningParam, 0)

	for _, guidanceParam := range guidanceParams {
		var matchedTuningParam *pb.AsciiServoTuningParam
		for _, tuningParam := range tuningParams {
			if tuningParam.Name == guidanceParam.ParameterName {
				matchedTuningParam = tuningParam
				break
			}
		}
		if matchedTuningParam == nil {
			return nil, errors.ErrInvalidArgument("No tuning provided for " + guidanceParam.ParameterName)
		}

		guidanceGeneratedParams, err := getRawParamsOfSimpleParam(guidanceParam, matchedTuningParam)
		if err != nil {
			return nil, err
		}

		for _, param := range guidanceGeneratedParams {
			params = addParam(params, param.Name, param.Value)
		}
	}

	return params, nil
}

func simple4CalcGain(tuningInfo *dto.ServoTuningInfo, totalMass float64, stepsPerMetre float64) (float64, errors.SdkError) {
	if totalMass <= 0 {
		return 0, errors.ErrInvalidArgument("Total load inertia must be greater than zero.")
	}

	gainAmps := totalMass / (stepsPerMetre * tuningInfo.Forque)

	return gainAmps * servoAmpToMicroamp * servoGainScaling, nil
}

func simple4ShiftClosedLoopFeedForward(bandwidth float64, fcla float64, fclg float64) (float64, float64) {
	a := math.Exp((-2.0 * math.Pi * fcla) / servoLoopFrequency)
	b := (a - fclg) / (1.0 - fclg)

	newA := (-math.Log(a) * servoLoopFrequency) / (2.0 * math.Pi)

	newG := 0.0
	if (a >= 0.0) && (b >= 0.0) && (a < 1.0) && (a > b) && (bandwidth > 0.0) {
		a2 := math.Pow(a, bandwidth)
		b2 := math.Pow(b, bandwidth)
		newG = (a2 - b2) / (1.0 - b2)
		if newG < 0.0 {
			newG = 0.0
		}
	}

	return newA, newG
}

func simple4CalcFinv2(tuningInfo *dto.ServoTuningInfo, totalMass float64, stepsPerMetre float64, gain float64) float64 {
	feedforwardPrescaling := math.Pow(2, 14)

	ff := (totalMass * servoLoopFrequency * servoLoopFrequency) / (stepsPerMetre * tuningInfo.Forque * feedforwardPrescaling)

	return ff * servoAmpToMicroamp / gain
}

func (manager *servoTuningManager) calculateSimpleTuningParams(
	request *pb.SetSimpleTuning,
	guidanceParams []dto.ServoTuningSimpleParam,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) ([]*pb.AsciiServoTuningParam, errors.SdkError) {
	calculatedParams, err := getRawParamsOfSimpleParams(guidanceParams, request.TuningParams)
	if err != nil {
		return nil, err
	}

	expectedParamNames, err := manager.getParamNames(request)
	if err != nil {
		return nil, err
	}

	params := make([]*pb.AsciiServoTuningParam, 0)

	for _, name := range expectedParamNames {
		if value, exists := getParam(calculatedParams, name); exists {
			params = append(params, &pb.AsciiServoTuningParam{Name: name, Value: value})
		}
	}

	// tuningInfo.CarriageInertia is in grams, while this algorithm expects kilograms
	defaultCarriageMass := tuningInfo.CarriageInertia / 1000.0
	carriageMass := defaultCarriageMass
	if request.CarriageMass != nil {
		carriageMass = *request.CarriageMass
	}
	totalMass := carriageMass + request.LoadMass
	bandwidth := math.Sqrt(defaultCarriageMass / totalMass)

	stepsPerMotorUnit, err := getStepsPerMotorUnit(request, deviceInfo)
	if err != nil {
		return nil, err
	}

	gain, err := simple4CalcGain(tuningInfo, totalMass, stepsPerMotorUnit)
	if err != nil {
		return nil, err
	}

	finv2 := simple4CalcFinv2(tuningInfo, totalMass, stepsPerMotorUnit, gain)

	useFc1 := false
	useFc := false
	useFc2 := false
	useTf := false

	fc1 := 0.0
	fc2 := 0.0
	tf := 0.0
	fc1Hs := 0.0
	fc2Hs := 0.0
	tfHs := 0.0

	if fc1Calculated, exists := getParam(calculatedParams, "fc1"); exists {
		fc1 = fc1Calculated
		fc1Hs, _ = getParam(calculatedParams, "fc1.hs")
		useFc1 = true
	} else if fcCalculated, exists := getParam(calculatedParams, "fc"); exists {
		fc1 = fcCalculated
		fc1Hs, _ = getParam(calculatedParams, "fc.hs")
		useFc = true
	} else if tfCalculated, exists := getParam(calculatedParams, "tf"); exists {
		tf = tfCalculated
		tfHs, _ = getParam(calculatedParams, "tf.hs")
		useTf = true
	} else {
		return nil, errors.ErrNotSupported("This device's guidance did not include a value for fc1, fc, or tf")
	}

	if fc2Calculated, exists := getParam(calculatedParams, "fc2"); exists {
		fc2 = fc2Calculated
		fc2Hs, _ = getParam(calculatedParams, "fc2.hs")
		useFc2 = true
	}

	kdUnscaled, _ := getParam(calculatedParams, "kd")
	kiUnscaled, _ := getParam(calculatedParams, "ki")
	kpUnscaled, _ := getParam(calculatedParams, "kp")
	fclaNoShift, _ := getParam(calculatedParams, "fcla")
	fclgNoShift, _ := getParam(calculatedParams, "fclg")
	fcla, fclg := simple4ShiftClosedLoopFeedForward(bandwidth, fclaNoShift, fclgNoShift)
	kd := kdUnscaled / servoGainScaling
	ki := kiUnscaled / servoGainScaling
	kp := kpUnscaled / servoGainScaling

	kdHsUnscaled, _ := getParam(calculatedParams, "kd.hs")
	kiHsUnscaled, _ := getParam(calculatedParams, "ki.hs")
	kpHsUnscaled, _ := getParam(calculatedParams, "kp.hs")
	fclaHsNoShift, _ := getParam(calculatedParams, "fcla.hs")
	fclgHsNoShift, _ := getParam(calculatedParams, "fclg.hs")
	fclaHs, fclgHs := simple4ShiftClosedLoopFeedForward(bandwidth, fclaHsNoShift, fclgHsNoShift)
	kdHs := kdHsUnscaled / servoGainScaling
	kiHs := kiHsUnscaled / servoGainScaling
	kpHs := kpHsUnscaled / servoGainScaling

	params = addParam(params, "gain", gain)
	params = addParam(params, "finv2", finv2)
	params = addParam(params, "kd", kd*bandwidth)
	params = addParam(params, "ki", ki*bandwidth*bandwidth*bandwidth)
	params = addParam(params, "kp", kp*bandwidth*bandwidth)
	params = addParam(params, "fcla", fcla)
	params = addParam(params, "fclg", fclg)
	params = addParam(params, "kd.hs", kdHs*bandwidth)
	params = addParam(params, "ki.hs", kiHs*bandwidth*bandwidth*bandwidth)
	params = addParam(params, "kp.hs", kpHs*bandwidth*bandwidth)
	params = addParam(params, "fcla.hs", fclaHs)
	params = addParam(params, "fclg.hs", fclgHs)

	if useFc1 {
		params = addParam(params, "fc1", fc1*bandwidth)
		params = addParam(params, "fc1.hs", fc1Hs*bandwidth)
	} else if useFc {
		params = addParam(params, "fc", fc1*bandwidth)
		params = addParam(params, "fc.hs", fc1Hs*bandwidth)
	} else if useTf {
		params = addParam(params, "tf", tf)
		params = addParam(params, "tf.hs", tfHs)
	}

	if useFc2 {
		params = addParam(params, "fc2", fc2*bandwidth)
		params = addParam(params, "fc2.hs", fc2Hs*bandwidth)
	}

	return params, nil
}

func (manager *servoTuningManager) simple4Tune(
	request *pb.SetSimpleTuning,
	guidanceParams []dto.ServoTuningSimpleParam,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) errors.SdkError {
	params, err := manager.calculateSimpleTuningParams(request, guidanceParams, deviceInfo, tuningInfo)
	if err != nil {
		return err
	}

	setErr := manager.setTuning(&pb.SetServoTuningRequest{
		InterfaceId:             request.InterfaceId,
		Device:                  request.Device,
		Axis:                    request.Axis,
		Paramset:                request.Paramset,
		TuningParams:            params,
		SetUnspecifiedToDefault: true,
	})
	if setErr != nil {
		return setErr
	}

	return nil
}

func simpleTuneMatches(simpleParams []*pb.AsciiServoTuningParam, deviceParams *pb.AsciiParamsetInfo) bool {
	for _, simple := range simpleParams {
		for _, device := range deviceParams.Params {
			if simple.Name == device.Name {
				diff := simple.Value - device.Value
				diffFraction := math.Abs(diff)
				if simple.Value != 0 {
					diffFraction = math.Abs(diff / simple.Value)
				}
				// There will always be a small loss in precision, make sure it's no greater than one part in 100,000 of the total value
				if diffFraction > (1.0 / 100_000) {
					return false
				}
				break
			}
		}
	}
	return true
}

func (manager *servoTuningManager) simple4Verify(
	request *pb.SetSimpleTuning,
	guidanceParams []dto.ServoTuningSimpleParam,
	deviceInfo ioc.DeviceInfo,
	tuningInfo *dto.ServoTuningInfo,
) (*pb.BoolResponse, errors.SdkError) {
	simpleParams, err := manager.calculateSimpleTuningParams(request, guidanceParams, deviceInfo, tuningInfo)
	if err != nil {
		return nil, err
	}
	fullSimpleSet, err := manager.addDefaultsToParams(request, simpleParams)
	if err != nil {
		return nil, err
	}

	deviceTune, err := manager.getTuning(request)
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{Value: simpleTuneMatches(fullSimpleSet, deviceTune)}, nil
}
