package servotuning

import (
	"fmt"
	"math"
	"zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

// Return the steps per metre for linear devices and steps per degree for rotational devices.
func getStepsPerMotorUnit(request paramsetTarget, deviceInfo ioc.DeviceInfo) (float64, errors.SdkError) {
	cmdInfo, err := deviceInfo.GetCommandIssuingInfo(request)
	if err != nil {
		return 0, err
	}
	posInfo, posExists := cmdInfo.SettingsTable.GetParamInfo("pos")
	dimensionName, posDimensionExists := cmdInfo.ConversionTable.GetDimensionName(posInfo.ContextualDimensionID)
	if !posExists || !posDimensionExists {
		return 0, errors.ErrInvalidArgument("Cannot get tuning information for an immobile axis.")
	}
	if dimensionName == "Angle" {
		stepsPerDegree, err := cmdInfo.ConversionTable.Convert(1, "Angle:degrees", posInfo.ContextualDimensionID)
		if err != nil {
			return 0, err
		}
		return stepsPerDegree * 360 / (2 * math.Pi), nil
	} else if dimensionName == "Length" {
		stepsPerMetre, err := cmdInfo.ConversionTable.Convert(1, "Length:metres", posInfo.ContextualDimensionID)
		if err != nil {
			return 0, err
		}
		return stepsPerMetre, nil
	} else {
		return 0, errors.ErrInternal(fmt.Sprintf("Cannot get tuning information for an axes with position units %s.", dimensionName))
	}
}

func (manager *servoTuningManager) getServoTuningDefaultParams(target paramsetTarget) ([]*dto.AsciiServoTuningParam, errors.SdkError) {
	deviceInfo, err := manager.deviceManager.GetDeviceInfo(target)
	if err != nil {
		return nil, err
	}

	return deviceInfo.GetServoTuningDefaultParams(func() ([]*dto.AsciiServoTuningParam, errors.SdkError) {
		defaultTuning, err := manager.getTuning(&dto.ServoTuningRequest{
			InterfaceId: target.GetInterfaceId(),
			Device:      target.GetDevice(),
			Axis:        target.GetAxis(),
			Paramset:    dto.AsciiServoTuningParamset_DEFAULT,
		})
		if err != nil {
			return nil, err
		}

		return defaultTuning.Params, nil
	})
}
