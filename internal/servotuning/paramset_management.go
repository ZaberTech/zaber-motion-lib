package servotuning

import (
	"fmt"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *servoTuningManager) getStartupParamset(request *pb.AxisEmptyRequest) (*pb.ServoTuningParamsetResponse, errors.SdkError) {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return nil, err
	}

	resp, err := commands.SingleCommand(requests, c.Command{
		Device:  int(request.Device),
		Axis:    int(request.Axis),
		Command: "servo get startup",
	})
	if err != nil {
		return nil, err
	}

	startupParamset, err := getParamsetEnumFromName(resp.Data)
	if err != nil {
		return nil, err
	}

	return &pb.ServoTuningParamsetResponse{Paramset: startupParamset}, nil
}

func (manager *servoTuningManager) setStartupParamset(request *pb.ServoTuningRequest) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	paramset, err := getParamsetName(request)
	if err != nil {
		return err
	}

	_, err = commands.SingleCommand(requests, c.Command{
		Device:  int(request.Device),
		Axis:    int(request.Axis),
		Command: fmt.Sprintf("servo set startup %s", paramset),
	})

	return err
}

func (manager *servoTuningManager) loadParamset(request *pb.LoadParamset) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	fromParamset, err := getParamsetNameFromEnum(request.FromParamset)
	if err != nil {
		return err
	}
	toParamset, err := getParamsetNameFromEnum(request.ToParamset)
	if err != nil {
		return err
	}

	_, err = commands.SingleCommand(requests, c.Command{
		Device:  int(request.Device),
		Axis:    int(request.Axis),
		Command: fmt.Sprintf("servo %s load %s", toParamset, fromParamset),
	})
	if err != nil {
		return err
	}
	storageCommandsSupported, err := manager.storageCommandsSupported(request)
	if err != nil {
		return err
	} else if !storageCommandsSupported {
		return nil
	}

	simpleTuning, err := manager.getSimpleTuning(&pb.ServoTuningRequest{
		InterfaceId: request.InterfaceId,
		Device:      request.Device,
		Axis:        request.Axis,
		Paramset:    request.FromParamset,
	})

	if err == nil && simpleTuning.IsUsed {
		err = manager.writeSimpleTuningToStorage(&pb.ServoTuningRequest{
			InterfaceId: request.InterfaceId,
			Device:      request.Device,
			Axis:        request.Axis,
			Paramset:    request.ToParamset,
		}, simpleTuning)
	}

	return err
}
