package servotuning

import (
	"fmt"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func (manager *servoTuningManager) setPIDTuning(request *pb.SetServoTuningPIDRequest) (*pb.AsciiPidTuning, errors.SdkError) {
	deviceInfo, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return nil, err
	}

	if tuningInfo.Algorithm == "ffpid" {
		switch tuningInfo.AlgorithmVersion {
		case 4:
			return manager.pidFfpid4SetTuningPid(request, deviceInfo, tuningInfo)
		case 3:
			return manager.pidFfpid3SetTuningPid(request, deviceInfo, tuningInfo)
		}
	}

	return nil, errors.ErrNotSupported(fmt.Sprintf("ZML does not support tuning devices with the %s %d algorithm", tuningInfo.Algorithm, tuningInfo.AlgorithmVersion))
}

func (manager *servoTuningManager) getPIDTuning(request *pb.ServoTuningRequest) (*pb.AsciiPidTuning, errors.SdkError) {
	deviceInfo, tuningInfo, err := manager.getInfo(request)
	if err != nil {
		return nil, err
	}

	if tuningInfo.Algorithm == "ffpid" {
		switch tuningInfo.AlgorithmVersion {
		case 4:
			return manager.pidFfpid4ReverseCalculation(request, deviceInfo, tuningInfo)
		case 3:
			return manager.pidFfpid3ReverseCalculation(request, tuningInfo)
		}

	}

	return nil, errors.ErrNotSupported(fmt.Sprintf("ZML does not support tuning devices with the %s %d algorithm", tuningInfo.Algorithm, tuningInfo.AlgorithmVersion))
}
