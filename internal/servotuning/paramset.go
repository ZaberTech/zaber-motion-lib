package servotuning

import (
	"fmt"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

type paramsetMapItem struct {
	name string
	enum pb.AsciiServoTuningParamset
}

var nameToEnumMap = []paramsetMapItem{
	{name: "live", enum: pb.AsciiServoTuningParamset_LIVE},
	{name: "1", enum: pb.AsciiServoTuningParamset_P_1},
	{name: "2", enum: pb.AsciiServoTuningParamset_P_2},
	{name: "3", enum: pb.AsciiServoTuningParamset_P_3},
	{name: "4", enum: pb.AsciiServoTuningParamset_P_4},
	{name: "5", enum: pb.AsciiServoTuningParamset_P_5},
	{name: "6", enum: pb.AsciiServoTuningParamset_P_6},
	{name: "7", enum: pb.AsciiServoTuningParamset_P_7},
	{name: "8", enum: pb.AsciiServoTuningParamset_P_8},
	{name: "9", enum: pb.AsciiServoTuningParamset_P_9},
	{name: "staging", enum: pb.AsciiServoTuningParamset_STAGING},
	{name: "default", enum: pb.AsciiServoTuningParamset_DEFAULT},
}

func getParamsetName(target paramsetTarget) (string, errors.SdkError) {
	return getParamsetNameFromEnum(target.GetParamset())
}

func getParamsetNameFromEnum(paramset pb.AsciiServoTuningParamset) (string, errors.SdkError) {
	for _, item := range nameToEnumMap {
		if item.enum == paramset {
			return item.name, nil
		}
	}

	return "", errors.ErrInvalidArgument(fmt.Sprintf("No paramset is indexed by: %d", paramset))
}

func getParamsetEnumFromName(name string) (pb.AsciiServoTuningParamset, errors.SdkError) {
	for _, item := range nameToEnumMap {
		if item.name == name {
			return item.enum, nil
		}
	}

	return 0, errors.ErrInvalidArgument(fmt.Sprintf("No paramset is named: %s", name))
}
