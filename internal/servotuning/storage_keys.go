package servotuning

import (
	"fmt"

	pb "zaber-motion-lib/internal/dto"

	"github.com/iancoleman/strcase"
)

func simpleTuneParamKey(paramset pb.AsciiServoTuningParamset, name string) string {
	return fmt.Sprintf("zaber.servo-tune.%d.%s", paramset, strcase.ToKebab(name))
}

func simpleTuneLoadMassKey(paramset pb.AsciiServoTuningParamset) string {
	return fmt.Sprintf("zaber.servo-tune.%d.load-mass", paramset)
}

func simpleTuneCarriageMassKey(paramset pb.AsciiServoTuningParamset) string {
	return fmt.Sprintf("zaber.servo-tune.%d.car-mass", paramset)
}
