//go:build wasm

package utils

import (
	"fmt"
	"syscall/js"
)

func JsConsoleLog(msg string, params ...interface{}) {
	log := fmt.Sprintf(msg, params...)
	console := js.Global().Get("console")
	console.Call("log", log)
}
