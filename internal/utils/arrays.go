package utils

import "golang.org/x/exp/constraints"

func IsStringInSlice(itemToCheck string, list []string) bool {
	for _, item := range list {
		if item == itemToCheck {
			return true
		}
	}
	return false
}
func SliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

func CompareSlice[T comparable](a []T, b []T) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func IsSubsetIntSlice(subset []int, set []int) bool {
	for _, v := range subset {
		if SliceIndex(len(set), func(i int) bool { return set[i] == v }) < 0 {
			return false
		}
	}
	return true
}

func FilterEmpty(array []string) []string {
	filtered := make([]string, 0, len(array))
	for _, str := range array {
		if str != "" {
			filtered = append(filtered, str)
		}
	}
	return filtered
}

func Range[T constraints.Integer](from T, to T) []T {
	var result []T
	for i := from; i < to; i++ {
		result = append(result, i)
	}
	return result
}
