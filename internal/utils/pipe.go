package utils

import (
	"errors"
	"sync"
)

var ErrPipeClosed = errors.New("Pipe closed")
var ErrNilData = errors.New("Data must not be nil")

type PipeData = interface{}

type Pipe struct {
	data                     chan PipeData
	lock                     sync.Mutex
	readLock                 sync.Mutex
	writeLock                sync.Mutex
	closed                   chan bool
	closingErr               error
	writeFinished            chan bool
	blockWriteUntilReadAgain bool
	notFirstRead             bool
}

func NewPipe(blockWriteUntilReadAgain bool) *Pipe {
	return &Pipe{
		data:                     make(chan PipeData, 1),
		closed:                   make(chan bool),
		writeFinished:            make(chan bool, 1),
		blockWriteUntilReadAgain: blockWriteUntilReadAgain,
	}
}

func (instance *Pipe) IsClosed() bool {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	return instance.closingErr != nil
}

func (instance *Pipe) isClosedError() error {
	instance.lock.Lock()
	defer instance.lock.Unlock()

	return instance.closingErr
}

func (instance *Pipe) Read() (PipeData, error) {
	instance.readLock.Lock()
	defer instance.readLock.Unlock()

	if err := instance.isClosedError(); err != nil {
		return nil, err
	}

	if instance.blockWriteUntilReadAgain {
		if instance.notFirstRead {
			instance.writeFinished <- true
		} else {
			instance.notFirstRead = true
		}
	}

	select {
	case <-instance.closed:
		return nil, instance.isClosedError()
	case data := <-instance.data:
		return data, nil
	}
}

func (instance *Pipe) Write(data PipeData) error {
	if data == nil {
		return ErrNilData
	}

	instance.writeLock.Lock()
	defer instance.writeLock.Unlock()

	if err := instance.isClosedError(); err != nil {
		return err
	}

	select {
	case <-instance.closed:
		return instance.isClosedError()
	case instance.data <- data:
	}

	if instance.blockWriteUntilReadAgain {
		select {
		case <-instance.closed:
			return instance.isClosedError()
		case <-instance.writeFinished:
		}
	}

	return nil
}

func (instance *Pipe) Close(closingErr error) {
	if closingErr == nil {
		closingErr = ErrPipeClosed
	}

	instance.lock.Lock()
	wasClosed := instance.closingErr != nil
	if instance.closingErr == nil {
		instance.closingErr = closingErr
	}
	instance.lock.Unlock()

	if wasClosed {
		return
	}

	close(instance.closed)

	instance.writeLock.Lock()
	//nolint:staticcheck
	instance.writeLock.Unlock()

	instance.readLock.Lock()
	//nolint:staticcheck
	instance.readLock.Unlock()

	close(instance.data)
	close(instance.writeFinished)
}
