package utils

import (
	"fmt"
	"strconv"
	"zaber-motion-lib/internal/errors"
)

func ParseInt32(s string) (int32, errors.SdkError) {
	num, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf("Cannot parse int32: %s", s))
	}
	return int32(num), nil
}

func ParseInt64(s string) (int64, errors.SdkError) {
	num, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf("Cannot parse int64: %s", s))
	}
	return num, nil
}

func ParseFloat64(s string) (float64, errors.SdkError) {
	num, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf("Cannot parse float64: %s", s))
	}
	return num, nil
}
