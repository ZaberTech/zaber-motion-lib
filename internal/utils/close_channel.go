package utils

import "sync"

type CloseChannel struct {
	closed        bool
	closedChannel chan error
	lock          sync.Mutex
}

func NewCloseChannel() *CloseChannel {
	return &CloseChannel{
		closedChannel: make(chan error, 1),
	}
}

func (ctl *CloseChannel) Channel() chan error {
	return ctl.closedChannel
}

func (ctl *CloseChannel) IsClosed() bool {
	ctl.lock.Lock()
	defer ctl.lock.Unlock()
	return ctl.closed
}

func (ctl *CloseChannel) CloseWithErr(err error) {
	ctl.lock.Lock()
	wasClosed := ctl.closed
	ctl.closed = true
	ctl.lock.Unlock()

	if wasClosed {
		return
	}

	if err != nil {
		ctl.closedChannel <- err
	}

	close(ctl.closedChannel)
}

func (ctl *CloseChannel) Close() {
	ctl.CloseWithErr(nil)
}
