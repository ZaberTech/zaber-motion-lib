package utils

import (
	"fmt"
	"math"
	"sync"
	"sync/atomic"
)

type TokenMap[TEntry any] struct {
	_map     sync.Map
	latestID int32
}

func NewTokenMap[TEntry any]() *TokenMap[TEntry] {
	return &TokenMap[TEntry]{}
}

func (tokenMap *TokenMap[TEntry]) Store(item TEntry) int32 {
	var id int32
	for {
		id = atomic.AddInt32(&tokenMap.latestID, 1)
		if id <= 0 {
			if id == math.MinInt32 {
				atomic.StoreInt32(&tokenMap.latestID, 0)
			}
			continue
		}

		if _, isUsed := tokenMap._map.Load(id); !isUsed {
			break
		}
	}

	tokenMap._map.Store(id, item)

	return id
}

func (tokenMap *TokenMap[TEntry]) Load(id int32) (TEntry, error) {
	itemAny, ok := tokenMap._map.Load(id)
	if !ok {
		var nothing TEntry
		return nothing, fmt.Errorf("cannot find resource with id: %d", id)
	}
	return itemAny.(TEntry), nil
}

func (tokenMap *TokenMap[TEntry]) Delete(id int32) (TEntry, bool) {
	// This method should return if key was deleted but sync.Map does not support it.
	itemAny, ok := tokenMap._map.LoadAndDelete(id)
	if !ok {
		var nothing TEntry
		return nothing, false
	}
	return itemAny.(TEntry), true
}
