package utils

import "math"

func RoundNumber(number float64, decimalPlaces int) float64 {
	return math.Round(number*math.Pow10(decimalPlaces)) / math.Pow10(decimalPlaces)
}

func MaxInt(n1 int, n2 int) int {
	if n1 > n2 {
		return n1
	} else {
		return n2
	}
}

func MinInt(n1 int, n2 int) int {
	if n1 < n2 {
		return n1
	} else {
		return n2
	}
}

func MinFloatArray(array []float64) float64 {
	val := math.Inf(1)
	for _, item := range array {
		if item < val {
			val = item
		}
	}
	return val
}
