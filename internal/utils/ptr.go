package utils

import "golang.org/x/exp/constraints"

func ToPointer[T any](v T) *T {
	return &v
}

func CastEnumPointer[TFrom constraints.Integer, TTo constraints.Integer](ptr *TFrom) *TTo {
	if ptr == nil {
		return nil
	}
	value := (TTo)(*ptr)
	return &value
}

func EnumDefault[T constraints.Integer](ptr *T, defaultValue T) T {
	if ptr == nil {
		return defaultValue
	}
	return *ptr
}

func NumberDefault[T constraints.Ordered](ptr *T, defaultValue T) T {
	if ptr == nil {
		return defaultValue
	}
	return *ptr
}

func PtrDefault[T any](ptr *T, defaultValue T) T {
	if ptr == nil {
		return defaultValue
	}
	return *ptr
}
