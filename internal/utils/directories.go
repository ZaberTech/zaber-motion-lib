package utils

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"zaber-motion-lib/internal/errors"
)

func GetLibraryDataDir() (errors.SdkError, string) {
	if runtime.GOOS == "windows" {
		appData := os.Getenv("LOCALAPPDATA")
		if appData != "" {
			return nil, filepath.Join(appData, "Zaber Technologies", "Zaber Motion Library")
		}
	}

	baseDir := os.Getenv("XDG_CACHE_HOME")
	xdgCacheValid := baseDir != "" && filepath.IsAbs(baseDir)
	if !xdgCacheValid {
		home, err := os.UserHomeDir()
		if err != nil {
			return errors.ErrDeviceDbFailed(fmt.Sprintf("Cannot obtain user home directory: %s", err.Error()), "no-access"), ""
		}

		if runtime.GOOS == "darwin" {
			baseDir = path.Join(home, "Library", "Caches")
		} else {
			baseDir = path.Join(home, ".cache")
		}
	}

	return nil, path.Join(baseDir, "zaber-motion-lib")
}
