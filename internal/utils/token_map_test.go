package utils

import (
	"testing"

	"gotest.tools/assert"
)

func TestTokenMap_StoresAndLoadsItem(t *testing.T) {
	tokenMap := NewTokenMap[string]()

	id := tokenMap.Store("item1")
	item1, err := tokenMap.Load(id)

	if item1 != "item1" || err != nil {
		t.Error(item1, "!= item1 ||", err, "!= nil", "Failed to load item")
	}
}

func TestTokenMap_ReturnsErrorOnNonExisting(t *testing.T) {
	tokenMap := NewTokenMap[string]()

	_, err := tokenMap.Load(1)
	if err == nil {
		t.Error("err is nil")
	}
}

func TestTokenMap_AssignsDistinctId(t *testing.T) {
	tokenMap := NewTokenMap[string]()

	id := tokenMap.Store("item1")
	id2 := tokenMap.Store("item2")

	if id == id2 {
		t.Error(id, "==", id2, "Ids are not distinct")
	}
}

func TestTokenMap_DeletesItem(t *testing.T) {
	tokenMap := NewTokenMap[string]()

	id := tokenMap.Store("item1")

	stored, deleted := tokenMap.Delete(id)
	assert.Equal(t, stored, "item1")
	assert.Equal(t, deleted, true)

	_, err := tokenMap.Load(id)
	if err == nil {
		t.Error(err, "== nil", "Load succeeded")
	}

	stored, deleted = tokenMap.Delete(id)
	assert.Equal(t, stored, "")
	assert.Equal(t, deleted, false)
}
