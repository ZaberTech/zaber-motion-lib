package utils

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

func TestPipe_RoutesData(t *testing.T) {
	pipe := NewPipe(false)

	err := pipe.Write("bla")
	if err != nil {
		t.Fatal("Error", err)
	}

	data, err := pipe.Read()
	if data != "bla" || err != nil {
		t.Fatal(data, "!= bla ||", err, "!= nil")
	}
}

func TestPipe_ClosingPipeReturnsErrorFromRead(t *testing.T) {
	pipe := NewPipe(false)

	errChannel := make(chan error, 1)
	go func() {
		_, err := pipe.Read()
		errChannel <- err
	}()

	errorClosed := errors.New("Bad error")
	pipe.Close(errorClosed)

	errorReturned := <-errChannel
	if errorReturned != errorClosed {
		t.Fatal("The returned error does not match the one provided in close")
	}
}

func TestPipe_ClosingPipeReturnsErrorFromWrite(t *testing.T) {
	pipe := NewPipe(false)

	errChannel := make(chan error, 1)
	go func() {
		for {
			err := pipe.Write("test")
			if err != nil {
				errChannel <- err
				return
			}
		}
	}()

	errorClosed := errors.New("Bad error")
	pipe.Close(errorClosed)

	errorReturned := <-errChannel
	if errorReturned != errorClosed {
		t.Fatal("The returned error does not match the one provided in close")
	}
}

func TestPipe_ClosingTwiceDoesNotPanic(_ *testing.T) {
	pipe := NewPipe(false)

	doneChannel := make(chan bool, 2)

	go func() {
		for {
			err := pipe.Write("test")
			if err != nil {
				if err != ErrPipeClosed {
					panic(err)
				}
				break
			}
		}
		doneChannel <- true
	}()

	go func() {
		for {
			_, err := pipe.Read()
			if err != nil {
				if err != ErrPipeClosed {
					panic(err)
				}
				break
			}
		}
		doneChannel <- true
	}()

	pipe.Close(nil)
	pipe.Close(nil)

	<-doneChannel
	<-doneChannel
}

func TestPipe_WriteIsBlockedUntilSubsequentReadIsCalled(t *testing.T) {
	pipe := NewPipe(true)

	doneChannel := make(chan bool, 1)
	writtenChannel := make(chan int, 10)
	readPokeChannel := make(chan bool, 1)

	go func() {
		for i := 1; true; i++ {
			err := pipe.Write(fmt.Sprintf("test%d", i))
			if err != nil {
				if err != ErrPipeClosed {
					panic(err)
				}
				break
			}
			writtenChannel <- i
		}
		doneChannel <- true
	}()

	go func() {
		for {
			<-readPokeChannel
			_, err := pipe.Read()
			if err != nil {
				if err != ErrPipeClosed {
					panic(err)
				}
				break
			}
		}
		doneChannel <- true
	}()

	readPokeChannel <- true

	for i := 1; i <= 3; i++ {
		select {
		case <-writtenChannel:
			t.Fatal("Write was not blocked")
		case <-time.After(100 * time.Millisecond):
		}

		readPokeChannel <- true

		written := <-writtenChannel
		if written != i {
			t.Fatal("Wrong data returned", written)
		}
	}

	close(readPokeChannel)
	pipe.Close(nil)
	<-doneChannel
}

func TestPipe_BlockedWriteReturnsClosedError(t *testing.T) {
	pipe := NewPipe(true)

	errChannel := make(chan error, 1)

	go func() {
		err := pipe.Write("test")
		errChannel <- err
	}()

	data, err := pipe.Read()
	if data != "test" || err != nil {
		t.Fatal(data, "!= test ||", err, "!= nil")
	}

	errorClosed := errors.New("Bad error")
	pipe.Close(errorClosed)

	errorReturned := <-errChannel
	if errorReturned != errorClosed {
		t.Fatal("The returned error does not match the one provided in close")
	}
}
