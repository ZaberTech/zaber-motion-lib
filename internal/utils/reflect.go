package utils

import "reflect"

func GetExportedFields(value interface{}) []reflect.StructField {
	reflectType := reflect.TypeOf(value)
	if reflectType.Kind() == reflect.Pointer {
		reflectType = reflectType.Elem()
	}
	fields := reflect.VisibleFields(reflectType)
	var exportedFields []reflect.StructField
	for _, field := range fields {
		if field.IsExported() {
			exportedFields = append(exportedFields, field)
		}
	}
	return exportedFields
}
