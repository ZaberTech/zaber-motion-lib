package utils

import (
	"zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

func IoPortTypeToFirmwarePort(portType dto.AsciiIoPortType) (string, errors.SdkError) {
	switch portType {
	case dto.AsciiIoPortType_ANALOG_INPUT:
		return "ai", nil
	case dto.AsciiIoPortType_ANALOG_OUTPUT:
		return "ao", nil
	case dto.AsciiIoPortType_DIGITAL_INPUT:
		return "di", nil
	case dto.AsciiIoPortType_DIGITAL_OUTPUT:
		return "do", nil
	default:
		return "", errors.ErrInvalidArgument("Port type must be specified.")
	}
}

func FirmwarePortToIoPortType(portType string) (dto.AsciiIoPortType, errors.SdkError) {
	switch portType {
	case "ai":
		return dto.AsciiIoPortType_ANALOG_INPUT, nil
	case "ao":
		return dto.AsciiIoPortType_ANALOG_OUTPUT, nil
	case "di":
		return dto.AsciiIoPortType_DIGITAL_INPUT, nil
	case "do":
		return dto.AsciiIoPortType_DIGITAL_OUTPUT, nil
	default:
		return dto.AsciiIoPortType_NONE, errors.ErrInvalidArgument("Invalid port type string.")
	}
}

func IsEmptyFirmwareVersion(version *dto.FirmwareVersion) bool {
	return version == nil || (version.Major == 0 && version.Minor == 0 && version.Build == 0)
}
