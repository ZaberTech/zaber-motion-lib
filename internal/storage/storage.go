package storage

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

type storageManager struct {
	deviceManager ioc.DeviceManager
	gateway       ioc.GatewayManager
}

func NewStorageManager(gateway ioc.GatewayManager, deviceManager ioc.DeviceManager) ioc.StorageManager {
	storage := &storageManager{gateway: gateway, deviceManager: deviceManager}
	deviceManager.SetStorageManager(storage)
	storage.register()
	return storage
}

func (manager *storageManager) register() {
	gateway := manager.gateway
	gateway.RegisterCallback("device/set_storage", func() pb.Message {
		return &pb.DeviceSetStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setStorageRequest(request.(*pb.DeviceSetStorageRequest))
	})
	gateway.RegisterCallback("device/get_storage", func() pb.Message {
		return &pb.DeviceGetStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getStorageRequest(request.(*pb.DeviceGetStorageRequest))
	})
	gateway.RegisterCallback("device/set_storage_number", func() pb.Message {
		return &pb.DeviceSetStorageNumberRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setStorageNumber(request.(*pb.DeviceSetStorageNumberRequest))
	})
	gateway.RegisterCallback("device/get_storage_number", func() pb.Message {
		return &pb.DeviceStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getStorageNumber(request.(*pb.DeviceStorageRequest))
	})
	gateway.RegisterCallback("device/set_storage_bool", func() pb.Message {
		return &pb.DeviceSetStorageBoolRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setStorageBool(request.(*pb.DeviceSetStorageBoolRequest))
	})
	gateway.RegisterCallback("device/get_storage_bool", func() pb.Message {
		return &pb.DeviceStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getStorageBool(request.(*pb.DeviceStorageRequest))
	})
	gateway.RegisterCallback("device/erase_storage", func() pb.Message {
		return &pb.DeviceStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.eraseStorage(request.(*pb.DeviceStorageRequest))
	})
	gateway.RegisterCallback("device/storage_list_keys", func() pb.Message {
		return &pb.DeviceStorageListKeysRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.listKeys(request.(*pb.DeviceStorageListKeysRequest))
	})
	gateway.RegisterCallback("device/storage_key_exists", func() pb.Message {
		return &pb.DeviceStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.keyExists(request.(*pb.DeviceStorageRequest))
	})
	gateway.RegisterCallback("device/set_label", func() pb.Message {
		return &pb.DeviceSetStorageRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.setLabel(request.(*pb.DeviceSetStorageRequest))
	})
	gateway.RegisterCallback("device/get_label", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getLabel(request.(*pb.AxisEmptyRequest))
	})
}

// Returns the longest word that can be sent to this device
// This function has the side effect of fetching the device protocol info if this is not already cached
func (manager *storageManager) getMaxWordLen(request ioc.AxisTarget) (int, errors.SdkError) {
	limits, packetSize, err := manager.deviceManager.GetProtocolInfo(request)
	if err != nil {
		return 0, err
	}
	maxWordLen := limits.MaxWordLen
	if limits.MaxPackets <= 1 {
		// For devices without line continuations
		// make sure it will pack alongside the longest possible storage command
		maxWordLen = packetSize - len(`/DD AA ID storage axis append \:CCff`)
	}

	return maxWordLen, nil
}

func makeStoreCommand(request ioc.AxisTarget, isAppend bool, key, value string) c.Command {
	builder := strings.Builder{}

	builder.WriteString("storage ")

	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	}

	if isAppend {
		builder.WriteString("append ")
	} else {
		builder.WriteString("set ")
	}

	builder.WriteString(key)
	builder.WriteString(" ")
	builder.WriteString(value)

	return c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: builder.String(),
	}
}

// Writes as much of the value as possible and returns any part that could not be returned
func writePartialValue(requests *c.RequestManager, target ioc.AxisTarget, isAppend bool, key, value string) (string, errors.SdkError) {
	_, err := commands.SingleCommand(requests, makeStoreCommand(target, isAppend, key, value))
	if err == nil {
		return "", nil
	} else if commands.IsBadCommandErr(err) {
		return "", errors.ErrNotSupported("This device does not support key-value storage")
	} else if err.Type() != pb.Errors_COMMAND_TOO_LONG {
		return "", err
	}
	tooLongData := err.CustomData().(*pb.ExceptionsCommandTooLongExceptionData)

	_, err = commands.SingleCommand(requests, c.Command{
		Device:  int(target.GetDevice()),
		Axis:    int(target.GetAxis()),
		Command: tooLongData.Fit,
	})
	if err != nil {
		return "", err
	}
	if len(tooLongData.Remainder) >= len(value) {
		msg := fmt.Sprintf("Cannot write value '%s' in %d packets of length %d", value, tooLongData.PacketsMax, tooLongData.PacketSize)
		return "", errors.ErrInvalidArgument(msg)
	}
	return tooLongData.Remainder, nil
}

func (manager *storageManager) setStorageRaw(request ioc.AxisTarget, key, value string) errors.SdkError {
	if value == "" {
		return errors.ErrInvalidArgument("Empty values are not supported.")
	}

	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	remainder, err := writePartialValue(requests, request, false, key, value)
	if err != nil {
		return err
	}

	for remainder != "" {
		remainder, err = writePartialValue(requests, request, true, key, remainder)
		if err != nil {
			return err
		}
	}

	return nil
}

func (manager *storageManager) setStorageEncoded(request ioc.AxisTarget, key string, unencodedValue string) errors.SdkError {
	maxWordLen, err := manager.getMaxWordLen(request)
	if err != nil {
		return err
	}
	encodedValue := base64.URLEncoding.EncodeToString([]byte(unencodedValue))
	var lines []string
	for len(encodedValue) > 0 {
		cutIndex := len(encodedValue)
		if cutIndex > maxWordLen {
			cutIndex = maxWordLen
		}
		lines = append(lines, encodedValue[:cutIndex])
		encodedValue = encodedValue[cutIndex:]
	}

	return manager.setStorageRaw(request, key, strings.Join(lines, " "))
}

func (manager *storageManager) SetStorage(request ioc.AxisTarget, key string, value string, encoded bool) errors.SdkError {
	if encoded {
		return manager.setStorageEncoded(request, key, value)
	} else {
		return manager.setStorageRaw(request, key, value)
	}
}

func (manager *storageManager) getStoredValue(request ioc.AxisTarget, key string) (string, errors.SdkError) {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return "", err
	}

	getCmd := "storage get"
	if request.GetAxis() != 0 {
		getCmd = "storage axis get"
	}
	valueResp, err := commands.SingleCommand(requests, c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: fmt.Sprintf("%s %s", getCmd, key),
	})
	if err != nil {
		if commands.IsBadCommandErr(err) {
			return "", errors.ErrNotSupported("This device does not support key-value storage")
		} else if commands.IsBadDataErr(err) {
			return "", errors.ErrNoValueForKey(key)
		}
		return "", err
	}
	return valueResp.Data, nil
}

func (manager *storageManager) decode(encodedValue string) (string, errors.SdkError) {
	decodedValue, err := base64.URLEncoding.DecodeString(encodedValue)
	if err != nil {
		return "", errors.ErrInvalidData("Could not decode the value. This value is probably not encoded.")
	}
	return string(decodedValue), nil
}

func (manager *storageManager) GetStorage(request ioc.AxisTarget, key string, encoded bool) (string, errors.SdkError) {
	storedValue, err := manager.getStoredValue(request, key)
	if err != nil {
		return "", err
	} else if !encoded {
		return storedValue, nil
	}

	decodedValue, err := manager.decode(strings.Replace(storedValue, " ", "", -1))
	if err != nil {
		return "", errors.ErrInvalidData(fmt.Sprintf("Could not decode the value at %s. This value is probably not encoded.", key))
	}
	return decodedValue, nil
}

func (manager *storageManager) SetNumber(request ioc.AxisTarget, key string, value float64) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	maxWordLen, err := manager.getMaxWordLen(request)
	if err != nil {
		return err
	}

	intValue := int64(value)
	isInt := value == float64(intValue)

	var setValue string
	if isInt {
		setValue = strconv.FormatInt(intValue, 10)
		if len(setValue) > maxWordLen {
			return errors.ErrInvalidArgument("This int is too long to store.")
		}
	} else {
		// 7 is taken off the allowed setLen to handle:
		// * A possible minus sign (1 rune)
		// * The "." (1 rune)
		// * The exponent "e±ddd" (5 runes since floats will have at most 3 digits in the exponent)
		setValue = strconv.FormatFloat(value, 'g', maxWordLen-7, 64)
	}

	_, err = requests.Request(makeStoreCommand(request, false, key, setValue), nil)
	return err
}

func (manager *storageManager) setStorageNumber(request *pb.DeviceSetStorageNumberRequest) errors.SdkError {
	return manager.SetNumber(request, request.Key, request.Value)
}

func (manager *storageManager) GetNumber(request ioc.AxisTarget, key string) (float64, errors.SdkError) {
	storedValue, err := manager.getStoredValue(request, key)
	if err != nil {
		return 0, err
	}

	value, parseErr := strconv.ParseFloat(storedValue, 64)
	if parseErr != nil {
		return 0, errors.ErrInvalidData(fmt.Sprintf("Could not parse %s to a numeric value", storedValue))
	}

	return value, nil
}

func (manager *storageManager) getStorageNumber(request *pb.DeviceStorageRequest) (*pb.DoubleResponse, errors.SdkError) {
	value, err := manager.GetNumber(request, request.Key)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: value,
	}, nil
}

func (manager *storageManager) setStorageBool(request *pb.DeviceSetStorageBoolRequest) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	var value string
	if request.Value {
		value = "1"
	} else {
		value = "0"
	}

	_, err = requests.Request(makeStoreCommand(request, false, request.Key, value), nil)
	return err
}

func (manager *storageManager) getStorageBool(request *pb.DeviceStorageRequest) (*pb.BoolResponse, errors.SdkError) {
	value, err := manager.GetNumber(request, request.Key)
	if err != nil {
		return nil, err
	}

	if value == 0 {
		return &pb.BoolResponse{Value: false}, nil
	} else {
		return &pb.BoolResponse{Value: true}, nil
	}
}

func (manager *storageManager) Erase(request ioc.AxisTarget, key string) (bool, errors.SdkError) {
	builder := strings.Builder{}
	builder.WriteString("storage ")

	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	}

	builder.WriteString("erase ")
	builder.WriteString(key)

	_, err := commands.SingleRequestAxis(manager.deviceManager, request, builder.String())
	if err != nil && !commands.IsBadDataErr(err) {
		return false, err
	}

	return err == nil, nil
}

func (manager *storageManager) eraseStorage(request *pb.DeviceStorageRequest) (*pb.BoolResponse, errors.SdkError) {
	erased, err := manager.Erase(request, request.Key)
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{Value: erased}, nil
}

func (manager *storageManager) GetStoredStructure(target ioc.AxisTarget, key string, dataStruct interface{}) (bool, errors.SdkError) {
	value, err := manager.GetStorage(target, key, true)
	if err != nil {
		if err.Type() == pb.Errors_NO_VALUE_FOR_KEY || err.Type() == pb.Errors_NOT_SUPPORTED {
			return false, nil
		}
		return false, err
	}

	if err := json.NewDecoder(strings.NewReader(value)).Decode(dataStruct); err != nil {
		return false, errors.ErrInvalidData("Cannot decode stored value: " + err.Error())
	}
	return true, nil
}

func (manager *storageManager) SetStorageStructure(target ioc.AxisTarget, key string, dataStruct interface{}) errors.SdkError {
	var writer strings.Builder
	err := json.NewEncoder(&writer).Encode(dataStruct)
	if err != nil {
		return errors.ErrInvalidData("Cannot encode stored value: " + err.Error())
	}

	return manager.SetStorage(target, key, writer.String(), true)
}

func (manager *storageManager) listKeys(request *pb.DeviceStorageListKeysRequest) (*pb.StringArrayResponse, errors.SdkError) {
	builder := strings.Builder{}
	builder.WriteString("storage ")

	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	}

	builder.WriteString("print keys ")
	if utils.PtrDefault(request.Prefix, "") != "" {
		builder.WriteString("prefix ")
		builder.WriteString(*request.Prefix)
	}

	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return nil, err
	}
	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Command: builder.String(),
		Device:  int(request.Device),
		Axis:    int(request.Axis),
	})
	if err != nil {
		return nil, err
	}

	response := &pb.StringArrayResponse{}
	for _, reply := range replies[1:] {
		parts := strings.Fields(reply.Data)
		if parts[0] != "set" {
			return nil, errors.ErrInvalidData("Invalid reply data: " + reply.Data)
		}
		response.Values = append(response.Values, parts[1])
	}
	return response, nil
}

func (manager *storageManager) ListKeyValuePairs(request ioc.AxisTarget, prefix string, encoded bool) (map[string]string, errors.SdkError) {
	builder := strings.Builder{}
	builder.WriteString("storage ")

	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	}

	builder.WriteString("print ")
	if prefix != "" {
		builder.WriteString("prefix ")
		builder.WriteString(prefix)
	}

	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return nil, err
	}
	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Command: builder.String(),
		Device:  int(request.GetDevice()),
	})
	if err != nil {
		return nil, err
	}

	response := make(map[string]string)
	for _, reply := range replies[1:] {
		parts := strings.Fields(reply.Data)
		if parts[0] != "set" {
			return nil, errors.ErrInvalidData("Invalid reply data: " + reply.Data)
		}
		key := parts[1]

		if encoded {
			decodedValue, err := manager.decode(parts[2])
			if err != nil {
				return nil, errors.ErrInvalidData(fmt.Sprintf("Could not decode the value at key %s. This value is probably not encoded.", key))
			}
			response[key] = decodedValue
		} else {
			response[key] = strings.Join(parts[2:], " ")
		}
	}
	return response, nil
}

func (manager *storageManager) keyExists(request *pb.DeviceStorageRequest) (*pb.BoolResponse, errors.SdkError) {
	builder := strings.Builder{}
	builder.WriteString("storage ")

	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	}

	builder.WriteString("exists ")
	builder.WriteString(request.Key)

	reply, err := commands.SingleRequestAxis(manager.deviceManager, request, builder.String())
	if err != nil {
		return nil, err
	}
	exists, err := reply.DataAsInt()
	if err != nil {
		return nil, err
	}

	return &pb.BoolResponse{
		Value: exists > 0,
	}, nil
}

func (manager *storageManager) setLabel(request *pb.DeviceSetStorageRequest) errors.SdkError {
	axisNumber := int(request.Axis)
	device, err := manager.deviceManager.GetDeviceInfo(request)
	if err != nil {
		return err
	}
	isIdentified := device.IsIdentified()

	if axisNumber > 0 && isIdentified && !device.IsPeripheralLikeAxis(axisNumber) {
		return errors.ErrInvalidOperation("Cannot set the name of an axis of integrated device")
	}

	if request.Value != "" {
		err = manager.SetStorage(request, constants.LabelStorageKey, request.Value, true)
	} else {
		_, err = manager.Erase(request, constants.LabelStorageKey)
	}
	if err != nil {
		return err
	}

	if isIdentified {
		if err := device.SetLabel(request, request.Value); err != nil {
			return err
		}
	}

	return nil
}

func (manager *storageManager) getLabel(request *pb.AxisEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	device, err := manager.deviceManager.GetDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	label, err := device.GetLabel(request)
	if err != nil {
		return nil, err
	}

	return &pb.StringResponse{Value: label}, nil
}

func (manager *storageManager) setStorageRequest(request *pb.DeviceSetStorageRequest) errors.SdkError {
	return manager.SetStorage(request, request.Key, request.Value, request.Encode)
}

func (manager *storageManager) getStorageRequest(request *pb.DeviceGetStorageRequest) (*pb.StringResponse, errors.SdkError) {
	value, err := manager.GetStorage(request, request.Key, request.Decode)
	if err != nil {
		return nil, err
	}
	return &pb.StringResponse{Value: value}, nil
}

func (manager *storageManager) EraseWithPrefix(request ioc.AxisTarget, prefix string, allAxes bool) errors.SdkError {
	requests, err := manager.deviceManager.GetRequests(request)
	if err != nil {
		return err
	}

	builder := strings.Builder{}
	builder.WriteString("storage ")
	if request.GetAxis() != 0 {
		builder.WriteString("axis ")
	} else if allAxes {
		builder.WriteString("all ")
	}
	builder.WriteString("print keys prefix ")
	builder.WriteString(prefix)

	printResps, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(request.GetDevice()),
		Axis:    int(request.GetAxis()),
		Command: builder.String(),
	})
	if err != nil {
		return err
	}

	for _, resp := range printResps[1:] {
		parts := resp.DataAsStrArray()
		if parts[0] != "set" {
			continue
		}

		eraseCmd := strings.Builder{}
		eraseCmd.WriteString("storage ")
		if resp.Axis != 0 {
			eraseCmd.WriteString("axis ")
		}
		eraseCmd.WriteString("erase ")
		eraseCmd.WriteString(parts[1])

		if _, err := commands.SingleCommand(requests, c.Command{
			Device:  resp.Device,
			Axis:    resp.Axis,
			Command: eraseCmd.String(),
		}); err != nil {
			return err
		}
	}

	return nil
}
