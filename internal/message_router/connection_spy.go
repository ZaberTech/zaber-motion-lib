//go:build !wasm

package messagerouter

import (
	"fmt"
	"io"
	"time"
	"zaber-motion-lib/internal/utils"

	rpc "github.com/ethereum/go-ethereum/rpc"
)

type connectionSpy struct {
	connection rpc.Conn
	closeChan  *utils.CloseChannel
}

func NewConnectionSpy(connection rpc.Conn, closeChan *utils.CloseChannel) rpc.Conn {
	return &connectionSpy{
		connection: connection,
		closeChan:  closeChan,
	}
}

func (spy *connectionSpy) SetWriteDeadline(time time.Time) error {
	return spy.connection.SetWriteDeadline(time)
}
func (spy *connectionSpy) Read(p []byte) (n int, err error) {
	n, err = spy.connection.Read(p)
	if err != nil {
		if err == io.EOF {
			spy.closeChan.CloseWithErr(fmt.Errorf("Connection closed by the remote host"))
		} else {
			spy.closeChan.CloseWithErr(fmt.Errorf("Connection failed: %s", err))
		}
	}
	return n, err
}
func (spy *connectionSpy) Write(p []byte) (n int, err error) {
	n, err = spy.connection.Write(p)
	if err != nil {
		spy.closeChan.CloseWithErr(fmt.Errorf("Write failed: %s", err))
	}
	return n, err
}
func (spy *connectionSpy) Close() error {
	return spy.connection.Close()
}
