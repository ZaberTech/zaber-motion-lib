//go:build !wasm

package messagerouter

import (
	"context"
	"fmt"
	"strings"
	"time"

	rpc "github.com/ethereum/go-ethereum/rpc"
	"gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
)

var minRouterAPIVersion = utils.Version{Major: 1, Minor: 0}
var clientAPIVersion = utils.Version{Major: 2, Minor: 0}

type connectionInfo struct {
	Type string `json:"type"`
	ID   string `json:"id"`

	SerialPort string `json:"serial_port,omitempty"`
	BaudRate   uint   `json:"baud_rate,omitempty"`

	Hostname string `json:"hostname,omitempty"`
	TCPPort  uint   `json:"tcp_port,omitempty"`
}

type RouterAPI struct {
	rpc     *rpc.Client
	timeout time.Duration
}

func NewRouterAPI(rpc *rpc.Client, timeout time.Duration) *RouterAPI {
	return &RouterAPI{
		rpc:     rpc,
		timeout: timeout,
	}
}

func (api *RouterAPI) ResolveConnectionName(connectionName string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), api.timeout)
	defer cancel()
	var connections []*connectionInfo
	if err := api.rpc.CallContext(ctx, &connections, "routing_listConnections"); err != nil {
		return "", fmt.Errorf("Could not list message router connections")
	}

	if len(connections) == 0 {
		return "", fmt.Errorf("There are no connections available on this message router")
	}
	if connectionName == "" && len(connections) == 1 {
		return connections[0].ID, nil
	}

	foundMatchingConnection := false
	allConnectionNames := make([]string, 0, len(connections))
	for _, conn := range connections {
		if conn.ID == connectionName {
			foundMatchingConnection = true
			break
		}
		allConnectionNames = append(allConnectionNames, conn.ID)
	}
	if !foundMatchingConnection {
		return "", fmt.Errorf("Connection %s not available. Choose one of [%s]", connectionName, strings.Join(allConnectionNames, ", "))
	}
	return connectionName, nil
}

func (api *RouterAPI) CheckConnection() error {
	ctx, cancel := context.WithTimeout(context.Background(), api.timeout)
	defer cancel()
	if err := api.rpc.CallContext(ctx, nil, "status_ping"); err != nil {
		return fmt.Errorf("Message router cannot be reached: %s", err)
	}
	return nil
}

func (api *RouterAPI) Warmup(connectionName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), api.timeout)
	defer cancel()
	if err := api.rpc.CallContext(ctx, nil, "routing_warmup", connectionName); err != nil {
		return fmt.Errorf("Could not warm up connection %s: %s", connectionName, err)
	}
	return nil
}

func (api *RouterAPI) Cooldown(connectionName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), api.timeout)
	defer cancel()
	if err := api.rpc.CallContext(ctx, nil, "routing_cooldown", connectionName); err != nil {
		return fmt.Errorf("Could not cooldown connection %s: %s", connectionName, err)
	}
	return nil
}

func (api *RouterAPI) WriteLine(connectionName string, line string, id uint32) error {
	ctx, cancel := context.WithTimeout(context.Background(), api.timeout)
	defer cancel()
	if err := api.rpc.Notify(ctx, "routing_sendMessage", connectionName, line, id); err != nil {
		return fmt.Errorf("Writeline error %s: %s", connectionName, err)
	}
	return nil
}

func (api *RouterAPI) RegisterReceiver(name string, receiver interface{}) {
	if err := api.rpc.RegisterName(name, receiver); err != nil {
		panic(fmt.Errorf("Cannot register RPC received: %s", err))
	}
}

type ExchangeVersionsResult struct {
	// returned by the server
	Server utils.Version
	// claimed by this client (based on the server)
	Client utils.Version
}

func (api *RouterAPI) ExchangeVersions() (*ExchangeVersionsResult, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*api.timeout)
	defer cancel()

	var err error
	result := &ExchangeVersionsResult{}

	var apiVersionStr string
	if err := api.rpc.CallContext(ctx, &apiVersionStr, "status_getApiVersion"); err != nil {
		return nil, fmt.Errorf("status_getApiVersion failed: %s", err)
	}
	result.Server, err = utils.ParseVersion(apiVersionStr)
	if err != nil {
		return nil, fmt.Errorf("status_getApiVersion (parse) failed: %s", err)
	}
	if !utils.VersionCompatible(result.Server, minRouterAPIVersion) {
		return nil, fmt.Errorf("message router API not compatible: %s (required ^%s)", result.Server, minRouterAPIVersion)
	}

	if utils.VersionGte(result.Server, utils.Version{Major: 1, Minor: 2}) {
		result.Client = clientAPIVersion
		if err := api.rpc.CallContext(ctx, nil, "client_setVersion", clientAPIVersion.String()); err != nil {
			return nil, fmt.Errorf("client_setVersion failed: %s", err)
		}
	} else {
		result.Client = utils.Version{Major: 1}
	}

	return result, nil
}
