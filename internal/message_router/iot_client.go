//go:build !wasm

package messagerouter

import (
	"encoding/json"
	"fmt"
	go_io "io"
	"net/http"
	"time"
	"zaber-motion-lib/internal/utils"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	rpc "github.com/ethereum/go-ethereum/rpc"
)

const iotTimeout = 30 * time.Second
const iotQos = 0

type CloudConfig struct {
	API    string
	Token  string
	Realm  string
	ID     string
	Router string
}

type midRead struct {
	data []byte
	at   int
}

type mqttIoClient struct {
	client mqtt.Client
	config *CloudConfig

	rx      chan []byte
	reading *midRead
	control *utils.CloseChannel
}

func NewMqttIoClient(config *CloudConfig) (rpc.Conn, error) {
	controlChannel := utils.NewCloseChannel()
	client, err := connect(config, controlChannel)
	if err != nil {
		return nil, err
	}

	ioClient := &mqttIoClient{
		client: client,
		config: config,

		rx:      make(chan []byte, 10),
		control: controlChannel,
	}

	if err := ioClient.subscribe(); err != nil {
		ioClient.Close()
		return nil, fmt.Errorf("Subscribing to %s failed: %s", config.Router, err)
	}

	return ioClient, nil
}

type apiConfig struct {
	URL          string `json:"url"`
	DefaultRealm string `json:"defaultRealm"`
}

func getConfigFromToken(config *CloudConfig) (*apiConfig, error) {
	resp, err := http.Get(fmt.Sprintf("%s/iot-token/websocket-url?token=%s", config.API, config.Token))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Invalid status code: %d", resp.StatusCode)
	}

	body, err := go_io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	response := &apiConfig{}
	if err := json.Unmarshal(body, response); err != nil {
		return nil, err
	}
	return response, nil
}

func connect(config *CloudConfig, controlChannel *utils.CloseChannel) (mqtt.Client, error) {
	apiConfig, err := getConfigFromToken(config)
	if err != nil {
		return nil, fmt.Errorf("Could not obtain authentication token: %s", err)
	}
	if config.Realm == "" {
		config.Realm = apiConfig.DefaultRealm
	}

	options := mqtt.NewClientOptions()
	options.SetClientID(fmt.Sprintf("%s-%s", config.Realm, config.ID))
	options.SetProtocolVersion(4)
	options.SetAutoReconnect(false)
	options.SetConnectTimeout(iotTimeout)
	options.SetConnectionLostHandler(func(_ mqtt.Client, errLost error) {
		controlChannel.CloseWithErr(fmt.Errorf("Connection Lost: %s", errLost.Error()))
	})
	options.AddBroker(apiConfig.URL)

	mqttClient := mqtt.NewClient(options)
	token := mqttClient.Connect()
	token.Wait()
	if err := token.Error(); err != nil {
		return nil, fmt.Errorf("Could not connect to broker: %s", err)
	}

	return mqttClient, nil
}

func (io *mqttIoClient) subscribe() error {
	rxTopic := fmt.Sprintf("%s/message-router/%s/%s/rpc", io.config.Realm, io.config.ID, io.config.Router)
	token := io.client.Subscribe(rxTopic, iotQos, func(_ mqtt.Client, message mqtt.Message) {
		io.rx <- message.Payload()
	})
	if !token.WaitTimeout(iotTimeout) {
		return fmt.Errorf("Timeout when subscribing")
	}
	return token.Error()
}

func (io *mqttIoClient) Read(b []byte) (int, error) {
	if io.reading != nil {
		return io.readFromCurrentMessage(b)
	}
	select {
	case data := <-io.rx:
		io.reading = &midRead{
			data: data,
			at:   0,
		}
		return io.readFromCurrentMessage(b)
	case err := <-io.control.Channel():
		if err != nil {
			return 0, err
		}
		return 0, go_io.EOF
	}
}

func (io *mqttIoClient) readFromCurrentMessage(b []byte) (int, error) {
	bytesRead := copy(b, io.reading.data[io.reading.at:])
	io.reading.at = io.reading.at + bytesRead
	if io.reading.at >= len(io.reading.data) {
		io.reading = nil
	}
	return bytesRead, nil
}

func (io *mqttIoClient) Write(b []byte) (int, error) {
	// Writer assumes that each write is a separate RPC call.
	// That is true due to RPC library implementation but is not universal.
	txTopic := fmt.Sprintf("%s/message-router/%s/%s/rpc", io.config.Realm, io.config.Router, io.config.ID)
	token := io.client.Publish(txTopic, iotQos, false, b)
	if !token.WaitTimeout(iotTimeout) {
		return 0, fmt.Errorf("Timeout")
	}
	if err := token.Error(); err != nil {
		return 0, err
	}
	return len(b), nil
}

func (io *mqttIoClient) Close() error {
	io.control.Close()
	io.client.Disconnect(0)
	return nil
}

func (io *mqttIoClient) SetWriteDeadline(time.Time) error {
	return nil
}
