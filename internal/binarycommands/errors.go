package binarycommands

import (
	"fmt"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
)

type BinaryCommandError struct {
	command    *c.BinaryMessage
	reply      *c.BinaryMessage
	message    string
	customData pb.Message
}

func (err *BinaryCommandError) Type() pb.Errors {
	return pb.Errors_BINARY_COMMAND_FAILED
}
func (err *BinaryCommandError) TypeStr() string {
	return pb.Errors_name[err.Type()]
}
func (err *BinaryCommandError) Command() *c.BinaryMessage {
	return err.command
}
func (err *BinaryCommandError) Reply() *c.BinaryMessage {
	return err.reply
}
func (err *BinaryCommandError) Data() int32 {
	return err.reply.Data
}
func (err *BinaryCommandError) ErrCode() pb.BinaryErrorCode {
	return (pb.BinaryErrorCode)(err.reply.Data)
}
func (err *BinaryCommandError) Message() string {
	return err.message
}
func (err *BinaryCommandError) AddToMessage(message string) {
	err.message += " " + message
}
func (err *BinaryCommandError) AddToMessagef(format string, args ...interface{}) {
	err.AddToMessage(fmt.Sprintf(format, args...))
}
func (err *BinaryCommandError) String() string {
	return fmt.Sprintf("%s: %s", err.TypeStr(), err.message)
}
func (err *BinaryCommandError) CustomData() pb.Message {
	return err.customData
}

func BinaryCommandErr(cmd *c.BinaryMessage, reply *c.BinaryMessage) errors.SdkError {
	customData := &pb.ExceptionsBinaryCommandFailedExceptionData{
		ResponseData: reply.Data,
	}

	return &BinaryCommandError{
		command:    cmd,
		reply:      reply,
		message:    fmt.Sprintf("Command %d with data %d resulted in error code %d.", cmd.Command, cmd.Data, reply.Data),
		customData: customData,
	}
}

func CheckOk(cmd *c.BinaryMessage, reply *c.BinaryMessage) errors.SdkError {
	if pb.BinaryReplyCode(reply.Command) == pb.BinaryReplyCode_ERROR {
		return BinaryCommandErr(cmd, reply)
	}
	return nil
}

func IsErrorWithCode(err errors.SdkError, code pb.BinaryErrorCode) bool {
	if binaryErr, ok := err.(*BinaryCommandError); ok {
		return binaryErr.ErrCode() == code
	}
	return false
}

func IsInvalidCommandOrSettingErr(err errors.SdkError) bool {
	return IsErrorWithCode(err, pb.BinaryErrorCode_SETTING_INVALID) || IsErrorWithCode(err, pb.BinaryErrorCode_COMMAND_INVALID)
}
