package wdi

import (
	"fmt"
	"log"
	"net"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type communicationInterface struct {
	idString   string
	connection net.Conn
	session    *Session
	onClose    func()
}

type interfaceManager struct {
	gateway    ioc.GatewayManager
	microscopy ioc.Microscopy
}

func NewInterfaceManager(gateway ioc.GatewayManager, microscopy ioc.Microscopy) *interfaceManager {
	manager := &interfaceManager{
		gateway:    gateway,
		microscopy: microscopy,
	}
	manager.register()
	return manager
}

func (manager *interfaceManager) register() {
	gateway := manager.gateway

	gateway.RegisterCallback("wdi/open", func() pb.Message {
		return &pb.OpenInterfaceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.openInterface(request.(*pb.OpenInterfaceRequest))
	})
	gateway.RegisterCallback("wdi/close", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.closeInterfaceRequest(request.(*pb.InterfaceEmptyRequest))
	})
	gateway.RegisterCallback("wdi/to_string", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.toString(request.(*pb.InterfaceEmptyRequest))
	})
	gateway.RegisterCallback("wdi/write", func() pb.Message {
		return &pb.WdiGenericRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, manager.genericWrite(request.(*pb.WdiGenericRequest))
	})
	gateway.RegisterCallback("wdi/read", func() pb.Message {
		return &pb.WdiGenericRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.genericRead(request.(*pb.WdiGenericRequest))
	})
	gateway.RegisterCallback("wdi/get_status", func() pb.Message {
		return &pb.InterfaceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return manager.getStatusRequest(request.(*pb.InterfaceEmptyRequest))
	})
}

func (manager *interfaceManager) getInterface(id int32) (*communicationInterface, errors.SdkError) {
	value, ok := manager.microscopy.GetAutofocusProvider(id)
	if !ok {
		return nil, errors.ErrConnectionClosed()
	}
	return value.(*communicationInterface), nil
}

func (manager *interfaceManager) openInterface(request *pb.OpenInterfaceRequest) (*pb.IntResponse, errors.SdkError) {
	endpoint := fmt.Sprintf("%s:%d", request.HostName, request.Port)
	io, dialErr := net.Dial("tcp", endpoint)
	if dialErr != nil {
		return nil, errors.ErrConnectionFailed(dialErr.Error())
	}

	session := NewSession(NewTransport(io))
	verify, err := verifyAutofocus(session)
	if err != nil {
		_ = io.Close()
		return nil, err
	}

	log.Printf("WDI autofocus with serial number %d found at %s", verify.serialNumber, endpoint)

	communication := &communicationInterface{
		idString:   fmt.Sprintf("WDI autofocus %d at %s", verify.serialNumber, endpoint),
		connection: io,
		session:    session,
	}
	id := manager.microscopy.RegisterAutofocusProvider(communication)
	communication.onClose = func() {
		_ = manager.closeInterface(id)
	}

	return &pb.IntResponse{
		Value: id,
	}, nil
}

func (manager *interfaceManager) closeInterfaceRequest(request *pb.InterfaceEmptyRequest) errors.SdkError {
	return manager.closeInterface(request.InterfaceId)
}

func (manager *interfaceManager) closeInterface(interfaceID int32) errors.SdkError {
	provider, removed := manager.microscopy.RemoveAutofocusProvider(interfaceID)
	if !removed {
		return nil // repeated close is noop
	}
	communication := provider.(*communicationInterface)

	log.Printf("Closing connection with %s", communication.idString)
	if err := communication.connection.Close(); err != nil {
		return errors.ErrConnectionFailed(err.Error())
	}

	return nil
}

func (manager *interfaceManager) toString(request *pb.InterfaceEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	communication, err := manager.getInterface(request.InterfaceId)
	var value string
	if err != nil {
		value = "WDI autofocus (Connection closed)"
	} else {
		value = communication.idString
	}
	return &pb.StringResponse{
		Value: value,
	}, nil
}

type verifyResult struct {
	serialNumber uint32
}

func verifyAutofocus(session *Session) (*verifyResult, errors.SdkError) {
	// TODO: verify FW version?

	serialNumber, err := session.ReadRequest(&Request{
		RegisterID: 82,
		DataType:   RegisterDataTypeDWord,
		DataOffset: 3,
	})
	if err != nil {
		return nil, errors.ErrConnectionFailed("initial request to WDI autofocus failed: " + err.Error())
	}

	return &verifyResult{
		serialNumber: uint32(serialNumber[0]),
	}, nil
}

func dataTypeFromSize(size int32) (RegisterDataType, errors.SdkError) {
	switch size {
	case 0:
		return RegisterDataTypeNoData, nil
	case 1:
		return RegisterDataTypeByte, nil
	case 2:
		return RegisterDataTypeWord, nil
	case 4:
		return RegisterDataTypeDWord, nil
	default:
		return RegisterDataTypeNoData, errors.ErrInvalidArgument("Invalid data size. Only values (0, 1, 2, 4) are allowed.")
	}
}

func (manager *interfaceManager) genericWrite(request *pb.WdiGenericRequest) errors.SdkError {
	communication, err := manager.getInterface(request.InterfaceId)
	if err != nil {
		return err
	}

	dataType, err := dataTypeFromSize(request.Size)
	if err != nil {
		return err
	}

	bank := RegisterBankUser
	if len(request.RegisterBank) > 0 {
		bank = byte(request.RegisterBank[0])
	}

	reqErr := communication.session.WriteRequest(&Request{
		Bank:       bank,
		RegisterID: byte(request.RegisterId),
		DataType:   dataType,
		DataOffset: int(request.Offset),
	}, request.Data)
	if reqErr != nil {
		return communication.checkErr(reqErr)
	}

	return nil
}

func (manager *interfaceManager) genericRead(request *pb.WdiGenericRequest) (*pb.IntArrayResponse, errors.SdkError) {
	communication, err := manager.getInterface(request.InterfaceId)
	if err != nil {
		return nil, err
	}

	dataType, err := dataTypeFromSize(request.Size)
	if err != nil {
		return nil, err
	}

	bank := RegisterBankUser
	if len(request.RegisterBank) > 0 {
		bank = byte(request.RegisterBank[0])
	}

	data, reqErr := communication.session.ReadRequest(&Request{
		Bank:       bank,
		RegisterID: byte(request.RegisterId),
		DataType:   dataType,
		DataOffset: int(request.Offset),
		DataCount:  int(request.Count),
	})
	if reqErr != nil {
		return nil, communication.checkErr(reqErr)
	}

	return &pb.IntArrayResponse{
		Values: data,
	}, nil
}

func (manager *interfaceManager) getStatusRequest(request *pb.InterfaceEmptyRequest) (*pb.WdiGetStatusResponse, errors.SdkError) {
	communication, err := manager.getInterface(request.InterfaceId)
	if err != nil {
		return nil, err
	}

	status, err := communication.getStatus()
	if err != nil {
		return nil, err
	}

	return &pb.WdiGetStatusResponse{
		Status: &pb.MicroscopyWdiAutofocusProviderStatus{
			InRange: status.inRange,
			LaserOn: status.laserOn,
		},
	}, nil
}
