package wdi

import "fmt"

type TransportError struct {
	Err error
}

func transportErrorf(format string, args ...interface{}) *TransportError {
	return &TransportError{Err: fmt.Errorf(format, args...)}
}

func (e *TransportError) Error() string {
	return "transport error: " + e.Err.Error()
}

type InvalidArgumentError struct {
	Err string
}

func invalidArgumentErrorf(format string, args ...interface{}) *InvalidArgumentError {
	return &InvalidArgumentError{Err: fmt.Sprintf(format, args...)}
}

func (e *InvalidArgumentError) Error() string {
	return "invalid argument: " + e.Err
}

type ProtocolError struct {
	Err string
}

func protocolErrorf(format string, args ...interface{}) *ProtocolError {
	return &ProtocolError{Err: fmt.Sprintf(format, args...)}
}

func (e *ProtocolError) Error() string {
	return "protocol error: " + e.Err
}

type NakError struct {
	Err string
}

func nakErrorf(format string, args ...interface{}) *NakError {
	return &NakError{Err: fmt.Sprintf(format, args...)}
}

func (e *NakError) Error() string {
	return "NAK: " + e.Err
}
