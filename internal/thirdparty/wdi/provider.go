package wdi

import (
	"zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"github.com/elliotchance/pie/v2"
)

// External Documentation:
// - UM801379B PFA-DT Functions Library Reference Guide.pdf

type status struct {
	inRange bool
	inFocus bool
	laserOn bool
}

const floatMultiplier = 1024.0

func checkStatus(status *status, checkInRange bool) errors.SdkError {
	if !status.laserOn {
		return errors.ErrInvalidOperation("Laser is off.")
	} else if checkInRange && !status.inRange {
		return errors.ErrInvalidOperation("Autofocus is out of range.")
	}
	return nil
}

func (provider *communicationInterface) IsReadyToFocus(options ioc.IsReadyToFocusOptions) errors.SdkError {
	status, err := provider.getStatus()
	if err != nil {
		return err
	}
	return checkStatus(status, !options.IgnoreInRange)
}

func (provider *communicationInterface) SetFocusZero() errors.SdkError {
	status, err := provider.getStatus()
	if err != nil {
		return err
	}

	if err := checkStatus(status, true); err != nil {
		return err
	}

	reqErr := provider.session.WriteRequest(&Request{
		RegisterID: 62,
	}, nil)
	return provider.checkErr(reqErr)
}

func (status *status) GetInRange() bool {
	return status.inRange
}

func (provider *communicationInterface) getStatus() (*status, errors.SdkError) {
	values, err := provider.session.ReadRequest(&Request{
		RegisterID: 20,
		DataType:   RegisterDataTypeWord,
	})
	if err != nil {
		return nil, provider.checkErr(err)
	}
	status := &status{
		laserOn: values[0]&(1<<4) > 0,
		inRange: values[0]&(1<<12) > 0,
		inFocus: values[0]&(1<<13) > 0,
	}
	return status, nil
}

func (provider *communicationInterface) GetStatus() (ioc.AutofocusProviderStatus, errors.SdkError) {
	return provider.getStatus()
}

func (provider *communicationInterface) GetCurrentObjective() (int, errors.SdkError) {
	values, err := provider.session.ReadRequest(&Request{
		RegisterID: 50,
		DataType:   RegisterDataTypeDWord,
	})
	if err != nil {
		return 0, provider.checkErr(err)
	}
	return int(values[0] + 1), nil
}

func (provider *communicationInterface) SetCurrentObjective(objective int) errors.SdkError {
	err := provider.session.WriteRequest(&Request{
		RegisterID: 50,
		DataType:   RegisterDataTypeDWord,
	}, []int32{int32(objective - 1)})
	return provider.checkErr(err)
}

// Finds the first higher or equal magnification.
func findClosestMagnification(magnifications []int32, magnification float64) int32 {
	if magnification <= 0 {
		return 0
	}

	closest := len(magnifications) - 1
	for i, candidate := range magnifications {
		if float64(candidate) >= magnification {
			closest = i
			break
		}
	}
	return magnifications[closest]
}

func (provider *communicationInterface) GetObjectiveParameters(objective int) ([]*dto.NamedParameter, errors.SdkError) {
	magnification, err := provider.getMagnification(objective)
	if err != nil {
		return nil, err
	}

	var params []*dto.NamedParameter
	params = append(params, &dto.NamedParameter{
		Name:  "magnification",
		Value: utils.ToPointer(float64(magnification)),
	})

	if isDisabled := magnification == 0; isDisabled {
		for param, info := range parameterInfo {
			if info.currentObjective {
				params = append(params, &dto.NamedParameter{Name: param})
			}
		}
		return params, nil
	}

	if err = provider.switchCurrentObjectiveTemporarily(objective, func() errors.SdkError {
		maxExposure, err := provider.ReadRequest(&Request{
			RegisterID: 230,
			DataType:   RegisterDataTypeDWord,
		})
		if err != nil {
			return err
		}
		params = append(params, &dto.NamedParameter{
			Name:  "max_exposure",
			Value: utils.ToPointer(float64(maxExposure[0])),
		})

		signalToNoise, err := provider.ReadRequest(&Request{
			RegisterID: 231,
			DataType:   RegisterDataTypeDWord,
		})
		if err != nil {
			return err
		}
		params = append(params, &dto.NamedParameter{
			Name:  "signal_to_noise_ratio",
			Value: utils.ToPointer(float64(signalToNoise[0]) / floatMultiplier),
		})

		return nil
	}); err != nil {
		return nil, err
	}

	return params, nil
}

var parameterInfo = map[string]struct {
	order            int
	currentObjective bool
}{
	"magnification":         {0, false}, // resets all other parameters
	"max_exposure":          {1, true},
	"signal_to_noise_ratio": {2, true},
}

func (provider *communicationInterface) IsValidParameter(parameter string) bool {
	_, ok := parameterInfo[parameter]
	return ok
}

func (provider *communicationInterface) SetObjectiveParameters(objective int, parameters []*dto.NamedParameter) errors.SdkError {
	if err := provider.setObjectiveParametersToAnyObjective(objective, parameters); err != nil {
		return err
	}

	if hasCurrentObjectiveSettings := pie.Any(parameters, func(param *dto.NamedParameter) bool {
		return param.Value != nil && parameterInfo[param.Name].currentObjective
	}); !hasCurrentObjectiveSettings {
		return nil
	}

	if magnification, err := provider.getMagnification(objective); err != nil {
		return err
	} else if isDisabled := magnification == 0; isDisabled {
		// No point in setting parameters.
		return nil
	}

	return provider.switchCurrentObjectiveTemporarily(objective, func() errors.SdkError {
		return provider.setObjectiveParametersToCurrentObjective(parameters)
	})
}

func (provider *communicationInterface) switchCurrentObjectiveTemporarily(
	objective int,
	operation func() errors.SdkError,
) errors.SdkError {
	currentObjective, err := provider.GetCurrentObjective()
	if err != nil {
		return err
	}
	switchObjective := currentObjective != objective
	if switchObjective {
		if err := provider.SetCurrentObjective(objective); err != nil {
			return err
		}
	}

	errOperation := operation()

	if switchObjective {
		if err := provider.SetCurrentObjective(currentObjective); err != nil {
			if errOperation != nil {
				return errOperation
			}
			return err
		}
	}

	return errOperation
}

func sortObjectiveParametersForApplication(parameters []*dto.NamedParameter) []*dto.NamedParameter {
	return pie.SortUsing(parameters, func(a, b *dto.NamedParameter) bool {
		order1 := parameterInfo[a.Name].order
		order2 := parameterInfo[b.Name].order
		return order1 < order2
	})
}

func (provider *communicationInterface) setObjectiveParametersToAnyObjective(objective int, parameters []*dto.NamedParameter) errors.SdkError {
	for _, param := range sortObjectiveParametersForApplication(parameters) {
		if param.Value == nil {
			continue
		}

		switch param.Name {
		case "magnification":
			available, err := provider.getAvailableMagnifications()
			if err != nil {
				return err
			}
			current, err := provider.getMagnification(objective)
			if err != nil {
				return err
			}
			closestMagnification := findClosestMagnification(available, *param.Value)

			// The value must be compared because writing resets all other parameters.
			if current != closestMagnification {
				if err := provider.WriteRequest(&Request{
					RegisterID: 54,
					DataType:   RegisterDataTypeWord,
					DataOffset: objective - 1,
				}, []int32{closestMagnification}); err != nil {
					err.AddToMessagef("(parameter: %s)", param.Name)
					return err
				}
			}
		}
	}

	return nil
}

func (provider *communicationInterface) setObjectiveParametersToCurrentObjective(parameters []*dto.NamedParameter) errors.SdkError {
	for _, param := range sortObjectiveParametersForApplication(parameters) {
		if param.Value == nil {
			continue
		}

		switch param.Name {
		case "max_exposure":
			if err := provider.WriteRequest(&Request{
				RegisterID: 230,
				DataType:   RegisterDataTypeDWord,
			}, []int32{int32(utils.RoundNumber(*param.Value, 0))}); err != nil {
				err.AddToMessagef("(parameter: %s)", param.Name)
				return err
			}
		case "signal_to_noise_ratio":
			if err := provider.WriteRequest(&Request{
				RegisterID: 231,
				DataType:   RegisterDataTypeDWord,
			}, []int32{int32(utils.RoundNumber(*param.Value*floatMultiplier, 0))}); err != nil {
				err.AddToMessagef("(parameter: %s)", param.Name)
				return err
			}
		}
	}

	return nil
}

func (provider *communicationInterface) WriteRequest(request *Request, data []int32) errors.SdkError {
	return provider.checkErr(provider.session.WriteRequest(request, data))
}

func (provider *communicationInterface) ReadRequest(request *Request) ([]int32, errors.SdkError) {
	data, err := provider.session.ReadRequest(request)
	if err != nil {
		return nil, provider.checkErr(err)
	}
	return data, nil
}

func (provider *communicationInterface) getAvailableMagnifications() ([]int32, errors.SdkError) {
	magnifications, err := provider.ReadRequest(&Request{
		RegisterID: 78,
		DataType:   RegisterDataTypeWord,
		DataCount:  16,
	})
	if err != nil {
		return nil, err
	}
	return pie.Filter(magnifications, func(magnification int32) bool { return magnification > 0 }), nil
}

func (provider *communicationInterface) getMagnification(objective int) (int32, errors.SdkError) {
	request := &Request{
		RegisterID: 54,
		DataType:   RegisterDataTypeWord,
		DataOffset: objective - 1,
	}
	data, err := provider.ReadRequest(request)
	if err != nil {
		return 0, err
	}
	return data[0], nil
}

func (provider *communicationInterface) ToString() string {
	return provider.idString
}

func (provider *communicationInterface) checkErr(err error) errors.SdkError {
	if err == nil {
		return nil
	}

	_, isTransportErr := err.(*TransportError)
	_, isProtocolErr := err.(*ProtocolError)
	if isTransportErr || isProtocolErr {
		go provider.onClose()
		return errors.ErrConnectionFailed("connection failed: " + err.Error())
	}

	if _, isInvalidArgErr := err.(*InvalidArgumentError); isInvalidArgErr {
		return errors.ErrInvalidArgument(err.Error())
	}

	if _, isNakErr := err.(*NakError); isNakErr {
		return errors.ErrInvalidData("Device responded with " + err.Error())
	}

	return errors.ErrInvalidData(err.Error())
}
