package wdi

import (
	"encoding/binary"
	"log"
	"sync"
)

// External Documentation:
// - AN0129 PFA-DT Serial Command Protocol AN revA.pdf

type Session struct {
	transport *Transport
	lock      sync.Mutex
}

type RequestType = byte

const (
	RequestTypeRead         RequestType = byte('R')
	RequestTypeReadResponse             = byte('r')
	RequestTypeWrite                    = byte('W')
)

type RegisterDataType = byte

const (
	RegisterDataTypeNoData RegisterDataType = iota
	RegisterDataTypeByte
	RegisterDataTypeWord
	RegisterDataTypeDWord
)

type RegisterBank = byte

const (
	RegisterBankUser RegisterBank = byte('U')
	RegisterBankTest              = byte('t')
	RegisterBankPFA               = byte('P')
)

type Request struct {
	Bank       RegisterBank
	RegisterID byte

	DataType   RegisterDataType
	DataCount  int // Data Number in the protocol
	DataOffset int
}

const dataTagsConstant = 0x07
const dataTagsArray = 1 << 6
const dataTagsOffset = 1 << 7

func NewSession(transport *Transport) *Session {
	return &Session{transport: transport}
}

func (s *Session) ReadRequest(request *Request) ([]int32, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if request.DataType == RegisterDataTypeNoData {
		return nil, &InvalidArgumentError{"no data type specified"}
	}

	log.Printf("TX: %+v\n", request)
	if err := s.transport.Write(&TransportRequest{
		Payload: makeHeader(RequestTypeRead, request),
	}); err != nil {
		return nil, err
	}

	response, err := s.transport.Read(true)
	if err != nil {
		return nil, err
	} else if !response.Ack {
		log.Println("RX: NAK")
		return nil, nakErrorf("Read request from %c%d", request.Bank, request.RegisterID)
	}

	if response.Payload[0] != byte(RequestTypeReadResponse) {
		return nil, protocolErrorf("unexpected response type: %v", response.Payload[0])
	} else if response.Payload[1] != byte(request.Bank) {
		return nil, protocolErrorf("unexpected bank: %v", response.Payload[1])
	} else if response.Payload[2] != request.RegisterID {
		return nil, protocolErrorf("unexpected register ID: %v", response.Payload[2])
	}

	dataCount := 1
	if request.DataCount > 1 {
		dataCount = request.DataCount
	}
	offset := 4

	if dataCount > 1 {
		if (response.Payload[3] & dataTagsArray) == 0 {
			return nil, protocolErrorf("expected array data but got single data")
		}

		responseDataCount := int(binary.LittleEndian.Uint16(response.Payload[offset:]))
		if responseDataCount != dataCount {
			return nil, protocolErrorf("unexpected data count: %v != %v", responseDataCount, dataCount)
		}
		offset += 2
	} else {
		if (response.Payload[3] & dataTagsArray) > 0 {
			return nil, protocolErrorf("expected single data but got array data")
		}
	}

	result := make([]int32, dataCount)

	for i := 0; i < dataCount; i++ {
		switch request.DataType {
		case RegisterDataTypeByte:
			result[i] = int32(int8(response.Payload[offset]))
			offset += 1
		case RegisterDataTypeWord:
			result[i] = int32(int16(binary.LittleEndian.Uint16(response.Payload[offset:])))
			offset += 2
		case RegisterDataTypeDWord:
			result[i] = int32(binary.LittleEndian.Uint32(response.Payload[offset:]))
			offset += 4
		}
	}

	log.Printf("RX: %+v\n", result)
	return result, nil
}

func (s *Session) WriteRequest(request *Request, data []int32) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	if request.DataCount == 0 {
		request.DataCount = len(data)
	}

	payload := makeHeader(RequestTypeWrite, request)

	if len(data) != request.DataCount {
		return invalidArgumentErrorf("data count does not match the request: %v != %v", request.DataCount, len(data))
	} else if request.DataCount > 1 && request.DataType == RegisterDataTypeNoData {
		return invalidArgumentErrorf("data count is greater than 1 but no data type specified")
	}

	for _, dataElement := range data {
		switch request.DataType {
		case RegisterDataTypeByte:
			payload = append(payload, byte(dataElement))
		case RegisterDataTypeWord:
			payload = binary.LittleEndian.AppendUint16(payload, uint16(dataElement))
		case RegisterDataTypeDWord:
			payload = binary.LittleEndian.AppendUint32(payload, uint32(dataElement))
		default:
			return invalidArgumentErrorf("unsupported data type: %v", request.DataType)
		}
	}

	log.Printf("TX: %+v\n", request)
	if err := s.transport.Write(&TransportRequest{
		Payload: payload,
	}); err != nil {
		return err
	}

	response, err := s.transport.Read(false)
	if err != nil {
		return err
	} else if !response.Ack {
		log.Println("RX: NAK")
		return nakErrorf("Write request to %c%d", request.Bank, request.RegisterID)
	}

	log.Println("RX: ACK")
	return nil
}

func makeHeader(requestType RequestType, request *Request) []byte {
	if request.Bank == 0 {
		request.Bank = RegisterBankUser
	}

	payload := []byte{
		byte(requestType),
		byte(request.Bank),
		request.RegisterID,
		dataTagsConstant + (request.DataType << 4), // data tags
	}
	if request.DataCount > 1 {
		payload[3] |= dataTagsArray
		payload = binary.LittleEndian.AppendUint16(payload, uint16(request.DataCount))
	}
	if request.DataOffset > 0 {
		payload[3] |= dataTagsOffset
		payload = binary.LittleEndian.AppendUint16(payload, uint16(request.DataOffset))
	}
	return payload
}
