package wdi

import (
	"fmt"
	"testing"

	"gotest.tools/assert"
)

// findClosestMagnification finds the first higher or equal magnification
func TestFindClosestMagnification(t *testing.T) {
	magnifications := []int32{1, 2, 5, 10, 20, 50, 100}

	tests := []struct {
		input    float64
		expected int32
	}{
		{0, 0},
		{0.5, 1},
		{1, 1},
		{40, 50},
		{50, 50},
		{60, 100},
		{100, 100},
		{120, 100},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("input=%v", test.input), func(t *testing.T) {
			closest := findClosestMagnification(magnifications, test.input)
			assert.Equal(t, test.expected, closest)
		})
	}
}
