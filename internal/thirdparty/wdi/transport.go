package wdi

import (
	"encoding/binary"
	"io"
	"time"
)

// External Documentation:
// - AN0129 PFA-DT Serial Command Protocol AN revA.pdf

const littleEndianByte = byte('l')
const ackByte = byte('A')
const checksumSize = 1

type ReadWriterTimeout interface {
	io.ReadWriter
	SetReadDeadline(t time.Time) error
	SetWriteDeadline(t time.Time) error
}

type Transport struct {
	stream  ReadWriterTimeout
	Timeout time.Duration
}

type TransportReply struct {
	Ack     bool
	Payload []byte
}

type TransportRequest struct {
	Payload []byte
}

func NewTransport(stream ReadWriterTimeout) *Transport {
	return &Transport{stream: stream, Timeout: 10 * time.Second}
}

func (t *Transport) Read(expectData bool) (*TransportReply, error) {
	if t.Timeout > 0 {
		_ = t.stream.SetReadDeadline(time.Now().Add(t.Timeout))
	}

	ack, err := t.readCount(1)
	if err != nil {
		return nil, err
	}

	reply := &TransportReply{Ack: ack[0] == ackByte}
	if !reply.Ack || !expectData {
		return reply, nil
	}

	header, err := t.readCount(4)
	if err != nil {
		return nil, err
	}
	if header[0] != littleEndianByte {
		return nil, transportErrorf("unsupported byte order: %v", header[0])
	}
	payloadSize := int(binary.LittleEndian.Uint16(header[1:])) - checksumSize

	payload, err := t.readCount(payloadSize)
	if err != nil {
		return nil, err
	}

	checksum := 0
	for _, b := range header {
		checksum += int(b)
	}
	for _, b := range payload {
		checksum += int(b)
	}
	if (checksum & 0xff) != 0 {
		return nil, transportErrorf("checksum error")
	}

	reply.Payload = payload
	return reply, nil
}

func (t *Transport) readCount(count int) ([]byte, error) {
	buffer := make([]byte, count)
	_, err := io.ReadFull(t.stream, buffer)
	if err != nil {
		return nil, &TransportError{Err: err}
	}
	return buffer, nil
}

func (t *Transport) Write(request *TransportRequest) error {
	if len(request.Payload) == 0 {
		return invalidArgumentErrorf("empty payload")
	}

	if t.Timeout > 0 {
		_ = t.stream.SetWriteDeadline(time.Now().Add(t.Timeout))
	}

	headerSize := 4
	packet := make([]byte, headerSize, len(request.Payload)+headerSize)

	packet[0] = littleEndianByte
	binary.LittleEndian.PutUint16(packet[1:], uint16(len(request.Payload)+checksumSize))
	// packet[3] is the checksum

	packet = append(packet, request.Payload...)

	checksum := 0
	for _, b := range packet {
		checksum += int(b)
	}
	checksum = 0x100 - (checksum & 0xff)
	packet[3] = byte(checksum)

	if _, err := t.stream.Write(packet); err != nil {
		return &TransportError{Err: err}
	}
	return nil
}
