package constants

import "time"

const defaultIdlePollingPeriod time.Duration = 50 * time.Millisecond

// not a true constant but only should change for tests, internal
var idlePollingPeriod = defaultIdlePollingPeriod

func SetIdlePollingPeriod(period time.Duration) {
	if period < 0 {
		period = defaultIdlePollingPeriod
	}
	idlePollingPeriod = period
}

func GetIdlePollingPeriod() time.Duration {
	return idlePollingPeriod
}
