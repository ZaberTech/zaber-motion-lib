package constants

import "time"

const NativeUnit = ""
const CommandArgument = "?"
const InfiniteArity = -1
const FwSixParkingAxisError = "Firmware of a version 6 and lower do not support parking of a specific axis."
const ParkingRejectedAxisError = "This device type does not support parking."
const MaxLineLength = 80
const UnitConversionTableKey = "zaber.unit_conversions"
const LabelStorageKey = "zaber.label"
const UnitConversionTableVersion = 1
const DriverEnableTimeout = 10 * time.Second
