package microscopy

import (
	"time"
	"zaber-motion-lib/internal/constants"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/units"
)

func (instance *microscopyManager) triggerCamera(request *pb.MicroscopeTriggerCameraRequest) errors.SdkError {
	if err := instance.devices.SetDigitalOutputSchedule(&pb.DeviceSetDigitalOutputScheduleRequest{
		InterfaceId:   request.InterfaceId,
		Device:        request.Device,
		ChannelNumber: request.ChannelNumber,
		Value:         1,
		FutureValue:   0,
		Delay:         request.Delay,
		Unit:          request.Unit,
	}); err != nil {
		return err
	} else if !request.Wait {
		return nil
	}

	waitMs := request.Delay
	if request.Unit != constants.NativeUnit {
		if converted, err := instance.units.ConvertStaticUnit(
			request.Delay, request.Unit, units.Units_TimeMs,
		); err != nil {
			return err
		} else {
			waitMs = converted
		}
	}

	<-time.After(time.Duration(waitMs) * time.Millisecond)

	return nil
}
