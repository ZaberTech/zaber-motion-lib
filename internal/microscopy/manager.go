package microscopy

import (
	"fmt"
	"strings"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"

	"github.com/elliotchance/pie/v2"
)

type microscopyManager struct {
	gateway            ioc.GatewayManager
	units              ioc.Units
	devices            ioc.DeviceManager
	storage            ioc.StorageManager
	autofocusProviders *utils.TokenMap[ioc.AutofocusProvider]
}

func NewMicroscopy(gateway ioc.GatewayManager, units ioc.Units, devices ioc.DeviceManager, storage ioc.StorageManager) ioc.Microscopy {
	instance := &microscopyManager{
		gateway:            gateway,
		units:              units,
		devices:            devices,
		storage:            storage,
		autofocusProviders: utils.NewTokenMap[ioc.AutofocusProvider](),
	}

	instance.register()

	return instance
}

func (instance *microscopyManager) register() {
	gateway := instance.gateway

	gateway.RegisterCallback("objective_changer/detect", func() pb.Message {
		return &pb.ObjectiveChangerRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.objectiveChangeDetect(request.(*pb.ObjectiveChangerRequest))
	})
	gateway.RegisterCallback("objective_changer/change", func() pb.Message {
		return &pb.ObjectiveChangerChangeRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.objectiveChangerChangeRequest(request.(*pb.ObjectiveChangerChangeRequest))
	})
	gateway.RegisterCallback("objective_changer/release", func() pb.Message {
		return &pb.ObjectiveChangerRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.objectiveChangerRelease(request.(*pb.ObjectiveChangerRequest))
	})
	gateway.RegisterCallback("objective_changer/get_datum", func() pb.Message {
		return &pb.ObjectiveChangerSetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.objectiveChangerGetDatum(request.(*pb.ObjectiveChangerSetRequest))
	})
	gateway.RegisterCallback("objective_changer/set_datum", func() pb.Message {
		return &pb.ObjectiveChangerSetRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.objectiveChangerSetDatum(request.(*pb.ObjectiveChangerSetRequest))
	})
	gateway.RegisterCallback("objective_changer/verify", func() pb.Message {
		return &pb.ObjectiveChangerRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.objectiveChangerVerify(request.(*pb.ObjectiveChangerRequest))
	})

	gateway.RegisterCallback("illuminator/detect", func() pb.Message {
		return &pb.FindDeviceRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.illuminatorDetectRequest(request.(*pb.FindDeviceRequest))
	})
	gateway.RegisterCallback("illuminator/on", func() pb.Message {
		return &pb.ChannelOn{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.illuminatorOn(request.(*pb.ChannelOn))
	})
	gateway.RegisterCallback("illuminator/is_on", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.illuminatorIsOn(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("illuminator/set_intensity", func() pb.Message {
		return &pb.ChannelSetIntensity{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.illuminatorSetIntensity(request.(*pb.ChannelSetIntensity))
	})
	gateway.RegisterCallback("illuminator/get_intensity", func() pb.Message {
		return &pb.AxisEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.illuminatorGetIntensity(request.(*pb.AxisEmptyRequest))
	})
	gateway.RegisterCallback("illuminator/verify", func() pb.Message {
		return &pb.DeviceEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.illuminatorVerify(request.(*pb.DeviceEmptyRequest))
	})

	gateway.RegisterCallback("microscope/detect", func() pb.Message {
		return &pb.MicroscopeFindRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.microscopeDetect(request.(*pb.MicroscopeFindRequest))
	})
	gateway.RegisterCallback("microscope/initialize", func() pb.Message {
		return &pb.MicroscopeInitRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.microscopeInitialize(request.(*pb.MicroscopeInitRequest))
	})
	gateway.RegisterCallback("microscope/is_initialized", func() pb.Message {
		return &pb.MicroscopeEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.microscopeIsInitialized(request.(*pb.MicroscopeEmptyRequest))
	})
	gateway.RegisterCallback("microscope/to_string", func() pb.Message {
		return &pb.MicroscopeEmptyRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.microscopeToString(request.(*pb.MicroscopeEmptyRequest))
	})

	gateway.RegisterCallback("autofocus/to_string", func() pb.Message {
		return &pb.EmptyAutofocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.autofocusToString(request.(*pb.EmptyAutofocusRequest))
	})
	gateway.RegisterCallback("autofocus/set_zero", func() pb.Message {
		return &pb.EmptyAutofocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusSetZero(request.(*pb.EmptyAutofocusRequest))
	})
	gateway.RegisterCallback("autofocus/focus_once", func() pb.Message {
		return &pb.AutofocusFocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusFocus(request.(*pb.AutofocusFocusRequest))
	})
	gateway.RegisterCallback("autofocus/start_focus_loop", func() pb.Message {
		return &pb.AutofocusFocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusFocus(request.(*pb.AutofocusFocusRequest))
	})
	gateway.RegisterCallback("autofocus/stop_focus_loop", func() pb.Message {
		return &pb.EmptyAutofocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusStopLoop(request.(*pb.EmptyAutofocusRequest))
	})
	gateway.RegisterCallback("autofocus/get_status", func() pb.Message {
		return &pb.EmptyAutofocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.autofocusGetStatus(request.(*pb.EmptyAutofocusRequest))
	})
	gateway.RegisterCallback("autofocus/sync_params", func() pb.Message {
		return &pb.EmptyAutofocusRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusSyncParamsRequest(request.(*pb.EmptyAutofocusRequest))
	})
	gateway.RegisterCallback("autofocus/set_objective_params", func() pb.Message {
		return &pb.AutofocusSetObjectiveParamsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.autofocusSetObjectiveParams(request.(*pb.AutofocusSetObjectiveParamsRequest))
	})
	gateway.RegisterCallback("autofocus/get_objective_params", func() pb.Message {
		return &pb.AutofocusGetObjectiveParamsRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return instance.autofocusGetObjectiveParams(request.(*pb.AutofocusGetObjectiveParamsRequest))
	})
	gateway.RegisterCallback("microscope/trigger_camera", func() pb.Message {
		return &pb.MicroscopeTriggerCameraRequest{}
	}, func(request pb.Message) (pb.Message, errors.SdkError) {
		return nil, instance.triggerCamera(request.(*pb.MicroscopeTriggerCameraRequest))
	})
}

// Locates device with specified device.id on the connection. Uses identified devices if it can.
func (instance *microscopyManager) locateDevice(
	requests *c.RequestManager, identifiedDevices []ioc.DeviceInfo, name string, deviceIDs []int, addressHint int,
) (int, errors.SdkError) {
	sameDevice := pie.Filter(identifiedDevices, func(device ioc.DeviceInfo) bool {
		id := device.GetProduct().DeviceID
		return pie.Contains(deviceIDs, id)
	})

	if addressHint == 0 {
		if len(sameDevice) == 1 {
			return sameDevice[0].GetAddress(), nil
		} else if len(sameDevice) > 1 {
			addressStr := strings.Join(pie.Sort(pie.Map(sameDevice, func(device ioc.DeviceInfo) string {
				return fmt.Sprintf("%d", device.GetAddress())
			})), ", ")
			return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"Multiple %s devices found on the connection. Provide an address to specify one (found %s).",
				name, addressStr), errors.DeviceDetectionFailed_MultipleFound)
		}

		replies, err := commands.BroadcastCommand(requests, c.Command{
			Command: "get device.id",
		})
		if err != nil {
			if err.Type() == pb.Errors_REQUEST_TIMEOUT {
				return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
					"Device %s not found on the connection.",
					name), errors.DeviceDetectionFailed_NothingFound)
			}
			return -1, err
		}

		sameDeviceReply := pie.Filter(replies, func(reply *c.Response) bool {
			id, _ := reply.DataAsInt()
			return pie.Contains(deviceIDs, id)
		})
		if len(sameDeviceReply) == 0 {
			return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"Device %s not found on the connection.",
				name), errors.DeviceDetectionFailed_NothingFound)
		} else if len(sameDeviceReply) > 1 {
			addressStr := strings.Join(pie.Sort(pie.Map(sameDeviceReply, func(reply *c.Response) string {
				return fmt.Sprintf("%d", reply.Device)
			})), ", ")
			return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"Multiple %s devices found on the connection. Provide an address to specify one (found %s).",
				name, addressStr), errors.DeviceDetectionFailed_MultipleFound)
		}

		return sameDeviceReply[0].Device, nil
	} else {
		sameAddress := pie.Filter(sameDevice, func(detected ioc.DeviceInfo) bool {
			return detected.GetAddress() == addressHint
		})

		if len(sameAddress) == 0 {
			reply, err := commands.SingleCommand(requests, c.Command{
				Device:  addressHint,
				Command: "get device.id",
			})
			if err != nil {
				if err.Type() == pb.Errors_REQUEST_TIMEOUT {
					return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
						"Device %s with address %d not found on the connection.",
						name, addressHint), errors.DeviceDetectionFailed_NothingFound)
				}
				return -1, err
			}

			retrievedID, err := reply.DataAsInt()
			if err != nil {
				return -1, err
			}

			if !pie.Contains(deviceIDs, retrievedID) {
				return -1, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
					"Device %s with address %d does not match (found device with device.id %d).",
					name, addressHint, retrievedID), errors.DeviceDetectionFailed_IncorrectFound)
			}
		}

		return addressHint, nil
	}
}

// Locates device with specified setting on the connection. Uses identified devices if it can and possibly returns device info.
func (instance *microscopyManager) locateDeviceBasedOnSetting(
	requests *c.RequestManager, identifiedDevices []ioc.DeviceInfo, name string, settingName string, addressHint int,
) (int, ioc.DeviceInfo, errors.SdkError) {
	sameDevice := pie.Filter(identifiedDevices, func(device ioc.DeviceInfo) bool {
		axes := device.GetAxes()
		return pie.Any(axes, func(axis ioc.AxisInfo) bool {
			_, ok := axis.GetCommandIssuingInfo().SettingsTable.GetParamInfo(settingName)
			return ok
		})
	})

	if addressHint == 0 {
		if len(sameDevice) == 1 {
			device := sameDevice[0]
			return device.GetAddress(), device, nil
		} else if len(sameDevice) > 1 {
			addressStr := strings.Join(pie.Sort(pie.Map(sameDevice, func(device ioc.DeviceInfo) string {
				return fmt.Sprintf("%d", device.GetAddress())
			})), ", ")
			return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"Multiple %s devices found on the connection. Provide an address to specify one (found %s).",
				name, addressStr), errors.DeviceDetectionFailed_MultipleFound)
		}

		replies, err := commands.BroadcastCommand(requests, c.Command{
			Command: "get " + settingName,
		})
		if err != nil {
			if err.Type() == pb.Errors_REQUEST_TIMEOUT {
				return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
					"%s not found on the connection.",
					name), errors.DeviceDetectionFailed_NothingFound)
			}
			return -1, nil, err
		}

		sameDeviceReply := pie.Filter(replies, func(reply *c.Response) bool {
			return reply.ReplyFlag == "OK"
		})
		if len(sameDeviceReply) == 0 {
			return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"%s not found on the connection.",
				name), errors.DeviceDetectionFailed_NothingFound)
		} else if len(sameDeviceReply) > 1 {
			addressStr := strings.Join(pie.Sort(pie.Map(sameDeviceReply, func(reply *c.Response) string {
				return fmt.Sprintf("%d", reply.Device)
			})), ", ")
			return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
				"Multiple %s devices found on the connection. Provide an address to specify one (found %s).",
				name, addressStr), errors.DeviceDetectionFailed_MultipleFound)
		}

		return sameDeviceReply[0].Device, nil, nil
	} else {
		sameAddress := pie.Filter(sameDevice, func(detected ioc.DeviceInfo) bool {
			return detected.GetAddress() == addressHint
		})

		if len(sameAddress) != 0 {
			return addressHint, sameAddress[0], nil
		}

		_, err := commands.SingleCommand(requests, c.Command{
			Device:  addressHint,
			Command: "get " + settingName,
		})
		if err != nil {
			if err.Type() == pb.Errors_REQUEST_TIMEOUT {
				return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
					"%s with address %d not found on the connection.",
					name, addressHint), errors.DeviceDetectionFailed_NothingFound)
			} else if commands.IsBadCommandErr(err) {
				return -1, nil, errors.ErrDeviceDetectionFailed(fmt.Sprintf(
					"Device with address %d does not have setting %s and is not the expected %s.",
					addressHint, settingName, name), errors.DeviceDetectionFailed_IncorrectFound)
			}
			return -1, nil, err
		}

		return addressHint, nil, nil
	}
}

func (instance *microscopyManager) RegisterAutofocusProvider(provider ioc.AutofocusProvider) int32 {
	return instance.autofocusProviders.Store(provider)
}

func (instance *microscopyManager) RemoveAutofocusProvider(id int32) (ioc.AutofocusProvider, bool) {
	return instance.autofocusProviders.Delete(id)
}

func (instance *microscopyManager) getAutofocusProvider(id int32) (ioc.AutofocusProvider, errors.SdkError) {
	provider, err := instance.autofocusProviders.Load(id)
	if err != nil {
		return nil, errors.ErrInvalidOperation(fmt.Sprintf("Autofocus provider with id %d not found.", id))
	}
	return provider, nil
}

func (instance *microscopyManager) GetAutofocusProvider(id int32) (ioc.AutofocusProvider, bool) {
	provider, err := instance.autofocusProviders.Load(id)
	return provider, err == nil
}
