package microscopy

import (
	"fmt"
	"regexp"
	cmd "zaber-motion-lib/internal/commandbuilding"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/constants"
	"zaber-motion-lib/internal/devices"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

var FaultWarningFlags, _ = regexp.Compile(`^(F\w)$`)

const lampStatusSetting = "lamp.status"

type IlluminatorChannel struct {
	fluxMax float64
}

func (instance *microscopyManager) illuminatorDetectRequest(request *pb.FindDeviceRequest) (*pb.FindDeviceResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	return instance.illuminatorDetect(requests, request)
}

func (instance *microscopyManager) illuminatorDetect(requests *c.RequestManager, request *pb.FindDeviceRequest) (*pb.FindDeviceResponse, errors.SdkError) {
	allDevices, err := instance.devices.GetAllIdentifiedDevices(request)
	if err != nil {
		return nil, err
	}

	address, illuminator, err := instance.locateDeviceBasedOnSetting(requests, allDevices, "Illuminator", lampStatusSetting, int(request.DeviceAddress))
	if err != nil {
		return nil, err
	}
	target := &pb.AxisEmptyRequest{
		InterfaceId: request.InterfaceId, Device: int32(address),
	}

	var axisCount int
	if illuminator != nil {
		axisCount = len(illuminator.GetAxes())
	} else {
		resp, err := commands.SingleCommandDevice(requests, target, "get system.axiscount")
		if err != nil {
			return nil, err
		}
		axisCount, err = resp.DataAsInt()
		if err != nil {
			return nil, err
		}
	}

	for i := 0; i < axisCount; i++ {
		target.Axis = int32(i + 1)
		_, err = instance.illuminatorGetChannelInfoBase(requests, target, true)
		if err != nil {
			if commands.IsBadCommandErr(err) {
				continue
			}
			return nil, err
		}
	}

	return &pb.FindDeviceResponse{
		Address: int32(address),
	}, nil
}

func (instance *microscopyManager) illuminatorOn(request *pb.ChannelOn) errors.SdkError {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}

	cmd := "lamp off"
	if request.On {
		cmd = "lamp on"
	}
	resp, err := commands.SingleCommandAxis(requests, request, cmd)
	if err != nil {
		if commands.IsBadCommandErr(err) || commands.IsBadDataErr(err) {
			err.AddToMessage("The channel is likely in fault, not connected, or not supported.")
		}
		return err
	}

	if commands.HasFaultOrWarning(resp) {
		warnings, _, err := devices.GetAxisWarnings(requests, request.Device, request.Axis, true)
		if err != nil {
			return err
		}
		err = devices.CheckErrorFlags(warnings, FaultWarningFlags, func(reason string, flags []string) errors.SdkError {
			return errors.ErrOperationFailed(reason, flags, request.Device, request.Axis)
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (instance *microscopyManager) illuminatorIsOn(request *pb.AxisEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	resp, err := commands.SingleRequestAxis(instance.devices, request, "get "+lampStatusSetting)
	if err != nil {
		return nil, err
	}
	status, err := resp.DataAsInt()
	if err != nil {
		return nil, err
	}
	return &pb.BoolResponse{
		Value: status == 2,
	}, nil
}

func (instance *microscopyManager) illuminatorGetChannelInfo(requests *c.RequestManager, request ioc.AxisTarget) (*IlluminatorChannel, errors.SdkError) {
	return instance.illuminatorGetChannelInfoBase(requests, request, false)
}

func (instance *microscopyManager) illuminatorGetChannelInfoBase(requests *c.RequestManager, request ioc.AxisTarget, renew bool) (*IlluminatorChannel, errors.SdkError) {
	info, err := instance.devices.GetUnidentifiedDeviceInfo(request)
	if err != nil {
		return nil, err
	}

	key := fmt.Sprintf("lamp.channel%d", request.GetAxis())

	lampInfo, ok := info.GetAssociatedStruct(key)
	if ok && !renew {
		return lampInfo.(*IlluminatorChannel), nil
	}

	resp, err := commands.SingleCommandAxis(requests, request, "get lamp.flux.max")
	if err != nil {
		return nil, err
	}
	fluxMax, err := resp.DataAsFloat()
	if err != nil {
		return nil, err
	}

	lampInfo = &IlluminatorChannel{
		fluxMax: fluxMax,
	}
	info.AssociateStruct(key, lampInfo)

	return lampInfo.(*IlluminatorChannel), nil
}

func (instance *microscopyManager) illuminatorSetIntensity(request *pb.ChannelSetIntensity) errors.SdkError {
	if request.Intensity < 0 || request.Intensity > 1 || request.Intensity != request.Intensity {
		return errors.ErrInvalidArgument("Intensity must be between 0 and 1.")
	}

	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}

	info, err := instance.illuminatorGetChannelInfo(requests, request)
	if err != nil {
		return err
	}

	fluxToSet := info.fluxMax * request.Intensity
	setFluxCmd, err := instance.devices.BuildCommand(request,
		[]string{"set", "lamp.flux", constants.CommandArgument},
		[]ioc.CommandArg{
			cmd.CommandArgImpl{Value: fluxToSet},
		})
	if err != nil {
		return err
	}
	_, err = commands.SingleCommandAxis(requests, request, setFluxCmd)
	return err
}

func (instance *microscopyManager) illuminatorGetIntensity(request *pb.AxisEmptyRequest) (*pb.DoubleResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}

	info, err := instance.illuminatorGetChannelInfo(requests, request)
	if err != nil {
		return nil, err
	}

	resp, err := commands.SingleCommandAxis(requests, request, "get lamp.flux")
	if err != nil {
		return nil, err
	}
	flux, err := resp.DataAsFloat()
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{
		Value: flux / info.fluxMax,
	}, nil
}

func (instance *microscopyManager) illuminatorVerify(request *pb.DeviceEmptyRequest) errors.SdkError {
	isIlluminator, err := instance.illuminatorIsIlluminator(request)
	if err != nil {
		return err
	} else if !isIlluminator {
		return errors.ErrInvalidArgument(fmt.Sprintf("Device with address %d is not an Illuminator.", request.Device))
	}
	return nil
}

func (instance *microscopyManager) illuminatorIsIlluminator(request *pb.DeviceEmptyRequest) (bool, errors.SdkError) {
	deviceInfo, err := instance.devices.GetUnidentifiedDeviceInfo(request)
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return true, nil
		}
		return false, err
	}
	identifiedInfo := deviceInfo.GetIdentified()
	if identifiedInfo == nil {
		return true, nil
	}
	axes := identifiedInfo.GetAxes()
	return pie.Any(axes, func(axis ioc.AxisInfo) bool {
		_, exists := axis.GetCommandIssuingInfo().SettingsTable.GetParamInfo(lampStatusSetting)
		return exists
	}), nil
}
