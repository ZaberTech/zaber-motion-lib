package microscopy

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/devices"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
)

type autofocus struct {
	target     autofocusTarget
	objectives map[int]*autofocusObjective
	lock       sync.Mutex
}

type autofocusObjective struct {
	number int
	kp     *float64
	ki     *float64
}

func (objective *autofocusObjective) GetNumber() int {
	return objective.number
}

type autofocusTarget interface {
	ioc.InterfaceTarget
	GetFocusAddress() int32
	GetFocusAxis() int32
	GetTurretAddress() int32
	GetProviderId() int32
}

const objectiveStorageKeyPrefix = "zaber.microscope.af.obj."

func getObjectiveStoragePrefix(objective int) string {
	return fmt.Sprintf("%s%d.", objectiveStorageKeyPrefix, objective)
}

func parseObjectiveStorageKey(key string) (objective int, param string, err error) {
	parts := strings.Split(key, ".")
	objective, parseErr := strconv.Atoi(parts[len(parts)-2])
	if parseErr != nil {
		return 0, "", parseErr
	}
	return objective, parts[len(parts)-1], nil
}

var whitespaceRegexp = regexp.MustCompile(`\s+`)

func (instance *microscopyManager) autofocusToString(request *pb.EmptyAutofocusRequest) (*pb.StringResponse, errors.SdkError) {
	provider, err := instance.autofocusProviders.Load(request.ProviderId)
	if err != nil {
		return &pb.StringResponse{Value: fmt.Sprintf("Unknown provider %d", request.ProviderId)}, nil
	}
	return &pb.StringResponse{Value: provider.ToString()}, nil
}

func (instance *microscopyManager) autofocusSetZero(request *pb.EmptyAutofocusRequest) errors.SdkError {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}
	_, provider, err := instance.getAutofocus(requests, request, nil)
	if err != nil {
		return err
	}
	return provider.SetFocusZero()
}

func (instance *microscopyManager) autofocusGetStatus(request *pb.EmptyAutofocusRequest) (*pb.AutofocusGetStatusResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	_, provider, err := instance.getAutofocus(requests, request, nil)
	if err != nil {
		return nil, err
	}

	providerStatus, err := provider.GetStatus()
	if err != nil {
		return nil, err
	}

	focusTarget := getFocusAxisTarget(request)
	reply, err := commands.SingleCommandAxis(requests, focusTarget, "get motion.tracking.settle.tolerance.met")
	if err != nil {
		return nil, err
	}
	inFocus := reply.Data != "0"

	return &pb.AutofocusGetStatusResponse{Status: &pb.MicroscopyAutofocusStatus{
		InRange: providerStatus.GetInRange(),
		InFocus: inFocus,
	}}, nil
}

func (instance *microscopyManager) autofocusSyncParamsRequest(request *pb.EmptyAutofocusRequest) errors.SdkError {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}

	_, _, err = instance.getAutofocus(requests, request, &getAutofocusOptions{
		renew: true, // renewing the autofocus essentially means re-syncing everything
	})
	return err
}

func getFocusAxisTarget(request autofocusTarget) *pb.DeviceMoveRequest {
	return &pb.DeviceMoveRequest{
		InterfaceId:   request.GetInterfaceId(),
		Device:        request.GetFocusAddress(),
		Axis:          request.GetFocusAxis(),
		WaitUntilIdle: true,
	}
}

func (instance *microscopyManager) autofocusFocus(request *pb.AutofocusFocusRequest) errors.SdkError {
	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return err
	}
	_, provider, err := instance.getAutofocus(comm.Requests(), request, nil)
	if err != nil {
		return err
	}

	if err := provider.IsReadyToFocus(ioc.IsReadyToFocusOptions{
		IgnoreInRange: request.Scan,
	}); err != nil {
		return err
	}

	target := getFocusAxisTarget(request)
	target.WaitUntilIdle = request.Once
	cmd := "move"
	if request.Scan {
		cmd += " scan"
	}
	cmd += " track"
	if request.Once {
		cmd += " once"
	}

	if request.Timeout < 0 || !request.Once {
		if err := devices.MoveCommand(comm, target, cmd); err != nil {
			return addToMovementFailed(err)
		}
	} else {
		timeout := time.Duration(request.Timeout) * time.Millisecond
		if err, timedOut := devices.MoveCommandTimeout(comm, target, cmd, timeout); err != nil {
			return addToMovementFailed(err)
		} else if timedOut {
			return errors.ErrTimeout("The autofocus operation timed out.")
		}
	}

	return nil
}

func (instance *microscopyManager) autofocusStopLoop(request *pb.EmptyAutofocusRequest) errors.SdkError {
	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return err
	}
	requests := comm.Requests()

	target := getFocusAxisTarget(request)
	reply, err := commands.SingleCommandAxis(requests, target, "")
	if err != nil {
		return err
	}

	if err := devices.CheckWarningsForWaitUntilIdle(requests, reply, devices.WaitUntilIdleOptions{
		CheckErrors: true,
	}); err != nil {
		addToMovementFailed(err)
		return err
	}

	if reply.Status == "IDLE" {
		return nil
	}

	return devices.MoveCommand(comm, target, "stop")
}

func addToMovementFailed(err errors.SdkError) errors.SdkError {
	if err != nil && err.Type() == pb.Errors_MOVEMENT_FAILED {
		err.AddToMessage("The autofocus loop has either reached the tracking limits or moved out of range.")
	}
	return err
}

type getAutofocusOptions struct {
	renew bool
}

func (instance *microscopyManager) getAutofocus(
	requests *c.RequestManager, target autofocusTarget, options *getAutofocusOptions,
) (*autofocus, ioc.AutofocusProvider, errors.SdkError) {
	renew := options != nil && options.renew

	provider, err := instance.getAutofocusProvider(target.GetProviderId())
	if err != nil {
		return nil, nil, err
	}

	focusDevice, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
		InterfaceId: target.GetInterfaceId(),
		Device:      target.GetFocusAddress(),
	})
	if err != nil {
		return nil, nil, err
	}

	dataKey := fmt.Sprintf("autofocus_%d", target.GetFocusAxis())
	associatedData, found := focusDevice.GetAssociatedStruct(dataKey)
	if found && !renew {
		focusData := associatedData.(*autofocus)
		if isAutofocusSameTarget(target, focusData.target) {
			return focusData, provider, nil
		}
	}

	focusData := &autofocus{
		target: &pb.EmptyAutofocusRequest{
			ProviderId:    target.GetProviderId(),
			InterfaceId:   target.GetInterfaceId(),
			FocusAddress:  target.GetFocusAddress(),
			FocusAxis:     target.GetFocusAxis(),
			TurretAddress: target.GetTurretAddress(),
		},
	}
	focusDevice.AssociateStruct(dataKey, focusData)

	if _, err := focusData.loadAutofocusObjectives(requests, target, 0); err != nil {
		return nil, nil, err
	}

	if err := instance.autofocusSyncCurrentObjective(requests, focusData, provider); err != nil {
		return nil, nil, err
	}

	return focusData, provider, nil
}

func isAutofocusSameTarget(target1 autofocusTarget, target2 autofocusTarget) bool {
	return target1.GetFocusAddress() == target2.GetFocusAddress() &&
		target1.GetFocusAxis() == target2.GetFocusAxis() &&
		target1.GetTurretAddress() == target2.GetTurretAddress() &&
		target1.GetProviderId() == target2.GetProviderId()
}

func (focusData *autofocus) getObjective(number int) (*autofocusObjective, bool) {
	focusData.lock.Lock()
	defer focusData.lock.Unlock()

	objective, found := focusData.objectives[number]
	return objective, found
}

func (focusData *autofocus) loadAutofocusObjectives(
	requests *c.RequestManager, target autofocusTarget, objective int,
) ([]*autofocusObjective, errors.SdkError) {
	prefix := objectiveStorageKeyPrefix
	isSpecificObjective := objective > 0
	if isSpecificObjective {
		prefix = getObjectiveStoragePrefix(objective)
	}

	replies, err := commands.SingleCommandMultiResponse(requests, c.Command{
		Device:  int(target.GetFocusAddress()),
		Axis:    int(target.GetFocusAxis()),
		Command: "storage axis print prefix " + prefix,
	})
	if err != nil {
		return nil, err
	}

	focusData.lock.Lock()
	defer focusData.lock.Unlock()

	if isSpecificObjective {
		delete(focusData.objectives, objective)
	} else {
		focusData.objectives = make(map[int]*autofocusObjective)
	}

	var loaded []*autofocusObjective

	for _, reply := range replies[1:] {
		data := whitespaceRegexp.Split(reply.Data, -1)
		dataErr := errors.ErrInvalidData("cannot parse data: " + reply.Data)

		if data[0] != "set" {
			continue
		}

		objectiveNum, param, parseErr := parseObjectiveStorageKey(data[1])
		if parseErr != nil {
			return nil, dataErr
		}
		objective, ok := focusData.objectives[objectiveNum]
		if !ok {
			objective = &autofocusObjective{number: objectiveNum}
			focusData.objectives[objectiveNum] = objective
			loaded = append(loaded, objective)
		}

		value, parseErr := strconv.ParseFloat(data[2], 64)
		if parseErr != nil {
			return nil, dataErr
		}

		switch param {
		case "kp":
			objective.kp = &value
		case "ki":
			objective.ki = &value
		}
	}

	return loaded, nil
}

func (instance *microscopyManager) getAutofocusFromObjectiveChanger(target objectiveChangerTarget) (*autofocus, ioc.AutofocusProvider, bool) {
	focusDevice, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
		InterfaceId: target.GetInterfaceId(),
		Device:      target.GetFocusAddress(),
	})
	if err != nil {
		return nil, nil, false
	}

	dataKey := fmt.Sprintf("autofocus_%d", target.GetFocusAxis())
	associatedData, found := focusDevice.GetAssociatedStruct(dataKey)
	if !found {
		return nil, nil, false
	}

	focusData := associatedData.(*autofocus)
	if isConsistent := focusData.target.GetTurretAddress() == target.GetTurretAddress(); !isConsistent {
		return nil, nil, false
	}

	provider, err := instance.getAutofocusProvider(focusData.target.GetProviderId())
	if err != nil {
		return nil, nil, false
	}

	return focusData, provider, true
}

func (instance *microscopyManager) autofocusGetObjectiveParams(
	request *pb.AutofocusGetObjectiveParamsRequest,
) (*pb.AutofocusGetObjectiveParamsResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	autofocus, provider, err := instance.getAutofocus(requests, request, nil)
	if err != nil {
		return nil, err
	}

	objectiveNum := int(request.Objective)

	// reload to ensure freshness
	objectives, err := autofocus.loadAutofocusObjectives(requests, autofocus.target, objectiveNum)
	if err != nil {
		return nil, err
	}

	kp := &pb.NamedParameter{
		Name: "control_loop_kp",
	}
	ki := &pb.NamedParameter{
		Name: "control_loop_ki",
	}
	params := []*pb.NamedParameter{kp, ki}

	if len(objectives) > 0 {
		objective := objectives[0]
		kp.Value = objective.kp
		ki.Value = objective.ki
	}

	providerParams, err := provider.GetObjectiveParameters(objectiveNum)
	if err != nil {
		return nil, err
	}
	params = append(params, providerParams...)

	return &pb.AutofocusGetObjectiveParamsResponse{Parameters: params}, nil
}

func (instance *microscopyManager) autofocusSetObjectiveParams(request *pb.AutofocusSetObjectiveParamsRequest) errors.SdkError {
	if request.Objective < 1 {
		return errors.ErrInvalidArgument("Objective number must be greater than 0")
	}

	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}
	autofocus, provider, err := instance.getAutofocus(requests, request, nil)
	if err != nil {
		return err
	}

	focusTarget := getFocusAxisTarget(autofocus.target)
	objectiveNum := int(request.Objective)

	for _, param := range request.Parameters {
		var key string
		switch param.Name {
		case "control_loop_kp":
			key = "kp"
		case "control_loop_ki":
			key = "ki"
		default:
			if !provider.IsValidParameter(param.Name) {
				return errors.ErrInvalidArgument("Invalid parameter: " + param.Name)
			}
			continue
		}
		storageKey := getObjectiveStoragePrefix(objectiveNum) + key

		if param.Value != nil {
			if err := instance.storage.SetNumber(focusTarget, storageKey, *param.Value); err != nil {
				return err
			}
		} else {
			if _, err := instance.storage.Erase(focusTarget, storageKey); err != nil {
				return err
			}
		}
	}

	if err := provider.SetObjectiveParameters(objectiveNum, request.Parameters); err != nil {
		return err
	}

	objectives, err := autofocus.loadAutofocusObjectives(requests, autofocus.target, objectiveNum)
	if err != nil {
		return err
	} else if len(objectives) == 0 {
		return nil
	}

	currentObjective, err := instance.autofocusGetCurrentObjective(requests, autofocus)
	if err != nil {
		return err
	} else if currentObjective != objectiveNum {
		return nil
	}

	return instance.autofocusSyncTrackingParams(requests, autofocus, objectives[0])
}

func (instance *microscopyManager) autofocusGetCurrentObjective(
	requests *c.RequestManager,
	autofocus *autofocus,
) (int, errors.SdkError) {
	if noTurret := autofocus.target.GetTurretAddress() == 0; noTurret {
		return 1, nil
	}

	reply, err := commands.SingleCommand(requests, c.Command{
		Device:  int(autofocus.target.GetTurretAddress()),
		Axis:    MORAxisNumber,
		Command: "get motion.index.num",
	})
	if err != nil {
		return 0, err
	}

	return reply.DataAsInt()
}

func (instance *microscopyManager) autofocusSyncCurrentObjective(
	requests *c.RequestManager,
	autofocus *autofocus,
	provider ioc.AutofocusProvider,
) errors.SdkError {
	currentObjective, err := instance.autofocusGetCurrentObjective(requests, autofocus)
	if err != nil {
		return err
	} else if currentObjective <= 0 { // likely not homed
		return nil
	}

	providerObjective, err := provider.GetCurrentObjective()
	if err != nil {
		return err
	} else if providerObjective != currentObjective {
		if err := provider.SetCurrentObjective(currentObjective); err != nil {
			return err
		}
	}

	objective, found := autofocus.getObjective(currentObjective)
	if !found {
		return nil
	}

	return instance.autofocusSyncTrackingParams(requests, autofocus, objective)
}

func (instance *microscopyManager) autofocusSyncTrackingParams(
	requests *c.RequestManager,
	autofocus *autofocus,
	objective *autofocusObjective,
) errors.SdkError {
	if objective.ki == nil && objective.kp == nil {
		return nil
	}

	focusTarget := getFocusAxisTarget(autofocus.target)

	reply, err := commands.SingleCommandAxis(requests, focusTarget, "get motion.tracking.kp motion.tracking.ki")
	if err != nil {
		return err
	}
	trackingParams, err := reply.DataAsFloatArrayArray(false)
	if err != nil {
		return err
	}
	kp := trackingParams[0][0]
	ki := trackingParams[1][0]

	if objective.kp != nil && !trackingParamsEq(kp, *objective.kp) {
		_, err := commands.SingleCommandAxis(requests, focusTarget, "set motion.tracking.kp "+fstr(objective.kp))
		if err != nil {
			return err
		}
	}

	if objective.ki != nil && !trackingParamsEq(ki, *objective.ki) {
		_, err := commands.SingleCommandAxis(requests, focusTarget, "set motion.tracking.ki "+fstr(objective.ki))
		if err != nil {
			return err
		}
	}

	return nil
}

func trackingParamsEq(value1, value2 float64) bool {
	return math.Abs(value1-value2) < 0.001
}

func fstr(number *float64) string {
	if number == nil {
		return ""
	}
	return strconv.FormatFloat(*number, 'f', -1, 64)
}
