package microscopy

import (
	"fmt"
	"math"
	"sync"
	"zaber-motion-lib/internal/commands"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/devices"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"

	"github.com/elliotchance/pie/v2"
)

var MORDeviceID = []int{53115}
var LDADeviceID = []int{50852, 53217}

const MORAxisNumber = 1

const offsetAfterCrashing = 1e6 // 1mm
const focusDatumSetting = "limit.home.offset"

type objectiveChanger struct {
	lock         sync.Mutex
	focusTarget  ioc.WaitUntilIdleTarget
	focusDatum   float64
	turretTarget ioc.WaitUntilIdleTarget
}

type objectiveChangerTarget interface {
	ioc.InterfaceTarget
	GetTurretAddress() int32
	GetFocusAddress() int32
	GetFocusAxis() int32
}

func (instance *microscopyManager) getObjectiveChanger(requests *c.RequestManager, target objectiveChangerTarget) (*objectiveChanger, errors.SdkError) {
	return instance.getObjectiveChangerBase(requests, target, false)
}

func (instance *microscopyManager) getObjectiveChangerBase(requests *c.RequestManager, target objectiveChangerTarget, renew bool) (*objectiveChanger, errors.SdkError) {
	turret, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
		InterfaceId: target.GetInterfaceId(),
		Device:      target.GetTurretAddress(),
	})
	if err != nil {
		return nil, err
	}

	data, found := turret.GetAssociatedStruct("objective_changer")
	if found && !renew {
		changer := data.(*objectiveChanger)
		if isConsistent := changer.focusTarget.GetDevice() == target.GetFocusAddress() &&
			changer.focusTarget.GetAxis() == target.GetFocusAxis(); isConsistent {
			return changer, nil
		}
	}

	turretTarget := &pb.DeviceMoveRequest{
		InterfaceId:   target.GetInterfaceId(),
		Device:        target.GetTurretAddress(),
		Axis:          MORAxisNumber,
		WaitUntilIdle: true,
	}
	focusTarget := &pb.DeviceMoveRequest{
		InterfaceId:   target.GetInterfaceId(),
		Device:        target.GetFocusAddress(),
		Axis:          target.GetFocusAxis(),
		WaitUntilIdle: true,
	}

	datumReply, err := commands.SingleCommandAxis(requests, focusTarget, "get "+focusDatumSetting)
	if err != nil {
		return nil, err
	}
	focusDatum, err := datumReply.DataAsFloat()
	if err != nil {
		return nil, err
	}

	changer := &objectiveChanger{
		turretTarget: turretTarget,
		focusTarget:  focusTarget,
		focusDatum:   focusDatum,
	}
	turret.AssociateStruct("objective_changer", changer)

	return changer, nil
}

func (instance *microscopyManager) objectiveChangeDetect(request *pb.ObjectiveChangerRequest) (*pb.ObjectiveChangerCreateResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}

	allDevices, err := instance.devices.GetAllIdentifiedDevices(request)
	if err != nil {
		return nil, err
	}

	turretAddress, err := instance.locateDevice(requests, allDevices, "X-MOR", MORDeviceID, int(request.TurretAddress))
	if err != nil {
		return nil, err
	}

	focusAddress, err := instance.locateDevice(requests, allDevices, "X-LDA", LDADeviceID, int(request.FocusAddress))
	if err != nil {
		return nil, err
	}

	changer, err := instance.getObjectiveChangerBase(requests, &pb.ObjectiveChangerRequest{
		InterfaceId:   request.InterfaceId,
		TurretAddress: int32(turretAddress),
		FocusAddress:  int32(focusAddress),
		// Detect looks for objective changer based on LDA device.id, therefore axis is always 1.
		FocusAxis: 1,
	}, true)
	if err != nil {
		return nil, err
	}

	return &pb.ObjectiveChangerCreateResponse{
		Turret:       changer.turretTarget.GetDevice(),
		FocusAddress: changer.focusTarget.GetDevice(),
		FocusAxis:    changer.focusTarget.GetAxis(),
	}, nil
}

func (instance *microscopyManager) objectiveChangerChangeRequest(request *pb.ObjectiveChangerChangeRequest) errors.SdkError {
	if request.Objective <= 0 || request.Objective > 4 {
		return errors.ErrInvalidArgument(fmt.Sprintf("Objective argument must be in <1, 4> range (was %d).", request.Objective))
	}
	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return err
	}
	return instance.objectiveChangerChange(comm, request, ObjectiveChangerModes.Change)
}

type ObjectiveChangerMode int

var ObjectiveChangerModes = struct {
	Change    ObjectiveChangerMode
	Home      ObjectiveChangerMode
	ForceHome ObjectiveChangerMode
}{
	Change:    0,
	Home:      1,
	ForceHome: 2,
}

func (instance *microscopyManager) objectiveChangerChange(
	comm ioc.ASCIICommunicationInterface,
	request *pb.ObjectiveChangerChangeRequest,
	mode ObjectiveChangerMode,
) errors.SdkError {
	requests := comm.Requests()

	changer, err := instance.getObjectiveChanger(requests, request)
	if err != nil {
		return err
	}

	isFocusHomed, err := checkAxisWarnings(requests, changer.focusTarget.GetDevice(), changer.focusTarget.GetAxis(), true)
	if err != nil {
		return err
	}
	homeFocus := !isFocusHomed || mode == ObjectiveChangerModes.ForceHome

	focusPosition := changer.getFocusDatum()
	if request.FocusOffset != nil && request.FocusOffset.Value != 0 {
		offset := request.FocusOffset
		offsetNative, err := instance.devices.ConvertUnitSetting(changer.focusTarget, "pos", offset.Value, offset.Unit, false)
		if err != nil {
			return err
		}
		focusPosition += offsetNative
	}

	currentIndex, isTurretHomed, err := getCurrentIndexPosition(requests, request, true)
	if err != nil {
		return err
	}
	changeObjective := mode == ObjectiveChangerModes.Change && currentIndex != int(request.Objective)
	homeTurret := !isTurretHomed || mode == ObjectiveChangerModes.ForceHome

	if changeObjective || homeTurret {
		if err := moveFocusMin(comm, changer, isFocusHomed); err != nil {
			return err
		}

		if homeTurret {
			if err := devices.MoveCommand(comm, changer.turretTarget, "home"); err != nil {
				return err
			}
		}

		if mode == ObjectiveChangerModes.Change {
			if err := devices.MoveCommand(comm, changer.turretTarget, fmt.Sprintf("move index %d", request.Objective)); err != nil {
				return err
			}
		}
	}

	if homeFocus {
		if err := devices.MoveCommand(comm, changer.focusTarget, "home"); err != nil {
			return err
		}
	}

	if mode == ObjectiveChangerModes.Change {
		if err := devices.MoveCommand(comm, changer.focusTarget, fmt.Sprintf("move abs %.0f", focusPosition)); err != nil {
			return err
		}
	}

	if autofocus, provider, found := instance.getAutofocusFromObjectiveChanger(request); found {
		if err := instance.autofocusSyncCurrentObjective(requests, autofocus, provider); err != nil {
			return err
		}
	}

	return nil
}

func (instance *microscopyManager) objectiveChangerRelease(request *pb.ObjectiveChangerRequest) errors.SdkError {
	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return err
	}
	requests := comm.Requests()

	changer, err := instance.getObjectiveChanger(requests, request)
	if err != nil {
		return err
	}

	isFocusHomed, err := checkAxisWarnings(requests, changer.focusTarget.GetDevice(), changer.focusTarget.GetAxis(), true)
	if err != nil {
		return err
	}

	return moveFocusMin(comm, changer, isFocusHomed)
}

func (instance *microscopyManager) objectiveChangerGetDatum(request *pb.ObjectiveChangerSetRequest) (*pb.DoubleResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}

	changer, err := instance.getObjectiveChanger(requests, request)
	if err != nil {
		return nil, err
	}

	datumConverted, err := instance.devices.ConvertUnitSetting(changer.focusTarget, "pos", changer.getFocusDatum(), request.Unit, true)
	if err != nil {
		return nil, err
	}

	return &pb.DoubleResponse{Value: datumConverted}, nil
}

func (instance *microscopyManager) objectiveChangerSetDatum(request *pb.ObjectiveChangerSetRequest) errors.SdkError {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return err
	}

	changer, err := instance.getObjectiveChanger(requests, request)
	if err != nil {
		return err
	}

	focusDatum, err := instance.devices.ConvertUnitSetting(changer.focusTarget, "pos", request.Value, request.Unit, false)
	if err != nil {
		return err
	}
	focusDatum = math.Round(focusDatum)

	_, err = commands.SingleCommandAxis(requests, changer.focusTarget, fmt.Sprintf("set %s %.0f", focusDatumSetting, focusDatum))
	if err != nil {
		return err
	}

	changer.lock.Lock()
	defer changer.lock.Unlock()
	changer.focusDatum = focusDatum

	return nil
}

func getCurrentIndexPosition(requests *c.RequestManager, request objectiveChangerTarget, clearWarnings bool) (currentIndex int, isHomed bool, err errors.SdkError) {
	currentIndexReply, err := commands.SingleCommand(requests, c.Command{
		Device:  int(request.GetTurretAddress()),
		Axis:    MORAxisNumber,
		Command: "get motion.index.num",
	})
	if err != nil {
		return 0, false, err
	}
	currentIndex, err = currentIndexReply.DataAsInt()
	if err != nil {
		return 0, false, err
	}

	isHomed = true
	if commands.HasWarnings(currentIndexReply) {
		if isHomed, err = checkAxisWarnings(requests, request.GetTurretAddress(), MORAxisNumber, clearWarnings); err != nil {
			return 0, false, err
		}
	}

	return currentIndex, isHomed, nil
}

func moveFocusMin(comm ioc.ASCIICommunicationInterface, changer *objectiveChanger, isHomed bool) errors.SdkError {
	if isHomed {
		return devices.MoveCommand(comm, changer.focusTarget, "move min")
	}

	if err := devices.MoveCommand(comm, changer.focusTarget, "tools gotolimit hardstop neg 1 0"); err != nil {
		if commands.IsBadCommandErr(err) {
			return moveFocusMinCompatible(comm, changer)
		} else {
			return err
		}
	}

	return nil
}

// Moves the axis to the end-stop using compatible commands for older firmware versions.
func moveFocusMinCompatible(comm ioc.ASCIICommunicationInterface, changer *objectiveChanger) errors.SdkError {
	requests := comm.Requests()
	focus := changer.focusTarget

	speedReply, err := commands.SingleCommandAxis(requests, focus, "get limit.approach.maxspeed")
	if err != nil {
		return err
	}

	_, err = commands.SingleCommandAxis(requests, focus, fmt.Sprintf("move min %s", speedReply.Data))
	if err != nil {
		return err
	}

	stalled := false
	for !stalled {
		warnings, reply, err := devices.GetAxisWarnings(requests, focus.GetDevice(), focus.GetAxis(), true)
		if err != nil {
			return err
		}

		for _, flag := range []string{"WS", "FS"} {
			if warnings.Remove(flag) {
				stalled = true
			}
		}

		if err := devices.CheckErrorFlags(warnings, devices.MovementErrorWarningFlags,
			errors.BindErrMovementFailed(focus.GetDevice(), focus.GetAxis())); err != nil {
			return err
		}

		if reply.Status == "IDLE" {
			break
		}
	}

	if stalled {
		if err := devices.MoveCommand(comm, focus, fmt.Sprintf("move rel %.0f", offsetAfterCrashing)); err != nil {
			return err
		}
	}

	return nil
}

func checkAxisWarnings(requests *c.RequestManager, address int32, axis int32, clearWarnings bool) (isHomed bool, err errors.SdkError) {
	warnings, _, err := devices.GetAxisWarnings(requests, address, axis, clearWarnings)
	if err != nil {
		return false, err
	}

	isHomed = !warnings.Has(NoReferenceFlag)
	err = devices.CheckErrorFlags(warnings, devices.MovementErrorWarningFlags, errors.BindErrMovementFailed(address, axis))
	return isHomed, err
}

func (instance *microscopyManager) objectiveChangerVerify(request *pb.ObjectiveChangerRequest) errors.SdkError {
	isChanger, err := instance.objectiveChangerIsChanger(request)
	if err != nil {
		return err
	} else if !isChanger {
		return errors.ErrInvalidArgument(fmt.Sprintf("Device with address %d is not an X-MOR.", request.GetTurretAddress()))
	}
	return nil
}

func (instance *microscopyManager) objectiveChangerIsChanger(request *pb.ObjectiveChangerRequest) (bool, errors.SdkError) {
	deviceInfo, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
		InterfaceId: request.InterfaceId, Device: request.TurretAddress,
	})
	if err != nil {
		if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
			return true, nil
		}
		return false, err
	}
	identifiedInfo := deviceInfo.GetIdentified()
	if identifiedInfo == nil {
		return true, nil
	}
	deviceID := identifiedInfo.GetProduct().DeviceID
	return pie.Contains(MORDeviceID, deviceID), nil
}

func (changer *objectiveChanger) getFocusDatum() float64 {
	changer.lock.Lock()
	defer changer.lock.Unlock()
	return changer.focusDatum
}
