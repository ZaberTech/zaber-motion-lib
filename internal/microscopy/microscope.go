package microscopy

import (
	"fmt"
	"log"
	"strings"
	c "zaber-motion-lib/internal/communication"
	"zaber-motion-lib/internal/devices"
	pb "zaber-motion-lib/internal/dto"
	"zaber-motion-lib/internal/errors"
	"zaber-motion-lib/internal/ioc"
	"zaber-motion-lib/internal/utils"
)

const FilterTurretAxisNumber = 1
const NoReferenceFlag = "WR"
const PartStorageKey = "zaber.microscope.part"
const AltPartStorageKey = PartStorageKey + "."

func (instance *microscopyManager) microscopeDetect(request *pb.MicroscopeFindRequest) (*pb.MicroscopeConfigResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}
	allDevices, err := instance.devices.GetAllIdentifiedDevices(request)
	if err != nil {
		return nil, err
	}

	config := &pb.MicroscopyMicroscopeConfig{}

	if illuminator, err := instance.illuminatorDetect(requests, &pb.FindDeviceRequest{
		InterfaceId: request.InterfaceId,
	}); err != nil {
		if detectErr, ok := err.(*errors.DeviceDetectionFailedError); ok {
			if detectErr.ErrType() == errors.DeviceDetectionFailed_MultipleFound {
				return nil, duplicateMicroscopePartErr("illuminators")
			}
		} else {
			return nil, err
		}
	} else {
		config.Illuminator = utils.ToPointer(illuminator.Address)
	}

	replies, err := requests.Request(c.Command{
		Command: "storage all print prefix " + PartStorageKey,
	}, &c.RequestOptions{
		CollectMultiple: true,
	})
	if err != nil && !errors.IsAnyOf(err, pb.Errors_REQUEST_TIMEOUT) {
		return nil, err
	}

	for _, reply := range replies {
		if reply.Type != c.ResponseTypeInfo ||
			!strings.HasPrefix(reply.Data, "set "+PartStorageKey) {
			continue
		}

		parts := strings.Split(reply.Data, " ")
		var partKey string
		var dataIndex int
		if parts[1] == PartStorageKey {
			partKey = parts[2]
			dataIndex = 3
		} else if strings.HasPrefix(parts[1], AltPartStorageKey) {
			partKey = parts[1][len(AltPartStorageKey):]
			dataIndex = 2
		} else {
			continue
		}
		additionalData := parts[dataIndex:]

		switch partKey {
		case "focus":
			if config.FocusAxis != nil {
				return nil, duplicateMicroscopePartErr("focus axes")
			}
			config.FocusAxis = &pb.AxisAddress{
				Device: int32(reply.Device),
				Axis:   int32(reply.Axis),
			}
		case "x":
			if config.XAxis != nil {
				return nil, duplicateMicroscopePartErr("X axes")
			}
			config.XAxis = &pb.AxisAddress{
				Device: int32(reply.Device),
				Axis:   int32(reply.Axis),
			}
		case "y":
			if config.YAxis != nil {
				return nil, duplicateMicroscopePartErr("Y axes")
			}
			config.YAxis = &pb.AxisAddress{
				Device: int32(reply.Device),
				Axis:   int32(reply.Axis),
			}
		case "filter_changer":
			if config.FilterChanger != nil {
				return nil, duplicateMicroscopePartErr("filter changers")
			}
			config.FilterChanger = utils.ToPointer((int32)(reply.Device))
		case "camera_trigger":
			if config.CameraTrigger != nil {
				return nil, duplicateMicroscopePartErr("camera triggers")
			}
			config.CameraTrigger = &pb.ChannelAddress{
				Device: int32(reply.Device),
			}
			for _, part := range additionalData {
				if strings.HasPrefix(part, "do=") {
					channel, err := utils.ParseInt32(strings.Split(part, "=")[1])
					if err != nil {
						err.AddToMessagef("Cannot parse part reply: %s", reply.Data)
						return nil, err
					}
					config.CameraTrigger.Channel = channel
				}
			}
		default:
			log.Printf("Unknown microscope part: %s. Consider updating the library.", reply.Data)
		}
	}

	if hasAxis(config.FocusAxis) {
		if turretAddress, err := instance.locateDevice(requests, allDevices, "X-MOR", MORDeviceID, 0); err != nil {
			if detectErr, ok := err.(*errors.DeviceDetectionFailedError); ok {
				if detectErr.ErrType() == errors.DeviceDetectionFailed_MultipleFound {
					return nil, duplicateMicroscopePartErr("objective changers")
				}
			} else {
				return nil, err
			}
		} else {
			config.ObjectiveChanger = utils.ToPointer(int32(turretAddress))
		}

		if hasDevice(config.ObjectiveChanger) {
			if _, err := instance.getObjectiveChangerBase(requests, &pb.ObjectiveChangerRequest{
				InterfaceId:   request.InterfaceId,
				TurretAddress: *config.ObjectiveChanger,
				FocusAddress:  config.FocusAxis.Device,
				FocusAxis:     config.FocusAxis.Axis,
			}, true); err != nil {
				return nil, err
			}
		}
	}

	external := request.ThirdParty
	if external == nil {
		external = &pb.MicroscopyThirdPartyComponents{}
	}

	if id := utils.NumberDefault(external.Autofocus, 0); id != 0 {
		if _, err := instance.getAutofocusProvider(id); err != nil {
			return nil, err
		}

		config.Autofocus = utils.ToPointer(id)

		if hasAxis(config.FocusAxis) {
			if _, _, err := instance.getAutofocus(requests, &pb.EmptyAutofocusRequest{
				ProviderId:    id,
				InterfaceId:   request.InterfaceId,
				FocusAddress:  config.FocusAxis.Device,
				FocusAxis:     config.FocusAxis.Axis,
				TurretAddress: utils.NumberDefault(config.ObjectiveChanger, 0),
			}, &getAutofocusOptions{renew: true}); err != nil {
				return nil, err
			}
		}
	}

	return &pb.MicroscopeConfigResponse{
		Config: config,
	}, nil
}

func duplicateMicroscopePartErr(name string) errors.SdkError {
	return errors.ErrDeviceDetectionFailed(
		fmt.Sprintf("Multiple %s detected. Create the microscope configuration manually.", name),
		errors.DeviceDetectionFailed_MultipleFound)
}

func (instance *microscopyManager) microscopeInitialize(request *pb.MicroscopeInitRequest) errors.SdkError {
	log.Printf("Initializing microscope")
	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return err
	}

	config := request.Config
	if hasAxis(config.FocusAxis) {
		if hasDevice(config.ObjectiveChanger) {
			changeMode := ObjectiveChangerModes.Home
			if request.Force {
				changeMode = ObjectiveChangerModes.ForceHome
			}
			err = instance.objectiveChangerChange(comm, &pb.ObjectiveChangerChangeRequest{
				InterfaceId:   request.InterfaceId,
				TurretAddress: *config.ObjectiveChanger,
				FocusAddress:  config.FocusAxis.Device,
				FocusAxis:     config.FocusAxis.Axis,
				Objective:     1,
				FocusOffset:   &pb.Measurement{},
			}, changeMode)
			if err != nil {
				return err
			}
		} else {
			if err := homeAxis(comm, config.FocusAxis, request.Force); err != nil {
				return err
			}
		}
	}

	if hasAxis(config.XAxis) {
		if err := homeAxis(comm, config.XAxis, request.Force); err != nil {
			return err
		}
	}

	if hasAxis(config.YAxis) {
		if err := homeAxis(comm, config.YAxis, request.Force); err != nil {
			return err
		}
	}

	if hasDevice(config.FilterChanger) {
		if err := homeAxis(comm, &pb.AxisAddress{
			Device: *config.FilterChanger,
			Axis:   FilterTurretAxisNumber,
		}, request.Force); err != nil {
			return err
		}
	}

	return nil
}

func (instance *microscopyManager) microscopeIsInitialized(request *pb.MicroscopeEmptyRequest) (*pb.BoolResponse, errors.SdkError) {
	requests, err := instance.devices.GetRequests(request)
	if err != nil {
		return nil, err
	}

	var axes []*pb.AxisAddress

	config := request.Config
	if hasAxis(config.FocusAxis) {
		if hasDevice(config.ObjectiveChanger) {
			axes = append(axes, &pb.AxisAddress{
				Device: *config.ObjectiveChanger,
				Axis:   MORAxisNumber,
			})
		}

		axes = append(axes, config.FocusAxis)
	}

	if hasAxis(config.XAxis) {
		axes = append(axes, config.XAxis)
	}

	if hasAxis(config.YAxis) {
		axes = append(axes, config.YAxis)
	}

	if hasDevice(config.FilterChanger) {
		axes = append(axes, &pb.AxisAddress{
			Device: *config.FilterChanger,
			Axis:   FilterTurretAxisNumber,
		})
	}

	isInitialized := true
	for _, axis := range axes {
		isHomed, err := hasReference(requests, axis)
		if err != nil {
			return nil, err
		} else if !isHomed {
			isInitialized = false
			break
		}
	}

	return &pb.BoolResponse{
		Value: isInitialized,
	}, nil
}

func hasReference(requests *c.RequestManager, axisAddress *pb.AxisAddress) (bool, errors.SdkError) {
	warnings, _, err := devices.GetAxisWarnings(requests, axisAddress.Device, axisAddress.Axis, false)
	if err != nil {
		return false, err
	}
	return !warnings.Has(NoReferenceFlag), nil
}

func homeAxis(comm ioc.ASCIICommunicationInterface, axisAddress *pb.AxisAddress, force bool) errors.SdkError {
	willHome := force
	if !force {
		warnings, _, err := devices.GetAxisWarnings(comm.Requests(), axisAddress.Device, axisAddress.Axis, true)
		if err != nil {
			return err
		}
		willHome = warnings.Has(NoReferenceFlag)
	}

	if willHome {
		if err := devices.MoveCommand(comm, &pb.DeviceMoveRequest{
			InterfaceId:   comm.ID(),
			Device:        axisAddress.Device,
			Axis:          axisAddress.Axis,
			WaitUntilIdle: true,
		}, "home"); err != nil {
			return err
		}
	}

	return nil
}

func handleToStringErr(err errors.SdkError) (*pb.StringResponse, errors.SdkError) {
	if errors.IsAnyOf(err, pb.Errors_CONNECTION_CLOSED) {
		return &pb.StringResponse{
			Value: "Microscope -> Connection Closed",
		}, nil
	}
	return nil, err
}

func (instance *microscopyManager) microscopeToString(request *pb.MicroscopeEmptyRequest) (*pb.StringResponse, errors.SdkError) {
	config := request.Config

	comm, err := instance.devices.GetInterface(request)
	if err != nil {
		return handleToStringErr(err)
	}

	var parts []string

	if hasDevice(config.Illuminator) {
		info, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      *config.Illuminator,
		})
		if err != nil {
			return handleToStringErr(err)
		}
		parts = append(parts, info.ToString("Illuminator"))
	}

	if hasDevice(config.ObjectiveChanger) {
		info, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      *config.ObjectiveChanger,
		})
		if err != nil {
			return handleToStringErr(err)
		}
		parts = append(parts, info.ToString("Objective Changer"))
	}

	if hasDevice(config.FilterChanger) {
		info, err := instance.devices.GetUnidentifiedDeviceInfo(&pb.DeviceEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      *config.FilterChanger,
		})
		if err != nil {
			return handleToStringErr(err)
		}
		parts = append(parts, info.ToString("Filter Changer"))
	}

	if hasAxis(config.FocusAxis) {
		target := &pb.AxisEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      config.FocusAxis.Device,
			Axis:        config.FocusAxis.Axis,
		}
		info, err := instance.devices.GetUnidentifiedDeviceInfo(target)
		if err != nil {
			return handleToStringErr(err)
		}
		str, err := info.ToStringAxis(target, "Focus Axis")
		if err != nil {
			return nil, err
		}
		parts = append(parts, fmt.Sprintf("%s -> %s", str, info.ToString("")))
	}

	if hasAxis(config.XAxis) {
		target := &pb.AxisEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      config.XAxis.Device,
			Axis:        config.XAxis.Axis,
		}
		info, err := instance.devices.GetUnidentifiedDeviceInfo(target)
		if err != nil {
			return handleToStringErr(err)
		}
		str, err := info.ToStringAxis(target, "X Axis")
		if err != nil {
			return nil, err
		}
		parts = append(parts, fmt.Sprintf("%s -> %s", str, info.ToString("")))
	}

	if hasAxis(config.YAxis) {
		target := &pb.AxisEmptyRequest{
			InterfaceId: request.InterfaceId,
			Device:      config.YAxis.Device,
			Axis:        config.YAxis.Axis,
		}
		info, err := instance.devices.GetUnidentifiedDeviceInfo(target)
		if err != nil {
			return handleToStringErr(err)
		}
		str, err := info.ToStringAxis(target, "Y Axis")
		if err != nil {
			return nil, err
		}
		parts = append(parts, fmt.Sprintf("%s -> %s", str, info.ToString("")))
	}

	if hasDevice(config.Autofocus) {
		id := *config.Autofocus
		provider, _ := instance.autofocusProviders.Load(id)
		var providerStr string
		if provider != nil {
			providerStr = provider.ToString()
		} else {
			providerStr = fmt.Sprintf("Unknown provider %d", id)
		}
		parts = append(parts, "Autofocus -> "+providerStr)
	}

	result := fmt.Sprintf("Microscope -> %s", comm.ToString())
	if len(parts) != 0 {
		result += fmt.Sprintf(", %s", strings.Join(parts, ", "))
	}

	return &pb.StringResponse{
		Value: result,
	}, nil
}

func hasAxis(axis *pb.AxisAddress) bool {
	return axis != nil && axis.Device != 0
}

func hasDevice(device *int32) bool {
	return device != nil && *device != 0
}
