## Tool Chain Setup

* Look into `images` folder to see how to make docker build environment.
* Install the [Go](https://golang.org/) language tools. You need at least
  version 1.11, which means the default Linux package may not be new enough.
* (Windows) Install the 64-bit [TDM-GCC](http://tdm-gcc.tdragon.net/download)
  distribution of mingw.
* (Linux) Run `sudo apt-get install libc6-dev-i386 gcc-multilib g++-multilib` to enable 32-bit builds
  on a 64-bit install.
* For C# development, make sure you have .NET SDK 6.0 or later installed.
* Install Node.js (LTS)
* Install Python 3.8+
* Install Java JDK 11+ and maven
* Install Cmake 3.5+ and a C++11 compatible compiler
* (Linux/OSX) Install the clang-tidy linter
* (Windows) Install the [Nullsoft Scriptable Install System](https://nsis.sourceforge.io/Download)

## Environment Setup

* (Windows) You must have long path support enabled. How to do this varies
  between Windows 7 and 10.  Note that it only enables long path support for
  applications that are long path aware and Windows 7's Explorer is not one
  of them, but the command shell is.
* (Windows) If using the Visual Studio compiler run the build from the
  `x64 Native Tools Command Prompt` or otherwise have the MSVC environment
  variables configured.
* Create a directory somewhere to be the root of your Go workspace. In there,
  create a subdirectory named `src`.
  * Check out this repository under `src`. It functions as a Go package,
    and `src` is the location required by Go.
  * (Windows) To keep paths shorter, put it near the root directory of your
     disk, or use the `subst` command to map a drive letter to your workspace.
     Subst also makes an easy way to switch between Go workspaces without
     having to change GOPATH.
* Set the environment variable GOPATH to point to your workspace and
  add $GOPATH/bin to your PATH. You might want to use a shell script to set
  up environment variables if you are using Go for other projects, as it's
  easier to change this way than using the system properties dialog.
  * There are shell scripts named `*-env.*` that will set up some of the
    environment variables for you. Run the appropriate one for your platform
    once when starting your shell.
* Change directory to src/zaber-motion-lib
* Run `npm install`
* Run `npx gulp dependencies`

### ES Lint

In order to get eslint working in VS Code, you may need to add the following to your project settings

```
  "eslint.useFlatConfig": true,
  "eslint.validate": [
    "javascript",
    "typescript",
  ],
  "eslint.nodePath": "./node_modules",
```

Flat config is the new eslint standard, and because there are so many `node-modules` in this project, it is necessary to point to the root directory.

## Linting

It's recommended to install the eslint plugin (`dbaeumer.vscode-eslint`) if you are using vscode for development, as this will enable vscode to mark linting errors as you type. Because of the multiple subdirectories that this project is organized into, to properly configure this tool, add the following to `.vscode/settings.json`:
```
  "eslint.workingDirectories": [
    "./",
    "./js",
    "./templates",
    "./dev-portal"
  ],
```

## Compiling

In the zaber-motion-lib directory:

* Run `npx gulp dependencies` to build the go and python environment. This will need to be periodically re-run when python upgrades.
* Run `npx gulp gen_from_device_db` to refresh code generated from the device database.
* Run `npx gulp gen_code` to refresh code generated from templates.
* Run `npx gulp build` to build the library for all languages.
* Run `npx gulp test` to run tests.

## Things to be aware of

* The C# solution contains some conditionally included file references.
  Both MSBuild and XBuild will compile correctly, but the Visual Studio
  solution explorer will not show the files that are excluded, and the
  MonoDevelop solution explorer won't even show the ones that are
  conditionally included.
* If you get an error relating to not being able to find ChromeHeadless, you need to configure the CHROME_BIN enviorment variable.
  Run the following command on linux to use Chrome: export CHROME_BIN=google-chrome


## Running examples against a local build

### C#

The main Visual Studio `.sln` file includes the examples and they will pick up the local build of ZML automatically via the project reference. If you add new examples make sure to add them to the solution and set up their internal project references.

### Python

Temporarily insert the following before the ZML imports:

    import sys
    sys.path.insert(0, "path/to/py/zaber_motion")

Substitute your local repo paths in the above. Remove these lines before committing the example code.
