const tsEslint = require('typescript-eslint');
const { includeIgnoreFile } = require('@eslint/compat');
const path = require('node:path');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');

module.exports = tsEslint.config(
  includeIgnoreFile(path.resolve(__dirname, '.gitignore')),
  includeIgnoreFile(path.resolve(__dirname, 'templates', '.gitignore')),
  includeIgnoreFile(path.resolve(__dirname, 'py', '.gitignore')),
  {
    ignores: ['js/', 'templates/', '**/*.ets.ts'],
  },
  {
    files: ['**/*.js'],
    extends: [
      zaberEslintConfig.javascript,
    ],
    rules: {
      'no-console': 'off',
    }
  },
  {
    files: ['**/*.ts'],
    extends: [
      zaberEslintConfig.typescript(['./tsconfig.json', './generated/tsconfig.json']),
    ],
    rules: {
      '@typescript-eslint/no-misused-promises': [
        'error',
        { checksVoidReturn: false }
      ],
      'no-console': 'off',
    }
  },
);
