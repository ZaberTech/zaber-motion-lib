import { generateDto } from './dto';
import { genTemplates } from './templates';
import { getTemplatesDtoDepending } from './templates/templates';

async function main() {
  await Promise.all([
    generateDto(),
    genTemplates(),
  ]);
  await getTemplatesDtoDepending();
}

main().catch(err => {
  console.error(err);
  process.exit(1);
});
