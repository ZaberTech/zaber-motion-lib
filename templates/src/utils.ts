import { exec as execNode } from 'node:child_process';

import { P, isMatching } from 'ts-pattern';

export * from '@zaber/toolbox';

/** Equivalent to isMatching from ts-pattern except it verifies that value overlaps with the pattern. */
export function isMatch<T, PType extends P.Pattern<T>>(pattern: PType, value: T): value is P.infer<PType> {
  return isMatching(pattern, value);
}

export const exec = (command: string) =>
  new Promise<{ stdout: string; stderr: string }>((resolve, reject) =>
    execNode(command, (err, stdout, stderr) => err ? reject(err) : resolve({ stdout, stderr })));
