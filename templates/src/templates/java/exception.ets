import { Context } from '../context';
import { ExceptionNameDefinition } from '../types';

export default function({definitions, exception, exceptionCustomDataType}: {exception: ExceptionNameDefinition} & Context)
/* template */
<%- include('generated.ejs') %>
<% const dataPojo = exceptionCustomDataType(definitions.exceptions, exception); -%>

package zaber.motion.exceptions;

<%- include('javadoc_simple.ejs', {type: exception, indent: 0}) %>
public class <%- exception.name %> extends <%- exception.baseException %> {
<% if (exception.customDataPojo) { -%>
    private <%- exception.customDataPojo %> details;

    /**
     * @return Additional data for <%- exception.name %>
     */
    public <%- exception.customDataPojo %> getDetails() {
        return this.details;
    }

    public <%- exception.name %>(String message, byte[] customData) {
        super(message);

        this.details = <%- exception.customDataPojo %>.fromByteArray(customData);
    }

    public <%- exception.name %>(String message, <%- exception.customDataPojo %> customData) {
        super(message);

        this.details = customData;
    }
<% } else if (dataPojo) { -%>
    public <%- exception.name %>(String message, byte[] customData) {
        super(message, customData);
    }

    public <%- exception.name %>(String message, <%- dataPojo %> customData) {
        super(message, customData);
    }
<% } else { -%>
    public <%- exception.name %>(String message) {
        super(message);
    }
<% } -%>
}
