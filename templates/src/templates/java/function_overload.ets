import { Context } from '../context';
import { MethodDefinition } from '../types';

export default function({ fn, java, isSyncWrapper, allParams, mapParams, nonVariadicParams }: { fn: MethodDefinition, isSyncWrapper: boolean } & Context)
/* template */
<%
  const requiredParams = fn.params.filter(param => !java.paramDefaultValue(param));
  const nonVarParams = nonVariadicParams(fn);
  for (let showParams = nonVarParams.length - 1; showParams >= requiredParams.length; showParams--) {
    const isAsync = !fn.sync && !isSyncWrapper;
%>
<%- include('javadoc', {function: fn, overloadParamCount: showParams, isSyncWrapper}) -%>
<% if (fn.obsoleteReason) { -%>
    @Deprecated
<% } -%>
    <%- fn.private ? 'private' : 'public' %> <% if (fn.static) { %>static <% } %><%- isSyncWrapper ? java.returnType(fn, true) : java.returnType(fn) %> <%- java.fnName(fn, isAsync) -%>(<%-
    showParams > 0 ? '\n        ':''-%><%- java.allParamSignatures(fn, showParams) -%>) {
        <% const noReturn = java.returnType(fn, true) === 'void' && (fn.sync || isSyncWrapper); -%>
<%- noReturn ? '' : 'return ' %><%- java.fnName(fn, isAsync) %>(<%- mapParams(fn, (param, type, index) => index < showParams ? java.paramName(param) : java.defaultInSignature(param)).join(', ') -%>);
    }
<% } -%>
