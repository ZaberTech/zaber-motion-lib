import { Context } from '../context';
import { MethodDefinition } from '../types';

export default function({fn, java, allParams}: {fn: MethodDefinition} & Context)
/* template */
<%- include('javadoc', {function: fn, overloadParamCount: undefined, isSyncWrapper: false}) -%>
<% if (fn.obsoleteReason) { -%>
    @Deprecated
<% } -%>
    <%- fn.private ? 'private' : 'public' %> <% if (fn.static) { %>static <% } %><%- java.returnType(fn) %> <%- java.fnName(fn) %><% if (!fn.sync) { %>Async<% } %>(<%- allParams(fn).length > 0 ? '\n        ' : ''-%>
<%- java.allParamSignatures(fn) -%>) {
<% (fn.paramsCheck || []).forEach(check => { -%>
        if (<%- check.code.java %>) {
            throw new IllegalArgumentException("<%- check.message %>");
        }
<% }) -%>
<% if (fn.code) { -%>
    <%- java.indent(fn.code.java)%>
<% } else { -%>
        zaber.motion.requests.<%- fn.request %> request =
            new zaber.motion.requests.<%- fn.request %>();
<% fn.args?.forEach(arg => { -%>
        request.set<%- java.argRequest(arg) %>(<%- java.argValue(arg) %>);
<% }) -%>
<% allParams(fn).forEach(param => { -%>
        request.set<%- java.paramRequest(param) %>(<%- param.type === 'uint' ? '(int) ' : '' %><%- java.paramValue(param) %>);
<% }) -%>
<% if (fn.returnType) {
    const Response = fn.response ? `zaber.motion.requests.${fn.response}` : java.typeNameDef(fn.returnType) ;-%>
        <%- fn.sync ? Response : `CompletableFuture<${Response}>` %> response = <%- fn.sync ? 'Call.callSync' : 'Call.callAsync' %>(
            "<%- fn.route %>",
            request,
            <%- Response %>.parser());
<% if (fn.customReturn) {
      if (fn.sync) { -%>
        return <%- fn.customReturn.java %>;
<%    } else { -%>
        return response
            .thenApply(r -> <%- fn.customReturn.java %>);
<%      }
   } else if (fn.returnPropertyName) {
      if (fn.sync) { -%>
        return response.<%- java.protoProperty(fn) %>;
<%    } else { -%>
        return response
            .thenApply(r -> r.<%- java.protoProperty(fn) %>);
<%    } -%>
<% } else { -%>
        return response;
<% } -%>
<% } else {
      if (fn.sync) { -%>
        Call.callSync("<%- fn.route %>", request, null);
<%    } else { -%>
        return Call.callAsync("<%- fn.route %>", request, null)
            .thenApply(r -> (Void) null);
<%    }
   }} -%>
    }
<%- include('function_overload', {function: fn, isSyncWrapper: false}) -%>

<% if (!fn.sync) {
        const isVoid = java.returnType(fn, true) === 'void'; -%>
<%- include('javadoc', {function: fn, overloadParamCount: undefined, isSyncWrapper: true}) -%>
    <%- fn.private ? 'private' : 'public' %> <% if (fn.static) { %>static <% } %><%- java.returnType(fn, true) %> <%- java.fnName(fn) %>(<%- allParams(fn).length > 0 ? '\n        ' : ''-%>
<%- java.allParamSignatures(fn) %>) {
        try {
            <% if (!isVoid) { -%>return <% } -%><%- java.fnName(fn) %>Async(<%- allParams(fn).map((param) => java.paramName(param)).join(', ') %>).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }
<%- include('function_overload', {function: fn, isSyncWrapper: true}) -%>
<% } -%>
