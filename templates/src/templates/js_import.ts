import fs from 'fs';
import os from 'os';
import path from 'path';

import { glob } from 'glob';
import { notNil } from '@zaber/toolbox';

const BASE_DIR = '../js/src';

const NO_EXPORT_DIRS = new Set([
  'requests',
  'gateway',
]);
const NO_EXPORT_FILES = new Set([
  'wasm.ts',
  'finalizer.ts',
  'custom_bindings.ts',
]);
const DO_NOT_ENTER_DIRS = new Set([
  'gateway',
]);
const EXPORT_THROUGH = new Set([
  'exceptions',
]);

type Tree = {
  [key: string]: Tree | string;
};

async function generateIndex(tree: Tree, folder: string) {
  let content = Object.keys(tree).map(key => {
    if (typeof tree[key] === 'string' || EXPORT_THROUGH.has(key)) {
      if (NO_EXPORT_FILES.has(key)) {
        return null;
      }
      const keyStr = key.replace('.ts', '');
      return `export * from './${keyStr}';`;
    } else {
      if (NO_EXPORT_DIRS.has(key)) {
        return null;
      }
      return `export * as ${key} from './${key}';`;
    }
  }).filter(notNil);
  content.sort();
  content = [
    '/* This file is generated. Do not modify by hand. */',
    ...content,
    '',
  ];

  await fs.promises.writeFile(`${folder}/index.ts`, content.join(os.EOL), 'utf-8');

  for (const key of Object.keys(tree)) {
    if (typeof tree[key] === 'object') {
      if (DO_NOT_ENTER_DIRS.has(key)) {
        continue;
      }

      await generateIndex(tree[key] as Tree, `${folder}/${key}`);
    }
  }
}

export async function generateJsExports() {
  const tsFiles = glob.sync('**/*.ts', { cwd: BASE_DIR });

  const tree: Tree = {};

  for (const tsFile of tsFiles) {
    if (tsFile.endsWith('index.ts') || tsFile.endsWith('.d.ts')) {
      continue;
    }

    const parts = tsFile.split(path.sep);

    let current = tree;
    for (const part of parts) {
      if (part.endsWith('.ts')) {
        current[part] = '';
      } else {
        if (!current[part]) {
          current[part] = {};
        }
        current = current[part] as Tree;
      }
    }
  }

  await generateIndex(tree, BASE_DIR);
}
