import fs from 'fs';
import path from 'path';

import { glob } from 'glob';
import { renderFile, Data } from 'ejs';
import _ from 'lodash';
import { ensureArray } from '@zaber/toolbox';

import { context } from './context';
import { Language, StringConstantDefinitions } from './types';
import { generatePythonExports } from './python_import';
import { generateJsExports } from './js_import';

const mkdirp = (dir: string) =>  fs.promises.mkdir(dir, { recursive: true });

const writeFileUtf8 = (filePath: string, content: string) => {
  const addBom = !filePath.endsWith('.java');
  if (addBom) {
    content = `\ufeff${content}`;
  }
  return fs.promises.writeFile(filePath, content, 'utf-8');
};

const renderAndWriteFile = async (language: string, srcFile: string, context: unknown, destinationDir: string, destination: string) => {
  const templateFile = path.join(__dirname, language, srcFile);
  const compiled = await renderFile(templateFile, context as Data);

  const destinationFile = path.join('..', destinationDir, destination);
  await mkdirp(path.dirname(destinationFile));
  await writeFileUtf8(destinationFile, compiled);
};

interface LanguageInfo {
  destination: string;
  fileName: (name: string) => string;
  namespaceFolder?: (namespace: string[]) => string;
  headerFileExtension?: string;
  exceptionsDir: string;
}

const targetLanguages: Record<string, LanguageInfo> = {
  ts: {
    destination: 'js/src',
    fileName: fn => _.snakeCase(fn),
    exceptionsDir: 'js/src/exceptions',
  },
  cs: {
    destination: 'csharp/Zaber.Motion/Api',
    fileName: fn => fn,
    namespaceFolder: namespace => namespace.map(fn => _.upperFirst(fn)).join('/'),
    exceptionsDir: 'csharp/Zaber.Motion/Exceptions',
  },
  py: {
    destination: 'py/zaber_motion',
    fileName: fn => _.snakeCase(fn),
    exceptionsDir: 'py/zaber_motion/exceptions',
  },
  java: {
    destination: 'java/src/main/java/zaber/motion',
    fileName: fn => fn,
    exceptionsDir: 'java/src/main/java/zaber/motion/exceptions',
  },
  cpp: {
    destination: 'cpp/src/zaber/motion',
    fileName: fn => _.snakeCase(fn),
    headerFileExtension: 'h',
    exceptionsDir: 'cpp/src/zaber/motion/exceptions',
  },
  swift: {
    destination: 'swift/Sources/ZaberMotion',
    fileName: fn => _.identity(fn),
    exceptionsDir: 'swift/Sources/ZaberMotion/Exceptions',
  },
};

const genException = async (language: Language) => {
  const destDir = targetLanguages[language].exceptionsDir;
  await Promise.all(context.definitions.exceptions.map(async exception => {
    const destFilename =  `${targetLanguages[language].fileName(exception.name)}.${language}`;
    await renderAndWriteFile(language, 'exception.ejs', { exception, ...context }, destDir, destFilename);
  }));

  if (targetLanguages[language].headerFileExtension) {
    await Promise.all(context.definitions.exceptions.map(async exception => {
      const destFilename =  `${targetLanguages[language].fileName(exception.name)}.${targetLanguages[language].headerFileExtension}`;
      await renderAndWriteFile(language, 'exceptionh.ejs', { exception, ...context }, destDir, destFilename);
    }));
  }
};

const genExceptions = async () => {
  await Promise.all([
    genException('ts'),
    genException('java'),
    genException('py'),
    genException('cs'),
    genException('cpp'),
    genException('swift'),

    renderAndWriteFile('ts', 'exceptions_import.ejs', context, targetLanguages.ts.exceptionsDir, 'index.ts'),
    renderAndWriteFile('java', 'exception_converter.ejs', context, targetLanguages.java.exceptionsDir, 'ExceptionConverter.java'),
    renderAndWriteFile('ts', 'exception_converter.ejs', context, 'js/src/gateway', 'convert_exceptions.ts'),
    renderAndWriteFile('cs', 'exception_converter.ejs', context, targetLanguages.cs.exceptionsDir, 'ExceptionConverter.cs'),
    renderAndWriteFile('py', 'exception_converter.ejs', context, 'py/zaber_motion', 'convert_exception.py'),
    renderAndWriteFile('cpp', 'exception_converterh.ejs', context, targetLanguages.cpp.exceptionsDir, 'convert_exception.h'),
    renderAndWriteFile('cpp', 'exception_converter.ejs', context, targetLanguages.cpp.exceptionsDir, 'convert_exception.cpp'),
    renderAndWriteFile('swift', 'exception_converter.ejs', context, targetLanguages.swift.exceptionsDir, 'ExceptionConverter.swift'),
  ]);
};

const genTemplate = async (src: string, dest: string, ext: string) => {
  let files = await glob(`${src}/**/*.${ext}.ejs`);
  files = files.map(file => path.relative(src, file));

  await Promise.all(
    files.map(async file => {
      await renderAndWriteFile(ext, file, context, dest, file.substr(0, file.length - 4));
    })
  );
};

const genTemplateHeaders = async (src: string, dest: string, language: Language) => {
  let files = await glob(`${src}/**/*.${targetLanguages[language].headerFileExtension}.ejs`);
  files = files.map(file => path.relative(src, file));

  await Promise.all(
    files.map(async file => {
      await renderAndWriteFile(language, file, context, dest, file.substr(0, file.length - 4));
    })
  );
};

const genCommonHeaders = async (
  template: string,
  srcPaths: string[],
  importBase: string,
  destinationDir: string,
  language: Language,
  exclude: string[] = [],
) => {
  const targetLanguage = targetLanguages[language];
  const files = srcPaths.map(src =>
    glob(`${targetLanguage.destination}/${src}/*.${targetLanguage.headerFileExtension}`, { cwd: '..' })
  );
  const headers = _.flatten(await Promise.all(files))
    .map(header => header.replaceAll(path.sep, '/'))
    .map(header => header.replace(importBase, ''))
    .filter(header => !exclude.includes(header))
    .sort();

  await renderAndWriteFile(language, template, { headers }, destinationDir,
    `${path.basename(template, '.ejs')}.${targetLanguages[language].headerFileExtension}`);
};

async function genSimpleTypes(typeDefs: StringConstantDefinitions, template: string, isHeader: boolean = false): Promise<void> {
  for (const [language, languageDef] of _.toPairs(targetLanguages)) {
    if (isHeader && !languageDef.headerFileExtension) {
      continue;
    }
    for (const typeDef of _.values(typeDefs)) {
      const destFilename = `${languageDef.fileName(typeDef.name)}.${isHeader ? languageDef.headerFileExtension : language}`;
      let destinationDir = languageDef.destination;
      const namespace = ensureArray(typeDef.namespace);
      if (namespace) {
        destinationDir = path.join(destinationDir, languageDef.namespaceFolder?.(namespace) ?? namespace.join('/'));
      }
      await renderAndWriteFile(language, template, { ...context, type: typeDef }, destinationDir, destFilename);
    }
  }
}

export async function genTemplates(): Promise<void> {
  await genTemplate('build/templates/ts', targetLanguages.ts.destination, 'ts');
  await genTemplate('build/templates/cs', targetLanguages.cs.destination, 'cs');
  await genTemplate('build/templates/py', targetLanguages.py.destination, 'py');
  await genTemplate('build/templates/java', targetLanguages.java.destination, 'java');
  await genTemplate('build/templates/cpp', targetLanguages.cpp.destination, 'cpp');
  await genTemplate('build/templates/swift', targetLanguages.swift.destination, 'swift');
  await genTemplate('build/templates/go', 'internal/generated', 'go');

  await genTemplateHeaders('build/templates/cpp', targetLanguages.cpp.destination, 'cpp');

  await genSimpleTypes(context.definitions.stringConstants, 'string_constant.ejs');
  await genSimpleTypes(context.definitions.stringConstants, 'string_constanth.ejs', true);

  await genExceptions();
}

export async function getTemplatesDtoDepending() {
  await genCppIncludeFiles();

  await generatePythonExports();
  await generateJsExports();
}

async function genCppIncludeFiles() {
  const NAMESPACES = _(context.definitions.classes)
    .map(cls => ensureArray(cls.namespace))
    .filter(ns => ns.length === 1)
    .map(ns => ns[0])
    .uniq()
    .value();

  await genCommonHeaders('includes.ejs', ['exceptions', '.'], 'cpp/src/', targetLanguages.cpp.destination, 'cpp', [
    'zaber/motion/includes.h',
    ...NAMESPACES.map(space => `zaber/motion/${space}.h`),
  ]);
  for (const space of NAMESPACES) {
    await genCommonHeaders(`${space}.ejs`, [space], 'cpp/src/', targetLanguages.cpp.destination, 'cpp');
  }
}
