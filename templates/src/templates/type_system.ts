// The type system is similar to DTO types, however there are small differences that make it difficult to unify.

import { Namespace } from '../common_types';

export * from '../common_types';

export type SpecialUnits =  'LengthUnits' | 'VelocityUnits' | 'TimeUnits' | 'FrequencyUnits' | 'AccelerationUnits';

export type PrimitiveType = 'string' | 'uint' | 'int' | 'double' | 'bool' | 'Units' | SpecialUnits;
export type ReferenceType = { type: 'ref'; name: string; namespace: Namespace };
export type EnumType = { type: 'enum'; name: string; namespace: Namespace };
export type StartType = PrimitiveType | ReferenceType | EnumType;

export type OptionalType = { type: 'optional'; of: AnyType };
export type CollectionType = { type: 'array' | 'set'; of: AnyType };
export type AnyType = StartType | CollectionType | OptionalType;


export function getBaseType(type: AnyType): StartType {
  if (isBasicType(type)) { return type }
  switch (type.type) {
    case 'ref':
    case 'enum':
      return type;
    default:
      return getBaseType(type.of);
  }
}

export function isBasicType(type: AnyType): type is PrimitiveType {
  return typeof type === 'string';
}

export function isUnitsType(type: AnyType): type is 'Units' | SpecialUnits {
  return isBasicType(type) && type.endsWith('Units');
}

export function isReferenceType(type: AnyType): type is ReferenceType {
  return typeof type === 'object' && type.type === 'ref';
}

export function isEnumType(type: AnyType): type is EnumType {
  return typeof type === 'object' && type.type === 'enum';
}

export function isOptionalType(type: AnyType): type is OptionalType {
  return typeof type === 'object' && type.type === 'optional';
}

export function isCollectionType(type: AnyType): type is CollectionType {
  return typeof type === 'object' && (type.type === 'array' || type.type === 'set');
}

export function hasNamespace(type: AnyType): type is ReferenceType | EnumType {
  return isReferenceType(type) || isEnumType(type);
}

export function stripOptional(type: AnyType): AnyType {
  if (isOptionalType(type)) {
    return stripOptional(type.of);
  } else if (isCollectionType(type)) {
    return { type: type.type, of: stripOptional(type.of) };
  } else {
    return type;
  }
}

export function typeChain(type: AnyType): AnyType[] {
  if (isCollectionType(type) || isOptionalType(type)) {
    return [type, ...typeChain(type.of)];
  } else {
    return [type];
  }
}
