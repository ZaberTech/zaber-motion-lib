import { Context } from '../context';

export default function({definitions, exceptionCustomDataType}: Context)
/* template */
<%- include('generated.ejs') %>
#pragma warning disable CA1502, CA1506
using System;

namespace Zaber.Motion.Exceptions
{
    /// <summary>
    /// Converts errors from the native library to specific exceptions.
    /// </summary>
    public static class ExceptionConverter
    {
        /// <summary>
        /// Converts errors from the native library to specific exceptions.
        /// </summary>
        /// <param name="error">Error type from the native library.</param>
        /// <param name="message">Error message from the native library.</param>
        /// <param name="customData">Raw unparsed custom data for the exception.</param>
        /// <returns>Specific exception converted from native library error.</returns>
        internal static MotionLibException Convert(Requests.Errors error, string message, byte[]? customData = null)
        {
            switch (error)
            {
<% definitions.exceptions.forEach((exception) => { -%>
                case Requests.Errors.<%- exception.pbNames.cs %>:
<% const customPojo = exceptionCustomDataType(definitions.exceptions, exception); -%>
<% if (customPojo) { -%>
                    if (customData == null)
                    {
                        throw new ArgumentNullException(nameof(customData));
                    }

<% } -%>
                    return new <%- exception.name %>(message<% if (customPojo) { -%>, customData<% } -%>);
<% }); -%>
                default:
                    return new MotionLibException(message);
            }
        }
    }
}
