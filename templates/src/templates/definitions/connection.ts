import _ from 'lodash';

import { ClassDefinition, MethodArg, MethodDefinition, OptionalMethodParam } from '../types';

import { genericCommand, genericCommandMultiResponse, genericCommandNoResponse } from './generic_commands';
import { defaultBool, defaultEmptyArray, defaultOptional, defaultString } from './helpers';

function genConnectionArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'InterfaceId',
    },
  ];
}

const openConnectionBase: Partial<MethodDefinition> = {
  route: 'interface/open',
  request: 'OpenInterfaceRequest',
  customReturn: {
    cs: 'new Connection(response.InterfaceId)',
    py: 'Connection(response.interface_id)',
    ts: 'new Connection(response.interfaceId)',
    java: 'new Connection(r.getInterfaceId())',
    cpp: '{response.getInterfaceId()}',
    swift: 'Connection(interfaceId: response.interfaceId)',
  },
  returnType: { type: 'ref', name: 'Connection', namespace: 'ascii' },
  response: 'OpenInterfaceResponse',
  py: {
    asyncContextManager: {
      name: 'AsyncConnectionOpener',
      description: 'Provides a connection in an asynchronous context using `await` or `async with`',
      closer: 'close_async',
    },
  },
};

const connectionNameParam = {
  name: 'connectionName', type: { type: 'optional', of: 'string' }, defaultValue: defaultOptional(),
  description: [
    'The name of the connection to open.',
    'Can be left empty to default to the only connection present.',
    'Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.',
  ]
} satisfies OptionalMethodParam;

const definition: ClassDefinition = {
  name: 'Connection',
  namespace: 'ascii',
  constants: [{
    name: 'DefaultBaudRate',
    description: 'Default baud rate for serial connections.',
    type: 'int',
    value: '115200',
  }, {
    name: 'TcpPortChain',
    description: [
      'Commands sent over this port are forwarded to the device chain.',
      'The bandwidth may be limited as the commands are forwarded over a serial connection.',
    ],
    type: 'int',
    value: '55550',
  }, {
    name: 'NetworkSharePort',
    description: 'Local area network share port.',
    type: 'int',
    value: '11421',
  }, {
    name: 'TcpPortDeviceOnly',
    description: [
      'Commands send over this port are processed only by the device',
      'and not forwarded to the rest of the chain.',
      'Using this port typically makes the communication faster.',
    ],
    type: 'int',
    value: '55551',
  }],
  properties: [
    {
      name: 'InterfaceId',
      description: 'The interface ID identifies this Connection instance with the underlying library.',
      type: 'int',
    },
    {
      name: 'DefaultRequestTimeout',
      description: [
        'The default timeout, in milliseconds, for a device to respond to a request.',
        'Setting the timeout to a too low value may cause request timeout exceptions.',
        'The initial value is 1000 (one second).'
      ],
      type: 'int',
      getterFunctionName: 'RetrieveTimeout',
      getterCanThrow: true,
      setterCanThrow: true,
      setter: {
        cs: 'ChangeTimeout(value)',
        py: 'self.__change_timeout(value)',
        ts: 'this._changeTimeout(value)',
        java: 'this.changeTimeout(value)',
        cpp: 'this->changeTimeout(value)',
        swift: 'changeTimeout(timeout: value)',
      }
    },
    {
      name: 'ChecksumEnabled',
      description: 'Controls whether outgoing messages contain checksum.',
      type: 'bool',
      getterFunctionName: 'RetrieveChecksumEnabled',
      getterCanThrow: true,
      setterCanThrow: true,
      setter: {
        cs: 'ChangeChecksumEnabled(value)',
        py: 'self.__change_checksum_enabled(value)',
        ts: 'this._changeChecksumEnabled(value)',
        java: 'this.changeChecksumEnabled(value)',
        cpp: 'this->changeChecksumEnabled(value)',
        swift: 'changeChecksumEnabled(isEnabled: value)',
      }
    },
  ],
  events: {
    unknownResponse: {
      name: 'UnknownResponse',
      description: 'Event invoked when a response from a device cannot be matched to any known request.',
      emitType: { type: 'ref', name: 'UnknownResponseEvent', namespace: 'ascii' },
      emitTypeDescription: 'Reply that could not be matched to a request.'
    },
    alert: {
      name: 'Alert',
      description: 'Event invoked when an alert is received from a device.',
      emitType: { type: 'ref', name: 'AlertEvent', namespace: 'ascii' },
      emitTypeDescription: 'Alert message received from the device.'
    },
    disconnected: {
      name: 'Disconnected',
      description: 'Event invoked when connection is interrupted or closed.',
      emitType: { type: 'ref', name: 'MotionLibException', namespace: 'exceptions' },
      emitTypeDescription: 'Error that caused disconnection.'
    },
  },
  description: 'Class representing access to particular connection (serial port, TCP connection).',
  ctor: {
    code: {
      cs: 'Subscribe();',
      py: 'self.__setup_events()',
      ts: 'this._subscribe();',
      java: 'this.subscribe();',
      cpp: 'this->subscribe();',
      swift: 'ensureSubscribed()',
    }
  },
  functions: [
    {
      name: 'OpenSerialPort',
      static: true,
      description: [
        'Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.',
        'Zaber Launcher allows sharing of the port between multiple applications,',
        'If port sharing is not desirable, use the `direct` parameter.',
      ],
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.SerialPort',
          py: 'dto.InterfaceType.SERIAL_PORT',
          ts: 'requests.InterfaceType.SERIAL_PORT',
          java: 'InterfaceType.SERIAL_PORT',
          cpp: '::zaber::motion::requests::InterfaceType::SERIAL_PORT',
          swift: 'InterfaceType.serialPort',
        }
      }],
      params: [
        { name: 'portName', type: 'string', description: 'Name of the port to open.' },
      ],
      options: [
        {
          name: 'baudRate', type: 'int',
          defaultValue: {
            all: 'DEFAULT_BAUD_RATE',
            cs: 'DefaultBaudRate',
            ts: 'Connection.DEFAULT_BAUD_RATE',
            swift: 'Connection.defaultBaudRate',
          },
          description: 'Optional baud rate (defaults to 115200).'
        },
        {
          name: 'direct', requestName: 'rejectRoutedConnection', type: 'bool', defaultValue: defaultBool(false),
          description: [
            'If true will connect to the serial port directly,', 'failing if the connection is already opened by a message router instance.'
          ]
        },
      ],
      returnDescription: 'An object representing the port.',
      ...openConnectionBase,
    },
    {
      name: 'OpenTcp',
      static: true,
      description: 'Opens a TCP connection.',
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.Tcp',
          py: 'dto.InterfaceType.TCP',
          ts: 'requests.InterfaceType.TCP',
          java: 'InterfaceType.TCP',
          cpp: '::zaber::motion::requests::InterfaceType::TCP',
          swift: 'DtoRequests.InterfaceType.tcp',
        }
      }],
      params: [
        { name: 'hostName', type: 'string', description: 'Hostname or IP address.' },
        {
          name: 'port', type: 'int',
          description: 'Optional port number (defaults to 55550).',
          defaultValue: { all: 'TCP_PORT_CHAIN', cs: 'TcpPortChain', ts: 'Connection.TCP_PORT_CHAIN', swift: 'Connection.tcpPortChain' },
        },
      ],
      returnDescription: 'An object representing the connection.',
      ...openConnectionBase,
    },
    {
      name: 'OpenCustom',
      static: true,
      description: 'Opens a connection using a custom transport.',
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.Custom',
          py: 'dto.InterfaceType.CUSTOM',
          ts: 'requests.InterfaceType.CUSTOM',
          java: 'InterfaceType.CUSTOM',
          cpp: '::zaber::motion::requests::InterfaceType::CUSTOM',
          swift: 'DtoRequests.InterfaceType.custom',
        }
      }],
      params: [
        {
          name: 'transport',
          description: 'The custom connection transport.',
          type: { type: 'ref', name: 'Transport', namespace: 'ascii' },
          value: {
            cs: 'transport.TransportId',
            py: 'transport.transport_id',
            ts: 'transport.transportId',
            java: 'transport.getTransportId()',
            cpp: 'transport.getTransportId()',
            swift: 'transport.transportId',
          }
        },
      ],
      returnDescription: 'An object representing the connection.',
      ...openConnectionBase,
    },
    {
      name: 'OpenIot',
      static: true,
      description: [
        'Opens a secured connection to a cloud connected device chain.',
        'Use this method to connect to devices on your account.'
      ],
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.Iot',
          py: 'dto.InterfaceType.IOT',
          ts: 'requests.InterfaceType.IOT',
          java: 'InterfaceType.IOT',
          cpp: '::zaber::motion::requests::InterfaceType::IOT',
          swift: 'DtoRequests.InterfaceType.iot',
        }
      }],
      params: [
        { name: 'cloudId', type: 'string', description: 'The cloud ID to connect to.' },
      ],
      options: [
        {
          name: 'token', type: 'string', defaultValue: defaultString('unauthenticated'),
          description: 'The token to authenticate with. By default the connection will be unauthenticated.'
        },
        connectionNameParam,
        {
          name: 'realm', type: { type: 'optional', of: 'string' },
          description: ['The realm to connect to.', 'Can be left empty for the default account realm.'],
          defaultValue: defaultOptional()
        },
        {
          name: 'api', type: 'string', description: 'The URL of the API to receive connection info from.',
          defaultValue: defaultString('https://api.zaber.io')
        },
      ],
      returnDescription: 'An object representing the connection.',
      ...openConnectionBase,
    },
    {
      name: 'OpenNetworkShare',
      static: true,
      description: [
        'Opens a connection to Zaber Launcher in your Local Area Network.',
        'The connection is not secured.',
      ],
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.NetworkShare',
          py: 'dto.InterfaceType.NETWORK_SHARE',
          ts: 'requests.InterfaceType.NETWORK_SHARE',
          java: 'InterfaceType.NETWORK_SHARE',
          cpp: '::zaber::motion::requests::InterfaceType::NETWORK_SHARE',
          swift: 'DtoRequests.InterfaceType.networkShare',
        }
      }],
      params: [
        {
          name: 'hostName',
          type: 'string',
          description: 'Hostname or IP address.'
        }, {
          name: 'port',
          type: 'int',
          description: 'Port number.',
          defaultValue: {
            all: 'NETWORK_SHARE_PORT',
            cs: 'NetworkSharePort',
            ts: 'Connection.NETWORK_SHARE_PORT',
            swift: 'Connection.networkSharePort',
          },
        },
        connectionNameParam,
      ],
      returnDescription: 'An object representing the connection.',
      ...openConnectionBase,
    },
    genericCommand({
      interfaceId: 'InterfaceId',
      description: [
        'Sends a generic ASCII command to this connection.',
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
    }),
    genericCommandNoResponse({
      interfaceId: 'InterfaceId',
      description: [
        'Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.',
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
    }),
    genericCommandMultiResponse({
      interfaceId: 'InterfaceId',
      description: [
        'Sends a generic ASCII command to this connection and expect multiple responses,',
        'either from one device or from many devices.',
        'Responses are returned in order of arrival.',
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
    }),
    {
      name: 'EnableAlerts',
      description: [
        'Enables alerts for all devices on the connection.',
        'This will change the "comm.alert" setting to 1 on all supported devices.',
      ],
      route: 'interface/generic_command_no_response',
      request: 'GenericCommandRequest',
      args: [
        ...genConnectionArgs(),
        { name: 'command', value: defaultString('set comm.alert 1') },
      ],
      params: [],
    },
    {
      name: 'DisableAlerts',
      description: [
        'Disables alerts for all devices on the connection.',
        'This will change the "comm.alert" setting to 0 on all supported devices.',
      ],
      route: 'interface/generic_command_no_response',
      request: 'GenericCommandRequest',
      args: [
        ...genConnectionArgs(),
        { name: 'command', value: defaultString('set comm.alert 0') },
      ],
      params: [],
    },
    {
      name: 'ResetIds',
      description: 'Resets ASCII protocol message IDs. Only for testing purposes.',
      route: 'interface/reset_ids',
      request: 'InterfaceEmptyRequest',
      args: genConnectionArgs(),
      sync: true,
      params: [],
      hidden: true,
    },
    {
      name: 'Close',
      description: 'Close the connection.',
      route: 'interface/close',
      request: 'InterfaceEmptyRequest',
      args: genConnectionArgs(),
      params: [],
    },
    {
      name: 'GetDevice',
      description: [
        'Gets a Device class instance which allows you to control a particular device on this connection.',
        'Devices are numbered from 1.',
      ],
      sync: true,
      params: [
        {
          name: 'deviceAddress',
          type: 'int',
          description: 'Address of device intended to control. Address is configured for each device.'
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'deviceAddress <= 0',
            py: 'device_address <= 0',
            ts: 'deviceAddress <= 0',
            java: 'deviceAddress <= 0',
            cpp: 'deviceAddress <= 0',
            swift: 'deviceAddress > 0',
          },
          message: 'Invalid value; physical devices are numbered from 1.',
        }
      ],
      code: {
        cs: 'return new Device(this, deviceAddress);',
        py: 'return Device(self, device_address)',
        ts: 'return new Device(this, deviceAddress);',
        java: 'return new Device(this, deviceAddress);',
        cpp: 'return { *this, deviceAddress };',
        swift: 'return Device(connection: self, deviceAddress: deviceAddress)',
      },
      returnType: { type: 'ref', name: 'Device', namespace: 'ascii' },
      returnDescription: 'Device instance.',
    },
    {
      name: 'RenumberDevices',
      description: 'Renumbers devices present on this connection. After renumbering, devices need to be identified again.',
      route: 'device/renumber_all',
      request: 'RenumberRequest',
      args: genConnectionArgs(),
      params: [],
      options: [
        {
          name: 'firstAddress',
          type: 'int',
          defaultValue: '1',
          requestName: 'Address',
          description: [
            'This is the address that the device closest to the computer is given.',
            'Remaining devices are numbered consecutively.',
          ]
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'firstAddress <= 0',
            py: 'first_address <= 0',
            ts: 'firstAddress <= 0',
            java: 'firstAddress <= 0',
            cpp: 'firstAddress <= 0',
            swift: 'firstAddress > 0',
          },
          message: 'Invalid value; device addresses are numbered from 1.',
        }
      ],
      returnPropertyName: 'Value',
      returnType: 'int',
      response: 'IntResponse',
      returnDescription: 'Total number of devices that responded to the renumber.',
    },
    {
      name: 'DetectDevices',
      description: 'Attempts to detect any devices present on this connection.',
      route: 'device/detect',
      request: 'DeviceDetectRequest',
      args: genConnectionArgs(),
      params: [],
      options: [
        {
          name: 'identifyDevices', type: 'bool', defaultValue: { all: 'true', py: 'True' },
          description: 'Determines whether device identification should be performed as well.'
        },
      ],
      customReturn: {
        cs: 'response.Devices.Select(device => GetDevice(device)).ToArray()',
        py: 'list(map(self.get_device, response.devices))',
        ts: 'response.devices.map(deviceAddress => this.getDevice(deviceAddress))',
        java: 'ArrayUtility.arrayFromInt(Device[]::new, r.getDevices(), address -> this.getDevice(address))',
        cpp: 'zml_util::map_vec<int,Device>(response.getDevices(), [this](const int& i) { return Device(*this, i); })',
        swift: 'try response.devices.map { try getDevice(deviceAddress: $0) }',
      },
      returnType: { type: 'array', of: { type: 'ref', name: 'Device', namespace: 'ascii' } },
      response: 'DeviceDetectResponse',
      returnDescription: 'Array of detected devices.',
    },
    {
      name: 'ForgetDevices',
      description: [
        'Forgets all the information associated with devices on the connection.',
        'Useful when devices are removed from the chain indefinitely.',
      ],
      route: 'device/forget',
      request: 'ForgetDevicesRequest',
      args: genConnectionArgs(),
      params: [{
        name: 'exceptDevices',
        type: { type: 'array', of: 'int' },
        description: 'Addresses of devices that should not be forgotten.',
        defaultValue: defaultEmptyArray('int'),
      }],
      sync: true,
    },
    {
      name: 'StopAll',
      description: 'Stops all of the devices on this connection.',
      route: 'device/stop_all',
      request: 'DeviceOnAllRequest',
      args: genConnectionArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: ['Determines whether the function should return immediately', 'or wait until the devices are stopped.']
        },
      ],
      returnType: { type: 'array', of: 'int' },
      returnPropertyName: 'DeviceAddresses',
      response: 'DeviceOnAllResponse',
      returnDescription: 'The addresses of the devices that were stopped by this command.',
    },
    {
      name: 'HomeAll',
      description: 'Homes all of the devices on this connection.',
      route: 'device/home_all',
      request: 'DeviceOnAllRequest',
      args: genConnectionArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: ['Determines whether the function should return immediately', 'or wait until the devices are homed.']
        },
      ],
      returnType: { type: 'array', of: 'int' },
      returnPropertyName: 'DeviceAddresses',
      response: 'DeviceOnAllResponse',
      returnDescription: 'The addresses of the devices that were homed by this command.',
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the connection.',
      route: 'interface/to_string',
      request: 'InterfaceEmptyRequest',
      response: 'StringResponse',
      args: genConnectionArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the connection.',
    },
    {
      name: 'RetrieveTimeout',
      description: 'Returns default request timeout.',
      route: 'interface/get_timeout',
      request: 'InterfaceEmptyRequest',
      response: 'IntResponse',
      args: genConnectionArgs(),
      params: [],
      returnType: 'int',
      returnPropertyName: 'Value',
      returnDescription: 'Default request timeout.',
      sync: true,
      private: true,
    },
    {
      name: 'ChangeTimeout',
      description: 'Sets default request timeout.',
      route: 'interface/set_timeout',
      request: 'SetInterfaceTimeoutRequest',
      args: genConnectionArgs(),
      params: [{
        name: 'timeout',
        description: 'Default request timeout.',
        type: 'int',
      }],
      sync: true,
      private: true,
    },
    {
      name: 'RetrieveChecksumEnabled',
      description: 'Returns checksum enabled.',
      route: 'interface/get_checksum_enabled',
      request: 'InterfaceEmptyRequest',
      response: 'BoolResponse',
      args: genConnectionArgs(),
      params: [],
      returnType: 'bool',
      returnPropertyName: 'Value',
      returnDescription: 'Checksum enabled.',
      sync: true,
      private: true,
    },
    {
      name: 'ChangeChecksumEnabled',
      description: 'Sets checksum enabled.',
      route: 'interface/set_checksum_enabled',
      request: 'SetInterfaceChecksumEnabledRequest',
      args: genConnectionArgs(),
      params: [{
        name: 'isEnabled',
        description: 'Checksum enabled.',
        type: 'bool',
      }],
      sync: true,
      private: true,
    },
  ],
  instantiation: [
    {
      method: 'static',
      source: {
        namespace: 'ascii',
        class: 'Connection',
        member: 'OpenSerialPort',
      },
      args: ['COM3'],
    }, {
      method: 'static',
      source: {
        namespace: 'ascii',
        class: 'Connection',
        member: 'OpenTcp',
      },
      args: ['zaber-12345.local'],
    }, {
      method: 'static',
      source: {
        namespace: 'ascii',
        class: 'Connection',
        member: 'OpenIot',
      },
      args: ['xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx'],
    }, {
      method: 'static',
      source: {
        namespace: 'ascii',
        class: 'Connection',
        member: 'OpenNetworkShare',
      },
      args: ['localhost'],
    }, {
      method: 'static',
      source: {
        namespace: 'ascii',
        class: 'Connection',
        member: 'OpenCustom',
      },
      args: [{
        namespace: 'ascii',
        class: 'Transport',
      }],
    }
  ],
  py: {
    forwardTypes: ['AsyncConnectionOpener'],
  }
} satisfies ClassDefinition;

export function makeCppConnections(connection: ClassDefinition): { base: ClassDefinition; derived: ClassDefinition } {
  function isBaseMethod(method: MethodDefinition) {
    return !method.name.match(/^(Open|Close)/);
  }

  const baseConnection = _.cloneDeep(connection);
  baseConnection.name = 'BaseConnection';
  baseConnection.functions = baseConnection.functions.filter(isBaseMethod);
  baseConnection.ctor = {
    description: 'Creates an instance of BaseConnection.',
  };
  baseConnection.description = [
    ...(Array.isArray(baseConnection.description) ? baseConnection.description : [baseConnection.description]),
    'Unlike Connection, BaseConnection instances are copyable.',
    'Additionally, BaseConnection instance does not close the connection upon destruction.',
  ];
  baseConnection.events = undefined;

  const derivedConnection = _.cloneDeep(connection);
  derivedConnection.functions = derivedConnection.functions.filter(method => !isBaseMethod(method));
  derivedConnection.properties = [];
  derivedConnection.constants = [];
  derivedConnection.ctor = false;
  derivedConnection.cpp = { noCopyCtor: true };
  derivedConnection.baseClass = baseConnection.name;

  return {
    base: baseConnection,
    derived: derivedConnection,
  };
}

export const connection = definition;
export const cppConnections = makeCppConnections(definition);
