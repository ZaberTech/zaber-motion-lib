import { ClassDefinition, MethodArg } from '../types';

import { defaultBool, nullValue } from './helpers';

function genServoTuningArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Axis.Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Axis.Device.DeviceAddress',
    },
    {
      name: 'Axis',
      value: 'Axis.AxisNumber',
    },
  ];
}

const definition: ClassDefinition = {
  name: 'ServoTuner',
  namespace: 'ascii',
  properties: [
    {
      name: 'Axis',
      description: 'The axis that will be tuned.',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
    },
  ],
  description: 'Exposes the capabilities to inspect and edit an axis\' servo tuning.',
  minFw: [{ major: 6, minor: 25 }, { major: 7, minor: 0 }],
  ctor: {
    public: true,
    description: 'Creates instance of ServoTuner for the given axis.',
  },
  functions: [
    {
      name: 'GetStartupParamset',
      description: 'Get the paramset that this device uses by default when it starts up.',
      route: 'servotuning/get_startup_set',
      request: 'AxisEmptyRequest',
      response: 'ServoTuningParamsetResponse',
      returnPropertyName: 'Paramset',
      args: genServoTuningArgs(),
      params: [],
      returnType: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
      returnDescription: 'The paramset used when the device restarts.',
    }, {
      name: 'SetStartupParamset',
      description: 'Set the paramset that this device uses by default when it starts up.',
      route: 'servotuning/set_startup_set',
      request: 'ServoTuningRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to use at startup.',
        },
      ],
    }, {
      name: 'LoadParamset',
      description: 'Load the values from one paramset into another.',
      route: 'servotuning/load_paramset',
      request: 'LoadParamset',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'ToParamset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to load into.',
        }, {
          name: 'FromParamset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to load from.',
        },
      ],
    }, {
      name: 'GetTuning',
      description: 'Get the full set of tuning parameters used by the firmware driving this axis.',
      route: 'servotuning/get_raw',
      request: 'ServoTuningRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to get tuning for.',
        },
      ],
      returnType: { type: 'ref', name: 'ParamsetInfo', namespace: 'ascii' },
      returnDescription: 'The raw representation of the current tuning.',
    }, {
      name: 'SetTuning',
      description: [
        'Set individual tuning parameters.',
        'Only use this method if you have a strong understanding of Zaber specific tuning parameters.'
      ],
      route: 'servotuning/set_raw',
      request: 'SetServoTuningRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to set tuning of.',
        },
        {
          name: 'TuningParams',
          type: { type: 'array', of: { type: 'ref', name: 'ServoTuningParam', namespace: 'ascii' } },
          description: 'The params to set.',
        },
        {
          name: 'SetUnspecifiedToDefault',
          type: 'bool',
          defaultValue: defaultBool(false),
          description: [
            'If true, any tuning parameters not included in TuningParams',
            'are reset to their default values.'
          ],
        }
      ],
    }, {
      name: 'SetPidTuning',
      description: 'Sets the tuning of a paramset using the PID method.',
      route: 'servotuning/set_pid',
      request: 'SetServoTuningPIDRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to get tuning for.',
        }, {
          name: 'P',
          type: 'double',
          description: 'The proportional gain. Must be in units of N/m.',
        }, {
          name: 'I',
          description: 'The integral gain. Must be in units of N/m⋅s.',
          type: 'double',
        }, {
          name: 'D',
          description: 'The derivative gain. Must be in units of N⋅s/m.',
          type: 'double',
        }, {
          name: 'Fc',
          description: 'The cutoff frequency. Must be in units of Hz.',
          type: 'double',
        },
      ],
      returnType: { type: 'ref', name: 'PidTuning', namespace: 'ascii' },
      returnDescription: 'The PID representation of the current tuning after your changes have been applied.',
    }, {
      name: 'GetPidTuning',
      description: 'Gets the PID representation of this paramset\'s servo tuning.',
      route: 'servotuning/get_pid',
      request: 'ServoTuningRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to get tuning for.',
        },
      ],
      returnType: { type: 'ref', name: 'PidTuning', namespace: 'ascii' },
      returnDescription: 'The PID representation of the current tuning.',
    }, {
      name: 'GetSimpleTuningParamDefinitions',
      description: 'Gets the parameters that are required to tune this device.',
      route: 'servotuning/get_simple_params_definition',
      request: 'AxisEmptyRequest',
      response: 'GetSimpleTuningParamDefinitionResponse',
      args: genServoTuningArgs(),
      params: [],
      returnType: { type: 'array', of: { type: 'ref', name: 'SimpleTuningParamDefinition', namespace: 'ascii' } },
      returnPropertyName: 'Params',
      returnDescription: 'The tuning parameters.',
    }, {
      name: 'SetSimpleTuning',
      description: 'Set the tuning of this device using the simple input method.',
      route: 'servotuning/set_simple_tuning',
      request: 'SetSimpleTuning',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to set tuning for.',
        }, {
          name: 'TuningParams',
          type: { type: 'array', of: { type: 'ref', name: 'ServoTuningParam', namespace: 'ascii' } },
          description: [
            'The params used to tune this device.',
            'To get what parameters are expected, call GetSimpleTuningParamList.',
            'All values must be between 0 and 1.'
          ],
        }, {
          name: 'LoadMass',
          type: 'double',
          description: 'The mass loaded on the stage (excluding the mass of the carriage itself) in kg.',
        },
      ],
      options: [
        {
          name: 'CarriageMass',
          type: { type: 'optional', of: 'double' },
          description: 'The mass of the carriage in kg. If this value is not set the default carriage mass is used.',
          defaultValue: nullValue(),
        },
      ],
    }, {
      name: 'GetSimpleTuning',
      description: 'Get the simple tuning parameters for this device.',
      route: 'servotuning/get_simple_tuning',
      request: 'ServoTuningRequest',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to get tuning for.',
        },
      ],
      returnType: { type: 'ref', name: 'SimpleTuning', namespace: 'ascii' },
      returnDescription: 'The simple tuning parameters.',
    }, {
      name: 'IsUsingSimpleTuning',
      description: 'Checks if the provided simple tuning is being stored by this paramset.',
      hidden: true,
      obsoleteReason: 'Use GetSimpleTuning instead.',
      route: 'servotuning/is_using_simple_tuning',
      request: 'SetSimpleTuning',
      response: 'BoolResponse',
      args: genServoTuningArgs(),
      params: [
        {
          name: 'Paramset',
          type: { type: 'enum', name: 'ServoTuningParamset', namespace: 'ascii' },
          description: 'The paramset to set tuning for.',
        }, {
          name: 'TuningParams',
          type: { type: 'array', of: { type: 'ref', name: 'ServoTuningParam', namespace: 'ascii' } },
          description: [
            'The params used to tune this device.',
            'To get what parameters are expected, call GetSimpleTuningParamList.',
            'All values must be between 0 and 1.'
          ],
        }, {
          name: 'LoadMass',
          type: 'double',
          description: 'The mass loaded on the stage (excluding the mass of the carriage itself) in kg.',
        }
      ],
      options: [
        {
          name: 'CarriageMass',
          type: { type: 'optional', of: 'double' },
          description: 'The mass of the carriage in kg. If this value is not set the default carriage mass is used.',
          defaultValue: nullValue(),
        },
      ],
      returnType: 'bool',
      returnPropertyName: 'Value',
      returnDescription: 'True if the provided simple tuning is currently stored in this paramset.',
    }
  ],
  instantiation: [{
    method: 'constructor',
    source: {
      namespace: 'ascii',
      class: 'Axis',
    },
  }],
};

export default definition;
