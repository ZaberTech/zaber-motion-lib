import { ClassDefinition, MethodArg, MethodDefinition } from '../../types';
import { defaultPojo, unitsValue } from '../helpers';

function genTranslatorArgs(): MethodArg[] {
  return [
    {
      name: 'TranslatorId',
      value: 'TranslatorId',
    },
  ];
}

export const sharedMethods: MethodDefinition[] = [
  {
    name: 'SetTraverseRate',
    description: 'Sets the speed at which the device moves when traversing (G0).',
    route: 'gcode/set_traverse_rate',
    request: 'TranslatorSetTraverseRateRequest',
    args: genTranslatorArgs(),
    params: [{
      name: 'traverseRate', type: 'double',
      description: 'The traverse rate.',
    }, {
      name: 'unit', type: 'VelocityUnits',
      value: unitsValue,
      description: 'Units of the traverse rate.',
    }],
    sync: true,
  },
  {
    name: 'SetAxisPosition',
    description: [
      'Sets position of translator\'s axis.',
      'Use this method to set position after performing movement outside of the translator.',
      'This method does not cause any movement.',
    ],
    route: 'gcode/set_axis_position',
    request: 'TranslatorSetAxisPositionRequest',
    args: genTranslatorArgs(),
    params: [{
      name: 'axis', type: 'string',
      description: 'Letter of the axis.',
    }, {
      name: 'position', type: 'double',
      description: 'The position.'
    }, {
      name: 'unit', type: 'LengthUnits',
      value: unitsValue,
      description: 'Units of position.',
    }],
    sync: true,
  },
  {
    name: 'GetAxisPosition',
    description: [
      'Gets position of translator\'s axis.',
      'This method does not query device but returns value from translator\'s state.',
    ],
    route: 'gcode/get_axis_position',
    request: 'TranslatorGetAxisPositionRequest',
    response: 'DoubleResponse',
    args: genTranslatorArgs(),
    params: [{
      name: 'axis',
      description: 'Letter of the axis.',
      type: 'string',
    }, {
      name: 'unit', type: 'LengthUnits',
      value: unitsValue,
      description: 'Units of position.',
    }],
    returnType: 'double',
    returnDescription: 'Position of translator\'s axis.',
    returnPropertyName: 'Value',
    sync: true,
  },
  {
    name: 'SetAxisHomePosition',
    description: [
      'Sets the home position of translator\'s axis.',
      'This position is used by G28.',
    ],
    route: 'gcode/set_axis_home',
    request: 'TranslatorSetAxisPositionRequest',
    args: genTranslatorArgs(),
    params: [{
      name: 'axis', type: 'string',
      description: 'Letter of the axis.',
    }, {
      name: 'position', type: 'double',
      description: 'The home position.'
    }, {
      name: 'unit', type: 'LengthUnits',
      value: unitsValue,
      description: 'Units of position.',
    }],
    sync: true,
  },
  {
    name: 'SetAxisSecondaryHomePosition',
    description: [
      'Sets the secondary home position of translator\'s axis.',
      'This position is used by G30.',
    ],
    route: 'gcode/set_axis_secondary_home',
    request: 'TranslatorSetAxisPositionRequest',
    args: genTranslatorArgs(),
    params: [{
      name: 'axis', type: 'string',
      description: 'Letter of the axis.',
    }, {
      name: 'position', type: 'double',
      description: 'The home position.'
    }, {
      name: 'unit', type: 'LengthUnits',
      value: unitsValue,
      description: 'Units of position.',
    }],
    sync: true,
  },
  {
    name: 'GetAxisCoordinateSystemOffset',
    description: 'Gets offset of an axis in a given coordinate system.',
    route: 'gcode/get_axis_offset',
    request: 'TranslatorGetAxisOffsetRequest',
    response: 'DoubleResponse',
    args: genTranslatorArgs(),
    params: [{
      name: 'coordinateSystem', type: 'string',
      description: 'Coordinate system (e.g. G54).',
    }, {
      name: 'axis',
      description: 'Letter of the axis.',
      type: 'string',
    }, {
      name: 'unit', type: 'LengthUnits',
      value: unitsValue,
      description: 'Units of position.',
    }],
    returnType: 'double',
    returnDescription: 'Offset in translator units of the axis.',
    returnPropertyName: 'Value',
    sync: true,
  },
  {
    name: 'ResetAfterStreamError',
    description: [
      'Resets internal state after device rejected generated command.',
      'Axis positions become uninitialized.',
    ],
    route: 'gcode/reset_after_stream_error',
    request: 'TranslatorEmptyRequest',
    args: genTranslatorArgs(),
    params: [],
    sync: true,
  },
  {
    name: 'setFeedRateOverride',
    description: 'Allows to scale feed rate of the translated code by a coefficient.',
    route: 'gcode/set_feed_rate_override',
    request: 'TranslatorSetFeedRateOverrideRequest',
    args: genTranslatorArgs(),
    params: [{
      name: 'coefficient', type: 'double',
      description: 'Coefficient of the original feed rate.',
    }],
    sync: true,
  },
  {
    name: 'Free',
    description: 'Releases native resources of a translator.',
    route: 'gcode/free',
    request: 'TranslatorEmptyRequest',
    args: [],
    params: [{
      name: 'translatorId',
      type: 'int',
      description: 'The ID of the translator.',
    }],
    sync: true,
    private: true,
    static: true,
  },
  {
    name: 'GetCurrentCoordinateSystem',
    description: 'Gets current coordinate system (e.g. G54).',
    route: 'gcode/get_current_coordinate_system',
    request: 'TranslatorEmptyRequest',
    response: 'StringResponse',
    args: genTranslatorArgs(),
    params: [],
    returnType: 'string',
    returnDescription: 'Current coordinate system.',
    returnPropertyName: 'Value',
    sync: true,
    private: true,
  },
];

const definition: ClassDefinition = {
  name: 'OfflineTranslator',
  namespace: 'gcode',
  description: [
    'Represents an offline G-Code translator.',
    'It allows to translate G-Code blocks to Zaber ASCII protocol stream commands.',
    'This translator does not need a connected device to perform translation.',
  ],
  minFw: [{ major: 7, minor: 11 }],
  properties: [
    {
      name: 'TranslatorId',
      description: 'The ID of the translator that serves to identify native resources.',
      type: 'int',
    },
    {
      name: 'CoordinateSystem',
      description: 'Current coordinate system.',
      type: 'string',
      getterCanThrow: true,
      getterFunctionName: 'GetCurrentCoordinateSystem',
    },
  ],
  ctor: {
    code: {
      ts: 'registerForFinalization(this, OfflineTranslator._free.bind(null, translatorId));',
    },
  },
  functions: [
    {
      name: 'Setup',
      static: true,
      description: 'Sets up translator from provided device definition and configuration.',
      route: 'gcode/create',
      request: 'TranslatorCreateRequest',
      args: [],
      params: [{
        name: 'definition',
        description: [
          'Definition of device and its peripherals.',
          'The definition must match a device that later performs the commands.',
        ],
        type: { type: 'ref', name: 'DeviceDefinition', namespace: 'gcode' },
      }, {
        name: 'config',
        description: 'Configuration of the translator.',
        type: { type: 'optional', of: { type: 'ref', name: 'TranslatorConfig', namespace: 'gcode' } },
        defaultValue: defaultPojo(),
      }],
      customReturn: {
        cs: 'new OfflineTranslator(response.TranslatorId)',
        py: 'OfflineTranslator(response.translator_id)',
        ts: 'new OfflineTranslator(response.translatorId)',
        java: 'new OfflineTranslator(r.getTranslatorId())',
        cpp: '{response.getTranslatorId()}',
        swift: 'OfflineTranslator(translatorId: response.translatorId)',
      },
      returnType: { type: 'ref', name: 'OfflineTranslator', namespace: 'gcode' },
      response: 'TranslatorCreateResponse',
      returnDescription: 'New instance of translator.',
    },
    {
      name: 'SetupFromDevice',
      static: true,
      description: 'Sets up an offline translator from provided device, axes, and configuration.',
      route: 'gcode/create_from_device',
      request: 'TranslatorCreateFromDeviceRequest',
      args: [{
        name: 'interfaceId',
        value: {
          ts: 'device.connection.interfaceId',
          cs: 'device.Connection.InterfaceId',
          py: 'device.connection.interface_id',
          java: 'device.getConnection().getInterfaceId()',
          cpp: 'device.getConnection().getInterfaceId()',
          swift: 'device.connection.interfaceId',
        },
      }],
      params: [{
        name: 'device',
        description: 'Device that later performs the command streaming.',
        type: { type: 'ref', name: 'Device', namespace: 'ascii' },
        value: {
          ts: 'device.deviceAddress',
          cs: 'device.DeviceAddress',
          py: 'device.device_address',
          java: 'device.getDeviceAddress()',
          cpp: 'device.getDeviceAddress()',
          swift: 'device.deviceAddress',
        },
      }, {
        name: 'axes',
        description: [
          'Axis numbers that are later used to setup the stream.',
          'For a lockstep group specify only the first axis of the group.',
        ],
        type: { type: 'array', of: 'int' },
      }, {
        name: 'config',
        description: 'Configuration of the translator.',
        type: { type: 'optional', of: { type: 'ref', name: 'TranslatorConfig', namespace: 'gcode' } },
        defaultValue: defaultPojo(),
      }],
      customReturn: {
        cs: 'new OfflineTranslator(response.TranslatorId)',
        py: 'OfflineTranslator(response.translator_id)',
        ts: 'new OfflineTranslator(response.translatorId)',
        java: 'new OfflineTranslator(r.getTranslatorId())',
        cpp: '{response.getTranslatorId()}',
        swift: 'OfflineTranslator(translatorId: response.translatorId)',
      },
      returnType: { type: 'ref', name: 'OfflineTranslator', namespace: 'gcode' },
      response: 'TranslatorCreateResponse',
      returnDescription: 'New instance of translator.',
    },
    {
      name: 'Translate',
      description: 'Translates a single block (line) of G-code.',
      route: 'gcode/translate',
      request: 'TranslatorTranslateRequest',
      args: genTranslatorArgs(),
      params: [{
        name: 'block',
        description: 'Block (line) of G-code.',
        type: 'string',
      }],
      returnType: { type: 'ref', name: 'TranslateResult', namespace: 'gcode' },
      returnDescription: 'Result of translation containing the stream commands.',
      sync: true,
    },
    {
      name: 'Flush',
      description: [
        'Flushes the remaining stream commands waiting in optimization buffer.',
        'The flush is also performed by M2 and M30 codes.',
      ],
      route: 'gcode/flush',
      request: 'TranslatorEmptyRequest',
      args: genTranslatorArgs(),
      params: [],
      returnType: { type: 'array', of: 'string' },
      response: 'TranslatorFlushResponse',
      returnPropertyName: 'Commands',
      returnDescription: 'The remaining stream commands.',
      sync: true,
    },
    ...sharedMethods,
  ],
  cpp: {
    noCopyCtor: true,
  },
  instantiation: [{
    method: 'static',
    source: {
      namespace: 'gcode',
      class: 'OfflineTranslator',
      member: 'SetupFromDevice',
    },
    args: [{
      namespace: 'ascii',
      class: 'Device',
    },
    '1, 2'
    ],
  }, {
    method: 'static',
    source: {
      namespace: 'gcode',
      class: 'OfflineTranslator',
      member: 'Setup',
    },
    args: ['...'],
  }],
};

export default definition;
