import { ClassDefinition, MethodArg } from '../../types';
import { defaultBool, defaultPojo } from '../helpers';

import deathTranslator, { sharedMethods } from './offline_translator';

function genTranslatorArgs(): MethodArg[] {
  return [
    {
      name: 'TranslatorId',
      value: 'TranslatorId',
    },
  ];
}

const definition: ClassDefinition = {
  name: 'Translator',
  namespace: 'gcode',
  description: [
    'Represents a live G-Code translator.',
    'It allows to stream G-Code blocks to a connected device.',
    'It requires a stream to be setup on the device.',
  ],
  minFw: [{ major: 7, minor: 11 }],
  properties: deathTranslator.properties,
  ctor: {
    code: {
      ts: 'registerForFinalization(this, Translator._free.bind(null, translatorId));',
    },
  },
  functions: [
    {
      name: 'Setup',
      static: true,
      description: 'Sets up the translator on top of a provided stream.',
      route: 'gcode/create_live',
      request: 'TranslatorCreateLiveRequest',
      args: [{
        name: 'device',
        value: {
          ts: 'stream.device.deviceAddress',
          cs: 'stream.Device.DeviceAddress',
          py: 'stream.device.device_address',
          java: 'stream.getDevice().getDeviceAddress()',
          cpp: 'stream.getDevice().getDeviceAddress()',
          swift: 'stream.device.deviceAddress',
        },
      }, {
        name: 'interfaceId',
        value: {
          ts: 'stream.device.connection.interfaceId',
          cs: 'stream.Device.Connection.InterfaceId',
          py: 'stream.device.connection.interface_id',
          java: 'stream.getDevice().getConnection().getInterfaceId()',
          cpp: 'stream.getDevice().getConnection().getInterfaceId()',
          swift: 'stream.device.connection.interfaceId',
        },
      }],
      params: [{
        name: 'stream',
        requestName: 'streamId',
        description: [
          'The stream to setup the translator on.',
          'The stream must be already setup in a live or a store mode.',
        ],
        type: { type: 'ref', name: 'Stream', namespace: 'ascii' },
        value: {
          cs: 'stream.StreamId',
          ts: 'stream.streamId',
          py: 'stream.stream_id',
          java: 'stream.getStreamId()',
          cpp: 'stream.getStreamId()',
          swift: 'stream.streamId',
        },
      }, {
        name: 'config',
        description: 'Configuration of the translator.',
        type: { type: 'optional', of: { type: 'ref', name: 'TranslatorConfig', namespace: 'gcode' } },
        defaultValue: defaultPojo(),
      }],
      customReturn: {
        cs: 'new Translator(response.TranslatorId)',
        py: 'Translator(response.translator_id)',
        ts: 'new Translator(response.translatorId)',
        java: 'new Translator(r.getTranslatorId())',
        cpp: '{response.getTranslatorId()}',
        swift: 'Translator(translatorId: response.translatorId)',
      },
      returnType: { type: 'ref', name: 'Translator', namespace: 'gcode' },
      response: 'TranslatorCreateResponse',
      returnDescription: 'New instance of translator.',
    },
    {
      name: 'Translate',
      description: [
        'Translates a single block (line) of G-code.',
        'The commands are queued in the underlying stream to ensure smooth continues movement.',
        'Returning of this method indicates that the commands are queued (not necessarily executed).',
      ],
      route: 'gcode/translate_live',
      request: 'TranslatorTranslateRequest',
      args: genTranslatorArgs(),
      params: [{
        name: 'block',
        description: 'Block (line) of G-code.',
        type: 'string',
      }],
      returnType: { type: 'ref', name: 'TranslateResult', namespace: 'gcode' },
      returnDescription: 'Result of translation containing the commands sent to the device.',
    },
    {
      name: 'Flush',
      description: [
        'Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.',
        'The flush is also performed by M2 and M30 codes.',
      ],
      route: 'gcode/flush_live',
      request: 'TranslatorFlushLiveRequest',
      args: genTranslatorArgs(),
      params: [],
      options: [{
        name: 'waitUntilIdle', type: 'bool',
        defaultValue: defaultBool(true),
        description: 'Determines whether to wait for the stream to finish all the movements.',
      }],
      returnType: { type: 'array', of: 'string' },
      response: 'TranslatorFlushResponse',
      returnPropertyName: 'Commands',
      returnDescription: 'The remaining stream commands.',
    },
    {
      name: 'ResetPosition',
      description: [
        'Resets position of the translator from the underlying stream.',
        'Call this method after performing a movement outside of translator.',
      ],
      route: 'gcode/reset_position_from_stream',
      request: 'TranslatorEmptyRequest',
      args: genTranslatorArgs(),
      params: [],
    },
    ...sharedMethods,
  ],
  cpp: {
    noCopyCtor: true,
  },
  instantiation: [{
    method: 'static',
    source: {
      namespace: 'gcode',
      class: 'Translator',
      member: 'Setup',
    },
    args: [{
      namespace: 'ascii',
      class: 'Stream',
    }],
  }],
};

export default definition;
