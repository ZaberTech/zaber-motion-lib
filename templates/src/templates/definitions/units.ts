/* eslint-disable */
// This file is generated from the Zaber device database. Do not manually edit this file.
export default [
  {
    "DimensionName": "Native",
    "dimensionName": "Native",
    "units": [
      {
        "LongName": "Device native units",
        "ShortName": null,
        "names": {
          "ts": "NATIVE",
          "cs": "Native",
          "py": "NATIVE",
          "java": "NATIVE",
          "cpp": "NATIVE",
          "swift": "native"
        }
      }
    ]
  },
  {
    "DimensionName": "Length",
    "dimensionName": "Length",
    "units": [
      {
        "LongName": "metres",
        "ShortName": "m",
        "names": {
          "ts": "METRES",
          "cs": "Length_Metres",
          "py": "LENGTH_METRES",
          "java": "LENGTH_METRES",
          "cpp": "LENGTH_METRES",
          "swift": "lengthMetres"
        }
      },
      {
        "LongName": "centimetres",
        "ShortName": "cm",
        "names": {
          "ts": "CENTIMETRES",
          "cs": "Length_Centimetres",
          "py": "LENGTH_CENTIMETRES",
          "java": "LENGTH_CENTIMETRES",
          "cpp": "LENGTH_CENTIMETRES",
          "swift": "lengthCentimetres"
        }
      },
      {
        "LongName": "millimetres",
        "ShortName": "mm",
        "names": {
          "ts": "MILLIMETRES",
          "cs": "Length_Millimetres",
          "py": "LENGTH_MILLIMETRES",
          "java": "LENGTH_MILLIMETRES",
          "cpp": "LENGTH_MILLIMETRES",
          "swift": "lengthMillimetres"
        }
      },
      {
        "LongName": "micrometres",
        "ShortName": "µm",
        "names": {
          "ts": "MICROMETRES",
          "cs": "Length_Micrometres",
          "py": "LENGTH_MICROMETRES",
          "java": "LENGTH_MICROMETRES",
          "cpp": "LENGTH_MICROMETRES",
          "swift": "lengthMicrometres"
        }
      },
      {
        "LongName": "nanometres",
        "ShortName": "nm",
        "names": {
          "ts": "NANOMETRES",
          "cs": "Length_Nanometres",
          "py": "LENGTH_NANOMETRES",
          "java": "LENGTH_NANOMETRES",
          "cpp": "LENGTH_NANOMETRES",
          "swift": "lengthNanometres"
        }
      },
      {
        "LongName": "inches",
        "ShortName": "in",
        "names": {
          "ts": "INCHES",
          "cs": "Length_Inches",
          "py": "LENGTH_INCHES",
          "java": "LENGTH_INCHES",
          "cpp": "LENGTH_INCHES",
          "swift": "lengthInches"
        }
      }
    ]
  },
  {
    "DimensionName": "Velocity",
    "dimensionName": "Velocity",
    "units": [
      {
        "LongName": "metres per second",
        "ShortName": "m/s",
        "names": {
          "ts": "METRES_PER_SECOND",
          "cs": "Velocity_MetresPerSecond",
          "py": "VELOCITY_METRES_PER_SECOND",
          "java": "VELOCITY_METRES_PER_SECOND",
          "cpp": "VELOCITY_METRES_PER_SECOND",
          "swift": "velocityMetresPerSecond"
        }
      },
      {
        "LongName": "centimetres per second",
        "ShortName": "cm/s",
        "names": {
          "ts": "CENTIMETRES_PER_SECOND",
          "cs": "Velocity_CentimetresPerSecond",
          "py": "VELOCITY_CENTIMETRES_PER_SECOND",
          "java": "VELOCITY_CENTIMETRES_PER_SECOND",
          "cpp": "VELOCITY_CENTIMETRES_PER_SECOND",
          "swift": "velocityCentimetresPerSecond"
        }
      },
      {
        "LongName": "millimetres per second",
        "ShortName": "mm/s",
        "names": {
          "ts": "MILLIMETRES_PER_SECOND",
          "cs": "Velocity_MillimetresPerSecond",
          "py": "VELOCITY_MILLIMETRES_PER_SECOND",
          "java": "VELOCITY_MILLIMETRES_PER_SECOND",
          "cpp": "VELOCITY_MILLIMETRES_PER_SECOND",
          "swift": "velocityMillimetresPerSecond"
        }
      },
      {
        "LongName": "micrometres per second",
        "ShortName": "µm/s",
        "names": {
          "ts": "MICROMETRES_PER_SECOND",
          "cs": "Velocity_MicrometresPerSecond",
          "py": "VELOCITY_MICROMETRES_PER_SECOND",
          "java": "VELOCITY_MICROMETRES_PER_SECOND",
          "cpp": "VELOCITY_MICROMETRES_PER_SECOND",
          "swift": "velocityMicrometresPerSecond"
        }
      },
      {
        "LongName": "nanometres per second",
        "ShortName": "nm/s",
        "names": {
          "ts": "NANOMETRES_PER_SECOND",
          "cs": "Velocity_NanometresPerSecond",
          "py": "VELOCITY_NANOMETRES_PER_SECOND",
          "java": "VELOCITY_NANOMETRES_PER_SECOND",
          "cpp": "VELOCITY_NANOMETRES_PER_SECOND",
          "swift": "velocityNanometresPerSecond"
        }
      },
      {
        "LongName": "inches per second",
        "ShortName": "in/s",
        "names": {
          "ts": "INCHES_PER_SECOND",
          "cs": "Velocity_InchesPerSecond",
          "py": "VELOCITY_INCHES_PER_SECOND",
          "java": "VELOCITY_INCHES_PER_SECOND",
          "cpp": "VELOCITY_INCHES_PER_SECOND",
          "swift": "velocityInchesPerSecond"
        }
      }
    ]
  },
  {
    "DimensionName": "Acceleration",
    "dimensionName": "Acceleration",
    "units": [
      {
        "LongName": "metres per second squared",
        "ShortName": "m/s²",
        "names": {
          "ts": "METRES_PER_SECOND_SQUARED",
          "cs": "Acceleration_MetresPerSecondSquared",
          "py": "ACCELERATION_METRES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_METRES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_METRES_PER_SECOND_SQUARED",
          "swift": "accelerationMetresPerSecondSquared"
        }
      },
      {
        "LongName": "centimetres per second squared",
        "ShortName": "cm/s²",
        "names": {
          "ts": "CENTIMETRES_PER_SECOND_SQUARED",
          "cs": "Acceleration_CentimetresPerSecondSquared",
          "py": "ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED",
          "swift": "accelerationCentimetresPerSecondSquared"
        }
      },
      {
        "LongName": "millimetres per second squared",
        "ShortName": "mm/s²",
        "names": {
          "ts": "MILLIMETRES_PER_SECOND_SQUARED",
          "cs": "Acceleration_MillimetresPerSecondSquared",
          "py": "ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED",
          "swift": "accelerationMillimetresPerSecondSquared"
        }
      },
      {
        "LongName": "micrometres per second squared",
        "ShortName": "µm/s²",
        "names": {
          "ts": "MICROMETRES_PER_SECOND_SQUARED",
          "cs": "Acceleration_MicrometresPerSecondSquared",
          "py": "ACCELERATION_MICROMETRES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_MICROMETRES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_MICROMETRES_PER_SECOND_SQUARED",
          "swift": "accelerationMicrometresPerSecondSquared"
        }
      },
      {
        "LongName": "nanometres per second squared",
        "ShortName": "nm/s²",
        "names": {
          "ts": "NANOMETRES_PER_SECOND_SQUARED",
          "cs": "Acceleration_NanometresPerSecondSquared",
          "py": "ACCELERATION_NANOMETRES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_NANOMETRES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_NANOMETRES_PER_SECOND_SQUARED",
          "swift": "accelerationNanometresPerSecondSquared"
        }
      },
      {
        "LongName": "inches per second squared",
        "ShortName": "in/s²",
        "names": {
          "ts": "INCHES_PER_SECOND_SQUARED",
          "cs": "Acceleration_InchesPerSecondSquared",
          "py": "ACCELERATION_INCHES_PER_SECOND_SQUARED",
          "java": "ACCELERATION_INCHES_PER_SECOND_SQUARED",
          "cpp": "ACCELERATION_INCHES_PER_SECOND_SQUARED",
          "swift": "accelerationInchesPerSecondSquared"
        }
      }
    ]
  },
  {
    "DimensionName": "Angle",
    "dimensionName": "Angle",
    "units": [
      {
        "LongName": "degrees",
        "ShortName": "°",
        "names": {
          "ts": "DEGREES",
          "cs": "Angle_Degrees",
          "py": "ANGLE_DEGREES",
          "java": "ANGLE_DEGREES",
          "cpp": "ANGLE_DEGREES",
          "swift": "angleDegrees"
        }
      },
      {
        "LongName": "radians",
        "ShortName": "rad",
        "names": {
          "ts": "RADIANS",
          "cs": "Angle_Radians",
          "py": "ANGLE_RADIANS",
          "java": "ANGLE_RADIANS",
          "cpp": "ANGLE_RADIANS",
          "swift": "angleRadians"
        }
      }
    ]
  },
  {
    "DimensionName": "Angular Velocity",
    "dimensionName": "AngularVelocity",
    "units": [
      {
        "LongName": "degrees per second",
        "ShortName": "°/s",
        "names": {
          "ts": "DEGREES_PER_SECOND",
          "cs": "AngularVelocity_DegreesPerSecond",
          "py": "ANGULAR_VELOCITY_DEGREES_PER_SECOND",
          "java": "ANGULAR_VELOCITY_DEGREES_PER_SECOND",
          "cpp": "ANGULAR_VELOCITY_DEGREES_PER_SECOND",
          "swift": "angularVelocityDegreesPerSecond"
        }
      },
      {
        "LongName": "radians per second",
        "ShortName": "rad/s",
        "names": {
          "ts": "RADIANS_PER_SECOND",
          "cs": "AngularVelocity_RadiansPerSecond",
          "py": "ANGULAR_VELOCITY_RADIANS_PER_SECOND",
          "java": "ANGULAR_VELOCITY_RADIANS_PER_SECOND",
          "cpp": "ANGULAR_VELOCITY_RADIANS_PER_SECOND",
          "swift": "angularVelocityRadiansPerSecond"
        }
      }
    ]
  },
  {
    "DimensionName": "Angular Acceleration",
    "dimensionName": "AngularAcceleration",
    "units": [
      {
        "LongName": "degrees per second squared",
        "ShortName": "°/s²",
        "names": {
          "ts": "DEGREES_PER_SECOND_SQUARED",
          "cs": "AngularAcceleration_DegreesPerSecondSquared",
          "py": "ANGULAR_ACCELERATION_DEGREES_PER_SECOND_SQUARED",
          "java": "ANGULAR_ACCELERATION_DEGREES_PER_SECOND_SQUARED",
          "cpp": "ANGULAR_ACCELERATION_DEGREES_PER_SECOND_SQUARED",
          "swift": "angularAccelerationDegreesPerSecondSquared"
        }
      },
      {
        "LongName": "radians per second squared",
        "ShortName": "rad/s²",
        "names": {
          "ts": "RADIANS_PER_SECOND_SQUARED",
          "cs": "AngularAcceleration_RadiansPerSecondSquared",
          "py": "ANGULAR_ACCELERATION_RADIANS_PER_SECOND_SQUARED",
          "java": "ANGULAR_ACCELERATION_RADIANS_PER_SECOND_SQUARED",
          "cpp": "ANGULAR_ACCELERATION_RADIANS_PER_SECOND_SQUARED",
          "swift": "angularAccelerationRadiansPerSecondSquared"
        }
      }
    ]
  },
  {
    "DimensionName": "AC Electric Current",
    "dimensionName": "ACElectricCurrent",
    "units": [
      {
        "LongName": "amperes peak",
        "ShortName": "A(peak)",
        "names": {
          "ts": "AMPERES_PEAK",
          "cs": "ACElectricCurrent_AmperesPeak",
          "py": "AC_ELECTRIC_CURRENT_AMPERES_PEAK",
          "java": "AC_ELECTRIC_CURRENT_AMPERES_PEAK",
          "cpp": "AC_ELECTRIC_CURRENT_AMPERES_PEAK",
          "swift": "aCElectricCurrentAmperesPeak"
        }
      },
      {
        "LongName": "amperes RMS",
        "ShortName": "A(RMS)",
        "names": {
          "ts": "AMPERES_RMS",
          "cs": "ACElectricCurrent_AmperesRms",
          "py": "AC_ELECTRIC_CURRENT_AMPERES_RMS",
          "java": "AC_ELECTRIC_CURRENT_AMPERES_RMS",
          "cpp": "AC_ELECTRIC_CURRENT_AMPERES_RMS",
          "swift": "aCElectricCurrentAmperesRms"
        }
      }
    ]
  },
  {
    "DimensionName": "Percent",
    "dimensionName": "Percent",
    "units": [
      {
        "LongName": "percent",
        "ShortName": "%",
        "names": {
          "ts": "PERCENT",
          "cs": "Percent_Percent",
          "py": "PERCENT_PERCENT",
          "java": "PERCENT_PERCENT",
          "cpp": "PERCENT_PERCENT",
          "swift": "percentPercent"
        }
      }
    ]
  },
  {
    "DimensionName": "DC Electric Current",
    "dimensionName": "DCElectricCurrent",
    "units": [
      {
        "LongName": "amperes",
        "ShortName": "A",
        "names": {
          "ts": "AMPERES",
          "cs": "DCElectricCurrent_Amperes",
          "py": "DC_ELECTRIC_CURRENT_AMPERES",
          "java": "DC_ELECTRIC_CURRENT_AMPERES",
          "cpp": "DC_ELECTRIC_CURRENT_AMPERES",
          "swift": "dCElectricCurrentAmperes"
        }
      }
    ]
  },
  {
    "DimensionName": "Force",
    "dimensionName": "Force",
    "units": [
      {
        "LongName": "newtons",
        "ShortName": "N",
        "names": {
          "ts": "NEWTONS",
          "cs": "Force_Newtons",
          "py": "FORCE_NEWTONS",
          "java": "FORCE_NEWTONS",
          "cpp": "FORCE_NEWTONS",
          "swift": "forceNewtons"
        }
      },
      {
        "LongName": "millinewtons",
        "ShortName": "mN",
        "names": {
          "ts": "MILLINEWTONS",
          "cs": "Force_Millinewtons",
          "py": "FORCE_MILLINEWTONS",
          "java": "FORCE_MILLINEWTONS",
          "cpp": "FORCE_MILLINEWTONS",
          "swift": "forceMillinewtons"
        }
      },
      {
        "LongName": "pounds-force",
        "ShortName": "lbf",
        "names": {
          "ts": "POUNDS_FORCE",
          "cs": "Force_PoundsForce",
          "py": "FORCE_POUNDS_FORCE",
          "java": "FORCE_POUNDS_FORCE",
          "cpp": "FORCE_POUNDS_FORCE",
          "swift": "forcePoundsForce"
        }
      },
      {
        "LongName": "kilonewtons",
        "ShortName": "kN",
        "names": {
          "ts": "KILONEWTONS",
          "cs": "Force_Kilonewtons",
          "py": "FORCE_KILONEWTONS",
          "java": "FORCE_KILONEWTONS",
          "cpp": "FORCE_KILONEWTONS",
          "swift": "forceKilonewtons"
        }
      }
    ]
  },
  {
    "DimensionName": "Time",
    "dimensionName": "Time",
    "units": [
      {
        "LongName": "seconds",
        "ShortName": "s",
        "names": {
          "ts": "SECONDS",
          "cs": "Time_Seconds",
          "py": "TIME_SECONDS",
          "java": "TIME_SECONDS",
          "cpp": "TIME_SECONDS",
          "swift": "timeSeconds"
        }
      },
      {
        "LongName": "milliseconds",
        "ShortName": "ms",
        "names": {
          "ts": "MILLISECONDS",
          "cs": "Time_Milliseconds",
          "py": "TIME_MILLISECONDS",
          "java": "TIME_MILLISECONDS",
          "cpp": "TIME_MILLISECONDS",
          "swift": "timeMilliseconds"
        }
      },
      {
        "LongName": "microseconds",
        "ShortName": "µs",
        "names": {
          "ts": "MICROSECONDS",
          "cs": "Time_Microseconds",
          "py": "TIME_MICROSECONDS",
          "java": "TIME_MICROSECONDS",
          "cpp": "TIME_MICROSECONDS",
          "swift": "timeMicroseconds"
        }
      }
    ]
  },
  {
    "DimensionName": "Torque",
    "dimensionName": "Torque",
    "units": [
      {
        "LongName": "newton metres",
        "ShortName": "N⋅m",
        "names": {
          "ts": "NEWTON_METRES",
          "cs": "Torque_NewtonMetres",
          "py": "TORQUE_NEWTON_METRES",
          "java": "TORQUE_NEWTON_METRES",
          "cpp": "TORQUE_NEWTON_METRES",
          "swift": "torqueNewtonMetres"
        }
      },
      {
        "LongName": "newton centimetres",
        "ShortName": "N⋅cm",
        "names": {
          "ts": "NEWTON_CENTIMETRES",
          "cs": "Torque_NewtonCentimetres",
          "py": "TORQUE_NEWTON_CENTIMETRES",
          "java": "TORQUE_NEWTON_CENTIMETRES",
          "cpp": "TORQUE_NEWTON_CENTIMETRES",
          "swift": "torqueNewtonCentimetres"
        }
      },
      {
        "LongName": "pound-force-feet",
        "ShortName": "lbf⋅ft",
        "names": {
          "ts": "POUND_FORCE_FEET",
          "cs": "Torque_PoundForceFeet",
          "py": "TORQUE_POUND_FORCE_FEET",
          "java": "TORQUE_POUND_FORCE_FEET",
          "cpp": "TORQUE_POUND_FORCE_FEET",
          "swift": "torquePoundForceFeet"
        }
      },
      {
        "LongName": "ounce-force-inches",
        "ShortName": "ozf⋅in",
        "names": {
          "ts": "OUNCE_FORCE_INCHES",
          "cs": "Torque_OunceForceInches",
          "py": "TORQUE_OUNCE_FORCE_INCHES",
          "java": "TORQUE_OUNCE_FORCE_INCHES",
          "cpp": "TORQUE_OUNCE_FORCE_INCHES",
          "swift": "torqueOunceForceInches"
        }
      }
    ]
  },
  {
    "DimensionName": "Inertia",
    "dimensionName": "Inertia",
    "units": [
      {
        "LongName": "grams",
        "ShortName": "g",
        "names": {
          "ts": "GRAMS",
          "cs": "Inertia_Grams",
          "py": "INERTIA_GRAMS",
          "java": "INERTIA_GRAMS",
          "cpp": "INERTIA_GRAMS",
          "swift": "inertiaGrams"
        }
      },
      {
        "LongName": "kilograms",
        "ShortName": "kg",
        "names": {
          "ts": "KILOGRAMS",
          "cs": "Inertia_Kilograms",
          "py": "INERTIA_KILOGRAMS",
          "java": "INERTIA_KILOGRAMS",
          "cpp": "INERTIA_KILOGRAMS",
          "swift": "inertiaKilograms"
        }
      },
      {
        "LongName": "milligrams",
        "ShortName": "mg",
        "names": {
          "ts": "MILLIGRAMS",
          "cs": "Inertia_Milligrams",
          "py": "INERTIA_MILLIGRAMS",
          "java": "INERTIA_MILLIGRAMS",
          "cpp": "INERTIA_MILLIGRAMS",
          "swift": "inertiaMilligrams"
        }
      },
      {
        "LongName": "pounds",
        "ShortName": "lb",
        "names": {
          "ts": "POUNDS",
          "cs": "Inertia_Pounds",
          "py": "INERTIA_POUNDS",
          "java": "INERTIA_POUNDS",
          "cpp": "INERTIA_POUNDS",
          "swift": "inertiaPounds"
        }
      },
      {
        "LongName": "ounces",
        "ShortName": "oz",
        "names": {
          "ts": "OUNCES",
          "cs": "Inertia_Ounces",
          "py": "INERTIA_OUNCES",
          "java": "INERTIA_OUNCES",
          "cpp": "INERTIA_OUNCES",
          "swift": "inertiaOunces"
        }
      }
    ]
  },
  {
    "DimensionName": "Rotational Inertia",
    "dimensionName": "RotationalInertia",
    "units": [
      {
        "LongName": "gram-square metre",
        "ShortName": "g⋅m²",
        "names": {
          "ts": "GRAM_SQUARE_METRE",
          "cs": "RotationalInertia_GramSquareMetre",
          "py": "ROTATIONAL_INERTIA_GRAM_SQUARE_METRE",
          "java": "ROTATIONAL_INERTIA_GRAM_SQUARE_METRE",
          "cpp": "ROTATIONAL_INERTIA_GRAM_SQUARE_METRE",
          "swift": "rotationalInertiaGramSquareMetre"
        }
      },
      {
        "LongName": "kilogram-square metre",
        "ShortName": "kg⋅m²",
        "names": {
          "ts": "KILOGRAM_SQUARE_METRE",
          "cs": "RotationalInertia_KilogramSquareMetre",
          "py": "ROTATIONAL_INERTIA_KILOGRAM_SQUARE_METRE",
          "java": "ROTATIONAL_INERTIA_KILOGRAM_SQUARE_METRE",
          "cpp": "ROTATIONAL_INERTIA_KILOGRAM_SQUARE_METRE",
          "swift": "rotationalInertiaKilogramSquareMetre"
        }
      },
      {
        "LongName": "pound-square-feet",
        "ShortName": "lb⋅ft²",
        "names": {
          "ts": "POUND_SQUARE_FEET",
          "cs": "RotationalInertia_PoundSquareFeet",
          "py": "ROTATIONAL_INERTIA_POUND_SQUARE_FEET",
          "java": "ROTATIONAL_INERTIA_POUND_SQUARE_FEET",
          "cpp": "ROTATIONAL_INERTIA_POUND_SQUARE_FEET",
          "swift": "rotationalInertiaPoundSquareFeet"
        }
      }
    ]
  },
  {
    "DimensionName": "Force Constant",
    "dimensionName": "ForceConstant",
    "units": [
      {
        "LongName": "newtons per amp",
        "ShortName": "N/A",
        "names": {
          "ts": "NEWTONS_PER_AMP",
          "cs": "ForceConstant_NewtonsPerAmp",
          "py": "FORCE_CONSTANT_NEWTONS_PER_AMP",
          "java": "FORCE_CONSTANT_NEWTONS_PER_AMP",
          "cpp": "FORCE_CONSTANT_NEWTONS_PER_AMP",
          "swift": "forceConstantNewtonsPerAmp"
        }
      },
      {
        "LongName": "millinewtons per amp",
        "ShortName": "mN/A",
        "names": {
          "ts": "MILLINEWTONS_PER_AMP",
          "cs": "ForceConstant_MillinewtonsPerAmp",
          "py": "FORCE_CONSTANT_MILLINEWTONS_PER_AMP",
          "java": "FORCE_CONSTANT_MILLINEWTONS_PER_AMP",
          "cpp": "FORCE_CONSTANT_MILLINEWTONS_PER_AMP",
          "swift": "forceConstantMillinewtonsPerAmp"
        }
      },
      {
        "LongName": "kilonewtons per amp",
        "ShortName": "kN/A",
        "names": {
          "ts": "KILONEWTONS_PER_AMP",
          "cs": "ForceConstant_KilonewtonsPerAmp",
          "py": "FORCE_CONSTANT_KILONEWTONS_PER_AMP",
          "java": "FORCE_CONSTANT_KILONEWTONS_PER_AMP",
          "cpp": "FORCE_CONSTANT_KILONEWTONS_PER_AMP",
          "swift": "forceConstantKilonewtonsPerAmp"
        }
      },
      {
        "LongName": "pounds-force per amp",
        "ShortName": "lbf/A",
        "names": {
          "ts": "POUNDS_FORCE_PER_AMP",
          "cs": "ForceConstant_PoundsForcePerAmp",
          "py": "FORCE_CONSTANT_POUNDS_FORCE_PER_AMP",
          "java": "FORCE_CONSTANT_POUNDS_FORCE_PER_AMP",
          "cpp": "FORCE_CONSTANT_POUNDS_FORCE_PER_AMP",
          "swift": "forceConstantPoundsForcePerAmp"
        }
      }
    ]
  },
  {
    "DimensionName": "Torque Constant",
    "dimensionName": "TorqueConstant",
    "units": [
      {
        "LongName": "newton metres per amp",
        "ShortName": "N⋅m/A",
        "names": {
          "ts": "NEWTON_METRES_PER_AMP",
          "cs": "TorqueConstant_NewtonMetresPerAmp",
          "py": "TORQUE_CONSTANT_NEWTON_METRES_PER_AMP",
          "java": "TORQUE_CONSTANT_NEWTON_METRES_PER_AMP",
          "cpp": "TORQUE_CONSTANT_NEWTON_METRES_PER_AMP",
          "swift": "torqueConstantNewtonMetresPerAmp"
        }
      },
      {
        "LongName": "millinewton metres per amp",
        "ShortName": "mN⋅m/A",
        "names": {
          "ts": "MILLINEWTON_METRES_PER_AMP",
          "cs": "TorqueConstant_MillinewtonMetresPerAmp",
          "py": "TORQUE_CONSTANT_MILLINEWTON_METRES_PER_AMP",
          "java": "TORQUE_CONSTANT_MILLINEWTON_METRES_PER_AMP",
          "cpp": "TORQUE_CONSTANT_MILLINEWTON_METRES_PER_AMP",
          "swift": "torqueConstantMillinewtonMetresPerAmp"
        }
      },
      {
        "LongName": "kilonewton metres per amp",
        "ShortName": "kN⋅m/A",
        "names": {
          "ts": "KILONEWTON_METRES_PER_AMP",
          "cs": "TorqueConstant_KilonewtonMetresPerAmp",
          "py": "TORQUE_CONSTANT_KILONEWTON_METRES_PER_AMP",
          "java": "TORQUE_CONSTANT_KILONEWTON_METRES_PER_AMP",
          "cpp": "TORQUE_CONSTANT_KILONEWTON_METRES_PER_AMP",
          "swift": "torqueConstantKilonewtonMetresPerAmp"
        }
      },
      {
        "LongName": "pound-force-feet per amp",
        "ShortName": "lbf⋅ft/A",
        "names": {
          "ts": "POUND_FORCE_FEET_PER_AMP",
          "cs": "TorqueConstant_PoundForceFeetPerAmp",
          "py": "TORQUE_CONSTANT_POUND_FORCE_FEET_PER_AMP",
          "java": "TORQUE_CONSTANT_POUND_FORCE_FEET_PER_AMP",
          "cpp": "TORQUE_CONSTANT_POUND_FORCE_FEET_PER_AMP",
          "swift": "torqueConstantPoundForceFeetPerAmp"
        }
      }
    ]
  },
  {
    "DimensionName": "Voltage",
    "dimensionName": "Voltage",
    "units": [
      {
        "LongName": "volts",
        "ShortName": "V",
        "names": {
          "ts": "VOLTS",
          "cs": "Voltage_Volts",
          "py": "VOLTAGE_VOLTS",
          "java": "VOLTAGE_VOLTS",
          "cpp": "VOLTAGE_VOLTS",
          "swift": "voltageVolts"
        }
      },
      {
        "LongName": "millivolts",
        "ShortName": "mV",
        "names": {
          "ts": "MILLIVOLTS",
          "cs": "Voltage_Millivolts",
          "py": "VOLTAGE_MILLIVOLTS",
          "java": "VOLTAGE_MILLIVOLTS",
          "cpp": "VOLTAGE_MILLIVOLTS",
          "swift": "voltageMillivolts"
        }
      },
      {
        "LongName": "microvolts",
        "ShortName": "µV",
        "names": {
          "ts": "MICROVOLTS",
          "cs": "Voltage_Microvolts",
          "py": "VOLTAGE_MICROVOLTS",
          "java": "VOLTAGE_MICROVOLTS",
          "cpp": "VOLTAGE_MICROVOLTS",
          "swift": "voltageMicrovolts"
        }
      }
    ]
  },
  {
    "DimensionName": "Current Controller Proportional Gain",
    "dimensionName": "CurrentControllerProportionalGain",
    "units": [
      {
        "LongName": "volts per amp",
        "ShortName": "V/A",
        "names": {
          "ts": "VOLTS_PER_AMP",
          "cs": "CurrentControllerProportionalGain_VoltsPerAmp",
          "py": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_VOLTS_PER_AMP",
          "java": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_VOLTS_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_VOLTS_PER_AMP",
          "swift": "currentControllerProportionalGainVoltsPerAmp"
        }
      },
      {
        "LongName": "millivolts per amp",
        "ShortName": "mV/A",
        "names": {
          "ts": "MILLIVOLTS_PER_AMP",
          "cs": "CurrentControllerProportionalGain_MillivoltsPerAmp",
          "py": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MILLIVOLTS_PER_AMP",
          "java": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MILLIVOLTS_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MILLIVOLTS_PER_AMP",
          "swift": "currentControllerProportionalGainMillivoltsPerAmp"
        }
      },
      {
        "LongName": "microvolts per amp",
        "ShortName": "µV/A",
        "names": {
          "ts": "MICROVOLTS_PER_AMP",
          "cs": "CurrentControllerProportionalGain_MicrovoltsPerAmp",
          "py": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MICROVOLTS_PER_AMP",
          "java": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MICROVOLTS_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MICROVOLTS_PER_AMP",
          "swift": "currentControllerProportionalGainMicrovoltsPerAmp"
        }
      }
    ]
  },
  {
    "DimensionName": "Current Controller Integral Gain",
    "dimensionName": "CurrentControllerIntegralGain",
    "units": [
      {
        "LongName": "volts per amp per second",
        "ShortName": "V/(A⋅s)",
        "names": {
          "ts": "VOLTS_PER_AMP_PER_SECOND",
          "cs": "CurrentControllerIntegralGain_VoltsPerAmpPerSecond",
          "py": "CURRENT_CONTROLLER_INTEGRAL_GAIN_VOLTS_PER_AMP_PER_SECOND",
          "java": "CURRENT_CONTROLLER_INTEGRAL_GAIN_VOLTS_PER_AMP_PER_SECOND",
          "cpp": "CURRENT_CONTROLLER_INTEGRAL_GAIN_VOLTS_PER_AMP_PER_SECOND",
          "swift": "currentControllerIntegralGainVoltsPerAmpPerSecond"
        }
      },
      {
        "LongName": "millivolts per amp per second",
        "ShortName": "mV/(A⋅s)",
        "names": {
          "ts": "MILLIVOLTS_PER_AMP_PER_SECOND",
          "cs": "CurrentControllerIntegralGain_MillivoltsPerAmpPerSecond",
          "py": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MILLIVOLTS_PER_AMP_PER_SECOND",
          "java": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MILLIVOLTS_PER_AMP_PER_SECOND",
          "cpp": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MILLIVOLTS_PER_AMP_PER_SECOND",
          "swift": "currentControllerIntegralGainMillivoltsPerAmpPerSecond"
        }
      },
      {
        "LongName": "microvolts per amp per second",
        "ShortName": "µV/(A⋅s)",
        "names": {
          "ts": "MICROVOLTS_PER_AMP_PER_SECOND",
          "cs": "CurrentControllerIntegralGain_MicrovoltsPerAmpPerSecond",
          "py": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MICROVOLTS_PER_AMP_PER_SECOND",
          "java": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MICROVOLTS_PER_AMP_PER_SECOND",
          "cpp": "CURRENT_CONTROLLER_INTEGRAL_GAIN_MICROVOLTS_PER_AMP_PER_SECOND",
          "swift": "currentControllerIntegralGainMicrovoltsPerAmpPerSecond"
        }
      }
    ]
  },
  {
    "DimensionName": "Current Controller Derivative Gain",
    "dimensionName": "CurrentControllerDerivativeGain",
    "units": [
      {
        "LongName": "volts second per amp",
        "ShortName": "V⋅s/A",
        "names": {
          "ts": "VOLTS_SECOND_PER_AMP",
          "cs": "CurrentControllerDerivativeGain_VoltsSecondPerAmp",
          "py": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_VOLTS_SECOND_PER_AMP",
          "java": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_VOLTS_SECOND_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_VOLTS_SECOND_PER_AMP",
          "swift": "currentControllerDerivativeGainVoltsSecondPerAmp"
        }
      },
      {
        "LongName": "millivolts second per amp",
        "ShortName": "mV⋅s/A",
        "names": {
          "ts": "MILLIVOLTS_SECOND_PER_AMP",
          "cs": "CurrentControllerDerivativeGain_MillivoltsSecondPerAmp",
          "py": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MILLIVOLTS_SECOND_PER_AMP",
          "java": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MILLIVOLTS_SECOND_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MILLIVOLTS_SECOND_PER_AMP",
          "swift": "currentControllerDerivativeGainMillivoltsSecondPerAmp"
        }
      },
      {
        "LongName": "microvolts second per amp",
        "ShortName": "µV⋅s/A",
        "names": {
          "ts": "MICROVOLTS_SECOND_PER_AMP",
          "cs": "CurrentControllerDerivativeGain_MicrovoltsSecondPerAmp",
          "py": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MICROVOLTS_SECOND_PER_AMP",
          "java": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MICROVOLTS_SECOND_PER_AMP",
          "cpp": "CURRENT_CONTROLLER_DERIVATIVE_GAIN_MICROVOLTS_SECOND_PER_AMP",
          "swift": "currentControllerDerivativeGainMicrovoltsSecondPerAmp"
        }
      }
    ]
  },
  {
    "DimensionName": "Resistance",
    "dimensionName": "Resistance",
    "units": [
      {
        "LongName": "kiloohms",
        "ShortName": "kΩ",
        "names": {
          "ts": "KILOOHMS",
          "cs": "Resistance_Kiloohms",
          "py": "RESISTANCE_KILOOHMS",
          "java": "RESISTANCE_KILOOHMS",
          "cpp": "RESISTANCE_KILOOHMS",
          "swift": "resistanceKiloohms"
        }
      },
      {
        "LongName": "ohms",
        "ShortName": "Ω",
        "names": {
          "ts": "OHMS",
          "cs": "Resistance_Ohms",
          "py": "RESISTANCE_OHMS",
          "java": "RESISTANCE_OHMS",
          "cpp": "RESISTANCE_OHMS",
          "swift": "resistanceOhms"
        }
      },
      {
        "LongName": "milliohms",
        "ShortName": "mΩ",
        "names": {
          "ts": "MILLIOHMS",
          "cs": "Resistance_Milliohms",
          "py": "RESISTANCE_MILLIOHMS",
          "java": "RESISTANCE_MILLIOHMS",
          "cpp": "RESISTANCE_MILLIOHMS",
          "swift": "resistanceMilliohms"
        }
      },
      {
        "LongName": "microohms",
        "ShortName": "µΩ",
        "names": {
          "ts": "MICROOHMS",
          "cs": "Resistance_Microohms",
          "py": "RESISTANCE_MICROOHMS",
          "java": "RESISTANCE_MICROOHMS",
          "cpp": "RESISTANCE_MICROOHMS",
          "swift": "resistanceMicroohms"
        }
      },
      {
        "LongName": "nanoohms",
        "ShortName": "nΩ",
        "names": {
          "ts": "NANOOHMS",
          "cs": "Resistance_Nanoohms",
          "py": "RESISTANCE_NANOOHMS",
          "java": "RESISTANCE_NANOOHMS",
          "cpp": "RESISTANCE_NANOOHMS",
          "swift": "resistanceNanoohms"
        }
      }
    ]
  },
  {
    "DimensionName": "Inductance",
    "dimensionName": "Inductance",
    "units": [
      {
        "LongName": "henries",
        "ShortName": "H",
        "names": {
          "ts": "HENRIES",
          "cs": "Inductance_Henries",
          "py": "INDUCTANCE_HENRIES",
          "java": "INDUCTANCE_HENRIES",
          "cpp": "INDUCTANCE_HENRIES",
          "swift": "inductanceHenries"
        }
      },
      {
        "LongName": "millihenries",
        "ShortName": "mH",
        "names": {
          "ts": "MILLIHENRIES",
          "cs": "Inductance_Millihenries",
          "py": "INDUCTANCE_MILLIHENRIES",
          "java": "INDUCTANCE_MILLIHENRIES",
          "cpp": "INDUCTANCE_MILLIHENRIES",
          "swift": "inductanceMillihenries"
        }
      },
      {
        "LongName": "microhenries",
        "ShortName": "µH",
        "names": {
          "ts": "MICROHENRIES",
          "cs": "Inductance_Microhenries",
          "py": "INDUCTANCE_MICROHENRIES",
          "java": "INDUCTANCE_MICROHENRIES",
          "cpp": "INDUCTANCE_MICROHENRIES",
          "swift": "inductanceMicrohenries"
        }
      },
      {
        "LongName": "nanohenries",
        "ShortName": "nH",
        "names": {
          "ts": "NANOHENRIES",
          "cs": "Inductance_Nanohenries",
          "py": "INDUCTANCE_NANOHENRIES",
          "java": "INDUCTANCE_NANOHENRIES",
          "cpp": "INDUCTANCE_NANOHENRIES",
          "swift": "inductanceNanohenries"
        }
      }
    ]
  },
  {
    "DimensionName": "Voltage Constant",
    "dimensionName": "VoltageConstant",
    "units": [
      {
        "LongName": "volt seconds per radian",
        "ShortName": "V·s/rad",
        "names": {
          "ts": "VOLT_SECONDS_PER_RADIAN",
          "cs": "VoltageConstant_VoltSecondsPerRadian",
          "py": "VOLTAGE_CONSTANT_VOLT_SECONDS_PER_RADIAN",
          "java": "VOLTAGE_CONSTANT_VOLT_SECONDS_PER_RADIAN",
          "cpp": "VOLTAGE_CONSTANT_VOLT_SECONDS_PER_RADIAN",
          "swift": "voltageConstantVoltSecondsPerRadian"
        }
      },
      {
        "LongName": "millivolt seconds per radian",
        "ShortName": "mV·s/rad",
        "names": {
          "ts": "MILLIVOLT_SECONDS_PER_RADIAN",
          "cs": "VoltageConstant_MillivoltSecondsPerRadian",
          "py": "VOLTAGE_CONSTANT_MILLIVOLT_SECONDS_PER_RADIAN",
          "java": "VOLTAGE_CONSTANT_MILLIVOLT_SECONDS_PER_RADIAN",
          "cpp": "VOLTAGE_CONSTANT_MILLIVOLT_SECONDS_PER_RADIAN",
          "swift": "voltageConstantMillivoltSecondsPerRadian"
        }
      },
      {
        "LongName": "microvolt seconds per radian",
        "ShortName": "µV·s/rad",
        "names": {
          "ts": "MICROVOLT_SECONDS_PER_RADIAN",
          "cs": "VoltageConstant_MicrovoltSecondsPerRadian",
          "py": "VOLTAGE_CONSTANT_MICROVOLT_SECONDS_PER_RADIAN",
          "java": "VOLTAGE_CONSTANT_MICROVOLT_SECONDS_PER_RADIAN",
          "cpp": "VOLTAGE_CONSTANT_MICROVOLT_SECONDS_PER_RADIAN",
          "swift": "voltageConstantMicrovoltSecondsPerRadian"
        }
      }
    ]
  },
  {
    "DimensionName": "Absolute Temperature",
    "dimensionName": "AbsoluteTemperature",
    "units": [
      {
        "LongName": "degrees Celsius",
        "ShortName": "°C",
        "names": {
          "ts": "DEGREES_CELSIUS",
          "cs": "AbsoluteTemperature_DegreesCelsius",
          "py": "ABSOLUTE_TEMPERATURE_DEGREES_CELSIUS",
          "java": "ABSOLUTE_TEMPERATURE_DEGREES_CELSIUS",
          "cpp": "ABSOLUTE_TEMPERATURE_DEGREES_CELSIUS",
          "swift": "absoluteTemperatureDegreesCelsius"
        }
      },
      {
        "LongName": "kelvins",
        "ShortName": "K",
        "names": {
          "ts": "KELVINS",
          "cs": "AbsoluteTemperature_Kelvins",
          "py": "ABSOLUTE_TEMPERATURE_KELVINS",
          "java": "ABSOLUTE_TEMPERATURE_KELVINS",
          "cpp": "ABSOLUTE_TEMPERATURE_KELVINS",
          "swift": "absoluteTemperatureKelvins"
        }
      },
      {
        "LongName": "degrees Fahrenheit",
        "ShortName": "°F",
        "names": {
          "ts": "DEGREES_FAHRENHEIT",
          "cs": "AbsoluteTemperature_DegreesFahrenheit",
          "py": "ABSOLUTE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "java": "ABSOLUTE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "cpp": "ABSOLUTE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "swift": "absoluteTemperatureDegreesFahrenheit"
        }
      },
      {
        "LongName": "degrees Rankine",
        "ShortName": "°R",
        "names": {
          "ts": "DEGREES_RANKINE",
          "cs": "AbsoluteTemperature_DegreesRankine",
          "py": "ABSOLUTE_TEMPERATURE_DEGREES_RANKINE",
          "java": "ABSOLUTE_TEMPERATURE_DEGREES_RANKINE",
          "cpp": "ABSOLUTE_TEMPERATURE_DEGREES_RANKINE",
          "swift": "absoluteTemperatureDegreesRankine"
        }
      }
    ]
  },
  {
    "DimensionName": "Relative Temperature",
    "dimensionName": "RelativeTemperature",
    "units": [
      {
        "LongName": "degrees Celsius",
        "ShortName": "°C",
        "names": {
          "ts": "DEGREES_CELSIUS",
          "cs": "RelativeTemperature_DegreesCelsius",
          "py": "RELATIVE_TEMPERATURE_DEGREES_CELSIUS",
          "java": "RELATIVE_TEMPERATURE_DEGREES_CELSIUS",
          "cpp": "RELATIVE_TEMPERATURE_DEGREES_CELSIUS",
          "swift": "relativeTemperatureDegreesCelsius"
        }
      },
      {
        "LongName": "kelvins",
        "ShortName": "K",
        "names": {
          "ts": "KELVINS",
          "cs": "RelativeTemperature_Kelvins",
          "py": "RELATIVE_TEMPERATURE_KELVINS",
          "java": "RELATIVE_TEMPERATURE_KELVINS",
          "cpp": "RELATIVE_TEMPERATURE_KELVINS",
          "swift": "relativeTemperatureKelvins"
        }
      },
      {
        "LongName": "degrees Fahrenheit",
        "ShortName": "°F",
        "names": {
          "ts": "DEGREES_FAHRENHEIT",
          "cs": "RelativeTemperature_DegreesFahrenheit",
          "py": "RELATIVE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "java": "RELATIVE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "cpp": "RELATIVE_TEMPERATURE_DEGREES_FAHRENHEIT",
          "swift": "relativeTemperatureDegreesFahrenheit"
        }
      },
      {
        "LongName": "degrees Rankine",
        "ShortName": "°R",
        "names": {
          "ts": "DEGREES_RANKINE",
          "cs": "RelativeTemperature_DegreesRankine",
          "py": "RELATIVE_TEMPERATURE_DEGREES_RANKINE",
          "java": "RELATIVE_TEMPERATURE_DEGREES_RANKINE",
          "cpp": "RELATIVE_TEMPERATURE_DEGREES_RANKINE",
          "swift": "relativeTemperatureDegreesRankine"
        }
      }
    ]
  },
  {
    "DimensionName": "Frequency",
    "dimensionName": "Frequency",
    "units": [
      {
        "LongName": "gigahertz",
        "ShortName": "GHz",
        "names": {
          "ts": "GIGAHERTZ",
          "cs": "Frequency_Gigahertz",
          "py": "FREQUENCY_GIGAHERTZ",
          "java": "FREQUENCY_GIGAHERTZ",
          "cpp": "FREQUENCY_GIGAHERTZ",
          "swift": "frequencyGigahertz"
        }
      },
      {
        "LongName": "megahertz",
        "ShortName": "MHz",
        "names": {
          "ts": "MEGAHERTZ",
          "cs": "Frequency_Megahertz",
          "py": "FREQUENCY_MEGAHERTZ",
          "java": "FREQUENCY_MEGAHERTZ",
          "cpp": "FREQUENCY_MEGAHERTZ",
          "swift": "frequencyMegahertz"
        }
      },
      {
        "LongName": "kilohertz",
        "ShortName": "kHz",
        "names": {
          "ts": "KILOHERTZ",
          "cs": "Frequency_Kilohertz",
          "py": "FREQUENCY_KILOHERTZ",
          "java": "FREQUENCY_KILOHERTZ",
          "cpp": "FREQUENCY_KILOHERTZ",
          "swift": "frequencyKilohertz"
        }
      },
      {
        "LongName": "hertz",
        "ShortName": "Hz",
        "names": {
          "ts": "HERTZ",
          "cs": "Frequency_Hertz",
          "py": "FREQUENCY_HERTZ",
          "java": "FREQUENCY_HERTZ",
          "cpp": "FREQUENCY_HERTZ",
          "swift": "frequencyHertz"
        }
      },
      {
        "LongName": "millihertz",
        "ShortName": "mHz",
        "names": {
          "ts": "MILLIHERTZ",
          "cs": "Frequency_Millihertz",
          "py": "FREQUENCY_MILLIHERTZ",
          "java": "FREQUENCY_MILLIHERTZ",
          "cpp": "FREQUENCY_MILLIHERTZ",
          "swift": "frequencyMillihertz"
        }
      },
      {
        "LongName": "microhertz",
        "ShortName": "µHz",
        "names": {
          "ts": "MICROHERTZ",
          "cs": "Frequency_Microhertz",
          "py": "FREQUENCY_MICROHERTZ",
          "java": "FREQUENCY_MICROHERTZ",
          "cpp": "FREQUENCY_MICROHERTZ",
          "swift": "frequencyMicrohertz"
        }
      },
      {
        "LongName": "nanohertz",
        "ShortName": "nHz",
        "names": {
          "ts": "NANOHERTZ",
          "cs": "Frequency_Nanohertz",
          "py": "FREQUENCY_NANOHERTZ",
          "java": "FREQUENCY_NANOHERTZ",
          "cpp": "FREQUENCY_NANOHERTZ",
          "swift": "frequencyNanohertz"
        }
      }
    ]
  }
];
