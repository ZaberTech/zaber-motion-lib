import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { defaultBool, defaultOptional } from './helpers';

function genArgs(axis: boolean): MethodArg[] {
  const device = axis ? '_axis.device' : '_device';
  const deviceCs = axis ? '_axis.Device' : '_device';
  const getDeviceJava = axis ? 'axis.getDevice()' : 'device';
  const getDeviceCpp = axis ? '_axis.getDevice()' : '_device';
  const getDeviceSwift = axis ? 'axis.device' : 'device';
  const axisArg = axis ? [{
    name: 'Axis',
    value: {
      cs: '_axis.AxisNumber',
      py: 'self._axis.axis_number',
      ts: 'this._axis.axisNumber',
      java: 'this.axis.getAxisNumber()',
      cpp: 'this->_axis.getAxisNumber()',
      swift: 'axis.axisNumber',
    }
  }] : [];

  return [
    {
      name: 'InterfaceId',
      value: {
        cs: `${deviceCs}.Connection.InterfaceId`,
        py: `self.${device}.connection.interface_id`,
        ts: `this.${device}.connection.interfaceId`,
        java: `this.${getDeviceJava}.getConnection().getInterfaceId()`,
        cpp: `this->${getDeviceCpp}.getConnection().getInterfaceId()`,
        swift: `${getDeviceSwift}.connection.interfaceId`,
      }
    },
    {
      name: 'Device',
      value: {
        cs: `${deviceCs}.DeviceAddress`,
        py: `self.${device}.device_address`,
        ts: `this.${device}.deviceAddress`,
        java: `this.${getDeviceJava}.getDeviceAddress()`,
        cpp: `this->${getDeviceCpp}.getDeviceAddress()`,
        swift: `${getDeviceSwift}.deviceAddress`,
      }
    },
    ...axisArg,
  ];
}

function generateCommonStorageMethods(deviceOrAxis: 'axis' | 'device', genCommonArgs: () => MethodArg[]): MethodDefinition[] {
  return [
    {
      name: 'SetString',
      description: `Sets the ${deviceOrAxis} value stored at the provided key.`,
      route: 'device/set_storage',
      request: 'DeviceSetStorageRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to set the value at.',
        },
        {
          name: 'value', type: 'string',
          description: 'Value to set.',
        },
      ],
      options: [
        {
          name: 'encode', type: 'bool',
          defaultValue: defaultBool(false),
          description: [
            'Whether the stored value should be base64 encoded before being stored.',
            'This makes the string unreadable to humans using the ASCII protocol,',
            'however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.',
          ],
        },
      ]
    },
    {
      name: 'GetString',
      description: `Gets the ${deviceOrAxis} value stored with the provided key.`,
      route: 'device/get_storage',
      request: 'DeviceGetStorageRequest',
      response: 'StringResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to read the value of.',
        },
      ],
      options: [
        {
          name: 'decode', type: 'bool',
          defaultValue: defaultBool(false),
          description: [
            'Whether the stored value should be decoded.',
            'Only use this when reading values set by storage.set with "encode" true.',
          ],
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'Stored value.',
    },
    {
      name: 'SetNumber',
      description: 'Sets the value at the provided key to the provided number.',
      route: 'device/set_storage_number',
      request: 'DeviceSetStorageNumberRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to set the value at.',
        },
        {
          name: 'value', type: 'double',
          description: 'Value to set.',
        },
      ],
    },
    {
      name: 'GetNumber',
      description: 'Gets the value at the provided key interpreted as a number.',
      route: 'device/get_storage_number',
      request: 'DeviceStorageRequest',
      response: 'DoubleResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to get the value at.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Stored value.',
    },
    {
      name: 'SetBool',
      description: 'Sets the value at the provided key to the provided boolean.',
      route: 'device/set_storage_bool',
      request: 'DeviceSetStorageBoolRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to set the value at.',
        },
        {
          name: 'value', type: 'bool',
          description: 'Value to set.',
        },
      ],
    },
    {
      name: 'GetBool',
      description: 'Gets the value at the provided key interpreted as a boolean.',
      route: 'device/get_storage_bool',
      request: 'DeviceStorageRequest',
      response: 'BoolResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to get the value at.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'Stored value.',
    },
    {
      name: 'EraseKey',
      description: `Erases the ${deviceOrAxis} value stored at the provided key.`,
      route: 'device/erase_storage',
      request: 'DeviceStorageRequest',
      response: 'BoolResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key to erase.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'A boolean indicating if the key existed.',
    },
    {
      name: 'listKeys',
      description: [
        `Lists the ${deviceOrAxis} storage keys matching a given prefix.`,
        'Omit the prefix to list all the keys.'
      ],
      route: 'device/storage_list_keys',
      request: 'DeviceStorageListKeysRequest',
      response: 'StringArrayResponse',
      args: genCommonArgs(),
      params: [],
      options: [
        {
          name: 'prefix', type: { type: 'optional', of: 'string' },
          defaultValue: defaultOptional(),
          description: 'Optional key prefix.',
        },
      ],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'string' },
      returnDescription: 'Storage keys matching the given prefix.',
    },
    {
      name: 'keyExists',
      description: `Determines whether a given key exists in ${deviceOrAxis} storage.`,
      route: 'device/storage_key_exists',
      request: 'DeviceStorageRequest',
      response: 'BoolResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'key', type: 'string',
          description: 'Key which existence to determine.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True indicating that the key exists, false otherwise.',
    },
  ];
}

export const axisStorage: ClassDefinition = {
  name: 'AxisStorage',
  namespace: 'ascii',
  properties: [
    {
      name: 'Axis',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      private: true,
    },
  ],
  description: 'Class providing access to axis storage.',
  minFw: [{ major: 7, minor: 30 }],
  ctor: { },
  functions: generateCommonStorageMethods('axis', () => genArgs(true)),
  py: {
    forwardTypes: ['Axis']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Axis',
      member: 'Storage',
    },
  }],
};

export const deviceStorage: ClassDefinition = {
  name: 'DeviceStorage',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
  ],
  description: 'Class providing access to device storage.',
  minFw: [{ major: 7, minor: 30 }],
  ctor: { },
  functions: generateCommonStorageMethods('device', () => genArgs(false)),
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Storage',
    },
  }],
};


export function generateLabelMethods(deviceOrAxis: 'peripheral' | 'device', genCommonArgs: () => MethodArg[]): MethodDefinition[] {
  return [
    {
      name: 'SetLabel',
      description: [
        `Sets the user-assigned ${deviceOrAxis} label.`,
        'The label is stored on the controller and recognized by other software.',
      ],
      route: 'device/set_label',
      request: 'DeviceSetStorageRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'label', type: 'string', requestName: 'value',
          description: 'Label to set.',
        },
      ],
    }, {
      name: 'RetrieveLabel',
      private: true,
      sync: true,
      description: `Gets the ${deviceOrAxis} name.`,
      route: 'device/get_label',
      request: 'AxisEmptyRequest',
      response: 'StringResponse',
      args: genCommonArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'The label.',
    },
  ];
}
