import { ClassDefinition } from '../types';

const definition: ClassDefinition = {
  name: 'Tools',
  namespace: [],
  properties: [],
  description: 'Class providing various utility functions.',
  ctor: false,
  functions: [
    {
      name: 'ListSerialPorts',
      description: 'Lists all serial ports on the computer.',
      route: 'tools/list_serial_ports',
      request: 'EmptyRequest',
      response: 'ToolsListSerialPortsResponse',
      args: [],
      params: [],
      returnPropertyName: 'Ports',
      returnType: { type: 'array', of: 'string' },
      returnDescription: 'Array of serial port names.',
      static: true,
    },
    {
      name: 'GetMessageRouterPipePath',
      description: [
        'Returns path of message router named pipe on Windows',
        'or file path of unix domain socket on UNIX.',
      ],
      route: 'tools/get_message_router_pipe',
      request: 'EmptyRequest',
      response: 'StringResponse',
      args: [],
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'Path of message router\'s named pipe or unix domain socket.',
      static: true,
      sync: true,
    },
    {
      name: 'GetDbServicePipePath',
      description: [
        'Returns the path for communicating with a local device database service.',
        'This will be a named pipe on Windows and the file path of a unix domain socket on UNIX.',
      ],
      route: 'tools/get_db_service_pipe',
      request: 'EmptyRequest',
      response: 'StringResponse',
      args: [],
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'Path of database service\'s named pipe or unix domain socket.',
      static: true,
      hidden: true,
      sync: true,
    },
  ]
};

export default definition;
