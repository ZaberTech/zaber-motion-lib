import _ from 'lodash';

import { ExceptionBasicDefinition, ExceptionNameDefinition } from '../types';
import { errorsEnum } from '../../dto/definitions/requests/errors_enum';

const descriptions: Record<string, ExceptionBasicDefinition> = {
  BAD_COMMAND: {
    description: 'Thrown when a device receives an invalid command.',
    baseException: 'CommandFailedException',
  },
  BAD_DATA: {
    description: 'Thrown when a parameter of a command is judged to be out of range by the receiving device.',
    baseException: 'CommandFailedException',
  },
  BINARY_COMMAND_FAILED: {
    description: 'Thrown when a device rejects a binary command with an error.',
    customDataPojo: 'BinaryCommandFailedExceptionData',
  },
  COMMAND_FAILED: {
    description: 'Thrown when a device rejects a command.',
    customDataPojo: 'CommandFailedExceptionData',
    isInheritable: true,
  },
  COMMAND_PREEMPTED: { description: 'Thrown when a movement command gets preempted by another command.' },
  COMMAND_TOO_LONG: {
    description: 'Thrown when a command is too long to be written by the ASCII protocol, even when continued across multiple lines.',
    customDataPojo: 'CommandTooLongExceptionData',
  },
  CONNECTION_CLOSED: { description: 'Thrown when attempting to communicate on a closed connection.' },
  CONNECTION_FAILED: {
    description: 'Thrown when a connection breaks during a request.',
    isInheritable: true,
  },
  CONVERSION_FAILED: { description: 'Thrown when a value cannot be converted using the provided units.' },
  DEVICE_ADDRESS_CONFLICT: {
    description: 'Thrown when there is a conflict in device numbers preventing unique addressing.',
    customDataPojo: 'DeviceAddressConflictExceptionData',
  },
  DEVICE_BUSY: { description: 'Thrown when a requested operation fails because the device is currently busy.' },
  DEVICE_DB_FAILED: {
    description: 'Thrown when device information cannot be retrieved from the device database.',
    customDataPojo: 'DeviceDbFailedExceptionData',
  },
  DEVICE_DETECTION_FAILED: { description: 'Thrown when device detection fails.' },
  DEVICE_FAILED: { description: ['Thrown when a device registers fatal failure.', 'Contact support if you observe this exception.'] },
  DEVICE_NOT_IDENTIFIED: { description: 'Thrown when attempting an operation that requires an identified device.' },
  DRIVER_DISABLED: {
    description: 'Thrown when a device cannot carry out a movement command because the motor driver is disabled.',
    baseException: 'CommandFailedException',
  },
  G_CODE_EXECUTION: { description: 'Thrown when a block of G-Code cannot be executed.', customDataPojo: 'GCodeExecutionExceptionData' },
  G_CODE_SYNTAX: { description: 'Thrown when a block of G-Code cannot be parsed.', customDataPojo: 'GCodeSyntaxExceptionData' },
  INCOMPATIBLE_SHARED_LIBRARY: {
    description: [
      'Thrown when the loaded shared library is incompatible with the running code.',
      'Typically caused by mixed library binary files. Reinstall the library.',
    ],
  },
  INTERNAL_ERROR: { description: 'Used for internal error handling. Please report an issue if observed.' },
  INVALID_ARGUMENT: { description: 'Thrown when a function is called with invalid values.' },
  INVALID_DATA: { description: 'Thrown when incoming device data cannot be parsed as expected.' },
  INVALID_OPERATION: { description: 'Thrown when operation cannot be performed at given time or context.' },
  INVALID_PACKET: {
    description: 'Thrown when a packet from a device cannot be parsed.',
    customDataPojo: 'InvalidPacketExceptionData',
  },
  INVALID_PARK_STATE: { description: 'Thrown when a device is unable to park.' },
  INVALID_REQUEST_DATA: {
    description: [
      'Used for internal error handling.',
      'Indicates passing values of incorrect type from scripting languages or mixed library binary files.',
    ],
  },
  INVALID_RESPONSE: {
    description: 'Thrown when a device sends a response with unexpected type or data.',
    customDataPojo: 'InvalidResponseExceptionData',
  },
  IO_CHANNEL_OUT_OF_RANGE: {
    description: 'Thrown when a device IO operation cannot be performed because the provided channel is not valid.'
  },
  IO_FAILED: { description: 'Thrown when the library cannot perform an operation on a file.' },
  LOCKSTEP_ENABLED: { description: 'Thrown when an operation cannot be performed because lockstep motion is enabled.' },
  LOCKSTEP_NOT_ENABLED: { description: 'Thrown when an operation cannot be performed because lockstep motion is not enabled.' },
  MOVEMENT_FAILED: {
    description: 'Thrown when a device registers a fault during movement.',
    customDataPojo: 'MovementFailedExceptionData',
  },
  MOVEMENT_INTERRUPTED: {
    description: 'Thrown when ongoing movement is interrupted by another command or user input.',
    customDataPojo: 'MovementInterruptedExceptionData',
  },
  NO_DEVICE_FOUND: { description: 'Thrown when no devices can be found on a connection.' },
  NO_VALUE_FOR_KEY: { description: 'Thrown when trying to access a key that has not been set.' },
  NOT_SUPPORTED: { description: 'Thrown when a device does not support a requested command or setting.' },
  OPERATION_FAILED: {
    description: 'Thrown when a non-motion device fails to perform a requested operation.',
    customDataPojo: 'OperationFailedExceptionData',
  },
  OS_FAILED: { description: 'Thrown when an operation fails due to underlying operating system error.' },
  OUT_OF_REQUEST_IDS: { description: 'Thrown when the library is overwhelmed with too many simultaneous requests.' },
  PVT_DISCONTINUITY: { description: 'Thrown when a PVT sequence encounters discontinuity and interrupts the sequence.' },
  PVT_EXECUTION: {
    description: 'Thrown when a PVT sequence motion fails.',
    customDataPojo: 'PvtExecutionExceptionData',
  },
  PVT_MODE: { description: 'Thrown when an operation is not supported by a mode the PVT sequence is currently set up in.' },
  PVT_MOVEMENT_FAILED: {
    description: 'Thrown when a device registers a fault during PVT movement.',
    customDataPojo: 'PvtMovementFailedExceptionData',
  },
  PVT_MOVEMENT_INTERRUPTED: {
    description: 'Thrown when ongoing PVT movement is interrupted by another command or user input.',
    customDataPojo: 'PvtMovementInterruptedExceptionData',
  },
  PVT_SETUP_FAILED: { description: 'Thrown when setting up a PVT sequence fails.' },
  REMOTE_MODE: {
    description: 'Thrown when a command is rejected because the device is in EtherCAT Control (remote) mode.',
    baseException: 'CommandFailedException',
  },
  REQUEST_TIMEOUT: { description: 'Thrown when a device does not respond to a request in time.' },
  SERIAL_PORT_BUSY: {
    description: 'Thrown when a serial port cannot be opened because it is in use by another application.',
    baseException: 'ConnectionFailedException',
  },
  SET_DEVICE_STATE_FAILED: {
    description: 'Thrown when a device cannot be set to the supplied state.',
    customDataPojo: 'SetDeviceStateExceptionData',
  },
  SET_PERIPHERAL_STATE_FAILED: {
    description: 'Thrown when an axis cannot be set to the supplied state.',
    customDataPojo: 'SetPeripheralStateExceptionData',
  },
  SETTING_NOT_FOUND: { description: 'Thrown when a get or a set command cannot be found for a setting.' },
  STREAM_DISCONTINUITY: { description: 'Thrown when a stream encounters discontinuity and interrupts the movement.' },
  STREAM_EXECUTION: {
    description: 'Thrown when a streamed motion fails.',
    customDataPojo: 'StreamExecutionExceptionData',
  },
  STREAM_MODE: { description: 'Thrown when an operation is not supported by a mode the stream is currently set up in.' },
  STREAM_MOVEMENT_FAILED: {
    description: 'Thrown when a device registers a fault during streamed movement.',
    customDataPojo: 'StreamMovementFailedExceptionData',
  },
  STREAM_MOVEMENT_INTERRUPTED: {
    description: 'Thrown when ongoing stream movement is interrupted by another command or user input.',
    customDataPojo: 'StreamMovementInterruptedExceptionData',
  },
  STREAM_SETUP_FAILED: { description: 'Thrown when setting up a stream fails.' },
  TIMEOUT: { description: 'Thrown for various timeouts across the library excluding request to a device (see RequestTimeoutException).' },
  TRANSPORT_ALREADY_USED: { description: 'Thrown when a transport has already been used to open another connection.' },
  UNKNOWN_REQUEST: { description: 'Used for internal error handling. Indicates mixed library binary files. Reinstall the library.' },
};

const convertExceptionName = (name: string) => _.upperFirst(_.camelCase(name));

function genExceptionNames(): ExceptionNameDefinition[] {
  const dtoErrors = errorsEnum.values.map(({ name }) => _.snakeCase(name).toUpperCase());

  const rawExceptions = dtoErrors.map(name => {
    const pbNames = {
      ts: name,
      py: name,
      java: name,
      cs: convertExceptionName(name),
      cpp: name,
      swift: _.lowerFirst(_.camelCase(name)),
    };

    const additionalData = descriptions[name];
    if (!additionalData) {
      throw new Error(`No additional data defined for exception: ${pbNames.ts}`);
    }

    const exception: ExceptionNameDefinition = {
      baseException: 'MotionLibException',
      ...additionalData,
      pbNames,
      name: `${convertExceptionName(name)}Exception`,
    };

    return exception;
  });

  return _.sortBy(rawExceptions, 'name');
}

export const exceptions = genExceptionNames();
