import _ from 'lodash';

import { definitions as dto } from '../../dto/definitions';
import { ClassDefinition, MethodArg, MethodDefinition, OptionalMethodParam } from '../types';
import { namespaceEquals } from '../type_system';

import { defaultBool, defaultUnits, unitsValue } from './helpers';
import { genericCommand, genericCommandMultiResponse, genericCommandNoResponse } from './generic_commands';
import { getState, setState, canSetState } from './get_set_state';
import { generateLabelMethods } from './storage';
import { toStringAxis } from './to_string';

export const ENABLE_DRIVER_TIMEOUT = 10;

function genAxisArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
    {
      name: 'Axis',
      value: 'AxisNumber',
    },
  ];
}

const waitUntilIdle: OptionalMethodParam = {
  name: 'waitUntilIdle', type: 'bool',
  defaultValue: defaultBool(true),
  description: 'Determines whether function should return after the movement is finished or just started.',
};

export function additionalMoveArgs(velocity: boolean = true): OptionalMethodParam[] {
  const args: OptionalMethodParam[] = [];
  if (velocity) {
    args.push({
      name: 'velocity', type: 'double',
      description: [
        'Movement velocity.',
        'Default value of 0 indicates that the maxspeed setting is used instead.',
      ],
      minFw: [{ major: 7, minor: 25 }],
      defaultValue: '0'
    },
    {
      name: 'velocityUnit', type: 'VelocityUnits',
      defaultValue: defaultUnits,
      description: 'Units of velocity.',
    });
  }
  args.push({
    name: 'acceleration', type: 'double',
    description: [
      'Movement acceleration.',
      'Default value of 0 indicates that the accel setting is used instead.',
    ],
    minFw: [{ major: 7, minor: 25 }],
    defaultValue: '0'
  },
  {
    name: 'accelerationUnit', type: 'AccelerationUnits',
    defaultValue: defaultUnits,
    description: 'Units of acceleration.',
  });
  return args;
}

const IGNORED_IDENTITY_PROPS = ['IsModified'];
const axisIdentity = dto.classes.find(c => c.name === 'AxisIdentity' && namespaceEquals(c.namespace, 'ascii'))!;

export const moveSinFunctions: Record<string, MethodDefinition> = {
  moveSin: {
    name: 'MoveSin',
    description: 'Moves the axis in a sinusoidal trajectory.',
    route: 'device/move_sin',
    request: 'DeviceMoveSinRequest',
    args: genAxisArgs(),
    params: [
      {
        name: 'Amplitude',
        type: 'double',
        description: 'Amplitude of the sinusoidal motion (half of the motion\'s peak-to-peak range).'
      },
      {
        name: 'AmplitudeUnits',
        type: 'LengthUnits',
        value: unitsValue,
        description: 'Units of position.',
      },
      {
        name: 'Period',
        type: 'double',
        description: 'Period of the sinusoidal motion in milliseconds.'
      },
      {
        name: 'PeriodUnits',
        type: 'TimeUnits',
        value: unitsValue,
        description: 'Units of time.',
      },
    ],
    options: [
      {
        name: 'Count',
        type: 'double',
        defaultValue: '0',
        description: [
          'Number of sinusoidal cycles to complete.',
          'Must be a multiple of 0.5',
          'If count is not specified or set to 0, the axis will move indefinitely.'
        ]
      },
      {
        name: 'waitUntilIdle',
        type: 'bool',
        defaultValue: {
          all: 'true',
          py: 'True'
        },
        description: 'Determines whether function should return after the movement is finished or just started.'
      },
    ],
  },
  moveSinStop: {
    name: 'MoveSinStop',
    description: [
      'Stops the axis at the end of the sinusoidal trajectory.',
      'If the sinusoidal motion was started with an integer-plus-half cycle count,',
      'the motion ends at the half-way point of the sinusoidal trajectory.'
    ],
    route: 'device/move_sin_stop',
    request: 'DeviceStopRequest',
    args: genAxisArgs(),
    params: [],
    options: [
      {
        name: 'waitUntilIdle',
        type: 'bool',
        defaultValue: {
          all: 'true',
          py: 'True'
        },
        description: 'Determines whether function should return after the movement is finished.'
      },
    ],
  },
};

const definition: ClassDefinition = {
  name: 'Axis',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that controls this axis.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: {
        emptyCtorValue: '',
      },
    },
    {
      name: 'AxisNumber',
      description: [
        'The axis number identifies the axis on the device.',
        'The first axis has the number one.'
      ],
      type: 'int',
      cpp: {
        emptyCtorValue: '-1',
      },
    },
    {
      name: 'Settings',
      description: 'Settings and properties of this axis.',
      type: { type: 'ref', name: 'AxisSettings', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new AxisSettings(this)',
        py: 'AxisSettings(self)',
        ts: 'new AxisSettings(this)',
        java: 'new AxisSettings(this)',
        swift: 'AxisSettings(axis: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Storage',
      description: 'Key-value storage of this axis.',
      minFw: [{ major: 7, minor: 30 }],
      type: { type: 'ref', name: 'AxisStorage', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new AxisStorage(this)',
        py: 'AxisStorage(self)',
        ts: 'new AxisStorage(this)',
        java: 'new AxisStorage(this)',
        swift: 'AxisStorage(axis: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Warnings',
      description: 'Warnings and faults of this axis.',
      type: { type: 'ref', name: 'Warnings', namespace: 'ascii' },
      value: {
        cs: 'new Warnings(device, axisNumber)',
        py: 'Warnings(device, axis_number)',
        ts: 'new Warnings(device, axisNumber)',
        java: 'new Warnings(device, axisNumber)',
        swift: 'Warnings(device: device, axisNumber: axisNumber)',
      },
      getter: {
        cpp: '{this->_device, this->_axisNumber}'
      },
    },
    {
      name: 'Identity',
      description: 'Identity of the axis.',
      type: { type: 'ref', name: 'AxisIdentity', namespace: 'ascii' },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveIdentity',
    },
    ...axisIdentity.properties
      .filter(property => !IGNORED_IDENTITY_PROPS.includes(property.name))
      .map(property => ({
        name: property.name,
        description: property.description!,
        type: property.type,
        getterCanThrow: true,
        getter: {
          cs: `Identity.${property.name}`,
          py: `self.identity.${_.snakeCase(property.name)}`,
          ts: `this.identity.${_.lowerFirst(property.name)}`,
          java: `this.getIdentity().get${property.name}()`,
          cpp: `this->getIdentity().get${property.name}()`,
          swift: `identity.${_.lowerFirst(property.name)}`,
        }
      })),
    {
      name: 'Label',
      description: 'User-assigned label of the peripheral.',
      type: 'string',
      getterCanThrow: true,
      getterFunctionName: 'RetrieveLabel',
    },
  ],
  description: 'Represents an axis of motion associated with a device.',
  ctor: { },
  functions: [
    {
      name: 'Home',
      description: 'Homes axis. Axis returns to its homing position.',
      route: 'device/home',
      request: 'DeviceHomeRequest',
      args: genAxisArgs(),
      params: [],
      options: [waitUntilIdle],
    },
    {
      name: 'Stop',
      description: 'Stops ongoing axis movement. Decelerates until zero speed.',
      route: 'device/stop',
      request: 'DeviceStopRequest',
      args: genAxisArgs(),
      params: [],
      options: [waitUntilIdle],
    },
    {
      name: 'Park',
      description: [
        'Parks the axis in anticipation of turning the power off.',
        'It can later be powered on, unparked, and moved without first having to home it.'
      ],
      route: 'device/park',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
    },
    {
      name: 'Unpark',
      description: 'Unparks axis. Axis will now be able to move.',
      route: 'device/unpark',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
    },
    {
      name: 'IsParked',
      description: 'Returns bool indicating whether the axis is parked or not.',
      route: 'device/is_parked',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the axis is currently parked. False otherwise.',
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until axis stops moving.',
      route: 'device/wait_until_idle',
      request: 'DeviceWaitUntilIdleRequest',
      args: genAxisArgs(),
      params: [],
      options: [
        {
          name: 'throwErrorOnFault',
          type: 'bool',
          defaultValue: defaultBool(true),
          description: 'Determines whether to throw error when fault is observed.'
        }
      ],
    },
    {
      name: 'IsBusy',
      description: 'Returns bool indicating whether the axis is executing a motion command.',
      route: 'device/is_busy',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the axis is currently executing a motion command.',
    },
    {
      name: 'IsHomed',
      description: 'Returns bool indicating whether the axis has position reference and was homed.',
      route: 'device/is_homed',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the axis has position reference and was homed.',
    },
    {
      name: 'MoveAbsolute',
      description: 'Move axis to absolute position.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: genAxisArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Abs',
            py: 'dto.AxisMoveType.ABS',
            ts: 'requests.AxisMoveType.ABS',
            java: 'AxisMoveType.ABS',
            cpp: '::zaber::motion::requests::AxisMoveType::ABS',
            swift: 'DtoRequests.AxisMoveType.abs',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Absolute position.' },
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      options: [
        waitUntilIdle,
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveMax',
      description: 'Moves the axis to the maximum position as specified by limit.max.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: [
        ...genAxisArgs(),
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Max',
            py: 'dto.AxisMoveType.MAX',
            ts: 'requests.AxisMoveType.MAX',
            java: 'AxisMoveType.MAX',
            cpp: '::zaber::motion::requests::AxisMoveType::MAX',
            swift: 'DtoRequests.AxisMoveType.max',
          }
        },
      ],
      params: [],
      options: [
        waitUntilIdle,
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveMin',
      description: 'Moves the axis to the minimum position as specified by limit.min.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: [
        ...genAxisArgs(),
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Min',
            py: 'dto.AxisMoveType.MIN',
            ts: 'requests.AxisMoveType.MIN',
            java: 'AxisMoveType.MIN',
            cpp: '::zaber::motion::requests::AxisMoveType::MIN',
            swift: 'DtoRequests.AxisMoveType.min',
          }
        },
      ],
      params: [],
      options: [
        waitUntilIdle,
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveRelative',
      description: 'Move axis to position relative to current position.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: genAxisArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Rel',
            py: 'dto.AxisMoveType.REL',
            ts: 'requests.AxisMoveType.REL',
            java: 'AxisMoveType.REL',
            cpp: '::zaber::motion::requests::AxisMoveType::REL',
            swift: 'DtoRequests.AxisMoveType.rel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Relative position.' },
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      options: [
        waitUntilIdle,
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveVelocity',
      description: 'Begins to move axis at specified speed.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: genAxisArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Vel',
            py: 'dto.AxisMoveType.VEL',
            ts: 'requests.AxisMoveType.VEL',
            java: 'AxisMoveType.VEL',
            cpp: '::zaber::motion::requests::AxisMoveType::VEL',
            swift: 'DtoRequests.AxisMoveType.vel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'velocity', type: 'double', description: 'Movement velocity.' },
        {
          name: 'unit', type: 'VelocityUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of velocity.',
        },
      ],
      options: additionalMoveArgs(false),
    },
    {
      name: 'GetPosition',
      description: 'Returns current axis position.',
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: [
        ...genAxisArgs(),
        {
          name: 'Setting',
          value: {
            py: '"pos"',
            cs: '"pos"',
            ts: '\'pos\'',
            java: '"pos"',
            cpp: '"pos"',
            swift: '"pos"',
          }
        },
      ],
      params: [
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Axis position.',
    },
    {
      name: 'GetNumberOfIndexPositions',
      description: 'Gets number of index positions of the axis.',
      route: 'device/get_index_count',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of index positions.',
    },
    {
      name: 'GetIndexPosition',
      description: 'Returns current axis index position.',
      route: 'device/get_index_position',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Index position starting from 1 or 0 if the position is not an index position.',
    },
    {
      name: 'MoveIndex',
      description: 'Moves the axis to index position.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: genAxisArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Index',
            py: 'dto.AxisMoveType.INDEX',
            ts: 'requests.AxisMoveType.INDEX',
            java: 'AxisMoveType.INDEX',
            cpp: '::zaber::motion::requests::AxisMoveType::INDEX',
            swift: 'DtoRequests.AxisMoveType.index',
          }
        },
      ]),
      params: [{
        name: 'index', type: 'int', requestName: 'ArgInt',
        description: ['Index position. Index positions are numbered from 1.'],
      }],
      options: [
        waitUntilIdle,
        ...additionalMoveArgs(),
      ],
    },
    genericCommand({
      interfaceId: 'Device.Connection.InterfaceId', device: 'Device.DeviceAddress', axis: 'AxisNumber', description: [
        'Sends a generic ASCII command to this axis.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandMultiResponse({
      interfaceId: 'Device.Connection.InterfaceId', device: 'Device.DeviceAddress', axis: 'AxisNumber', description: [
        'Sends a generic ASCII command to this axis and expect multiple responses.',
        'Responses are returned in order of arrival.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandNoResponse({
      interfaceId: 'Device.Connection.InterfaceId', device: 'Device.DeviceAddress', axis: 'AxisNumber', description: [
        'Sends a generic ASCII command to this axis without expecting a response and without adding a message ID',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    {
      name: 'PrepareCommand',
      description: [
        'Formats parameters into a command and performs unit conversions.',
        'Parameters in the command template are denoted by a question mark.',
        'Command returned is only valid for this axis and this device.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
      route: 'device/prepare_command',
      request: 'PrepareCommandRequest',
      response: 'StringResponse',
      returnType: 'string',
      returnPropertyName: 'Value',
      returnDescription: 'Command with converted parameters.',
      sync: true,
      args: genAxisArgs(),
      params: [
        {
          name: 'commandTemplate', type: 'string',
          description: 'Template of a command to prepare. Parameters are denoted by question marks.'
        },
      ],
      variadic: {
        name: 'parameters',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: 'Variable number of command parameters.',
      },
    },
    ...generateLabelMethods('peripheral', genAxisArgs),
    toStringAxis(genAxisArgs()),
    getState({ name: 'axis', args: genAxisArgs() }),
    setState({ name: 'axis', args: genAxisArgs() }),
    canSetState({ name: 'axis', args: genAxisArgs() }),
    {
      name: 'RetrieveIdentity',
      description: 'Returns identity.',
      route: 'device/get_axis_identity',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
      returnType: { type: 'ref', name: 'AxisIdentity', namespace: 'ascii' },
      returnDescription: 'Axis identity.',
      sync: true,
      private: true,
    },
    {
      name: 'DriverDisable',
      description: [
        'Disables the driver, which prevents current from being sent to the motor or load.',
        'If the driver is already disabled, the driver remains disabled.'
      ],
      route: 'device/driver_disable',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
      options: [],
    },
    {
      name: 'DriverEnable',
      description: [
        'Attempts to enable the driver repeatedly for the specified timeout.',
        'If the driver is already enabled, the driver remains enabled.',
      ],
      route: 'device/driver_enable',
      request: 'DriverEnableRequest',
      args: genAxisArgs(),
      params: [],
      options: [{
        name: 'Timeout',
        type: 'double',
        defaultValue: `${ENABLE_DRIVER_TIMEOUT}`,
        description: 'Timeout in seconds. Specify 0 to attempt to enable the driver once.',
      }],
    },
    {
      name: 'Activate',
      description: [
        'Activates a peripheral on this axis.',
        'Removes all identity information for the device.',
        'Run the identify method on the device after activating to refresh the information.',
      ],
      route: 'device/activate',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
      options: [],
    },
    {
      name: 'Restore',
      description: [
        'Restores all axis settings to their default values.',
        'Deletes all zaber axis storage keys.',
        'Disables lockstep if the axis is part of one. Unparks the axis.',
        'Preserves storage.',
        'The device needs to be identified again after the restore.',
      ],
      route: 'device/restore',
      request: 'DeviceRestoreRequest',
      args: genAxisArgs(),
      params: [],
    },
    moveSinFunctions.moveSin,
    moveSinFunctions.moveSinStop,
  ],
  py: {
    forwardTypes: ['Device']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'GetAxis',
    },
    args: ['1'],
  }],
};

export default definition;
