import { ClassDefinition } from '../types';

import { literalBool } from './helpers';

const definition: ClassDefinition = {
  name: 'Pvt',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      description: 'Device that this PVT belongs to.',
    },
  ],
  description: [
    'Class providing access to device PVT (Position-Velocity-Time) features.',
  ],
  minFw: [{ major: 7, minor: 33 }],
  ctor: { },
  functions: [
    {
      name: 'GetSequence',
      description: 'Gets a PvtSequence class instance which allows you to control a particular PVT sequence on the device.',
      sync: true,
      params: [
        { name: 'pvtId', type: 'int', description: 'The ID of the PVT sequence to control. The IDs start at 1.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'pvtId <= 0',
            py: 'pvt_id <= 0',
            ts: 'pvtId <= 0',
            java: 'pvtId <= 0',
            cpp: 'pvtId <= 0',
            swift: 'pvtId > 0',
          },
          message: 'Invalid value; PVT sequences are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new PvtSequence(Device, pvtId);',
        py: 'return PvtSequence(self.device, pvt_id)',
        ts: 'return new PvtSequence(this.device, pvtId);',
        java: 'return new PvtSequence(this.device, pvtId);',
        cpp: 'return {this->_device, pvtId};',
        swift: 'return PvtSequence(device: self.device, pvtId: pvtId)',
      },
      returnType: { type: 'ref', name: 'PvtSequence', namespace: 'ascii' },
      returnDescription: 'PvtSequence instance.',
    },
    {
      name: 'GetBuffer',
      description: 'Gets a PvtBuffer class instance which is a handle for a PVT buffer on the device.',
      sync: true,
      params: [
        { name: 'pvtBufferId', type: 'int', description: 'The ID of the PVT buffer to control. PVT buffer IDs start at one.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'pvtBufferId <= 0',
            py: 'pvt_buffer_id <= 0',
            ts: 'pvtBufferId <= 0',
            java: 'pvtBufferId <= 0',
            cpp: 'pvtBufferId <= 0',
            swift: 'pvtBufferId > 0',
          },
          message: 'Invalid value; PVT buffers are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new PvtBuffer(Device, pvtBufferId);',
        py: 'return PvtBuffer(self.device, pvt_buffer_id)',
        ts: 'return new PvtBuffer(this.device, pvtBufferId);',
        java: 'return new PvtBuffer(this.device, pvtBufferId);',
        cpp: 'return {this->_device, pvtBufferId};',
        swift: 'return PvtBuffer(device: self.device, bufferId: pvtBufferId)',
      },
      returnType: { type: 'ref', name: 'PvtBuffer', namespace: 'ascii' },
      returnDescription: 'PvtBuffer instance.',
    },
    {
      name: 'ListBufferIds',
      description: 'Get a list of buffer IDs that are currently in use.',
      route: 'device/stream_buffer_list',
      request: 'StreamBufferList',
      response: 'IntArrayResponse',
      args: [
        {
          name: 'InterfaceId',
          value: 'Device.Connection.InterfaceId',
        },
        {
          name: 'Device',
          value: 'Device.DeviceAddress',
        },
        {
          name: 'Pvt',
          value: literalBool(true),
        }
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'int' },
      returnDescription: 'List of buffer IDs.'
    }
  ],
  py: {
    forwardTypes: ['Device', 'PvtSequence', 'PvtBuffer']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Pvt',
    },
  }],
};

export default definition;
