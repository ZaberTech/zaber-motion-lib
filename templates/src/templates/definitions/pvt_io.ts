import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { literalBool } from './helpers';
import { streamIoFunctions } from './stream_io';

const addPvtIoArg = (previousArgs: MethodArg[] | undefined): MethodArg[] => ([
  ...(previousArgs ?? []),
  {
    name: 'Pvt',
    value: literalBool(true),
  }
]);

export const pvtIoFunctions: Record<string, MethodDefinition> = {
  setDigitalOutput: {
    ...streamIoFunctions.setDigitalOutput,
    args: addPvtIoArg(streamIoFunctions.setDigitalOutput.args),
  },
  setAllDigitalOutputs: {
    ...streamIoFunctions.setAllDigitalOutputs,
    args: addPvtIoArg(streamIoFunctions.setAllDigitalOutputs.args),
  },
  setAnalogOutput: {
    ...streamIoFunctions.setAnalogOutput,
    args: addPvtIoArg(streamIoFunctions.setAnalogOutput.args),
  },
  setAllAnalogOutputs: {
    ...streamIoFunctions.setAllAnalogOutputs,
    args: addPvtIoArg(streamIoFunctions.setAllAnalogOutputs.args),
  },
};

const definition: ClassDefinition = {
  name: 'PvtIo',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
    {
      name: 'StreamId',
      type: 'int',
      private: true,
    },
  ],
  description: [
    'Class providing access to I/O for a PVT sequence.',
  ],
  ctor: {},
  functions: [
    pvtIoFunctions.setDigitalOutput,
    pvtIoFunctions.setAllDigitalOutputs,
    pvtIoFunctions.setAnalogOutput,
    pvtIoFunctions.setAllAnalogOutputs,
    {
      ...streamIoFunctions.setDigitalOutputSchedule,
      args: addPvtIoArg(streamIoFunctions.setDigitalOutputSchedule.args),
    },
    {
      ...streamIoFunctions.setAllDigitalOutputsSchedule,
      args: addPvtIoArg(streamIoFunctions.setAllDigitalOutputsSchedule.args),
    },
    {
      ...streamIoFunctions.setAnalogOutputSchedule,
      args: addPvtIoArg(streamIoFunctions.setAnalogOutputSchedule.args),
    },
    {
      ...streamIoFunctions.setAllAnalogOutputsSchedule,
      args: addPvtIoArg(streamIoFunctions.setAllAnalogOutputsSchedule.args),
    },
    {
      ...streamIoFunctions.cancelDigitalOutputSchedule,
      args: addPvtIoArg(streamIoFunctions.cancelDigitalOutputSchedule.args),
    },
    {
      ...streamIoFunctions.cancelAllDigitalOutputsSchedule,
      args: addPvtIoArg(streamIoFunctions.cancelAllDigitalOutputsSchedule.args),
    },
    {
      ...streamIoFunctions.cancelAnalogOutputSchedule,
      args: addPvtIoArg(streamIoFunctions.cancelAnalogOutputSchedule.args),
    },
    {
      ...streamIoFunctions.cancelAllAnalogOutputsSchedule,
      args: addPvtIoArg(streamIoFunctions.cancelAllAnalogOutputsSchedule.args),
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
};

export default definition;
