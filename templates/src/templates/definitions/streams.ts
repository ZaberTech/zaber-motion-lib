import { ClassDefinition } from '../types';

import { literalBool } from './helpers';

const definition: ClassDefinition = {
  name: 'Streams',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      description: 'Device that these streams belong to.',
    },
  ],
  description: [
    'Class providing access to device streams.',
  ],
  minFw: [{ major: 7, minor: 5 }],
  ctor: { },
  functions: [
    {
      name: 'GetStream',
      description: 'Gets a Stream class instance which allows you to control a particular stream on the device.',
      sync: true,
      params: [
        { name: 'streamId', type: 'int', description: 'The ID of the stream to control. Stream IDs start at one.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'streamId <= 0',
            py: 'stream_id <= 0',
            ts: 'streamId <= 0',
            java: 'streamId <= 0',
            cpp: 'streamId <= 0',
            swift: 'streamId > 0',
          },
          message: 'Invalid value; streams are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new Stream(Device, streamId);',
        py: 'return Stream(self.device, stream_id)',
        ts: 'return new Stream(this.device, streamId);',
        java: 'return new Stream(this.device, streamId);',
        cpp: 'return {this->_device, streamId};',
        swift: 'return Stream(device: self.device, streamId: streamId)',
      },
      returnType: { type: 'ref', name: 'Stream', namespace: 'ascii' },
      returnDescription: 'Stream instance.',
    },
    {
      name: 'GetBuffer',
      description: 'Gets a StreamBuffer class instance which is a handle for a stream buffer on the device.',
      sync: true,
      params: [
        { name: 'streamBufferId', type: 'int', description: 'The ID of the stream buffer to control. Stream buffer IDs start at one.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'streamBufferId <= 0',
            py: 'stream_buffer_id <= 0',
            ts: 'streamBufferId <= 0',
            java: 'streamBufferId <= 0',
            cpp: 'streamBufferId <= 0',
            swift: 'streamBufferId > 0',
          },
          message: 'Invalid value; stream buffers are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new StreamBuffer(Device, streamBufferId);',
        py: 'return StreamBuffer(self.device, stream_buffer_id)',
        ts: 'return new StreamBuffer(this.device, streamBufferId);',
        java: 'return new StreamBuffer(this.device, streamBufferId);',
        cpp: 'return {this->_device, streamBufferId};',
        swift: 'return StreamBuffer(device: self.device, bufferId: streamBufferId)',
      },
      returnType: { type: 'ref', name: 'StreamBuffer', namespace: 'ascii' },
      returnDescription: 'StreamBuffer instance.',
    },
    {
      name: 'ListBufferIds',
      description: 'Get a list of buffer IDs that are currently in use.',
      route: 'device/stream_buffer_list',
      request: 'StreamBufferList',
      response: 'IntArrayResponse',
      args: [
        {
          name: 'InterfaceId',
          value: 'Device.Connection.InterfaceId',
        },
        {
          name: 'Device',
          value: 'Device.DeviceAddress',
        },
        {
          name: 'Pvt',
          value: literalBool(false),
        }
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'int' },
      returnDescription: 'List of buffer IDs.'
    }
  ],
  py: {
    forwardTypes: ['Device', 'Stream', 'StreamBuffer']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Streams',
    },
  }],
};

export default definition;
