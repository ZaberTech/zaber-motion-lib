import _ from 'lodash';

import { definitions as dto } from '../../dto/definitions';
import { ClassDefinition, MethodArg } from '../types';
import { namespaceEquals } from '../type_system';

import { genericCommand, genericCommandMultiResponse, genericCommandNoResponse } from './generic_commands';
import { canSetState, getState, setState } from './get_set_state';
import { defaultBool, defaultPojo } from './helpers';
import { generateLabelMethods } from './storage';
import { toStringDevice } from './to_string';

function genDeviceArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'DeviceAddress',
    },
  ];
}

const IGNORED_IDENTITY_PROPS = ['IsModified'];
const deviceIdentity = dto.classes.find(c => c.name === 'DeviceIdentity' && namespaceEquals(c.namespace, 'ascii'))!;

const definition: ClassDefinition = {
  name: 'Device',
  namespace: 'ascii',
  properties: [
    {
      name: 'Connection',
      description: 'Connection of this device.',
      type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
      cpp: {
        emptyCtorValue: '',
      },
    },
    {
      name: 'DeviceAddress',
      description: [
        'The device address uniquely identifies the device on the connection.',
        'It can be configured or automatically assigned by the renumber command.'
      ],
      type: 'int',
      cpp: {
        emptyCtorValue: '-1',
      },
    },
    {
      name: 'Settings',
      description: 'Settings and properties of this device.',
      type: { type: 'ref', name: 'DeviceSettings', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new DeviceSettings(this)',
        py: 'DeviceSettings(self)',
        ts: 'new DeviceSettings(this)',
        java: 'new DeviceSettings(this)',
        swift: 'DeviceSettings(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Storage',
      description: 'Key-value storage of this device.',
      type: { type: 'ref', name: 'DeviceStorage', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new DeviceStorage(this)',
        py: 'DeviceStorage(self)',
        ts: 'new DeviceStorage(this)',
        java: 'new DeviceStorage(this)',
        swift: 'DeviceStorage(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'IO',
      description: 'I/O channels of this device.',
      type: { type: 'ref', name: 'DeviceIO', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new DeviceIO(this)',
        py: 'DeviceIO(self)',
        ts: 'new DeviceIO(this)',
        java: 'new DeviceIO(this)',
        swift: 'DeviceIO(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'AllAxes',
      description: 'Virtual axis which allows you to target all axes of this device.',
      type: { type: 'ref', name: 'AllAxes', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new AllAxes(this)',
        py: 'AllAxes(self)',
        ts: 'new AllAxes(this)',
        java: 'new AllAxes(this)',
        swift: 'AllAxes(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Warnings',
      description: 'Warnings and faults of this device and all its axes.',
      type: { type: 'ref', name: 'Warnings', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new Warnings(this, 0)',
        py: 'Warnings(self, 0)',
        ts: 'new Warnings(this, 0)',
        java: 'new Warnings(this, 0)',
        swift: 'Warnings(device: self, axisNumber: 0)',
      },
      getter: {
        cpp: '{*this, 0}'
      },
    },
    {
      name: 'Identity',
      description: 'Identity of the device.',
      type: { type: 'ref', name: 'DeviceIdentity', namespace: 'ascii' },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveIdentity',
    },
    {
      name: 'IsIdentified',
      description: 'Indicates whether or not the device has been identified.',
      type: 'bool',
      getterCanThrow: true,
      getterFunctionName: 'RetrieveIsIdentified',
    },
    {
      name: 'Oscilloscope',
      description: 'Oscilloscope recording helper for this device.',
      minFw: [{ major: 7, minor: 0 }],
      type: { type: 'ref', name: 'Oscilloscope', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new Oscilloscope(this)',
        py: 'Oscilloscope(self)',
        ts: 'new Oscilloscope(this)',
        java: 'new Oscilloscope(this)',
        swift: 'Oscilloscope(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    ...deviceIdentity.properties
      .filter(property => !IGNORED_IDENTITY_PROPS.includes(property.name))
      .map(property => ({
        name: property.name,
        description: property.description!,
        type: property.type,
        getterCanThrow: true,
        getter: {
          cs: `Identity.${property.name}`,
          py: `self.identity.${_.snakeCase(property.name)}`,
          ts: `this.identity.${_.lowerFirst(property.name)}`,
          java: `this.getIdentity().get${property.name}()`,
          cpp: `this->getIdentity().get${property.name}()`,
          swift: `identity.${_.lowerFirst(property.name)}`,
        }
      })),
    {
      name: 'Label',
      description: 'User-assigned label of the device.',
      type: 'string',
      getterCanThrow: true,
      getterFunctionName: 'RetrieveLabel',
    },
    {
      name: 'Triggers',
      description: 'Triggers for this device.',
      minFw: [{ major: 7, minor: 6 }],
      type: { type: 'ref', name: 'Triggers', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new Triggers(this)',
        py: 'Triggers(self)',
        ts: 'new Triggers(this)',
        java: 'new Triggers(this)',
        swift: 'Triggers(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Streams',
      description: 'Gets an object that provides access to Streams on this device.',
      minFw: [{ major: 7, minor: 5 }],
      type: { type: 'ref', name: 'Streams', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new Streams(this)',
        py: 'Streams(self)',
        ts: 'new Streams(this)',
        java: 'new Streams(this)',
        swift: 'Streams(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
    {
      name: 'Pvt',
      description: [
        'Gets an object that provides access to PVT functions of this device.',
        'Note that as of ZML v5.0.0, this returns a Pvt object and NOT a PvtSequence object.',
        'The PvtSequence can now be obtained from the Pvt object.',
      ],
      minFw: [{ major: 7, minor: 33 }],
      type: { type: 'ref', name: 'Pvt', namespace: 'ascii' },
      referencesSelf: true,
      value: {
        cs: 'new Pvt(this)',
        py: 'Pvt(self)',
        ts: 'new Pvt(this)',
        java: 'new Pvt(this)',
        swift: 'Pvt(device: self)',
      },
      getter: {
        cpp: '{*this}'
      },
    },
  ],
  description: 'Represents the controller part of one device - may be either a standalone controller or an integrated controller.',
  ctor: { },
  functions: [
    {
      name: 'Identify',
      description: [
        'Queries the device and the database, gathering information about the product.',
        'Without this information features such as unit conversions will not work.',
        'Usually, called automatically by detect devices method.'
      ],
      route: 'device/identify',
      request: 'DeviceIdentifyRequest',
      args: genDeviceArgs(),
      params: [],
      options: [{
        name: 'assumeVersion',
        type: { type: 'optional', of: { type: 'ref', name: 'FirmwareVersion', namespace: [] } },
        description: [
          'The identification assumes the specified firmware version',
          'instead of the version queried from the device.',
          'Providing this argument can lead to unexpected compatibility issues.',
        ],
        defaultValue: defaultPojo(),
      }],
      returnType: { type: 'ref', name: 'DeviceIdentity', namespace: 'ascii' },
      returnDescription: 'Device identification data.',
    },
    genericCommand({
      interfaceId: 'Connection.InterfaceId', device: 'DeviceAddress',
      description: [
        'Sends a generic ASCII command to this device.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
    }),
    genericCommandMultiResponse({
      interfaceId: 'Connection.InterfaceId', device: 'DeviceAddress', description: [
        'Sends a generic ASCII command to this device and expect multiple responses.',
        'Responses are returned in order of arrival.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ],
    }),
    genericCommandNoResponse({
      interfaceId: 'Connection.InterfaceId', device: 'DeviceAddress', description: [
        'Sends a generic ASCII command to this device without expecting a response and without adding a message ID',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
    }),
    {
      name: 'GetAxis',
      description: [
        'Gets an Axis class instance which allows you to control a particular axis on this device.',
        'Axes are numbered from 1.',
      ],
      sync: true,
      params: [
        { name: 'axisNumber', type: 'int', description: 'Number of axis intended to control.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'axisNumber <= 0',
            py: 'axis_number <= 0',
            ts: 'axisNumber <= 0',
            java: 'axisNumber <= 0',
            cpp: 'axisNumber <= 0',
            swift: 'axisNumber > 0',
          },
          message: 'Invalid value; physical axes are numbered from 1.',
        }
      ],
      code: {
        cs: 'return new Axis(this, axisNumber);',
        py: 'return Axis(self, axis_number)',
        ts: 'return new Axis(this, axisNumber);',
        java: 'return new Axis(this, axisNumber);',
        cpp: 'return {*this, axisNumber};',
        swift: 'return Axis(device: self, axisNumber: axisNumber)',
      },
      returnType: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      returnDescription: 'Axis instance.',
    },
    {
      name: 'GetLockstep',
      description: 'Gets a Lockstep class instance which allows you to control a particular lockstep group on the device.',
      minFw: [{ major: 6, minor: 15 }, { major: 7, minor: 11 }],
      sync: true,
      params: [
        { name: 'lockstepGroupId', type: 'int', description: 'The ID of the lockstep group to control. Lockstep group IDs start at one.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'lockstepGroupId <= 0',
            py: 'lockstep_group_id <= 0',
            ts: 'lockstepGroupId <= 0',
            java: 'lockstepGroupId <= 0',
            cpp: 'lockstepGroupId <= 0',
            swift: 'lockstepGroupId > 0',
          },
          message: 'Invalid value; lockstep groups are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new Lockstep(this, lockstepGroupId);',
        py: 'return Lockstep(self, lockstep_group_id)',
        ts: 'return new Lockstep(this, lockstepGroupId);',
        java: 'return new Lockstep(this, lockstepGroupId);',
        cpp: 'return {*this, lockstepGroupId};',
        swift: 'return Lockstep(device: self, lockstepGroupId: lockstepGroupId)',
      },
      returnType: { type: 'ref', name: 'Lockstep', namespace: 'ascii' },
      returnDescription: 'Lockstep instance.',
    },
    {
      name: 'PrepareCommand',
      description: [
        'Formats parameters into a command and performs unit conversions.',
        'Parameters in the command template are denoted by a question mark.',
        'Command returned is only valid for this device.',
        'Unit conversion is not supported for commands where axes can be remapped, such as stream and PVT commands.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).'
      ],
      route: 'device/prepare_command',
      request: 'PrepareCommandRequest',
      response: 'StringResponse',
      returnType: 'string',
      returnPropertyName: 'Value',
      returnDescription: 'Command with converted parameters.',
      sync: true,
      args: genDeviceArgs(),
      params: [
        {
          name: 'commandTemplate', type: 'string',
          description: 'Template of a command to prepare. Parameters are denoted by question marks.'
        },
      ],
      variadic: {
        name: 'parameters',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: 'Variable number of command parameters.',
      },
    },
    ...generateLabelMethods('device', genDeviceArgs),
    toStringDevice(genDeviceArgs()),
    getState({ name: 'device', args: genDeviceArgs() }),
    setState({ name: 'device', args: genDeviceArgs() }),
    canSetState({ name: 'device', args: genDeviceArgs() }),
    {
      name: 'waitToRespond',
      hidden: true,
      description: [
        'Waits for the device to start responding to messages.',
        'Useful to call after resetting the device.',
        'Throws RequestTimeoutException upon timeout.',
      ],
      route: 'device/wait_to_respond',
      request: 'WaitToRespondRequest',
      args: genDeviceArgs(),
      params: [{
        name: 'Timeout', type: 'double',
        description: [
          'For how long to wait in milliseconds for the device to start responding.',
        ],
      }],
    },
    {
      name: 'Renumber',
      description: [
        'Changes the address of this device.',
        'After the address is successfully changed, the existing device class instance no longer represents the device.',
        'Instead, use the new device instance returned by this method.'
      ],
      route: 'device/renumber',
      request: 'RenumberRequest',
      response: 'IntResponse',
      args: genDeviceArgs(),
      params: [
        {
          name: 'Address',
          type: 'int',
          description: 'The new address to assign to the device.',
        }
      ],
      paramsCheck: [
        {
          code: {
            cs: 'address < 1 || address > 99',
            py: 'address < 1 or address > 99',
            ts: 'address < 1 || address > 99',
            java: 'address < 1 || address > 99',
            cpp: 'address < 1 || address > 99',
            swift: 'address >= 1 && address <= 99',
          },
          message: 'Invalid value; device addresses are numbered from 1 to 99.'
        }
      ],
      returnType: { type: 'ref', name: 'Device', namespace: 'ascii' },
      returnDescription: 'New device instance with the new address.',
      customReturn: {
        cs: 'new Device(Connection, response.Value)',
        py: 'Device(self.connection, response.value)',
        ts: 'new Device(this.connection, response.value)',
        java: 'new Device(this.connection, r.getValue())',
        cpp: '{this->_connection, response.getValue()}',
        swift: 'Device(connection: self.connection, deviceAddress: response.value)',
      },
    },
    {
      name: 'Restore',
      description: [
        'Restores most of the settings to their default values.',
        'Deletes all triggers, stream and PVT buffers, servo tunings.',
        'Deletes all zaber storage keys.',
        'Disables locksteps, unparks axes.',
        'Preserves storage, communication settings, peripherals (unless hard is specified).',
        'The device needs to be identified again after the restore.',
      ],
      route: 'device/restore',
      request: 'DeviceRestoreRequest',
      args: genDeviceArgs(),
      params: [
        {
          name: 'Hard',
          type: 'bool',
          description: 'If true, completely erases device\'s memory. The device also resets.',
          defaultValue: defaultBool(false),
        }
      ],
    },
    {
      name: 'RetrieveIdentity',
      description: 'Returns identity.',
      route: 'device/get_identity',
      request: 'DeviceEmptyRequest',
      args: genDeviceArgs(),
      params: [],
      returnType: { type: 'ref', name: 'DeviceIdentity', namespace: 'ascii' },
      returnDescription: 'Device identity.',
      sync: true,
      private: true,
    },
    {
      name: 'RetrieveIsIdentified',
      description: 'Returns whether or not the device have been identified.',
      route: 'device/get_is_identified',
      request: 'DeviceEmptyRequest',
      response: 'BoolResponse',
      args: genDeviceArgs(),
      params: [],
      returnType: 'bool',
      returnDescription: 'True if the device has already been identified. False otherwise.',
      returnPropertyName: 'Value',
      sync: true,
      private: true,
    },
  ],
  py: {
    forwardTypes: ['Connection']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Connection',
      member: 'GetDevice'
    },
    args: ['1'],
  }],
};

export default definition;
