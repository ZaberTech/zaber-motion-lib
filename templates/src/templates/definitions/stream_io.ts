import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { outputFunctions } from './device_io';

const streamIoArgs: MethodArg[] = [
  {
    name: 'InterfaceId',
    value: {
      cs: '_device.Connection.InterfaceId',
      py: 'self._device.connection.interface_id',
      ts: 'this._device.connection.interfaceId',
      java: 'this.device.getConnection().getInterfaceId()',
      cpp: 'this->_device.getConnection().getInterfaceId()',
      swift: 'device.connection.interfaceId',
    }
  },
  {
    name: 'Device',
    value: {
      cs: '_device.DeviceAddress',
      py: 'self._device.device_address',
      ts: 'this._device.deviceAddress',
      java: 'this.device.getDeviceAddress()',
      cpp: 'this->_device.getDeviceAddress()',
      swift: 'device.deviceAddress',
    }
  },
  {
    name: 'StreamId',
    value: {
      cs: '_streamId',
      py: 'self._stream_id',
      ts: 'this._streamId',
      java: 'this.streamId',
      cpp: 'this->getStreamId()',
      swift: 'streamId',
    }
  }
];

const addStreamIoArg = (previousArgs: MethodArg[] | undefined): MethodArg[] => ([
  ...(previousArgs ?? []),
  {
    name: 'StreamId',
    value: {
      cs: '_streamId',
      py: 'self._stream_id',
      ts: 'this._streamId',
      java: 'this.streamId',
      cpp: 'this->getStreamId()',
      swift: 'streamId',
    }
  }
]);

export const streamIoFunctions: Record<string, MethodDefinition> = {
  setDigitalOutput: {
    ...outputFunctions.setDigitalOutput,
    route: 'device/stream_set_digital_output',
    request: 'StreamSetDigitalOutputRequest',
    args: addStreamIoArg(outputFunctions.setDigitalOutput.args),
  },
  setAllDigitalOutputs: {
    ...outputFunctions.setAllDigitalOutputs,
    route: 'device/stream_set_all_digital_outputs',
    request: 'StreamSetAllDigitalOutputsRequest',
    args: addStreamIoArg(outputFunctions.setAllDigitalOutputs.args),
  },
  setDigitalOutputSchedule: {
    ...outputFunctions.setDigitalOutputSchedule,
    route: 'device/stream_set_digital_output_schedule',
    request: 'StreamSetDigitalOutputScheduleRequest',
    args: addStreamIoArg(outputFunctions.setDigitalOutputSchedule.args),
  },
  setAllDigitalOutputsSchedule: {
    ...outputFunctions.setAllDigitalOutputsSchedule,
    route: 'device/stream_set_all_digital_outputs_schedule',
    request: 'StreamSetAllDigitalOutputsScheduleRequest',
    args: addStreamIoArg(outputFunctions.setAllDigitalOutputsSchedule.args),
  },
  setAnalogOutput: {
    ...outputFunctions.setAnalogOutput,
    route: 'device/stream_set_analog_output',
    request: 'StreamSetAnalogOutputRequest',
    args: addStreamIoArg(outputFunctions.setAnalogOutput.args),
  },
  setAllAnalogOutputs: {
    ...outputFunctions.setAllAnalogOutputs,
    route: 'device/stream_set_all_analog_outputs',
    request: 'StreamSetAllAnalogOutputsRequest',
    args: addStreamIoArg(outputFunctions.setAllAnalogOutputs.args),
  },
  setAnalogOutputSchedule: {
    ...outputFunctions.setAnalogOutputSchedule,
    route: 'device/stream_set_analog_output_schedule',
    request: 'StreamSetAnalogOutputScheduleRequest',
    args: addStreamIoArg(outputFunctions.setAnalogOutputSchedule.args),
  },
  setAllAnalogOutputsSchedule: {
    ...outputFunctions.setAllAnalogOutputsSchedule,
    route: 'device/stream_set_all_analog_outputs_schedule',
    request: 'StreamSetAllAnalogOutputsScheduleRequest',
    args: addStreamIoArg(outputFunctions.setAllAnalogOutputsSchedule.args),
  },
  cancelDigitalOutputSchedule: {
    ...outputFunctions.cancelDigitalOutputSchedule,
    route: 'device/stream_cancel_output_schedule',
    request: 'StreamCancelOutputScheduleRequest',
    args: addStreamIoArg(outputFunctions.cancelDigitalOutputSchedule.args),
  },
  cancelAllDigitalOutputsSchedule: {
    ...outputFunctions.cancelAllDigitalOutputsSchedule,
    route: 'device/stream_cancel_all_outputs_schedule',
    request: 'StreamCancelAllOutputsScheduleRequest',
    args: addStreamIoArg(outputFunctions.cancelAllDigitalOutputsSchedule.args),
  },
  cancelAnalogOutputSchedule: {
    ...outputFunctions.cancelAnalogOutputSchedule,
    route: 'device/stream_cancel_output_schedule',
    request: 'StreamCancelOutputScheduleRequest',
    args: addStreamIoArg(outputFunctions.cancelAnalogOutputSchedule.args),
  },
  cancelAllAnalogOutputsSchedule: {
    ...outputFunctions.cancelAllAnalogOutputsSchedule,
    route: 'device/stream_cancel_all_outputs_schedule',
    request: 'StreamCancelAllOutputsScheduleRequest',
    args: addStreamIoArg(outputFunctions.cancelAllAnalogOutputsSchedule.args),
  },
  waitDigitalInput: {
    name: 'WaitDigitalInput',
    description: ['Wait for a digital input channel to reach a given value.'],
    route: 'device/stream_wait_digital_input',
    request: 'StreamWaitDigitalInputRequest',
    args: streamIoArgs,
    params: [
      {
        name: 'channelNumber',
        type: 'int',
        description: ['The number of the digital input channel.',
          'Channel numbers are numbered from one.']
      },
      {
        name: 'value',
        type: 'bool',
        description: 'The value that the stream should wait for.'
      }
    ],
  },
  waitAnalogInput: {
    name: 'WaitAnalogInput',
    description: ['Wait for the value of a analog input channel to reach a condition concerning a given value.'],
    route: 'device/stream_wait_analog_input',
    request: 'StreamWaitAnalogInputRequest',
    args: streamIoArgs,
    params: [
      {
        name: 'channelNumber',
        type: 'int',
        description: ['The number of the analog input channel.',
          'Channel numbers are numbered from one.']
      },
      {
        name: 'condition',
        type: 'string',
        description: 'A condition (e.g. <, <=, ==, !=).'
      },
      {
        name: 'value',
        type: 'double',
        description: 'The value that the condition concerns, in Volts.'
      }
    ],
  },
};

const definition: ClassDefinition = {
  name: 'StreamIo',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
    {
      name: 'StreamId',
      type: 'int',
      private: true,
    },
  ],
  description: [
    'Class providing access to I/O for a stream.',
  ],
  ctor: {},
  functions: [
    streamIoFunctions.setDigitalOutput,
    streamIoFunctions.setAllDigitalOutputs,
    streamIoFunctions.setDigitalOutputSchedule,
    streamIoFunctions.setAllDigitalOutputsSchedule,
    streamIoFunctions.setAnalogOutput,
    streamIoFunctions.setAllAnalogOutputs,
    streamIoFunctions.setAnalogOutputSchedule,
    streamIoFunctions.setAllAnalogOutputsSchedule,
    streamIoFunctions.cancelDigitalOutputSchedule,
    streamIoFunctions.cancelAllDigitalOutputsSchedule,
    streamIoFunctions.cancelAnalogOutputSchedule,
    streamIoFunctions.cancelAllAnalogOutputsSchedule,
    streamIoFunctions.waitDigitalInput,
    streamIoFunctions.waitAnalogInput,
  ],
  py: {
    forwardTypes: ['Device']
  },
};

export default definition;
