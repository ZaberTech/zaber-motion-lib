import { ClassDefinition, MethodArg } from '../types';

import { defaultUnits, defaultString, unitsValue } from './helpers';

export function genOscilloscopeArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
  ];
}

function genSettingNameArg(name: string): MethodArg {
  return {
    name: 'setting',
    value: defaultString(name),
  };
}

const definition: ClassDefinition = {
  name: 'Oscilloscope',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that this Oscilloscope measures.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
  ],
  description: [
    'Provides a convenient way to control the oscilloscope data recording feature of some devices.',
    'The oscilloscope can record the values of some settings over time at high resolution.',
  ],
  minFw: [{ major: 7, minor: 0 }],
  ctor: { },
  functions: [
    {
      name: 'AddChannel',
      description: 'Select a setting to be recorded.',
      route: 'oscilloscope/add_setting_channel',
      request: 'OscilloscopeAddSettingChannelRequest',
      args: genOscilloscopeArgs(),
      params: [
        {
          name: 'Axis',
          type: 'int',
          description: 'The 1-based index of the axis to record the value from.',
        },
        {
          name: 'Setting',
          type: 'string',
          description: 'The name of a setting to record.',
        },
      ],
    },
    {
      name: 'AddIoChannel',
      description: 'Select an I/O pin to be recorded.',
      minFw: [{ major: 7, minor: 33 }],
      route: 'oscilloscope/add_io_channel',
      request: 'OscilloscopeAddIoChannelRequest',
      args: genOscilloscopeArgs(),
      params: [
        {
          name: 'IoType',
          type: { type: 'enum', name: 'IoPortType', namespace: 'ascii' },
          description: 'The I/O port type to read data from.'
        },
        {
          name: 'IoChannel',
          type: 'int',
          description: 'The 1-based index of the I/O pin to read from.',
        },
      ],
    },
    {
      name: 'Clear',
      description: ['Clear the list of channels to record.'],
      route: 'oscilloscope/clear_channels',
      request: 'DeviceEmptyRequest',
      args: genOscilloscopeArgs(),
      params: [],
    },
    {
      name: 'GetTimebase',
      description: 'Get the current sampling interval.',
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.timebase')]),
      params: [
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the timebase in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The current sampling interval in the selected time units.',
    },
    {
      name: 'SetTimebase',
      description: 'Set the sampling interval.',
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.timebase')]),
      params: [
        {
          name: 'Interval',
          type: 'double',
          requestName: 'Value',
          description: 'Sample interval for the next oscilloscope recording. Minimum value is 100µs.'
        }, {
          name: 'Unit', type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure the timebase is represented in.',
        },
      ],
    },
    {
      name: 'GetFrequency',
      description: [
        'Get the current sampling frequency.',
        'The values is calculated as the inverse of the current sampling interval.',
      ],
      route: 'oscilloscope/get_frequency',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.timebase')]),
      params: [
        {
          name: 'unit',
          type: 'FrequencyUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the frequency in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The inverse of current sampling interval in the selected units.',
    },
    {
      name: 'SetFrequency',
      description: [
        'Set the sampling frequency (inverse of the sampling interval).',
        'The value is quantized to the next closest value supported by the firmware.',
      ],
      route: 'oscilloscope/set_frequency',
      request: 'DeviceSetSettingRequest',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.timebase')]),
      params: [
        {
          name: 'Frequency',
          type: 'double',
          requestName: 'Value',
          description: 'Sample frequency for the next oscilloscope recording.',
        }, {
          name: 'Unit', type: 'FrequencyUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure the frequency is represented in.',
        },
      ],
    },
    {
      name: 'GetDelay',
      description: 'Get the delay before oscilloscope recording starts.',
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.delay')]),
      params: [
        {
          name: 'unit', type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the delay in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The current start delay in the selected time units.',
    },
    {
      name: 'SetDelay',
      description: 'Set the sampling start delay.',
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.delay')]),
      params: [
        {
          name: 'Interval',
          type: 'double',
          requestName: 'Value',
          description: 'Delay time between triggering a recording and the first data point being recorded.'
        }, {
          name: 'Unit', type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure the delay is represented in.',
        },
      ],
    },
    {
      name: 'GetMaxChannels',
      description: 'Get the maximum number of channels that can be recorded.',
      route: 'oscilloscope/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'IntResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.numchannels')]),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'The maximum number of channels that can be added to an Oscilloscope recording.',
    },
    {
      name: 'GetMaxBufferSize',
      description: 'Get the maximum number of samples that can be recorded per Oscilloscope channel.',
      route: 'oscilloscope/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'IntResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.channel.size.max')]),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'The maximum number of samples that can be recorded per Oscilloscope channel.',
    },
    {
      name: 'GetBufferSize',
      description: 'Get the number of samples that can be recorded per channel given the current number of channels added.',
      route: 'oscilloscope/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'IntResponse',
      args: ([...genOscilloscopeArgs(), genSettingNameArg('scope.channel.size')]),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of samples that will be recorded per channel with the current channels. Zero if none have been added.',
    },
    {
      name: 'Start',
      description: 'Trigger data recording.',
      route: 'oscilloscope/start',
      request: 'OscilloscopeStartRequest',
      args: genOscilloscopeArgs(),
      params: [{
        name: 'CaptureLength',
        type: 'int',
        defaultValue: '0',
        description: ['Optional number of samples to record per channel.',
          'If left empty, the device records samples until the buffer fills.'],
        minFw: [{ major: 7, minor: 29 }],
      }],
    },
    {
      name: 'Stop',
      description: 'End data recording if currently in progress.',
      route: 'oscilloscope/stop',
      request: 'OscilloscopeRequest',
      args: genOscilloscopeArgs(),
      params: [],
    },
    {
      name: 'Read',
      description: 'Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.',
      route: 'oscilloscope/read',
      request: 'DeviceEmptyRequest',
      response: 'OscilloscopeReadResponse',
      args: genOscilloscopeArgs(),
      params: [],
      returnType: { type: 'array', of: { type: 'ref', name: 'OscilloscopeData', namespace: 'ascii' } },
      returnDescription: 'Array of recorded channel data arrays, in the order added.',
      customReturn: {
        cs: 'response.DataIds.Select(id => new OscilloscopeData(id)).ToArray()',
        py: 'list(map(OscilloscopeData, response.data_ids))',
        ts: 'response.dataIds.map(id => new OscilloscopeData(id))',
        java: 'ArrayUtility.arrayFromInt(OscilloscopeData[]::new, r.getDataIds(), id -> new OscilloscopeData(id))',
        cpp: 'zml_util::map_vec<int,OscilloscopeData>(response.getDataIds(), [](const int& id) { return OscilloscopeData(id); })',
        swift: 'response.dataIds.map { OscilloscopeData(dataId: $0) }',
      },
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Oscilloscope',
    },
  }],
};

export default definition;
