import { connection, cppConnections } from './connection';
import { connection as binaryConnection, cppConnections as cppBinaryConnections } from './binary_connection';
import binaryDevice from './binary_device';
import binaryDeviceSettings from './binary_device_settings';
import device from './device';
import axis from './axis';
import axisSettings from './axis_settings';
import deviceSettings from './device_settings';
import { axisStorage, deviceStorage } from './storage';
import deviceIO from './device_io';
import allAxes from './all_axes';
import units from './units';
import unitTable from './unit_table';
import warnings from './warnings';
import warningFlags from './warning_flags';
import library from './library';
import tools from './tools';
import motionLibException from './motion_lib_exception';
import lockstep from './lockstep';
import oscilloscope from './oscilloscope';
import oscilloscopeData from './oscilloscope_data';
import process from './process';
import processController from './process_controller';
import stream from './stream';
import streamBuffer from './stream_buffer';
import streamIo from './stream_io';
import pvt from './pvt';
import pvtSequence from './pvt_sequence';
import pvtBuffer from './pvt_buffer';
import pvtIo from './pvt_io';
import transport from './transport';
import * as generated from './generated';
import { exceptions } from './exceptions';
import offlineTranslator from './gcode/offline_translator';
import translator from './gcode/translator';
import servoTuner from './servo_tuner';
import objectiveChanger from './microscopy/objective_changer';
import illuminator from './microscopy/illuminator';
import illuminatorChannel from './microscopy/illuminator_channel';
import microscope from './microscopy/microscope';
import filterChanger from './microscopy/filter_changer';
import wdiAutofocus from './microscopy/wdi_autofocus';
import autofocus from './microscopy/autofocus';
import cameraTrigger from './microscopy/camera_trigger';
import axisGroup from './axis_group';
import triggers from './triggers';
import trigger from './trigger';
import streams from './streams';

export const definitions = {
  classes: {
    connection,
    device,
    binaryConnection,
    axis,
    axisSettings,
    deviceSettings,
    axisStorage,
    deviceStorage,
    deviceIO,
    allAxes,
    warnings,
    library,
    binaryDevice,
    binaryDeviceSettings,
    tools,
    lockstep,
    process,
    processController,
    servoTuner,
    oscilloscope,
    oscilloscopeData,
    stream,
    streamBuffer,
    streamIo,
    transport,
    translator,
    offlineTranslator,
    objectiveChanger,
    pvt,
    pvtSequence,
    pvtBuffer,
    pvtIo,
    illuminator,
    illuminatorChannel,
    microscope,
    filterChanger,
    unitTable,
    axisGroup,
    triggers,
    trigger,
    streams,
    wdiAutofocus,
    autofocus,
    cameraTrigger,
  },
  cppConnections,
  cppBinaryConnections,
  units,
  stringConstants: { warningFlags, ...generated.stringConstants },
  motionLibException,
  exceptions,
};
