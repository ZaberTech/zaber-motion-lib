import { ClassDefinition } from '../types';

const definition: ClassDefinition = {
  name: 'MotionLibException',
  namespace: 'exceptions',
  properties: [
    {
      name: 'Message',
      description: 'Error message of the exception.',
      type: 'string',
    },
    {
      name: 'Details',
      description: 'Additional data for a given exception subclass. The structure of this data is different for each subclass'
        + ' and some subclasses may not even have this property. See the list of available subclasses below for more details.',
      type: { type: 'ref', name: 'ExceptionData', namespace: 'exceptions' }
    },
  ],
  description: 'Exception originating in the library.',
  ctor: {
  },
  functions: [
    {
      name: 'ToString',
      description: 'Returns a string that represents the exception.',
      sync: true,
      override: true,
      const: true,
      params: [],
      returnType: 'string',
      returnDescription: 'A string that represents the exception.',
      noThrow: true,
    },
  ],
};

export default definition;
