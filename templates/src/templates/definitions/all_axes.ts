import { ClassDefinition, MethodArg } from '../types';

import { ENABLE_DRIVER_TIMEOUT } from './axis';

function genAxisArgs(): MethodArg[] {
  return [
    ...genAllAxesArgs(),
    {
      name: 'Axis',
      value: {
        cs: '0',
        py: '0',
        ts: '0',
        java: '0',
        cpp: '0',
        swift: '0',
      },
    },
  ];
}
function genAllAxesArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
  ];
}

const definition: ClassDefinition = {
  name: 'AllAxes',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that controls this axis.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
  ],
  description: 'Represents all axes of motion associated with a device.',
  ctor: { },
  functions: [
    {
      name: 'Home',
      description: 'Homes all axes. Axes return to their homing positions.',
      route: 'device/home',
      request: 'DeviceHomeRequest',
      args: genAxisArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
      ],
    },
    {
      name: 'Stop',
      description: 'Stops ongoing axes movement. Decelerates until zero speed.',
      route: 'device/stop',
      request: 'DeviceStopRequest',
      args: genAxisArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
      ],
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until all axes of device stop moving.',
      route: 'device/wait_until_idle',
      request: 'DeviceWaitUntilIdleRequest',
      args: genAxisArgs(),
      params: [],
      options: [
        {
          name: 'throwErrorOnFault',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether to throw error when fault is observed.'
        }
      ],
    },
    {
      name: 'Park',
      description: [
        'Parks the device in anticipation of turning the power off.',
        'It can later be powered on, unparked, and moved without first having to home it.'
      ],
      route: 'device/park',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
    },
    {
      name: 'Unpark',
      description: 'Unparks the device. The device will now be able to move.',
      route: 'device/unpark',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
    },
    {
      name: 'IsBusy',
      description: 'Returns bool indicating whether any axis is executing a motion command.',
      route: 'device/is_busy',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if any axis is currently executing a motion command.',
    },
    {
      name: 'IsHomed',
      description: 'Returns bool indicating whether all axes have position reference and were homed.',
      route: 'device/is_homed',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      args: genAxisArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if all axes have position reference and were homed.',
    },
    {
      name: 'DriverDisable',
      description: [
        'Disables all axes drivers, which prevents current from being sent to the motor or load.',
      ],
      route: 'device/driver_disable',
      request: 'AxisEmptyRequest',
      args: genAxisArgs(),
      params: [],
      options: [],
    },
    {
      name: 'DriverEnable',
      description: [
        'Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.',
        'If the driver is already enabled, the driver remains enabled.',
      ],
      route: 'device/driver_enable',
      request: 'DriverEnableRequest',
      args: genAxisArgs(),
      params: [],
      options: [{
        name: 'Timeout',
        type: 'double',
        defaultValue: `${ENABLE_DRIVER_TIMEOUT}`,
        description: 'Timeout in seconds. Specify 0 to attempt to enable the driver once.',
      }],
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the axes.',
      route: 'device/all_axes_to_string',
      request: 'AxisEmptyRequest',
      response: 'StringResponse',
      args: genAxisArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the axes.',
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'AllAxes',
    },
  }],
};

export default definition;
