import { ClassDefinition, MethodArg } from '../types';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'Interfaces',
      value: {
        ts: 'this._axes.map(axis => axis.device.connection.interfaceId)',
        py: '[axis.device.connection.interface_id for axis in self._axes]',
        cs: 'Axes.Select(axis => axis.Device.Connection.InterfaceId).ToArray()',
        java: 'ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId())',
        cpp: 'zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getConnection().getInterfaceId(); })',
        swift: 'axes.map { $0.device.connection.interfaceId }',
      },
      type: { type: 'array', of: 'int' },
    },
    {
      name: 'Devices',
      value: {
        ts: 'this._axes.map(axis => axis.device.deviceAddress)',
        py: '[axis.device.device_address for axis in self._axes]',
        cs: 'Axes.Select(axis => axis.Device.DeviceAddress).ToArray()',
        java: 'ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress())',
        cpp: 'zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getDevice().getDeviceAddress(); })',
        swift: 'axes.map { $0.device.deviceAddress }',
      },
      type: { type: 'array', of: 'int' },
    },
    {
      name: 'Axes',
      value: {
        ts: 'this._axes.map(axis => axis.axisNumber)',
        py: '[axis.axis_number for axis in self._axes]',
        cs: 'Axes.Select(axis => axis.AxisNumber).ToArray()',
        java: 'ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber())',
        cpp: 'zml_util::map_vec<Axis,int>(this->_axes, [](const Axis& axis) { return axis.getAxisNumber(); })',
        swift: 'axes.map { $0.axisNumber }',
      },
      type: { type: 'array', of: 'int' },
    },
  ];
}

const definition: ClassDefinition = {
  name: 'AxisGroup',
  namespace: 'ascii',
  properties: [
    {
      name: 'Axes',
      description: 'Axes of the group.',
      type: { type: 'array', of: { type: 'ref', name: 'Axis', namespace: 'ascii' } },
      cpp: {
        emptyCtorValue: '',
      },
      value: {
        ctorArgument: true,
        ts: 'axes.slice()',
        py: 'axes.copy()',
        cs: 'axes.ToArray()',
        java: 'axes.clone()',
        cpp: 'std::move(axes)',
        swift: 'axes',
      },
    },
  ],
  description: [
    'Groups multiple axes across devices into a single group to allow for simultaneous movement.',
    'Note that the movement is not coordinated and trajectory is inconsistent and not repeatable between calls.',
    'Make sure that any possible trajectory is clear of potential obstacles.',
    'The movement methods return after all the axes finish the movement successfully',
    'or throw an error as soon as possible.'
  ],
  ctor: {
    public: true,
    description: 'Initializes the group with the axes to be controlled.',
  },
  functions: [
    {
      name: 'Home',
      description: 'Homes the axes.',
      route: 'axes/home',
      request: 'AxesEmptyRequest',
      args: genArgs(),
      params: [],
      options: [],
    },
    {
      name: 'Stop',
      description: 'Stops the axes.',
      route: 'axes/stop',
      request: 'AxesEmptyRequest',
      args: genArgs(),
      params: [],
      options: [],
    },
    {
      name: 'MoveAbsolute',
      description: 'Moves the axes to absolute position.',
      route: 'axes/move_absolute',
      request: 'AxesMoveRequest',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'position',
        description: 'Position.',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
      }
    },
    {
      name: 'MoveRelative',
      description: 'Move axes to position relative to the current position.',
      route: 'axes/move_relative',
      request: 'AxesMoveRequest',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'position',
        description: 'Position.',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
      }
    },
    {
      name: 'MoveMin',
      description: 'Moves axes to the minimum position as specified by limit.min.',
      route: 'axes/move_min',
      request: 'AxesEmptyRequest',
      args: genArgs(),
      params: [],
    },
    {
      name: 'MoveMax',
      description: 'Moves axes to the maximum position as specified by limit.max.',
      route: 'axes/move_max',
      request: 'AxesEmptyRequest',
      args: genArgs(),
      params: [],
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until all the axes stop moving.',
      route: 'axes/wait_until_idle',
      request: 'AxesEmptyRequest',
      args: genArgs(),
      params: [],
      options: [],
    },
    {
      name: 'IsBusy',
      description: 'Returns bool indicating whether any of the axes is executing a motion command.',
      route: 'axes/is_busy',
      request: 'AxesEmptyRequest',
      response: 'BoolResponse',
      args: genArgs(),
      params: [],
      options: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if any of the axes is currently executing a motion command. False otherwise.',
    },
    {
      name: 'IsHomed',
      description: 'Returns bool indicating whether all the axes are homed.',
      route: 'axes/is_homed',
      request: 'AxesEmptyRequest',
      response: 'BoolResponse',
      args: genArgs(),
      params: [],
      options: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if all the axes are homed. False otherwise.',
    },
    {
      name: 'GetPosition',
      description: [
        'Returns current axes position.',
        'The positions are requested sequentially.',
        'The result position may not be accurate if the axes are moving.',
      ],
      route: 'axes/get_setting',
      request: 'AxesGetSettingRequest',
      response: 'DoubleArrayResponse',
      args: [
        ...genArgs(),
        {
          name: 'Setting',
          value: {
            py: '"pos"',
            cs: '"pos"',
            ts: '\'pos\'',
            java: '"pos"',
            cpp: '"pos"',
            swift: '"pos"',
          }
        },
      ],
      params: [],
      variadic: {
        name: 'unit', type: { type: 'array', of: 'LengthUnits' },
        description: [
          'Units of position. You can specify units once or for each axis separately.',
        ],
      },
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'Axes position.',
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the axes.',
      route: 'axes/to_string',
      request: 'AxesEmptyRequest',
      response: 'StringResponse',
      args: genArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the axes.',
    },
  ],
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'constructor',
    source: {
      namespace: 'ascii',
      class: 'AxisGroup',
    },
    args: [{
      class: 'Axis',
      namespace: 'ascii',
    }],
  }],
};

export default definition;
