import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { defaultEmptyArray, defaultUnits, literalBool, unitsValue } from './helpers';

const deviceIoArgs: MethodArg[] = [
  {
    name: 'InterfaceId',
    value: {
      cs: '_device.Connection.InterfaceId',
      py: 'self._device.connection.interface_id',
      ts: 'this._device.connection.interfaceId',
      java: 'this.device.getConnection().getInterfaceId()',
      cpp: 'this->_device.getConnection().getInterfaceId()',
      swift: 'self.device.connection.interfaceId',
    }
  },
  {
    name: 'Device',
    value: {
      cs: '_device.DeviceAddress',
      py: 'self._device.device_address',
      ts: 'this._device.deviceAddress',
      java: 'this.device.getDeviceAddress()',
      cpp: 'this->_device.getDeviceAddress()',
      swift: 'self.device.deviceAddress',
    }
  },
];

const digitalInput: MethodArg = {
  name: 'ChannelType',
  value: {
    cs: '"di"',
    py: '"di"',
    ts: '\'di\'',
    java: '"di"',
    cpp: '"di"',
    swift: '"di"',
  }
};

const digitalOutput: MethodArg = {
  name: 'ChannelType',
  value: {
    cs: '"do"',
    py: '"do"',
    ts: '\'do\'',
    java: '"do"',
    cpp: '"do"',
    swift: '"do"',
  }
};

const analogInput: MethodArg = {
  name: 'ChannelType',
  value: {
    cs: '"ai"',
    py: '"ai"',
    ts: '\'ai\'',
    java: '"ai"',
    cpp: '"ai"',
    swift: '"ai"',
  }
};

const analogOutput: MethodArg = {
  name: 'ChannelType',
  value: {
    cs: '"ao"',
    py: '"ao"',
    ts: '\'ao\'',
    java: '"ao"',
    cpp: '"ao"',
    swift: '"ao"',
  }
};

const minVersionDigitalSchedule = { major: 7, minor: 37 };
const minVersionAnalogSchedule = { major: 7, minor: 38 };

const scheduleParamsCheck = [
  {
    code: {
      cs: 'delay <= 0',
      py: 'delay <= 0',
      ts: 'delay <= 0',
      java: 'delay <= 0',
      cpp: 'delay <= 0',
      swift: 'delay > 0',
    },
    message: 'Delay must be a positive value.'
  }
];

export const outputFunctions: Record<string, MethodDefinition> = {
  setDigitalOutput: {
    name: 'SetDigitalOutput',
    description: 'Sets value for the specified digital output channel.',
    route: 'device/set_digital_output',
    request: 'DeviceSetDigitalOutputRequest',
    args: deviceIoArgs,
    params: [
      {
        name: 'ChannelNumber',
        type: 'int',
        description: 'Channel number starting at 1.',
      },
      {
        name: 'Value',
        type: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' },
        description: 'The type of action to perform on the channel.',
      },
    ],
  },
  setAllDigitalOutputs: {
    name: 'SetAllDigitalOutputs',
    description: 'Sets values for all digital output channels.',
    route: 'device/set_all_digital_outputs',
    request: 'DeviceSetAllDigitalOutputsRequest',
    args: deviceIoArgs,
    params: [
      {
        name: 'Values',
        type: { type: 'array', of: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' } },
        description: 'The type of action to perform on the channel.',
      },
    ],
  },
  setDigitalOutputSchedule: {
    name: 'SetDigitalOutputSchedule',
    description: 'Sets current and future value for the specified digital output channel.',
    route: 'device/set_digital_output_schedule',
    request: 'DeviceSetDigitalOutputScheduleRequest',
    minFw: [minVersionDigitalSchedule],
    args: deviceIoArgs,
    params: [
      {
        name: 'ChannelNumber',
        type: 'int',
        description: 'Channel number starting at 1.',
      },
      {
        name: 'Value',
        type: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' },
        description: 'The type of action to perform immediately on the channel.',
      },
      {
        name: 'FutureValue',
        type: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' },
        description: 'The type of action to perform in the future on the channel.',
      },
      {
        name: 'Delay',
        type: 'double',
        description: 'Delay between setting current value and setting future value.',
      },
      {
        name: 'Unit',
        type: 'TimeUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of time.',
      },
    ],
    paramsCheck: scheduleParamsCheck,
  },
  setAllDigitalOutputsSchedule: {
    name: 'SetAllDigitalOutputsSchedule',
    description: 'Sets current and future values for all digital output channels.',
    route: 'device/set_all_digital_outputs_schedule',
    request: 'DeviceSetAllDigitalOutputsScheduleRequest',
    minFw: [minVersionDigitalSchedule],
    args: deviceIoArgs,
    params: [
      {
        name: 'Values',
        type: { type: 'array', of: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' } },
        description: 'The type of actions to perform immediately on output channels.',
      },
      {
        name: 'FutureValues',
        type: { type: 'array', of: { type: 'enum', name: 'DigitalOutputAction', namespace: 'ascii' } },
        description: 'The type of actions to perform in the future on output channels.',
      },
      {
        name: 'Delay',
        type: 'double',
        description: 'Delay between setting current values and setting future values.',
      },
      {
        name: 'Unit',
        type: 'TimeUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of time.',
      },
    ],
    paramsCheck: scheduleParamsCheck,
  },
  setAnalogOutput: {
    name: 'SetAnalogOutput',
    description: 'Sets value for the specified analog output channel.',
    route: 'device/set_analog_output',
    request: 'DeviceSetAnalogOutputRequest',
    args: deviceIoArgs,
    params: [
      {
        name: 'ChannelNumber', type: 'int',
        description: 'Channel number starting at 1.',
      },
      {
        name: 'Value', type: 'double',
        description: 'Value to set the output channel voltage to.',
      },
    ],
  },
  setAllAnalogOutputs: {
    name: 'SetAllAnalogOutputs',
    description: 'Sets values for all analog output channels.',
    route: 'device/set_all_analog_outputs',
    request: 'DeviceSetAllAnalogOutputsRequest',
    args: deviceIoArgs,
    params: [
      {
        name: 'Values', type: { type: 'array', of: 'double' },
        description: 'Voltage values to set the output channels to.',
      },
    ],
  },
  setAnalogOutputSchedule: {
    name: 'SetAnalogOutputSchedule',
    description: 'Sets current and future value for the specified analog output channel.',
    route: 'device/set_analog_output_schedule',
    request: 'DeviceSetAnalogOutputScheduleRequest',
    minFw: [minVersionAnalogSchedule],
    args: deviceIoArgs,
    params: [
      {
        name: 'ChannelNumber',
        type: 'int',
        description: 'Channel number starting at 1.',
      },
      {
        name: 'Value',
        type: 'double',
        description: 'Value to set the output channel voltage to immediately.',
      },
      {
        name: 'FutureValue',
        type: 'double',
        description: 'Value to set the output channel voltage to in the future.',
      },
      {
        name: 'Delay',
        type: 'double',
        description: 'Delay between setting current value and setting future value.',
      },
      {
        name: 'Unit',
        type: 'TimeUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of time.',
      },
    ],
    paramsCheck: scheduleParamsCheck,
  },
  setAllAnalogOutputsSchedule: {
    name: 'SetAllAnalogOutputsSchedule',
    description: 'Sets current and future values for all analog output channels.',
    route: 'device/set_all_analog_outputs_schedule',
    request: 'DeviceSetAllAnalogOutputsScheduleRequest',
    minFw: [minVersionAnalogSchedule],
    args: deviceIoArgs,
    params: [
      {
        name: 'Values',
        type: { type: 'array', of: 'double' },
        description: 'Voltage values to set the output channels to immediately.',
      },
      {
        name: 'FutureValues',
        type: { type: 'array', of: 'double' },
        description: 'Voltage values to set the output channels to in the future.',
      },
      {
        name: 'Delay',
        type: 'double',
        description: 'Delay between setting current values and setting future values.',
      },
      {
        name: 'Unit',
        type: 'TimeUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of time.',
      },
    ],
    paramsCheck: scheduleParamsCheck,
  },
  cancelDigitalOutputSchedule: {
    name: 'CancelDigitalOutputSchedule',
    description: 'Cancels a scheduled digital output action.',
    route: 'device/cancel_output_schedule',
    request: 'DeviceCancelOutputScheduleRequest',
    minFw: [minVersionDigitalSchedule],
    args: [
      ...deviceIoArgs,
      { name: 'analog', value: literalBool(false) }
    ],
    params: [
      {
        name: 'ChannelNumber',
        type: 'int',
        description: 'Channel number starting at 1.',
      },
    ],
  },
  cancelAllDigitalOutputsSchedule: {
    name: 'CancelAllDigitalOutputsSchedule',
    description: 'Cancel all scheduled digital output actions.',
    route: 'device/cancel_all_outputs_schedule',
    request: 'DeviceCancelAllOutputsScheduleRequest',
    minFw: [minVersionDigitalSchedule],
    args: [
      ...deviceIoArgs,
      { name: 'analog', value: literalBool(false) }
    ],
    params: [
      {
        name: 'Channels',
        type: { type: 'array', of: 'bool' },
        defaultValue: defaultEmptyArray('boolean'),
        description: [
          'Optionally specify which channels to cancel.',
          'Array length must be empty or equal to the number of channels on device.',
          'Specifying "True" for a channel will cancel the scheduled digital output action for that channel.',
        ]
      },
    ],
  },
  cancelAnalogOutputSchedule: {
    name: 'CancelAnalogOutputSchedule',
    description: 'Cancels a scheduled analog output value.',
    route: 'device/cancel_output_schedule',
    request: 'DeviceCancelOutputScheduleRequest',
    minFw: [minVersionAnalogSchedule],
    args: [
      ...deviceIoArgs,
      { name: 'analog', value: literalBool(true) }
    ],
    params: [
      {
        name: 'ChannelNumber',
        type: 'int',
        description: 'Channel number starting at 1.',
      },
    ],
  },
  cancelAllAnalogOutputsSchedule: {
    name: 'CancelAllAnalogOutputsSchedule',
    description: 'Cancel all scheduled analog output actions.',
    route: 'device/cancel_all_outputs_schedule',
    request: 'DeviceCancelAllOutputsScheduleRequest',
    minFw: [minVersionAnalogSchedule],
    args: [
      ...deviceIoArgs,
      { name: 'analog', value: literalBool(true) }
    ],
    params: [
      {
        name: 'Channels',
        type: { type: 'array', of: 'bool' },
        defaultValue: defaultEmptyArray('boolean'),
        description: [
          'Optionally specify which channels to cancel.',
          'Array length must be empty or equal to the number of channels on device.',
          'Specifying "True" for a channel will cancel the scheduled analog output value for that channel.',
        ]
      },
    ],
  },
  setLowpassFilter: {
    name: 'SetAnalogInputLowpassFilter',
    description: [
      'Sets the cutoff frequency of the low-pass filter for the specified analog input channel.',
      'Set the frequency to 0 to disable the filter.',
    ],
    route: 'device/set_lowpass_filter',
    request: 'DeviceSetLowpassFilterRequest',
    args: deviceIoArgs,
    params: [
      {
        name: 'ChannelNumber', type: 'int',
        description: 'Channel number starting at 1.',
      },
      {
        name: 'CutoffFrequency', type: 'double',
        description: 'Cutoff frequency of the low-pass filter.',
      },
      {
        name: 'Unit', type: 'FrequencyUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of frequency.',
      }
    ],
  },
};

const definition: ClassDefinition = {
  name: 'DeviceIO',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
  ],
  description: 'Class providing access to the I/O channels of the device.',
  ctor: {},
  functions: [
    {
      name: 'GetChannelsInfo',
      description: 'Returns the number of I/O channels the device has.',
      route: 'device/get_io_info',
      request: 'DeviceEmptyRequest',
      args: deviceIoArgs,
      params: [],
      returnType: { type: 'ref', name: 'DeviceIOInfo', namespace: 'ascii' },
      returnDescription: 'An object containing the number of I/O channels the device has.',
    },
    {
      name: 'SetLabel',
      description: 'Sets the label of the specified channel.',
      route: 'device/set_io_label',
      request: 'SetIoPortLabelRequest',
      args: deviceIoArgs,
      params: [
        {
          name: 'PortType',
          type: { type: 'enum', name: 'IoPortType', namespace: 'ascii' },
          description: 'The type of channel to set the label of.',
        },
        {
          name: 'ChannelNumber',
          type: 'int',
          description: 'Channel number starting at 1.',
        },
        {
          name: 'Label',
          type: { type: 'optional', of: 'string' },
          description: [
            'The label to set for the specified channel.',
            'If no value or an empty string is provided, this label is deleted.'
          ],
        },
      ],
    },
    {
      name: 'GetLabel',
      description: 'Returns the label of the specified channel.',
      route: 'device/get_io_label',
      request: 'GetIoPortLabelRequest',
      response: 'StringResponse',
      args: deviceIoArgs,
      params: [
        {
          name: 'PortType',
          type: { type: 'enum', name: 'IoPortType', namespace: 'ascii' },
          description: 'The type of channel to get the label of.',
        },
        {
          name: 'ChannelNumber',
          type: 'int',
          description: 'Channel number starting at 1.',
        },
      ],
      returnType: 'string',
      returnDescription: 'The label of the specified channel.',
      returnPropertyName: 'Value',
    },
    {
      name: 'GetAllLabels',
      description: 'Returns every label assigned to an IO port on this device.',
      route: 'device/get_all_io_labels',
      request: 'DeviceEmptyRequest',
      response: 'GetAllIoPortLabelsResponse',
      args: deviceIoArgs,
      params: [],
      returnType: { type: 'array', of: { type: 'ref', name: 'IoPortLabel', namespace: 'ascii' } },
      returnDescription: 'The labels set for this device\'s IO.',
      returnPropertyName: 'Labels',
    },
    {
      name: 'GetDigitalInput',
      description: 'Returns the current value of the specified digital input channel.',
      route: 'device/get_digital_io',
      request: 'DeviceGetDigitalIORequest',
      response: 'BoolResponse',
      args: [
        ...deviceIoArgs,
        digitalInput,
      ],
      params: [
        {
          name: 'ChannelNumber', type: 'int',
          description: 'Channel number starting at 1.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if voltage is present on the input channel and false otherwise.',
    },
    {
      name: 'GetDigitalOutput',
      description: 'Returns the current value of the specified digital output channel.',
      route: 'device/get_digital_io',
      request: 'DeviceGetDigitalIORequest',
      response: 'BoolResponse',
      args: [
        ...deviceIoArgs,
        digitalOutput,
      ],
      params: [
        {
          name: 'ChannelNumber', type: 'int',
          description: 'Channel number starting at 1.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the output channel is conducting and false otherwise.',
    },
    {
      name: 'GetAnalogInput',
      description: 'Returns the current value of the specified analog input channel.',
      route: 'device/get_analog_io',
      request: 'DeviceGetAnalogIORequest',
      response: 'DoubleResponse',
      args: [
        ...deviceIoArgs,
        analogInput,
      ],
      params: [
        {
          name: 'ChannelNumber', type: 'int',
          description: 'Channel number starting at 1.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: ' A measurement of the voltage present on the input channel.',
    },
    {
      name: 'GetAnalogOutput',
      description: 'Returns the current values of the specified analog output channel.',
      route: 'device/get_analog_io',
      request: 'DeviceGetAnalogIORequest',
      response: 'DoubleResponse',
      args: [
        ...deviceIoArgs,
        analogOutput,
      ],
      params: [
        {
          name: 'ChannelNumber', type: 'int',
          description: 'Channel number starting at 1.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'A measurement of voltage that the output channel is conducting.',
    },
    {
      name: 'GetAllDigitalInputs',
      description: 'Returns the current values of all digital input channels.',
      route: 'device/get_all_digital_io',
      request: 'DeviceGetAllDigitalIORequest',
      response: 'DeviceGetAllDigitalIOResponse',
      args: [
        ...deviceIoArgs,
        digitalInput,
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'bool' },
      returnDescription: 'True if voltage is present on the input channel and false otherwise.',
    },
    {
      name: 'GetAllDigitalOutputs',
      description: 'Returns the current values of all digital output channels.',
      route: 'device/get_all_digital_io',
      request: 'DeviceGetAllDigitalIORequest',
      response: 'DeviceGetAllDigitalIOResponse',
      args: [
        ...deviceIoArgs,
        digitalOutput,
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'bool' },
      returnDescription: 'True if the output channel is conducting and false otherwise.',
    },
    {
      name: 'GetAllAnalogInputs',
      description: 'Returns the current values of all analog input channels.',
      route: 'device/get_all_analog_io',
      request: 'DeviceGetAllAnalogIORequest',
      response: 'DeviceGetAllAnalogIOResponse',
      args: [
        ...deviceIoArgs,
        analogInput,
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'Measurements of the voltages present on the input channels.',
    },
    {
      name: 'GetAllAnalogOutputs',
      description: 'Returns the current values of all analog output channels.',
      route: 'device/get_all_analog_io',
      request: 'DeviceGetAllAnalogIORequest',
      response: 'DeviceGetAllAnalogIOResponse',
      args: [
        ...deviceIoArgs,
        analogOutput,
      ],
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'Measurements of voltage that the output channels are conducting.',
    },
    outputFunctions.setDigitalOutput,
    outputFunctions.setAllDigitalOutputs,
    outputFunctions.setDigitalOutputSchedule,
    outputFunctions.setAllDigitalOutputsSchedule,
    outputFunctions.setAnalogOutput,
    outputFunctions.setAllAnalogOutputs,
    outputFunctions.setAnalogOutputSchedule,
    outputFunctions.setAllAnalogOutputsSchedule,
    outputFunctions.setLowpassFilter,
    outputFunctions.cancelDigitalOutputSchedule,
    outputFunctions.cancelAllDigitalOutputsSchedule,
    outputFunctions.cancelAnalogOutputSchedule,
    outputFunctions.cancelAllAnalogOutputsSchedule,
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'IO',
    },
  }],
};

export default definition;
