import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { makeCppConnections } from './connection';

function genConnectionArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'InterfaceId',
    },
  ];
}

const openConnectionBase: Partial<MethodDefinition> = {
  static: true,
  route: 'binary/interface/open',
  request: 'OpenBinaryInterfaceRequest',
  response: 'OpenInterfaceResponse',
  customReturn: {
    cs: 'new Connection(response.InterfaceId)',
    py: 'Connection(response.interface_id)',
    ts: 'new Connection(response.interfaceId)',
    java: 'new Connection(r.getInterfaceId())',
    cpp: '{response.getInterfaceId()}',
    swift: 'Connection(interfaceId: response.interfaceId)',
  },
  returnType: { type: 'ref', name: 'Connection', namespace: 'binary' },
  py: {
    asyncContextManager: {
      name: 'AsyncBinaryConnectionOpener',
      description: 'Provides a connection in an asynchronous context using `await` or `async with`',
      closer: 'close_async',
    },
  },
};

const definition: ClassDefinition = {
  name: 'Connection',
  namespace: 'binary',
  constants: [{
    name: 'DefaultBaudRate',
    description: 'Default baud rate for serial connections.',
    type: 'int',
    value: '9600',
  }],
  properties: [
    {
      name: 'InterfaceId',
      description: 'The interface ID identifies thisConnection instance with the underlying library.',
      type: 'int',
    },
  ],
  events: {
    unknownResponse: {
      name: 'UnknownResponse',
      description: 'Event invoked when a response from a device cannot be matched to any known request.',
      emitType: { type: 'ref', name: 'UnknownResponseEvent', namespace: 'binary' },
      emitTypeDescription: 'Reply that could not be matched to a request.'
    },
    replyOnly: {
      name: 'ReplyOnly',
      description: 'Event invoked when a reply-only command such as a move tracking message is received from a device.',
      emitType: { type: 'ref', name: 'ReplyOnlyEvent', namespace: 'binary' },
      emitTypeDescription: 'Reply-only message received from the device.'
    },
    disconnected: {
      name: 'Disconnected',
      description: 'Event invoked when connection is interrupted or closed.',
      emitType: { type: 'ref', name: 'MotionLibException', namespace: 'exceptions' },
      emitTypeDescription: 'Error that caused disconnection.'
    },
  },
  description: 'Class representing access to particular connection (serial port, TCP connection) using the legacy Binary protocol.',
  ctor: {
    code: {
      cs: 'Subscribe();',
      py: 'self.__setup_events()',
      ts: 'this._subscribe();',
      java: 'this.subscribe();',
      cpp: 'this->subscribe();',
      swift: 'ensureSubscribed()'
    }
  },
  functions: [
    {
      name: 'OpenSerialPort',
      description: 'Opens a serial port.',
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.SerialPort',
          py: 'dto.InterfaceType.SERIAL_PORT',
          ts: 'requests.InterfaceType.SERIAL_PORT',
          java: 'InterfaceType.SERIAL_PORT',
          cpp: '::zaber::motion::requests::InterfaceType::SERIAL_PORT',
          swift: 'DtoRequests.InterfaceType.serialPort',
        }
      }],
      params: [
        { name: 'portName', type: 'string', description: 'Name of the port to open.' },
      ],
      options: [
        {
          name: 'baudRate', type: 'int',
          defaultValue: {
            all: 'DEFAULT_BAUD_RATE',
            cs: 'DefaultBaudRate',
            ts: 'Connection.DEFAULT_BAUD_RATE',
            swift: 'Connection.defaultBaudRate',
          },
          description: 'Optional baud rate (defaults to 9600).'
        },
        {
          name: 'useMessageIds', type: 'bool', defaultValue: { all: 'false', py: 'False' },
          description: [
            'Enable use of message IDs (defaults to disabled).',
            'All your devices must be pre-configured to match.',
          ],
        },
      ],
      returnDescription: 'An object representing the port.',
      ...openConnectionBase,
    },
    {
      name: 'OpenTcp',
      description: 'Opens a TCP connection.',
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.Tcp',
          py: 'dto.InterfaceType.TCP',
          ts: 'requests.InterfaceType.TCP',
          java: 'InterfaceType.TCP',
          cpp: '::zaber::motion::requests::InterfaceType::TCP',
          swift: 'DtoRequests.InterfaceType.tcp',
        }
      }],
      params: [
        { name: 'hostName', type: 'string', description: 'Hostname or IP address.' },
        { name: 'port', type: 'int', description: 'Port number.' },
      ],
      options: [
        {
          name: 'useMessageIds', type: 'bool', defaultValue: { all: 'false', py: 'False' },
          description: [
            'Enable use of message IDs (defaults to disabled).',
            'All your devices must be pre-configured to match.',
          ],
        },
      ],
      returnDescription: 'An object representing the connection.',
      ...openConnectionBase,
    },
    {
      name: 'Close',
      description: 'Close the connection.',
      route: 'interface/close',
      request: 'InterfaceEmptyRequest',
      args: genConnectionArgs(),
      params: [],
    },
    {
      name: 'GenericCommand',
      description: [
        'Sends a generic Binary command to this connection.',
        'For more information please refer to the',
        '[Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).'
      ],
      route: 'binary/interface/generic_command',
      request: 'GenericBinaryRequest',
      returnType: { type: 'ref', name: 'Message', namespace: 'binary' },
      returnDescription: 'A response to the command.',
      args: genConnectionArgs(),
      params: [
        { name: 'device', type: 'int', description: 'Device address to send the command to. Use zero for broadcast.' },
        { name: 'command', type: { type: 'enum', name: 'CommandCode', namespace: 'binary' }, description: 'Command to send.' },
        { name: 'data', type: 'int', defaultValue: '0', description: 'Optional data argument to the command. Defaults to zero.' },
      ],
      options: [
        {
          name: 'timeout', type: 'double', defaultValue: '0.0',
          description: 'Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.'
        },
        {
          name: 'checkErrors', type: 'bool', defaultValue: { all: 'true', py: 'True' },
          description: 'Controls whether to throw an exception when the device rejects the command.'
        },
      ],
    },
    {
      name: 'GenericCommandNoResponse',
      description: [
        'Sends a generic Binary command to this connection without expecting a response.',
        'For more information please refer to the',
        '[Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).'
      ],
      route: 'binary/interface/generic_command_no_response',
      request: 'GenericBinaryRequest',
      args: genConnectionArgs(),
      params: [
        { name: 'device', type: 'int', description: 'Device address to send the command to. Use zero for broadcast.' },
        { name: 'command', type: { type: 'enum', name: 'CommandCode', namespace: 'binary' }, description: 'Command to send.' },
        { name: 'data', type: 'int', defaultValue: '0', description: 'Optional data argument to the command. Defaults to zero.' },
      ],
    },
    {
      name: 'GenericCommandMultiResponse',
      description: [
        'Sends a generic Binary command to this connection and expects responses from one or more devices.',
        'Responses are returned in order of arrival.',
        'For more information please refer to the',
        '[Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).'
      ],
      route: 'binary/interface/generic_command_multi_response',
      request: 'GenericBinaryRequest',
      response: 'BinaryMessageCollection',
      returnPropertyName: 'Messages',
      returnType: { type: 'array', of: { type: 'ref', name: 'Message', namespace: 'binary' } },
      returnDescription: 'All responses to the command.',
      args: genConnectionArgs(),
      params: [
        { name: 'command', type: { type: 'enum', name: 'CommandCode', namespace: 'binary' }, description: 'Command to send.' },
        { name: 'data', type: 'int', defaultValue: '0', description: 'Optional data argument to the command. Defaults to zero.' },
      ],
      options: [
        {
          name: 'timeout', type: 'double', defaultValue: '0.0',
          description: 'Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.'
        },
        {
          name: 'checkErrors', type: 'bool', defaultValue: { all: 'true', py: 'True' },
          description: 'Controls whether to throw an exception when any device rejects the command.'
        },
      ],
    },
    {
      name: 'RenumberDevices',
      description: 'Renumbers devices present on this connection. After renumbering, you must identify devices again.',
      route: 'binary/device/renumber',
      request: 'InterfaceEmptyRequest',
      args: genConnectionArgs(),
      params: [],
      paramsCheck: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      response: 'IntResponse',
      returnDescription: 'Total number of devices that responded to the renumber.',
    },
    {
      name: 'DetectDevices',
      description: 'Attempts to detect any devices present on this connection.',
      route: 'binary/device/detect',
      request: 'BinaryDeviceDetectRequest',
      args: genConnectionArgs(),
      params: [],
      options: [
        {
          name: 'identifyDevices', type: 'bool', defaultValue: { all: 'true', py: 'True' },
          description: 'Determines whether device identification should be performed as well.'
        },
      ],
      customReturn: {
        cs: 'response.Devices.Select(device => GetDevice(device)).ToArray()',
        py: 'list(map(self.get_device, response.devices))',
        ts: 'response.devices.map(deviceAddress => this.getDevice(deviceAddress))',
        java: 'ArrayUtility.arrayFromInt(Device[]::new, r.getDevices(), address -> this.getDevice(address))',
        cpp: 'zml_util::map_vec<int,Device>(response.getDevices(), [this](const int& i) { return Device(*this, i); })',
        swift: 'try response.devices.map { try getDevice(deviceAddress: $0) }',
      },
      returnType: { type: 'array', of: { type: 'ref', name: 'Device', namespace: 'binary' } },
      response: 'BinaryDeviceDetectResponse',
      returnDescription: 'Array of detected devices.',
    },
    {
      name: 'GetDevice',
      description: [
        'Gets a Device class instance which allows you to control a particular device on this connection.',
        'Devices are numbered from 1.',
      ],
      sync: true,
      params: [
        {
          name: 'deviceAddress',
          type: 'int',
          description: 'Address of device intended to control. Address is configured for each device.'
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'deviceAddress <= 0',
            py: 'device_address <= 0',
            ts: 'deviceAddress <= 0',
            java: 'deviceAddress <= 0',
            cpp: 'deviceAddress <= 0',
            swift: 'deviceAddress > 0',
          },
          message: 'Invalid value; physical devices are numbered from 1.',
        }
      ],
      code: {
        cs: 'return new Device(this, deviceAddress);',
        py: 'return Device(self, device_address)',
        ts: 'return new Device(this, deviceAddress);',
        java: 'return new Device(this, deviceAddress);',
        cpp: 'return { *this, deviceAddress };',
        swift: 'return Device(connection: self, deviceAddress: deviceAddress)',
      },
      returnType: { type: 'ref', name: 'Device', namespace: 'binary' },
      returnDescription: 'Device instance.',
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the connection.',
      route: 'interface/to_string',
      request: 'InterfaceEmptyRequest',
      response: 'StringResponse',
      args: genConnectionArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the connection.',
    },
  ],
  py: {
    forwardTypes: ['AsyncBinaryConnectionOpener'],
  }
};

export const connection = definition;
export const cppConnections = makeCppConnections(definition);
