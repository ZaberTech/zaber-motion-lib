import { ClassDefinition, MethodArg } from '../types';

import { defaultUnits, unitsValue } from './helpers';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: {
        cs: '_device.Connection.InterfaceId',
        py: 'self._device.connection.interface_id',
        ts: 'this._device.connection.interfaceId',
        java: 'this.device.getConnection().getInterfaceId()',
        cpp: 'this->_device.getConnection().getInterfaceId()',
        swift: 'device.connection.interfaceId',
      }
    },
    {
      name: 'Device',
      value: {
        cs: '_device.DeviceAddress',
        py: 'self._device.device_address',
        ts: 'this._device.deviceAddress',
        java: 'this.device.getDeviceAddress()',
        cpp: 'this->_device.getDeviceAddress()',
        swift: 'device.deviceAddress',
      }
    },
  ];
}

const definition: ClassDefinition = {
  name: 'DeviceSettings',
  namespace: 'binary',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'binary' },
      private: true,
    },
  ],
  description: 'Class providing access to various device settings and properties.',
  ctor: { },
  functions: [
    {
      name: 'Get',
      description: 'Returns any device setting or property.',
      route: 'binary/device/get_setting',
      request: 'BinaryDeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: genArgs(),
      params: [
        {
          name: 'setting', type: { type: 'enum', name: 'BinarySettings', namespace: 'binary' },
          description: 'Setting to get.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Setting value.',
    },
    {
      name: 'Set',
      description: 'Sets any device setting.',
      route: 'binary/device/set_setting',
      request: 'BinaryDeviceSetSettingRequest',
      args: genArgs(),
      params: [
        {
          name: 'setting', type: { type: 'enum', name: 'BinarySettings', namespace: 'binary' },
          description: 'Setting to set.',
        },
        {
          name: 'value', type: 'double',
          description: 'Value of the setting.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of setting.',
        },
      ],
    },
  ],
  py: {
    forwardTypes: ['Device']
  }
};

export default definition;
