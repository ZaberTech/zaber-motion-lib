import _ from 'lodash';

import { ClassDefinition } from '../types';

import { defaultBool } from './helpers';
import { toStringDevice } from './to_string';

const DEVICE_ARGS = [
  { name: 'InterfaceId', value: 'Device.Connection.InterfaceId' },
  { name: 'Device', value: 'Device.DeviceAddress' },
];

const definition: ClassDefinition = {
  name: 'ProcessController',
  namespace: 'product',
  description: 'Use to manage a process controller.',
  minFw: [{ major: 7, minor: 35 }],
  properties: [
    {
      name: 'Device',
      description: 'The base device of this process controller.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
  ],
  ctor: {
    public: true,
    description: [
      'Creates instance of `ProcessController` of the given device.',
      'If the device is identified, this constructor will ensure it is a process controller.'
    ],
    code: {
      cs: 'VerifyIsProcessController();',
      py: 'self.__verify_is_process_controller()',
      ts: 'this._verifyIsProcessController();',
      java: 'this.verifyIsProcessController();',
      cpp: 'this->verifyIsProcessController();',
      swift: 'try verifyIsProcessController()',
    },
    canThrow: true,
  },
  functions: [
    {
      name: 'Detect',
      static: true,
      description: 'Detects the process controllers on the connection.',
      route: 'device/detect',
      request: 'DeviceDetectRequest',
      response: 'DeviceDetectResponse',
      params: [{
        name: 'Connection',  requestName: 'InterfaceId',
        type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
        description: 'The connection to detect process controllers on.',
        value: {
          cs: 'connection.InterfaceId',
          py: 'connection.interface_id',
          ts: 'connection.interfaceId',
          java: 'connection.getInterfaceId()',
          cpp: 'connection.getInterfaceId()',
          swift: 'connection.interfaceId',
        }
      }, {
        name: 'Identify', type: 'bool', requestName: 'IdentifyDevices',
        description: 'If the Process Controllers should be identified upon detection.',
        defaultValue: defaultBool(true),
      }],
      args: [{
        name: 'Type',
        value: {
          cs: 'Requests.DeviceType.ProcessController',
          py: 'dto.DeviceType.PROCESS_CONTROLLER',
          ts: 'requests.DeviceType.PROCESS_CONTROLLER',
          java: 'DeviceType.PROCESS_CONTROLLER',
          cpp: '::zaber::motion::requests::DeviceType::PROCESS_CONTROLLER',
          swift: 'DeviceType.processController',
        }
      }],
      returnType: { type: 'array', of: { type: 'ref', name: 'ProcessController', namespace: 'product' } },
      returnDescription: 'A list of all `ProcessController`s on the connection.',
      customReturn: {
        cs: 'response.Devices.Select(device => new ProcessController(connection.GetDevice(device))).ToArray()',
        py: '[ProcessController(connection.get_device(device)) for device in response.devices]',
        ts: 'response.devices.map(deviceAddress => new ProcessController(connection.getDevice(deviceAddress)))',
        java: [
          'ArrayUtility.arrayFromInt(ProcessController[]::new, r.getDevices(),',
          `${_.repeat(' ', 16)}address -> new ProcessController(connection.getDevice(address)))`,
        ].join('\n'),
        cpp: [
          'zml_util::map_vec<int,ProcessController>(response.getDevices(), [connection](const int& adr) {',
          '        return ProcessController(Device(connection, adr));',
          '    })',
        ].join('\n'),
        swift: 'try response.devices.map { try ProcessController(device: Device(connection: connection, deviceAddress: $0)) }',
      },
    },
    {
      name: 'GetProcess',
      description: [
        'Gets an Process class instance which allows you to control a particular voltage source.',
        'Axes are numbered from 1.',
      ],
      sync: true,
      params: [
        { name: 'ProcessNumber', type: 'int', description: 'Number of process to control.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'processNumber <= 0',
            py: 'process_number <= 0',
            ts: 'processNumber <= 0',
            java: 'processNumber <= 0',
            cpp: 'processNumber <= 0',
            swift: 'processNumber > 0',
          },
          message: 'Invalid value; processes are numbered from 1.',
        }
      ],
      code: {
        cs: 'return new Process(this, processNumber);',
        py: 'return Process(self, process_number)',
        ts: 'return new Process(this, processNumber);',
        java: 'return new Process(this, processNumber);',
        cpp: 'return {*this, processNumber};',
        swift: 'return Process(controller: self, processNumber: processNumber)',
      },
      returnType: { type: 'ref', name: 'Process', namespace: 'product' },
      returnDescription: 'Process instance.',
    },
    {
      name: 'VerifyIsProcessController',
      private: true,
      sync: true,
      description: 'Checks if this is a process controller or some other type of device and throws an error if it is not.',
      params: [],
      args: [
        { name: 'InterfaceId', value: 'Device.Connection.InterfaceId' },
        { name: 'Device', value: 'Device.DeviceAddress' },
      ],
      route: 'process_controller/verify',
      request: 'DeviceEmptyRequest',
    },
    toStringDevice(DEVICE_ARGS),
  ],
  py: {
    forwardTypes: ['ProcessController']
  },
};

export default definition;
