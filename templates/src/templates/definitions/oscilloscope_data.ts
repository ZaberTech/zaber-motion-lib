import _ from 'lodash';

import { definitions as dto } from '../../dto/definitions';
import { ClassDefinition, MethodArg } from '../types';

import { defaultUnits, unitsValue } from './helpers';

const captureProperties = dto.classes.find(c => c.name === 'OscilloscopeCaptureProperties')!;

const ID_ARG: MethodArg = {
  name: 'DataId',
  value: 'DataId',
};

const definition: ClassDefinition = {
  name: 'OscilloscopeData',
  namespace: 'ascii',
  ctor: {
    code: {
      ts: 'registerForFinalization(this, OscilloscopeData._free.bind(null, dataId));',
    },
  },
  properties: [
    {
      name: 'DataId',
      description: 'Unique ID for this block of recorded data.',
      type: 'int',
    },
    ...captureProperties.properties
      .map(property => ({
        name: property.name,
        description: property.description!,
        type: property.type,
        getterCanThrow: true,
        getter: {
          cs: `RetrieveProperties().${property.name}`,
          py: `self.__retrieve_properties().${_.snakeCase(property.name)}`,
          ts: `this._retrieveProperties().${_.lowerFirst(property.name)}`,
          java: `this.retrieveProperties().get${property.name}()`,
          cpp: `this->retrieveProperties().get${property.name}()`,
          swift: `self.retrieveProperties().${_.lowerFirst(property.name)}`,
        }
      })),
  ],
  description: [
    'Contains a block of contiguous recorded data for one channel of the device\'s oscilloscope.',
  ],
  functions: [
    {
      name: 'GetTimebase',
      description: 'Get the sample interval that this data was recorded with.',
      route: 'oscilloscopedata/get_timebase',
      request: 'OscilloscopeDataGetRequest',
      response: 'DoubleResponse',
      args: [ID_ARG],
      params: [
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the timebase in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The timebase setting at the time the data was recorded.',
      sync: true,
    },
    {
      name: 'GetFrequency',
      description: 'Get the sampling frequency that this data was recorded with.',
      route: 'oscilloscopedata/get_frequency',
      request: 'OscilloscopeDataGetRequest',
      response: 'DoubleResponse',
      args: [ID_ARG],
      params: [
        {
          name: 'unit',
          type: 'FrequencyUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the frequency in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The frequency (inverse of the timebase setting) at the time the data was recorded.',
      sync: true,
    },
    {
      name: 'GetDelay',
      description: [
        'Get the user-specified time period between receipt of the start command and the first data point.',
        'Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.',
      ],
      route: 'oscilloscopedata/get_delay',
      request: 'OscilloscopeDataGetRequest',
      response: 'DoubleResponse',
      args: [ID_ARG],
      params: [
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the delay in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The delay setting at the time the data was recorded.',
      sync: true,
    },
    {
      name: 'GetSampleTime',
      description: 'Calculate the time a sample was recorded, relative to when the recording was triggered.',
      route: 'oscilloscopedata/get_sample_time',
      request: 'OscilloscopeDataGetSampleTimeRequest',
      response: 'DoubleResponse',
      args: [ID_ARG],
      params: [
        {
          name: 'index',
          type: 'int',
          description: '0-based index of the sample to calculate the time of.',
        },
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the calculated time in.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The calculated time offset of the data sample at the given index.',
      sync: true,
    },
    {
      name: 'GetSampleTimes',
      description: 'Calculate the time for all samples, relative to when the recording was triggered.',
      route: 'oscilloscopedata/get_sample_times',
      request: 'OscilloscopeDataGetSampleTimeRequest',
      response: 'DoubleArrayResponse',
      args: [ID_ARG],
      params: [
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit of measure to represent the calculated time in.',
        },
      ],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'The calculated time offsets of all data samples.',
      sync: true,
    },
    {
      name: 'GetData',
      description: ['Get the recorded data as an array of doubles, with optional unit conversion.',
        'Note that not all quantities can be unit converted.',
        'For example, digital I/O channels and pure numbers such as device mode settings have no units.'],
      route: 'oscilloscopedata/get_samples',
      request: 'OscilloscopeDataGetRequest',
      response: 'OscilloscopeDataGetSamplesResponse',
      args: [ID_ARG],
      params: [{
        name: 'Unit',
        type: 'Units',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Unit of measure to convert the data to.',
      }],
      returnPropertyName: 'Data',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'The recorded data for one oscilloscope channel, converted to the units specified.',
      sync: true,
    },
    {
      name: 'Free',
      description: 'Releases native resources of an oscilloscope data buffer.',
      route: 'oscilloscopedata/free',
      request: 'OscilloscopeDataIdentifier',
      args: [],
      params: [{
        name: 'dataId',
        type: 'int',
        description: 'The ID of the data buffer to delete.',
      }],
      sync: true,
      private: true,
      static: true,
    },
    {
      name: 'RetrieveProperties',
      description: 'Returns recording properties.',
      route: 'oscilloscopedata/get_properties',
      request: 'OscilloscopeDataIdentifier',
      args: [ID_ARG],
      params: [],
      returnType: { type: 'ref', name: 'OscilloscopeCaptureProperties', namespace: 'ascii' },
      returnDescription: 'Capture properties.',
      sync: true,
      private: true,
    },
  ],
  cpp: {
    noCopyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Oscilloscope',
      member: 'Read',
    },
  }],
};

export default definition;
