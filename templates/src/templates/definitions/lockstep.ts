import { ClassDefinition, MethodArg } from '../types';

import { additionalMoveArgs, moveSinFunctions } from './axis';
import { defaultUnits, unitsValue } from './helpers';

function genLockstepArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
    {
      name: 'LockstepGroupId',
      value: 'LockstepGroupId',
    },
  ];
}

const definition: ClassDefinition = {
  name: 'Lockstep',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that controls this lockstep group.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
    {
      name: 'LockstepGroupId',
      description: 'The number that identifies the lockstep group on the device.',
      type: 'int',
    },
  ],
  description: [
    'Represents a lockstep group with this ID on a device.',
    'A lockstep group is a movement synchronized pair of axes on a device.',
  ],
  minFw: [{ major: 6, minor: 15 }, { major: 7, minor: 11 }],
  ctor: { },
  functions: [
    {
      name: 'Enable',
      description: 'Activate the lockstep group on the axes specified.',
      route: 'device/lockstep_enable',
      request: 'LockstepEnableRequest',
      args: genLockstepArgs(),
      params: [],
      variadic: {
        name: 'Axes',
        type: { type: 'array', of: 'int' },
        description: ['The numbers of axes in the lockstep group.'],
      },
    },
    {
      name: 'Disable',
      description: ['Disable the lockstep group.'],
      route: 'device/lockstep_disable',
      request: 'LockstepDisableRequest',
      args: genLockstepArgs(),
      params: [],
    },
    {
      name: 'Stop',
      description: 'Stops ongoing lockstep group movement. Decelerates until zero speed.',
      route: 'device/lockstep_stop',
      request: 'LockstepStopRequest',
      args: genLockstepArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
      ],
    },
    {
      name: 'Home',
      description: 'Retracts the axes of the lockstep group until a home associated with an individual axis is detected.',
      route: 'device/lockstep_home',
      request: 'LockstepHomeRequest',
      args: genLockstepArgs(),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
      ],
    },
    {
      name: 'MoveAbsolute',
      description: ['Move the first axis of the lockstep group to an absolute position.',
        'The other axes in the lockstep group maintain their offsets throughout movement.'],
      route: 'device/lockstep_move',
      request: 'LockstepMoveRequest',
      args: genLockstepArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Abs',
            py: 'dto.AxisMoveType.ABS',
            ts: 'requests.AxisMoveType.ABS',
            java: 'AxisMoveType.ABS',
            cpp: '::zaber::motion::requests::AxisMoveType::ABS',
            swift: 'DtoRequests.AxisMoveType.abs',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Absolute position.' },
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      options: [
        {
          name: 'waitUntilIdle', type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.',
        },
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveRelative',
      description: ['Move the first axis of the lockstep group to a position relative to its current position.',
        'The other axes in the lockstep group maintain their offsets throughout movement.'],
      route: 'device/lockstep_move',
      request: 'LockstepMoveRequest',
      args: genLockstepArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Rel',
            py: 'dto.AxisMoveType.REL',
            ts: 'requests.AxisMoveType.REL',
            java: 'AxisMoveType.REL',
            cpp: '::zaber::motion::requests::AxisMoveType::REL',
            swift: 'DtoRequests.AxisMoveType.rel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Relative position.' },
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveVelocity',
      description: ['Moves the first axis of the lockstep group at the specified speed.',
        'The other axes in the lockstep group maintain their offsets throughout movement.'],
      route: 'device/lockstep_move',
      request: 'LockstepMoveRequest',
      args: genLockstepArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Vel',
            py: 'dto.AxisMoveType.VEL',
            ts: 'requests.AxisMoveType.VEL',
            java: 'AxisMoveType.VEL',
            cpp: '::zaber::motion::requests::AxisMoveType::VEL',
            swift: 'DtoRequests.AxisMoveType.vel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'velocity', type: 'double', description: 'Movement velocity.' },
        {
          name: 'unit', type: 'VelocityUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of velocity.',
        },
      ],
      options: additionalMoveArgs(false),
    },
    {
      ...moveSinFunctions.moveSin,
      description: ['Moves the first axis of the lockstep group in a sinusoidal trajectory.',
        'The other axes in the lockstep group maintain their offsets throughout movement.'],
      route: 'device/lockstep_move_sin',
      request: 'LockstepMoveSinRequest',
      args: genLockstepArgs(),
    },
    {
      ...moveSinFunctions.moveSinStop,
      description: [
        'Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.',
        'If the sinusoidal motion was started with an integer-plus-half cycle count,',
        'the motion ends at the half-way point of the sinusoidal trajectory.'
      ],
      route: 'device/lockstep_move_sin_stop',
      request: 'LockstepStopRequest',
      args: genLockstepArgs(),
    },
    {
      name: 'MoveMax',
      description: ['Moves the axes to the maximum valid position.',
        'The axes in the lockstep group maintain their offsets throughout movement.',
        'Respects lim.max for all axes in the group.'],
      route: 'device/lockstep_move',
      request: 'LockstepMoveRequest',
      args: genLockstepArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Max',
            py: 'dto.AxisMoveType.MAX',
            ts: 'requests.AxisMoveType.MAX',
            java: 'AxisMoveType.MAX',
            cpp: '::zaber::motion::requests::AxisMoveType::MAX',
            swift: 'DtoRequests.AxisMoveType.max',
          }
        },
      ]),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'MoveMin',
      description: ['Moves the axes to the minimum valid position.',
        'The axes in the lockstep group maintain their offsets throughout movement.',
        'Respects lim.min for all axes in the group.'],
      route: 'device/lockstep_move',
      request: 'LockstepMoveRequest',
      args: genLockstepArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Min',
            py: 'dto.AxisMoveType.MIN',
            ts: 'requests.AxisMoveType.MIN',
            java: 'AxisMoveType.MIN',
            cpp: '::zaber::motion::requests::AxisMoveType::MIN',
            swift: 'DtoRequests.AxisMoveType.min',
          }
        },
      ]),
      params: [],
      options: [
        {
          name: 'waitUntilIdle',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether function should return after the movement is finished or just started.'
        },
        ...additionalMoveArgs(),
      ],
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until the lockstep group stops moving.',
      route: 'device/lockstep_wait_until_idle',
      request: 'LockstepWaitUntilIdleRequest',
      args: genLockstepArgs(),
      params: [],
      options: [
        {
          name: 'throwErrorOnFault',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether to throw error when fault is observed.'
        }
      ],
    },
    {
      name: 'IsBusy',
      description: 'Returns bool indicating whether the lockstep group is executing a motion command.',
      route: 'device/lockstep_is_busy',
      request: 'LockstepEmptyRequest',
      response: 'BoolResponse',
      args: genLockstepArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the axes are currently executing a motion command.',
    },
    {
      name: 'GetAxes',
      description: 'Gets the axes of the lockstep group.',
      route: 'device/lockstep_get_axes',
      request: 'LockstepEmptyRequest',
      args: genLockstepArgs(),
      params: [],
      paramsCheck: [],
      returnType: { type: 'ref', name: 'LockstepAxes', namespace: 'ascii' },
      returnDescription: 'LockstepAxes instance which contains the axes numbers of the lockstep group.',
      obsoleteReason: 'Use GetAxisNumbers instead.'
    },
    {
      name: 'GetAxisNumbers',
      description: 'Gets the axis numbers of the lockstep group.',
      route: 'device/lockstep_get_axis_numbers',
      request: 'LockstepEmptyRequest',
      response: 'LockstepGetAxisNumbersResponse',
      returnDescription: 'Axis numbers in order specified when enabling lockstep.',
      args: genLockstepArgs(),
      params: [],
      returnPropertyName: 'Axes',
      returnType: { type: 'array', of: 'int' },
    },
    {
      name: 'GetOffsets',
      description: 'Gets the initial offsets of secondary axes of an enabled lockstep group.',
      route: 'device/lockstep_get_offsets',
      request: 'LockstepGetRequest',
      response: 'DoubleArrayResponse',
      returnDescription: 'Initial offset for each axis of the lockstep group.',
      args: genLockstepArgs(),
      params: [
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: [
            'Units of position.',
            'Uses primary axis unit conversion.',
          ],
        },
      ],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
    },
    {
      name: 'GetTwists',
      description: 'Gets the twists of secondary axes of an enabled lockstep group.',
      route: 'device/lockstep_get_twists',
      request: 'LockstepGetRequest',
      response: 'DoubleArrayResponse',
      returnDescription: 'Difference between the initial offset and the actual offset for each axis of the lockstep group.',
      args: genLockstepArgs(),
      params: [
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: [
            'Units of position.',
            'Uses primary axis unit conversion.',
          ],
        },
      ],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
    },
    {
      name: 'GetPosition',
      description: 'Returns current position of the primary axis.',
      route: 'device/lockstep_get_pos',
      request: 'LockstepGetRequest',
      response: 'DoubleResponse',
      args: genLockstepArgs(),
      params: [
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of the position.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Primary axis position.',
    },
    {
      name: 'SetTolerance',
      description: [
        'Sets lockstep twist tolerance.',
        'Twist tolerances that do not match the system configuration can reduce performance or damage the system.',
      ],
      route: 'device/lockstep_set_tolerance',
      request: 'LockstepSetRequest',
      args: genLockstepArgs(),
      params: [{
        name: 'tolerance', type: 'double',
        description: 'Twist tolerance.',
        requestName: 'value',
      },
      {
        name: 'unit', type: 'LengthUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: [
          'Units of the tolerance.',
          'Uses primary axis unit conversion when setting to all axes,',
          'otherwise uses specified secondary axis unit conversion.',
        ],
      }],
      options: [{
        name: 'axisIndex', type: 'int',
        description: [
          'Optional index of a secondary axis to set the tolerance for.',
          'If left empty or set to 0, the tolerance is set to all the secondary axes.',
        ],
        defaultValue: '0',
      }],
    },
    {
      name: 'IsEnabled',
      description: 'Checks if the lockstep group is currently enabled on the device.',
      route: 'device/lockstep_is_enabled',
      request: 'LockstepEmptyRequest',
      response: 'BoolResponse',
      returnDescription: 'True if a lockstep group with this ID is enabled on the device.',
      args: genLockstepArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
    },
    {
      name: 'ToString',
      description: 'Returns a string which represents the enabled lockstep group.',
      route: 'device/lockstep_to_string',
      request: 'LockstepEmptyRequest',
      response: 'StringResponse',
      args: genLockstepArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'String which represents the enabled lockstep group.',
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'GetLockstep',
    },
    args: ['1'],
  }],
};

export default definition;
