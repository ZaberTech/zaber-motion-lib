import { ClassDefinition, MethodArg } from '../types';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: {
        cs: '_device.Connection.InterfaceId',
        py: 'self._device.connection.interface_id',
        ts: 'this._device.connection.interfaceId',
        java: 'this.device.getConnection().getInterfaceId()',
        cpp: 'this->_device.getConnection().getInterfaceId()',
        swift: 'device.connection.interfaceId',
      }
    },
    {
      name: 'Device',
      value: {
        cs: '_device.DeviceAddress',
        py: 'self._device.device_address',
        ts: 'this._device.deviceAddress',
        java: 'this.device.getDeviceAddress()',
        cpp: 'this->_device.getDeviceAddress()',
        swift: 'device.deviceAddress',
      }
    },
    {
      name: 'Axis',
      value: {
        cs: '_axisNumber',
        py: 'self._axis_number',
        ts: 'this._axisNumber',
        java: 'this.axisNumber',
        cpp: 'this->getAxisNumber()',
        swift: 'axisNumber',
      }
    },
  ];
}

const flagsReturn = {
  cs: 'new HashSet<string>(response.Flags)',
  py: 'set(response.flags)',
  ts: 'new Set<string>(response.flags)',
  java: 'new HashSet<String>(ArrayUtility.asList(r.getFlags()))',
  cpp: 'std::unordered_set<std::string>(response.flags.begin(), response.flags.end())',
  swift: 'Set(response.flags)',
};

const definition: ClassDefinition = {
  name: 'Warnings',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that triggers the warnings.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
    {
      name: 'AxisNumber',
      type: 'int',
      private: true,
    },
  ],
  description: 'Class used to check and reset warnings and faults on device or axis.',
  ctor: { },
  functions: [
    {
      name: 'GetFlags',
      description: 'Returns current warnings and faults on axis or device.',
      route: 'device/get_warnings',
      request: 'DeviceGetWarningsRequest',
      response: 'DeviceGetWarningsResponse',
      args: [
        ...genArgs(),
        {
          name: 'Clear',
          value: {
            cs: 'false',
            py: 'False',
            ts: 'false',
            java: 'false',
            cpp: 'false',
            swift: 'false',
          },
        },
      ],
      params: [],
      customReturn: flagsReturn,
      returnType: { type: 'set', of: 'string' },
      returnDescription: 'Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.',
    },
    {
      name: 'ClearFlags',
      description: 'Clears (acknowledges) current warnings and faults on axis or device and returns them.',
      route: 'device/get_warnings',
      request: 'DeviceGetWarningsRequest',
      response: 'DeviceGetWarningsResponse',
      args: [
        ...genArgs(),
        {
          name: 'Clear',
          value: {
            cs: 'true',
            py: 'True',
            ts: 'true',
            java: 'true',
            cpp: 'true',
            swift: 'true',
          },
        },
      ],
      params: [],
      customReturn: flagsReturn,
      returnType: { type: 'set', of: 'string' },
      returnDescription: 'Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.',
    },
    {
      name: 'WaitToClear',
      hidden: true,
      description: [
        'Waits for the specified flags to clear.',
        'Use for warnings flags that clear on their own.',
        'Does not clear clearable warnings flags.',
        'Throws TimeoutException if the flags don\'t clear in the specified time.'
      ],
      route: 'device/wait_to_clear_warnings',
      request: 'WaitToClearWarningsRequest',
      args: genArgs(),
      params: [{
        name: 'Timeout', type: 'double',
        description: 'For how long to wait in milliseconds for the flags to clear.',
      }],
      variadic: {
        name: 'warningFlags',
        type: { type: 'array', of: 'string' },
        description: 'The specific warning flags for which to wait to clear.'
      },
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Warnings',
    },
  }, {
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Axis',
      member: 'Warnings',
    },
  }],
};

export default definition;
