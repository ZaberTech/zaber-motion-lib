import _ from 'lodash';

import { definitions as dto } from '../../dto/definitions';
import { ClassDefinition, MethodArg, MethodDefinition } from '../types';
import { hasNamespace, namespaceEquals } from '../type_system';

import { connection } from './binary_connection';
import { defaultPojo, defaultUnits, unitsValue } from './helpers';

function genDeviceArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'DeviceAddress',
    },
  ];
}

function customizeConnectionFunction(name: string): MethodDefinition {
  const newFunc = _.cloneDeep(connection.functions.find(func => func.name === name)!);
  newFunc.params = newFunc.params.filter(param => param.name !== 'device');
  newFunc.args = genDeviceArgs();
  return newFunc;
}

const genericCommand = customizeConnectionFunction('GenericCommand');
genericCommand.description = [
  'Sends a generic Binary command to this device.',
  'For more information please refer to the',
  '[Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).'
];

const genericCommandNoResponse = customizeConnectionFunction('GenericCommandNoResponse');
genericCommandNoResponse.description = [
  'Sends a generic Binary command to this device without expecting a response.',
  'For more information please refer to the',
  '[Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).'
];

const deviceIdentity = dto.classes.find(c => c.name === 'DeviceIdentity' && namespaceEquals(c.namespace, 'binary'))!;

const definition: ClassDefinition = {
  name: 'Device',
  namespace: 'binary',
  constants: [{
    name: 'DefaultMovementTimeout',
    description: 'Default timeout for move commands in seconds.',
    type: 'double',
    value: '60',
  }],
  properties: [
    {
      name: 'Connection',
      description: 'Connection of this device.',
      type: { type: 'ref', name: 'Connection', namespace: 'binary' },
      cpp: {
        emptyCtorValue: '',
      },
    },
    {
      name: 'Settings',
      description: 'Settings and properties of this axis.',
      type: { type: 'ref', name: 'DeviceSettings', namespace: 'binary' },
      referencesSelf: true,
      value: {
        cs: 'new DeviceSettings(this)',
        py: 'DeviceSettings(self)',
        ts: 'new DeviceSettings(this)',
        java: 'new DeviceSettings(this)',
        swift: 'DeviceSettings(device: self)',
      },
      getter: {
        cpp: '{*this}',
      },
    },
    {
      name: 'DeviceAddress',
      description: [
        'The device address uniquely identifies the device on the connection.',
        'It can be configured or automatically assigned by the renumber command.'
      ],
      type: 'int',
      cpp: {
        emptyCtorValue: '-1',
      },
    },
    {
      name: 'Identity',
      description: 'Identity of the device.',
      type: { type: 'ref', name: 'DeviceIdentity', namespace: 'binary' },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveIdentity',
    },
    {
      name: 'IsIdentified',
      description: 'Indicates whether or not the device has been identified.',
      type: 'bool',
      getterCanThrow: true,
      getterFunctionName: 'RetrieveIsIdentified',
    },
    ...deviceIdentity.properties.map(property => ({
      name: property.name,
      description: property.description!,
      type: property.type,
      dtoNamespace: hasNamespace(property.type) ? property.type.namespace : undefined,
      getterCanThrow: true,
      getter: {
        cs: `Identity.${property.name}`,
        py: `self.identity.${_.snakeCase(property.name)}`,
        ts: `this.identity.${_.lowerFirst(property.name)}`,
        java: `this.getIdentity().get${property.name}()`,
        cpp: `this->getIdentity().get${property.name}()`,
        swift: `identity.${_.lowerFirst(property.name)}`,
      }
    })),
  ],
  description: 'Represents a device using the binary protocol.',
  ctor: {},
  functions: [
    genericCommand,
    genericCommandNoResponse,
    {
      name: 'GenericCommandWithUnits',
      description: 'Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.',
      route: 'binary/device/generic_command_with_units',
      request: 'BinaryGenericWithUnitsRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs(),
      params: [
        { name: 'command', type: { type: 'enum', name: 'CommandCode', namespace: 'binary' }, description: 'Command to send.' },
        { name: 'data', type: 'double', defaultValue: '0', description: 'Data argument to the command. Defaults to zero.' },
        {
          name: 'fromUnit', type: 'Units',
          defaultValue: defaultUnits,
          description: 'Unit to convert sent data from.',
        },
        {
          name: 'toUnit', type: 'Units',
          defaultValue: defaultUnits,
          description: 'Unit to convert retrieved data to.',
        },
      ],
      options: [
        {
          name: 'timeout',
          type: 'double',
          defaultValue: '0.0',
          description: 'Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.',
        }
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Data that has been converted to the provided unit.',
    },
    {
      name: 'Home',
      description: 'Homes device. Device returns to its homing position.',
      route: 'binary/device/home',
      request: 'BinaryDeviceHomeRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs(),
      params: [],
      options: [
        {
          name: 'Unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit to convert returned position to.',
        },
        {
          name: 'timeout',
          type: 'double',
          defaultValue: {
            all: 'DEFAULT_MOVEMENT_TIMEOUT',
            cs: 'DefaultMovementTimeout',
            ts: 'Device.DEFAULT_MOVEMENT_TIMEOUT',
            swift: 'Device.defaultMovementTimeout',
          },
          description: 'Number of seconds to wait for response from the device chain (defaults to 60s).',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Current position that has been converted to the provided unit.',
    },
    {
      name: 'Stop',
      description: 'Stops ongoing device movement. Decelerates until zero speed.',
      route: 'binary/device/stop',
      request: 'BinaryDeviceStopRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs(),
      params: [],
      options: [
        {
          name: 'Unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit to convert returned position to.',
        },
        {
          name: 'timeout',
          type: 'double',
          defaultValue: {
            all: 'DEFAULT_MOVEMENT_TIMEOUT',
            cs: 'DefaultMovementTimeout',
            ts: 'Device.DEFAULT_MOVEMENT_TIMEOUT',
            swift: 'Device.defaultMovementTimeout',
          },
          description: 'Number of seconds to wait for response from the device chain (defaults to 60s).',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Current position that has been converted to the provided unit.',
    },
    {
      name: 'MoveAbsolute',
      description: 'Move device to absolute position.',
      route: 'binary/device/move',
      request: 'BinaryDeviceMoveRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Abs',
            py: 'dto.AxisMoveType.ABS',
            ts: 'requests.AxisMoveType.ABS',
            java: 'AxisMoveType.ABS',
            cpp: '::zaber::motion::requests::AxisMoveType::ABS',
            swift: 'DtoRequests.AxisMoveType.abs',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Absolute position.' },
        {
          name: 'Unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit for the provided position as well as position returned by the device.',
        },
      ],
      options: [
        {
          name: 'timeout',
          type: 'double',
          defaultValue: {
            all: 'DEFAULT_MOVEMENT_TIMEOUT',
            cs: 'DefaultMovementTimeout',
            ts: 'Device.DEFAULT_MOVEMENT_TIMEOUT',
            swift: 'Device.defaultMovementTimeout',
          },
          description: 'Number of seconds to wait for response from the device chain (defaults to 60s).',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Current position that has been converted to the provided unit.',
    },
    {
      name: 'MoveRelative',
      description: 'Move device to position relative to current position.',
      route: 'binary/device/move',
      request: 'BinaryDeviceMoveRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Rel',
            py: 'dto.AxisMoveType.REL',
            ts: 'requests.AxisMoveType.REL',
            java: 'AxisMoveType.REL',
            cpp: '::zaber::motion::requests::AxisMoveType::REL',
            swift: 'DtoRequests.AxisMoveType.rel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'position', type: 'double', description: 'Relative position.' },
        {
          name: 'Unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit for the provided position as well as position returned by the device.',
        },
      ],
      options: [
        {
          name: 'timeout',
          type: 'double',
          defaultValue: {
            all: 'DEFAULT_MOVEMENT_TIMEOUT',
            cs: 'DefaultMovementTimeout',
            ts: 'Device.DEFAULT_MOVEMENT_TIMEOUT',
            swift: 'Device.defaultMovementTimeout',
          },
          description: 'Number of seconds to wait for response from the device chain (defaults to 60s).',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Current position that has been converted to the provided unit.',
    },
    {
      name: 'MoveVelocity',
      description: 'Begins to move device at specified speed.',
      route: 'binary/device/move',
      request: 'BinaryDeviceMoveRequest',
      response: 'DoubleResponse',
      args: genDeviceArgs().concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.AxisMoveType.Vel',
            py: 'dto.AxisMoveType.VEL',
            ts: 'requests.AxisMoveType.VEL',
            java: 'AxisMoveType.VEL',
            cpp: '::zaber::motion::requests::AxisMoveType::VEL',
            swift: 'DtoRequests.AxisMoveType.vel',
          }
        },
      ]),
      params: [
        { requestName: 'Arg', name: 'velocity', type: 'double', description: 'Movement velocity.' },
        {
          name: 'Unit', type: 'VelocityUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Unit to convert returned velocity to.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Device velocity that has been converted to the provided unit.',
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until device stops moving.',
      route: 'binary/device/wait_until_idle',
      request: 'DeviceEmptyRequest',
      args: genDeviceArgs(),
      params: [],
    },
    {
      name: 'IsBusy',
      description: 'Check whether the device is moving.',
      route: 'binary/device/is_busy',
      request: 'DeviceEmptyRequest',
      response: 'BoolResponse',
      args: genDeviceArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the device is currently executing a motion command.',
    },
    {
      name: 'Identify',
      description: [
        'Queries the device and the database, gathering information about the product.',
        'Without this information features such as unit conversions will not work.',
        'Usually, called automatically by detect devices method.'
      ],
      route: 'binary/device/identify',
      request: 'DeviceIdentifyRequest',
      args: genDeviceArgs(),
      params: [],
      options: [{
        name: 'assumeVersion',
        type: { type: 'optional', of: { type: 'ref', name: 'FirmwareVersion', namespace: [] } },
        description: [
          'The identification assumes the specified firmware version',
          'instead of the version queried from the device.',
          'Providing this argument can lead to unexpected compatibility issues.',
        ],
        defaultValue: defaultPojo(),
      }],
      returnType: { type: 'ref', name: 'DeviceIdentity', namespace: 'binary' },
      returnDescription: 'Device identification data.',
    },
    {
      name: 'Park',
      description: [
        'Parks the axis.',
        'Motor drivers remain enabled and hold current continues to be applied until the device is powered off.',
        'It can later be unparked and moved without first having to home it.'
      ],
      minFw: [{ major: 6, minor: 6 }],
      route: 'binary/device/park',
      request: 'DeviceEmptyRequest',
      args: genDeviceArgs(),
      params: [],
    },
    {
      name: 'Unpark',
      description: 'Unparks axis. Axis will now be able to move.',
      minFw: [{ major: 6, minor: 6 }],
      route: 'binary/device/unpark',
      request: 'DeviceEmptyRequest',
      args: genDeviceArgs(),
      params: [],
    },
    {
      name: 'IsParked',
      description: 'Returns bool indicating whether the axis is parked or not.',
      minFw: [{ major: 6, minor: 6 }],
      route: 'binary/device/is_parked',
      request: 'DeviceEmptyRequest',
      response: 'BoolResponse',
      args: genDeviceArgs(),
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the axis is currently parked. False otherwise.',
    },
    {
      name: 'GetPosition',
      description: 'Returns current device position.',
      route: 'binary/device/get_setting',
      request: 'BinaryDeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: [
        ...genDeviceArgs(),
        {
          name: 'Setting',
          value: {
            py: 'BinarySettings.CURRENT_POSITION',
            cs: 'BinarySettings.CurrentPosition',
            ts: 'BinarySettings.CURRENT_POSITION',
            java: 'BinarySettings.CURRENT_POSITION',
            cpp: 'BinarySettings::CURRENT_POSITION',
            swift: 'BinarySettings.currentPosition',
          },
        },
      ],
      params: [
        {
          name: 'unit', type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of position.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Axis position.',
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the device.',
      route: 'binary/device/device_to_string',
      request: 'DeviceEmptyRequest',
      response: 'StringResponse',
      args: genDeviceArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the device.',
    },
    {
      name: 'RetrieveIdentity',
      description: 'Returns identity.',
      route: 'binary/device/get_identity',
      request: 'DeviceEmptyRequest',
      args: genDeviceArgs(),
      params: [],
      returnType: { type: 'ref', name: 'DeviceIdentity', namespace: 'binary' },
      returnDescription: 'Device identity.',
      sync: true,
      private: true,
    },
    {
      name: 'RetrieveIsIdentified',
      description: 'Returns whether or not the device have been identified.',
      route: 'binary/device/get_is_identified',
      request: 'DeviceEmptyRequest',
      response: 'BoolResponse',
      args: genDeviceArgs(),
      params: [],
      returnType: 'bool',
      returnDescription: 'True if the device has already been identified. False otherwise.',
      returnPropertyName: 'Value',
      sync: true,
      private: true,
    },
  ],
  py: {
    forwardTypes: ['Connection']
  },
  cpp: {
    emptyCtor: true,
  }
};

export default definition;
