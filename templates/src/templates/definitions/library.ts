import { ClassDefinition } from '../types';

import { defaultOptional, defaultString } from './helpers';

// eslint-disable-next-line @typescript-eslint/no-require-imports, @typescript-eslint/no-unsafe-member-access
const zmlVersion = require('../../../../package.json').version as string;

const definition: ClassDefinition = {
  name: 'Library',
  properties: [],
  namespace: [],
  description: 'Access class to general library information and configuration.',
  ctor: false,
  functions: [
    {
      name: 'setLogOutput',
      description: 'Sets library logging output.',
      route: 'logging/set_output',
      request: 'SetLogOutputRequest',
      args: [],
      params: [{
        name: 'mode',
        type: { type: 'ref', name: 'LogOutputMode', namespace: [] },
        description: 'Logging output mode.',
      }, {
        name: 'filePath',
        type: { type: 'optional', of: 'string' },
        description: 'Path of the file to open.', defaultValue: defaultOptional(),
      }],
      static: true,
      sync: true,
    },
    {
      name: 'setDeviceDbSource',
      description: 'Sets source of Device DB data. Allows selection of a web service or a local file.',
      route: 'device_db/set_source',
      request: 'SetDeviceDbSourceRequest',
      args: [],
      params: [
        {
          name: 'sourceType',
          type: { type: 'ref', name: 'DeviceDbSourceType', namespace: [] },
          description: 'Source type.'
        },
        {
          name: 'urlOrFilePath', type: { type: 'optional', of: 'string' },
          description: [
            'URL of the web service or path to the local file.',
            'Leave empty for the default URL of Zaber web service.'
          ],
          defaultValue: defaultOptional(),
        },
      ],
      static: true,
      sync: true,
    },
    {
      name: 'enableDeviceDbStore',
      description: [
        'Enables Device DB store.',
        'The store uses filesystem to save information obtained from the Device DB.',
        'The stored data are later used instead of the Device DB.',
      ],
      route: 'device_db/toggle_store',
      request: 'ToggleDeviceDbStoreRequest',
      args: [{
        name: 'toggleOn',
        value: {
          cs: 'true',
          py: 'True',
          ts: 'true',
          java: 'true',
          cpp: 'true',
          swift: 'true',
        },
      }],
      params: [
        {
          name: 'storeLocation', type: { type: 'optional', of: 'string' },
          description: [
            'Specifies relative or absolute path of the folder used by the store.',
            'If left empty defaults to a folder in user home directory.',
            'Must be accessible by the process.',
          ],
          defaultValue: defaultOptional(),
        },
      ],
      static: true,
      sync: true,
    },
    {
      name: 'disableDeviceDbStore',
      description: [
        'Disables Device DB store.',
      ],
      route: 'device_db/toggle_store',
      request: 'ToggleDeviceDbStoreRequest',
      args: [],
      params: [],
      static: true,
      sync: true,
    },
    {
      name: 'setInternalMode',
      description: [
        'Disables certain customer checks (like FF flag).',
      ],
      route: 'library/set_internal_mode',
      request: 'SetInternalModeRequest',
      args: [],
      params: [
        { name: 'mode', type: 'bool', description: 'Whether to turn internal mode on or off.' },
      ],
      static: true,
      sync: true,
      hidden: true,
    },
    {
      name: 'setIdlePollingPeriod',
      description: [
        'Sets the period between polling for IDLE during movements.',
        'Caution: Setting the period too low may cause performance issues.'
      ],
      route: 'library/set_idle_polling_period',
      request: 'IntRequest',
      args: [],
      params: [
        {
          name: 'period', type: 'int', requestName: 'value',
          description: [
            'Period in milliseconds.',
            'Negative value restores the default period.',
          ],
        },
      ],
      static: true,
      sync: true,
      hidden: true,
    },
    {
      name: 'checkVersion',
      description: 'Throws an error if the version of the loaded shared library does not match the caller\'s version.',
      route: 'library/check_version',
      request: 'CheckVersionRequest',
      args: [
        {
          name: 'host', type: 'string',
          value: {
            py: '"py"',
            cpp: '"cpp"',
            cs: '"cs"',
            ts: '\'js\'',
            java: '"java"',
            swift: '"swift"',
          },
        },
        { name: 'version', type: 'string', value: defaultString(zmlVersion) },
      ],
      params: [],
      static: true,
      sync: true,
    },
  ]
};

export default definition;
