import { ClassDefinition, MethodArg } from '../../types';
import { defaultBool, defaultUnits, unitsValue } from '../helpers';
import { toStringDevice } from '../to_string';

const DEVICE_ARGS: MethodArg[] = [
  { name: 'InterfaceId', value: 'Device.Connection.InterfaceId' },
  { name: 'Device', value: 'Device.DeviceAddress' },
];

const definition: ClassDefinition = {
  name: 'CameraTrigger',
  namespace: 'microscopy',
  description: 'An abstraction over a device and it\'s digital output channel.',
  properties: [
    {
      name: 'Device',
      description: 'The device whose digital output triggers the camera.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' },
    },
    {
      name: 'Channel',
      description: 'The digital output channel that triggers the camera.',
      type: 'int',
      cpp: { emptyCtorValue: '0' },
    }
  ],
  ctor: {
    public: true,
    description: 'Creates instance of `CameraTrigger` based on the given device and digital output channel.',
  },
  functions: [
    {
      name: 'Trigger',
      description: [
        'Triggers the camera.',
        'Schedules trigger pulse on the digital output channel.',
        'By default, the method waits until the trigger pulse is finished.',
      ],
      route: 'microscope/trigger_camera',
      request: 'MicroscopeTriggerCameraRequest',
      args: DEVICE_ARGS.concat([{
        name: 'ChannelNumber',
        value: 'Channel',
      }]),
      params: [
        {
          name: 'PulseWidth',
          type: 'double',
          requestName: 'Delay',
          description: [
            'The time duration of the trigger pulse.',
            'Depending on the camera setting, the argument can be use to specify exposure.',
          ],
        },
        {
          name: 'Unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of time.',
        },
      ],
      options: [{
        name: 'Wait',
        type: 'bool',
        description: 'If false, the method does not wait until the trigger pulse is finished.',
        defaultValue: defaultBool(true),
      }],
    },
    toStringDevice(DEVICE_ARGS),
  ],
  py: {
    forwardTypes: ['CameraTrigger']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'CameraTrigger',
    },
  }, {
    method: 'constructor',
    source: {
      namespace: 'microscopy',
      class: 'CameraTrigger',
    },
    args: [{
      namespace: 'ascii',
      class: 'Device',
    }, '1'],
  }],
};

export default definition;
