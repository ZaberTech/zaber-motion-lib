import { ClassDefinition } from '../../types';
import { defaultNumber } from '../helpers';
import { toStringDevice } from '../to_string';

const DEVICE_ARGS = [
  { name: 'InterfaceId', value: 'Device.Connection.InterfaceId' },
  { name: 'Device', value: 'Device.DeviceAddress' },
];

const definition: ClassDefinition = {
  name: 'Illuminator',
  namespace: 'microscopy',
  description: [
    'Use to manage an LED controller.',
  ],
  minFw: [{ major: 7, minor: 9 }],
  properties: [
    {
      name: 'Device',
      description: 'The base device of this illuminator.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' },
    },
    {
      name: 'IO',
      description: 'I/O channels of this device.',
      type: { type: 'ref', name: 'DeviceIO', namespace: 'ascii' },
      value: {
        cs: 'new DeviceIO(device)',
        py: 'DeviceIO(device)',
        ts: 'new DeviceIO(device)',
        java: 'new DeviceIO(device)',
        swift: 'DeviceIO(device: device)',
      },
      getter: {
        cpp: '{this->_device}'
      },
    },
  ],
  ctor: {
    public: true,
    description: [
      'Creates instance of `Illuminator` based on the given device.',
      'If the device is identified, this constructor will ensure it is an illuminator.'
    ],
    canThrow: true,
    code: {
      cs: 'VerifyIsIlluminator();',
      py: 'self.__verify_is_illuminator()',
      ts: 'this._verifyIsIlluminator();',
      java: 'this.verifyIsIlluminator();',
      cpp: 'this->verifyIsIlluminator();',
      swift: 'try verifyIsIlluminator()',
    }
  },
  functions: [
    {
      name: 'GetChannel',
      description: [
        'Gets an IlluminatorChannel class instance that allows control of a particular channel.',
        'Channels are numbered from 1.',
      ],
      sync: true,
      params: [
        { name: 'ChannelNumber', type: 'int', description: 'Number of channel to control.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'channelNumber <= 0',
            py: 'channel_number <= 0',
            ts: 'channelNumber <= 0',
            java: 'channelNumber <= 0',
            cpp: 'channelNumber <= 0',
            swift: 'channelNumber > 0',
          },
          message: 'Invalid value; channels are numbered from 1.',
        }
      ],
      code: {
        cs: 'return new IlluminatorChannel(this, channelNumber);',
        py: 'return IlluminatorChannel(self, channel_number)',
        ts: 'return new IlluminatorChannel(this, channelNumber);',
        java: 'return new IlluminatorChannel(this, channelNumber);',
        cpp: 'return {*this, channelNumber};',
        swift: 'return IlluminatorChannel(illuminator: self, channelNumber: channelNumber)',
      },
      returnType: { type: 'ref', name: 'IlluminatorChannel', namespace: 'microscopy' },
      returnDescription: 'Illuminator channel instance.',
    },
    {
      name: 'VerifyIsIlluminator',
      private: true,
      sync: true,
      description: 'Checks if this is an illuminator or some other type of device and throws an error if it is not.',
      params: [],
      args: DEVICE_ARGS,
      route: 'illuminator/verify',
      request: 'DeviceEmptyRequest',
    },
    {
      name: 'Find',
      static: true,
      description: [
        'Finds an illuminator on a connection.',
        'In case of conflict, specify the optional device address.',
      ],
      route: 'illuminator/detect',
      request: 'FindDeviceRequest',
      args: [],
      params: [{
        name: 'connection',
        requestName: 'interfaceId',
        description: 'Connection on which to detect the illuminator.',
        type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
        value: {
          ts: 'connection.interfaceId',
          cs: 'connection.InterfaceId',
          py: 'connection.interface_id',
          java: 'connection.getInterfaceId()',
          cpp: 'connection.getInterfaceId()',
          swift: 'connection.interfaceId',
        },
      }],
      options: [{
        name: 'deviceAddress',
        description: 'Optional device address of the illuminator.',
        type: 'int',
        defaultValue: defaultNumber(0),
      }],
      customReturn: {
        cs: 'new Illuminator(new Device(connection, response.Address))',
        py: 'Illuminator(Device(connection, response.address))',
        ts: 'new Illuminator(new Device(connection, response.address))',
        java: 'new Illuminator(new Device(connection, r.getAddress()))',
        cpp: '{{connection, response.getAddress()}}',
        swift: 'try Illuminator(device: Device(connection: connection, deviceAddress: response.address))',
      },
      returnType: { type: 'ref', name: 'Illuminator', namespace: 'microscopy' },
      response: 'FindDeviceResponse',
      returnDescription: 'New instance of illuminator.',
    },
    toStringDevice(DEVICE_ARGS),
  ],
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'Illuminator',
    },
  }, {
    method: 'static',
    source: {
      namespace: 'microscopy',
      class: 'Illuminator',
      member: 'Find',
    },
    args: [{
      namespace: 'ascii',
      class: 'Connection',
    }],
  }],
};

export default definition;
