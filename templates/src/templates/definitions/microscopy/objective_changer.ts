/* eslint-disable max-len */
import { EOL } from 'os';

import { ClassDefinition, MethodArg } from '../../types';
import { defaultNumber, defaultUnits, unitsValue } from '../helpers';
import { toStringDevice } from '../to_string';

const ARGS: MethodArg[] = [{
  name: 'InterfaceId',
  value: 'Turret.Connection.InterfaceId',
}, {
  name: 'TurretAddress',
  value: 'Turret.DeviceAddress',
}, {
  name: 'FocusAddress',
  value: 'FocusAxis.Device.DeviceAddress',
}, {
  name: 'FocusAxis',
  value: 'FocusAxis.AxisNumber',
}];

const AXIS_ARGS = [{
  name: 'InterfaceId',
  value: 'Turret.Connection.InterfaceId',
}, {
  name: 'Device',
  value: 'Turret.DeviceAddress',
}, {
  name: 'Axis',
  value: {
    ts: '1',
    py: '1',
    cs: '1',
    java: '1',
    cpp: '1',
    swift: '1',
  },
}];

const DATUM_DESC = [
  'The focus datum is the position that the focus stage moves to after an objective change.',
  'It is backed by the limit.home.offset setting.',
];

const definition: ClassDefinition = {
  name: 'ObjectiveChanger',
  namespace: 'microscopy',
  description: [
    'Represents an objective changer of a microscope.',
    'Unstable. Expect breaking changes in future releases.',
  ],
  minFw: [{ major: 7, minor: 32 }],
  properties: [
    {
      name: 'Turret',
      description: 'Device address of the turret.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' }
    },
    {
      name: 'FocusAxis',
      description: 'The focus axis.',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' },
    },
  ],
  ctor: {
    public: true,
    description: [
      'Creates instance of `ObjectiveChanger` based on the given device.',
      'If the device is identified, this constructor will ensure it is an objective changer.'
    ],
    canThrow: true,
    code: {
      cs: 'VerifyIsChanger();',
      py: 'self.__verify_is_changer()',
      ts: 'this._verifyIsChanger();',
      java: 'this.verifyIsChanger();',
      cpp: 'this->verifyIsChanger();',
      swift: 'try verifyIsChanger()',
    }
  },
  functions: [
    {
      name: 'Find',
      obsoleteReason: 'Use microscope\'s `Find` method instead or instantiate manually.',
      static: true,
      description: [
        'Finds an objective changer on a connection.',
        'In case of conflict, specify the optional device addresses.',
        'Devices on the connection must be identified.',
      ],
      route: 'objective_changer/detect',
      request: 'ObjectiveChangerRequest',
      args: [],
      params: [{
        name: 'connection',
        requestName: 'interfaceId',
        description: 'Connection on which to detect the objective changer.',
        type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
        value: {
          ts: 'connection.interfaceId',
          cs: 'connection.InterfaceId',
          py: 'connection.interface_id',
          java: 'connection.getInterfaceId()',
          cpp: 'connection.getInterfaceId()',
          swift: 'connection.interfaceId',
        },
      }],
      options: [{
        name: 'turretAddress',
        description: 'Optional device address of the turret device (X-MOR).',
        type: 'int',
        defaultValue: defaultNumber(0),
      }, {
        name: 'focusAddress',
        description: 'Optional device address of the focus device (X-LDA).',
        type: 'int',
        defaultValue: defaultNumber(0),
      }],
      customReturn: {
        cs: 'new ObjectiveChanger(new Device(connection, response.Turret), new Axis(new Device(connection, response.FocusAddress), response.FocusAxis))',
        py: [
          'ObjectiveChanger(',
          'Device(connection, response.turret),',
          'Axis(Device(connection, response.focus_address), response.focus_axis))',
        ].join(`${EOL}            `),
        ts: 'new ObjectiveChanger(new Device(connection, response.turret), new Axis(new Device(connection, response.focusAddress), response.focusAxis))',
        java: [
          'new ObjectiveChanger(',
          'new Device(connection, r.getTurret()),',
          'new Axis(new Device(connection, r.getFocusAddress()), r.getFocusAxis()))',
        ].join(`${EOL}                `),
        cpp: '{{connection, response.getTurret()}, {{connection, response.getFocusAddress()}, response.getFocusAxis()}}',
        swift: [
          'try ObjectiveChanger(turret: Device(connection: connection, deviceAddress: response.turret),',
          'focusAxis: Axis(device: Device(connection: connection, deviceAddress: response.focusAddress), axisNumber: response.focusAxis))',
        ].join(`${EOL}            `),
      },
      returnType: { type: 'ref', name: 'ObjectiveChanger', namespace: 'microscopy' },
      response: 'ObjectiveChangerCreateResponse',
      returnDescription: 'New instance of objective changer.',
    },
    {
      name: 'Change',
      description: [
        'Changes the objective.',
        'Runs a sequence of movements switching from the current objective to the new one.',
        'The focus stage moves to the focus datum after the objective change.',
      ],
      route: 'objective_changer/change',
      request: 'ObjectiveChangerChangeRequest',
      args: ARGS,
      params: [{
        name: 'objective',
        type: 'int',
        description: 'Objective number starting from 1.',
      }],
      options: [{
        name: 'focusOffset',
        description: 'Optional offset from the focus datum.',
        type: { type: 'optional', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        defaultValue: {
          py: 'None',
          java: 'null',
          cpp: 'std::nullopt',
          cs: 'null',
          swift: 'nil',
        },
      }]
    }, {
      name: 'Release',
      description: [
        'Moves the focus stage out of the turret releasing the current objective.',
      ],
      route: 'objective_changer/release',
      request: 'ObjectiveChangerRequest',
      args: ARGS,
      params: [],
    }, {
      name: 'getCurrentObjective',
      description: [
        'Returns current objective number starting from 1.',
        'The value of 0 indicates that the position is either unknown or between two objectives.'
      ],
      route: 'device/get_index_position',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: AXIS_ARGS,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Current objective number starting from 1 or 0 if not applicable.',
    }, {
      name: 'GetNumberOfObjectives',
      description: 'Gets number of objectives that the turret can accommodate.',
      route: 'device/get_index_count',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: AXIS_ARGS,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of positions.',
    }, {
      name: 'getFocusDatum',
      description: [
        'Gets the focus datum.',
        ...DATUM_DESC,
      ],
      route: 'objective_changer/get_datum',
      request: 'ObjectiveChangerSetRequest',
      response: 'DoubleResponse',
      args: ARGS,
      params: [{
        name: 'unit', type: 'LengthUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of datum.',
      }],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'The datum.',
    }, {
      name: 'setFocusDatum',
      description: [
        'Sets the focus datum.',
        ...DATUM_DESC,
      ],
      route: 'objective_changer/set_datum',
      request: 'ObjectiveChangerSetRequest',
      args: ARGS,
      params: [{
        name: 'datum', type: 'double', requestName: 'value',
        description: 'Value of datum.',
      }, {
        name: 'unit', type: 'LengthUnits',
        value: unitsValue,
        defaultValue: defaultUnits,
        description: 'Units of datum.',
      }],
    }, {
      name: 'VerifyIsChanger',
      private: true,
      sync: true,
      description: 'Checks if this is a objective changer and throws an error if it is not.',
      params: [],
      args: ARGS,
      route: 'objective_changer/verify',
      request: 'ObjectiveChangerRequest',
    },
    toStringDevice(AXIS_ARGS.slice(0, 2)),
  ],
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'ObjectiveChanger',
    },
  }, {
    method: 'static',
    source: {
      namespace: 'microscopy',
      class: 'ObjectiveChanger',
      member: 'Find',
    },
    args: [{
      namespace: 'ascii',
      class: 'Connection',
    }],
  }],
};

export default definition;
