import { ClassDefinition, MethodArg } from '../../types';
import { defaultBool, literalBool, defaultString, defaultUnits, defaultNumber } from '../helpers';
import { toStringBasis } from '../to_string';


const ARGS: MethodArg[] = [
  { name: 'ProviderId', value: 'ProviderId' },
  {
    name: 'InterfaceId',
    value: 'FocusAxis.Device.Connection.InterfaceId',
  }, {
    name: 'FocusAddress',
    value: 'FocusAxis.Device.DeviceAddress',
  }, {
    name: 'FocusAxis',
    value: 'FocusAxis.AxisNumber',
  }, {
    name: 'TurretAddress',
    value: {
      py: 'self.objective_turret.device_address if self.objective_turret else 0',
      ts: 'this.objectiveTurret?.deviceAddress ?? 0',
      cpp: 'this->getObjectiveTurret() ? this->getObjectiveTurret()->getDeviceAddress() : 0',
      cs: 'ObjectiveTurret?.DeviceAddress ?? 0',
      java: 'objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0',
      swift: 'objectiveTurret?.deviceAddress ?? 0',
    },
  }
];

const FOCUS_AXIS_ARGS = [{
  name: 'InterfaceId',
  value: 'FocusAxis.Device.Connection.InterfaceId',
},
{
  name: 'Device',
  value: 'FocusAxis.Device.DeviceAddress',
},
{
  name: 'Axis',
  value: 'FocusAxis.AxisNumber',
}];

const definition: ClassDefinition = {
  name: 'Autofocus',
  namespace: 'microscopy',
  description: 'A generic autofocus device.',
  properties: [
    {
      name: 'ProviderId',
      description: 'The identification of external device providing the capability.',
      type: 'int',
      cpp: { emptyCtorValue: '' },
    },
    {
      name: 'FocusAxis',
      description: 'The focus axis.',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' },
    },
    {
      name: 'ObjectiveTurret',
      description: 'The objective turret device if the microscope has one.',
      type: { type: 'optional', of: { type: 'ref', name: 'Device', namespace: 'ascii' } },
      cpp: { emptyCtorValue: '' },
    },
  ],
  ctor: {
    public: true,
    description: 'Creates instance of `Autofocus` based on the given provider id.',
  },
  functions: [
    {
      name: 'SetFocusZero',
      description: 'Sets the current focus to be target for the autofocus control loop.',
      route: 'autofocus/set_zero',
      request: 'EmptyAutofocusRequest',
      args: ARGS,
      params: [],
    },
    {
      name: 'GetStatus',
      description: 'Returns the status of the autofocus.',
      route: 'autofocus/get_status',
      request: 'EmptyAutofocusRequest',
      response: 'AutofocusGetStatusResponse',
      args: ARGS,
      params: [],
      returnPropertyName: 'Status',
      returnDescription: 'The status of the autofocus.',
      returnType: { type: 'ref', name: 'AutofocusStatus', namespace: 'microscopy' },
    },
    {
      name: 'FocusOnce',
      description: 'Moves the device until it\'s in focus.',
      route: 'autofocus/focus_once',
      request: 'AutofocusFocusRequest',
      args: [
        ...ARGS,
        { name: 'Once', value: literalBool(true) },
      ],
      params: [{
        name: 'Scan',
        type: 'bool',
        description: 'If true, the autofocus will approach from the limit moving until it\'s in range.',
        defaultValue: defaultBool(false),
      }, {
        name: 'Timeout',
        type: 'int',
        description: 'Sets autofocus timeout duration in milliseconds.',
        defaultValue: defaultNumber(-1),
      }],
    },
    {
      name: 'StartFocusLoop',
      description: [
        'Moves the focus axis continuously maintaining focus.',
        'Starts the autofocus control loop.',
        'Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.',
      ],
      route: 'autofocus/start_focus_loop',
      request: 'AutofocusFocusRequest',
      args: ARGS,
      params: [],
    },
    {
      name: 'StopFocusLoop',
      description: [
        'Stops autofocus control loop.',
        'If the focus axis already stopped moving because of an error, an exception will be thrown.',
      ],
      route: 'autofocus/stop_focus_loop',
      request: 'EmptyAutofocusRequest',
      args: ARGS,
      params: [],
    },
    {
      name: 'GetLimitMin',
      description: [
        'Gets the lower motion limit for the autofocus control loop.',
        'Gets motion.tracking.limit.min setting of the focus axis.',
      ],
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: [
        ...FOCUS_AXIS_ARGS,
        { name: 'setting', value: defaultString('motion.tracking.limit.min') },
      ],
      params: [
        {
          name: 'Unit',
          type: 'LengthUnits',
          defaultValue: defaultUnits,
          description: 'The units of the limit.',
        }
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Limit value.',
    },
    {
      name: 'GetLimitMax',
      description: [
        'Gets the upper motion limit for the autofocus control loop.',
        'Gets motion.tracking.limit.max setting of the focus axis.',
      ],
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: [
        ...FOCUS_AXIS_ARGS,
        { name: 'setting', value: defaultString('motion.tracking.limit.max') },
      ],
      params: [
        {
          name: 'Unit',
          type: 'LengthUnits',
          defaultValue: defaultUnits,
          description: 'The units of the limit.',
        }
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Limit value.',
    },
    {
      name: 'SetLimitMin',
      description: [
        'Sets the lower motion limit for the autofocus control loop.',
        'Use the limits to prevent the focus axis from crashing into the sample.',
        'Changes motion.tracking.limit.min setting of the focus axis.',
      ],
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      args: [
        ...FOCUS_AXIS_ARGS,
        { name: 'setting', value: defaultString('motion.tracking.limit.min') },
      ],
      params: [
        {
          name: 'limit',
          type: 'double',
          description: ['The lower limit of the focus axis.'],
          requestName: 'value',
        },
        {
          name: 'Unit',
          type: 'LengthUnits',
          defaultValue: defaultUnits,
          description: 'The units of the limit.',
        }
      ],
    },
    {
      name: 'SetLimitMax',
      description: [
        'Sets the upper motion limit for the autofocus control loop.',
        'Use the limits to prevent the focus axis from crashing into the sample.',
        'Changes motion.tracking.limit.max setting of the focus axis.',
      ],
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      args: [
        ...FOCUS_AXIS_ARGS,
        { name: 'setting', value: defaultString('motion.tracking.limit.max') },
      ],
      params: [
        {
          name: 'limit',
          type: 'double',
          description: ['The upper limit of the focus axis.'],
          requestName: 'value',
        },
        {
          name: 'Unit',
          type: 'LengthUnits',
          defaultValue: defaultUnits,
          description: 'The units of the limit.',
        }
      ],
    },
    {
      name: 'SynchronizeParameters',
      description: [
        'Typically, the control loop parameters and objective are kept synchronized by the library.',
        'If the parameters or current objective changes outside of the library, call this method to synchronize them.',
      ],
      route: 'autofocus/sync_params',
      request: 'EmptyAutofocusRequest',
      args: ARGS,
      params: [],
    },
    {
      name: 'SetObjectiveParameters',
      description: [
        'Sets the parameters for the autofocus objective.',
        'Note that the method temporarily switches current objective to set the parameters.',
      ],
      route: 'autofocus/set_objective_params',
      request: 'AutofocusSetObjectiveParamsRequest',
      args: ARGS,
      params: [{
        name: 'Objective',
        type: 'int',
        description: [
          'The objective (numbered from 1) to set the parameters for.',
          'If your microscope has only one objective, use value of 1.',
        ],
      }, {
        name: 'Parameters',
        type: { type: 'array', of: { type: 'ref', name: 'NamedParameter', namespace: [] } },
        description: 'The parameters for the autofocus objective.',
      }],
    }, {
      name: 'GetObjectiveParameters',
      description: 'Returns the parameters for the autofocus objective.',
      route: 'autofocus/get_objective_params',
      request: 'AutofocusGetObjectiveParamsRequest',
      response: 'AutofocusGetObjectiveParamsResponse',
      args: ARGS,
      params: [{
        name: 'objective',
        type: 'int',
        description: [
          'The objective (numbered from 1) to get the parameters for.',
          'If your microscope has only one objective, use value of 1.',
          'Note that the method temporarily switches current objective to get the parameters.',
        ],
      }],
      returnPropertyName: 'Parameters',
      returnType: { type: 'array', of: { type: 'ref', name: 'NamedParameter', namespace: [] } },
      returnDescription: 'The parameters for the autofocus objective.',
    }, {
      ...toStringBasis('autofocus'),
      request: 'EmptyAutofocusRequest',
      args: ARGS,
      route: 'autofocus/to_string',
    },
  ],
  py: {
    forwardTypes: ['Autofocus']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'Autofocus',
    },
  }],
};

export default definition;
