/* eslint-disable max-len */
import { EOL } from 'os';

import _ from 'lodash';

import { ClassDefinition, MethodArg, PropertyDefinition } from '../../types';
import { defaultBool, defaultPojo } from '../helpers';

const BR_PY = `\\${EOL}            `;
const BR_JAVA = `${EOL}            `;
const BR_CPP = `${EOL}        `;
const BR_SWIFT = `${EOL}            `;

const ARGS: MethodArg[] = [{
  name: 'InterfaceId',
  value: 'Connection.InterfaceId',
}, {
  name: 'config',
  value: {
    cs: '_config',
    py: 'self._config',
    ts: 'this._config',
    java: 'this.config',
    cpp: 'this->_config',
    swift: 'config',
  },
  type: { type: 'ref', name: 'MicroscopeConfig', namespace: 'microscopy' },
}];

function axis(name: string, description: string) {
  const pyName = _.snakeCase(name);
  const jsName = _.lowerFirst(name);
  return  {
    name,
    description,
    type: { type: 'optional', of: { type: 'ref', name: 'Axis', namespace: 'ascii' } },
    value: {
      cs: `config.${name}?.Device > 0 ? new Axis(new Device(connection, config.${name}.Device), config.${name}.Axis) : null`,
      py: [
        `Axis(Device(connection, config.${pyName}.device), config.${pyName}.axis)`,
        `if config.${pyName} and config.${pyName}.device else None`,
      ].join(BR_PY),
      ts: `config.${jsName}?.device ? new Axis(new Device(connection, config.${jsName}.device), config.${jsName}.axis) : undefined`,
      java: [
        `config.get${name}() != null && config.get${name}().getDevice() > 0`,
        `? new Axis(new Device(connection, config.get${name}().getDevice()), config.get${name}().getAxis()) : null`,
      ].join(BR_JAVA),
      cpp: [
        '',
        `_config.get${name}() && _config.get${name}()->getDevice() > 0 ?`,
        `std::optional<Axis>({Device{_connection, _config.get${name}()->getDevice()}, _config.get${name}()->getAxis()}) : std::nullopt`,
      ].join(BR_CPP),
      swift: [
        `config.${jsName} != nil && config.${jsName}!.device > 0 ?`,
        `Axis(device: Device(connection: connection, deviceAddress: config.${jsName}!.device), axisNumber: config.${jsName}!.axis) : nil`,
      ].join(BR_SWIFT),
    },
    cpp: {
      emptyCtorValue: '',
    },
  } satisfies PropertyDefinition;
}

const definition: ClassDefinition = {
  name: 'Microscope',
  namespace: 'microscopy',
  description: [
    'Represent a microscope.',
    'Parts of the microscope may or may not be instantiated depending on the configuration.',
  ],
  minFw: [{ major: 7, minor: 34 }],
  properties: [{
    name: 'Connection',
    description: 'Connection of the microscope.',
    type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
    cpp: {
      emptyCtorValue: '',
    },
  }, {
    name: 'config',
    description: [
      'The configuration.',
      'The parts of the microscope are instantiated depending on the specified data.',
    ],
    type: { type: 'ref', name: 'MicroscopeConfig', namespace: 'microscopy' },
    private: true,
    value: {
      cs: 'MicroscopeConfig.FromByteArray((config as Runtime.IMessage).ToByteArray())',
      py: 'MicroscopeConfig.from_binary(MicroscopeConfig.to_binary(config))',
      ts: 'MicroscopeConfig.fromBinary(MicroscopeConfig.toBinary(config))',
      java: 'MicroscopeConfig.fromByteArray(config.toByteArray())',
      ctorArgument: true,
    },
    cpp: {
      emptyCtorValue: '',
    },
  }, {
    name: 'Illuminator',
    description: 'The illuminator.',
    type: { type: 'optional', of: { type: 'ref', name: 'Illuminator', namespace: 'microscopy' } },
    value: {
      cs: 'config.Illuminator > 0 ? new Illuminator(new Device(connection, config.Illuminator.Value)) : null',
      py: 'Illuminator(Device(connection, config.illuminator)) if config.illuminator else None',
      ts: 'config.illuminator ? new Illuminator(new Device(connection, config.illuminator)) : undefined',
      java: [
        'config.getIlluminator() != null && config.getIlluminator() > 0',
        '? new Illuminator(new Device(connection, config.getIlluminator())) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        '_config.getIlluminator().value_or(0) > 0 ?',
        'std::optional<Illuminator>(Device{_connection, _config.getIlluminator().value()}) : std::nullopt'
      ].join(BR_CPP),
      swift: [
        'config.illuminator != nil && config.illuminator! > 0 ?',
        'try Illuminator(device: Device(connection: connection, deviceAddress: config.illuminator!)) : nil',
      ].join(BR_SWIFT),
    },
    cpp: {
      emptyCtorValue: '',
    },
  },
  axis('FocusAxis', 'The focus axis.'),
  axis('XAxis', 'The X axis.'),
  axis('YAxis', 'The Y axis.'),
  {
    name: 'Plate',
    description: 'Axis group consisting of X and Y axes representing the plate of the microscope.',
    type: { type: 'optional', of: { type: 'ref', name: 'AxisGroup', namespace: 'ascii' } },
    value: {
      cs: 'XAxis != null && YAxis != null ? new AxisGroup(new Axis[] { XAxis, YAxis }) : null',
      py: [
        'AxisGroup([self._x_axis, self._y_axis])',
        'if self._x_axis is not None and self._y_axis is not None else None',
      ].join(BR_PY),
      ts: 'this._xAxis && this._yAxis ? new AxisGroup([this._xAxis, this._yAxis]) : undefined',
      java: [
        'this.xAxis != null && this.yAxis != null',
        '? new AxisGroup(new Axis[] {this.xAxis, this.yAxis}) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        'getXAxis() && getYAxis() ?',
        'std::optional<AxisGroup>({{getXAxis().value(), getYAxis().value()}}) : std::nullopt',
      ].join(BR_CPP),
      swift: 'xAxis != nil && yAxis != nil ? AxisGroup(axes: [xAxis!, yAxis!]) : nil',
    },
    cpp: {
      emptyCtorValue: '',
    },
  },
  {
    name: 'ObjectiveChanger',
    description: 'The objective changer.',
    type: { type: 'optional', of: { type: 'ref', name: 'ObjectiveChanger', namespace: 'microscopy' } },
    value: {
      cs: 'config.ObjectiveChanger > 0 && FocusAxis != null ? new ObjectiveChanger(new Device(connection, config.ObjectiveChanger.Value), FocusAxis) : null',
      py: [
        'ObjectiveChanger(Device(connection, config.objective_changer), self._focus_axis)',
        'if config.objective_changer and self._focus_axis else None',
      ].join(BR_PY),
      ts: 'config.objectiveChanger && this._focusAxis ? new ObjectiveChanger(new Device(connection, config.objectiveChanger), this._focusAxis) : undefined',
      java: [
        'config.getObjectiveChanger() != null && config.getObjectiveChanger() > 0 && this.focusAxis != null',
        '? new ObjectiveChanger(new Device(connection, config.getObjectiveChanger()), this.focusAxis) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        '_config.getObjectiveChanger().value_or(0) > 0 && getFocusAxis() ?',
        'std::optional<ObjectiveChanger>({Device{_connection, _config.getObjectiveChanger().value()}, getFocusAxis().value()}) : std::nullopt',
      ].join(BR_CPP),
      swift: [
        'config.objectiveChanger != nil && config.objectiveChanger! > 0 && focusAxis != nil ?',
        'try ObjectiveChanger(turret: Device(connection: connection, deviceAddress: config.objectiveChanger!), focusAxis: focusAxis!) : nil'
      ].join(BR_SWIFT),
    },
    cpp: {
      emptyCtorValue: '',
    },
  }, {
    name: 'FilterChanger',
    description: 'The filter changer.',
    type: { type: 'optional', of: { type: 'ref', name: 'FilterChanger', namespace: 'microscopy' } },
    value: {
      cs: 'config.FilterChanger > 0 ? new FilterChanger(new Device(connection, config.FilterChanger.Value)) : null',
      py: ['FilterChanger(Device(connection, config.filter_changer))', 'if config.filter_changer else None'].join(BR_PY),
      ts: 'config.filterChanger ? new FilterChanger(new Device(connection, config.filterChanger)) : undefined',
      java: [
        'config.getFilterChanger() != null && config.getFilterChanger() > 0',
        '? new FilterChanger(new Device(connection, config.getFilterChanger())) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        '_config.getFilterChanger().value_or(0) > 0 ?',
        'std::optional<FilterChanger>(Device{_connection, _config.getFilterChanger().value()}) : std::nullopt'
      ].join(BR_CPP),
      swift: 'config.filterChanger ?? 0 > 0 ? FilterChanger(device: Device(connection: connection, deviceAddress: config.filterChanger!)) : nil',
    },
    cpp: {
      emptyCtorValue: '',
    },
  }, {
    name: 'Autofocus',
    description: 'The autofocus feature.',
    type: { type: 'optional', of: { type: 'ref', name: 'Autofocus', namespace: 'microscopy' } },
    value: {
      cs: 'config.Autofocus > 0 && FocusAxis != null ? new Autofocus(config.Autofocus.Value, FocusAxis, ObjectiveChanger?.Turret) : null',
      py: [
        [
          'Autofocus(',
          'config.autofocus,',
          'self._focus_axis,',
          'self._objective_changer.turret if self._objective_changer else None)',
        ].join(BR_PY.slice(1)),
        'if config.autofocus and self._focus_axis else None',
      ].join(BR_PY),
      ts: 'config.autofocus && this._focusAxis ? new Autofocus(config.autofocus, this._focusAxis, this._objectiveChanger?.turret) : undefined',
      java: [
        'config.getAutofocus() != null && config.getAutofocus() > 0 && getFocusAxis() != null',
        '? new Autofocus(config.getAutofocus(), getFocusAxis(), this.objectiveChanger != null ? this.objectiveChanger.getTurret() : null) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        'this->_config.getAutofocus().value_or(0) > 0 && getFocusAxis() ?',
        'std::optional<Autofocus>({',
        [
          'this->_config.getAutofocus().value(),',
          'getFocusAxis().value(),',
          '_objectiveChanger ? std::optional<Device>(_objectiveChanger->getTurret()) : std::nullopt',
        ].map(l => `    ${l}`).join(BR_CPP),
        '}) : std::nullopt'
      ].join(BR_CPP),
      swift: [
        'config.autofocus != nil && config.autofocus! > 0 && focusAxis != nil ?',
        'Autofocus(providerId: config.autofocus!, focusAxis: focusAxis!, objectiveTurret: objectiveChanger?.turret ?? nil) : nil',
      ].join(BR_SWIFT),
    },
    cpp: {
      emptyCtorValue: '',
    },
  }, {
    name: 'CameraTrigger',
    description: 'The camera trigger.',
    type: { type: 'optional', of: { type: 'ref', name: 'CameraTrigger', namespace: 'microscopy' } },
    value: {
      cs: 'config.CameraTrigger?.Device > 0 ? new CameraTrigger(new Device(connection, config.CameraTrigger.Device), config.CameraTrigger.Channel) : null',
      py: [
        [
          'CameraTrigger(',
          'Device(connection, config.camera_trigger.device),',
          'config.camera_trigger.channel)',
        ].join(BR_PY.slice(1)),
        'if config.camera_trigger and config.camera_trigger.device else None',
      ].join(BR_PY),
      ts: 'config.cameraTrigger?.device ? new CameraTrigger(new Device(connection, config.cameraTrigger.device), config.cameraTrigger.channel) : undefined',
      java: [
        'config.getCameraTrigger() != null && config.getCameraTrigger().getDevice() > 0',
        '? new CameraTrigger(new Device(connection, config.getCameraTrigger().getDevice()), config.getCameraTrigger().getChannel()) : null',
      ].join(BR_JAVA),
      cpp: [
        '',
        '_config.getCameraTrigger() && _config.getCameraTrigger()->getDevice() > 0 ?',
        'std::optional<CameraTrigger>({Device{_connection, _config.getCameraTrigger()->getDevice()}, _config.getCameraTrigger()->getChannel()}) : std::nullopt',
      ].join(BR_CPP),
      swift: [
        'config.cameraTrigger != nil && config.cameraTrigger!.device > 0 ?',
        'CameraTrigger(device: Device(connection: connection, deviceAddress: config.cameraTrigger!.device), channel: config.cameraTrigger!.channel) : nil',
      ].join(BR_SWIFT),
    },
  }],
  ctor: {
    public: true,
    canThrow: true,
    description: [
      'Creates instance of `Microscope` from the given config.',
      'Parts are instantiated depending on device addresses in the config.',
    ],
  },
  functions: [{
    name: 'Find',
    static: true,
    description: 'Finds a microscope on a connection.',
    route: 'microscope/detect',
    request: 'MicroscopeFindRequest',
    args: [],
    params: [{
      name: 'connection',
      requestName: 'interfaceId',
      description: 'Connection on which to detect the microscope.',
      type: { type: 'ref', name: 'Connection', namespace: 'ascii' },
      value: {
        ts: 'connection.interfaceId',
        cs: 'connection.InterfaceId',
        py: 'connection.interface_id',
        java: 'connection.getInterfaceId()',
        cpp: 'connection.getInterfaceId()',
        swift: 'connection.interfaceId',
      },
    }, {
      name: 'thirdPartyComponents',
      requestName: 'thirdParty',
      description: 'Third party components of the microscope that cannot be found on the connection.',
      type: { type: 'optional', of: { type: 'ref', name: 'ThirdPartyComponents', namespace: 'microscopy' } },
      defaultValue: defaultPojo(),
    }],
    customReturn: {
      cs: 'new Microscope(connection, response.Config)',
      py: 'Microscope(connection, response.config)',
      ts: 'new Microscope(connection, response.config)',
      java: 'new Microscope(connection, r.getConfig())',
      cpp: '{connection, response.getConfig()}',
      swift: 'try Microscope(connection: connection, config: response.config)',
    },
    returnType: { type: 'ref', name: 'Microscope', namespace: 'microscopy' },
    response: 'MicroscopeConfigResponse',
    returnDescription: 'New instance of microscope.',
  }, {
    name: 'Initialize',
    description: [
      'Initializes the microscope.',
      'Homes all axes, filter changer, and objective changer if they require it.'
    ],
    route: 'microscope/initialize',
    request: 'MicroscopeInitRequest',
    args: ARGS,
    params: [],
    options: [{
      name: 'force',
      type: 'bool',
      description: 'Forces all devices to home even when not required.',
      defaultValue: defaultBool(false),
    }]
  }, {
    name: 'IsInitialized',
    description: 'Checks whether the microscope is initialized.',
    route: 'microscope/is_initialized',
    request: 'MicroscopeEmptyRequest',
    response: 'BoolResponse',
    args: ARGS,
    params: [],
    returnPropertyName: 'Value',
    returnType: 'bool',
    returnDescription: 'True, when the microscope is initialized. False, otherwise.',
  }, {
    name: 'ToString',
    description: 'Returns a string that represents the microscope.',
    route: 'microscope/to_string',
    request: 'MicroscopeEmptyRequest',
    response: 'StringResponse',
    sync: true,
    override: true,
    const: true,
    args: ARGS,
    params: [],
    returnPropertyName: 'Value',
    returnType: 'string',
    returnDescription: 'A string that represents the microscope.',
  }],
  cpp: {
    emptyCtor: true,
  },
  py: {
    forwardTypes: ['Microscope']
  },
  instantiation: [{
    method: 'static',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'Find',
    },
    args: [{
      namespace: 'ascii',
      class: 'Connection',
    }],
  }],
};

export default definition;
