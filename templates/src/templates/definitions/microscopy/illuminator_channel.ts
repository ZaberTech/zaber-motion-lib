import { ClassDefinition, MethodArg } from '../../types';
import { genericCommand, genericCommandMultiResponse, genericCommandNoResponse } from '../generic_commands';
import { getState, setState, canSetState } from '../get_set_state';
import { literalBool } from '../helpers';
import { toStringAxis } from '../to_string';

function genChannelArgs(): MethodArg[] {
  return [
    { name: 'InterfaceId', value: 'Illuminator.Device.Connection.InterfaceId' },
    { name: 'Device', value: 'Illuminator.Device.DeviceAddress' },
    { name: 'Axis', value: 'ChannelNumber' },
  ];
}

const definition: ClassDefinition = {
  name: 'IlluminatorChannel',
  namespace: 'microscopy',
  description: [
    'Use to control a channel (LED lamp) on an illuminator.',
  ],
  minFw: [{ major: 7, minor: 9 }],
  properties: [
    {
      name: 'Illuminator',
      description: 'Illuminator of this channel.',
      type: { type: 'ref', name: 'Illuminator', namespace: 'microscopy' },
      cpp: { emptyCtorValue: '' },
    },
    {
      name: 'ChannelNumber',
      description: 'The channel number identifies the channel on the illuminator.',
      type: 'int',
      cpp: { emptyCtorValue: '0' },
    },
    {
      name: 'Axis',
      private: true,
      description: 'The underlying axis for this channel.',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      value: {
        cs: 'new Axis(illuminator.Device, channelNumber)',
        py: 'Axis(illuminator.device, channel_number)',
        ts: 'new Axis(illuminator.device, channelNumber)',
        java: 'new Axis(illuminator.getDevice(), channelNumber)',
        swift: 'Axis(device: illuminator.device, axisNumber: channelNumber)',
      },
      getter: {
        cpp: '{this->_illuminator.getDevice(), this->_channelNumber}'
      },
    },
    {
      name: 'Settings',
      description: 'Settings and properties of this channel.',
      type: { type: 'ref', name: 'AxisSettings', namespace: 'ascii' },
      value: {
        cs: 'new AxisSettings(_axis)',
        py: 'AxisSettings(self._axis)',
        ts: 'new AxisSettings(this._axis)',
        java: 'new AxisSettings(this.axis)',
        swift: 'AxisSettings(axis: self.axis)',
      },
      getter: {
        cpp: '{this->getAxis()}'
      },
    },
    {
      name: 'Storage',
      description: 'Key-value storage of this channel.',
      type: { type: 'ref', name: 'AxisStorage', namespace: 'ascii' },
      value: {
        cs: 'new AxisStorage(_axis)',
        py: 'AxisStorage(self._axis)',
        ts: 'new AxisStorage(this._axis)',
        java: 'new AxisStorage(this.axis)',
        swift: 'AxisStorage(axis: self.axis)',
      },
      getter: {
        cpp: '{this->getAxis()}'
      },
    },
    {
      name: 'Warnings',
      description: 'Warnings and faults of this channel.',
      type: { type: 'ref', name: 'Warnings', namespace: 'ascii' },
      value: {
        cs: 'new Warnings(illuminator.Device, channelNumber)',
        py: 'Warnings(illuminator.device, channel_number)',
        ts: 'new Warnings(illuminator.device, channelNumber)',
        java: 'new Warnings(illuminator.getDevice(), channelNumber)',
        swift: 'Warnings(device: illuminator.device, axisNumber: channelNumber)',
      },
      getter: {
        cpp: '{this->_illuminator.getDevice(), this->_channelNumber}'
      },
    },
  ],
  ctor: { },
  functions: [
    {
      name: 'On',
      description: 'Turns this channel on.',
      route: 'illuminator/on',
      request: 'ChannelOn',
      args: [
        ...genChannelArgs(),
        { name: 'On', value: literalBool(true) },
      ],
      params: [],
    }, {
      name: 'Off',
      description: 'Turns this channel off.',
      route: 'illuminator/on',
      request: 'ChannelOn',
      args: [
        ...genChannelArgs(),
        { name: 'On', value: literalBool(false) }
      ],
      params: [],
    }, {
      name: 'SetOn',
      description: 'Turns this channel on or off.',
      route: 'illuminator/on',
      request: 'ChannelOn',
      args: genChannelArgs(),
      params: [{
        name: 'On', type: 'bool',
        description: 'True to turn channel on, false to turn it off.',
      }],
    }, {
      name: 'IsOn',
      description: 'Checks if this channel is on.',
      route: 'illuminator/is_on',
      request: 'AxisEmptyRequest',
      args: genChannelArgs(),
      params: [],
      response: 'BoolResponse',
      returnType: 'bool',
      returnPropertyName: 'Value',
      returnDescription: 'True if channel is on, false otherwise.',
    }, {
      name: 'SetIntensity',
      description: 'Sets channel intensity as a fraction of the maximum flux.',
      route: 'illuminator/set_intensity',
      request: 'ChannelSetIntensity',
      args: genChannelArgs(),
      params: [{
        name: 'Intensity', type: 'double',
        description: 'Fraction of intensity to set (between 0 and 1).',
      }],
    }, {
      name: 'GetIntensity',
      description: 'Gets the current intensity of this channel.',
      route: 'illuminator/get_intensity',
      request: 'AxisEmptyRequest',
      args: genChannelArgs(),
      params: [],
      response: 'DoubleResponse',
      returnType: 'double',
      returnPropertyName: 'Value',
      returnDescription: 'Current intensity as fraction of maximum flux.',
    },
    genericCommand({
      interfaceId: 'Illuminator.Device.Connection.InterfaceId', device: 'Illuminator.Device.DeviceAddress', axis: 'ChannelNumber',
      description: [
        'Sends a generic ASCII command to this channel.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandMultiResponse({
      interfaceId: 'Illuminator.Device.Connection.InterfaceId', device: 'Illuminator.Device.DeviceAddress', axis: 'ChannelNumber',
      description: [
        'Sends a generic ASCII command to this channel and expects multiple responses.',
        'Responses are returned in order of arrival.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandNoResponse({
      interfaceId: 'Illuminator.Device.Connection.InterfaceId', device: 'Illuminator.Device.DeviceAddress', axis: 'ChannelNumber',
      description: [
        'Sends a generic ASCII command to this channel without expecting a response and without adding a message ID',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    getState({ name: 'channel', args: genChannelArgs() }),
    setState({ name: 'channel', args: genChannelArgs() }),
    canSetState({ name: 'channel', args: genChannelArgs() }),
    toStringAxis(genChannelArgs(), 'channel'),
  ],
  py: {
    forwardTypes: ['Illuminator'],
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'microscopy',
      class: 'Illuminator',
      member: 'GetChannel',
    },
    args: ['1'],
  }],
};

export default definition;
