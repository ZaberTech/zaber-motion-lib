import { ClassDefinition, MethodArg } from '../../types';
import { toStringDevice } from '../to_string';

const DEVICE_ARGS: MethodArg[] = [
  { name: 'InterfaceId', value: 'Device.Connection.InterfaceId' },
  { name: 'Device', value: 'Device.DeviceAddress' },
];
const AXIS_ARGS: MethodArg[] = [
  ...DEVICE_ARGS,
  {
    name: 'Axis',
    value: {
      ts: '1',
      py: '1',
      cs: '1',
      java: '1',
      cpp: '1',
      swift: '1',
    },
  },
];

const definition: ClassDefinition = {
  name: 'FilterChanger',
  namespace: 'microscopy',
  description: 'A generic turret device.',
  properties: [
    {
      name: 'Device',
      description: 'The base device of this turret.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: { emptyCtorValue: '' },
    },
  ],
  ctor: {
    public: true,
    description: 'Creates instance of `FilterChanger` based on the given device.',
  },
  functions: [
    {
      name: 'GetNumberOfFilters',
      description: 'Gets number of filters of the changer.',
      route: 'device/get_index_count',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: AXIS_ARGS,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of positions.',
    },
    {
      name: 'GetCurrentFilter',
      description: [
        'Returns the current filter number starting from 1.',
        'The value of 0 indicates that the position is either unknown or between two filters.',
      ],
      route: 'device/get_index_position',
      request: 'AxisEmptyRequest',
      response: 'IntResponse',
      args: AXIS_ARGS,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Filter number starting from 1 or 0 if the position cannot be determined.',
    },
    {
      name: 'Change',
      description: 'Changes to the specified filter.',
      route: 'device/move',
      request: 'DeviceMoveRequest',
      args: AXIS_ARGS.concat([{
        name: 'Type',
        value: {
          cs: 'Requests.AxisMoveType.Index',
          py: 'dto.AxisMoveType.INDEX',
          ts: 'requests.AxisMoveType.INDEX',
          java: 'AxisMoveType.INDEX',
          cpp: '::zaber::motion::requests::AxisMoveType::INDEX',
          swift: 'DtoRequests.AxisMoveType.index',
        }
      }, {
        name: 'waitUntilIdle',
        value: {
          cs: 'true',
          ts: 'true',
          java: 'true',
          cpp: 'true',
          py: 'True',
          swift: 'true',
        },
      }]),
      params: [{
        name: 'filter', type: 'int', requestName: 'ArgInt',
        description: ['Filter number starting from 1.'],
      }],
    },
    toStringDevice(DEVICE_ARGS),
  ],
  py: {
    forwardTypes: ['FilterChanger']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'microscopy',
      class: 'Microscope',
      member: 'FilterChanger',
    },
  }, {
    method: 'constructor',
    source: {
      namespace: 'microscopy',
      class: 'FilterChanger',
    },
    args: [{
      namespace: 'ascii',
      class: 'Device',
    }],
  }],
};

export default definition;
