import { ClassDefinition, MethodArg, MethodDefinition } from '../../types';
import { defaultEmptyArray, defaultNumber, defaultString } from '../helpers';

function genConnectionArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'ProviderId',
    },
  ];
}

const openConnectionBase: Partial<MethodDefinition> = {
  route: 'wdi/open',
  request: 'OpenInterfaceRequest',
  customReturn: {
    cs: 'new WdiAutofocusProvider(response.Value)',
    py: 'WdiAutofocusProvider(response.value)',
    ts: 'new WdiAutofocusProvider(response.value)',
    java: 'new WdiAutofocusProvider(r.getValue())',
    cpp: '{response.getValue()}',
    swift: 'WdiAutofocusProvider(providerId: response.value)',
  },
  returnType: { type: 'ref', name: 'WdiAutofocusProvider', namespace: 'microscopy' },
  response: 'IntResponse',
  py: {
    asyncContextManager: {
      name: 'AsyncWdiAutofocusOpener',
      description: 'Provides a connection in an asynchronous context using `await` or `async with`',
      closer: 'close_async',
    },
  },
};

const definition: ClassDefinition = {
  name: 'WdiAutofocusProvider',
  namespace: 'microscopy',
  constants: [{
    name: 'DefaultTcpPort',
    description: [
      'Default port number for TCP connections to WDI autofocus.',
    ],
    type: 'int',
    value: '27',
  }],
  properties: [
    {
      name: 'ProviderId',
      description: 'The ID identifies the autofocus with the underlying library.',
      type: 'int',
      cpp: { emptyCtorValue: '' },
    },
  ],
  ctor: {},
  description: 'Class representing access to WDI Autofocus connection.',
  functions: [
    {
      name: 'OpenTcp',
      static: true,
      description: 'Opens a TCP connection to WDI autofocus.',
      args: [{
        name: 'InterfaceType',
        value: {
          cs: 'Requests.InterfaceType.Tcp',
          py: 'dto.InterfaceType.TCP',
          ts: 'requests.InterfaceType.TCP',
          java: 'InterfaceType.TCP',
          cpp: '::zaber::motion::requests::InterfaceType::TCP',
          swift: 'DtoRequests.InterfaceType.tcp',
        }
      }],
      params: [
        { name: 'hostName', type: 'string', description: 'Hostname or IP address.' },
        {
          name: 'port', type: 'int',
          description: 'Optional port number (defaults to 27).',
          defaultValue: {
            all: 'DEFAULT_TCP_PORT',
            cs: 'DefaultTcpPort',
            ts: 'WdiAutofocusProvider.DEFAULT_TCP_PORT',
            swift: 'WdiAutofocusProvider.defaultTcpPort',
          },
        },
      ],
      returnDescription: 'An object representing the autofocus connection.',
      ...openConnectionBase,
    },
    {
      name: 'Close',
      description: 'Close the connection.',
      route: 'wdi/close',
      request: 'InterfaceEmptyRequest',
      args: genConnectionArgs(),
      params: [],
    },
    {
      name: 'GenericRead',
      description: 'Generic read operation.',
      route: 'wdi/read',
      request: 'WdiGenericRequest',
      response: 'IntArrayResponse',
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'int' },
      returnDescription: 'Array of integers read from the device.',
      args: genConnectionArgs(),
      params: [{
        name: 'RegisterId',
        type: 'int',
        description: 'Register address to read from.',
      }, {
        name: 'Size',
        type: 'int',
        description: 'Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).',
      }],
      options: [{
        name: 'Count',
        type: 'int',
        description: 'Number of values to read (defaults to 1).',
        defaultValue: '1',
      }, {
        name: 'Offset',
        type: 'int',
        description: 'Offset within the register (defaults to 0).',
        defaultValue: '0',
      }, {
        name: 'RegisterBank',
        type: 'string',
        description: 'Register bank letter (defaults to U for user bank).',
        defaultValue: defaultString('U'),
      }],
    },
    {
      name: 'GenericWrite',
      description: 'Generic write operation.',
      route: 'wdi/write',
      request: 'WdiGenericRequest',
      args: genConnectionArgs(),
      params: [{
        name: 'RegisterId',
        type: 'int',
        description: 'Register address to read from.',
      }, {
        name: 'Size',
        type: 'int',
        description: 'Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).',
        defaultValue: '0',
      }, {
        name: 'Data',
        type: { type: 'array', of: 'int' },
        description: 'Array of values to write to the register. Empty array is allowed.',
        defaultValue: defaultEmptyArray('int'),
      }],
      options: [{
        name: 'Offset',
        type: 'int',
        description: 'Offset within the register (defaults to 0).',
        defaultValue: '0',
      }, {
        name: 'RegisterBank',
        type: 'string',
        description: 'Register bank letter (defaults to U for user bank).',
        defaultValue: defaultString('U'),
      }],
    },
    {
      name: 'EnableLaser',
      description: 'Enables the laser.',
      route: 'wdi/write',
      request: 'WdiGenericRequest',
      args: [
        ...genConnectionArgs(),
        {
          name: 'RegisterId',
          value: defaultNumber(1),
        }
      ],
      params: [],
    },
    {
      name: 'DisableLaser',
      description: 'Disables the laser.',
      route: 'wdi/write',
      request: 'WdiGenericRequest',
      args: [
        ...genConnectionArgs(),
        {
          name: 'RegisterId',
          value: defaultNumber(2),
        }
      ],
      params: [],
    },
    {
      name: 'GetStatus',
      description: 'Gets the status of the autofocus.',
      route: 'wdi/get_status',
      request: 'InterfaceEmptyRequest',
      response: 'WdiGetStatusResponse',
      args: genConnectionArgs(),
      params: [],
      returnPropertyName: 'Status',
      returnType: { type: 'ref', name: 'WdiAutofocusProviderStatus', namespace: 'microscopy' },
      returnDescription: 'The status of the autofocus.',
    },
    {
      name: 'ToString',
      description: 'Returns a string that represents the autofocus connection.',
      route: 'wdi/to_string',
      request: 'InterfaceEmptyRequest',
      response: 'StringResponse',
      args: genConnectionArgs(),
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'A string that represents the connection.',
    },
  ],
  instantiation: [{
    method: 'static',
    source: {
      namespace: 'microscopy',
      class: 'WdiAutofocusProvider',
      member: 'OpenTcp',
    },
    args: ['169.254.64.162'],
  }],
  py: {
    forwardTypes: ['AsyncConnectionOpener'],
  },
} satisfies ClassDefinition;

export default definition;
