import { MethodArg, MethodDefinition, OptionalMethodParam } from '../types';

type GenericCommandTemplate = {
  /** The value passed as interface ID */
  interfaceId: string;
  /** The value passed as device address */
  device?: string;
  /** The value passed as axis number */
  axis?: string;
  /** A description of what this function does */
  description: string | string[];
};

function generateGenericCommandArgs(template: GenericCommandTemplate): MethodArg[] {
  const args: MethodArg[] = [];
  args.push({ name: 'InterfaceId', value: template.interfaceId });
  if (template.device != null) {
    args.push({ name: 'Device', value: template.device });
  }
  if (template.axis != null) {
    args.push({ name: 'Axis', value: template.axis });
  }
  return args;
}

function generateOptionalGenericParams(template: GenericCommandTemplate, canOmit = false): OptionalMethodParam[] {
  const params: OptionalMethodParam[] = [];
  if (template.device == null) {
    params.push({
      name: 'device', type: 'int', defaultValue: '0', description: [
        'Optional device address to send the command to.',
        ...(canOmit ? ['Specifying -1 omits the number completely.'] : []),
      ],
    });
  }
  if (template.axis == null) {
    params.push({
      name: 'axis', type: 'int', defaultValue: '0', description: [
        'Optional axis number to send the command to.',
        ...(canOmit ? ['Specifying -1 omits the number completely.'] : []),
      ],
    });
  }
  return params;
}

export function genericCommand(template: GenericCommandTemplate): MethodDefinition {
  return {
    name: 'GenericCommand',
    description: template.description,
    route: 'interface/generic_command',
    request: 'GenericCommandRequest',
    returnType: { type: 'ref', name: 'Response', namespace: 'ascii' },
    returnDescription: 'A response to the command.',
    args: generateGenericCommandArgs(template),
    params: [
      { name: 'command', type: 'string', description: 'Command and its parameters.' },
    ],
    options: [
      ...generateOptionalGenericParams(template),
      {
        name: 'checkErrors', type: 'bool', defaultValue: { all: 'true', py: 'True' },
        description: 'Controls whether to throw an exception when the device rejects the command.'
      },
      {
        name: 'timeout', type: 'int', defaultValue: '0',
        description: [
          'The timeout, in milliseconds, for a device to respond to the command.',
          'Overrides the connection default request timeout.',
        ]
      },
    ],
  };
}

export function genericCommandMultiResponse(template: GenericCommandTemplate): MethodDefinition {
  return {
    name: 'GenericCommandMultiResponse',
    description: template.description,
    route: 'interface/generic_command_multi_response',
    request: 'GenericCommandRequest',
    response: 'GenericCommandResponseCollection',
    returnPropertyName: 'Responses',
    returnType: { type: 'array', of: { type: 'ref', name: 'Response', namespace: 'ascii' } },
    returnDescription: 'All responses to the command.',
    args: generateGenericCommandArgs(template),
    params: [
      { name: 'command', type: 'string', description: 'Command and its parameters.' },
    ],
    options: [
      ...generateOptionalGenericParams(template),
      {
        name: 'checkErrors', type: 'bool', defaultValue: { all: 'true', py: 'True' },
        description: 'Controls whether to throw an exception when a device rejects the command.'
      },
      {
        name: 'timeout', type: 'int', defaultValue: '0',
        description: [
          'The timeout, in milliseconds, for a device to respond to the command.',
          'Overrides the connection default request timeout.',
        ]
      },
    ],
  };
}

export function genericCommandNoResponse(template: GenericCommandTemplate): MethodDefinition {
  return {
    name: 'GenericCommandNoResponse',
    description: template.description,
    route: 'interface/generic_command_no_response',
    request: 'GenericCommandRequest',
    args: generateGenericCommandArgs(template),
    params: [
      { name: 'command', type: 'string', description: 'Command and its parameters.' },
    ],
    options: generateOptionalGenericParams(template, true),
  };
}
