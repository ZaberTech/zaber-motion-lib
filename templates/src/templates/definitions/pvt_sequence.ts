import { ClassDefinition, MethodArg, MethodParam } from '../types';

import { literalBool } from './helpers';
import { pvtIoFunctions } from './pvt_io';

const pvtArgs: MethodArg[] = [
  {
    name: 'InterfaceId',
    value: 'Device.Connection.InterfaceId',
  },
  {
    name: 'Device',
    value: 'Device.DeviceAddress',
  },
  {
    name: 'StreamId',
    value: 'PvtId',
  },
  {
    name: 'Pvt',
    value: literalBool(true),
  },
];


const pvtBufferParam: MethodParam = {
  name: 'pvtBuffer',
  type: { type: 'ref', name: 'PvtBuffer', namespace: 'ascii' },
  description: 'The PVT buffer to queue actions in.',
  value: {
    cs: 'pvtBuffer.BufferId',
    py: 'pvt_buffer.buffer_id',
    ts: 'pvtBuffer.bufferId',
    java: 'pvtBuffer.getBufferId()',
    cpp: 'pvtBuffer.getBufferId()',
    swift: 'pvtBuffer.bufferId',
  }
};

const definition: ClassDefinition = {
  name: 'PvtSequence',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that controls this PVT sequence.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: {
        emptyCtorValue: '',
      },
    },
    {
      name: 'PvtId',
      description: 'The number that identifies the PVT sequence on the device.',
      type: 'int',
      cpp: {
        emptyCtorValue: '-1',
      },
    },
    {
      name: 'Mode',
      description: 'Current mode of the PVT sequence.',
      type: { type: 'ref', name: 'PvtMode', namespace: 'ascii' },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveMode',
    },
    {
      name: 'Axes',
      description: 'An array of axes definitions the PVT sequence is set up to control.',
      type: { type: 'array', of: { type: 'ref', name: 'PvtAxisDefinition', namespace: 'ascii' } },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveAxes',
    },
    {
      name: 'Io',
      description: 'Gets an object that provides access to I/O for this sequence.',
      type: { type: 'ref', name: 'PvtIo', namespace: 'ascii' },
      value: {
        cs: 'new PvtIo(Device, pvtId)',
        py: 'PvtIo(device, pvt_id)',
        ts: 'new PvtIo(device, pvtId)',
        java: 'new PvtIo(device, pvtId)',
        swift: 'PvtIo(device: device, streamId: pvtId)',
      },
      getter: {
        cpp: '{this->_device, this->getPvtId()}',
      },
    },
  ],
  description: [
    'A handle for a PVT sequence with this number on the device.',
    'PVT sequences provide a way execute or store trajectory',
    'consisting of points with defined position, velocity, and time.',
    'PVT sequence methods append actions to a queue which executes',
    'or stores actions in a first in, first out order.',
  ],
  ctor: {},
  functions: [
    {
      name: 'SetupLiveComposite',
      description: [
        'Setup the PVT sequence to control the specified axes and to queue actions on the device.',
        'Allows use of lockstep axes in a PVT sequence.',
      ],
      route: 'device/stream_setup_live_composite',
      request: 'StreamSetupLiveCompositeRequest',
      args: pvtArgs,
      params: [],
      variadic: {
        name: 'pvtAxes',
        type: { type: 'array', of: { type: 'ref', name: 'PvtAxisDefinition', namespace: 'ascii' } },
        description: 'Definition of the PVT sequence axes.',
      },
    },
    {
      name: 'SetupLive',
      description: 'Setup the PVT sequence to control the specified axes and to queue actions on the device.',
      route: 'device/stream_setup_live',
      request: 'StreamSetupLiveRequest',
      args: pvtArgs,
      params: [],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: 'int' },
        description: 'Numbers of physical axes to setup the PVT sequence on.',
      },
    },
    {
      name: 'SetupStoreComposite',
      description: [
        'Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.',
        'Allows use of lockstep axes in a PVT sequence.',
      ],
      route: 'device/stream_setup_store_composite',
      request: 'StreamSetupStoreCompositeRequest',
      args: pvtArgs,
      params: [
        pvtBufferParam,
      ],
      variadic: {
        name: 'pvtAxes',
        type: { type: 'array', of: { type: 'ref', name: 'PvtAxisDefinition', namespace: 'ascii' } },
        description: 'Definition of the PVT sequence axes.',
      },
    },
    {
      name: 'SetupStore',
      description: ['Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.'],
      route: 'device/stream_setup_store',
      request: 'StreamSetupStoreRequest',
      args: pvtArgs,
      params: [
        pvtBufferParam,
      ],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: 'int' },
        description: 'Numbers of physical axes to setup the PVT sequence on.',
      },
    },
    {
      name: 'Call',
      description: ['Append the actions in a PVT buffer to the sequence\'s queue.'],
      route: 'device/stream_call',
      request: 'StreamCallRequest',
      args: pvtArgs,
      params: [
        { ...pvtBufferParam, description: 'The PVT buffer to call.' }
      ],
    },
    {
      name: 'Point',
      description: [
        'Queues a point with absolute coordinates in the PVT sequence.',
        'If some or all velocities are not provided, the sequence calculates the velocities',
        'from surrounding points using finite difference.',
        'The last point of the sequence must have defined velocity (likely zero).',
      ],
      route: 'device/stream_point',
      request: 'PvtPointRequest',
      args: pvtArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [{
        name: 'positions',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the axes to move through, relative to their home positions.'],
      }, {
        name: 'velocities',
        type: { type: 'array', of: { type: 'optional', of: { type: 'ref', name: 'Measurement', namespace: [] } } },
        description: [
          'The axes velocities at the given point.',
          'Specify an empty array or null for specific axes to make the sequence calculate the velocity.',
        ],
      }, {
        name: 'time',
        type: { type: 'ref', name: 'Measurement', namespace: [] },
        description: ['The duration between the previous point in the sequence and this one.'],
      }],
    },
    {
      name: 'PointRelative',
      description: [
        'Queues a point with coordinates relative to the previous point in the PVT sequence.',
        'If some or all velocities are not provided, the sequence calculates the velocities',
        'from surrounding points using finite difference.',
        'The last point of the sequence must have defined velocity (likely zero).',
      ],
      route: 'device/stream_point',
      request: 'PvtPointRequest',
      args: pvtArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [{
        name: 'positions',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the axes to move through, relative to the previous point.'],
      }, {
        name: 'velocities',
        type: { type: 'array', of: { type: 'optional', of: { type: 'ref', name: 'Measurement', namespace: [] } } },
        description: [
          'The axes velocities at the given point.',
          'Specify an empty array or null for specific axes to make the sequence calculate the velocity.',
        ],
      }, {
        name: 'time',
        type: { type: 'ref', name: 'Measurement', namespace: [] },
        description: ['The duration between the previous point in the sequence and this one.'],
      }],
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until the live PVT sequence executes all queued actions.',
      route: 'device/stream_wait_until_idle',
      request: 'StreamWaitUntilIdleRequest',
      args: pvtArgs,
      params: [],
      options: [
        {
          name: 'throwErrorOnFault',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether to throw error when fault is observed.'
        }
      ],
    },
    {
      name: 'Cork',
      description: ['Cork the front of the PVT sequences\'s action queue, blocking execution.',
        'Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.',
        'Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.',
        'You can only cork an idle live PVT sequence.'],
      route: 'device/stream_cork',
      request: 'StreamEmptyRequest',
      args: pvtArgs,
      params: [],
    },
    {
      name: 'Uncork',
      description: ['Uncork the front of the queue, unblocking command execution.',
        'You can only uncork an idle live PVT sequence that is corked.'],
      route: 'device/stream_uncork',
      request: 'StreamEmptyRequest',
      args: pvtArgs,
      params: [],
    },
    {
      name: 'IsBusy',
      description: 'Returns a boolean value indicating whether the live PVT sequence is executing a queued action.',
      route: 'device/stream_is_busy',
      request: 'StreamEmptyRequest',
      response: 'BoolResponse',
      args: pvtArgs,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the PVT sequence is executing a queued action.',
    },
    {
      name: 'ToString',
      description: 'Returns a string which represents the PVT sequence.',
      route: 'device/stream_to_string',
      request: 'StreamEmptyRequest',
      response: 'StringResponse',
      args: pvtArgs,
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'String which represents the PVT sequence.',
    },
    {
      name: 'Disable',
      description: [
        'Disables the PVT sequence.',
        'If the PVT sequence is not setup, this command does nothing.',
        'Once disabled, the PVT sequence will no longer accept PVT commands.',
        'The PVT sequence will process the rest of the commands in the queue until it is empty.',
      ],
      route: 'device/stream_disable',
      request: 'StreamEmptyRequest',
      args: pvtArgs,
      params: [],
    },
    {
      name: 'GenericCommand',
      description: [
        'Sends a generic ASCII command to the PVT sequence.',
        'Keeps resending the command while the device rejects with AGAIN reason.',
      ],
      route: 'device/stream_generic_command',
      request: 'StreamGenericCommandRequest',
      args: pvtArgs,
      params: [
        { name: 'command', type: 'string', description: 'Command and its parameters.' },
      ],
    },
    {
      name: 'GenericCommandBatch',
      description: [
        'Sends a batch of generic ASCII commands to the PVT sequence.',
        'Keeps resending command while the device rejects with AGAIN reason.',
        'The batch is atomic in terms of thread safety.'
      ],
      route: 'device/stream_generic_command_batch',
      request: 'StreamGenericCommandBatchRequest',
      args: pvtArgs,
      params: [
        { name: 'batch', type: { type: 'array', of: 'string' }, description: 'Array of commands.' },
      ],
    },
    {
      name: 'CheckDisabled',
      description: [
        'Queries the PVT sequence status from the device',
        'and returns boolean indicating whether the PVT sequence is disabled.',
        'Useful to determine if execution was interrupted by other movements.'
      ],
      route: 'device/stream_check_disabled',
      request: 'StreamEmptyRequest',
      response: 'BoolResponse',
      args: pvtArgs,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the PVT sequence is disabled.',
    },
    {
      name: 'treatDiscontinuitiesAsError',
      sync: true,
      description: [
        'Makes the PVT sequence throw PvtDiscontinuityException when it encounters discontinuities (ND warning flag).',
      ],
      route: 'device/stream_treat_discontinuities',
      request: 'StreamEmptyRequest',
      args: pvtArgs,
      params: [],
    },
    {
      name: 'ignoreCurrentDiscontinuity',
      sync: true,
      description: [
        'Prevents PvtDiscontinuityException as a result of expected discontinuity when resuming the sequence.',
      ],
      route: 'device/stream_ignore_discontinuity',
      request: 'StreamEmptyRequest',
      args: pvtArgs,
      params: [],
    },
    {
      name: 'RetrieveAxes',
      description: 'Gets the axes of the PVT sequence.',
      route: 'device/stream_get_axes',
      request: 'StreamEmptyRequest',
      response: 'StreamGetAxesResponse',
      returnPropertyName: 'PvtAxes',
      args: pvtArgs,
      params: [],
      paramsCheck: [],
      returnType: { type: 'array', of: { type: 'ref', name: 'PvtAxisDefinition', namespace: 'ascii' } },
      returnDescription: 'An array of axis numbers of the axes the PVT sequence is set up to control.',
      sync: true,
      private: true,
    },
    {
      name: 'RetrieveMode',
      description: 'Get the mode of the PVT sequence.',
      route: 'device/stream_get_mode',
      request: 'StreamEmptyRequest',
      response: 'StreamModeResponse',
      returnPropertyName: 'PvtMode',
      args: pvtArgs,
      params: [],
      paramsCheck: [],
      returnType: { type: 'ref', name: 'PvtMode', namespace: 'ascii' },
      returnDescription: 'Mode of the PVT sequence.',
      sync: true,
      private: true,
    },
    /* ZML-832 - Remove the following functions at a major release */
    {
      ...pvtIoFunctions.setDigitalOutput,
      args: pvtArgs,
      obsoleteReason: 'Use PvtSequence.Io.SetDigitalOutput instead.',
    },
    {
      ...pvtIoFunctions.setAllDigitalOutputs,
      args: pvtArgs,
      obsoleteReason: 'Use PvtSequence.Io.SetAllDigitalOutputs instead.',
    },
    {
      ...pvtIoFunctions.setAnalogOutput,
      args: pvtArgs,
      obsoleteReason: 'Use PvtSequence.Io.SetAnalogOutput instead.',
    },
    {
      ...pvtIoFunctions.setAllAnalogOutputs,
      args: pvtArgs,
      obsoleteReason: 'Use PvtSequence.Io.SetAllAnalogOutputs instead.',
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Pvt',
      member: 'GetSequence',
    },
    args: ['1'],
  }],
};

export default definition;
