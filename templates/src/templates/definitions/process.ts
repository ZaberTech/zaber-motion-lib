import { ClassDefinition, MethodArg } from '../types';

import { genericCommand, genericCommandMultiResponse, genericCommandNoResponse } from './generic_commands';
import { getState, setState, canSetState } from './get_set_state';
import { defaultBool, defaultNumber, defaultString, defaultUnits, literalBool, unitsValue } from './helpers';
import { toStringAxis } from './to_string';

function genProcessArgs(): MethodArg[] {
  return [
    { name: 'InterfaceId', value: 'Controller.Device.Connection.InterfaceId' },
    { name: 'Device', value: 'Controller.Device.DeviceAddress' },
    { name: 'Axis', value: 'ProcessNumber' },
  ];
}

const definition: ClassDefinition = {
  name: 'Process',
  namespace: 'product',
  description: 'Use to drive voltage for a process such as a heater, valve, Peltier device, etc.',
  minFw: [{ major: 7, minor: 35 }],
  properties: [
    {
      name: 'Controller',
      description: 'Controller for this process.',
      type: { type: 'ref', name: 'ProcessController', namespace: 'product' },
    },
    {
      name: 'ProcessNumber',
      description: 'The process number identifies the process on the controller.',
      type: 'int',
    },
    {
      name: 'Axis',
      private: true,
      description: 'The underlying axis for this process.',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      value: {
        cs: 'new Axis(controller.Device, processNumber)',
        py: 'Axis(controller.device, process_number)',
        ts: 'new Axis(controller.device, processNumber)',
        java: 'new Axis(controller.getDevice(), processNumber)',
        swift: 'Axis(device: controller.device, axisNumber: processNumber)',
      },
      getter: {
        cpp: '{this->_controller.getDevice(), this->_processNumber}'
      },
    },
    {
      name: 'Settings',
      description: 'Settings and properties of this process.',
      type: { type: 'ref', name: 'AxisSettings', namespace: 'ascii' },
      value: {
        cs: 'new AxisSettings(_axis)',
        py: 'AxisSettings(self._axis)',
        ts: 'new AxisSettings(this._axis)',
        java: 'new AxisSettings(this.axis)',
        swift: 'AxisSettings(axis: self.axis)',
      },
      getter: {
        cpp: '{this->getAxis()}'
      },
    },
    {
      name: 'Storage',
      description: 'Key-value storage of this process.',
      type: { type: 'ref', name: 'AxisStorage', namespace: 'ascii' },
      value: {
        cs: 'new AxisStorage(_axis)',
        py: 'AxisStorage(self._axis)',
        ts: 'new AxisStorage(this._axis)',
        java: 'new AxisStorage(this.axis)',
        swift: 'AxisStorage(axis: self.axis)',
      },
      getter: {
        cpp: '{this->getAxis()}'
      },
    },
    {
      name: 'Warnings',
      description: 'Warnings and faults of this process.',
      type: { type: 'ref', name: 'Warnings', namespace: 'ascii' },
      value: {
        cs: 'new Warnings(controller.Device, processNumber)',
        py: 'Warnings(controller.device, process_number)',
        ts: 'new Warnings(controller.device, processNumber)',
        java: 'new Warnings(controller.getDevice(), processNumber)',
        swift: 'Warnings(device: controller.device, axisNumber: processNumber)',
      },
      getter: {
        cpp: '{this->_controller.getDevice(), this->_processNumber}'
      },
    },
  ],
  ctor: { },
  functions: [
    {
      name: 'Enable',
      description: 'Sets the enabled state of the driver.',
      route: 'process-controller/enable',
      request: 'ProcessOn',
      args: genProcessArgs(),
      params: [{
        name: 'Enabled', type: 'bool', defaultValue: defaultBool(true), requestName: 'On',
        description: 'If true (default) enables drive. If false disables.',
      }],
    },
    {
      name: 'On',
      description: 'Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.',
      route: 'process-controller/on',
      request: 'ProcessOn',
      args: [
        ...genProcessArgs(),
        { name: 'On', value: literalBool(true) },
      ],
      params: [
        {
          name: 'Duration',
          type: 'double',
          defaultValue: defaultNumber(0),
          description: 'How long to leave the process on.',
        },
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of time.',
        },
      ],
    },
    {
      name: 'Off',
      description: 'Turns this process off.',
      route: 'process-controller/on',
      request: 'ProcessOn',
      args: [
        ...genProcessArgs(),
        { name: 'On', value: literalBool(false) }
      ],
      params: [],
    },
    {
      name: 'SetMode',
      description: 'Sets the control mode of this process.',
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      params: [{
        name: 'Mode', type: { type: 'enum', name: 'ProcessControllerMode', namespace: 'product' },
        description: 'Mode to set this process to.',
        requestName: 'Value', value: {
          cs: '(double)mode',
          java: 'mode.getValue()',
          cpp: 'static_cast<double>(mode)',
          py: 'mode.value',
          swift: 'Double(mode.rawValue)',
        }
      }],
      args: [
        ...genProcessArgs(),
        { name: 'Setting', value: defaultString('process.control.mode') },
      ],
    },
    {
      name: 'GetMode',
      description: 'Gets the control mode of this process.',
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      params: [],
      args: [
        ...genProcessArgs(),
        { name: 'Setting', value: defaultString('process.control.mode') },
      ],
      returnType: { type: 'enum', name: 'ProcessControllerMode', namespace: 'product' },
      returnDescription: 'Control mode.',
      customReturn: {
        ts: 'response.value',
        cs: '(ProcessControllerMode)response.Value',
        py: 'ProcessControllerMode(response.value)',
        java: 'ProcessControllerMode.values()[(int) r.getValue()]',
        cpp: 'static_cast<ProcessControllerMode>(response.getValue())',
        swift: 'ProcessControllerMode(rawValue: Int(response.value))!',
      }
    },
    {
      name: 'GetSource',
      description: 'Gets the source used to control this process.',
      route: 'process_controller/get_source',
      request: 'AxisEmptyRequest',
      params: [],
      args: genProcessArgs(),
      returnType: { type: 'ref', name: 'ProcessControllerSource', namespace: 'product' },
      returnDescription: 'The source providing feedback for this process.',
    },
    {
      name: 'SetSource',
      description: 'Sets the source used to control this process.',
      route: 'process_controller/set_source',
      request: 'SetProcessControllerSource',
      params: [{
        name: 'Source', type: { type: 'ref', name: 'ProcessControllerSource', namespace: 'product' },
        description: 'Sets the source that should provide feedback for this process.',
      }],
      args: genProcessArgs(),
    },
    {
      name: 'GetInput',
      description: 'Gets the current value of the source used to control this process.',
      route: 'process_controller/get_input',
      request: 'AxisEmptyRequest',
      params: [],
      args: genProcessArgs(),
      returnType: { type: 'ref', name: 'Measurement', namespace: [] },
      returnDescription: 'The current value of this process\'s controlling source.',
    },
    {
      name: 'Bridge',
      description: 'Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.',
      route: 'process_controller/bridge',
      request: 'ProcessOn',
      params: [],
      args: [
        ...genProcessArgs(),
        { name: 'On', value: literalBool(true) },
      ],
    },
    {
      name: 'Unbridge',
      description: [
        'Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.',
        'This method is only callable on axis 1 and 3.',
      ],
      route: 'process_controller/bridge',
      request: 'ProcessOn',
      params: [],
      args: [
        ...genProcessArgs(),
        { name: 'On', value: literalBool(false) },
      ],
    },
    {
      name: 'IsBridge',
      description: 'Detects if the given process is in bridging mode.',
      route: 'process_controller/is_bridge',
      request: 'AxisEmptyRequest',
      response: 'BoolResponse',
      params: [],
      args: genProcessArgs(),
      returnType: 'bool',
      returnDescription: 'Whether this process is bridged with its neighbor.',
      returnPropertyName: 'Value',
    },
    genericCommand({
      interfaceId: 'Controller.Device.Connection.InterfaceId', device: 'Controller.Device.DeviceAddress', axis: 'ProcessNumber',
      description: [
        'Sends a generic ASCII command to this process\' underlying axis.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandMultiResponse({
      interfaceId: 'Controller.Device.Connection.InterfaceId', device: 'Controller.Device.DeviceAddress', axis: 'ProcessNumber',
      description: [
        'Sends a generic ASCII command to this process and expect multiple responses.',
        'Responses are returned in order of arrival.',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    genericCommandNoResponse({
      interfaceId: 'Controller.Device.Connection.InterfaceId', device: 'Controller.Device.DeviceAddress', axis: 'ProcessNumber',
      description: [
        'Sends a generic ASCII command to this process without expecting a response and without adding a message ID',
        'For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).',
      ]
    }),
    getState({ name: 'process', args: genProcessArgs() }),
    setState({ name: 'process', args: genProcessArgs() }),
    canSetState({ name: 'process', args: genProcessArgs() }),
    toStringAxis(genProcessArgs(), 'process'),
  ],
  py: {
    forwardTypes: ['ProcessController']
  },
};

export default definition;
