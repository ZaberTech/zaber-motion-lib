import _ from 'lodash';

import type { MethodArg, MethodDefinition } from '../types';

import { defaultString } from './helpers';

export function toStringAxis(args: MethodArg[], what = 'axis'): MethodDefinition {
  if (what !== 'axis') {
    args = [
      ...args,
      { name: 'TypeOverride', value: defaultString(_.capitalize(what)) },
    ];
  }
  return {
    ...toStringBasis(what),
    route: 'device/axis_to_string',
    request: 'AxisToStringRequest',
    args,
  };
}

export function toStringDevice(args: MethodArg[]): MethodDefinition {
  return {
    ...toStringBasis('device'),
    route: 'device/device_to_string',
    request: 'AxisToStringRequest',
    args,
  };
}

export function toStringBasis(what: string) {
  return {
    name: 'ToString',
    description: `Returns a string that represents the ${what}.`,
    response: 'StringResponse',
    sync: true,
    override: true,
    const: true,
    params: [],
    returnPropertyName: 'Value',
    returnType: 'string',
    returnDescription: `A string that represents the ${what}.`,
  } satisfies Partial<MethodDefinition>;
}
