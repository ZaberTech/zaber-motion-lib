import { ClassDefinition, MethodArg } from '../types';

import { defaultUnits, unitsValue } from './helpers';

function genTriggerArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
    {
      name: 'TriggerNumber',
      value: 'TriggerNumber',
    }
  ];
}

const definition: ClassDefinition = {
  name: 'Trigger',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      description: 'Device that this trigger belongs to.',
    },
    {
      name: 'TriggerNumber',
      type: 'int',
      description: 'Number of this trigger.',
    },
  ],
  description: [
    'A handle for a trigger with this number on the device.',
    'Triggers allow setting up actions that occur when a certain condition has been met or an event has occurred.',
    'Please note that the Triggers API is currently an experimental feature.',
  ],
  minFw: [{ major: 7, minor: 6 }],
  ctor: { },
  functions: [
    {
      name: 'Enable',
      description: [
        'Enables the trigger.',
        'Once a trigger is enabled, it will fire whenever its condition transitions from false to true.',
        'If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.'
      ],
      route: 'trigger/enable',
      request: 'TriggerEnableRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'count',
          type: 'int',
          defaultValue: '0',
          description: [
            'Number of times the trigger will fire before disabling itself.',
            'If count is not specified, or 0, the trigger will fire indefinitely.'
          ],
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'count < 0',
            py: 'count < 0',
            ts: 'count < 0',
            java: 'count < 0',
            cpp: 'count < 0',
            swift: 'count >= 0',
          },
          message: 'Invalid value; count must be 0 or positive.'
        }
      ],
    },
    {
      name: 'Disable',
      description: [
        'Disables the trigger.',
        'Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.',
      ],
      route: 'trigger/disable',
      request: 'TriggerEmptyRequest',
      args: genTriggerArgs(),
      params: [],
    },
    {
      name: 'GetState',
      description: 'Gets the state of the trigger.',
      route: 'trigger/get_state',
      request: 'TriggerEmptyRequest',
      args: genTriggerArgs(),
      params: [],
      returnType: { type: 'ref', name: 'TriggerState', namespace: 'ascii' },
      returnDescription: 'Complete state of the trigger.',
    },
    {
      name: 'GetEnabledState',
      description: 'Gets the enabled state of the trigger.',
      route: 'trigger/get_enabled_state',
      request: 'TriggerEmptyRequest',
      args: genTriggerArgs(),
      params: [],
      returnType: { type: 'ref', name: 'TriggerEnabledState', namespace: 'ascii' },
      returnDescription: 'Whether the trigger is enabled and the number of times it will fire.',
    },
    {
      name: 'FireWhen',
      description: 'Set a generic trigger condition.',
      route: 'trigger/fire_when',
      request: 'TriggerFireWhenRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'condition',
          type: 'string',
          description: 'The condition to set for this trigger.'
        },
      ],
    },
    {
      name: 'FireWhenEncoderDistanceTravelled',
      description: 'Set a trigger condition for when an encoder position has changed by a specific distance.',
      route: 'trigger/fire_when_encoder_distance_travelled',
      request: 'TriggerFireWhenDistanceTravelledRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis to monitor for this condition.',
            'May be set to 0 on single-axis devices only.',
          ],
        },
        {
          name: 'distance',
          type: 'double',
          description: 'The measured encoder distance between trigger fires.'
        },
        {
          name: 'unit',
          type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of dist.',
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'distance <= 0',
            py: 'distance <= 0',
            ts: 'distance <= 0',
            java: 'distance <= 0',
            cpp: 'distance <= 0',
            swift: 'distance > 0',
          },
          message: 'Invalid value; encoder distance must be a positive value.'
        }
      ],
    },
    {
      name: 'FireWhenDistanceTravelled',
      description: 'Set a trigger condition for when an axis position has changed by a specific distance.',
      route: 'trigger/fire_when_distance_travelled',
      request: 'TriggerFireWhenDistanceTravelledRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis to monitor for this condition.',
            'May be set to 0 on single-axis devices only.',
          ],
        },
        {
          name: 'distance',
          type: 'double',
          description: 'The measured distance between trigger fires.'
        },
        {
          name: 'unit',
          type: 'LengthUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of dist.',
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'distance <= 0',
            py: 'distance <= 0',
            ts: 'distance <= 0',
            java: 'distance <= 0',
            cpp: 'distance <= 0',
            swift: 'distance > 0',
          },
          message: 'Invalid value; distance must be a positive value.'
        }
      ],
    },
    {
      name: 'FireWhenIo',
      description: 'Set a trigger condition based on an IO channel value.',
      route: 'trigger/fire_when_io',
      request: 'TriggerFireWhenIoRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'portType',
          type: { type: 'enum', name: 'IoPortType', namespace: 'ascii' },
          description: 'The type of IO channel to monitor.',
        },
        {
          name: 'channel',
          type: 'int',
          description: 'The IO channel to monitor.'
        },
        {
          name: 'triggerCondition',
          type: { type: 'enum', name: 'TriggerCondition', namespace: 'ascii' },
          description: 'Comparison operator.',
        },
        {
          name: 'value',
          type: 'double',
          description: 'Comparison value.',
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'channel <= 0',
            py: 'channel <= 0',
            ts: 'channel <= 0',
            java: 'channel <= 0',
            cpp: 'channel <= 0',
            swift: 'channel > 0',
          },
          message: 'Invalid value; channel must be a positive value.'
        }
      ],
    },
    {
      name: 'FireWhenSetting',
      description: 'Set a trigger condition based on a setting value.',
      route: 'trigger/fire_when_setting',
      request: 'TriggerFireWhenSettingRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis to monitor for this condition.',
            'Set to 0 for device-scope settings.'
          ],
        },
        {
          name: 'setting',
          type: 'string',
          description: 'The setting to monitor.'
        },
        {
          name: 'triggerCondition',
          type: { type: 'enum', name: 'TriggerCondition', namespace: 'ascii' },
          description: 'Comparison operator.',
        },
        {
          name: 'value',
          type: 'double',
          description: 'Comparison value.',
        },
        {
          name: 'unit',
          type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of value.',
        },
      ],
    },
    {
      name: 'FireWhenAbsoluteSetting',
      description: 'Set a trigger condition based on an absolute setting value.',
      route: 'trigger/fire_when_setting_absolute',
      request: 'TriggerFireWhenSettingRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis to monitor for this condition.',
            'Set to 0 for device-scope settings.'
          ],
        },
        {
          name: 'setting',
          type: 'string',
          description: 'The setting to monitor.'
        },
        {
          name: 'triggerCondition',
          type: { type: 'enum', name: 'TriggerCondition', namespace: 'ascii' },
          description: 'Comparison operator.',
        },
        {
          name: 'value',
          type: 'double',
          description: 'Comparison value.',
        },
        {
          name: 'unit',
          type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of value.',
        },
      ],
    },
    {
      name: 'FireAtInterval',
      description: 'Set a trigger condition based on a time interval.',
      route: 'trigger/fire_at_interval',
      request: 'TriggerFireAtIntervalRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'interval',
          type: 'double',
          description: 'The time interval between trigger fires.',
        },
        {
          name: 'unit',
          type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of time.',
        },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'interval <= 0',
            py: 'interval <= 0',
            ts: 'interval <= 0',
            java: 'interval <= 0',
            cpp: 'interval <= 0',
            swift: 'interval > 0',
          },
          message: 'Invalid value; interval must be a positive value.'
        }
      ],
    },
    {
      name: 'OnFire',
      description: 'Set a command to be a trigger action.',
      route: 'trigger/on_fire',
      request: 'TriggerOnFireRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'action',
          type: { type: 'enum', name: 'TriggerAction', namespace: 'ascii' },
          description: 'The action number to assign the command to.',
        },
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis to on which to run this command.',
            'Set to 0 for device-scope settings or to run command on all axes.'
          ],
        },
        {
          name: 'command',
          type: 'string',
          description: 'The command to run when the action is triggered.',
        },
      ],
    },
    {
      name: 'OnFireSet',
      description: 'Set a trigger action to update a setting.',
      route: 'trigger/on_fire_set',
      request: 'TriggerOnFireSetRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'action',
          type: { type: 'enum', name: 'TriggerAction', namespace: 'ascii' },
          description: 'The action number to assign the command to.',
        },
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis on which to change the setting.',
            'Set to 0 to change the setting for the device.',
          ],
        },
        {
          name: 'setting',
          type: 'string',
          description: 'The name of the setting to change.',
        },
        {
          name: 'operation',
          type: { type: 'enum', name: 'TriggerOperation', namespace: 'ascii' },
          description: 'The operation to apply to the setting.',
        },
        {
          name: 'value',
          type: 'double',
          description: 'Operation value.',
        },
        {
          name: 'unit',
          type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of value.',
        },
      ],
    },
    {
      name: 'OnFireSetToSetting',
      description: 'Set a trigger action to update a setting with the value of another setting.',
      route: 'trigger/on_fire_set_to_setting',
      request: 'TriggerOnFireSetToSettingRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'action',
          type: { type: 'enum', name: 'TriggerAction', namespace: 'ascii' },
          description: 'The action number to assign the command to.',
        },
        {
          name: 'axis',
          type: 'int',
          description: [
            'The axis on which to change the setting.',
            'Set to 0 to change the setting for the device.',
          ],
        },
        {
          name: 'setting',
          type: 'string',
          description: [
            'The name of the setting to change.',
            'Must have either integer or boolean type.',
          ],
        },
        {
          name: 'operation',
          type: { type: 'enum', name: 'TriggerOperation', namespace: 'ascii' },
          description: 'The operation to apply to the setting.',
        },
        {
          name: 'fromAxis',
          type: 'int',
          description: [
            'The axis from which to read the setting.',
            'Set to 0 to read the setting from the device.',
          ],
        },
        {
          name: 'fromSetting',
          type: 'string',
          description: [
            'The name of the setting to read.',
            'Must have either integer or boolean type.',
          ],
        },
      ],
    },
    {
      name: 'clearAction',
      description: 'Clear a trigger action.',
      route: 'trigger/clear_action',
      request: 'TriggerClearActionRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'action',
          type: { type: 'enum', name: 'TriggerAction', namespace: 'ascii' },
          defaultValue: {
            all: 'TriggerAction.ALL',
            cs: 'TriggerAction.All',
            cpp: 'TriggerAction::ALL',
            swift: 'DtoAscii.TriggerAction.all',
          },
          description: [
            'The action number to clear.',
            'The default option is to clear all actions.',
          ],
        },
      ],
    },
    {
      name: 'getLabel',
      description: 'Returns the label for the trigger.',
      route: 'trigger/get_label',
      request: 'TriggerEmptyRequest',
      response: 'StringResponse',
      args: genTriggerArgs(),
      params: [],
      returnType: 'string',
      returnDescription: 'The label for the trigger.',
      returnPropertyName: 'Value',
    },
    {
      name: 'setLabel',
      description: 'Sets the label for the trigger.',
      route: 'trigger/set_label',
      request: 'TriggerSetLabelRequest',
      args: genTriggerArgs(),
      params: [
        {
          name: 'label',
          type: { type: 'optional', of: 'string' },
          description: [
            'The label to set for this trigger.',
            'If no value or an empty string is provided, this label is deleted.',
          ],
        },
      ],
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
};

export default definition;
