import { ClassDefinition, MethodArg } from '../types';

function genStreamBufferArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
    {
      name: 'BufferId',
      value: 'BufferId',
    },
  ];
}

const definition: ClassDefinition = {
  name: 'StreamBuffer',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'The Device this buffer exists on.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
    {
      name: 'BufferId',
      description: 'The number identifying the buffer on the device.',
      type: 'int',
    },
  ],
  description: [
    'Represents a stream buffer with this number on a device.',
    'A stream buffer is a place to store a queue of stream actions.'
  ],
  ctor: {},
  functions: [
    {
      name: 'GetContent',
      description: ['Get the buffer contents as an array of strings.'],
      route: 'device/stream_buffer_get_content',
      request: 'StreamBufferGetContentRequest',
      response: 'StreamBufferGetContentResponse',
      args: genStreamBufferArgs(),
      params: [],
      returnType: { type: 'array', of: 'string' },
      returnPropertyName: 'BufferLines',
      returnDescription: 'A string array containing all the stream commands stored in the buffer.',
    },
    {
      name: 'Erase',
      description: [
        'Erase the contents of the buffer.',
        'This method fails if there is a stream writing to the buffer.',
      ],
      route: 'device/stream_buffer_erase',
      request: 'StreamBufferEraseRequest',
      args: genStreamBufferArgs(),
      params: [],
    }
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Streams',
      member: 'GetBuffer',
    },
    args: ['1'],
  }],
};

export default definition;
