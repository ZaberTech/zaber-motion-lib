import { ClassDefinition, MethodArg } from '../types';

import { generateCommonSettingsMethods } from './axis_settings';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: {
        cs: '_device.Connection.InterfaceId',
        py: 'self._device.connection.interface_id',
        ts: 'this._device.connection.interfaceId',
        java: 'this.device.getConnection().getInterfaceId()',
        cpp: 'this->_device.getConnection().getInterfaceId()',
        swift: 'device.connection.interfaceId',
      }
    },
    {
      name: 'Device',
      value: {
        cs: '_device.DeviceAddress',
        py: 'self._device.device_address',
        ts: 'this._device.deviceAddress',
        java: 'this.device.getDeviceAddress()',
        cpp: 'this->_device.getDeviceAddress()',
        swift: 'device.deviceAddress',
      }
    },
  ];
}

const definition: ClassDefinition = {
  name: 'DeviceSettings',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      private: true,
    },
  ],
  description: 'Class providing access to various device settings and properties.',
  ctor: { },
  functions: [
    ...generateCommonSettingsMethods('device', genArgs),
    {
      name: 'GetFromAllAxes',
      description: [
        'Gets the value of an axis scope setting for each axis on the device.',
        'Values may be NaN where the setting is not applicable.',
      ],
      route: 'device/get_setting_from_all_axes',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleArrayResponse',
      args: genArgs(),
      params: [{ name: 'setting', type: 'string', description: 'Name of the setting.' }],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'double' },
      returnDescription: 'The setting values on each axis.',
    },
    {
      name: 'GetMany',
      description: 'Gets many setting values in as few device requests as possible.',
      route: 'device/get_many_settings',
      request: 'DeviceMultiGetSettingRequest',
      response: 'GetSettingResults',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'Settings',
        type: { type: 'array', of: { type: 'ref', name: 'GetSetting', namespace: 'ascii' } },
        description: 'The settings to read.',
      },
      returnPropertyName: 'Results',
      returnType: { type: 'array', of: { type: 'ref', name: 'GetSettingResult', namespace: 'ascii' } },
      returnDescription: 'The setting values read.',
    },
    {
      name: 'GetSynchronized',
      description: 'Gets many setting values in the same tick, ensuring their values are synchronized.',
      minFw: [{ major: 7, minor: 35 }],
      route: 'device/get_sync_settings',
      request: 'DeviceMultiGetSettingRequest',
      response: 'GetSettingResults',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'Settings',
        type: { type: 'array', of: { type: 'ref', name: 'GetSetting', namespace: 'ascii' } },
        description: 'The settings to read.',
      },
      returnPropertyName: 'Results',
      returnType: { type: 'array', of: { type: 'ref', name: 'GetSettingResult', namespace: 'ascii' } },
      returnDescription: 'The setting values read.',
    },
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Settings',
    },
  }],
};

export default definition;
