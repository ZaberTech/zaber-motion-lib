import { ClassDefinition, MethodArg } from '../types';

import { defaultString } from './helpers';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
  ];
}

function genSettingNameArg(name: string): MethodArg {
  return {
    name: 'setting',
    value: defaultString(name),
  };
}

const definition: ClassDefinition = {
  name: 'Triggers',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      description: 'Device that these triggers belong to.',
    },
  ],
  description: [
    'Class providing access to device triggers.',
    'Please note that the Triggers API is currently an experimental feature.'
  ],
  minFw: [{ major: 7, minor: 6 }],
  ctor: { },
  functions: [
    {
      name: 'GetNumberOfTriggers',
      description: 'Get the number of triggers for this device.',
      route: 'triggers/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'IntResponse',
      args: [
        ...genArgs(),
        genSettingNameArg('trigger.numtriggers')
      ],
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of triggers for this device.',
    },
    {
      name: 'GetNumberOfActions',
      description: 'Get the number of actions for each trigger for this device.',
      route: 'triggers/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'IntResponse',
      args: [
        ...genArgs(),
        genSettingNameArg('trigger.numactions')
      ],
      params: [],
      returnPropertyName: 'Value',
      returnType: 'int',
      returnDescription: 'Number of actions for each trigger for this device.',
    },
    {
      name: 'GetTrigger',
      description: 'Get a specific trigger for this device.',
      sync: true,
      params: [
        { name: 'triggerNumber', type: 'int', description: 'The number of the trigger to control. Trigger numbers start at 1.' },
      ],
      paramsCheck: [
        {
          code: {
            cs: 'triggerNumber <= 0',
            py: 'trigger_number <= 0',
            ts: 'triggerNumber <= 0',
            java: 'triggerNumber <= 0',
            cpp: 'triggerNumber <= 0',
            swift: 'triggerNumber > 0',
          },
          message: 'Invalid value; triggers are numbered from 1.'
        }
      ],
      code: {
        cs: 'return new Trigger(Device, triggerNumber);',
        py: 'return Trigger(self.device, trigger_number)',
        ts: 'return new Trigger(this.device, triggerNumber);',
        java: 'return new Trigger(this.device, triggerNumber);',
        cpp: 'return {this->_device, triggerNumber};',
        swift: 'return Trigger(device: self.device, triggerNumber: triggerNumber)',
      },
      returnType: { type: 'ref', name: 'Trigger', namespace: 'ascii' },
      returnDescription: 'Trigger instance.',
    },
    {
      name: 'GetTriggerStates',
      description: 'Get the state for every trigger for this device.',
      route: 'triggers/get_trigger_states',
      request: 'DeviceEmptyRequest',
      response: 'TriggerStates',
      args: genArgs(),
      params: [],
      returnPropertyName: 'States',
      returnType: { type: 'array', of: { type: 'ref', name: 'TriggerState', namespace: 'ascii' } },
      returnDescription: 'Complete state for every trigger.',
    },
    {
      name: 'GetEnabledStates',
      description: 'Gets the enabled state for every trigger for this device.',
      route: 'triggers/get_enabled_states',
      request: 'DeviceEmptyRequest',
      response: 'TriggerEnabledStates',
      args: genArgs(),
      params: [],
      returnPropertyName: 'States',
      returnType: { type: 'array', of: { type: 'ref', name: 'TriggerEnabledState', namespace: 'ascii' } },
      returnDescription: 'Whether triggers are enabled and the number of times they will fire.',
    },
    {
      name: 'GetAllLabels',
      description: 'Gets the labels for every trigger for this device.',
      route: 'triggers/get_all_labels',
      request: 'DeviceEmptyRequest',
      response: 'StringArrayResponse',
      args: genArgs(),
      params: [],
      returnPropertyName: 'Values',
      returnType: { type: 'array', of: 'string' },
      returnDescription: 'The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.',
    },
  ],
  py: {
    forwardTypes: ['Device', 'Trigger']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Device',
      member: 'Triggers',
    },
  }],
};

export default definition;
