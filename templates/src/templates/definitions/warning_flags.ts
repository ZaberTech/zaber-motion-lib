import { StringConstantDefinition } from '../types';

const definition: StringConstantDefinition = {
  name: 'WarningFlags',
  namespace: 'ascii',
  description: ['Warning flag constants that indicate whether any device fault or warning is active.',
    'For more information please refer to the',
    '[ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format_warning_flags).'],
  values: [{
    name: 'CriticalSystemError',
    description: 'Critical System Error',
    value: 'FF',
  }, {
    name: 'PeripheralNotSupported',
    description: 'Peripheral Not Supported',
    value: 'FN',
  }, {
    name: 'PeripheralInactive',
    description: 'Peripheral Inactive',
    value: 'FZ',
  }, {
    name: 'HardwareEmergencyStop',
    description: 'Hardware Emergency Stop Driver Disabled',
    value: 'FH',
  }, {
    name: 'OvervoltageOrUndervoltage',
    description: 'Overvoltage or Undervoltage Driver Disabled',
    value: 'FV',
  }, {
    name: 'DriverDisabledNoFault',
    description: [
      'Driver Disabled on Startup or by Command',
      'Devices with Firmware 7.11 and above',
    ],
    value: 'FO',
  }, {
    name: 'CurrentInrushError',
    description: 'Current Inrush Error',
    value: 'FC',
  }, {
    name: 'MotorTemperatureError',
    description: 'Motor Temperature Error',
    value: 'FM',
  }, {
    name: 'DriverDisabled',
    description: [
      'Driver Disabled',
      'Devices with Firmware 7.10 and lower',
    ],
    value: 'FD',
  }, {
    name: 'EncoderError',
    description: 'Encoder Error',
    value: 'FQ',
  }, {
    name: 'IndexError',
    description: 'Index Error',
    value: 'FI',
  }, {
    name: 'AnalogEncoderSyncError',
    description: 'Analog Encoder Sync Error',
    value: 'FA',
  }, {
    name: 'OverdriveLimitExceeded',
    description: 'Overdrive Limit Exceeded',
    value: 'FR',
  }, {
    name: 'StalledAndStopped',
    description: 'Stalled and Stopped',
    value: 'FS',
  }, {
    name: 'StreamBoundsError',
    description: 'Stream Bounds Error',
    value: 'FB',
  }, {
    name: 'InterpolatedPathDeviation',
    description: 'Interpolated Path Deviation',
    value: 'FP',
  }, {
    name: 'LimitError',
    description: 'Limit Error',
    value: 'FE',
  }, {
    name: 'ExcessiveTwist',
    description: 'Excessive Twist',
    value: 'FT',
  }, {
    name: 'UnexpectedLimitTrigger',
    description: 'Unexpected Limit Trigger',
    value: 'WL',
  }, {
    name: 'VoltageOutOfRange',
    description: 'Voltage Out of Range',
    value: 'WV',
  }, {
    name: 'ControllerTemperatureHigh',
    description: 'Controller Temperature High',
    value: 'WT',
  }, {
    name: 'StalledWithRecovery',
    description: 'Stalled with Recovery',
    value: 'WS',
  }, {
    name: 'DisplacedWhenStationary',
    description: 'Displaced When Stationary',
    value: 'WM',
  }, {
    name: 'InvalidCalibrationType',
    description: 'Invalid Calibration Type',
    value: 'WP',
  }, {
    name: 'NoReferencePosition',
    description: 'No Reference Position',
    value: 'WR',
  }, {
    name: 'DeviceNotHomed',
    description: 'Device Not Homed',
    value: 'WH',
  }, {
    name: 'ManualControl',
    description: 'Manual Control',
    value: 'NC',
  }, {
    name: 'MovementInterrupted',
    description: 'Movement Interrupted',
    value: 'NI',
  }, {
    name: 'StreamDiscontinuity',
    description: 'Stream Discontinuity',
    value: 'ND',
  }, {
    name: 'ValueRounded',
    description: 'Value Rounded',
    value: 'NR',
  }, {
    name: 'ValueTruncated',
    description: 'Value Truncated',
    value: 'NT',
  }, {
    name: 'SettingUpdatePending',
    description: 'Setting Update Pending',
    value: 'NU',
  }, {
    name: 'JoystickCalibrating',
    description: 'Joystick Calibrating',
    value: 'NJ',
  }, {
    name: 'FirmwareUpdateMode',
    description: [
      'Device in Firmware Update Mode',
      'Firmware 6.xx only',
    ],
    value: 'NB',
  }]
};

export default definition;
