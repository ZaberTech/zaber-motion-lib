import { ClassDefinition } from '../types';

import { unitsValue } from './helpers';

const definition: ClassDefinition = {
  name: 'UnitTable',
  namespace: [],
  properties: [],
  description: 'Helper for working with units of measure.',
  ctor: false,
  functions: [
    {
      name: 'getSymbol',
      description: 'Gets the standard symbol associated with a given unit.',
      route: 'units/get_symbol',
      request: 'UnitGetSymbolRequest',
      response: 'UnitGetSymbolResponse',
      args: [],
      params: [{ name: 'unit', type: 'Units', value: unitsValue, description: 'Unit of measure.' }],
      static: true,
      sync: true,
      returnPropertyName: 'Symbol',
      returnDescription: 'Symbols corresponding to the given unit. Throws NoValueForKey if no symbol is defined.',
      returnType: 'string',
    },
    {
      name: 'getUnit',
      description: ['Gets the unit enum value associated with a standard symbol.',
        'Note not all units can be retrieved this way.'],
      route: 'units/get_enum',
      request: 'UnitGetEnumRequest',
      response: 'UnitGetEnumResponse',
      args: [],
      params: [{ name: 'symbol', type: 'string', description: 'Symbol to look up.' }],
      static: true,
      sync: true,
      returnDescription: ['The unit enum value with the given symbols.',
        'Throws NoValueForKey if the symbol is not supported for lookup.'].join(' '),
      returnType: 'Units',
      returnPropertyName: 'Unit',
    },
  ],
  cpp: {
    noCopyCtor: true,
  },
};

export default definition;
