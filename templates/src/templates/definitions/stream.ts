import { ClassDefinition, MethodArg, MethodParam } from '../types';

import { defaultUnits, unitsValue } from './helpers';
import { streamIoFunctions } from './stream_io';


function genCircleParams(): MethodParam[] {
  return [
    {
      name: 'RotationDirection',
      type: { type: 'enum', name: 'RotationDirection', namespace: [] },
      description: 'The direction of the rotation.'
    },
    {
      name: 'CenterX',
      type: { type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The first dimension of the position of the center of the circle.',
    },
    {
      name: 'CenterY',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The second dimension of the position of the center of the circle.',
    },
  ];
}

function genArcParams(): MethodParam[] {
  return [
    {
      name: 'RotationDirection',
      type: {  type: 'enum', name: 'RotationDirection', namespace: [] },
      description: 'The direction of the rotation.',
    },
    {
      name: 'CenterX',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The first dimension of the position of the center of the circle on which the arc exists.',
    },
    {
      name: 'CenterY',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The second dimension of the position of the center of the circle on which the arc exists.',
    },
    {
      name: 'EndX',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The first dimension of the end position of the arc.',
    },
    {
      name: 'EndY',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The second dimension of the end position of the arc.',
    },
  ];
}

function genHelixParams(): MethodParam[] {
  return [
    {
      name: 'RotationDirection',
      type: {  type: 'enum', name: 'RotationDirection', namespace: [] },
      description: 'The direction of the rotation.',
    },
    {
      name: 'CenterX',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The first dimension of the position of the center of the circle on which the helix projects.',
    },
    {
      name: 'CenterY',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The second dimension of the position of the center of the circle on which the helix projects.',
    },
    {
      name: 'EndX',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The first dimension of the end position of the helix\'s arc component.',
    },
    {
      name: 'EndY',
      type: {  type: 'ref', name: 'Measurement', namespace: [] },
      description: 'The second dimension of the end position of the helix\'s arc component.',
    },
  ];
}

const streamArgs: MethodArg[] = [
  {
    name: 'InterfaceId',
    value: 'Device.Connection.InterfaceId',
  },
  {
    name: 'Device',
    value: 'Device.DeviceAddress',
  },
  {
    name: 'StreamId',
    value: 'StreamId',
  },
];

const targetAxes: MethodParam = {
  name: 'TargetAxesIndices',
  type: { type: 'array', of: 'int' },
  description: [
    'Indices of the axes in the stream the movement targets.',
    'Refers to the axes provided during the stream setup or further execution.',
    'Indices are zero-based.'
  ],
};
const targetAxesHelix: MethodParam = {
  ...targetAxes,
  description: [
    ...targetAxes.description,
    'The first two axes refer to the helix\'s arc component,',
    'while the rest refers to the helix\'s line component.',
  ],
};

const streamBufferParam: MethodParam = {
  name: 'streamBuffer',
  type: { type: 'ref', name: 'StreamBuffer', namespace: 'ascii' },
  description: 'The stream buffer to queue actions in.',
  value: {
    cs: 'streamBuffer.BufferId',
    py: 'stream_buffer.buffer_id',
    ts: 'streamBuffer.bufferId',
    java: 'streamBuffer.getBufferId()',
    cpp: 'streamBuffer.getBufferId()',
    swift: 'streamBuffer.bufferId',
  }
};

const definition: ClassDefinition = {
  name: 'Stream',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'Device that controls this stream.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
      cpp: {
        emptyCtorValue: '',
      },
    },
    {
      name: 'StreamId',
      description: 'The number that identifies the stream on the device.',
      type: 'int',
      cpp: {
        emptyCtorValue: '-1',
      },
    },
    {
      name: 'Mode',
      description: 'Current mode of the stream.',
      type: { type: 'enum', name: 'StreamMode', namespace: 'ascii' },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveMode',
    },
    {
      name: 'Axes',
      description: 'An array of axes definitions the stream is set up to control.',
      type: { type: 'array', of: { type: 'ref', name: 'StreamAxisDefinition', namespace: 'ascii' } },
      getterCanThrow: true,
      getterFunctionName: 'RetrieveAxes',
    },
    {
      name: 'Io',
      description: 'Gets an object that provides access to I/O for this stream.',
      type: { type: 'ref', name: 'StreamIo', namespace: 'ascii' },
      value: {
        cs: 'new StreamIo(Device, StreamId)',
        py: 'StreamIo(device, stream_id)',
        ts: 'new StreamIo(device, streamId)',
        java: 'new StreamIo(device, streamId)',
        swift: 'StreamIo(device: device, streamId: streamId)',
      },
      getter: {
        cpp: '{this->_device, this->_streamId}',
      },
    },
  ],
  description: [
    'A handle for a stream with this number on the device.',
    'Streams provide a way to execute or store a sequence of actions.',
    'Stream methods append actions to a queue which executes or stores actions in a first in, first out order.'
  ],
  ctor: {},
  functions: [
    {
      name: 'SetupLiveComposite',
      description: [
        'Setup the stream to control the specified axes and to queue actions on the device.',
        'Allows use of lockstep axes in a stream.',
      ],
      route: 'device/stream_setup_live_composite',
      request: 'StreamSetupLiveCompositeRequest',
      args: streamArgs,
      params: [],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: { type: 'ref', name: 'StreamAxisDefinition', namespace: 'ascii' } },
        description: 'Definition of the stream axes.',
      },
    },
    {
      name: 'SetupLive',
      description: 'Setup the stream to control the specified axes and to queue actions on the device.',
      route: 'device/stream_setup_live',
      request: 'StreamSetupLiveRequest',
      args: streamArgs,
      params: [],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: 'int' },
        description: 'Numbers of physical axes to setup the stream on.',
      },
    },
    {
      name: 'SetupStoreComposite',
      description: [
        'Setup the stream to control the specified axes and queue actions into a stream buffer.',
        'Allows use of lockstep axes in a stream.',
      ],
      route: 'device/stream_setup_store_composite',
      request: 'StreamSetupStoreCompositeRequest',
      args: streamArgs,
      params: [
        streamBufferParam,
      ],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: { type: 'ref', name: 'StreamAxisDefinition', namespace: 'ascii' } },
        description: 'Definition of the stream axes.',
      },
    },
    {
      name: 'SetupStore',
      description: ['Setup the stream to control the specified axes and queue actions into a stream buffer.'],
      route: 'device/stream_setup_store',
      request: 'StreamSetupStoreRequest',
      args: streamArgs,
      params: [
        streamBufferParam,
      ],
      variadic: {
        name: 'axes',
        type: { type: 'array', of: 'int' },
        description: 'Numbers of physical axes to setup the stream on.',
      },
    },
    {
      name: 'SetupStoreArbitraryAxes',
      description: [
        'Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.',
        'Afterwards, you may call the resulting stream buffer on arbitrary axes.',
        'This mode does not allow for unit conversions.'
      ],
      route: 'device/stream_setup_store_arbitrary_axes',
      request: 'StreamSetupStoreArbitraryAxesRequest',
      args: streamArgs,
      params: [
        streamBufferParam,
        {
          name: 'axesCount',
          type: 'int',
          description: ['The number of axes in the stream.'],
        },
      ],
    },
    {
      name: 'Call',
      description: ['Append the actions in a stream buffer to the queue.'],
      route: 'device/stream_call',
      request: 'StreamCallRequest',
      args: streamArgs,
      params: [
        { ...streamBufferParam, description: 'The stream buffer to call.' }
      ],
    },
    {
      name: 'LineAbsolute',
      description: ['Queue an absolute line movement in the stream.'],
      route: 'device/stream_line',
      request: 'StreamLineRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [],
      variadic: {
        name: 'endpoint',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the axes to move to, relative to their home positions.'],
      },
    },
    {
      name: 'LineRelative',
      description: ['Queue a relative line movement in the stream.'],
      route: 'device/stream_line',
      request: 'StreamLineRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [],
      variadic: {
        name: 'endpoint',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the axes to move to, relative to their positions before movement.'],
      },
    },
    {
      name: 'LineAbsoluteOn',
      description: ['Queue an absolute line movement in the stream, targeting a subset of the stream axes.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_line',
      request: 'StreamLineRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [
        targetAxes,
        {
          name: 'endpoint',
          type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
          description: ['Positions for the axes to move to, relative to their home positions.'],
        },
      ],
    },
    {
      name: 'LineRelativeOn',
      description: ['Queue a relative line movement in the stream, targeting a subset of the stream axes.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_line',
      request: 'StreamLineRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [
        targetAxes,
        {
          name: 'endpoint',
          type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
          description: ['Positions for the axes to move to, relative to their positions before movement.'],
        },
      ],
    },
    {
      name: 'ArcAbsolute',
      description: ['Queue an absolute arc movement on the first two axes of the stream.',
        'Absolute meaning that the home positions of the axes is treated as the origin.'],
      route: 'device/stream_arc',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: genArcParams(),
    },
    {
      name: 'ArcRelative',
      description: ['Queue a relative arc movement on the first two axes of the stream.',
        'Relative meaning that the current position of the axes is treated as the origin.'],
      route: 'device/stream_arc',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: genArcParams(),
    },
    {
      name: 'ArcAbsoluteOn',
      description: ['Queue an absolute arc movement in the stream.',
        'The movement will only target the specified subset of axes in the stream.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_arc',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [targetAxes].concat(genArcParams()),
    },
    {
      name: 'ArcRelativeOn',
      description: ['Queue a relative arc movement in the stream.',
        'The movement will only target the specified subset of axes in the stream.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_arc',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [targetAxes].concat(genArcParams()),
    },
    {
      name: 'HelixAbsoluteOn',
      description: ['Queue an absolute helix movement in the stream.'],
      minFw: [{ major: 7, minor: 28 }],
      route: 'device/stream_helix',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [targetAxesHelix].concat(genHelixParams()),
      variadic: {
        name: 'endpoint',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the helix\'s line component axes, relative to their home positions.'],
      },
    },
    {
      name: 'HelixRelativeOn',
      description: ['Queue a relative helix movement in the stream.'],
      minFw: [{ major: 7, minor: 28 }],
      route: 'device/stream_helix',
      request: 'StreamArcRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [targetAxesHelix].concat(genHelixParams()),
      variadic: {
        name: 'endpoint',
        type: { type: 'array', of: { type: 'ref', name: 'Measurement', namespace: [] } },
        description: ['Positions for the helix\'s line component axes, relative to their positions before movement.'],
      },
    },
    {
      name: 'CircleAbsolute',
      description: ['Queue an absolute circle movement on the first two axes of the stream.',
        'Absolute meaning that the home positions of the axes are treated as the origin.'],
      route: 'device/stream_circle',
      request: 'StreamCircleRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: genCircleParams(),
    },
    {
      name: 'CircleRelative',
      description: ['Queue a relative circle movement on the first two axes of the stream.',
        'Relative meaning that the current position of the axes is treated as the origin.'],
      route: 'device/stream_circle',
      request: 'StreamCircleRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: genCircleParams(),
    },
    {
      name: 'CircleAbsoluteOn',
      description: ['Queue an absolute circle movement in the stream.',
        'The movement will only target the specified subset of axes in the stream.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_circle',
      request: 'StreamCircleRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Abs',
            py: 'dto.StreamSegmentType.ABS',
            ts: 'requests.StreamSegmentType.ABS',
            java: 'StreamSegmentType.ABS',
            cpp: '::zaber::motion::requests::StreamSegmentType::ABS',
            swift: 'DtoRequests.StreamSegmentType.abs',
          }
        },
      ]),
      params: [targetAxes].concat(genCircleParams()),
    },
    {
      name: 'CircleRelativeOn',
      description: ['Queue a relative circle movement in the stream.',
        'The movement will only target the specified subset of axes in the stream.'],
      minFw: [{ major: 7, minor: 11 }],
      route: 'device/stream_circle',
      request: 'StreamCircleRequest',
      args: streamArgs.concat([
        {
          name: 'Type',
          value: {
            cs: 'Requests.StreamSegmentType.Rel',
            py: 'dto.StreamSegmentType.REL',
            ts: 'requests.StreamSegmentType.REL',
            java: 'StreamSegmentType.REL',
            cpp: '::zaber::motion::requests::StreamSegmentType::REL',
            swift: 'DtoRequests.StreamSegmentType.rel',
          }
        },
      ]),
      params: [targetAxes].concat(genCircleParams()),
    },
    {
      name: 'Wait',
      description: 'Wait a specified time.',
      route: 'device/stream_wait',
      request: 'StreamWaitRequest',
      args: streamArgs,
      params: [
        {
          name: 'time',
          type: 'double',
          description: 'Amount of time to wait.'
        },
        {
          name: 'unit', type: 'TimeUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of time.'
        }
      ],
    },
    {
      name: 'WaitUntilIdle',
      description: 'Waits until the live stream executes all queued actions.',
      route: 'device/stream_wait_until_idle',
      request: 'StreamWaitUntilIdleRequest',
      args: streamArgs,
      params: [],
      options: [
        {
          name: 'throwErrorOnFault',
          type: 'bool',
          defaultValue: {
            all: 'true',
            py: 'True'
          },
          description: 'Determines whether to throw error when fault is observed.'
        }
      ],
    },
    {
      name: 'Cork',
      description: ['Cork the front of the stream\'s action queue, blocking execution.',
        'Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.',
        'Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.',
        'You can only cork an idle live stream.'],
      route: 'device/stream_cork',
      request: 'StreamEmptyRequest',
      args: streamArgs,
      params: [],
    },
    {
      name: 'Uncork',
      description: ['Uncork the front of the queue, unblocking command execution.',
        'You can only uncork an idle live stream that is corked.'],
      route: 'device/stream_uncork',
      request: 'StreamEmptyRequest',
      args: streamArgs,
      params: [],
    },
    {
      name: 'SetHold',
      description: [
        'Pauses or resumes execution of the stream in live mode.',
        'The hold only takes effect during execution of motion segments.',
      ],
      route: 'device/stream_set_hold',
      request: 'StreamSetHoldRequest',
      args: streamArgs,
      params: [
        {
          name: 'hold', type: 'bool',
          description: 'True to pause execution, false to resume.',
        },
      ],
    },
    {
      name: 'IsBusy',
      description: 'Returns a boolean value indicating whether the live stream is executing a queued action.',
      route: 'device/stream_is_busy',
      request: 'StreamEmptyRequest',
      response: 'BoolResponse',
      args: streamArgs,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the stream is executing a queued action.',
    },
    {
      name: 'GetMaxSpeed',
      description: ['Gets the maximum speed of the live stream.', 'Converts the units using the first axis of the stream.'],
      route: 'device/stream_get_max_speed',
      request: 'StreamGetMaxSpeedRequest',
      response: 'DoubleResponse',
      returnDescription: 'The maximum speed of the stream.',
      args: streamArgs,
      params: [
        {
          name: 'unit', type: 'VelocityUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of velocity.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
    },
    {
      name: 'SetMaxSpeed',
      description: ['Sets the maximum speed of the live stream.', 'Converts the units using the first axis of the stream.'],
      route: 'device/stream_set_max_speed',
      request: 'StreamSetMaxSpeedRequest',
      args: streamArgs,
      params: [
        {
          name: 'maxSpeed', type: 'double',
          description: 'Maximum speed at which any stream action is executed.',
        },
        {
          name: 'unit', type: 'VelocityUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of velocity.',
        },
      ],
    },
    {
      name: 'GetMaxTangentialAcceleration',
      description: [
        'Gets the maximum tangential acceleration of the live stream.',
        'Converts the units using the first axis of the stream.'
      ],
      route: 'device/stream_get_max_tangential_acceleration',
      request: 'StreamGetMaxTangentialAccelerationRequest',
      response: 'DoubleResponse',
      returnDescription: 'The maximum tangential acceleration of the live stream.',
      args: streamArgs,
      params: [
        {
          name: 'unit', type: 'AccelerationUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of acceleration.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
    },
    {
      name: 'SetMaxTangentialAcceleration',
      description: [
        'Sets the maximum tangential acceleration of the live stream.',
        'Converts the units using the first axis of the stream.'
      ],
      route: 'device/stream_set_max_tangential_acceleration',
      request: 'StreamSetMaxTangentialAccelerationRequest',
      args: streamArgs,
      params: [
        {
          name: 'maxTangentialAcceleration', type: 'double',
          description: 'Maximum tangential acceleration at which any stream action is executed.',
        },
        {
          name: 'unit', type: 'AccelerationUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of acceleration.',
        },
      ],
    },
    {
      name: 'GetMaxCentripetalAcceleration',
      description: [
        'Gets the maximum centripetal acceleration of the live stream.',
        'Converts the units using the first axis of the stream.'
      ],
      route: 'device/stream_get_max_centripetal_acceleration',
      request: 'StreamGetMaxCentripetalAccelerationRequest',
      response: 'DoubleResponse',
      returnDescription: 'The maximum centripetal acceleration of the live stream.',
      args: streamArgs,
      params: [
        {
          name: 'unit', type: 'AccelerationUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of acceleration.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
    },
    {
      name: 'SetMaxCentripetalAcceleration',
      description: [
        'Sets the maximum centripetal acceleration of the live stream.',
        'Converts the units using the first axis of the stream.'
      ],
      route: 'device/stream_set_max_centripetal_acceleration',
      request: 'StreamSetMaxCentripetalAccelerationRequest',
      args: streamArgs,
      params: [
        {
          name: 'maxCentripetalAcceleration', type: 'double',
          description: 'Maximum centripetal acceleration at which any stream action is executed.',
        },
        {
          name: 'unit', type: 'AccelerationUnits',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of acceleration.',
        },
      ],
    },
    {
      name: 'ToString',
      description: 'Returns a string which represents the stream.',
      route: 'device/stream_to_string',
      request: 'StreamEmptyRequest',
      response: 'StringResponse',
      args: streamArgs,
      sync: true,
      override: true,
      const: true,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'String which represents the stream.',
    },
    {
      name: 'Disable',
      description: [
        'Disables the stream.',
        'If the stream is not setup, this command does nothing.',
        'Once disabled, the stream will no longer accept stream commands.',
        'The stream will process the rest of the commands in the queue until it is empty.',
      ],
      route: 'device/stream_disable',
      request: 'StreamEmptyRequest',
      args: streamArgs,
      params: [],
    },
    {
      name: 'GenericCommand',
      description: [
        'Sends a generic ASCII command to the stream.',
        'Keeps resending the command while the device rejects with AGAIN reason.',
      ],
      route: 'device/stream_generic_command',
      request: 'StreamGenericCommandRequest',
      args: streamArgs,
      params: [
        { name: 'command', type: 'string', description: 'Command and its parameters.' },
      ],
    },
    {
      name: 'GenericCommandBatch',
      description: [
        'Sends a batch of generic ASCII commands to the stream.',
        'Keeps resending command while the device rejects with AGAIN reason.',
        'The batch is atomic in terms of thread safety.'
      ],
      route: 'device/stream_generic_command_batch',
      request: 'StreamGenericCommandBatchRequest',
      args: streamArgs,
      params: [
        { name: 'batch', type: { type: 'array', of: 'string' }, description: 'Array of commands.' },
      ],
    },
    {
      name: 'CheckDisabled',
      description: [
        'Queries the stream status from the device',
        'and returns boolean indicating whether the stream is disabled.',
        'Useful to determine if streaming was interrupted by other movements.'
      ],
      route: 'device/stream_check_disabled',
      request: 'StreamEmptyRequest',
      response: 'BoolResponse',
      args: streamArgs,
      params: [],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if the stream is disabled.',
    },
    {
      name: 'treatDiscontinuitiesAsError',
      sync: true,
      description: [
        'Makes the stream throw StreamDiscontinuityException when it encounters discontinuities (ND warning flag).',
      ],
      route: 'device/stream_treat_discontinuities',
      request: 'StreamEmptyRequest',
      args: streamArgs,
      params: [],
    },
    {
      name: 'ignoreCurrentDiscontinuity',
      sync: true,
      description: [
        'Prevents StreamDiscontinuityException as a result of expected discontinuity when resuming streaming.',
      ],
      route: 'device/stream_ignore_discontinuity',
      request: 'StreamEmptyRequest',
      args: streamArgs,
      params: [],
    },
    {
      name: 'RetrieveAxes',
      description: 'Gets the axes of the stream.',
      route: 'device/stream_get_axes',
      request: 'StreamEmptyRequest',
      response: 'StreamGetAxesResponse',
      returnPropertyName: 'Axes',
      args: streamArgs,
      params: [],
      paramsCheck: [],
      returnType: { type: 'array', of: { type: 'ref', name: 'StreamAxisDefinition', namespace: 'ascii' } },
      returnDescription: 'An array of axis numbers of the axes the stream is set up to control.',
      sync: true,
      private: true,
    },
    {
      name: 'RetrieveMode',
      description: 'Get the mode of the stream.',
      route: 'device/stream_get_mode',
      request: 'StreamEmptyRequest',
      response: 'StreamModeResponse',
      returnPropertyName: 'StreamMode',
      args: streamArgs,
      params: [],
      paramsCheck: [],
      returnType: { type: 'enum', name: 'StreamMode', namespace: 'ascii' },
      returnDescription: 'Mode of the stream.',
      sync: true,
      private: true,
    },
    /* ZML-832 - Remove the following functions at a major release */
    {
      ...streamIoFunctions.waitDigitalInput,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.WaitDigitalInput instead.',
    },
    {
      ...streamIoFunctions.waitAnalogInput,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.WaitAnalogInput instead.',
    },
    {
      ...streamIoFunctions.setDigitalOutput,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.SetDigitalOutput instead.',
    },
    {
      ...streamIoFunctions.setAllDigitalOutputs,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.SetAllDigitalOutputs instead.',
    },
    {
      ...streamIoFunctions.setAnalogOutput,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.setAnalogOutput instead.',
    },
    {
      ...streamIoFunctions.setAllAnalogOutputs,
      args: streamArgs,
      obsoleteReason: 'Use Stream.Io.setAllAnalogOutputs instead.',
    }
  ],
  py: {
    forwardTypes: ['Device']
  },
  cpp: {
    emptyCtor: true,
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Streams',
      member: 'GetStream',
    },
    args: ['1'],
  }],
};

export default definition;
