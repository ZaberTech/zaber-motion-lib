import { StringConstantDefinitions } from '../../types';

import asciiSettings from './ascii_settings';

export const stringConstants: StringConstantDefinitions = {
  asciiSettings,
};
