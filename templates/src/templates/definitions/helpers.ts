import { LanguageValue, Value } from '../types';

export const defaultUnits: Value = {
  cs: 'Units.Native',
  py: 'Units.NATIVE',
  ts: 'Units.NATIVE',
  java: 'Units.NATIVE',
  cpp: 'Units::NATIVE',
  swift: 'Units.native',
};
export const defaultEmptyArray = (javaType: string): Value => ({
  cpp: '{}',
  // This is bad but there is not compile-time constant for empty arrays in C#.
  // The request DTOs and Go will tolerate the null.
  cs: 'null!',
  swift: '[]',
  java: `new ${javaType}[0]`,
  ts: '[]',
  py: '[]'
});
export const unitsValue = undefined;

export const defaultString = (value: string): LanguageValue => ({
  all: `"${value}"`,
  cs: `"${value}"`,
  py: `"${value}"`,
  java: `"${value}"`,
  cpp: `"${value}"`,
  swift: `"${value}"`,
  ts: `'${value}'`,
});

export const defaultNumber = (value: number): LanguageValue => ({
  all: `${value}`,
  cs: `${value}`,
  py: `${value}`,
  java: `${value}`,
  cpp: `${value}`,
  swift: `${value}`,
  ts: `${value}`,
});

export const defaultBool = (bool: boolean): LanguageValue => {
  if (bool) {
    return {
      all: 'true',
      py: 'True'
    };
  } else {
    return {
      all: 'false',
      py: 'False'
    };
  }
};

export const defaultPojo = (): LanguageValue => ({
  cpp: '{}',
  cs: 'null',
  java: 'null',
  py: 'None',
  swift: 'nil',
});

export const nullValue = (): LanguageValue => ({
  cpp: '{}',
  ts: 'null',
  cs: 'null',
  java: 'null',
  swift: 'nil',
  py: 'None',
});

export const defaultOptional = (): LanguageValue => ({
  cpp: '{}',
  cs: 'null',
  java: 'null',
  swift: 'nil',
  py: 'None',
});

// Use this to pass a literal boolean value as a protobuf arg (ie not a default value).
export const literalBool = (bool: boolean): LanguageValue => {
  if (bool) {
    return {
      cpp: 'true',
      swift: 'true',
      cs: 'true',
      java: 'true',
      ts: 'true',
      py: 'True'
    };
  } else {
    return {
      cpp: 'false',
      swift: 'false',
      cs: 'false',
      java: 'false',
      ts: 'false',
      py: 'False'
    };
  }
};
