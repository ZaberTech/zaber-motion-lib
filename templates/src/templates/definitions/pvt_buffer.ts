import { ClassDefinition, MethodArg } from '../types';

import { literalBool } from './helpers';

function genPvtBufferArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: 'Device.Connection.InterfaceId',
    },
    {
      name: 'Device',
      value: 'Device.DeviceAddress',
    },
    {
      name: 'BufferId',
      value: 'BufferId',
    },
    {
      name: 'Pvt',
      value: literalBool(true),
    },
  ];
}

const definition: ClassDefinition = {
  name: 'PvtBuffer',
  namespace: 'ascii',
  properties: [
    {
      name: 'Device',
      description: 'The Device this buffer exists on.',
      type: { type: 'ref', name: 'Device', namespace: 'ascii' },
    },
    {
      name: 'BufferId',
      description: 'The number identifying the buffer on the device.',
      type: 'int',
    },
  ],
  description: [
    'Represents a PVT buffer with this number on a device.',
    'A PVT buffer is a place to store a queue of PVT actions.'
  ],
  ctor: {},
  functions: [
    {
      name: 'GetContent',
      description: ['Gets the buffer contents as an array of strings.'],
      route: 'device/stream_buffer_get_content',
      request: 'StreamBufferGetContentRequest',
      response: 'StreamBufferGetContentResponse',
      args: genPvtBufferArgs(),
      params: [],
      returnType: { type: 'array', of: 'string' },
      returnPropertyName: 'BufferLines',
      returnDescription: 'A string array containing all the PVT commands stored in the buffer.',
    },
    {
      name: 'Erase',
      description: [
        'Erases the contents of the buffer.',
        'This method fails if there is a PVT sequence writing to the buffer.',
      ],
      route: 'device/stream_buffer_erase',
      request: 'StreamBufferEraseRequest',
      args: genPvtBufferArgs(),
      params: [],
    }
  ],
  py: {
    forwardTypes: ['Device']
  },
  instantiation: [{
    method: 'function',
    source: {
      namespace: 'ascii',
      class: 'Pvt',
      member: 'GetBuffer',
    },
    args: ['1'],
  }],
};

export default definition;
