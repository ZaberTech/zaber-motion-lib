import { MethodArg, MethodDefinition, OptionalMethodParam } from '../types';

import { defaultBool, defaultOptional } from './helpers';

type GetSetTemplate = {
  /** A description of this product (device, axis, process, etc) */
  name: string;
  /** The arguments to pass. Should be an AxisEmptyRequest. */
  args: MethodArg[];
};

export function getState(template: GetSetTemplate): MethodDefinition {
  return {
    name: 'GetState',
    description: `Returns a serialization of the current ${template.name} state that can be saved and reapplied.`,
    route: 'device/get_state',
    request: 'AxisEmptyRequest',
    response: 'StringResponse',
    args: template.args,
    params: [],
    returnType: 'string',
    returnDescription: `A serialization of the current state of the ${template.name}.`,
    returnPropertyName: 'Value',
  };
}

export function setState(template: GetSetTemplate): MethodDefinition {
  const targetsAxis = template.args.some(arg => arg.name === 'Axis');
  const options: OptionalMethodParam[] | undefined = targetsAxis ? undefined : [{
    name: 'DeviceOnly',
    description: 'If true, only device scope settings and features will be set.',
    type: 'bool',
    defaultValue: defaultBool(false),
  }];
  return {
    name: 'SetState',
    description: `Applies a saved state to this ${template.name}.`,
    route: targetsAxis ? 'device/set_axis_state' : 'device/set_device_state',
    request: 'SetStateRequest',
    args: template.args,
    params: [{
      name: 'State', type: 'string',
      description: `The state object to apply to this ${template.name}.`
    }],
    options,
    returnType: { type: 'ref', name: targetsAxis ? 'SetStateAxisResponse' : 'SetStateDeviceResponse', namespace: 'ascii' },
    returnDescription: 'Reports of any issues that were handled, but caused the state to not be exactly restored.'
  };
}

export function canSetState(template: GetSetTemplate): MethodDefinition {
  const targetsAxis = template.args.some(arg => arg.name === 'Axis');
  const responseTemplate: Partial<MethodDefinition> = targetsAxis ? {
    route: 'device/can_set_axis_state',
    response: 'CanSetStateAxisResponse',
    returnType: { type: 'optional', of: 'string' },
    returnDescription: `An explanation of why this state cannot be set to this ${template.name}.`,
    returnPropertyName: 'Error',
  } : {
    route: 'device/can_set_state',
    returnDescription: 'An object listing errors that come up when trying to set the state.',
    returnType: { type: 'ref', name: 'CanSetStateDeviceResponse', namespace: 'ascii' },
  };
  return {
    name: 'CanSetState',
    hidden: true,
    description: [
      `Checks if a state can be applied to this ${template.name}.`,
      'This only covers exceptions that can be determined statically such as mismatches of ID or version,',
      'the process of applying the state can still fail when running.'
    ],
    request: 'CanSetStateRequest',
    args: template.args,
    params: [{
      name: 'State', type: 'string',
      description: 'The state object to check against.'
    }],
    options: [{
      name: 'FirmwareVersion',
      description: [
        'The firmware version of the device to apply the state to.',
        'Use this to ensure the state will still be compatible after an update.'
      ],
      type: { type: 'optional', of: { type: 'ref', name: 'FirmwareVersion', namespace: [] } },
      defaultValue: defaultOptional(),
    }],
    ...responseTemplate,
  };
}
