import { ClassDefinition, MethodArg, MethodDefinition } from '../types';

import { defaultUnits, unitsValue } from './helpers';

function genArgs(): MethodArg[] {
  return [
    {
      name: 'InterfaceId',
      value: {
        cs: '_axis.Device.Connection.InterfaceId',
        py: 'self._axis.device.connection.interface_id',
        ts: 'this._axis.device.connection.interfaceId',
        java: 'this.axis.getDevice().getConnection().getInterfaceId()',
        cpp: 'this->_axis.getDevice().getConnection().getInterfaceId()',
        swift: 'self.axis.device.connection.interfaceId',
      }
    },
    {
      name: 'Device',
      value: {
        cs: '_axis.Device.DeviceAddress',
        py: 'self._axis.device.device_address',
        ts: 'this._axis.device.deviceAddress',
        java: 'this.axis.getDevice().getDeviceAddress()',
        cpp: 'this->_axis.getDevice().getDeviceAddress()',
        swift: 'self.axis.device.deviceAddress',
      }
    },
    {
      name: 'Axis',
      value: {
        cs: '_axis.AxisNumber',
        py: 'self._axis.axis_number',
        ts: 'this._axis.axisNumber',
        java: 'this.axis.getAxisNumber()',
        cpp: 'this->_axis.getAxisNumber()',
        swift: 'self.axis.axisNumber',
      }
    },
  ];
}

export function generateCommonSettingsMethods(deviceOrAxis: 'axis' | 'device', genCommonArgs: () => MethodArg[]): MethodDefinition[] {
  return [
    {
      name: 'Get',
      description: [
        `Returns any ${deviceOrAxis} setting or property.`,
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).',
      ],
      route: 'device/get_setting',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Setting value.',
    },
    {
      name: 'Set',
      description: [
        `Sets any ${deviceOrAxis} setting.`,
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).',
      ],
      route: 'device/set_setting',
      request: 'DeviceSetSettingRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'value', type: 'double',
          description: 'Value of the setting.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of setting.',
        },
      ],
    },
    {
      name: 'GetString',
      description: [
        `Returns any ${deviceOrAxis} setting or property as a string.`,
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).',
      ],
      route: 'device/get_setting_str',
      request: 'DeviceGetSettingRequest',
      response: 'StringResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'Setting value.',
    },
    {
      name: 'SetString',
      description: [
        `Sets any ${deviceOrAxis} setting as a string.`,
        'For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).',
      ],
      route: 'device/set_setting_str',
      request: 'DeviceSetSettingStrRequest',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'value', type: 'string',
          description: 'Value of the setting.',
        },
      ],
    },
    {
      name: 'ConvertToNativeUnits',
      description: 'Convert arbitrary setting value to Zaber native units.',
      route: 'device/convert_setting',
      request: 'DeviceConvertSettingRequest',
      response: 'DoubleResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'value', type: 'double',
          description: 'Value of the setting in units specified by following argument.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          description: 'Units of the value.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Setting value.',
      sync: true,
    },
    {
      name: 'ConvertFromNativeUnits',
      description: 'Convert arbitrary setting value from Zaber native units.',
      route: 'device/convert_setting',
      request: 'DeviceConvertSettingRequest',
      response: 'DoubleResponse',
      args: [
        ...genCommonArgs(),
        {
          name: 'FromNative',
          value: {
            cpp: 'true',
            cs: 'true',
            java: 'true',
            ts: 'true',
            py: 'True',
            swift: 'true',
          }
        }
      ],
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'value', type: 'double',
          description: 'Value of the setting in Zaber native units.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          description: 'Units to convert value to.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Setting value.',
      sync: true,
    },
    {
      name: 'GetDefault',
      description: 'Returns the default value of a setting.',
      route: 'device/get_setting_default',
      request: 'DeviceGetSettingRequest',
      response: 'DoubleResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
        {
          name: 'unit', type: 'Units',
          value: unitsValue,
          defaultValue: defaultUnits,
          description: 'Units of setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'double',
      returnDescription: 'Default setting value.',
      sync: true,
    },
    {
      name: 'GetDefaultString',
      description: 'Returns the default value of a setting as a string.',
      route: 'device/get_setting_default_str',
      request: 'DeviceGetSettingRequest',
      response: 'StringResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'string',
      returnDescription: 'Default setting value.',
      sync: true,
    },
    {
      name: 'CanConvertNativeUnits',
      description: 'Indicates if given setting can be converted from and to native units.',
      route: 'device/can_convert_setting',
      request: 'DeviceGetSettingRequest',
      response: 'BoolResponse',
      args: genCommonArgs(),
      params: [
        {
          name: 'setting', type: 'string',
          description: 'Name of the setting.',
        },
      ],
      returnPropertyName: 'Value',
      returnType: 'bool',
      returnDescription: 'True if unit conversion can be performed.',
      sync: true,
    },
  ];
}

const definition: ClassDefinition = {
  name: 'AxisSettings',
  namespace: 'ascii',
  properties: [
    {
      name: 'Axis',
      type: { type: 'ref', name: 'Axis', namespace: 'ascii' },
      private: true,
    },
  ],
  description: 'Class providing access to various axis settings and properties.',
  ctor: { },
  functions: [
    ...generateCommonSettingsMethods('axis', genArgs),
    {
      name: 'SetCustomUnitConversions',
      description: [
        'Overrides default unit conversions.',
        'Conversion factors are specified by setting names representing underlying dimensions.',
      ],
      minFw: [{ major: 7, minor: 30 }],
      route: 'device/set_unit_conversions',
      request: 'DeviceSetUnitConversionsRequest',
      args: genArgs(),
      params: [
        {
          name: 'conversions',
          type: { type: 'array', of: { type: 'ref', name: 'ConversionFactor', namespace: 'ascii' } },
          description: 'Factors of all conversions to override.',
        },
      ],
      hidden: true,
    },
    {
      name: 'GetMany',
      description: 'Gets many setting values in as few requests as possible.',
      route: 'device/get_many_settings',
      request: 'DeviceMultiGetSettingRequest',
      response: 'GetAxisSettingResults',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'AxisSettings',
        type: { type: 'array', of: { type: 'ref', name: 'GetAxisSetting', namespace: 'ascii' } },
        description: 'The settings to read.',
      },
      returnPropertyName: 'Results',
      returnType: { type: 'array', of: { type: 'ref', name: 'GetAxisSettingResult', namespace: 'ascii' } },
      returnDescription: 'The setting values read.',
    },
    {
      name: 'GetSynchronized',
      description: 'Gets many setting values in the same tick, ensuring their values are synchronized.',
      minFw: [{ major: 7, minor: 35 }],
      route: 'device/get_sync_settings',
      request: 'DeviceMultiGetSettingRequest',
      response: 'GetAxisSettingResults',
      args: genArgs(),
      params: [],
      variadic: {
        name: 'AxisSettings',
        type: { type: 'array', of: { type: 'ref', name: 'GetAxisSetting', namespace: 'ascii' } },
        description: 'The settings to read.',
      },
      returnPropertyName: 'Results',
      returnType: { type: 'array', of: { type: 'ref', name: 'GetAxisSettingResult', namespace: 'ascii' } },
      returnDescription: 'The setting values read.',
    },
  ],
  py: {
    forwardTypes: ['Axis']
  },
  instantiation: [{
    method: 'property',
    source: {
      namespace: 'ascii',
      class: 'Axis',
      member: 'Settings',
    },
  }],
};

export default definition;
