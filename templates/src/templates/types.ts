import { Namespace } from '../common_types';

import type { AnyType } from './type_system';

export type Language = 'ts' | 'cs' | 'py' | 'java' | 'cpp' | 'swift';

export interface LanguageCustom {
  ts: string;
  py: string;
  cs: string;
  java: string;
  cpp: string;
  swift: string;
}

export interface LanguageValue extends Partial<LanguageCustom> {
  all?: string;
}

export type Value = LanguageValue | string;

export type TypeDefinition = AnyType;

export type Description = string | string[];

export interface WithName {
  name: string;
}

export interface QualifiedName {
  namespace: string;
  class: string;
  member?: string;
}

export interface InstantiationSource {
  source: QualifiedName;
  method: 'static' | 'function' | 'property' | 'constructor';
  args?: (QualifiedName | string)[];
}

export interface Version {
  major: number;
  minor: number;
}

export interface ClassDefinition {
  name: string;
  description: Description;
  namespace: Namespace;
  minFw?: Version[];

  ctor: {
    code?: Partial<LanguageCustom>;
    public?: boolean;
    description?: Description;
    canThrow?: boolean;
  } | false;

  functions: MethodDefinition[];
  properties: PropertyDefinition[];
  constants?: ConstantDefinition[];
  events?: Record<string, EventDefinition>;

  py?: {
    forwardTypes?: string[];
  };
  cpp?: {
    noCopyCtor?: true;
    emptyCtor?: true;
  };

  instantiation?: InstantiationSource[];
  baseClass?: string;
}

/** for Python with async */
export interface AsyncContextManager {
  name: string;
  description: string;
  /** An async function to be called to close the created resource */
  closer: string;
}

export interface ConstantDefinition {
  name: string;
  description: Description;
  type: TypeDefinition;
  dtoNamespace?: Namespace; // for handling ambiguous DTO typenames in different namespaces
  value: Value;
}

export interface PropertyDefinition {
  name: string;
  description?: Description;
  type: TypeDefinition;
  dtoNamespace?: Namespace; // for handling ambiguous DTO typenames in different namespaces
  minFw?: Version[];
  private?: boolean;
  value?: Partial<LanguageCustom> & { ctorArgument?: true };
  transform?: Partial<LanguageCustom>;
  getter?: Partial<LanguageCustom>;
  setter?: LanguageCustom;
  getterCanThrow?: boolean;
  setterCanThrow?: boolean;
  getterFunctionName?: string; // getter or getterFunction can throw
  referencesSelf?: boolean; // in Swift, self cannot be referenced from constructor call
  cpp?: {
    copyCtorValue?: string;
    emptyCtorValue?: string;
  };
}

export interface EventDefinition {
  name: string;
  description: Description;
  emitType: TypeDefinition;
  emitTypeDescription: Description;
}

export interface MethodArg {
  name: string;
  value: Value;
  type?: TypeDefinition;
}

export interface MethodParam {
  /** The name of the passed parameter */
  name: string;
  description: Description;
  type: TypeDefinition;
  /** The name of the field in the protobuf request to write this param to */
  requestName?: string;
  /** Custom value to write to the protobuf request */
  value?: Value;
  defaultValue?: Value;
  minFw?: Version[];
}

export interface OptionalMethodParam extends MethodParam {
  defaultValue: Value;
}

export interface MethodParamCheck {
  code: LanguageCustom;
  message: string;
}

export interface ExceptionDefinition {
  type: string;
  description: string;
}

export interface MethodDefinition {
  name: string;
  description: Description;
  hidden?: true;
  minFw?: Version[];

  route?: string;
  request?: string;
  response?: string;

  /** Any arguments for the method to set on the protobuf request before sending */
  args?: MethodArg[];
  /** The parameters this method takes as input */
  params: MethodParam[];
  /**
   * The optional parameters this function takes as input.
   * These will be positional but optional, named, or in a property object,
   * depending on the language
   * */
  options?: OptionalMethodParam[];
  /** The type of variadic param to expect. Should always be an array. */
  variadic?: MethodParam;
  paramsCheck?: MethodParamCheck[];

  returnType?: TypeDefinition;
  returnPropertyName?: string;
  returnDescription?: string;
  customReturn?: LanguageCustom;

  code?: LanguageCustom;

  sync?: true;
  static?: true;
  override?: true;
  private?: true;
  const?: true;

  obsoleteReason?: string;

  py?: {
    asyncContextManager?: AsyncContextManager;
  };

  noThrow?: true;
}

export interface StringConstantValue {
  name: string;
  description: Description;
  value: string;
}

export interface StringConstantDefinition {
  name: string;
  description: Description;
  namespace: Namespace;
  values: StringConstantValue[];
}

export type StringConstantDefinitions = Record<string, StringConstantDefinition>;

export interface ExceptionBasicDefinition {
  description: Description;
  customDataPojo?: string;
  baseException?: string;
  isInheritable?: boolean;
}

export interface ExceptionNameDefinition extends ExceptionBasicDefinition {
  pbNames: LanguageCustom;
  name: string;
  customDataPojo?: string;
  baseException: string;
}
