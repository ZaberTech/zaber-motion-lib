import { Context } from '../context';
import { ExceptionNameDefinition } from '../types';

export default function({definitions, exception, exceptionCustomDataType, _}: {exception: ExceptionNameDefinition} & Context)
/* template */
<%- include('generated.ejs') %>

import { <%- exception.baseException %> } from './<%- _.snakeCase(exception.baseException) %>';
<% const dataPojo = exceptionCustomDataType(definitions.exceptions, exception); -%>
<% if (dataPojo) { -%>
import { <%- dataPojo %> } from './<%- _.snakeCase(dataPojo) %>';
<% } -%>

<%- include('jsdoc_simple.ejs', {type: exception, indent: 0}) %>
export class <%- exception.name %> extends <%- exception.baseException %> {
<% if (exception.customDataPojo) { -%>
  /**
   * Additional data for <%- exception.name %>
   */
  public get details(): <%- dataPojo %> {
    return this._details;
  }
  private _details: <%- dataPojo %>;
<% } -%>
  constructor(
    message: string<% if (dataPojo) { -%>,
    customData: Uint8Array | <%- dataPojo %><% } %>
  ) {
<% if (dataPojo && !exception.customDataPojo) { -%>
    super(message, customData);
<% } else { -%>
    super(message);
<% } -%>
    Object.setPrototypeOf(this, <%- exception.name %>.prototype);
<% if (exception.customDataPojo) { -%>

    if (customData instanceof Uint8Array) {
      this._details = <%- dataPojo %>.fromBinary(customData);
    } else {
      this._details = customData;
    }
<% } -%>
  }
}
