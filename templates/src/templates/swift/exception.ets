import { Context } from '../context';
import { ExceptionNameDefinition } from '../types';

export default function({exception, exceptionCustomDataType, definitions}: {exception: ExceptionNameDefinition} & Context)
/* template */
<%- include('generated.ejs') %>
<% const dataPojo = exceptionCustomDataType(definitions.exceptions, exception); -%>
<% const isFinal = exception.isInheritable !== true; -%>

import Foundation

<%- include('swiftdoc_summary.ejs', {type: exception, indent: 0, isFunction: false}) %>
public <%- isFinal ? 'final ' : '' %>class <%- exception.name %>: <%- exception.baseException %>, @unchecked Sendable {
<% if (exception.customDataPojo) { -%>

    /**
     Additional data for <%- exception.name %>.
     */
    public let details: <%- exception.customDataPojo %>

    /**
     Initializes a new instance of the <%- exception.name %> class.

     - Parameters:
        - message: Message from the native library.
        - customData: Raw unparsed custom data from the native library for the exception.
     */
    public init(message: String, customData: Data) throws {
        self.details = try <%- exception.customDataPojo %>.fromByteArray(customData);
        super.init(message: message)
    }

    /**
     Initializes a new instance of the <%- exception.name %> class.

     - Parameters:
        - message: Message from the native library.
        - customData: Additional data about the exception.
     */
    public init(message: String, customData: <%- dataPojo %>) {
        self.details = customData;
        super.init(message: message)
    }
<% } else if (dataPojo) { -%>

    /**
     Initializes a new instance of the <%- exception.name %> class.

     - Parameters:
        - message: Message from the native library.
        - customData: Raw unparsed custom data from the native library for the exception.
     */
    public override init(message: String, customData: Data) throws {
        try super.init(message: message, customData: customData)
    }

    /**
     Initializes a new instance of the <%- exception.name %> class.

     - Parameters:
        - message: Message from the native library.
        - customData: Additional data about the exception.
     */
    public override init(message: String, customData: <%- dataPojo %>) {
        super.init(message: message, customData: customData)
    }
<% } else { -%>

    /**
     Initializes a new instance of the <%- exception.name %> class.

     - Parameters:
        - message: Message from the native library.
     */
    public override init(message: String) {
        super.init(message: message)
    }
<% } -%>
}
