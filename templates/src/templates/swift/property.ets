import { Context } from '../context';
import { MethodDefinition, PropertyDefinition } from '../types';

export default function({prop, swift, getterFunction}: {prop: PropertyDefinition, getterFunction?: MethodDefinition} & Context)
/* template */
<% if (!prop.private) { -%>
<%- include('swiftdoc_summary.ejs', {type: prop, indent: 1, isFunction: false}) %>
<% const getterCanThrow = prop.getterCanThrow === true -%>
<% const setter = prop.setter?.swift != null -%>
<%      if (prop.getter?.swift || getterFunction) { -%>
<% const getter = prop.getter?.swift != null ? prop.getter!.swift : `${swift.fnName(getterFunction!)}()`; -%>
<%          if (getterCanThrow && setter) { -%>
    public func get<%- prop.name %>() throws -> <%- swift.propType(prop) %> {
        return try <%- getter %>
    }

<% const setterCanThrow = prop.setterCanThrow === true; -%>
<%- include('swiftdoc_summary.ejs', {type: prop, indent: 1, isFunction: false}) %>
    public func set<%- prop.name %>(_ value: <%- swift.propType(prop) %>) <%- setterCanThrow ? 'throws ' : ''%>{
        <%- setterCanThrow ? 'try ' : ''%><%- prop.setter!.swift %>
    }
<%          } else { -%>
    public var <%- swift.propName(prop) %>: <%- swift.propType(prop) %> {
        get <%- getterCanThrow ? 'throws ' : ''%>{ return <%- getterCanThrow ? 'try ' : ''%><%- getter %> }
    }
<%          } -%>
<%      } else { -%>
<% const lazyValue = prop.referencesSelf === true; -%>
    public <%- lazyValue ? 'private(set) lazy var ' : 'let ' %><%- swift.propName(prop) %>: <%- swift.propType(prop) %><%- lazyValue ? ` = ${prop.value!.swift}` : '' %>
<%      } -%>
<% } else { -%>
    private let <%- swift.propName(prop) %>: <%- swift.propType(prop) %>
<% } -%>
