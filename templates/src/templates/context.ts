import fs from 'fs';
import os from 'os';

import _ from 'lodash';
import { glob } from 'glob';
import { ensureArray } from '@zaber/toolbox';
import { P, match, isMatching } from 'ts-pattern';

import { Namespace } from '../common_types';

import { definitions } from './definitions';
import {
  ClassDefinition, ConstantDefinition, EventDefinition, ExceptionNameDefinition, Language, MethodArg, MethodDefinition, MethodParam,
  PropertyDefinition, TypeDefinition, Value, Version, WithName
} from './types';
import { getBaseType, isCollectionType, isEnumType, isOptionalType, isUnitsType, WithNamespace, isReferenceType } from './type_system';

type MethodOrProperty = MethodParam | PropertyDefinition;

export interface AllTemplates {
  mapType: (type: TypeDefinition) => string;
  typeName(type: WithName): string;
  constName(type: WithName): string;
  propName(prop: WithName): string;
  fnName(fn: MethodDefinition): string;
  eventName(event: EventDefinition): string;
  paramName(param: MethodOrProperty): string;
  paramType: (param: MethodOrProperty, variadic?: boolean) => string;
  paramDefaultValue(param: MethodParam): string;
  defaultInSignature(param: MethodParam): string;
  variadicSymbol(variadic: boolean | MethodParamType): string;
  addGetter?(value: string): string;
  namespace?(type: WithNamespace): string;
  optionsName?(fn: MethodDefinition, className?: string): string;
  instanceName(type: WithName): string;
  importStatement(type: WithName & WithNamespace): string;
  varDecl(typeName: WithName, typeDef: TypeDefinition): string;
  newKeyword(): string;
  statementTerminator(): string;
  quoteMark(): string;
  arrayLiteral(data: string, elementType: string): string;
}

type TemplateEtc = AllTemplates & Record<string, unknown>;

function templateHasOptions(template: AllTemplates): template is AllTemplates & { optionsName: NonNullable<AllTemplates['optionsName']> } {
  return 'optionsName' in template;
}

function getValue(value: Value | undefined, language?: Language): string | undefined {
  if (language) {
    return typeof value === 'object' ? value[language] : undefined;
  }
  return typeof value === 'object' ? value.all : value;
}

function indent(str: string, indentation: string): string {
  return indentation + str.replace(/\n/, `\n${indentation}`);
}

function versionToStr(v: Version): string {
  return `${v.major}.${v.minor.toString().padStart(2, '0')}`;
}

function getLines(strArrayOrStr: string | string[] | undefined, minFw?: Version[]): string[] {
  const result: string[] = [];
  switch (typeof strArrayOrStr) {
    case 'undefined': break;
    case 'string': result.push(strArrayOrStr); break;
    default: result.push(...strArrayOrStr); break;
  }

  if (minFw?.length) {
    result.push(`Requires at least Firmware ${minFw.map(versionToStr).join(' or ')}.`);
  }

  return result;
}

function isFuncWithOptions(fn: MethodDefinition): fn is MethodDefinition & { options: NonNullable<MethodDefinition['options']> } {
  return fn.options != null && fn.options.length > 0;
}
function isFuncVariadic(fn: MethodDefinition): fn is MethodDefinition & { variadic: NonNullable<MethodDefinition['variadic']> } {
  return fn.variadic != null;
}

function nonVariadicParams(fn: MethodDefinition) {
  return [...fn.params, ...(fn.options ?? [])];
}
function allParams(fn: MethodDefinition) {
  const nonVariadics = nonVariadicParams(fn);
  return fn.variadic ? [...nonVariadics, fn.variadic] : nonVariadics;
}

export type MethodParamType = 'normal' | 'option' | 'variadic';

function mapParams<T>(
  fn: MethodDefinition, mapper: (param: MethodParam, type: MethodParamType, index: number, subindex: number) => T | null
): T[] {
  let index = 0;
  let subindex = 0;
  const mappedList: T[] = [];
  for (const param of fn.params) {
    const mapped = mapper(param, 'normal', index, subindex);
    if (mapped != null) { mappedList.push(mapped) }
    index++;
    subindex++;
  }
  subindex = 0;
  for (const param of fn.options ?? []) {
    const mapped = mapper(param, 'option', index, subindex);
    if (mapped != null) { mappedList.push(mapped) }
    index++;
    subindex++;
  }
  subindex = 0;
  if (fn.variadic) {
    const mapped = mapper(fn.variadic, 'variadic', index, subindex);
    if (mapped != null) { mappedList.push(mapped) }
  }
  return mappedList;
}

function isInCtor(prop: PropertyDefinition, language: Language): boolean {
  return (!prop.value?.[language] || prop.value?.ctorArgument === true) && !prop.getter?.[language] && !prop.getterFunctionName;
}

const convertTitle = (str?: string): string => _.upperFirst(str);
const convertCamel = (str?: string) => _.camelCase(str);
const convertSnake = (str?: string) => _.snakeCase(str);
const convertLower = (str?: string) => _.lowerCase(str);

const csharpTypeMap: TypeMap = {
  uint: 'long', // For serial numbers. Unsigned types other than byte are not CLS compliant.
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

const swiftTypeMap: TypeMap = {
  int: 'Int',
  uint: 'UInt',
  double: 'Double',
  string: 'String',
  bool: 'Bool',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

const pythonTypeMap: TypeMap = {
  uint: 'int',
  double: 'float',
  string: 'str',
  LengthUnits: 'LengthUnits',
  VelocityUnits: 'VelocityUnits',
  TimeUnits: 'TimeUnits',
  FrequencyUnits: 'FrequencyUnits',
  AccelerationUnits: 'AccelerationUnits',
  Units: 'UnitsAndLiterals',
};

const pythonGenericFunctionsMap: Record<string, string> = {
  ToString: '__repr__'
};

const tsTypeMap: TypeMap = {
  int: 'number',
  uint: 'number',
  double: 'number',
  bool: 'boolean',
  LengthUnits: 'Length | Angle | Native',
  VelocityUnits: 'Velocity | AngularVelocity | Native',
  TimeUnits: 'Time | Native',
  FrequencyUnits: 'Frequency | Native',
  AccelerationUnits: 'Acceleration | AngularAcceleration | Native',
};

const javaTypeMap: TypeMap = {
  uint: 'long',
  string: 'String',
  bool: 'boolean',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

const javaPrimitiveToObjectMap: TypeMap = {
  'int': 'Integer',
  'double': 'Double',
  'boolean': 'Boolean',
  'long': 'Long',
  'int[]': 'Integer[]',
  'double[]': 'Double[]',
  'boolean[]': 'Boolean[]',
  'long[]': 'Long[]',
};

const matlabTypeMap: TypeMap = {
  uint: 'int64',
  string: 'char',
  bool: 'logical',
  double: 'double',
  int: 'int32',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

const cppTypeMap: TypeMap = {
  uint: 'unsigned int',
  string: 'std::string',
  bool: 'bool',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

type TypeMap = Record<string, string>;

function getTypeName(type: TypeDefinition): string {
  const baseType = getBaseType(type);
  if (typeof baseType === 'string') {
    return baseType;
  } else {
    return baseType.name;
  }
}

function argMap(self: string, formatFn: (type: string) => string, input: string, separator: string = '.', selfSeparator?: string): string {
  return (self ? self + (selfSeparator ?? separator) : '') + input.split('.').map(formatFn).join(separator);
}

function reformatArrayLiteral(data: string, prefix: string, suffix: string): string {
  const items = data.match(/(?:\s*([^,]+)\s*)+/g) ?? [];
  return `${prefix}${items?.map(match => match.trim()).join(', ')}${suffix}`;
}

const csharp = {
  docEscape: (str: string) => {
    const symbolFix = str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    const linkFix = symbolFix.replace(
      /\[([^\]]+)\]\(([^\)]+)\)/g, // eslint-disable-line no-useless-escape
      '<see href="$2">$1</see>'
    );
    return linkFix;
  },
  mapType: (type: TypeDefinition): string => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
        .with({ type: 'set' }, ({ of }) => `ISet<${mapType(of)}>`)
        .with({ type: 'optional' }, ({ of }) => {
          if (isCollectionType(of)) {
            return mapType(of);
          }
          return `${mapType(of)}?`;
        })
        .with(P.string, type => csharpTypeMap[type] || type)
        .exhaustive();
    }
    return mapType(type);
  },
  namespace: (type: WithNamespace) =>
    `Zaber.Motion${ensureArray(type.namespace).map(n => `.${convertTitle(n)}`).join('')}`,
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'DeviceIdLegacy' :
    type.name === 'Peripheralid' ? 'PeripheralIdLegacy' :
    convertTitle(type.name)
  ),
  propName: (prop: PropertyDefinition) =>
    (prop as PropertyDefinition).private === true ? `_${convertCamel(prop.name)}` : convertTitle(prop.name),
  propType: (prop: PropertyDefinition | ConstantDefinition) => csharp.mapType(prop.type),
  eventName: (event: EventDefinition) => convertTitle(event.name),
  fnName: (fn: MethodDefinition) => convertTitle(fn.name),
  returnType: (fn: MethodDefinition, sync?: boolean) => {
    let type = '';
    if (fn.returnType) {
      type = csharp.mapType(fn.returnType);
    } else if (fn.response) {
      type = fn.response;
    }

    if (sync ?? fn.sync) {
      return (type ? type : 'void');
    } else {
      return `Task${type ? `<${type}>` : ''}`;
    }
  },
  paramName: (param: MethodOrProperty) => convertCamel(param.name),
  paramType: (param: MethodOrProperty): string => csharp.mapType(param.type),
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'cs') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam) => {
    const defaultValue: string = csharp.paramDefaultValue(param);
    return defaultValue ? ` = ${defaultValue}` : '';
  },
  paramSignature: (param: MethodParam, variadic: MethodParamType = 'normal') => (
    `${csharp.variadicSymbol(variadic)}${csharp.paramType(param)} ${csharp.paramName(param)}${csharp.defaultInSignature(param)}`
  ),
  allParamSignatures: (fn: MethodDefinition) => mapParams(fn, (param, type) => csharp.paramSignature(param, type)).join(', '),
  argRequest: (arg: MethodArg) => convertTitle(arg.name),
  argValue: (arg: MethodArg) => getValue(arg.value, 'cs') ?? argMap('', convertTitle, getValue(arg.value)!),
  paramRequest: (param: MethodParam) => convertTitle(param.requestName) || convertTitle(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'cs');
    if (value) { return value }

    value = convertTitle(getValue(param.value));
    if (value) { return value }

    const name = convertCamel(param.name);
    return name;
  },
  indent: (code: string) => indent(code, '            '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? 'params ' : '',
  instanceName: (type: WithName) => _.lowerFirst(type.name),
  importStatement: (type: WithName & WithNamespace): string => `using ${csharp.namespace(type)};`,
  protoProperty: (fn: MethodDefinition) => fn.returnPropertyName,
  varDecl: () => 'var',
  newKeyword: () => 'new',
  statementTerminator: () => ';',
  quoteMark: () => '"',
  arrayLiteral: (data: string, elementType: string) => reformatArrayLiteral(data, `new ${elementType}[] { `, ' }'),
} satisfies TemplateEtc;

export type Csharp = typeof csharp;

const python = {
  docEscape: (str: string) => str,
  mapType: (type: TypeDefinition, parentType?: ClassDefinition): string => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => {
          if (parentType?.py?.forwardTypes?.includes(name)) {
            return `'${name}'`;
          } else {
            return name;
          }
        })
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `List[${mapType(of)}]`)
        .with({ type: 'set' }, ({ of }) => `Set[${mapType(of)}]`)
        .with({ type: 'optional' }, ({ of }) => `Optional[${mapType(of)}]`)
        .with(P.string, type => pythonTypeMap[type] || type)
        .exhaustive();
    }

    return mapType(type);
  },
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'DEVICE_ID_LEGACY' :
    type.name === 'Peripheralid' ? 'PERIPHERAL_ID_LEGACY' :
    _.snakeCase(type.name).toUpperCase()
  ),
  propName: (prop: PropertyDefinition) => convertSnake(prop.name),
  propType: (prop: PropertyDefinition, parentType: ClassDefinition) => python.mapType(prop.type, parentType),
  eventName: (event: EventDefinition) => convertSnake(event.name),
  fnName: (fn: MethodDefinition) => {
    let name = pythonGenericFunctionsMap[fn.name];
    if (name) {
      return name;
    }
    name = convertSnake(fn.name);
    if (fn.private) {
      name = `__${name}`;
    }
    return name;
  },
  returnType: (fn: MethodDefinition, parentType: ClassDefinition, isAsync?: boolean) => {
    if (isAsync && fn.py?.asyncContextManager) {
      return `'${fn.py?.asyncContextManager.name}'`;
    } else if (fn.returnType) {
      return python.mapType(fn.returnType, parentType);
    } else if (fn.response) {
      return `main_pb2.${fn.response}`;
    } else {
      return 'None';
    }
  },
  paramName: (param: MethodOrProperty) => convertSnake(param.name),
  paramType: (param: MethodOrProperty, variadic = false, parentType?: ClassDefinition): string => (
    python.mapType(variadic ? getBaseType(param.type) : param.type, parentType)
  ),
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'py') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam) => {
    const defaultValue: string = python.paramDefaultValue(param);
    return defaultValue ? ` = ${defaultValue}` : '';
  },
  argRequest: (arg: MethodArg) => convertSnake(arg.name),
  argValue: (arg: MethodArg) => getValue(arg.value, 'py') ?? argMap('self', convertSnake, getValue(arg.value)!),
  paramRequest: (param: MethodParam) => convertSnake(param.requestName) || convertSnake(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'py');
    if (value) { return value }

    value = convertSnake(getValue(param.value));
    if (value) { return value }

    return convertSnake(param.name);
  },
  paramSignature: (param: MethodParam, variadic = false) => (
    `${python.variadicSymbol(variadic)}${python.paramName(param)}: ${python.paramType(param, variadic)}${python.defaultInSignature(param)}`
  ),
  allParamSignatures: (fn: MethodDefinition) => {
    const indent = '            ';
    const selfSignature = fn.static ? [] : [`${indent}self`];
    const paramSignatures = mapParams(fn, (param, type) => indent + python.paramSignature(param, type === 'variadic'));
    const signatures = [...selfSignature, ...paramSignatures];
    return signatures.length > 0 ? `${os.EOL}${signatures.join(`,${os.EOL}`)}${os.EOL}    ` : '';
  },
  indent: (code: string) => indent(code, '        '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? '*' : '',
  instanceName: (type: WithName) => convertSnake(type.name),
  importStatement: (type: WithName & WithNamespace): string => {
    const namespace = ensureArray(type.namespace).map(n => `.${convertSnake(n)}`).join('');
    return `from zaber_motion${namespace} import ${python.typeName(type)}`;
  },
  protoProperty: (fn: MethodDefinition) => convertSnake(fn.returnPropertyName),
  varDecl: () => '',
  newKeyword: () => '',
  statementTerminator: () => '',
  quoteMark: () => '\'',
  arrayLiteral: (data: string) => reformatArrayLiteral(data, '[', ']'),
} satisfies TemplateEtc;

export type Python = typeof python;

const ts = {
  docEscape: (str: string) => str,
  mapType: (type: TypeDefinition, context?: 'returnType') => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: P.union('array', 'set') }, ({ type, of }) => {
          let mapped = mapType(of);
          if (mapped.includes('|')) {
            mapped = `(${mapped})`;
          }
          if (type === 'array') {
            return `${mapped}[]`;
          } else {
            return `Set<${mapped}>`;
          }
        })
        .with({ type: 'optional' }, ({ of }) => `${mapType(of)} | null`)
        .with(P.string, type => tsTypeMap[type] || type)
        .exhaustive();
    }
    let typeToMap = type;
    if (context !== 'returnType') {
      // Optionality is handled by the property or the argument.
      typeToMap = isOptionalType(type) ? type.of : type;
    }
    return mapType(typeToMap);
  },
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'DEVICE_ID_LEGACY' :
    type.name === 'Peripheralid' ? 'PERIPHERAL_ID_LEGACY' :
    _.snakeCase(type.name).toUpperCase()
  ),
  propName: (prop: PropertyDefinition) => convertCamel(prop.name),
  propType: (prop: PropertyDefinition | ConstantDefinition) => ts.mapType(prop.type),
  eventName: (event: EventDefinition) => convertCamel(event.name),
  fnName: (fn: MethodDefinition) => {
    let name = convertCamel(fn.name);
    if (fn.private) {
      name = `_${name}`;
    }
    return name;
  },
  returnType: (fn: MethodDefinition) => {
    let type = 'void';
    if (fn.returnType) {
      type = ts.mapType(fn.returnType, 'returnType');
    } else if (fn.response) {
      type = fn.response;
    }

    if (fn.sync) {
      return type;
    } else {
      return `Promise<${type}>`;
    }
  },
  paramName: (param: MethodOrProperty, optional?: boolean) => {
    let name = convertCamel(param.name);
    if (optional ?? isOptionalType(param.type)) {
      name += '?';
    }
    return name;
  },
  paramType: (param: MethodParam| PropertyDefinition): string => ts.mapType(param.type),
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'ts') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam) => {
    const defaultValue: string = ts.paramDefaultValue(param);
    return defaultValue ? ` = ${defaultValue}` : '';
  },
  optionsName: (fn: MethodDefinition, withClassName?: string) => {
    let name = `${convertTitle(fn.name)}Options`;
    if (withClassName) {
      name = `${withClassName}.${name}`;
    }
    return name;
  },
  paramSignature: (param: MethodParam, variadic = false) => (
    `${ts.variadicSymbol(variadic)}${ts.paramName(param)}: ${ts.paramType(param)}${ts.defaultInSignature(param)}`
  ),
  allParamSignatures: (fn: MethodDefinition, className: string = '') => {
    const signatures = mapParams(fn, (param, type, _i, subindex) => (
      type === 'normal' ? ts.paramSignature(param) :
      type === 'option' && subindex === 0 ? `options: ${className}.${ts.optionsName(fn)} = {}` :
      type === 'variadic' ? ts.paramSignature(param, true) :
      null
    )).map(sig => `    ${sig}`).join(`,${os.EOL}`);
    return signatures === '' ? '' : `${os.EOL}${signatures}${os.EOL}  `;
  },
  argRequest: (arg: MethodArg) => _.lowerFirst(arg.name),
  argValue: (arg: MethodArg) => getValue(arg.value, 'ts') ?? argMap('this', convertCamel, getValue(arg.value)!),
  paramRequest: (param: MethodParam) => _.lowerFirst(param.requestName) || _.lowerFirst(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'ts');
    if (value) { return value }

    value = convertSnake(getValue(param.value));
    if (value) { return value }

    const name = convertCamel(param.name);
    return name;
  },
  indent: (code: string) => indent(code, '    '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? '...' : '',
  instanceName: (type: WithName) => _.lowerFirst(type.name),
  importStatement: (type: WithName & WithNamespace): string => {
    const namespace = ensureArray(type.namespace).map(ns => `/${convertSnake(ns)}`).join('');
    return `const { ${ts.typeName(type)} } = require('@zaber/motion${namespace}');`;
  },
  protoProperty: (fn: MethodDefinition) => `${_.lowerFirst(fn.returnPropertyName)}`,
  varDecl: () => 'const',
  newKeyword: () => 'new',
  statementTerminator: () => ';',
  quoteMark: () => '\'',
  arrayLiteral: (data: string) => reformatArrayLiteral(data, '[', ']'),
} satisfies TemplateEtc;

export type Ts = typeof ts;

const java = {
  docEscape: (str: string) => str.replace(/(\S*[<>]\S*)/g, symbol => `{@literal ${symbol}}`),
  mapType: (type: TypeDefinition) => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
        .with({ type: 'set' }, ({ of }) => `Set<${mapType(of)}>`)
        .with({ type: 'optional' }, ({ of }) => {
          const nullable = isMatching(P.string, of);
          let mapped = mapType(of);
          mapped = nullable ? `${_.upperFirst(mapped)}` : mapped;
          if (mapped === 'Int') {
            mapped = 'Integer';
          }
          return mapped;
        })
        .with(P.string, type => javaTypeMap[type] || type)
        .exhaustive();
    }
    return mapType(type);
  },
  namespace: (type: WithNamespace) =>
    `zaber.motion${ensureArray(type.namespace).map(n => `.${convertLower(n)}`).join('')}`,
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'DEVICE_ID_LEGACY' :
    type.name === 'Peripheralid' ? 'PERIPHERAL_ID_LEGACY' :
    _.snakeCase(type.name).toUpperCase()
  ),
  propName: (prop: PropertyDefinition) => convertCamel(prop.name),
  propType: (prop: PropertyDefinition | ConstantDefinition) => java.mapType(prop.type),
  eventName: (event: EventDefinition) => convertCamel(event.name),
  fnName: (fn: MethodDefinition, async = false) => convertCamel(fn.name) + (async ? 'Async' : ''),
  getOrSetPropName: (prop: WithName) => convertTitle(prop.name),
  returnType: (fn: MethodDefinition, sync?: boolean) => {
    let type = '';
    if (fn.returnType) {
      type = java.mapType(fn.returnType);
    } else if (fn.response) {
      type = fn.response;
    }

    if (sync ?? fn.sync) {
      return type || 'void';
    } else {
      if (fn.returnType && !isCollectionType(fn.returnType)) {
        type = javaPrimitiveToObjectMap[type] || type;
      }
      return `CompletableFuture<${type || 'Void'}>`;
    }
  },
  paramName: (param: MethodOrProperty) => convertCamel(param.name),
  paramType: (param: MethodOrProperty, variadic = false): string => java.mapType(variadic ? getBaseType(param.type) : param.type),
  paramSignature: (param: MethodParam, variadic = false) => (
    `${java.paramType(param, variadic)}${java.variadicSymbol(variadic)} ${java.paramName(param)}`
  ),
  allParamSignatures: (fn: MethodDefinition, overloadParamCount?: number) => {
    const overloadCount = overloadParamCount ?? allParams(fn).length;
    const signatures = mapParams(fn, (param, type, index) => (
      index < overloadCount ? java.paramSignature(param, type === 'variadic') : null
    ));
    return signatures
      .map((signature, i) => i > 0 ? `        ${signature}` : signature)
      .join(`,${os.EOL}`);
  },
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'java') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam): string => java.paramDefaultValue(param),
  argRequest: (arg: MethodArg) => convertTitle(arg.name),
  argValue: (arg: MethodArg) => {
    const value = getValue(arg.value, 'java') ?? argMap('', java.addGetter, getValue(arg.value)!);
    return value;
  },
  paramRequest: (param: MethodParam) => convertTitle(param.requestName) || convertTitle(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'java');
    if (value) { return value }

    value = convertCamel(getValue(param.value));
    if (value) { return value }

    const name = convertCamel(param.name);
    return name;
  },
  indent: (code: string) => indent(code, '    '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? '...' : '',
  addGetter: (value: string) => `get${convertTitle(value)}()`,
  instanceName: (type: WithName) => _.lowerFirst(type.name),
  importStatement: (type: WithName & WithNamespace): string => {
    const namespace = ensureArray(type.namespace).map(n => `.${convertSnake(n)}`).join('');
    return `import zaber.motion${namespace}.${java.typeName(type)};`;
  },
  boxType: (type: string) => javaPrimitiveToObjectMap[type] || type,
  protoProperty: (fn: MethodDefinition) => `get${fn.returnPropertyName}()`,
  varDecl: (typeName: WithName, typeDef: TypeDefinition) => {
    let decl: string = convertTitle(typeName.name);

    if (isCollectionType(typeDef)) {
      if (typeDef.type === 'array') {
        decl = `${decl}[]`;
      } else if (typeDef.type === 'set') {
        decl = `Set<${decl}>`;
      }
    }

    return decl;
  },
  newKeyword: () => 'new',
  statementTerminator: () => ';',
  quoteMark: () => '"',
  arrayLiteral: (data: string, elementType: string) => reformatArrayLiteral(data, `new ${elementType}[] { `, ' }'),
} satisfies TemplateEtc;

export type Java = typeof java;

const matlab = {
  docEscape: (str: string) => java.docEscape(str),
  mapType: (type: TypeDefinition) => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
        .with({ type: 'set' }, ({ of }) => `java.util.Set<${mapType(of)}>`)
        .with({ type: 'optional' }, ({ of }) => mapType(of))
        .with(P.string, type => matlabTypeMap[type] || type)
        .exhaustive();
    }
    return mapType(type);
  },
  namespace: (type: WithNamespace) => java.namespace(type),
  typeName: (type: WithName) => java.typeName(type),
  constName: (type: WithName) => java.constName(type),
  propName: (prop: PropertyDefinition) => java.propName(prop),
  propType: (prop: PropertyDefinition) => java.propType(prop),
  eventName: (event: EventDefinition) => java.eventName(event),
  fnName: (fn: MethodDefinition) => java.fnName(fn),
  returnType: (fn: MethodDefinition, sync?: boolean) => java.returnType(fn, sync),
  paramName: (param: MethodOrProperty) => java.paramName(param),
  paramType: (param: MethodOrProperty, variadic = false): string => matlab.mapType(variadic ? getBaseType(param.type) : param.type),
  allParamSignatures: java.allParamSignatures,
  variadicSymbol: java.variadicSymbol,
  paramDefaultValue: (param: MethodParam) => java.paramDefaultValue(param),
  defaultInSignature: (param: MethodParam) => java.defaultInSignature(param),
  addGetter: (value: string) => java.addGetter(value),
  instanceName: (type: WithName) => java.instanceName(type),
  importStatement: (type: WithName & WithNamespace): string => java.importStatement(type),
  varDecl: () => '',
  newKeyword: () => '',
  statementTerminator: () => ';',
  quoteMark: () => '\'',
  arrayLiteral: (data: string) => reformatArrayLiteral(data, '[', ']'),
} satisfies TemplateEtc;

export type Matlab = typeof matlab;

const cppPrimitiveTypes = ['int', 'unsigned int', 'double', 'float', 'bool', 'char'];
const cpp = {
  docEscape: (str: string) => str,
  isPrimitive: (mappedType: string) => cppPrimitiveTypes.includes(mappedType) || mappedType === 'Units',
  mapType: (type: TypeDefinition, context?: 'classProp'): string => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `std::vector<${mapType(of)}>`)
        .with({ type: 'set' }, ({ of }) => `std::unordered_set<${mapType(of)}>`)
        .with({ type: 'optional' }, ({ of }) => `std::optional<${mapType(of)}>`)
        .with(P.string, type => cppTypeMap[type] || type)
        .exhaustive();
    }
    _.noop(context);
    return mapType(type);
  },
  namespace: (type: WithNamespace) =>
    `zaber::motion${ensureArray(type.namespace).map(n => `::${n}`).join('')}`,
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  namespaceName: (type: WithName) => convertSnake(type.name),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'DEVICE_ID_LEGACY' :
    type.name === 'Peripheralid' ? 'PERIPHERAL_ID_LEGACY' :
    _.snakeCase(type.name).toUpperCase()
  ),
  constType: (type: ConstantDefinition) => cpp.mapType(type.type),
  propName: (prop: PropertyDefinition) => convertCamel(prop.name),
  propType: (prop: PropertyDefinition, context?: 'classProp') => {
    let propType = cpp.mapType(prop.type, context);
    if (prop.name === 'Connection') {
      propType = 'BaseConnection';
    }
    return propType;
  },
  initValue: (prop: PropertyDefinition) => {
    let value = prop.value?.cpp;
    if (value) { return value }

    value = cpp.paramName(prop);
    const mappedType = cpp.mapType(prop.type);
    if (!cpp.isPrimitive(mappedType)) {
      // This solution is sometimes not optimal because
      // we cannot detect enums and some simple types (like BaseConnection, Measurement).
      value = `std::move(${value})`;
    }
    return value;
  },
  eventName: (event: EventDefinition) => convertCamel(event.name),
  fnName: (fn: MethodDefinition) => convertCamel(fn.name),
  getOrSetPropName: (prop: WithName) => convertTitle(prop.name),
  getOrSetProtobufPropName: (prop: WithName) => convertTitle(prop.name),
  returnType: (fn: MethodDefinition) => {
    let type = '';
    if (fn.returnType) {
      type = cpp.mapType(fn.returnType);
    } else if (fn.response) {
      type = fn.response;
    }
    return (type ? type : 'void');
  },
  paramName: (param: MethodOrProperty) => convertCamel(param.name),
  paramType: (param: MethodOrProperty, variadic = false): string => {
    let typeToMap = param.type;
    if (variadic && isCollectionType(param.type)) {
      typeToMap = param.type.of;
    }
    let paramType = cpp.mapType(typeToMap);
    if (!variadic) {
      if (paramType === 'Connection') {
        paramType = 'BaseConnection';
      }
      if (!cpp.isPrimitive(paramType) && !isEnumType(param.type)) {
        paramType = `const ${paramType}&`;
      }
    }
    return paramType;
  },
  optionsName: (fn: MethodDefinition, withClassName?: string) => {
    let baseName = `${convertTitle(fn.name)}Options`;
    if (withClassName) {
      baseName = `${convertTitle(withClassName)}::${baseName}`;
    }
    return baseName;
  },
  paramSignature: (param: MethodParam, withDefault: boolean) => (
    `${cpp.paramType(param, false)} ${cpp.paramName(param)}${withDefault ? cpp.defaultInSignature(param) : ''}`
  ),
  allParamSignatures: (fn: MethodDefinition, formatVariadic: (param: MethodParam) => string, withDefault = false, withOptionClass = '') => (
    mapParams(fn, (param, type, _, subindex) => {
      if (type === 'normal') {
        return cpp.paramSignature(param, withDefault);
      } else if (type === 'option') {
        if (withOptionClass !== '') {
          if (subindex === 0) {
            return `const ${cpp.optionsName(fn, withOptionClass)}& options`;
          } else {
            return null;
          }
        } else {
          return cpp.paramSignature(param, withDefault);
        }
      } else {
        return formatVariadic(param);
      }
    }).join(', ')
  ),
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'cpp') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam) => {
    const defaultValue: string = cpp.paramDefaultValue(param);
    return defaultValue ? ` = ${defaultValue}` : '';
  },
  argRequest: (arg: MethodArg) => convertTitle(arg.name),
  argValue: (arg: MethodArg) => {
    const value = getValue(arg.value, 'cpp') ?? argMap('this', cpp.addGetter, getValue(arg.value)!, undefined, '->');
    return value;
  },
  paramRequest: (param: MethodParam) => convertTitle(param.requestName) || convertTitle(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'cpp');
    if (value) { return value }

    value = convertSnake(getValue(param.value));
    if (value) { return value }

    const name = convertCamel(param.name);

    if (isCollectionType(param.type)) {
      return `${cpp.mapType(param.type)}(${name})`;
    } else {
      return name;
    }
  },
  indent: (code: string) => indent(code, '    '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? '&...' : '',
  addGetter: (value: string) => `get${convertTitle(value)}()`,
  instanceName: (type: WithName) => _.lowerFirst(type.name),
  importStatement: (type: WithName & WithNamespace): string => {
    const namespace = ensureArray(type.namespace).map(n => `${n}/`).join('');
    return `#include <zaber/motion/${namespace}${convertSnake(cpp.typeName(type))}.h>`;
  },
  dtoProperty: (fn: MethodDefinition) => fn.returnPropertyName && `get${convertTitle(fn.returnPropertyName)}()`,
  getAutoConstRef: (type: TypeDefinition) => {
    const mappedType = cpp.mapType(getBaseType(type));
    return `auto const${cpp.isPrimitive(mappedType) ? '' : '&'}`;
  },
  typeDefaultValue: (type: TypeDefinition) => {
    if (isUnitsType(type)) {
      return 'Units::NATIVE';
    }
    const mappedType = cpp.mapType(type);
    if (!cpp.isPrimitive(mappedType)) {
      return null;
    }
    return '0';
  },
  varDecl: (typeName: WithName, typeDef: TypeDefinition) => {
    let decl: string = convertTitle(typeName.name);
    if (decl === 'BaseConnection') {
      decl = 'Connection';
    }

    if (isCollectionType(typeDef)) {
      if (typeDef.type === 'array') {
        decl = `std::vector<${decl}>`;
      } else if (typeDef.type === 'set') {
        decl = `std::unordered_set<${decl}>`;
      }
    }

    return decl;
  },
  newKeyword: () => 'new',
  statementTerminator: () => ';',
  quoteMark: () => '"',
  arrayLiteral: (data: string) => reformatArrayLiteral(data, '{ ', ' }'),
} satisfies TemplateEtc;

export type Cpp = typeof cpp;

const swift = {
  docEscape: (str: string) => str,
  mapNamespace(namespace: Namespace): string {
    const parts = ensureArray(namespace).map(n => _.upperFirst(n));
    if (parts.length === 0) {
      return 'Dto';
    } else {
      return `Dto${parts.join('.')}`;
    }
  },
  mapType: (type: TypeDefinition): string => {
    function mapType(type: TypeDefinition): string {
      return match(type)
        .with({ type: 'ref' }, ({ name }) => name)
        .with({ type: 'enum' }, ({ name }) => name)
        .with({ type: 'array' }, ({ of }) => `[${mapType(of)}]`)
        .with({ type: 'set' }, ({ of }) => `Set<${mapType(of)}>`)
        .with({ type: 'optional' }, ({ of }) => {
          if (isCollectionType(of)) {
            return mapType(of);
          }
          return `${mapType(of)}?`;
        })
        .with(P.string, type => swiftTypeMap[type] || type)
        .exhaustive();
    }
    return mapType(type);
  },
  namespace: (type: WithNamespace) => {
    const parts = ensureArray(type.namespace).map(n => _.upperFirst(n));
    return `ZaberMotion${parts.join('')}`;
  },
  typeName: (type: WithName) => convertTitle(type.name),
  typeNameDef: (type: TypeDefinition) => convertTitle(getTypeName(type)),
  constName: (type: WithName) => (
    type.name === 'Deviceid' ? 'deviceIdLegacy' :
    type.name === 'Peripheralid' ? 'peripheralIdLegacy' :
    convertCamel(type.name)
  ),
  propName: (prop: PropertyDefinition) => convertCamel(prop.name),
  propType: (prop: PropertyDefinition | ConstantDefinition) => {
    if (prop.dtoNamespace) {
      return `${swift.mapNamespace(prop.dtoNamespace)}.${swift.mapType(prop.type)}`;
    } else {
      return swift.mapType(prop.type);
    }
  },
  eventName: (event: EventDefinition) => convertTitle(event.name),
  fnName: (fn: MethodDefinition) => convertCamel(fn.name),
  returnType: (fn: MethodDefinition) => {
    let type = '';
    if (fn.returnType) {
      type = swift.mapType(fn.returnType);
    } else if (fn.response) {
      type = fn.response;
    }
    return (type ? type : 'Void');
  },
  baseType: (fn: MethodDefinition) => {
    if (fn.returnType) {
      return getBaseType(fn.returnType);
    }
    return null;
  },
  paramName: (param: MethodOrProperty) => convertCamel(param.name),
  paramType: (param: MethodOrProperty): string => swift.mapType(param.type),
  variadicParamType: (param: MethodOrProperty): string => {
    if (isCollectionType(param.type)) {
      return swift.mapType(param.type.of);
    } else {
      return swift.mapType(param.type);
    }
  },
  paramDefaultValue: (param: MethodParam) => getValue(param.defaultValue, 'swift') ?? getValue(param.defaultValue) ?? '',
  defaultInSignature: (param: MethodParam) => {
    const defaultValue: string = swift.paramDefaultValue(param);
    return defaultValue ? ` = ${defaultValue}` : '';
  },
  paramSignature: (param: MethodParam, variadic: MethodParamType = 'normal') => {
    if (variadic === 'normal' || variadic === 'option') {
      return `${swift.paramName(param)}: ${swift.paramType(param)}${swift.defaultInSignature(param)}`;
    } else {
      const variadicType = swift.variadicParamType(param);
      const variadicSymbol = swift.variadicSymbol(variadic);
      return `_ ${swift.paramName(param)}: ${variadicType}${variadicSymbol}${swift.defaultInSignature(param)}`;
    }
  },
  allParamSignatures: (fn: MethodDefinition) => mapParams(fn, (param, type) => swift.paramSignature(param, type)).join(', '),
  argRequest: (arg: MethodArg) => convertCamel(arg.name),
  argValue: (arg: MethodArg) => getValue(arg.value, 'swift') ?? argMap('self', convertCamel, getValue(arg.value)!),
  paramRequest: (param: MethodParam) => convertCamel(param.requestName) || convertCamel(param.name),
  paramValue: (param: MethodParam) => {
    let value = getValue(param.value, 'swift');
    if (value) { return value }

    value = convertCamel(getValue(param.value));
    if (value) { return value }

    const name = convertCamel(param.name);
    return name;
  },
  indent: (code: string) => indent(code, '        '),
  variadicSymbol: (variadic: boolean | MethodParamType) => variadic === true || variadic === 'variadic' ? '...' : '',
  instanceName: (type: WithName) => _.lowerFirst(type.name),
  importStatement: (type: WithName & WithNamespace): string => `import ${swift.namespace(type)}`,
  protoProperty: (fn: MethodDefinition) => _.lowerFirst(fn.returnPropertyName),
  varDecl: () => 'let',
  newKeyword: () => '',
  statementTerminator: () => '',
  quoteMark: () => '"',
  arrayLiteral: (data: string) => reformatArrayLiteral(data, '[', ']'),
} satisfies TemplateEtc;

export type Swift = typeof swift;

const allGoSourceFiles = _.once(() => glob.sync('../internal/**/*.go').filter(file => !file.endsWith('main.pb.go')));

function checkMatchOfRouteAndRequest(fn: MethodDefinition) {
  if (fn.route == null) {
    return;
  }
  for (const file of allGoSourceFiles()) {
    const lines = fs.readFileSync(file, 'utf8').split('\n');
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].includes(`"${fn.route}"`)) {
        const pbCtor = lines[i + 1].match(/return &pb\.(\w+)/)?.[1];
        const pbCall = lines[i + 3].match(/request\.\(\*pb\.(\w+)/)?.[1];
        const isOK = pbCtor != null && pbCtor === fn.request && (pbCtor === pbCall || (pbCtor === 'EmptyRequest' && pbCall == null));
        if (!isOK) {
          const error = `File ${file}:${i + 1} route ${fn.route} ${fn.request}!=${pbCtor}!=${pbCall}`;
          console.error(error);
          throw new Error(error);
        }
      }
    }
  }
}

// Finds the custom data pojo name from an exception definition, searching up its base class definitions.
function exceptionCustomDataType(exceptions: ExceptionNameDefinition[], exceptionType: ExceptionNameDefinition): string | null {
  let dataType = exceptionType.customDataPojo;
  let exception: ExceptionNameDefinition | null = exceptionType;
  while (dataType == null && exception?.baseException) {
    exception = exceptions.find(e => e.name === exception!.baseException) ?? null;
    dataType = exception?.customDataPojo;
  }

  return dataType ?? null;
}

export const context = {
  _,
  getValue,
  getTypeName,
  csharp,
  python,
  ts,
  java,
  cpp,
  matlab,
  swift,
  isFuncWithOptions,
  isFuncVariadic,
  allParams,
  nonVariadicParams,
  mapParams,
  getLines,
  definitions,
  checkMatchOfRouteAndRequest,
  templateHasOptions,
  isInCtor,
  isOptionalType,
  isReferenceType,
  exceptionCustomDataType,
};

export type Context = typeof context;
