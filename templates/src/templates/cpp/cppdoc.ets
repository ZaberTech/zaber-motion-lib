import { Context } from '../context';
import { MethodDefinition } from '../types';

export default function({fn, cpp, withOptionClass, getLines, mapParams, indent, _}: {fn: MethodDefinition, withOptionClass: string, indent: number} & Context)
/* template */
<%_ const istr = _.repeat('    ', indent) -%>
<%- istr %>/**
<% if (fn.obsoleteReason) { -%>
<%- istr %> * Deprecated: <%- fn.obsoleteReason %>
<%- istr %> *
<% } -%>
<% const lines = getLines(fn.description, fn.minFw) -%>
<% lines.forEach(line => { -%>
<%- istr %> * <%- cpp.docEscape(line) %>
<% }) -%>
<%- mapParams<string[]>(fn, (param, type, _, subindex) => {
   const lines = getLines(param.description, param.minFw);
   const docLines: string[] = [];
    if (withOptionClass !== '' && type === 'option') {
        if (subindex === 0) {
            docLines.push(`* @param options A struct of type ${cpp.optionsName(fn)}. It has the following members:`);
        }
        lines.forEach((line, i) => {
            docLines.push(i === 0 ? `* * \`${cpp.paramName(param)}\`: ${cpp.docEscape(line)}` : `*   ${cpp.docEscape(line)}`);
        })
    } else {
        lines.forEach((line, i) => {
            docLines.push(i === 0 ? `* @param ${cpp.paramName(param)} ${cpp.docEscape(line)}` : `* ${cpp.docEscape(line)}`);
        });
    }
    return docLines;
}).flat().map(doc => `${istr} ${doc}\n`).join('') -%>
<% if (fn.returnDescription) { -%>
<%- istr %> * @return <%- cpp.docEscape(fn.returnDescription) %>
<% } -%>
<%- istr %> */
