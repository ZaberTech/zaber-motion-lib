import { Definitions, namespaceEquals } from './types';

export function checkDefinitions(definitions: Definitions) {
  checkForComments(definitions);
  checkForNaming(definitions);
}

function checkForComments(definitions: Definitions) {
  for (const dtoClass of definitions.classes) {
    if (namespaceEquals(dtoClass.namespace, 'requests')) {
      continue;
    }

    if (!dtoClass.description) {
      throw new Error(`Class ${dtoClass.name} is missing a description`);
    }

    for (const property of dtoClass.properties) {
      if (!property.description) {
        throw new Error(`Property ${property.name} in class ${dtoClass.name} is missing a description`);
      }
    }
  }
}

function checkForNaming(definitions: Definitions) {
  for (const dtoClass of definitions.classes) {
    if (!dtoClass.name.match(/^[A-Z]/)) {
      throw new Error(`Class ${dtoClass.name} does not start with a capital letter`);
    }
    for (const property of dtoClass.properties) {
      if (!property.name.match(/^[A-Z]/)) {
        throw new Error(`Property ${property.name} in class ${dtoClass.name} does not start with a capital letter`);
      }
    }
  }
}
