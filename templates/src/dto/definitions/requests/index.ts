import classes from './classes';
import enums from './enums';
import { errorsEnum } from './errors_enum';

export const requests = {
  classes,
  enums: [errorsEnum, ...enums],
};
