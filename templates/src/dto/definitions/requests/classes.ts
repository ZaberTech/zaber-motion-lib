import { DtoClass } from '../../types';

const emptyAutofocusRequest: DtoClass = {
  name: 'EmptyAutofocusRequest',
  namespace: 'requests',
  properties: [
    {
      name: 'ProviderId',
      type: 'int',
    },
    {
      name: 'InterfaceId',
      type: 'int',
    },
    {
      name: 'FocusAddress',
      type: 'int',
    },
    {
      name: 'FocusAxis',
      type: 'int',
    },
    {
      name: 'TurretAddress',
      type: 'int',
    },
  ]
};

export default [
  {
    name: 'GatewayRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Request',
        type: 'string'
      }
    ]
  },
  {
    name: 'GatewayResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Response',
        type: {
          type: 'enum',
          name: 'ResponseType',
          namespace: 'requests'
        }
      },
      {
        name: 'ErrorType',
        type: {
          type: 'enum',
          name: 'Errors',
          namespace: 'requests'
        }
      },
      {
        name: 'ErrorMessage',
        type: 'string'
      }
    ]
  },
  {
    name: 'GatewayEvent',
    namespace: 'requests',
    properties: [
      {
        name: 'Event',
        type: 'string'
      }
    ]
  },
  {
    name: 'EmptyRequest',
    namespace: 'requests',
    properties: []
  },
  {
    name: 'IntRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'int'
      }
    ]
  },
  {
    name: 'BoolResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DoubleResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'DoubleArrayResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      }
    ]
  },
  {
    name: 'IntResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'int'
      }
    ]
  },
  {
    name: 'IntArrayResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'StringResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'string'
      }
    ]
  },
  {
    name: 'StringArrayResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'TestRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'ReturnError',
        type: 'bool'
      },
      {
        name: 'DataPing',
        type: 'string'
      },
      {
        name: 'ReturnErrorWithData',
        type: 'bool'
      }
    ]
  },
  {
    name: 'TestResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'DataPong',
        type: 'string'
      }
    ]
  },
  {
    name: 'TestResponseLong',
    namespace: 'requests',
    properties: [
      {
        name: 'DataPong',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'TestEvent',
    namespace: 'requests',
    properties: [
      {
        name: 'Data',
        type: 'string'
      }
    ]
  },
  {
    name: 'ToolsListSerialPortsResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Ports',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'SetInternalModeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Mode',
        type: 'bool'
      }
    ]
  },
  {
    name: 'SetDeviceDbSourceRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'SourceType',
        type: {
          type: 'enum',
          name: 'DeviceDbSourceType',
          namespace: []
        }
      },
      {
        name: 'UrlOrFilePath',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'ToggleDeviceDbStoreRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'ToggleOn',
        type: 'bool'
      },
      {
        name: 'StoreLocation',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'OpenInterfaceRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceType',
        type: {
          type: 'enum',
          name: 'InterfaceType',
          namespace: 'requests'
        }
      },
      {
        name: 'PortName',
        type: 'string'
      },
      {
        name: 'BaudRate',
        type: 'int'
      },
      {
        name: 'HostName',
        type: 'string'
      },
      {
        name: 'Port',
        type: 'int'
      },
      {
        name: 'Transport',
        type: 'int'
      },
      {
        name: 'RejectRoutedConnection',
        type: 'bool'
      },
      {
        name: 'CloudId',
        type: 'string'
      },
      {
        name: 'ConnectionName',
        type: {
          type: 'optional',
          of: 'string'
        }
      },
      {
        name: 'Realm',
        type: {
          type: 'optional',
          of: 'string'
        }
      },
      {
        name: 'Token',
        type: 'string'
      },
      {
        name: 'Api',
        type: 'string'
      }
    ]
  },
  {
    name: 'OpenInterfaceResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      }
    ]
  },
  {
    name: 'InterfaceEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      }
    ]
  },
  {
    name: 'SetInterfaceTimeoutRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'int'
      }
    ]
  },
  {
    name: 'SetInterfaceChecksumEnabledRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'IsEnabled',
        type: 'bool'
      }
    ]
  },
  {
    name: 'AxisEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      }
    ]
  },
  {
    name: 'GenericCommandRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Command',
        type: 'string'
      },
      {
        name: 'CheckErrors',
        type: 'bool'
      },
      {
        name: 'Timeout',
        type: 'int'
      }
    ]
  },
  {
    name: 'DeviceIdentifyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'AssumeVersion',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'FirmwareVersion',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'AxisToStringRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'TypeOverride',
        type: 'string'
      }
    ]
  },
  {
    name: 'DeviceDetectRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'IdentifyDevices',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'DeviceType',
          namespace: 'requests'
        }
      }
    ]
  },
  {
    name: 'DeviceDetectResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Devices',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'DeviceHomeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceWaitUntilIdleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'ThrowErrorOnFault',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceMoveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'AxisMoveType',
          namespace: 'requests'
        }
      },
      {
        name: 'Arg',
        type: 'double'
      },
      {
        name: 'ArgInt',
        type: 'int'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'Velocity',
        type: 'double'
      },
      {
        name: 'VelocityUnit',
        type: 'Units'
      },
      {
        name: 'Acceleration',
        type: 'double'
      },
      {
        name: 'AccelerationUnit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceMoveSinRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Amplitude',
        type: 'double'
      },
      {
        name: 'AmplitudeUnits',
        type: 'Units'
      },
      {
        name: 'Period',
        type: 'double'
      },
      {
        name: 'PeriodUnits',
        type: 'Units'
      },
      {
        name: 'Count',
        type: 'double'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceStopRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceOnAllRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceOnAllResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'DeviceAddresses',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'DeviceGetWarningsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Clear',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceGetWarningsResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Flags',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'WaitToClearWarningsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double'
      },
      {
        name: 'WarningFlags',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'DeviceGetAllDigitalIORequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelType',
        type: 'string'
      }
    ]
  },
  {
    name: 'DeviceGetAllDigitalIOResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'bool'
        }
      }
    ]
  },
  {
    name: 'DeviceGetAllAnalogIORequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelType',
        type: 'string'
      }
    ]
  },
  {
    name: 'DeviceGetAllAnalogIOResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      }
    ]
  },
  {
    name: 'DeviceGetDigitalIORequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelType',
        type: 'string'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'DeviceGetAnalogIORequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelType',
        type: 'string'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'DeviceSetAllDigitalOutputsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'DeviceSetAllDigitalOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'FutureValues',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceCancelAllOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Analog',
        type: 'bool'
      },
      {
        name: 'Channels',
        type: {
          type: 'array',
          of: 'bool'
        }
      }
    ]
  },
  {
    name: 'DeviceSetAllAnalogOutputsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      }
    ]
  },
  {
    name: 'DeviceSetAllAnalogOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      },
      {
        name: 'FutureValues',
        type: {
          type: 'array',
          of: 'double'
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceSetDigitalOutputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'DeviceSetDigitalOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'FutureValue',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceCancelOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Analog',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'DeviceSetAnalogOutputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'DeviceSetAnalogOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'FutureValue',
        type: 'double'
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceSetLowpassFilterRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'CutoffFrequency',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'SetLogOutputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Mode',
        type: {
          type: 'enum',
          name: 'LogOutputMode',
          namespace: []
        }
      },
      {
        name: 'FilePath',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'DeviceGetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceMultiGetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Settings',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'GetSetting',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'AxisSettings',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'GetAxisSetting',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'DeviceConvertSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'FromNative',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceSetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'DeviceSetSettingStrRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'string'
      }
    ]
  },
  {
    name: 'PrepareCommandRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'CommandTemplate',
        type: 'string'
      },
      {
        name: 'Parameters',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'WaitToRespondRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double'
      }
    ]
  },
  {
    name: 'RenumberRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Address',
        type: 'int'
      }
    ]
  },
  {
    name: 'LockstepEnableRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'LockstepDisableRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LockstepStopRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LockstepHomeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LockstepMoveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'AxisMoveType',
          namespace: 'requests'
        }
      },
      {
        name: 'Arg',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'Velocity',
        type: 'double'
      },
      {
        name: 'VelocityUnit',
        type: 'Units'
      },
      {
        name: 'Acceleration',
        type: 'double'
      },
      {
        name: 'AccelerationUnit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'LockstepMoveSinRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'Amplitude',
        type: 'double'
      },
      {
        name: 'AmplitudeUnits',
        type: 'Units'
      },
      {
        name: 'Period',
        type: 'double'
      },
      {
        name: 'PeriodUnits',
        type: 'Units'
      },
      {
        name: 'Count',
        type: 'double'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LockstepWaitUntilIdleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'ThrowErrorOnFault',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LockstepEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      }
    ]
  },
  {
    name: 'LockstepGetAxisNumbersResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'LockstepGetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'LockstepSetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'LockstepGroupId',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'AxisIndex',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeAddSettingChannelRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      }
    ]
  },
  {
    name: 'OscilloscopeAddIoChannelRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'IoType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        }
      },
      {
        name: 'IoChannel',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeStartRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'CaptureLength',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeReadResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'DataIds',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'OscilloscopeDataGetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'DataId',
        type: 'int'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'OscilloscopeDataGetSampleTimeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'DataId',
        type: 'int'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'Index',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeDataGetSamplesResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Data',
        type: {
          type: 'array',
          of: 'double'
        }
      }
    ]
  },
  {
    name: 'StreamSetupLiveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'StreamSetupLiveCompositeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'StreamAxisDefinition',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'PvtAxes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'PvtAxisDefinition',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'StreamSetupStoreRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'StreamBuffer',
        type: 'int'
      },
      {
        name: 'PvtBuffer',
        type: 'int'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'StreamSetupStoreCompositeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'StreamBuffer',
        type: 'int'
      },
      {
        name: 'PvtBuffer',
        type: 'int'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'StreamAxisDefinition',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'PvtAxes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'PvtAxisDefinition',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'StreamSetupStoreArbitraryAxesRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'StreamBuffer',
        type: 'int'
      },
      {
        name: 'PvtBuffer',
        type: 'int'
      },
      {
        name: 'AxesCount',
        type: 'int'
      }
    ]
  },
  {
    name: 'StreamCallRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'StreamBuffer',
        type: 'int'
      },
      {
        name: 'PvtBuffer',
        type: 'int'
      }
    ]
  },
  {
    name: 'PvtPointRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'StreamSegmentType',
          namespace: 'requests'
        }
      },
      {
        name: 'Positions',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      },
      {
        name: 'Velocities',
        type: {
          type: 'array',
          of: {
            type: 'optional',
            of: {
              type: 'ref',
              name: 'Measurement',
              namespace: []
            }
          }
        }
      },
      {
        name: 'Time',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      }
    ]
  },
  {
    name: 'StreamLineRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'StreamSegmentType',
          namespace: 'requests'
        }
      },
      {
        name: 'Endpoint',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      },
      {
        name: 'TargetAxesIndices',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'StreamArcRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'StreamSegmentType',
          namespace: 'requests'
        }
      },
      {
        name: 'RotationDirection',
        type: {
          type: 'enum',
          name: 'RotationDirection',
          namespace: []
        }
      },
      {
        name: 'CenterX',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'CenterY',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'EndX',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'EndY',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'TargetAxesIndices',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Endpoint',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'StreamCircleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'StreamSegmentType',
          namespace: 'requests'
        }
      },
      {
        name: 'RotationDirection',
        type: {
          type: 'enum',
          name: 'RotationDirection',
          namespace: []
        }
      },
      {
        name: 'CenterX',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'CenterY',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      },
      {
        name: 'TargetAxesIndices',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'StreamWaitDigitalInputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'bool'
      }
    ]
  },
  {
    name: 'StreamWaitAnalogInputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Condition',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'StreamSetDigitalOutputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'StreamSetDigitalOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'FutureValue',
        type: {
          type: 'enum',
          name: 'DigitalOutputAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamCancelOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Analog',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
    ]
  },
  {
    name: 'StreamSetAnalogOutputRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'StreamSetAnalogOutputScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'FutureValue',
        type: 'double'
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
    ]
  },
  {
    name: 'StreamSetAllDigitalOutputsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'StreamSetAllDigitalOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'FutureValues',
        type: {
          type: 'array',
          of: {
            type: 'enum',
            name: 'DigitalOutputAction',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamCancelAllOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Analog',
        type: 'bool'
      },
      {
        name: 'Channels',
        type: {
          type: 'array',
          of: 'bool',
        }
      },
    ]
  },
  {
    name: 'StreamSetAllAnalogOutputsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      }
    ]
  },
  {
    name: 'StreamSetAllAnalogOutputsScheduleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Values',
        type: {
          type: 'array',
          of: 'double'
        }
      },
      {
        name: 'FutureValues',
        type: {
          type: 'array',
          of: 'double'
        }
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamWaitRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Time',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamWaitUntilIdleRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'ThrowErrorOnFault',
        type: 'bool'
      }
    ]
  },
  {
    name: 'StreamEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      }
    ]
  },
  {
    name: 'StreamGetAxesResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'StreamAxisDefinition',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'PvtAxes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'PvtAxisDefinition',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'StreamGetMaxSpeedRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamSetMaxSpeedRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'MaxSpeed',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamGetMaxTangentialAccelerationRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamSetMaxTangentialAccelerationRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'MaxTangentialAcceleration',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamGetMaxCentripetalAccelerationRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamSetMaxCentripetalAccelerationRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'MaxCentripetalAcceleration',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'StreamBufferGetContentRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'BufferId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      }
    ]
  },
  {
    name: 'StreamBufferGetContentResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'BufferLines',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'StreamBufferEraseRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'BufferId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      }
    ]
  },
  {
    name: 'StreamGenericCommandRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Command',
        type: 'string'
      }
    ]
  },
  {
    name: 'StreamGenericCommandBatchRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Batch',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'StreamSetHoldRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      },
      {
        name: 'Hold',
        type: 'bool'
      }
    ],
  },
  {
    name: 'TriggerEnableRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Count',
        type: 'int'
      }
    ]
  },
  {
    name: 'TriggerEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'TriggerFireWhenRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Condition',
        type: 'string'
      }
    ]
  },
  {
    name: 'TriggerFireWhenDistanceTravelledRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Distance',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TriggerFireWhenIoRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'PortType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        }
      },
      {
        name: 'Channel',
        type: 'int'
      },
      {
        name: 'TriggerCondition',
        type: {
          type: 'enum',
          name: 'TriggerCondition',
          namespace: 'ascii'
        }
      },
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'TriggerFireWhenSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'TriggerCondition',
        type: {
          type: 'enum',
          name: 'TriggerCondition',
          namespace: 'ascii'
        }
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TriggerFireAtIntervalRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Interval',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TriggerOnFireRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Action',
        type: {
          type: 'enum',
          name: 'TriggerAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Command',
        type: 'string'
      }
    ]
  },
  {
    name: 'TriggerOnFireSetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Action',
        type: {
          type: 'enum',
          name: 'TriggerAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Operation',
        type: {
          type: 'enum',
          name: 'TriggerOperation',
          namespace: 'ascii'
        }
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TriggerOnFireSetToSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Action',
        type: {
          type: 'enum',
          name: 'TriggerAction',
          namespace: 'ascii'
        }
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Setting',
        type: 'string'
      },
      {
        name: 'Operation',
        type: {
          type: 'enum',
          name: 'TriggerOperation',
          namespace: 'ascii'
        }
      },
      {
        name: 'FromAxis',
        type: 'int'
      },
      {
        name: 'FromSetting',
        type: 'string'
      }
    ]
  },
  {
    name: 'TriggerClearActionRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Action',
        type: {
          type: 'enum',
          name: 'TriggerAction',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'TriggerSetLabelRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'TriggerNumber',
        type: 'int'
      },
      {
        name: 'Label',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'OpenBinaryInterfaceRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceType',
        type: {
          type: 'enum',
          name: 'InterfaceType',
          namespace: 'requests'
        }
      },
      {
        name: 'PortName',
        type: 'string'
      },
      {
        name: 'BaudRate',
        type: 'int'
      },
      {
        name: 'HostName',
        type: 'string'
      },
      {
        name: 'Port',
        type: 'int'
      },
      {
        name: 'UseMessageIds',
        type: 'bool'
      }
    ]
  },
  {
    name: 'GenericBinaryRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Command',
        type: {
          type: 'enum',
          name: 'CommandCode',
          namespace: 'binary'
        }
      },
      {
        name: 'Data',
        type: 'int'
      },
      {
        name: 'CheckErrors',
        type: 'bool'
      },
      {
        name: 'Timeout',
        type: 'double'
      }
    ]
  },
  {
    name: 'DeviceEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      }
    ]
  },
  {
    name: 'BinaryGenericWithUnitsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Command',
        type: {
          type: 'enum',
          name: 'CommandCode',
          namespace: 'binary'
        }
      },
      {
        name: 'Data',
        type: 'double'
      },
      {
        name: 'FromUnit',
        type: 'Units'
      },
      {
        name: 'ToUnit',
        type: 'Units'
      },
      {
        name: 'Timeout',
        type: 'double'
      }
    ]
  },
  {
    name: 'BinaryDeviceHomeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'BinaryDeviceMoveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double'
      },
      {
        name: 'Type',
        type: {
          type: 'enum',
          name: 'AxisMoveType',
          namespace: 'requests'
        }
      },
      {
        name: 'Arg',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'BinaryDeviceStopRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'BinaryDeviceDetectRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'IdentifyDevices',
        type: 'bool'
      }
    ]
  },
  {
    name: 'BinaryDeviceDetectResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Devices',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'BinaryDeviceGetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Setting',
        type: {
          type: 'enum',
          name: 'BinarySettings',
          namespace: 'binary'
        }
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'BinaryDeviceSetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Setting',
        type: {
          type: 'enum',
          name: 'BinarySettings',
          namespace: 'binary'
        }
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'CustomInterfaceReadRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TransportId',
        type: 'int'
      }
    ]
  },
  {
    name: 'CustomInterfaceWriteRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TransportId',
        type: 'int'
      },
      {
        name: 'Message',
        type: 'string'
      }
    ]
  },
  {
    name: 'CustomInterfaceOpenResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'TransportId',
        type: 'int'
      }
    ]
  },
  {
    name: 'CustomInterfaceCloseRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TransportId',
        type: 'int'
      },
      {
        name: 'ErrorMessage',
        type: 'string'
      }
    ]
  },
  {
    name: 'CanSetStateRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'State',
        type: 'string'
      },
      {
        name: 'FirmwareVersion',
        type: { type: 'optional', of: { type: 'ref', name: 'FirmwareVersion', namespace: [] } },
      }
    ]
  },
  {
    name: 'SetStateRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'State',
        type: 'string'
      },
      {
        name: 'DeviceOnly',
        type: 'bool'
      }
    ]
  },
  {
    name: 'ServoTuningRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Paramset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'SetServoTuningRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Paramset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      },
      {
        name: 'TuningParams',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'ServoTuningParam',
            namespace: 'ascii'
          }
        }
      },
      {
        name: 'SetUnspecifiedToDefault',
        type: 'bool',
      },
    ]
  },
  {
    name: 'SetServoTuningPIDRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Paramset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      },
      {
        name: 'P',
        type: 'double'
      },
      {
        name: 'I',
        type: 'double'
      },
      {
        name: 'D',
        type: 'double'
      },
      {
        name: 'Fc',
        type: 'double'
      }
    ]
  },
  {
    name: 'GetSimpleTuningParamDefinitionResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Params',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'SimpleTuningParamDefinition',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'TranslatorCreateRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Definition',
        type: {
          type: 'ref',
          name: 'DeviceDefinition',
          namespace: 'gcode'
        }
      },
      {
        name: 'Config',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'TranslatorConfig',
            namespace: 'gcode'
          }
        }
      }
    ]
  },
  {
    name: 'TranslatorCreateResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      }
    ]
  },
  {
    name: 'TranslatorTranslateRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'Block',
        type: 'string'
      }
    ]
  },
  {
    name: 'TranslatorFlushLiveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'WaitUntilIdle',
        type: 'bool'
      }
    ]
  },
  {
    name: 'TranslatorFlushResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Commands',
        type: {
          type: 'array',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'TranslatorCreateLiveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'StreamId',
        type: 'int'
      },
      {
        name: 'Config',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'TranslatorConfig',
            namespace: 'gcode'
          }
        }
      }
    ]
  },
  {
    name: 'TranslatorCreateFromDeviceRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Config',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'TranslatorConfig',
            namespace: 'gcode'
          }
        }
      }
    ]
  },
  {
    name: 'TranslatorEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      }
    ]
  },
  {
    name: 'TranslatorSetTraverseRateRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'TraverseRate',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TranslatorSetAxisPositionRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'string'
      },
      {
        name: 'Position',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TranslatorGetAxisPositionRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'string'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TranslatorGetAxisOffsetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'CoordinateSystem',
        type: 'string'
      },
      {
        name: 'Axis',
        type: 'string'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TranslatorSetFeedRateOverrideRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'TranslatorId',
        type: 'int'
      },
      {
        name: 'Coefficient',
        type: 'double'
      }
    ]
  },
  {
    name: 'DeviceSetStorageRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'string'
      },
      {
        name: 'Encode',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceGetStorageRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      },
      {
        name: 'Decode',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceSetStorageNumberRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'double'
      }
    ]
  },
  {
    name: 'DeviceSetStorageBoolRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      },
      {
        name: 'Value',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceStorageRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      }
    ]
  },
  {
    name: 'DeviceStorageListKeysRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Prefix',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'DeviceSetUnitConversionsRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Key',
        type: 'string'
      },
      {
        name: 'Conversions',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'ConversionFactor',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'ObjectiveChangerRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'TurretAddress',
        type: 'int'
      },
      {
        name: 'FocusAddress',
        type: 'int'
      },
      {
        name: 'FocusAxis',
        type: 'int'
      }
    ]
  },
  {
    name: 'ObjectiveChangerCreateResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Turret',
        type: 'int'
      },
      {
        name: 'FocusAddress',
        type: 'int'
      },
      {
        name: 'FocusAxis',
        type: 'int'
      }
    ]
  },
  {
    name: 'ObjectiveChangerChangeRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'TurretAddress',
        type: 'int'
      },
      {
        name: 'FocusAddress',
        type: 'int'
      },
      {
        name: 'FocusAxis',
        type: 'int'
      },
      {
        name: 'Objective',
        type: 'int'
      },
      {
        name: 'FocusOffset',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'ObjectiveChangerGetCurrentResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Value',
        type: 'int'
      }
    ]
  },
  {
    name: 'ObjectiveChangerSetRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'TurretAddress',
        type: 'int'
      },
      {
        name: 'FocusAddress',
        type: 'int'
      },
      {
        name: 'FocusAxis',
        type: 'int'
      },
      {
        name: 'Value',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'FindDeviceRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'DeviceAddress',
        type: 'int'
      }
    ]
  },
  {
    name: 'FindDeviceResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Address',
        type: 'int'
      }
    ]
  },
  {
    name: 'MicroscopeEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Config',
        type: {
          type: 'ref',
          name: 'MicroscopeConfig',
          namespace: 'microscopy'
        }
      }
    ]
  },
  {
    name: 'MicroscopeConfigResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Config',
        type: {
          type: 'ref',
          name: 'MicroscopeConfig',
          namespace: 'microscopy'
        }
      }
    ]
  },
  {
    name: 'MicroscopeInitRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Config',
        type: {
          type: 'ref',
          name: 'MicroscopeConfig',
          namespace: 'microscopy'
        }
      },
      {
        name: 'Force',
        type: 'bool'
      },
    ]
  },
  {
    name: 'UnitGetSymbolRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'UnitGetSymbolResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Symbol',
        type: 'string'
      }
    ]
  },
  {
    name: 'UnitGetEnumRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Symbol',
        type: 'string'
      }
    ]
  },
  {
    name: 'UnitGetEnumResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'AxesEmptyRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Interfaces',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Devices',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      }
    ]
  },
  {
    name: 'AxesMoveRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Interfaces',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Devices',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Position',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'AxesGetSettingRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'Interfaces',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Devices',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Axes',
        type: {
          type: 'array',
          of: 'int'
        }
      },
      {
        name: 'Unit',
        type: {
          type: 'array',
          of: 'Units'
        }
      },
      {
        name: 'Setting',
        type: 'string'
      }
    ]
  },
  {
    name: 'GetSettingResults',
    namespace: 'requests',
    properties: [
      {
        name: 'Results',
        type: {
          of: {
            type: 'ref',
            name: 'GetSettingResult',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'GetAxisSettingResults',
    namespace: 'requests',
    properties: [
      {
        name: 'Results',
        type: {
          of: {
            type: 'ref',
            name: 'GetAxisSettingResult',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'ProcessOn',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'On',
        type: 'bool'
      },
      {
        name: 'Duration',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'SetProcessControllerSource',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Source',
        type: {
          type: 'ref',
          name: 'ProcessControllerSource',
          namespace: 'product'
        }
      }
    ]
  },
  {
    name: 'ChannelOn',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'On',
        type: 'bool'
      }
    ]
  },
  {
    name: 'SetSimpleTuning',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Paramset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      },
      {
        name: 'CarriageMass',
        type: {
          type: 'optional',
          of: 'double'
        }
      },
      {
        name: 'LoadMass',
        type: 'double'
      },
      {
        name: 'TuningParams',
        type: {
          of: {
            type: 'ref',
            name: 'ServoTuningParam',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'GenericCommandResponseCollection',
    namespace: 'requests',
    properties: [
      {
        name: 'Responses',
        type: {
          of: {
            type: 'ref',
            name: 'Response',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'OscilloscopeDataIdentifier',
    namespace: 'requests',
    properties: [
      {
        name: 'DataId',
        type: 'int'
      }
    ]
  },
  {
    name: 'StreamBufferList',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Pvt',
        type: 'bool'
      }
    ]
  },
  {
    name: 'LoadParamset',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'ToParamset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      },
      {
        name: 'FromParamset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'BinaryMessageCollection',
    namespace: 'requests',
    properties: [
      {
        name: 'Messages',
        type: {
          of: {
            type: 'ref',
            name: 'Message',
            namespace: 'binary'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'ChannelSetIntensity',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Intensity',
        type: 'double'
      }
    ]
  },
  {
    name: 'TriggerStates',
    namespace: 'requests',
    properties: [
      {
        name: 'States',
        type: {
          of: {
            type: 'ref',
            name: 'TriggerState',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'TriggerEnabledStates',
    namespace: 'requests',
    properties: [
      {
        name: 'States',
        type: {
          of: {
            type: 'ref',
            name: 'TriggerEnabledState',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'CanSetStateAxisResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Error',
        type: {
          type: 'optional',
          of: 'string'
        }
      },
      {
        name: 'AxisNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'ServoTuningParamsetResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Paramset',
        type: {
          type: 'enum',
          name: 'ServoTuningParamset',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'StreamModeResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'StreamMode',
        type: {
          type: 'enum',
          name: 'StreamMode',
          namespace: 'ascii'
        }
      },
      {
        name: 'PvtMode',
        type: {
          type: 'enum',
          name: 'PvtMode',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'SetIoPortLabelRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'PortType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        }
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Label',
        type: {
          type: 'optional',
          of: 'string'
        }
      }
    ]
  },
  {
    name: 'GetIoPortLabelRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'PortType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        }
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      }
    ]
  },
  {
    name: 'GetAllIoPortLabelsResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Labels',
        type: {
          of: {
            type: 'ref',
            name: 'IoPortLabel',
            namespace: 'ascii'
          },
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'UnknownResponseEventWrapper',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'UnknownResponse',
        type: {
          type: 'ref',
          name: 'UnknownResponseEvent',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'UnknownBinaryResponseEventWrapper',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'UnknownResponse',
        type: {
          type: 'ref',
          name: 'UnknownResponseEvent',
          namespace: 'binary'
        }
      }
    ]
  },
  {
    name: 'AlertEventWrapper',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Alert',
        type: {
          type: 'ref',
          name: 'AlertEvent',
          namespace: 'ascii'
        }
      }
    ]
  },
  {
    name: 'BinaryReplyOnlyEventWrapper',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Reply',
        type: {
          type: 'ref',
          name: 'ReplyOnlyEvent',
          namespace: 'binary'
        }
      }
    ]
  },
  {
    name: 'DisconnectedEvent',
    namespace: 'requests',
    description: 'Event that is sent when a connection is lost.',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int',
        description: 'The id of the interface that was disconnected.'
      },
      {
        name: 'ErrorType',
        type: {
          type: 'enum',
          name: 'Errors',
          namespace: 'requests'
        },
        description: 'The type of error that caused the disconnection.'
      },
      {
        name: 'ErrorMessage',
        type: 'string',
        description: 'The message describing the error.'
      }
    ]
  }, {
    name: 'CheckVersionRequest',
    namespace: 'requests',
    properties: [{
      name: 'Version',
      type: 'string',
    }, {
      name: 'Host',
      type: 'string',
    }]
  }, {
    name: 'DriverEnableRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'Axis',
        type: 'int'
      },
      {
        name: 'Timeout',
        type: 'double',
      }
    ]
  }, {
    name: 'ForgetDevicesRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'ExceptDevices',
        type: { type: 'array', of: 'int' },
      }
    ]
  }, emptyAutofocusRequest, {
    name: 'MicroscopeFindRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int',
      },
      {
        name: 'ThirdParty',
        type: { type: 'optional', of: { type: 'ref', name: 'ThirdPartyComponents', namespace: 'microscopy' } },
      }
    ]
  }, {
    name: 'AutofocusGetStatusResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Status',
        type: {
          type: 'ref',
          name: 'AutofocusStatus',
          namespace: 'microscopy',
        }
      },
    ]
  }, {
    name: 'WdiGetStatusResponse',
    namespace: 'requests',
    properties: [
      {
        name: 'Status',
        type: {
          type: 'ref',
          name: 'WdiAutofocusProviderStatus',
          namespace: 'microscopy',
        }
      },
    ]
  }, {
    name: 'WdiGenericRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int',
      }, {
        name: 'RegisterId',
        type: 'int',
      }, {
        name: 'Size',
        type: 'int',
      }, {
        name: 'Count',
        type: 'int',
      }, {
        name: 'Offset',
        type: 'int',
      }, {
        name: 'RegisterBank',
        type: 'string',
      }, {
        name: 'Data',
        type: { type: 'array', of: 'int' },
      }
    ],
  }, {
    name: 'AutofocusSetObjectiveParamsRequest',
    namespace: 'requests',
    properties: [...emptyAutofocusRequest.properties, {
      name: 'Objective',
      type: 'int',
    }, {
      name: 'Parameters',
      type: { type: 'array', of: { type: 'ref', name: 'NamedParameter', namespace: [] } },
    }]
  }, {
    name: 'AutofocusGetObjectiveParamsRequest',
    namespace: 'requests',
    properties: [...emptyAutofocusRequest.properties, {
      name: 'Objective',
      type: 'int',
    }],
  }, {
    name: 'AutofocusGetObjectiveParamsResponse',
    namespace: 'requests',
    properties: [{
      name: 'Parameters',
      type: { type: 'array', of: { type: 'ref', name: 'NamedParameter', namespace: [] } },
    }],
  }, {
    name: 'AutofocusFocusRequest',
    namespace: 'requests',
    properties: [...emptyAutofocusRequest.properties, {
      name: 'Scan',
      type: 'bool',
    }, {
      name: 'Once',
      type: 'bool',
    }, {
      name: 'Timeout',
      type: 'int',
    }],
  }, {
    name: 'MicroscopeTriggerCameraRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int'
      },
      {
        name: 'Device',
        type: 'int'
      },
      {
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        name: 'Delay',
        type: 'double'
      },
      {
        name: 'Unit',
        type: 'Units'
      },
      {
        name: 'Wait',
        type: 'bool',
      }
    ]
  }, {
    name: 'DeviceRestoreRequest',
    namespace: 'requests',
    properties: [
      {
        name: 'InterfaceId',
        type: 'int',
      }, {
        name: 'Device',
        type: 'int',
      }, {
        name: 'Axis',
        type: 'int',
      }, {
        name: 'Hard',
        type: 'bool',
      },
    ]
  },
] as DtoClass[];
