import { DtoEnum } from '../../types';

export default [
  {
    name: 'ResponseType',
    namespace: 'requests',
    values: [
      {
        name: 'Ok'
      },
      {
        name: 'Error'
      }
    ]
  },
  {
    name: 'InterfaceType',
    namespace: 'requests',
    values: [
      {
        name: 'SerialPort'
      },
      {
        name: 'Tcp'
      },
      {
        name: 'Custom'
      },
      {
        name: 'Iot'
      },
      {
        name: 'NetworkShare'
      }
    ]
  },
  {
    name: 'DeviceType',
    namespace: 'requests',
    values: [
      {
        name: 'Any'
      },
      {
        name: 'ProcessController'
      }
    ]
  },
  {
    name: 'AxisMoveType',
    namespace: 'requests',
    values: [
      {
        name: 'Abs'
      },
      {
        name: 'Rel'
      },
      {
        name: 'Vel'
      },
      {
        name: 'Max'
      },
      {
        name: 'Min'
      },
      {
        name: 'Index'
      }
    ]
  },
  {
    name: 'StreamSegmentType',
    namespace: 'requests',
    values: [
      {
        name: 'Abs'
      },
      {
        name: 'Rel'
      }
    ]
  },
] as DtoEnum[];
