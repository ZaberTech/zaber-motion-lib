import { DtoClass } from '../../types';

export default [
  {
    name: 'DeviceDefinition',
    namespace: 'gcode',
    description: 'Holds information about device and its axes for purpose of a translator.',
    properties: [
      {
        description: [
          'Device ID of the controller.',
          'Can be obtained from device settings.'
        ],
        name: 'DeviceId',
        type: 'int'
      },
      {
        description: 'Applicable axes of the device.',
        name: 'Axes',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'AxisDefinition',
            namespace: 'gcode'
          }
        }
      },
      {
        description: [
          'The smallest of each axis\' maxspeed setting value.',
          'This value becomes the traverse rate of the translator.'
        ],
        name: 'MaxSpeed',
        type: {
          type: 'ref',
          name: 'Measurement',
          namespace: []
        }
      }
    ]
  },
  {
    name: 'AxisDefinition',
    namespace: 'gcode',
    description: 'Defines an axis of the translator.',
    properties: [
      {
        description: 'ID of the peripheral.',
        name: 'PeripheralId',
        type: 'int'
      },
      {
        description: [
          'Microstep resolution of the axis.',
          'Can be obtained by reading the resolution setting.',
          'Leave empty if the axis does not have the setting.'
        ],
        name: 'MicrostepResolution',
        type: {
          of: 'int',
          type: 'optional'
        }
      }
    ]
  },
  {
    name: 'TranslatorConfig',
    namespace: 'gcode',
    description: 'Configuration of a translator.',
    properties: [
      {
        description: 'Optional custom mapping of translator axes to stream axes.',
        name: 'AxisMappings',
        type: {
          type: 'optional',
          of: {
            type: 'array',
            of: {
              type: 'ref',
              name: 'AxisMapping',
              namespace: 'gcode'
            }
          }
        }
      },
      {
        description: 'Optional transformation of axes.',
        name: 'AxisTransformations',
        type: {
          type: 'optional',
          of: {
            type: 'array',
            of: {
              type: 'ref',
              name: 'AxisTransformation',
              namespace: 'gcode'
            }
          }
        }
      }
    ]
  },
  {
    name: 'AxisMapping',
    namespace: 'gcode',
    description: 'Maps a translator axis to a Zaber stream axis.',
    properties: [
      {
        description: 'Letter of the translator axis (X,Y,Z,A,B,C,E).',
        name: 'AxisLetter',
        type: 'string'
      },
      {
        description: 'Index of the stream axis.',
        name: 'AxisIndex',
        type: 'int'
      }
    ]
  },
  {
    name: 'AxisTransformation',
    namespace: 'gcode',
    description: 'Represents a transformation of a translator axis.',
    properties: [
      {
        description: 'Letter of the translator axis (X,Y,Z,A,B,C,E).',
        name: 'AxisLetter',
        type: 'string'
      },
      {
        description: 'Scaling factor.',
        name: 'Scaling',
        type: {
          of: 'double',
          type: 'optional'
        }
      },
      {
        description: 'Translation distance.',
        name: 'Translation',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'Measurement',
            namespace: []
          }
        }
      }
    ]
  },
  {
    name: 'TranslateResult',
    namespace: 'gcode',
    description: 'Represents a result of a G-code block translation.',
    properties: [
      {
        description: 'Stream commands resulting from the block.',
        name: 'Commands',
        type: {
          type: 'array',
          of: 'string'
        }
      },
      {
        description: 'Messages informing about unsupported codes and features.',
        name: 'Warnings',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'TranslateMessage',
            namespace: 'gcode'
          }
        }
      }
    ]
  },
  {
    name: 'TranslateMessage',
    namespace: 'gcode',
    description: 'Represents a message from translator regarding a block translation.',
    properties: [
      {
        description: 'The message describing an occurrence.',
        name: 'Message',
        type: 'string'
      },
      {
        description: 'The index in the block string that the message regards to.',
        name: 'FromBlock',
        type: 'int'
      },
      {
        description: [
          'The end index in the block string that the message regards to.',
          'The end index is exclusive.'
        ],
        name: 'ToBlock',
        type: 'int'
      }
    ]
  }
] as DtoClass[];
