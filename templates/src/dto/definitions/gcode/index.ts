import { DtoEnum } from '../../types';

import classes from './classes';

export const gcode = {
  classes,
  enums: [] as DtoEnum[],
};
