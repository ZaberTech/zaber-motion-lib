import { DtoEnum } from '../../types';

export default [
  {
    name: 'StreamAxisType',
    namespace: 'ascii',
    description: 'Denotes type of the stream axis.',
    values: [
      {
        name: 'Physical'
      },
      {
        name: 'Lockstep'
      }
    ]
  },
  {
    name: 'PvtAxisType',
    namespace: 'ascii',
    description: 'Denotes type of the PVT sequence axis.',
    values: [
      {
        name: 'Physical'
      },
      {
        name: 'Lockstep'
      }
    ]
  },
  {
    name: 'MessageType',
    namespace: 'ascii',
    description: [
      'Denotes type of the response message.',
      'For more information refer to:',
      '[ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format).'
    ],
    values: [
      {
        name: 'Reply'
      },
      {
        name: 'Info'
      },
      {
        name: 'Alert'
      }
    ]
  },
  {
    name: 'AxisType',
    namespace: 'ascii',
    description: 'Denotes type of an axis and units it accepts.',
    values: [
      {
        name: 'Unknown'
      },
      {
        name: 'Linear'
      },
      {
        name: 'Rotary'
      },
      {
        name: 'Process'
      },
      {
        name: 'Lamp'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'StreamMode',
    description: 'Mode of a stream.',
    values: [
      {
        name: 'Disabled'
      },
      {
        name: 'Store'
      },
      {
        name: 'StoreArbitraryAxes'
      },
      {
        name: 'Live'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'PvtMode',
    description: 'Mode of a PVT sequence.',
    values: [
      {
        name: 'Disabled'
      },
      {
        name: 'Store'
      },
      {
        name: 'Live'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'ServoTuningParamset',
    description: 'Servo Tuning Parameter Set to target.',
    values: [
      {
        name: 'Live'
      },
      {
        name: 'P1'
      },
      {
        name: 'P2'
      },
      {
        name: 'P3'
      },
      {
        name: 'P4'
      },
      {
        name: 'P5'
      },
      {
        name: 'P6'
      },
      {
        name: 'P7'
      },
      {
        name: 'P8'
      },
      {
        name: 'P9'
      },
      {
        name: 'Staging'
      },
      {
        name: 'Default'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'IoPortType',
    description: 'Kind of I/O pin to use.',
    values: [
      {
        name: 'None'
      },
      {
        name: 'AnalogInput'
      },
      {
        name: 'AnalogOutput'
      },
      {
        name: 'DigitalInput'
      },
      {
        name: 'DigitalOutput'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'OscilloscopeDataSource',
    description: 'Kind of channel to record in the Oscilloscope.',
    values: [
      {
        name: 'Setting'
      },
      {
        name: 'Io'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'TriggerCondition',
    description: 'Comparison operator for trigger condition.',
    values: [
      {
        name: 'EQ',
        value: 0,
        description: 'Equal To (==)'
      },
      {
        name: 'NE',
        value: 1,
        description: 'Not Equal To (!=)'
      },
      {
        name: 'GT',
        value: 2,
        description: 'Greater Than (>)'
      },
      {
        name: 'GE',
        value: 3,
        description: 'Greater Than or Equal To (>=)'
      },
      {
        name: 'LT',
        value: 4,
        description: 'Less Than (<)'
      },
      {
        name: 'LE',
        value: 5,
        description: 'Less Than or Equal To (<=)'
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'TriggerAction',
    description: 'Trigger action identifier.',
    values: [
      {
        name: 'All',
        value: 0
      },
      {
        name: 'A',
        value: 1
      },
      {
        name: 'B',
        value: 2
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'TriggerOperation',
    description: 'Operation for trigger action.',
    values: [
      {
        name: 'SetTo',
        value: 0
      },
      {
        name: 'IncrementBy',
        value: 1
      },
      {
        name: 'DecrementBy',
        value: 2
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'DigitalOutputAction',
    description: 'Action type for digital output.',
    values: [
      {
        name: 'Off',
        value: 0
      },
      {
        name: 'On',
        value: 1
      },
      {
        name: 'Toggle',
        value: 2
      },
      {
        name: 'Keep',
        value: 3
      }
    ]
  }
] as DtoEnum[];
