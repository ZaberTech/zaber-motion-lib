import { DtoClass, DtoProperty } from '../../types';

const responseProperties: DtoProperty[] = [{
  description: 'Number of the device that sent the message.',
  name: 'DeviceAddress',
  type: 'int'
}, {
  description: 'Number of the axis which the response applies to. Zero denotes device scope.',
  name: 'AxisNumber',
  type: 'int'
}, {
  description: 'The reply flag indicates if the request was accepted (OK) or rejected (RJ).',
  name: 'ReplyFlag',
  type: 'string'
}, {
  description: 'The device status contains BUSY when the axis is moving and IDLE otherwise.',
  name: 'Status',
  type: 'string'
}, {
  description: 'The warning flag contains the highest priority warning currently active for the device or axis.',
  name: 'WarningFlag',
  type: 'string'
}, {
  description: 'Response data which varies depending on the request.',
  name: 'Data',
  type: 'string'
}, {
  description: 'Type of the reply received.',
  name: 'MessageType',
  type: { type: 'enum', name: 'MessageType', namespace: 'ascii' },
}];

const alertProperties: DtoProperty[] = responseProperties.filter(property =>
  property.name !== 'ReplyFlag' && property.name !== 'MessageType');

export default [
  {
    name: 'Response',
    namespace: 'ascii',
    description: 'Response message from the device.',
    properties: responseProperties,
  },
  {
    name: 'DeviceIdentity',
    namespace: 'ascii',
    description: 'Representation of data gathered during device identification.',
    properties: [
      {
        description: 'Unique ID of the device hardware.',
        name: 'DeviceId',
        type: 'int'
      },
      {
        description: 'Serial number of the device.',
        name: 'SerialNumber',
        type: 'uint'
      },
      {
        description: 'Name of the product.',
        name: 'Name',
        type: 'string'
      },
      {
        description: 'Number of axes this device has.',
        name: 'AxisCount',
        type: 'int'
      },
      {
        description: 'Version of the firmware.',
        name: 'FirmwareVersion',
        type: {
          type: 'ref',
          name: 'FirmwareVersion',
          namespace: []
        }
      },
      {
        description: 'The device has hardware modifications.',
        name: 'IsModified',
        type: 'bool'
      },
      {
        description: 'The device is an integrated product.',
        name: 'IsIntegrated',
        type: 'bool'
      }
    ]
  },
  {
    name: 'AxisIdentity',
    namespace: 'ascii',
    description: 'Representation of data gathered during axis identification.',
    properties: [
      {
        description: 'Unique ID of the peripheral hardware.',
        name: 'PeripheralId',
        type: 'int'
      },
      {
        description: 'Name of the peripheral.',
        name: 'PeripheralName',
        type: 'string'
      },
      {
        description: 'Serial number of the peripheral, or 0 when not applicable.',
        name: 'PeripheralSerialNumber',
        type: 'uint'
      },
      {
        description: 'Indicates whether the axis is a peripheral or part of an integrated device.',
        name: 'IsPeripheral',
        type: 'bool'
      },
      {
        description: 'Determines the type of an axis and units it accepts.',
        name: 'AxisType',
        type: {
          type: 'enum',
          name: 'AxisType',
          namespace: 'ascii'
        }
      },
      {
        description: 'The peripheral has hardware modifications.',
        name: 'IsModified',
        type: 'bool'
      }
    ]
  },
  {
    name: 'DeviceIOInfo',
    namespace: 'ascii',
    description: 'Class representing information on the I/O channels of the device.',
    properties: [
      {
        description: 'Number of analog output channels.',
        name: 'NumberAnalogOutputs',
        type: 'int'
      },
      {
        description: 'Number of analog input channels.',
        name: 'NumberAnalogInputs',
        type: 'int'
      },
      {
        description: 'Number of digital output channels.',
        name: 'NumberDigitalOutputs',
        type: 'int'
      },
      {
        description: 'Number of digital input channels.',
        name: 'NumberDigitalInputs',
        type: 'int'
      }
    ]
  },
  {
    name: 'IoPortLabel',
    namespace: 'ascii',
    description: 'The label of an IO port.',
    properties: [
      {
        description: 'The type of the port.',
        name: 'PortType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        }
      },
      {
        description: 'The number of the port.',
        name: 'ChannelNumber',
        type: 'int'
      },
      {
        description: 'The label of the port.',
        name: 'Label',
        type: 'string'
      }
    ]
  },
  {
    name: 'UnknownResponseEvent',
    namespace: 'ascii',
    description: 'Reply that could not be matched to a request.',
    properties: responseProperties,
  },
  {
    name: 'AlertEvent',
    namespace: 'ascii',
    description: 'Alert message received from the device.',
    properties: alertProperties,
  },
  {
    name: 'LockstepAxes',
    namespace: 'ascii',
    description: 'The axis numbers of a lockstep group.',
    properties: [
      {
        description: 'The axis number used to set the first axis.',
        name: 'Axis1',
        type: 'int'
      },
      {
        description: 'The axis number used to set the second axis.',
        name: 'Axis2',
        type: 'int'
      },
      {
        description: 'The axis number used to set the third axis.',
        name: 'Axis3',
        type: 'int'
      },
      {
        description: 'The axis number used to set the fourth axis.',
        name: 'Axis4',
        type: 'int'
      }
    ]
  },
  {
    name: 'OscilloscopeCaptureProperties',
    namespace: 'ascii',
    description: 'The public properties of one channel of recorded oscilloscope data.',
    properties: [
      {
        name: 'DataSource',
        type: {
          type: 'enum',
          name: 'OscilloscopeDataSource',
          namespace: 'ascii'
        },
        description: 'Indicates whether the data came from a setting or an I/O pin.'
      },
      {
        name: 'Setting',
        type: 'string',
        description: 'The name of the recorded setting.'
      },
      {
        name: 'AxisNumber',
        type: 'int',
        description: 'The number of the axis the data was recorded from, or 0 for the controller.'
      },
      {
        name: 'IoType',
        type: {
          type: 'enum',
          name: 'IoPortType',
          namespace: 'ascii'
        },
        description: 'Which kind of I/O port data was recorded from.'
      },
      {
        name: 'IoChannel',
        type: 'int',
        description: 'Which I/O pin within the port was recorded.'
      }
    ]
  },
  {
    name: 'StreamAxisDefinition',
    namespace: 'ascii',
    description: 'Defines an axis of the stream.',
    properties: [
      {
        description: 'Number of a physical axis or a lockstep group.',
        name: 'AxisNumber',
        type: 'int'
      },
      {
        description: 'Defines the type of the axis.',
        name: 'AxisType',
        type: {
          type: 'optional',
          of: {
            type: 'enum',
            name: 'StreamAxisType',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'PvtAxisDefinition',
    namespace: 'ascii',
    description: 'Defines an axis of the PVT sequence.',
    properties: [
      {
        description: 'Number of a physical axis or a lockstep group.',
        name: 'AxisNumber',
        type: 'int'
      },
      {
        description: 'Defines the type of the axis.',
        name: 'AxisType',
        type: {
          type: 'optional',
          of: {
            type: 'enum',
            name: 'PvtAxisType',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'SetStateAxisResponse',
    namespace: 'ascii',
    description: 'An object containing any non-blocking issues encountered when loading a saved state to an axis.',
    properties: [
      {
        name: 'Warnings',
        description: 'The warnings encountered when applying this state to the given axis.',
        type: {
          type: 'array',
          of: 'string'
        }
      },
      {
        name: 'AxisNumber',
        description: 'The number of the axis that was set.',
        type: 'int'
      }
    ]
  },
  {
    name: 'SetStateDeviceResponse',
    namespace: 'ascii',
    description: 'An object containing any non-blocking issues encountered when loading a saved state to a device.',
    properties: [
      {
        name: 'Warnings',
        description: 'The warnings encountered when applying this state to the given device.',
        type: {
          type: 'array',
          of: 'string'
        }
      },
      {
        name: 'AxisResponses',
        description: 'A list of warnings encountered when applying this state to the device\'s axes.',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'SetStateAxisResponse',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'CanSetStateAxisResponse',
    namespace: 'ascii',
    description: 'An object containing any setup issues that will prevent setting a state to a given axis.',
    properties: [
      {
        name: 'Error',
        description: 'The error blocking applying this state to the given axis.',
        type: {
          type: 'optional',
          of: 'string'
        }
      },
      {
        name: 'AxisNumber',
        description: 'The number of the axis that cannot be set.',
        type: 'int'
      }
    ]
  },
  {
    name: 'CanSetStateDeviceResponse',
    namespace: 'ascii',
    description: 'An object containing any setup issues that will prevent setting a state to a given device.',
    properties: [
      {
        name: 'Error',
        description: 'The error blocking applying this state to the given device.',
        type: {
          type: 'optional',
          of: 'string'
        }
      },
      {
        name: 'AxisErrors',
        description: 'A list of errors that block setting state of device\'s axes.',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'CanSetStateAxisResponse',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'ServoTuningParam',
    namespace: 'ascii',
    description: 'A parameter used to establish the servo tuning of an axis.',
    properties: [
      {
        name: 'Name',
        description: 'The name of the parameter to set.',
        type: 'string'
      },
      {
        name: 'Value',
        description: 'The value to use for this parameter.',
        type: 'double'
      }
    ]
  },
  {
    name: 'ParamsetInfo',
    namespace: 'ascii',
    description: 'The raw parameters currently saved to a given paramset.',
    properties: [
      {
        name: 'Type',
        description: 'The tuning algorithm used for this axis.',
        type: 'string'
      },
      {
        name: 'Version',
        description: 'The version of the tuning algorithm used for this axis.',
        type: 'int'
      },
      {
        name: 'Params',
        description: 'The raw tuning parameters of this device.',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'ServoTuningParam',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    name: 'PidTuning',
    namespace: 'ascii',
    description: 'The tuning of this axis represented by PID parameters.',
    properties: [
      {
        name: 'Type',
        description: 'The tuning algorithm used to tune this axis.',
        type: 'string'
      },
      {
        name: 'Version',
        description: 'The version of the tuning algorithm used to tune this axis.',
        type: 'int'
      },
      {
        name: 'P',
        description: 'The positional tuning argument.',
        type: 'double'
      },
      {
        name: 'I',
        description: 'The integral tuning argument.',
        type: 'double'
      },
      {
        name: 'D',
        description: 'The derivative tuning argument.',
        type: 'double'
      },
      {
        name: 'Fc',
        description: 'The frequency cutoff for the tuning.',
        type: 'double'
      }
    ]
  },
  {
    name: 'SimpleTuningParamDefinition',
    namespace: 'ascii',
    description: 'Information about a parameter used for the simple tuning method.',
    properties: [
      {
        name: 'Name',
        description: 'The name of the parameter.',
        type: 'string'
      },
      {
        name: 'MinLabel',
        description: 'The human readable description of the effect of a lower value on this setting.',
        type: 'string'
      },
      {
        name: 'MaxLabel',
        description: 'The human readable description of the effect of a higher value on this setting.',
        type: 'string'
      },
      {
        name: 'DataType',
        description: 'How this parameter will be parsed by the tuner.',
        type: 'string'
      },
      {
        name: 'DefaultValue',
        description: 'The default value of this parameter.',
        type: {
          of: 'double',
          type: 'optional'
        }
      }
    ]
  },
  {
    name: 'SimpleTuning',
    namespace: 'ascii',
    description: 'The masses and parameters last used by simple tuning.',
    properties: [
      {
        name: 'IsUsed',
        description: [
          'Whether the tuning returned is currently in use by this paramset,',
          'or if it has been overwritten by a later change.'
        ],
        type: 'bool'
      },
      {
        name: 'CarriageMass',
        description: 'The mass of the carriage in kg.',
        type: {
          of: 'double',
          type: 'optional'
        }
      },
      {
        name: 'LoadMass',
        description: 'The mass of the load in kg, excluding the mass of the carriage.',
        type: 'double'
      },
      {
        name: 'TuningParams',
        description: 'The parameters used by simple tuning.',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'ServoTuningParam',
            namespace: 'ascii'
          }
        }
      }
    ]
  },
  {
    namespace: 'ascii',
    name: 'ConversionFactor',
    description: 'Represents unit conversion factor for a single dimension.',
    properties: [
      {
        description: 'Setting representing the dimension.',
        name: 'Setting',
        type: 'string'
      },
      {
        description: 'Value representing 1 native device unit in specified real-word units.',
        name: 'Value',
        type: 'double'
      },
      {
        description: 'Units of the value.',
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'GetSetting',
    namespace: 'ascii',
    description: 'Specifies a setting to get with one of the multi-get commands.',
    properties: [
      {
        description: 'The setting to read.',
        name: 'Setting',
        type: 'string'
      },
      {
        description: 'The list of axes to read.',
        name: 'Axes',
        type: {
          type: 'optional',
          of: {
            type: 'array',
            of: 'int'
          }
        }
      },
      {
        description: 'The unit to convert the read settings to.',
        name: 'Unit',
        type: {
          type: 'optional',
          of: 'Units'
        }
      }
    ]
  },
  {
    name: 'GetAxisSetting',
    namespace: 'ascii',
    description: 'Specifies a setting to get with one of the multi-get commands.',
    properties: [
      {
        description: 'The setting to read.',
        name: 'Setting',
        type: 'string'
      },
      {
        description: 'The unit to convert the read setting to.',
        name: 'Unit',
        type: {
          of: 'Units',
          type: 'optional'
        }
      }
    ]
  },
  {
    name: 'GetSettingResult',
    namespace: 'ascii',
    description: 'The response from a multi-get command.',
    properties: [
      {
        description: 'The setting read.',
        name: 'Setting',
        type: 'string'
      },
      {
        description: 'The list of values returned.',
        name: 'Values',
        type: {
          of: 'double',
          type: 'array'
        }
      },
      {
        description: 'The unit of the values.',
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'GetAxisSettingResult',
    namespace: 'ascii',
    description: 'The response from a multi-get axis command.',
    properties: [
      {
        description: 'The setting read.',
        name: 'Setting',
        type: 'string'
      },
      {
        description: 'The value read.',
        name: 'Value',
        type: 'double'
      },
      {
        description: 'The unit of the values.',
        name: 'Unit',
        type: 'Units'
      }
    ]
  },
  {
    name: 'TriggerState',
    namespace: 'ascii',
    description: 'The complete state of a trigger.',
    properties: [
      {
        name: 'Condition',
        type: 'string',
        description: 'The firing condition for a trigger.'
      },
      {
        name: 'Actions',
        type: {
          of: 'string',
          type: 'array'
        },
        description: 'The actions for a trigger.'
      },
      {
        name: 'Enabled',
        type: 'bool',
        description: 'The enabled state for a trigger.'
      },
      {
        name: 'FiresTotal',
        type: 'int',
        description: [
          'The number of total fires for this trigger.',
          'A value of -1 indicates unlimited fires.'
        ]
      },
      {
        name: 'FiresRemaining',
        type: 'int',
        description: [
          'The number of remaining fires for this trigger.',
          'A value of -1 indicates unlimited fires remaining.'
        ]
      }
    ]
  },
  {
    name: 'TriggerEnabledState',
    namespace: 'ascii',
    description: [
      'The enabled state of a single trigger.',
      'Returns whether the given trigger is enabled and the number of times it will fire.',
      'This is a subset of the complete state, and is faster to query.'
    ],
    properties: [
      {
        name: 'Enabled',
        type: 'bool',
        description: 'The enabled state for a trigger.'
      },
      {
        name: 'FiresRemaining',
        type: 'int',
        description: [
          'The number of remaining fires for this trigger.',
          'A value of -1 indicates unlimited fires remaining.'
        ]
      }
    ]
  }
] as DtoClass[];
