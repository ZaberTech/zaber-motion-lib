import classes from './classes';
import enums from './enums';

export const ascii = {
  classes,
  enums,
};
