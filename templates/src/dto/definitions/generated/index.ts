import binaryCommands from './binary_commands';
import binaryErrors from './binary_errors';
import binaryReplies from './binary_replies';
import binarySettings from './binary_settings';

export const generatedEnums = [
  binaryCommands,
  binaryErrors,
  binaryReplies,
  binarySettings,
];
