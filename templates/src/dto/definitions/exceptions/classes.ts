import { DtoClass } from '../../types';

export default [
  {
    name: 'InvalidPacketExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for the InvalidPacketException.',
    properties: [
      {
        description: 'The invalid packet that caused the exception.',
        name: 'Packet',
        type: 'string'
      },
      {
        description: 'The reason for the exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'DeviceAddressConflictExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for DeviceAddressConflictException.',
    properties: [
      {
        description: 'The full list of detected device addresses.',
        name: 'DeviceAddresses',
        type: {
          of: 'int',
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'MovementInterruptedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for MovementInterruptedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      },
      {
        description: 'The address of the device that caused the interruption.',
        name: 'Device',
        type: 'int'
      },
      {
        description: 'The number of the axis that caused the interruption.',
        name: 'Axis',
        type: 'int'
      }
    ]
  },
  {
    name: 'StreamMovementInterruptedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for StreamMovementInterruptedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'PvtMovementInterruptedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for PvtMovementInterruptedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'MovementFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for MovementFailedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      },
      {
        description: 'The address of the device that performed the failed movement.',
        name: 'Device',
        type: 'int'
      },
      {
        description: 'The number of the axis that performed the failed movement.',
        name: 'Axis',
        type: 'int'
      }
    ]
  },
  {
    name: 'StreamMovementFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for StreamMovementFailedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'PvtMovementFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for PvtMovementFailedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'StreamExecutionExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for StreamExecutionException.',
    properties: [
      {
        description: 'The error flag that caused the exception.',
        name: 'ErrorFlag',
        type: 'string'
      },
      {
        description: 'The reason for the exception.',
        name: 'Reason',
        type: 'string'
      }
    ]
  },
  {
    name: 'PvtExecutionExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for PvtExecutionException.',
    properties: [
      {
        description: 'The error flag that caused the exception.',
        name: 'ErrorFlag',
        type: 'string'
      },
      {
        description: 'The reason for the exception.',
        name: 'Reason',
        type: 'string'
      },
      {
        name: 'InvalidPoints',
        description: 'A list of points that cause the error (if applicable).',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'InvalidPvtPoint',
            namespace: 'exceptions'
          }
        }
      }
    ]
  },
  {
    name: 'InvalidPvtPoint',
    namespace: 'exceptions',
    description: 'Contains invalid PVT points for PvtExecutionException.',
    properties: [
      {
        description: 'Index of the point numbered from the last submitted point.',
        name: 'Index',
        type: 'int'
      },
      {
        description: 'The textual representation of the point.',
        name: 'Point',
        type: 'string'
      }
    ]
  },
  {
    name: 'OperationFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for OperationFailedException.',
    properties: [
      {
        description: 'The full list of warnings.',
        name: 'Warnings',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        description: 'The reason for the Exception.',
        name: 'Reason',
        type: 'string'
      },
      {
        description: 'The address of the device that attempted the failed operation.',
        name: 'Device',
        type: 'int'
      },
      {
        description: 'The number of the axis that attempted the failed operation.',
        name: 'Axis',
        type: 'int'
      }
    ]
  },
  {
    name: 'InvalidResponseExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for InvalidResponseException.',
    properties: [
      {
        description: 'The response data.',
        name: 'Response',
        type: 'string'
      }
    ]
  },
  {
    name: 'CommandFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for CommandFailedException.',
    properties: [
      {
        description: 'The command that got rejected.',
        name: 'Command',
        type: 'string'
      },
      {
        description: 'The data from the reply containing the rejection reason.',
        name: 'ResponseData',
        type: 'string'
      },
      {
        description: 'The flag indicating that the command was rejected.',
        name: 'ReplyFlag',
        type: 'string'
      },
      {
        description: 'The current device or axis status.',
        name: 'Status',
        type: 'string'
      },
      {
        description: 'The highest priority warning flag on the device or axis.',
        name: 'WarningFlag',
        type: 'string'
      },
      {
        description: 'The address of the device that rejected the command.',
        name: 'DeviceAddress',
        type: 'int'
      },
      {
        description: 'The number of the axis which the rejection relates to.',
        name: 'AxisNumber',
        type: 'int'
      },
      {
        description: 'The message ID of the reply.',
        name: 'Id',
        type: 'int'
      }
    ]
  },
  {
    name: 'BinaryCommandFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for BinaryCommandFailedException.',
    properties: [
      {
        description: 'The response data.',
        name: 'ResponseData',
        type: 'int'
      }
    ]
  },
  {
    name: 'SetPeripheralStateExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for a SetPeripheralStateFailedException.',
    properties: [
      {
        name: 'AxisNumber',
        description: 'The number of axis where the exception originated.',
        type: 'int'
      },
      {
        name: 'Settings',
        description: 'A list of settings which could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'ServoTuning',
        description: 'The reason servo tuning could not be set.',
        type: 'string'
      },
      {
        name: 'StoredPositions',
        description: 'The reasons stored positions could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'Storage',
        description: 'The reasons storage could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      }
    ]
  },
  {
    name: 'SetDeviceStateExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for a SetDeviceStateFailedException.',
    properties: [
      {
        name: 'Settings',
        description: 'A list of settings which could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'StreamBuffers',
        description: 'The reason the stream buffers could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'PvtBuffers',
        description: 'The reason the pvt buffers could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'Triggers',
        description: 'The reason the triggers could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'ServoTuning',
        description: 'The reason servo tuning could not be set.',
        type: 'string'
      },
      {
        name: 'StoredPositions',
        description: 'The reasons stored positions could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'Storage',
        description: 'The reasons storage could not be set.',
        type: {
          of: 'string',
          type: 'array'
        }
      },
      {
        name: 'Peripherals',
        description: 'Errors for any peripherals that could not be set.',
        type: {
          type: 'array',
          of: {
            type: 'ref',
            name: 'SetPeripheralStateExceptionData',
            namespace: 'exceptions'
          }
        }
      }
    ]
  },
  {
    name: 'CommandTooLongExceptionData',
    namespace: 'exceptions',
    description: 'Information describing why the command could not fit.',
    properties: [
      {
        name: 'Fit',
        description: 'The part of the command that could be successfully fit in the space provided by the protocol.',
        type: 'string'
      },
      {
        name: 'Remainder',
        description: 'The part of the command that could not fit within the space provided.',
        type: 'string'
      },
      {
        name: 'PacketSize',
        description: 'The length of the ascii string that can be written to a single line.',
        type: 'int'
      },
      {
        name: 'PacketsMax',
        description: 'The number of lines a command can be split over using continuations.',
        type: 'int'
      }
    ]
  },
  {
    name: 'DeviceDbFailedExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for a DeviceDbFailedException.',
    properties: [
      {
        name: 'Code',
        description: 'Code describing type of the error.',
        type: 'string'
      }
    ]
  },
  {
    name: 'GCodeSyntaxExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for GCodeSyntaxException.',
    properties: [
      {
        description: 'The index in the block string that caused the exception.',
        name: 'FromBlock',
        type: 'int'
      },
      {
        description: [
          'The end index in the block string that caused the exception.',
          'The end index is exclusive.'
        ],
        name: 'ToBlock',
        type: 'int'
      }
    ]
  },
  {
    name: 'GCodeExecutionExceptionData',
    namespace: 'exceptions',
    description: 'Contains additional data for GCodeExecutionException.',
    properties: [
      {
        description: 'The index in the block string that caused the exception.',
        name: 'FromBlock',
        type: 'int'
      },
      {
        description: [
          'The end index in the block string that caused the exception.',
          'The end index is exclusive.'
        ],
        name: 'ToBlock',
        type: 'int'
      }
    ]
  }
] as DtoClass[];
