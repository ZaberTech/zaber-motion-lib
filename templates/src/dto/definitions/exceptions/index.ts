import { DtoEnum } from '../../types';

import classes from './classes';

export const exceptions = {
  classes,
  enums: [] as DtoEnum[],
};
