import { DtoEnum } from '../../types';

export default [
  {
    name: 'DeviceType',
    namespace: 'binary',
    description: 'Denotes type of an device and units it accepts.',
    values: [
      {
        name: 'Unknown'
      },
      {
        name: 'Linear'
      },
      {
        name: 'Rotary'
      }
    ]
  },
] as DtoEnum[];
