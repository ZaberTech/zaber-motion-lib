import { DtoClass, DtoProperty } from '../../types';

const messageProperties: DtoProperty[] = [{
  description: 'Number of the device that sent or should receive the message.',
  name: 'DeviceAddress',
  type: 'int'
}, {
  description: 'The warning flag contains the highest priority warning currently active for the device or axis.',
  name: 'Command',
  type: 'int'
}, {
  description: 'Data payload of the message, if applicable, or zero otherwise.',
  name: 'Data',
  type: 'int'
}];

export default [
  {
    name: 'Message',
    namespace: 'binary',
    description: 'A message in the Binary protocol.',
    properties: messageProperties,
  },
  {
    name: 'DeviceIdentity',
    namespace: 'binary',
    description: 'Representation of data gathered during device identification.',
    properties: [
      {
        description: 'Unique ID of the device hardware.',
        name: 'DeviceId',
        type: 'int'
      },
      {
        description: [
          'Serial number of the device.',
          'Requires at least Firmware 6.15 for devices or 6.24 for peripherals.'
        ],
        name: 'SerialNumber',
        type: 'uint'
      },
      {
        description: 'Name of the product.',
        name: 'Name',
        type: 'string'
      },
      {
        description: 'Version of the firmware.',
        name: 'FirmwareVersion',
        type: {
          type: 'ref',
          name: 'FirmwareVersion',
          namespace: []
        }
      },
      {
        description: 'Indicates whether the device is a peripheral or part of an integrated device.',
        name: 'IsPeripheral',
        type: 'bool'
      },
      {
        description: 'Unique ID of the peripheral hardware.',
        name: 'PeripheralId',
        type: 'int'
      },
      {
        description: 'Name of the peripheral hardware.',
        name: 'PeripheralName',
        type: 'string'
      },
      {
        description: 'Determines the type of an device and units it accepts.',
        name: 'DeviceType',
        type: {
          type: 'enum',
          name: 'DeviceType',
          namespace: 'binary'
        }
      }
    ]
  },
  {
    name: 'UnknownResponseEvent',
    description: 'Reply that could not be matched to a request.',
    namespace: 'binary',
    properties: messageProperties,
  },
  {
    name: 'ReplyOnlyEvent',
    description: 'Spontaneous message received from the device.',
    namespace: 'binary',
    properties: messageProperties,
  }
] as DtoClass[];
