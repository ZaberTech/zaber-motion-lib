import classes from './classes';
import enums from './enums';

export const root = {
  classes,
  enums,
};
