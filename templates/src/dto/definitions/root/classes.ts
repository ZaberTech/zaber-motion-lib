import { DtoClass } from '../../types';

export default [
  {
    name: 'FirmwareVersion',
    namespace: [],
    description: 'Class representing version of firmware in the controller.',
    properties: [
      {
        description: 'Major version number.',
        name: 'Major',
        type: 'int'
      },
      {
        description: 'Minor version number.',
        name: 'Minor',
        type: 'int'
      },
      {
        description: 'Build version number.',
        name: 'Build',
        type: 'int'
      }
    ]
  },
  {
    name: 'Measurement',
    namespace: [],
    description: 'Represents a numerical value with optional units specified.',
    properties: [
      {
        description: 'Value of the measurement.',
        name: 'Value',
        type: 'double'
      },
      {
        description: 'Optional units of the measurement.',
        name: 'Unit',
        type: {
          of: 'Units',
          type: 'optional'
        }
      }
    ]
  },
  {
    name: 'AxisAddress',
    namespace: [],
    description: 'Holds device address and axis number.',
    properties: [
      {
        description: 'Device address.',
        name: 'Device',
        type: 'int'
      },
      {
        description: 'Axis number.',
        name: 'Axis',
        type: 'int'
      }
    ]
  }, {
    name: 'NamedParameter',
    namespace: [],
    description: 'Named parameter with optional value.',
    properties: [
      {
        name: 'Name',
        description: 'Name of the parameter.',
        type: 'string'
      },
      {
        name: 'Value',
        description: 'Optional value of the parameter.',
        type: {
          type: 'optional',
          of: 'double',
        },
      },
    ],
  }, {
    name: 'ChannelAddress',
    namespace: [],
    description: 'Holds device address and IO channel number.',
    properties: [
      {
        description: 'Device address.',
        name: 'Device',
        type: 'int'
      },
      {
        description: 'IO channel number.',
        name: 'Channel',
        type: 'int',
      }
    ]
  }
] as DtoClass[];
