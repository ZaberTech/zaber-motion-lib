import { DtoEnum } from '../../types';

export default [
  {
    name: 'LogOutputMode',
    namespace: [],
    description: 'Mode of logging output of the library.',
    values: [
      {
        name: 'Off'
      },
      {
        name: 'Stdout'
      },
      {
        name: 'Stderr'
      },
      {
        name: 'File'
      }
    ]
  },
  {
    name: 'DeviceDbSourceType',
    namespace: [],
    description: 'Type of source of Device DB data.',
    values: [
      {
        name: 'WebService'
      },
      {
        name: 'File'
      }
    ]
  },
  {
    name: 'RotationDirection',
    namespace: [],
    description: 'Direction of rotation.',
    values: [
      {
        name: 'Clockwise',
        value: 0
      },
      {
        name: 'Counterclockwise',
        value: 1
      },
      {
        name: 'CW',
        value: 0
      },
      {
        name: 'CCW',
        value: 1
      }
    ]
  }
] as DtoEnum[];
