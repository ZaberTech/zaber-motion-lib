import { DtoEnum } from '../../types';

export default [
  {
    name: 'WdiDataTypes',
    namespace: 'microscopy',
    values: [
      {
        name: 'NoData'
      },
      {
        name: 'DtByte'
      },
      {
        name: 'DtWord'
      },
      {
        name: 'DtDWord'
      }
    ]
  },
] as DtoEnum[];
