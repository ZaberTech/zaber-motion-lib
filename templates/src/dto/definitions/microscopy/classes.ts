import { DtoClass } from '../../types';

export default [
  {
    name: 'MicroscopeConfig',
    namespace: 'microscopy',
    description: [
      'Configuration representing a microscope setup.',
      'Device address of value 0 means that the part is not present.'
    ],
    properties: [
      {
        name: 'FocusAxis',
        description: 'Focus axis of the microscope.',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'AxisAddress',
            namespace: []
          }
        }
      },
      {
        name: 'XAxis',
        description: 'X axis of the microscope.',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'AxisAddress',
            namespace: []
          }
        }
      },
      {
        name: 'YAxis',
        description: 'Y axis of the microscope.',
        type: {
          type: 'optional',
          of: {
            type: 'ref',
            name: 'AxisAddress',
            namespace: []
          }
        }
      },
      {
        description: 'Illuminator device address.',
        name: 'Illuminator',
        type: {
          type: 'optional',
          of: 'int'
        }
      },
      {
        description: 'Filter changer device address.',
        name: 'FilterChanger',
        type: {
          type: 'optional',
          of: 'int'
        }
      },
      {
        description: 'Objective changer device address.',
        name: 'ObjectiveChanger',
        type: {
          type: 'optional',
          of: 'int'
        }
      },
      {
        description: 'Autofocus identifier.',
        name: 'Autofocus',
        type: {
          type: 'optional',
          of: 'int'
        }
      },
      {
        description: 'Camera trigger digital output address.',
        name: 'CameraTrigger',
        type: {
          type: 'optional',
          of: { type: 'ref', name: 'ChannelAddress', namespace: [] },
        }
      },
    ]
  }, {
    name: 'ThirdPartyComponents',
    description: 'Third party components of the microscope.',
    namespace: 'microscopy',
    properties: [
      {
        description: 'Autofocus provider identifier.',
        name: 'Autofocus',
        type: {
          type: 'optional',
          of: 'int'
        }
      },
    ],
  }, {
    name: 'AutofocusStatus',
    description: 'Status of the autofocus.',
    namespace: 'microscopy',
    properties: [{
      name: 'InFocus',
      description: 'Indicates whether the autofocus is in focus.',
      type: 'bool'
    }, {
      name: 'InRange',
      description: 'Indicates whether the autofocus is in range.',
      type: 'bool'
    }],
  }, {
    name: 'WdiAutofocusProviderStatus',
    description: 'Status of the WDI autofocus.',
    namespace: 'microscopy',
    properties: [{
      name: 'InRange',
      description: 'Indicates whether the autofocus is in range.',
      type: 'bool',
    }, {
      name: 'LaserOn',
      description: 'Indicates whether the laser is turned on.',
      type: 'bool',
    }],
  },
] as DtoClass[];
