import { DtoEnum } from '../../types';

import classes from './classes';

export const microscopy = {
  classes,
  enums: [] as DtoEnum[],
};
