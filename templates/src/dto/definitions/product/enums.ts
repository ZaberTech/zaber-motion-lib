import { DtoEnum } from '../../types';

export default [
  {
    namespace: 'product',
    name: 'ProcessControllerMode',
    description: 'Servo Tuning Parameter Set to target.',
    values: [
      {
        name: 'Manual'
      },
      {
        name: 'Pid'
      },
      {
        name: 'PidHeater'
      },
      {
        name: 'OnOff'
      }
    ]
  },
  {
    namespace: 'product',
    name: 'ProcessControllerSourceSensor',
    description: 'Servo Tuning Parameter Set to target.',
    values: [
      {
        name: 'Thermistor',
        value: 10
      },
      {
        name: 'AnalogInput',
        value: 20
      }
    ]
  }
] as DtoEnum[];
