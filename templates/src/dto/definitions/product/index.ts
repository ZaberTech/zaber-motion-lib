import classes from './classes';
import enums from './enums';

export const product = {
  classes,
  enums,
};
