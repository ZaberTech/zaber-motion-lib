import { DtoClass } from '../../types';

export default [
  {
    namespace: 'product',
    name: 'ProcessControllerSource',
    description: 'The source used by a process in a closed-loop mode.',
    properties: [
      {
        name: 'Sensor',
        description: 'The type of input sensor.',
        type: {
          type: 'enum',
          name: 'ProcessControllerSourceSensor',
          namespace: 'product'
        }
      },
      {
        name: 'Port',
        description: 'The specific input to use.',
        type: 'int'
      }
    ]
  }
] as DtoClass[];
