import { Definitions } from '../types';

import { generatedEnums } from './generated';
import { ascii } from './ascii';
import { binary } from './binary';
import { exceptions } from './exceptions';
import { microscopy } from './microscopy';
import { product } from './product';
import { gcode } from './gcode';
import { requests } from './requests';
import { root } from './root';

const allNamespaces = [
  ascii,
  binary,
  exceptions,
  gcode,
  microscopy,
  product,
  requests,
  root,
];

const rootDefinitions: Definitions = {
  classes: allNamespaces.flatMap(({ classes }) => classes),
  enums: [
    ...allNamespaces.flatMap(({ enums }) => enums),
    ...generatedEnums,
  ],
};

export const definitions = rootDefinitions;
