import { Namespace } from '../common_types';

export * from '../common_types';

export type PrimitiveType = 'string' | 'uint' | 'int' | 'double' | 'bool' | 'Units';
export type ReferenceType = { type: 'ref'; name: string; namespace: Namespace };
export type EnumType = { type: 'enum'; name: string; namespace: Namespace };
export type StartType = PrimitiveType | ReferenceType | EnumType;

export type OptionalType = { type: 'optional'; of: AnyType };
export type CollectionType = { type: 'array'; of: AnyType };
export type AnyType = StartType | CollectionType | OptionalType;

export type Description = string[] | string;

export interface DtoProperty {
  name: string;
  type: AnyType;
  description?: Description;
}

export interface DtoClass {
  name: string;
  namespace: Namespace;
  description?: Description;
  properties: DtoProperty[];
}

export interface DtoEnum {
  name: string;
  namespace: Namespace;
  description?: Description;
  values: { name: string; value?: number; description?: string }[];
}

export interface Definitions {
  classes: DtoClass[];
  enums: DtoEnum[];
}

export type DtoType = DtoClass | DtoEnum;

export function getBaseType(type: AnyType): StartType {
  if (isBasicType(type)) { return type }
  switch (type.type) {
    case 'ref':
    case 'enum':
      return type;
    default:
      return getBaseType(type.of);
  }
}

export function isBasicType(type: AnyType): type is PrimitiveType {
  return typeof type === 'string';
}

export function isUnitsType(type: AnyType): type is 'Units' {
  return type === 'Units';
}

export function isNumericType(type: AnyType): type is 'uint' | 'int' | 'double' {
  return type === 'uint' || type === 'int' || type === 'double';
}

export function isReferenceType(type: AnyType): type is ReferenceType {
  return typeof type === 'object' && type.type === 'ref';
}

export function isEnumType(type: AnyType): type is EnumType {
  return typeof type === 'object' && type.type === 'enum';
}

export function isOptionalType(type: AnyType): type is OptionalType {
  return typeof type === 'object' && type.type === 'optional';
}

export function isArrayType(type: AnyType): type is CollectionType {
  return typeof type === 'object' && type.type === 'array';
}

export function hasNamespace(type: AnyType): type is ReferenceType | EnumType {
  return isReferenceType(type) || isEnumType(type);
}

export function stripOptional(type: AnyType): AnyType {
  if (isOptionalType(type)) {
    return stripOptional(type.of);
  } else if (isArrayType(type)) {
    return { type: type.type, of: stripOptional(type.of) };
  } else {
    return type;
  }
}

export function typeChain(type: AnyType): AnyType[] {
  if (isArrayType(type) || isOptionalType(type)) {
    return [type, ...typeChain(type.of)];
  } else {
    return [type];
  }
}

export function getItemType(type: AnyType): AnyType {
  const arrayType = typeChain(type).find(isArrayType);
  if (arrayType == null) {
    throw new Error('Expected array type');
  }
  return arrayType.of;
}
