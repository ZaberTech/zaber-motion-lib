import _ from 'lodash';

import { DtoClass, DtoProperty, getBaseType, hasNamespace, namespaceEquals } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace, visibility } from './common';
import { typeMapper } from './type_mapper';
import { b } from './utils';

function generateProperty(property: DtoProperty): Output {
  const defaultValue = typeMapper.mapDefault(property.type);
  return [
    description(property),
    `[JsonProperty("${_.lowerFirst(property.name)}")]`,
    [
      `public ${typeMapper.mapType(property.type)} ${property.name} { get; set; }`,
      defaultValue != null ? ` = ${defaultValue};` : '',
    ].join(''),
    '',
    { indent: 0 },
  ];
}

function generateUsings(dtoClass: DtoClass): Output {
  const basePropertyTypes = dtoClass.properties.map(property => getBaseType(property.type));
  const withNamespace = basePropertyTypes.filter(hasNamespace);
  const otherNamespace = withNamespace.filter(type => !namespaceEquals(type.namespace, dtoClass.namespace));
  const usings = new Set<string>();
  for (const type of otherNamespace) {
    usings.add(`using ${namespace(type)};`);
  }
  return [
    ...usings,
    'using Zaber.Motion.Runtime;',
    'using Zaber.Motion.Utils;',
    '',
    { indent: 0 },
  ];
}

function generateEquals(dtoClass: DtoClass): Output {
  if (dtoClass.properties.length === 0) {
    return null;
  }

  return [
    '/// <summary>',
    '/// Determines whether the specified object is equal to the current object.',
    '/// </summary>',
    '/// <param name="obj">The object to compare with the current object.</param>',
    '/// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>',
    '#pragma warning disable CA1309, CS0472, CA1502',
    'public override bool Equals(object? obj)',
    b(
      `if (obj == null || !(obj is ${dtoClass.name}))`,
      b(
        'return false;',
      ),
      '',
      `${dtoClass.name} other = (${dtoClass.name})obj;`,
      'return (',
      dtoClass.properties.map((p, i) =>
        `EqualityUtils.CheckEquals(${p.name}, other.${p.name})${i < dtoClass.properties.length - 1 ? ' &&' : ''}`),
      ');',
    ),
    { indent: 0 },
    '#pragma warning restore CA1309, CS0472, CA1502',
    '',
  ];
}

function generateGetHashCode(dtoClass: DtoClass): Output {
  if (dtoClass.properties.length === 0) {
    return null;
  }

  return [
    '/// <summary>',
    '/// Serves as the default hash function.',
    '/// </summary>',
    '/// <returns>A hash code for the current object.</returns>',
    'public override int GetHashCode()',
    b(
      'HashCode hash = default(HashCode);',
      ...dtoClass.properties.map(p => `hash.Add(EqualityUtils.GenerateHashCode(${p.name}));`),
      'return hash.ToHashCode();',
    ),
    { indent: 0 },
    '',
  ];
}

export function generateClass(dtoClass: DtoClass): Output {
  return [
    GENERATED_WARNING,
    'using System;',
    'using Newtonsoft.Json;',
    'using Newtonsoft.Json.Bson;',
    generateUsings(dtoClass),
    `namespace ${namespace(dtoClass)}`,
    b(
      description(dtoClass),
      `${visibility(dtoClass)} class ${dtoClass.name} : IMessage`,
      b(
        ...dtoClass.properties.map(generateProperty),
        generateEquals(dtoClass),
        generateGetHashCode(dtoClass),
        '/// <summary>',
        '/// Returns a string that represents the current instance.',
        '/// </summary>',
        '/// <returns>A string that represents the current instance.</returns>',
        'public override string ToString() => ObjectDumper.Dump(this);',
        '',
        `byte[] IMessage.ToByteArray() => ${dtoClass.name}.ToByteArray(this);`,
        '',
        `internal static ${dtoClass.name} FromByteArray(byte[] buffer)`,
        b(
          'using (var stream = new System.IO.MemoryStream(buffer))',
          b(
            'using (var reader = new BsonDataReader(stream))',
            b(
              `return Serialization.Serializer.Deserialize<${dtoClass.name}>(reader);`,
            ),
          ),
        ),
        '',
        `internal static byte[] ToByteArray(${dtoClass.name} instance)`,
        b(
          'using (var stream = new System.IO.MemoryStream())',
          b(
            'using (var writer = new BsonDataWriter(stream))',
            b(
              'Serialization.Serializer.Serialize(writer, instance);',
            ),
            '',
            'return stream.ToArray();',
          ),
        ),
      ),
    ),
    '',
  ];
}
