import { P, match } from 'ts-pattern';

import { AnyType, isArrayType } from '../../types';

const typeMap: Record<string, string> = {
  uint: 'long', // For serial numbers. Unsigned types other than byte are not CLS compliant.
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

function mapType(type: AnyType): string {
  return match(type)
    .with({ type: 'ref' }, ({ name }) => name)
    .with({ type: 'enum' }, ({ name }) => name)
    .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
    .with({ type: 'optional' }, ({ of }) => {
      if (isArrayType(of)) {
        return mapType(of);
      }
      return `${mapType(of)}?`;
    })
    .with(P.string, type => typeMap[type] || type)
    .exhaustive();
}

export const typeMapper = {
  mapType,
  mapDefault(type: AnyType) {
    return match(type)
      .with({ type: 'ref' }, ({ name }) => `new ${name} { }`)
      .with({ type: 'enum' }, () => null)
      .with({ type: 'array' }, ({ of }) => `System.Array.Empty<${mapType(of)}>()`)
      .with({ type: 'optional' }, ({ of }) => {
        if (isArrayType(of)) {
          return `System.Array.Empty<${mapType(of.of)}>()`;
        }
        return null;
      })
      .with(P.string, type => {
        switch (type) {
          case 'string': return 'string.Empty';
          default: return null;
        }
      })
      .exhaustive();
  },
};
