import { OutputNode } from '../output';

export const BASE_INDENT = 4;

export function b(...nodes: OutputNode[]): OutputNode {
  return [
    '{',
    nodes,
    '}',
    { indent: 0 },
  ];
}
