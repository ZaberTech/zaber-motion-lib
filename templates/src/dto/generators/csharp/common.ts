import _ from 'lodash';

import { Description, Namespace } from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

export const GENERATED_WARNING = '/* This file is generated. Do not modify by hand. */';

export function namespace({ namespace }: { namespace: Namespace }): string {
  return ['Zaber', 'Motion', ...ensureArray(namespace).map(n => _.capitalize(n))].join('.');
}

export function visibility({ namespace }: { namespace: Namespace }): string {
  return ensureArray(namespace)[0] === 'requests' ? 'internal' : 'public';
}

export function description({ description }: { description?: Description }): Output {
  if (!description) {
    return null;
  }
  return [
    '/// <summary>',
    ...ensureArray(description).map(line => `/// ${line}`),
    '/// </summary>',
    { indent: 0 },
  ];
}
