import { DtoEnum } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace, visibility } from './common';

export function generateEnum(dtoEnum: DtoEnum): Output {
  return [
    GENERATED_WARNING,
    `namespace ${namespace(dtoEnum)}`,
    '{',
    [
      description(dtoEnum),
      `${visibility(dtoEnum)} enum ${dtoEnum.name}`,
      '{',
      dtoEnum.values.flatMap((value, i) => ([
        `/// <summary>${value.name}.</summary>`,
        `${value.name} = ${value.value ?? i},`,
        '',
      ])),
      '}',
    ],
    '}',
    '',
  ];
}
