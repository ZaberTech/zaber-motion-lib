import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description } from './common';

export function generateEnum(dtoEnum: DtoEnum): Output {
  return [
    GENERATED_WARNING,
    description(dtoEnum),
    `export enum ${dtoEnum.name} {`,
    dtoEnum.values.flatMap((value, i) => ([
      `/** ${value.name}. */`,
      `${_.snakeCase(value.name).toUpperCase()} = ${value.value ?? i},`,
    ])),
    '}',
    '',
  ];
}
