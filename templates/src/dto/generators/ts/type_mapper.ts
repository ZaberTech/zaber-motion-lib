import { P, match } from 'ts-pattern';

import { AnyType } from '../../types';

const typeMap: Record<string, string> = {
  uint: 'number',
  int: 'number',
  double: 'number',
  bool: 'boolean',
};

function mapType(type: AnyType): string {
  return match(type)
    .with({ type: 'ref' }, ({ name }) => name)
    .with({ type: 'enum' }, ({ name }) => name)
    .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
    .with({ type: 'optional' }, ({ of }) => `(${mapType(of)} | null)`)
    .with(P.string, type => typeMap[type] || type)
    .exhaustive();
}

export const typeMapper = {
  mapType,
  mapDefault(type: AnyType) {
    return match(type)
      .with({ type: 'ref' }, ({ name }) => `${name}.DEFAULT`)
      .with({ type: 'enum' }, ({ name }) => `0 as ${name}`)
      .with({ type: 'array' }, () => '[]')
      .with({ type: 'optional' }, () => 'null')
      .with(P.string, type => {
        switch (type) {
          case 'string': return '\'\'';
          case 'bool': return 'false';
          case 'Units': return 'Units.NATIVE';
          default: return '0';
        }
      })
      .exhaustive();
  },
};
