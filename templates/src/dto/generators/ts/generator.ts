import _ from 'lodash';

import type { DtoClass, DtoEnum, Namespace } from '../../types';
import { ensureArray } from '../../../utils';
import { outputToString, type FileOutput } from '../output';
import { LanguageGenerator } from '../types';

import { generateClass } from './class';
import { generateEnum } from './enum';
import { BASE_INDENT } from './utils';

function namespaceToPath({ namespace }: { namespace: Namespace }): string {
  return ensureArray(namespace).join('/');
}

function fileName(something: { name: string; namespace: Namespace }): string {
  const namespace = namespaceToPath(something);
  let path = `${_.snakeCase(something.name)}.ts`;
  if (namespace) {
    path = `${namespace}/${path}`;
  }
  return path;
}

export const tsGenerator: LanguageGenerator = {
  generateClass(dtoClass: DtoClass): FileOutput[] {
    const content = generateClass(dtoClass);

    return [{
      path: fileName(dtoClass),
      content: outputToString(content, BASE_INDENT),
    }];
  },
  generateEnum(dtoEnum: DtoEnum): FileOutput[] {
    const content = generateEnum(dtoEnum);

    return [{
      path: fileName(dtoEnum),
      content: outputToString(content, BASE_INDENT),
    }];
  },
};
