import _ from 'lodash';
import { match, P } from 'ts-pattern';

import {
  AnyType, DtoClass, DtoProperty, getBaseType, hasNamespace, isArrayType, isBasicType, isEnumType, isOptionalType,
  isUnitsType,
} from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace } from './common';
import { typeMapper } from './type_mapper';

function propName(property: DtoProperty): string {
  return _.lowerFirst(property.name);
}

function generateProperty(property: DtoProperty): Output {
  let name: string = propName(property);
  if (isOptionalType(property.type)) {
    name = `${name}?`;
  }
  return [
    description(property),
    `${name}: ${typeMapper.mapType(property.type)};`,
    '',
    { indent: 0 }
  ];
}

function generateImports(dtoClass: DtoClass): Output {
  const basePropertyTypes = dtoClass.properties.map(property => getBaseType(property.type));
  const withNamespace = basePropertyTypes.filter(hasNamespace);
  if (basePropertyTypes.some(isUnitsType)) {
    withNamespace.push({ name: 'Units', namespace: [], type: 'enum' });
  }
  const depth = ensureArray(dtoClass.namespace).length;
  const dotDot = depth > 0 ? _.repeat('../', depth) : './';

  const usings = new Set<string>();
  for (const type of withNamespace) {
    let typePath = namespace(type);
    if (typePath) {
      typePath = `${typePath}/`;
    }
    usings.add(`import { ${type.name} } from '${dotDot}${typePath}${_.snakeCase(type.name)}';`);
  }

  const needSanitizer = basePropertyTypes.some(type => isEnumType(type) || isBasicType(type));

  return [
    needSanitizer ? `import { sanitizer } from '${dotDot}gateway/sanitizer';` : null,
    ...usings,
    '',
    { indent: 0 },
  ];
}

function sanitizeValue(value: string, valueType: AnyType, label: string): string {
  return match(valueType)
    .with({ type: 'optional' }, ({ of }) => {
      if (isArrayType(of)) {
        return sanitizeValue(value, of, label);
      }
      return `${value} != null ? ${sanitizeValue(value, of, label)} : null`;
    })
    .with({ type: 'ref' }, ({ name }) => `${name}.sanitize(${value})`)
    .with({ type: 'enum' }, ({ name }) => `sanitizer.sanitizeEnum(${value}, '${name}', ${name}, '${label}')`)
    .with({ type: 'array' }, ({ of }) => `Array.from(${value} ?? [], item => ${sanitizeValue('item', of, `items of ${label}`)})`)
    .with('Units', () => `sanitizer.sanitizeUnits(${value}, '${label}')`)
    .with('string', () => `sanitizer.sanitizeString(${value}, '${label}')`)
    .with('bool', () => `sanitizer.sanitizeBool(${value}, '${label}')`)
    .with('double', () => `sanitizer.sanitizeNumber(${value}, '${label}')`)
    .with(P.union('int', 'uint'), () => `sanitizer.sanitizeInt(${value}, '${label}')`)
    .exhaustive();
}

function sanitizeProperty(property: DtoProperty): string {
  const name = propName(property);
  const value = sanitizeValue(`value.${name}`, property.type, name);
  return value;
}


function sanitize(dtoClass: DtoClass): Output {
  return [
    `sanitize: (value: ${dtoClass.name}): ${dtoClass.name} => {`,
    [
      `if (value == null) { throw new TypeError('Expected ${dtoClass.name} object but got null or undefined.') }`,
      `if (typeof value !== 'object') { throw new TypeError(\`Expected ${dtoClass.name} object but got \${typeof value}.\`) }`,
      'return {',
      dtoClass.properties.map(property =>
        `${propName(property)}: ${sanitizeProperty(property)},`),
      '};',
    ],
    '},',
    { indent: 0 },
  ];
}

export function generateClass(dtoClass: DtoClass): Output {
  return [
    GENERATED_WARNING,
    'import { BSON } from \'bson\';',
    generateImports(dtoClass),
    description(dtoClass),
    dtoClass.properties.length === 0 ? '// eslint-disable-next-line @typescript-eslint/no-empty-object-type' : null,
    `export interface ${dtoClass.name} {`,
    dtoClass.properties.map(generateProperty),
    '}',
    '',
    `export const ${dtoClass.name} = {`,
    [
      `fromBinary: (buffer: Uint8Array): ${dtoClass.name} => BSON.deserialize(buffer) as ${dtoClass.name},`,
      `toBinary: (value: ${dtoClass.name}): Uint8Array => BSON.serialize(${dtoClass.name}.sanitize(value)),`,
      'DEFAULT: Object.freeze({',
      dtoClass.properties.map(property =>
        `${propName(property)}: ${typeMapper.mapDefault(property.type)},`),
      `}) as Readonly<${dtoClass.name}>,`,
      sanitize(dtoClass),
    ],
    '};',
    '',
  ];
}
