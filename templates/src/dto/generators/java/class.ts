import _ from 'lodash';

import { DtoClass, DtoProperty, getBaseType, hasNamespace, isOptionalType, isUnitsType, namespaceEquals } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace } from './common';
import { typeMapper } from './type_mapper';

function generateProperty(property: DtoProperty, dtoClass: DtoClass): Output {
  const propertyType = typeMapper.mapType(property.type);
  const fieldName = _.lowerFirst(property.name);
  return [
    `private ${propertyType} ${fieldName};`,
    '',
    description(property),
    `@com.fasterxml.jackson.annotation.JsonProperty("${fieldName}")`,
    `public void set${property.name}(${propertyType} ${fieldName}) {`,
    [
      `this.${fieldName} = ${fieldName};`,
    ],
    '}',
    '',
    description(property),
    `@com.fasterxml.jackson.annotation.JsonProperty("${fieldName}")`,
    `public ${propertyType} get${property.name}() {`,
    [
      `return this.${_.lowerFirst(property.name)};`,
    ],
    '}',
    '',
    description(property),
    `public ${dtoClass.name} with${property.name}(${propertyType} a${property.name}) {`,
    [
      `this.set${property.name}(a${property.name});`,
      'return this;',
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

function generateImports(dtoClass: DtoClass): Output {
  const basePropertyTypes = dtoClass.properties.map(property => getBaseType(property.type));
  const withNamespace = basePropertyTypes.filter(hasNamespace);
  if (basePropertyTypes.some(isUnitsType)) {
    withNamespace.push({ name: 'Units', namespace: [], type: 'enum' });
  }
  const otherNamespace = withNamespace.filter(type => !namespaceEquals(type.namespace, dtoClass.namespace));
  const usings = new Set<string>();
  for (const type of otherNamespace) {
    usings.add(`import ${namespace(type)}.${type.name};`);
  }
  if (dtoClass.namespace.length > 0 && dtoClass.properties.length > 0) {
    usings.add('import zaber.motion.EqualityUtility;');
  }
  if (usings.size === 0) {
    return null;
  }
  return [
    ...usings,
    '',
    { indent: 0 },
  ];
}

function isOptionalSort(a: DtoProperty, b: DtoProperty): number {
  return Number(isOptionalType(a.type)) - Number(isOptionalType(b.type));
}

function constructors(dtoClass: DtoClass): Output {
  let lines: Output = [];

  const properties = dtoClass.properties.slice().sort(isOptionalSort);
  const required = properties.filter(property => !isOptionalType(property.type));

  lines = [
    ...lines,
    description({ description: 'Empty constructor.' }),
    `public ${dtoClass.name}() {`,
    '}',
    '',
  ];

  const countsToGenerate = _.uniqBy([
    {
      count: properties.length,
      message: 'Constructor with all properties.',
    }, {
      count: required.length,
      message: 'Constructor with only required properties.',
    },
  ], 'count').filter(({ count }) => count > 0);

  for (const argumentTake of countsToGenerate) {
    const current = _.take(properties, argumentTake.count);
    lines = [
      ...lines,
      description({ description: argumentTake.message }),
      `public ${dtoClass.name}(`,
      current.map((property, i) =>
        `${typeMapper.mapType(property.type)} ${_.lowerFirst(property.name)}${i < current.length - 1 ? ',' : ''}`),
      ') {',
      current.map(property => `this.${_.lowerFirst(property.name)} = ${_.lowerFirst(property.name)};`),
      '}',
      '',
    ];
  }

  return [...lines, { indent: 0 }];
}

function equals(dtoClass: DtoClass): Output {
  if (dtoClass.properties.length === 0) {
    return null;
  }

  return [
    '@Override',
    'public boolean equals(Object obj) {',
    [
      'if (this == obj) {',
      '    return true;',
      '}',
      '',
      'if (obj == null || getClass() != obj.getClass()) {',
      '    return false;',
      '}',
      '',
      `${dtoClass.name} other = (${dtoClass.name}) obj;`,
      '',
      'return (',
      dtoClass.properties.map((p, i) =>
        `${i > 0 ? '&& ' : ''}EqualityUtility.equals(${_.lowerFirst(p.name)}, other.${_.lowerFirst(p.name)})`),
      [');'],
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

function hashCode(dtoClass: DtoClass): Output {
  if (dtoClass.properties.length === 0) {
    return null;
  }

  return [
    '@Override',
    'public int hashCode() {',
    [
      'return Objects.hash(',
      dtoClass.properties.map((p, i) =>
        `EqualityUtility.generateHashCode(${_.lowerFirst(p.name)})${i < dtoClass.properties.length - 1 ? ',' : ''}`),
      ');',
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

function toString(dtoClass: DtoClass): Output {
  return [
    '@Override',
    'public String toString() {',
    [
      'StringBuilder sb = new StringBuilder();',
      `sb.append("${dtoClass.name} { ");`,
      ...dtoClass.properties.flatMap((property, i) => [
        `sb.append("${_.lowerFirst(property.name)}: ");`,
        `sb.append(this.${_.lowerFirst(property.name)});`,
        i < dtoClass.properties.length - 1 ? 'sb.append(", ");' : null,
      ]),
      'sb.append(" }");',
      'return sb.toString();',
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

function serializationMethods(dtoClass: DtoClass): Output {
  return [
    '@Override',
    'public byte[] toByteArray() {',
    '    try {',
    '        return Mapper.getDefault().writeValueAsBytes(this);',
    '    } catch (Exception e) {',
    '        throw new zaber.motion.dto.SerializationException(e);',
    '    }',
    '}',
    '',
    `public static ${dtoClass.name} fromByteArray(byte[] data) {`,
    '    try {',
    `        return Mapper.getDefault().readValue(data, ${dtoClass.name}.class);`,
    '    } catch (Exception e) {',
    '        throw new zaber.motion.dto.SerializationException(e);',
    '    }',
    '}',
    '',
    `static final zaber.motion.dto.Parser<${dtoClass.name}> PARSER = `,
    `    new zaber.motion.dto.Parser<${dtoClass.name}>() {`,
    '        @Override',
    `        public ${dtoClass.name} fromByteArray(byte[] data) {`,
    `            return ${dtoClass.name}.fromByteArray(data);`,
    '        }',
    '    };',
    '',
    `public static zaber.motion.dto.Parser<${dtoClass.name}> parser() {`,
    '    return PARSER;',
    '}',
    '',
    { indent: 0 },
  ];
}

export function generateClass(dtoClass: DtoClass): Output {
  return [
    GENERATED_WARNING,
    '',
    `package ${namespace(dtoClass)};`,
    '',
    dtoClass.properties.length > 0 ?
      [
        'import java.util.Objects;',
        '',
        { indent: 0 }
      ]
      : null,
    'import zaber.motion.dto.Mapper;',
    generateImports(dtoClass),
    '',
    description(dtoClass),
    `public final class ${dtoClass.name} implements zaber.motion.dto.Message {`,
    [
      '',
      ...dtoClass.properties.map(property => generateProperty(property, dtoClass)),
      constructors(dtoClass),
      equals(dtoClass),
      hashCode(dtoClass),
      toString(dtoClass),
      serializationMethods(dtoClass),
    ],
    '}',
    '',
  ];
}
