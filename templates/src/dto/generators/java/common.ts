import { Description, Namespace } from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

export const GENERATED_WARNING = '/* This file is generated. Do not modify by hand. */';

export function namespace({ namespace }: { namespace: Namespace }): string {
  return ['zaber', 'motion', ...ensureArray(namespace)].join('.');
}

export function description({ description }: { description?: Description }): Output {
  if (!description) {
    return null;
  }
  return [
    '/**',
    ...ensureArray(description).map(line => ` * ${line}`),
    ' */',
    { indent: 0 },
  ];
}
