import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace } from './common';

export function generateEnum(dtoEnum: DtoEnum): Output {
  return [
    GENERATED_WARNING,
    '',
    `package ${namespace(dtoEnum)};`,
    '',
    description(dtoEnum),
    `public enum ${dtoEnum.name} {`,
    [
      '',
      ...dtoEnum.values.map((value, i) => ([
        `${_.snakeCase(value.name).toUpperCase()}(${value.value ?? i})${i < dtoEnum.values.length - 1 ? ',' : ';'} `,
        '',
        { indent: 0 },
      ])),

      'private int value;',
      '',
      `${dtoEnum.name}(int value) {`,
      [
        'this.value = value;',
      ],
      '}',
      '',
      '@com.fasterxml.jackson.annotation.JsonValue',
      'public int getValue() {',
      [
        'return value;',
      ],
      '}',
      '',
      `public static ${dtoEnum.name} valueOf(int argValue) {`,
      [
        `for (${dtoEnum.name} value : values()) {`,
        [
          'if (value.value == argValue) {',
          [
            'return value;',
          ],
          '}',
        ],
        '}',
        'throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));',
      ],
      '}',
    ],
    '}',
    '',
  ];
}
