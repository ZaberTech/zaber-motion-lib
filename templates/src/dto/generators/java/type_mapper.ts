import { P, match } from 'ts-pattern';
import _ from 'lodash';

import { AnyType } from '../../types';
import { isMatch } from '../../../utils';

const typeMap: Record<string, string> = {
  bool: 'boolean',
  uint: 'long',
  string: 'String',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

function mapType(type: AnyType): string {
  return match(type)
    .with({ type: 'ref' }, ({ name }) => name)
    .with({ type: 'enum' }, ({ name }) => name)
    .with({ type: 'array' }, ({ of }) => `${mapType(of)}[]`)
    .with({ type: 'optional' }, ({ of }) => {
      const nullable = isMatch(P.string, of);
      let mapped = mapType(of);
      mapped = nullable ? `${_.upperFirst(mapped)}` : mapped;
      if (mapped === 'Int') {
        mapped = 'Integer';
      }
      return mapped;
    })
    .with(P.string, type => typeMap[type] || type)
    .exhaustive();
}

export const typeMapper = {
  mapType,
};
