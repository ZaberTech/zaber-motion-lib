import { csharpGenerator, csharpTypeMapper } from './csharp';
import { goGenerator, goTypeMapper } from './go';
import { javaGenerator, javaTypeMapper } from './java';
import { pyGenerator, pyTypeMapper } from './py';
import { tsGenerator, tsTypeMapper } from './ts';
import { cppGenerator, cppTypeMapper } from './cpp';
import { Languages, LanguageGenerator, TypeMapper } from './types';
import { swiftGenerator, swiftTypeMapper } from './swift';

export type { FileOutput } from './output';
export type { Languages, LanguageGenerator, TypeMapper } from './types';

export const generators = {
  cs: csharpGenerator,
  go: goGenerator,
  ts: tsGenerator,
  cpp: cppGenerator,
  py: pyGenerator,
  java: javaGenerator,
  swift: swiftGenerator,
} satisfies Record<Languages, LanguageGenerator>;

export const typeMappers = {
  cs: csharpTypeMapper,
  go: goTypeMapper,
  ts: tsTypeMapper,
  cpp: cppTypeMapper,
  py: pyTypeMapper,
  java: javaTypeMapper,
  swift: swiftTypeMapper,
} satisfies Record<Languages, TypeMapper>;
