import _ from 'lodash';

import { Description, Namespace } from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

export const GENERATED_WARNING = '/* This file is generated. Do not modify by hand. */';

export function className(dtoClass: { name: string; namespace: Namespace }): string {
  return `${namespace(dtoClass)}${dtoClass.name}`;
}

export function namespace({ namespace }: { namespace: Namespace }): string {
  let namespaceStr = _.capitalize(ensureArray(namespace).map(n => _.lowerCase(n)).join(''));
  if (namespaceStr === 'Requests') {
    namespaceStr = '';
  }
  return namespaceStr;
}

export function description({ description }: { description?: Description }): Output {
  if (!description) {
    return null;
  }
  return [
    ...ensureArray(description).map(line => `// ${line}`),
    { indent: 0 },
  ];
}
