import _ from 'lodash';

import { DtoClass, getItemType, isArrayType, isOptionalType, isReferenceType, stripOptional } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, className, description } from './common';
import { typeMapper } from './type_mapper';

function generateProperty(property: DtoClass['properties'][number]): Output {
  return [
    description(property),
    `${property.name} ${typeMapper.mapType(property.type)} \`bson:"${_.lowerFirst(property.name)}"\``,
    { indent: 0 },
  ];
}
function generatePropertyGet(property: DtoClass['properties'][number], dtoClass: DtoClass): Output {
  return [
    description(property),
    `func (c *${className(dtoClass)}) Get${property.name}() ${typeMapper.mapType(property.type)} {`,
    [`return c.${property.name}`],
    '}',
    '',
    { indent: 0 },
  ];
}

function sanitizeMethod(dtoClass: DtoClass): Output {
  return [
    description({
      description: [
        'Checks for nil values on required properties.',
        'Initializes required nil slices to zero length.',
      ]
    }),
    `func (c *${className(dtoClass)}) Sanitize() error {`,
    ...dtoClass.properties.map(property => {
      let lines: Output = [];

      if (isArrayType(property.type)) {
        lines = [
          ...lines,
          `if c.${property.name} == nil {`,
          [
            `c.${property.name} = make(${typeMapper.mapType(property.type)}, 0)`,
          ],
          '}',
          '',
        ];
      }

      const stripped = stripOptional(property.type);
      if (isReferenceType(stripped)) {
        lines = [
          ...lines,
          `if c.${property.name} != nil {`,
          [
            `if err := c.${property.name}.Sanitize(); err != nil {`,
            ['return err'],
            '}',
          ],
          !isOptionalType(property.type) ? [
            '} else {',
            [
              `return makeError("Required property ${property.name} of ${dtoClass.name} is nil")`,
            ],
            { indent: 0 },
          ] : null,
          '}',
          '',
        ];
      } else if (isArrayType(stripped) && isReferenceType(stripped.of)) {
        const itemType = getItemType(property.type);
        lines = [
          ...lines,
          `for _, item := range c.${property.name} {`,
          [
            'if item != nil {',
            [
              'if err := item.Sanitize(); err != nil {',
              ['return err'],
              '}',
            ],
            !isOptionalType(itemType) ? [
              '} else {',
              [
                `return makeError("Required item in property ${property.name} of ${dtoClass.name} is nil")`,
              ],
              { indent: 0 },
            ] : null,
            '}',
          ],
          '}',
          '',
        ];
      }

      return lines;
    }),
    ['return nil'],
    '}',
    '',
    { indent: 0 },
  ];
}

function cloneMethod(dtoClass: DtoClass): Output {
  return [
    `func (c *${className(dtoClass)}) Clone() (*${className(dtoClass)}, error) {`,
    [
      'bytes, err := c.ToBytes()',
      'if err != nil {',
      ['return nil, err'],
      '}',
      `cloned := &${className(dtoClass)}{}`,
      'if err := cloned.FromBytes(bytes); err != nil {',
      ['return nil, err'],
      '}',
      'return cloned, nil',
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

function serializationMethods(dtoClass: DtoClass): Output {
  return [
    `func (c *${className(dtoClass)}) ToBytes() ([]byte, error) {`,
    [
      'if err := c.Sanitize(); err != nil {',
      ['return nil, err'],
      '}',
      'return bson.Marshal(c)',
    ],
    '}',
    '',
    `func (c *${className(dtoClass)}) FromBytes(b []byte) error {`,
    [
      'if err := bson.Unmarshal(b, c); err != nil {',
      ['return err'],
      '}',
      'return c.Sanitize()',
    ],
    '}',
    '',
    { indent: 0 },
  ];
}

export function generateClass(dtoClass: DtoClass): Output {
  return [
    GENERATED_WARNING,
    'package dto',
    '',
    'import (',
    [
      '"go.mongodb.org/mongo-driver/bson"',
    ],
    ')',
    '',
    description(dtoClass),
    `type ${className(dtoClass)} struct {`,
    dtoClass.properties.map(generateProperty),
    '}',
    '',
    ...dtoClass.properties.map(property => generatePropertyGet(property, dtoClass)),
    serializationMethods(dtoClass),
    cloneMethod(dtoClass),
    sanitizeMethod(dtoClass),
  ];
}
