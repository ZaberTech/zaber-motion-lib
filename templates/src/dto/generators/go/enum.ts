import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description, namespace } from './common';

export function generateEnum(dtoEnum: DtoEnum): Output {
  const typeName = `${namespace(dtoEnum)}${dtoEnum.name}`;
  const withValues = dtoEnum.values.map((value, i) => ({ ...value, value: value.value ?? i }));
  return [
    GENERATED_WARNING,
    'package dto',
    '',
    description(dtoEnum),
    `type ${typeName} int32`,
    '',
    'const (',
    withValues.map(value => `${typeName}_${_.snakeCase(value.name).toUpperCase()} ${typeName} = ${value.value}`),
    ')',
    '',
    `var ${typeName}_name = map[${typeName}]string{`,
    _.unionBy(withValues, 'value').map(value => `${typeName}_${_.snakeCase(value.name).toUpperCase()}: "${value.name}",`),
    '}',
    '',
  ];
}
