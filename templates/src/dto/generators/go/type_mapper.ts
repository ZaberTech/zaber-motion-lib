import { P, match } from 'ts-pattern';

import { AnyType } from '../../types';
import { isMatch } from '../../../utils';

import { namespace as namespaceName } from './common';

const typeMap: Record<string, string> = {
  uint: 'uint32',
  int: 'int32',
  long: 'int64',
  double: 'float64',
  LengthUnits: 'string',
  VelocityUnits: 'string',
  TimeUnits: 'string',
  FrequencyUnits: 'string',
  AccelerationUnits: 'string',
  Units: 'string',
};

function mapType(type: AnyType): string {
  return match(type)
    .with({ type: 'ref' }, type => `*${namespaceName(type)}${type.name}`)
    .with({ type: 'enum' }, type => `${namespaceName(type)}${type.name}`)
    .with({ type: 'array' }, ({ of }) => `[]${mapType(of)}`)
    .with({ type: 'optional' }, ({ of }) => {
      const nullable = ((isMatch(P.string, of) && !of.endsWith('Units')) || isMatch({ type: 'enum' }, of));
      const mapped = mapType(of);
      return nullable ? `*${mapped}` : mapped;
    })
    .with(P.string, type => typeMap[type] || type)
    .exhaustive();
}

export const typeMapper = {
  mapType,
};
