import _ from 'lodash';

import { Description, Namespace, DtoType } from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

export const templateHeaderComment = [
  '// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //',
  '// ============== DO NOT EDIT DIRECTLY ============== //',
  '',
];

export const pragmaOnce = [
  '#pragma once',
  '',
];

export function namespace({ namespace }: { namespace: Namespace }): Output {
  return ['zaber', 'motion', ...ensureArray(namespace).map(n => _.snakeCase(n))].join('::');
}

export function namespaceOpen({ namespace }: { namespace: Namespace }): Output[] {
  return [
    'namespace zaber {',
    'namespace motion {',
    ...ensureArray(namespace).map(namespace => `namespace ${_.snakeCase(namespace)} {`),
    '',
  ];
}

export function namespaceClose({ namespace }: { namespace: Namespace }): Output[] {
  return [
    '',
    ...ensureArray(namespace).reverse().map(namespace => `} // namespace ${_.snakeCase(namespace)}`),
    '} // namespace motion',
    '} // namespace zaber',
    '',
  ];
}

export function visibility({ namespace }: { namespace: Namespace }): string {
  return ensureArray(namespace)[0] === 'requests' ? 'internal' : 'public';
}

export function description({ description }: { description?: Description }): Output {
  if (!description) {
    return null;
  }
  return [
    '/**',
    ...ensureArray(description).map(line => ` * ${line}`),
    ' */',
    { indent: 0 },
  ];
}

export function generateHeaderInclude(dtoType: DtoType, extension: string): string {
  const namespace = ensureArray(dtoType.namespace).map(ns => `${_.snakeCase(ns)}/`).join('');
  return `#include "zaber/motion/dto/${namespace}${_.snakeCase(dtoType.name)}${extension}"`;
}

export function removeSuffix(input: string, suffix: string): string {
  if (input.endsWith(suffix)) {
    return input.substring(0, input.length - suffix.length);
  }
  return input;
}

const CONFLICT_WORDS = new Set<string>([
  'major', 'minor', // <sys/types.h>
  'error', // <windows.h>
].map(w => _.toLower(w)));

export function isConflictWord(word: string): boolean {
  return CONFLICT_WORDS.has(word.toLowerCase());
}

export function undefineMacros(macros: string[]): Output {
  if (!macros.length) { return null }
  return [
    ...macros.flatMap(macro => [
      `#pragma push_macro("${macro}")`,
      `#undef ${macro}`,
    ]),
    '',
    { indent: 0 },
  ];
}

export function redefineMacros(macros: string[]): Output {
  if (!macros.length) { return null }
  return [
    ...macros.map(macro => `#pragma pop_macro("${macro}")`),
    '',
    { indent: 0 },
  ];
}
