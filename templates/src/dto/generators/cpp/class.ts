import _ from 'lodash';
import { P, match } from 'ts-pattern';

import { ensureArray } from '../../../utils';
import {
  DtoClass,
  DtoProperty,
  StartType,
  AnyType,
  getBaseType,
  hasNamespace,
  isUnitsType,
  isOptionalType,
  isArrayType,
  isBasicType,
  isReferenceType,
  stripOptional,
} from '../../types';
import { Output } from '../output';

import {
  description,
  namespaceOpen,
  namespaceClose,
  templateHeaderComment,
  pragmaOnce,
  generateHeaderInclude,
  undefineMacros,
  redefineMacros,
  isConflictWord,
} from './common';
import { typeMapper } from './type_mapper';

interface VariableInfo {
  name: string;
  type: AnyType;
}

const defaultStlHeaders: string[] = ['string'];

const toStringDeclaration: Output = [
  description({ description: 'Convert object to human-readable string format' }),
  'std::string toString() const;'
];

const toByteArrayDeclaration: Output = [
  'std::string toByteArray() const override;',
];

const populateFromByteArrayDeclaration: Output = [
  'void populateFromByteArray(const std::string& buffer) override;',
];

function isArrayTypeCPP(type: AnyType): boolean {
  return isArrayType(type) || (isOptionalType(type) && isArrayType(type.of));
}

function generateMemberName(propertyName: string): string {
  return `${_.camelCase(propertyName)}`;
}

function generateParamName(propertyName: string): string {
  return `p_${_.camelCase(propertyName)}`;
}

function isNonPrimitive(type: AnyType): boolean {
  const actualType = stripOptional(type);
  return !isBasicType(actualType) || actualType === 'string';
}

function generateGetterAndSetterHeader(property: DtoClass['properties'][number]): Output {
  const propertyType = typeMapper.mapType(property.type);
  const paramName = generateParamName(property.name);
  const constRef = isNonPrimitive(property.type) ? 'const& ' : '';
  return [
    description(property),
    `${propertyType} ${constRef}${_.camelCase(`get${property.name}`)}() const;`,
    `void ${_.camelCase(`set${property.name}`)}(${propertyType} ${paramName});`,
    '',
  ];
}

function generateGetterAndSetterSource(property: DtoClass['properties'][number], dtoClass: DtoClass): Output {
  const propertyType = typeMapper.mapType(property.type);
  const memberName = generateMemberName(property.name);
  const paramName = generateParamName(property.name);
  const nonPrimitive = isNonPrimitive(property.type);
  const constRef = nonPrimitive ? 'const& ' : '';
  const moveOpen = nonPrimitive ? 'std::move(' : '';
  const moveClose = nonPrimitive ? ')' : '';
  return [
    `${propertyType} ${constRef}${dtoClass.name}::${_.camelCase(`get${property.name}`)}() const {`,
    `    return ${memberName};`,
    '}',
    `void ${dtoClass.name}::${_.camelCase(`set${property.name}`)}(${propertyType} ${paramName}) {`,
    `    ${memberName} = ${moveOpen}${paramName}${moveClose};`,
    '}',
    '',
  ];
}

function generateToStringSource(dtoClass: DtoClass): Output[] {
  const propertyToString = (property: DtoClass['properties'][number], addComma: boolean): Output[] => {
    const memberName = generateMemberName(property.name);
    const ret = [
      `ss << "${memberName}: ";`,
      ...variableToString({ name: `this->${memberName}`, type: property.type }),
    ];
    if (addComma) {
      ret.push('ss << ", ";');
    }
    return ret;
  };
  const numProperties = dtoClass.properties.length;
  const propertyStrings: Output[] = dtoClass.properties.flatMap((property, index) =>
    propertyToString(property, index < numProperties - 1));
  return [
    `std::string ${dtoClass.name}::toString() const {`,
    [
      'std::stringstream ss;',
      `ss << "${dtoClass.name} { ";`,
      ...propertyStrings,
      'ss << " }";',
      'return ss.str();',
    ],
    '}',
    '',
  ];
}

function variableToString(v: VariableInfo): Output[] {
  return match(v.type)
    .with({ type: 'ref' }, () => [`ss << ${v.name}.toString();`])
    .with({ type: 'enum' }, ({ name }) => [`ss << ${name}_toString(${v.name});`])
    .with({ type: 'array' }, ({ of }) => [
      'ss << "[ ";',
      `for (size_t i = 0; i < ${v.name}.size(); i++) {`,
      [...variableToString({ name: `${v.name}[i]`, type: of })],
      [
        `if (i < ${v.name}.size() - 1) {`,
        ['ss << ", ";'],
        '}',
      ],
      '}',
      'ss << " ]";',
    ])
    .with({ type: 'optional' }, ({ of }) => {
      if (isArrayTypeCPP(of)) {
        return variableToString({ name: v.name, type: of });
      } else {
        return [
          `if (${v.name}.has_value()) {`,
          [...variableToString({ name: `${v.name}.value()`, type: of })],
          '} else {',
          ['ss << "null";'],
          '}',
        ];
      }
    })
    .with(P.string, () => {
      if (isUnitsType(v.type)) { return [`ss << getUnitLongName(${v.name});`] } else { return [`ss << ${v.name};`] }
    })
    .exhaustive();
}

const generateConstructorParam = (property: DtoClass['properties'][number], addComma: boolean): string => {
  const paramType = typeMapper.mapType(property.type);
  const comma = addComma ? ',' : '';
  return `${paramType} ${generateParamName(property.name)}${comma}`;
};

function generateConstructorsHeader(dtoClass: DtoClass): Output[] {
  const numProperties = dtoClass.properties.length;
  if (numProperties < 1) {
    return [];
  }

  const obligatoryProperties = dtoClass.properties.filter((property: DtoProperty) =>
    !isOptionalType(property.type));
  let constructorWithoutOptional: Output = [];
  if (obligatoryProperties.length > 0 && obligatoryProperties.length != numProperties) {
    constructorWithoutOptional = [
      `${dtoClass.name}(`,
      obligatoryProperties.map((property, index) =>
        generateConstructorParam(property, index < obligatoryProperties.length - 1)),
      ');',
      '',
    ];
  }

  return [
    `${dtoClass.name}();`,
    '',
    `${dtoClass.name}(`,
    dtoClass.properties.map((property, index) =>
      generateConstructorParam(property, index < numProperties - 1)),
    ');',
    '',
    ...constructorWithoutOptional
  ];
}

function generateConstructorsSource(dtoClass: DtoClass): Output[] {
  const generateConstructorSetter = (property: DtoClass['properties'][number], addComma: boolean): string => {
    const comma = addComma ? ',' : '';
    const moveOpen = isNonPrimitive(property.type) ? 'std::move(' : '';
    const moveClose = isNonPrimitive(property.type) ? ')' : '';
    return `${generateMemberName(property.name)}(${moveOpen}${generateParamName(property.name)}${moveClose})${comma}`;
  };
  const numProperties = dtoClass.properties.length;
  if (numProperties < 1) {
    return [];
  }

  const obligatoryProperties = dtoClass.properties.filter((property: DtoProperty) =>
    !isOptionalType(property.type));
  let constructorWithoutOptional: Output = [];
  if (obligatoryProperties.length > 0 && obligatoryProperties.length != numProperties) {
    constructorWithoutOptional = [
      `${dtoClass.name}::${dtoClass.name}(`,
      obligatoryProperties.map((property, index) =>
        generateConstructorParam(property, index < obligatoryProperties.length - 1)),
      ') : ',
      obligatoryProperties.map((property, index) =>
        generateConstructorSetter(property, index < obligatoryProperties.length - 1)),
      '{ }',
      '',
    ];
  }

  return [
    `${dtoClass.name}::${dtoClass.name}() { }`,
    '',
    `${dtoClass.name}::${dtoClass.name}(`,
    dtoClass.properties.map((property, index) => generateConstructorParam(property, index < numProperties - 1)),
    ') : ',
    dtoClass.properties.map((property, index) => generateConstructorSetter(property, index < numProperties - 1)),
    '{ }',
    '',
    ...constructorWithoutOptional,
  ];
}

function generateEqualityHeaders(dtoClass: DtoClass): Output[] {
  const numProperties = dtoClass.properties.length;
  if (numProperties < 1) {
    return [];
  }

  return [
    `bool operator==(const ${dtoClass.name}& other) const;`,
    '',
    `bool operator!=(const ${dtoClass.name}& other) const {`,
    '    return !(*this == other);',
    '}',
  ];
}

function generateEqualitySource(dtoClass: DtoClass): Output[] {
  const numProperties = dtoClass.properties.length;
  if (numProperties < 1) {
    return [];
  }

  return [
    `bool ${dtoClass.name}::operator==(const ${dtoClass.name}& other) const {`,
    '    return std::tie(',
    dtoClass.properties.map((p, i) => `${generateMemberName(p.name)}${i < numProperties - 1 ? ',' : ''}`),
    '    ) == std::tie(',
    dtoClass.properties.map((p, i) => `other.${generateMemberName(p.name)}${i < numProperties - 1 ? ',' : ''}`),
    '    );',
    '}',
    '',
  ];
}

function generateJsonDefines(dtoClass: DtoClass): Output[] {
  if (dtoClass.properties.length < 1) {
    return [
      `void to_json(nlohmann::json& j, const ${dtoClass.name}&) {`,
      '    j = nlohmann::json::object();',
      '}',
      '',
      `void from_json(const nlohmann::json&, const ${dtoClass.name}&) {`,
      '}',
    ];
  }
  return [
    [
      `NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(${dtoClass.name}`,
      `${dtoClass.properties.map(property => generateMemberName(property.name)).join(', ')})`
    ].join(', ')
  ];
}

function generateFromBytesSource(dtoClass: DtoClass): Output[] {
  return [
    `void ${dtoClass.name}::populateFromByteArray(const std::string& buffer) {`,
    [
      'nlohmann::json obj = nlohmann::json::from_bson(buffer);',
      `*this = obj.template get<${dtoClass.name}>();`,
    ],
    '}',
    '',
  ];
}

function generateToBytesSource(dtoClass: DtoClass): Output[] {
  return [
    `std::string ${dtoClass.name}::toByteArray() const {`,
    [
      'nlohmann::json obj = *this;',
      'std::string buffer;',
      'nlohmann::json::to_bson(obj, buffer);',
      'return buffer;',
    ],
    '}',
  ];
}

function generateMemberField(property: DtoClass['properties'][number]): Output {
  const propertyType = typeMapper.mapType(property.type);
  const memberName = generateMemberName(property.name);
  const valueInitializer = typeMapper.mapValueInitializer(property.type);
  return `${propertyType} ${memberName}${valueInitializer};`;
}

function includeInternalType(type: StartType, extension: string) {
  if (hasNamespace(type)) {
    const typeNamespace = ensureArray(type.namespace).map(ns => `${_.snakeCase(ns)}/`).join('');
    return `#include "zaber/motion/dto/${typeNamespace}${_.snakeCase(type.name)}${extension}"`;
  } else if (isUnitsType(type)) {
    return `#include "zaber/motion/units${extension}"`;
  } else {
    throw new Error(`invalid include type: ${type}`);
  }
}

function conflictMacros(dtoClass: DtoClass) {
  return dtoClass.properties.filter(prop => isConflictWord(prop.name)).map(prop => prop.name.toLowerCase());
}

function undefineMacrosClass(dtoClass: DtoClass): Output {
  return undefineMacros(conflictMacros(dtoClass));
}

function redefineMacrosClass(dtoClass: DtoClass): Output {
  return redefineMacros(conflictMacros(dtoClass));
}

function generateIncludesHeader(dtoClass: DtoClass): Output[] {
  let stlHeaders: string[] = dtoClass.properties.flatMap(property => typeMapper.mapStlHeader(property.type));
  stlHeaders = stlHeaders.concat(defaultStlHeaders);
  const stlIncludes = new Set<string>();
  for (const stlHeader of stlHeaders) {
    stlIncludes.add(`#include <${stlHeader}>`);
  }

  const basePropertyTypes = dtoClass.properties.map(property => getBaseType(property.type));
  const internalIncludes = new Set<string>();
  const internalTypes = basePropertyTypes.filter(e => hasNamespace(e) || isUnitsType(e));
  for (const internalType of internalTypes) {
    internalIncludes.add(includeInternalType(internalType, '.h'));
  }

  return [
    ...stlIncludes,
    '',
    '#include "zaber/motion/dto/serializable.h"',
    ...internalIncludes,
  ];
}

function generateIncludesPrivateHeader(dtoClass: DtoClass): Output[] {
  const classNamespace = ensureArray(dtoClass.namespace).map(ns => `${_.snakeCase(ns)}/`).join('');
  const customTypes = dtoClass.properties.map(property => getBaseType(property.type))
    .filter(e => isReferenceType(e) || isUnitsType(e));

  const privateIncludes = new Set<string>();
  for (const customType of customTypes) {
    privateIncludes.add(includeInternalType(customType, '.private.h'));
  }
  return [
    `#include "zaber/motion/dto/${classNamespace}${_.snakeCase(dtoClass.name)}.h"`,
    '',
    '#include "zaber/motion/utils/serialization_utils.private.h"',
    ...privateIncludes,
    '',
  ];
}

function generateIncludesSource(dtoClass: DtoClass): Output[] {
  return [
    generateHeaderInclude(dtoClass, '.private.h'),
    '',
    '#include <sstream>',
    '',
  ];
}

export function generateClassHeader(dtoClass: DtoClass): Output {
  typeMapper.currentNamespace = dtoClass.namespace;

  return [
    ...templateHeaderComment,
    ...pragmaOnce,
    undefineMacrosClass(dtoClass),
    ...generateIncludesHeader(dtoClass),
    '',
    ...namespaceOpen(dtoClass),
    description(dtoClass),
    `class ${dtoClass.name}: public Serializable {`,
    'public:',
    dtoClass.properties.map(generateMemberField),
    '',
    generateConstructorsHeader(dtoClass),
    generateEqualityHeaders(dtoClass),
    '',
    ...dtoClass.properties.map(generateGetterAndSetterHeader),
    toStringDeclaration,
    '',
    '#ifdef ZML_SERIALIZATION_PUBLIC',
    'public:',
    '#else',
    'private:',
    '#endif',
    '',
    toByteArrayDeclaration,
    populateFromByteArrayDeclaration,
    '',
    '};',
    ...namespaceClose(dtoClass),
    redefineMacrosClass(dtoClass),
  ];
}

export function generateClassPrivateHeader(dtoClass: DtoClass): Output {
  typeMapper.currentNamespace = dtoClass.namespace;

  return [
    ...templateHeaderComment,
    ...pragmaOnce,
    ...generateIncludesPrivateHeader(dtoClass),
    undefineMacrosClass(dtoClass),
    ...namespaceOpen(dtoClass),
    ...generateJsonDefines(dtoClass),
    ...namespaceClose(dtoClass),
    redefineMacrosClass(dtoClass),
  ];
}

export function generateClassSource(dtoClass: DtoClass): Output {
  typeMapper.currentNamespace = dtoClass.namespace;

  return [
    ...templateHeaderComment,
    ...generateIncludesSource(dtoClass),
    undefineMacrosClass(dtoClass),
    ...namespaceOpen(dtoClass),
    ...generateConstructorsSource(dtoClass),
    ...generateEqualitySource(dtoClass),
    ...dtoClass.properties.flatMap(property => generateGetterAndSetterSource(property, dtoClass)),
    ...generateToStringSource(dtoClass),
    ...generateFromBytesSource(dtoClass),
    ...generateToBytesSource(dtoClass),
    ...namespaceClose(dtoClass),
    redefineMacrosClass(dtoClass),
  ];
}
