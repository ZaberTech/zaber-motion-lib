import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { typeMapper } from './type_mapper';
import {
  description,
  namespaceOpen,
  namespaceClose,
  templateHeaderComment,
  pragmaOnce,
  generateHeaderInclude,
  isConflictWord,
  undefineMacros,
  redefineMacros,
} from './common';

function screamingSnakeCase(str : string): string {
  return _.toUpper(_.snakeCase(str));
}

function generateToStringHeader(dtoEnum : DtoEnum): Output[] {
  return [
    `std::string ${dtoEnum.name}_toString(${dtoEnum.name} value);`,
  ];
}

function generateToStringSource(dtoEnum : DtoEnum): Output[] {
  const enumSet = new Set<number>();
  const uniqueValues = dtoEnum.values.filter(
    value => {
      if ('value' in value && typeof value.value === 'number') {
        if (enumSet.has(value.value)) {
          return false;
        }
        enumSet.add(value.value);
      }
      return true;
    }
  );
  const switchCases: string[] = uniqueValues.map(
    value => `case ${dtoEnum.name}::${screamingSnakeCase(value.name)}: return "${screamingSnakeCase(value.name)}";`);

  return [
    `std::string ${dtoEnum.name}_toString(${dtoEnum.name} value) {`,
    [
      'switch (value) {',
      switchCases,
      '}',
      'return "<Invalid value>";',
    ],
    '}',
  ];
}

function conflictMacros(dtoEnum: DtoEnum) {
  return dtoEnum.values.filter(value => isConflictWord(value.name)).map(value => value.name.toUpperCase());
}

function undefineMacrosEnum(dtoEnum: DtoEnum): Output {
  return undefineMacros(conflictMacros(dtoEnum));
}

function redefineMacrosEnum(dtoEnum: DtoEnum): Output {
  return redefineMacros(conflictMacros(dtoEnum));
}

export function generateEnumHeader(dtoEnum: DtoEnum): Output {
  typeMapper.currentNamespace = dtoEnum.namespace;
  return [
    ...templateHeaderComment,
    ...pragmaOnce,
    undefineMacrosEnum(dtoEnum),
    '#include <string>',
    '',
    ...namespaceOpen(dtoEnum),
    description(dtoEnum),
    `enum class ${dtoEnum.name} {`,
    dtoEnum.values.flatMap((value, i) => ([`${screamingSnakeCase(value.name)} = ${value.value ?? i},`])),
    '};',
    '',
    description({ description: `Returns string representation of ${dtoEnum.name} value` }),
    ...generateToStringHeader(dtoEnum),
    '',
    ...namespaceClose(dtoEnum),
    redefineMacrosEnum(dtoEnum),
  ];
}

export function generateEnumSource(dtoEnum: DtoEnum): Output {
  typeMapper.currentNamespace = dtoEnum.namespace;
  return [
    ...templateHeaderComment,
    generateHeaderInclude(dtoEnum, '.h'),
    '',
    undefineMacrosEnum(dtoEnum),
    ...namespaceOpen(dtoEnum),
    ...generateToStringSource(dtoEnum),
    ...namespaceClose(dtoEnum),
    redefineMacrosEnum(dtoEnum),
  ];
}
