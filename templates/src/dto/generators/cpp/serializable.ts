import { DtoType } from '../../types';
import { Output } from '../output';

import {
  namespaceOpen,
  namespaceClose,
  templateHeaderComment,
  pragmaOnce,
} from './common';

export const serializableDto: DtoType = {
  name: 'Serializable',
  namespace: [],
  properties: [],
};

const defaultFriends: string[] = [
  'AsyncContext',
  'Serializer',
  'SdkEvent',
];

function addDeclaration(name: string, type: string, prefix: string = ''): string {
  return `${type} ${prefix}${name};`;
}

export function generateSerializableHeader(errors: { name: string }[]): Output {
  const exceptions = errors.map(error => `${error.name}Exception`);

  return [
    ...templateHeaderComment,
    ...pragmaOnce,
    ...namespaceOpen({ namespace: serializableDto.namespace }),
    ...defaultFriends.map(friend => addDeclaration(friend, 'class')),
    '',
    'namespace exceptions {',
    ...exceptions.map(exception => addDeclaration(exception, 'class')),
    '} // namespace exceptions',
    '',
    `class ${serializableDto.name} {`,
    'public:',
    [
      `virtual ~${serializableDto.name}() { }`,
    ],
    '',
    'protected:',
    defaultFriends.map(friend => addDeclaration(friend, 'friend')),
    exceptions.map(exception => addDeclaration(exception, 'friend', 'exceptions::')),
    '',
    [
      'virtual void populateFromByteArray(const std::string& buffer) = 0;',
      'virtual std::string toByteArray() const = 0;',
    ],
    '',
    '};',
    ...namespaceClose({ namespace: serializableDto.namespace }),
  ];
}
