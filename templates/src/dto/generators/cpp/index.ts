import { hasNamespace } from '../../types';
import { TypeMapper } from '../types';
import { typeMapper } from './type_mapper';

export { cppGenerator } from './generator';

export const cppTypeMapper = {
  mapType: type => {
    if (hasNamespace(type)) {
      typeMapper.currentNamespace = type.namespace;
    }
    return typeMapper.mapType(type);
  },
} satisfies TypeMapper;
