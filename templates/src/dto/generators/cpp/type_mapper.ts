import { P, match } from 'ts-pattern';
import { ensureArray } from '@zaber/toolbox';

import { AnyType, EnumType, isArrayType, Namespace, namespaceEquals, ReferenceType } from '../../types';

const typeMap: Record<string, string> = {
  uint: 'unsigned int',
  string: 'std::string',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

class TypeMapper {
  public currentNamespace: Namespace = [];

  public mapType(type: AnyType): string {
    return match(type)
      .with({ type: 'ref' }, dtoType => this.checkAddNamespace(dtoType))
      .with({ type: 'enum' }, dtoType => this.checkAddNamespace(dtoType))
      .with({ type: 'array' }, ({ of }) => `std::vector<${this.mapType(of)}>`)
      .with({ type: 'optional' }, ({ of }) => {
        if (isArrayType(of)) {
          return this.mapType(of);
        } else {
          return `std::optional<${this.mapType(of)}>`;
        }
      })
      .with(P.string, type => typeMap[type] || type)
      .exhaustive();
  }

  public mapValueInitializer(type: AnyType): string {
    return match(type)
      .with({ type: 'ref' }, () => '')
      .with({ type: 'enum' }, () => ' {0}')
      .with({ type: 'array' }, () => '')
      .with({ type: 'optional' }, () => '')
      .with(P.string, () =>
        match(type)
          .with('uint', () => ' {0}')
          .with('int', () => ' {0}')
          .with('double', () => ' {0.0}')
          .with('bool', () => ' {false}')
          .with('Units', () => ' {Units::NATIVE}')
          .with('string', () => '')
          .otherwise(() => ''))
      .exhaustive();
  }

  public useConstReference(type: AnyType): boolean {
    return match(type)
      .with({ type: 'ref' }, () => true)
      .with({ type: 'enum' }, () => false)
      .with({ type: 'array' }, () => true)
      .with({ type: 'optional' }, () => true)
      .with(P.string, () => type === 'string')
      .exhaustive();
  }

  public mapStlHeader(type: AnyType): string[] {
    return match(type)
      .with({ type: 'array' }, ({ of }) => ['vector'].concat(this.mapStlHeader(of)))
      .with({ type: 'optional' }, ({ of }) => ['optional'].concat(this.mapStlHeader(of)))
      .with(P.string, type => type === 'string' ? ['string'] : [])
      .with(P._, () => [])
      .exhaustive();
  }

  private checkAddNamespace(dtoType: ReferenceType | EnumType): string {
    if (!namespaceEquals(dtoType.namespace, this.currentNamespace)) {
      return [...ensureArray(dtoType.namespace), dtoType.name].join('::');
    } else {
      return dtoType.name;
    }
  }
}

export const typeMapper = new TypeMapper();
