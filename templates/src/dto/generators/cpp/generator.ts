import _ from 'lodash';

import { namespaceEquals, type Definitions, type DtoClass, type DtoEnum, type DtoType, type Namespace } from '../../types';
import { ensureArray } from '../../../utils';
import { outputToString, type FileOutput } from '../output';
import { LanguageGenerator } from '../types';
import { errorsEnum } from '../../definitions/requests/errors_enum';

import { templateHeaderComment, pragmaOnce } from './common';
import { generateClassHeader, generateClassPrivateHeader, generateClassSource } from './class';
import { generateEnumHeader, generateEnumSource } from './enum';
import { generateSerializableHeader, serializableDto } from './serializable';

const BASE_INDENT = 4;

function namespaceToPath({ namespace }: { namespace: Namespace }): string {
  return ensureArray(namespace).map(ns => `${_.snakeCase(ns)}/`).join('');
}

function filename(dtoObject: DtoType, extension: string): string {
  return `${namespaceToPath(dtoObject)}${_.snakeCase(dtoObject.name)}${extension}`;
}

function generateIncludeFileForNamespace(dtoTypes: DtoType[], namespace: Namespace): FileOutput {
  const filtered = dtoTypes.filter(dtoType => namespaceEquals(dtoType.namespace, namespace));
  const includes = filtered.map(dtoType =>
    `#include "zaber/motion/dto/${namespaceToPath(dtoType)}${_.snakeCase(dtoType.name)}.h"`);
  includes.sort();

  return {
    path: `/${namespaceToPath({ namespace })}includes.h`,
    content: outputToString([
      templateHeaderComment,
      pragmaOnce,
      ...includes,
      '',
    ])
  };
}

export const cppGenerator: LanguageGenerator = {
  generateClass(dtoClass: DtoClass): FileOutput[] {
    const headerContent = generateClassHeader(dtoClass);
    const privateHeaderContent = generateClassPrivateHeader(dtoClass);
    const sourceContent = generateClassSource(dtoClass);

    return [
      {
        path: filename(dtoClass, '.h'),
        content: outputToString(headerContent, BASE_INDENT),
      },
      {
        path: filename(dtoClass, '.private.h'),
        content: outputToString(privateHeaderContent, BASE_INDENT),
      },
      {
        path: filename(dtoClass, '.cpp'),
        content: outputToString(sourceContent, BASE_INDENT)
      }
    ];
  },
  generateEnum(dtoEnum: DtoEnum): FileOutput[] {
    const headerContent = generateEnumHeader(dtoEnum);
    const sourceContent = generateEnumSource(dtoEnum);
    return [
      {
        path: filename(dtoEnum, '.h'),
        content: outputToString(headerContent, BASE_INDENT),
      },
      {
        path: filename(dtoEnum, '.cpp'),
        content: outputToString(sourceContent, BASE_INDENT),
      }
    ];
  },
  generateIndexFiles(definitions: Definitions): FileOutput[] {
    const dtoTypes = [
      ...definitions.classes,
      ...definitions.enums,
      serializableDto,
    ];
    const namespaces = _.uniqWith(dtoTypes.map(dtoType => dtoType.namespace), namespaceEquals);
    const indexFiles = namespaces.map(namespace => generateIncludeFileForNamespace(dtoTypes, namespace));
    const serializableFile = generateSerializableHeader(errorsEnum.values);
    return [
      ...indexFiles,
      {
        path: filename(serializableDto, '.h'),
        content: outputToString(serializableFile, BASE_INDENT),
      },
    ];
  }
};
