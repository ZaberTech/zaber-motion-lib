import { AnyType, Definitions, DtoClass, DtoEnum } from '../types';

import { FileOutput } from './output';

export interface LanguageGenerator {
  generateClass(dtoClass: DtoClass): FileOutput[];
  generateEnum(dtoEnum: DtoEnum): FileOutput[];
  generateIndexFiles?(definitions: Definitions): FileOutput[];
  formatter?(file: string): Promise<void>;
}

export interface TypeMapper {
  mapType(type: AnyType): string;
}

export type Languages = 'cs' | 'go' | 'ts' | 'py' | 'java' | 'cpp' | 'swift';
