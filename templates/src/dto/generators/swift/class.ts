import _ from 'lodash';

import { DtoClass, DtoProperty, getBaseType, hasNamespace, isUnitsType, namespaceEquals } from '../../types';
import { Output, outputToString } from '../output';

import { description, GENERATED_WARNING, namespace } from './common';
import { typeMapper } from './type_mapper';


export const generateClass = (dtoClass: DtoClass): Output => [
  GENERATED_WARNING,
  '',
  'import Foundation',
  'import SwiftBSON',
  'import DtoSerializable',
  imports(dtoClass),
  '',
  description(dtoClass),
  `public struct ${dtoClass.name}: Serializable {`,
  [
    ...dtoClass.properties.map(property => memberField(property, dtoClass)),
    '',
    initializer(dtoClass.properties, dtoClass),
    '',
    serializationMethods(dtoClass),
  ],
  '}',
  '',
];

const imports = (dtoClass: DtoClass): Output => {
  const modules = dtoClass.properties.map(property => propertyModule(property, dtoClass));
  const usings = new Set<string>();
  for (const module of modules) {
    if (module) {
      usings.add(`import ${outputToString(module)[0]}`);
    }
  }
  if (usings.size === 0) {
    return null;
  }
  return [
    ...usings,
    '',
    { indent: 0 },
  ];
};

const memberField = (property: DtoProperty, dtoClass: DtoClass): Output => {
  const descriptionText = description(property);
  const nameAndType = memberNameAndType(property, dtoClass);

  return [
    descriptionText && '',
    descriptionText,
    `public var ${nameAndType}`,
    { indent: 0 },
  ];
};

const initializer = (properties: DtoProperty[], dtoClass: DtoClass): Output => {
  const args = properties.map(property =>
    `${memberNameAndType(property, dtoClass)} = ${typeMapper.mapDefault(property.type)}`);

  return [
    `public init(${args.join(', ')}) {`,
    [
      ...dtoClass.properties.map(property => {
        const name = _.lowerFirst(property.name);
        return `self.${name} = ${name}`;
      }),
    ],
    '}',
    { indent: 0 },
  ];
};

const memberNameAndType = (property: DtoProperty, dtoClass: DtoClass): string => {
  const name = _.lowerFirst(property.name);
  const module = propertyModule(property, dtoClass);
  const propertyType = typeMapper.mapType(property.type, module?.toString());
  return `${name}: ${propertyType}`;
};

const serializationMethods = (dtoClass: DtoClass): Output => [
  `public static func fromByteArray(_ byteArray: Data) throws -> ${dtoClass.name} {`,
  [
    'do {',
    [
      'let bson = try BSONDocument(fromBSON: byteArray)',
      `return try BSONDecoder().decode(${dtoClass.name}.self, from: bson)`
    ],
    '} catch {',
    [
      `throw SerializationError.deserializationFailed(object: "${dtoClass.name}", error: error)`
    ],
    '}',
  ],
  '}',
  '',
  'public func toByteArray() throws -> Data {',
  [
    'do {',
    [
      'let bson = try BSONEncoder().encode(self)',
      'return bson.toData()'
    ],
    '} catch {',
    [
      `throw SerializationError.serializationFailed(object: "${dtoClass.name}", error: error)`
    ],
    '}',
  ],
  '}',
  { indent: 0 },
];

const propertyModule = (property: DtoProperty, dtoClass: DtoClass): Output => {
  const baseType = getBaseType(property.type);

  if (hasNamespace(baseType)) {
    if (namespaceEquals(baseType.namespace, dtoClass.namespace)) {
      return null;
    }
    return namespace(baseType);
  } else if (isUnitsType(baseType)) {
    return 'UnitsInternal';
  } else {
    return null;
  }
};
