import { ensureArray } from '@zaber/toolbox';
import _ from 'lodash';

import { Description, Namespace } from '../../types';
import { Output } from '../output';


export const GENERATED_WARNING = '/* This file is generated. Do not modify by hand. */';

export function namespace({ namespace }: { namespace: Namespace }): Output {
  const parts = ensureArray(namespace).map(n => _.upperFirst(n));
  if (parts.length === 0) {
    return 'Dto';
  } else {
    return `Dto${parts.join('.')}`;
  }
}

export const description = ({ description }: { description?: Description }): Output => {
  if (!description) {
    return null;
  }
  return [
    '/**',
    ...ensureArray(description).map(line => ` * ${line}`),
    ' */',
    { indent: 0 },
  ];
};
