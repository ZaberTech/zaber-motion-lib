import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { description, GENERATED_WARNING } from './common';


export const generateEnum = (dtoEnum: DtoEnum): Output => {
  // Keep track of enum variants to check for same value and handle them differently.
  // Swift requires unique enum variant values.
  const variants: Record<number, string> = {};

  return [
    GENERATED_WARNING,
    '',
    description(dtoEnum),
    `public enum ${dtoEnum.name}: Int, Codable, Sendable {`,
    dtoEnum.values.map((value, i) => {
      if (value.value == null) {
        return `case \`${_.lowerFirst(value.name)}\` = ${value.value ?? i}`;
      } else if (variants[value.value]) {
        const firstVariantName = variants[value.value];
        return `public static var ${_.camelCase(value.name)}: ${dtoEnum.name} { .${firstVariantName} } // = ${value.value}`;
      } else {
        variants[value.value] = _.lowerFirst(value.name);
        return `case \`${_.lowerFirst(value.name)}\` = ${value.value ?? i}`;
      }
    }),
    '}',
    '',
  ];
};
