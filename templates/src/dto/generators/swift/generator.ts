import { ensureArray } from '@zaber/toolbox';
import _ from 'lodash';

import { Definitions, DtoClass, DtoEnum, Namespace, namespaceEquals } from '../../types';
import { FileOutput, outputToString } from '../output';
import { LanguageGenerator } from '../types';

import { generateClass } from './class';
import { generateEnum } from './enum';
import { GENERATED_WARNING } from './common';


const BASE_INDENT = 4;

const namespaceToPath = (namespace: Namespace): string => ensureArray(namespace).map(n => _.capitalize(n)).join('/');

const fileName = (dto: {name: string; namespace: Namespace}): string => {
  const namespacePath = namespaceToPath(dto.namespace);
  let path = `${dto.name}.swift`;
  if (namespacePath) {
    path = `${namespacePath}/${path}`;
  }
  return path;
};

export const swiftGenerator: LanguageGenerator = {
  generateClass(dtoClass: DtoClass): FileOutput[] {
    const content = generateClass(dtoClass);

    return [{
      path: fileName(dtoClass),
      content: outputToString(content, BASE_INDENT),
    }];
  },
  generateEnum(dtoEnum: DtoEnum): FileOutput[] {
    const content = generateEnum(dtoEnum);

    return [{
      path: fileName(dtoEnum),
      content: outputToString(content, BASE_INDENT),
    }];
  },
  generateIndexFiles(definitions: Definitions): FileOutput[] {
    const dtoTypes = [
      ...definitions.classes,
      ...definitions.enums
    ];
    const modules = _.uniqWith(dtoTypes.map(dtoType => dtoType.namespace), namespaceEquals)
      .filter(ns => ns !== 'requests');

    return modules.map(module => {
      const moduleName = _.capitalize(outputToString(module)[0]);
      const dtoModuleName = `Dto${moduleName}`;

      const imports = [`import ${dtoModuleName}`];
      const typeAliases = dtoTypes.filter(dtoType => namespaceEquals(dtoType.namespace, module))
        .map(dtoType => `public typealias ${dtoType.name} = ${dtoModuleName}.${dtoType.name}`);

      if (module.length === 0) {
        imports.push('import UnitsInternal');
        typeAliases.push('public typealias Units = UnitsInternal.Units');
      }

      return {
        path: `../${moduleName}/Exports.swift`,
        content: [
          GENERATED_WARNING,
          '',
          ...imports,
          '',
          ...typeAliases,
          ''
        ],
      };
    });
  }
};
