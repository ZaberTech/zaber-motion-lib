import { match, P } from 'ts-pattern';

import { AnyType } from '../../types';


const typeMap: Record<string, string> = {
  bool: 'Bool',
  int: 'Int',
  uint: 'UInt',
  double: 'Double',
  string: 'String',
  LengthUnits: 'Units',
  VelocityUnits: 'Units',
  TimeUnits: 'Units',
  FrequencyUnits: 'Units',
  AccelerationUnits: 'Units',
};

const mapType = (type: AnyType, module?: string): string => match(type)
  .with({ type: 'ref' }, ({ name }) => `${module ? `${module}.` : ''}${name}`)
  .with({ type: 'enum' }, ({ name }) => `${module ? `${module}.` : ''}${name}`)
  .with({ type: 'array' }, ({ of }) => `[${mapType(of, module)}]`)
  .with({ type: 'optional' }, ({ of }) => `${mapType(of, module)}?`)
  .with(P.string, type => typeMap[type] || type)
  .exhaustive();

const mapDefault = (type: AnyType): string => match(type)
  .with({ type: 'ref' }, ({ name }) => `${name}()`)
  .with({ type: 'enum' }, ({ name }) => `${name}(rawValue: 0)!`)
  .with({ type: 'array' }, () => '[]')
  .with({ type: 'optional' }, () => 'nil')
  .with(P.string, type => match(type)
    .with('bool', () => 'false')
    .with('int', () => '0')
    .with('uint', () => '0')
    .with('double', () => '0.0')
    .with('string', () => '""')
    .with('Units', () => 'Units.native')
    .otherwise(() => ''))
  .exhaustive();

export const typeMapper = {
  mapType,
  mapDefault,
};
