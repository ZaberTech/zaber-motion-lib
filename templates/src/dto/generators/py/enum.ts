import _ from 'lodash';

import { DtoEnum } from '../../types';
import { Output } from '../output';

import { GENERATED_WARNING, description } from './common';

export function generateEnum(dtoEnum: DtoEnum): Output {
  return  [
    GENERATED_WARNING,
    'from enum import Enum',
    '',
    '',
    `class ${dtoEnum.name}(Enum):`,
    [
      description(dtoEnum),
      '',
      ...dtoEnum.values.map((value, i) =>
        `${_.snakeCase(value.name).toUpperCase()} = ${value.value ?? i}`)
    ],
    '',
  ];
}
