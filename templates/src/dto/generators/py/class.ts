import _ from 'lodash';
import { P, match } from 'ts-pattern';

import {
  AnyType, DtoClass, DtoProperty, getBaseType, getItemType, hasNamespace, isArrayType, isEnumType, isNumericType,
  isOptionalType, isReferenceType, isUnitsType, stripOptional, typeChain,
} from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

import { GENERATED_WARNING, description } from './common';
import { typeMapper } from './type_mapper';

function propName(property: DtoProperty): string {
  return _.snakeCase(property.name).toLowerCase();
}

function isRequestNamespace(parent: DtoClass): boolean {
  return ensureArray(parent.namespace).join() === 'requests';
}

function propertyDefault(property: DtoProperty, parent: DtoClass): string {
  let defaultValue: string | undefined;
  if (isRequestNamespace(parent)) {
    // requests have default values for all properties to allow request mixing
    defaultValue = typeMapper.mapDefaultField(property.type);
  } else if (isOptionalType(property.type)) {
    defaultValue = 'None';
  }
  return defaultValue ? ` = ${defaultValue}` : '';
}

function generateProperty(property: DtoProperty, parent: DtoClass): Output {
  const name = propName(property);
  return [
    `${name}: ${typeMapper.mapType(property.type)}${propertyDefault(property, parent)}`,
    description(property),
    '',
    { indent: 0 },
  ];
}

function generateImports(dtoClass: DtoClass): Output {
  const propertyTypes = dtoClass.properties.map(property => property.type);
  const allTypeChains = propertyTypes.flatMap(typeChain);
  const basePropertyTypes = propertyTypes.map(getBaseType);
  const withNamespace = basePropertyTypes.filter(hasNamespace);
  if (basePropertyTypes.some(isUnitsType)) {
    withNamespace.push({ name: 'Units', namespace: [], type: 'enum' });
  }

  const usings = new Set<string>();
  for (const type of withNamespace) {
    const mySpace = ensureArray(dtoClass.namespace).slice();
    const typeSpace = ensureArray(type.namespace).slice();
    while (mySpace.length && typeSpace.length && mySpace[0] === typeSpace[0]) {
      mySpace.shift();
      typeSpace.shift();
    }

    const depth = mySpace.length;
    let dotDot = _.repeat('.', depth + 1);

    let typePath = typeSpace.join('.');
    if (typePath) {
      typePath = `${typePath}.`;
    }

    let name = type.name;
    if (name === 'Units') {
      name = 'Units, UnitsAndLiterals, units_from_literals';
      dotDot += '.';
    }

    usings.add(`from ${dotDot}${typePath}${_.snakeCase(type.name)} import ${name}`);
  }

  const typingImports = ['Any', 'Dict'];
  if (allTypeChains.some(isArrayType)) {
    typingImports.push('List');
  }
  if (allTypeChains.some(isOptionalType)) {
    typingImports.push('Optional');
  }

  const dataclassImports = ['dataclass'];
  if (isRequestNamespace(dtoClass)) {
    if (propertyTypes.some(type => isReferenceType(type) || isArrayType(type))) {
      dataclassImports.push('field');
    }
  }

  return [
    `from dataclasses import ${dataclassImports.join(', ')}`,
    `from typing import ${typingImports.join(', ')}`,
    allTypeChains.some(isNumericType) ? 'import decimal' : null,
    allTypeChains.some(isArrayType) ? 'from collections.abc import Iterable' : null,
    'import zaber_bson',
    ...usings,
    { indent: 0 },
  ];
}

function zeroValuesFunction(dtoClass: DtoClass): Output {
  return [
    '@staticmethod',
    `def zero_values() -> '${dtoClass.name}':`,
    [
      `return ${dtoClass.name}(`,
      dtoClass.properties.map(property => {
        const defaultValue = typeMapper.mapDefault(property.type);
        return `${_.snakeCase(property.name).toLowerCase()}=${defaultValue},`;
      }),
      ')',
    ],
    '',
    { indent: 0 },
  ];
}

function serializationValue(type: AnyType, parentType: AnyType | null, valueName: string): string {
  // In Python we tolerate numeric types interchangably and tolerate None as string.
  // Additionally, basic types can be inherited in various libraries.
  // To ensure we end up with a valid value, we cast everything to the expected type.
  return match(type)
    .with({ type: 'optional' }, ({ of }) => {
      const notNil = serializationValue(of, type, valueName);
      if (isArrayType(of) || notNil === valueName) {
        return notNil;
      }
      return `${notNil} if ${valueName} is not None else None`;
    })
    .with({ type: 'ref' }, () => `${valueName}.to_dict()`)
    .with({ type: 'array' }, ({ of }) => {
      const itemValue = serializationValue(of, type, 'item');
      if (itemValue === 'item') {
        return `list(${valueName} or [])`;
      }
      return `[${itemValue} for item in ${valueName}] if ${valueName} is not None else []`;
    })
    .with({ type: 'enum' }, () => `${valueName}.value`)
    .with('Units', () => `units_from_literals(${valueName}).value`)
    .with('string', () => {
      if (parentType == null || !isOptionalType(parentType)) {
        // None is tolerated as empty string
        return `str(${valueName} or '')`;
      }
      return `str(${valueName})`;
    })
    .with('bool', () => `bool(${valueName})`)
    .with('double', () => `float(${valueName})`)
    .with(P.union('int', 'uint'), () => `int(${valueName})`)
    .exhaustive();
}

function deserializationValue(type: AnyType, valueName: string): string {
  if (isEnumType(type)) {
    return `${type.name}(${valueName})`;
  } else if (isReferenceType(type)) {
    return `${type.name}.from_dict(${valueName})`;
  } else if (isUnitsType(type)) {
    return `Units(${valueName})`;
  } else if (isOptionalType(type)) {
    const notNil = deserializationValue(type.of, valueName);
    if (notNil === valueName) {
      return valueName;
    }
    return `${notNil} if ${valueName} is not None else None`;
  } else if (isArrayType(type)) {
    const itemValue = deserializationValue(type.of, 'item');
    if (itemValue === 'item') {
      return valueName;
    }
    return `[${itemValue} for item in ${valueName}]`;
  }
  return valueName;
}

function propertyToDict(property: DtoProperty): string {
  const name = propName(property);
  const dictName = _.lowerFirst(property.name);
  const value = serializationValue(property.type, null, `self.${name}`);
  return `'${dictName}': ${value},`;
}

function propertyFromDict(property: DtoProperty): string {
  const name = propName(property);
  const dictName = _.lowerFirst(property.name);
  const value = deserializationValue(property.type, `data.get('${dictName}')`);
  return `${name}=${value},  # type: ignore`;
}

function toDictionaryMethods(dtoClass: DtoClass): Output {
  return [
    'def to_dict(self) -> Dict[str, Any]:',
    [
      'return {',
      dtoClass.properties.map(propertyToDict),
      '}',
    ],
    '',
    '@staticmethod',
    `def from_dict(data: Dict[str, Any]) -> '${dtoClass.name}':`,
    [
      `return ${dtoClass.name}(`,
      dtoClass.properties.map(propertyFromDict),
      ')',
    ],
    '',
    { indent: 0 },
  ];
}

function propertySorter(a: DtoProperty, b: DtoProperty): number {
  return Number(isOptionalType(a.type)) - Number(isOptionalType(b.type));
}

function binaryMethods(dtoClass: DtoClass): Output {
  return [
    '@staticmethod',
    `def from_binary(data_bytes: bytes) -> '${dtoClass.name}':`,
    '    """" Deserialize a binary representation of this class. """',
    '    data = zaber_bson.loads(data_bytes)  # type: Dict[str, Any]',
    `    return ${dtoClass.name}.from_dict(data)`,
    '',
    'def to_binary(self) -> bytes:',
    '    """" Serialize this class to a binary representation. """',
    '    self.validate()',
    '    return zaber_bson.dumps(self.to_dict())  # type: ignore',
    '',
    { indent: 0 },
  ];
}

function sanitizeNotNull(property: DtoProperty, dtoClass: DtoClass, expression: string, errorText: string): Output {
  return [
    `if ${expression} is None:`,
    [
      `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is None.')`,
      '',
    ],
    { indent: 0 },
  ];
}

function sanitizeInstanceOf(property: DtoProperty, dtoClass: DtoClass, type: AnyType, expression: string, errorText: string): Output {
  const propType = typeMapper.mapType(type);
  return [
    `if not isinstance(${expression}, ${propType}):`,
    [
      `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is not an instance of "${propType}".')`,
      '',
    ],
    { indent: 0 },
  ];
}

function sanitizeNumber(property: DtoProperty, dtoClass: DtoClass, type: AnyType, expression: string, errorText: string): Output {
  const output: Output = [
    { indent: 0 },
    `if not isinstance(${expression}, (int, float, decimal.Decimal)):`,
    [
      `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is not a number.')`,
      '',
    ],
  ];

  if (type === 'int' || type === 'uint') {
    output.push([
      `if int(${expression}) != ${expression}:`,
      [
        `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is not integer value.')`,
        '',
      ],
      { indent: 0 }
    ]);
  }

  return output;
}

function sanitizeString(property: DtoProperty, dtoClass: DtoClass, expression: string, errorText: string): Output {
  return [
    `if not isinstance(${expression}, str):`,
    [
      `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is not a string.')`,
      '',
    ],
    { indent: 0 },
  ];
}

function sanitizeUnits(property: DtoProperty, dtoClass: DtoClass, expression: string, errorText: string): Output {
  return [
    `if not isinstance(${expression}, (Units, str)):`,
    [
      `raise ValueError(f'${errorText} "${property.name}" of "${dtoClass.name}" is not Units.')`,
      '',
    ],
    { indent: 0 },
  ];
}

function isPyOptional(type: AnyType): boolean {
  return isOptionalType(type) || type === 'string' || type === 'bool';
}

function propertyChecks(
  dtoClass: DtoClass, property: DtoProperty,
  type: AnyType, expression: string, errorText: string,
): Output[] {
  const stripped = stripOptional(type);
  const checks: Output = [];

  if (isReferenceType(stripped) || isEnumType(stripped)) {
    checks.push(sanitizeInstanceOf(property, dtoClass, stripped, expression, errorText));
    if (isReferenceType(stripped)) {
      checks.push(`${expression}.validate()`, '');
    }
  } else if (isNumericType(stripped)) {
    checks.push(sanitizeNumber(property, dtoClass, stripped, expression, errorText));
  } else if (stripped === 'string') {
    checks.push(sanitizeString(property, dtoClass, expression, errorText));
  } else if (stripped === 'Units') {
    checks.push(sanitizeUnits(property, dtoClass, expression, errorText));
  }

  return checks;
}

function sanitizeMethod(dtoClass: DtoClass): Output {
  const output: Output = [];

  for (const property of dtoClass.properties) {
    const stripped = stripOptional(property.type);

    if (isArrayType(stripped)) {
      const item = `${propName(property)}_item`;
      const itemType = getItemType(property.type);
      let itemCheck: Output = [];

      const checks = propertyChecks(dtoClass, property, itemType, item, 'Item {i} in property');
      if (isPyOptional(itemType)) {
        if (checks.length > 0) {
          itemCheck.push(...[
            `if ${item} is not None:`,
            checks,
          ]);
        }
      } else {
        itemCheck.push(sanitizeNotNull(property, dtoClass, item, 'Item {i} in property'));
        itemCheck.push(...checks);
      }

      if (itemCheck.length > 0) {
        itemCheck = [
          `for i, ${item} in enumerate(self.${propName(property)}):`,
          itemCheck,
          { indent: 0 },
        ];
      }

      output.push([
        `if self.${propName(property)} is not None:`,
        [
          `if not isinstance(self.${propName(property)}, Iterable):`,
          [
            `raise ValueError('Property "${property.name}" of "${dtoClass.name}" is not iterable.')`,
            '',
          ],
          itemCheck,
        ],
        { indent: 0 }
      ]);
    } else {
      const checks = propertyChecks(dtoClass, property, property.type, `self.${propName(property)}`, 'Property');
      if (isPyOptional(property.type)) {
        if (checks.length) {
          output.push(...[
            `if self.${propName(property)} is not None:`,
            checks,
          ]);
        }
      } else {
        output.push(sanitizeNotNull(property, dtoClass, `self.${propName(property)}`, 'Property'));
        output.push(...checks);
      }
    }
  }

  return [
    'def validate(self) -> None:',
    '    """" Validates the properties of the instance. """',
    output.length ? output : ['pass', ''],
    { indent: 0 },
  ];
}

export function generateClass(dtoClass: DtoClass): Output {
  return [
    GENERATED_WARNING,
    `# pylint: disable=${[
      'line-too-long',
      'unused-argument',
      'f-string-without-interpolation',
      'too-many-branches',
      'too-many-statements',
      'unnecessary-pass',
    ].join(', ')}`,
    generateImports(dtoClass),
    '',
    '',
    '@dataclass',
    `class ${dtoClass.name}:`,
    [
      description(dtoClass),
      '',
      ...dtoClass.properties
        .slice().sort(propertySorter)
        .map(property => generateProperty(property, dtoClass)),
      zeroValuesFunction(dtoClass),
      binaryMethods(dtoClass),
      toDictionaryMethods(dtoClass),
      sanitizeMethod(dtoClass),
    ],
  ];
}
