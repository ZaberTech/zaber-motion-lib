import _ from 'lodash';

import type { DtoClass, DtoEnum, Namespace, Definitions } from '../../types';
import { ensureArray } from '../../../utils';
import { outputToString, type FileOutput, Output } from '../output';
import { LanguageGenerator } from '../types';

import { generateClass } from './class';
import { generateEnum } from './enum';
import { BASE_INDENT } from './utils';
import { GENERATED_WARNING } from './common';

function namespaceToPath({ namespace }: { namespace: Namespace }): string {
  return ensureArray(namespace).join('/');
}

function fileName(something: { name: string; namespace: Namespace }): string {
  const namespace = namespaceToPath(something);
  let path = `${_.snakeCase(something.name)}.py`;
  if (namespace) {
    path = `${namespace}/${path}`;
  }
  return path;
}

export const pyGenerator: LanguageGenerator = {
  generateClass(dtoClass: DtoClass): FileOutput[] {
    const content = generateClass(dtoClass);

    return [{
      path: fileName(dtoClass),
      content: outputToString(content, BASE_INDENT),
    }];
  },
  generateEnum(dtoEnum: DtoEnum): FileOutput[] {
    const content = generateEnum(dtoEnum);

    return [{
      path: fileName(dtoEnum),
      content: outputToString(content, BASE_INDENT),
    }];
  },
  generateIndexFiles(definitions: Definitions): FileOutput[] {
    const byNamespace = _.groupBy([
      ...definitions.classes,
      ...definitions.enums,
    ], file => ensureArray(file.namespace).join('/'));
    const files = _.map(byNamespace, (files, namespace) => {
      files.sort((a, b) => a.name.localeCompare(b.name));
      const content = [
        GENERATED_WARNING,
        '# pylint: disable=line-too-long',
        ...files.map(file => `from .${_.snakeCase(file.name)} import ${file.name} as ${file.name}`),
        '',
      ] satisfies Output;
      return {
        path: `${namespace}/__init__.py`,
        content: outputToString(content, BASE_INDENT),
      };
    });
    return files;
  }
};
