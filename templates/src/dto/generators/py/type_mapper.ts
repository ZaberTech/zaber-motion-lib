import { P, match } from 'ts-pattern';

import { AnyType } from '../../types';

const typeMap: Record<string, string> = {
  uint: 'int',
  double: 'float',
  string: 'str',
  Units: 'UnitsAndLiterals',
};

function mapType(type: AnyType): string {
  return match(type)
    .with({ type: 'ref' }, ({ name }) => name)
    .with({ type: 'enum' }, ({ name }) => name)
    .with({ type: 'array' }, ({ of }) => `List[${mapType(of)}]`)
    .with({ type: 'optional' }, ({ of }) => `Optional[${mapType(of)}]`)
    .with(P.string, type => typeMap[type] || type)
    .exhaustive();
}

export const typeMapper = {
  mapType,
  mapDefault(type: AnyType): string {
    return match(type)
      .with({ type: 'ref' }, ({ name }) => `${name}.zero_values()`)
      .with({ type: 'enum' }, ({ name }) => `next(first for first in ${name})`)
      .with({ type: 'array' }, () => '[]')
      .with({ type: 'optional' }, () => 'None')
      .with(P.string, type => {
        switch (type) {
          case 'string': return '""';
          case 'bool': return 'False';
          case 'Units': return 'Units.NATIVE';
          default: return '0';
        }
      })
      .exhaustive();
  },
  mapDefaultField(type: AnyType): string {
    return match(type)
      .with({ type: 'ref' }, ({ name }) => `field(default_factory=${name}.zero_values)`)
      .with({ type: 'array' }, () => 'field(default_factory=list)')
      .otherwise(() => this.mapDefault(type));
  },
};
