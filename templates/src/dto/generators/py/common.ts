import { Description } from '../../types';
import { ensureArray } from '../../../utils';
import { Output } from '../output';

export const GENERATED_WARNING = '# This file is generated. Do not modify by hand.';

export function description({ description }: { description?: Description }): Output {
  if (!description) {
    return null;
  }
  return [
    '"""',
    ...ensureArray(description),
    '"""',
    { indent: 0 },
  ];
}
