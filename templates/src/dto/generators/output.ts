import { P } from 'ts-pattern';

import { isMatch } from '../../utils';

export type Output = OutputNode;

export type Line = string;
export type OutputNode = Line | OutputNode[] | OutputOptions | null;
type OutputOptions = { indent?: number };

function isOptions(node: OutputNode): node is OutputOptions {
  return isMatch({ indent: P.union(P.nullish, P.number) }, node);
}

export type FileOutput = {
  path: string;
  content: string[];
};

export function outputToString(output: Output, baseIndent = 0): string[] {
  function recurse(output: Output, topLevel: boolean): string[] {
    if (typeof output === 'string') {
      return [output];
    } else if (Array.isArray(output)) {
      const options = output.filter(isOptions);
      if (options.length > 1) {
        throw new Error('Too many options');
      }
      const indentStr = ' '.repeat(options[0]?.indent ?? (topLevel ? 0 : baseIndent));
      return output.flatMap(node => recurse(node, false)).map(line => `${indentStr}${line}`);
    } else {
      return [];
    }
  }

  return recurse(output, true);
}
