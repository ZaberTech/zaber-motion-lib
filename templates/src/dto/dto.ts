import fs from 'node:fs/promises';
import path from 'node:path';
import os from 'node:os';

import { rimraf } from 'rimraf';

import { definitions } from './definitions';
import { Languages, LanguageGenerator, generators, FileOutput } from './generators';
import { checkDefinitions } from './checks';

const DRY_RUN = !!process.env.DRY_RUN;

const baseDirs: Record<Languages, string> = {
  cs: `${!DRY_RUN ? '..' : 'out'}/csharp/Zaber.Motion/Api`,
  go: `${!DRY_RUN ? '..' : 'out'}/internal`,
  ts: `${!DRY_RUN ? '..' : 'out'}/js/src`,
  py: `${!DRY_RUN ? '..' : 'out'}/py/zaber_motion/dto`,
  java: `${!DRY_RUN ? '..' : 'out'}/java/src/main/java/zaber/motion`,
  cpp: `${!DRY_RUN ? '..' : 'out'}/cpp/src/zaber/motion/dto`,
  swift: `${!DRY_RUN ? '..' : 'out'}/swift/Sources/ZaberMotion/Dto`,
};

let writeFileCounter = 0;

async function writeFile(baseDir: string, output: FileOutput) {
  const fullPath = `${baseDir}/${output.path}`.replace(/\/+/g, path.sep);
  await fs.mkdir(path.dirname(fullPath), { recursive: true });
  await fs.writeFile(fullPath, output.content.map(line => line.trimEnd()).join(os.EOL), 'utf-8');
  writeFileCounter++;
  return fullPath;
}

export async function generateDto() {
  checkDefinitions(definitions);

  await rimraf('out');

  for (const [language, generator] of Object.entries(generators) as [Languages, LanguageGenerator][]) {
    const allFiles: string[] = [];

    for (const classDto of definitions.classes) {
      const files = generator.generateClass(classDto);
      for (const file of files) {
        const fullPath = await writeFile(baseDirs[language], file);
        allFiles.push(fullPath);
      }
    }
    for (const enumDto of definitions.enums) {
      const files = generator.generateEnum(enumDto);
      for (const file of files) {
        const fullPath = await writeFile(baseDirs[language], file);
        allFiles.push(fullPath);
      }
    }
    if (generator.generateIndexFiles) {
      const files = generator.generateIndexFiles(definitions);
      for (const file of files) {
        const fullPath = await writeFile(baseDirs[language], file);
        allFiles.push(fullPath);
      }
    }

    if (generator.formatter) {
      for (const file of allFiles) {
        await generator.formatter(file);
      }
    }
  }

  console.log(`Generated ${writeFileCounter} DTO files.`);
}
