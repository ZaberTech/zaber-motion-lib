import _ from 'lodash';
import { ensureArray } from '@zaber/toolbox';

export type Namespace = string[] | string;

export function namespaceEquals(a: Namespace, b: Namespace): boolean {
  return _.isEqual(ensureArray(a), ensureArray(b));
}

export interface WithNamespace {
  name: string;
  namespace: Namespace;
}
