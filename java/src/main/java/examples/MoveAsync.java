package examples;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.Library;
import zaber.motion.LogOutputMode;
import zaber.motion.Units;

public class MoveAsync {

    private static Connection connection;
    private static Device device;
    private static Axis axis;

    public static void main(String[] args) {
        Library.setLogOutput(LogOutputMode.STDOUT, "");

        CompletableFuture<Connection> conn = Connection.openSerialPortAsync("COM2", 115200);

        CompletableFuture<Void> setDevice = conn.thenCompose(c -> {
            connection = c;
            return c.detectDevicesAsync(true);
        }).thenAccept(devices -> {
            device = devices[0];
        });

        CompletableFuture<Void> homeDeviceAndSetAxis = setDevice.thenApply(discarded -> {
            return device.getAllAxes().homeAsync(true);
        }).thenRun(() -> {
            axis = device.getAxis(1);
        });

        CompletableFuture<Void> moveSeries = homeDeviceAndSetAxis.thenCompose(discarded -> {
            return axis.moveAbsoluteAsync(1.0, Units.LENGTH_CENTIMETRES, true);
        }).thenCompose(discarded -> {
            return axis.moveRelativeAsync(-5.0, Units.LENGTH_MILLIMETRES, true);
        }).thenCompose(discarded -> {
            return axis.moveVelocityAsync(1.0, Units.VELOCITY_MILLIMETRES_PER_SECOND);
        });

        CompletableFuture<Void> sleepThenStopAxis = moveSeries.thenRun(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).thenCompose(discarded -> {
            return axis.stopAsync(true);
        });

        CompletableFuture<Void> getPosition = sleepThenStopAxis.thenCompose(discarded -> {
            return axis.getPositionAsync(Units.LENGTH_MILLIMETRES);
        }).thenAccept(position -> {
            System.out.println("Position: " + position);
        });

        try {
            getPosition.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.closeAsync().get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
