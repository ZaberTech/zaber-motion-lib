package examples;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.Library;
import zaber.motion.LogOutputMode;
import zaber.motion.Units;

public class Move {

    public static void main(String[] args) {
        Library.setLogOutput(LogOutputMode.STDOUT);

        try (Connection conn = Connection.openSerialPort("COM2")) {
            Device device = conn.detectDevices()[0];

            device.getAllAxes().home();

            Axis axis = device.getAxis(1);

            axis.moveAbsolute(1.0, Units.LENGTH_CENTIMETRES);

            axis.moveRelative(-5.0, Units.LENGTH_MILLIMETRES);

            axis.moveVelocity(1.0, Units.VELOCITY_MILLIMETRES_PER_SECOND);

            Thread.sleep(2000);
            axis.stop();

            double position = axis.getPosition(Units.LENGTH_MILLIMETRES);
            System.out.println("Position: " + position);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
