package examples;

import zaber.motion.ascii.Connection;
import java.lang.Thread;
import java.util.List;
import java.util.ArrayList;

public class SpeedTest {
    private static final int THREAD_COUNT = 12;
    private static final int CYCLE_COUNT = 10000;

    public static void main(String[] args) {
        for (int i = 1; i <= SpeedTest.THREAD_COUNT; i++) {
            runTest(i);
        }
    }

    private static void runTest(int threadCount) {
        List<Thread> threads = new ArrayList<>();
        final List<Double> averages = new ArrayList<>();

        for (int i = 0; i < threadCount; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    double avg = 0;
                    try (Connection conn = Connection.openTcp("127.0.0.1", 11234)) {

                        for (int i = 0; i < SpeedTest.CYCLE_COUNT; i++) {

                            long startTime = System.nanoTime();
                            conn.genericCommand("");
                            long endTime = System.nanoTime();

                            double elapsedMs = (endTime - startTime) / 1000000.0;
                            avg += elapsedMs;
                        }

                        avg /= SpeedTest.CYCLE_COUNT;

                        synchronized (averages) {
                            averages.add(avg);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Runtime.getRuntime().exit(1);
                    }
                }
            });
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }

        Double average = averages.stream().reduce(0.0, (sum, value) -> sum + value) / averages.size();
        System.out.println(threadCount + " " + average);
    }
}
