package examples;

import zaber.motion.binary.Connection;
import zaber.motion.binary.Device;
import java.util.Arrays;
import java.util.Comparator;

import zaber.motion.Library;
import zaber.motion.LogOutputMode;
import zaber.motion.Units;

public class BinaryMove {

    public class SortByAddress implements Comparator<Device> {
        public int compare(Device a, Device b) {
            return a.getDeviceAddress() - b.getDeviceAddress();
        }
    }

    public static void main(String[] args) {
        Library.setLogOutput(LogOutputMode.STDOUT);

        try (Connection conn = Connection.openSerialPort("COM3")) {
            conn.getUnknownResponse().subscribe(s -> System.out.println("Unhandled response: " + s.toString()));

            Device[] devices = conn.detectDevices();
            Arrays.sort(devices, new BinaryMove().new SortByAddress());
            System.out.println("Detected " + devices.length + " devices.");

            Device device = devices[0];
            System.out.println("Device " + device.getDeviceAddress() + " has device ID " + device.getIdentity().getDeviceId());

            double pos = device.home(Units.LENGTH_CENTIMETRES);
            System.out.println(String.format("Position after home: %.2f cm", pos));

            pos = device.moveAbsolute(1.0, Units.LENGTH_CENTIMETRES);
            System.out.println(String.format("Position after move absolute: %.2f cm", pos));

            pos = device.moveRelative(5.0, Units.LENGTH_MILLIMETRES);
            System.out.println(String.format("Position after move relative: %.2f mm", pos));

            double velocity = device.moveVelocity(1.0, Units.VELOCITY_MILLIMETRES_PER_SECOND);
            System.out.println(String.format("Starting move velocity with speed: %.2f mm/s", velocity));

            Thread.sleep(2000);

            pos = device.stop(Units.LENGTH_CENTIMETRES);
            System.out.println(String.format("Position after stop: %.2f cm", pos));

            System.out.println(String.format("Final position in microsteps: %.2f", device.getPosition()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
