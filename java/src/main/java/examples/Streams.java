package examples;

import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.ascii.DigitalOutputAction;
import zaber.motion.ascii.Stream;
import zaber.motion.ascii.StreamBuffer;

import java.util.Arrays;

import zaber.motion.DeviceDbSourceType;
import zaber.motion.Library;
import zaber.motion.LogOutputMode;
import zaber.motion.Measurement;
import zaber.motion.RotationDirection;
import zaber.motion.Units;

public class Streams {
    public static void main(String[] args) {
        Library.setLogOutput(LogOutputMode.STDOUT);
        Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, "https://api.zaber.io/device-db/master");

        try (Connection conn = Connection.openTcp("localhost", Connection.TCP_PORT_DEVICE_ONLY)) {
            Device device = conn.detectDevices()[0];

            device.getAllAxes().home();

            double numStreams = device.getSettings().get("stream.numstreams");
            System.out.println("Number of streams possible: " + numStreams);

            Stream stream = device.getStreams().getStream(1);

            StreamBuffer streamBuffer = device.getStreams().getBuffer(1);
            streamBuffer.erase();

            stream.setupStore(streamBuffer, 1, 2);

            stream.lineAbsolute(
                new Measurement(29.0047, Units.LENGTH_MILLIMETRES),
                new Measurement(40.49, Units.LENGTH_MILLIMETRES)
            );
            stream.lineRelative(
                new Measurement(0),
                new Measurement(50.5, Units.LENGTH_MILLIMETRES)
            );

            double[][] pathInCm = {{0.00, 3.00}, {2.25, 7.10}, {5.35, 0.15}, {1.45, 10.20}, {9.00, 9.00}};
            for (double[] point : pathInCm) {
                stream.lineAbsolute(
                    new Measurement(point[0], Units.LENGTH_CENTIMETRES),
                    new Measurement(point[1], Units.LENGTH_CENTIMETRES)
                );
            }

            String[] content = streamBuffer.getContent();
            System.out.println(Arrays.toString(content));

            stream.disable();

            stream.setupLive(1, 2);

            stream.call(streamBuffer);

            Measurement[] circleCenterAbs = {
                new Measurement(2, Units.LENGTH_CENTIMETRES),
                new Measurement(4, Units.LENGTH_CENTIMETRES),
            };
            stream.circleAbsolute(RotationDirection.CW, circleCenterAbs[0], circleCenterAbs[1]);

            Measurement[] circleCenterRel = {
                new Measurement(-2, Units.LENGTH_CENTIMETRES),
                new Measurement(0, Units.LENGTH_CENTIMETRES),
            };
            stream.circleRelative(RotationDirection.CCW, circleCenterRel[0], circleCenterRel[1]);

            Measurement[] arcCircleCenterRel = {
                new Measurement(-2, Units.LENGTH_CENTIMETRES),
                new Measurement(0, Units.LENGTH_CENTIMETRES),
            };
            Measurement[] arcEndRel = {
                new Measurement(-2, Units.LENGTH_CENTIMETRES),
                new Measurement(0, Units.LENGTH_CENTIMETRES),
            };
            stream.arcRelative(
                RotationDirection.CCW,
                arcCircleCenterRel[0], arcCircleCenterRel[1],
                arcEndRel[0], arcEndRel[1]
            );

            Measurement[] arcCircleCenterAbs = {
                new Measurement(2, Units.LENGTH_CENTIMETRES),
                new Measurement(4, Units.LENGTH_CENTIMETRES),
            };
            Measurement[] arcEndAbs = {
                new Measurement(4, Units.LENGTH_CENTIMETRES),
                new Measurement(4, Units.LENGTH_CENTIMETRES),
            };
            stream.arcAbsolute(
                RotationDirection.CW,
                arcCircleCenterAbs[0], arcCircleCenterAbs[1],
                arcEndAbs[0], arcEndAbs[1]
            );

            stream.lineAbsoluteOn(new int[] {1}, new Measurement[] { new Measurement(1) });

            stream.setMaxCentripetalAcceleration(5, Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
            stream.setMaxTangentialAcceleration(5, Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED);
            stream.setMaxSpeed(0.5, Units.VELOCITY_MILLIMETRES_PER_SECOND);

            stream.wait(2, Units.TIME_SECONDS);

            stream.getIo().setDigitalOutput(1, DigitalOutputAction.ON);
            stream.waitDigitalInput(1, true);

            stream.getIo().setDigitalOutput(1, DigitalOutputAction.TOGGLE);
            stream.getIo().setDigitalOutput(1, DigitalOutputAction.TOGGLE);

            stream.getIo().setAnalogOutput(1, 0.42);
            stream.waitAnalogInput(1, ">=", 0.50);

            stream.waitUntilIdle();

            System.out.println(stream.toString());
            System.out.println(Arrays.toString(stream.getAxes())); // TODO: problem
            System.out.println(stream.getMaxSpeed(Units.VELOCITY_CENTIMETRES_PER_SECOND));
            System.out.println(stream.getMaxTangentialAcceleration(Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED));
            System.out.println(stream.getMaxCentripetalAcceleration(Units.ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED));

            stream.cork();
            stream.uncork();

            if (stream.isBusy()) {
                stream.waitUntilIdle();
            }

            stream.disable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
