package examples;

import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.ascii.Stream;
import zaber.motion.gcode.*;

import java.util.Arrays;

import zaber.motion.DeviceDbSourceType;
import zaber.motion.Library;
import zaber.motion.LogOutputMode;
import zaber.motion.Measurement;
import zaber.motion.RotationDirection;
import zaber.motion.Units;

public class Gcode {
    public static void main(String[] args) {
        Library.setLogOutput(LogOutputMode.STDOUT);
        Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, "https://api.zaber.io/device-db/master");

        try (Connection conn = Connection.openTcp("localhost", 11321)) {
            Device device = conn.detectDevices()[0];

            device.getAllAxes().home();

            Stream stream = device.getStreams().getStream(1);

            stream.setupLive(1, 2);

            Translator t = Translator.setup(stream, new TranslatorConfig());

            t.translate("G28 Y10");
            t.translate("G0 X10 Y20");

            t.flush();
            stream.disable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
