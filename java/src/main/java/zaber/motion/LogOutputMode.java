/* This file is generated. Do not modify by hand. */

package zaber.motion;

/**
 * Mode of logging output of the library.
 */
public enum LogOutputMode {

    OFF(0),

    STDOUT(1),

    STDERR(2),

    FILE(3);

    private int value;

    LogOutputMode(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static LogOutputMode valueOf(int argValue) {
        for (LogOutputMode value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
