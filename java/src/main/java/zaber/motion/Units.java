// This file is generated from the Zaber device database. Do not manually edit this file.

package zaber.motion;

/**
* Units used by various functions.
*/
public enum Units {

    NATIVE(""),

    /**
    * Dimension: Length, unit: metres
    */
    LENGTH_METRES("Length:metres"),

    /**
    * Dimension: Length, unit: centimetres
    */
    LENGTH_CENTIMETRES("Length:centimetres"),

    /**
    * Dimension: Length, unit: millimetres
    */
    LENGTH_MILLIMETRES("Length:millimetres"),

    /**
    * Dimension: Length, unit: micrometres
    */
    LENGTH_MICROMETRES("Length:micrometres"),

    /**
    * Dimension: Length, unit: nanometres
    */
    LENGTH_NANOMETRES("Length:nanometres"),

    /**
    * Dimension: Length, unit: inches
    */
    LENGTH_INCHES("Length:inches"),

    /**
    * Dimension: Velocity, unit: metres per second
    */
    VELOCITY_METRES_PER_SECOND("Velocity:metres per second"),

    /**
    * Dimension: Velocity, unit: centimetres per second
    */
    VELOCITY_CENTIMETRES_PER_SECOND("Velocity:centimetres per second"),

    /**
    * Dimension: Velocity, unit: millimetres per second
    */
    VELOCITY_MILLIMETRES_PER_SECOND("Velocity:millimetres per second"),

    /**
    * Dimension: Velocity, unit: micrometres per second
    */
    VELOCITY_MICROMETRES_PER_SECOND("Velocity:micrometres per second"),

    /**
    * Dimension: Velocity, unit: nanometres per second
    */
    VELOCITY_NANOMETRES_PER_SECOND("Velocity:nanometres per second"),

    /**
    * Dimension: Velocity, unit: inches per second
    */
    VELOCITY_INCHES_PER_SECOND("Velocity:inches per second"),

    /**
    * Dimension: Acceleration, unit: metres per second squared
    */
    ACCELERATION_METRES_PER_SECOND_SQUARED("Acceleration:metres per second squared"),

    /**
    * Dimension: Acceleration, unit: centimetres per second squared
    */
    ACCELERATION_CENTIMETRES_PER_SECOND_SQUARED("Acceleration:centimetres per second squared"),

    /**
    * Dimension: Acceleration, unit: millimetres per second squared
    */
    ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED("Acceleration:millimetres per second squared"),

    /**
    * Dimension: Acceleration, unit: micrometres per second squared
    */
    ACCELERATION_MICROMETRES_PER_SECOND_SQUARED("Acceleration:micrometres per second squared"),

    /**
    * Dimension: Acceleration, unit: nanometres per second squared
    */
    ACCELERATION_NANOMETRES_PER_SECOND_SQUARED("Acceleration:nanometres per second squared"),

    /**
    * Dimension: Acceleration, unit: inches per second squared
    */
    ACCELERATION_INCHES_PER_SECOND_SQUARED("Acceleration:inches per second squared"),

    /**
    * Dimension: Angle, unit: degrees
    */
    ANGLE_DEGREES("Angle:degrees"),

    /**
    * Dimension: Angle, unit: radians
    */
    ANGLE_RADIANS("Angle:radians"),

    /**
    * Dimension: Angular Velocity, unit: degrees per second
    */
    ANGULAR_VELOCITY_DEGREES_PER_SECOND("Angular Velocity:degrees per second"),

    /**
    * Dimension: Angular Velocity, unit: radians per second
    */
    ANGULAR_VELOCITY_RADIANS_PER_SECOND("Angular Velocity:radians per second"),

    /**
    * Dimension: Angular Acceleration, unit: degrees per second squared
    */
    ANGULAR_ACCELERATION_DEGREES_PER_SECOND_SQUARED("Angular Acceleration:degrees per second squared"),

    /**
    * Dimension: Angular Acceleration, unit: radians per second squared
    */
    ANGULAR_ACCELERATION_RADIANS_PER_SECOND_SQUARED("Angular Acceleration:radians per second squared"),

    /**
    * Dimension: AC Electric Current, unit: amperes peak
    */
    AC_ELECTRIC_CURRENT_AMPERES_PEAK("AC Electric Current:amperes peak"),

    /**
    * Dimension: AC Electric Current, unit: amperes RMS
    */
    AC_ELECTRIC_CURRENT_AMPERES_RMS("AC Electric Current:amperes RMS"),

    /**
    * Dimension: Percent, unit: percent
    */
    PERCENT_PERCENT("Percent:percent"),

    /**
    * Dimension: DC Electric Current, unit: amperes
    */
    DC_ELECTRIC_CURRENT_AMPERES("DC Electric Current:amperes"),

    /**
    * Dimension: Force, unit: newtons
    */
    FORCE_NEWTONS("Force:newtons"),

    /**
    * Dimension: Force, unit: millinewtons
    */
    FORCE_MILLINEWTONS("Force:millinewtons"),

    /**
    * Dimension: Force, unit: pounds-force
    */
    FORCE_POUNDS_FORCE("Force:pounds-force"),

    /**
    * Dimension: Force, unit: kilonewtons
    */
    FORCE_KILONEWTONS("Force:kilonewtons"),

    /**
    * Dimension: Time, unit: seconds
    */
    TIME_SECONDS("Time:seconds"),

    /**
    * Dimension: Time, unit: milliseconds
    */
    TIME_MILLISECONDS("Time:milliseconds"),

    /**
    * Dimension: Time, unit: microseconds
    */
    TIME_MICROSECONDS("Time:microseconds"),

    /**
    * Dimension: Torque, unit: newton metres
    */
    TORQUE_NEWTON_METRES("Torque:newton metres"),

    /**
    * Dimension: Torque, unit: newton centimetres
    */
    TORQUE_NEWTON_CENTIMETRES("Torque:newton centimetres"),

    /**
    * Dimension: Torque, unit: pound-force-feet
    */
    TORQUE_POUND_FORCE_FEET("Torque:pound-force-feet"),

    /**
    * Dimension: Torque, unit: ounce-force-inches
    */
    TORQUE_OUNCE_FORCE_INCHES("Torque:ounce-force-inches"),

    /**
    * Dimension: Inertia, unit: grams
    */
    INERTIA_GRAMS("Inertia:grams"),

    /**
    * Dimension: Inertia, unit: kilograms
    */
    INERTIA_KILOGRAMS("Inertia:kilograms"),

    /**
    * Dimension: Inertia, unit: milligrams
    */
    INERTIA_MILLIGRAMS("Inertia:milligrams"),

    /**
    * Dimension: Inertia, unit: pounds
    */
    INERTIA_POUNDS("Inertia:pounds"),

    /**
    * Dimension: Inertia, unit: ounces
    */
    INERTIA_OUNCES("Inertia:ounces"),

    /**
    * Dimension: Rotational Inertia, unit: gram-square metre
    */
    ROTATIONAL_INERTIA_GRAM_SQUARE_METRE("Rotational Inertia:gram-square metre"),

    /**
    * Dimension: Rotational Inertia, unit: kilogram-square metre
    */
    ROTATIONAL_INERTIA_KILOGRAM_SQUARE_METRE("Rotational Inertia:kilogram-square metre"),

    /**
    * Dimension: Rotational Inertia, unit: pound-square-feet
    */
    ROTATIONAL_INERTIA_POUND_SQUARE_FEET("Rotational Inertia:pound-square-feet"),

    /**
    * Dimension: Force Constant, unit: newtons per amp
    */
    FORCE_CONSTANT_NEWTONS_PER_AMP("Force Constant:newtons per amp"),

    /**
    * Dimension: Force Constant, unit: millinewtons per amp
    */
    FORCE_CONSTANT_MILLINEWTONS_PER_AMP("Force Constant:millinewtons per amp"),

    /**
    * Dimension: Force Constant, unit: kilonewtons per amp
    */
    FORCE_CONSTANT_KILONEWTONS_PER_AMP("Force Constant:kilonewtons per amp"),

    /**
    * Dimension: Force Constant, unit: pounds-force per amp
    */
    FORCE_CONSTANT_POUNDS_FORCE_PER_AMP("Force Constant:pounds-force per amp"),

    /**
    * Dimension: Torque Constant, unit: newton metres per amp
    */
    TORQUE_CONSTANT_NEWTON_METRES_PER_AMP("Torque Constant:newton metres per amp"),

    /**
    * Dimension: Torque Constant, unit: millinewton metres per amp
    */
    TORQUE_CONSTANT_MILLINEWTON_METRES_PER_AMP("Torque Constant:millinewton metres per amp"),

    /**
    * Dimension: Torque Constant, unit: kilonewton metres per amp
    */
    TORQUE_CONSTANT_KILONEWTON_METRES_PER_AMP("Torque Constant:kilonewton metres per amp"),

    /**
    * Dimension: Torque Constant, unit: pound-force-feet per amp
    */
    TORQUE_CONSTANT_POUND_FORCE_FEET_PER_AMP("Torque Constant:pound-force-feet per amp"),

    /**
    * Dimension: Voltage, unit: volts
    */
    VOLTAGE_VOLTS("Voltage:volts"),

    /**
    * Dimension: Voltage, unit: millivolts
    */
    VOLTAGE_MILLIVOLTS("Voltage:millivolts"),

    /**
    * Dimension: Voltage, unit: microvolts
    */
    VOLTAGE_MICROVOLTS("Voltage:microvolts"),

    /**
    * Dimension: Current Controller Proportional Gain, unit: volts per amp
    */
    CURRENT_CONTROLLER_PROPORTIONAL_GAIN_VOLTS_PER_AMP("Current Controller Proportional Gain:volts per amp"),

    /**
    * Dimension: Current Controller Proportional Gain, unit: millivolts per amp
    */
    CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MILLIVOLTS_PER_AMP("Current Controller Proportional Gain:millivolts per amp"),

    /**
    * Dimension: Current Controller Proportional Gain, unit: microvolts per amp
    */
    CURRENT_CONTROLLER_PROPORTIONAL_GAIN_MICROVOLTS_PER_AMP("Current Controller Proportional Gain:microvolts per amp"),

    /**
    * Dimension: Current Controller Integral Gain, unit: volts per amp per second
    */
    CURRENT_CONTROLLER_INTEGRAL_GAIN_VOLTS_PER_AMP_PER_SECOND("Current Controller Integral Gain:volts per amp per second"),

    /**
    * Dimension: Current Controller Integral Gain, unit: millivolts per amp per second
    */
    CURRENT_CONTROLLER_INTEGRAL_GAIN_MILLIVOLTS_PER_AMP_PER_SECOND("Current Controller Integral Gain:millivolts per amp per second"),

    /**
    * Dimension: Current Controller Integral Gain, unit: microvolts per amp per second
    */
    CURRENT_CONTROLLER_INTEGRAL_GAIN_MICROVOLTS_PER_AMP_PER_SECOND("Current Controller Integral Gain:microvolts per amp per second"),

    /**
    * Dimension: Current Controller Derivative Gain, unit: volts second per amp
    */
    CURRENT_CONTROLLER_DERIVATIVE_GAIN_VOLTS_SECOND_PER_AMP("Current Controller Derivative Gain:volts second per amp"),

    /**
    * Dimension: Current Controller Derivative Gain, unit: millivolts second per amp
    */
    CURRENT_CONTROLLER_DERIVATIVE_GAIN_MILLIVOLTS_SECOND_PER_AMP("Current Controller Derivative Gain:millivolts second per amp"),

    /**
    * Dimension: Current Controller Derivative Gain, unit: microvolts second per amp
    */
    CURRENT_CONTROLLER_DERIVATIVE_GAIN_MICROVOLTS_SECOND_PER_AMP("Current Controller Derivative Gain:microvolts second per amp"),

    /**
    * Dimension: Resistance, unit: kiloohms
    */
    RESISTANCE_KILOOHMS("Resistance:kiloohms"),

    /**
    * Dimension: Resistance, unit: ohms
    */
    RESISTANCE_OHMS("Resistance:ohms"),

    /**
    * Dimension: Resistance, unit: milliohms
    */
    RESISTANCE_MILLIOHMS("Resistance:milliohms"),

    /**
    * Dimension: Resistance, unit: microohms
    */
    RESISTANCE_MICROOHMS("Resistance:microohms"),

    /**
    * Dimension: Resistance, unit: nanoohms
    */
    RESISTANCE_NANOOHMS("Resistance:nanoohms"),

    /**
    * Dimension: Inductance, unit: henries
    */
    INDUCTANCE_HENRIES("Inductance:henries"),

    /**
    * Dimension: Inductance, unit: millihenries
    */
    INDUCTANCE_MILLIHENRIES("Inductance:millihenries"),

    /**
    * Dimension: Inductance, unit: microhenries
    */
    INDUCTANCE_MICROHENRIES("Inductance:microhenries"),

    /**
    * Dimension: Inductance, unit: nanohenries
    */
    INDUCTANCE_NANOHENRIES("Inductance:nanohenries"),

    /**
    * Dimension: Voltage Constant, unit: volt seconds per radian
    */
    VOLTAGE_CONSTANT_VOLT_SECONDS_PER_RADIAN("Voltage Constant:volt seconds per radian"),

    /**
    * Dimension: Voltage Constant, unit: millivolt seconds per radian
    */
    VOLTAGE_CONSTANT_MILLIVOLT_SECONDS_PER_RADIAN("Voltage Constant:millivolt seconds per radian"),

    /**
    * Dimension: Voltage Constant, unit: microvolt seconds per radian
    */
    VOLTAGE_CONSTANT_MICROVOLT_SECONDS_PER_RADIAN("Voltage Constant:microvolt seconds per radian"),

    /**
    * Dimension: Absolute Temperature, unit: degrees Celsius
    */
    ABSOLUTE_TEMPERATURE_DEGREES_CELSIUS("Absolute Temperature:degrees Celsius"),

    /**
    * Dimension: Absolute Temperature, unit: kelvins
    */
    ABSOLUTE_TEMPERATURE_KELVINS("Absolute Temperature:kelvins"),

    /**
    * Dimension: Absolute Temperature, unit: degrees Fahrenheit
    */
    ABSOLUTE_TEMPERATURE_DEGREES_FAHRENHEIT("Absolute Temperature:degrees Fahrenheit"),

    /**
    * Dimension: Absolute Temperature, unit: degrees Rankine
    */
    ABSOLUTE_TEMPERATURE_DEGREES_RANKINE("Absolute Temperature:degrees Rankine"),

    /**
    * Dimension: Relative Temperature, unit: degrees Celsius
    */
    RELATIVE_TEMPERATURE_DEGREES_CELSIUS("Relative Temperature:degrees Celsius"),

    /**
    * Dimension: Relative Temperature, unit: kelvins
    */
    RELATIVE_TEMPERATURE_KELVINS("Relative Temperature:kelvins"),

    /**
    * Dimension: Relative Temperature, unit: degrees Fahrenheit
    */
    RELATIVE_TEMPERATURE_DEGREES_FAHRENHEIT("Relative Temperature:degrees Fahrenheit"),

    /**
    * Dimension: Relative Temperature, unit: degrees Rankine
    */
    RELATIVE_TEMPERATURE_DEGREES_RANKINE("Relative Temperature:degrees Rankine"),

    /**
    * Dimension: Frequency, unit: gigahertz
    */
    FREQUENCY_GIGAHERTZ("Frequency:gigahertz"),

    /**
    * Dimension: Frequency, unit: megahertz
    */
    FREQUENCY_MEGAHERTZ("Frequency:megahertz"),

    /**
    * Dimension: Frequency, unit: kilohertz
    */
    FREQUENCY_KILOHERTZ("Frequency:kilohertz"),

    /**
    * Dimension: Frequency, unit: hertz
    */
    FREQUENCY_HERTZ("Frequency:hertz"),

    /**
    * Dimension: Frequency, unit: millihertz
    */
    FREQUENCY_MILLIHERTZ("Frequency:millihertz"),

    /**
    * Dimension: Frequency, unit: microhertz
    */
    FREQUENCY_MICROHERTZ("Frequency:microhertz"),

    /**
    * Dimension: Frequency, unit: nanohertz
    */
    FREQUENCY_NANOHERTZ("Frequency:nanohertz");


    private String name;

    Units(String name) {
        this.name = name;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public String getName() {
        return name;
    }

    public static Units fromName(String name) {
        for (Units unit : Units.values()) {
            if (unit.name.equals(name)) {
                return unit;
            }
        }
        return Units.NATIVE;
    }
}
