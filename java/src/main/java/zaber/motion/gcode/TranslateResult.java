/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Represents a result of a G-code block translation.
 */
public final class TranslateResult implements zaber.motion.dto.Message {

    private String[] commands;

    /**
     * Stream commands resulting from the block.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("commands")
    public void setCommands(String[] commands) {
        this.commands = commands;
    }

    /**
     * Stream commands resulting from the block.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("commands")
    public String[] getCommands() {
        return this.commands;
    }

    /**
     * Stream commands resulting from the block.
     */
    public TranslateResult withCommands(String[] aCommands) {
        this.setCommands(aCommands);
        return this;
    }

    private TranslateMessage[] warnings;

    /**
     * Messages informing about unsupported codes and features.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(TranslateMessage[] warnings) {
        this.warnings = warnings;
    }

    /**
     * Messages informing about unsupported codes and features.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public TranslateMessage[] getWarnings() {
        return this.warnings;
    }

    /**
     * Messages informing about unsupported codes and features.
     */
    public TranslateResult withWarnings(TranslateMessage[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslateResult() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslateResult(
        String[] commands,
        TranslateMessage[] warnings
    ) {
        this.commands = commands;
        this.warnings = warnings;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslateResult other = (TranslateResult) obj;

        return (
            EqualityUtility.equals(commands, other.commands)
            && EqualityUtility.equals(warnings, other.warnings)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(commands),
            EqualityUtility.generateHashCode(warnings)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslateResult { ");
        sb.append("commands: ");
        sb.append(this.commands);
        sb.append(", ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslateResult fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslateResult.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslateResult> PARSER =
        new zaber.motion.dto.Parser<TranslateResult>() {
            @Override
            public TranslateResult fromByteArray(byte[] data) {
                return TranslateResult.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslateResult> parser() {
        return PARSER;
    }

}
