// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.gcode;

import zaber.motion.Units;
import zaber.motion.ascii.Stream;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents a live G-Code translator.
 * It allows to stream G-Code blocks to a connected device.
 * It requires a stream to be setup on the device.
 * Requires at least Firmware 7.11.
 */
public class Translator {
    private int translatorId;

    /**
     * @return The ID of the translator that serves to identify native resources.
     */
    public int getTranslatorId() {
        return this.translatorId;
    }


    /**
     * @return Current coordinate system.
     */
    public String getCoordinateSystem() {
        return this.getCurrentCoordinateSystem();
    }

    public Translator(
        int translatorId) {
        this.translatorId = translatorId;
    }

    /**
     * Sets up the translator on top of a provided stream.
     * @param stream The stream to setup the translator on.
     * The stream must be already setup in a live or a store mode.
     * @param config Configuration of the translator.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<Translator> setupAsync(
        Stream stream,
        TranslatorConfig config) {
        zaber.motion.requests.TranslatorCreateLiveRequest request =
            new zaber.motion.requests.TranslatorCreateLiveRequest();
        request.setDevice(stream.getDevice().getDeviceAddress());
        request.setInterfaceId(stream.getDevice().getConnection().getInterfaceId());
        request.setStreamId(stream.getStreamId());
        request.setConfig(config);
        CompletableFuture<zaber.motion.requests.TranslatorCreateResponse> response = Call.callAsync(
            "gcode/create_live",
            request,
            zaber.motion.requests.TranslatorCreateResponse.parser());
        return response
            .thenApply(r -> new Translator(r.getTranslatorId()));
    }

    /**
     * Sets up the translator on top of a provided stream.
     * @param stream The stream to setup the translator on.
     * The stream must be already setup in a live or a store mode.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<Translator> setupAsync(
        Stream stream) {
        return setupAsync(stream, null);
    }

    /**
     * Sets up the translator on top of a provided stream.
     * @param stream The stream to setup the translator on.
     * The stream must be already setup in a live or a store mode.
     * @param config Configuration of the translator.
     * @return New instance of translator.
     */
    public static Translator setup(
        Stream stream,
        TranslatorConfig config) {
        try {
            return setupAsync(stream, config).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets up the translator on top of a provided stream.
     * @param stream The stream to setup the translator on.
     * The stream must be already setup in a live or a store mode.
     * @return New instance of translator.
     */
    public static Translator setup(
        Stream stream) {
        return setup(stream, null);
    }

    /**
     * Translates a single block (line) of G-code.
     * The commands are queued in the underlying stream to ensure smooth continues movement.
     * Returning of this method indicates that the commands are queued (not necessarily executed).
     * @param block Block (line) of G-code.
     * @return A CompletableFuture that can be completed to get the result:
     * Result of translation containing the commands sent to the device.
     */
    public CompletableFuture<TranslateResult> translateAsync(
        String block) {
        zaber.motion.requests.TranslatorTranslateRequest request =
            new zaber.motion.requests.TranslatorTranslateRequest();
        request.setTranslatorId(getTranslatorId());
        request.setBlock(block);
        CompletableFuture<TranslateResult> response = Call.callAsync(
            "gcode/translate_live",
            request,
            TranslateResult.parser());
        return response;
    }

    /**
     * Translates a single block (line) of G-code.
     * The commands are queued in the underlying stream to ensure smooth continues movement.
     * Returning of this method indicates that the commands are queued (not necessarily executed).
     * @param block Block (line) of G-code.
     * @return Result of translation containing the commands sent to the device.
     */
    public TranslateResult translate(
        String block) {
        try {
            return translateAsync(block).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
     * The flush is also performed by M2 and M30 codes.
     * @param waitUntilIdle Determines whether to wait for the stream to finish all the movements.
     * @return A CompletableFuture that can be completed to get the result:
     * The remaining stream commands.
     */
    public CompletableFuture<String[]> flushAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.TranslatorFlushLiveRequest request =
            new zaber.motion.requests.TranslatorFlushLiveRequest();
        request.setTranslatorId(getTranslatorId());
        request.setWaitUntilIdle(waitUntilIdle);
        CompletableFuture<zaber.motion.requests.TranslatorFlushResponse> response = Call.callAsync(
            "gcode/flush_live",
            request,
            zaber.motion.requests.TranslatorFlushResponse.parser());
        return response
            .thenApply(r -> r.getCommands());
    }

    /**
     * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
     * The flush is also performed by M2 and M30 codes.
     * @return A CompletableFuture that can be completed to get the result:
     * The remaining stream commands.
     */
    public CompletableFuture<String[]> flushAsync() {
        return flushAsync(true);
    }

    /**
     * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
     * The flush is also performed by M2 and M30 codes.
     * @param waitUntilIdle Determines whether to wait for the stream to finish all the movements.
     * @return The remaining stream commands.
     */
    public String[] flush(
        boolean waitUntilIdle) {
        try {
            return flushAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Flushes the remaining stream commands waiting in optimization buffer into the underlying stream.
     * The flush is also performed by M2 and M30 codes.
     * @return The remaining stream commands.
     */
    public String[] flush() {
        return flush(true);
    }

    /**
     * Resets position of the translator from the underlying stream.
     * Call this method after performing a movement outside of translator.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> resetPositionAsync() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        return Call.callAsync("gcode/reset_position_from_stream", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Resets position of the translator from the underlying stream.
     * Call this method after performing a movement outside of translator.
     */
    public void resetPosition() {
        try {
            resetPositionAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the speed at which the device moves when traversing (G0).
     * @param traverseRate The traverse rate.
     * @param unit Units of the traverse rate.
     */
    public void setTraverseRate(
        double traverseRate,
        Units unit) {
        zaber.motion.requests.TranslatorSetTraverseRateRequest request =
            new zaber.motion.requests.TranslatorSetTraverseRateRequest();
        request.setTranslatorId(getTranslatorId());
        request.setTraverseRate(traverseRate);
        request.setUnit(unit);
        Call.callSync("gcode/set_traverse_rate", request, null);
    }


    /**
     * Sets position of translator's axis.
     * Use this method to set position after performing movement outside of the translator.
     * This method does not cause any movement.
     * @param axis Letter of the axis.
     * @param position The position.
     * @param unit Units of position.
     */
    public void setAxisPosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_position", request, null);
    }


    /**
     * Gets position of translator's axis.
     * This method does not query device but returns value from translator's state.
     * @param axis Letter of the axis.
     * @param unit Units of position.
     * @return Position of translator's axis.
     */
    public double getAxisPosition(
        String axis,
        Units unit) {
        zaber.motion.requests.TranslatorGetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorGetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "gcode/get_axis_position",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Sets the home position of translator's axis.
     * This position is used by G28.
     * @param axis Letter of the axis.
     * @param position The home position.
     * @param unit Units of position.
     */
    public void setAxisHomePosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_home", request, null);
    }


    /**
     * Sets the secondary home position of translator's axis.
     * This position is used by G30.
     * @param axis Letter of the axis.
     * @param position The home position.
     * @param unit Units of position.
     */
    public void setAxisSecondaryHomePosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_secondary_home", request, null);
    }


    /**
     * Gets offset of an axis in a given coordinate system.
     * @param coordinateSystem Coordinate system (e.g. G54).
     * @param axis Letter of the axis.
     * @param unit Units of position.
     * @return Offset in translator units of the axis.
     */
    public double getAxisCoordinateSystemOffset(
        String coordinateSystem,
        String axis,
        Units unit) {
        zaber.motion.requests.TranslatorGetAxisOffsetRequest request =
            new zaber.motion.requests.TranslatorGetAxisOffsetRequest();
        request.setTranslatorId(getTranslatorId());
        request.setCoordinateSystem(coordinateSystem);
        request.setAxis(axis);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "gcode/get_axis_offset",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Resets internal state after device rejected generated command.
     * Axis positions become uninitialized.
     */
    public void resetAfterStreamError() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        Call.callSync("gcode/reset_after_stream_error", request, null);
    }


    /**
     * Allows to scale feed rate of the translated code by a coefficient.
     * @param coefficient Coefficient of the original feed rate.
     */
    public void setFeedRateOverride(
        double coefficient) {
        zaber.motion.requests.TranslatorSetFeedRateOverrideRequest request =
            new zaber.motion.requests.TranslatorSetFeedRateOverrideRequest();
        request.setTranslatorId(getTranslatorId());
        request.setCoefficient(coefficient);
        Call.callSync("gcode/set_feed_rate_override", request, null);
    }


    /**
     * Releases native resources of a translator.
     * @param translatorId The ID of the translator.
     */
    private static void free(
        int translatorId) {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(translatorId);
        Call.callSync("gcode/free", request, null);
    }


    /**
     * Gets current coordinate system (e.g. G54).
     * @return Current coordinate system.
     */
    private String getCurrentCoordinateSystem() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "gcode/get_current_coordinate_system",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }



    /**
    * Finalize is used over Cleaner due compatibility with Java 8.
    */
    @Override
    @SuppressWarnings("deprecation")
    public void finalize() {
        Translator.free(this.translatorId);
    }
}
