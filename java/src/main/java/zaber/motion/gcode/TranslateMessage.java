/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Represents a message from translator regarding a block translation.
 */
public final class TranslateMessage implements zaber.motion.dto.Message {

    private String message;

    /**
     * The message describing an occurrence.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * The message describing an occurrence.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public String getMessage() {
        return this.message;
    }

    /**
     * The message describing an occurrence.
     */
    public TranslateMessage withMessage(String aMessage) {
        this.setMessage(aMessage);
        return this;
    }

    private int fromBlock;

    /**
     * The index in the block string that the message regards to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fromBlock")
    public void setFromBlock(int fromBlock) {
        this.fromBlock = fromBlock;
    }

    /**
     * The index in the block string that the message regards to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fromBlock")
    public int getFromBlock() {
        return this.fromBlock;
    }

    /**
     * The index in the block string that the message regards to.
     */
    public TranslateMessage withFromBlock(int aFromBlock) {
        this.setFromBlock(aFromBlock);
        return this;
    }

    private int toBlock;

    /**
     * The end index in the block string that the message regards to.
     * The end index is exclusive.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("toBlock")
    public void setToBlock(int toBlock) {
        this.toBlock = toBlock;
    }

    /**
     * The end index in the block string that the message regards to.
     * The end index is exclusive.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("toBlock")
    public int getToBlock() {
        return this.toBlock;
    }

    /**
     * The end index in the block string that the message regards to.
     * The end index is exclusive.
     */
    public TranslateMessage withToBlock(int aToBlock) {
        this.setToBlock(aToBlock);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslateMessage() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslateMessage(
        String message,
        int fromBlock,
        int toBlock
    ) {
        this.message = message;
        this.fromBlock = fromBlock;
        this.toBlock = toBlock;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslateMessage other = (TranslateMessage) obj;

        return (
            EqualityUtility.equals(message, other.message)
            && EqualityUtility.equals(fromBlock, other.fromBlock)
            && EqualityUtility.equals(toBlock, other.toBlock)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(message),
            EqualityUtility.generateHashCode(fromBlock),
            EqualityUtility.generateHashCode(toBlock)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslateMessage { ");
        sb.append("message: ");
        sb.append(this.message);
        sb.append(", ");
        sb.append("fromBlock: ");
        sb.append(this.fromBlock);
        sb.append(", ");
        sb.append("toBlock: ");
        sb.append(this.toBlock);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslateMessage fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslateMessage.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslateMessage> PARSER =
        new zaber.motion.dto.Parser<TranslateMessage>() {
            @Override
            public TranslateMessage fromByteArray(byte[] data) {
                return TranslateMessage.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslateMessage> parser() {
        return PARSER;
    }

}
