/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


/**
 * Holds information about device and its axes for purpose of a translator.
 */
public final class DeviceDefinition implements zaber.motion.dto.Message {

    private int deviceId;

    /**
     * Device ID of the controller.
     * Can be obtained from device settings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Device ID of the controller.
     * Can be obtained from device settings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public int getDeviceId() {
        return this.deviceId;
    }

    /**
     * Device ID of the controller.
     * Can be obtained from device settings.
     */
    public DeviceDefinition withDeviceId(int aDeviceId) {
        this.setDeviceId(aDeviceId);
        return this;
    }

    private AxisDefinition[] axes;

    /**
     * Applicable axes of the device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(AxisDefinition[] axes) {
        this.axes = axes;
    }

    /**
     * Applicable axes of the device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public AxisDefinition[] getAxes() {
        return this.axes;
    }

    /**
     * Applicable axes of the device.
     */
    public DeviceDefinition withAxes(AxisDefinition[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private Measurement maxSpeed;

    /**
     * The smallest of each axis' maxspeed setting value.
     * This value becomes the traverse rate of the translator.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("maxSpeed")
    public void setMaxSpeed(Measurement maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    /**
     * The smallest of each axis' maxspeed setting value.
     * This value becomes the traverse rate of the translator.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("maxSpeed")
    public Measurement getMaxSpeed() {
        return this.maxSpeed;
    }

    /**
     * The smallest of each axis' maxspeed setting value.
     * This value becomes the traverse rate of the translator.
     */
    public DeviceDefinition withMaxSpeed(Measurement aMaxSpeed) {
        this.setMaxSpeed(aMaxSpeed);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceDefinition() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceDefinition(
        int deviceId,
        AxisDefinition[] axes,
        Measurement maxSpeed
    ) {
        this.deviceId = deviceId;
        this.axes = axes;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceDefinition other = (DeviceDefinition) obj;

        return (
            EqualityUtility.equals(deviceId, other.deviceId)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(maxSpeed, other.maxSpeed)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceId),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(maxSpeed)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceDefinition { ");
        sb.append("deviceId: ");
        sb.append(this.deviceId);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("maxSpeed: ");
        sb.append(this.maxSpeed);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceDefinition fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceDefinition.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceDefinition> PARSER =
        new zaber.motion.dto.Parser<DeviceDefinition>() {
            @Override
            public DeviceDefinition fromByteArray(byte[] data) {
                return DeviceDefinition.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceDefinition> parser() {
        return PARSER;
    }

}
