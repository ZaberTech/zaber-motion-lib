/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Configuration of a translator.
 */
public final class TranslatorConfig implements zaber.motion.dto.Message {

    private AxisMapping[] axisMappings;

    /**
     * Optional custom mapping of translator axes to stream axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisMappings")
    public void setAxisMappings(AxisMapping[] axisMappings) {
        this.axisMappings = axisMappings;
    }

    /**
     * Optional custom mapping of translator axes to stream axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisMappings")
    public AxisMapping[] getAxisMappings() {
        return this.axisMappings;
    }

    /**
     * Optional custom mapping of translator axes to stream axes.
     */
    public TranslatorConfig withAxisMappings(AxisMapping[] aAxisMappings) {
        this.setAxisMappings(aAxisMappings);
        return this;
    }

    private AxisTransformation[] axisTransformations;

    /**
     * Optional transformation of axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisTransformations")
    public void setAxisTransformations(AxisTransformation[] axisTransformations) {
        this.axisTransformations = axisTransformations;
    }

    /**
     * Optional transformation of axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisTransformations")
    public AxisTransformation[] getAxisTransformations() {
        return this.axisTransformations;
    }

    /**
     * Optional transformation of axes.
     */
    public TranslatorConfig withAxisTransformations(AxisTransformation[] aAxisTransformations) {
        this.setAxisTransformations(aAxisTransformations);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorConfig() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorConfig(
        AxisMapping[] axisMappings,
        AxisTransformation[] axisTransformations
    ) {
        this.axisMappings = axisMappings;
        this.axisTransformations = axisTransformations;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorConfig other = (TranslatorConfig) obj;

        return (
            EqualityUtility.equals(axisMappings, other.axisMappings)
            && EqualityUtility.equals(axisTransformations, other.axisTransformations)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisMappings),
            EqualityUtility.generateHashCode(axisTransformations)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorConfig { ");
        sb.append("axisMappings: ");
        sb.append(this.axisMappings);
        sb.append(", ");
        sb.append("axisTransformations: ");
        sb.append(this.axisTransformations);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorConfig fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorConfig.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorConfig> PARSER =
        new zaber.motion.dto.Parser<TranslatorConfig>() {
            @Override
            public TranslatorConfig fromByteArray(byte[] data) {
                return TranslatorConfig.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorConfig> parser() {
        return PARSER;
    }

}
