/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Defines an axis of the translator.
 */
public final class AxisDefinition implements zaber.motion.dto.Message {

    private int peripheralId;

    /**
     * ID of the peripheral.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public void setPeripheralId(int peripheralId) {
        this.peripheralId = peripheralId;
    }

    /**
     * ID of the peripheral.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public int getPeripheralId() {
        return this.peripheralId;
    }

    /**
     * ID of the peripheral.
     */
    public AxisDefinition withPeripheralId(int aPeripheralId) {
        this.setPeripheralId(aPeripheralId);
        return this;
    }

    private Integer microstepResolution;

    /**
     * Microstep resolution of the axis.
     * Can be obtained by reading the resolution setting.
     * Leave empty if the axis does not have the setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("microstepResolution")
    public void setMicrostepResolution(Integer microstepResolution) {
        this.microstepResolution = microstepResolution;
    }

    /**
     * Microstep resolution of the axis.
     * Can be obtained by reading the resolution setting.
     * Leave empty if the axis does not have the setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("microstepResolution")
    public Integer getMicrostepResolution() {
        return this.microstepResolution;
    }

    /**
     * Microstep resolution of the axis.
     * Can be obtained by reading the resolution setting.
     * Leave empty if the axis does not have the setting.
     */
    public AxisDefinition withMicrostepResolution(Integer aMicrostepResolution) {
        this.setMicrostepResolution(aMicrostepResolution);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisDefinition() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisDefinition(
        int peripheralId,
        Integer microstepResolution
    ) {
        this.peripheralId = peripheralId;
        this.microstepResolution = microstepResolution;
    }

    /**
     * Constructor with only required properties.
     */
    public AxisDefinition(
        int peripheralId
    ) {
        this.peripheralId = peripheralId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisDefinition other = (AxisDefinition) obj;

        return (
            EqualityUtility.equals(peripheralId, other.peripheralId)
            && EqualityUtility.equals(microstepResolution, other.microstepResolution)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(peripheralId),
            EqualityUtility.generateHashCode(microstepResolution)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisDefinition { ");
        sb.append("peripheralId: ");
        sb.append(this.peripheralId);
        sb.append(", ");
        sb.append("microstepResolution: ");
        sb.append(this.microstepResolution);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisDefinition fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisDefinition.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisDefinition> PARSER =
        new zaber.motion.dto.Parser<AxisDefinition>() {
            @Override
            public AxisDefinition fromByteArray(byte[] data) {
                return AxisDefinition.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisDefinition> parser() {
        return PARSER;
    }

}
