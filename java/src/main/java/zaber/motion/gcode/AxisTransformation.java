/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


/**
 * Represents a transformation of a translator axis.
 */
public final class AxisTransformation implements zaber.motion.dto.Message {

    private String axisLetter;

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisLetter")
    public void setAxisLetter(String axisLetter) {
        this.axisLetter = axisLetter;
    }

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisLetter")
    public String getAxisLetter() {
        return this.axisLetter;
    }

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    public AxisTransformation withAxisLetter(String aAxisLetter) {
        this.setAxisLetter(aAxisLetter);
        return this;
    }

    private Double scaling;

    /**
     * Scaling factor.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("scaling")
    public void setScaling(Double scaling) {
        this.scaling = scaling;
    }

    /**
     * Scaling factor.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("scaling")
    public Double getScaling() {
        return this.scaling;
    }

    /**
     * Scaling factor.
     */
    public AxisTransformation withScaling(Double aScaling) {
        this.setScaling(aScaling);
        return this;
    }

    private Measurement translation;

    /**
     * Translation distance.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("translation")
    public void setTranslation(Measurement translation) {
        this.translation = translation;
    }

    /**
     * Translation distance.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("translation")
    public Measurement getTranslation() {
        return this.translation;
    }

    /**
     * Translation distance.
     */
    public AxisTransformation withTranslation(Measurement aTranslation) {
        this.setTranslation(aTranslation);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisTransformation() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisTransformation(
        String axisLetter,
        Double scaling,
        Measurement translation
    ) {
        this.axisLetter = axisLetter;
        this.scaling = scaling;
        this.translation = translation;
    }

    /**
     * Constructor with only required properties.
     */
    public AxisTransformation(
        String axisLetter
    ) {
        this.axisLetter = axisLetter;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisTransformation other = (AxisTransformation) obj;

        return (
            EqualityUtility.equals(axisLetter, other.axisLetter)
            && EqualityUtility.equals(scaling, other.scaling)
            && EqualityUtility.equals(translation, other.translation)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisLetter),
            EqualityUtility.generateHashCode(scaling),
            EqualityUtility.generateHashCode(translation)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisTransformation { ");
        sb.append("axisLetter: ");
        sb.append(this.axisLetter);
        sb.append(", ");
        sb.append("scaling: ");
        sb.append(this.scaling);
        sb.append(", ");
        sb.append("translation: ");
        sb.append(this.translation);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisTransformation fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisTransformation.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisTransformation> PARSER =
        new zaber.motion.dto.Parser<AxisTransformation>() {
            @Override
            public AxisTransformation fromByteArray(byte[] data) {
                return AxisTransformation.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisTransformation> parser() {
        return PARSER;
    }

}
