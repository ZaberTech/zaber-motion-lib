/* This file is generated. Do not modify by hand. */

package zaber.motion.gcode;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Maps a translator axis to a Zaber stream axis.
 */
public final class AxisMapping implements zaber.motion.dto.Message {

    private String axisLetter;

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisLetter")
    public void setAxisLetter(String axisLetter) {
        this.axisLetter = axisLetter;
    }

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisLetter")
    public String getAxisLetter() {
        return this.axisLetter;
    }

    /**
     * Letter of the translator axis (X,Y,Z,A,B,C,E).
     */
    public AxisMapping withAxisLetter(String aAxisLetter) {
        this.setAxisLetter(aAxisLetter);
        return this;
    }

    private int axisIndex;

    /**
     * Index of the stream axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisIndex")
    public void setAxisIndex(int axisIndex) {
        this.axisIndex = axisIndex;
    }

    /**
     * Index of the stream axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisIndex")
    public int getAxisIndex() {
        return this.axisIndex;
    }

    /**
     * Index of the stream axis.
     */
    public AxisMapping withAxisIndex(int aAxisIndex) {
        this.setAxisIndex(aAxisIndex);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisMapping() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisMapping(
        String axisLetter,
        int axisIndex
    ) {
        this.axisLetter = axisLetter;
        this.axisIndex = axisIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisMapping other = (AxisMapping) obj;

        return (
            EqualityUtility.equals(axisLetter, other.axisLetter)
            && EqualityUtility.equals(axisIndex, other.axisIndex)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisLetter),
            EqualityUtility.generateHashCode(axisIndex)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisMapping { ");
        sb.append("axisLetter: ");
        sb.append(this.axisLetter);
        sb.append(", ");
        sb.append("axisIndex: ");
        sb.append(this.axisIndex);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisMapping fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisMapping.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisMapping> PARSER =
        new zaber.motion.dto.Parser<AxisMapping>() {
            @Override
            public AxisMapping fromByteArray(byte[] data) {
                return AxisMapping.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisMapping> parser() {
        return PARSER;
    }

}
