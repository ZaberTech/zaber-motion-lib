// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.gcode;

import zaber.motion.Units;
import zaber.motion.ascii.Device;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents an offline G-Code translator.
 * It allows to translate G-Code blocks to Zaber ASCII protocol stream commands.
 * This translator does not need a connected device to perform translation.
 * Requires at least Firmware 7.11.
 */
public class OfflineTranslator {
    private int translatorId;

    /**
     * @return The ID of the translator that serves to identify native resources.
     */
    public int getTranslatorId() {
        return this.translatorId;
    }


    /**
     * @return Current coordinate system.
     */
    public String getCoordinateSystem() {
        return this.getCurrentCoordinateSystem();
    }

    public OfflineTranslator(
        int translatorId) {
        this.translatorId = translatorId;
    }

    /**
     * Sets up translator from provided device definition and configuration.
     * @param definition Definition of device and its peripherals.
     * The definition must match a device that later performs the commands.
     * @param config Configuration of the translator.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<OfflineTranslator> setupAsync(
        DeviceDefinition definition,
        TranslatorConfig config) {
        zaber.motion.requests.TranslatorCreateRequest request =
            new zaber.motion.requests.TranslatorCreateRequest();
        request.setDefinition(definition);
        request.setConfig(config);
        CompletableFuture<zaber.motion.requests.TranslatorCreateResponse> response = Call.callAsync(
            "gcode/create",
            request,
            zaber.motion.requests.TranslatorCreateResponse.parser());
        return response
            .thenApply(r -> new OfflineTranslator(r.getTranslatorId()));
    }

    /**
     * Sets up translator from provided device definition and configuration.
     * @param definition Definition of device and its peripherals.
     * The definition must match a device that later performs the commands.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<OfflineTranslator> setupAsync(
        DeviceDefinition definition) {
        return setupAsync(definition, null);
    }

    /**
     * Sets up translator from provided device definition and configuration.
     * @param definition Definition of device and its peripherals.
     * The definition must match a device that later performs the commands.
     * @param config Configuration of the translator.
     * @return New instance of translator.
     */
    public static OfflineTranslator setup(
        DeviceDefinition definition,
        TranslatorConfig config) {
        try {
            return setupAsync(definition, config).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets up translator from provided device definition and configuration.
     * @param definition Definition of device and its peripherals.
     * The definition must match a device that later performs the commands.
     * @return New instance of translator.
     */
    public static OfflineTranslator setup(
        DeviceDefinition definition) {
        return setup(definition, null);
    }

    /**
     * Sets up an offline translator from provided device, axes, and configuration.
     * @param device Device that later performs the command streaming.
     * @param axes Axis numbers that are later used to setup the stream.
     * For a lockstep group specify only the first axis of the group.
     * @param config Configuration of the translator.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<OfflineTranslator> setupFromDeviceAsync(
        Device device,
        int[] axes,
        TranslatorConfig config) {
        zaber.motion.requests.TranslatorCreateFromDeviceRequest request =
            new zaber.motion.requests.TranslatorCreateFromDeviceRequest();
        request.setInterfaceId(device.getConnection().getInterfaceId());
        request.setDevice(device.getDeviceAddress());
        request.setAxes(axes);
        request.setConfig(config);
        CompletableFuture<zaber.motion.requests.TranslatorCreateResponse> response = Call.callAsync(
            "gcode/create_from_device",
            request,
            zaber.motion.requests.TranslatorCreateResponse.parser());
        return response
            .thenApply(r -> new OfflineTranslator(r.getTranslatorId()));
    }

    /**
     * Sets up an offline translator from provided device, axes, and configuration.
     * @param device Device that later performs the command streaming.
     * @param axes Axis numbers that are later used to setup the stream.
     * For a lockstep group specify only the first axis of the group.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of translator.
     */
    public static CompletableFuture<OfflineTranslator> setupFromDeviceAsync(
        Device device,
        int[] axes) {
        return setupFromDeviceAsync(device, axes, null);
    }

    /**
     * Sets up an offline translator from provided device, axes, and configuration.
     * @param device Device that later performs the command streaming.
     * @param axes Axis numbers that are later used to setup the stream.
     * For a lockstep group specify only the first axis of the group.
     * @param config Configuration of the translator.
     * @return New instance of translator.
     */
    public static OfflineTranslator setupFromDevice(
        Device device,
        int[] axes,
        TranslatorConfig config) {
        try {
            return setupFromDeviceAsync(device, axes, config).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets up an offline translator from provided device, axes, and configuration.
     * @param device Device that later performs the command streaming.
     * @param axes Axis numbers that are later used to setup the stream.
     * For a lockstep group specify only the first axis of the group.
     * @return New instance of translator.
     */
    public static OfflineTranslator setupFromDevice(
        Device device,
        int[] axes) {
        return setupFromDevice(device, axes, null);
    }

    /**
     * Translates a single block (line) of G-code.
     * @param block Block (line) of G-code.
     * @return Result of translation containing the stream commands.
     */
    public TranslateResult translate(
        String block) {
        zaber.motion.requests.TranslatorTranslateRequest request =
            new zaber.motion.requests.TranslatorTranslateRequest();
        request.setTranslatorId(getTranslatorId());
        request.setBlock(block);
        TranslateResult response = Call.callSync(
            "gcode/translate",
            request,
            TranslateResult.parser());
        return response;
    }


    /**
     * Flushes the remaining stream commands waiting in optimization buffer.
     * The flush is also performed by M2 and M30 codes.
     * @return The remaining stream commands.
     */
    public String[] flush() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        zaber.motion.requests.TranslatorFlushResponse response = Call.callSync(
            "gcode/flush",
            request,
            zaber.motion.requests.TranslatorFlushResponse.parser());
        return response.getCommands();
    }


    /**
     * Sets the speed at which the device moves when traversing (G0).
     * @param traverseRate The traverse rate.
     * @param unit Units of the traverse rate.
     */
    public void setTraverseRate(
        double traverseRate,
        Units unit) {
        zaber.motion.requests.TranslatorSetTraverseRateRequest request =
            new zaber.motion.requests.TranslatorSetTraverseRateRequest();
        request.setTranslatorId(getTranslatorId());
        request.setTraverseRate(traverseRate);
        request.setUnit(unit);
        Call.callSync("gcode/set_traverse_rate", request, null);
    }


    /**
     * Sets position of translator's axis.
     * Use this method to set position after performing movement outside of the translator.
     * This method does not cause any movement.
     * @param axis Letter of the axis.
     * @param position The position.
     * @param unit Units of position.
     */
    public void setAxisPosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_position", request, null);
    }


    /**
     * Gets position of translator's axis.
     * This method does not query device but returns value from translator's state.
     * @param axis Letter of the axis.
     * @param unit Units of position.
     * @return Position of translator's axis.
     */
    public double getAxisPosition(
        String axis,
        Units unit) {
        zaber.motion.requests.TranslatorGetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorGetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "gcode/get_axis_position",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Sets the home position of translator's axis.
     * This position is used by G28.
     * @param axis Letter of the axis.
     * @param position The home position.
     * @param unit Units of position.
     */
    public void setAxisHomePosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_home", request, null);
    }


    /**
     * Sets the secondary home position of translator's axis.
     * This position is used by G30.
     * @param axis Letter of the axis.
     * @param position The home position.
     * @param unit Units of position.
     */
    public void setAxisSecondaryHomePosition(
        String axis,
        double position,
        Units unit) {
        zaber.motion.requests.TranslatorSetAxisPositionRequest request =
            new zaber.motion.requests.TranslatorSetAxisPositionRequest();
        request.setTranslatorId(getTranslatorId());
        request.setAxis(axis);
        request.setPosition(position);
        request.setUnit(unit);
        Call.callSync("gcode/set_axis_secondary_home", request, null);
    }


    /**
     * Gets offset of an axis in a given coordinate system.
     * @param coordinateSystem Coordinate system (e.g. G54).
     * @param axis Letter of the axis.
     * @param unit Units of position.
     * @return Offset in translator units of the axis.
     */
    public double getAxisCoordinateSystemOffset(
        String coordinateSystem,
        String axis,
        Units unit) {
        zaber.motion.requests.TranslatorGetAxisOffsetRequest request =
            new zaber.motion.requests.TranslatorGetAxisOffsetRequest();
        request.setTranslatorId(getTranslatorId());
        request.setCoordinateSystem(coordinateSystem);
        request.setAxis(axis);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "gcode/get_axis_offset",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Resets internal state after device rejected generated command.
     * Axis positions become uninitialized.
     */
    public void resetAfterStreamError() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        Call.callSync("gcode/reset_after_stream_error", request, null);
    }


    /**
     * Allows to scale feed rate of the translated code by a coefficient.
     * @param coefficient Coefficient of the original feed rate.
     */
    public void setFeedRateOverride(
        double coefficient) {
        zaber.motion.requests.TranslatorSetFeedRateOverrideRequest request =
            new zaber.motion.requests.TranslatorSetFeedRateOverrideRequest();
        request.setTranslatorId(getTranslatorId());
        request.setCoefficient(coefficient);
        Call.callSync("gcode/set_feed_rate_override", request, null);
    }


    /**
     * Releases native resources of a translator.
     * @param translatorId The ID of the translator.
     */
    private static void free(
        int translatorId) {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(translatorId);
        Call.callSync("gcode/free", request, null);
    }


    /**
     * Gets current coordinate system (e.g. G54).
     * @return Current coordinate system.
     */
    private String getCurrentCoordinateSystem() {
        zaber.motion.requests.TranslatorEmptyRequest request =
            new zaber.motion.requests.TranslatorEmptyRequest();
        request.setTranslatorId(getTranslatorId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "gcode/get_current_coordinate_system",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }



    /**
    * Finalize is used over Cleaner due compatibility with Java 8.
    */
    @Override
    @SuppressWarnings("deprecation")
    public void finalize() {
        OfflineTranslator.free(this.translatorId);
    }
}
