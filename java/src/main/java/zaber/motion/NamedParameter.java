/* This file is generated. Do not modify by hand. */

package zaber.motion;

import java.util.Objects;

import zaber.motion.dto.Mapper;

/**
 * Named parameter with optional value.
 */
public final class NamedParameter implements zaber.motion.dto.Message {

    private String name;

    /**
     * Name of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * Name of the parameter.
     */
    public NamedParameter withName(String aName) {
        this.setName(aName);
        return this;
    }

    private Double value;

    /**
     * Optional value of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * Optional value of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public Double getValue() {
        return this.value;
    }

    /**
     * Optional value of the parameter.
     */
    public NamedParameter withValue(Double aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public NamedParameter() {
    }

    /**
     * Constructor with all properties.
     */
    public NamedParameter(
        String name,
        Double value
    ) {
        this.name = name;
        this.value = value;
    }

    /**
     * Constructor with only required properties.
     */
    public NamedParameter(
        String name
    ) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        NamedParameter other = (NamedParameter) obj;

        return (
            EqualityUtility.equals(name, other.name)
            && EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(name),
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NamedParameter { ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static NamedParameter fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, NamedParameter.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<NamedParameter> PARSER =
        new zaber.motion.dto.Parser<NamedParameter>() {
            @Override
            public NamedParameter fromByteArray(byte[] data) {
                return NamedParameter.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<NamedParameter> parser() {
        return PARSER;
    }

}
