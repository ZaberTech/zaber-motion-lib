/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.ascii.ServoTuningParam;
import zaber.motion.EqualityUtility;


public final class SetSimpleTuning implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetSimpleTuning withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetSimpleTuning withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public SetSimpleTuning withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ServoTuningParamset paramset;

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public void setParamset(ServoTuningParamset paramset) {
        this.paramset = paramset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public ServoTuningParamset getParamset() {
        return this.paramset;
    }

    public SetSimpleTuning withParamset(ServoTuningParamset aParamset) {
        this.setParamset(aParamset);
        return this;
    }

    private Double carriageMass;

    @com.fasterxml.jackson.annotation.JsonProperty("carriageMass")
    public void setCarriageMass(Double carriageMass) {
        this.carriageMass = carriageMass;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("carriageMass")
    public Double getCarriageMass() {
        return this.carriageMass;
    }

    public SetSimpleTuning withCarriageMass(Double aCarriageMass) {
        this.setCarriageMass(aCarriageMass);
        return this;
    }

    private double loadMass;

    @com.fasterxml.jackson.annotation.JsonProperty("loadMass")
    public void setLoadMass(double loadMass) {
        this.loadMass = loadMass;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("loadMass")
    public double getLoadMass() {
        return this.loadMass;
    }

    public SetSimpleTuning withLoadMass(double aLoadMass) {
        this.setLoadMass(aLoadMass);
        return this;
    }

    private ServoTuningParam[] tuningParams;

    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public void setTuningParams(ServoTuningParam[] tuningParams) {
        this.tuningParams = tuningParams;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public ServoTuningParam[] getTuningParams() {
        return this.tuningParams;
    }

    public SetSimpleTuning withTuningParams(ServoTuningParam[] aTuningParams) {
        this.setTuningParams(aTuningParams);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetSimpleTuning() {
    }

    /**
     * Constructor with all properties.
     */
    public SetSimpleTuning(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset paramset,
        double loadMass,
        ServoTuningParam[] tuningParams,
        Double carriageMass
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.paramset = paramset;
        this.loadMass = loadMass;
        this.tuningParams = tuningParams;
        this.carriageMass = carriageMass;
    }

    /**
     * Constructor with only required properties.
     */
    public SetSimpleTuning(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset paramset,
        double loadMass,
        ServoTuningParam[] tuningParams
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.paramset = paramset;
        this.loadMass = loadMass;
        this.tuningParams = tuningParams;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetSimpleTuning other = (SetSimpleTuning) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(paramset, other.paramset)
            && EqualityUtility.equals(carriageMass, other.carriageMass)
            && EqualityUtility.equals(loadMass, other.loadMass)
            && EqualityUtility.equals(tuningParams, other.tuningParams)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(paramset),
            EqualityUtility.generateHashCode(carriageMass),
            EqualityUtility.generateHashCode(loadMass),
            EqualityUtility.generateHashCode(tuningParams)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetSimpleTuning { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("paramset: ");
        sb.append(this.paramset);
        sb.append(", ");
        sb.append("carriageMass: ");
        sb.append(this.carriageMass);
        sb.append(", ");
        sb.append("loadMass: ");
        sb.append(this.loadMass);
        sb.append(", ");
        sb.append("tuningParams: ");
        sb.append(this.tuningParams);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetSimpleTuning fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetSimpleTuning.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetSimpleTuning> PARSER =
        new zaber.motion.dto.Parser<SetSimpleTuning>() {
            @Override
            public SetSimpleTuning fromByteArray(byte[] data) {
                return SetSimpleTuning.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetSimpleTuning> parser() {
        return PARSER;
    }

}
