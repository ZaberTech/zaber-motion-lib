/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TranslatorTranslateRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorTranslateRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    private String block;

    @com.fasterxml.jackson.annotation.JsonProperty("block")
    public void setBlock(String block) {
        this.block = block;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("block")
    public String getBlock() {
        return this.block;
    }

    public TranslatorTranslateRequest withBlock(String aBlock) {
        this.setBlock(aBlock);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorTranslateRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorTranslateRequest(
        int translatorId,
        String block
    ) {
        this.translatorId = translatorId;
        this.block = block;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorTranslateRequest other = (TranslatorTranslateRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            && EqualityUtility.equals(block, other.block)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId),
            EqualityUtility.generateHashCode(block)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorTranslateRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(", ");
        sb.append("block: ");
        sb.append(this.block);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorTranslateRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorTranslateRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorTranslateRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorTranslateRequest>() {
            @Override
            public TranslatorTranslateRequest fromByteArray(byte[] data) {
                return TranslatorTranslateRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorTranslateRequest> parser() {
        return PARSER;
    }

}
