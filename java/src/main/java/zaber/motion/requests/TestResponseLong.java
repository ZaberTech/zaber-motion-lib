/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TestResponseLong implements zaber.motion.dto.Message {

    private String[] dataPong;

    @com.fasterxml.jackson.annotation.JsonProperty("dataPong")
    public void setDataPong(String[] dataPong) {
        this.dataPong = dataPong;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataPong")
    public String[] getDataPong() {
        return this.dataPong;
    }

    public TestResponseLong withDataPong(String[] aDataPong) {
        this.setDataPong(aDataPong);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TestResponseLong() {
    }

    /**
     * Constructor with all properties.
     */
    public TestResponseLong(
        String[] dataPong
    ) {
        this.dataPong = dataPong;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TestResponseLong other = (TestResponseLong) obj;

        return (
            EqualityUtility.equals(dataPong, other.dataPong)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataPong)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TestResponseLong { ");
        sb.append("dataPong: ");
        sb.append(this.dataPong);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TestResponseLong fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TestResponseLong.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TestResponseLong> PARSER =
        new zaber.motion.dto.Parser<TestResponseLong>() {
            @Override
            public TestResponseLong fromByteArray(byte[] data) {
                return TestResponseLong.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TestResponseLong> parser() {
        return PARSER;
    }

}
