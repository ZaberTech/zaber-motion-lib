/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class WdiGenericRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public WdiGenericRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int registerId;

    @com.fasterxml.jackson.annotation.JsonProperty("registerId")
    public void setRegisterId(int registerId) {
        this.registerId = registerId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("registerId")
    public int getRegisterId() {
        return this.registerId;
    }

    public WdiGenericRequest withRegisterId(int aRegisterId) {
        this.setRegisterId(aRegisterId);
        return this;
    }

    private int size;

    @com.fasterxml.jackson.annotation.JsonProperty("size")
    public void setSize(int size) {
        this.size = size;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("size")
    public int getSize() {
        return this.size;
    }

    public WdiGenericRequest withSize(int aSize) {
        this.setSize(aSize);
        return this;
    }

    private int count;

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public void setCount(int count) {
        this.count = count;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public int getCount() {
        return this.count;
    }

    public WdiGenericRequest withCount(int aCount) {
        this.setCount(aCount);
        return this;
    }

    private int offset;

    @com.fasterxml.jackson.annotation.JsonProperty("offset")
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("offset")
    public int getOffset() {
        return this.offset;
    }

    public WdiGenericRequest withOffset(int aOffset) {
        this.setOffset(aOffset);
        return this;
    }

    private String registerBank;

    @com.fasterxml.jackson.annotation.JsonProperty("registerBank")
    public void setRegisterBank(String registerBank) {
        this.registerBank = registerBank;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("registerBank")
    public String getRegisterBank() {
        return this.registerBank;
    }

    public WdiGenericRequest withRegisterBank(String aRegisterBank) {
        this.setRegisterBank(aRegisterBank);
        return this;
    }

    private int[] data;

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(int[] data) {
        this.data = data;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public int[] getData() {
        return this.data;
    }

    public WdiGenericRequest withData(int[] aData) {
        this.setData(aData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public WdiGenericRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public WdiGenericRequest(
        int interfaceId,
        int registerId,
        int size,
        int count,
        int offset,
        String registerBank,
        int[] data
    ) {
        this.interfaceId = interfaceId;
        this.registerId = registerId;
        this.size = size;
        this.count = count;
        this.offset = offset;
        this.registerBank = registerBank;
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WdiGenericRequest other = (WdiGenericRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(registerId, other.registerId)
            && EqualityUtility.equals(size, other.size)
            && EqualityUtility.equals(count, other.count)
            && EqualityUtility.equals(offset, other.offset)
            && EqualityUtility.equals(registerBank, other.registerBank)
            && EqualityUtility.equals(data, other.data)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(registerId),
            EqualityUtility.generateHashCode(size),
            EqualityUtility.generateHashCode(count),
            EqualityUtility.generateHashCode(offset),
            EqualityUtility.generateHashCode(registerBank),
            EqualityUtility.generateHashCode(data)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WdiGenericRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("registerId: ");
        sb.append(this.registerId);
        sb.append(", ");
        sb.append("size: ");
        sb.append(this.size);
        sb.append(", ");
        sb.append("count: ");
        sb.append(this.count);
        sb.append(", ");
        sb.append("offset: ");
        sb.append(this.offset);
        sb.append(", ");
        sb.append("registerBank: ");
        sb.append(this.registerBank);
        sb.append(", ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static WdiGenericRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, WdiGenericRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<WdiGenericRequest> PARSER =
        new zaber.motion.dto.Parser<WdiGenericRequest>() {
            @Override
            public WdiGenericRequest fromByteArray(byte[] data) {
                return WdiGenericRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<WdiGenericRequest> parser() {
        return PARSER;
    }

}
