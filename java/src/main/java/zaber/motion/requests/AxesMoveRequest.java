/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class AxesMoveRequest implements zaber.motion.dto.Message {

    private int[] interfaces;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public void setInterfaces(int[] interfaces) {
        this.interfaces = interfaces;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public int[] getInterfaces() {
        return this.interfaces;
    }

    public AxesMoveRequest withInterfaces(int[] aInterfaces) {
        this.setInterfaces(aInterfaces);
        return this;
    }

    private int[] devices;

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public void setDevices(int[] devices) {
        this.devices = devices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public int[] getDevices() {
        return this.devices;
    }

    public AxesMoveRequest withDevices(int[] aDevices) {
        this.setDevices(aDevices);
        return this;
    }

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public AxesMoveRequest withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private Measurement[] position;

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public void setPosition(Measurement[] position) {
        this.position = position;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public Measurement[] getPosition() {
        return this.position;
    }

    public AxesMoveRequest withPosition(Measurement[] aPosition) {
        this.setPosition(aPosition);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxesMoveRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AxesMoveRequest(
        int[] interfaces,
        int[] devices,
        int[] axes,
        Measurement[] position
    ) {
        this.interfaces = interfaces;
        this.devices = devices;
        this.axes = axes;
        this.position = position;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxesMoveRequest other = (AxesMoveRequest) obj;

        return (
            EqualityUtility.equals(interfaces, other.interfaces)
            && EqualityUtility.equals(devices, other.devices)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(position, other.position)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaces),
            EqualityUtility.generateHashCode(devices),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(position)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxesMoveRequest { ");
        sb.append("interfaces: ");
        sb.append(this.interfaces);
        sb.append(", ");
        sb.append("devices: ");
        sb.append(this.devices);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("position: ");
        sb.append(this.position);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxesMoveRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxesMoveRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxesMoveRequest> PARSER =
        new zaber.motion.dto.Parser<AxesMoveRequest>() {
            @Override
            public AxesMoveRequest fromByteArray(byte[] data) {
                return AxesMoveRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxesMoveRequest> parser() {
        return PARSER;
    }

}
