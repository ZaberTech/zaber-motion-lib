/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StringResponse implements zaber.motion.dto.Message {

    private String value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public String getValue() {
        return this.value;
    }

    public StringResponse withValue(String aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StringResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public StringResponse(
        String value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StringResponse other = (StringResponse) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StringResponse { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StringResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StringResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StringResponse> PARSER =
        new zaber.motion.dto.Parser<StringResponse>() {
            @Override
            public StringResponse fromByteArray(byte[] data) {
                return StringResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StringResponse> parser() {
        return PARSER;
    }

}
