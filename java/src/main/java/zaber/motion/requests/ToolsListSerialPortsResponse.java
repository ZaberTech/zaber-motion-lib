/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ToolsListSerialPortsResponse implements zaber.motion.dto.Message {

    private String[] ports;

    @com.fasterxml.jackson.annotation.JsonProperty("ports")
    public void setPorts(String[] ports) {
        this.ports = ports;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("ports")
    public String[] getPorts() {
        return this.ports;
    }

    public ToolsListSerialPortsResponse withPorts(String[] aPorts) {
        this.setPorts(aPorts);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ToolsListSerialPortsResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public ToolsListSerialPortsResponse(
        String[] ports
    ) {
        this.ports = ports;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ToolsListSerialPortsResponse other = (ToolsListSerialPortsResponse) obj;

        return (
            EqualityUtility.equals(ports, other.ports)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(ports)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ToolsListSerialPortsResponse { ");
        sb.append("ports: ");
        sb.append(this.ports);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ToolsListSerialPortsResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ToolsListSerialPortsResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ToolsListSerialPortsResponse> PARSER =
        new zaber.motion.dto.Parser<ToolsListSerialPortsResponse>() {
            @Override
            public ToolsListSerialPortsResponse fromByteArray(byte[] data) {
                return ToolsListSerialPortsResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ToolsListSerialPortsResponse> parser() {
        return PARSER;
    }

}
