/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class LockstepMoveRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LockstepMoveRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LockstepMoveRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int lockstepGroupId;

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public void setLockstepGroupId(int lockstepGroupId) {
        this.lockstepGroupId = lockstepGroupId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public LockstepMoveRequest withLockstepGroupId(int aLockstepGroupId) {
        this.setLockstepGroupId(aLockstepGroupId);
        return this;
    }

    private boolean waitUntilIdle;

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public void setWaitUntilIdle(boolean waitUntilIdle) {
        this.waitUntilIdle = waitUntilIdle;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public boolean getWaitUntilIdle() {
        return this.waitUntilIdle;
    }

    public LockstepMoveRequest withWaitUntilIdle(boolean aWaitUntilIdle) {
        this.setWaitUntilIdle(aWaitUntilIdle);
        return this;
    }

    private AxisMoveType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(AxisMoveType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public AxisMoveType getType() {
        return this.type;
    }

    public LockstepMoveRequest withType(AxisMoveType aType) {
        this.setType(aType);
        return this;
    }

    private double arg;

    @com.fasterxml.jackson.annotation.JsonProperty("arg")
    public void setArg(double arg) {
        this.arg = arg;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("arg")
    public double getArg() {
        return this.arg;
    }

    public LockstepMoveRequest withArg(double aArg) {
        this.setArg(aArg);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public LockstepMoveRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private double velocity;

    @com.fasterxml.jackson.annotation.JsonProperty("velocity")
    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("velocity")
    public double getVelocity() {
        return this.velocity;
    }

    public LockstepMoveRequest withVelocity(double aVelocity) {
        this.setVelocity(aVelocity);
        return this;
    }

    private Units velocityUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("velocityUnit")
    public void setVelocityUnit(Units velocityUnit) {
        this.velocityUnit = velocityUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("velocityUnit")
    public Units getVelocityUnit() {
        return this.velocityUnit;
    }

    public LockstepMoveRequest withVelocityUnit(Units aVelocityUnit) {
        this.setVelocityUnit(aVelocityUnit);
        return this;
    }

    private double acceleration;

    @com.fasterxml.jackson.annotation.JsonProperty("acceleration")
    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("acceleration")
    public double getAcceleration() {
        return this.acceleration;
    }

    public LockstepMoveRequest withAcceleration(double aAcceleration) {
        this.setAcceleration(aAcceleration);
        return this;
    }

    private Units accelerationUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("accelerationUnit")
    public void setAccelerationUnit(Units accelerationUnit) {
        this.accelerationUnit = accelerationUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("accelerationUnit")
    public Units getAccelerationUnit() {
        return this.accelerationUnit;
    }

    public LockstepMoveRequest withAccelerationUnit(Units aAccelerationUnit) {
        this.setAccelerationUnit(aAccelerationUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepMoveRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepMoveRequest(
        int interfaceId,
        int device,
        int lockstepGroupId,
        boolean waitUntilIdle,
        AxisMoveType type,
        double arg,
        Units unit,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
        this.waitUntilIdle = waitUntilIdle;
        this.type = type;
        this.arg = arg;
        this.unit = unit;
        this.velocity = velocity;
        this.velocityUnit = velocityUnit;
        this.acceleration = acceleration;
        this.accelerationUnit = accelerationUnit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepMoveRequest other = (LockstepMoveRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(lockstepGroupId, other.lockstepGroupId)
            && EqualityUtility.equals(waitUntilIdle, other.waitUntilIdle)
            && EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(arg, other.arg)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(velocity, other.velocity)
            && EqualityUtility.equals(velocityUnit, other.velocityUnit)
            && EqualityUtility.equals(acceleration, other.acceleration)
            && EqualityUtility.equals(accelerationUnit, other.accelerationUnit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(lockstepGroupId),
            EqualityUtility.generateHashCode(waitUntilIdle),
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(arg),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(velocity),
            EqualityUtility.generateHashCode(velocityUnit),
            EqualityUtility.generateHashCode(acceleration),
            EqualityUtility.generateHashCode(accelerationUnit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepMoveRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("lockstepGroupId: ");
        sb.append(this.lockstepGroupId);
        sb.append(", ");
        sb.append("waitUntilIdle: ");
        sb.append(this.waitUntilIdle);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("arg: ");
        sb.append(this.arg);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("velocity: ");
        sb.append(this.velocity);
        sb.append(", ");
        sb.append("velocityUnit: ");
        sb.append(this.velocityUnit);
        sb.append(", ");
        sb.append("acceleration: ");
        sb.append(this.acceleration);
        sb.append(", ");
        sb.append("accelerationUnit: ");
        sb.append(this.accelerationUnit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepMoveRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepMoveRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepMoveRequest> PARSER =
        new zaber.motion.dto.Parser<LockstepMoveRequest>() {
            @Override
            public LockstepMoveRequest fromByteArray(byte[] data) {
                return LockstepMoveRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepMoveRequest> parser() {
        return PARSER;
    }

}
