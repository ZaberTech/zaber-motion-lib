/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class StreamSetAllAnalogOutputsScheduleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private double[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    private double[] futureValues;

    @com.fasterxml.jackson.annotation.JsonProperty("futureValues")
    public void setFutureValues(double[] futureValues) {
        this.futureValues = futureValues;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("futureValues")
    public double[] getFutureValues() {
        return this.futureValues;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withFutureValues(double[] aFutureValues) {
        this.setFutureValues(aFutureValues);
        return this;
    }

    private double delay;

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public void setDelay(double delay) {
        this.delay = delay;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public double getDelay() {
        return this.delay;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withDelay(double aDelay) {
        this.setDelay(aDelay);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public StreamSetAllAnalogOutputsScheduleRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetAllAnalogOutputsScheduleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetAllAnalogOutputsScheduleRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        double[] values,
        double[] futureValues,
        double delay,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.values = values;
        this.futureValues = futureValues;
        this.delay = delay;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetAllAnalogOutputsScheduleRequest other = (StreamSetAllAnalogOutputsScheduleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(values, other.values)
            && EqualityUtility.equals(futureValues, other.futureValues)
            && EqualityUtility.equals(delay, other.delay)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(values),
            EqualityUtility.generateHashCode(futureValues),
            EqualityUtility.generateHashCode(delay),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetAllAnalogOutputsScheduleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(", ");
        sb.append("futureValues: ");
        sb.append(this.futureValues);
        sb.append(", ");
        sb.append("delay: ");
        sb.append(this.delay);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetAllAnalogOutputsScheduleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetAllAnalogOutputsScheduleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetAllAnalogOutputsScheduleRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetAllAnalogOutputsScheduleRequest>() {
            @Override
            public StreamSetAllAnalogOutputsScheduleRequest fromByteArray(byte[] data) {
                return StreamSetAllAnalogOutputsScheduleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetAllAnalogOutputsScheduleRequest> parser() {
        return PARSER;
    }

}
