/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ToggleDeviceDbStoreRequest implements zaber.motion.dto.Message {

    private boolean toggleOn;

    @com.fasterxml.jackson.annotation.JsonProperty("toggleOn")
    public void setToggleOn(boolean toggleOn) {
        this.toggleOn = toggleOn;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("toggleOn")
    public boolean getToggleOn() {
        return this.toggleOn;
    }

    public ToggleDeviceDbStoreRequest withToggleOn(boolean aToggleOn) {
        this.setToggleOn(aToggleOn);
        return this;
    }

    private String storeLocation;

    @com.fasterxml.jackson.annotation.JsonProperty("storeLocation")
    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("storeLocation")
    public String getStoreLocation() {
        return this.storeLocation;
    }

    public ToggleDeviceDbStoreRequest withStoreLocation(String aStoreLocation) {
        this.setStoreLocation(aStoreLocation);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ToggleDeviceDbStoreRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ToggleDeviceDbStoreRequest(
        boolean toggleOn,
        String storeLocation
    ) {
        this.toggleOn = toggleOn;
        this.storeLocation = storeLocation;
    }

    /**
     * Constructor with only required properties.
     */
    public ToggleDeviceDbStoreRequest(
        boolean toggleOn
    ) {
        this.toggleOn = toggleOn;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ToggleDeviceDbStoreRequest other = (ToggleDeviceDbStoreRequest) obj;

        return (
            EqualityUtility.equals(toggleOn, other.toggleOn)
            && EqualityUtility.equals(storeLocation, other.storeLocation)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(toggleOn),
            EqualityUtility.generateHashCode(storeLocation)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ToggleDeviceDbStoreRequest { ");
        sb.append("toggleOn: ");
        sb.append(this.toggleOn);
        sb.append(", ");
        sb.append("storeLocation: ");
        sb.append(this.storeLocation);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ToggleDeviceDbStoreRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ToggleDeviceDbStoreRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ToggleDeviceDbStoreRequest> PARSER =
        new zaber.motion.dto.Parser<ToggleDeviceDbStoreRequest>() {
            @Override
            public ToggleDeviceDbStoreRequest fromByteArray(byte[] data) {
                return ToggleDeviceDbStoreRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ToggleDeviceDbStoreRequest> parser() {
        return PARSER;
    }

}
