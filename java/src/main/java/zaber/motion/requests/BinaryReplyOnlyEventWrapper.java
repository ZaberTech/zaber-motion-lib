/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.ReplyOnlyEvent;
import zaber.motion.EqualityUtility;


public final class BinaryReplyOnlyEventWrapper implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryReplyOnlyEventWrapper withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private ReplyOnlyEvent reply;

    @com.fasterxml.jackson.annotation.JsonProperty("reply")
    public void setReply(ReplyOnlyEvent reply) {
        this.reply = reply;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("reply")
    public ReplyOnlyEvent getReply() {
        return this.reply;
    }

    public BinaryReplyOnlyEventWrapper withReply(ReplyOnlyEvent aReply) {
        this.setReply(aReply);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryReplyOnlyEventWrapper() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryReplyOnlyEventWrapper(
        int interfaceId,
        ReplyOnlyEvent reply
    ) {
        this.interfaceId = interfaceId;
        this.reply = reply;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryReplyOnlyEventWrapper other = (BinaryReplyOnlyEventWrapper) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(reply, other.reply)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(reply)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryReplyOnlyEventWrapper { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("reply: ");
        sb.append(this.reply);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryReplyOnlyEventWrapper fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryReplyOnlyEventWrapper.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryReplyOnlyEventWrapper> PARSER =
        new zaber.motion.dto.Parser<BinaryReplyOnlyEventWrapper>() {
            @Override
            public BinaryReplyOnlyEventWrapper fromByteArray(byte[] data) {
                return BinaryReplyOnlyEventWrapper.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryReplyOnlyEventWrapper> parser() {
        return PARSER;
    }

}
