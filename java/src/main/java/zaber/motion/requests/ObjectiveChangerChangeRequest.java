/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class ObjectiveChangerChangeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ObjectiveChangerChangeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public ObjectiveChangerChangeRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public ObjectiveChangerChangeRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public ObjectiveChangerChangeRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private int objective;

    @com.fasterxml.jackson.annotation.JsonProperty("objective")
    public void setObjective(int objective) {
        this.objective = objective;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("objective")
    public int getObjective() {
        return this.objective;
    }

    public ObjectiveChangerChangeRequest withObjective(int aObjective) {
        this.setObjective(aObjective);
        return this;
    }

    private Measurement focusOffset;

    @com.fasterxml.jackson.annotation.JsonProperty("focusOffset")
    public void setFocusOffset(Measurement focusOffset) {
        this.focusOffset = focusOffset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusOffset")
    public Measurement getFocusOffset() {
        return this.focusOffset;
    }

    public ObjectiveChangerChangeRequest withFocusOffset(Measurement aFocusOffset) {
        this.setFocusOffset(aFocusOffset);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ObjectiveChangerChangeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ObjectiveChangerChangeRequest(
        int interfaceId,
        int turretAddress,
        int focusAddress,
        int focusAxis,
        int objective,
        Measurement focusOffset
    ) {
        this.interfaceId = interfaceId;
        this.turretAddress = turretAddress;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.objective = objective;
        this.focusOffset = focusOffset;
    }

    /**
     * Constructor with only required properties.
     */
    public ObjectiveChangerChangeRequest(
        int interfaceId,
        int turretAddress,
        int focusAddress,
        int focusAxis,
        int objective
    ) {
        this.interfaceId = interfaceId;
        this.turretAddress = turretAddress;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.objective = objective;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ObjectiveChangerChangeRequest other = (ObjectiveChangerChangeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(objective, other.objective)
            && EqualityUtility.equals(focusOffset, other.focusOffset)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(turretAddress),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(objective),
            EqualityUtility.generateHashCode(focusOffset)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectiveChangerChangeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("objective: ");
        sb.append(this.objective);
        sb.append(", ");
        sb.append("focusOffset: ");
        sb.append(this.focusOffset);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ObjectiveChangerChangeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ObjectiveChangerChangeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ObjectiveChangerChangeRequest> PARSER =
        new zaber.motion.dto.Parser<ObjectiveChangerChangeRequest>() {
            @Override
            public ObjectiveChangerChangeRequest fromByteArray(byte[] data) {
                return ObjectiveChangerChangeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ObjectiveChangerChangeRequest> parser() {
        return PARSER;
    }

}
