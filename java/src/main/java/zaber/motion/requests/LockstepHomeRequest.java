/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class LockstepHomeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LockstepHomeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LockstepHomeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int lockstepGroupId;

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public void setLockstepGroupId(int lockstepGroupId) {
        this.lockstepGroupId = lockstepGroupId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public LockstepHomeRequest withLockstepGroupId(int aLockstepGroupId) {
        this.setLockstepGroupId(aLockstepGroupId);
        return this;
    }

    private boolean waitUntilIdle;

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public void setWaitUntilIdle(boolean waitUntilIdle) {
        this.waitUntilIdle = waitUntilIdle;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public boolean getWaitUntilIdle() {
        return this.waitUntilIdle;
    }

    public LockstepHomeRequest withWaitUntilIdle(boolean aWaitUntilIdle) {
        this.setWaitUntilIdle(aWaitUntilIdle);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepHomeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepHomeRequest(
        int interfaceId,
        int device,
        int lockstepGroupId,
        boolean waitUntilIdle
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
        this.waitUntilIdle = waitUntilIdle;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepHomeRequest other = (LockstepHomeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(lockstepGroupId, other.lockstepGroupId)
            && EqualityUtility.equals(waitUntilIdle, other.waitUntilIdle)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(lockstepGroupId),
            EqualityUtility.generateHashCode(waitUntilIdle)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepHomeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("lockstepGroupId: ");
        sb.append(this.lockstepGroupId);
        sb.append(", ");
        sb.append("waitUntilIdle: ");
        sb.append(this.waitUntilIdle);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepHomeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepHomeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepHomeRequest> PARSER =
        new zaber.motion.dto.Parser<LockstepHomeRequest>() {
            @Override
            public LockstepHomeRequest fromByteArray(byte[] data) {
                return LockstepHomeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepHomeRequest> parser() {
        return PARSER;
    }

}
