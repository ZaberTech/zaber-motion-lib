/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class IntArrayResponse implements zaber.motion.dto.Message {

    private int[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(int[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public int[] getValues() {
        return this.values;
    }

    public IntArrayResponse withValues(int[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public IntArrayResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public IntArrayResponse(
        int[] values
    ) {
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        IntArrayResponse other = (IntArrayResponse) obj;

        return (
            EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IntArrayResponse { ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static IntArrayResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, IntArrayResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<IntArrayResponse> PARSER =
        new zaber.motion.dto.Parser<IntArrayResponse>() {
            @Override
            public IntArrayResponse fromByteArray(byte[] data) {
                return IntArrayResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<IntArrayResponse> parser() {
        return PARSER;
    }

}
