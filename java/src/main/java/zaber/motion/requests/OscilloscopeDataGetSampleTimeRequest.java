/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class OscilloscopeDataGetSampleTimeRequest implements zaber.motion.dto.Message {

    private int dataId;

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public int getDataId() {
        return this.dataId;
    }

    public OscilloscopeDataGetSampleTimeRequest withDataId(int aDataId) {
        this.setDataId(aDataId);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public OscilloscopeDataGetSampleTimeRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private int index;

    @com.fasterxml.jackson.annotation.JsonProperty("index")
    public void setIndex(int index) {
        this.index = index;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("index")
    public int getIndex() {
        return this.index;
    }

    public OscilloscopeDataGetSampleTimeRequest withIndex(int aIndex) {
        this.setIndex(aIndex);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeDataGetSampleTimeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeDataGetSampleTimeRequest(
        int dataId,
        Units unit,
        int index
    ) {
        this.dataId = dataId;
        this.unit = unit;
        this.index = index;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeDataGetSampleTimeRequest other = (OscilloscopeDataGetSampleTimeRequest) obj;

        return (
            EqualityUtility.equals(dataId, other.dataId)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(index, other.index)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataId),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(index)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeDataGetSampleTimeRequest { ");
        sb.append("dataId: ");
        sb.append(this.dataId);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("index: ");
        sb.append(this.index);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeDataGetSampleTimeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeDataGetSampleTimeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeDataGetSampleTimeRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeDataGetSampleTimeRequest>() {
            @Override
            public OscilloscopeDataGetSampleTimeRequest fromByteArray(byte[] data) {
                return OscilloscopeDataGetSampleTimeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeDataGetSampleTimeRequest> parser() {
        return PARSER;
    }

}
