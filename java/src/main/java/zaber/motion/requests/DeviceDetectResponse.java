/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceDetectResponse implements zaber.motion.dto.Message {

    private int[] devices;

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public void setDevices(int[] devices) {
        this.devices = devices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public int[] getDevices() {
        return this.devices;
    }

    public DeviceDetectResponse withDevices(int[] aDevices) {
        this.setDevices(aDevices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceDetectResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceDetectResponse(
        int[] devices
    ) {
        this.devices = devices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceDetectResponse other = (DeviceDetectResponse) obj;

        return (
            EqualityUtility.equals(devices, other.devices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(devices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceDetectResponse { ");
        sb.append("devices: ");
        sb.append(this.devices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceDetectResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceDetectResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceDetectResponse> PARSER =
        new zaber.motion.dto.Parser<DeviceDetectResponse>() {
            @Override
            public DeviceDetectResponse fromByteArray(byte[] data) {
                return DeviceDetectResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceDetectResponse> parser() {
        return PARSER;
    }

}
