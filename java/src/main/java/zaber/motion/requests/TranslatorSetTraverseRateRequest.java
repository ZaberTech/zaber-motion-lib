/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class TranslatorSetTraverseRateRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorSetTraverseRateRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    private double traverseRate;

    @com.fasterxml.jackson.annotation.JsonProperty("traverseRate")
    public void setTraverseRate(double traverseRate) {
        this.traverseRate = traverseRate;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("traverseRate")
    public double getTraverseRate() {
        return this.traverseRate;
    }

    public TranslatorSetTraverseRateRequest withTraverseRate(double aTraverseRate) {
        this.setTraverseRate(aTraverseRate);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public TranslatorSetTraverseRateRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorSetTraverseRateRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorSetTraverseRateRequest(
        int translatorId,
        double traverseRate,
        Units unit
    ) {
        this.translatorId = translatorId;
        this.traverseRate = traverseRate;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorSetTraverseRateRequest other = (TranslatorSetTraverseRateRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            && EqualityUtility.equals(traverseRate, other.traverseRate)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId),
            EqualityUtility.generateHashCode(traverseRate),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorSetTraverseRateRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(", ");
        sb.append("traverseRate: ");
        sb.append(this.traverseRate);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorSetTraverseRateRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorSetTraverseRateRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorSetTraverseRateRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorSetTraverseRateRequest>() {
            @Override
            public TranslatorSetTraverseRateRequest fromByteArray(byte[] data) {
                return TranslatorSetTraverseRateRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorSetTraverseRateRequest> parser() {
        return PARSER;
    }

}
