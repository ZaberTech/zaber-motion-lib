/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

public enum DeviceType {

    ANY(0),

    PROCESS_CONTROLLER(1);

    private int value;

    DeviceType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static DeviceType valueOf(int argValue) {
        for (DeviceType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
