/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OpenBinaryInterfaceRequest implements zaber.motion.dto.Message {

    private InterfaceType interfaceType;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceType")
    public void setInterfaceType(InterfaceType interfaceType) {
        this.interfaceType = interfaceType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceType")
    public InterfaceType getInterfaceType() {
        return this.interfaceType;
    }

    public OpenBinaryInterfaceRequest withInterfaceType(InterfaceType aInterfaceType) {
        this.setInterfaceType(aInterfaceType);
        return this;
    }

    private String portName;

    @com.fasterxml.jackson.annotation.JsonProperty("portName")
    public void setPortName(String portName) {
        this.portName = portName;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("portName")
    public String getPortName() {
        return this.portName;
    }

    public OpenBinaryInterfaceRequest withPortName(String aPortName) {
        this.setPortName(aPortName);
        return this;
    }

    private int baudRate;

    @com.fasterxml.jackson.annotation.JsonProperty("baudRate")
    public void setBaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("baudRate")
    public int getBaudRate() {
        return this.baudRate;
    }

    public OpenBinaryInterfaceRequest withBaudRate(int aBaudRate) {
        this.setBaudRate(aBaudRate);
        return this;
    }

    private String hostName;

    @com.fasterxml.jackson.annotation.JsonProperty("hostName")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("hostName")
    public String getHostName() {
        return this.hostName;
    }

    public OpenBinaryInterfaceRequest withHostName(String aHostName) {
        this.setHostName(aHostName);
        return this;
    }

    private int port;

    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public int getPort() {
        return this.port;
    }

    public OpenBinaryInterfaceRequest withPort(int aPort) {
        this.setPort(aPort);
        return this;
    }

    private boolean useMessageIds;

    @com.fasterxml.jackson.annotation.JsonProperty("useMessageIds")
    public void setUseMessageIds(boolean useMessageIds) {
        this.useMessageIds = useMessageIds;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("useMessageIds")
    public boolean getUseMessageIds() {
        return this.useMessageIds;
    }

    public OpenBinaryInterfaceRequest withUseMessageIds(boolean aUseMessageIds) {
        this.setUseMessageIds(aUseMessageIds);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OpenBinaryInterfaceRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OpenBinaryInterfaceRequest(
        InterfaceType interfaceType,
        String portName,
        int baudRate,
        String hostName,
        int port,
        boolean useMessageIds
    ) {
        this.interfaceType = interfaceType;
        this.portName = portName;
        this.baudRate = baudRate;
        this.hostName = hostName;
        this.port = port;
        this.useMessageIds = useMessageIds;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OpenBinaryInterfaceRequest other = (OpenBinaryInterfaceRequest) obj;

        return (
            EqualityUtility.equals(interfaceType, other.interfaceType)
            && EqualityUtility.equals(portName, other.portName)
            && EqualityUtility.equals(baudRate, other.baudRate)
            && EqualityUtility.equals(hostName, other.hostName)
            && EqualityUtility.equals(port, other.port)
            && EqualityUtility.equals(useMessageIds, other.useMessageIds)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceType),
            EqualityUtility.generateHashCode(portName),
            EqualityUtility.generateHashCode(baudRate),
            EqualityUtility.generateHashCode(hostName),
            EqualityUtility.generateHashCode(port),
            EqualityUtility.generateHashCode(useMessageIds)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OpenBinaryInterfaceRequest { ");
        sb.append("interfaceType: ");
        sb.append(this.interfaceType);
        sb.append(", ");
        sb.append("portName: ");
        sb.append(this.portName);
        sb.append(", ");
        sb.append("baudRate: ");
        sb.append(this.baudRate);
        sb.append(", ");
        sb.append("hostName: ");
        sb.append(this.hostName);
        sb.append(", ");
        sb.append("port: ");
        sb.append(this.port);
        sb.append(", ");
        sb.append("useMessageIds: ");
        sb.append(this.useMessageIds);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OpenBinaryInterfaceRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OpenBinaryInterfaceRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OpenBinaryInterfaceRequest> PARSER =
        new zaber.motion.dto.Parser<OpenBinaryInterfaceRequest>() {
            @Override
            public OpenBinaryInterfaceRequest fromByteArray(byte[] data) {
                return OpenBinaryInterfaceRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OpenBinaryInterfaceRequest> parser() {
        return PARSER;
    }

}
