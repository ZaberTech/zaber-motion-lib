/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceWaitUntilIdleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceWaitUntilIdleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceWaitUntilIdleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceWaitUntilIdleRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean throwErrorOnFault;

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public void setThrowErrorOnFault(boolean throwErrorOnFault) {
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public boolean getThrowErrorOnFault() {
        return this.throwErrorOnFault;
    }

    public DeviceWaitUntilIdleRequest withThrowErrorOnFault(boolean aThrowErrorOnFault) {
        this.setThrowErrorOnFault(aThrowErrorOnFault);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceWaitUntilIdleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceWaitUntilIdleRequest(
        int interfaceId,
        int device,
        int axis,
        boolean throwErrorOnFault
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceWaitUntilIdleRequest other = (DeviceWaitUntilIdleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(throwErrorOnFault, other.throwErrorOnFault)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(throwErrorOnFault)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceWaitUntilIdleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("throwErrorOnFault: ");
        sb.append(this.throwErrorOnFault);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceWaitUntilIdleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceWaitUntilIdleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceWaitUntilIdleRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceWaitUntilIdleRequest>() {
            @Override
            public DeviceWaitUntilIdleRequest fromByteArray(byte[] data) {
                return DeviceWaitUntilIdleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceWaitUntilIdleRequest> parser() {
        return PARSER;
    }

}
