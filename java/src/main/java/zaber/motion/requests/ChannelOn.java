/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ChannelOn implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ChannelOn withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public ChannelOn withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public ChannelOn withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean on;

    @com.fasterxml.jackson.annotation.JsonProperty("on")
    public void setOn(boolean on) {
        this.on = on;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("on")
    public boolean getOn() {
        return this.on;
    }

    public ChannelOn withOn(boolean aOn) {
        this.setOn(aOn);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ChannelOn() {
    }

    /**
     * Constructor with all properties.
     */
    public ChannelOn(
        int interfaceId,
        int device,
        int axis,
        boolean on
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.on = on;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ChannelOn other = (ChannelOn) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(on, other.on)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(on)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChannelOn { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("on: ");
        sb.append(this.on);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ChannelOn fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ChannelOn.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ChannelOn> PARSER =
        new zaber.motion.dto.Parser<ChannelOn>() {
            @Override
            public ChannelOn fromByteArray(byte[] data) {
                return ChannelOn.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ChannelOn> parser() {
        return PARSER;
    }

}
