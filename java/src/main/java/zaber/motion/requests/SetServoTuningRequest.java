/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.ascii.ServoTuningParam;
import zaber.motion.EqualityUtility;


public final class SetServoTuningRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetServoTuningRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetServoTuningRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public SetServoTuningRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ServoTuningParamset paramset;

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public void setParamset(ServoTuningParamset paramset) {
        this.paramset = paramset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public ServoTuningParamset getParamset() {
        return this.paramset;
    }

    public SetServoTuningRequest withParamset(ServoTuningParamset aParamset) {
        this.setParamset(aParamset);
        return this;
    }

    private ServoTuningParam[] tuningParams;

    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public void setTuningParams(ServoTuningParam[] tuningParams) {
        this.tuningParams = tuningParams;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public ServoTuningParam[] getTuningParams() {
        return this.tuningParams;
    }

    public SetServoTuningRequest withTuningParams(ServoTuningParam[] aTuningParams) {
        this.setTuningParams(aTuningParams);
        return this;
    }

    private boolean setUnspecifiedToDefault;

    @com.fasterxml.jackson.annotation.JsonProperty("setUnspecifiedToDefault")
    public void setSetUnspecifiedToDefault(boolean setUnspecifiedToDefault) {
        this.setUnspecifiedToDefault = setUnspecifiedToDefault;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setUnspecifiedToDefault")
    public boolean getSetUnspecifiedToDefault() {
        return this.setUnspecifiedToDefault;
    }

    public SetServoTuningRequest withSetUnspecifiedToDefault(boolean aSetUnspecifiedToDefault) {
        this.setSetUnspecifiedToDefault(aSetUnspecifiedToDefault);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetServoTuningRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetServoTuningRequest(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        boolean setUnspecifiedToDefault
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.paramset = paramset;
        this.tuningParams = tuningParams;
        this.setUnspecifiedToDefault = setUnspecifiedToDefault;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetServoTuningRequest other = (SetServoTuningRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(paramset, other.paramset)
            && EqualityUtility.equals(tuningParams, other.tuningParams)
            && EqualityUtility.equals(setUnspecifiedToDefault, other.setUnspecifiedToDefault)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(paramset),
            EqualityUtility.generateHashCode(tuningParams),
            EqualityUtility.generateHashCode(setUnspecifiedToDefault)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetServoTuningRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("paramset: ");
        sb.append(this.paramset);
        sb.append(", ");
        sb.append("tuningParams: ");
        sb.append(this.tuningParams);
        sb.append(", ");
        sb.append("setUnspecifiedToDefault: ");
        sb.append(this.setUnspecifiedToDefault);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetServoTuningRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetServoTuningRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetServoTuningRequest> PARSER =
        new zaber.motion.dto.Parser<SetServoTuningRequest>() {
            @Override
            public SetServoTuningRequest fromByteArray(byte[] data) {
                return SetServoTuningRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetServoTuningRequest> parser() {
        return PARSER;
    }

}
