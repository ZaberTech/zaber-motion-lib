/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceSetStorageBoolRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetStorageBoolRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetStorageBoolRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceSetStorageBoolRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String key;

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public String getKey() {
        return this.key;
    }

    public DeviceSetStorageBoolRequest withKey(String aKey) {
        this.setKey(aKey);
        return this;
    }

    private boolean value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(boolean value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public boolean getValue() {
        return this.value;
    }

    public DeviceSetStorageBoolRequest withValue(boolean aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetStorageBoolRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetStorageBoolRequest(
        int interfaceId,
        int device,
        int axis,
        String key,
        boolean value
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetStorageBoolRequest other = (DeviceSetStorageBoolRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(key, other.key)
            && EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(key),
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetStorageBoolRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("key: ");
        sb.append(this.key);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetStorageBoolRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetStorageBoolRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetStorageBoolRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetStorageBoolRequest>() {
            @Override
            public DeviceSetStorageBoolRequest fromByteArray(byte[] data) {
                return DeviceSetStorageBoolRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetStorageBoolRequest> parser() {
        return PARSER;
    }

}
