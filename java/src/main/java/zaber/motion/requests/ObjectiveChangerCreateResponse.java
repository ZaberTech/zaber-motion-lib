/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ObjectiveChangerCreateResponse implements zaber.motion.dto.Message {

    private int turret;

    @com.fasterxml.jackson.annotation.JsonProperty("turret")
    public void setTurret(int turret) {
        this.turret = turret;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turret")
    public int getTurret() {
        return this.turret;
    }

    public ObjectiveChangerCreateResponse withTurret(int aTurret) {
        this.setTurret(aTurret);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public ObjectiveChangerCreateResponse withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public ObjectiveChangerCreateResponse withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ObjectiveChangerCreateResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public ObjectiveChangerCreateResponse(
        int turret,
        int focusAddress,
        int focusAxis
    ) {
        this.turret = turret;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ObjectiveChangerCreateResponse other = (ObjectiveChangerCreateResponse) obj;

        return (
            EqualityUtility.equals(turret, other.turret)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(turret),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectiveChangerCreateResponse { ");
        sb.append("turret: ");
        sb.append(this.turret);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ObjectiveChangerCreateResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ObjectiveChangerCreateResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ObjectiveChangerCreateResponse> PARSER =
        new zaber.motion.dto.Parser<ObjectiveChangerCreateResponse>() {
            @Override
            public ObjectiveChangerCreateResponse fromByteArray(byte[] data) {
                return ObjectiveChangerCreateResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ObjectiveChangerCreateResponse> parser() {
        return PARSER;
    }

}
