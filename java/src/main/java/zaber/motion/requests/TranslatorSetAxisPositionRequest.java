/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class TranslatorSetAxisPositionRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorSetAxisPositionRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    private String axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(String axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public String getAxis() {
        return this.axis;
    }

    public TranslatorSetAxisPositionRequest withAxis(String aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double position;

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public void setPosition(double position) {
        this.position = position;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public double getPosition() {
        return this.position;
    }

    public TranslatorSetAxisPositionRequest withPosition(double aPosition) {
        this.setPosition(aPosition);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public TranslatorSetAxisPositionRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorSetAxisPositionRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorSetAxisPositionRequest(
        int translatorId,
        String axis,
        double position,
        Units unit
    ) {
        this.translatorId = translatorId;
        this.axis = axis;
        this.position = position;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorSetAxisPositionRequest other = (TranslatorSetAxisPositionRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(position, other.position)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(position),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorSetAxisPositionRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("position: ");
        sb.append(this.position);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorSetAxisPositionRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorSetAxisPositionRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorSetAxisPositionRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorSetAxisPositionRequest>() {
            @Override
            public TranslatorSetAxisPositionRequest fromByteArray(byte[] data) {
                return TranslatorSetAxisPositionRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorSetAxisPositionRequest> parser() {
        return PARSER;
    }

}
