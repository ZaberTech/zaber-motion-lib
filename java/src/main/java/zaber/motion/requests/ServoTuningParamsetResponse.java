/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.EqualityUtility;


public final class ServoTuningParamsetResponse implements zaber.motion.dto.Message {

    private ServoTuningParamset paramset;

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public void setParamset(ServoTuningParamset paramset) {
        this.paramset = paramset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public ServoTuningParamset getParamset() {
        return this.paramset;
    }

    public ServoTuningParamsetResponse withParamset(ServoTuningParamset aParamset) {
        this.setParamset(aParamset);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ServoTuningParamsetResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public ServoTuningParamsetResponse(
        ServoTuningParamset paramset
    ) {
        this.paramset = paramset;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ServoTuningParamsetResponse other = (ServoTuningParamsetResponse) obj;

        return (
            EqualityUtility.equals(paramset, other.paramset)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(paramset)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ServoTuningParamsetResponse { ");
        sb.append("paramset: ");
        sb.append(this.paramset);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ServoTuningParamsetResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ServoTuningParamsetResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ServoTuningParamsetResponse> PARSER =
        new zaber.motion.dto.Parser<ServoTuningParamsetResponse>() {
            @Override
            public ServoTuningParamsetResponse fromByteArray(byte[] data) {
                return ServoTuningParamsetResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ServoTuningParamsetResponse> parser() {
        return PARSER;
    }

}
