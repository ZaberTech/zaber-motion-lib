/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.gcode.TranslatorConfig;
import zaber.motion.EqualityUtility;


public final class TranslatorCreateLiveRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TranslatorCreateLiveRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TranslatorCreateLiveRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public TranslatorCreateLiveRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private TranslatorConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(TranslatorConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public TranslatorConfig getConfig() {
        return this.config;
    }

    public TranslatorCreateLiveRequest withConfig(TranslatorConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorCreateLiveRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorCreateLiveRequest(
        int interfaceId,
        int device,
        int streamId,
        TranslatorConfig config
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.config = config;
    }

    /**
     * Constructor with only required properties.
     */
    public TranslatorCreateLiveRequest(
        int interfaceId,
        int device,
        int streamId
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorCreateLiveRequest other = (TranslatorCreateLiveRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(config, other.config)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(config)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorCreateLiveRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorCreateLiveRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorCreateLiveRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorCreateLiveRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorCreateLiveRequest>() {
            @Override
            public TranslatorCreateLiveRequest fromByteArray(byte[] data) {
                return TranslatorCreateLiveRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorCreateLiveRequest> parser() {
        return PARSER;
    }

}
