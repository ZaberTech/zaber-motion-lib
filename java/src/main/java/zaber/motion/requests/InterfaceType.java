/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

public enum InterfaceType {

    SERIAL_PORT(0),

    TCP(1),

    CUSTOM(2),

    IOT(3),

    NETWORK_SHARE(4);

    private int value;

    InterfaceType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static InterfaceType valueOf(int argValue) {
        for (InterfaceType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
