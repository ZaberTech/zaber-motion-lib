/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class AxesEmptyRequest implements zaber.motion.dto.Message {

    private int[] interfaces;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public void setInterfaces(int[] interfaces) {
        this.interfaces = interfaces;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public int[] getInterfaces() {
        return this.interfaces;
    }

    public AxesEmptyRequest withInterfaces(int[] aInterfaces) {
        this.setInterfaces(aInterfaces);
        return this;
    }

    private int[] devices;

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public void setDevices(int[] devices) {
        this.devices = devices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public int[] getDevices() {
        return this.devices;
    }

    public AxesEmptyRequest withDevices(int[] aDevices) {
        this.setDevices(aDevices);
        return this;
    }

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public AxesEmptyRequest withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxesEmptyRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AxesEmptyRequest(
        int[] interfaces,
        int[] devices,
        int[] axes
    ) {
        this.interfaces = interfaces;
        this.devices = devices;
        this.axes = axes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxesEmptyRequest other = (AxesEmptyRequest) obj;

        return (
            EqualityUtility.equals(interfaces, other.interfaces)
            && EqualityUtility.equals(devices, other.devices)
            && EqualityUtility.equals(axes, other.axes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaces),
            EqualityUtility.generateHashCode(devices),
            EqualityUtility.generateHashCode(axes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxesEmptyRequest { ");
        sb.append("interfaces: ");
        sb.append(this.interfaces);
        sb.append(", ");
        sb.append("devices: ");
        sb.append(this.devices);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxesEmptyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxesEmptyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxesEmptyRequest> PARSER =
        new zaber.motion.dto.Parser<AxesEmptyRequest>() {
            @Override
            public AxesEmptyRequest fromByteArray(byte[] data) {
                return AxesEmptyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxesEmptyRequest> parser() {
        return PARSER;
    }

}
