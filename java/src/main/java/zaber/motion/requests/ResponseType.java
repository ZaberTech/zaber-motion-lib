/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

public enum ResponseType {

    OK(0),

    ERROR(1);

    private int value;

    ResponseType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static ResponseType valueOf(int argValue) {
        for (ResponseType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
