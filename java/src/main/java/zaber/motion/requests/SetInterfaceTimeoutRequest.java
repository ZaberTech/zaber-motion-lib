/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class SetInterfaceTimeoutRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetInterfaceTimeoutRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public int getTimeout() {
        return this.timeout;
    }

    public SetInterfaceTimeoutRequest withTimeout(int aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetInterfaceTimeoutRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetInterfaceTimeoutRequest(
        int interfaceId,
        int timeout
    ) {
        this.interfaceId = interfaceId;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetInterfaceTimeoutRequest other = (SetInterfaceTimeoutRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetInterfaceTimeoutRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetInterfaceTimeoutRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetInterfaceTimeoutRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetInterfaceTimeoutRequest> PARSER =
        new zaber.motion.dto.Parser<SetInterfaceTimeoutRequest>() {
            @Override
            public SetInterfaceTimeoutRequest fromByteArray(byte[] data) {
                return SetInterfaceTimeoutRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetInterfaceTimeoutRequest> parser() {
        return PARSER;
    }

}
