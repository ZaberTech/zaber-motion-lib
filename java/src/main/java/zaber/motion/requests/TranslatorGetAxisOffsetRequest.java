/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class TranslatorGetAxisOffsetRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorGetAxisOffsetRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    private String coordinateSystem;

    @com.fasterxml.jackson.annotation.JsonProperty("coordinateSystem")
    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("coordinateSystem")
    public String getCoordinateSystem() {
        return this.coordinateSystem;
    }

    public TranslatorGetAxisOffsetRequest withCoordinateSystem(String aCoordinateSystem) {
        this.setCoordinateSystem(aCoordinateSystem);
        return this;
    }

    private String axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(String axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public String getAxis() {
        return this.axis;
    }

    public TranslatorGetAxisOffsetRequest withAxis(String aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public TranslatorGetAxisOffsetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorGetAxisOffsetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorGetAxisOffsetRequest(
        int translatorId,
        String coordinateSystem,
        String axis,
        Units unit
    ) {
        this.translatorId = translatorId;
        this.coordinateSystem = coordinateSystem;
        this.axis = axis;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorGetAxisOffsetRequest other = (TranslatorGetAxisOffsetRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            && EqualityUtility.equals(coordinateSystem, other.coordinateSystem)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId),
            EqualityUtility.generateHashCode(coordinateSystem),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorGetAxisOffsetRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(", ");
        sb.append("coordinateSystem: ");
        sb.append(this.coordinateSystem);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorGetAxisOffsetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorGetAxisOffsetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorGetAxisOffsetRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorGetAxisOffsetRequest>() {
            @Override
            public TranslatorGetAxisOffsetRequest fromByteArray(byte[] data) {
                return TranslatorGetAxisOffsetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorGetAxisOffsetRequest> parser() {
        return PARSER;
    }

}
