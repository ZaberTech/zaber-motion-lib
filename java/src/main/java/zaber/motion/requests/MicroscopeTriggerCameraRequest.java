/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class MicroscopeTriggerCameraRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public MicroscopeTriggerCameraRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public MicroscopeTriggerCameraRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public MicroscopeTriggerCameraRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private double delay;

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public void setDelay(double delay) {
        this.delay = delay;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public double getDelay() {
        return this.delay;
    }

    public MicroscopeTriggerCameraRequest withDelay(double aDelay) {
        this.setDelay(aDelay);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public MicroscopeTriggerCameraRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private boolean wait;

    @com.fasterxml.jackson.annotation.JsonProperty("wait")
    public void setWait(boolean wait) {
        this.wait = wait;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("wait")
    public boolean getWait() {
        return this.wait;
    }

    public MicroscopeTriggerCameraRequest withWait(boolean aWait) {
        this.setWait(aWait);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeTriggerCameraRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeTriggerCameraRequest(
        int interfaceId,
        int device,
        int channelNumber,
        double delay,
        Units unit,
        boolean wait
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelNumber = channelNumber;
        this.delay = delay;
        this.unit = unit;
        this.wait = wait;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeTriggerCameraRequest other = (MicroscopeTriggerCameraRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(delay, other.delay)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(wait, other.wait)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(delay),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(wait)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeTriggerCameraRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("delay: ");
        sb.append(this.delay);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("wait: ");
        sb.append(this.wait);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeTriggerCameraRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeTriggerCameraRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeTriggerCameraRequest> PARSER =
        new zaber.motion.dto.Parser<MicroscopeTriggerCameraRequest>() {
            @Override
            public MicroscopeTriggerCameraRequest fromByteArray(byte[] data) {
                return MicroscopeTriggerCameraRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeTriggerCameraRequest> parser() {
        return PARSER;
    }

}
