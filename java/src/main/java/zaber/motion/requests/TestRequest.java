/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TestRequest implements zaber.motion.dto.Message {

    private boolean returnError;

    @com.fasterxml.jackson.annotation.JsonProperty("returnError")
    public void setReturnError(boolean returnError) {
        this.returnError = returnError;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("returnError")
    public boolean getReturnError() {
        return this.returnError;
    }

    public TestRequest withReturnError(boolean aReturnError) {
        this.setReturnError(aReturnError);
        return this;
    }

    private String dataPing;

    @com.fasterxml.jackson.annotation.JsonProperty("dataPing")
    public void setDataPing(String dataPing) {
        this.dataPing = dataPing;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataPing")
    public String getDataPing() {
        return this.dataPing;
    }

    public TestRequest withDataPing(String aDataPing) {
        this.setDataPing(aDataPing);
        return this;
    }

    private boolean returnErrorWithData;

    @com.fasterxml.jackson.annotation.JsonProperty("returnErrorWithData")
    public void setReturnErrorWithData(boolean returnErrorWithData) {
        this.returnErrorWithData = returnErrorWithData;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("returnErrorWithData")
    public boolean getReturnErrorWithData() {
        return this.returnErrorWithData;
    }

    public TestRequest withReturnErrorWithData(boolean aReturnErrorWithData) {
        this.setReturnErrorWithData(aReturnErrorWithData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TestRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TestRequest(
        boolean returnError,
        String dataPing,
        boolean returnErrorWithData
    ) {
        this.returnError = returnError;
        this.dataPing = dataPing;
        this.returnErrorWithData = returnErrorWithData;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TestRequest other = (TestRequest) obj;

        return (
            EqualityUtility.equals(returnError, other.returnError)
            && EqualityUtility.equals(dataPing, other.dataPing)
            && EqualityUtility.equals(returnErrorWithData, other.returnErrorWithData)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(returnError),
            EqualityUtility.generateHashCode(dataPing),
            EqualityUtility.generateHashCode(returnErrorWithData)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TestRequest { ");
        sb.append("returnError: ");
        sb.append(this.returnError);
        sb.append(", ");
        sb.append("dataPing: ");
        sb.append(this.dataPing);
        sb.append(", ");
        sb.append("returnErrorWithData: ");
        sb.append(this.returnErrorWithData);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TestRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TestRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TestRequest> PARSER =
        new zaber.motion.dto.Parser<TestRequest>() {
            @Override
            public TestRequest fromByteArray(byte[] data) {
                return TestRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TestRequest> parser() {
        return PARSER;
    }

}
