/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.GetSettingResult;
import zaber.motion.EqualityUtility;


public final class GetSettingResults implements zaber.motion.dto.Message {

    private GetSettingResult[] results;

    @com.fasterxml.jackson.annotation.JsonProperty("results")
    public void setResults(GetSettingResult[] results) {
        this.results = results;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("results")
    public GetSettingResult[] getResults() {
        return this.results;
    }

    public GetSettingResults withResults(GetSettingResult[] aResults) {
        this.setResults(aResults);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetSettingResults() {
    }

    /**
     * Constructor with all properties.
     */
    public GetSettingResults(
        GetSettingResult[] results
    ) {
        this.results = results;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetSettingResults other = (GetSettingResults) obj;

        return (
            EqualityUtility.equals(results, other.results)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(results)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetSettingResults { ");
        sb.append("results: ");
        sb.append(this.results);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetSettingResults fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetSettingResults.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetSettingResults> PARSER =
        new zaber.motion.dto.Parser<GetSettingResults>() {
            @Override
            public GetSettingResults fromByteArray(byte[] data) {
                return GetSettingResults.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetSettingResults> parser() {
        return PARSER;
    }

}
