/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.LogOutputMode;
import zaber.motion.EqualityUtility;


public final class SetLogOutputRequest implements zaber.motion.dto.Message {

    private LogOutputMode mode;

    @com.fasterxml.jackson.annotation.JsonProperty("mode")
    public void setMode(LogOutputMode mode) {
        this.mode = mode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("mode")
    public LogOutputMode getMode() {
        return this.mode;
    }

    public SetLogOutputRequest withMode(LogOutputMode aMode) {
        this.setMode(aMode);
        return this;
    }

    private String filePath;

    @com.fasterxml.jackson.annotation.JsonProperty("filePath")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("filePath")
    public String getFilePath() {
        return this.filePath;
    }

    public SetLogOutputRequest withFilePath(String aFilePath) {
        this.setFilePath(aFilePath);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetLogOutputRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetLogOutputRequest(
        LogOutputMode mode,
        String filePath
    ) {
        this.mode = mode;
        this.filePath = filePath;
    }

    /**
     * Constructor with only required properties.
     */
    public SetLogOutputRequest(
        LogOutputMode mode
    ) {
        this.mode = mode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetLogOutputRequest other = (SetLogOutputRequest) obj;

        return (
            EqualityUtility.equals(mode, other.mode)
            && EqualityUtility.equals(filePath, other.filePath)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(mode),
            EqualityUtility.generateHashCode(filePath)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetLogOutputRequest { ");
        sb.append("mode: ");
        sb.append(this.mode);
        sb.append(", ");
        sb.append("filePath: ");
        sb.append(this.filePath);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetLogOutputRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetLogOutputRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetLogOutputRequest> PARSER =
        new zaber.motion.dto.Parser<SetLogOutputRequest>() {
            @Override
            public SetLogOutputRequest fromByteArray(byte[] data) {
                return SetLogOutputRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetLogOutputRequest> parser() {
        return PARSER;
    }

}
