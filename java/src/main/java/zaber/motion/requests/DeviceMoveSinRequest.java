/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class DeviceMoveSinRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceMoveSinRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceMoveSinRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceMoveSinRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double amplitude;

    @com.fasterxml.jackson.annotation.JsonProperty("amplitude")
    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("amplitude")
    public double getAmplitude() {
        return this.amplitude;
    }

    public DeviceMoveSinRequest withAmplitude(double aAmplitude) {
        this.setAmplitude(aAmplitude);
        return this;
    }

    private Units amplitudeUnits;

    @com.fasterxml.jackson.annotation.JsonProperty("amplitudeUnits")
    public void setAmplitudeUnits(Units amplitudeUnits) {
        this.amplitudeUnits = amplitudeUnits;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("amplitudeUnits")
    public Units getAmplitudeUnits() {
        return this.amplitudeUnits;
    }

    public DeviceMoveSinRequest withAmplitudeUnits(Units aAmplitudeUnits) {
        this.setAmplitudeUnits(aAmplitudeUnits);
        return this;
    }

    private double period;

    @com.fasterxml.jackson.annotation.JsonProperty("period")
    public void setPeriod(double period) {
        this.period = period;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("period")
    public double getPeriod() {
        return this.period;
    }

    public DeviceMoveSinRequest withPeriod(double aPeriod) {
        this.setPeriod(aPeriod);
        return this;
    }

    private Units periodUnits;

    @com.fasterxml.jackson.annotation.JsonProperty("periodUnits")
    public void setPeriodUnits(Units periodUnits) {
        this.periodUnits = periodUnits;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("periodUnits")
    public Units getPeriodUnits() {
        return this.periodUnits;
    }

    public DeviceMoveSinRequest withPeriodUnits(Units aPeriodUnits) {
        this.setPeriodUnits(aPeriodUnits);
        return this;
    }

    private double count;

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public void setCount(double count) {
        this.count = count;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public double getCount() {
        return this.count;
    }

    public DeviceMoveSinRequest withCount(double aCount) {
        this.setCount(aCount);
        return this;
    }

    private boolean waitUntilIdle;

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public void setWaitUntilIdle(boolean waitUntilIdle) {
        this.waitUntilIdle = waitUntilIdle;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public boolean getWaitUntilIdle() {
        return this.waitUntilIdle;
    }

    public DeviceMoveSinRequest withWaitUntilIdle(boolean aWaitUntilIdle) {
        this.setWaitUntilIdle(aWaitUntilIdle);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceMoveSinRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceMoveSinRequest(
        int interfaceId,
        int device,
        int axis,
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count,
        boolean waitUntilIdle
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.amplitude = amplitude;
        this.amplitudeUnits = amplitudeUnits;
        this.period = period;
        this.periodUnits = periodUnits;
        this.count = count;
        this.waitUntilIdle = waitUntilIdle;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceMoveSinRequest other = (DeviceMoveSinRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(amplitude, other.amplitude)
            && EqualityUtility.equals(amplitudeUnits, other.amplitudeUnits)
            && EqualityUtility.equals(period, other.period)
            && EqualityUtility.equals(periodUnits, other.periodUnits)
            && EqualityUtility.equals(count, other.count)
            && EqualityUtility.equals(waitUntilIdle, other.waitUntilIdle)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(amplitude),
            EqualityUtility.generateHashCode(amplitudeUnits),
            EqualityUtility.generateHashCode(period),
            EqualityUtility.generateHashCode(periodUnits),
            EqualityUtility.generateHashCode(count),
            EqualityUtility.generateHashCode(waitUntilIdle)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceMoveSinRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("amplitude: ");
        sb.append(this.amplitude);
        sb.append(", ");
        sb.append("amplitudeUnits: ");
        sb.append(this.amplitudeUnits);
        sb.append(", ");
        sb.append("period: ");
        sb.append(this.period);
        sb.append(", ");
        sb.append("periodUnits: ");
        sb.append(this.periodUnits);
        sb.append(", ");
        sb.append("count: ");
        sb.append(this.count);
        sb.append(", ");
        sb.append("waitUntilIdle: ");
        sb.append(this.waitUntilIdle);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceMoveSinRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceMoveSinRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceMoveSinRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceMoveSinRequest>() {
            @Override
            public DeviceMoveSinRequest fromByteArray(byte[] data) {
                return DeviceMoveSinRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceMoveSinRequest> parser() {
        return PARSER;
    }

}
