/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamGenericCommandBatchRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamGenericCommandBatchRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamGenericCommandBatchRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamGenericCommandBatchRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamGenericCommandBatchRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private String[] batch;

    @com.fasterxml.jackson.annotation.JsonProperty("batch")
    public void setBatch(String[] batch) {
        this.batch = batch;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("batch")
    public String[] getBatch() {
        return this.batch;
    }

    public StreamGenericCommandBatchRequest withBatch(String[] aBatch) {
        this.setBatch(aBatch);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamGenericCommandBatchRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamGenericCommandBatchRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        String[] batch
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.batch = batch;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamGenericCommandBatchRequest other = (StreamGenericCommandBatchRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(batch, other.batch)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(batch)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamGenericCommandBatchRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("batch: ");
        sb.append(this.batch);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamGenericCommandBatchRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamGenericCommandBatchRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamGenericCommandBatchRequest> PARSER =
        new zaber.motion.dto.Parser<StreamGenericCommandBatchRequest>() {
            @Override
            public StreamGenericCommandBatchRequest fromByteArray(byte[] data) {
                return StreamGenericCommandBatchRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamGenericCommandBatchRequest> parser() {
        return PARSER;
    }

}
