/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.DeviceDbSourceType;
import zaber.motion.EqualityUtility;


public final class SetDeviceDbSourceRequest implements zaber.motion.dto.Message {

    private DeviceDbSourceType sourceType;

    @com.fasterxml.jackson.annotation.JsonProperty("sourceType")
    public void setSourceType(DeviceDbSourceType sourceType) {
        this.sourceType = sourceType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("sourceType")
    public DeviceDbSourceType getSourceType() {
        return this.sourceType;
    }

    public SetDeviceDbSourceRequest withSourceType(DeviceDbSourceType aSourceType) {
        this.setSourceType(aSourceType);
        return this;
    }

    private String urlOrFilePath;

    @com.fasterxml.jackson.annotation.JsonProperty("urlOrFilePath")
    public void setUrlOrFilePath(String urlOrFilePath) {
        this.urlOrFilePath = urlOrFilePath;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("urlOrFilePath")
    public String getUrlOrFilePath() {
        return this.urlOrFilePath;
    }

    public SetDeviceDbSourceRequest withUrlOrFilePath(String aUrlOrFilePath) {
        this.setUrlOrFilePath(aUrlOrFilePath);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetDeviceDbSourceRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetDeviceDbSourceRequest(
        DeviceDbSourceType sourceType,
        String urlOrFilePath
    ) {
        this.sourceType = sourceType;
        this.urlOrFilePath = urlOrFilePath;
    }

    /**
     * Constructor with only required properties.
     */
    public SetDeviceDbSourceRequest(
        DeviceDbSourceType sourceType
    ) {
        this.sourceType = sourceType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetDeviceDbSourceRequest other = (SetDeviceDbSourceRequest) obj;

        return (
            EqualityUtility.equals(sourceType, other.sourceType)
            && EqualityUtility.equals(urlOrFilePath, other.urlOrFilePath)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(sourceType),
            EqualityUtility.generateHashCode(urlOrFilePath)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetDeviceDbSourceRequest { ");
        sb.append("sourceType: ");
        sb.append(this.sourceType);
        sb.append(", ");
        sb.append("urlOrFilePath: ");
        sb.append(this.urlOrFilePath);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetDeviceDbSourceRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetDeviceDbSourceRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetDeviceDbSourceRequest> PARSER =
        new zaber.motion.dto.Parser<SetDeviceDbSourceRequest>() {
            @Override
            public SetDeviceDbSourceRequest fromByteArray(byte[] data) {
                return SetDeviceDbSourceRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetDeviceDbSourceRequest> parser() {
        return PARSER;
    }

}
