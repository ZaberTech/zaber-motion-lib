/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class StreamSetMaxCentripetalAccelerationRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetMaxCentripetalAccelerationRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetMaxCentripetalAccelerationRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetMaxCentripetalAccelerationRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetMaxCentripetalAccelerationRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private double maxCentripetalAcceleration;

    @com.fasterxml.jackson.annotation.JsonProperty("maxCentripetalAcceleration")
    public void setMaxCentripetalAcceleration(double maxCentripetalAcceleration) {
        this.maxCentripetalAcceleration = maxCentripetalAcceleration;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("maxCentripetalAcceleration")
    public double getMaxCentripetalAcceleration() {
        return this.maxCentripetalAcceleration;
    }

    public StreamSetMaxCentripetalAccelerationRequest withMaxCentripetalAcceleration(double aMaxCentripetalAcceleration) {
        this.setMaxCentripetalAcceleration(aMaxCentripetalAcceleration);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public StreamSetMaxCentripetalAccelerationRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetMaxCentripetalAccelerationRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetMaxCentripetalAccelerationRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        double maxCentripetalAcceleration,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.maxCentripetalAcceleration = maxCentripetalAcceleration;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetMaxCentripetalAccelerationRequest other = (StreamSetMaxCentripetalAccelerationRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(maxCentripetalAcceleration, other.maxCentripetalAcceleration)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(maxCentripetalAcceleration),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetMaxCentripetalAccelerationRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("maxCentripetalAcceleration: ");
        sb.append(this.maxCentripetalAcceleration);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetMaxCentripetalAccelerationRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetMaxCentripetalAccelerationRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetMaxCentripetalAccelerationRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetMaxCentripetalAccelerationRequest>() {
            @Override
            public StreamSetMaxCentripetalAccelerationRequest fromByteArray(byte[] data) {
                return StreamSetMaxCentripetalAccelerationRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetMaxCentripetalAccelerationRequest> parser() {
        return PARSER;
    }

}
