/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceRestoreRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceRestoreRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceRestoreRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceRestoreRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean hard;

    @com.fasterxml.jackson.annotation.JsonProperty("hard")
    public void setHard(boolean hard) {
        this.hard = hard;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("hard")
    public boolean getHard() {
        return this.hard;
    }

    public DeviceRestoreRequest withHard(boolean aHard) {
        this.setHard(aHard);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceRestoreRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceRestoreRequest(
        int interfaceId,
        int device,
        int axis,
        boolean hard
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.hard = hard;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceRestoreRequest other = (DeviceRestoreRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(hard, other.hard)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(hard)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceRestoreRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("hard: ");
        sb.append(this.hard);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceRestoreRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceRestoreRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceRestoreRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceRestoreRequest>() {
            @Override
            public DeviceRestoreRequest fromByteArray(byte[] data) {
                return DeviceRestoreRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceRestoreRequest> parser() {
        return PARSER;
    }

}
