/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StringArrayResponse implements zaber.motion.dto.Message {

    private String[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(String[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public String[] getValues() {
        return this.values;
    }

    public StringArrayResponse withValues(String[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StringArrayResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public StringArrayResponse(
        String[] values
    ) {
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StringArrayResponse other = (StringArrayResponse) obj;

        return (
            EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StringArrayResponse { ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StringArrayResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StringArrayResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StringArrayResponse> PARSER =
        new zaber.motion.dto.Parser<StringArrayResponse>() {
            @Override
            public StringArrayResponse fromByteArray(byte[] data) {
                return StringArrayResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StringArrayResponse> parser() {
        return PARSER;
    }

}
