/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class PvtPointRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public PvtPointRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public PvtPointRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public PvtPointRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public PvtPointRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private StreamSegmentType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(StreamSegmentType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public StreamSegmentType getType() {
        return this.type;
    }

    public PvtPointRequest withType(StreamSegmentType aType) {
        this.setType(aType);
        return this;
    }

    private Measurement[] positions;

    @com.fasterxml.jackson.annotation.JsonProperty("positions")
    public void setPositions(Measurement[] positions) {
        this.positions = positions;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("positions")
    public Measurement[] getPositions() {
        return this.positions;
    }

    public PvtPointRequest withPositions(Measurement[] aPositions) {
        this.setPositions(aPositions);
        return this;
    }

    private Measurement[] velocities;

    @com.fasterxml.jackson.annotation.JsonProperty("velocities")
    public void setVelocities(Measurement[] velocities) {
        this.velocities = velocities;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("velocities")
    public Measurement[] getVelocities() {
        return this.velocities;
    }

    public PvtPointRequest withVelocities(Measurement[] aVelocities) {
        this.setVelocities(aVelocities);
        return this;
    }

    private Measurement time;

    @com.fasterxml.jackson.annotation.JsonProperty("time")
    public void setTime(Measurement time) {
        this.time = time;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("time")
    public Measurement getTime() {
        return this.time;
    }

    public PvtPointRequest withTime(Measurement aTime) {
        this.setTime(aTime);
        return this;
    }

    /**
     * Empty constructor.
     */
    public PvtPointRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public PvtPointRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        StreamSegmentType type,
        Measurement[] positions,
        Measurement[] velocities,
        Measurement time
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.type = type;
        this.positions = positions;
        this.velocities = velocities;
        this.time = time;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PvtPointRequest other = (PvtPointRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(positions, other.positions)
            && EqualityUtility.equals(velocities, other.velocities)
            && EqualityUtility.equals(time, other.time)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(positions),
            EqualityUtility.generateHashCode(velocities),
            EqualityUtility.generateHashCode(time)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PvtPointRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("positions: ");
        sb.append(this.positions);
        sb.append(", ");
        sb.append("velocities: ");
        sb.append(this.velocities);
        sb.append(", ");
        sb.append("time: ");
        sb.append(this.time);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static PvtPointRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, PvtPointRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<PvtPointRequest> PARSER =
        new zaber.motion.dto.Parser<PvtPointRequest>() {
            @Override
            public PvtPointRequest fromByteArray(byte[] data) {
                return PvtPointRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<PvtPointRequest> parser() {
        return PARSER;
    }

}
