/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class LockstepWaitUntilIdleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LockstepWaitUntilIdleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LockstepWaitUntilIdleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int lockstepGroupId;

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public void setLockstepGroupId(int lockstepGroupId) {
        this.lockstepGroupId = lockstepGroupId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public LockstepWaitUntilIdleRequest withLockstepGroupId(int aLockstepGroupId) {
        this.setLockstepGroupId(aLockstepGroupId);
        return this;
    }

    private boolean throwErrorOnFault;

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public void setThrowErrorOnFault(boolean throwErrorOnFault) {
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public boolean getThrowErrorOnFault() {
        return this.throwErrorOnFault;
    }

    public LockstepWaitUntilIdleRequest withThrowErrorOnFault(boolean aThrowErrorOnFault) {
        this.setThrowErrorOnFault(aThrowErrorOnFault);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepWaitUntilIdleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepWaitUntilIdleRequest(
        int interfaceId,
        int device,
        int lockstepGroupId,
        boolean throwErrorOnFault
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepWaitUntilIdleRequest other = (LockstepWaitUntilIdleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(lockstepGroupId, other.lockstepGroupId)
            && EqualityUtility.equals(throwErrorOnFault, other.throwErrorOnFault)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(lockstepGroupId),
            EqualityUtility.generateHashCode(throwErrorOnFault)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepWaitUntilIdleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("lockstepGroupId: ");
        sb.append(this.lockstepGroupId);
        sb.append(", ");
        sb.append("throwErrorOnFault: ");
        sb.append(this.throwErrorOnFault);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepWaitUntilIdleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepWaitUntilIdleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepWaitUntilIdleRequest> PARSER =
        new zaber.motion.dto.Parser<LockstepWaitUntilIdleRequest>() {
            @Override
            public LockstepWaitUntilIdleRequest fromByteArray(byte[] data) {
                return LockstepWaitUntilIdleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepWaitUntilIdleRequest> parser() {
        return PARSER;
    }

}
