/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetAllAnalogIOResponse implements zaber.motion.dto.Message {

    private double[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    public DeviceGetAllAnalogIOResponse withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetAllAnalogIOResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetAllAnalogIOResponse(
        double[] values
    ) {
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetAllAnalogIOResponse other = (DeviceGetAllAnalogIOResponse) obj;

        return (
            EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetAllAnalogIOResponse { ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetAllAnalogIOResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetAllAnalogIOResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetAllAnalogIOResponse> PARSER =
        new zaber.motion.dto.Parser<DeviceGetAllAnalogIOResponse>() {
            @Override
            public DeviceGetAllAnalogIOResponse fromByteArray(byte[] data) {
                return DeviceGetAllAnalogIOResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetAllAnalogIOResponse> parser() {
        return PARSER;
    }

}
