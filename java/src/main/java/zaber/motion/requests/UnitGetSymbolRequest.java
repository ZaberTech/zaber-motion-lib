/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class UnitGetSymbolRequest implements zaber.motion.dto.Message {

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public UnitGetSymbolRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public UnitGetSymbolRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public UnitGetSymbolRequest(
        Units unit
    ) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        UnitGetSymbolRequest other = (UnitGetSymbolRequest) obj;

        return (
            EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnitGetSymbolRequest { ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static UnitGetSymbolRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, UnitGetSymbolRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<UnitGetSymbolRequest> PARSER =
        new zaber.motion.dto.Parser<UnitGetSymbolRequest>() {
            @Override
            public UnitGetSymbolRequest fromByteArray(byte[] data) {
                return UnitGetSymbolRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<UnitGetSymbolRequest> parser() {
        return PARSER;
    }

}
