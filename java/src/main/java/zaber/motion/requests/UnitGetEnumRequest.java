/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class UnitGetEnumRequest implements zaber.motion.dto.Message {

    private String symbol;

    @com.fasterxml.jackson.annotation.JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("symbol")
    public String getSymbol() {
        return this.symbol;
    }

    public UnitGetEnumRequest withSymbol(String aSymbol) {
        this.setSymbol(aSymbol);
        return this;
    }

    /**
     * Empty constructor.
     */
    public UnitGetEnumRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public UnitGetEnumRequest(
        String symbol
    ) {
        this.symbol = symbol;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        UnitGetEnumRequest other = (UnitGetEnumRequest) obj;

        return (
            EqualityUtility.equals(symbol, other.symbol)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(symbol)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnitGetEnumRequest { ");
        sb.append("symbol: ");
        sb.append(this.symbol);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static UnitGetEnumRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, UnitGetEnumRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<UnitGetEnumRequest> PARSER =
        new zaber.motion.dto.Parser<UnitGetEnumRequest>() {
            @Override
            public UnitGetEnumRequest fromByteArray(byte[] data) {
                return UnitGetEnumRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<UnitGetEnumRequest> parser() {
        return PARSER;
    }

}
