/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class OscilloscopeDataGetRequest implements zaber.motion.dto.Message {

    private int dataId;

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public int getDataId() {
        return this.dataId;
    }

    public OscilloscopeDataGetRequest withDataId(int aDataId) {
        this.setDataId(aDataId);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public OscilloscopeDataGetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeDataGetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeDataGetRequest(
        int dataId,
        Units unit
    ) {
        this.dataId = dataId;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeDataGetRequest other = (OscilloscopeDataGetRequest) obj;

        return (
            EqualityUtility.equals(dataId, other.dataId)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataId),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeDataGetRequest { ");
        sb.append("dataId: ");
        sb.append(this.dataId);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeDataGetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeDataGetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeDataGetRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeDataGetRequest>() {
            @Override
            public OscilloscopeDataGetRequest fromByteArray(byte[] data) {
                return OscilloscopeDataGetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeDataGetRequest> parser() {
        return PARSER;
    }

}
