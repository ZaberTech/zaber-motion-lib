/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.DigitalOutputAction;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class DeviceSetDigitalOutputScheduleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetDigitalOutputScheduleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetDigitalOutputScheduleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceSetDigitalOutputScheduleRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private DigitalOutputAction value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(DigitalOutputAction value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public DigitalOutputAction getValue() {
        return this.value;
    }

    public DeviceSetDigitalOutputScheduleRequest withValue(DigitalOutputAction aValue) {
        this.setValue(aValue);
        return this;
    }

    private DigitalOutputAction futureValue;

    @com.fasterxml.jackson.annotation.JsonProperty("futureValue")
    public void setFutureValue(DigitalOutputAction futureValue) {
        this.futureValue = futureValue;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("futureValue")
    public DigitalOutputAction getFutureValue() {
        return this.futureValue;
    }

    public DeviceSetDigitalOutputScheduleRequest withFutureValue(DigitalOutputAction aFutureValue) {
        this.setFutureValue(aFutureValue);
        return this;
    }

    private double delay;

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public void setDelay(double delay) {
        this.delay = delay;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public double getDelay() {
        return this.delay;
    }

    public DeviceSetDigitalOutputScheduleRequest withDelay(double aDelay) {
        this.setDelay(aDelay);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public DeviceSetDigitalOutputScheduleRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetDigitalOutputScheduleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetDigitalOutputScheduleRequest(
        int interfaceId,
        int device,
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelNumber = channelNumber;
        this.value = value;
        this.futureValue = futureValue;
        this.delay = delay;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetDigitalOutputScheduleRequest other = (DeviceSetDigitalOutputScheduleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(futureValue, other.futureValue)
            && EqualityUtility.equals(delay, other.delay)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(futureValue),
            EqualityUtility.generateHashCode(delay),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetDigitalOutputScheduleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("futureValue: ");
        sb.append(this.futureValue);
        sb.append(", ");
        sb.append("delay: ");
        sb.append(this.delay);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetDigitalOutputScheduleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetDigitalOutputScheduleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetDigitalOutputScheduleRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetDigitalOutputScheduleRequest>() {
            @Override
            public DeviceSetDigitalOutputScheduleRequest fromByteArray(byte[] data) {
                return DeviceSetDigitalOutputScheduleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetDigitalOutputScheduleRequest> parser() {
        return PARSER;
    }

}
