/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TranslatorSetFeedRateOverrideRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorSetFeedRateOverrideRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    private double coefficient;

    @com.fasterxml.jackson.annotation.JsonProperty("coefficient")
    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("coefficient")
    public double getCoefficient() {
        return this.coefficient;
    }

    public TranslatorSetFeedRateOverrideRequest withCoefficient(double aCoefficient) {
        this.setCoefficient(aCoefficient);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorSetFeedRateOverrideRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorSetFeedRateOverrideRequest(
        int translatorId,
        double coefficient
    ) {
        this.translatorId = translatorId;
        this.coefficient = coefficient;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorSetFeedRateOverrideRequest other = (TranslatorSetFeedRateOverrideRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            && EqualityUtility.equals(coefficient, other.coefficient)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId),
            EqualityUtility.generateHashCode(coefficient)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorSetFeedRateOverrideRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(", ");
        sb.append("coefficient: ");
        sb.append(this.coefficient);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorSetFeedRateOverrideRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorSetFeedRateOverrideRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorSetFeedRateOverrideRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorSetFeedRateOverrideRequest>() {
            @Override
            public TranslatorSetFeedRateOverrideRequest fromByteArray(byte[] data) {
                return TranslatorSetFeedRateOverrideRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorSetFeedRateOverrideRequest> parser() {
        return PARSER;
    }

}
