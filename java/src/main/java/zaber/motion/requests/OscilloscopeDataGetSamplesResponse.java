/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeDataGetSamplesResponse implements zaber.motion.dto.Message {

    private double[] data;

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(double[] data) {
        this.data = data;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public double[] getData() {
        return this.data;
    }

    public OscilloscopeDataGetSamplesResponse withData(double[] aData) {
        this.setData(aData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeDataGetSamplesResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeDataGetSamplesResponse(
        double[] data
    ) {
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeDataGetSamplesResponse other = (OscilloscopeDataGetSamplesResponse) obj;

        return (
            EqualityUtility.equals(data, other.data)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(data)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeDataGetSamplesResponse { ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeDataGetSamplesResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeDataGetSamplesResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeDataGetSamplesResponse> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeDataGetSamplesResponse>() {
            @Override
            public OscilloscopeDataGetSamplesResponse fromByteArray(byte[] data) {
                return OscilloscopeDataGetSamplesResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeDataGetSamplesResponse> parser() {
        return PARSER;
    }

}
