/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class GatewayEvent implements zaber.motion.dto.Message {

    private String event;

    @com.fasterxml.jackson.annotation.JsonProperty("event")
    public void setEvent(String event) {
        this.event = event;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("event")
    public String getEvent() {
        return this.event;
    }

    public GatewayEvent withEvent(String aEvent) {
        this.setEvent(aEvent);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GatewayEvent() {
    }

    /**
     * Constructor with all properties.
     */
    public GatewayEvent(
        String event
    ) {
        this.event = event;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GatewayEvent other = (GatewayEvent) obj;

        return (
            EqualityUtility.equals(event, other.event)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(event)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GatewayEvent { ");
        sb.append("event: ");
        sb.append(this.event);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GatewayEvent fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GatewayEvent.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GatewayEvent> PARSER =
        new zaber.motion.dto.Parser<GatewayEvent>() {
            @Override
            public GatewayEvent fromByteArray(byte[] data) {
                return GatewayEvent.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GatewayEvent> parser() {
        return PARSER;
    }

}
