/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class SetInternalModeRequest implements zaber.motion.dto.Message {

    private boolean mode;

    @com.fasterxml.jackson.annotation.JsonProperty("mode")
    public void setMode(boolean mode) {
        this.mode = mode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("mode")
    public boolean getMode() {
        return this.mode;
    }

    public SetInternalModeRequest withMode(boolean aMode) {
        this.setMode(aMode);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetInternalModeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetInternalModeRequest(
        boolean mode
    ) {
        this.mode = mode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetInternalModeRequest other = (SetInternalModeRequest) obj;

        return (
            EqualityUtility.equals(mode, other.mode)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(mode)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetInternalModeRequest { ");
        sb.append("mode: ");
        sb.append(this.mode);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetInternalModeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetInternalModeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetInternalModeRequest> PARSER =
        new zaber.motion.dto.Parser<SetInternalModeRequest>() {
            @Override
            public SetInternalModeRequest fromByteArray(byte[] data) {
                return SetInternalModeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetInternalModeRequest> parser() {
        return PARSER;
    }

}
