/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.MicroscopeConfig;
import zaber.motion.EqualityUtility;


public final class MicroscopeInitRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public MicroscopeInitRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private MicroscopeConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(MicroscopeConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public MicroscopeConfig getConfig() {
        return this.config;
    }

    public MicroscopeInitRequest withConfig(MicroscopeConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    private boolean force;

    @com.fasterxml.jackson.annotation.JsonProperty("force")
    public void setForce(boolean force) {
        this.force = force;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("force")
    public boolean getForce() {
        return this.force;
    }

    public MicroscopeInitRequest withForce(boolean aForce) {
        this.setForce(aForce);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeInitRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeInitRequest(
        int interfaceId,
        MicroscopeConfig config,
        boolean force
    ) {
        this.interfaceId = interfaceId;
        this.config = config;
        this.force = force;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeInitRequest other = (MicroscopeInitRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(config, other.config)
            && EqualityUtility.equals(force, other.force)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(config),
            EqualityUtility.generateHashCode(force)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeInitRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(", ");
        sb.append("force: ");
        sb.append(this.force);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeInitRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeInitRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeInitRequest> PARSER =
        new zaber.motion.dto.Parser<MicroscopeInitRequest>() {
            @Override
            public MicroscopeInitRequest fromByteArray(byte[] data) {
                return MicroscopeInitRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeInitRequest> parser() {
        return PARSER;
    }

}
