/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.EqualityUtility;


public final class LoadParamset implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LoadParamset withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LoadParamset withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public LoadParamset withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ServoTuningParamset toParamset;

    @com.fasterxml.jackson.annotation.JsonProperty("toParamset")
    public void setToParamset(ServoTuningParamset toParamset) {
        this.toParamset = toParamset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("toParamset")
    public ServoTuningParamset getToParamset() {
        return this.toParamset;
    }

    public LoadParamset withToParamset(ServoTuningParamset aToParamset) {
        this.setToParamset(aToParamset);
        return this;
    }

    private ServoTuningParamset fromParamset;

    @com.fasterxml.jackson.annotation.JsonProperty("fromParamset")
    public void setFromParamset(ServoTuningParamset fromParamset) {
        this.fromParamset = fromParamset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fromParamset")
    public ServoTuningParamset getFromParamset() {
        return this.fromParamset;
    }

    public LoadParamset withFromParamset(ServoTuningParamset aFromParamset) {
        this.setFromParamset(aFromParamset);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LoadParamset() {
    }

    /**
     * Constructor with all properties.
     */
    public LoadParamset(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset toParamset,
        ServoTuningParamset fromParamset
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.toParamset = toParamset;
        this.fromParamset = fromParamset;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LoadParamset other = (LoadParamset) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(toParamset, other.toParamset)
            && EqualityUtility.equals(fromParamset, other.fromParamset)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(toParamset),
            EqualityUtility.generateHashCode(fromParamset)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LoadParamset { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("toParamset: ");
        sb.append(this.toParamset);
        sb.append(", ");
        sb.append("fromParamset: ");
        sb.append(this.fromParamset);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LoadParamset fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LoadParamset.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LoadParamset> PARSER =
        new zaber.motion.dto.Parser<LoadParamset>() {
            @Override
            public LoadParamset fromByteArray(byte[] data) {
                return LoadParamset.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LoadParamset> parser() {
        return PARSER;
    }

}
