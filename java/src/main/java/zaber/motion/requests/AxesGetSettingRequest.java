/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class AxesGetSettingRequest implements zaber.motion.dto.Message {

    private int[] interfaces;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public void setInterfaces(int[] interfaces) {
        this.interfaces = interfaces;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaces")
    public int[] getInterfaces() {
        return this.interfaces;
    }

    public AxesGetSettingRequest withInterfaces(int[] aInterfaces) {
        this.setInterfaces(aInterfaces);
        return this;
    }

    private int[] devices;

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public void setDevices(int[] devices) {
        this.devices = devices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public int[] getDevices() {
        return this.devices;
    }

    public AxesGetSettingRequest withDevices(int[] aDevices) {
        this.setDevices(aDevices);
        return this;
    }

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public AxesGetSettingRequest withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private Units[] unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units[] unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units[] getUnit() {
        return this.unit;
    }

    public AxesGetSettingRequest withUnit(Units[] aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private String setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    public AxesGetSettingRequest withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxesGetSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AxesGetSettingRequest(
        int[] interfaces,
        int[] devices,
        int[] axes,
        Units[] unit,
        String setting
    ) {
        this.interfaces = interfaces;
        this.devices = devices;
        this.axes = axes;
        this.unit = unit;
        this.setting = setting;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxesGetSettingRequest other = (AxesGetSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaces, other.interfaces)
            && EqualityUtility.equals(devices, other.devices)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(setting, other.setting)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaces),
            EqualityUtility.generateHashCode(devices),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(setting)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxesGetSettingRequest { ");
        sb.append("interfaces: ");
        sb.append(this.interfaces);
        sb.append(", ");
        sb.append("devices: ");
        sb.append(this.devices);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxesGetSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxesGetSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxesGetSettingRequest> PARSER =
        new zaber.motion.dto.Parser<AxesGetSettingRequest>() {
            @Override
            public AxesGetSettingRequest fromByteArray(byte[] data) {
                return AxesGetSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxesGetSettingRequest> parser() {
        return PARSER;
    }

}
