/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ObjectiveChangerGetCurrentResponse implements zaber.motion.dto.Message {

    private int value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public int getValue() {
        return this.value;
    }

    public ObjectiveChangerGetCurrentResponse withValue(int aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ObjectiveChangerGetCurrentResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public ObjectiveChangerGetCurrentResponse(
        int value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ObjectiveChangerGetCurrentResponse other = (ObjectiveChangerGetCurrentResponse) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectiveChangerGetCurrentResponse { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ObjectiveChangerGetCurrentResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ObjectiveChangerGetCurrentResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ObjectiveChangerGetCurrentResponse> PARSER =
        new zaber.motion.dto.Parser<ObjectiveChangerGetCurrentResponse>() {
            @Override
            public ObjectiveChangerGetCurrentResponse fromByteArray(byte[] data) {
                return ObjectiveChangerGetCurrentResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ObjectiveChangerGetCurrentResponse> parser() {
        return PARSER;
    }

}
