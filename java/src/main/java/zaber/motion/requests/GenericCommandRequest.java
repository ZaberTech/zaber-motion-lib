/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class GenericCommandRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public GenericCommandRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public GenericCommandRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public GenericCommandRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String command;

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(String command) {
        this.command = command;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public String getCommand() {
        return this.command;
    }

    public GenericCommandRequest withCommand(String aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    private boolean checkErrors;

    @com.fasterxml.jackson.annotation.JsonProperty("checkErrors")
    public void setCheckErrors(boolean checkErrors) {
        this.checkErrors = checkErrors;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("checkErrors")
    public boolean getCheckErrors() {
        return this.checkErrors;
    }

    public GenericCommandRequest withCheckErrors(boolean aCheckErrors) {
        this.setCheckErrors(aCheckErrors);
        return this;
    }

    private int timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public int getTimeout() {
        return this.timeout;
    }

    public GenericCommandRequest withTimeout(int aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GenericCommandRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public GenericCommandRequest(
        int interfaceId,
        int device,
        int axis,
        String command,
        boolean checkErrors,
        int timeout
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.command = command;
        this.checkErrors = checkErrors;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GenericCommandRequest other = (GenericCommandRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(command, other.command)
            && EqualityUtility.equals(checkErrors, other.checkErrors)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(command),
            EqualityUtility.generateHashCode(checkErrors),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GenericCommandRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(", ");
        sb.append("checkErrors: ");
        sb.append(this.checkErrors);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GenericCommandRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GenericCommandRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GenericCommandRequest> PARSER =
        new zaber.motion.dto.Parser<GenericCommandRequest>() {
            @Override
            public GenericCommandRequest fromByteArray(byte[] data) {
                return GenericCommandRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GenericCommandRequest> parser() {
        return PARSER;
    }

}
