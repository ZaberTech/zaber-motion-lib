/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeAddSettingChannelRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public OscilloscopeAddSettingChannelRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public OscilloscopeAddSettingChannelRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public OscilloscopeAddSettingChannelRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    public OscilloscopeAddSettingChannelRequest withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeAddSettingChannelRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeAddSettingChannelRequest(
        int interfaceId,
        int device,
        int axis,
        String setting
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.setting = setting;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeAddSettingChannelRequest other = (OscilloscopeAddSettingChannelRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(setting, other.setting)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(setting)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeAddSettingChannelRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeAddSettingChannelRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeAddSettingChannelRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeAddSettingChannelRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeAddSettingChannelRequest>() {
            @Override
            public OscilloscopeAddSettingChannelRequest fromByteArray(byte[] data) {
                return OscilloscopeAddSettingChannelRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeAddSettingChannelRequest> parser() {
        return PARSER;
    }

}
