/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class InterfaceEmptyRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public InterfaceEmptyRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public InterfaceEmptyRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public InterfaceEmptyRequest(
        int interfaceId
    ) {
        this.interfaceId = interfaceId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        InterfaceEmptyRequest other = (InterfaceEmptyRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("InterfaceEmptyRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static InterfaceEmptyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, InterfaceEmptyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<InterfaceEmptyRequest> PARSER =
        new zaber.motion.dto.Parser<InterfaceEmptyRequest>() {
            @Override
            public InterfaceEmptyRequest fromByteArray(byte[] data) {
                return InterfaceEmptyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<InterfaceEmptyRequest> parser() {
        return PARSER;
    }

}
