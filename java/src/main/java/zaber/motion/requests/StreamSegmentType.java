/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

public enum StreamSegmentType {

    ABS(0),

    REL(1);

    private int value;

    StreamSegmentType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static StreamSegmentType valueOf(int argValue) {
        for (StreamSegmentType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
