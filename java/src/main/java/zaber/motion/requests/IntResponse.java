/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class IntResponse implements zaber.motion.dto.Message {

    private int value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public int getValue() {
        return this.value;
    }

    public IntResponse withValue(int aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public IntResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public IntResponse(
        int value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        IntResponse other = (IntResponse) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IntResponse { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static IntResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, IntResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<IntResponse> PARSER =
        new zaber.motion.dto.Parser<IntResponse>() {
            @Override
            public IntResponse fromByteArray(byte[] data) {
                return IntResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<IntResponse> parser() {
        return PARSER;
    }

}
