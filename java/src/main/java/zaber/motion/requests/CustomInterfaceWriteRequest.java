/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CustomInterfaceWriteRequest implements zaber.motion.dto.Message {

    private int transportId;

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public int getTransportId() {
        return this.transportId;
    }

    public CustomInterfaceWriteRequest withTransportId(int aTransportId) {
        this.setTransportId(aTransportId);
        return this;
    }

    private String message;

    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public String getMessage() {
        return this.message;
    }

    public CustomInterfaceWriteRequest withMessage(String aMessage) {
        this.setMessage(aMessage);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CustomInterfaceWriteRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public CustomInterfaceWriteRequest(
        int transportId,
        String message
    ) {
        this.transportId = transportId;
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CustomInterfaceWriteRequest other = (CustomInterfaceWriteRequest) obj;

        return (
            EqualityUtility.equals(transportId, other.transportId)
            && EqualityUtility.equals(message, other.message)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(transportId),
            EqualityUtility.generateHashCode(message)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CustomInterfaceWriteRequest { ");
        sb.append("transportId: ");
        sb.append(this.transportId);
        sb.append(", ");
        sb.append("message: ");
        sb.append(this.message);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CustomInterfaceWriteRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CustomInterfaceWriteRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CustomInterfaceWriteRequest> PARSER =
        new zaber.motion.dto.Parser<CustomInterfaceWriteRequest>() {
            @Override
            public CustomInterfaceWriteRequest fromByteArray(byte[] data) {
                return CustomInterfaceWriteRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CustomInterfaceWriteRequest> parser() {
        return PARSER;
    }

}
