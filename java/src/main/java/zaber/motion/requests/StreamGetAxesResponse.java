/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.StreamAxisDefinition;
import zaber.motion.ascii.PvtAxisDefinition;
import zaber.motion.EqualityUtility;


public final class StreamGetAxesResponse implements zaber.motion.dto.Message {

    private StreamAxisDefinition[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(StreamAxisDefinition[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public StreamAxisDefinition[] getAxes() {
        return this.axes;
    }

    public StreamGetAxesResponse withAxes(StreamAxisDefinition[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private PvtAxisDefinition[] pvtAxes;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public void setPvtAxes(PvtAxisDefinition[] pvtAxes) {
        this.pvtAxes = pvtAxes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public PvtAxisDefinition[] getPvtAxes() {
        return this.pvtAxes;
    }

    public StreamGetAxesResponse withPvtAxes(PvtAxisDefinition[] aPvtAxes) {
        this.setPvtAxes(aPvtAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamGetAxesResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamGetAxesResponse(
        StreamAxisDefinition[] axes,
        PvtAxisDefinition[] pvtAxes
    ) {
        this.axes = axes;
        this.pvtAxes = pvtAxes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamGetAxesResponse other = (StreamGetAxesResponse) obj;

        return (
            EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(pvtAxes, other.pvtAxes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(pvtAxes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamGetAxesResponse { ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("pvtAxes: ");
        sb.append(this.pvtAxes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamGetAxesResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamGetAxesResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamGetAxesResponse> PARSER =
        new zaber.motion.dto.Parser<StreamGetAxesResponse>() {
            @Override
            public StreamGetAxesResponse fromByteArray(byte[] data) {
                return StreamGetAxesResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamGetAxesResponse> parser() {
        return PARSER;
    }

}
