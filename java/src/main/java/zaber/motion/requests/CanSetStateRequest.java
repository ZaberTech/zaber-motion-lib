/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.FirmwareVersion;
import zaber.motion.EqualityUtility;


public final class CanSetStateRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public CanSetStateRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public CanSetStateRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public CanSetStateRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String state;

    @com.fasterxml.jackson.annotation.JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("state")
    public String getState() {
        return this.state;
    }

    public CanSetStateRequest withState(String aState) {
        this.setState(aState);
        return this;
    }

    private FirmwareVersion firmwareVersion;

    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public void setFirmwareVersion(FirmwareVersion firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public FirmwareVersion getFirmwareVersion() {
        return this.firmwareVersion;
    }

    public CanSetStateRequest withFirmwareVersion(FirmwareVersion aFirmwareVersion) {
        this.setFirmwareVersion(aFirmwareVersion);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CanSetStateRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public CanSetStateRequest(
        int interfaceId,
        int device,
        int axis,
        String state,
        FirmwareVersion firmwareVersion
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.state = state;
        this.firmwareVersion = firmwareVersion;
    }

    /**
     * Constructor with only required properties.
     */
    public CanSetStateRequest(
        int interfaceId,
        int device,
        int axis,
        String state
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.state = state;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CanSetStateRequest other = (CanSetStateRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(state, other.state)
            && EqualityUtility.equals(firmwareVersion, other.firmwareVersion)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(state),
            EqualityUtility.generateHashCode(firmwareVersion)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CanSetStateRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("state: ");
        sb.append(this.state);
        sb.append(", ");
        sb.append("firmwareVersion: ");
        sb.append(this.firmwareVersion);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CanSetStateRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CanSetStateRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CanSetStateRequest> PARSER =
        new zaber.motion.dto.Parser<CanSetStateRequest>() {
            @Override
            public CanSetStateRequest fromByteArray(byte[] data) {
                return CanSetStateRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CanSetStateRequest> parser() {
        return PARSER;
    }

}
