/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class StreamLineRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamLineRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamLineRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamLineRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamLineRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private StreamSegmentType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(StreamSegmentType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public StreamSegmentType getType() {
        return this.type;
    }

    public StreamLineRequest withType(StreamSegmentType aType) {
        this.setType(aType);
        return this;
    }

    private Measurement[] endpoint;

    @com.fasterxml.jackson.annotation.JsonProperty("endpoint")
    public void setEndpoint(Measurement[] endpoint) {
        this.endpoint = endpoint;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("endpoint")
    public Measurement[] getEndpoint() {
        return this.endpoint;
    }

    public StreamLineRequest withEndpoint(Measurement[] aEndpoint) {
        this.setEndpoint(aEndpoint);
        return this;
    }

    private int[] targetAxesIndices;

    @com.fasterxml.jackson.annotation.JsonProperty("targetAxesIndices")
    public void setTargetAxesIndices(int[] targetAxesIndices) {
        this.targetAxesIndices = targetAxesIndices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("targetAxesIndices")
    public int[] getTargetAxesIndices() {
        return this.targetAxesIndices;
    }

    public StreamLineRequest withTargetAxesIndices(int[] aTargetAxesIndices) {
        this.setTargetAxesIndices(aTargetAxesIndices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamLineRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamLineRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        StreamSegmentType type,
        Measurement[] endpoint,
        int[] targetAxesIndices
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.type = type;
        this.endpoint = endpoint;
        this.targetAxesIndices = targetAxesIndices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamLineRequest other = (StreamLineRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(endpoint, other.endpoint)
            && EqualityUtility.equals(targetAxesIndices, other.targetAxesIndices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(endpoint),
            EqualityUtility.generateHashCode(targetAxesIndices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamLineRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("endpoint: ");
        sb.append(this.endpoint);
        sb.append(", ");
        sb.append("targetAxesIndices: ");
        sb.append(this.targetAxesIndices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamLineRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamLineRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamLineRequest> PARSER =
        new zaber.motion.dto.Parser<StreamLineRequest>() {
            @Override
            public StreamLineRequest fromByteArray(byte[] data) {
                return StreamLineRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamLineRequest> parser() {
        return PARSER;
    }

}
