/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.AlertEvent;
import zaber.motion.EqualityUtility;


public final class AlertEventWrapper implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public AlertEventWrapper withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private AlertEvent alert;

    @com.fasterxml.jackson.annotation.JsonProperty("alert")
    public void setAlert(AlertEvent alert) {
        this.alert = alert;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("alert")
    public AlertEvent getAlert() {
        return this.alert;
    }

    public AlertEventWrapper withAlert(AlertEvent aAlert) {
        this.setAlert(aAlert);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AlertEventWrapper() {
    }

    /**
     * Constructor with all properties.
     */
    public AlertEventWrapper(
        int interfaceId,
        AlertEvent alert
    ) {
        this.interfaceId = interfaceId;
        this.alert = alert;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AlertEventWrapper other = (AlertEventWrapper) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(alert, other.alert)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(alert)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AlertEventWrapper { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("alert: ");
        sb.append(this.alert);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AlertEventWrapper fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AlertEventWrapper.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AlertEventWrapper> PARSER =
        new zaber.motion.dto.Parser<AlertEventWrapper>() {
            @Override
            public AlertEventWrapper fromByteArray(byte[] data) {
                return AlertEventWrapper.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AlertEventWrapper> parser() {
        return PARSER;
    }

}
