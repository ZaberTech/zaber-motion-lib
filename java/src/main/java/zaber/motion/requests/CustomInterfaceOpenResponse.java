/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CustomInterfaceOpenResponse implements zaber.motion.dto.Message {

    private int transportId;

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public int getTransportId() {
        return this.transportId;
    }

    public CustomInterfaceOpenResponse withTransportId(int aTransportId) {
        this.setTransportId(aTransportId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CustomInterfaceOpenResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public CustomInterfaceOpenResponse(
        int transportId
    ) {
        this.transportId = transportId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CustomInterfaceOpenResponse other = (CustomInterfaceOpenResponse) obj;

        return (
            EqualityUtility.equals(transportId, other.transportId)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(transportId)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CustomInterfaceOpenResponse { ");
        sb.append("transportId: ");
        sb.append(this.transportId);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CustomInterfaceOpenResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CustomInterfaceOpenResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CustomInterfaceOpenResponse> PARSER =
        new zaber.motion.dto.Parser<CustomInterfaceOpenResponse>() {
            @Override
            public CustomInterfaceOpenResponse fromByteArray(byte[] data) {
                return CustomInterfaceOpenResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CustomInterfaceOpenResponse> parser() {
        return PARSER;
    }

}
