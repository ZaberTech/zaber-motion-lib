/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TriggerSetLabelRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerSetLabelRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerSetLabelRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerSetLabelRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private String label;

    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public String getLabel() {
        return this.label;
    }

    public TriggerSetLabelRequest withLabel(String aLabel) {
        this.setLabel(aLabel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerSetLabelRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerSetLabelRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        String label
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.label = label;
    }

    /**
     * Constructor with only required properties.
     */
    public TriggerSetLabelRequest(
        int interfaceId,
        int device,
        int triggerNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerSetLabelRequest other = (TriggerSetLabelRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(label, other.label)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(label)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerSetLabelRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("label: ");
        sb.append(this.label);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerSetLabelRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerSetLabelRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerSetLabelRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerSetLabelRequest>() {
            @Override
            public TriggerSetLabelRequest fromByteArray(byte[] data) {
                return TriggerSetLabelRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerSetLabelRequest> parser() {
        return PARSER;
    }

}
