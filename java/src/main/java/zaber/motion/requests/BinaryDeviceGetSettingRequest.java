/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.BinarySettings;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class BinaryDeviceGetSettingRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryDeviceGetSettingRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public BinaryDeviceGetSettingRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private BinarySettings setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(BinarySettings setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public BinarySettings getSetting() {
        return this.setting;
    }

    public BinaryDeviceGetSettingRequest withSetting(BinarySettings aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public BinaryDeviceGetSettingRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryDeviceGetSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryDeviceGetSettingRequest(
        int interfaceId,
        int device,
        BinarySettings setting,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.setting = setting;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryDeviceGetSettingRequest other = (BinaryDeviceGetSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryDeviceGetSettingRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryDeviceGetSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryDeviceGetSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryDeviceGetSettingRequest> PARSER =
        new zaber.motion.dto.Parser<BinaryDeviceGetSettingRequest>() {
            @Override
            public BinaryDeviceGetSettingRequest fromByteArray(byte[] data) {
                return BinaryDeviceGetSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryDeviceGetSettingRequest> parser() {
        return PARSER;
    }

}
