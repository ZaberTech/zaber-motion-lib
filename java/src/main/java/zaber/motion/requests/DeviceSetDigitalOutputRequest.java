/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.DigitalOutputAction;
import zaber.motion.EqualityUtility;


public final class DeviceSetDigitalOutputRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetDigitalOutputRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetDigitalOutputRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceSetDigitalOutputRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private DigitalOutputAction value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(DigitalOutputAction value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public DigitalOutputAction getValue() {
        return this.value;
    }

    public DeviceSetDigitalOutputRequest withValue(DigitalOutputAction aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetDigitalOutputRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetDigitalOutputRequest(
        int interfaceId,
        int device,
        int channelNumber,
        DigitalOutputAction value
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelNumber = channelNumber;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetDigitalOutputRequest other = (DeviceSetDigitalOutputRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetDigitalOutputRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetDigitalOutputRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetDigitalOutputRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetDigitalOutputRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetDigitalOutputRequest>() {
            @Override
            public DeviceSetDigitalOutputRequest fromByteArray(byte[] data) {
                return DeviceSetDigitalOutputRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetDigitalOutputRequest> parser() {
        return PARSER;
    }

}
