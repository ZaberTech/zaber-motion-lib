/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ChannelSetIntensity implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ChannelSetIntensity withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public ChannelSetIntensity withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public ChannelSetIntensity withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double intensity;

    @com.fasterxml.jackson.annotation.JsonProperty("intensity")
    public void setIntensity(double intensity) {
        this.intensity = intensity;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("intensity")
    public double getIntensity() {
        return this.intensity;
    }

    public ChannelSetIntensity withIntensity(double aIntensity) {
        this.setIntensity(aIntensity);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ChannelSetIntensity() {
    }

    /**
     * Constructor with all properties.
     */
    public ChannelSetIntensity(
        int interfaceId,
        int device,
        int axis,
        double intensity
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.intensity = intensity;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ChannelSetIntensity other = (ChannelSetIntensity) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(intensity, other.intensity)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(intensity)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChannelSetIntensity { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("intensity: ");
        sb.append(this.intensity);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ChannelSetIntensity fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ChannelSetIntensity.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ChannelSetIntensity> PARSER =
        new zaber.motion.dto.Parser<ChannelSetIntensity>() {
            @Override
            public ChannelSetIntensity fromByteArray(byte[] data) {
                return ChannelSetIntensity.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ChannelSetIntensity> parser() {
        return PARSER;
    }

}
