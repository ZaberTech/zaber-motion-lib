/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TestResponse implements zaber.motion.dto.Message {

    private String dataPong;

    @com.fasterxml.jackson.annotation.JsonProperty("dataPong")
    public void setDataPong(String dataPong) {
        this.dataPong = dataPong;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataPong")
    public String getDataPong() {
        return this.dataPong;
    }

    public TestResponse withDataPong(String aDataPong) {
        this.setDataPong(aDataPong);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TestResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public TestResponse(
        String dataPong
    ) {
        this.dataPong = dataPong;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TestResponse other = (TestResponse) obj;

        return (
            EqualityUtility.equals(dataPong, other.dataPong)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataPong)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TestResponse { ");
        sb.append("dataPong: ");
        sb.append(this.dataPong);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TestResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TestResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TestResponse> PARSER =
        new zaber.motion.dto.Parser<TestResponse>() {
            @Override
            public TestResponse fromByteArray(byte[] data) {
                return TestResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TestResponse> parser() {
        return PARSER;
    }

}
