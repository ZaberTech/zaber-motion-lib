/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class AutofocusFocusRequest implements zaber.motion.dto.Message {

    private int providerId;

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public int getProviderId() {
        return this.providerId;
    }

    public AutofocusFocusRequest withProviderId(int aProviderId) {
        this.setProviderId(aProviderId);
        return this;
    }

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public AutofocusFocusRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public AutofocusFocusRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public AutofocusFocusRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public AutofocusFocusRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    private boolean scan;

    @com.fasterxml.jackson.annotation.JsonProperty("scan")
    public void setScan(boolean scan) {
        this.scan = scan;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("scan")
    public boolean getScan() {
        return this.scan;
    }

    public AutofocusFocusRequest withScan(boolean aScan) {
        this.setScan(aScan);
        return this;
    }

    private boolean once;

    @com.fasterxml.jackson.annotation.JsonProperty("once")
    public void setOnce(boolean once) {
        this.once = once;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("once")
    public boolean getOnce() {
        return this.once;
    }

    public AutofocusFocusRequest withOnce(boolean aOnce) {
        this.setOnce(aOnce);
        return this;
    }

    private int timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public int getTimeout() {
        return this.timeout;
    }

    public AutofocusFocusRequest withTimeout(int aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AutofocusFocusRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AutofocusFocusRequest(
        int providerId,
        int interfaceId,
        int focusAddress,
        int focusAxis,
        int turretAddress,
        boolean scan,
        boolean once,
        int timeout
    ) {
        this.providerId = providerId;
        this.interfaceId = interfaceId;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.turretAddress = turretAddress;
        this.scan = scan;
        this.once = once;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AutofocusFocusRequest other = (AutofocusFocusRequest) obj;

        return (
            EqualityUtility.equals(providerId, other.providerId)
            && EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            && EqualityUtility.equals(scan, other.scan)
            && EqualityUtility.equals(once, other.once)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(providerId),
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(turretAddress),
            EqualityUtility.generateHashCode(scan),
            EqualityUtility.generateHashCode(once),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AutofocusFocusRequest { ");
        sb.append("providerId: ");
        sb.append(this.providerId);
        sb.append(", ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(", ");
        sb.append("scan: ");
        sb.append(this.scan);
        sb.append(", ");
        sb.append("once: ");
        sb.append(this.once);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AutofocusFocusRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AutofocusFocusRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AutofocusFocusRequest> PARSER =
        new zaber.motion.dto.Parser<AutofocusFocusRequest>() {
            @Override
            public AutofocusFocusRequest fromByteArray(byte[] data) {
                return AutofocusFocusRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AutofocusFocusRequest> parser() {
        return PARSER;
    }

}
