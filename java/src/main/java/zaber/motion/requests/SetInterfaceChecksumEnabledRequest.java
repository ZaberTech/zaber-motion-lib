/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class SetInterfaceChecksumEnabledRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetInterfaceChecksumEnabledRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private boolean isEnabled;

    @com.fasterxml.jackson.annotation.JsonProperty("isEnabled")
    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("isEnabled")
    public boolean getIsEnabled() {
        return this.isEnabled;
    }

    public SetInterfaceChecksumEnabledRequest withIsEnabled(boolean aIsEnabled) {
        this.setIsEnabled(aIsEnabled);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetInterfaceChecksumEnabledRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetInterfaceChecksumEnabledRequest(
        int interfaceId,
        boolean isEnabled
    ) {
        this.interfaceId = interfaceId;
        this.isEnabled = isEnabled;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetInterfaceChecksumEnabledRequest other = (SetInterfaceChecksumEnabledRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(isEnabled, other.isEnabled)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(isEnabled)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetInterfaceChecksumEnabledRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("isEnabled: ");
        sb.append(this.isEnabled);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetInterfaceChecksumEnabledRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetInterfaceChecksumEnabledRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetInterfaceChecksumEnabledRequest> PARSER =
        new zaber.motion.dto.Parser<SetInterfaceChecksumEnabledRequest>() {
            @Override
            public SetInterfaceChecksumEnabledRequest fromByteArray(byte[] data) {
                return SetInterfaceChecksumEnabledRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetInterfaceChecksumEnabledRequest> parser() {
        return PARSER;
    }

}
