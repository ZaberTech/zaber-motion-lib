/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.gcode.TranslatorConfig;
import zaber.motion.EqualityUtility;


public final class TranslatorCreateFromDeviceRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TranslatorCreateFromDeviceRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TranslatorCreateFromDeviceRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public TranslatorCreateFromDeviceRequest withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private TranslatorConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(TranslatorConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public TranslatorConfig getConfig() {
        return this.config;
    }

    public TranslatorCreateFromDeviceRequest withConfig(TranslatorConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorCreateFromDeviceRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorCreateFromDeviceRequest(
        int interfaceId,
        int device,
        int[] axes,
        TranslatorConfig config
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axes = axes;
        this.config = config;
    }

    /**
     * Constructor with only required properties.
     */
    public TranslatorCreateFromDeviceRequest(
        int interfaceId,
        int device,
        int[] axes
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axes = axes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorCreateFromDeviceRequest other = (TranslatorCreateFromDeviceRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(config, other.config)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(config)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorCreateFromDeviceRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorCreateFromDeviceRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorCreateFromDeviceRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorCreateFromDeviceRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorCreateFromDeviceRequest>() {
            @Override
            public TranslatorCreateFromDeviceRequest fromByteArray(byte[] data) {
                return TranslatorCreateFromDeviceRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorCreateFromDeviceRequest> parser() {
        return PARSER;
    }

}
