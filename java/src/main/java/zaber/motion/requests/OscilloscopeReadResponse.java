/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeReadResponse implements zaber.motion.dto.Message {

    private int[] dataIds;

    @com.fasterxml.jackson.annotation.JsonProperty("dataIds")
    public void setDataIds(int[] dataIds) {
        this.dataIds = dataIds;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataIds")
    public int[] getDataIds() {
        return this.dataIds;
    }

    public OscilloscopeReadResponse withDataIds(int[] aDataIds) {
        this.setDataIds(aDataIds);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeReadResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeReadResponse(
        int[] dataIds
    ) {
        this.dataIds = dataIds;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeReadResponse other = (OscilloscopeReadResponse) obj;

        return (
            EqualityUtility.equals(dataIds, other.dataIds)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataIds)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeReadResponse { ");
        sb.append("dataIds: ");
        sb.append(this.dataIds);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeReadResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeReadResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeReadResponse> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeReadResponse>() {
            @Override
            public OscilloscopeReadResponse fromByteArray(byte[] data) {
                return OscilloscopeReadResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeReadResponse> parser() {
        return PARSER;
    }

}
