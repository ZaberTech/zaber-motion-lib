/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetAllDigitalIOResponse implements zaber.motion.dto.Message {

    private boolean[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(boolean[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public boolean[] getValues() {
        return this.values;
    }

    public DeviceGetAllDigitalIOResponse withValues(boolean[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetAllDigitalIOResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetAllDigitalIOResponse(
        boolean[] values
    ) {
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetAllDigitalIOResponse other = (DeviceGetAllDigitalIOResponse) obj;

        return (
            EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetAllDigitalIOResponse { ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetAllDigitalIOResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetAllDigitalIOResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetAllDigitalIOResponse> PARSER =
        new zaber.motion.dto.Parser<DeviceGetAllDigitalIOResponse>() {
            @Override
            public DeviceGetAllDigitalIOResponse fromByteArray(byte[] data) {
                return DeviceGetAllDigitalIOResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetAllDigitalIOResponse> parser() {
        return PARSER;
    }

}
