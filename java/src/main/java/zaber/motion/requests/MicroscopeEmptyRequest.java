/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.MicroscopeConfig;
import zaber.motion.EqualityUtility;


public final class MicroscopeEmptyRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public MicroscopeEmptyRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private MicroscopeConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(MicroscopeConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public MicroscopeConfig getConfig() {
        return this.config;
    }

    public MicroscopeEmptyRequest withConfig(MicroscopeConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeEmptyRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeEmptyRequest(
        int interfaceId,
        MicroscopeConfig config
    ) {
        this.interfaceId = interfaceId;
        this.config = config;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeEmptyRequest other = (MicroscopeEmptyRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(config, other.config)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(config)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeEmptyRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeEmptyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeEmptyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeEmptyRequest> PARSER =
        new zaber.motion.dto.Parser<MicroscopeEmptyRequest>() {
            @Override
            public MicroscopeEmptyRequest fromByteArray(byte[] data) {
                return MicroscopeEmptyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeEmptyRequest> parser() {
        return PARSER;
    }

}
