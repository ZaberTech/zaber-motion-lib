/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.product.ProcessControllerSource;
import zaber.motion.EqualityUtility;


public final class SetProcessControllerSource implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetProcessControllerSource withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetProcessControllerSource withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public SetProcessControllerSource withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ProcessControllerSource source;

    @com.fasterxml.jackson.annotation.JsonProperty("source")
    public void setSource(ProcessControllerSource source) {
        this.source = source;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("source")
    public ProcessControllerSource getSource() {
        return this.source;
    }

    public SetProcessControllerSource withSource(ProcessControllerSource aSource) {
        this.setSource(aSource);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetProcessControllerSource() {
    }

    /**
     * Constructor with all properties.
     */
    public SetProcessControllerSource(
        int interfaceId,
        int device,
        int axis,
        ProcessControllerSource source
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.source = source;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetProcessControllerSource other = (SetProcessControllerSource) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(source, other.source)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(source)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetProcessControllerSource { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("source: ");
        sb.append(this.source);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetProcessControllerSource fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetProcessControllerSource.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetProcessControllerSource> PARSER =
        new zaber.motion.dto.Parser<SetProcessControllerSource>() {
            @Override
            public SetProcessControllerSource fromByteArray(byte[] data) {
                return SetProcessControllerSource.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetProcessControllerSource> parser() {
        return PARSER;
    }

}
