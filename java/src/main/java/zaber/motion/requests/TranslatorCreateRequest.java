/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.gcode.DeviceDefinition;
import zaber.motion.gcode.TranslatorConfig;
import zaber.motion.EqualityUtility;


public final class TranslatorCreateRequest implements zaber.motion.dto.Message {

    private DeviceDefinition definition;

    @com.fasterxml.jackson.annotation.JsonProperty("definition")
    public void setDefinition(DeviceDefinition definition) {
        this.definition = definition;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("definition")
    public DeviceDefinition getDefinition() {
        return this.definition;
    }

    public TranslatorCreateRequest withDefinition(DeviceDefinition aDefinition) {
        this.setDefinition(aDefinition);
        return this;
    }

    private TranslatorConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(TranslatorConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public TranslatorConfig getConfig() {
        return this.config;
    }

    public TranslatorCreateRequest withConfig(TranslatorConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorCreateRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorCreateRequest(
        DeviceDefinition definition,
        TranslatorConfig config
    ) {
        this.definition = definition;
        this.config = config;
    }

    /**
     * Constructor with only required properties.
     */
    public TranslatorCreateRequest(
        DeviceDefinition definition
    ) {
        this.definition = definition;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorCreateRequest other = (TranslatorCreateRequest) obj;

        return (
            EqualityUtility.equals(definition, other.definition)
            && EqualityUtility.equals(config, other.config)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(definition),
            EqualityUtility.generateHashCode(config)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorCreateRequest { ");
        sb.append("definition: ");
        sb.append(this.definition);
        sb.append(", ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorCreateRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorCreateRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorCreateRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorCreateRequest>() {
            @Override
            public TranslatorCreateRequest fromByteArray(byte[] data) {
                return TranslatorCreateRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorCreateRequest> parser() {
        return PARSER;
    }

}
