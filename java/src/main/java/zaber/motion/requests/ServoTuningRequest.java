/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.EqualityUtility;


public final class ServoTuningRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ServoTuningRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public ServoTuningRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public ServoTuningRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ServoTuningParamset paramset;

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public void setParamset(ServoTuningParamset paramset) {
        this.paramset = paramset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public ServoTuningParamset getParamset() {
        return this.paramset;
    }

    public ServoTuningRequest withParamset(ServoTuningParamset aParamset) {
        this.setParamset(aParamset);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ServoTuningRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ServoTuningRequest(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset paramset
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.paramset = paramset;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ServoTuningRequest other = (ServoTuningRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(paramset, other.paramset)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(paramset)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ServoTuningRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("paramset: ");
        sb.append(this.paramset);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ServoTuningRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ServoTuningRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ServoTuningRequest> PARSER =
        new zaber.motion.dto.Parser<ServoTuningRequest>() {
            @Override
            public ServoTuningRequest fromByteArray(byte[] data) {
                return ServoTuningRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ServoTuningRequest> parser() {
        return PARSER;
    }

}
