/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetWarningsResponse implements zaber.motion.dto.Message {

    private String[] flags;

    @com.fasterxml.jackson.annotation.JsonProperty("flags")
    public void setFlags(String[] flags) {
        this.flags = flags;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("flags")
    public String[] getFlags() {
        return this.flags;
    }

    public DeviceGetWarningsResponse withFlags(String[] aFlags) {
        this.setFlags(aFlags);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetWarningsResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetWarningsResponse(
        String[] flags
    ) {
        this.flags = flags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetWarningsResponse other = (DeviceGetWarningsResponse) obj;

        return (
            EqualityUtility.equals(flags, other.flags)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(flags)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetWarningsResponse { ");
        sb.append("flags: ");
        sb.append(this.flags);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetWarningsResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetWarningsResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetWarningsResponse> PARSER =
        new zaber.motion.dto.Parser<DeviceGetWarningsResponse>() {
            @Override
            public DeviceGetWarningsResponse fromByteArray(byte[] data) {
                return DeviceGetWarningsResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetWarningsResponse> parser() {
        return PARSER;
    }

}
