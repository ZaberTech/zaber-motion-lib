/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceDetectRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceDetectRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private boolean identifyDevices;

    @com.fasterxml.jackson.annotation.JsonProperty("identifyDevices")
    public void setIdentifyDevices(boolean identifyDevices) {
        this.identifyDevices = identifyDevices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("identifyDevices")
    public boolean getIdentifyDevices() {
        return this.identifyDevices;
    }

    public DeviceDetectRequest withIdentifyDevices(boolean aIdentifyDevices) {
        this.setIdentifyDevices(aIdentifyDevices);
        return this;
    }

    private DeviceType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(DeviceType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public DeviceType getType() {
        return this.type;
    }

    public DeviceDetectRequest withType(DeviceType aType) {
        this.setType(aType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceDetectRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceDetectRequest(
        int interfaceId,
        boolean identifyDevices,
        DeviceType type
    ) {
        this.interfaceId = interfaceId;
        this.identifyDevices = identifyDevices;
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceDetectRequest other = (DeviceDetectRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(identifyDevices, other.identifyDevices)
            && EqualityUtility.equals(type, other.type)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(identifyDevices),
            EqualityUtility.generateHashCode(type)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceDetectRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("identifyDevices: ");
        sb.append(this.identifyDevices);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceDetectRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceDetectRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceDetectRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceDetectRequest>() {
            @Override
            public DeviceDetectRequest fromByteArray(byte[] data) {
                return DeviceDetectRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceDetectRequest> parser() {
        return PARSER;
    }

}
