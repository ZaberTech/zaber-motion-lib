/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TriggerEnableRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerEnableRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerEnableRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerEnableRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private int count;

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public void setCount(int count) {
        this.count = count;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("count")
    public int getCount() {
        return this.count;
    }

    public TriggerEnableRequest withCount(int aCount) {
        this.setCount(aCount);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerEnableRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerEnableRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        int count
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.count = count;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerEnableRequest other = (TriggerEnableRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(count, other.count)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(count)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerEnableRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("count: ");
        sb.append(this.count);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerEnableRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerEnableRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerEnableRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerEnableRequest>() {
            @Override
            public TriggerEnableRequest fromByteArray(byte[] data) {
                return TriggerEnableRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerEnableRequest> parser() {
        return PARSER;
    }

}
