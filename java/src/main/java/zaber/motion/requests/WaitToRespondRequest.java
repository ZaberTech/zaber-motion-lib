/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class WaitToRespondRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public WaitToRespondRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public WaitToRespondRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public WaitToRespondRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public WaitToRespondRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public WaitToRespondRequest(
        int interfaceId,
        int device,
        double timeout
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WaitToRespondRequest other = (WaitToRespondRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WaitToRespondRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static WaitToRespondRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, WaitToRespondRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<WaitToRespondRequest> PARSER =
        new zaber.motion.dto.Parser<WaitToRespondRequest>() {
            @Override
            public WaitToRespondRequest fromByteArray(byte[] data) {
                return WaitToRespondRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<WaitToRespondRequest> parser() {
        return PARSER;
    }

}
