/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.IoPortType;
import zaber.motion.EqualityUtility;


public final class SetIoPortLabelRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetIoPortLabelRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetIoPortLabelRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private IoPortType portType;

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public void setPortType(IoPortType portType) {
        this.portType = portType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public IoPortType getPortType() {
        return this.portType;
    }

    public SetIoPortLabelRequest withPortType(IoPortType aPortType) {
        this.setPortType(aPortType);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public SetIoPortLabelRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private String label;

    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public String getLabel() {
        return this.label;
    }

    public SetIoPortLabelRequest withLabel(String aLabel) {
        this.setLabel(aLabel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetIoPortLabelRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetIoPortLabelRequest(
        int interfaceId,
        int device,
        IoPortType portType,
        int channelNumber,
        String label
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.portType = portType;
        this.channelNumber = channelNumber;
        this.label = label;
    }

    /**
     * Constructor with only required properties.
     */
    public SetIoPortLabelRequest(
        int interfaceId,
        int device,
        IoPortType portType,
        int channelNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.portType = portType;
        this.channelNumber = channelNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetIoPortLabelRequest other = (SetIoPortLabelRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(portType, other.portType)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(label, other.label)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(portType),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(label)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetIoPortLabelRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("portType: ");
        sb.append(this.portType);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("label: ");
        sb.append(this.label);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetIoPortLabelRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetIoPortLabelRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetIoPortLabelRequest> PARSER =
        new zaber.motion.dto.Parser<SetIoPortLabelRequest>() {
            @Override
            public SetIoPortLabelRequest fromByteArray(byte[] data) {
                return SetIoPortLabelRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetIoPortLabelRequest> parser() {
        return PARSER;
    }

}
