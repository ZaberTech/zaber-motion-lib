/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.IoPortType;
import zaber.motion.EqualityUtility;


public final class OscilloscopeAddIoChannelRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public OscilloscopeAddIoChannelRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public OscilloscopeAddIoChannelRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private IoPortType ioType;

    @com.fasterxml.jackson.annotation.JsonProperty("ioType")
    public void setIoType(IoPortType ioType) {
        this.ioType = ioType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("ioType")
    public IoPortType getIoType() {
        return this.ioType;
    }

    public OscilloscopeAddIoChannelRequest withIoType(IoPortType aIoType) {
        this.setIoType(aIoType);
        return this;
    }

    private int ioChannel;

    @com.fasterxml.jackson.annotation.JsonProperty("ioChannel")
    public void setIoChannel(int ioChannel) {
        this.ioChannel = ioChannel;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("ioChannel")
    public int getIoChannel() {
        return this.ioChannel;
    }

    public OscilloscopeAddIoChannelRequest withIoChannel(int aIoChannel) {
        this.setIoChannel(aIoChannel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeAddIoChannelRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeAddIoChannelRequest(
        int interfaceId,
        int device,
        IoPortType ioType,
        int ioChannel
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.ioType = ioType;
        this.ioChannel = ioChannel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeAddIoChannelRequest other = (OscilloscopeAddIoChannelRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(ioType, other.ioType)
            && EqualityUtility.equals(ioChannel, other.ioChannel)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(ioType),
            EqualityUtility.generateHashCode(ioChannel)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeAddIoChannelRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("ioType: ");
        sb.append(this.ioType);
        sb.append(", ");
        sb.append("ioChannel: ");
        sb.append(this.ioChannel);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeAddIoChannelRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeAddIoChannelRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeAddIoChannelRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeAddIoChannelRequest>() {
            @Override
            public OscilloscopeAddIoChannelRequest fromByteArray(byte[] data) {
                return OscilloscopeAddIoChannelRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeAddIoChannelRequest> parser() {
        return PARSER;
    }

}
