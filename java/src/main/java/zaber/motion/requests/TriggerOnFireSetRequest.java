/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.TriggerAction;
import zaber.motion.ascii.TriggerOperation;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class TriggerOnFireSetRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerOnFireSetRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerOnFireSetRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerOnFireSetRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private TriggerAction action;

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public void setAction(TriggerAction action) {
        this.action = action;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public TriggerAction getAction() {
        return this.action;
    }

    public TriggerOnFireSetRequest withAction(TriggerAction aAction) {
        this.setAction(aAction);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public TriggerOnFireSetRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    public TriggerOnFireSetRequest withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private TriggerOperation operation;

    @com.fasterxml.jackson.annotation.JsonProperty("operation")
    public void setOperation(TriggerOperation operation) {
        this.operation = operation;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("operation")
    public TriggerOperation getOperation() {
        return this.operation;
    }

    public TriggerOnFireSetRequest withOperation(TriggerOperation aOperation) {
        this.setOperation(aOperation);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public TriggerOnFireSetRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public TriggerOnFireSetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerOnFireSetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerOnFireSetRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        double value,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.action = action;
        this.axis = axis;
        this.setting = setting;
        this.operation = operation;
        this.value = value;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerOnFireSetRequest other = (TriggerOnFireSetRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(action, other.action)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(operation, other.operation)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(action),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(operation),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerOnFireSetRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("action: ");
        sb.append(this.action);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("operation: ");
        sb.append(this.operation);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerOnFireSetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerOnFireSetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerOnFireSetRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerOnFireSetRequest>() {
            @Override
            public TriggerOnFireSetRequest fromByteArray(byte[] data) {
                return TriggerOnFireSetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerOnFireSetRequest> parser() {
        return PARSER;
    }

}
