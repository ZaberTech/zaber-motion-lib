/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.AutofocusStatus;
import zaber.motion.EqualityUtility;


public final class AutofocusGetStatusResponse implements zaber.motion.dto.Message {

    private AutofocusStatus status;

    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public void setStatus(AutofocusStatus status) {
        this.status = status;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public AutofocusStatus getStatus() {
        return this.status;
    }

    public AutofocusGetStatusResponse withStatus(AutofocusStatus aStatus) {
        this.setStatus(aStatus);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AutofocusGetStatusResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public AutofocusGetStatusResponse(
        AutofocusStatus status
    ) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AutofocusGetStatusResponse other = (AutofocusGetStatusResponse) obj;

        return (
            EqualityUtility.equals(status, other.status)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(status)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AutofocusGetStatusResponse { ");
        sb.append("status: ");
        sb.append(this.status);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AutofocusGetStatusResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AutofocusGetStatusResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AutofocusGetStatusResponse> PARSER =
        new zaber.motion.dto.Parser<AutofocusGetStatusResponse>() {
            @Override
            public AutofocusGetStatusResponse fromByteArray(byte[] data) {
                return AutofocusGetStatusResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AutofocusGetStatusResponse> parser() {
        return PARSER;
    }

}
