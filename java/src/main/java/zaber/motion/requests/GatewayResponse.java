/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class GatewayResponse implements zaber.motion.dto.Message {

    private ResponseType response;

    @com.fasterxml.jackson.annotation.JsonProperty("response")
    public void setResponse(ResponseType response) {
        this.response = response;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("response")
    public ResponseType getResponse() {
        return this.response;
    }

    public GatewayResponse withResponse(ResponseType aResponse) {
        this.setResponse(aResponse);
        return this;
    }

    private Errors errorType;

    @com.fasterxml.jackson.annotation.JsonProperty("errorType")
    public void setErrorType(Errors errorType) {
        this.errorType = errorType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("errorType")
    public Errors getErrorType() {
        return this.errorType;
    }

    public GatewayResponse withErrorType(Errors aErrorType) {
        this.setErrorType(aErrorType);
        return this;
    }

    private String errorMessage;

    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public String getErrorMessage() {
        return this.errorMessage;
    }

    public GatewayResponse withErrorMessage(String aErrorMessage) {
        this.setErrorMessage(aErrorMessage);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GatewayResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public GatewayResponse(
        ResponseType response,
        Errors errorType,
        String errorMessage
    ) {
        this.response = response;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GatewayResponse other = (GatewayResponse) obj;

        return (
            EqualityUtility.equals(response, other.response)
            && EqualityUtility.equals(errorType, other.errorType)
            && EqualityUtility.equals(errorMessage, other.errorMessage)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(response),
            EqualityUtility.generateHashCode(errorType),
            EqualityUtility.generateHashCode(errorMessage)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GatewayResponse { ");
        sb.append("response: ");
        sb.append(this.response);
        sb.append(", ");
        sb.append("errorType: ");
        sb.append(this.errorType);
        sb.append(", ");
        sb.append("errorMessage: ");
        sb.append(this.errorMessage);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GatewayResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GatewayResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GatewayResponse> PARSER =
        new zaber.motion.dto.Parser<GatewayResponse>() {
            @Override
            public GatewayResponse fromByteArray(byte[] data) {
                return GatewayResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GatewayResponse> parser() {
        return PARSER;
    }

}
