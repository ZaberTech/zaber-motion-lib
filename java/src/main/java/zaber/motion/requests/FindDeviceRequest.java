/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class FindDeviceRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public FindDeviceRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int deviceAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public void setDeviceAddress(int deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public int getDeviceAddress() {
        return this.deviceAddress;
    }

    public FindDeviceRequest withDeviceAddress(int aDeviceAddress) {
        this.setDeviceAddress(aDeviceAddress);
        return this;
    }

    /**
     * Empty constructor.
     */
    public FindDeviceRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public FindDeviceRequest(
        int interfaceId,
        int deviceAddress
    ) {
        this.interfaceId = interfaceId;
        this.deviceAddress = deviceAddress;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        FindDeviceRequest other = (FindDeviceRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(deviceAddress, other.deviceAddress)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(deviceAddress)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FindDeviceRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("deviceAddress: ");
        sb.append(this.deviceAddress);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static FindDeviceRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, FindDeviceRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<FindDeviceRequest> PARSER =
        new zaber.motion.dto.Parser<FindDeviceRequest>() {
            @Override
            public FindDeviceRequest fromByteArray(byte[] data) {
                return FindDeviceRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<FindDeviceRequest> parser() {
        return PARSER;
    }

}
