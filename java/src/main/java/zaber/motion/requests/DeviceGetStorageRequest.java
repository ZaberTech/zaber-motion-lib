/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetStorageRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceGetStorageRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceGetStorageRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceGetStorageRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String key;

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public String getKey() {
        return this.key;
    }

    public DeviceGetStorageRequest withKey(String aKey) {
        this.setKey(aKey);
        return this;
    }

    private boolean decode;

    @com.fasterxml.jackson.annotation.JsonProperty("decode")
    public void setDecode(boolean decode) {
        this.decode = decode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("decode")
    public boolean getDecode() {
        return this.decode;
    }

    public DeviceGetStorageRequest withDecode(boolean aDecode) {
        this.setDecode(aDecode);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetStorageRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetStorageRequest(
        int interfaceId,
        int device,
        int axis,
        String key,
        boolean decode
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.key = key;
        this.decode = decode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetStorageRequest other = (DeviceGetStorageRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(key, other.key)
            && EqualityUtility.equals(decode, other.decode)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(key),
            EqualityUtility.generateHashCode(decode)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetStorageRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("key: ");
        sb.append(this.key);
        sb.append(", ");
        sb.append("decode: ");
        sb.append(this.decode);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetStorageRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetStorageRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetStorageRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceGetStorageRequest>() {
            @Override
            public DeviceGetStorageRequest fromByteArray(byte[] data) {
                return DeviceGetStorageRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetStorageRequest> parser() {
        return PARSER;
    }

}
