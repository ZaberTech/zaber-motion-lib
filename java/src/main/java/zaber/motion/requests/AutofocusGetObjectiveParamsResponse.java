/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.NamedParameter;
import zaber.motion.EqualityUtility;


public final class AutofocusGetObjectiveParamsResponse implements zaber.motion.dto.Message {

    private NamedParameter[] parameters;

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public void setParameters(NamedParameter[] parameters) {
        this.parameters = parameters;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public NamedParameter[] getParameters() {
        return this.parameters;
    }

    public AutofocusGetObjectiveParamsResponse withParameters(NamedParameter[] aParameters) {
        this.setParameters(aParameters);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AutofocusGetObjectiveParamsResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public AutofocusGetObjectiveParamsResponse(
        NamedParameter[] parameters
    ) {
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AutofocusGetObjectiveParamsResponse other = (AutofocusGetObjectiveParamsResponse) obj;

        return (
            EqualityUtility.equals(parameters, other.parameters)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(parameters)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AutofocusGetObjectiveParamsResponse { ");
        sb.append("parameters: ");
        sb.append(this.parameters);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AutofocusGetObjectiveParamsResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AutofocusGetObjectiveParamsResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AutofocusGetObjectiveParamsResponse> PARSER =
        new zaber.motion.dto.Parser<AutofocusGetObjectiveParamsResponse>() {
            @Override
            public AutofocusGetObjectiveParamsResponse fromByteArray(byte[] data) {
                return AutofocusGetObjectiveParamsResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AutofocusGetObjectiveParamsResponse> parser() {
        return PARSER;
    }

}
