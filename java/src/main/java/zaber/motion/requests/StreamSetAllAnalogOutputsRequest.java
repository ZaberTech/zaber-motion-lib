/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamSetAllAnalogOutputsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetAllAnalogOutputsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetAllAnalogOutputsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetAllAnalogOutputsRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetAllAnalogOutputsRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private double[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    public StreamSetAllAnalogOutputsRequest withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetAllAnalogOutputsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetAllAnalogOutputsRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        double[] values
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetAllAnalogOutputsRequest other = (StreamSetAllAnalogOutputsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetAllAnalogOutputsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetAllAnalogOutputsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetAllAnalogOutputsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetAllAnalogOutputsRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetAllAnalogOutputsRequest>() {
            @Override
            public StreamSetAllAnalogOutputsRequest fromByteArray(byte[] data) {
                return StreamSetAllAnalogOutputsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetAllAnalogOutputsRequest> parser() {
        return PARSER;
    }

}
