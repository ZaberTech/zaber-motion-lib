/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TranslatorEmptyRequest implements zaber.motion.dto.Message {

    private int translatorId;

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public void setTranslatorId(int translatorId) {
        this.translatorId = translatorId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("translatorId")
    public int getTranslatorId() {
        return this.translatorId;
    }

    public TranslatorEmptyRequest withTranslatorId(int aTranslatorId) {
        this.setTranslatorId(aTranslatorId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorEmptyRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorEmptyRequest(
        int translatorId
    ) {
        this.translatorId = translatorId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorEmptyRequest other = (TranslatorEmptyRequest) obj;

        return (
            EqualityUtility.equals(translatorId, other.translatorId)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(translatorId)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorEmptyRequest { ");
        sb.append("translatorId: ");
        sb.append(this.translatorId);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorEmptyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorEmptyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorEmptyRequest> PARSER =
        new zaber.motion.dto.Parser<TranslatorEmptyRequest>() {
            @Override
            public TranslatorEmptyRequest fromByteArray(byte[] data) {
                return TranslatorEmptyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorEmptyRequest> parser() {
        return PARSER;
    }

}
