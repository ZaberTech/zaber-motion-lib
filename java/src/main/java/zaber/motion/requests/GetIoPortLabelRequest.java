/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.IoPortType;
import zaber.motion.EqualityUtility;


public final class GetIoPortLabelRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public GetIoPortLabelRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public GetIoPortLabelRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private IoPortType portType;

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public void setPortType(IoPortType portType) {
        this.portType = portType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public IoPortType getPortType() {
        return this.portType;
    }

    public GetIoPortLabelRequest withPortType(IoPortType aPortType) {
        this.setPortType(aPortType);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public GetIoPortLabelRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetIoPortLabelRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public GetIoPortLabelRequest(
        int interfaceId,
        int device,
        IoPortType portType,
        int channelNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.portType = portType;
        this.channelNumber = channelNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetIoPortLabelRequest other = (GetIoPortLabelRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(portType, other.portType)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(portType),
            EqualityUtility.generateHashCode(channelNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetIoPortLabelRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("portType: ");
        sb.append(this.portType);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetIoPortLabelRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetIoPortLabelRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetIoPortLabelRequest> PARSER =
        new zaber.motion.dto.Parser<GetIoPortLabelRequest>() {
            @Override
            public GetIoPortLabelRequest fromByteArray(byte[] data) {
                return GetIoPortLabelRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetIoPortLabelRequest> parser() {
        return PARSER;
    }

}
