/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.StreamAxisDefinition;
import zaber.motion.ascii.PvtAxisDefinition;
import zaber.motion.EqualityUtility;


public final class StreamSetupStoreCompositeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetupStoreCompositeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetupStoreCompositeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetupStoreCompositeRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetupStoreCompositeRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private int streamBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public void setStreamBuffer(int streamBuffer) {
        this.streamBuffer = streamBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public int getStreamBuffer() {
        return this.streamBuffer;
    }

    public StreamSetupStoreCompositeRequest withStreamBuffer(int aStreamBuffer) {
        this.setStreamBuffer(aStreamBuffer);
        return this;
    }

    private int pvtBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public void setPvtBuffer(int pvtBuffer) {
        this.pvtBuffer = pvtBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public int getPvtBuffer() {
        return this.pvtBuffer;
    }

    public StreamSetupStoreCompositeRequest withPvtBuffer(int aPvtBuffer) {
        this.setPvtBuffer(aPvtBuffer);
        return this;
    }

    private StreamAxisDefinition[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(StreamAxisDefinition[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public StreamAxisDefinition[] getAxes() {
        return this.axes;
    }

    public StreamSetupStoreCompositeRequest withAxes(StreamAxisDefinition[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private PvtAxisDefinition[] pvtAxes;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public void setPvtAxes(PvtAxisDefinition[] pvtAxes) {
        this.pvtAxes = pvtAxes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public PvtAxisDefinition[] getPvtAxes() {
        return this.pvtAxes;
    }

    public StreamSetupStoreCompositeRequest withPvtAxes(PvtAxisDefinition[] aPvtAxes) {
        this.setPvtAxes(aPvtAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetupStoreCompositeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetupStoreCompositeRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        int streamBuffer,
        int pvtBuffer,
        StreamAxisDefinition[] axes,
        PvtAxisDefinition[] pvtAxes
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.streamBuffer = streamBuffer;
        this.pvtBuffer = pvtBuffer;
        this.axes = axes;
        this.pvtAxes = pvtAxes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetupStoreCompositeRequest other = (StreamSetupStoreCompositeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(streamBuffer, other.streamBuffer)
            && EqualityUtility.equals(pvtBuffer, other.pvtBuffer)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(pvtAxes, other.pvtAxes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(streamBuffer),
            EqualityUtility.generateHashCode(pvtBuffer),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(pvtAxes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetupStoreCompositeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("streamBuffer: ");
        sb.append(this.streamBuffer);
        sb.append(", ");
        sb.append("pvtBuffer: ");
        sb.append(this.pvtBuffer);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("pvtAxes: ");
        sb.append(this.pvtAxes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetupStoreCompositeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetupStoreCompositeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetupStoreCompositeRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetupStoreCompositeRequest>() {
            @Override
            public StreamSetupStoreCompositeRequest fromByteArray(byte[] data) {
                return StreamSetupStoreCompositeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetupStoreCompositeRequest> parser() {
        return PARSER;
    }

}
