/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class BoolResponse implements zaber.motion.dto.Message {

    private boolean value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(boolean value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public boolean getValue() {
        return this.value;
    }

    public BoolResponse withValue(boolean aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BoolResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public BoolResponse(
        boolean value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BoolResponse other = (BoolResponse) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BoolResponse { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BoolResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BoolResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BoolResponse> PARSER =
        new zaber.motion.dto.Parser<BoolResponse>() {
            @Override
            public BoolResponse fromByteArray(byte[] data) {
                return BoolResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BoolResponse> parser() {
        return PARSER;
    }

}
