/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class StreamSetAnalogOutputScheduleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetAnalogOutputScheduleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetAnalogOutputScheduleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetAnalogOutputScheduleRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetAnalogOutputScheduleRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public StreamSetAnalogOutputScheduleRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public StreamSetAnalogOutputScheduleRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private double futureValue;

    @com.fasterxml.jackson.annotation.JsonProperty("futureValue")
    public void setFutureValue(double futureValue) {
        this.futureValue = futureValue;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("futureValue")
    public double getFutureValue() {
        return this.futureValue;
    }

    public StreamSetAnalogOutputScheduleRequest withFutureValue(double aFutureValue) {
        this.setFutureValue(aFutureValue);
        return this;
    }

    private double delay;

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public void setDelay(double delay) {
        this.delay = delay;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("delay")
    public double getDelay() {
        return this.delay;
    }

    public StreamSetAnalogOutputScheduleRequest withDelay(double aDelay) {
        this.setDelay(aDelay);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public StreamSetAnalogOutputScheduleRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetAnalogOutputScheduleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetAnalogOutputScheduleRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        int channelNumber,
        double value,
        double futureValue,
        double delay,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.channelNumber = channelNumber;
        this.value = value;
        this.futureValue = futureValue;
        this.delay = delay;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetAnalogOutputScheduleRequest other = (StreamSetAnalogOutputScheduleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(futureValue, other.futureValue)
            && EqualityUtility.equals(delay, other.delay)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(futureValue),
            EqualityUtility.generateHashCode(delay),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetAnalogOutputScheduleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("futureValue: ");
        sb.append(this.futureValue);
        sb.append(", ");
        sb.append("delay: ");
        sb.append(this.delay);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetAnalogOutputScheduleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetAnalogOutputScheduleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetAnalogOutputScheduleRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetAnalogOutputScheduleRequest>() {
            @Override
            public StreamSetAnalogOutputScheduleRequest fromByteArray(byte[] data) {
                return StreamSetAnalogOutputScheduleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetAnalogOutputScheduleRequest> parser() {
        return PARSER;
    }

}
