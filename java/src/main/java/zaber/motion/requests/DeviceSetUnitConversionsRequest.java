/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ConversionFactor;
import zaber.motion.EqualityUtility;


public final class DeviceSetUnitConversionsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetUnitConversionsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetUnitConversionsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceSetUnitConversionsRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String key;

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public String getKey() {
        return this.key;
    }

    public DeviceSetUnitConversionsRequest withKey(String aKey) {
        this.setKey(aKey);
        return this;
    }

    private ConversionFactor[] conversions;

    @com.fasterxml.jackson.annotation.JsonProperty("conversions")
    public void setConversions(ConversionFactor[] conversions) {
        this.conversions = conversions;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("conversions")
    public ConversionFactor[] getConversions() {
        return this.conversions;
    }

    public DeviceSetUnitConversionsRequest withConversions(ConversionFactor[] aConversions) {
        this.setConversions(aConversions);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetUnitConversionsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetUnitConversionsRequest(
        int interfaceId,
        int device,
        int axis,
        String key,
        ConversionFactor[] conversions
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.key = key;
        this.conversions = conversions;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetUnitConversionsRequest other = (DeviceSetUnitConversionsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(key, other.key)
            && EqualityUtility.equals(conversions, other.conversions)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(key),
            EqualityUtility.generateHashCode(conversions)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetUnitConversionsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("key: ");
        sb.append(this.key);
        sb.append(", ");
        sb.append("conversions: ");
        sb.append(this.conversions);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetUnitConversionsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetUnitConversionsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetUnitConversionsRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetUnitConversionsRequest>() {
            @Override
            public DeviceSetUnitConversionsRequest fromByteArray(byte[] data) {
                return DeviceSetUnitConversionsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetUnitConversionsRequest> parser() {
        return PARSER;
    }

}
