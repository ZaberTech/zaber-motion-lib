/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class DeviceConvertSettingRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceConvertSettingRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceConvertSettingRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceConvertSettingRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    public DeviceConvertSettingRequest withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public DeviceConvertSettingRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public DeviceConvertSettingRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private boolean fromNative;

    @com.fasterxml.jackson.annotation.JsonProperty("fromNative")
    public void setFromNative(boolean fromNative) {
        this.fromNative = fromNative;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fromNative")
    public boolean getFromNative() {
        return this.fromNative;
    }

    public DeviceConvertSettingRequest withFromNative(boolean aFromNative) {
        this.setFromNative(aFromNative);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceConvertSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceConvertSettingRequest(
        int interfaceId,
        int device,
        int axis,
        String setting,
        Units unit,
        double value,
        boolean fromNative
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.setting = setting;
        this.unit = unit;
        this.value = value;
        this.fromNative = fromNative;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceConvertSettingRequest other = (DeviceConvertSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(fromNative, other.fromNative)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(fromNative)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceConvertSettingRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("fromNative: ");
        sb.append(this.fromNative);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceConvertSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceConvertSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceConvertSettingRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceConvertSettingRequest>() {
            @Override
            public DeviceConvertSettingRequest fromByteArray(byte[] data) {
                return DeviceConvertSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceConvertSettingRequest> parser() {
        return PARSER;
    }

}
