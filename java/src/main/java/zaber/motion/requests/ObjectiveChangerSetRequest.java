/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class ObjectiveChangerSetRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ObjectiveChangerSetRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public ObjectiveChangerSetRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public ObjectiveChangerSetRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public ObjectiveChangerSetRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public ObjectiveChangerSetRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public ObjectiveChangerSetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ObjectiveChangerSetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ObjectiveChangerSetRequest(
        int interfaceId,
        int turretAddress,
        int focusAddress,
        int focusAxis,
        double value,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.turretAddress = turretAddress;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.value = value;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ObjectiveChangerSetRequest other = (ObjectiveChangerSetRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(turretAddress),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectiveChangerSetRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ObjectiveChangerSetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ObjectiveChangerSetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ObjectiveChangerSetRequest> PARSER =
        new zaber.motion.dto.Parser<ObjectiveChangerSetRequest>() {
            @Override
            public ObjectiveChangerSetRequest fromByteArray(byte[] data) {
                return ObjectiveChangerSetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ObjectiveChangerSetRequest> parser() {
        return PARSER;
    }

}
