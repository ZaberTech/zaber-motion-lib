/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.FirmwareVersion;
import zaber.motion.EqualityUtility;


public final class DeviceIdentifyRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceIdentifyRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceIdentifyRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private FirmwareVersion assumeVersion;

    @com.fasterxml.jackson.annotation.JsonProperty("assumeVersion")
    public void setAssumeVersion(FirmwareVersion assumeVersion) {
        this.assumeVersion = assumeVersion;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("assumeVersion")
    public FirmwareVersion getAssumeVersion() {
        return this.assumeVersion;
    }

    public DeviceIdentifyRequest withAssumeVersion(FirmwareVersion aAssumeVersion) {
        this.setAssumeVersion(aAssumeVersion);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceIdentifyRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceIdentifyRequest(
        int interfaceId,
        int device,
        FirmwareVersion assumeVersion
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.assumeVersion = assumeVersion;
    }

    /**
     * Constructor with only required properties.
     */
    public DeviceIdentifyRequest(
        int interfaceId,
        int device
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceIdentifyRequest other = (DeviceIdentifyRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(assumeVersion, other.assumeVersion)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(assumeVersion)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceIdentifyRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("assumeVersion: ");
        sb.append(this.assumeVersion);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceIdentifyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceIdentifyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceIdentifyRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceIdentifyRequest>() {
            @Override
            public DeviceIdentifyRequest fromByteArray(byte[] data) {
                return DeviceIdentifyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceIdentifyRequest> parser() {
        return PARSER;
    }

}
