/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.ServoTuningParamset;
import zaber.motion.EqualityUtility;


public final class SetServoTuningPIDRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetServoTuningPIDRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetServoTuningPIDRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public SetServoTuningPIDRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private ServoTuningParamset paramset;

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public void setParamset(ServoTuningParamset paramset) {
        this.paramset = paramset;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("paramset")
    public ServoTuningParamset getParamset() {
        return this.paramset;
    }

    public SetServoTuningPIDRequest withParamset(ServoTuningParamset aParamset) {
        this.setParamset(aParamset);
        return this;
    }

    private double p;

    @com.fasterxml.jackson.annotation.JsonProperty("p")
    public void setP(double p) {
        this.p = p;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("p")
    public double getP() {
        return this.p;
    }

    public SetServoTuningPIDRequest withP(double aP) {
        this.setP(aP);
        return this;
    }

    private double i;

    @com.fasterxml.jackson.annotation.JsonProperty("i")
    public void setI(double i) {
        this.i = i;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("i")
    public double getI() {
        return this.i;
    }

    public SetServoTuningPIDRequest withI(double aI) {
        this.setI(aI);
        return this;
    }

    private double d;

    @com.fasterxml.jackson.annotation.JsonProperty("d")
    public void setD(double d) {
        this.d = d;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("d")
    public double getD() {
        return this.d;
    }

    public SetServoTuningPIDRequest withD(double aD) {
        this.setD(aD);
        return this;
    }

    private double fc;

    @com.fasterxml.jackson.annotation.JsonProperty("fc")
    public void setFc(double fc) {
        this.fc = fc;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fc")
    public double getFc() {
        return this.fc;
    }

    public SetServoTuningPIDRequest withFc(double aFc) {
        this.setFc(aFc);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetServoTuningPIDRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetServoTuningPIDRequest(
        int interfaceId,
        int device,
        int axis,
        ServoTuningParamset paramset,
        double p,
        double i,
        double d,
        double fc
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.paramset = paramset;
        this.p = p;
        this.i = i;
        this.d = d;
        this.fc = fc;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetServoTuningPIDRequest other = (SetServoTuningPIDRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(paramset, other.paramset)
            && EqualityUtility.equals(p, other.p)
            && EqualityUtility.equals(i, other.i)
            && EqualityUtility.equals(d, other.d)
            && EqualityUtility.equals(fc, other.fc)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(paramset),
            EqualityUtility.generateHashCode(p),
            EqualityUtility.generateHashCode(i),
            EqualityUtility.generateHashCode(d),
            EqualityUtility.generateHashCode(fc)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetServoTuningPIDRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("paramset: ");
        sb.append(this.paramset);
        sb.append(", ");
        sb.append("p: ");
        sb.append(this.p);
        sb.append(", ");
        sb.append("i: ");
        sb.append(this.i);
        sb.append(", ");
        sb.append("d: ");
        sb.append(this.d);
        sb.append(", ");
        sb.append("fc: ");
        sb.append(this.fc);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetServoTuningPIDRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetServoTuningPIDRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetServoTuningPIDRequest> PARSER =
        new zaber.motion.dto.Parser<SetServoTuningPIDRequest>() {
            @Override
            public SetServoTuningPIDRequest fromByteArray(byte[] data) {
                return SetServoTuningPIDRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetServoTuningPIDRequest> parser() {
        return PARSER;
    }

}
