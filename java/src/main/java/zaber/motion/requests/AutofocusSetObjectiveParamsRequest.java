/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.NamedParameter;
import zaber.motion.EqualityUtility;


public final class AutofocusSetObjectiveParamsRequest implements zaber.motion.dto.Message {

    private int providerId;

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public int getProviderId() {
        return this.providerId;
    }

    public AutofocusSetObjectiveParamsRequest withProviderId(int aProviderId) {
        this.setProviderId(aProviderId);
        return this;
    }

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public AutofocusSetObjectiveParamsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public AutofocusSetObjectiveParamsRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public AutofocusSetObjectiveParamsRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public AutofocusSetObjectiveParamsRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    private int objective;

    @com.fasterxml.jackson.annotation.JsonProperty("objective")
    public void setObjective(int objective) {
        this.objective = objective;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("objective")
    public int getObjective() {
        return this.objective;
    }

    public AutofocusSetObjectiveParamsRequest withObjective(int aObjective) {
        this.setObjective(aObjective);
        return this;
    }

    private NamedParameter[] parameters;

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public void setParameters(NamedParameter[] parameters) {
        this.parameters = parameters;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public NamedParameter[] getParameters() {
        return this.parameters;
    }

    public AutofocusSetObjectiveParamsRequest withParameters(NamedParameter[] aParameters) {
        this.setParameters(aParameters);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AutofocusSetObjectiveParamsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AutofocusSetObjectiveParamsRequest(
        int providerId,
        int interfaceId,
        int focusAddress,
        int focusAxis,
        int turretAddress,
        int objective,
        NamedParameter[] parameters
    ) {
        this.providerId = providerId;
        this.interfaceId = interfaceId;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.turretAddress = turretAddress;
        this.objective = objective;
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AutofocusSetObjectiveParamsRequest other = (AutofocusSetObjectiveParamsRequest) obj;

        return (
            EqualityUtility.equals(providerId, other.providerId)
            && EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            && EqualityUtility.equals(objective, other.objective)
            && EqualityUtility.equals(parameters, other.parameters)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(providerId),
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(turretAddress),
            EqualityUtility.generateHashCode(objective),
            EqualityUtility.generateHashCode(parameters)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AutofocusSetObjectiveParamsRequest { ");
        sb.append("providerId: ");
        sb.append(this.providerId);
        sb.append(", ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(", ");
        sb.append("objective: ");
        sb.append(this.objective);
        sb.append(", ");
        sb.append("parameters: ");
        sb.append(this.parameters);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AutofocusSetObjectiveParamsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AutofocusSetObjectiveParamsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AutofocusSetObjectiveParamsRequest> PARSER =
        new zaber.motion.dto.Parser<AutofocusSetObjectiveParamsRequest>() {
            @Override
            public AutofocusSetObjectiveParamsRequest fromByteArray(byte[] data) {
                return AutofocusSetObjectiveParamsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AutofocusSetObjectiveParamsRequest> parser() {
        return PARSER;
    }

}
