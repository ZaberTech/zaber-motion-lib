/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.RotationDirection;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class StreamCircleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamCircleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamCircleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamCircleRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamCircleRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private StreamSegmentType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(StreamSegmentType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public StreamSegmentType getType() {
        return this.type;
    }

    public StreamCircleRequest withType(StreamSegmentType aType) {
        this.setType(aType);
        return this;
    }

    private RotationDirection rotationDirection;

    @com.fasterxml.jackson.annotation.JsonProperty("rotationDirection")
    public void setRotationDirection(RotationDirection rotationDirection) {
        this.rotationDirection = rotationDirection;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("rotationDirection")
    public RotationDirection getRotationDirection() {
        return this.rotationDirection;
    }

    public StreamCircleRequest withRotationDirection(RotationDirection aRotationDirection) {
        this.setRotationDirection(aRotationDirection);
        return this;
    }

    private Measurement centerX;

    @com.fasterxml.jackson.annotation.JsonProperty("centerX")
    public void setCenterX(Measurement centerX) {
        this.centerX = centerX;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("centerX")
    public Measurement getCenterX() {
        return this.centerX;
    }

    public StreamCircleRequest withCenterX(Measurement aCenterX) {
        this.setCenterX(aCenterX);
        return this;
    }

    private Measurement centerY;

    @com.fasterxml.jackson.annotation.JsonProperty("centerY")
    public void setCenterY(Measurement centerY) {
        this.centerY = centerY;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("centerY")
    public Measurement getCenterY() {
        return this.centerY;
    }

    public StreamCircleRequest withCenterY(Measurement aCenterY) {
        this.setCenterY(aCenterY);
        return this;
    }

    private int[] targetAxesIndices;

    @com.fasterxml.jackson.annotation.JsonProperty("targetAxesIndices")
    public void setTargetAxesIndices(int[] targetAxesIndices) {
        this.targetAxesIndices = targetAxesIndices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("targetAxesIndices")
    public int[] getTargetAxesIndices() {
        return this.targetAxesIndices;
    }

    public StreamCircleRequest withTargetAxesIndices(int[] aTargetAxesIndices) {
        this.setTargetAxesIndices(aTargetAxesIndices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamCircleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamCircleRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        StreamSegmentType type,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        int[] targetAxesIndices
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.type = type;
        this.rotationDirection = rotationDirection;
        this.centerX = centerX;
        this.centerY = centerY;
        this.targetAxesIndices = targetAxesIndices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamCircleRequest other = (StreamCircleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(rotationDirection, other.rotationDirection)
            && EqualityUtility.equals(centerX, other.centerX)
            && EqualityUtility.equals(centerY, other.centerY)
            && EqualityUtility.equals(targetAxesIndices, other.targetAxesIndices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(rotationDirection),
            EqualityUtility.generateHashCode(centerX),
            EqualityUtility.generateHashCode(centerY),
            EqualityUtility.generateHashCode(targetAxesIndices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamCircleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("rotationDirection: ");
        sb.append(this.rotationDirection);
        sb.append(", ");
        sb.append("centerX: ");
        sb.append(this.centerX);
        sb.append(", ");
        sb.append("centerY: ");
        sb.append(this.centerY);
        sb.append(", ");
        sb.append("targetAxesIndices: ");
        sb.append(this.targetAxesIndices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamCircleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamCircleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamCircleRequest> PARSER =
        new zaber.motion.dto.Parser<StreamCircleRequest>() {
            @Override
            public StreamCircleRequest fromByteArray(byte[] data) {
                return StreamCircleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamCircleRequest> parser() {
        return PARSER;
    }

}
