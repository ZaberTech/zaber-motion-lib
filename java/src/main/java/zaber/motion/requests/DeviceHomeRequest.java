/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceHomeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceHomeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceHomeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceHomeRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean waitUntilIdle;

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public void setWaitUntilIdle(boolean waitUntilIdle) {
        this.waitUntilIdle = waitUntilIdle;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public boolean getWaitUntilIdle() {
        return this.waitUntilIdle;
    }

    public DeviceHomeRequest withWaitUntilIdle(boolean aWaitUntilIdle) {
        this.setWaitUntilIdle(aWaitUntilIdle);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceHomeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceHomeRequest(
        int interfaceId,
        int device,
        int axis,
        boolean waitUntilIdle
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.waitUntilIdle = waitUntilIdle;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceHomeRequest other = (DeviceHomeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(waitUntilIdle, other.waitUntilIdle)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(waitUntilIdle)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceHomeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("waitUntilIdle: ");
        sb.append(this.waitUntilIdle);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceHomeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceHomeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceHomeRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceHomeRequest>() {
            @Override
            public DeviceHomeRequest fromByteArray(byte[] data) {
                return DeviceHomeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceHomeRequest> parser() {
        return PARSER;
    }

}
