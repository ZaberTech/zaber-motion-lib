/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class UnitGetSymbolResponse implements zaber.motion.dto.Message {

    private String symbol;

    @com.fasterxml.jackson.annotation.JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("symbol")
    public String getSymbol() {
        return this.symbol;
    }

    public UnitGetSymbolResponse withSymbol(String aSymbol) {
        this.setSymbol(aSymbol);
        return this;
    }

    /**
     * Empty constructor.
     */
    public UnitGetSymbolResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public UnitGetSymbolResponse(
        String symbol
    ) {
        this.symbol = symbol;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        UnitGetSymbolResponse other = (UnitGetSymbolResponse) obj;

        return (
            EqualityUtility.equals(symbol, other.symbol)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(symbol)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnitGetSymbolResponse { ");
        sb.append("symbol: ");
        sb.append(this.symbol);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static UnitGetSymbolResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, UnitGetSymbolResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<UnitGetSymbolResponse> PARSER =
        new zaber.motion.dto.Parser<UnitGetSymbolResponse>() {
            @Override
            public UnitGetSymbolResponse fromByteArray(byte[] data) {
                return UnitGetSymbolResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<UnitGetSymbolResponse> parser() {
        return PARSER;
    }

}
