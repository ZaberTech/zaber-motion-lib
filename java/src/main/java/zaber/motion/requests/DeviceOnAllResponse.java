/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceOnAllResponse implements zaber.motion.dto.Message {

    private int[] deviceAddresses;

    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddresses")
    public void setDeviceAddresses(int[] deviceAddresses) {
        this.deviceAddresses = deviceAddresses;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddresses")
    public int[] getDeviceAddresses() {
        return this.deviceAddresses;
    }

    public DeviceOnAllResponse withDeviceAddresses(int[] aDeviceAddresses) {
        this.setDeviceAddresses(aDeviceAddresses);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceOnAllResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceOnAllResponse(
        int[] deviceAddresses
    ) {
        this.deviceAddresses = deviceAddresses;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceOnAllResponse other = (DeviceOnAllResponse) obj;

        return (
            EqualityUtility.equals(deviceAddresses, other.deviceAddresses)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceAddresses)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOnAllResponse { ");
        sb.append("deviceAddresses: ");
        sb.append(this.deviceAddresses);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceOnAllResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceOnAllResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceOnAllResponse> PARSER =
        new zaber.motion.dto.Parser<DeviceOnAllResponse>() {
            @Override
            public DeviceOnAllResponse fromByteArray(byte[] data) {
                return DeviceOnAllResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceOnAllResponse> parser() {
        return PARSER;
    }

}
