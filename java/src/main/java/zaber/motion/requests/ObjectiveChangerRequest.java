/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ObjectiveChangerRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ObjectiveChangerRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public ObjectiveChangerRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public ObjectiveChangerRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public ObjectiveChangerRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ObjectiveChangerRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ObjectiveChangerRequest(
        int interfaceId,
        int turretAddress,
        int focusAddress,
        int focusAxis
    ) {
        this.interfaceId = interfaceId;
        this.turretAddress = turretAddress;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ObjectiveChangerRequest other = (ObjectiveChangerRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(turretAddress),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectiveChangerRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ObjectiveChangerRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ObjectiveChangerRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ObjectiveChangerRequest> PARSER =
        new zaber.motion.dto.Parser<ObjectiveChangerRequest>() {
            @Override
            public ObjectiveChangerRequest fromByteArray(byte[] data) {
                return ObjectiveChangerRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ObjectiveChangerRequest> parser() {
        return PARSER;
    }

}
