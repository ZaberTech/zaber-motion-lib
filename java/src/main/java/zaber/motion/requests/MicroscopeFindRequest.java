/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.ThirdPartyComponents;
import zaber.motion.EqualityUtility;


public final class MicroscopeFindRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public MicroscopeFindRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private ThirdPartyComponents thirdParty;

    @com.fasterxml.jackson.annotation.JsonProperty("thirdParty")
    public void setThirdParty(ThirdPartyComponents thirdParty) {
        this.thirdParty = thirdParty;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("thirdParty")
    public ThirdPartyComponents getThirdParty() {
        return this.thirdParty;
    }

    public MicroscopeFindRequest withThirdParty(ThirdPartyComponents aThirdParty) {
        this.setThirdParty(aThirdParty);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeFindRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeFindRequest(
        int interfaceId,
        ThirdPartyComponents thirdParty
    ) {
        this.interfaceId = interfaceId;
        this.thirdParty = thirdParty;
    }

    /**
     * Constructor with only required properties.
     */
    public MicroscopeFindRequest(
        int interfaceId
    ) {
        this.interfaceId = interfaceId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeFindRequest other = (MicroscopeFindRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(thirdParty, other.thirdParty)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(thirdParty)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeFindRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("thirdParty: ");
        sb.append(this.thirdParty);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeFindRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeFindRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeFindRequest> PARSER =
        new zaber.motion.dto.Parser<MicroscopeFindRequest>() {
            @Override
            public MicroscopeFindRequest fromByteArray(byte[] data) {
                return MicroscopeFindRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeFindRequest> parser() {
        return PARSER;
    }

}
