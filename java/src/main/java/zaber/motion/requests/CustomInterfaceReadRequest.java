/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CustomInterfaceReadRequest implements zaber.motion.dto.Message {

    private int transportId;

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public int getTransportId() {
        return this.transportId;
    }

    public CustomInterfaceReadRequest withTransportId(int aTransportId) {
        this.setTransportId(aTransportId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CustomInterfaceReadRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public CustomInterfaceReadRequest(
        int transportId
    ) {
        this.transportId = transportId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CustomInterfaceReadRequest other = (CustomInterfaceReadRequest) obj;

        return (
            EqualityUtility.equals(transportId, other.transportId)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(transportId)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CustomInterfaceReadRequest { ");
        sb.append("transportId: ");
        sb.append(this.transportId);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CustomInterfaceReadRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CustomInterfaceReadRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CustomInterfaceReadRequest> PARSER =
        new zaber.motion.dto.Parser<CustomInterfaceReadRequest>() {
            @Override
            public CustomInterfaceReadRequest fromByteArray(byte[] data) {
                return CustomInterfaceReadRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CustomInterfaceReadRequest> parser() {
        return PARSER;
    }

}
