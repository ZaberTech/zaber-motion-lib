/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetWarningsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceGetWarningsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceGetWarningsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceGetWarningsRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean clear;

    @com.fasterxml.jackson.annotation.JsonProperty("clear")
    public void setClear(boolean clear) {
        this.clear = clear;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("clear")
    public boolean getClear() {
        return this.clear;
    }

    public DeviceGetWarningsRequest withClear(boolean aClear) {
        this.setClear(aClear);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetWarningsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetWarningsRequest(
        int interfaceId,
        int device,
        int axis,
        boolean clear
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.clear = clear;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetWarningsRequest other = (DeviceGetWarningsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(clear, other.clear)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(clear)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetWarningsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("clear: ");
        sb.append(this.clear);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetWarningsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetWarningsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetWarningsRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceGetWarningsRequest>() {
            @Override
            public DeviceGetWarningsRequest fromByteArray(byte[] data) {
                return DeviceGetWarningsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetWarningsRequest> parser() {
        return PARSER;
    }

}
