/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class GatewayRequest implements zaber.motion.dto.Message {

    private String request;

    @com.fasterxml.jackson.annotation.JsonProperty("request")
    public void setRequest(String request) {
        this.request = request;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("request")
    public String getRequest() {
        return this.request;
    }

    public GatewayRequest withRequest(String aRequest) {
        this.setRequest(aRequest);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GatewayRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public GatewayRequest(
        String request
    ) {
        this.request = request;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GatewayRequest other = (GatewayRequest) obj;

        return (
            EqualityUtility.equals(request, other.request)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(request)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GatewayRequest { ");
        sb.append("request: ");
        sb.append(this.request);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GatewayRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GatewayRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GatewayRequest> PARSER =
        new zaber.motion.dto.Parser<GatewayRequest>() {
            @Override
            public GatewayRequest fromByteArray(byte[] data) {
                return GatewayRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GatewayRequest> parser() {
        return PARSER;
    }

}
