/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import zaber.motion.dto.Mapper;

public final class EmptyRequest implements zaber.motion.dto.Message {

    /**
     * Empty constructor.
     */
    public EmptyRequest() {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmptyRequest { ");
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static EmptyRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, EmptyRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<EmptyRequest> PARSER =
        new zaber.motion.dto.Parser<EmptyRequest>() {
            @Override
            public EmptyRequest fromByteArray(byte[] data) {
                return EmptyRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<EmptyRequest> parser() {
        return PARSER;
    }

}
