/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CanSetStateAxisResponse implements zaber.motion.dto.Message {

    private String error;

    @com.fasterxml.jackson.annotation.JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("error")
    public String getError() {
        return this.error;
    }

    public CanSetStateAxisResponse withError(String aError) {
        this.setError(aError);
        return this;
    }

    private int axisNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    public CanSetStateAxisResponse withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CanSetStateAxisResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public CanSetStateAxisResponse(
        int axisNumber,
        String error
    ) {
        this.axisNumber = axisNumber;
        this.error = error;
    }

    /**
     * Constructor with only required properties.
     */
    public CanSetStateAxisResponse(
        int axisNumber
    ) {
        this.axisNumber = axisNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CanSetStateAxisResponse other = (CanSetStateAxisResponse) obj;

        return (
            EqualityUtility.equals(error, other.error)
            && EqualityUtility.equals(axisNumber, other.axisNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(error),
            EqualityUtility.generateHashCode(axisNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CanSetStateAxisResponse { ");
        sb.append("error: ");
        sb.append(this.error);
        sb.append(", ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CanSetStateAxisResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CanSetStateAxisResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CanSetStateAxisResponse> PARSER =
        new zaber.motion.dto.Parser<CanSetStateAxisResponse>() {
            @Override
            public CanSetStateAxisResponse fromByteArray(byte[] data) {
                return CanSetStateAxisResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CanSetStateAxisResponse> parser() {
        return PARSER;
    }

}
