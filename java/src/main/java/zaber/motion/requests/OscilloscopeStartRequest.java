/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeStartRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public OscilloscopeStartRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public OscilloscopeStartRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int captureLength;

    @com.fasterxml.jackson.annotation.JsonProperty("captureLength")
    public void setCaptureLength(int captureLength) {
        this.captureLength = captureLength;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("captureLength")
    public int getCaptureLength() {
        return this.captureLength;
    }

    public OscilloscopeStartRequest withCaptureLength(int aCaptureLength) {
        this.setCaptureLength(aCaptureLength);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeStartRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeStartRequest(
        int interfaceId,
        int device,
        int captureLength
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.captureLength = captureLength;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeStartRequest other = (OscilloscopeStartRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(captureLength, other.captureLength)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(captureLength)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeStartRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("captureLength: ");
        sb.append(this.captureLength);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeStartRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeStartRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeStartRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeStartRequest>() {
            @Override
            public OscilloscopeStartRequest fromByteArray(byte[] data) {
                return OscilloscopeStartRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeStartRequest> parser() {
        return PARSER;
    }

}
