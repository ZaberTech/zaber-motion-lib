/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceCancelOutputScheduleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceCancelOutputScheduleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceCancelOutputScheduleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private boolean analog;

    @com.fasterxml.jackson.annotation.JsonProperty("analog")
    public void setAnalog(boolean analog) {
        this.analog = analog;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("analog")
    public boolean getAnalog() {
        return this.analog;
    }

    public DeviceCancelOutputScheduleRequest withAnalog(boolean aAnalog) {
        this.setAnalog(aAnalog);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceCancelOutputScheduleRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceCancelOutputScheduleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceCancelOutputScheduleRequest(
        int interfaceId,
        int device,
        boolean analog,
        int channelNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.analog = analog;
        this.channelNumber = channelNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceCancelOutputScheduleRequest other = (DeviceCancelOutputScheduleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(analog, other.analog)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(analog),
            EqualityUtility.generateHashCode(channelNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceCancelOutputScheduleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("analog: ");
        sb.append(this.analog);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceCancelOutputScheduleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceCancelOutputScheduleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceCancelOutputScheduleRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceCancelOutputScheduleRequest>() {
            @Override
            public DeviceCancelOutputScheduleRequest fromByteArray(byte[] data) {
                return DeviceCancelOutputScheduleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceCancelOutputScheduleRequest> parser() {
        return PARSER;
    }

}
