/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class LockstepGetRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LockstepGetRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LockstepGetRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int lockstepGroupId;

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public void setLockstepGroupId(int lockstepGroupId) {
        this.lockstepGroupId = lockstepGroupId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public LockstepGetRequest withLockstepGroupId(int aLockstepGroupId) {
        this.setLockstepGroupId(aLockstepGroupId);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public LockstepGetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepGetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepGetRequest(
        int interfaceId,
        int device,
        int lockstepGroupId,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepGetRequest other = (LockstepGetRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(lockstepGroupId, other.lockstepGroupId)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(lockstepGroupId),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepGetRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("lockstepGroupId: ");
        sb.append(this.lockstepGroupId);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepGetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepGetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepGetRequest> PARSER =
        new zaber.motion.dto.Parser<LockstepGetRequest>() {
            @Override
            public LockstepGetRequest fromByteArray(byte[] data) {
                return LockstepGetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepGetRequest> parser() {
        return PARSER;
    }

}
