/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class LockstepSetRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public LockstepSetRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public LockstepSetRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int lockstepGroupId;

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public void setLockstepGroupId(int lockstepGroupId) {
        this.lockstepGroupId = lockstepGroupId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("lockstepGroupId")
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public LockstepSetRequest withLockstepGroupId(int aLockstepGroupId) {
        this.setLockstepGroupId(aLockstepGroupId);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public LockstepSetRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public LockstepSetRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private int axisIndex;

    @com.fasterxml.jackson.annotation.JsonProperty("axisIndex")
    public void setAxisIndex(int axisIndex) {
        this.axisIndex = axisIndex;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axisIndex")
    public int getAxisIndex() {
        return this.axisIndex;
    }

    public LockstepSetRequest withAxisIndex(int aAxisIndex) {
        this.setAxisIndex(aAxisIndex);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepSetRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepSetRequest(
        int interfaceId,
        int device,
        int lockstepGroupId,
        double value,
        Units unit,
        int axisIndex
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
        this.value = value;
        this.unit = unit;
        this.axisIndex = axisIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepSetRequest other = (LockstepSetRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(lockstepGroupId, other.lockstepGroupId)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(axisIndex, other.axisIndex)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(lockstepGroupId),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(axisIndex)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepSetRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("lockstepGroupId: ");
        sb.append(this.lockstepGroupId);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("axisIndex: ");
        sb.append(this.axisIndex);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepSetRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepSetRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepSetRequest> PARSER =
        new zaber.motion.dto.Parser<LockstepSetRequest>() {
            @Override
            public LockstepSetRequest fromByteArray(byte[] data) {
                return LockstepSetRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepSetRequest> parser() {
        return PARSER;
    }

}
