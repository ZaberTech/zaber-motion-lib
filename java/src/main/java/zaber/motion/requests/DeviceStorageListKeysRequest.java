/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceStorageListKeysRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceStorageListKeysRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceStorageListKeysRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceStorageListKeysRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String prefix;

    @com.fasterxml.jackson.annotation.JsonProperty("prefix")
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("prefix")
    public String getPrefix() {
        return this.prefix;
    }

    public DeviceStorageListKeysRequest withPrefix(String aPrefix) {
        this.setPrefix(aPrefix);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceStorageListKeysRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceStorageListKeysRequest(
        int interfaceId,
        int device,
        int axis,
        String prefix
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.prefix = prefix;
    }

    /**
     * Constructor with only required properties.
     */
    public DeviceStorageListKeysRequest(
        int interfaceId,
        int device,
        int axis
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceStorageListKeysRequest other = (DeviceStorageListKeysRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(prefix, other.prefix)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(prefix)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceStorageListKeysRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("prefix: ");
        sb.append(this.prefix);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceStorageListKeysRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceStorageListKeysRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceStorageListKeysRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceStorageListKeysRequest>() {
            @Override
            public DeviceStorageListKeysRequest fromByteArray(byte[] data) {
                return DeviceStorageListKeysRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceStorageListKeysRequest> parser() {
        return PARSER;
    }

}
