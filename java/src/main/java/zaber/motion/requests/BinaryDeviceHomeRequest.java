/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class BinaryDeviceHomeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryDeviceHomeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public BinaryDeviceHomeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public BinaryDeviceHomeRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public BinaryDeviceHomeRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryDeviceHomeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryDeviceHomeRequest(
        int interfaceId,
        int device,
        double timeout,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.timeout = timeout;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryDeviceHomeRequest other = (BinaryDeviceHomeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(timeout, other.timeout)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(timeout),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryDeviceHomeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryDeviceHomeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryDeviceHomeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryDeviceHomeRequest> PARSER =
        new zaber.motion.dto.Parser<BinaryDeviceHomeRequest>() {
            @Override
            public BinaryDeviceHomeRequest fromByteArray(byte[] data) {
                return BinaryDeviceHomeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryDeviceHomeRequest> parser() {
        return PARSER;
    }

}
