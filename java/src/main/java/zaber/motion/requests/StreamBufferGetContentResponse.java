/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamBufferGetContentResponse implements zaber.motion.dto.Message {

    private String[] bufferLines;

    @com.fasterxml.jackson.annotation.JsonProperty("bufferLines")
    public void setBufferLines(String[] bufferLines) {
        this.bufferLines = bufferLines;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("bufferLines")
    public String[] getBufferLines() {
        return this.bufferLines;
    }

    public StreamBufferGetContentResponse withBufferLines(String[] aBufferLines) {
        this.setBufferLines(aBufferLines);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamBufferGetContentResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamBufferGetContentResponse(
        String[] bufferLines
    ) {
        this.bufferLines = bufferLines;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamBufferGetContentResponse other = (StreamBufferGetContentResponse) obj;

        return (
            EqualityUtility.equals(bufferLines, other.bufferLines)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(bufferLines)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamBufferGetContentResponse { ");
        sb.append("bufferLines: ");
        sb.append(this.bufferLines);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamBufferGetContentResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamBufferGetContentResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamBufferGetContentResponse> PARSER =
        new zaber.motion.dto.Parser<StreamBufferGetContentResponse>() {
            @Override
            public StreamBufferGetContentResponse fromByteArray(byte[] data) {
                return StreamBufferGetContentResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamBufferGetContentResponse> parser() {
        return PARSER;
    }

}
