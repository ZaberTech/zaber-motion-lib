/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetAllAnalogIORequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceGetAllAnalogIORequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceGetAllAnalogIORequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private String channelType;

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public String getChannelType() {
        return this.channelType;
    }

    public DeviceGetAllAnalogIORequest withChannelType(String aChannelType) {
        this.setChannelType(aChannelType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetAllAnalogIORequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetAllAnalogIORequest(
        int interfaceId,
        int device,
        String channelType
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelType = channelType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetAllAnalogIORequest other = (DeviceGetAllAnalogIORequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelType, other.channelType)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelType)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetAllAnalogIORequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelType: ");
        sb.append(this.channelType);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetAllAnalogIORequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetAllAnalogIORequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetAllAnalogIORequest> PARSER =
        new zaber.motion.dto.Parser<DeviceGetAllAnalogIORequest>() {
            @Override
            public DeviceGetAllAnalogIORequest fromByteArray(byte[] data) {
                return DeviceGetAllAnalogIORequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetAllAnalogIORequest> parser() {
        return PARSER;
    }

}
