/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceSetAllAnalogOutputsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetAllAnalogOutputsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetAllAnalogOutputsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private double[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    public DeviceSetAllAnalogOutputsRequest withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetAllAnalogOutputsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetAllAnalogOutputsRequest(
        int interfaceId,
        int device,
        double[] values
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetAllAnalogOutputsRequest other = (DeviceSetAllAnalogOutputsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetAllAnalogOutputsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetAllAnalogOutputsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetAllAnalogOutputsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetAllAnalogOutputsRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetAllAnalogOutputsRequest>() {
            @Override
            public DeviceSetAllAnalogOutputsRequest fromByteArray(byte[] data) {
                return DeviceSetAllAnalogOutputsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetAllAnalogOutputsRequest> parser() {
        return PARSER;
    }

}
