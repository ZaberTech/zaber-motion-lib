/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.TriggerAction;
import zaber.motion.ascii.TriggerOperation;
import zaber.motion.EqualityUtility;


public final class TriggerOnFireSetToSettingRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerOnFireSetToSettingRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerOnFireSetToSettingRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerOnFireSetToSettingRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private TriggerAction action;

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public void setAction(TriggerAction action) {
        this.action = action;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public TriggerAction getAction() {
        return this.action;
    }

    public TriggerOnFireSetToSettingRequest withAction(TriggerAction aAction) {
        this.setAction(aAction);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public TriggerOnFireSetToSettingRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    public TriggerOnFireSetToSettingRequest withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private TriggerOperation operation;

    @com.fasterxml.jackson.annotation.JsonProperty("operation")
    public void setOperation(TriggerOperation operation) {
        this.operation = operation;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("operation")
    public TriggerOperation getOperation() {
        return this.operation;
    }

    public TriggerOnFireSetToSettingRequest withOperation(TriggerOperation aOperation) {
        this.setOperation(aOperation);
        return this;
    }

    private int fromAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("fromAxis")
    public void setFromAxis(int fromAxis) {
        this.fromAxis = fromAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fromAxis")
    public int getFromAxis() {
        return this.fromAxis;
    }

    public TriggerOnFireSetToSettingRequest withFromAxis(int aFromAxis) {
        this.setFromAxis(aFromAxis);
        return this;
    }

    private String fromSetting;

    @com.fasterxml.jackson.annotation.JsonProperty("fromSetting")
    public void setFromSetting(String fromSetting) {
        this.fromSetting = fromSetting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fromSetting")
    public String getFromSetting() {
        return this.fromSetting;
    }

    public TriggerOnFireSetToSettingRequest withFromSetting(String aFromSetting) {
        this.setFromSetting(aFromSetting);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerOnFireSetToSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerOnFireSetToSettingRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        int fromAxis,
        String fromSetting
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.action = action;
        this.axis = axis;
        this.setting = setting;
        this.operation = operation;
        this.fromAxis = fromAxis;
        this.fromSetting = fromSetting;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerOnFireSetToSettingRequest other = (TriggerOnFireSetToSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(action, other.action)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(operation, other.operation)
            && EqualityUtility.equals(fromAxis, other.fromAxis)
            && EqualityUtility.equals(fromSetting, other.fromSetting)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(action),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(operation),
            EqualityUtility.generateHashCode(fromAxis),
            EqualityUtility.generateHashCode(fromSetting)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerOnFireSetToSettingRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("action: ");
        sb.append(this.action);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("operation: ");
        sb.append(this.operation);
        sb.append(", ");
        sb.append("fromAxis: ");
        sb.append(this.fromAxis);
        sb.append(", ");
        sb.append("fromSetting: ");
        sb.append(this.fromSetting);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerOnFireSetToSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerOnFireSetToSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerOnFireSetToSettingRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerOnFireSetToSettingRequest>() {
            @Override
            public TriggerOnFireSetToSettingRequest fromByteArray(byte[] data) {
                return TriggerOnFireSetToSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerOnFireSetToSettingRequest> parser() {
        return PARSER;
    }

}
