/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.MicroscopeConfig;
import zaber.motion.EqualityUtility;


public final class MicroscopeConfigResponse implements zaber.motion.dto.Message {

    private MicroscopeConfig config;

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public void setConfig(MicroscopeConfig config) {
        this.config = config;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("config")
    public MicroscopeConfig getConfig() {
        return this.config;
    }

    public MicroscopeConfigResponse withConfig(MicroscopeConfig aConfig) {
        this.setConfig(aConfig);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeConfigResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeConfigResponse(
        MicroscopeConfig config
    ) {
        this.config = config;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeConfigResponse other = (MicroscopeConfigResponse) obj;

        return (
            EqualityUtility.equals(config, other.config)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(config)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeConfigResponse { ");
        sb.append("config: ");
        sb.append(this.config);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeConfigResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeConfigResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeConfigResponse> PARSER =
        new zaber.motion.dto.Parser<MicroscopeConfigResponse>() {
            @Override
            public MicroscopeConfigResponse fromByteArray(byte[] data) {
                return MicroscopeConfigResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeConfigResponse> parser() {
        return PARSER;
    }

}
