/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.IoPortType;
import zaber.motion.ascii.TriggerCondition;
import zaber.motion.EqualityUtility;


public final class TriggerFireWhenIoRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerFireWhenIoRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerFireWhenIoRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerFireWhenIoRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private IoPortType portType;

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public void setPortType(IoPortType portType) {
        this.portType = portType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public IoPortType getPortType() {
        return this.portType;
    }

    public TriggerFireWhenIoRequest withPortType(IoPortType aPortType) {
        this.setPortType(aPortType);
        return this;
    }

    private int channel;

    @com.fasterxml.jackson.annotation.JsonProperty("channel")
    public void setChannel(int channel) {
        this.channel = channel;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channel")
    public int getChannel() {
        return this.channel;
    }

    public TriggerFireWhenIoRequest withChannel(int aChannel) {
        this.setChannel(aChannel);
        return this;
    }

    private TriggerCondition triggerCondition;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerCondition")
    public void setTriggerCondition(TriggerCondition triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerCondition")
    public TriggerCondition getTriggerCondition() {
        return this.triggerCondition;
    }

    public TriggerFireWhenIoRequest withTriggerCondition(TriggerCondition aTriggerCondition) {
        this.setTriggerCondition(aTriggerCondition);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public TriggerFireWhenIoRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerFireWhenIoRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerFireWhenIoRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        IoPortType portType,
        int channel,
        TriggerCondition triggerCondition,
        double value
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.portType = portType;
        this.channel = channel;
        this.triggerCondition = triggerCondition;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerFireWhenIoRequest other = (TriggerFireWhenIoRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(portType, other.portType)
            && EqualityUtility.equals(channel, other.channel)
            && EqualityUtility.equals(triggerCondition, other.triggerCondition)
            && EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(portType),
            EqualityUtility.generateHashCode(channel),
            EqualityUtility.generateHashCode(triggerCondition),
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerFireWhenIoRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("portType: ");
        sb.append(this.portType);
        sb.append(", ");
        sb.append("channel: ");
        sb.append(this.channel);
        sb.append(", ");
        sb.append("triggerCondition: ");
        sb.append(this.triggerCondition);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerFireWhenIoRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerFireWhenIoRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerFireWhenIoRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerFireWhenIoRequest>() {
            @Override
            public TriggerFireWhenIoRequest fromByteArray(byte[] data) {
                return TriggerFireWhenIoRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerFireWhenIoRequest> parser() {
        return PARSER;
    }

}
