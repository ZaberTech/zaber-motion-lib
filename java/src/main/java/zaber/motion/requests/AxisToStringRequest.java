/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class AxisToStringRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public AxisToStringRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public AxisToStringRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public AxisToStringRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String typeOverride;

    @com.fasterxml.jackson.annotation.JsonProperty("typeOverride")
    public void setTypeOverride(String typeOverride) {
        this.typeOverride = typeOverride;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("typeOverride")
    public String getTypeOverride() {
        return this.typeOverride;
    }

    public AxisToStringRequest withTypeOverride(String aTypeOverride) {
        this.setTypeOverride(aTypeOverride);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisToStringRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisToStringRequest(
        int interfaceId,
        int device,
        int axis,
        String typeOverride
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.typeOverride = typeOverride;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisToStringRequest other = (AxisToStringRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(typeOverride, other.typeOverride)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(typeOverride)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisToStringRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("typeOverride: ");
        sb.append(this.typeOverride);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisToStringRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisToStringRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisToStringRequest> PARSER =
        new zaber.motion.dto.Parser<AxisToStringRequest>() {
            @Override
            public AxisToStringRequest fromByteArray(byte[] data) {
                return AxisToStringRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisToStringRequest> parser() {
        return PARSER;
    }

}
