/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class TriggerFireWhenDistanceTravelledRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerFireWhenDistanceTravelledRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerFireWhenDistanceTravelledRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerFireWhenDistanceTravelledRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public TriggerFireWhenDistanceTravelledRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double distance;

    @com.fasterxml.jackson.annotation.JsonProperty("distance")
    public void setDistance(double distance) {
        this.distance = distance;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("distance")
    public double getDistance() {
        return this.distance;
    }

    public TriggerFireWhenDistanceTravelledRequest withDistance(double aDistance) {
        this.setDistance(aDistance);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public TriggerFireWhenDistanceTravelledRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerFireWhenDistanceTravelledRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerFireWhenDistanceTravelledRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        int axis,
        double distance,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.axis = axis;
        this.distance = distance;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerFireWhenDistanceTravelledRequest other = (TriggerFireWhenDistanceTravelledRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(distance, other.distance)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(distance),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerFireWhenDistanceTravelledRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("distance: ");
        sb.append(this.distance);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerFireWhenDistanceTravelledRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerFireWhenDistanceTravelledRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerFireWhenDistanceTravelledRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerFireWhenDistanceTravelledRequest>() {
            @Override
            public TriggerFireWhenDistanceTravelledRequest fromByteArray(byte[] data) {
                return TriggerFireWhenDistanceTravelledRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerFireWhenDistanceTravelledRequest> parser() {
        return PARSER;
    }

}
