/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.StreamAxisDefinition;
import zaber.motion.ascii.PvtAxisDefinition;
import zaber.motion.EqualityUtility;


public final class StreamSetupLiveCompositeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetupLiveCompositeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetupLiveCompositeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetupLiveCompositeRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetupLiveCompositeRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private StreamAxisDefinition[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(StreamAxisDefinition[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public StreamAxisDefinition[] getAxes() {
        return this.axes;
    }

    public StreamSetupLiveCompositeRequest withAxes(StreamAxisDefinition[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private PvtAxisDefinition[] pvtAxes;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public void setPvtAxes(PvtAxisDefinition[] pvtAxes) {
        this.pvtAxes = pvtAxes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtAxes")
    public PvtAxisDefinition[] getPvtAxes() {
        return this.pvtAxes;
    }

    public StreamSetupLiveCompositeRequest withPvtAxes(PvtAxisDefinition[] aPvtAxes) {
        this.setPvtAxes(aPvtAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetupLiveCompositeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetupLiveCompositeRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        StreamAxisDefinition[] axes,
        PvtAxisDefinition[] pvtAxes
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.axes = axes;
        this.pvtAxes = pvtAxes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetupLiveCompositeRequest other = (StreamSetupLiveCompositeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(pvtAxes, other.pvtAxes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(pvtAxes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetupLiveCompositeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("pvtAxes: ");
        sb.append(this.pvtAxes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetupLiveCompositeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetupLiveCompositeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetupLiveCompositeRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetupLiveCompositeRequest>() {
            @Override
            public StreamSetupLiveCompositeRequest fromByteArray(byte[] data) {
                return StreamSetupLiveCompositeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetupLiveCompositeRequest> parser() {
        return PARSER;
    }

}
