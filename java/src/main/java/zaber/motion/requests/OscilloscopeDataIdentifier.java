/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeDataIdentifier implements zaber.motion.dto.Message {

    private int dataId;

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("dataId")
    public int getDataId() {
        return this.dataId;
    }

    public OscilloscopeDataIdentifier withDataId(int aDataId) {
        this.setDataId(aDataId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeDataIdentifier() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeDataIdentifier(
        int dataId
    ) {
        this.dataId = dataId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeDataIdentifier other = (OscilloscopeDataIdentifier) obj;

        return (
            EqualityUtility.equals(dataId, other.dataId)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataId)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeDataIdentifier { ");
        sb.append("dataId: ");
        sb.append(this.dataId);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeDataIdentifier fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeDataIdentifier.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeDataIdentifier> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeDataIdentifier>() {
            @Override
            public OscilloscopeDataIdentifier fromByteArray(byte[] data) {
                return OscilloscopeDataIdentifier.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeDataIdentifier> parser() {
        return PARSER;
    }

}
