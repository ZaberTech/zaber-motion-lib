/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamSetupStoreArbitraryAxesRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetupStoreArbitraryAxesRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetupStoreArbitraryAxesRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetupStoreArbitraryAxesRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetupStoreArbitraryAxesRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private int streamBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public void setStreamBuffer(int streamBuffer) {
        this.streamBuffer = streamBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public int getStreamBuffer() {
        return this.streamBuffer;
    }

    public StreamSetupStoreArbitraryAxesRequest withStreamBuffer(int aStreamBuffer) {
        this.setStreamBuffer(aStreamBuffer);
        return this;
    }

    private int pvtBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public void setPvtBuffer(int pvtBuffer) {
        this.pvtBuffer = pvtBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public int getPvtBuffer() {
        return this.pvtBuffer;
    }

    public StreamSetupStoreArbitraryAxesRequest withPvtBuffer(int aPvtBuffer) {
        this.setPvtBuffer(aPvtBuffer);
        return this;
    }

    private int axesCount;

    @com.fasterxml.jackson.annotation.JsonProperty("axesCount")
    public void setAxesCount(int axesCount) {
        this.axesCount = axesCount;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axesCount")
    public int getAxesCount() {
        return this.axesCount;
    }

    public StreamSetupStoreArbitraryAxesRequest withAxesCount(int aAxesCount) {
        this.setAxesCount(aAxesCount);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetupStoreArbitraryAxesRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetupStoreArbitraryAxesRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        int streamBuffer,
        int pvtBuffer,
        int axesCount
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.streamBuffer = streamBuffer;
        this.pvtBuffer = pvtBuffer;
        this.axesCount = axesCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetupStoreArbitraryAxesRequest other = (StreamSetupStoreArbitraryAxesRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(streamBuffer, other.streamBuffer)
            && EqualityUtility.equals(pvtBuffer, other.pvtBuffer)
            && EqualityUtility.equals(axesCount, other.axesCount)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(streamBuffer),
            EqualityUtility.generateHashCode(pvtBuffer),
            EqualityUtility.generateHashCode(axesCount)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetupStoreArbitraryAxesRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("streamBuffer: ");
        sb.append(this.streamBuffer);
        sb.append(", ");
        sb.append("pvtBuffer: ");
        sb.append(this.pvtBuffer);
        sb.append(", ");
        sb.append("axesCount: ");
        sb.append(this.axesCount);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetupStoreArbitraryAxesRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetupStoreArbitraryAxesRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetupStoreArbitraryAxesRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetupStoreArbitraryAxesRequest>() {
            @Override
            public StreamSetupStoreArbitraryAxesRequest fromByteArray(byte[] data) {
                return StreamSetupStoreArbitraryAxesRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetupStoreArbitraryAxesRequest> parser() {
        return PARSER;
    }

}
