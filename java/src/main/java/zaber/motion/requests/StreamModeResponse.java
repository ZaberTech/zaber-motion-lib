/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.StreamMode;
import zaber.motion.ascii.PvtMode;
import zaber.motion.EqualityUtility;


public final class StreamModeResponse implements zaber.motion.dto.Message {

    private StreamMode streamMode;

    @com.fasterxml.jackson.annotation.JsonProperty("streamMode")
    public void setStreamMode(StreamMode streamMode) {
        this.streamMode = streamMode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamMode")
    public StreamMode getStreamMode() {
        return this.streamMode;
    }

    public StreamModeResponse withStreamMode(StreamMode aStreamMode) {
        this.setStreamMode(aStreamMode);
        return this;
    }

    private PvtMode pvtMode;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtMode")
    public void setPvtMode(PvtMode pvtMode) {
        this.pvtMode = pvtMode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtMode")
    public PvtMode getPvtMode() {
        return this.pvtMode;
    }

    public StreamModeResponse withPvtMode(PvtMode aPvtMode) {
        this.setPvtMode(aPvtMode);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamModeResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamModeResponse(
        StreamMode streamMode,
        PvtMode pvtMode
    ) {
        this.streamMode = streamMode;
        this.pvtMode = pvtMode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamModeResponse other = (StreamModeResponse) obj;

        return (
            EqualityUtility.equals(streamMode, other.streamMode)
            && EqualityUtility.equals(pvtMode, other.pvtMode)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(streamMode),
            EqualityUtility.generateHashCode(pvtMode)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamModeResponse { ");
        sb.append("streamMode: ");
        sb.append(this.streamMode);
        sb.append(", ");
        sb.append("pvtMode: ");
        sb.append(this.pvtMode);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamModeResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamModeResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamModeResponse> PARSER =
        new zaber.motion.dto.Parser<StreamModeResponse>() {
            @Override
            public StreamModeResponse fromByteArray(byte[] data) {
                return StreamModeResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamModeResponse> parser() {
        return PARSER;
    }

}
