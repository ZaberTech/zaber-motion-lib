/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Event that is sent when a connection is lost.
 */
public final class DisconnectedEvent implements zaber.motion.dto.Message {

    private int interfaceId;

    /**
     * The id of the interface that was disconnected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    /**
     * The id of the interface that was disconnected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    /**
     * The id of the interface that was disconnected.
     */
    public DisconnectedEvent withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private Errors errorType;

    /**
     * The type of error that caused the disconnection.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorType")
    public void setErrorType(Errors errorType) {
        this.errorType = errorType;
    }

    /**
     * The type of error that caused the disconnection.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorType")
    public Errors getErrorType() {
        return this.errorType;
    }

    /**
     * The type of error that caused the disconnection.
     */
    public DisconnectedEvent withErrorType(Errors aErrorType) {
        this.setErrorType(aErrorType);
        return this;
    }

    private String errorMessage;

    /**
     * The message describing the error.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * The message describing the error.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public String getErrorMessage() {
        return this.errorMessage;
    }

    /**
     * The message describing the error.
     */
    public DisconnectedEvent withErrorMessage(String aErrorMessage) {
        this.setErrorMessage(aErrorMessage);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DisconnectedEvent() {
    }

    /**
     * Constructor with all properties.
     */
    public DisconnectedEvent(
        int interfaceId,
        Errors errorType,
        String errorMessage
    ) {
        this.interfaceId = interfaceId;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DisconnectedEvent other = (DisconnectedEvent) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(errorType, other.errorType)
            && EqualityUtility.equals(errorMessage, other.errorMessage)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(errorType),
            EqualityUtility.generateHashCode(errorMessage)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DisconnectedEvent { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("errorType: ");
        sb.append(this.errorType);
        sb.append(", ");
        sb.append("errorMessage: ");
        sb.append(this.errorMessage);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DisconnectedEvent fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DisconnectedEvent.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DisconnectedEvent> PARSER =
        new zaber.motion.dto.Parser<DisconnectedEvent>() {
            @Override
            public DisconnectedEvent fromByteArray(byte[] data) {
                return DisconnectedEvent.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DisconnectedEvent> parser() {
        return PARSER;
    }

}
