/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class ForgetDevicesRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ForgetDevicesRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int[] exceptDevices;

    @com.fasterxml.jackson.annotation.JsonProperty("exceptDevices")
    public void setExceptDevices(int[] exceptDevices) {
        this.exceptDevices = exceptDevices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("exceptDevices")
    public int[] getExceptDevices() {
        return this.exceptDevices;
    }

    public ForgetDevicesRequest withExceptDevices(int[] aExceptDevices) {
        this.setExceptDevices(aExceptDevices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ForgetDevicesRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public ForgetDevicesRequest(
        int interfaceId,
        int[] exceptDevices
    ) {
        this.interfaceId = interfaceId;
        this.exceptDevices = exceptDevices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ForgetDevicesRequest other = (ForgetDevicesRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(exceptDevices, other.exceptDevices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(exceptDevices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ForgetDevicesRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("exceptDevices: ");
        sb.append(this.exceptDevices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ForgetDevicesRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ForgetDevicesRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ForgetDevicesRequest> PARSER =
        new zaber.motion.dto.Parser<ForgetDevicesRequest>() {
            @Override
            public ForgetDevicesRequest fromByteArray(byte[] data) {
                return ForgetDevicesRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ForgetDevicesRequest> parser() {
        return PARSER;
    }

}
