/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.TriggerAction;
import zaber.motion.EqualityUtility;


public final class TriggerOnFireRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public TriggerOnFireRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public TriggerOnFireRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int triggerNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public void setTriggerNumber(int triggerNumber) {
        this.triggerNumber = triggerNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("triggerNumber")
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public TriggerOnFireRequest withTriggerNumber(int aTriggerNumber) {
        this.setTriggerNumber(aTriggerNumber);
        return this;
    }

    private TriggerAction action;

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public void setAction(TriggerAction action) {
        this.action = action;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("action")
    public TriggerAction getAction() {
        return this.action;
    }

    public TriggerOnFireRequest withAction(TriggerAction aAction) {
        this.setAction(aAction);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public TriggerOnFireRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String command;

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(String command) {
        this.command = command;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public String getCommand() {
        return this.command;
    }

    public TriggerOnFireRequest withCommand(String aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerOnFireRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerOnFireRequest(
        int interfaceId,
        int device,
        int triggerNumber,
        TriggerAction action,
        int axis,
        String command
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.triggerNumber = triggerNumber;
        this.action = action;
        this.axis = axis;
        this.command = command;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerOnFireRequest other = (TriggerOnFireRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(triggerNumber, other.triggerNumber)
            && EqualityUtility.equals(action, other.action)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(command, other.command)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(triggerNumber),
            EqualityUtility.generateHashCode(action),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(command)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerOnFireRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("triggerNumber: ");
        sb.append(this.triggerNumber);
        sb.append(", ");
        sb.append("action: ");
        sb.append(this.action);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerOnFireRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerOnFireRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerOnFireRequest> PARSER =
        new zaber.motion.dto.Parser<TriggerOnFireRequest>() {
            @Override
            public TriggerOnFireRequest fromByteArray(byte[] data) {
                return TriggerOnFireRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerOnFireRequest> parser() {
        return PARSER;
    }

}
