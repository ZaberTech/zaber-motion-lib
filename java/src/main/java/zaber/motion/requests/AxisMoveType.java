/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

public enum AxisMoveType {

    ABS(0),

    REL(1),

    VEL(2),

    MAX(3),

    MIN(4),

    INDEX(5);

    private int value;

    AxisMoveType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static AxisMoveType valueOf(int argValue) {
        for (AxisMoveType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
