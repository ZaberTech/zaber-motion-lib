/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class SetStateRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public SetStateRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public SetStateRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public SetStateRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String state;

    @com.fasterxml.jackson.annotation.JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("state")
    public String getState() {
        return this.state;
    }

    public SetStateRequest withState(String aState) {
        this.setState(aState);
        return this;
    }

    private boolean deviceOnly;

    @com.fasterxml.jackson.annotation.JsonProperty("deviceOnly")
    public void setDeviceOnly(boolean deviceOnly) {
        this.deviceOnly = deviceOnly;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("deviceOnly")
    public boolean getDeviceOnly() {
        return this.deviceOnly;
    }

    public SetStateRequest withDeviceOnly(boolean aDeviceOnly) {
        this.setDeviceOnly(aDeviceOnly);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetStateRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public SetStateRequest(
        int interfaceId,
        int device,
        int axis,
        String state,
        boolean deviceOnly
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.state = state;
        this.deviceOnly = deviceOnly;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetStateRequest other = (SetStateRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(state, other.state)
            && EqualityUtility.equals(deviceOnly, other.deviceOnly)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(state),
            EqualityUtility.generateHashCode(deviceOnly)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetStateRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("state: ");
        sb.append(this.state);
        sb.append(", ");
        sb.append("deviceOnly: ");
        sb.append(this.deviceOnly);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetStateRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetStateRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetStateRequest> PARSER =
        new zaber.motion.dto.Parser<SetStateRequest>() {
            @Override
            public SetStateRequest fromByteArray(byte[] data) {
                return SetStateRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetStateRequest> parser() {
        return PARSER;
    }

}
