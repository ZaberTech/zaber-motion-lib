/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.microscopy.WdiAutofocusProviderStatus;
import zaber.motion.EqualityUtility;


public final class WdiGetStatusResponse implements zaber.motion.dto.Message {

    private WdiAutofocusProviderStatus status;

    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public void setStatus(WdiAutofocusProviderStatus status) {
        this.status = status;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public WdiAutofocusProviderStatus getStatus() {
        return this.status;
    }

    public WdiGetStatusResponse withStatus(WdiAutofocusProviderStatus aStatus) {
        this.setStatus(aStatus);
        return this;
    }

    /**
     * Empty constructor.
     */
    public WdiGetStatusResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public WdiGetStatusResponse(
        WdiAutofocusProviderStatus status
    ) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WdiGetStatusResponse other = (WdiGetStatusResponse) obj;

        return (
            EqualityUtility.equals(status, other.status)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(status)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WdiGetStatusResponse { ");
        sb.append("status: ");
        sb.append(this.status);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static WdiGetStatusResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, WdiGetStatusResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<WdiGetStatusResponse> PARSER =
        new zaber.motion.dto.Parser<WdiGetStatusResponse>() {
            @Override
            public WdiGetStatusResponse fromByteArray(byte[] data) {
                return WdiGetStatusResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<WdiGetStatusResponse> parser() {
        return PARSER;
    }

}
