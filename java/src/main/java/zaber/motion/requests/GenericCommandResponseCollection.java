/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.Response;
import zaber.motion.EqualityUtility;


public final class GenericCommandResponseCollection implements zaber.motion.dto.Message {

    private Response[] responses;

    @com.fasterxml.jackson.annotation.JsonProperty("responses")
    public void setResponses(Response[] responses) {
        this.responses = responses;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("responses")
    public Response[] getResponses() {
        return this.responses;
    }

    public GenericCommandResponseCollection withResponses(Response[] aResponses) {
        this.setResponses(aResponses);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GenericCommandResponseCollection() {
    }

    /**
     * Constructor with all properties.
     */
    public GenericCommandResponseCollection(
        Response[] responses
    ) {
        this.responses = responses;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GenericCommandResponseCollection other = (GenericCommandResponseCollection) obj;

        return (
            EqualityUtility.equals(responses, other.responses)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(responses)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GenericCommandResponseCollection { ");
        sb.append("responses: ");
        sb.append(this.responses);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GenericCommandResponseCollection fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GenericCommandResponseCollection.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GenericCommandResponseCollection> PARSER =
        new zaber.motion.dto.Parser<GenericCommandResponseCollection>() {
            @Override
            public GenericCommandResponseCollection fromByteArray(byte[] data) {
                return GenericCommandResponseCollection.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GenericCommandResponseCollection> parser() {
        return PARSER;
    }

}
