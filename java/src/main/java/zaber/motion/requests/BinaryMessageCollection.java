/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.Message;
import zaber.motion.EqualityUtility;


public final class BinaryMessageCollection implements zaber.motion.dto.Message {

    private Message[] messages;

    @com.fasterxml.jackson.annotation.JsonProperty("messages")
    public void setMessages(Message[] messages) {
        this.messages = messages;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("messages")
    public Message[] getMessages() {
        return this.messages;
    }

    public BinaryMessageCollection withMessages(Message[] aMessages) {
        this.setMessages(aMessages);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryMessageCollection() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryMessageCollection(
        Message[] messages
    ) {
        this.messages = messages;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryMessageCollection other = (BinaryMessageCollection) obj;

        return (
            EqualityUtility.equals(messages, other.messages)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(messages)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryMessageCollection { ");
        sb.append("messages: ");
        sb.append(this.messages);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryMessageCollection fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryMessageCollection.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryMessageCollection> PARSER =
        new zaber.motion.dto.Parser<BinaryMessageCollection>() {
            @Override
            public BinaryMessageCollection fromByteArray(byte[] data) {
                return BinaryMessageCollection.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryMessageCollection> parser() {
        return PARSER;
    }

}
