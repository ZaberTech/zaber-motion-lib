/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class DeviceMoveRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceMoveRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceMoveRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceMoveRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean waitUntilIdle;

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public void setWaitUntilIdle(boolean waitUntilIdle) {
        this.waitUntilIdle = waitUntilIdle;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("waitUntilIdle")
    public boolean getWaitUntilIdle() {
        return this.waitUntilIdle;
    }

    public DeviceMoveRequest withWaitUntilIdle(boolean aWaitUntilIdle) {
        this.setWaitUntilIdle(aWaitUntilIdle);
        return this;
    }

    private AxisMoveType type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(AxisMoveType type) {
        this.type = type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public AxisMoveType getType() {
        return this.type;
    }

    public DeviceMoveRequest withType(AxisMoveType aType) {
        this.setType(aType);
        return this;
    }

    private double arg;

    @com.fasterxml.jackson.annotation.JsonProperty("arg")
    public void setArg(double arg) {
        this.arg = arg;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("arg")
    public double getArg() {
        return this.arg;
    }

    public DeviceMoveRequest withArg(double aArg) {
        this.setArg(aArg);
        return this;
    }

    private int argInt;

    @com.fasterxml.jackson.annotation.JsonProperty("argInt")
    public void setArgInt(int argInt) {
        this.argInt = argInt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("argInt")
    public int getArgInt() {
        return this.argInt;
    }

    public DeviceMoveRequest withArgInt(int aArgInt) {
        this.setArgInt(aArgInt);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public DeviceMoveRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    private double velocity;

    @com.fasterxml.jackson.annotation.JsonProperty("velocity")
    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("velocity")
    public double getVelocity() {
        return this.velocity;
    }

    public DeviceMoveRequest withVelocity(double aVelocity) {
        this.setVelocity(aVelocity);
        return this;
    }

    private Units velocityUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("velocityUnit")
    public void setVelocityUnit(Units velocityUnit) {
        this.velocityUnit = velocityUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("velocityUnit")
    public Units getVelocityUnit() {
        return this.velocityUnit;
    }

    public DeviceMoveRequest withVelocityUnit(Units aVelocityUnit) {
        this.setVelocityUnit(aVelocityUnit);
        return this;
    }

    private double acceleration;

    @com.fasterxml.jackson.annotation.JsonProperty("acceleration")
    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("acceleration")
    public double getAcceleration() {
        return this.acceleration;
    }

    public DeviceMoveRequest withAcceleration(double aAcceleration) {
        this.setAcceleration(aAcceleration);
        return this;
    }

    private Units accelerationUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("accelerationUnit")
    public void setAccelerationUnit(Units accelerationUnit) {
        this.accelerationUnit = accelerationUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("accelerationUnit")
    public Units getAccelerationUnit() {
        return this.accelerationUnit;
    }

    public DeviceMoveRequest withAccelerationUnit(Units aAccelerationUnit) {
        this.setAccelerationUnit(aAccelerationUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceMoveRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceMoveRequest(
        int interfaceId,
        int device,
        int axis,
        boolean waitUntilIdle,
        AxisMoveType type,
        double arg,
        int argInt,
        Units unit,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.waitUntilIdle = waitUntilIdle;
        this.type = type;
        this.arg = arg;
        this.argInt = argInt;
        this.unit = unit;
        this.velocity = velocity;
        this.velocityUnit = velocityUnit;
        this.acceleration = acceleration;
        this.accelerationUnit = accelerationUnit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceMoveRequest other = (DeviceMoveRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(waitUntilIdle, other.waitUntilIdle)
            && EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(arg, other.arg)
            && EqualityUtility.equals(argInt, other.argInt)
            && EqualityUtility.equals(unit, other.unit)
            && EqualityUtility.equals(velocity, other.velocity)
            && EqualityUtility.equals(velocityUnit, other.velocityUnit)
            && EqualityUtility.equals(acceleration, other.acceleration)
            && EqualityUtility.equals(accelerationUnit, other.accelerationUnit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(waitUntilIdle),
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(arg),
            EqualityUtility.generateHashCode(argInt),
            EqualityUtility.generateHashCode(unit),
            EqualityUtility.generateHashCode(velocity),
            EqualityUtility.generateHashCode(velocityUnit),
            EqualityUtility.generateHashCode(acceleration),
            EqualityUtility.generateHashCode(accelerationUnit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceMoveRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("waitUntilIdle: ");
        sb.append(this.waitUntilIdle);
        sb.append(", ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("arg: ");
        sb.append(this.arg);
        sb.append(", ");
        sb.append("argInt: ");
        sb.append(this.argInt);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(", ");
        sb.append("velocity: ");
        sb.append(this.velocity);
        sb.append(", ");
        sb.append("velocityUnit: ");
        sb.append(this.velocityUnit);
        sb.append(", ");
        sb.append("acceleration: ");
        sb.append(this.acceleration);
        sb.append(", ");
        sb.append("accelerationUnit: ");
        sb.append(this.accelerationUnit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceMoveRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceMoveRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceMoveRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceMoveRequest>() {
            @Override
            public DeviceMoveRequest fromByteArray(byte[] data) {
                return DeviceMoveRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceMoveRequest> parser() {
        return PARSER;
    }

}
