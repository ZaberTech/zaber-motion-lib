/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.TriggerEnabledState;
import zaber.motion.EqualityUtility;


public final class TriggerEnabledStates implements zaber.motion.dto.Message {

    private TriggerEnabledState[] states;

    @com.fasterxml.jackson.annotation.JsonProperty("states")
    public void setStates(TriggerEnabledState[] states) {
        this.states = states;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("states")
    public TriggerEnabledState[] getStates() {
        return this.states;
    }

    public TriggerEnabledStates withStates(TriggerEnabledState[] aStates) {
        this.setStates(aStates);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerEnabledStates() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerEnabledStates(
        TriggerEnabledState[] states
    ) {
        this.states = states;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerEnabledStates other = (TriggerEnabledStates) obj;

        return (
            EqualityUtility.equals(states, other.states)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(states)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerEnabledStates { ");
        sb.append("states: ");
        sb.append(this.states);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerEnabledStates fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerEnabledStates.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerEnabledStates> PARSER =
        new zaber.motion.dto.Parser<TriggerEnabledStates>() {
            @Override
            public TriggerEnabledStates fromByteArray(byte[] data) {
                return TriggerEnabledStates.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerEnabledStates> parser() {
        return PARSER;
    }

}
