/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OscilloscopeRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public OscilloscopeRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public OscilloscopeRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeRequest(
        int interfaceId,
        int device
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeRequest other = (OscilloscopeRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeRequest> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeRequest>() {
            @Override
            public OscilloscopeRequest fromByteArray(byte[] data) {
                return OscilloscopeRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeRequest> parser() {
        return PARSER;
    }

}
