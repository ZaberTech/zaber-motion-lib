/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DoubleArrayResponse implements zaber.motion.dto.Message {

    private double[] values;

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    public DoubleArrayResponse withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DoubleArrayResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DoubleArrayResponse(
        double[] values
    ) {
        this.values = values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DoubleArrayResponse other = (DoubleArrayResponse) obj;

        return (
            EqualityUtility.equals(values, other.values)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(values)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DoubleArrayResponse { ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DoubleArrayResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DoubleArrayResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DoubleArrayResponse> PARSER =
        new zaber.motion.dto.Parser<DoubleArrayResponse>() {
            @Override
            public DoubleArrayResponse fromByteArray(byte[] data) {
                return DoubleArrayResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DoubleArrayResponse> parser() {
        return PARSER;
    }

}
