/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TestEvent implements zaber.motion.dto.Message {

    private String data;

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public String getData() {
        return this.data;
    }

    public TestEvent withData(String aData) {
        this.setData(aData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TestEvent() {
    }

    /**
     * Constructor with all properties.
     */
    public TestEvent(
        String data
    ) {
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TestEvent other = (TestEvent) obj;

        return (
            EqualityUtility.equals(data, other.data)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(data)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TestEvent { ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TestEvent fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TestEvent.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TestEvent> PARSER =
        new zaber.motion.dto.Parser<TestEvent>() {
            @Override
            public TestEvent fromByteArray(byte[] data) {
                return TestEvent.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TestEvent> parser() {
        return PARSER;
    }

}
