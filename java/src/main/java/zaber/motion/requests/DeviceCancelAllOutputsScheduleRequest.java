/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceCancelAllOutputsScheduleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceCancelAllOutputsScheduleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceCancelAllOutputsScheduleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private boolean analog;

    @com.fasterxml.jackson.annotation.JsonProperty("analog")
    public void setAnalog(boolean analog) {
        this.analog = analog;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("analog")
    public boolean getAnalog() {
        return this.analog;
    }

    public DeviceCancelAllOutputsScheduleRequest withAnalog(boolean aAnalog) {
        this.setAnalog(aAnalog);
        return this;
    }

    private boolean[] channels;

    @com.fasterxml.jackson.annotation.JsonProperty("channels")
    public void setChannels(boolean[] channels) {
        this.channels = channels;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channels")
    public boolean[] getChannels() {
        return this.channels;
    }

    public DeviceCancelAllOutputsScheduleRequest withChannels(boolean[] aChannels) {
        this.setChannels(aChannels);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceCancelAllOutputsScheduleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceCancelAllOutputsScheduleRequest(
        int interfaceId,
        int device,
        boolean analog,
        boolean[] channels
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.analog = analog;
        this.channels = channels;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceCancelAllOutputsScheduleRequest other = (DeviceCancelAllOutputsScheduleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(analog, other.analog)
            && EqualityUtility.equals(channels, other.channels)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(analog),
            EqualityUtility.generateHashCode(channels)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceCancelAllOutputsScheduleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("analog: ");
        sb.append(this.analog);
        sb.append(", ");
        sb.append("channels: ");
        sb.append(this.channels);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceCancelAllOutputsScheduleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceCancelAllOutputsScheduleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceCancelAllOutputsScheduleRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceCancelAllOutputsScheduleRequest>() {
            @Override
            public DeviceCancelAllOutputsScheduleRequest fromByteArray(byte[] data) {
                return DeviceCancelAllOutputsScheduleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceCancelAllOutputsScheduleRequest> parser() {
        return PARSER;
    }

}
