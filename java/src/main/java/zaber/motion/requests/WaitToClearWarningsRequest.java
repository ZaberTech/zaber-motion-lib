/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class WaitToClearWarningsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public WaitToClearWarningsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public WaitToClearWarningsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public WaitToClearWarningsRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public WaitToClearWarningsRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    private String[] warningFlags;

    @com.fasterxml.jackson.annotation.JsonProperty("warningFlags")
    public void setWarningFlags(String[] warningFlags) {
        this.warningFlags = warningFlags;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("warningFlags")
    public String[] getWarningFlags() {
        return this.warningFlags;
    }

    public WaitToClearWarningsRequest withWarningFlags(String[] aWarningFlags) {
        this.setWarningFlags(aWarningFlags);
        return this;
    }

    /**
     * Empty constructor.
     */
    public WaitToClearWarningsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public WaitToClearWarningsRequest(
        int interfaceId,
        int device,
        int axis,
        double timeout,
        String[] warningFlags
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.timeout = timeout;
        this.warningFlags = warningFlags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WaitToClearWarningsRequest other = (WaitToClearWarningsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(timeout, other.timeout)
            && EqualityUtility.equals(warningFlags, other.warningFlags)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(timeout),
            EqualityUtility.generateHashCode(warningFlags)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WaitToClearWarningsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(", ");
        sb.append("warningFlags: ");
        sb.append(this.warningFlags);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static WaitToClearWarningsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, WaitToClearWarningsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<WaitToClearWarningsRequest> PARSER =
        new zaber.motion.dto.Parser<WaitToClearWarningsRequest>() {
            @Override
            public WaitToClearWarningsRequest fromByteArray(byte[] data) {
                return WaitToClearWarningsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<WaitToClearWarningsRequest> parser() {
        return PARSER;
    }

}
