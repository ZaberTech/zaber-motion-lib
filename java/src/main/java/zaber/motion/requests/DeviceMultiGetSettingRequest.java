/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.GetSetting;
import zaber.motion.ascii.GetAxisSetting;
import zaber.motion.EqualityUtility;


public final class DeviceMultiGetSettingRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceMultiGetSettingRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceMultiGetSettingRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceMultiGetSettingRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private GetSetting[] settings;

    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public void setSettings(GetSetting[] settings) {
        this.settings = settings;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public GetSetting[] getSettings() {
        return this.settings;
    }

    public DeviceMultiGetSettingRequest withSettings(GetSetting[] aSettings) {
        this.setSettings(aSettings);
        return this;
    }

    private GetAxisSetting[] axisSettings;

    @com.fasterxml.jackson.annotation.JsonProperty("axisSettings")
    public void setAxisSettings(GetAxisSetting[] axisSettings) {
        this.axisSettings = axisSettings;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axisSettings")
    public GetAxisSetting[] getAxisSettings() {
        return this.axisSettings;
    }

    public DeviceMultiGetSettingRequest withAxisSettings(GetAxisSetting[] aAxisSettings) {
        this.setAxisSettings(aAxisSettings);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceMultiGetSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceMultiGetSettingRequest(
        int interfaceId,
        int device,
        int axis,
        GetSetting[] settings,
        GetAxisSetting[] axisSettings
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.settings = settings;
        this.axisSettings = axisSettings;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceMultiGetSettingRequest other = (DeviceMultiGetSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(settings, other.settings)
            && EqualityUtility.equals(axisSettings, other.axisSettings)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(settings),
            EqualityUtility.generateHashCode(axisSettings)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceMultiGetSettingRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("settings: ");
        sb.append(this.settings);
        sb.append(", ");
        sb.append("axisSettings: ");
        sb.append(this.axisSettings);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceMultiGetSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceMultiGetSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceMultiGetSettingRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceMultiGetSettingRequest>() {
            @Override
            public DeviceMultiGetSettingRequest fromByteArray(byte[] data) {
                return DeviceMultiGetSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceMultiGetSettingRequest> parser() {
        return PARSER;
    }

}
