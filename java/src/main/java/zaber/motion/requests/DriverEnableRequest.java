/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DriverEnableRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DriverEnableRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DriverEnableRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DriverEnableRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public DriverEnableRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DriverEnableRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DriverEnableRequest(
        int interfaceId,
        int device,
        int axis,
        double timeout
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DriverEnableRequest other = (DriverEnableRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DriverEnableRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DriverEnableRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DriverEnableRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DriverEnableRequest> PARSER =
        new zaber.motion.dto.Parser<DriverEnableRequest>() {
            @Override
            public DriverEnableRequest fromByteArray(byte[] data) {
                return DriverEnableRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DriverEnableRequest> parser() {
        return PARSER;
    }

}
