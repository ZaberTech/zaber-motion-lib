/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class OpenInterfaceRequest implements zaber.motion.dto.Message {

    private InterfaceType interfaceType;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceType")
    public void setInterfaceType(InterfaceType interfaceType) {
        this.interfaceType = interfaceType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceType")
    public InterfaceType getInterfaceType() {
        return this.interfaceType;
    }

    public OpenInterfaceRequest withInterfaceType(InterfaceType aInterfaceType) {
        this.setInterfaceType(aInterfaceType);
        return this;
    }

    private String portName;

    @com.fasterxml.jackson.annotation.JsonProperty("portName")
    public void setPortName(String portName) {
        this.portName = portName;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("portName")
    public String getPortName() {
        return this.portName;
    }

    public OpenInterfaceRequest withPortName(String aPortName) {
        this.setPortName(aPortName);
        return this;
    }

    private int baudRate;

    @com.fasterxml.jackson.annotation.JsonProperty("baudRate")
    public void setBaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("baudRate")
    public int getBaudRate() {
        return this.baudRate;
    }

    public OpenInterfaceRequest withBaudRate(int aBaudRate) {
        this.setBaudRate(aBaudRate);
        return this;
    }

    private String hostName;

    @com.fasterxml.jackson.annotation.JsonProperty("hostName")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("hostName")
    public String getHostName() {
        return this.hostName;
    }

    public OpenInterfaceRequest withHostName(String aHostName) {
        this.setHostName(aHostName);
        return this;
    }

    private int port;

    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public int getPort() {
        return this.port;
    }

    public OpenInterfaceRequest withPort(int aPort) {
        this.setPort(aPort);
        return this;
    }

    private int transport;

    @com.fasterxml.jackson.annotation.JsonProperty("transport")
    public void setTransport(int transport) {
        this.transport = transport;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("transport")
    public int getTransport() {
        return this.transport;
    }

    public OpenInterfaceRequest withTransport(int aTransport) {
        this.setTransport(aTransport);
        return this;
    }

    private boolean rejectRoutedConnection;

    @com.fasterxml.jackson.annotation.JsonProperty("rejectRoutedConnection")
    public void setRejectRoutedConnection(boolean rejectRoutedConnection) {
        this.rejectRoutedConnection = rejectRoutedConnection;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("rejectRoutedConnection")
    public boolean getRejectRoutedConnection() {
        return this.rejectRoutedConnection;
    }

    public OpenInterfaceRequest withRejectRoutedConnection(boolean aRejectRoutedConnection) {
        this.setRejectRoutedConnection(aRejectRoutedConnection);
        return this;
    }

    private String cloudId;

    @com.fasterxml.jackson.annotation.JsonProperty("cloudId")
    public void setCloudId(String cloudId) {
        this.cloudId = cloudId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("cloudId")
    public String getCloudId() {
        return this.cloudId;
    }

    public OpenInterfaceRequest withCloudId(String aCloudId) {
        this.setCloudId(aCloudId);
        return this;
    }

    private String connectionName;

    @com.fasterxml.jackson.annotation.JsonProperty("connectionName")
    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("connectionName")
    public String getConnectionName() {
        return this.connectionName;
    }

    public OpenInterfaceRequest withConnectionName(String aConnectionName) {
        this.setConnectionName(aConnectionName);
        return this;
    }

    private String realm;

    @com.fasterxml.jackson.annotation.JsonProperty("realm")
    public void setRealm(String realm) {
        this.realm = realm;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("realm")
    public String getRealm() {
        return this.realm;
    }

    public OpenInterfaceRequest withRealm(String aRealm) {
        this.setRealm(aRealm);
        return this;
    }

    private String token;

    @com.fasterxml.jackson.annotation.JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("token")
    public String getToken() {
        return this.token;
    }

    public OpenInterfaceRequest withToken(String aToken) {
        this.setToken(aToken);
        return this;
    }

    private String api;

    @com.fasterxml.jackson.annotation.JsonProperty("api")
    public void setApi(String api) {
        this.api = api;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("api")
    public String getApi() {
        return this.api;
    }

    public OpenInterfaceRequest withApi(String aApi) {
        this.setApi(aApi);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OpenInterfaceRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public OpenInterfaceRequest(
        InterfaceType interfaceType,
        String portName,
        int baudRate,
        String hostName,
        int port,
        int transport,
        boolean rejectRoutedConnection,
        String cloudId,
        String token,
        String api,
        String connectionName,
        String realm
    ) {
        this.interfaceType = interfaceType;
        this.portName = portName;
        this.baudRate = baudRate;
        this.hostName = hostName;
        this.port = port;
        this.transport = transport;
        this.rejectRoutedConnection = rejectRoutedConnection;
        this.cloudId = cloudId;
        this.token = token;
        this.api = api;
        this.connectionName = connectionName;
        this.realm = realm;
    }

    /**
     * Constructor with only required properties.
     */
    public OpenInterfaceRequest(
        InterfaceType interfaceType,
        String portName,
        int baudRate,
        String hostName,
        int port,
        int transport,
        boolean rejectRoutedConnection,
        String cloudId,
        String token,
        String api
    ) {
        this.interfaceType = interfaceType;
        this.portName = portName;
        this.baudRate = baudRate;
        this.hostName = hostName;
        this.port = port;
        this.transport = transport;
        this.rejectRoutedConnection = rejectRoutedConnection;
        this.cloudId = cloudId;
        this.token = token;
        this.api = api;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OpenInterfaceRequest other = (OpenInterfaceRequest) obj;

        return (
            EqualityUtility.equals(interfaceType, other.interfaceType)
            && EqualityUtility.equals(portName, other.portName)
            && EqualityUtility.equals(baudRate, other.baudRate)
            && EqualityUtility.equals(hostName, other.hostName)
            && EqualityUtility.equals(port, other.port)
            && EqualityUtility.equals(transport, other.transport)
            && EqualityUtility.equals(rejectRoutedConnection, other.rejectRoutedConnection)
            && EqualityUtility.equals(cloudId, other.cloudId)
            && EqualityUtility.equals(connectionName, other.connectionName)
            && EqualityUtility.equals(realm, other.realm)
            && EqualityUtility.equals(token, other.token)
            && EqualityUtility.equals(api, other.api)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceType),
            EqualityUtility.generateHashCode(portName),
            EqualityUtility.generateHashCode(baudRate),
            EqualityUtility.generateHashCode(hostName),
            EqualityUtility.generateHashCode(port),
            EqualityUtility.generateHashCode(transport),
            EqualityUtility.generateHashCode(rejectRoutedConnection),
            EqualityUtility.generateHashCode(cloudId),
            EqualityUtility.generateHashCode(connectionName),
            EqualityUtility.generateHashCode(realm),
            EqualityUtility.generateHashCode(token),
            EqualityUtility.generateHashCode(api)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OpenInterfaceRequest { ");
        sb.append("interfaceType: ");
        sb.append(this.interfaceType);
        sb.append(", ");
        sb.append("portName: ");
        sb.append(this.portName);
        sb.append(", ");
        sb.append("baudRate: ");
        sb.append(this.baudRate);
        sb.append(", ");
        sb.append("hostName: ");
        sb.append(this.hostName);
        sb.append(", ");
        sb.append("port: ");
        sb.append(this.port);
        sb.append(", ");
        sb.append("transport: ");
        sb.append(this.transport);
        sb.append(", ");
        sb.append("rejectRoutedConnection: ");
        sb.append(this.rejectRoutedConnection);
        sb.append(", ");
        sb.append("cloudId: ");
        sb.append(this.cloudId);
        sb.append(", ");
        sb.append("connectionName: ");
        sb.append(this.connectionName);
        sb.append(", ");
        sb.append("realm: ");
        sb.append(this.realm);
        sb.append(", ");
        sb.append("token: ");
        sb.append(this.token);
        sb.append(", ");
        sb.append("api: ");
        sb.append(this.api);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OpenInterfaceRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OpenInterfaceRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OpenInterfaceRequest> PARSER =
        new zaber.motion.dto.Parser<OpenInterfaceRequest>() {
            @Override
            public OpenInterfaceRequest fromByteArray(byte[] data) {
                return OpenInterfaceRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OpenInterfaceRequest> parser() {
        return PARSER;
    }

}
