/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class StreamSetMaxTangentialAccelerationRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetMaxTangentialAccelerationRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetMaxTangentialAccelerationRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetMaxTangentialAccelerationRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetMaxTangentialAccelerationRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private double maxTangentialAcceleration;

    @com.fasterxml.jackson.annotation.JsonProperty("maxTangentialAcceleration")
    public void setMaxTangentialAcceleration(double maxTangentialAcceleration) {
        this.maxTangentialAcceleration = maxTangentialAcceleration;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("maxTangentialAcceleration")
    public double getMaxTangentialAcceleration() {
        return this.maxTangentialAcceleration;
    }

    public StreamSetMaxTangentialAccelerationRequest withMaxTangentialAcceleration(double aMaxTangentialAcceleration) {
        this.setMaxTangentialAcceleration(aMaxTangentialAcceleration);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public StreamSetMaxTangentialAccelerationRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetMaxTangentialAccelerationRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetMaxTangentialAccelerationRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        double maxTangentialAcceleration,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.maxTangentialAcceleration = maxTangentialAcceleration;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetMaxTangentialAccelerationRequest other = (StreamSetMaxTangentialAccelerationRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(maxTangentialAcceleration, other.maxTangentialAcceleration)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(maxTangentialAcceleration),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetMaxTangentialAccelerationRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("maxTangentialAcceleration: ");
        sb.append(this.maxTangentialAcceleration);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetMaxTangentialAccelerationRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetMaxTangentialAccelerationRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetMaxTangentialAccelerationRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetMaxTangentialAccelerationRequest>() {
            @Override
            public StreamSetMaxTangentialAccelerationRequest fromByteArray(byte[] data) {
                return StreamSetMaxTangentialAccelerationRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetMaxTangentialAccelerationRequest> parser() {
        return PARSER;
    }

}
