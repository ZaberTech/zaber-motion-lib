/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamSetupStoreRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamSetupStoreRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamSetupStoreRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamSetupStoreRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamSetupStoreRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private int streamBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public void setStreamBuffer(int streamBuffer) {
        this.streamBuffer = streamBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffer")
    public int getStreamBuffer() {
        return this.streamBuffer;
    }

    public StreamSetupStoreRequest withStreamBuffer(int aStreamBuffer) {
        this.setStreamBuffer(aStreamBuffer);
        return this;
    }

    private int pvtBuffer;

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public void setPvtBuffer(int pvtBuffer) {
        this.pvtBuffer = pvtBuffer;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffer")
    public int getPvtBuffer() {
        return this.pvtBuffer;
    }

    public StreamSetupStoreRequest withPvtBuffer(int aPvtBuffer) {
        this.setPvtBuffer(aPvtBuffer);
        return this;
    }

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public StreamSetupStoreRequest withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamSetupStoreRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamSetupStoreRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        int streamBuffer,
        int pvtBuffer,
        int[] axes
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.streamBuffer = streamBuffer;
        this.pvtBuffer = pvtBuffer;
        this.axes = axes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamSetupStoreRequest other = (StreamSetupStoreRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(streamBuffer, other.streamBuffer)
            && EqualityUtility.equals(pvtBuffer, other.pvtBuffer)
            && EqualityUtility.equals(axes, other.axes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(streamBuffer),
            EqualityUtility.generateHashCode(pvtBuffer),
            EqualityUtility.generateHashCode(axes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamSetupStoreRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("streamBuffer: ");
        sb.append(this.streamBuffer);
        sb.append(", ");
        sb.append("pvtBuffer: ");
        sb.append(this.pvtBuffer);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamSetupStoreRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamSetupStoreRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamSetupStoreRequest> PARSER =
        new zaber.motion.dto.Parser<StreamSetupStoreRequest>() {
            @Override
            public StreamSetupStoreRequest fromByteArray(byte[] data) {
                return StreamSetupStoreRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamSetupStoreRequest> parser() {
        return PARSER;
    }

}
