/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class DeviceSetLowpassFilterRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetLowpassFilterRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetLowpassFilterRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceSetLowpassFilterRequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private double cutoffFrequency;

    @com.fasterxml.jackson.annotation.JsonProperty("cutoffFrequency")
    public void setCutoffFrequency(double cutoffFrequency) {
        this.cutoffFrequency = cutoffFrequency;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("cutoffFrequency")
    public double getCutoffFrequency() {
        return this.cutoffFrequency;
    }

    public DeviceSetLowpassFilterRequest withCutoffFrequency(double aCutoffFrequency) {
        this.setCutoffFrequency(aCutoffFrequency);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public DeviceSetLowpassFilterRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetLowpassFilterRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetLowpassFilterRequest(
        int interfaceId,
        int device,
        int channelNumber,
        double cutoffFrequency,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelNumber = channelNumber;
        this.cutoffFrequency = cutoffFrequency;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetLowpassFilterRequest other = (DeviceSetLowpassFilterRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(cutoffFrequency, other.cutoffFrequency)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(cutoffFrequency),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetLowpassFilterRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("cutoffFrequency: ");
        sb.append(this.cutoffFrequency);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetLowpassFilterRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetLowpassFilterRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetLowpassFilterRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetLowpassFilterRequest>() {
            @Override
            public DeviceSetLowpassFilterRequest fromByteArray(byte[] data) {
                return DeviceSetLowpassFilterRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetLowpassFilterRequest> parser() {
        return PARSER;
    }

}
