/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceSetStorageRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceSetStorageRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceSetStorageRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public DeviceSetStorageRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String key;

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public String getKey() {
        return this.key;
    }

    public DeviceSetStorageRequest withKey(String aKey) {
        this.setKey(aKey);
        return this;
    }

    private String value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public String getValue() {
        return this.value;
    }

    public DeviceSetStorageRequest withValue(String aValue) {
        this.setValue(aValue);
        return this;
    }

    private boolean encode;

    @com.fasterxml.jackson.annotation.JsonProperty("encode")
    public void setEncode(boolean encode) {
        this.encode = encode;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("encode")
    public boolean getEncode() {
        return this.encode;
    }

    public DeviceSetStorageRequest withEncode(boolean aEncode) {
        this.setEncode(aEncode);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceSetStorageRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceSetStorageRequest(
        int interfaceId,
        int device,
        int axis,
        String key,
        String value,
        boolean encode
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.key = key;
        this.value = value;
        this.encode = encode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceSetStorageRequest other = (DeviceSetStorageRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(key, other.key)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(encode, other.encode)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(key),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(encode)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSetStorageRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("key: ");
        sb.append(this.key);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("encode: ");
        sb.append(this.encode);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceSetStorageRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceSetStorageRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceSetStorageRequest> PARSER =
        new zaber.motion.dto.Parser<DeviceSetStorageRequest>() {
            @Override
            public DeviceSetStorageRequest fromByteArray(byte[] data) {
                return DeviceSetStorageRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceSetStorageRequest> parser() {
        return PARSER;
    }

}
