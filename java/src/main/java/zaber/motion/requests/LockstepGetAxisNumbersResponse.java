/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class LockstepGetAxisNumbersResponse implements zaber.motion.dto.Message {

    private int[] axes;

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    public LockstepGetAxisNumbersResponse withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepGetAxisNumbersResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepGetAxisNumbersResponse(
        int[] axes
    ) {
        this.axes = axes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepGetAxisNumbersResponse other = (LockstepGetAxisNumbersResponse) obj;

        return (
            EqualityUtility.equals(axes, other.axes)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axes)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepGetAxisNumbersResponse { ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepGetAxisNumbersResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepGetAxisNumbersResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepGetAxisNumbersResponse> PARSER =
        new zaber.motion.dto.Parser<LockstepGetAxisNumbersResponse>() {
            @Override
            public LockstepGetAxisNumbersResponse fromByteArray(byte[] data) {
                return LockstepGetAxisNumbersResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepGetAxisNumbersResponse> parser() {
        return PARSER;
    }

}
