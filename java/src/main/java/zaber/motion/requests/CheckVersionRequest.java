/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CheckVersionRequest implements zaber.motion.dto.Message {

    private String version;

    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public String getVersion() {
        return this.version;
    }

    public CheckVersionRequest withVersion(String aVersion) {
        this.setVersion(aVersion);
        return this;
    }

    private String host;

    @com.fasterxml.jackson.annotation.JsonProperty("host")
    public void setHost(String host) {
        this.host = host;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("host")
    public String getHost() {
        return this.host;
    }

    public CheckVersionRequest withHost(String aHost) {
        this.setHost(aHost);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CheckVersionRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public CheckVersionRequest(
        String version,
        String host
    ) {
        this.version = version;
        this.host = host;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CheckVersionRequest other = (CheckVersionRequest) obj;

        return (
            EqualityUtility.equals(version, other.version)
            && EqualityUtility.equals(host, other.host)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(version),
            EqualityUtility.generateHashCode(host)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CheckVersionRequest { ");
        sb.append("version: ");
        sb.append(this.version);
        sb.append(", ");
        sb.append("host: ");
        sb.append(this.host);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CheckVersionRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CheckVersionRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CheckVersionRequest> PARSER =
        new zaber.motion.dto.Parser<CheckVersionRequest>() {
            @Override
            public CheckVersionRequest fromByteArray(byte[] data) {
                return CheckVersionRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CheckVersionRequest> parser() {
        return PARSER;
    }

}
