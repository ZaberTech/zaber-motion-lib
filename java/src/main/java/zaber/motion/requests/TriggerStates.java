/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.TriggerState;
import zaber.motion.EqualityUtility;


public final class TriggerStates implements zaber.motion.dto.Message {

    private TriggerState[] states;

    @com.fasterxml.jackson.annotation.JsonProperty("states")
    public void setStates(TriggerState[] states) {
        this.states = states;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("states")
    public TriggerState[] getStates() {
        return this.states;
    }

    public TriggerStates withStates(TriggerState[] aStates) {
        this.setStates(aStates);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerStates() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerStates(
        TriggerState[] states
    ) {
        this.states = states;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerStates other = (TriggerStates) obj;

        return (
            EqualityUtility.equals(states, other.states)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(states)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerStates { ");
        sb.append("states: ");
        sb.append(this.states);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerStates fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerStates.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerStates> PARSER =
        new zaber.motion.dto.Parser<TriggerStates>() {
            @Override
            public TriggerStates fromByteArray(byte[] data) {
                return TriggerStates.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerStates> parser() {
        return PARSER;
    }

}
