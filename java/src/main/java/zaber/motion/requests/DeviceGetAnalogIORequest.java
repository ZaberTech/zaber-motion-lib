/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetAnalogIORequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceGetAnalogIORequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceGetAnalogIORequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private String channelType;

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public String getChannelType() {
        return this.channelType;
    }

    public DeviceGetAnalogIORequest withChannelType(String aChannelType) {
        this.setChannelType(aChannelType);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceGetAnalogIORequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetAnalogIORequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetAnalogIORequest(
        int interfaceId,
        int device,
        String channelType,
        int channelNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelType = channelType;
        this.channelNumber = channelNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetAnalogIORequest other = (DeviceGetAnalogIORequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelType, other.channelType)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelType),
            EqualityUtility.generateHashCode(channelNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetAnalogIORequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelType: ");
        sb.append(this.channelType);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetAnalogIORequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetAnalogIORequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetAnalogIORequest> PARSER =
        new zaber.motion.dto.Parser<DeviceGetAnalogIORequest>() {
            @Override
            public DeviceGetAnalogIORequest fromByteArray(byte[] data) {
                return DeviceGetAnalogIORequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetAnalogIORequest> parser() {
        return PARSER;
    }

}
