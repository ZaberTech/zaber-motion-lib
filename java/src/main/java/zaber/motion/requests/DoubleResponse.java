/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DoubleResponse implements zaber.motion.dto.Message {

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public DoubleResponse withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DoubleResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public DoubleResponse(
        double value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DoubleResponse other = (DoubleResponse) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DoubleResponse { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DoubleResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DoubleResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DoubleResponse> PARSER =
        new zaber.motion.dto.Parser<DoubleResponse>() {
            @Override
            public DoubleResponse fromByteArray(byte[] data) {
                return DoubleResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DoubleResponse> parser() {
        return PARSER;
    }

}
