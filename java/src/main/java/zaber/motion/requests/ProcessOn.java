/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class ProcessOn implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public ProcessOn withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public ProcessOn withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public ProcessOn withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private boolean on;

    @com.fasterxml.jackson.annotation.JsonProperty("on")
    public void setOn(boolean on) {
        this.on = on;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("on")
    public boolean getOn() {
        return this.on;
    }

    public ProcessOn withOn(boolean aOn) {
        this.setOn(aOn);
        return this;
    }

    private double duration;

    @com.fasterxml.jackson.annotation.JsonProperty("duration")
    public void setDuration(double duration) {
        this.duration = duration;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("duration")
    public double getDuration() {
        return this.duration;
    }

    public ProcessOn withDuration(double aDuration) {
        this.setDuration(aDuration);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public ProcessOn withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ProcessOn() {
    }

    /**
     * Constructor with all properties.
     */
    public ProcessOn(
        int interfaceId,
        int device,
        int axis,
        boolean on,
        double duration,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.on = on;
        this.duration = duration;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ProcessOn other = (ProcessOn) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(on, other.on)
            && EqualityUtility.equals(duration, other.duration)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(on),
            EqualityUtility.generateHashCode(duration),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProcessOn { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("on: ");
        sb.append(this.on);
        sb.append(", ");
        sb.append("duration: ");
        sb.append(this.duration);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ProcessOn fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ProcessOn.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ProcessOn> PARSER =
        new zaber.motion.dto.Parser<ProcessOn>() {
            @Override
            public ProcessOn fromByteArray(byte[] data) {
                return ProcessOn.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ProcessOn> parser() {
        return PARSER;
    }

}
