/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class BinaryDeviceDetectRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryDeviceDetectRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private boolean identifyDevices;

    @com.fasterxml.jackson.annotation.JsonProperty("identifyDevices")
    public void setIdentifyDevices(boolean identifyDevices) {
        this.identifyDevices = identifyDevices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("identifyDevices")
    public boolean getIdentifyDevices() {
        return this.identifyDevices;
    }

    public BinaryDeviceDetectRequest withIdentifyDevices(boolean aIdentifyDevices) {
        this.setIdentifyDevices(aIdentifyDevices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryDeviceDetectRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryDeviceDetectRequest(
        int interfaceId,
        boolean identifyDevices
    ) {
        this.interfaceId = interfaceId;
        this.identifyDevices = identifyDevices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryDeviceDetectRequest other = (BinaryDeviceDetectRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(identifyDevices, other.identifyDevices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(identifyDevices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryDeviceDetectRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("identifyDevices: ");
        sb.append(this.identifyDevices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryDeviceDetectRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryDeviceDetectRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryDeviceDetectRequest> PARSER =
        new zaber.motion.dto.Parser<BinaryDeviceDetectRequest>() {
            @Override
            public BinaryDeviceDetectRequest fromByteArray(byte[] data) {
                return BinaryDeviceDetectRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryDeviceDetectRequest> parser() {
        return PARSER;
    }

}
