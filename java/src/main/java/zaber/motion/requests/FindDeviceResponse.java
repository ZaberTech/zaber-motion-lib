/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class FindDeviceResponse implements zaber.motion.dto.Message {

    private int address;

    @com.fasterxml.jackson.annotation.JsonProperty("address")
    public void setAddress(int address) {
        this.address = address;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("address")
    public int getAddress() {
        return this.address;
    }

    public FindDeviceResponse withAddress(int aAddress) {
        this.setAddress(aAddress);
        return this;
    }

    /**
     * Empty constructor.
     */
    public FindDeviceResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public FindDeviceResponse(
        int address
    ) {
        this.address = address;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        FindDeviceResponse other = (FindDeviceResponse) obj;

        return (
            EqualityUtility.equals(address, other.address)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(address)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FindDeviceResponse { ");
        sb.append("address: ");
        sb.append(this.address);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static FindDeviceResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, FindDeviceResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<FindDeviceResponse> PARSER =
        new zaber.motion.dto.Parser<FindDeviceResponse>() {
            @Override
            public FindDeviceResponse fromByteArray(byte[] data) {
                return FindDeviceResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<FindDeviceResponse> parser() {
        return PARSER;
    }

}
