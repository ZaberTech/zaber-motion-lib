/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamWaitUntilIdleRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamWaitUntilIdleRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamWaitUntilIdleRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int streamId;

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("streamId")
    public int getStreamId() {
        return this.streamId;
    }

    public StreamWaitUntilIdleRequest withStreamId(int aStreamId) {
        this.setStreamId(aStreamId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamWaitUntilIdleRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    private boolean throwErrorOnFault;

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public void setThrowErrorOnFault(boolean throwErrorOnFault) {
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("throwErrorOnFault")
    public boolean getThrowErrorOnFault() {
        return this.throwErrorOnFault;
    }

    public StreamWaitUntilIdleRequest withThrowErrorOnFault(boolean aThrowErrorOnFault) {
        this.setThrowErrorOnFault(aThrowErrorOnFault);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamWaitUntilIdleRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamWaitUntilIdleRequest(
        int interfaceId,
        int device,
        int streamId,
        boolean pvt,
        boolean throwErrorOnFault
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.streamId = streamId;
        this.pvt = pvt;
        this.throwErrorOnFault = throwErrorOnFault;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamWaitUntilIdleRequest other = (StreamWaitUntilIdleRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(streamId, other.streamId)
            && EqualityUtility.equals(pvt, other.pvt)
            && EqualityUtility.equals(throwErrorOnFault, other.throwErrorOnFault)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(streamId),
            EqualityUtility.generateHashCode(pvt),
            EqualityUtility.generateHashCode(throwErrorOnFault)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamWaitUntilIdleRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("streamId: ");
        sb.append(this.streamId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(", ");
        sb.append("throwErrorOnFault: ");
        sb.append(this.throwErrorOnFault);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamWaitUntilIdleRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamWaitUntilIdleRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamWaitUntilIdleRequest> PARSER =
        new zaber.motion.dto.Parser<StreamWaitUntilIdleRequest>() {
            @Override
            public StreamWaitUntilIdleRequest fromByteArray(byte[] data) {
                return StreamWaitUntilIdleRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamWaitUntilIdleRequest> parser() {
        return PARSER;
    }

}
