/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class RenumberRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public RenumberRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public RenumberRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int address;

    @com.fasterxml.jackson.annotation.JsonProperty("address")
    public void setAddress(int address) {
        this.address = address;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("address")
    public int getAddress() {
        return this.address;
    }

    public RenumberRequest withAddress(int aAddress) {
        this.setAddress(aAddress);
        return this;
    }

    /**
     * Empty constructor.
     */
    public RenumberRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public RenumberRequest(
        int interfaceId,
        int device,
        int address
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.address = address;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        RenumberRequest other = (RenumberRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(address, other.address)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(address)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RenumberRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("address: ");
        sb.append(this.address);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static RenumberRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, RenumberRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<RenumberRequest> PARSER =
        new zaber.motion.dto.Parser<RenumberRequest>() {
            @Override
            public RenumberRequest fromByteArray(byte[] data) {
                return RenumberRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<RenumberRequest> parser() {
        return PARSER;
    }

}
