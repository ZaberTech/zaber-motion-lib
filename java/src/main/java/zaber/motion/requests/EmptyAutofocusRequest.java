/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class EmptyAutofocusRequest implements zaber.motion.dto.Message {

    private int providerId;

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("providerId")
    public int getProviderId() {
        return this.providerId;
    }

    public EmptyAutofocusRequest withProviderId(int aProviderId) {
        this.setProviderId(aProviderId);
        return this;
    }

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public EmptyAutofocusRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int focusAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public void setFocusAddress(int focusAddress) {
        this.focusAddress = focusAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAddress")
    public int getFocusAddress() {
        return this.focusAddress;
    }

    public EmptyAutofocusRequest withFocusAddress(int aFocusAddress) {
        this.setFocusAddress(aFocusAddress);
        return this;
    }

    private int focusAxis;

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(int focusAxis) {
        this.focusAxis = focusAxis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public int getFocusAxis() {
        return this.focusAxis;
    }

    public EmptyAutofocusRequest withFocusAxis(int aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private int turretAddress;

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public void setTurretAddress(int turretAddress) {
        this.turretAddress = turretAddress;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("turretAddress")
    public int getTurretAddress() {
        return this.turretAddress;
    }

    public EmptyAutofocusRequest withTurretAddress(int aTurretAddress) {
        this.setTurretAddress(aTurretAddress);
        return this;
    }

    /**
     * Empty constructor.
     */
    public EmptyAutofocusRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public EmptyAutofocusRequest(
        int providerId,
        int interfaceId,
        int focusAddress,
        int focusAxis,
        int turretAddress
    ) {
        this.providerId = providerId;
        this.interfaceId = interfaceId;
        this.focusAddress = focusAddress;
        this.focusAxis = focusAxis;
        this.turretAddress = turretAddress;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        EmptyAutofocusRequest other = (EmptyAutofocusRequest) obj;

        return (
            EqualityUtility.equals(providerId, other.providerId)
            && EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(focusAddress, other.focusAddress)
            && EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(turretAddress, other.turretAddress)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(providerId),
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(focusAddress),
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(turretAddress)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmptyAutofocusRequest { ");
        sb.append("providerId: ");
        sb.append(this.providerId);
        sb.append(", ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("focusAddress: ");
        sb.append(this.focusAddress);
        sb.append(", ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("turretAddress: ");
        sb.append(this.turretAddress);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static EmptyAutofocusRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, EmptyAutofocusRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<EmptyAutofocusRequest> PARSER =
        new zaber.motion.dto.Parser<EmptyAutofocusRequest>() {
            @Override
            public EmptyAutofocusRequest fromByteArray(byte[] data) {
                return EmptyAutofocusRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<EmptyAutofocusRequest> parser() {
        return PARSER;
    }

}
