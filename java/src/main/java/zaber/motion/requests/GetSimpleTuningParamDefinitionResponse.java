/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.SimpleTuningParamDefinition;
import zaber.motion.EqualityUtility;


public final class GetSimpleTuningParamDefinitionResponse implements zaber.motion.dto.Message {

    private SimpleTuningParamDefinition[] params;

    @com.fasterxml.jackson.annotation.JsonProperty("params")
    public void setParams(SimpleTuningParamDefinition[] params) {
        this.params = params;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("params")
    public SimpleTuningParamDefinition[] getParams() {
        return this.params;
    }

    public GetSimpleTuningParamDefinitionResponse withParams(SimpleTuningParamDefinition[] aParams) {
        this.setParams(aParams);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetSimpleTuningParamDefinitionResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public GetSimpleTuningParamDefinitionResponse(
        SimpleTuningParamDefinition[] params
    ) {
        this.params = params;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetSimpleTuningParamDefinitionResponse other = (GetSimpleTuningParamDefinitionResponse) obj;

        return (
            EqualityUtility.equals(params, other.params)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(params)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetSimpleTuningParamDefinitionResponse { ");
        sb.append("params: ");
        sb.append(this.params);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetSimpleTuningParamDefinitionResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetSimpleTuningParamDefinitionResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetSimpleTuningParamDefinitionResponse> PARSER =
        new zaber.motion.dto.Parser<GetSimpleTuningParamDefinitionResponse>() {
            @Override
            public GetSimpleTuningParamDefinitionResponse fromByteArray(byte[] data) {
                return GetSimpleTuningParamDefinitionResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetSimpleTuningParamDefinitionResponse> parser() {
        return PARSER;
    }

}
