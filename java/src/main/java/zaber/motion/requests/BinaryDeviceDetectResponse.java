/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class BinaryDeviceDetectResponse implements zaber.motion.dto.Message {

    private int[] devices;

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public void setDevices(int[] devices) {
        this.devices = devices;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("devices")
    public int[] getDevices() {
        return this.devices;
    }

    public BinaryDeviceDetectResponse withDevices(int[] aDevices) {
        this.setDevices(aDevices);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryDeviceDetectResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryDeviceDetectResponse(
        int[] devices
    ) {
        this.devices = devices;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryDeviceDetectResponse other = (BinaryDeviceDetectResponse) obj;

        return (
            EqualityUtility.equals(devices, other.devices)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(devices)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryDeviceDetectResponse { ");
        sb.append("devices: ");
        sb.append(this.devices);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryDeviceDetectResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryDeviceDetectResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryDeviceDetectResponse> PARSER =
        new zaber.motion.dto.Parser<BinaryDeviceDetectResponse>() {
            @Override
            public BinaryDeviceDetectResponse fromByteArray(byte[] data) {
                return BinaryDeviceDetectResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryDeviceDetectResponse> parser() {
        return PARSER;
    }

}
