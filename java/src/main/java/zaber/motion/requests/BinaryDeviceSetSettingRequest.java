/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.BinarySettings;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class BinaryDeviceSetSettingRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryDeviceSetSettingRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public BinaryDeviceSetSettingRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private BinarySettings setting;

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(BinarySettings setting) {
        this.setting = setting;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public BinarySettings getSetting() {
        return this.setting;
    }

    public BinaryDeviceSetSettingRequest withSetting(BinarySettings aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private double value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    public BinaryDeviceSetSettingRequest withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    public BinaryDeviceSetSettingRequest withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryDeviceSetSettingRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryDeviceSetSettingRequest(
        int interfaceId,
        int device,
        BinarySettings setting,
        double value,
        Units unit
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.setting = setting;
        this.value = value;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryDeviceSetSettingRequest other = (BinaryDeviceSetSettingRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryDeviceSetSettingRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryDeviceSetSettingRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryDeviceSetSettingRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryDeviceSetSettingRequest> PARSER =
        new zaber.motion.dto.Parser<BinaryDeviceSetSettingRequest>() {
            @Override
            public BinaryDeviceSetSettingRequest fromByteArray(byte[] data) {
                return BinaryDeviceSetSettingRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryDeviceSetSettingRequest> parser() {
        return PARSER;
    }

}
