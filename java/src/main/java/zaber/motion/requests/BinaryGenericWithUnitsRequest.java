/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.CommandCode;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


public final class BinaryGenericWithUnitsRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public BinaryGenericWithUnitsRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public BinaryGenericWithUnitsRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private CommandCode command;

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(CommandCode command) {
        this.command = command;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public CommandCode getCommand() {
        return this.command;
    }

    public BinaryGenericWithUnitsRequest withCommand(CommandCode aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    private double data;

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(double data) {
        this.data = data;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public double getData() {
        return this.data;
    }

    public BinaryGenericWithUnitsRequest withData(double aData) {
        this.setData(aData);
        return this;
    }

    private Units fromUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("fromUnit")
    public void setFromUnit(Units fromUnit) {
        this.fromUnit = fromUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("fromUnit")
    public Units getFromUnit() {
        return this.fromUnit;
    }

    public BinaryGenericWithUnitsRequest withFromUnit(Units aFromUnit) {
        this.setFromUnit(aFromUnit);
        return this;
    }

    private Units toUnit;

    @com.fasterxml.jackson.annotation.JsonProperty("toUnit")
    public void setToUnit(Units toUnit) {
        this.toUnit = toUnit;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("toUnit")
    public Units getToUnit() {
        return this.toUnit;
    }

    public BinaryGenericWithUnitsRequest withToUnit(Units aToUnit) {
        this.setToUnit(aToUnit);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public BinaryGenericWithUnitsRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryGenericWithUnitsRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryGenericWithUnitsRequest(
        int interfaceId,
        int device,
        CommandCode command,
        double data,
        Units fromUnit,
        Units toUnit,
        double timeout
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.command = command;
        this.data = data;
        this.fromUnit = fromUnit;
        this.toUnit = toUnit;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryGenericWithUnitsRequest other = (BinaryGenericWithUnitsRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(command, other.command)
            && EqualityUtility.equals(data, other.data)
            && EqualityUtility.equals(fromUnit, other.fromUnit)
            && EqualityUtility.equals(toUnit, other.toUnit)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(command),
            EqualityUtility.generateHashCode(data),
            EqualityUtility.generateHashCode(fromUnit),
            EqualityUtility.generateHashCode(toUnit),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryGenericWithUnitsRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(", ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(", ");
        sb.append("fromUnit: ");
        sb.append(this.fromUnit);
        sb.append(", ");
        sb.append("toUnit: ");
        sb.append(this.toUnit);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryGenericWithUnitsRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryGenericWithUnitsRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryGenericWithUnitsRequest> PARSER =
        new zaber.motion.dto.Parser<BinaryGenericWithUnitsRequest>() {
            @Override
            public BinaryGenericWithUnitsRequest fromByteArray(byte[] data) {
                return BinaryGenericWithUnitsRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryGenericWithUnitsRequest> parser() {
        return PARSER;
    }

}
