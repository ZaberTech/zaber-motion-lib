/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class TranslatorFlushResponse implements zaber.motion.dto.Message {

    private String[] commands;

    @com.fasterxml.jackson.annotation.JsonProperty("commands")
    public void setCommands(String[] commands) {
        this.commands = commands;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("commands")
    public String[] getCommands() {
        return this.commands;
    }

    public TranslatorFlushResponse withCommands(String[] aCommands) {
        this.setCommands(aCommands);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TranslatorFlushResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public TranslatorFlushResponse(
        String[] commands
    ) {
        this.commands = commands;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TranslatorFlushResponse other = (TranslatorFlushResponse) obj;

        return (
            EqualityUtility.equals(commands, other.commands)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(commands)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TranslatorFlushResponse { ");
        sb.append("commands: ");
        sb.append(this.commands);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TranslatorFlushResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TranslatorFlushResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TranslatorFlushResponse> PARSER =
        new zaber.motion.dto.Parser<TranslatorFlushResponse>() {
            @Override
            public TranslatorFlushResponse fromByteArray(byte[] data) {
                return TranslatorFlushResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TranslatorFlushResponse> parser() {
        return PARSER;
    }

}
