/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class CustomInterfaceCloseRequest implements zaber.motion.dto.Message {

    private int transportId;

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public void setTransportId(int transportId) {
        this.transportId = transportId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("transportId")
    public int getTransportId() {
        return this.transportId;
    }

    public CustomInterfaceCloseRequest withTransportId(int aTransportId) {
        this.setTransportId(aTransportId);
        return this;
    }

    private String errorMessage;

    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("errorMessage")
    public String getErrorMessage() {
        return this.errorMessage;
    }

    public CustomInterfaceCloseRequest withErrorMessage(String aErrorMessage) {
        this.setErrorMessage(aErrorMessage);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CustomInterfaceCloseRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public CustomInterfaceCloseRequest(
        int transportId,
        String errorMessage
    ) {
        this.transportId = transportId;
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CustomInterfaceCloseRequest other = (CustomInterfaceCloseRequest) obj;

        return (
            EqualityUtility.equals(transportId, other.transportId)
            && EqualityUtility.equals(errorMessage, other.errorMessage)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(transportId),
            EqualityUtility.generateHashCode(errorMessage)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CustomInterfaceCloseRequest { ");
        sb.append("transportId: ");
        sb.append(this.transportId);
        sb.append(", ");
        sb.append("errorMessage: ");
        sb.append(this.errorMessage);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CustomInterfaceCloseRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CustomInterfaceCloseRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CustomInterfaceCloseRequest> PARSER =
        new zaber.motion.dto.Parser<CustomInterfaceCloseRequest>() {
            @Override
            public CustomInterfaceCloseRequest fromByteArray(byte[] data) {
                return CustomInterfaceCloseRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CustomInterfaceCloseRequest> parser() {
        return PARSER;
    }

}
