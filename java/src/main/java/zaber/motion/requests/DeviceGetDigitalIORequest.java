/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class DeviceGetDigitalIORequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public DeviceGetDigitalIORequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public DeviceGetDigitalIORequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private String channelType;

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelType")
    public String getChannelType() {
        return this.channelType;
    }

    public DeviceGetDigitalIORequest withChannelType(String aChannelType) {
        this.setChannelType(aChannelType);
        return this;
    }

    private int channelNumber;

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    public DeviceGetDigitalIORequest withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceGetDigitalIORequest() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceGetDigitalIORequest(
        int interfaceId,
        int device,
        String channelType,
        int channelNumber
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.channelType = channelType;
        this.channelNumber = channelNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceGetDigitalIORequest other = (DeviceGetDigitalIORequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channelType, other.channelType)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channelType),
            EqualityUtility.generateHashCode(channelNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceGetDigitalIORequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channelType: ");
        sb.append(this.channelType);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceGetDigitalIORequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceGetDigitalIORequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceGetDigitalIORequest> PARSER =
        new zaber.motion.dto.Parser<DeviceGetDigitalIORequest>() {
            @Override
            public DeviceGetDigitalIORequest fromByteArray(byte[] data) {
                return DeviceGetDigitalIORequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceGetDigitalIORequest> parser() {
        return PARSER;
    }

}
