/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class IntRequest implements zaber.motion.dto.Message {

    private int value;

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public int getValue() {
        return this.value;
    }

    public IntRequest withValue(int aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public IntRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public IntRequest(
        int value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        IntRequest other = (IntRequest) obj;

        return (
            EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IntRequest { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static IntRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, IntRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<IntRequest> PARSER =
        new zaber.motion.dto.Parser<IntRequest>() {
            @Override
            public IntRequest fromByteArray(byte[] data) {
                return IntRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<IntRequest> parser() {
        return PARSER;
    }

}
