/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Measurement;
import zaber.motion.EqualityUtility;


public final class PrepareCommandRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public PrepareCommandRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public PrepareCommandRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    public PrepareCommandRequest withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    private String commandTemplate;

    @com.fasterxml.jackson.annotation.JsonProperty("commandTemplate")
    public void setCommandTemplate(String commandTemplate) {
        this.commandTemplate = commandTemplate;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("commandTemplate")
    public String getCommandTemplate() {
        return this.commandTemplate;
    }

    public PrepareCommandRequest withCommandTemplate(String aCommandTemplate) {
        this.setCommandTemplate(aCommandTemplate);
        return this;
    }

    private Measurement[] parameters;

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public void setParameters(Measurement[] parameters) {
        this.parameters = parameters;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("parameters")
    public Measurement[] getParameters() {
        return this.parameters;
    }

    public PrepareCommandRequest withParameters(Measurement[] aParameters) {
        this.setParameters(aParameters);
        return this;
    }

    /**
     * Empty constructor.
     */
    public PrepareCommandRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public PrepareCommandRequest(
        int interfaceId,
        int device,
        int axis,
        String commandTemplate,
        Measurement[] parameters
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.axis = axis;
        this.commandTemplate = commandTemplate;
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PrepareCommandRequest other = (PrepareCommandRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            && EqualityUtility.equals(commandTemplate, other.commandTemplate)
            && EqualityUtility.equals(parameters, other.parameters)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis),
            EqualityUtility.generateHashCode(commandTemplate),
            EqualityUtility.generateHashCode(parameters)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PrepareCommandRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(", ");
        sb.append("commandTemplate: ");
        sb.append(this.commandTemplate);
        sb.append(", ");
        sb.append("parameters: ");
        sb.append(this.parameters);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static PrepareCommandRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, PrepareCommandRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<PrepareCommandRequest> PARSER =
        new zaber.motion.dto.Parser<PrepareCommandRequest>() {
            @Override
            public PrepareCommandRequest fromByteArray(byte[] data) {
                return PrepareCommandRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<PrepareCommandRequest> parser() {
        return PARSER;
    }

}
