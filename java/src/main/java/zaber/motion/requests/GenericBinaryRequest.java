/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.binary.CommandCode;
import zaber.motion.EqualityUtility;


public final class GenericBinaryRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public GenericBinaryRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public GenericBinaryRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private CommandCode command;

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(CommandCode command) {
        this.command = command;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public CommandCode getCommand() {
        return this.command;
    }

    public GenericBinaryRequest withCommand(CommandCode aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    private int data;

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(int data) {
        this.data = data;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public int getData() {
        return this.data;
    }

    public GenericBinaryRequest withData(int aData) {
        this.setData(aData);
        return this;
    }

    private boolean checkErrors;

    @com.fasterxml.jackson.annotation.JsonProperty("checkErrors")
    public void setCheckErrors(boolean checkErrors) {
        this.checkErrors = checkErrors;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("checkErrors")
    public boolean getCheckErrors() {
        return this.checkErrors;
    }

    public GenericBinaryRequest withCheckErrors(boolean aCheckErrors) {
        this.setCheckErrors(aCheckErrors);
        return this;
    }

    private double timeout;

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("timeout")
    public double getTimeout() {
        return this.timeout;
    }

    public GenericBinaryRequest withTimeout(double aTimeout) {
        this.setTimeout(aTimeout);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GenericBinaryRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public GenericBinaryRequest(
        int interfaceId,
        int device,
        CommandCode command,
        int data,
        boolean checkErrors,
        double timeout
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.command = command;
        this.data = data;
        this.checkErrors = checkErrors;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GenericBinaryRequest other = (GenericBinaryRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(command, other.command)
            && EqualityUtility.equals(data, other.data)
            && EqualityUtility.equals(checkErrors, other.checkErrors)
            && EqualityUtility.equals(timeout, other.timeout)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(command),
            EqualityUtility.generateHashCode(data),
            EqualityUtility.generateHashCode(checkErrors),
            EqualityUtility.generateHashCode(timeout)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GenericBinaryRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(", ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(", ");
        sb.append("checkErrors: ");
        sb.append(this.checkErrors);
        sb.append(", ");
        sb.append("timeout: ");
        sb.append(this.timeout);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GenericBinaryRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GenericBinaryRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GenericBinaryRequest> PARSER =
        new zaber.motion.dto.Parser<GenericBinaryRequest>() {
            @Override
            public GenericBinaryRequest fromByteArray(byte[] data) {
                return GenericBinaryRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GenericBinaryRequest> parser() {
        return PARSER;
    }

}
