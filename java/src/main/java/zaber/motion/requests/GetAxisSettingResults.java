/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.GetAxisSettingResult;
import zaber.motion.EqualityUtility;


public final class GetAxisSettingResults implements zaber.motion.dto.Message {

    private GetAxisSettingResult[] results;

    @com.fasterxml.jackson.annotation.JsonProperty("results")
    public void setResults(GetAxisSettingResult[] results) {
        this.results = results;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("results")
    public GetAxisSettingResult[] getResults() {
        return this.results;
    }

    public GetAxisSettingResults withResults(GetAxisSettingResult[] aResults) {
        this.setResults(aResults);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetAxisSettingResults() {
    }

    /**
     * Constructor with all properties.
     */
    public GetAxisSettingResults(
        GetAxisSettingResult[] results
    ) {
        this.results = results;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetAxisSettingResults other = (GetAxisSettingResults) obj;

        return (
            EqualityUtility.equals(results, other.results)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(results)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetAxisSettingResults { ");
        sb.append("results: ");
        sb.append(this.results);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetAxisSettingResults fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetAxisSettingResults.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetAxisSettingResults> PARSER =
        new zaber.motion.dto.Parser<GetAxisSettingResults>() {
            @Override
            public GetAxisSettingResults fromByteArray(byte[] data) {
                return GetAxisSettingResults.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetAxisSettingResults> parser() {
        return PARSER;
    }

}
