/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.IoPortLabel;
import zaber.motion.EqualityUtility;


public final class GetAllIoPortLabelsResponse implements zaber.motion.dto.Message {

    private IoPortLabel[] labels;

    @com.fasterxml.jackson.annotation.JsonProperty("labels")
    public void setLabels(IoPortLabel[] labels) {
        this.labels = labels;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("labels")
    public IoPortLabel[] getLabels() {
        return this.labels;
    }

    public GetAllIoPortLabelsResponse withLabels(IoPortLabel[] aLabels) {
        this.setLabels(aLabels);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetAllIoPortLabelsResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public GetAllIoPortLabelsResponse(
        IoPortLabel[] labels
    ) {
        this.labels = labels;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetAllIoPortLabelsResponse other = (GetAllIoPortLabelsResponse) obj;

        return (
            EqualityUtility.equals(labels, other.labels)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(labels)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetAllIoPortLabelsResponse { ");
        sb.append("labels: ");
        sb.append(this.labels);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetAllIoPortLabelsResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetAllIoPortLabelsResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetAllIoPortLabelsResponse> PARSER =
        new zaber.motion.dto.Parser<GetAllIoPortLabelsResponse>() {
            @Override
            public GetAllIoPortLabelsResponse fromByteArray(byte[] data) {
                return GetAllIoPortLabelsResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetAllIoPortLabelsResponse> parser() {
        return PARSER;
    }

}
