/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.ascii.UnknownResponseEvent;
import zaber.motion.EqualityUtility;


public final class UnknownResponseEventWrapper implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public UnknownResponseEventWrapper withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private UnknownResponseEvent unknownResponse;

    @com.fasterxml.jackson.annotation.JsonProperty("unknownResponse")
    public void setUnknownResponse(UnknownResponseEvent unknownResponse) {
        this.unknownResponse = unknownResponse;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("unknownResponse")
    public UnknownResponseEvent getUnknownResponse() {
        return this.unknownResponse;
    }

    public UnknownResponseEventWrapper withUnknownResponse(UnknownResponseEvent aUnknownResponse) {
        this.setUnknownResponse(aUnknownResponse);
        return this;
    }

    /**
     * Empty constructor.
     */
    public UnknownResponseEventWrapper() {
    }

    /**
     * Constructor with all properties.
     */
    public UnknownResponseEventWrapper(
        int interfaceId,
        UnknownResponseEvent unknownResponse
    ) {
        this.interfaceId = interfaceId;
        this.unknownResponse = unknownResponse;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        UnknownResponseEventWrapper other = (UnknownResponseEventWrapper) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(unknownResponse, other.unknownResponse)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(unknownResponse)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnknownResponseEventWrapper { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("unknownResponse: ");
        sb.append(this.unknownResponse);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static UnknownResponseEventWrapper fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, UnknownResponseEventWrapper.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<UnknownResponseEventWrapper> PARSER =
        new zaber.motion.dto.Parser<UnknownResponseEventWrapper>() {
            @Override
            public UnknownResponseEventWrapper fromByteArray(byte[] data) {
                return UnknownResponseEventWrapper.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<UnknownResponseEventWrapper> parser() {
        return PARSER;
    }

}
