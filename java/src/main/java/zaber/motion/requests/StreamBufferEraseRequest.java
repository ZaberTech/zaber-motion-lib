/* This file is generated. Do not modify by hand. */

package zaber.motion.requests;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


public final class StreamBufferEraseRequest implements zaber.motion.dto.Message {

    private int interfaceId;

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public void setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("interfaceId")
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public StreamBufferEraseRequest withInterfaceId(int aInterfaceId) {
        this.setInterfaceId(aInterfaceId);
        return this;
    }

    private int device;

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    public StreamBufferEraseRequest withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int bufferId;

    @com.fasterxml.jackson.annotation.JsonProperty("bufferId")
    public void setBufferId(int bufferId) {
        this.bufferId = bufferId;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("bufferId")
    public int getBufferId() {
        return this.bufferId;
    }

    public StreamBufferEraseRequest withBufferId(int aBufferId) {
        this.setBufferId(aBufferId);
        return this;
    }

    private boolean pvt;

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public void setPvt(boolean pvt) {
        this.pvt = pvt;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("pvt")
    public boolean getPvt() {
        return this.pvt;
    }

    public StreamBufferEraseRequest withPvt(boolean aPvt) {
        this.setPvt(aPvt);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamBufferEraseRequest() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamBufferEraseRequest(
        int interfaceId,
        int device,
        int bufferId,
        boolean pvt
    ) {
        this.interfaceId = interfaceId;
        this.device = device;
        this.bufferId = bufferId;
        this.pvt = pvt;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamBufferEraseRequest other = (StreamBufferEraseRequest) obj;

        return (
            EqualityUtility.equals(interfaceId, other.interfaceId)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(bufferId, other.bufferId)
            && EqualityUtility.equals(pvt, other.pvt)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(interfaceId),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(bufferId),
            EqualityUtility.generateHashCode(pvt)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamBufferEraseRequest { ");
        sb.append("interfaceId: ");
        sb.append(this.interfaceId);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("bufferId: ");
        sb.append(this.bufferId);
        sb.append(", ");
        sb.append("pvt: ");
        sb.append(this.pvt);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamBufferEraseRequest fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamBufferEraseRequest.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamBufferEraseRequest> PARSER =
        new zaber.motion.dto.Parser<StreamBufferEraseRequest>() {
            @Override
            public StreamBufferEraseRequest fromByteArray(byte[] data) {
                return StreamBufferEraseRequest.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamBufferEraseRequest> parser() {
        return PARSER;
    }

}
