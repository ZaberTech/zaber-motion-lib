/* This file is generated. Do not modify by hand. */

package zaber.motion;

import java.util.Objects;

import zaber.motion.dto.Mapper;

/**
 * Holds device address and axis number.
 */
public final class AxisAddress implements zaber.motion.dto.Message {

    private int device;

    /**
     * Device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    /**
     * Device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    /**
     * Device address.
     */
    public AxisAddress withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    /**
     * Axis number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    /**
     * Axis number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    /**
     * Axis number.
     */
    public AxisAddress withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisAddress() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisAddress(
        int device,
        int axis
    ) {
        this.device = device;
        this.axis = axis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisAddress other = (AxisAddress) obj;

        return (
            EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisAddress { ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisAddress fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisAddress.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisAddress> PARSER =
        new zaber.motion.dto.Parser<AxisAddress>() {
            @Override
            public AxisAddress fromByteArray(byte[] data) {
                return AxisAddress.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisAddress> parser() {
        return PARSER;
    }

}
