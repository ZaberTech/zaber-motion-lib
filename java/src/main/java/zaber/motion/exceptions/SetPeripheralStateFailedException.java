// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an axis cannot be set to the supplied state.
 */
public class SetPeripheralStateFailedException extends MotionLibException {
    private SetPeripheralStateExceptionData details;

    /**
     * @return Additional data for SetPeripheralStateFailedException
     */
    public SetPeripheralStateExceptionData getDetails() {
        return this.details;
    }

    public SetPeripheralStateFailedException(String message, byte[] customData) {
        super(message);

        this.details = SetPeripheralStateExceptionData.fromByteArray(customData);
    }

    public SetPeripheralStateFailedException(String message, SetPeripheralStateExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
