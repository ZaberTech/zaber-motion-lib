// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a movement command gets preempted by another command.
 */
public class CommandPreemptedException extends MotionLibException {
    public CommandPreemptedException(String message) {
        super(message);
    }
}
