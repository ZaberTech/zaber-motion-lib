// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when the library cannot perform an operation on a file.
 */
public class IoFailedException extends MotionLibException {
    public IoFailedException(String message) {
        super(message);
    }
}
