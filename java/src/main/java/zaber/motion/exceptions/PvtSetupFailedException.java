// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when setting up a PVT sequence fails.
 */
public class PvtSetupFailedException extends MotionLibException {
    public PvtSetupFailedException(String message) {
        super(message);
    }
}
