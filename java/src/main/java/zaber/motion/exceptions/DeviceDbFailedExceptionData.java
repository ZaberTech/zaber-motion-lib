/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for a DeviceDbFailedException.
 */
public final class DeviceDbFailedExceptionData implements zaber.motion.dto.Message {

    private String code;

    /**
     * Code describing type of the error.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Code describing type of the error.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("code")
    public String getCode() {
        return this.code;
    }

    /**
     * Code describing type of the error.
     */
    public DeviceDbFailedExceptionData withCode(String aCode) {
        this.setCode(aCode);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceDbFailedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceDbFailedExceptionData(
        String code
    ) {
        this.code = code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceDbFailedExceptionData other = (DeviceDbFailedExceptionData) obj;

        return (
            EqualityUtility.equals(code, other.code)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(code)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceDbFailedExceptionData { ");
        sb.append("code: ");
        sb.append(this.code);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceDbFailedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceDbFailedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceDbFailedExceptionData> PARSER =
        new zaber.motion.dto.Parser<DeviceDbFailedExceptionData>() {
            @Override
            public DeviceDbFailedExceptionData fromByteArray(byte[] data) {
                return DeviceDbFailedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceDbFailedExceptionData> parser() {
        return PARSER;
    }

}
