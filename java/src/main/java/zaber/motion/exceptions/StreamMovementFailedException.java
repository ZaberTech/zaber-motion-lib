// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device registers a fault during streamed movement.
 */
public class StreamMovementFailedException extends MotionLibException {
    private StreamMovementFailedExceptionData details;

    /**
     * @return Additional data for StreamMovementFailedException
     */
    public StreamMovementFailedExceptionData getDetails() {
        return this.details;
    }

    public StreamMovementFailedException(String message, byte[] customData) {
        super(message);

        this.details = StreamMovementFailedExceptionData.fromByteArray(customData);
    }

    public StreamMovementFailedException(String message, StreamMovementFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
