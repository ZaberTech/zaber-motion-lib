// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device registers fatal failure.
 * Contact support if you observe this exception.
 */
public class DeviceFailedException extends MotionLibException {
    public DeviceFailedException(String message) {
        super(message);
    }
}
