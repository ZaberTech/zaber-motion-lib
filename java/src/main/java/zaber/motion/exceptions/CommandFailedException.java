// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device rejects a command.
 */
public class CommandFailedException extends MotionLibException {
    private CommandFailedExceptionData details;

    /**
     * @return Additional data for CommandFailedException
     */
    public CommandFailedExceptionData getDetails() {
        return this.details;
    }

    public CommandFailedException(String message, byte[] customData) {
        super(message);

        this.details = CommandFailedExceptionData.fromByteArray(customData);
    }

    public CommandFailedException(String message, CommandFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
