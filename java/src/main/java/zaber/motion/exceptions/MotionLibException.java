package zaber.motion.exceptions;

/**
 * Errors propagated from the native library.
 */
public class MotionLibException extends RuntimeException {

    public MotionLibException(String message) {
        super(message);
    }

    public MotionLibException(Throwable e) {
        super(e);
    }

    public MotionLibException(String message, Throwable e) {
        super(message, e);
    }

}
