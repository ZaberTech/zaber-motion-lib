// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a parameter of a command is judged to be out of range by the receiving device.
 */
public class BadDataException extends CommandFailedException {
    public BadDataException(String message, byte[] customData) {
        super(message, customData);
    }

    public BadDataException(String message, CommandFailedExceptionData customData) {
        super(message, customData);
    }
}
