// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an operation cannot be performed because lockstep motion is enabled.
 */
public class LockstepEnabledException extends MotionLibException {
    public LockstepEnabledException(String message) {
        super(message);
    }
}
