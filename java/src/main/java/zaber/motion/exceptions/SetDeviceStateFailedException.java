// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device cannot be set to the supplied state.
 */
public class SetDeviceStateFailedException extends MotionLibException {
    private SetDeviceStateExceptionData details;

    /**
     * @return Additional data for SetDeviceStateFailedException
     */
    public SetDeviceStateExceptionData getDetails() {
        return this.details;
    }

    public SetDeviceStateFailedException(String message, byte[] customData) {
        super(message);

        this.details = SetDeviceStateExceptionData.fromByteArray(customData);
    }

    public SetDeviceStateFailedException(String message, SetDeviceStateExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
