// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a value cannot be converted using the provided units.
 */
public class ConversionFailedException extends MotionLibException {
    public ConversionFailedException(String message) {
        super(message);
    }
}
