// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device is unable to park.
 */
public class InvalidParkStateException extends MotionLibException {
    public InvalidParkStateException(String message) {
        super(message);
    }
}
