// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device registers a fault during PVT movement.
 */
public class PvtMovementFailedException extends MotionLibException {
    private PvtMovementFailedExceptionData details;

    /**
     * @return Additional data for PvtMovementFailedException
     */
    public PvtMovementFailedExceptionData getDetails() {
        return this.details;
    }

    public PvtMovementFailedException(String message, byte[] customData) {
        super(message);

        this.details = PvtMovementFailedExceptionData.fromByteArray(customData);
    }

    public PvtMovementFailedException(String message, PvtMovementFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
