// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an operation fails due to underlying operating system error.
 */
public class OsFailedException extends MotionLibException {
    public OsFailedException(String message) {
        super(message);
    }
}
