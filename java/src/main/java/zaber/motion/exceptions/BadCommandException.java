// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device receives an invalid command.
 */
public class BadCommandException extends CommandFailedException {
    public BadCommandException(String message, byte[] customData) {
        super(message, customData);
    }

    public BadCommandException(String message, CommandFailedExceptionData customData) {
        super(message, customData);
    }
}
