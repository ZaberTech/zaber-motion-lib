// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a transport has already been used to open another connection.
 */
public class TransportAlreadyUsedException extends MotionLibException {
    public TransportAlreadyUsedException(String message) {
        super(message);
    }
}
