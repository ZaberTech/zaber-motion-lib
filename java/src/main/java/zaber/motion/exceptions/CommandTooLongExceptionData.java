/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Information describing why the command could not fit.
 */
public final class CommandTooLongExceptionData implements zaber.motion.dto.Message {

    private String fit;

    /**
     * The part of the command that could be successfully fit in the space provided by the protocol.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fit")
    public void setFit(String fit) {
        this.fit = fit;
    }

    /**
     * The part of the command that could be successfully fit in the space provided by the protocol.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fit")
    public String getFit() {
        return this.fit;
    }

    /**
     * The part of the command that could be successfully fit in the space provided by the protocol.
     */
    public CommandTooLongExceptionData withFit(String aFit) {
        this.setFit(aFit);
        return this;
    }

    private String remainder;

    /**
     * The part of the command that could not fit within the space provided.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("remainder")
    public void setRemainder(String remainder) {
        this.remainder = remainder;
    }

    /**
     * The part of the command that could not fit within the space provided.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("remainder")
    public String getRemainder() {
        return this.remainder;
    }

    /**
     * The part of the command that could not fit within the space provided.
     */
    public CommandTooLongExceptionData withRemainder(String aRemainder) {
        this.setRemainder(aRemainder);
        return this;
    }

    private int packetSize;

    /**
     * The length of the ascii string that can be written to a single line.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packetSize")
    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    /**
     * The length of the ascii string that can be written to a single line.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packetSize")
    public int getPacketSize() {
        return this.packetSize;
    }

    /**
     * The length of the ascii string that can be written to a single line.
     */
    public CommandTooLongExceptionData withPacketSize(int aPacketSize) {
        this.setPacketSize(aPacketSize);
        return this;
    }

    private int packetsMax;

    /**
     * The number of lines a command can be split over using continuations.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packetsMax")
    public void setPacketsMax(int packetsMax) {
        this.packetsMax = packetsMax;
    }

    /**
     * The number of lines a command can be split over using continuations.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packetsMax")
    public int getPacketsMax() {
        return this.packetsMax;
    }

    /**
     * The number of lines a command can be split over using continuations.
     */
    public CommandTooLongExceptionData withPacketsMax(int aPacketsMax) {
        this.setPacketsMax(aPacketsMax);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CommandTooLongExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public CommandTooLongExceptionData(
        String fit,
        String remainder,
        int packetSize,
        int packetsMax
    ) {
        this.fit = fit;
        this.remainder = remainder;
        this.packetSize = packetSize;
        this.packetsMax = packetsMax;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CommandTooLongExceptionData other = (CommandTooLongExceptionData) obj;

        return (
            EqualityUtility.equals(fit, other.fit)
            && EqualityUtility.equals(remainder, other.remainder)
            && EqualityUtility.equals(packetSize, other.packetSize)
            && EqualityUtility.equals(packetsMax, other.packetsMax)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(fit),
            EqualityUtility.generateHashCode(remainder),
            EqualityUtility.generateHashCode(packetSize),
            EqualityUtility.generateHashCode(packetsMax)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CommandTooLongExceptionData { ");
        sb.append("fit: ");
        sb.append(this.fit);
        sb.append(", ");
        sb.append("remainder: ");
        sb.append(this.remainder);
        sb.append(", ");
        sb.append("packetSize: ");
        sb.append(this.packetSize);
        sb.append(", ");
        sb.append("packetsMax: ");
        sb.append(this.packetsMax);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CommandTooLongExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CommandTooLongExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CommandTooLongExceptionData> PARSER =
        new zaber.motion.dto.Parser<CommandTooLongExceptionData>() {
            @Override
            public CommandTooLongExceptionData fromByteArray(byte[] data) {
                return CommandTooLongExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CommandTooLongExceptionData> parser() {
        return PARSER;
    }

}
