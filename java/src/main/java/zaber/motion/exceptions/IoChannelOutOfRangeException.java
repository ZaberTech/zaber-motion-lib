// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device IO operation cannot be performed because the provided channel is not valid.
 */
public class IoChannelOutOfRangeException extends MotionLibException {
    public IoChannelOutOfRangeException(String message) {
        super(message);
    }
}
