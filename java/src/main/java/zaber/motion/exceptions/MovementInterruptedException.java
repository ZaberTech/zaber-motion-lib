// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when ongoing movement is interrupted by another command or user input.
 */
public class MovementInterruptedException extends MotionLibException {
    private MovementInterruptedExceptionData details;

    /**
     * @return Additional data for MovementInterruptedException
     */
    public MovementInterruptedExceptionData getDetails() {
        return this.details;
    }

    public MovementInterruptedException(String message, byte[] customData) {
        super(message);

        this.details = MovementInterruptedExceptionData.fromByteArray(customData);
    }

    public MovementInterruptedException(String message, MovementInterruptedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
