/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for StreamExecutionException.
 */
public final class StreamExecutionExceptionData implements zaber.motion.dto.Message {

    private String errorFlag;

    /**
     * The error flag that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorFlag")
    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    /**
     * The error flag that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorFlag")
    public String getErrorFlag() {
        return this.errorFlag;
    }

    /**
     * The error flag that caused the exception.
     */
    public StreamExecutionExceptionData withErrorFlag(String aErrorFlag) {
        this.setErrorFlag(aErrorFlag);
        return this;
    }

    private String reason;

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the exception.
     */
    public StreamExecutionExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamExecutionExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamExecutionExceptionData(
        String errorFlag,
        String reason
    ) {
        this.errorFlag = errorFlag;
        this.reason = reason;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamExecutionExceptionData other = (StreamExecutionExceptionData) obj;

        return (
            EqualityUtility.equals(errorFlag, other.errorFlag)
            && EqualityUtility.equals(reason, other.reason)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(errorFlag),
            EqualityUtility.generateHashCode(reason)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamExecutionExceptionData { ");
        sb.append("errorFlag: ");
        sb.append(this.errorFlag);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamExecutionExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamExecutionExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamExecutionExceptionData> PARSER =
        new zaber.motion.dto.Parser<StreamExecutionExceptionData>() {
            @Override
            public StreamExecutionExceptionData fromByteArray(byte[] data) {
                return StreamExecutionExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamExecutionExceptionData> parser() {
        return PARSER;
    }

}
