// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a command is too long to be written by the ASCII protocol, even when continued across multiple lines.
 */
public class CommandTooLongException extends MotionLibException {
    private CommandTooLongExceptionData details;

    /**
     * @return Additional data for CommandTooLongException
     */
    public CommandTooLongExceptionData getDetails() {
        return this.details;
    }

    public CommandTooLongException(String message, byte[] customData) {
        super(message);

        this.details = CommandTooLongExceptionData.fromByteArray(customData);
    }

    public CommandTooLongException(String message, CommandTooLongExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
