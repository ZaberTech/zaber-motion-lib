// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when no devices can be found on a connection.
 */
public class NoDeviceFoundException extends MotionLibException {
    public NoDeviceFoundException(String message) {
        super(message);
    }
}
