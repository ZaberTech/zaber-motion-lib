// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device registers a fault during movement.
 */
public class MovementFailedException extends MotionLibException {
    private MovementFailedExceptionData details;

    /**
     * @return Additional data for MovementFailedException
     */
    public MovementFailedExceptionData getDetails() {
        return this.details;
    }

    public MovementFailedException(String message, byte[] customData) {
        super(message);

        this.details = MovementFailedExceptionData.fromByteArray(customData);
    }

    public MovementFailedException(String message, MovementFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
