/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains invalid PVT points for PvtExecutionException.
 */
public final class InvalidPvtPoint implements zaber.motion.dto.Message {

    private int index;

    /**
     * Index of the point numbered from the last submitted point.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("index")
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Index of the point numbered from the last submitted point.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("index")
    public int getIndex() {
        return this.index;
    }

    /**
     * Index of the point numbered from the last submitted point.
     */
    public InvalidPvtPoint withIndex(int aIndex) {
        this.setIndex(aIndex);
        return this;
    }

    private String point;

    /**
     * The textual representation of the point.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("point")
    public void setPoint(String point) {
        this.point = point;
    }

    /**
     * The textual representation of the point.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("point")
    public String getPoint() {
        return this.point;
    }

    /**
     * The textual representation of the point.
     */
    public InvalidPvtPoint withPoint(String aPoint) {
        this.setPoint(aPoint);
        return this;
    }

    /**
     * Empty constructor.
     */
    public InvalidPvtPoint() {
    }

    /**
     * Constructor with all properties.
     */
    public InvalidPvtPoint(
        int index,
        String point
    ) {
        this.index = index;
        this.point = point;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        InvalidPvtPoint other = (InvalidPvtPoint) obj;

        return (
            EqualityUtility.equals(index, other.index)
            && EqualityUtility.equals(point, other.point)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(index),
            EqualityUtility.generateHashCode(point)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("InvalidPvtPoint { ");
        sb.append("index: ");
        sb.append(this.index);
        sb.append(", ");
        sb.append("point: ");
        sb.append(this.point);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static InvalidPvtPoint fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, InvalidPvtPoint.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<InvalidPvtPoint> PARSER =
        new zaber.motion.dto.Parser<InvalidPvtPoint>() {
            @Override
            public InvalidPvtPoint fromByteArray(byte[] data) {
                return InvalidPvtPoint.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<InvalidPvtPoint> parser() {
        return PARSER;
    }

}
