// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an operation is not supported by a mode the PVT sequence is currently set up in.
 */
public class PvtModeException extends MotionLibException {
    public PvtModeException(String message) {
        super(message);
    }
}
