// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Used for internal error handling. Please report an issue if observed.
 */
public class InternalErrorException extends MotionLibException {
    public InternalErrorException(String message) {
        super(message);
    }
}
