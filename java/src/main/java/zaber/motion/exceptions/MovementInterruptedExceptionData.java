/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for MovementInterruptedException.
 */
public final class MovementInterruptedExceptionData implements zaber.motion.dto.Message {

    private String[] warnings;

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public String[] getWarnings() {
        return this.warnings;
    }

    /**
     * The full list of warnings.
     */
    public MovementInterruptedExceptionData withWarnings(String[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    private String reason;

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the Exception.
     */
    public MovementInterruptedExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    private int device;

    /**
     * The address of the device that caused the interruption.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    /**
     * The address of the device that caused the interruption.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    /**
     * The address of the device that caused the interruption.
     */
    public MovementInterruptedExceptionData withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    /**
     * The number of the axis that caused the interruption.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    /**
     * The number of the axis that caused the interruption.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    /**
     * The number of the axis that caused the interruption.
     */
    public MovementInterruptedExceptionData withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MovementInterruptedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public MovementInterruptedExceptionData(
        String[] warnings,
        String reason,
        int device,
        int axis
    ) {
        this.warnings = warnings;
        this.reason = reason;
        this.device = device;
        this.axis = axis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MovementInterruptedExceptionData other = (MovementInterruptedExceptionData) obj;

        return (
            EqualityUtility.equals(warnings, other.warnings)
            && EqualityUtility.equals(reason, other.reason)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(warnings),
            EqualityUtility.generateHashCode(reason),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MovementInterruptedExceptionData { ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MovementInterruptedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MovementInterruptedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MovementInterruptedExceptionData> PARSER =
        new zaber.motion.dto.Parser<MovementInterruptedExceptionData>() {
            @Override
            public MovementInterruptedExceptionData fromByteArray(byte[] data) {
                return MovementInterruptedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MovementInterruptedExceptionData> parser() {
        return PARSER;
    }

}
