// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Used for internal error handling.
 * Indicates passing values of incorrect type from scripting languages or mixed library binary files.
 */
public class InvalidRequestDataException extends MotionLibException {
    public InvalidRequestDataException(String message) {
        super(message);
    }
}
