/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for InvalidResponseException.
 */
public final class InvalidResponseExceptionData implements zaber.motion.dto.Message {

    private String response;

    /**
     * The response data.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("response")
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * The response data.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("response")
    public String getResponse() {
        return this.response;
    }

    /**
     * The response data.
     */
    public InvalidResponseExceptionData withResponse(String aResponse) {
        this.setResponse(aResponse);
        return this;
    }

    /**
     * Empty constructor.
     */
    public InvalidResponseExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public InvalidResponseExceptionData(
        String response
    ) {
        this.response = response;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        InvalidResponseExceptionData other = (InvalidResponseExceptionData) obj;

        return (
            EqualityUtility.equals(response, other.response)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(response)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("InvalidResponseExceptionData { ");
        sb.append("response: ");
        sb.append(this.response);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static InvalidResponseExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, InvalidResponseExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<InvalidResponseExceptionData> PARSER =
        new zaber.motion.dto.Parser<InvalidResponseExceptionData>() {
            @Override
            public InvalidResponseExceptionData fromByteArray(byte[] data) {
                return InvalidResponseExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<InvalidResponseExceptionData> parser() {
        return PARSER;
    }

}
