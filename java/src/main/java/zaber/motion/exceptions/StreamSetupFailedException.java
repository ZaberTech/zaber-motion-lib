// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when setting up a stream fails.
 */
public class StreamSetupFailedException extends MotionLibException {
    public StreamSetupFailedException(String message) {
        super(message);
    }
}
