/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for CommandFailedException.
 */
public final class CommandFailedExceptionData implements zaber.motion.dto.Message {

    private String command;

    /**
     * The command that got rejected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * The command that got rejected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public String getCommand() {
        return this.command;
    }

    /**
     * The command that got rejected.
     */
    public CommandFailedExceptionData withCommand(String aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    private String responseData;

    /**
     * The data from the reply containing the rejection reason.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("responseData")
    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    /**
     * The data from the reply containing the rejection reason.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("responseData")
    public String getResponseData() {
        return this.responseData;
    }

    /**
     * The data from the reply containing the rejection reason.
     */
    public CommandFailedExceptionData withResponseData(String aResponseData) {
        this.setResponseData(aResponseData);
        return this;
    }

    private String replyFlag;

    /**
     * The flag indicating that the command was rejected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("replyFlag")
    public void setReplyFlag(String replyFlag) {
        this.replyFlag = replyFlag;
    }

    /**
     * The flag indicating that the command was rejected.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("replyFlag")
    public String getReplyFlag() {
        return this.replyFlag;
    }

    /**
     * The flag indicating that the command was rejected.
     */
    public CommandFailedExceptionData withReplyFlag(String aReplyFlag) {
        this.setReplyFlag(aReplyFlag);
        return this;
    }

    private String status;

    /**
     * The current device or axis status.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * The current device or axis status.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    /**
     * The current device or axis status.
     */
    public CommandFailedExceptionData withStatus(String aStatus) {
        this.setStatus(aStatus);
        return this;
    }

    private String warningFlag;

    /**
     * The highest priority warning flag on the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warningFlag")
    public void setWarningFlag(String warningFlag) {
        this.warningFlag = warningFlag;
    }

    /**
     * The highest priority warning flag on the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warningFlag")
    public String getWarningFlag() {
        return this.warningFlag;
    }

    /**
     * The highest priority warning flag on the device or axis.
     */
    public CommandFailedExceptionData withWarningFlag(String aWarningFlag) {
        this.setWarningFlag(aWarningFlag);
        return this;
    }

    private int deviceAddress;

    /**
     * The address of the device that rejected the command.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public void setDeviceAddress(int deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * The address of the device that rejected the command.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public int getDeviceAddress() {
        return this.deviceAddress;
    }

    /**
     * The address of the device that rejected the command.
     */
    public CommandFailedExceptionData withDeviceAddress(int aDeviceAddress) {
        this.setDeviceAddress(aDeviceAddress);
        return this;
    }

    private int axisNumber;

    /**
     * The number of the axis which the rejection relates to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * The number of the axis which the rejection relates to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * The number of the axis which the rejection relates to.
     */
    public CommandFailedExceptionData withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private int id;

    /**
     * The message ID of the reply.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The message ID of the reply.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("id")
    public int getId() {
        return this.id;
    }

    /**
     * The message ID of the reply.
     */
    public CommandFailedExceptionData withId(int aId) {
        this.setId(aId);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CommandFailedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public CommandFailedExceptionData(
        String command,
        String responseData,
        String replyFlag,
        String status,
        String warningFlag,
        int deviceAddress,
        int axisNumber,
        int id
    ) {
        this.command = command;
        this.responseData = responseData;
        this.replyFlag = replyFlag;
        this.status = status;
        this.warningFlag = warningFlag;
        this.deviceAddress = deviceAddress;
        this.axisNumber = axisNumber;
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CommandFailedExceptionData other = (CommandFailedExceptionData) obj;

        return (
            EqualityUtility.equals(command, other.command)
            && EqualityUtility.equals(responseData, other.responseData)
            && EqualityUtility.equals(replyFlag, other.replyFlag)
            && EqualityUtility.equals(status, other.status)
            && EqualityUtility.equals(warningFlag, other.warningFlag)
            && EqualityUtility.equals(deviceAddress, other.deviceAddress)
            && EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(id, other.id)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(command),
            EqualityUtility.generateHashCode(responseData),
            EqualityUtility.generateHashCode(replyFlag),
            EqualityUtility.generateHashCode(status),
            EqualityUtility.generateHashCode(warningFlag),
            EqualityUtility.generateHashCode(deviceAddress),
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(id)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CommandFailedExceptionData { ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(", ");
        sb.append("responseData: ");
        sb.append(this.responseData);
        sb.append(", ");
        sb.append("replyFlag: ");
        sb.append(this.replyFlag);
        sb.append(", ");
        sb.append("status: ");
        sb.append(this.status);
        sb.append(", ");
        sb.append("warningFlag: ");
        sb.append(this.warningFlag);
        sb.append(", ");
        sb.append("deviceAddress: ");
        sb.append(this.deviceAddress);
        sb.append(", ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("id: ");
        sb.append(this.id);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CommandFailedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CommandFailedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CommandFailedExceptionData> PARSER =
        new zaber.motion.dto.Parser<CommandFailedExceptionData>() {
            @Override
            public CommandFailedExceptionData fromByteArray(byte[] data) {
                return CommandFailedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CommandFailedExceptionData> parser() {
        return PARSER;
    }

}
