/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for a SetDeviceStateFailedException.
 */
public final class SetDeviceStateExceptionData implements zaber.motion.dto.Message {

    private String[] settings;

    /**
     * A list of settings which could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public void setSettings(String[] settings) {
        this.settings = settings;
    }

    /**
     * A list of settings which could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public String[] getSettings() {
        return this.settings;
    }

    /**
     * A list of settings which could not be set.
     */
    public SetDeviceStateExceptionData withSettings(String[] aSettings) {
        this.setSettings(aSettings);
        return this;
    }

    private String[] streamBuffers;

    /**
     * The reason the stream buffers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffers")
    public void setStreamBuffers(String[] streamBuffers) {
        this.streamBuffers = streamBuffers;
    }

    /**
     * The reason the stream buffers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("streamBuffers")
    public String[] getStreamBuffers() {
        return this.streamBuffers;
    }

    /**
     * The reason the stream buffers could not be set.
     */
    public SetDeviceStateExceptionData withStreamBuffers(String[] aStreamBuffers) {
        this.setStreamBuffers(aStreamBuffers);
        return this;
    }

    private String[] pvtBuffers;

    /**
     * The reason the pvt buffers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffers")
    public void setPvtBuffers(String[] pvtBuffers) {
        this.pvtBuffers = pvtBuffers;
    }

    /**
     * The reason the pvt buffers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("pvtBuffers")
    public String[] getPvtBuffers() {
        return this.pvtBuffers;
    }

    /**
     * The reason the pvt buffers could not be set.
     */
    public SetDeviceStateExceptionData withPvtBuffers(String[] aPvtBuffers) {
        this.setPvtBuffers(aPvtBuffers);
        return this;
    }

    private String[] triggers;

    /**
     * The reason the triggers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("triggers")
    public void setTriggers(String[] triggers) {
        this.triggers = triggers;
    }

    /**
     * The reason the triggers could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("triggers")
    public String[] getTriggers() {
        return this.triggers;
    }

    /**
     * The reason the triggers could not be set.
     */
    public SetDeviceStateExceptionData withTriggers(String[] aTriggers) {
        this.setTriggers(aTriggers);
        return this;
    }

    private String servoTuning;

    /**
     * The reason servo tuning could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("servoTuning")
    public void setServoTuning(String servoTuning) {
        this.servoTuning = servoTuning;
    }

    /**
     * The reason servo tuning could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("servoTuning")
    public String getServoTuning() {
        return this.servoTuning;
    }

    /**
     * The reason servo tuning could not be set.
     */
    public SetDeviceStateExceptionData withServoTuning(String aServoTuning) {
        this.setServoTuning(aServoTuning);
        return this;
    }

    private String[] storedPositions;

    /**
     * The reasons stored positions could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storedPositions")
    public void setStoredPositions(String[] storedPositions) {
        this.storedPositions = storedPositions;
    }

    /**
     * The reasons stored positions could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storedPositions")
    public String[] getStoredPositions() {
        return this.storedPositions;
    }

    /**
     * The reasons stored positions could not be set.
     */
    public SetDeviceStateExceptionData withStoredPositions(String[] aStoredPositions) {
        this.setStoredPositions(aStoredPositions);
        return this;
    }

    private String[] storage;

    /**
     * The reasons storage could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storage")
    public void setStorage(String[] storage) {
        this.storage = storage;
    }

    /**
     * The reasons storage could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storage")
    public String[] getStorage() {
        return this.storage;
    }

    /**
     * The reasons storage could not be set.
     */
    public SetDeviceStateExceptionData withStorage(String[] aStorage) {
        this.setStorage(aStorage);
        return this;
    }

    private SetPeripheralStateExceptionData[] peripherals;

    /**
     * Errors for any peripherals that could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripherals")
    public void setPeripherals(SetPeripheralStateExceptionData[] peripherals) {
        this.peripherals = peripherals;
    }

    /**
     * Errors for any peripherals that could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripherals")
    public SetPeripheralStateExceptionData[] getPeripherals() {
        return this.peripherals;
    }

    /**
     * Errors for any peripherals that could not be set.
     */
    public SetDeviceStateExceptionData withPeripherals(SetPeripheralStateExceptionData[] aPeripherals) {
        this.setPeripherals(aPeripherals);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetDeviceStateExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public SetDeviceStateExceptionData(
        String[] settings,
        String[] streamBuffers,
        String[] pvtBuffers,
        String[] triggers,
        String servoTuning,
        String[] storedPositions,
        String[] storage,
        SetPeripheralStateExceptionData[] peripherals
    ) {
        this.settings = settings;
        this.streamBuffers = streamBuffers;
        this.pvtBuffers = pvtBuffers;
        this.triggers = triggers;
        this.servoTuning = servoTuning;
        this.storedPositions = storedPositions;
        this.storage = storage;
        this.peripherals = peripherals;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetDeviceStateExceptionData other = (SetDeviceStateExceptionData) obj;

        return (
            EqualityUtility.equals(settings, other.settings)
            && EqualityUtility.equals(streamBuffers, other.streamBuffers)
            && EqualityUtility.equals(pvtBuffers, other.pvtBuffers)
            && EqualityUtility.equals(triggers, other.triggers)
            && EqualityUtility.equals(servoTuning, other.servoTuning)
            && EqualityUtility.equals(storedPositions, other.storedPositions)
            && EqualityUtility.equals(storage, other.storage)
            && EqualityUtility.equals(peripherals, other.peripherals)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(settings),
            EqualityUtility.generateHashCode(streamBuffers),
            EqualityUtility.generateHashCode(pvtBuffers),
            EqualityUtility.generateHashCode(triggers),
            EqualityUtility.generateHashCode(servoTuning),
            EqualityUtility.generateHashCode(storedPositions),
            EqualityUtility.generateHashCode(storage),
            EqualityUtility.generateHashCode(peripherals)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetDeviceStateExceptionData { ");
        sb.append("settings: ");
        sb.append(this.settings);
        sb.append(", ");
        sb.append("streamBuffers: ");
        sb.append(this.streamBuffers);
        sb.append(", ");
        sb.append("pvtBuffers: ");
        sb.append(this.pvtBuffers);
        sb.append(", ");
        sb.append("triggers: ");
        sb.append(this.triggers);
        sb.append(", ");
        sb.append("servoTuning: ");
        sb.append(this.servoTuning);
        sb.append(", ");
        sb.append("storedPositions: ");
        sb.append(this.storedPositions);
        sb.append(", ");
        sb.append("storage: ");
        sb.append(this.storage);
        sb.append(", ");
        sb.append("peripherals: ");
        sb.append(this.peripherals);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetDeviceStateExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetDeviceStateExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetDeviceStateExceptionData> PARSER =
        new zaber.motion.dto.Parser<SetDeviceStateExceptionData>() {
            @Override
            public SetDeviceStateExceptionData fromByteArray(byte[] data) {
                return SetDeviceStateExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetDeviceStateExceptionData> parser() {
        return PARSER;
    }

}
