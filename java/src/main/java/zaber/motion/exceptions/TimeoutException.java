// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown for various timeouts across the library excluding request to a device (see RequestTimeoutException).
 */
public class TimeoutException extends MotionLibException {
    public TimeoutException(String message) {
        super(message);
    }
}
