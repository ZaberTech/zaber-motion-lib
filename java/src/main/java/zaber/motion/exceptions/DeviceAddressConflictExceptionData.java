/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for DeviceAddressConflictException.
 */
public final class DeviceAddressConflictExceptionData implements zaber.motion.dto.Message {

    private int[] deviceAddresses;

    /**
     * The full list of detected device addresses.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddresses")
    public void setDeviceAddresses(int[] deviceAddresses) {
        this.deviceAddresses = deviceAddresses;
    }

    /**
     * The full list of detected device addresses.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddresses")
    public int[] getDeviceAddresses() {
        return this.deviceAddresses;
    }

    /**
     * The full list of detected device addresses.
     */
    public DeviceAddressConflictExceptionData withDeviceAddresses(int[] aDeviceAddresses) {
        this.setDeviceAddresses(aDeviceAddresses);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceAddressConflictExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceAddressConflictExceptionData(
        int[] deviceAddresses
    ) {
        this.deviceAddresses = deviceAddresses;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceAddressConflictExceptionData other = (DeviceAddressConflictExceptionData) obj;

        return (
            EqualityUtility.equals(deviceAddresses, other.deviceAddresses)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceAddresses)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceAddressConflictExceptionData { ");
        sb.append("deviceAddresses: ");
        sb.append(this.deviceAddresses);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceAddressConflictExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceAddressConflictExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceAddressConflictExceptionData> PARSER =
        new zaber.motion.dto.Parser<DeviceAddressConflictExceptionData>() {
            @Override
            public DeviceAddressConflictExceptionData fromByteArray(byte[] data) {
                return DeviceAddressConflictExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceAddressConflictExceptionData> parser() {
        return PARSER;
    }

}
