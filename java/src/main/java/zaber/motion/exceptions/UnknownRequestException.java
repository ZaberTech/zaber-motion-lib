// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Used for internal error handling. Indicates mixed library binary files. Reinstall the library.
 */
public class UnknownRequestException extends MotionLibException {
    public UnknownRequestException(String message) {
        super(message);
    }
}
