// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an operation is not supported by a mode the stream is currently set up in.
 */
public class StreamModeException extends MotionLibException {
    public StreamModeException(String message) {
        super(message);
    }
}
