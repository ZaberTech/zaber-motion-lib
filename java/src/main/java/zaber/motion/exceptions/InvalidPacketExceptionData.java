/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for the InvalidPacketException.
 */
public final class InvalidPacketExceptionData implements zaber.motion.dto.Message {

    private String packet;

    /**
     * The invalid packet that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packet")
    public void setPacket(String packet) {
        this.packet = packet;
    }

    /**
     * The invalid packet that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("packet")
    public String getPacket() {
        return this.packet;
    }

    /**
     * The invalid packet that caused the exception.
     */
    public InvalidPacketExceptionData withPacket(String aPacket) {
        this.setPacket(aPacket);
        return this;
    }

    private String reason;

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the exception.
     */
    public InvalidPacketExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    /**
     * Empty constructor.
     */
    public InvalidPacketExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public InvalidPacketExceptionData(
        String packet,
        String reason
    ) {
        this.packet = packet;
        this.reason = reason;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        InvalidPacketExceptionData other = (InvalidPacketExceptionData) obj;

        return (
            EqualityUtility.equals(packet, other.packet)
            && EqualityUtility.equals(reason, other.reason)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(packet),
            EqualityUtility.generateHashCode(reason)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("InvalidPacketExceptionData { ");
        sb.append("packet: ");
        sb.append(this.packet);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static InvalidPacketExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, InvalidPacketExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<InvalidPacketExceptionData> PARSER =
        new zaber.motion.dto.Parser<InvalidPacketExceptionData>() {
            @Override
            public InvalidPacketExceptionData fromByteArray(byte[] data) {
                return InvalidPacketExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<InvalidPacketExceptionData> parser() {
        return PARSER;
    }

}
