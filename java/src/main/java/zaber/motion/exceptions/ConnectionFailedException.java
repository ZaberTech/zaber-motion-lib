// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a connection breaks during a request.
 */
public class ConnectionFailedException extends MotionLibException {
    public ConnectionFailedException(String message) {
        super(message);
    }
}
