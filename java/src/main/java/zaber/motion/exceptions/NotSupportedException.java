// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device does not support a requested command or setting.
 */
public class NotSupportedException extends MotionLibException {
    public NotSupportedException(String message) {
        super(message);
    }
}
