/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for GCodeSyntaxException.
 */
public final class GCodeSyntaxExceptionData implements zaber.motion.dto.Message {

    private int fromBlock;

    /**
     * The index in the block string that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fromBlock")
    public void setFromBlock(int fromBlock) {
        this.fromBlock = fromBlock;
    }

    /**
     * The index in the block string that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fromBlock")
    public int getFromBlock() {
        return this.fromBlock;
    }

    /**
     * The index in the block string that caused the exception.
     */
    public GCodeSyntaxExceptionData withFromBlock(int aFromBlock) {
        this.setFromBlock(aFromBlock);
        return this;
    }

    private int toBlock;

    /**
     * The end index in the block string that caused the exception.
     * The end index is exclusive.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("toBlock")
    public void setToBlock(int toBlock) {
        this.toBlock = toBlock;
    }

    /**
     * The end index in the block string that caused the exception.
     * The end index is exclusive.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("toBlock")
    public int getToBlock() {
        return this.toBlock;
    }

    /**
     * The end index in the block string that caused the exception.
     * The end index is exclusive.
     */
    public GCodeSyntaxExceptionData withToBlock(int aToBlock) {
        this.setToBlock(aToBlock);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GCodeSyntaxExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public GCodeSyntaxExceptionData(
        int fromBlock,
        int toBlock
    ) {
        this.fromBlock = fromBlock;
        this.toBlock = toBlock;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GCodeSyntaxExceptionData other = (GCodeSyntaxExceptionData) obj;

        return (
            EqualityUtility.equals(fromBlock, other.fromBlock)
            && EqualityUtility.equals(toBlock, other.toBlock)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(fromBlock),
            EqualityUtility.generateHashCode(toBlock)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GCodeSyntaxExceptionData { ");
        sb.append("fromBlock: ");
        sb.append(this.fromBlock);
        sb.append(", ");
        sb.append("toBlock: ");
        sb.append(this.toBlock);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GCodeSyntaxExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GCodeSyntaxExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GCodeSyntaxExceptionData> PARSER =
        new zaber.motion.dto.Parser<GCodeSyntaxExceptionData>() {
            @Override
            public GCodeSyntaxExceptionData fromByteArray(byte[] data) {
                return GCodeSyntaxExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GCodeSyntaxExceptionData> parser() {
        return PARSER;
    }

}
