// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a packet from a device cannot be parsed.
 */
public class InvalidPacketException extends MotionLibException {
    private InvalidPacketExceptionData details;

    /**
     * @return Additional data for InvalidPacketException
     */
    public InvalidPacketExceptionData getDetails() {
        return this.details;
    }

    public InvalidPacketException(String message, byte[] customData) {
        super(message);

        this.details = InvalidPacketExceptionData.fromByteArray(customData);
    }

    public InvalidPacketException(String message, InvalidPacketExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
