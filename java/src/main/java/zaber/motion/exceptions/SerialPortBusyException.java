// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a serial port cannot be opened because it is in use by another application.
 */
public class SerialPortBusyException extends ConnectionFailedException {
    public SerialPortBusyException(String message) {
        super(message);
    }
}
