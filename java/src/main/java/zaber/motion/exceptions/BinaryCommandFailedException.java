// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device rejects a binary command with an error.
 */
public class BinaryCommandFailedException extends MotionLibException {
    private BinaryCommandFailedExceptionData details;

    /**
     * @return Additional data for BinaryCommandFailedException
     */
    public BinaryCommandFailedExceptionData getDetails() {
        return this.details;
    }

    public BinaryCommandFailedException(String message, byte[] customData) {
        super(message);

        this.details = BinaryCommandFailedExceptionData.fromByteArray(customData);
    }

    public BinaryCommandFailedException(String message, BinaryCommandFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
