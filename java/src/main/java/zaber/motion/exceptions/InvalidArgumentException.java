// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a function is called with invalid values.
 */
public class InvalidArgumentException extends MotionLibException {
    public InvalidArgumentException(String message) {
        super(message);
    }
}
