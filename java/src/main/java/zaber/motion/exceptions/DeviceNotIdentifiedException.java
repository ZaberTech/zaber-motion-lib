// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when attempting an operation that requires an identified device.
 */
public class DeviceNotIdentifiedException extends MotionLibException {
    public DeviceNotIdentifiedException(String message) {
        super(message);
    }
}
