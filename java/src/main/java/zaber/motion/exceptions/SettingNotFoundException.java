// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a get or a set command cannot be found for a setting.
 */
public class SettingNotFoundException extends MotionLibException {
    public SettingNotFoundException(String message) {
        super(message);
    }
}
