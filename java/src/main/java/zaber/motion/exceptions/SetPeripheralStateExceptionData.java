/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for a SetPeripheralStateFailedException.
 */
public final class SetPeripheralStateExceptionData implements zaber.motion.dto.Message {

    private int axisNumber;

    /**
     * The number of axis where the exception originated.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * The number of axis where the exception originated.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * The number of axis where the exception originated.
     */
    public SetPeripheralStateExceptionData withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private String[] settings;

    /**
     * A list of settings which could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public void setSettings(String[] settings) {
        this.settings = settings;
    }

    /**
     * A list of settings which could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("settings")
    public String[] getSettings() {
        return this.settings;
    }

    /**
     * A list of settings which could not be set.
     */
    public SetPeripheralStateExceptionData withSettings(String[] aSettings) {
        this.setSettings(aSettings);
        return this;
    }

    private String servoTuning;

    /**
     * The reason servo tuning could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("servoTuning")
    public void setServoTuning(String servoTuning) {
        this.servoTuning = servoTuning;
    }

    /**
     * The reason servo tuning could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("servoTuning")
    public String getServoTuning() {
        return this.servoTuning;
    }

    /**
     * The reason servo tuning could not be set.
     */
    public SetPeripheralStateExceptionData withServoTuning(String aServoTuning) {
        this.setServoTuning(aServoTuning);
        return this;
    }

    private String[] storedPositions;

    /**
     * The reasons stored positions could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storedPositions")
    public void setStoredPositions(String[] storedPositions) {
        this.storedPositions = storedPositions;
    }

    /**
     * The reasons stored positions could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storedPositions")
    public String[] getStoredPositions() {
        return this.storedPositions;
    }

    /**
     * The reasons stored positions could not be set.
     */
    public SetPeripheralStateExceptionData withStoredPositions(String[] aStoredPositions) {
        this.setStoredPositions(aStoredPositions);
        return this;
    }

    private String[] storage;

    /**
     * The reasons storage could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storage")
    public void setStorage(String[] storage) {
        this.storage = storage;
    }

    /**
     * The reasons storage could not be set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("storage")
    public String[] getStorage() {
        return this.storage;
    }

    /**
     * The reasons storage could not be set.
     */
    public SetPeripheralStateExceptionData withStorage(String[] aStorage) {
        this.setStorage(aStorage);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetPeripheralStateExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public SetPeripheralStateExceptionData(
        int axisNumber,
        String[] settings,
        String servoTuning,
        String[] storedPositions,
        String[] storage
    ) {
        this.axisNumber = axisNumber;
        this.settings = settings;
        this.servoTuning = servoTuning;
        this.storedPositions = storedPositions;
        this.storage = storage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetPeripheralStateExceptionData other = (SetPeripheralStateExceptionData) obj;

        return (
            EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(settings, other.settings)
            && EqualityUtility.equals(servoTuning, other.servoTuning)
            && EqualityUtility.equals(storedPositions, other.storedPositions)
            && EqualityUtility.equals(storage, other.storage)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(settings),
            EqualityUtility.generateHashCode(servoTuning),
            EqualityUtility.generateHashCode(storedPositions),
            EqualityUtility.generateHashCode(storage)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetPeripheralStateExceptionData { ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("settings: ");
        sb.append(this.settings);
        sb.append(", ");
        sb.append("servoTuning: ");
        sb.append(this.servoTuning);
        sb.append(", ");
        sb.append("storedPositions: ");
        sb.append(this.storedPositions);
        sb.append(", ");
        sb.append("storage: ");
        sb.append(this.storage);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetPeripheralStateExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetPeripheralStateExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetPeripheralStateExceptionData> PARSER =
        new zaber.motion.dto.Parser<SetPeripheralStateExceptionData>() {
            @Override
            public SetPeripheralStateExceptionData fromByteArray(byte[] data) {
                return SetPeripheralStateExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetPeripheralStateExceptionData> parser() {
        return PARSER;
    }

}
