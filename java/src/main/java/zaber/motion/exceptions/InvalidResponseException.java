// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device sends a response with unexpected type or data.
 */
public class InvalidResponseException extends MotionLibException {
    private InvalidResponseExceptionData details;

    /**
     * @return Additional data for InvalidResponseException
     */
    public InvalidResponseExceptionData getDetails() {
        return this.details;
    }

    public InvalidResponseException(String message, byte[] customData) {
        super(message);

        this.details = InvalidResponseExceptionData.fromByteArray(customData);
    }

    public InvalidResponseException(String message, InvalidResponseExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
