// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when there is a conflict in device numbers preventing unique addressing.
 */
public class DeviceAddressConflictException extends MotionLibException {
    private DeviceAddressConflictExceptionData details;

    /**
     * @return Additional data for DeviceAddressConflictException
     */
    public DeviceAddressConflictExceptionData getDetails() {
        return this.details;
    }

    public DeviceAddressConflictException(String message, byte[] customData) {
        super(message);

        this.details = DeviceAddressConflictExceptionData.fromByteArray(customData);
    }

    public DeviceAddressConflictException(String message, DeviceAddressConflictExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
