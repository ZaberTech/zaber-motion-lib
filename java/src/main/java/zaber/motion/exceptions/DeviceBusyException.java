// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a requested operation fails because the device is currently busy.
 */
public class DeviceBusyException extends MotionLibException {
    public DeviceBusyException(String message) {
        super(message);
    }
}
