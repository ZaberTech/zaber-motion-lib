// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when the loaded shared library is incompatible with the running code.
 * Typically caused by mixed library binary files. Reinstall the library.
 */
public class IncompatibleSharedLibraryException extends MotionLibException {
    public IncompatibleSharedLibraryException(String message) {
        super(message);
    }
}
