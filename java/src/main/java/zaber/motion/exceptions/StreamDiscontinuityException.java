// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a stream encounters discontinuity and interrupts the movement.
 */
public class StreamDiscontinuityException extends MotionLibException {
    public StreamDiscontinuityException(String message) {
        super(message);
    }
}
