// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when operation cannot be performed at given time or context.
 */
public class InvalidOperationException extends MotionLibException {
    public InvalidOperationException(String message) {
        super(message);
    }
}
