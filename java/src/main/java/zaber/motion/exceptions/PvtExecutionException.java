// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a PVT sequence motion fails.
 */
public class PvtExecutionException extends MotionLibException {
    private PvtExecutionExceptionData details;

    /**
     * @return Additional data for PvtExecutionException
     */
    public PvtExecutionExceptionData getDetails() {
        return this.details;
    }

    public PvtExecutionException(String message, byte[] customData) {
        super(message);

        this.details = PvtExecutionExceptionData.fromByteArray(customData);
    }

    public PvtExecutionException(String message, PvtExecutionExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
