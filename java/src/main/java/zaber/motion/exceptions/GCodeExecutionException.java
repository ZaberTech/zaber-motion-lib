// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a block of G-Code cannot be executed.
 */
public class GCodeExecutionException extends MotionLibException {
    private GCodeExecutionExceptionData details;

    /**
     * @return Additional data for GCodeExecutionException
     */
    public GCodeExecutionExceptionData getDetails() {
        return this.details;
    }

    public GCodeExecutionException(String message, byte[] customData) {
        super(message);

        this.details = GCodeExecutionExceptionData.fromByteArray(customData);
    }

    public GCodeExecutionException(String message, GCodeExecutionExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
