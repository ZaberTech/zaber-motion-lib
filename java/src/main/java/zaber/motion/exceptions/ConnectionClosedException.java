// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when attempting to communicate on a closed connection.
 */
public class ConnectionClosedException extends MotionLibException {
    public ConnectionClosedException(String message) {
        super(message);
    }
}
