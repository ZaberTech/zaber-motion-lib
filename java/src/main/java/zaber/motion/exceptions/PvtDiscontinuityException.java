// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a PVT sequence encounters discontinuity and interrupts the sequence.
 */
public class PvtDiscontinuityException extends MotionLibException {
    public PvtDiscontinuityException(String message) {
        super(message);
    }
}
