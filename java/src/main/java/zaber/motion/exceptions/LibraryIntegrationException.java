package zaber.motion.exceptions;

public final class LibraryIntegrationException extends RuntimeException {

    public LibraryIntegrationException(String message) {
        super(message);
    }

    public LibraryIntegrationException(Throwable e) {
        super(e);
    }

    public LibraryIntegrationException(String message, Throwable e) {
        super(message, e);
    }
}
