/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for PvtExecutionException.
 */
public final class PvtExecutionExceptionData implements zaber.motion.dto.Message {

    private String errorFlag;

    /**
     * The error flag that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorFlag")
    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    /**
     * The error flag that caused the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("errorFlag")
    public String getErrorFlag() {
        return this.errorFlag;
    }

    /**
     * The error flag that caused the exception.
     */
    public PvtExecutionExceptionData withErrorFlag(String aErrorFlag) {
        this.setErrorFlag(aErrorFlag);
        return this;
    }

    private String reason;

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the exception.
     */
    public PvtExecutionExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    private InvalidPvtPoint[] invalidPoints;

    /**
     * A list of points that cause the error (if applicable).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("invalidPoints")
    public void setInvalidPoints(InvalidPvtPoint[] invalidPoints) {
        this.invalidPoints = invalidPoints;
    }

    /**
     * A list of points that cause the error (if applicable).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("invalidPoints")
    public InvalidPvtPoint[] getInvalidPoints() {
        return this.invalidPoints;
    }

    /**
     * A list of points that cause the error (if applicable).
     */
    public PvtExecutionExceptionData withInvalidPoints(InvalidPvtPoint[] aInvalidPoints) {
        this.setInvalidPoints(aInvalidPoints);
        return this;
    }

    /**
     * Empty constructor.
     */
    public PvtExecutionExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public PvtExecutionExceptionData(
        String errorFlag,
        String reason,
        InvalidPvtPoint[] invalidPoints
    ) {
        this.errorFlag = errorFlag;
        this.reason = reason;
        this.invalidPoints = invalidPoints;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PvtExecutionExceptionData other = (PvtExecutionExceptionData) obj;

        return (
            EqualityUtility.equals(errorFlag, other.errorFlag)
            && EqualityUtility.equals(reason, other.reason)
            && EqualityUtility.equals(invalidPoints, other.invalidPoints)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(errorFlag),
            EqualityUtility.generateHashCode(reason),
            EqualityUtility.generateHashCode(invalidPoints)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PvtExecutionExceptionData { ");
        sb.append("errorFlag: ");
        sb.append(this.errorFlag);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(", ");
        sb.append("invalidPoints: ");
        sb.append(this.invalidPoints);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static PvtExecutionExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, PvtExecutionExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<PvtExecutionExceptionData> PARSER =
        new zaber.motion.dto.Parser<PvtExecutionExceptionData>() {
            @Override
            public PvtExecutionExceptionData fromByteArray(byte[] data) {
                return PvtExecutionExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<PvtExecutionExceptionData> parser() {
        return PARSER;
    }

}
