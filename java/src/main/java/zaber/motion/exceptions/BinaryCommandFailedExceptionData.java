/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for BinaryCommandFailedException.
 */
public final class BinaryCommandFailedExceptionData implements zaber.motion.dto.Message {

    private int responseData;

    /**
     * The response data.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("responseData")
    public void setResponseData(int responseData) {
        this.responseData = responseData;
    }

    /**
     * The response data.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("responseData")
    public int getResponseData() {
        return this.responseData;
    }

    /**
     * The response data.
     */
    public BinaryCommandFailedExceptionData withResponseData(int aResponseData) {
        this.setResponseData(aResponseData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public BinaryCommandFailedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public BinaryCommandFailedExceptionData(
        int responseData
    ) {
        this.responseData = responseData;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BinaryCommandFailedExceptionData other = (BinaryCommandFailedExceptionData) obj;

        return (
            EqualityUtility.equals(responseData, other.responseData)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(responseData)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BinaryCommandFailedExceptionData { ");
        sb.append("responseData: ");
        sb.append(this.responseData);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static BinaryCommandFailedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, BinaryCommandFailedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<BinaryCommandFailedExceptionData> PARSER =
        new zaber.motion.dto.Parser<BinaryCommandFailedExceptionData>() {
            @Override
            public BinaryCommandFailedExceptionData fromByteArray(byte[] data) {
                return BinaryCommandFailedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<BinaryCommandFailedExceptionData> parser() {
        return PARSER;
    }

}
