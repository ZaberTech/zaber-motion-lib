// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a streamed motion fails.
 */
public class StreamExecutionException extends MotionLibException {
    private StreamExecutionExceptionData details;

    /**
     * @return Additional data for StreamExecutionException
     */
    public StreamExecutionExceptionData getDetails() {
        return this.details;
    }

    public StreamExecutionException(String message, byte[] customData) {
        super(message);

        this.details = StreamExecutionExceptionData.fromByteArray(customData);
    }

    public StreamExecutionException(String message, StreamExecutionExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
