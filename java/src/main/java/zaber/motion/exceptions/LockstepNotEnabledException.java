// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when an operation cannot be performed because lockstep motion is not enabled.
 */
public class LockstepNotEnabledException extends MotionLibException {
    public LockstepNotEnabledException(String message) {
        super(message);
    }
}
