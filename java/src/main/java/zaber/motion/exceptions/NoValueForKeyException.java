// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when trying to access a key that has not been set.
 */
public class NoValueForKeyException extends MotionLibException {
    public NoValueForKeyException(String message) {
        super(message);
    }
}
