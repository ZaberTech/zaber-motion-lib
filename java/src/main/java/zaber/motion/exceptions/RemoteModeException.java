// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a command is rejected because the device is in EtherCAT Control (remote) mode.
 */
public class RemoteModeException extends CommandFailedException {
    public RemoteModeException(String message, byte[] customData) {
        super(message, customData);
    }

    public RemoteModeException(String message, CommandFailedExceptionData customData) {
        super(message, customData);
    }
}
