// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when ongoing PVT movement is interrupted by another command or user input.
 */
public class PvtMovementInterruptedException extends MotionLibException {
    private PvtMovementInterruptedExceptionData details;

    /**
     * @return Additional data for PvtMovementInterruptedException
     */
    public PvtMovementInterruptedExceptionData getDetails() {
        return this.details;
    }

    public PvtMovementInterruptedException(String message, byte[] customData) {
        super(message);

        this.details = PvtMovementInterruptedExceptionData.fromByteArray(customData);
    }

    public PvtMovementInterruptedException(String message, PvtMovementInterruptedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
