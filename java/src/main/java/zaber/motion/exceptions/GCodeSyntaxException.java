// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a block of G-Code cannot be parsed.
 */
public class GCodeSyntaxException extends MotionLibException {
    private GCodeSyntaxExceptionData details;

    /**
     * @return Additional data for GCodeSyntaxException
     */
    public GCodeSyntaxExceptionData getDetails() {
        return this.details;
    }

    public GCodeSyntaxException(String message, byte[] customData) {
        super(message);

        this.details = GCodeSyntaxExceptionData.fromByteArray(customData);
    }

    public GCodeSyntaxException(String message, GCodeSyntaxExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
