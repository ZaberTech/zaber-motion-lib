// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when incoming device data cannot be parsed as expected.
 */
public class InvalidDataException extends MotionLibException {
    public InvalidDataException(String message) {
        super(message);
    }
}
