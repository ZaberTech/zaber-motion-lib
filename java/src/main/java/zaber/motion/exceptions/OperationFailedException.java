// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a non-motion device fails to perform a requested operation.
 */
public class OperationFailedException extends MotionLibException {
    private OperationFailedExceptionData details;

    /**
     * @return Additional data for OperationFailedException
     */
    public OperationFailedExceptionData getDetails() {
        return this.details;
    }

    public OperationFailedException(String message, byte[] customData) {
        super(message);

        this.details = OperationFailedExceptionData.fromByteArray(customData);
    }

    public OperationFailedException(String message, OperationFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
