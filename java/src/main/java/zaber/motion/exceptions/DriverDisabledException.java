// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device cannot carry out a movement command because the motor driver is disabled.
 */
public class DriverDisabledException extends CommandFailedException {
    public DriverDisabledException(String message, byte[] customData) {
        super(message, customData);
    }

    public DriverDisabledException(String message, CommandFailedExceptionData customData) {
        super(message, customData);
    }
}
