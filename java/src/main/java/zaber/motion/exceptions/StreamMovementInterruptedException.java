// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when ongoing stream movement is interrupted by another command or user input.
 */
public class StreamMovementInterruptedException extends MotionLibException {
    private StreamMovementInterruptedExceptionData details;

    /**
     * @return Additional data for StreamMovementInterruptedException
     */
    public StreamMovementInterruptedExceptionData getDetails() {
        return this.details;
    }

    public StreamMovementInterruptedException(String message, byte[] customData) {
        super(message);

        this.details = StreamMovementInterruptedExceptionData.fromByteArray(customData);
    }

    public StreamMovementInterruptedException(String message, StreamMovementInterruptedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
