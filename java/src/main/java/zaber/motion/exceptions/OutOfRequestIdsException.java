// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when the library is overwhelmed with too many simultaneous requests.
 */
public class OutOfRequestIdsException extends MotionLibException {
    public OutOfRequestIdsException(String message) {
        super(message);
    }
}
