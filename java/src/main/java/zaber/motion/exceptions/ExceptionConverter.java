// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

import zaber.motion.requests.Errors;

public final class ExceptionConverter {

    private ExceptionConverter() {
    }

    public static MotionLibException convert(Errors error, String message) {
        switch (error) {
        case COMMAND_PREEMPTED:
            return new CommandPreemptedException(message);
        case CONNECTION_CLOSED:
            return new ConnectionClosedException(message);
        case CONNECTION_FAILED:
            return new ConnectionFailedException(message);
        case CONVERSION_FAILED:
            return new ConversionFailedException(message);
        case DEVICE_BUSY:
            return new DeviceBusyException(message);
        case DEVICE_DETECTION_FAILED:
            return new DeviceDetectionFailedException(message);
        case DEVICE_FAILED:
            return new DeviceFailedException(message);
        case DEVICE_NOT_IDENTIFIED:
            return new DeviceNotIdentifiedException(message);
        case INCOMPATIBLE_SHARED_LIBRARY:
            return new IncompatibleSharedLibraryException(message);
        case INTERNAL_ERROR:
            return new InternalErrorException(message);
        case INVALID_ARGUMENT:
            return new InvalidArgumentException(message);
        case INVALID_DATA:
            return new InvalidDataException(message);
        case INVALID_OPERATION:
            return new InvalidOperationException(message);
        case INVALID_PARK_STATE:
            return new InvalidParkStateException(message);
        case INVALID_REQUEST_DATA:
            return new InvalidRequestDataException(message);
        case IO_CHANNEL_OUT_OF_RANGE:
            return new IoChannelOutOfRangeException(message);
        case IO_FAILED:
            return new IoFailedException(message);
        case LOCKSTEP_ENABLED:
            return new LockstepEnabledException(message);
        case LOCKSTEP_NOT_ENABLED:
            return new LockstepNotEnabledException(message);
        case NO_DEVICE_FOUND:
            return new NoDeviceFoundException(message);
        case NO_VALUE_FOR_KEY:
            return new NoValueForKeyException(message);
        case NOT_SUPPORTED:
            return new NotSupportedException(message);
        case OS_FAILED:
            return new OsFailedException(message);
        case OUT_OF_REQUEST_IDS:
            return new OutOfRequestIdsException(message);
        case PVT_DISCONTINUITY:
            return new PvtDiscontinuityException(message);
        case PVT_MODE:
            return new PvtModeException(message);
        case PVT_SETUP_FAILED:
            return new PvtSetupFailedException(message);
        case REQUEST_TIMEOUT:
            return new RequestTimeoutException(message);
        case SERIAL_PORT_BUSY:
            return new SerialPortBusyException(message);
        case SETTING_NOT_FOUND:
            return new SettingNotFoundException(message);
        case STREAM_DISCONTINUITY:
            return new StreamDiscontinuityException(message);
        case STREAM_MODE:
            return new StreamModeException(message);
        case STREAM_SETUP_FAILED:
            return new StreamSetupFailedException(message);
        case TIMEOUT:
            return new TimeoutException(message);
        case TRANSPORT_ALREADY_USED:
            return new TransportAlreadyUsedException(message);
        case UNKNOWN_REQUEST:
            return new UnknownRequestException(message);
        default:
            return new MotionLibException(message);
        }
    }

    public static MotionLibException convert(Errors error, String message, byte[] customData) {
        switch (error) {
        case BAD_COMMAND:
            return new BadCommandException(message, customData);
        case BAD_DATA:
            return new BadDataException(message, customData);
        case BINARY_COMMAND_FAILED:
            return new BinaryCommandFailedException(message, customData);
        case COMMAND_FAILED:
            return new CommandFailedException(message, customData);
        case COMMAND_TOO_LONG:
            return new CommandTooLongException(message, customData);
        case DEVICE_ADDRESS_CONFLICT:
            return new DeviceAddressConflictException(message, customData);
        case DEVICE_DB_FAILED:
            return new DeviceDbFailedException(message, customData);
        case DRIVER_DISABLED:
            return new DriverDisabledException(message, customData);
        case G_CODE_EXECUTION:
            return new GCodeExecutionException(message, customData);
        case G_CODE_SYNTAX:
            return new GCodeSyntaxException(message, customData);
        case INVALID_PACKET:
            return new InvalidPacketException(message, customData);
        case INVALID_RESPONSE:
            return new InvalidResponseException(message, customData);
        case MOVEMENT_FAILED:
            return new MovementFailedException(message, customData);
        case MOVEMENT_INTERRUPTED:
            return new MovementInterruptedException(message, customData);
        case OPERATION_FAILED:
            return new OperationFailedException(message, customData);
        case PVT_EXECUTION:
            return new PvtExecutionException(message, customData);
        case PVT_MOVEMENT_FAILED:
            return new PvtMovementFailedException(message, customData);
        case PVT_MOVEMENT_INTERRUPTED:
            return new PvtMovementInterruptedException(message, customData);
        case REMOTE_MODE:
            return new RemoteModeException(message, customData);
        case SET_DEVICE_STATE_FAILED:
            return new SetDeviceStateFailedException(message, customData);
        case SET_PERIPHERAL_STATE_FAILED:
            return new SetPeripheralStateFailedException(message, customData);
        case STREAM_EXECUTION:
            return new StreamExecutionException(message, customData);
        case STREAM_MOVEMENT_FAILED:
            return new StreamMovementFailedException(message, customData);
        case STREAM_MOVEMENT_INTERRUPTED:
            return new StreamMovementInterruptedException(message, customData);
        default:
            return new MotionLibException(message);
        }
    }

}
