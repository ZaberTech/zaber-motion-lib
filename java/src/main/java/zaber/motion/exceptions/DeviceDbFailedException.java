// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when device information cannot be retrieved from the device database.
 */
public class DeviceDbFailedException extends MotionLibException {
    private DeviceDbFailedExceptionData details;

    /**
     * @return Additional data for DeviceDbFailedException
     */
    public DeviceDbFailedExceptionData getDetails() {
        return this.details;
    }

    public DeviceDbFailedException(String message, byte[] customData) {
        super(message);

        this.details = DeviceDbFailedExceptionData.fromByteArray(customData);
    }

    public DeviceDbFailedException(String message, DeviceDbFailedExceptionData customData) {
        super(message);

        this.details = customData;
    }
}
