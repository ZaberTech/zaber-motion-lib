/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for StreamMovementFailedException.
 */
public final class StreamMovementFailedExceptionData implements zaber.motion.dto.Message {

    private String[] warnings;

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public String[] getWarnings() {
        return this.warnings;
    }

    /**
     * The full list of warnings.
     */
    public StreamMovementFailedExceptionData withWarnings(String[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    private String reason;

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the Exception.
     */
    public StreamMovementFailedExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamMovementFailedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamMovementFailedExceptionData(
        String[] warnings,
        String reason
    ) {
        this.warnings = warnings;
        this.reason = reason;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamMovementFailedExceptionData other = (StreamMovementFailedExceptionData) obj;

        return (
            EqualityUtility.equals(warnings, other.warnings)
            && EqualityUtility.equals(reason, other.reason)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(warnings),
            EqualityUtility.generateHashCode(reason)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamMovementFailedExceptionData { ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamMovementFailedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamMovementFailedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamMovementFailedExceptionData> PARSER =
        new zaber.motion.dto.Parser<StreamMovementFailedExceptionData>() {
            @Override
            public StreamMovementFailedExceptionData fromByteArray(byte[] data) {
                return StreamMovementFailedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamMovementFailedExceptionData> parser() {
        return PARSER;
    }

}
