// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when a device does not respond to a request in time.
 */
public class RequestTimeoutException extends MotionLibException {
    public RequestTimeoutException(String message) {
        super(message);
    }
}
