/* This file is generated. Do not modify by hand. */

package zaber.motion.exceptions;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Contains additional data for MovementFailedException.
 */
public final class MovementFailedExceptionData implements zaber.motion.dto.Message {

    private String[] warnings;

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * The full list of warnings.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public String[] getWarnings() {
        return this.warnings;
    }

    /**
     * The full list of warnings.
     */
    public MovementFailedExceptionData withWarnings(String[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    private String reason;

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * The reason for the Exception.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("reason")
    public String getReason() {
        return this.reason;
    }

    /**
     * The reason for the Exception.
     */
    public MovementFailedExceptionData withReason(String aReason) {
        this.setReason(aReason);
        return this;
    }

    private int device;

    /**
     * The address of the device that performed the failed movement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    /**
     * The address of the device that performed the failed movement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    /**
     * The address of the device that performed the failed movement.
     */
    public MovementFailedExceptionData withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int axis;

    /**
     * The number of the axis that performed the failed movement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public void setAxis(int axis) {
        this.axis = axis;
    }

    /**
     * The number of the axis that performed the failed movement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis")
    public int getAxis() {
        return this.axis;
    }

    /**
     * The number of the axis that performed the failed movement.
     */
    public MovementFailedExceptionData withAxis(int aAxis) {
        this.setAxis(aAxis);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MovementFailedExceptionData() {
    }

    /**
     * Constructor with all properties.
     */
    public MovementFailedExceptionData(
        String[] warnings,
        String reason,
        int device,
        int axis
    ) {
        this.warnings = warnings;
        this.reason = reason;
        this.device = device;
        this.axis = axis;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MovementFailedExceptionData other = (MovementFailedExceptionData) obj;

        return (
            EqualityUtility.equals(warnings, other.warnings)
            && EqualityUtility.equals(reason, other.reason)
            && EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(axis, other.axis)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(warnings),
            EqualityUtility.generateHashCode(reason),
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(axis)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MovementFailedExceptionData { ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(", ");
        sb.append("reason: ");
        sb.append(this.reason);
        sb.append(", ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("axis: ");
        sb.append(this.axis);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MovementFailedExceptionData fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MovementFailedExceptionData.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MovementFailedExceptionData> PARSER =
        new zaber.motion.dto.Parser<MovementFailedExceptionData>() {
            @Override
            public MovementFailedExceptionData fromByteArray(byte[] data) {
                return MovementFailedExceptionData.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MovementFailedExceptionData> parser() {
        return PARSER;
    }

}
