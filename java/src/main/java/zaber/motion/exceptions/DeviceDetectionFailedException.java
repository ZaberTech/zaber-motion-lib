// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.exceptions;

/**
 * Thrown when device detection fails.
 */
public class DeviceDetectionFailedException extends MotionLibException {
    public DeviceDetectionFailedException(String message) {
        super(message);
    }
}
