/* This file is generated. Do not modify by hand. */

package zaber.motion;

import java.util.Objects;

import zaber.motion.dto.Mapper;

/**
 * Represents a numerical value with optional units specified.
 */
public final class Measurement implements zaber.motion.dto.Message {

    private double value;

    /**
     * Value of the measurement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Value of the measurement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    /**
     * Value of the measurement.
     */
    public Measurement withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    /**
     * Optional units of the measurement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * Optional units of the measurement.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * Optional units of the measurement.
     */
    public Measurement withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public Measurement() {
    }

    /**
     * Constructor with all properties.
     */
    public Measurement(
        double value,
        Units unit
    ) {
        this.value = value;
        this.unit = unit;
    }

    /**
     * Constructor with only required properties.
     */
    public Measurement(
        double value
    ) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Measurement other = (Measurement) obj;

        return (
            EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Measurement { ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static Measurement fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, Measurement.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<Measurement> PARSER =
        new zaber.motion.dto.Parser<Measurement>() {
            @Override
            public Measurement fromByteArray(byte[] data) {
                return Measurement.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<Measurement> parser() {
        return PARSER;
    }

}
