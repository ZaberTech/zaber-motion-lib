// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.product;

import zaber.motion.Units;
import zaber.motion.FirmwareVersion;
import zaber.motion.ascii.Axis;
import zaber.motion.ascii.AxisSettings;
import zaber.motion.ascii.AxisStorage;
import zaber.motion.ascii.Response;
import zaber.motion.ascii.SetStateAxisResponse;
import zaber.motion.ascii.Warnings;
import zaber.motion.gateway.Call;
import zaber.motion.Measurement;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Use to drive voltage for a process such as a heater, valve, Peltier device, etc.
 * Requires at least Firmware 7.35.
 */
public class Process {
    private ProcessController controller;

    /**
     * @return Controller for this process.
     */
    public ProcessController getController() {
        return this.controller;
    }

    private int processNumber;

    /**
     * @return The process number identifies the process on the controller.
     */
    public int getProcessNumber() {
        return this.processNumber;
    }

    private Axis axis;

    private AxisSettings settings;

    /**
     * @return Settings and properties of this process.
     */
    public AxisSettings getSettings() {
        return this.settings;
    }

    private AxisStorage storage;

    /**
     * @return Key-value storage of this process.
     */
    public AxisStorage getStorage() {
        return this.storage;
    }

    private Warnings warnings;

    /**
     * @return Warnings and faults of this process.
     */
    public Warnings getWarnings() {
        return this.warnings;
    }

    public Process(
        ProcessController controller, int processNumber) {
        this.controller = controller;
        this.processNumber = processNumber;
        this.axis = new Axis(controller.getDevice(), processNumber);
        this.settings = new AxisSettings(this.axis);
        this.storage = new AxisStorage(this.axis);
        this.warnings = new Warnings(controller.getDevice(), processNumber);
    }

    /**
     * Sets the enabled state of the driver.
     * @param enabled If true (default) enables drive. If false disables.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAsync(
        boolean enabled) {
        zaber.motion.requests.ProcessOn request =
            new zaber.motion.requests.ProcessOn();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setOn(enabled);
        return Call.callAsync("process-controller/enable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the enabled state of the driver.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAsync() {
        return enableAsync(true);
    }

    /**
     * Sets the enabled state of the driver.
     * @param enabled If true (default) enables drive. If false disables.
     */
    public void enable(
        boolean enabled) {
        try {
            enableAsync(enabled).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the enabled state of the driver.
     */
    public void enable() {
        enable(true);
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     * @param duration How long to leave the process on.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onAsync(
        double duration,
        Units unit) {
        zaber.motion.requests.ProcessOn request =
            new zaber.motion.requests.ProcessOn();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setOn(true);
        request.setDuration(duration);
        request.setUnit(unit);
        return Call.callAsync("process-controller/on", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     * @param duration How long to leave the process on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onAsync(
        double duration) {
        return onAsync(duration, Units.NATIVE);
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onAsync() {
        return onAsync(0, Units.NATIVE);
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     * @param duration How long to leave the process on.
     * @param unit Units of time.
     */
    public void on(
        double duration,
        Units unit) {
        try {
            onAsync(duration, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     * @param duration How long to leave the process on.
     */
    public void on(
        double duration) {
        on(duration, Units.NATIVE);
    }

    /**
     * Turns this process on. In manual mode, this supplies voltage; in controlled mode, it starts the control loop.
     */
    public void on() {
        on(0, Units.NATIVE);
    }

    /**
     * Turns this process off.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> offAsync() {
        zaber.motion.requests.ProcessOn request =
            new zaber.motion.requests.ProcessOn();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setOn(false);
        return Call.callAsync("process-controller/on", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Turns this process off.
     */
    public void off() {
        try {
            offAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the control mode of this process.
     * @param mode Mode to set this process to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setModeAsync(
        ProcessControllerMode mode) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setSetting("process.control.mode");
        request.setValue(mode.getValue());
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the control mode of this process.
     * @param mode Mode to set this process to.
     */
    public void setMode(
        ProcessControllerMode mode) {
        try {
            setModeAsync(mode).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the control mode of this process.
     * @return A CompletableFuture that can be completed to get the result:
     * Control mode.
     */
    public CompletableFuture<ProcessControllerMode> getModeAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setSetting("process.control.mode");
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> ProcessControllerMode.values()[(int) r.getValue()]);
    }

    /**
     * Gets the control mode of this process.
     * @return Control mode.
     */
    public ProcessControllerMode getMode() {
        try {
            return getModeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the source used to control this process.
     * @return A CompletableFuture that can be completed to get the result:
     * The source providing feedback for this process.
     */
    public CompletableFuture<ProcessControllerSource> getSourceAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        CompletableFuture<ProcessControllerSource> response = Call.callAsync(
            "process_controller/get_source",
            request,
            ProcessControllerSource.parser());
        return response;
    }

    /**
     * Gets the source used to control this process.
     * @return The source providing feedback for this process.
     */
    public ProcessControllerSource getSource() {
        try {
            return getSourceAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the source used to control this process.
     * @param source Sets the source that should provide feedback for this process.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setSourceAsync(
        ProcessControllerSource source) {
        zaber.motion.requests.SetProcessControllerSource request =
            new zaber.motion.requests.SetProcessControllerSource();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setSource(source);
        return Call.callAsync("process_controller/set_source", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the source used to control this process.
     * @param source Sets the source that should provide feedback for this process.
     */
    public void setSource(
        ProcessControllerSource source) {
        try {
            setSourceAsync(source).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the current value of the source used to control this process.
     * @return A CompletableFuture that can be completed to get the result:
     * The current value of this process's controlling source.
     */
    public CompletableFuture<Measurement> getInputAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        CompletableFuture<Measurement> response = Call.callAsync(
            "process_controller/get_input",
            request,
            Measurement.parser());
        return response;
    }

    /**
     * Gets the current value of the source used to control this process.
     * @return The current value of this process's controlling source.
     */
    public Measurement getInput() {
        try {
            return getInputAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> bridgeAsync() {
        zaber.motion.requests.ProcessOn request =
            new zaber.motion.requests.ProcessOn();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setOn(true);
        return Call.callAsync("process_controller/bridge", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Creates an H-bridge between this process and its neighbor. This method is only callable on axis 1 and 3.
     */
    public void bridge() {
        try {
            bridgeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
     * This method is only callable on axis 1 and 3.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> unbridgeAsync() {
        zaber.motion.requests.ProcessOn request =
            new zaber.motion.requests.ProcessOn();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setOn(false);
        return Call.callAsync("process_controller/bridge", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Breaks the H-bridge between this process and its neighbor, allowing them to be independently controlled.
     * This method is only callable on axis 1 and 3.
     */
    public void unbridge() {
        try {
            unbridgeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Detects if the given process is in bridging mode.
     * @return A CompletableFuture that can be completed to get the result:
     * Whether this process is bridged with its neighbor.
     */
    public CompletableFuture<Boolean> isBridgeAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "process_controller/is_bridge",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Detects if the given process is in bridging mode.
     * @return Whether this process is bridged with its neighbor.
     */
    public boolean isBridge() {
        try {
            return isBridgeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<Response> response = Call.callAsync(
            "interface/generic_command",
            request,
            Response.parser());
        return response;
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors) {
        return genericCommandAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command) {
        return genericCommandAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors) {
        return genericCommand(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this process' underlying axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command) {
        return genericCommand(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.GenericCommandResponseCollection> response = Call.callAsync(
            "interface/generic_command_multi_response",
            request,
            zaber.motion.requests.GenericCommandResponseCollection.parser());
        return response
            .thenApply(r -> r.getResponses());
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponseAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command) {
        return genericCommandMultiResponseAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandMultiResponseAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponse(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this process and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command) {
        return genericCommandMultiResponse(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this process without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setCommand(command);
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to this process without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     */
    public void genericCommandNoResponse(
        String command) {
        try {
            genericCommandNoResponseAsync(command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a serialization of the current process state that can be saved and reapplied.
     * @return A CompletableFuture that can be completed to get the result:
     * A serialization of the current state of the process.
     */
    public CompletableFuture<String> getStateAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_state",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a serialization of the current process state that can be saved and reapplied.
     * @return A serialization of the current state of the process.
     */
    public String getState() {
        try {
            return getStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Applies a saved state to this process.
     * @param state The state object to apply to this process.
     * @return A CompletableFuture that can be completed to get the result:
     * Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public CompletableFuture<SetStateAxisResponse> setStateAsync(
        String state) {
        zaber.motion.requests.SetStateRequest request =
            new zaber.motion.requests.SetStateRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setState(state);
        CompletableFuture<SetStateAxisResponse> response = Call.callAsync(
            "device/set_axis_state",
            request,
            SetStateAxisResponse.parser());
        return response;
    }

    /**
     * Applies a saved state to this process.
     * @param state The state object to apply to this process.
     * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public SetStateAxisResponse setState(
        String state) {
        try {
            return setStateAsync(state).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this process.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this process.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state,
        FirmwareVersion firmwareVersion) {
        zaber.motion.requests.CanSetStateRequest request =
            new zaber.motion.requests.CanSetStateRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setState(state);
        request.setFirmwareVersion(firmwareVersion);
        CompletableFuture<zaber.motion.requests.CanSetStateAxisResponse> response = Call.callAsync(
            "device/can_set_axis_state",
            request,
            zaber.motion.requests.CanSetStateAxisResponse.parser());
        return response
            .thenApply(r -> r.getError());
    }

    /**
     * Checks if a state can be applied to this process.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this process.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state) {
        return canSetStateAsync(state, null);
    }

    /**
     * Checks if a state can be applied to this process.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return An explanation of why this state cannot be set to this process.
     */
    public String canSetState(
        String state,
        FirmwareVersion firmwareVersion) {
        try {
            return canSetStateAsync(state, firmwareVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this process.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return An explanation of why this state cannot be set to this process.
     */
    public String canSetState(
        String state) {
        return canSetState(state, null);
    }

    /**
     * Returns a string that represents the process.
     * @return A string that represents the process.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getController().getDevice().getConnection().getInterfaceId());
        request.setDevice(getController().getDevice().getDeviceAddress());
        request.setAxis(getProcessNumber());
        request.setTypeOverride("Process");
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/axis_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
