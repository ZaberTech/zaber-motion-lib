/* This file is generated. Do not modify by hand. */

package zaber.motion.product;

/**
 * Servo Tuning Parameter Set to target.
 */
public enum ProcessControllerMode {

    MANUAL(0),

    PID(1),

    PID_HEATER(2),

    ON_OFF(3);

    private int value;

    ProcessControllerMode(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static ProcessControllerMode valueOf(int argValue) {
        for (ProcessControllerMode value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
