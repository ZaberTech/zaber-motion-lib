/* This file is generated. Do not modify by hand. */

package zaber.motion.product;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The source used by a process in a closed-loop mode.
 */
public final class ProcessControllerSource implements zaber.motion.dto.Message {

    private ProcessControllerSourceSensor sensor;

    /**
     * The type of input sensor.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("sensor")
    public void setSensor(ProcessControllerSourceSensor sensor) {
        this.sensor = sensor;
    }

    /**
     * The type of input sensor.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("sensor")
    public ProcessControllerSourceSensor getSensor() {
        return this.sensor;
    }

    /**
     * The type of input sensor.
     */
    public ProcessControllerSource withSensor(ProcessControllerSourceSensor aSensor) {
        this.setSensor(aSensor);
        return this;
    }

    private int port;

    /**
     * The specific input to use.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * The specific input to use.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("port")
    public int getPort() {
        return this.port;
    }

    /**
     * The specific input to use.
     */
    public ProcessControllerSource withPort(int aPort) {
        this.setPort(aPort);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ProcessControllerSource() {
    }

    /**
     * Constructor with all properties.
     */
    public ProcessControllerSource(
        ProcessControllerSourceSensor sensor,
        int port
    ) {
        this.sensor = sensor;
        this.port = port;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ProcessControllerSource other = (ProcessControllerSource) obj;

        return (
            EqualityUtility.equals(sensor, other.sensor)
            && EqualityUtility.equals(port, other.port)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(sensor),
            EqualityUtility.generateHashCode(port)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProcessControllerSource { ");
        sb.append("sensor: ");
        sb.append(this.sensor);
        sb.append(", ");
        sb.append("port: ");
        sb.append(this.port);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ProcessControllerSource fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ProcessControllerSource.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ProcessControllerSource> PARSER =
        new zaber.motion.dto.Parser<ProcessControllerSource>() {
            @Override
            public ProcessControllerSource fromByteArray(byte[] data) {
                return ProcessControllerSource.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ProcessControllerSource> parser() {
        return PARSER;
    }

}
