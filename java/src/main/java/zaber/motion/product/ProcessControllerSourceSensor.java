/* This file is generated. Do not modify by hand. */

package zaber.motion.product;

/**
 * Servo Tuning Parameter Set to target.
 */
public enum ProcessControllerSourceSensor {

    THERMISTOR(10),

    ANALOG_INPUT(20);

    private int value;

    ProcessControllerSourceSensor(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static ProcessControllerSourceSensor valueOf(int argValue) {
        for (ProcessControllerSourceSensor value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
