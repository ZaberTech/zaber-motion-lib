// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.product;

import zaber.motion.ArrayUtility;
import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.DeviceType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Use to manage a process controller.
 * Requires at least Firmware 7.35.
 */
public class ProcessController {
    private Device device;

    /**
     * @return The base device of this process controller.
     */
    public Device getDevice() {
        return this.device;
    }

    /**
     * Creates instance of `ProcessController` of the given device.
     * If the device is identified, this constructor will ensure it is a process controller.
     */
    public ProcessController(
        Device device) {
        this.device = device;
        this.verifyIsProcessController();
    }

    /**
     * Detects the process controllers on the connection.
     * @param connection The connection to detect process controllers on.
     * @param identify If the Process Controllers should be identified upon detection.
     * @return A CompletableFuture that can be completed to get the result:
     * A list of all `ProcessController`s on the connection.
     */
    public static CompletableFuture<ProcessController[]> detectAsync(
        Connection connection,
        boolean identify) {
        zaber.motion.requests.DeviceDetectRequest request =
            new zaber.motion.requests.DeviceDetectRequest();
        request.setType(DeviceType.PROCESS_CONTROLLER);
        request.setInterfaceId(connection.getInterfaceId());
        request.setIdentifyDevices(identify);
        CompletableFuture<zaber.motion.requests.DeviceDetectResponse> response = Call.callAsync(
            "device/detect",
            request,
            zaber.motion.requests.DeviceDetectResponse.parser());
        return response
            .thenApply(r -> ArrayUtility.arrayFromInt(ProcessController[]::new, r.getDevices(),
                address -> new ProcessController(connection.getDevice(address))));
    }

    /**
     * Detects the process controllers on the connection.
     * @param connection The connection to detect process controllers on.
     * @return A CompletableFuture that can be completed to get the result:
     * A list of all `ProcessController`s on the connection.
     */
    public static CompletableFuture<ProcessController[]> detectAsync(
        Connection connection) {
        return detectAsync(connection, true);
    }

    /**
     * Detects the process controllers on the connection.
     * @param connection The connection to detect process controllers on.
     * @param identify If the Process Controllers should be identified upon detection.
     * @return A list of all `ProcessController`s on the connection.
     */
    public static ProcessController[] detect(
        Connection connection,
        boolean identify) {
        try {
            return detectAsync(connection, identify).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Detects the process controllers on the connection.
     * @param connection The connection to detect process controllers on.
     * @return A list of all `ProcessController`s on the connection.
     */
    public static ProcessController[] detect(
        Connection connection) {
        return detect(connection, true);
    }

    /**
     * Gets an Process class instance which allows you to control a particular voltage source.
     * Axes are numbered from 1.
     * @param processNumber Number of process to control.
     * @return Process instance.
     */
    public Process getProcess(
        int processNumber) {
        if (processNumber <= 0) {
            throw new IllegalArgumentException("Invalid value; processes are numbered from 1.");
        }
        return new Process(this, processNumber);
    }


    /**
     * Checks if this is a process controller or some other type of device and throws an error if it is not.
     */
    private void verifyIsProcessController() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        Call.callSync("process_controller/verify", request, null);
    }


    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
