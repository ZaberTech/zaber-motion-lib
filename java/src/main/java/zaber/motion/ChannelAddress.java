/* This file is generated. Do not modify by hand. */

package zaber.motion;

import java.util.Objects;

import zaber.motion.dto.Mapper;

/**
 * Holds device address and IO channel number.
 */
public final class ChannelAddress implements zaber.motion.dto.Message {

    private int device;

    /**
     * Device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public void setDevice(int device) {
        this.device = device;
    }

    /**
     * Device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("device")
    public int getDevice() {
        return this.device;
    }

    /**
     * Device address.
     */
    public ChannelAddress withDevice(int aDevice) {
        this.setDevice(aDevice);
        return this;
    }

    private int channel;

    /**
     * IO channel number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("channel")
    public void setChannel(int channel) {
        this.channel = channel;
    }

    /**
     * IO channel number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("channel")
    public int getChannel() {
        return this.channel;
    }

    /**
     * IO channel number.
     */
    public ChannelAddress withChannel(int aChannel) {
        this.setChannel(aChannel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ChannelAddress() {
    }

    /**
     * Constructor with all properties.
     */
    public ChannelAddress(
        int device,
        int channel
    ) {
        this.device = device;
        this.channel = channel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ChannelAddress other = (ChannelAddress) obj;

        return (
            EqualityUtility.equals(device, other.device)
            && EqualityUtility.equals(channel, other.channel)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(device),
            EqualityUtility.generateHashCode(channel)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChannelAddress { ");
        sb.append("device: ");
        sb.append(this.device);
        sb.append(", ");
        sb.append("channel: ");
        sb.append(this.channel);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ChannelAddress fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ChannelAddress.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ChannelAddress> PARSER =
        new zaber.motion.dto.Parser<ChannelAddress>() {
            @Override
            public ChannelAddress fromByteArray(byte[] data) {
                return ChannelAddress.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ChannelAddress> parser() {
        return PARSER;
    }

}
