/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The label of an IO port.
 */
public final class IoPortLabel implements zaber.motion.dto.Message {

    private IoPortType portType;

    /**
     * The type of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public void setPortType(IoPortType portType) {
        this.portType = portType;
    }

    /**
     * The type of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("portType")
    public IoPortType getPortType() {
        return this.portType;
    }

    /**
     * The type of the port.
     */
    public IoPortLabel withPortType(IoPortType aPortType) {
        this.setPortType(aPortType);
        return this;
    }

    private int channelNumber;

    /**
     * The number of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    /**
     * The number of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("channelNumber")
    public int getChannelNumber() {
        return this.channelNumber;
    }

    /**
     * The number of the port.
     */
    public IoPortLabel withChannelNumber(int aChannelNumber) {
        this.setChannelNumber(aChannelNumber);
        return this;
    }

    private String label;

    /**
     * The label of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * The label of the port.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("label")
    public String getLabel() {
        return this.label;
    }

    /**
     * The label of the port.
     */
    public IoPortLabel withLabel(String aLabel) {
        this.setLabel(aLabel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public IoPortLabel() {
    }

    /**
     * Constructor with all properties.
     */
    public IoPortLabel(
        IoPortType portType,
        int channelNumber,
        String label
    ) {
        this.portType = portType;
        this.channelNumber = channelNumber;
        this.label = label;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        IoPortLabel other = (IoPortLabel) obj;

        return (
            EqualityUtility.equals(portType, other.portType)
            && EqualityUtility.equals(channelNumber, other.channelNumber)
            && EqualityUtility.equals(label, other.label)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(portType),
            EqualityUtility.generateHashCode(channelNumber),
            EqualityUtility.generateHashCode(label)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IoPortLabel { ");
        sb.append("portType: ");
        sb.append(this.portType);
        sb.append(", ");
        sb.append("channelNumber: ");
        sb.append(this.channelNumber);
        sb.append(", ");
        sb.append("label: ");
        sb.append(this.label);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static IoPortLabel fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, IoPortLabel.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<IoPortLabel> PARSER =
        new zaber.motion.dto.Parser<IoPortLabel>() {
            @Override
            public IoPortLabel fromByteArray(byte[] data) {
                return IoPortLabel.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<IoPortLabel> parser() {
        return PARSER;
    }

}
