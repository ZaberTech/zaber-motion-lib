/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


/**
 * Represents unit conversion factor for a single dimension.
 */
public final class ConversionFactor implements zaber.motion.dto.Message {

    private String setting;

    /**
     * Setting representing the dimension.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * Setting representing the dimension.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * Setting representing the dimension.
     */
    public ConversionFactor withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private double value;

    /**
     * Value representing 1 native device unit in specified real-word units.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Value representing 1 native device unit in specified real-word units.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    /**
     * Value representing 1 native device unit in specified real-word units.
     */
    public ConversionFactor withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    /**
     * Units of the value.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * Units of the value.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * Units of the value.
     */
    public ConversionFactor withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ConversionFactor() {
    }

    /**
     * Constructor with all properties.
     */
    public ConversionFactor(
        String setting,
        double value,
        Units unit
    ) {
        this.setting = setting;
        this.value = value;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ConversionFactor other = (ConversionFactor) obj;

        return (
            EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ConversionFactor { ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ConversionFactor fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ConversionFactor.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ConversionFactor> PARSER =
        new zaber.motion.dto.Parser<ConversionFactor>() {
            @Override
            public ConversionFactor fromByteArray(byte[] data) {
                return ConversionFactor.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ConversionFactor> parser() {
        return PARSER;
    }

}
