// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing access to various axis settings and properties.
 */
public class AxisSettings {
    private Axis axis;

    public AxisSettings(
        Axis axis) {
        this.axis = axis;
    }

    /**
     * Returns any axis setting or property.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param unit Units of setting.
     * @return A CompletableFuture that can be completed to get the result:
     * Setting value.
     */
    public CompletableFuture<Double> getAsync(
        String setting,
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns any axis setting or property.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @return A CompletableFuture that can be completed to get the result:
     * Setting value.
     */
    public CompletableFuture<Double> getAsync(
        String setting) {
        return getAsync(setting, Units.NATIVE);
    }

    /**
     * Returns any axis setting or property.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param unit Units of setting.
     * @return Setting value.
     */
    public double get(
        String setting,
        Units unit) {
        try {
            return getAsync(setting, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns any axis setting or property.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @return Setting value.
     */
    public double get(
        String setting) {
        return get(setting, Units.NATIVE);
    }

    /**
     * Sets any axis setting.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     * @param unit Units of setting.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAsync(
        String setting,
        double value,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        request.setValue(value);
        request.setUnit(unit);
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets any axis setting.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAsync(
        String setting,
        double value) {
        return setAsync(setting, value, Units.NATIVE);
    }

    /**
     * Sets any axis setting.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     * @param unit Units of setting.
     */
    public void set(
        String setting,
        double value,
        Units unit) {
        try {
            setAsync(setting, value, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets any axis setting.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     */
    public void set(
        String setting,
        double value) {
        set(setting, value, Units.NATIVE);
    }

    /**
     * Returns any axis setting or property as a string.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @return A CompletableFuture that can be completed to get the result:
     * Setting value.
     */
    public CompletableFuture<String> getStringAsync(
        String setting) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_setting_str",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns any axis setting or property as a string.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @return Setting value.
     */
    public String getString(
        String setting) {
        try {
            return getStringAsync(setting).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets any axis setting as a string.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setStringAsync(
        String setting,
        String value) {
        zaber.motion.requests.DeviceSetSettingStrRequest request =
            new zaber.motion.requests.DeviceSetSettingStrRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        request.setValue(value);
        return Call.callAsync("device/set_setting_str", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets any axis setting as a string.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
     * @param setting Name of the setting.
     * @param value Value of the setting.
     */
    public void setString(
        String setting,
        String value) {
        try {
            setStringAsync(setting, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Convert arbitrary setting value to Zaber native units.
     * @param setting Name of the setting.
     * @param value Value of the setting in units specified by following argument.
     * @param unit Units of the value.
     * @return Setting value.
     */
    public double convertToNativeUnits(
        String setting,
        double value,
        Units unit) {
        zaber.motion.requests.DeviceConvertSettingRequest request =
            new zaber.motion.requests.DeviceConvertSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        request.setValue(value);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "device/convert_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Convert arbitrary setting value from Zaber native units.
     * @param setting Name of the setting.
     * @param value Value of the setting in Zaber native units.
     * @param unit Units to convert value to.
     * @return Setting value.
     */
    public double convertFromNativeUnits(
        String setting,
        double value,
        Units unit) {
        zaber.motion.requests.DeviceConvertSettingRequest request =
            new zaber.motion.requests.DeviceConvertSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setFromNative(true);
        request.setSetting(setting);
        request.setValue(value);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "device/convert_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }


    /**
     * Returns the default value of a setting.
     * @param setting Name of the setting.
     * @param unit Units of setting.
     * @return Default setting value.
     */
    public double getDefault(
        String setting,
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "device/get_setting_default",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }

    /**
     * Returns the default value of a setting.
     * @param setting Name of the setting.
     * @return Default setting value.
     */
    public double getDefault(
        String setting) {
        return getDefault(setting, Units.NATIVE);
    }


    /**
     * Returns the default value of a setting as a string.
     * @param setting Name of the setting.
     * @return Default setting value.
     */
    public String getDefaultString(
        String setting) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/get_setting_default_str",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Indicates if given setting can be converted from and to native units.
     * @param setting Name of the setting.
     * @return True if unit conversion can be performed.
     */
    public boolean canConvertNativeUnits(
        String setting) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setSetting(setting);
        zaber.motion.requests.BoolResponse response = Call.callSync(
            "device/can_convert_setting",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response.getValue();
    }


    /**
     * Overrides default unit conversions.
     * Conversion factors are specified by setting names representing underlying dimensions.
     * Requires at least Firmware 7.30.
     * @param conversions Factors of all conversions to override.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setCustomUnitConversionsAsync(
        ConversionFactor[] conversions) {
        zaber.motion.requests.DeviceSetUnitConversionsRequest request =
            new zaber.motion.requests.DeviceSetUnitConversionsRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setConversions(conversions);
        return Call.callAsync("device/set_unit_conversions", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Overrides default unit conversions.
     * Conversion factors are specified by setting names representing underlying dimensions.
     * Requires at least Firmware 7.30.
     * @param conversions Factors of all conversions to override.
     */
    public void setCustomUnitConversions(
        ConversionFactor[] conversions) {
        try {
            setCustomUnitConversionsAsync(conversions).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets many setting values in as few requests as possible.
     * @param axisSettings The settings to read.
     * @return A CompletableFuture that can be completed to get the result:
     * The setting values read.
     */
    public CompletableFuture<GetAxisSettingResult[]> getManyAsync(
        GetAxisSetting... axisSettings) {
        zaber.motion.requests.DeviceMultiGetSettingRequest request =
            new zaber.motion.requests.DeviceMultiGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setAxisSettings(axisSettings);
        CompletableFuture<zaber.motion.requests.GetAxisSettingResults> response = Call.callAsync(
            "device/get_many_settings",
            request,
            zaber.motion.requests.GetAxisSettingResults.parser());
        return response
            .thenApply(r -> r.getResults());
    }

    /**
     * Gets many setting values in as few requests as possible.
     * @param axisSettings The settings to read.
     * @return The setting values read.
     */
    public GetAxisSettingResult[] getMany(
        GetAxisSetting... axisSettings) {
        try {
            return getManyAsync(axisSettings).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets many setting values in the same tick, ensuring their values are synchronized.
     * Requires at least Firmware 7.35.
     * @param axisSettings The settings to read.
     * @return A CompletableFuture that can be completed to get the result:
     * The setting values read.
     */
    public CompletableFuture<GetAxisSettingResult[]> getSynchronizedAsync(
        GetAxisSetting... axisSettings) {
        zaber.motion.requests.DeviceMultiGetSettingRequest request =
            new zaber.motion.requests.DeviceMultiGetSettingRequest();
        request.setInterfaceId(this.axis.getDevice().getConnection().getInterfaceId());
        request.setDevice(this.axis.getDevice().getDeviceAddress());
        request.setAxis(this.axis.getAxisNumber());
        request.setAxisSettings(axisSettings);
        CompletableFuture<zaber.motion.requests.GetAxisSettingResults> response = Call.callAsync(
            "device/get_sync_settings",
            request,
            zaber.motion.requests.GetAxisSettingResults.parser());
        return response
            .thenApply(r -> r.getResults());
    }

    /**
     * Gets many setting values in the same tick, ensuring their values are synchronized.
     * Requires at least Firmware 7.35.
     * @param axisSettings The settings to read.
     * @return The setting values read.
     */
    public GetAxisSettingResult[] getSynchronized(
        GetAxisSetting... axisSettings) {
        try {
            return getSynchronizedAsync(axisSettings).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
