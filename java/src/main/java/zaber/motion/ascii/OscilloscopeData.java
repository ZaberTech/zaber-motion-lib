// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.gateway.Call;

/**
 * Contains a block of contiguous recorded data for one channel of the device's oscilloscope.
 */
public class OscilloscopeData {
    private int dataId;

    /**
     * @return Unique ID for this block of recorded data.
     */
    public int getDataId() {
        return this.dataId;
    }


    /**
     * @return Indicates whether the data came from a setting or an I/O pin.
     */
    public OscilloscopeDataSource getDataSource() {
        return this.retrieveProperties().getDataSource();
    }


    /**
     * @return The name of the recorded setting.
     */
    public String getSetting() {
        return this.retrieveProperties().getSetting();
    }


    /**
     * @return The number of the axis the data was recorded from, or 0 for the controller.
     */
    public int getAxisNumber() {
        return this.retrieveProperties().getAxisNumber();
    }


    /**
     * @return Which kind of I/O port data was recorded from.
     */
    public IoPortType getIoType() {
        return this.retrieveProperties().getIoType();
    }


    /**
     * @return Which I/O pin within the port was recorded.
     */
    public int getIoChannel() {
        return this.retrieveProperties().getIoChannel();
    }

    public OscilloscopeData(
        int dataId) {
        this.dataId = dataId;
    }

    /**
     * Get the sample interval that this data was recorded with.
     * @param unit Unit of measure to represent the timebase in.
     * @return The timebase setting at the time the data was recorded.
     */
    public double getTimebase(
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetRequest request =
            new zaber.motion.requests.OscilloscopeDataGetRequest();
        request.setDataId(getDataId());
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "oscilloscopedata/get_timebase",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }

    /**
     * Get the sample interval that this data was recorded with.
     * @return The timebase setting at the time the data was recorded.
     */
    public double getTimebase() {
        return getTimebase(Units.NATIVE);
    }


    /**
     * Get the sampling frequency that this data was recorded with.
     * @param unit Unit of measure to represent the frequency in.
     * @return The frequency (inverse of the timebase setting) at the time the data was recorded.
     */
    public double getFrequency(
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetRequest request =
            new zaber.motion.requests.OscilloscopeDataGetRequest();
        request.setDataId(getDataId());
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "oscilloscopedata/get_frequency",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }

    /**
     * Get the sampling frequency that this data was recorded with.
     * @return The frequency (inverse of the timebase setting) at the time the data was recorded.
     */
    public double getFrequency() {
        return getFrequency(Units.NATIVE);
    }


    /**
     * Get the user-specified time period between receipt of the start command and the first data point.
     * Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.
     * @param unit Unit of measure to represent the delay in.
     * @return The delay setting at the time the data was recorded.
     */
    public double getDelay(
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetRequest request =
            new zaber.motion.requests.OscilloscopeDataGetRequest();
        request.setDataId(getDataId());
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "oscilloscopedata/get_delay",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }

    /**
     * Get the user-specified time period between receipt of the start command and the first data point.
     * Under some circumstances, the actual delay may be different - call GetSampleTime(0) to get the effective delay.
     * @return The delay setting at the time the data was recorded.
     */
    public double getDelay() {
        return getDelay(Units.NATIVE);
    }


    /**
     * Calculate the time a sample was recorded, relative to when the recording was triggered.
     * @param index 0-based index of the sample to calculate the time of.
     * @param unit Unit of measure to represent the calculated time in.
     * @return The calculated time offset of the data sample at the given index.
     */
    public double getSampleTime(
        int index,
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetSampleTimeRequest request =
            new zaber.motion.requests.OscilloscopeDataGetSampleTimeRequest();
        request.setDataId(getDataId());
        request.setIndex(index);
        request.setUnit(unit);
        zaber.motion.requests.DoubleResponse response = Call.callSync(
            "oscilloscopedata/get_sample_time",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response.getValue();
    }

    /**
     * Calculate the time a sample was recorded, relative to when the recording was triggered.
     * @param index 0-based index of the sample to calculate the time of.
     * @return The calculated time offset of the data sample at the given index.
     */
    public double getSampleTime(
        int index) {
        return getSampleTime(index, Units.NATIVE);
    }


    /**
     * Calculate the time for all samples, relative to when the recording was triggered.
     * @param unit Unit of measure to represent the calculated time in.
     * @return The calculated time offsets of all data samples.
     */
    public double[] getSampleTimes(
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetSampleTimeRequest request =
            new zaber.motion.requests.OscilloscopeDataGetSampleTimeRequest();
        request.setDataId(getDataId());
        request.setUnit(unit);
        zaber.motion.requests.DoubleArrayResponse response = Call.callSync(
            "oscilloscopedata/get_sample_times",
            request,
            zaber.motion.requests.DoubleArrayResponse.parser());
        return response.getValues();
    }

    /**
     * Calculate the time for all samples, relative to when the recording was triggered.
     * @return The calculated time offsets of all data samples.
     */
    public double[] getSampleTimes() {
        return getSampleTimes(Units.NATIVE);
    }


    /**
     * Get the recorded data as an array of doubles, with optional unit conversion.
     * Note that not all quantities can be unit converted.
     * For example, digital I/O channels and pure numbers such as device mode settings have no units.
     * @param unit Unit of measure to convert the data to.
     * @return The recorded data for one oscilloscope channel, converted to the units specified.
     */
    public double[] getData(
        Units unit) {
        zaber.motion.requests.OscilloscopeDataGetRequest request =
            new zaber.motion.requests.OscilloscopeDataGetRequest();
        request.setDataId(getDataId());
        request.setUnit(unit);
        zaber.motion.requests.OscilloscopeDataGetSamplesResponse response = Call.callSync(
            "oscilloscopedata/get_samples",
            request,
            zaber.motion.requests.OscilloscopeDataGetSamplesResponse.parser());
        return response.getData();
    }

    /**
     * Get the recorded data as an array of doubles, with optional unit conversion.
     * Note that not all quantities can be unit converted.
     * For example, digital I/O channels and pure numbers such as device mode settings have no units.
     * @return The recorded data for one oscilloscope channel, converted to the units specified.
     */
    public double[] getData() {
        return getData(Units.NATIVE);
    }


    /**
     * Releases native resources of an oscilloscope data buffer.
     * @param dataId The ID of the data buffer to delete.
     */
    private static void free(
        int dataId) {
        zaber.motion.requests.OscilloscopeDataIdentifier request =
            new zaber.motion.requests.OscilloscopeDataIdentifier();
        request.setDataId(dataId);
        Call.callSync("oscilloscopedata/free", request, null);
    }


    /**
     * Returns recording properties.
     * @return Capture properties.
     */
    private OscilloscopeCaptureProperties retrieveProperties() {
        zaber.motion.requests.OscilloscopeDataIdentifier request =
            new zaber.motion.requests.OscilloscopeDataIdentifier();
        request.setDataId(getDataId());
        OscilloscopeCaptureProperties response = Call.callSync(
            "oscilloscopedata/get_properties",
            request,
            OscilloscopeCaptureProperties.parser());
        return response;
    }



    /**
    * Finalize is used over Cleaner due compatibility with Java 8.
    */
    @Override
    @SuppressWarnings("deprecation")
    public void finalize() {
        OscilloscopeData.free(this.dataId);
    }
}
