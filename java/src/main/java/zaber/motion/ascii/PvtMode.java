/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Mode of a PVT sequence.
 */
public enum PvtMode {

    DISABLED(0),

    STORE(1),

    LIVE(2);

    private int value;

    PvtMode(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static PvtMode valueOf(int argValue) {
        for (PvtMode value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
