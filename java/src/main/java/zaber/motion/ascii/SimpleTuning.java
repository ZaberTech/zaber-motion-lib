/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The masses and parameters last used by simple tuning.
 */
public final class SimpleTuning implements zaber.motion.dto.Message {

    private boolean isUsed;

    /**
     * Whether the tuning returned is currently in use by this paramset,
     * or if it has been overwritten by a later change.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isUsed")
    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }

    /**
     * Whether the tuning returned is currently in use by this paramset,
     * or if it has been overwritten by a later change.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isUsed")
    public boolean getIsUsed() {
        return this.isUsed;
    }

    /**
     * Whether the tuning returned is currently in use by this paramset,
     * or if it has been overwritten by a later change.
     */
    public SimpleTuning withIsUsed(boolean aIsUsed) {
        this.setIsUsed(aIsUsed);
        return this;
    }

    private Double carriageMass;

    /**
     * The mass of the carriage in kg.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("carriageMass")
    public void setCarriageMass(Double carriageMass) {
        this.carriageMass = carriageMass;
    }

    /**
     * The mass of the carriage in kg.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("carriageMass")
    public Double getCarriageMass() {
        return this.carriageMass;
    }

    /**
     * The mass of the carriage in kg.
     */
    public SimpleTuning withCarriageMass(Double aCarriageMass) {
        this.setCarriageMass(aCarriageMass);
        return this;
    }

    private double loadMass;

    /**
     * The mass of the load in kg, excluding the mass of the carriage.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("loadMass")
    public void setLoadMass(double loadMass) {
        this.loadMass = loadMass;
    }

    /**
     * The mass of the load in kg, excluding the mass of the carriage.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("loadMass")
    public double getLoadMass() {
        return this.loadMass;
    }

    /**
     * The mass of the load in kg, excluding the mass of the carriage.
     */
    public SimpleTuning withLoadMass(double aLoadMass) {
        this.setLoadMass(aLoadMass);
        return this;
    }

    private ServoTuningParam[] tuningParams;

    /**
     * The parameters used by simple tuning.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public void setTuningParams(ServoTuningParam[] tuningParams) {
        this.tuningParams = tuningParams;
    }

    /**
     * The parameters used by simple tuning.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("tuningParams")
    public ServoTuningParam[] getTuningParams() {
        return this.tuningParams;
    }

    /**
     * The parameters used by simple tuning.
     */
    public SimpleTuning withTuningParams(ServoTuningParam[] aTuningParams) {
        this.setTuningParams(aTuningParams);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SimpleTuning() {
    }

    /**
     * Constructor with all properties.
     */
    public SimpleTuning(
        boolean isUsed,
        double loadMass,
        ServoTuningParam[] tuningParams,
        Double carriageMass
    ) {
        this.isUsed = isUsed;
        this.loadMass = loadMass;
        this.tuningParams = tuningParams;
        this.carriageMass = carriageMass;
    }

    /**
     * Constructor with only required properties.
     */
    public SimpleTuning(
        boolean isUsed,
        double loadMass,
        ServoTuningParam[] tuningParams
    ) {
        this.isUsed = isUsed;
        this.loadMass = loadMass;
        this.tuningParams = tuningParams;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SimpleTuning other = (SimpleTuning) obj;

        return (
            EqualityUtility.equals(isUsed, other.isUsed)
            && EqualityUtility.equals(carriageMass, other.carriageMass)
            && EqualityUtility.equals(loadMass, other.loadMass)
            && EqualityUtility.equals(tuningParams, other.tuningParams)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(isUsed),
            EqualityUtility.generateHashCode(carriageMass),
            EqualityUtility.generateHashCode(loadMass),
            EqualityUtility.generateHashCode(tuningParams)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SimpleTuning { ");
        sb.append("isUsed: ");
        sb.append(this.isUsed);
        sb.append(", ");
        sb.append("carriageMass: ");
        sb.append(this.carriageMass);
        sb.append(", ");
        sb.append("loadMass: ");
        sb.append(this.loadMass);
        sb.append(", ");
        sb.append("tuningParams: ");
        sb.append(this.tuningParams);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SimpleTuning fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SimpleTuning.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SimpleTuning> PARSER =
        new zaber.motion.dto.Parser<SimpleTuning>() {
            @Override
            public SimpleTuning fromByteArray(byte[] data) {
                return SimpleTuning.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SimpleTuning> parser() {
        return PARSER;
    }

}
