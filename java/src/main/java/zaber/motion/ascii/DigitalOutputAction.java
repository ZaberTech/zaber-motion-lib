/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Action type for digital output.
 */
public enum DigitalOutputAction {

    OFF(0),

    ON(1),

    TOGGLE(2),

    KEEP(3);

    private int value;

    DigitalOutputAction(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static DigitalOutputAction valueOf(int argValue) {
        for (DigitalOutputAction value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
