/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Kind of I/O pin to use.
 */
public enum IoPortType {

    NONE(0),

    ANALOG_INPUT(1),

    ANALOG_OUTPUT(2),

    DIGITAL_INPUT(3),

    DIGITAL_OUTPUT(4);

    private int value;

    IoPortType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static IoPortType valueOf(int argValue) {
        for (IoPortType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
