// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Connection transport backend allowing to carry Zaber ASCII protocol over arbitrary protocols.
 * Can only be used with a single connection.
 */
public class Transport implements AutoCloseable {
    private int transportId;

    /**
     * @return The transport ID identifies this transport instance with the underlying library.
     */
    public int getTransportId() {
        return this.transportId;
    }

    public Transport(
        int transportId) {
        this.transportId = transportId;
    }

    /**
     * Creates new instance allowing to read/write messages from/to a single connection.
     * @return New instance of transport.
     */
    public static Transport open() {
        zaber.motion.requests.EmptyRequest request =
            new zaber.motion.requests.EmptyRequest();
        zaber.motion.requests.CustomInterfaceOpenResponse response = Call.callSync(
            "custom/interface/open",
            request,
            zaber.motion.requests.CustomInterfaceOpenResponse.parser());
        return new Transport(response.getTransportId());
    }


    /**
     * Closes the transport.
     * Also closes the connection using the transport.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> closeAsync() {
        zaber.motion.requests.CustomInterfaceCloseRequest request =
            new zaber.motion.requests.CustomInterfaceCloseRequest();
        request.setTransportId(getTransportId());
        return Call.callAsync("custom/interface/close", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Closes the transport.
     * Also closes the connection using the transport.
     */
    public void close() {
        try {
            closeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Closes the transport with error.
     * Also closes the connection using the transport propagating the error.
     * @param errorMessage Error to be propagated.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> closeWithErrorAsync(
        String errorMessage) {
        zaber.motion.requests.CustomInterfaceCloseRequest request =
            new zaber.motion.requests.CustomInterfaceCloseRequest();
        request.setTransportId(getTransportId());
        request.setErrorMessage(errorMessage);
        return Call.callAsync("custom/interface/close", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Closes the transport with error.
     * Also closes the connection using the transport propagating the error.
     * @param errorMessage Error to be propagated.
     */
    public void closeWithError(
        String errorMessage) {
        try {
            closeWithErrorAsync(errorMessage).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Writes a single message to the connection.
     * The message will be processed as a reply from the device.
     * @param message Single message of Zaber ASCII protocol.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> writeAsync(
        String message) {
        zaber.motion.requests.CustomInterfaceWriteRequest request =
            new zaber.motion.requests.CustomInterfaceWriteRequest();
        request.setTransportId(getTransportId());
        request.setMessage(message);
        return Call.callAsync("custom/interface/write", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Writes a single message to the connection.
     * The message will be processed as a reply from the device.
     * @param message Single message of Zaber ASCII protocol.
     */
    public void write(
        String message) {
        try {
            writeAsync(message).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Reads a single message generated by the connection.
     * The message is a request for the device.
     * Read should be called continuously in a loop to ensure all generated messages are processed.
     * Subsequent read call confirms that previous message was delivered to the device.
     * @return A CompletableFuture that can be completed to get the result:
     * Message generated by the connection.
     */
    public CompletableFuture<String> readAsync() {
        zaber.motion.requests.CustomInterfaceReadRequest request =
            new zaber.motion.requests.CustomInterfaceReadRequest();
        request.setTransportId(getTransportId());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "custom/interface/read",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Reads a single message generated by the connection.
     * The message is a request for the device.
     * Read should be called continuously in a loop to ensure all generated messages are processed.
     * Subsequent read call confirms that previous message was delivered to the device.
     * @return Message generated by the connection.
     */
    public String read() {
        try {
            return readAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
