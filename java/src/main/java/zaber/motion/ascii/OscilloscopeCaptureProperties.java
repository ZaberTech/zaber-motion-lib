/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The public properties of one channel of recorded oscilloscope data.
 */
public final class OscilloscopeCaptureProperties implements zaber.motion.dto.Message {

    private OscilloscopeDataSource dataSource;

    /**
     * Indicates whether the data came from a setting or an I/O pin.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("dataSource")
    public void setDataSource(OscilloscopeDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Indicates whether the data came from a setting or an I/O pin.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("dataSource")
    public OscilloscopeDataSource getDataSource() {
        return this.dataSource;
    }

    /**
     * Indicates whether the data came from a setting or an I/O pin.
     */
    public OscilloscopeCaptureProperties withDataSource(OscilloscopeDataSource aDataSource) {
        this.setDataSource(aDataSource);
        return this;
    }

    private String setting;

    /**
     * The name of the recorded setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * The name of the recorded setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * The name of the recorded setting.
     */
    public OscilloscopeCaptureProperties withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private int axisNumber;

    /**
     * The number of the axis the data was recorded from, or 0 for the controller.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * The number of the axis the data was recorded from, or 0 for the controller.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * The number of the axis the data was recorded from, or 0 for the controller.
     */
    public OscilloscopeCaptureProperties withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private IoPortType ioType;

    /**
     * Which kind of I/O port data was recorded from.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("ioType")
    public void setIoType(IoPortType ioType) {
        this.ioType = ioType;
    }

    /**
     * Which kind of I/O port data was recorded from.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("ioType")
    public IoPortType getIoType() {
        return this.ioType;
    }

    /**
     * Which kind of I/O port data was recorded from.
     */
    public OscilloscopeCaptureProperties withIoType(IoPortType aIoType) {
        this.setIoType(aIoType);
        return this;
    }

    private int ioChannel;

    /**
     * Which I/O pin within the port was recorded.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("ioChannel")
    public void setIoChannel(int ioChannel) {
        this.ioChannel = ioChannel;
    }

    /**
     * Which I/O pin within the port was recorded.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("ioChannel")
    public int getIoChannel() {
        return this.ioChannel;
    }

    /**
     * Which I/O pin within the port was recorded.
     */
    public OscilloscopeCaptureProperties withIoChannel(int aIoChannel) {
        this.setIoChannel(aIoChannel);
        return this;
    }

    /**
     * Empty constructor.
     */
    public OscilloscopeCaptureProperties() {
    }

    /**
     * Constructor with all properties.
     */
    public OscilloscopeCaptureProperties(
        OscilloscopeDataSource dataSource,
        String setting,
        int axisNumber,
        IoPortType ioType,
        int ioChannel
    ) {
        this.dataSource = dataSource;
        this.setting = setting;
        this.axisNumber = axisNumber;
        this.ioType = ioType;
        this.ioChannel = ioChannel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        OscilloscopeCaptureProperties other = (OscilloscopeCaptureProperties) obj;

        return (
            EqualityUtility.equals(dataSource, other.dataSource)
            && EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(ioType, other.ioType)
            && EqualityUtility.equals(ioChannel, other.ioChannel)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(dataSource),
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(ioType),
            EqualityUtility.generateHashCode(ioChannel)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OscilloscopeCaptureProperties { ");
        sb.append("dataSource: ");
        sb.append(this.dataSource);
        sb.append(", ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("ioType: ");
        sb.append(this.ioType);
        sb.append(", ");
        sb.append("ioChannel: ");
        sb.append(this.ioChannel);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static OscilloscopeCaptureProperties fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, OscilloscopeCaptureProperties.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<OscilloscopeCaptureProperties> PARSER =
        new zaber.motion.dto.Parser<OscilloscopeCaptureProperties>() {
            @Override
            public OscilloscopeCaptureProperties fromByteArray(byte[] data) {
                return OscilloscopeCaptureProperties.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<OscilloscopeCaptureProperties> parser() {
        return PARSER;
    }

}
