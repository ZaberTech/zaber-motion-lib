/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Denotes type of the response message.
 * For more information refer to:
 * [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format).
 */
public enum MessageType {

    REPLY(0),

    INFO(1),

    ALERT(2);

    private int value;

    MessageType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static MessageType valueOf(int argValue) {
        for (MessageType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
