/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Servo Tuning Parameter Set to target.
 */
public enum ServoTuningParamset {

    LIVE(0),

    P_1(1),

    P_2(2),

    P_3(3),

    P_4(4),

    P_5(5),

    P_6(6),

    P_7(7),

    P_8(8),

    P_9(9),

    STAGING(10),

    DEFAULT(11);

    private int value;

    ServoTuningParamset(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static ServoTuningParamset valueOf(int argValue) {
        for (ServoTuningParamset value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
