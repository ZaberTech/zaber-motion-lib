/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * An object containing any setup issues that will prevent setting a state to a given device.
 */
public final class CanSetStateDeviceResponse implements zaber.motion.dto.Message {

    private String error;

    /**
     * The error blocking applying this state to the given device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    /**
     * The error blocking applying this state to the given device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("error")
    public String getError() {
        return this.error;
    }

    /**
     * The error blocking applying this state to the given device.
     */
    public CanSetStateDeviceResponse withError(String aError) {
        this.setError(aError);
        return this;
    }

    private CanSetStateAxisResponse[] axisErrors;

    /**
     * A list of errors that block setting state of device's axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisErrors")
    public void setAxisErrors(CanSetStateAxisResponse[] axisErrors) {
        this.axisErrors = axisErrors;
    }

    /**
     * A list of errors that block setting state of device's axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisErrors")
    public CanSetStateAxisResponse[] getAxisErrors() {
        return this.axisErrors;
    }

    /**
     * A list of errors that block setting state of device's axes.
     */
    public CanSetStateDeviceResponse withAxisErrors(CanSetStateAxisResponse[] aAxisErrors) {
        this.setAxisErrors(aAxisErrors);
        return this;
    }

    /**
     * Empty constructor.
     */
    public CanSetStateDeviceResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public CanSetStateDeviceResponse(
        CanSetStateAxisResponse[] axisErrors,
        String error
    ) {
        this.axisErrors = axisErrors;
        this.error = error;
    }

    /**
     * Constructor with only required properties.
     */
    public CanSetStateDeviceResponse(
        CanSetStateAxisResponse[] axisErrors
    ) {
        this.axisErrors = axisErrors;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        CanSetStateDeviceResponse other = (CanSetStateDeviceResponse) obj;

        return (
            EqualityUtility.equals(error, other.error)
            && EqualityUtility.equals(axisErrors, other.axisErrors)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(error),
            EqualityUtility.generateHashCode(axisErrors)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CanSetStateDeviceResponse { ");
        sb.append("error: ");
        sb.append(this.error);
        sb.append(", ");
        sb.append("axisErrors: ");
        sb.append(this.axisErrors);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static CanSetStateDeviceResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, CanSetStateDeviceResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<CanSetStateDeviceResponse> PARSER =
        new zaber.motion.dto.Parser<CanSetStateDeviceResponse>() {
            @Override
            public CanSetStateDeviceResponse fromByteArray(byte[] data) {
                return CanSetStateDeviceResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<CanSetStateDeviceResponse> parser() {
        return PARSER;
    }

}
