/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Response message from the device.
 */
public final class Response implements zaber.motion.dto.Message {

    private int deviceAddress;

    /**
     * Number of the device that sent the message.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public void setDeviceAddress(int deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * Number of the device that sent the message.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public int getDeviceAddress() {
        return this.deviceAddress;
    }

    /**
     * Number of the device that sent the message.
     */
    public Response withDeviceAddress(int aDeviceAddress) {
        this.setDeviceAddress(aDeviceAddress);
        return this;
    }

    private int axisNumber;

    /**
     * Number of the axis which the response applies to. Zero denotes device scope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * Number of the axis which the response applies to. Zero denotes device scope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * Number of the axis which the response applies to. Zero denotes device scope.
     */
    public Response withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private String replyFlag;

    /**
     * The reply flag indicates if the request was accepted (OK) or rejected (RJ).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("replyFlag")
    public void setReplyFlag(String replyFlag) {
        this.replyFlag = replyFlag;
    }

    /**
     * The reply flag indicates if the request was accepted (OK) or rejected (RJ).
     */
    @com.fasterxml.jackson.annotation.JsonProperty("replyFlag")
    public String getReplyFlag() {
        return this.replyFlag;
    }

    /**
     * The reply flag indicates if the request was accepted (OK) or rejected (RJ).
     */
    public Response withReplyFlag(String aReplyFlag) {
        this.setReplyFlag(aReplyFlag);
        return this;
    }

    private String status;

    /**
     * The device status contains BUSY when the axis is moving and IDLE otherwise.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * The device status contains BUSY when the axis is moving and IDLE otherwise.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("status")
    public String getStatus() {
        return this.status;
    }

    /**
     * The device status contains BUSY when the axis is moving and IDLE otherwise.
     */
    public Response withStatus(String aStatus) {
        this.setStatus(aStatus);
        return this;
    }

    private String warningFlag;

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warningFlag")
    public void setWarningFlag(String warningFlag) {
        this.warningFlag = warningFlag;
    }

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warningFlag")
    public String getWarningFlag() {
        return this.warningFlag;
    }

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    public Response withWarningFlag(String aWarningFlag) {
        this.setWarningFlag(aWarningFlag);
        return this;
    }

    private String data;

    /**
     * Response data which varies depending on the request.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Response data which varies depending on the request.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public String getData() {
        return this.data;
    }

    /**
     * Response data which varies depending on the request.
     */
    public Response withData(String aData) {
        this.setData(aData);
        return this;
    }

    private MessageType messageType;

    /**
     * Type of the reply received.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("messageType")
    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    /**
     * Type of the reply received.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("messageType")
    public MessageType getMessageType() {
        return this.messageType;
    }

    /**
     * Type of the reply received.
     */
    public Response withMessageType(MessageType aMessageType) {
        this.setMessageType(aMessageType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public Response() {
    }

    /**
     * Constructor with all properties.
     */
    public Response(
        int deviceAddress,
        int axisNumber,
        String replyFlag,
        String status,
        String warningFlag,
        String data,
        MessageType messageType
    ) {
        this.deviceAddress = deviceAddress;
        this.axisNumber = axisNumber;
        this.replyFlag = replyFlag;
        this.status = status;
        this.warningFlag = warningFlag;
        this.data = data;
        this.messageType = messageType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Response other = (Response) obj;

        return (
            EqualityUtility.equals(deviceAddress, other.deviceAddress)
            && EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(replyFlag, other.replyFlag)
            && EqualityUtility.equals(status, other.status)
            && EqualityUtility.equals(warningFlag, other.warningFlag)
            && EqualityUtility.equals(data, other.data)
            && EqualityUtility.equals(messageType, other.messageType)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceAddress),
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(replyFlag),
            EqualityUtility.generateHashCode(status),
            EqualityUtility.generateHashCode(warningFlag),
            EqualityUtility.generateHashCode(data),
            EqualityUtility.generateHashCode(messageType)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Response { ");
        sb.append("deviceAddress: ");
        sb.append(this.deviceAddress);
        sb.append(", ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("replyFlag: ");
        sb.append(this.replyFlag);
        sb.append(", ");
        sb.append("status: ");
        sb.append(this.status);
        sb.append(", ");
        sb.append("warningFlag: ");
        sb.append(this.warningFlag);
        sb.append(", ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(", ");
        sb.append("messageType: ");
        sb.append(this.messageType);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static Response fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, Response.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<Response> PARSER =
        new zaber.motion.dto.Parser<Response>() {
            @Override
            public Response fromByteArray(byte[] data) {
                return Response.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<Response> parser() {
        return PARSER;
    }

}
