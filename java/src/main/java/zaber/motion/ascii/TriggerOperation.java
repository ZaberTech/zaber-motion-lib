/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Operation for trigger action.
 */
public enum TriggerOperation {

    SET_TO(0),

    INCREMENT_BY(1),

    DECREMENT_BY(2);

    private int value;

    TriggerOperation(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static TriggerOperation valueOf(int argValue) {
        for (TriggerOperation value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
