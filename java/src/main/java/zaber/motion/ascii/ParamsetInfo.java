/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The raw parameters currently saved to a given paramset.
 */
public final class ParamsetInfo implements zaber.motion.dto.Message {

    private String type;

    /**
     * The tuning algorithm used for this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The tuning algorithm used for this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public String getType() {
        return this.type;
    }

    /**
     * The tuning algorithm used for this axis.
     */
    public ParamsetInfo withType(String aType) {
        this.setType(aType);
        return this;
    }

    private int version;

    /**
     * The version of the tuning algorithm used for this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * The version of the tuning algorithm used for this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public int getVersion() {
        return this.version;
    }

    /**
     * The version of the tuning algorithm used for this axis.
     */
    public ParamsetInfo withVersion(int aVersion) {
        this.setVersion(aVersion);
        return this;
    }

    private ServoTuningParam[] params;

    /**
     * The raw tuning parameters of this device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("params")
    public void setParams(ServoTuningParam[] params) {
        this.params = params;
    }

    /**
     * The raw tuning parameters of this device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("params")
    public ServoTuningParam[] getParams() {
        return this.params;
    }

    /**
     * The raw tuning parameters of this device.
     */
    public ParamsetInfo withParams(ServoTuningParam[] aParams) {
        this.setParams(aParams);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ParamsetInfo() {
    }

    /**
     * Constructor with all properties.
     */
    public ParamsetInfo(
        String type,
        int version,
        ServoTuningParam[] params
    ) {
        this.type = type;
        this.version = version;
        this.params = params;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ParamsetInfo other = (ParamsetInfo) obj;

        return (
            EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(version, other.version)
            && EqualityUtility.equals(params, other.params)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(version),
            EqualityUtility.generateHashCode(params)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ParamsetInfo { ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("version: ");
        sb.append(this.version);
        sb.append(", ");
        sb.append("params: ");
        sb.append(this.params);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ParamsetInfo fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ParamsetInfo.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ParamsetInfo> PARSER =
        new zaber.motion.dto.Parser<ParamsetInfo>() {
            @Override
            public ParamsetInfo fromByteArray(byte[] data) {
                return ParamsetInfo.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ParamsetInfo> parser() {
        return PARSER;
    }

}
