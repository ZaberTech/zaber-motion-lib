/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Trigger action identifier.
 */
public enum TriggerAction {

    ALL(0),

    A(1),

    B(2);

    private int value;

    TriggerAction(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static TriggerAction valueOf(int argValue) {
        for (TriggerAction value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
