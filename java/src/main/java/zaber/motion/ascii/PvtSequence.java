// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Measurement;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.StreamSegmentType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * A handle for a PVT sequence with this number on the device.
 * PVT sequences provide a way execute or store trajectory
 * consisting of points with defined position, velocity, and time.
 * PVT sequence methods append actions to a queue which executes
 * or stores actions in a first in, first out order.
 */
public class PvtSequence {
    private Device device;

    /**
     * @return Device that controls this PVT sequence.
     */
    public Device getDevice() {
        return this.device;
    }

    private int pvtId;

    /**
     * @return The number that identifies the PVT sequence on the device.
     */
    public int getPvtId() {
        return this.pvtId;
    }


    /**
     * @return Current mode of the PVT sequence.
     */
    public PvtMode getMode() {
        return this.retrieveMode();
    }


    /**
     * @return An array of axes definitions the PVT sequence is set up to control.
     */
    public PvtAxisDefinition[] getAxes() {
        return this.retrieveAxes();
    }

    private PvtIo io;

    /**
     * @return Gets an object that provides access to I/O for this sequence.
     */
    public PvtIo getIo() {
        return this.io;
    }

    public PvtSequence(
        Device device, int pvtId) {
        this.device = device;
        this.pvtId = pvtId;
        this.io = new PvtIo(device, pvtId);
    }

    /**
     * Setup the PVT sequence to control the specified axes and to queue actions on the device.
     * Allows use of lockstep axes in a PVT sequence.
     * @param pvtAxes Definition of the PVT sequence axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupLiveCompositeAsync(
        PvtAxisDefinition... pvtAxes) {
        zaber.motion.requests.StreamSetupLiveCompositeRequest request =
            new zaber.motion.requests.StreamSetupLiveCompositeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setPvtAxes(pvtAxes);
        return Call.callAsync("device/stream_setup_live_composite", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the PVT sequence to control the specified axes and to queue actions on the device.
     * Allows use of lockstep axes in a PVT sequence.
     * @param pvtAxes Definition of the PVT sequence axes.
     */
    public void setupLiveComposite(
        PvtAxisDefinition... pvtAxes) {
        try {
            setupLiveCompositeAsync(pvtAxes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the PVT sequence to control the specified axes and to queue actions on the device.
     * @param axes Numbers of physical axes to setup the PVT sequence on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupLiveAsync(
        int... axes) {
        zaber.motion.requests.StreamSetupLiveRequest request =
            new zaber.motion.requests.StreamSetupLiveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_live", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the PVT sequence to control the specified axes and to queue actions on the device.
     * @param axes Numbers of physical axes to setup the PVT sequence on.
     */
    public void setupLive(
        int... axes) {
        try {
            setupLiveAsync(axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
     * Allows use of lockstep axes in a PVT sequence.
     * @param pvtBuffer The PVT buffer to queue actions in.
     * @param pvtAxes Definition of the PVT sequence axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupStoreCompositeAsync(
        PvtBuffer pvtBuffer,
        PvtAxisDefinition... pvtAxes) {
        zaber.motion.requests.StreamSetupStoreCompositeRequest request =
            new zaber.motion.requests.StreamSetupStoreCompositeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setPvtBuffer(pvtBuffer.getBufferId());
        request.setPvtAxes(pvtAxes);
        return Call.callAsync("device/stream_setup_store_composite", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
     * Allows use of lockstep axes in a PVT sequence.
     * @param pvtBuffer The PVT buffer to queue actions in.
     * @param pvtAxes Definition of the PVT sequence axes.
     */
    public void setupStoreComposite(
        PvtBuffer pvtBuffer,
        PvtAxisDefinition... pvtAxes) {
        try {
            setupStoreCompositeAsync(pvtBuffer, pvtAxes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
     * @param pvtBuffer The PVT buffer to queue actions in.
     * @param axes Numbers of physical axes to setup the PVT sequence on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupStoreAsync(
        PvtBuffer pvtBuffer,
        int... axes) {
        zaber.motion.requests.StreamSetupStoreRequest request =
            new zaber.motion.requests.StreamSetupStoreRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setPvtBuffer(pvtBuffer.getBufferId());
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_store", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the PVT sequence to use the specified axes and queue actions into a PVT buffer.
     * @param pvtBuffer The PVT buffer to queue actions in.
     * @param axes Numbers of physical axes to setup the PVT sequence on.
     */
    public void setupStore(
        PvtBuffer pvtBuffer,
        int... axes) {
        try {
            setupStoreAsync(pvtBuffer, axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Append the actions in a PVT buffer to the sequence's queue.
     * @param pvtBuffer The PVT buffer to call.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> callAsync(
        PvtBuffer pvtBuffer) {
        zaber.motion.requests.StreamCallRequest request =
            new zaber.motion.requests.StreamCallRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setPvtBuffer(pvtBuffer.getBufferId());
        return Call.callAsync("device/stream_call", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Append the actions in a PVT buffer to the sequence's queue.
     * @param pvtBuffer The PVT buffer to call.
     */
    public void call(
        PvtBuffer pvtBuffer) {
        try {
            callAsync(pvtBuffer).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queues a point with absolute coordinates in the PVT sequence.
     * If some or all velocities are not provided, the sequence calculates the velocities
     * from surrounding points using finite difference.
     * The last point of the sequence must have defined velocity (likely zero).
     * @param positions Positions for the axes to move through, relative to their home positions.
     * @param velocities The axes velocities at the given point.
     * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
     * @param time The duration between the previous point in the sequence and this one.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> pointAsync(
        Measurement[] positions,
        Measurement[] velocities,
        Measurement time) {
        zaber.motion.requests.PvtPointRequest request =
            new zaber.motion.requests.PvtPointRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setType(StreamSegmentType.ABS);
        request.setPositions(positions);
        request.setVelocities(velocities);
        request.setTime(time);
        return Call.callAsync("device/stream_point", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queues a point with absolute coordinates in the PVT sequence.
     * If some or all velocities are not provided, the sequence calculates the velocities
     * from surrounding points using finite difference.
     * The last point of the sequence must have defined velocity (likely zero).
     * @param positions Positions for the axes to move through, relative to their home positions.
     * @param velocities The axes velocities at the given point.
     * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
     * @param time The duration between the previous point in the sequence and this one.
     */
    public void point(
        Measurement[] positions,
        Measurement[] velocities,
        Measurement time) {
        try {
            pointAsync(positions, velocities, time).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queues a point with coordinates relative to the previous point in the PVT sequence.
     * If some or all velocities are not provided, the sequence calculates the velocities
     * from surrounding points using finite difference.
     * The last point of the sequence must have defined velocity (likely zero).
     * @param positions Positions for the axes to move through, relative to the previous point.
     * @param velocities The axes velocities at the given point.
     * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
     * @param time The duration between the previous point in the sequence and this one.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> pointRelativeAsync(
        Measurement[] positions,
        Measurement[] velocities,
        Measurement time) {
        zaber.motion.requests.PvtPointRequest request =
            new zaber.motion.requests.PvtPointRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setType(StreamSegmentType.REL);
        request.setPositions(positions);
        request.setVelocities(velocities);
        request.setTime(time);
        return Call.callAsync("device/stream_point", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queues a point with coordinates relative to the previous point in the PVT sequence.
     * If some or all velocities are not provided, the sequence calculates the velocities
     * from surrounding points using finite difference.
     * The last point of the sequence must have defined velocity (likely zero).
     * @param positions Positions for the axes to move through, relative to the previous point.
     * @param velocities The axes velocities at the given point.
     * Specify an empty array or null for specific axes to make the sequence calculate the velocity.
     * @param time The duration between the previous point in the sequence and this one.
     */
    public void pointRelative(
        Measurement[] positions,
        Measurement[] velocities,
        Measurement time) {
        try {
            pointRelativeAsync(positions, velocities, time).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until the live PVT sequence executes all queued actions.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync(
        boolean throwErrorOnFault) {
        zaber.motion.requests.StreamWaitUntilIdleRequest request =
            new zaber.motion.requests.StreamWaitUntilIdleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setThrowErrorOnFault(throwErrorOnFault);
        return Call.callAsync("device/stream_wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until the live PVT sequence executes all queued actions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        return waitUntilIdleAsync(true);
    }

    /**
     * Waits until the live PVT sequence executes all queued actions.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     */
    public void waitUntilIdle(
        boolean throwErrorOnFault) {
        try {
            waitUntilIdleAsync(throwErrorOnFault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until the live PVT sequence executes all queued actions.
     */
    public void waitUntilIdle() {
        waitUntilIdle(true);
    }

    /**
     * Cork the front of the PVT sequences's action queue, blocking execution.
     * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
     * Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
     * You can only cork an idle live PVT sequence.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> corkAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        return Call.callAsync("device/stream_cork", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cork the front of the PVT sequences's action queue, blocking execution.
     * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
     * Corking eliminates discontinuities in motion due to subsequent PVT commands reaching the device late.
     * You can only cork an idle live PVT sequence.
     */
    public void cork() {
        try {
            corkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Uncork the front of the queue, unblocking command execution.
     * You can only uncork an idle live PVT sequence that is corked.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> uncorkAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        return Call.callAsync("device/stream_uncork", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Uncork the front of the queue, unblocking command execution.
     * You can only uncork an idle live PVT sequence that is corked.
     */
    public void uncork() {
        try {
            uncorkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the PVT sequence is executing a queued action.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/stream_is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a boolean value indicating whether the live PVT sequence is executing a queued action.
     * @return True if the PVT sequence is executing a queued action.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string which represents the PVT sequence.
     * @return String which represents the PVT sequence.
     */
    public String toString() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/stream_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Disables the PVT sequence.
     * If the PVT sequence is not setup, this command does nothing.
     * Once disabled, the PVT sequence will no longer accept PVT commands.
     * The PVT sequence will process the rest of the commands in the queue until it is empty.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        return Call.callAsync("device/stream_disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables the PVT sequence.
     * If the PVT sequence is not setup, this command does nothing.
     * Once disabled, the PVT sequence will no longer accept PVT commands.
     * The PVT sequence will process the rest of the commands in the queue until it is empty.
     */
    public void disable() {
        try {
            disableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to the PVT sequence.
     * Keeps resending the command while the device rejects with AGAIN reason.
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandAsync(
        String command) {
        zaber.motion.requests.StreamGenericCommandRequest request =
            new zaber.motion.requests.StreamGenericCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setCommand(command);
        return Call.callAsync("device/stream_generic_command", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to the PVT sequence.
     * Keeps resending the command while the device rejects with AGAIN reason.
     * @param command Command and its parameters.
     */
    public void genericCommand(
        String command) {
        try {
            genericCommandAsync(command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a batch of generic ASCII commands to the PVT sequence.
     * Keeps resending command while the device rejects with AGAIN reason.
     * The batch is atomic in terms of thread safety.
     * @param batch Array of commands.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandBatchAsync(
        String[] batch) {
        zaber.motion.requests.StreamGenericCommandBatchRequest request =
            new zaber.motion.requests.StreamGenericCommandBatchRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setBatch(batch);
        return Call.callAsync("device/stream_generic_command_batch", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a batch of generic ASCII commands to the PVT sequence.
     * Keeps resending command while the device rejects with AGAIN reason.
     * The batch is atomic in terms of thread safety.
     * @param batch Array of commands.
     */
    public void genericCommandBatch(
        String[] batch) {
        try {
            genericCommandBatchAsync(batch).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queries the PVT sequence status from the device
     * and returns boolean indicating whether the PVT sequence is disabled.
     * Useful to determine if execution was interrupted by other movements.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the PVT sequence is disabled.
     */
    public CompletableFuture<Boolean> checkDisabledAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/stream_check_disabled",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Queries the PVT sequence status from the device
     * and returns boolean indicating whether the PVT sequence is disabled.
     * Useful to determine if execution was interrupted by other movements.
     * @return True if the PVT sequence is disabled.
     */
    public boolean checkDisabled() {
        try {
            return checkDisabledAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Makes the PVT sequence throw PvtDiscontinuityException when it encounters discontinuities (ND warning flag).
     */
    public void treatDiscontinuitiesAsError() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        Call.callSync("device/stream_treat_discontinuities", request, null);
    }


    /**
     * Prevents PvtDiscontinuityException as a result of expected discontinuity when resuming the sequence.
     */
    public void ignoreCurrentDiscontinuity() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        Call.callSync("device/stream_ignore_discontinuity", request, null);
    }


    /**
     * Gets the axes of the PVT sequence.
     * @return An array of axis numbers of the axes the PVT sequence is set up to control.
     */
    private PvtAxisDefinition[] retrieveAxes() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        zaber.motion.requests.StreamGetAxesResponse response = Call.callSync(
            "device/stream_get_axes",
            request,
            zaber.motion.requests.StreamGetAxesResponse.parser());
        return response.getPvtAxes();
    }


    /**
     * Get the mode of the PVT sequence.
     * @return Mode of the PVT sequence.
     */
    private PvtMode retrieveMode() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        zaber.motion.requests.StreamModeResponse response = Call.callSync(
            "device/stream_get_mode",
            request,
            zaber.motion.requests.StreamModeResponse.parser());
        return response.getPvtMode();
    }


    /**
     * @deprecated Use PvtSequence.Io.SetDigitalOutput instead.
     *
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setDigitalOutputAsync(
        int channelNumber,
        DigitalOutputAction value) {
        zaber.motion.requests.StreamSetDigitalOutputRequest request =
            new zaber.motion.requests.StreamSetDigitalOutputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_digital_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use PvtSequence.Io.SetDigitalOutput instead.
     *
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     */
    public void setDigitalOutput(
        int channelNumber,
        DigitalOutputAction value) {
        try {
            setDigitalOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAllDigitalOutputs instead.
     *
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAllDigitalOutputsAsync(
        DigitalOutputAction[] values) {
        zaber.motion.requests.StreamSetAllDigitalOutputsRequest request =
            new zaber.motion.requests.StreamSetAllDigitalOutputsRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_digital_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAllDigitalOutputs instead.
     *
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     */
    public void setAllDigitalOutputs(
        DigitalOutputAction[] values) {
        try {
            setAllDigitalOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAnalogOutput instead.
     *
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAnalogOutputAsync(
        int channelNumber,
        double value) {
        zaber.motion.requests.StreamSetAnalogOutputRequest request =
            new zaber.motion.requests.StreamSetAnalogOutputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_analog_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAnalogOutput instead.
     *
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     */
    public void setAnalogOutput(
        int channelNumber,
        double value) {
        try {
            setAnalogOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAllAnalogOutputs instead.
     *
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAllAnalogOutputsAsync(
        double[] values) {
        zaber.motion.requests.StreamSetAllAnalogOutputsRequest request =
            new zaber.motion.requests.StreamSetAllAnalogOutputsRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getPvtId());
        request.setPvt(true);
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_analog_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use PvtSequence.Io.SetAllAnalogOutputs instead.
     *
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     */
    public void setAllAnalogOutputs(
        double[] values) {
        try {
            setAllAnalogOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
