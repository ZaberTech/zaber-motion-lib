// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;


/**
 * Class providing access to device triggers.
 * Please note that the Triggers API is currently an experimental feature.
 * Requires at least Firmware 7.06.
 */
public class Triggers {
    private Device device;

    /**
     * @return Device that these triggers belong to.
     */
    public Device getDevice() {
        return this.device;
    }

    public Triggers(
        Device device) {
        this.device = device;
    }

    /**
     * Get the number of triggers for this device.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of triggers for this device.
     */
    public CompletableFuture<Integer> getNumberOfTriggersAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("trigger.numtriggers");
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "triggers/get_setting",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the number of triggers for this device.
     * @return Number of triggers for this device.
     */
    public int getNumberOfTriggers() {
        try {
            return getNumberOfTriggersAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the number of actions for each trigger for this device.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of actions for each trigger for this device.
     */
    public CompletableFuture<Integer> getNumberOfActionsAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("trigger.numactions");
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "triggers/get_setting",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the number of actions for each trigger for this device.
     * @return Number of actions for each trigger for this device.
     */
    public int getNumberOfActions() {
        try {
            return getNumberOfActionsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get a specific trigger for this device.
     * @param triggerNumber The number of the trigger to control. Trigger numbers start at 1.
     * @return Trigger instance.
     */
    public Trigger getTrigger(
        int triggerNumber) {
        if (triggerNumber <= 0) {
            throw new IllegalArgumentException("Invalid value; triggers are numbered from 1.");
        }
        return new Trigger(this.device, triggerNumber);
    }


    /**
     * Get the state for every trigger for this device.
     * @return A CompletableFuture that can be completed to get the result:
     * Complete state for every trigger.
     */
    public CompletableFuture<TriggerState[]> getTriggerStatesAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        CompletableFuture<zaber.motion.requests.TriggerStates> response = Call.callAsync(
            "triggers/get_trigger_states",
            request,
            zaber.motion.requests.TriggerStates.parser());
        return response
            .thenApply(r -> r.getStates());
    }

    /**
     * Get the state for every trigger for this device.
     * @return Complete state for every trigger.
     */
    public TriggerState[] getTriggerStates() {
        try {
            return getTriggerStatesAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the enabled state for every trigger for this device.
     * @return A CompletableFuture that can be completed to get the result:
     * Whether triggers are enabled and the number of times they will fire.
     */
    public CompletableFuture<TriggerEnabledState[]> getEnabledStatesAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        CompletableFuture<zaber.motion.requests.TriggerEnabledStates> response = Call.callAsync(
            "triggers/get_enabled_states",
            request,
            zaber.motion.requests.TriggerEnabledStates.parser());
        return response
            .thenApply(r -> r.getStates());
    }

    /**
     * Gets the enabled state for every trigger for this device.
     * @return Whether triggers are enabled and the number of times they will fire.
     */
    public TriggerEnabledState[] getEnabledStates() {
        try {
            return getEnabledStatesAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the labels for every trigger for this device.
     * @return A CompletableFuture that can be completed to get the result:
     * The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.
     */
    public CompletableFuture<String[]> getAllLabelsAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        CompletableFuture<zaber.motion.requests.StringArrayResponse> response = Call.callAsync(
            "triggers/get_all_labels",
            request,
            zaber.motion.requests.StringArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Gets the labels for every trigger for this device.
     * @return The labels for every trigger for this device. If a trigger has no label, the value will be an empty string.
     */
    public String[] getAllLabels() {
        try {
            return getAllLabelsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
