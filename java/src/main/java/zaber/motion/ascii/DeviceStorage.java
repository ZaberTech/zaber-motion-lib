// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing access to device storage.
 * Requires at least Firmware 7.30.
 */
public class DeviceStorage {
    private Device device;

    public DeviceStorage(
        Device device) {
        this.device = device;
    }

    /**
     * Sets the device value stored at the provided key.
     * @param key Key to set the value at.
     * @param value Value to set.
     * @param encode Whether the stored value should be base64 encoded before being stored.
     * This makes the string unreadable to humans using the ASCII protocol,
     * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setStringAsync(
        String key,
        String value,
        boolean encode) {
        zaber.motion.requests.DeviceSetStorageRequest request =
            new zaber.motion.requests.DeviceSetStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        request.setValue(value);
        request.setEncode(encode);
        return Call.callAsync("device/set_storage", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the device value stored at the provided key.
     * @param key Key to set the value at.
     * @param value Value to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setStringAsync(
        String key,
        String value) {
        return setStringAsync(key, value, false);
    }

    /**
     * Sets the device value stored at the provided key.
     * @param key Key to set the value at.
     * @param value Value to set.
     * @param encode Whether the stored value should be base64 encoded before being stored.
     * This makes the string unreadable to humans using the ASCII protocol,
     * however, values stored this way can be of any length and use non-ASCII and protocol reserved characters.
     */
    public void setString(
        String key,
        String value,
        boolean encode) {
        try {
            setStringAsync(key, value, encode).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the device value stored at the provided key.
     * @param key Key to set the value at.
     * @param value Value to set.
     */
    public void setString(
        String key,
        String value) {
        setString(key, value, false);
    }

    /**
     * Gets the device value stored with the provided key.
     * @param key Key to read the value of.
     * @param decode Whether the stored value should be decoded.
     * Only use this when reading values set by storage.set with "encode" true.
     * @return A CompletableFuture that can be completed to get the result:
     * Stored value.
     */
    public CompletableFuture<String> getStringAsync(
        String key,
        boolean decode) {
        zaber.motion.requests.DeviceGetStorageRequest request =
            new zaber.motion.requests.DeviceGetStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        request.setDecode(decode);
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_storage",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the device value stored with the provided key.
     * @param key Key to read the value of.
     * @return A CompletableFuture that can be completed to get the result:
     * Stored value.
     */
    public CompletableFuture<String> getStringAsync(
        String key) {
        return getStringAsync(key, false);
    }

    /**
     * Gets the device value stored with the provided key.
     * @param key Key to read the value of.
     * @param decode Whether the stored value should be decoded.
     * Only use this when reading values set by storage.set with "encode" true.
     * @return Stored value.
     */
    public String getString(
        String key,
        boolean decode) {
        try {
            return getStringAsync(key, decode).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the device value stored with the provided key.
     * @param key Key to read the value of.
     * @return Stored value.
     */
    public String getString(
        String key) {
        return getString(key, false);
    }

    /**
     * Sets the value at the provided key to the provided number.
     * @param key Key to set the value at.
     * @param value Value to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setNumberAsync(
        String key,
        double value) {
        zaber.motion.requests.DeviceSetStorageNumberRequest request =
            new zaber.motion.requests.DeviceSetStorageNumberRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        request.setValue(value);
        return Call.callAsync("device/set_storage_number", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the value at the provided key to the provided number.
     * @param key Key to set the value at.
     * @param value Value to set.
     */
    public void setNumber(
        String key,
        double value) {
        try {
            setNumberAsync(key, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the value at the provided key interpreted as a number.
     * @param key Key to get the value at.
     * @return A CompletableFuture that can be completed to get the result:
     * Stored value.
     */
    public CompletableFuture<Double> getNumberAsync(
        String key) {
        zaber.motion.requests.DeviceStorageRequest request =
            new zaber.motion.requests.DeviceStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_storage_number",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the value at the provided key interpreted as a number.
     * @param key Key to get the value at.
     * @return Stored value.
     */
    public double getNumber(
        String key) {
        try {
            return getNumberAsync(key).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the value at the provided key to the provided boolean.
     * @param key Key to set the value at.
     * @param value Value to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setBoolAsync(
        String key,
        boolean value) {
        zaber.motion.requests.DeviceSetStorageBoolRequest request =
            new zaber.motion.requests.DeviceSetStorageBoolRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        request.setValue(value);
        return Call.callAsync("device/set_storage_bool", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the value at the provided key to the provided boolean.
     * @param key Key to set the value at.
     * @param value Value to set.
     */
    public void setBool(
        String key,
        boolean value) {
        try {
            setBoolAsync(key, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the value at the provided key interpreted as a boolean.
     * @param key Key to get the value at.
     * @return A CompletableFuture that can be completed to get the result:
     * Stored value.
     */
    public CompletableFuture<Boolean> getBoolAsync(
        String key) {
        zaber.motion.requests.DeviceStorageRequest request =
            new zaber.motion.requests.DeviceStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/get_storage_bool",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the value at the provided key interpreted as a boolean.
     * @param key Key to get the value at.
     * @return Stored value.
     */
    public boolean getBool(
        String key) {
        try {
            return getBoolAsync(key).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Erases the device value stored at the provided key.
     * @param key Key to erase.
     * @return A CompletableFuture that can be completed to get the result:
     * A boolean indicating if the key existed.
     */
    public CompletableFuture<Boolean> eraseKeyAsync(
        String key) {
        zaber.motion.requests.DeviceStorageRequest request =
            new zaber.motion.requests.DeviceStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/erase_storage",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Erases the device value stored at the provided key.
     * @param key Key to erase.
     * @return A boolean indicating if the key existed.
     */
    public boolean eraseKey(
        String key) {
        try {
            return eraseKeyAsync(key).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Lists the device storage keys matching a given prefix.
     * Omit the prefix to list all the keys.
     * @param prefix Optional key prefix.
     * @return A CompletableFuture that can be completed to get the result:
     * Storage keys matching the given prefix.
     */
    public CompletableFuture<String[]> listKeysAsync(
        String prefix) {
        zaber.motion.requests.DeviceStorageListKeysRequest request =
            new zaber.motion.requests.DeviceStorageListKeysRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setPrefix(prefix);
        CompletableFuture<zaber.motion.requests.StringArrayResponse> response = Call.callAsync(
            "device/storage_list_keys",
            request,
            zaber.motion.requests.StringArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Lists the device storage keys matching a given prefix.
     * Omit the prefix to list all the keys.
     * @return A CompletableFuture that can be completed to get the result:
     * Storage keys matching the given prefix.
     */
    public CompletableFuture<String[]> listKeysAsync() {
        return listKeysAsync(null);
    }

    /**
     * Lists the device storage keys matching a given prefix.
     * Omit the prefix to list all the keys.
     * @param prefix Optional key prefix.
     * @return Storage keys matching the given prefix.
     */
    public String[] listKeys(
        String prefix) {
        try {
            return listKeysAsync(prefix).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Lists the device storage keys matching a given prefix.
     * Omit the prefix to list all the keys.
     * @return Storage keys matching the given prefix.
     */
    public String[] listKeys() {
        return listKeys(null);
    }

    /**
     * Determines whether a given key exists in device storage.
     * @param key Key which existence to determine.
     * @return A CompletableFuture that can be completed to get the result:
     * True indicating that the key exists, false otherwise.
     */
    public CompletableFuture<Boolean> keyExistsAsync(
        String key) {
        zaber.motion.requests.DeviceStorageRequest request =
            new zaber.motion.requests.DeviceStorageRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setKey(key);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/storage_key_exists",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Determines whether a given key exists in device storage.
     * @param key Key which existence to determine.
     * @return True indicating that the key exists, false otherwise.
     */
    public boolean keyExists(
        String key) {
        try {
            return keyExistsAsync(key).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
