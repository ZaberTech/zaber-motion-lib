/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Comparison operator for trigger condition.
 */
public enum TriggerCondition {

    EQ(0),

    NE(1),

    GT(2),

    GE(3),

    LT(4),

    LE(5);

    private int value;

    TriggerCondition(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static TriggerCondition valueOf(int argValue) {
        for (TriggerCondition value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
