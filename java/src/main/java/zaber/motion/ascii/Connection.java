// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.ArrayUtility;
import zaber.motion.gateway.Call;
import zaber.motion.gateway.Events;
import zaber.motion.exceptions.ExceptionConverter;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.UnknownResponseEventWrapper;
import zaber.motion.requests.AlertEventWrapper;
import zaber.motion.requests.DisconnectedEvent;
import zaber.motion.requests.InterfaceType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Class representing access to particular connection (serial port, TCP connection).
 */
public class Connection implements AutoCloseable {
    private Observable<UnknownResponseEvent> unknownResponse;

    /**
     * Event invoked when a response from a device cannot be matched to any known request.
     */
    public Observable<UnknownResponseEvent> getUnknownResponse() {
        return this.unknownResponse;
    }

    private Observable<AlertEvent> alert;

    /**
     * Event invoked when an alert is received from a device.
     */
    public Observable<AlertEvent> getAlert() {
        return this.alert;
    }

    private Subject<MotionLibException> disconnected = ReplaySubject.create();

    /**
     * Event invoked when connection is interrupted or closed.
     */
    public Observable<MotionLibException> getDisconnected() {
        return this.disconnected;
    }

    /**
     * Default baud rate for serial connections.
     */
    public static final int DEFAULT_BAUD_RATE = 115200;


    /**
     * Commands sent over this port are forwarded to the device chain.
     * The bandwidth may be limited as the commands are forwarded over a serial connection.
     */
    public static final int TCP_PORT_CHAIN = 55550;


    /**
     * Local area network share port.
     */
    public static final int NETWORK_SHARE_PORT = 11421;


    /**
     * Commands send over this port are processed only by the device
     * and not forwarded to the rest of the chain.
     * Using this port typically makes the communication faster.
     */
    public static final int TCP_PORT_DEVICE_ONLY = 55551;


    private int interfaceId;

    /**
     * @return The interface ID identifies this Connection instance with the underlying library.
     */
    public int getInterfaceId() {
        return this.interfaceId;
    }


    /**
     * @return The default timeout, in milliseconds, for a device to respond to a request.
     * Setting the timeout to a too low value may cause request timeout exceptions.
     * The initial value is 1000 (one second).
     */
    public int getDefaultRequestTimeout() {
        return this.retrieveTimeout();
    }

    /**
     * The default timeout, in milliseconds, for a device to respond to a request.
     * Setting the timeout to a too low value may cause request timeout exceptions.
     * The initial value is 1000 (one second).
     */
    public void setDefaultRequestTimeout(int value) {
        this.changeTimeout(value);
    }


    /**
     * @return Controls whether outgoing messages contain checksum.
     */
    public boolean getChecksumEnabled() {
        return this.retrieveChecksumEnabled();
    }

    /**
     * Controls whether outgoing messages contain checksum.
     */
    public void setChecksumEnabled(boolean value) {
        this.changeChecksumEnabled(value);
    }

    public Connection(
        int interfaceId) {
        this.interfaceId = interfaceId;
        this.subscribe();
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 115200).
     * @param direct If true will connect to the serial port directly,
     * failing if the connection is already opened by a message router instance.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName,
        int baudRate,
        boolean direct) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.SERIAL_PORT);
        request.setPortName(portName);
        request.setBaudRate(baudRate);
        request.setRejectRoutedConnection(direct);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 115200).
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName,
        int baudRate) {
        return openSerialPortAsync(portName, baudRate, false);
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName) {
        return openSerialPortAsync(portName, DEFAULT_BAUD_RATE, false);
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 115200).
     * @param direct If true will connect to the serial port directly,
     * failing if the connection is already opened by a message router instance.
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName,
        int baudRate,
        boolean direct) {
        try {
            return openSerialPortAsync(portName, baudRate, direct).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 115200).
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName,
        int baudRate) {
        return openSerialPort(portName, baudRate, false);
    }

    /**
     * Opens a serial port, if Zaber Launcher controls the port, the port will be opened through Zaber Launcher.
     * Zaber Launcher allows sharing of the port between multiple applications,
     * If port sharing is not desirable, use the `direct` parameter.
     * @param portName Name of the port to open.
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName) {
        return openSerialPort(portName, DEFAULT_BAUD_RATE, false);
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Optional port number (defaults to 55550).
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openTcpAsync(
        String hostName,
        int port) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.TCP);
        request.setHostName(hostName);
        request.setPort(port);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openTcpAsync(
        String hostName) {
        return openTcpAsync(hostName, TCP_PORT_CHAIN);
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Optional port number (defaults to 55550).
     * @return An object representing the connection.
     */
    public static Connection openTcp(
        String hostName,
        int port) {
        try {
            return openTcpAsync(hostName, port).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @return An object representing the connection.
     */
    public static Connection openTcp(
        String hostName) {
        return openTcp(hostName, TCP_PORT_CHAIN);
    }

    /**
     * Opens a connection using a custom transport.
     * @param transport The custom connection transport.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openCustomAsync(
        Transport transport) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.CUSTOM);
        request.setTransport(transport.getTransportId());
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a connection using a custom transport.
     * @param transport The custom connection transport.
     * @return An object representing the connection.
     */
    public static Connection openCustom(
        Transport transport) {
        try {
            return openCustomAsync(transport).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @param realm The realm to connect to.
     * Can be left empty for the default account realm.
     * @param api The URL of the API to receive connection info from.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openIotAsync(
        String cloudId,
        String token,
        String connectionName,
        String realm,
        String api) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.IOT);
        request.setCloudId(cloudId);
        request.setToken(token);
        request.setConnectionName(connectionName);
        request.setRealm(realm);
        request.setApi(api);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @param realm The realm to connect to.
     * Can be left empty for the default account realm.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openIotAsync(
        String cloudId,
        String token,
        String connectionName,
        String realm) {
        return openIotAsync(cloudId, token, connectionName, realm, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openIotAsync(
        String cloudId,
        String token,
        String connectionName) {
        return openIotAsync(cloudId, token, connectionName, null, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openIotAsync(
        String cloudId,
        String token) {
        return openIotAsync(cloudId, token, null, null, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openIotAsync(
        String cloudId) {
        return openIotAsync(cloudId, "unauthenticated", null, null, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @param realm The realm to connect to.
     * Can be left empty for the default account realm.
     * @param api The URL of the API to receive connection info from.
     * @return An object representing the connection.
     */
    public static Connection openIot(
        String cloudId,
        String token,
        String connectionName,
        String realm,
        String api) {
        try {
            return openIotAsync(cloudId, token, connectionName, realm, api).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @param realm The realm to connect to.
     * Can be left empty for the default account realm.
     * @return An object representing the connection.
     */
    public static Connection openIot(
        String cloudId,
        String token,
        String connectionName,
        String realm) {
        return openIot(cloudId, token, connectionName, realm, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @return An object representing the connection.
     */
    public static Connection openIot(
        String cloudId,
        String token,
        String connectionName) {
        return openIot(cloudId, token, connectionName, null, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @param token The token to authenticate with. By default the connection will be unauthenticated.
     * @return An object representing the connection.
     */
    public static Connection openIot(
        String cloudId,
        String token) {
        return openIot(cloudId, token, null, null, "https://api.zaber.io");
    }

    /**
     * Opens a secured connection to a cloud connected device chain.
     * Use this method to connect to devices on your account.
     * @param cloudId The cloud ID to connect to.
     * @return An object representing the connection.
     */
    public static Connection openIot(
        String cloudId) {
        return openIot(cloudId, "unauthenticated", null, null, "https://api.zaber.io");
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openNetworkShareAsync(
        String hostName,
        int port,
        String connectionName) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.NETWORK_SHARE);
        request.setHostName(hostName);
        request.setPort(port);
        request.setConnectionName(connectionName);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openNetworkShareAsync(
        String hostName,
        int port) {
        return openNetworkShareAsync(hostName, port, null);
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openNetworkShareAsync(
        String hostName) {
        return openNetworkShareAsync(hostName, NETWORK_SHARE_PORT, null);
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @param connectionName The name of the connection to open.
     * Can be left empty to default to the only connection present.
     * Otherwise, use serial port name for serial port connection or hostname:port for TCP connection.
     * @return An object representing the connection.
     */
    public static Connection openNetworkShare(
        String hostName,
        int port,
        String connectionName) {
        try {
            return openNetworkShareAsync(hostName, port, connectionName).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @return An object representing the connection.
     */
    public static Connection openNetworkShare(
        String hostName,
        int port) {
        return openNetworkShare(hostName, port, null);
    }

    /**
     * Opens a connection to Zaber Launcher in your Local Area Network.
     * The connection is not secured.
     * @param hostName Hostname or IP address.
     * @return An object representing the connection.
     */
    public static Connection openNetworkShare(
        String hostName) {
        return openNetworkShare(hostName, NETWORK_SHARE_PORT, null);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int device,
        int axis,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand(command);
        request.setDevice(device);
        request.setAxis(axis);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<Response> response = Call.callAsync(
            "interface/generic_command",
            request,
            Response.parser());
        return response;
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int device,
        int axis,
        boolean checkErrors) {
        return genericCommandAsync(command, device, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int device,
        int axis) {
        return genericCommandAsync(command, device, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int device) {
        return genericCommandAsync(command, device, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command) {
        return genericCommandAsync(command, 0, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int device,
        int axis,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandAsync(command, device, axis, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int device,
        int axis,
        boolean checkErrors) {
        return genericCommand(command, device, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int device,
        int axis) {
        return genericCommand(command, device, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int device) {
        return genericCommand(command, device, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command) {
        return genericCommand(command, 0, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * Specifying -1 omits the number completely.
     * @param axis Optional axis number to send the command to.
     * Specifying -1 omits the number completely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command,
        int device,
        int axis) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand(command);
        request.setDevice(device);
        request.setAxis(axis);
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * Specifying -1 omits the number completely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command,
        int device) {
        return genericCommandNoResponseAsync(command, device, 0);
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command) {
        return genericCommandNoResponseAsync(command, 0, 0);
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * Specifying -1 omits the number completely.
     * @param axis Optional axis number to send the command to.
     * Specifying -1 omits the number completely.
     */
    public void genericCommandNoResponse(
        String command,
        int device,
        int axis) {
        try {
            genericCommandNoResponseAsync(command, device, axis).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * Specifying -1 omits the number completely.
     */
    public void genericCommandNoResponse(
        String command,
        int device) {
        genericCommandNoResponse(command, device, 0);
    }

    /**
     * Sends a generic ASCII command to this connection without expecting a response and without adding a message ID.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     */
    public void genericCommandNoResponse(
        String command) {
        genericCommandNoResponse(command, 0, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int device,
        int axis,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand(command);
        request.setDevice(device);
        request.setAxis(axis);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.GenericCommandResponseCollection> response = Call.callAsync(
            "interface/generic_command_multi_response",
            request,
            zaber.motion.requests.GenericCommandResponseCollection.parser());
        return response
            .thenApply(r -> r.getResponses());
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int device,
        int axis,
        boolean checkErrors) {
        return genericCommandMultiResponseAsync(command, device, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int device,
        int axis) {
        return genericCommandMultiResponseAsync(command, device, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int device) {
        return genericCommandMultiResponseAsync(command, device, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command) {
        return genericCommandMultiResponseAsync(command, 0, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int device,
        int axis,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandMultiResponseAsync(command, device, axis, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int device,
        int axis,
        boolean checkErrors) {
        return genericCommandMultiResponse(command, device, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @param axis Optional axis number to send the command to.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int device,
        int axis) {
        return genericCommandMultiResponse(command, device, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param device Optional device address to send the command to.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int device) {
        return genericCommandMultiResponse(command, device, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this connection and expect multiple responses,
     * either from one device or from many devices.
     * Responses are returned in order of arrival.
     * For more information refer to the [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command) {
        return genericCommandMultiResponse(command, 0, 0, true, 0);
    }

    /**
     * Enables alerts for all devices on the connection.
     * This will change the "comm.alert" setting to 1 on all supported devices.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAlertsAsync() {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand("set comm.alert 1");
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Enables alerts for all devices on the connection.
     * This will change the "comm.alert" setting to 1 on all supported devices.
     */
    public void enableAlerts() {
        try {
            enableAlertsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Disables alerts for all devices on the connection.
     * This will change the "comm.alert" setting to 0 on all supported devices.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableAlertsAsync() {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand("set comm.alert 0");
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables alerts for all devices on the connection.
     * This will change the "comm.alert" setting to 0 on all supported devices.
     */
    public void disableAlerts() {
        try {
            disableAlertsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Resets ASCII protocol message IDs. Only for testing purposes.
     */
    public void resetIds() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        Call.callSync("interface/reset_ids", request, null);
    }


    /**
     * Close the connection.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> closeAsync() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        return Call.callAsync("interface/close", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Close the connection.
     */
    public void close() {
        try {
            closeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets a Device class instance which allows you to control a particular device on this connection.
     * Devices are numbered from 1.
     * @param deviceAddress Address of device intended to control. Address is configured for each device.
     * @return Device instance.
     */
    public Device getDevice(
        int deviceAddress) {
        if (deviceAddress <= 0) {
            throw new IllegalArgumentException("Invalid value; physical devices are numbered from 1.");
        }
        return new Device(this, deviceAddress);
    }


    /**
     * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
     * @param firstAddress This is the address that the device closest to the computer is given.
     * Remaining devices are numbered consecutively.
     * @return A CompletableFuture that can be completed to get the result:
     * Total number of devices that responded to the renumber.
     */
    public CompletableFuture<Integer> renumberDevicesAsync(
        int firstAddress) {
        if (firstAddress <= 0) {
            throw new IllegalArgumentException("Invalid value; device addresses are numbered from 1.");
        }
        zaber.motion.requests.RenumberRequest request =
            new zaber.motion.requests.RenumberRequest();
        request.setInterfaceId(getInterfaceId());
        request.setAddress(firstAddress);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/renumber_all",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
     * @return A CompletableFuture that can be completed to get the result:
     * Total number of devices that responded to the renumber.
     */
    public CompletableFuture<Integer> renumberDevicesAsync() {
        return renumberDevicesAsync(1);
    }

    /**
     * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
     * @param firstAddress This is the address that the device closest to the computer is given.
     * Remaining devices are numbered consecutively.
     * @return Total number of devices that responded to the renumber.
     */
    public int renumberDevices(
        int firstAddress) {
        try {
            return renumberDevicesAsync(firstAddress).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Renumbers devices present on this connection. After renumbering, devices need to be identified again.
     * @return Total number of devices that responded to the renumber.
     */
    public int renumberDevices() {
        return renumberDevices(1);
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @param identifyDevices Determines whether device identification should be performed as well.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of detected devices.
     */
    public CompletableFuture<Device[]> detectDevicesAsync(
        boolean identifyDevices) {
        zaber.motion.requests.DeviceDetectRequest request =
            new zaber.motion.requests.DeviceDetectRequest();
        request.setInterfaceId(getInterfaceId());
        request.setIdentifyDevices(identifyDevices);
        CompletableFuture<zaber.motion.requests.DeviceDetectResponse> response = Call.callAsync(
            "device/detect",
            request,
            zaber.motion.requests.DeviceDetectResponse.parser());
        return response
            .thenApply(r -> ArrayUtility.arrayFromInt(Device[]::new, r.getDevices(), address -> this.getDevice(address)));
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of detected devices.
     */
    public CompletableFuture<Device[]> detectDevicesAsync() {
        return detectDevicesAsync(true);
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @param identifyDevices Determines whether device identification should be performed as well.
     * @return Array of detected devices.
     */
    public Device[] detectDevices(
        boolean identifyDevices) {
        try {
            return detectDevicesAsync(identifyDevices).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @return Array of detected devices.
     */
    public Device[] detectDevices() {
        return detectDevices(true);
    }

    /**
     * Forgets all the information associated with devices on the connection.
     * Useful when devices are removed from the chain indefinitely.
     * @param exceptDevices Addresses of devices that should not be forgotten.
     */
    public void forgetDevices(
        int[] exceptDevices) {
        zaber.motion.requests.ForgetDevicesRequest request =
            new zaber.motion.requests.ForgetDevicesRequest();
        request.setInterfaceId(getInterfaceId());
        request.setExceptDevices(exceptDevices);
        Call.callSync("device/forget", request, null);
    }

    /**
     * Forgets all the information associated with devices on the connection.
     * Useful when devices are removed from the chain indefinitely.
     */
    public void forgetDevices() {
        forgetDevices(new int[0]);
    }


    /**
     * Stops all of the devices on this connection.
     * @param waitUntilIdle Determines whether the function should return immediately
     * or wait until the devices are stopped.
     * @return A CompletableFuture that can be completed to get the result:
     * The addresses of the devices that were stopped by this command.
     */
    public CompletableFuture<int[]> stopAllAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceOnAllRequest request =
            new zaber.motion.requests.DeviceOnAllRequest();
        request.setInterfaceId(getInterfaceId());
        request.setWaitUntilIdle(waitUntilIdle);
        CompletableFuture<zaber.motion.requests.DeviceOnAllResponse> response = Call.callAsync(
            "device/stop_all",
            request,
            zaber.motion.requests.DeviceOnAllResponse.parser());
        return response
            .thenApply(r -> r.getDeviceAddresses());
    }

    /**
     * Stops all of the devices on this connection.
     * @return A CompletableFuture that can be completed to get the result:
     * The addresses of the devices that were stopped by this command.
     */
    public CompletableFuture<int[]> stopAllAsync() {
        return stopAllAsync(true);
    }

    /**
     * Stops all of the devices on this connection.
     * @param waitUntilIdle Determines whether the function should return immediately
     * or wait until the devices are stopped.
     * @return The addresses of the devices that were stopped by this command.
     */
    public int[] stopAll(
        boolean waitUntilIdle) {
        try {
            return stopAllAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops all of the devices on this connection.
     * @return The addresses of the devices that were stopped by this command.
     */
    public int[] stopAll() {
        return stopAll(true);
    }

    /**
     * Homes all of the devices on this connection.
     * @param waitUntilIdle Determines whether the function should return immediately
     * or wait until the devices are homed.
     * @return A CompletableFuture that can be completed to get the result:
     * The addresses of the devices that were homed by this command.
     */
    public CompletableFuture<int[]> homeAllAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceOnAllRequest request =
            new zaber.motion.requests.DeviceOnAllRequest();
        request.setInterfaceId(getInterfaceId());
        request.setWaitUntilIdle(waitUntilIdle);
        CompletableFuture<zaber.motion.requests.DeviceOnAllResponse> response = Call.callAsync(
            "device/home_all",
            request,
            zaber.motion.requests.DeviceOnAllResponse.parser());
        return response
            .thenApply(r -> r.getDeviceAddresses());
    }

    /**
     * Homes all of the devices on this connection.
     * @return A CompletableFuture that can be completed to get the result:
     * The addresses of the devices that were homed by this command.
     */
    public CompletableFuture<int[]> homeAllAsync() {
        return homeAllAsync(true);
    }

    /**
     * Homes all of the devices on this connection.
     * @param waitUntilIdle Determines whether the function should return immediately
     * or wait until the devices are homed.
     * @return The addresses of the devices that were homed by this command.
     */
    public int[] homeAll(
        boolean waitUntilIdle) {
        try {
            return homeAllAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Homes all of the devices on this connection.
     * @return The addresses of the devices that were homed by this command.
     */
    public int[] homeAll() {
        return homeAll(true);
    }

    /**
     * Returns a string that represents the connection.
     * @return A string that represents the connection.
     */
    public String toString() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "interface/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns default request timeout.
     * @return Default request timeout.
     */
    private int retrieveTimeout() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        zaber.motion.requests.IntResponse response = Call.callSync(
            "interface/get_timeout",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response.getValue();
    }


    /**
     * Sets default request timeout.
     * @param timeout Default request timeout.
     */
    private void changeTimeout(
        int timeout) {
        zaber.motion.requests.SetInterfaceTimeoutRequest request =
            new zaber.motion.requests.SetInterfaceTimeoutRequest();
        request.setInterfaceId(getInterfaceId());
        request.setTimeout(timeout);
        Call.callSync("interface/set_timeout", request, null);
    }


    /**
     * Returns checksum enabled.
     * @return Checksum enabled.
     */
    private boolean retrieveChecksumEnabled() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        zaber.motion.requests.BoolResponse response = Call.callSync(
            "interface/get_checksum_enabled",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response.getValue();
    }


    /**
     * Sets checksum enabled.
     * @param isEnabled Checksum enabled.
     */
    private void changeChecksumEnabled(
        boolean isEnabled) {
        zaber.motion.requests.SetInterfaceChecksumEnabledRequest request =
            new zaber.motion.requests.SetInterfaceChecksumEnabledRequest();
        request.setInterfaceId(getInterfaceId());
        request.setIsEnabled(isEnabled);
        Call.callSync("interface/set_checksum_enabled", request, null);
    }


    private void subscribe() {
        this.unknownResponse = Events.getEventObservable()
            .takeUntil(this.disconnected)
            .filter(event -> event.getEventName().equals("interface/unknown_response"))
            .map(event -> (UnknownResponseEventWrapper) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .map(event -> event.getUnknownResponse());

        this.alert = Events.getEventObservable()
            .takeUntil(this.disconnected)
            .filter(event -> event.getEventName().equals("interface/alert"))
            .map(event -> (AlertEventWrapper) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .map(event -> event.getAlert());

        Events.getEventObservable()
            .filter(event -> event.getEventName().equals("interface/disconnected"))
            .map(event -> (DisconnectedEvent) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .take(1)
            .map(event -> ExceptionConverter.convert(event.getErrorType(), event.getErrorMessage()))
            .subscribe(this.disconnected);
    }

}
