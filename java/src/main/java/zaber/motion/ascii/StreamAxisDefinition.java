/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Defines an axis of the stream.
 */
public final class StreamAxisDefinition implements zaber.motion.dto.Message {

    private int axisNumber;

    /**
     * Number of a physical axis or a lockstep group.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * Number of a physical axis or a lockstep group.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * Number of a physical axis or a lockstep group.
     */
    public StreamAxisDefinition withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private StreamAxisType axisType;

    /**
     * Defines the type of the axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public void setAxisType(StreamAxisType axisType) {
        this.axisType = axisType;
    }

    /**
     * Defines the type of the axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public StreamAxisType getAxisType() {
        return this.axisType;
    }

    /**
     * Defines the type of the axis.
     */
    public StreamAxisDefinition withAxisType(StreamAxisType aAxisType) {
        this.setAxisType(aAxisType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public StreamAxisDefinition() {
    }

    /**
     * Constructor with all properties.
     */
    public StreamAxisDefinition(
        int axisNumber,
        StreamAxisType axisType
    ) {
        this.axisNumber = axisNumber;
        this.axisType = axisType;
    }

    /**
     * Constructor with only required properties.
     */
    public StreamAxisDefinition(
        int axisNumber
    ) {
        this.axisNumber = axisNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        StreamAxisDefinition other = (StreamAxisDefinition) obj;

        return (
            EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(axisType, other.axisType)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(axisType)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StreamAxisDefinition { ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("axisType: ");
        sb.append(this.axisType);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static StreamAxisDefinition fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, StreamAxisDefinition.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<StreamAxisDefinition> PARSER =
        new zaber.motion.dto.Parser<StreamAxisDefinition>() {
            @Override
            public StreamAxisDefinition fromByteArray(byte[] data) {
                return StreamAxisDefinition.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<StreamAxisDefinition> parser() {
        return PARSER;
    }

}
