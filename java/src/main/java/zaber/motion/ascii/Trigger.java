// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;


/**
 * A handle for a trigger with this number on the device.
 * Triggers allow setting up actions that occur when a certain condition has been met or an event has occurred.
 * Please note that the Triggers API is currently an experimental feature.
 * Requires at least Firmware 7.06.
 */
public class Trigger {
    private Device device;

    /**
     * @return Device that this trigger belongs to.
     */
    public Device getDevice() {
        return this.device;
    }

    private int triggerNumber;

    /**
     * @return Number of this trigger.
     */
    public int getTriggerNumber() {
        return this.triggerNumber;
    }

    public Trigger(
        Device device, int triggerNumber) {
        this.device = device;
        this.triggerNumber = triggerNumber;
    }

    /**
     * Enables the trigger.
     * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
     * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
     * @param count Number of times the trigger will fire before disabling itself.
     * If count is not specified, or 0, the trigger will fire indefinitely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAsync(
        int count) {
        if (count < 0) {
            throw new IllegalArgumentException("Invalid value; count must be 0 or positive.");
        }
        zaber.motion.requests.TriggerEnableRequest request =
            new zaber.motion.requests.TriggerEnableRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setCount(count);
        return Call.callAsync("trigger/enable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Enables the trigger.
     * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
     * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAsync() {
        return enableAsync(0);
    }

    /**
     * Enables the trigger.
     * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
     * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
     * @param count Number of times the trigger will fire before disabling itself.
     * If count is not specified, or 0, the trigger will fire indefinitely.
     */
    public void enable(
        int count) {
        try {
            enableAsync(count).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Enables the trigger.
     * Once a trigger is enabled, it will fire whenever its condition transitions from false to true.
     * If a trigger condition is true when a disabled trigger is enabled, the trigger will fire immediately.
     */
    public void enable() {
        enable(0);
    }

    /**
     * Disables the trigger.
     * Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableAsync() {
        zaber.motion.requests.TriggerEmptyRequest request =
            new zaber.motion.requests.TriggerEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        return Call.callAsync("trigger/disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables the trigger.
     * Once disabled, the trigger will not fire and trigger actions will not run, even if trigger conditions are met.
     */
    public void disable() {
        try {
            disableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the state of the trigger.
     * @return A CompletableFuture that can be completed to get the result:
     * Complete state of the trigger.
     */
    public CompletableFuture<TriggerState> getStateAsync() {
        zaber.motion.requests.TriggerEmptyRequest request =
            new zaber.motion.requests.TriggerEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        CompletableFuture<TriggerState> response = Call.callAsync(
            "trigger/get_state",
            request,
            TriggerState.parser());
        return response;
    }

    /**
     * Gets the state of the trigger.
     * @return Complete state of the trigger.
     */
    public TriggerState getState() {
        try {
            return getStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the enabled state of the trigger.
     * @return A CompletableFuture that can be completed to get the result:
     * Whether the trigger is enabled and the number of times it will fire.
     */
    public CompletableFuture<TriggerEnabledState> getEnabledStateAsync() {
        zaber.motion.requests.TriggerEmptyRequest request =
            new zaber.motion.requests.TriggerEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        CompletableFuture<TriggerEnabledState> response = Call.callAsync(
            "trigger/get_enabled_state",
            request,
            TriggerEnabledState.parser());
        return response;
    }

    /**
     * Gets the enabled state of the trigger.
     * @return Whether the trigger is enabled and the number of times it will fire.
     */
    public TriggerEnabledState getEnabledState() {
        try {
            return getEnabledStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a generic trigger condition.
     * @param condition The condition to set for this trigger.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenAsync(
        String condition) {
        zaber.motion.requests.TriggerFireWhenRequest request =
            new zaber.motion.requests.TriggerFireWhenRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setCondition(condition);
        return Call.callAsync("trigger/fire_when", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a generic trigger condition.
     * @param condition The condition to set for this trigger.
     */
    public void fireWhen(
        String condition) {
        try {
            fireWhenAsync(condition).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition for when an encoder position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured encoder distance between trigger fires.
     * @param unit Units of dist.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenEncoderDistanceTravelledAsync(
        int axis,
        double distance,
        Units unit) {
        if (distance <= 0) {
            throw new IllegalArgumentException("Invalid value; encoder distance must be a positive value.");
        }
        zaber.motion.requests.TriggerFireWhenDistanceTravelledRequest request =
            new zaber.motion.requests.TriggerFireWhenDistanceTravelledRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAxis(axis);
        request.setDistance(distance);
        request.setUnit(unit);
        return Call.callAsync("trigger/fire_when_encoder_distance_travelled", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition for when an encoder position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured encoder distance between trigger fires.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenEncoderDistanceTravelledAsync(
        int axis,
        double distance) {
        return fireWhenEncoderDistanceTravelledAsync(axis, distance, Units.NATIVE);
    }

    /**
     * Set a trigger condition for when an encoder position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured encoder distance between trigger fires.
     * @param unit Units of dist.
     */
    public void fireWhenEncoderDistanceTravelled(
        int axis,
        double distance,
        Units unit) {
        try {
            fireWhenEncoderDistanceTravelledAsync(axis, distance, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition for when an encoder position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured encoder distance between trigger fires.
     */
    public void fireWhenEncoderDistanceTravelled(
        int axis,
        double distance) {
        fireWhenEncoderDistanceTravelled(axis, distance, Units.NATIVE);
    }

    /**
     * Set a trigger condition for when an axis position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured distance between trigger fires.
     * @param unit Units of dist.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenDistanceTravelledAsync(
        int axis,
        double distance,
        Units unit) {
        if (distance <= 0) {
            throw new IllegalArgumentException("Invalid value; distance must be a positive value.");
        }
        zaber.motion.requests.TriggerFireWhenDistanceTravelledRequest request =
            new zaber.motion.requests.TriggerFireWhenDistanceTravelledRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAxis(axis);
        request.setDistance(distance);
        request.setUnit(unit);
        return Call.callAsync("trigger/fire_when_distance_travelled", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition for when an axis position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured distance between trigger fires.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenDistanceTravelledAsync(
        int axis,
        double distance) {
        return fireWhenDistanceTravelledAsync(axis, distance, Units.NATIVE);
    }

    /**
     * Set a trigger condition for when an axis position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured distance between trigger fires.
     * @param unit Units of dist.
     */
    public void fireWhenDistanceTravelled(
        int axis,
        double distance,
        Units unit) {
        try {
            fireWhenDistanceTravelledAsync(axis, distance, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition for when an axis position has changed by a specific distance.
     * @param axis The axis to monitor for this condition.
     * May be set to 0 on single-axis devices only.
     * @param distance The measured distance between trigger fires.
     */
    public void fireWhenDistanceTravelled(
        int axis,
        double distance) {
        fireWhenDistanceTravelled(axis, distance, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on an IO channel value.
     * @param portType The type of IO channel to monitor.
     * @param channel The IO channel to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenIoAsync(
        IoPortType portType,
        int channel,
        TriggerCondition triggerCondition,
        double value) {
        if (channel <= 0) {
            throw new IllegalArgumentException("Invalid value; channel must be a positive value.");
        }
        zaber.motion.requests.TriggerFireWhenIoRequest request =
            new zaber.motion.requests.TriggerFireWhenIoRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setPortType(portType);
        request.setChannel(channel);
        request.setTriggerCondition(triggerCondition);
        request.setValue(value);
        return Call.callAsync("trigger/fire_when_io", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition based on an IO channel value.
     * @param portType The type of IO channel to monitor.
     * @param channel The IO channel to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     */
    public void fireWhenIo(
        IoPortType portType,
        int channel,
        TriggerCondition triggerCondition,
        double value) {
        try {
            fireWhenIoAsync(portType, channel, triggerCondition, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition based on a setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @param unit Units of value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenSettingAsync(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value,
        Units unit) {
        zaber.motion.requests.TriggerFireWhenSettingRequest request =
            new zaber.motion.requests.TriggerFireWhenSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAxis(axis);
        request.setSetting(setting);
        request.setTriggerCondition(triggerCondition);
        request.setValue(value);
        request.setUnit(unit);
        return Call.callAsync("trigger/fire_when_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition based on a setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenSettingAsync(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value) {
        return fireWhenSettingAsync(axis, setting, triggerCondition, value, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on a setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @param unit Units of value.
     */
    public void fireWhenSetting(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value,
        Units unit) {
        try {
            fireWhenSettingAsync(axis, setting, triggerCondition, value, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition based on a setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     */
    public void fireWhenSetting(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value) {
        fireWhenSetting(axis, setting, triggerCondition, value, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on an absolute setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @param unit Units of value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenAbsoluteSettingAsync(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value,
        Units unit) {
        zaber.motion.requests.TriggerFireWhenSettingRequest request =
            new zaber.motion.requests.TriggerFireWhenSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAxis(axis);
        request.setSetting(setting);
        request.setTriggerCondition(triggerCondition);
        request.setValue(value);
        request.setUnit(unit);
        return Call.callAsync("trigger/fire_when_setting_absolute", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition based on an absolute setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireWhenAbsoluteSettingAsync(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value) {
        return fireWhenAbsoluteSettingAsync(axis, setting, triggerCondition, value, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on an absolute setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     * @param unit Units of value.
     */
    public void fireWhenAbsoluteSetting(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value,
        Units unit) {
        try {
            fireWhenAbsoluteSettingAsync(axis, setting, triggerCondition, value, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition based on an absolute setting value.
     * @param axis The axis to monitor for this condition.
     * Set to 0 for device-scope settings.
     * @param setting The setting to monitor.
     * @param triggerCondition Comparison operator.
     * @param value Comparison value.
     */
    public void fireWhenAbsoluteSetting(
        int axis,
        String setting,
        TriggerCondition triggerCondition,
        double value) {
        fireWhenAbsoluteSetting(axis, setting, triggerCondition, value, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on a time interval.
     * @param interval The time interval between trigger fires.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireAtIntervalAsync(
        double interval,
        Units unit) {
        if (interval <= 0) {
            throw new IllegalArgumentException("Invalid value; interval must be a positive value.");
        }
        zaber.motion.requests.TriggerFireAtIntervalRequest request =
            new zaber.motion.requests.TriggerFireAtIntervalRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setInterval(interval);
        request.setUnit(unit);
        return Call.callAsync("trigger/fire_at_interval", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger condition based on a time interval.
     * @param interval The time interval between trigger fires.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> fireAtIntervalAsync(
        double interval) {
        return fireAtIntervalAsync(interval, Units.NATIVE);
    }

    /**
     * Set a trigger condition based on a time interval.
     * @param interval The time interval between trigger fires.
     * @param unit Units of time.
     */
    public void fireAtInterval(
        double interval,
        Units unit) {
        try {
            fireAtIntervalAsync(interval, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger condition based on a time interval.
     * @param interval The time interval between trigger fires.
     */
    public void fireAtInterval(
        double interval) {
        fireAtInterval(interval, Units.NATIVE);
    }

    /**
     * Set a command to be a trigger action.
     * @param action The action number to assign the command to.
     * @param axis The axis to on which to run this command.
     * Set to 0 for device-scope settings or to run command on all axes.
     * @param command The command to run when the action is triggered.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onFireAsync(
        TriggerAction action,
        int axis,
        String command) {
        zaber.motion.requests.TriggerOnFireRequest request =
            new zaber.motion.requests.TriggerOnFireRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAction(action);
        request.setAxis(axis);
        request.setCommand(command);
        return Call.callAsync("trigger/on_fire", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a command to be a trigger action.
     * @param action The action number to assign the command to.
     * @param axis The axis to on which to run this command.
     * Set to 0 for device-scope settings or to run command on all axes.
     * @param command The command to run when the action is triggered.
     */
    public void onFire(
        TriggerAction action,
        int axis,
        String command) {
        try {
            onFireAsync(action, axis, command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger action to update a setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * @param operation The operation to apply to the setting.
     * @param value Operation value.
     * @param unit Units of value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onFireSetAsync(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        double value,
        Units unit) {
        zaber.motion.requests.TriggerOnFireSetRequest request =
            new zaber.motion.requests.TriggerOnFireSetRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAction(action);
        request.setAxis(axis);
        request.setSetting(setting);
        request.setOperation(operation);
        request.setValue(value);
        request.setUnit(unit);
        return Call.callAsync("trigger/on_fire_set", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger action to update a setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * @param operation The operation to apply to the setting.
     * @param value Operation value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onFireSetAsync(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        double value) {
        return onFireSetAsync(action, axis, setting, operation, value, Units.NATIVE);
    }

    /**
     * Set a trigger action to update a setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * @param operation The operation to apply to the setting.
     * @param value Operation value.
     * @param unit Units of value.
     */
    public void onFireSet(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        double value,
        Units unit) {
        try {
            onFireSetAsync(action, axis, setting, operation, value, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set a trigger action to update a setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * @param operation The operation to apply to the setting.
     * @param value Operation value.
     */
    public void onFireSet(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        double value) {
        onFireSet(action, axis, setting, operation, value, Units.NATIVE);
    }

    /**
     * Set a trigger action to update a setting with the value of another setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * Must have either integer or boolean type.
     * @param operation The operation to apply to the setting.
     * @param fromAxis The axis from which to read the setting.
     * Set to 0 to read the setting from the device.
     * @param fromSetting The name of the setting to read.
     * Must have either integer or boolean type.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onFireSetToSettingAsync(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        int fromAxis,
        String fromSetting) {
        zaber.motion.requests.TriggerOnFireSetToSettingRequest request =
            new zaber.motion.requests.TriggerOnFireSetToSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAction(action);
        request.setAxis(axis);
        request.setSetting(setting);
        request.setOperation(operation);
        request.setFromAxis(fromAxis);
        request.setFromSetting(fromSetting);
        return Call.callAsync("trigger/on_fire_set_to_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set a trigger action to update a setting with the value of another setting.
     * @param action The action number to assign the command to.
     * @param axis The axis on which to change the setting.
     * Set to 0 to change the setting for the device.
     * @param setting The name of the setting to change.
     * Must have either integer or boolean type.
     * @param operation The operation to apply to the setting.
     * @param fromAxis The axis from which to read the setting.
     * Set to 0 to read the setting from the device.
     * @param fromSetting The name of the setting to read.
     * Must have either integer or boolean type.
     */
    public void onFireSetToSetting(
        TriggerAction action,
        int axis,
        String setting,
        TriggerOperation operation,
        int fromAxis,
        String fromSetting) {
        try {
            onFireSetToSettingAsync(action, axis, setting, operation, fromAxis, fromSetting).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Clear a trigger action.
     * @param action The action number to clear.
     * The default option is to clear all actions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> clearActionAsync(
        TriggerAction action) {
        zaber.motion.requests.TriggerClearActionRequest request =
            new zaber.motion.requests.TriggerClearActionRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setAction(action);
        return Call.callAsync("trigger/clear_action", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Clear a trigger action.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> clearActionAsync() {
        return clearActionAsync(TriggerAction.ALL);
    }

    /**
     * Clear a trigger action.
     * @param action The action number to clear.
     * The default option is to clear all actions.
     */
    public void clearAction(
        TriggerAction action) {
        try {
            clearActionAsync(action).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Clear a trigger action.
     */
    public void clearAction() {
        clearAction(TriggerAction.ALL);
    }

    /**
     * Returns the label for the trigger.
     * @return A CompletableFuture that can be completed to get the result:
     * The label for the trigger.
     */
    public CompletableFuture<String> getLabelAsync() {
        zaber.motion.requests.TriggerEmptyRequest request =
            new zaber.motion.requests.TriggerEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "trigger/get_label",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the label for the trigger.
     * @return The label for the trigger.
     */
    public String getLabel() {
        try {
            return getLabelAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the label for the trigger.
     * @param label The label to set for this trigger.
     * If no value or an empty string is provided, this label is deleted.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLabelAsync(
        String label) {
        zaber.motion.requests.TriggerSetLabelRequest request =
            new zaber.motion.requests.TriggerSetLabelRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setTriggerNumber(getTriggerNumber());
        request.setLabel(label);
        return Call.callAsync("trigger/set_label", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the label for the trigger.
     * @param label The label to set for this trigger.
     * If no value or an empty string is provided, this label is deleted.
     */
    public void setLabel(
        String label) {
        try {
            setLabelAsync(label).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
