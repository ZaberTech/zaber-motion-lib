// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.CompletableFuture;

import zaber.motion.exceptions.MotionLibException;
import zaber.motion.gateway.Call;

/**
 * Class providing access to device PVT (Position-Velocity-Time) features.
 * Requires at least Firmware 7.33.
 */
public class Pvt {
    private Device device;

    /**
     * @return Device that this PVT belongs to.
     */
    public Device getDevice() {
        return this.device;
    }

    public Pvt(
        Device device) {
        this.device = device;
    }

    /**
     * Gets a PvtSequence class instance which allows you to control a particular PVT sequence on the device.
     * @param pvtId The ID of the PVT sequence to control. The IDs start at 1.
     * @return PvtSequence instance.
     */
    public PvtSequence getSequence(
        int pvtId) {
        if (pvtId <= 0) {
            throw new IllegalArgumentException("Invalid value; PVT sequences are numbered from 1.");
        }
        return new PvtSequence(this.device, pvtId);
    }


    /**
     * Gets a PvtBuffer class instance which is a handle for a PVT buffer on the device.
     * @param pvtBufferId The ID of the PVT buffer to control. PVT buffer IDs start at one.
     * @return PvtBuffer instance.
     */
    public PvtBuffer getBuffer(
        int pvtBufferId) {
        if (pvtBufferId <= 0) {
            throw new IllegalArgumentException("Invalid value; PVT buffers are numbered from 1.");
        }
        return new PvtBuffer(this.device, pvtBufferId);
    }


    /**
     * Get a list of buffer IDs that are currently in use.
     * @return A CompletableFuture that can be completed to get the result:
     * List of buffer IDs.
     */
    public CompletableFuture<int[]> listBufferIdsAsync() {
        zaber.motion.requests.StreamBufferList request =
            new zaber.motion.requests.StreamBufferList();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setPvt(true);
        CompletableFuture<zaber.motion.requests.IntArrayResponse> response = Call.callAsync(
            "device/stream_buffer_list",
            request,
            zaber.motion.requests.IntArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Get a list of buffer IDs that are currently in use.
     * @return List of buffer IDs.
     */
    public int[] listBufferIds() {
        try {
            return listBufferIdsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
