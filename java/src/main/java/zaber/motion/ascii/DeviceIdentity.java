/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.FirmwareVersion;
import zaber.motion.EqualityUtility;


/**
 * Representation of data gathered during device identification.
 */
public final class DeviceIdentity implements zaber.motion.dto.Message {

    private int deviceId;

    /**
     * Unique ID of the device hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Unique ID of the device hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public int getDeviceId() {
        return this.deviceId;
    }

    /**
     * Unique ID of the device hardware.
     */
    public DeviceIdentity withDeviceId(int aDeviceId) {
        this.setDeviceId(aDeviceId);
        return this;
    }

    private long serialNumber;

    /**
     * Serial number of the device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("serialNumber")
    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Serial number of the device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("serialNumber")
    public long getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * Serial number of the device.
     */
    public DeviceIdentity withSerialNumber(long aSerialNumber) {
        this.setSerialNumber(aSerialNumber);
        return this;
    }

    private String name;

    /**
     * Name of the product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name of the product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * Name of the product.
     */
    public DeviceIdentity withName(String aName) {
        this.setName(aName);
        return this;
    }

    private int axisCount;

    /**
     * Number of axes this device has.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisCount")
    public void setAxisCount(int axisCount) {
        this.axisCount = axisCount;
    }

    /**
     * Number of axes this device has.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisCount")
    public int getAxisCount() {
        return this.axisCount;
    }

    /**
     * Number of axes this device has.
     */
    public DeviceIdentity withAxisCount(int aAxisCount) {
        this.setAxisCount(aAxisCount);
        return this;
    }

    private FirmwareVersion firmwareVersion;

    /**
     * Version of the firmware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public void setFirmwareVersion(FirmwareVersion firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    /**
     * Version of the firmware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public FirmwareVersion getFirmwareVersion() {
        return this.firmwareVersion;
    }

    /**
     * Version of the firmware.
     */
    public DeviceIdentity withFirmwareVersion(FirmwareVersion aFirmwareVersion) {
        this.setFirmwareVersion(aFirmwareVersion);
        return this;
    }

    private boolean isModified;

    /**
     * The device has hardware modifications.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isModified")
    public void setIsModified(boolean isModified) {
        this.isModified = isModified;
    }

    /**
     * The device has hardware modifications.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isModified")
    public boolean getIsModified() {
        return this.isModified;
    }

    /**
     * The device has hardware modifications.
     */
    public DeviceIdentity withIsModified(boolean aIsModified) {
        this.setIsModified(aIsModified);
        return this;
    }

    private boolean isIntegrated;

    /**
     * The device is an integrated product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isIntegrated")
    public void setIsIntegrated(boolean isIntegrated) {
        this.isIntegrated = isIntegrated;
    }

    /**
     * The device is an integrated product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isIntegrated")
    public boolean getIsIntegrated() {
        return this.isIntegrated;
    }

    /**
     * The device is an integrated product.
     */
    public DeviceIdentity withIsIntegrated(boolean aIsIntegrated) {
        this.setIsIntegrated(aIsIntegrated);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceIdentity() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceIdentity(
        int deviceId,
        long serialNumber,
        String name,
        int axisCount,
        FirmwareVersion firmwareVersion,
        boolean isModified,
        boolean isIntegrated
    ) {
        this.deviceId = deviceId;
        this.serialNumber = serialNumber;
        this.name = name;
        this.axisCount = axisCount;
        this.firmwareVersion = firmwareVersion;
        this.isModified = isModified;
        this.isIntegrated = isIntegrated;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceIdentity other = (DeviceIdentity) obj;

        return (
            EqualityUtility.equals(deviceId, other.deviceId)
            && EqualityUtility.equals(serialNumber, other.serialNumber)
            && EqualityUtility.equals(name, other.name)
            && EqualityUtility.equals(axisCount, other.axisCount)
            && EqualityUtility.equals(firmwareVersion, other.firmwareVersion)
            && EqualityUtility.equals(isModified, other.isModified)
            && EqualityUtility.equals(isIntegrated, other.isIntegrated)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceId),
            EqualityUtility.generateHashCode(serialNumber),
            EqualityUtility.generateHashCode(name),
            EqualityUtility.generateHashCode(axisCount),
            EqualityUtility.generateHashCode(firmwareVersion),
            EqualityUtility.generateHashCode(isModified),
            EqualityUtility.generateHashCode(isIntegrated)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceIdentity { ");
        sb.append("deviceId: ");
        sb.append(this.deviceId);
        sb.append(", ");
        sb.append("serialNumber: ");
        sb.append(this.serialNumber);
        sb.append(", ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("axisCount: ");
        sb.append(this.axisCount);
        sb.append(", ");
        sb.append("firmwareVersion: ");
        sb.append(this.firmwareVersion);
        sb.append(", ");
        sb.append("isModified: ");
        sb.append(this.isModified);
        sb.append(", ");
        sb.append("isIntegrated: ");
        sb.append(this.isIntegrated);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceIdentity fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceIdentity.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceIdentity> PARSER =
        new zaber.motion.dto.Parser<DeviceIdentity>() {
            @Override
            public DeviceIdentity fromByteArray(byte[] data) {
                return DeviceIdentity.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceIdentity> parser() {
        return PARSER;
    }

}
