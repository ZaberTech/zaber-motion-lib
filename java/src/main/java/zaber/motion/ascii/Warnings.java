// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.ArrayUtility;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class used to check and reset warnings and faults on device or axis.
 */
public class Warnings {
    private Device device;

    private int axisNumber;

    public Warnings(
        Device device, int axisNumber) {
        this.device = device;
        this.axisNumber = axisNumber;
    }

    /**
     * Returns current warnings and faults on axis or device.
     * @return A CompletableFuture that can be completed to get the result:
     * Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.
     */
    public CompletableFuture<Set<String>> getFlagsAsync() {
        zaber.motion.requests.DeviceGetWarningsRequest request =
            new zaber.motion.requests.DeviceGetWarningsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAxis(this.axisNumber);
        request.setClear(false);
        CompletableFuture<zaber.motion.requests.DeviceGetWarningsResponse> response = Call.callAsync(
            "device/get_warnings",
            request,
            zaber.motion.requests.DeviceGetWarningsResponse.parser());
        return response
            .thenApply(r -> new HashSet<String>(ArrayUtility.asList(r.getFlags())));
    }

    /**
     * Returns current warnings and faults on axis or device.
     * @return Retrieved warnings and faults. Refer to WarningFlags to check a particular flag.
     */
    public Set<String> getFlags() {
        try {
            return getFlagsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Clears (acknowledges) current warnings and faults on axis or device and returns them.
     * @return A CompletableFuture that can be completed to get the result:
     * Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.
     */
    public CompletableFuture<Set<String>> clearFlagsAsync() {
        zaber.motion.requests.DeviceGetWarningsRequest request =
            new zaber.motion.requests.DeviceGetWarningsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAxis(this.axisNumber);
        request.setClear(true);
        CompletableFuture<zaber.motion.requests.DeviceGetWarningsResponse> response = Call.callAsync(
            "device/get_warnings",
            request,
            zaber.motion.requests.DeviceGetWarningsResponse.parser());
        return response
            .thenApply(r -> new HashSet<String>(ArrayUtility.asList(r.getFlags())));
    }

    /**
     * Clears (acknowledges) current warnings and faults on axis or device and returns them.
     * @return Warnings and faults before clearing. Refer to WarningFlags to check a particular flag.
     */
    public Set<String> clearFlags() {
        try {
            return clearFlagsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits for the specified flags to clear.
     * Use for warnings flags that clear on their own.
     * Does not clear clearable warnings flags.
     * Throws TimeoutException if the flags don't clear in the specified time.
     * @param timeout For how long to wait in milliseconds for the flags to clear.
     * @param warningFlags The specific warning flags for which to wait to clear.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitToClearAsync(
        double timeout,
        String... warningFlags) {
        zaber.motion.requests.WaitToClearWarningsRequest request =
            new zaber.motion.requests.WaitToClearWarningsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAxis(this.axisNumber);
        request.setTimeout(timeout);
        request.setWarningFlags(warningFlags);
        return Call.callAsync("device/wait_to_clear_warnings", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits for the specified flags to clear.
     * Use for warnings flags that clear on their own.
     * Does not clear clearable warnings flags.
     * Throws TimeoutException if the flags don't clear in the specified time.
     * @param timeout For how long to wait in milliseconds for the flags to clear.
     * @param warningFlags The specific warning flags for which to wait to clear.
     */
    public void waitToClear(
        double timeout,
        String... warningFlags) {
        try {
            waitToClearAsync(timeout, warningFlags).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
