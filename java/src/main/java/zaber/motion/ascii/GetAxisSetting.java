/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


/**
 * Specifies a setting to get with one of the multi-get commands.
 */
public final class GetAxisSetting implements zaber.motion.dto.Message {

    private String setting;

    /**
     * The setting to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * The setting to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * The setting to read.
     */
    public GetAxisSetting withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private Units unit;

    /**
     * The unit to convert the read setting to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * The unit to convert the read setting to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * The unit to convert the read setting to.
     */
    public GetAxisSetting withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetAxisSetting() {
    }

    /**
     * Constructor with all properties.
     */
    public GetAxisSetting(
        String setting,
        Units unit
    ) {
        this.setting = setting;
        this.unit = unit;
    }

    /**
     * Constructor with only required properties.
     */
    public GetAxisSetting(
        String setting
    ) {
        this.setting = setting;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetAxisSetting other = (GetAxisSetting) obj;

        return (
            EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetAxisSetting { ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetAxisSetting fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetAxisSetting.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetAxisSetting> PARSER =
        new zaber.motion.dto.Parser<GetAxisSetting>() {
            @Override
            public GetAxisSetting fromByteArray(byte[] data) {
                return GetAxisSetting.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetAxisSetting> parser() {
        return PARSER;
    }

}
