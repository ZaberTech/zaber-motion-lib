/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


/**
 * Specifies a setting to get with one of the multi-get commands.
 */
public final class GetSetting implements zaber.motion.dto.Message {

    private String setting;

    /**
     * The setting to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * The setting to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * The setting to read.
     */
    public GetSetting withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private int[] axes;

    /**
     * The list of axes to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public void setAxes(int[] axes) {
        this.axes = axes;
    }

    /**
     * The list of axes to read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axes")
    public int[] getAxes() {
        return this.axes;
    }

    /**
     * The list of axes to read.
     */
    public GetSetting withAxes(int[] aAxes) {
        this.setAxes(aAxes);
        return this;
    }

    private Units unit;

    /**
     * The unit to convert the read settings to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * The unit to convert the read settings to.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * The unit to convert the read settings to.
     */
    public GetSetting withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetSetting() {
    }

    /**
     * Constructor with all properties.
     */
    public GetSetting(
        String setting,
        int[] axes,
        Units unit
    ) {
        this.setting = setting;
        this.axes = axes;
        this.unit = unit;
    }

    /**
     * Constructor with only required properties.
     */
    public GetSetting(
        String setting
    ) {
        this.setting = setting;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetSetting other = (GetSetting) obj;

        return (
            EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(axes, other.axes)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(axes),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetSetting { ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("axes: ");
        sb.append(this.axes);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetSetting fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetSetting.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetSetting> PARSER =
        new zaber.motion.dto.Parser<GetSetting>() {
            @Override
            public GetSetting fromByteArray(byte[] data) {
                return GetSetting.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetSetting> parser() {
        return PARSER;
    }

}
