/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Defines an axis of the PVT sequence.
 */
public final class PvtAxisDefinition implements zaber.motion.dto.Message {

    private int axisNumber;

    /**
     * Number of a physical axis or a lockstep group.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * Number of a physical axis or a lockstep group.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * Number of a physical axis or a lockstep group.
     */
    public PvtAxisDefinition withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    private PvtAxisType axisType;

    /**
     * Defines the type of the axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public void setAxisType(PvtAxisType axisType) {
        this.axisType = axisType;
    }

    /**
     * Defines the type of the axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public PvtAxisType getAxisType() {
        return this.axisType;
    }

    /**
     * Defines the type of the axis.
     */
    public PvtAxisDefinition withAxisType(PvtAxisType aAxisType) {
        this.setAxisType(aAxisType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public PvtAxisDefinition() {
    }

    /**
     * Constructor with all properties.
     */
    public PvtAxisDefinition(
        int axisNumber,
        PvtAxisType axisType
    ) {
        this.axisNumber = axisNumber;
        this.axisType = axisType;
    }

    /**
     * Constructor with only required properties.
     */
    public PvtAxisDefinition(
        int axisNumber
    ) {
        this.axisNumber = axisNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PvtAxisDefinition other = (PvtAxisDefinition) obj;

        return (
            EqualityUtility.equals(axisNumber, other.axisNumber)
            && EqualityUtility.equals(axisType, other.axisType)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axisNumber),
            EqualityUtility.generateHashCode(axisType)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PvtAxisDefinition { ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(", ");
        sb.append("axisType: ");
        sb.append(this.axisType);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static PvtAxisDefinition fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, PvtAxisDefinition.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<PvtAxisDefinition> PARSER =
        new zaber.motion.dto.Parser<PvtAxisDefinition>() {
            @Override
            public PvtAxisDefinition fromByteArray(byte[] data) {
                return PvtAxisDefinition.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<PvtAxisDefinition> parser() {
        return PARSER;
    }

}
