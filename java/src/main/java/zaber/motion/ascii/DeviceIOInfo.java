/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Class representing information on the I/O channels of the device.
 */
public final class DeviceIOInfo implements zaber.motion.dto.Message {

    private int numberAnalogOutputs;

    /**
     * Number of analog output channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberAnalogOutputs")
    public void setNumberAnalogOutputs(int numberAnalogOutputs) {
        this.numberAnalogOutputs = numberAnalogOutputs;
    }

    /**
     * Number of analog output channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberAnalogOutputs")
    public int getNumberAnalogOutputs() {
        return this.numberAnalogOutputs;
    }

    /**
     * Number of analog output channels.
     */
    public DeviceIOInfo withNumberAnalogOutputs(int aNumberAnalogOutputs) {
        this.setNumberAnalogOutputs(aNumberAnalogOutputs);
        return this;
    }

    private int numberAnalogInputs;

    /**
     * Number of analog input channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberAnalogInputs")
    public void setNumberAnalogInputs(int numberAnalogInputs) {
        this.numberAnalogInputs = numberAnalogInputs;
    }

    /**
     * Number of analog input channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberAnalogInputs")
    public int getNumberAnalogInputs() {
        return this.numberAnalogInputs;
    }

    /**
     * Number of analog input channels.
     */
    public DeviceIOInfo withNumberAnalogInputs(int aNumberAnalogInputs) {
        this.setNumberAnalogInputs(aNumberAnalogInputs);
        return this;
    }

    private int numberDigitalOutputs;

    /**
     * Number of digital output channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberDigitalOutputs")
    public void setNumberDigitalOutputs(int numberDigitalOutputs) {
        this.numberDigitalOutputs = numberDigitalOutputs;
    }

    /**
     * Number of digital output channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberDigitalOutputs")
    public int getNumberDigitalOutputs() {
        return this.numberDigitalOutputs;
    }

    /**
     * Number of digital output channels.
     */
    public DeviceIOInfo withNumberDigitalOutputs(int aNumberDigitalOutputs) {
        this.setNumberDigitalOutputs(aNumberDigitalOutputs);
        return this;
    }

    private int numberDigitalInputs;

    /**
     * Number of digital input channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberDigitalInputs")
    public void setNumberDigitalInputs(int numberDigitalInputs) {
        this.numberDigitalInputs = numberDigitalInputs;
    }

    /**
     * Number of digital input channels.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("numberDigitalInputs")
    public int getNumberDigitalInputs() {
        return this.numberDigitalInputs;
    }

    /**
     * Number of digital input channels.
     */
    public DeviceIOInfo withNumberDigitalInputs(int aNumberDigitalInputs) {
        this.setNumberDigitalInputs(aNumberDigitalInputs);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceIOInfo() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceIOInfo(
        int numberAnalogOutputs,
        int numberAnalogInputs,
        int numberDigitalOutputs,
        int numberDigitalInputs
    ) {
        this.numberAnalogOutputs = numberAnalogOutputs;
        this.numberAnalogInputs = numberAnalogInputs;
        this.numberDigitalOutputs = numberDigitalOutputs;
        this.numberDigitalInputs = numberDigitalInputs;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceIOInfo other = (DeviceIOInfo) obj;

        return (
            EqualityUtility.equals(numberAnalogOutputs, other.numberAnalogOutputs)
            && EqualityUtility.equals(numberAnalogInputs, other.numberAnalogInputs)
            && EqualityUtility.equals(numberDigitalOutputs, other.numberDigitalOutputs)
            && EqualityUtility.equals(numberDigitalInputs, other.numberDigitalInputs)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(numberAnalogOutputs),
            EqualityUtility.generateHashCode(numberAnalogInputs),
            EqualityUtility.generateHashCode(numberDigitalOutputs),
            EqualityUtility.generateHashCode(numberDigitalInputs)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceIOInfo { ");
        sb.append("numberAnalogOutputs: ");
        sb.append(this.numberAnalogOutputs);
        sb.append(", ");
        sb.append("numberAnalogInputs: ");
        sb.append(this.numberAnalogInputs);
        sb.append(", ");
        sb.append("numberDigitalOutputs: ");
        sb.append(this.numberDigitalOutputs);
        sb.append(", ");
        sb.append("numberDigitalInputs: ");
        sb.append(this.numberDigitalInputs);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceIOInfo fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceIOInfo.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceIOInfo> PARSER =
        new zaber.motion.dto.Parser<DeviceIOInfo>() {
            @Override
            public DeviceIOInfo fromByteArray(byte[] data) {
                return DeviceIOInfo.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceIOInfo> parser() {
        return PARSER;
    }

}
