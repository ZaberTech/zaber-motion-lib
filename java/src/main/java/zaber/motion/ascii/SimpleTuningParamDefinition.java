/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Information about a parameter used for the simple tuning method.
 */
public final class SimpleTuningParamDefinition implements zaber.motion.dto.Message {

    private String name;

    /**
     * The name of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The name of the parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * The name of the parameter.
     */
    public SimpleTuningParamDefinition withName(String aName) {
        this.setName(aName);
        return this;
    }

    private String minLabel;

    /**
     * The human readable description of the effect of a lower value on this setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("minLabel")
    public void setMinLabel(String minLabel) {
        this.minLabel = minLabel;
    }

    /**
     * The human readable description of the effect of a lower value on this setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("minLabel")
    public String getMinLabel() {
        return this.minLabel;
    }

    /**
     * The human readable description of the effect of a lower value on this setting.
     */
    public SimpleTuningParamDefinition withMinLabel(String aMinLabel) {
        this.setMinLabel(aMinLabel);
        return this;
    }

    private String maxLabel;

    /**
     * The human readable description of the effect of a higher value on this setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("maxLabel")
    public void setMaxLabel(String maxLabel) {
        this.maxLabel = maxLabel;
    }

    /**
     * The human readable description of the effect of a higher value on this setting.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("maxLabel")
    public String getMaxLabel() {
        return this.maxLabel;
    }

    /**
     * The human readable description of the effect of a higher value on this setting.
     */
    public SimpleTuningParamDefinition withMaxLabel(String aMaxLabel) {
        this.setMaxLabel(aMaxLabel);
        return this;
    }

    private String dataType;

    /**
     * How this parameter will be parsed by the tuner.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("dataType")
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * How this parameter will be parsed by the tuner.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("dataType")
    public String getDataType() {
        return this.dataType;
    }

    /**
     * How this parameter will be parsed by the tuner.
     */
    public SimpleTuningParamDefinition withDataType(String aDataType) {
        this.setDataType(aDataType);
        return this;
    }

    private Double defaultValue;

    /**
     * The default value of this parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("defaultValue")
    public void setDefaultValue(Double defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * The default value of this parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("defaultValue")
    public Double getDefaultValue() {
        return this.defaultValue;
    }

    /**
     * The default value of this parameter.
     */
    public SimpleTuningParamDefinition withDefaultValue(Double aDefaultValue) {
        this.setDefaultValue(aDefaultValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SimpleTuningParamDefinition() {
    }

    /**
     * Constructor with all properties.
     */
    public SimpleTuningParamDefinition(
        String name,
        String minLabel,
        String maxLabel,
        String dataType,
        Double defaultValue
    ) {
        this.name = name;
        this.minLabel = minLabel;
        this.maxLabel = maxLabel;
        this.dataType = dataType;
        this.defaultValue = defaultValue;
    }

    /**
     * Constructor with only required properties.
     */
    public SimpleTuningParamDefinition(
        String name,
        String minLabel,
        String maxLabel,
        String dataType
    ) {
        this.name = name;
        this.minLabel = minLabel;
        this.maxLabel = maxLabel;
        this.dataType = dataType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SimpleTuningParamDefinition other = (SimpleTuningParamDefinition) obj;

        return (
            EqualityUtility.equals(name, other.name)
            && EqualityUtility.equals(minLabel, other.minLabel)
            && EqualityUtility.equals(maxLabel, other.maxLabel)
            && EqualityUtility.equals(dataType, other.dataType)
            && EqualityUtility.equals(defaultValue, other.defaultValue)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(name),
            EqualityUtility.generateHashCode(minLabel),
            EqualityUtility.generateHashCode(maxLabel),
            EqualityUtility.generateHashCode(dataType),
            EqualityUtility.generateHashCode(defaultValue)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SimpleTuningParamDefinition { ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("minLabel: ");
        sb.append(this.minLabel);
        sb.append(", ");
        sb.append("maxLabel: ");
        sb.append(this.maxLabel);
        sb.append(", ");
        sb.append("dataType: ");
        sb.append(this.dataType);
        sb.append(", ");
        sb.append("defaultValue: ");
        sb.append(this.defaultValue);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SimpleTuningParamDefinition fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SimpleTuningParamDefinition.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SimpleTuningParamDefinition> PARSER =
        new zaber.motion.dto.Parser<SimpleTuningParamDefinition>() {
            @Override
            public SimpleTuningParamDefinition fromByteArray(byte[] data) {
                return SimpleTuningParamDefinition.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SimpleTuningParamDefinition> parser() {
        return PARSER;
    }

}
