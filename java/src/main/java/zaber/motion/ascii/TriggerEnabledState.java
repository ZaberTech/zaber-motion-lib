/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The enabled state of a single trigger.
 * Returns whether the given trigger is enabled and the number of times it will fire.
 * This is a subset of the complete state, and is faster to query.
 */
public final class TriggerEnabledState implements zaber.motion.dto.Message {

    private boolean enabled;

    /**
     * The enabled state for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * The enabled state for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("enabled")
    public boolean getEnabled() {
        return this.enabled;
    }

    /**
     * The enabled state for a trigger.
     */
    public TriggerEnabledState withEnabled(boolean aEnabled) {
        this.setEnabled(aEnabled);
        return this;
    }

    private int firesRemaining;

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesRemaining")
    public void setFiresRemaining(int firesRemaining) {
        this.firesRemaining = firesRemaining;
    }

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesRemaining")
    public int getFiresRemaining() {
        return this.firesRemaining;
    }

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    public TriggerEnabledState withFiresRemaining(int aFiresRemaining) {
        this.setFiresRemaining(aFiresRemaining);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerEnabledState() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerEnabledState(
        boolean enabled,
        int firesRemaining
    ) {
        this.enabled = enabled;
        this.firesRemaining = firesRemaining;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerEnabledState other = (TriggerEnabledState) obj;

        return (
            EqualityUtility.equals(enabled, other.enabled)
            && EqualityUtility.equals(firesRemaining, other.firesRemaining)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(enabled),
            EqualityUtility.generateHashCode(firesRemaining)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerEnabledState { ");
        sb.append("enabled: ");
        sb.append(this.enabled);
        sb.append(", ");
        sb.append("firesRemaining: ");
        sb.append(this.firesRemaining);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerEnabledState fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerEnabledState.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerEnabledState> PARSER =
        new zaber.motion.dto.Parser<TriggerEnabledState>() {
            @Override
            public TriggerEnabledState fromByteArray(byte[] data) {
                return TriggerEnabledState.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerEnabledState> parser() {
        return PARSER;
    }

}
