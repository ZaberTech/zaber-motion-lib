/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


/**
 * The response from a multi-get command.
 */
public final class GetSettingResult implements zaber.motion.dto.Message {

    private String setting;

    /**
     * The setting read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * The setting read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * The setting read.
     */
    public GetSettingResult withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private double[] values;

    /**
     * The list of values returned.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public void setValues(double[] values) {
        this.values = values;
    }

    /**
     * The list of values returned.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("values")
    public double[] getValues() {
        return this.values;
    }

    /**
     * The list of values returned.
     */
    public GetSettingResult withValues(double[] aValues) {
        this.setValues(aValues);
        return this;
    }

    private Units unit;

    /**
     * The unit of the values.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * The unit of the values.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * The unit of the values.
     */
    public GetSettingResult withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetSettingResult() {
    }

    /**
     * Constructor with all properties.
     */
    public GetSettingResult(
        String setting,
        double[] values,
        Units unit
    ) {
        this.setting = setting;
        this.values = values;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetSettingResult other = (GetSettingResult) obj;

        return (
            EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(values, other.values)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(values),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetSettingResult { ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("values: ");
        sb.append(this.values);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetSettingResult fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetSettingResult.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetSettingResult> PARSER =
        new zaber.motion.dto.Parser<GetSettingResult>() {
            @Override
            public GetSettingResult fromByteArray(byte[] data) {
                return GetSettingResult.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetSettingResult> parser() {
        return PARSER;
    }

}
