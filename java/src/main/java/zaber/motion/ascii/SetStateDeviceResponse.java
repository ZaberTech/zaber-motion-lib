/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * An object containing any non-blocking issues encountered when loading a saved state to a device.
 */
public final class SetStateDeviceResponse implements zaber.motion.dto.Message {

    private String[] warnings;

    /**
     * The warnings encountered when applying this state to the given device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * The warnings encountered when applying this state to the given device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public String[] getWarnings() {
        return this.warnings;
    }

    /**
     * The warnings encountered when applying this state to the given device.
     */
    public SetStateDeviceResponse withWarnings(String[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    private SetStateAxisResponse[] axisResponses;

    /**
     * A list of warnings encountered when applying this state to the device's axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisResponses")
    public void setAxisResponses(SetStateAxisResponse[] axisResponses) {
        this.axisResponses = axisResponses;
    }

    /**
     * A list of warnings encountered when applying this state to the device's axes.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisResponses")
    public SetStateAxisResponse[] getAxisResponses() {
        return this.axisResponses;
    }

    /**
     * A list of warnings encountered when applying this state to the device's axes.
     */
    public SetStateDeviceResponse withAxisResponses(SetStateAxisResponse[] aAxisResponses) {
        this.setAxisResponses(aAxisResponses);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetStateDeviceResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public SetStateDeviceResponse(
        String[] warnings,
        SetStateAxisResponse[] axisResponses
    ) {
        this.warnings = warnings;
        this.axisResponses = axisResponses;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetStateDeviceResponse other = (SetStateDeviceResponse) obj;

        return (
            EqualityUtility.equals(warnings, other.warnings)
            && EqualityUtility.equals(axisResponses, other.axisResponses)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(warnings),
            EqualityUtility.generateHashCode(axisResponses)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetStateDeviceResponse { ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(", ");
        sb.append("axisResponses: ");
        sb.append(this.axisResponses);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetStateDeviceResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetStateDeviceResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetStateDeviceResponse> PARSER =
        new zaber.motion.dto.Parser<SetStateDeviceResponse>() {
            @Override
            public SetStateDeviceResponse fromByteArray(byte[] data) {
                return SetStateDeviceResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetStateDeviceResponse> parser() {
        return PARSER;
    }

}
