/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * An object containing any non-blocking issues encountered when loading a saved state to an axis.
 */
public final class SetStateAxisResponse implements zaber.motion.dto.Message {

    private String[] warnings;

    /**
     * The warnings encountered when applying this state to the given axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * The warnings encountered when applying this state to the given axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("warnings")
    public String[] getWarnings() {
        return this.warnings;
    }

    /**
     * The warnings encountered when applying this state to the given axis.
     */
    public SetStateAxisResponse withWarnings(String[] aWarnings) {
        this.setWarnings(aWarnings);
        return this;
    }

    private int axisNumber;

    /**
     * The number of the axis that was set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public void setAxisNumber(int axisNumber) {
        this.axisNumber = axisNumber;
    }

    /**
     * The number of the axis that was set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisNumber")
    public int getAxisNumber() {
        return this.axisNumber;
    }

    /**
     * The number of the axis that was set.
     */
    public SetStateAxisResponse withAxisNumber(int aAxisNumber) {
        this.setAxisNumber(aAxisNumber);
        return this;
    }

    /**
     * Empty constructor.
     */
    public SetStateAxisResponse() {
    }

    /**
     * Constructor with all properties.
     */
    public SetStateAxisResponse(
        String[] warnings,
        int axisNumber
    ) {
        this.warnings = warnings;
        this.axisNumber = axisNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SetStateAxisResponse other = (SetStateAxisResponse) obj;

        return (
            EqualityUtility.equals(warnings, other.warnings)
            && EqualityUtility.equals(axisNumber, other.axisNumber)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(warnings),
            EqualityUtility.generateHashCode(axisNumber)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SetStateAxisResponse { ");
        sb.append("warnings: ");
        sb.append(this.warnings);
        sb.append(", ");
        sb.append("axisNumber: ");
        sb.append(this.axisNumber);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static SetStateAxisResponse fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, SetStateAxisResponse.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<SetStateAxisResponse> PARSER =
        new zaber.motion.dto.Parser<SetStateAxisResponse>() {
            @Override
            public SetStateAxisResponse fromByteArray(byte[] data) {
                return SetStateAxisResponse.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<SetStateAxisResponse> parser() {
        return PARSER;
    }

}
