// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.CompletableFuture;

import zaber.motion.exceptions.MotionLibException;
import zaber.motion.gateway.Call;

/**
 * Class providing access to device streams.
 * Requires at least Firmware 7.05.
 */
public class Streams {
    private Device device;

    /**
     * @return Device that these streams belong to.
     */
    public Device getDevice() {
        return this.device;
    }

    public Streams(
        Device device) {
        this.device = device;
    }

    /**
     * Gets a Stream class instance which allows you to control a particular stream on the device.
     * @param streamId The ID of the stream to control. Stream IDs start at one.
     * @return Stream instance.
     */
    public Stream getStream(
        int streamId) {
        if (streamId <= 0) {
            throw new IllegalArgumentException("Invalid value; streams are numbered from 1.");
        }
        return new Stream(this.device, streamId);
    }


    /**
     * Gets a StreamBuffer class instance which is a handle for a stream buffer on the device.
     * @param streamBufferId The ID of the stream buffer to control. Stream buffer IDs start at one.
     * @return StreamBuffer instance.
     */
    public StreamBuffer getBuffer(
        int streamBufferId) {
        if (streamBufferId <= 0) {
            throw new IllegalArgumentException("Invalid value; stream buffers are numbered from 1.");
        }
        return new StreamBuffer(this.device, streamBufferId);
    }


    /**
     * Get a list of buffer IDs that are currently in use.
     * @return A CompletableFuture that can be completed to get the result:
     * List of buffer IDs.
     */
    public CompletableFuture<int[]> listBufferIdsAsync() {
        zaber.motion.requests.StreamBufferList request =
            new zaber.motion.requests.StreamBufferList();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setPvt(false);
        CompletableFuture<zaber.motion.requests.IntArrayResponse> response = Call.callAsync(
            "device/stream_buffer_list",
            request,
            zaber.motion.requests.IntArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Get a list of buffer IDs that are currently in use.
     * @return List of buffer IDs.
     */
    public int[] listBufferIds() {
        try {
            return listBufferIdsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
