// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents all axes of motion associated with a device.
 */
public class AllAxes {
    private Device device;

    /**
     * @return Device that controls this axis.
     */
    public Device getDevice() {
        return this.device;
    }

    public AllAxes(
        Device device) {
        this.device = device;
    }

    /**
     * Homes all axes. Axes return to their homing positions.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceHomeRequest request =
            new zaber.motion.requests.DeviceHomeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/home", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Homes all axes. Axes return to their homing positions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync() {
        return homeAsync(true);
    }

    /**
     * Homes all axes. Axes return to their homing positions.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void home(
        boolean waitUntilIdle) {
        try {
            homeAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Homes all axes. Axes return to their homing positions.
     */
    public void home() {
        home(true);
    }

    /**
     * Stops ongoing axes movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceStopRequest request =
            new zaber.motion.requests.DeviceStopRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops ongoing axes movement. Decelerates until zero speed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync() {
        return stopAsync(true);
    }

    /**
     * Stops ongoing axes movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void stop(
        boolean waitUntilIdle) {
        try {
            stopAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops ongoing axes movement. Decelerates until zero speed.
     */
    public void stop() {
        stop(true);
    }

    /**
     * Waits until all axes of device stop moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync(
        boolean throwErrorOnFault) {
        zaber.motion.requests.DeviceWaitUntilIdleRequest request =
            new zaber.motion.requests.DeviceWaitUntilIdleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        request.setThrowErrorOnFault(throwErrorOnFault);
        return Call.callAsync("device/wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until all axes of device stop moving.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        return waitUntilIdleAsync(true);
    }

    /**
     * Waits until all axes of device stop moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     */
    public void waitUntilIdle(
        boolean throwErrorOnFault) {
        try {
            waitUntilIdleAsync(throwErrorOnFault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until all axes of device stop moving.
     */
    public void waitUntilIdle() {
        waitUntilIdle(true);
    }

    /**
     * Parks the device in anticipation of turning the power off.
     * It can later be powered on, unparked, and moved without first having to home it.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> parkAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        return Call.callAsync("device/park", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Parks the device in anticipation of turning the power off.
     * It can later be powered on, unparked, and moved without first having to home it.
     */
    public void park() {
        try {
            parkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Unparks the device. The device will now be able to move.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> unparkAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        return Call.callAsync("device/unpark", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Unparks the device. The device will now be able to move.
     */
    public void unpark() {
        try {
            unparkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether any axis is executing a motion command.
     * @return A CompletableFuture that can be completed to get the result:
     * True if any axis is currently executing a motion command.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether any axis is executing a motion command.
     * @return True if any axis is currently executing a motion command.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether all axes have position reference and were homed.
     * @return A CompletableFuture that can be completed to get the result:
     * True if all axes have position reference and were homed.
     */
    public CompletableFuture<Boolean> isHomedAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/is_homed",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether all axes have position reference and were homed.
     * @return True if all axes have position reference and were homed.
     */
    public boolean isHomed() {
        try {
            return isHomedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Disables all axes drivers, which prevents current from being sent to the motor or load.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverDisableAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        return Call.callAsync("device/driver_disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables all axes drivers, which prevents current from being sent to the motor or load.
     */
    public void driverDisable() {
        try {
            driverDisableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverEnableAsync(
        double timeout) {
        zaber.motion.requests.DriverEnableRequest request =
            new zaber.motion.requests.DriverEnableRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        request.setTimeout(timeout);
        return Call.callAsync("device/driver_enable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverEnableAsync() {
        return driverEnableAsync(10);
    }

    /**
     * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
     */
    public void driverEnable(
        double timeout) {
        try {
            driverEnableAsync(timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to enable all axes drivers (where applicable) repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     */
    public void driverEnable() {
        driverEnable(10);
    }

    /**
     * Returns a string that represents the axes.
     * @return A string that represents the axes.
     */
    public String toString() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(0);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/all_axes_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
