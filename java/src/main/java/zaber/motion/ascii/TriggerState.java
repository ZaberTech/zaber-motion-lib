/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The complete state of a trigger.
 */
public final class TriggerState implements zaber.motion.dto.Message {

    private String condition;

    /**
     * The firing condition for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     * The firing condition for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("condition")
    public String getCondition() {
        return this.condition;
    }

    /**
     * The firing condition for a trigger.
     */
    public TriggerState withCondition(String aCondition) {
        this.setCondition(aCondition);
        return this;
    }

    private String[] actions;

    /**
     * The actions for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("actions")
    public void setActions(String[] actions) {
        this.actions = actions;
    }

    /**
     * The actions for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("actions")
    public String[] getActions() {
        return this.actions;
    }

    /**
     * The actions for a trigger.
     */
    public TriggerState withActions(String[] aActions) {
        this.setActions(aActions);
        return this;
    }

    private boolean enabled;

    /**
     * The enabled state for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("enabled")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * The enabled state for a trigger.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("enabled")
    public boolean getEnabled() {
        return this.enabled;
    }

    /**
     * The enabled state for a trigger.
     */
    public TriggerState withEnabled(boolean aEnabled) {
        this.setEnabled(aEnabled);
        return this;
    }

    private int firesTotal;

    /**
     * The number of total fires for this trigger.
     * A value of -1 indicates unlimited fires.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesTotal")
    public void setFiresTotal(int firesTotal) {
        this.firesTotal = firesTotal;
    }

    /**
     * The number of total fires for this trigger.
     * A value of -1 indicates unlimited fires.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesTotal")
    public int getFiresTotal() {
        return this.firesTotal;
    }

    /**
     * The number of total fires for this trigger.
     * A value of -1 indicates unlimited fires.
     */
    public TriggerState withFiresTotal(int aFiresTotal) {
        this.setFiresTotal(aFiresTotal);
        return this;
    }

    private int firesRemaining;

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesRemaining")
    public void setFiresRemaining(int firesRemaining) {
        this.firesRemaining = firesRemaining;
    }

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firesRemaining")
    public int getFiresRemaining() {
        return this.firesRemaining;
    }

    /**
     * The number of remaining fires for this trigger.
     * A value of -1 indicates unlimited fires remaining.
     */
    public TriggerState withFiresRemaining(int aFiresRemaining) {
        this.setFiresRemaining(aFiresRemaining);
        return this;
    }

    /**
     * Empty constructor.
     */
    public TriggerState() {
    }

    /**
     * Constructor with all properties.
     */
    public TriggerState(
        String condition,
        String[] actions,
        boolean enabled,
        int firesTotal,
        int firesRemaining
    ) {
        this.condition = condition;
        this.actions = actions;
        this.enabled = enabled;
        this.firesTotal = firesTotal;
        this.firesRemaining = firesRemaining;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        TriggerState other = (TriggerState) obj;

        return (
            EqualityUtility.equals(condition, other.condition)
            && EqualityUtility.equals(actions, other.actions)
            && EqualityUtility.equals(enabled, other.enabled)
            && EqualityUtility.equals(firesTotal, other.firesTotal)
            && EqualityUtility.equals(firesRemaining, other.firesRemaining)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(condition),
            EqualityUtility.generateHashCode(actions),
            EqualityUtility.generateHashCode(enabled),
            EqualityUtility.generateHashCode(firesTotal),
            EqualityUtility.generateHashCode(firesRemaining)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TriggerState { ");
        sb.append("condition: ");
        sb.append(this.condition);
        sb.append(", ");
        sb.append("actions: ");
        sb.append(this.actions);
        sb.append(", ");
        sb.append("enabled: ");
        sb.append(this.enabled);
        sb.append(", ");
        sb.append("firesTotal: ");
        sb.append(this.firesTotal);
        sb.append(", ");
        sb.append("firesRemaining: ");
        sb.append(this.firesRemaining);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static TriggerState fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, TriggerState.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<TriggerState> PARSER =
        new zaber.motion.dto.Parser<TriggerState>() {
            @Override
            public TriggerState fromByteArray(byte[] data) {
                return TriggerState.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<TriggerState> parser() {
        return PARSER;
    }

}
