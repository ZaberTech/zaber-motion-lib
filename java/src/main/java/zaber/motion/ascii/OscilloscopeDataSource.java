/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Kind of channel to record in the Oscilloscope.
 */
public enum OscilloscopeDataSource {

    SETTING(0),

    IO(1);

    private int value;

    OscilloscopeDataSource(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static OscilloscopeDataSource valueOf(int argValue) {
        for (OscilloscopeDataSource value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
