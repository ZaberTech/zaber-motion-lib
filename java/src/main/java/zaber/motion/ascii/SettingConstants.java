// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

/**
 * Named constants for all Zaber ASCII protocol settings.
 * For more information please refer to the
 * [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_settings).
 */
public final class SettingConstants {
    private SettingConstants() {
    }

    /**
    * Accel.
    */
    public static final String ACCEL = "accel";

    /**
    * Brake Mode.
    */
    public static final String BRAKE_MODE = "brake.mode";

    /**
    * Brake State.
    */
    public static final String BRAKE_STATE = "brake.state";

    /**
    * Calibration Type.
    */
    public static final String CALIBRATION_TYPE = "calibration.type";

    /**
    * Cloop Continuous Enable.
    */
    public static final String CLOOP_CONTINUOUS_ENABLE = "cloop.continuous.enable";

    /**
    * Cloop Counts.
    */
    public static final String CLOOP_COUNTS = "cloop.counts";

    /**
    * Cloop Displace Tolerance.
    */
    public static final String CLOOP_DISPLACE_TOLERANCE = "cloop.displace.tolerance";

    /**
    * Cloop Duration Max.
    */
    public static final String CLOOP_DURATION_MAX = "cloop.duration.max";

    /**
    * Cloop Enable.
    */
    public static final String CLOOP_ENABLE = "cloop.enable";

    /**
    * Cloop Following Tolerance.
    */
    public static final String CLOOP_FOLLOWING_TOLERANCE = "cloop.following.tolerance";

    /**
    * Cloop Mode.
    */
    public static final String CLOOP_MODE = "cloop.mode";

    /**
    * Cloop Recovery Enable.
    */
    public static final String CLOOP_RECOVERY_ENABLE = "cloop.recovery.enable";

    /**
    * Cloop Servo Effort.
    */
    public static final String CLOOP_SERVO_EFFORT = "cloop.servo.effort";

    /**
    * Cloop Servo Enable.
    */
    public static final String CLOOP_SERVO_ENABLE = "cloop.servo.enable";

    /**
    * Cloop Settle Period.
    */
    public static final String CLOOP_SETTLE_PERIOD = "cloop.settle.period";

    /**
    * Cloop Settle Tolerance.
    */
    public static final String CLOOP_SETTLE_TOLERANCE = "cloop.settle.tolerance";

    /**
    * Cloop Stall Action.
    */
    public static final String CLOOP_STALL_ACTION = "cloop.stall.action";

    /**
    * Cloop Stall Detect Mode.
    */
    public static final String CLOOP_STALL_DETECT_MODE = "cloop.stall.detect.mode";

    /**
    * Cloop Stall Tolerance.
    */
    public static final String CLOOP_STALL_TOLERANCE = "cloop.stall.tolerance";

    /**
    * Cloop Stalltimeout.
    */
    public static final String CLOOP_STALLTIMEOUT = "cloop.stalltimeout";

    /**
    * Cloop Steps.
    */
    public static final String CLOOP_STEPS = "cloop.steps";

    /**
    * Cloop Timeout.
    */
    public static final String CLOOP_TIMEOUT = "cloop.timeout";

    /**
    * Comm Address.
    */
    public static final String COMM_ADDRESS = "comm.address";

    /**
    * Comm Alert.
    */
    public static final String COMM_ALERT = "comm.alert";

    /**
    * Comm Checksum.
    */
    public static final String COMM_CHECKSUM = "comm.checksum";

    /**
    * Comm Command Packets Max.
    */
    public static final String COMM_COMMAND_PACKETS_MAX = "comm.command.packets.max";

    /**
    * Comm En Ipv 4 Address.
    */
    public static final String COMM_EN_IPV_4_ADDRESS = "comm.en.ipv4.address";

    /**
    * Comm En Ipv 4 Dhcp Enabled.
    */
    public static final String COMM_EN_IPV_4_DHCP_ENABLED = "comm.en.ipv4.dhcp.enabled";

    /**
    * Comm En Ipv 4 Gateway.
    */
    public static final String COMM_EN_IPV_4_GATEWAY = "comm.en.ipv4.gateway";

    /**
    * Comm En Ipv 4 Netmask.
    */
    public static final String COMM_EN_IPV_4_NETMASK = "comm.en.ipv4.netmask";

    /**
    * Comm En Mac.
    */
    public static final String COMM_EN_MAC = "comm.en.mac";

    /**
    * Comm En Mdns Enable.
    */
    public static final String COMM_EN_MDNS_ENABLE = "comm.en.mdns.enable";

    /**
    * Comm Ethercat Remote.
    */
    public static final String COMM_ETHERCAT_REMOTE = "comm.ethercat.remote";

    /**
    * Comm Next Owner.
    */
    public static final String COMM_NEXT_OWNER = "comm.next.owner";

    /**
    * Comm Packet Size Max.
    */
    public static final String COMM_PACKET_SIZE_MAX = "comm.packet.size.max";

    /**
    * Comm Protocol.
    */
    public static final String COMM_PROTOCOL = "comm.protocol";

    /**
    * Comm Rs 232 Baud.
    */
    public static final String COMM_RS_232_BAUD = "comm.rs232.baud";

    /**
    * Comm Rs 232 Protocol.
    */
    public static final String COMM_RS_232_PROTOCOL = "comm.rs232.protocol";

    /**
    * Comm Rs 485 Baud.
    */
    public static final String COMM_RS_485_BAUD = "comm.rs485.baud";

    /**
    * Comm Rs 485 Enable.
    */
    public static final String COMM_RS_485_ENABLE = "comm.rs485.enable";

    /**
    * Comm Rs 485 Protocol.
    */
    public static final String COMM_RS_485_PROTOCOL = "comm.rs485.protocol";

    /**
    * Comm Usb Protocol.
    */
    public static final String COMM_USB_PROTOCOL = "comm.usb.protocol";

    /**
    * Comm Word Size Max.
    */
    public static final String COMM_WORD_SIZE_MAX = "comm.word.size.max";

    /**
    * Device Hw Modified.
    */
    public static final String DEVICE_HW_MODIFIED = "device.hw.modified";

    /**
    * Device ID (Firmware 7 and higher).
    */
    public static final String DEVICE_ID = "device.id";

    /**
    * Device ID (Firmware 6 and lower).
    */
    public static final String DEVICE_ID_LEGACY = "deviceid";

    /**
    * Driver Bipolar.
    */
    public static final String DRIVER_BIPOLAR = "driver.bipolar";

    /**
    * Driver Current Approach.
    */
    public static final String DRIVER_CURRENT_APPROACH = "driver.current.approach";

    /**
    * Driver Current Continuous.
    */
    public static final String DRIVER_CURRENT_CONTINUOUS = "driver.current.continuous";

    /**
    * Driver Current Continuous Max.
    */
    public static final String DRIVER_CURRENT_CONTINUOUS_MAX = "driver.current.continuous.max";

    /**
    * Driver Current Hold.
    */
    public static final String DRIVER_CURRENT_HOLD = "driver.current.hold";

    /**
    * Driver Current Inject Noise Amplitude.
    */
    public static final String DRIVER_CURRENT_INJECT_NOISE_AMPLITUDE = "driver.current.inject.noise.amplitude";

    /**
    * Driver Current Max.
    */
    public static final String DRIVER_CURRENT_MAX = "driver.current.max";

    /**
    * Driver Current Overdrive.
    */
    public static final String DRIVER_CURRENT_OVERDRIVE = "driver.current.overdrive";

    /**
    * Driver Current Overdrive Duration.
    */
    public static final String DRIVER_CURRENT_OVERDRIVE_DURATION = "driver.current.overdrive.duration";

    /**
    * Driver Current Overdrive Max.
    */
    public static final String DRIVER_CURRENT_OVERDRIVE_MAX = "driver.current.overdrive.max";

    /**
    * Driver Current Run.
    */
    public static final String DRIVER_CURRENT_RUN = "driver.current.run";

    /**
    * Driver Current Servo.
    */
    public static final String DRIVER_CURRENT_SERVO = "driver.current.servo";

    /**
    * Driver Dir.
    */
    public static final String DRIVER_DIR = "driver.dir";

    /**
    * Driver Enable Mode.
    */
    public static final String DRIVER_ENABLE_MODE = "driver.enable.mode";

    /**
    * Driver Enabled.
    */
    public static final String DRIVER_ENABLED = "driver.enabled";

    /**
    * Driver I 2 T Measured.
    */
    public static final String DRIVER_I_2_T_MEASURED = "driver.i2t.measured";

    /**
    * Driver Overdrive State.
    */
    public static final String DRIVER_OVERDRIVE_STATE = "driver.overdrive.state";

    /**
    * Driver Temperature.
    */
    public static final String DRIVER_TEMPERATURE = "driver.temperature";

    /**
    * Encoder 1 Count.
    */
    public static final String ENCODER_1_COUNT = "encoder.1.count";

    /**
    * Encoder 1 Count Cal.
    */
    public static final String ENCODER_1_COUNT_CAL = "encoder.1.count.cal";

    /**
    * Encoder 1 Dir.
    */
    public static final String ENCODER_1_DIR = "encoder.1.dir";

    /**
    * Encoder 1 Fault Type.
    */
    public static final String ENCODER_1_FAULT_TYPE = "encoder.1.fault.type";

    /**
    * Encoder 1 Filter.
    */
    public static final String ENCODER_1_FILTER = "encoder.1.filter";

    /**
    * Encoder 1 Index Mode.
    */
    public static final String ENCODER_1_INDEX_MODE = "encoder.1.index.mode";

    /**
    * Encoder 1 Mode.
    */
    public static final String ENCODER_1_MODE = "encoder.1.mode";

    /**
    * Encoder 1 Pos.
    */
    public static final String ENCODER_1_POS = "encoder.1.pos";

    /**
    * Encoder 1 Pos Error.
    */
    public static final String ENCODER_1_POS_ERROR = "encoder.1.pos.error";

    /**
    * Encoder 1 Power Up Delay.
    */
    public static final String ENCODER_1_POWER_UP_DELAY = "encoder.1.power.up.delay";

    /**
    * Encoder 1 Ratio Div.
    */
    public static final String ENCODER_1_RATIO_DIV = "encoder.1.ratio.div";

    /**
    * Encoder 1 Ratio Mult.
    */
    public static final String ENCODER_1_RATIO_MULT = "encoder.1.ratio.mult";

    /**
    * Encoder 1 Ref Phase.
    */
    public static final String ENCODER_1_REF_PHASE = "encoder.1.ref.phase";

    /**
    * Encoder 1 Type.
    */
    public static final String ENCODER_1_TYPE = "encoder.1.type";

    /**
    * Encoder 2 Cos.
    */
    public static final String ENCODER_2_COS = "encoder.2.cos";

    /**
    * Encoder 2 Cos Dc.
    */
    public static final String ENCODER_2_COS_DC = "encoder.2.cos.dc";

    /**
    * Encoder 2 Cos Dc Tune.
    */
    public static final String ENCODER_2_COS_DC_TUNE = "encoder.2.cos.dc.tune";

    /**
    * Encoder 2 Cos Gain.
    */
    public static final String ENCODER_2_COS_GAIN = "encoder.2.cos.gain";

    /**
    * Encoder 2 Cos Gain Tune.
    */
    public static final String ENCODER_2_COS_GAIN_TUNE = "encoder.2.cos.gain.tune";

    /**
    * Encoder 2 Count.
    */
    public static final String ENCODER_2_COUNT = "encoder.2.count";

    /**
    * Encoder 2 Count Cal.
    */
    public static final String ENCODER_2_COUNT_CAL = "encoder.2.count.cal";

    /**
    * Encoder 2 Dir.
    */
    public static final String ENCODER_2_DIR = "encoder.2.dir";

    /**
    * Encoder 2 Fault Type.
    */
    public static final String ENCODER_2_FAULT_TYPE = "encoder.2.fault.type";

    /**
    * Encoder 2 Filter.
    */
    public static final String ENCODER_2_FILTER = "encoder.2.filter";

    /**
    * Encoder 2 Index Mode.
    */
    public static final String ENCODER_2_INDEX_MODE = "encoder.2.index.mode";

    /**
    * Encoder 2 Interpolation.
    */
    public static final String ENCODER_2_INTERPOLATION = "encoder.2.interpolation";

    /**
    * Encoder 2 Mode.
    */
    public static final String ENCODER_2_MODE = "encoder.2.mode";

    /**
    * Encoder 2 Out Enable.
    */
    public static final String ENCODER_2_OUT_ENABLE = "encoder.2.out.enable";

    /**
    * Encoder 2 Out Interpolation.
    */
    public static final String ENCODER_2_OUT_INTERPOLATION = "encoder.2.out.interpolation";

    /**
    * Encoder 2 Out Width.
    */
    public static final String ENCODER_2_OUT_WIDTH = "encoder.2.out.width";

    /**
    * Encoder 2 Pos.
    */
    public static final String ENCODER_2_POS = "encoder.2.pos";

    /**
    * Encoder 2 Pos Error.
    */
    public static final String ENCODER_2_POS_ERROR = "encoder.2.pos.error";

    /**
    * Encoder 2 Power Up Delay.
    */
    public static final String ENCODER_2_POWER_UP_DELAY = "encoder.2.power.up.delay";

    /**
    * Encoder 2 Ratio Div.
    */
    public static final String ENCODER_2_RATIO_DIV = "encoder.2.ratio.div";

    /**
    * Encoder 2 Ratio Mult.
    */
    public static final String ENCODER_2_RATIO_MULT = "encoder.2.ratio.mult";

    /**
    * Encoder 2 Signal Min.
    */
    public static final String ENCODER_2_SIGNAL_MIN = "encoder.2.signal.min";

    /**
    * Encoder 2 Sin.
    */
    public static final String ENCODER_2_SIN = "encoder.2.sin";

    /**
    * Encoder 2 Sin Dc.
    */
    public static final String ENCODER_2_SIN_DC = "encoder.2.sin.dc";

    /**
    * Encoder 2 Sin Dc Tune.
    */
    public static final String ENCODER_2_SIN_DC_TUNE = "encoder.2.sin.dc.tune";

    /**
    * Encoder 2 Sin Gain.
    */
    public static final String ENCODER_2_SIN_GAIN = "encoder.2.sin.gain";

    /**
    * Encoder 2 Sin Gain Tune.
    */
    public static final String ENCODER_2_SIN_GAIN_TUNE = "encoder.2.sin.gain.tune";

    /**
    * Encoder 2 Type.
    */
    public static final String ENCODER_2_TYPE = "encoder.2.type";

    /**
    * Encoder Count.
    */
    public static final String ENCODER_COUNT = "encoder.count";

    /**
    * Encoder Count Cal.
    */
    public static final String ENCODER_COUNT_CAL = "encoder.count.cal";

    /**
    * Encoder Count Calibrated.
    */
    public static final String ENCODER_COUNT_CALIBRATED = "encoder.count.calibrated";

    /**
    * Encoder Dir.
    */
    public static final String ENCODER_DIR = "encoder.dir";

    /**
    * Encoder Error.
    */
    public static final String ENCODER_ERROR = "encoder.error";

    /**
    * Encoder Fault Type.
    */
    public static final String ENCODER_FAULT_TYPE = "encoder.fault.type";

    /**
    * Encoder Filter.
    */
    public static final String ENCODER_FILTER = "encoder.filter";

    /**
    * Encoder Index Count.
    */
    public static final String ENCODER_INDEX_COUNT = "encoder.index.count";

    /**
    * Encoder Index Mode.
    */
    public static final String ENCODER_INDEX_MODE = "encoder.index.mode";

    /**
    * Encoder Index Phase.
    */
    public static final String ENCODER_INDEX_PHASE = "encoder.index.phase";

    /**
    * Encoder Mode.
    */
    public static final String ENCODER_MODE = "encoder.mode";

    /**
    * Encoder Port Default.
    */
    public static final String ENCODER_PORT_DEFAULT = "encoder.port.default";

    /**
    * Encoder Pos.
    */
    public static final String ENCODER_POS = "encoder.pos";

    /**
    * Encoder Pos Error.
    */
    public static final String ENCODER_POS_ERROR = "encoder.pos.error";

    /**
    * Encoder Power Up Delay.
    */
    public static final String ENCODER_POWER_UP_DELAY = "encoder.power.up.delay";

    /**
    * Encoder Ratio Div.
    */
    public static final String ENCODER_RATIO_DIV = "encoder.ratio.div";

    /**
    * Encoder Ratio Mult.
    */
    public static final String ENCODER_RATIO_MULT = "encoder.ratio.mult";

    /**
    * Encoder Vel.
    */
    public static final String ENCODER_VEL = "encoder.vel";

    /**
    * Filter Holderid.
    */
    public static final String FILTER_HOLDERID = "filter.holderid";

    /**
    * Force Average.
    */
    public static final String FORCE_AVERAGE = "force.average";

    /**
    * Force Max.
    */
    public static final String FORCE_MAX = "force.max";

    /**
    * Get Settings Max.
    */
    public static final String GET_SETTINGS_MAX = "get.settings.max";

    /**
    * Ictrl Advance A.
    */
    public static final String ICTRL_ADVANCE_A = "ictrl.advance.a";

    /**
    * Ictrl Advance Offset.
    */
    public static final String ICTRL_ADVANCE_OFFSET = "ictrl.advance.offset";

    /**
    * Ictrl Afcff Inductance.
    */
    public static final String ICTRL_AFCFF_INDUCTANCE = "ictrl.afcff.inductance";

    /**
    * Ictrl Afcff Ke.
    */
    public static final String ICTRL_AFCFF_KE = "ictrl.afcff.ke";

    /**
    * Ictrl Afcff Ki.
    */
    public static final String ICTRL_AFCFF_KI = "ictrl.afcff.ki";

    /**
    * Ictrl Afcff Max.
    */
    public static final String ICTRL_AFCFF_MAX = "ictrl.afcff.max";

    /**
    * Ictrl Afcff Ss.
    */
    public static final String ICTRL_AFCFF_SS = "ictrl.afcff.ss";

    /**
    * Ictrl Afcff Ss Max.
    */
    public static final String ICTRL_AFCFF_SS_MAX = "ictrl.afcff.ss.max";

    /**
    * Ictrl Delay.
    */
    public static final String ICTRL_DELAY = "ictrl.delay";

    /**
    * Ictrl Ff Kd.
    */
    public static final String ICTRL_FF_KD = "ictrl.ff.kd";

    /**
    * Ictrl Ff Kp.
    */
    public static final String ICTRL_FF_KP = "ictrl.ff.kp";

    /**
    * Ictrl Gain Coldmult.
    */
    public static final String ICTRL_GAIN_COLDMULT = "ictrl.gain.coldmult";

    /**
    * Ictrl Period.
    */
    public static final String ICTRL_PERIOD = "ictrl.period";

    /**
    * Ictrl Pi Ki.
    */
    public static final String ICTRL_PI_KI = "ictrl.pi.ki";

    /**
    * Ictrl Pi Kp.
    */
    public static final String ICTRL_PI_KP = "ictrl.pi.kp";

    /**
    * Ictrl Type.
    */
    public static final String ICTRL_TYPE = "ictrl.type";

    /**
    * Io Ai 1 Fc.
    */
    public static final String IO_AI_1_FC = "io.ai.1.fc";

    /**
    * Io Ai 2 Fc.
    */
    public static final String IO_AI_2_FC = "io.ai.2.fc";

    /**
    * Io Ai 3 Fc.
    */
    public static final String IO_AI_3_FC = "io.ai.3.fc";

    /**
    * Io Ai 4 Fc.
    */
    public static final String IO_AI_4_FC = "io.ai.4.fc";

    /**
    * Io Di Port.
    */
    public static final String IO_DI_PORT = "io.di.port";

    /**
    * Io Do Port.
    */
    public static final String IO_DO_PORT = "io.do.port";

    /**
    * Joy Debug.
    */
    public static final String JOY_DEBUG = "joy.debug";

    /**
    * Knob Dir.
    */
    public static final String KNOB_DIR = "knob.dir";

    /**
    * Knob Distance.
    */
    public static final String KNOB_DISTANCE = "knob.distance";

    /**
    * Knob Enable.
    */
    public static final String KNOB_ENABLE = "knob.enable";

    /**
    * Knob Force.
    */
    public static final String KNOB_FORCE = "knob.force";

    /**
    * Knob Forceprofile.
    */
    public static final String KNOB_FORCEPROFILE = "knob.forceprofile";

    /**
    * Knob Maxspeed.
    */
    public static final String KNOB_MAXSPEED = "knob.maxspeed";

    /**
    * Knob Mode.
    */
    public static final String KNOB_MODE = "knob.mode";

    /**
    * Knob Speedprofile.
    */
    public static final String KNOB_SPEEDPROFILE = "knob.speedprofile";

    /**
    * Lamp Current.
    */
    public static final String LAMP_CURRENT = "lamp.current";

    /**
    * Lamp Current Max.
    */
    public static final String LAMP_CURRENT_MAX = "lamp.current.max";

    /**
    * Lamp Flux.
    */
    public static final String LAMP_FLUX = "lamp.flux";

    /**
    * Lamp Flux Max.
    */
    public static final String LAMP_FLUX_MAX = "lamp.flux.max";

    /**
    * Lamp Status.
    */
    public static final String LAMP_STATUS = "lamp.status";

    /**
    * Lamp Temperature.
    */
    public static final String LAMP_TEMPERATURE = "lamp.temperature";

    /**
    * Lamp Wavelength Fwhm.
    */
    public static final String LAMP_WAVELENGTH_FWHM = "lamp.wavelength.fwhm";

    /**
    * Lamp Wavelength Peak.
    */
    public static final String LAMP_WAVELENGTH_PEAK = "lamp.wavelength.peak";

    /**
    * Limit Approach Accel.
    */
    public static final String LIMIT_APPROACH_ACCEL = "limit.approach.accel";

    /**
    * Limit Approach Maxspeed.
    */
    public static final String LIMIT_APPROACH_MAXSPEED = "limit.approach.maxspeed";

    /**
    * Limit Away Action.
    */
    public static final String LIMIT_AWAY_ACTION = "limit.away.action";

    /**
    * Limit Away Edge.
    */
    public static final String LIMIT_AWAY_EDGE = "limit.away.edge";

    /**
    * Limit Away Offset.
    */
    public static final String LIMIT_AWAY_OFFSET = "limit.away.offset";

    /**
    * Limit Away Pos.
    */
    public static final String LIMIT_AWAY_POS = "limit.away.pos";

    /**
    * Limit Away Posupdate.
    */
    public static final String LIMIT_AWAY_POSUPDATE = "limit.away.posupdate";

    /**
    * Limit Away Preset.
    */
    public static final String LIMIT_AWAY_PRESET = "limit.away.preset";

    /**
    * Limit Away Source.
    */
    public static final String LIMIT_AWAY_SOURCE = "limit.away.source";

    /**
    * Limit Away State.
    */
    public static final String LIMIT_AWAY_STATE = "limit.away.state";

    /**
    * Limit Away Triggered.
    */
    public static final String LIMIT_AWAY_TRIGGERED = "limit.away.triggered";

    /**
    * Limit Away Tune.
    */
    public static final String LIMIT_AWAY_TUNE = "limit.away.tune";

    /**
    * Limit Away Type.
    */
    public static final String LIMIT_AWAY_TYPE = "limit.away.type";

    /**
    * Limit Away Width.
    */
    public static final String LIMIT_AWAY_WIDTH = "limit.away.width";

    /**
    * Limit C Action.
    */
    public static final String LIMIT_C_ACTION = "limit.c.action";

    /**
    * Limit C Edge.
    */
    public static final String LIMIT_C_EDGE = "limit.c.edge";

    /**
    * Limit C Offset.
    */
    public static final String LIMIT_C_OFFSET = "limit.c.offset";

    /**
    * Limit C Pos.
    */
    public static final String LIMIT_C_POS = "limit.c.pos";

    /**
    * Limit C Posupdate.
    */
    public static final String LIMIT_C_POSUPDATE = "limit.c.posupdate";

    /**
    * Limit C Preset.
    */
    public static final String LIMIT_C_PRESET = "limit.c.preset";

    /**
    * Limit C Source.
    */
    public static final String LIMIT_C_SOURCE = "limit.c.source";

    /**
    * Limit C State.
    */
    public static final String LIMIT_C_STATE = "limit.c.state";

    /**
    * Limit C Triggered.
    */
    public static final String LIMIT_C_TRIGGERED = "limit.c.triggered";

    /**
    * Limit C Tune.
    */
    public static final String LIMIT_C_TUNE = "limit.c.tune";

    /**
    * Limit C Type.
    */
    public static final String LIMIT_C_TYPE = "limit.c.type";

    /**
    * Limit C Width.
    */
    public static final String LIMIT_C_WIDTH = "limit.c.width";

    /**
    * Limit Cycle Dist.
    */
    public static final String LIMIT_CYCLE_DIST = "limit.cycle.dist";

    /**
    * Limit D Action.
    */
    public static final String LIMIT_D_ACTION = "limit.d.action";

    /**
    * Limit D Edge.
    */
    public static final String LIMIT_D_EDGE = "limit.d.edge";

    /**
    * Limit D Pos.
    */
    public static final String LIMIT_D_POS = "limit.d.pos";

    /**
    * Limit D Posupdate.
    */
    public static final String LIMIT_D_POSUPDATE = "limit.d.posupdate";

    /**
    * Limit D Preset.
    */
    public static final String LIMIT_D_PRESET = "limit.d.preset";

    /**
    * Limit D State.
    */
    public static final String LIMIT_D_STATE = "limit.d.state";

    /**
    * Limit D Triggered.
    */
    public static final String LIMIT_D_TRIGGERED = "limit.d.triggered";

    /**
    * Limit D Type.
    */
    public static final String LIMIT_D_TYPE = "limit.d.type";

    /**
    * Limit Detect Decelonly.
    */
    public static final String LIMIT_DETECT_DECELONLY = "limit.detect.decelonly";

    /**
    * Limit Detect Maxspeed.
    */
    public static final String LIMIT_DETECT_MAXSPEED = "limit.detect.maxspeed";

    /**
    * Limit Hardstop Retraction.
    */
    public static final String LIMIT_HARDSTOP_RETRACTION = "limit.hardstop.retraction";

    /**
    * Limit Home Action.
    */
    public static final String LIMIT_HOME_ACTION = "limit.home.action";

    /**
    * Limit Home Bidirectional.
    */
    public static final String LIMIT_HOME_BIDIRECTIONAL = "limit.home.bidirectional";

    /**
    * Limit Home Edge.
    */
    public static final String LIMIT_HOME_EDGE = "limit.home.edge";

    /**
    * Limit Home Offset.
    */
    public static final String LIMIT_HOME_OFFSET = "limit.home.offset";

    /**
    * Limit Home Pos.
    */
    public static final String LIMIT_HOME_POS = "limit.home.pos";

    /**
    * Limit Home Posupdate.
    */
    public static final String LIMIT_HOME_POSUPDATE = "limit.home.posupdate";

    /**
    * Limit Home Preset.
    */
    public static final String LIMIT_HOME_PRESET = "limit.home.preset";

    /**
    * Limit Home Source.
    */
    public static final String LIMIT_HOME_SOURCE = "limit.home.source";

    /**
    * Limit Home State.
    */
    public static final String LIMIT_HOME_STATE = "limit.home.state";

    /**
    * Limit Home Triggered.
    */
    public static final String LIMIT_HOME_TRIGGERED = "limit.home.triggered";

    /**
    * Limit Home Tune.
    */
    public static final String LIMIT_HOME_TUNE = "limit.home.tune";

    /**
    * Limit Home Type.
    */
    public static final String LIMIT_HOME_TYPE = "limit.home.type";

    /**
    * Limit Home Width.
    */
    public static final String LIMIT_HOME_WIDTH = "limit.home.width";

    /**
    * Limit Max.
    */
    public static final String LIMIT_MAX = "limit.max";

    /**
    * Limit Min.
    */
    public static final String LIMIT_MIN = "limit.min";

    /**
    * Limit Range Mode.
    */
    public static final String LIMIT_RANGE_MODE = "limit.range.mode";

    /**
    * Limit Ref Phase.
    */
    public static final String LIMIT_REF_PHASE = "limit.ref.phase";

    /**
    * Limit Ref Phase Measured.
    */
    public static final String LIMIT_REF_PHASE_MEASURED = "limit.ref.phase.measured";

    /**
    * Limit Start Pos.
    */
    public static final String LIMIT_START_POS = "limit.start.pos";

    /**
    * Limit Swapinputs.
    */
    public static final String LIMIT_SWAPINPUTS = "limit.swapinputs";

    /**
    * Lockstep Numgroups.
    */
    public static final String LOCKSTEP_NUMGROUPS = "lockstep.numgroups";

    /**
    * Lockstep Tolerance.
    */
    public static final String LOCKSTEP_TOLERANCE = "lockstep.tolerance";

    /**
    * Maxspeed.
    */
    public static final String MAXSPEED = "maxspeed";

    /**
    * Motion Accel Ramptime.
    */
    public static final String MOTION_ACCEL_RAMPTIME = "motion.accel.ramptime";

    /**
    * Motion Accelonly.
    */
    public static final String MOTION_ACCELONLY = "motion.accelonly";

    /**
    * Motion Busy.
    */
    public static final String MOTION_BUSY = "motion.busy";

    /**
    * Motion Decelonly.
    */
    public static final String MOTION_DECELONLY = "motion.decelonly";

    /**
    * Motion Index Dist.
    */
    public static final String MOTION_INDEX_DIST = "motion.index.dist";

    /**
    * Motion Index Num.
    */
    public static final String MOTION_INDEX_NUM = "motion.index.num";

    /**
    * Motion Tracking Ai.
    */
    public static final String MOTION_TRACKING_AI = "motion.tracking.ai";

    /**
    * Motion Tracking Dir.
    */
    public static final String MOTION_TRACKING_DIR = "motion.tracking.dir";

    /**
    * Motion Tracking Ki.
    */
    public static final String MOTION_TRACKING_KI = "motion.tracking.ki";

    /**
    * Motion Tracking Kp.
    */
    public static final String MOTION_TRACKING_KP = "motion.tracking.kp";

    /**
    * Motion Tracking Limit Max.
    */
    public static final String MOTION_TRACKING_LIMIT_MAX = "motion.tracking.limit.max";

    /**
    * Motion Tracking Limit Min.
    */
    public static final String MOTION_TRACKING_LIMIT_MIN = "motion.tracking.limit.min";

    /**
    * Motion Tracking Mode.
    */
    public static final String MOTION_TRACKING_MODE = "motion.tracking.mode";

    /**
    * Motion Tracking Scan Dir.
    */
    public static final String MOTION_TRACKING_SCAN_DIR = "motion.tracking.scan.dir";

    /**
    * Motion Tracking Scan Maxspeed.
    */
    public static final String MOTION_TRACKING_SCAN_MAXSPEED = "motion.tracking.scan.maxspeed";

    /**
    * Motion Tracking Scan Offset.
    */
    public static final String MOTION_TRACKING_SCAN_OFFSET = "motion.tracking.scan.offset";

    /**
    * Motion Tracking Scan Period.
    */
    public static final String MOTION_TRACKING_SCAN_PERIOD = "motion.tracking.scan.period";

    /**
    * Motion Tracking Scan Signal Valid Delay.
    */
    public static final String MOTION_TRACKING_SCAN_SIGNAL_VALID_DELAY = "motion.tracking.scan.signal.valid.delay";

    /**
    * Motion Tracking Scan Tolerance.
    */
    public static final String MOTION_TRACKING_SCAN_TOLERANCE = "motion.tracking.scan.tolerance";

    /**
    * Motion Tracking Setpoint.
    */
    public static final String MOTION_TRACKING_SETPOINT = "motion.tracking.setpoint";

    /**
    * Motion Tracking Settle Mode.
    */
    public static final String MOTION_TRACKING_SETTLE_MODE = "motion.tracking.settle.mode";

    /**
    * Motion Tracking Settle Period.
    */
    public static final String MOTION_TRACKING_SETTLE_PERIOD = "motion.tracking.settle.period";

    /**
    * Motion Tracking Settle Tolerance.
    */
    public static final String MOTION_TRACKING_SETTLE_TOLERANCE = "motion.tracking.settle.tolerance";

    /**
    * Motion Tracking Settle Tolerance Met.
    */
    public static final String MOTION_TRACKING_SETTLE_TOLERANCE_MET = "motion.tracking.settle.tolerance.met";

    /**
    * Motion Tracking Settled.
    */
    public static final String MOTION_TRACKING_SETTLED = "motion.tracking.settled";

    /**
    * Motion Tracking Signal Valid Di.
    */
    public static final String MOTION_TRACKING_SIGNAL_VALID_DI = "motion.tracking.signal.valid.di";

    /**
    * Motor Current Continuous Max.
    */
    public static final String MOTOR_CURRENT_CONTINUOUS_MAX = "motor.current.continuous.max";

    /**
    * Motor Current Max.
    */
    public static final String MOTOR_CURRENT_MAX = "motor.current.max";

    /**
    * Motor Current Overdrive Duration.
    */
    public static final String MOTOR_CURRENT_OVERDRIVE_DURATION = "motor.current.overdrive.duration";

    /**
    * Motor Current Overdrive Max.
    */
    public static final String MOTOR_CURRENT_OVERDRIVE_MAX = "motor.current.overdrive.max";

    /**
    * Motor I 2 T Measured.
    */
    public static final String MOTOR_I_2_T_MEASURED = "motor.i2t.measured";

    /**
    * Motor Inductance.
    */
    public static final String MOTOR_INDUCTANCE = "motor.inductance";

    /**
    * Motor Ke.
    */
    public static final String MOTOR_KE = "motor.ke";

    /**
    * Motor Phase.
    */
    public static final String MOTOR_PHASE = "motor.phase";

    /**
    * Motor Phase Ratio Div 1.
    */
    public static final String MOTOR_PHASE_RATIO_DIV_1 = "motor.phase.ratio.div1";

    /**
    * Motor Phase Ratio Div 2.
    */
    public static final String MOTOR_PHASE_RATIO_DIV_2 = "motor.phase.ratio.div2";

    /**
    * Motor Phase Ratio Mult.
    */
    public static final String MOTOR_PHASE_RATIO_MULT = "motor.phase.ratio.mult";

    /**
    * Motor Resistance.
    */
    public static final String MOTOR_RESISTANCE = "motor.resistance";

    /**
    * Parking State.
    */
    public static final String PARKING_STATE = "parking.state";

    /**
    * Peripheral Hw Modified.
    */
    public static final String PERIPHERAL_HW_MODIFIED = "peripheral.hw.modified";

    /**
    * Peripheral ID (Firmware 7 and higher).
    */
    public static final String PERIPHERAL_ID = "peripheral.id";

    /**
    * Peripheral Id Pending.
    */
    public static final String PERIPHERAL_ID_PENDING = "peripheral.id.pending";

    /**
    * Peripheral Serial.
    */
    public static final String PERIPHERAL_SERIAL = "peripheral.serial";

    /**
    * Peripheral Serial Pending.
    */
    public static final String PERIPHERAL_SERIAL_PENDING = "peripheral.serial.pending";

    /**
    * Peripheral ID (Firmware 6 and lower).
    */
    public static final String PERIPHERAL_ID_LEGACY = "peripheralid";

    /**
    * Pos.
    */
    public static final String POS = "pos";

    /**
    * Process Control Dir.
    */
    public static final String PROCESS_CONTROL_DIR = "process.control.dir";

    /**
    * Process Control Hysteresis Temperature.
    */
    public static final String PROCESS_CONTROL_HYSTERESIS_TEMPERATURE = "process.control.hysteresis.temperature";

    /**
    * Process Control Hysteresis Voltage.
    */
    public static final String PROCESS_CONTROL_HYSTERESIS_VOLTAGE = "process.control.hysteresis.voltage";

    /**
    * Process Control Mode.
    */
    public static final String PROCESS_CONTROL_MODE = "process.control.mode";

    /**
    * Process Control Setpoint Temperature.
    */
    public static final String PROCESS_CONTROL_SETPOINT_TEMPERATURE = "process.control.setpoint.temperature";

    /**
    * Process Control Setpoint Temperature Filtered.
    */
    public static final String PROCESS_CONTROL_SETPOINT_TEMPERATURE_FILTERED = "process.control.setpoint.temperature.filtered";

    /**
    * Process Control Setpoint Tf.
    */
    public static final String PROCESS_CONTROL_SETPOINT_TF = "process.control.setpoint.tf";

    /**
    * Process Control Setpoint Voltage.
    */
    public static final String PROCESS_CONTROL_SETPOINT_VOLTAGE = "process.control.setpoint.voltage";

    /**
    * Process Control Setpoint Voltage Filtered.
    */
    public static final String PROCESS_CONTROL_SETPOINT_VOLTAGE_FILTERED = "process.control.setpoint.voltage.filtered";

    /**
    * Process Control Source.
    */
    public static final String PROCESS_CONTROL_SOURCE = "process.control.source";

    /**
    * Process Control Voltage Max.
    */
    public static final String PROCESS_CONTROL_VOLTAGE_MAX = "process.control.voltage.max";

    /**
    * Process Control Voltage Min.
    */
    public static final String PROCESS_CONTROL_VOLTAGE_MIN = "process.control.voltage.min";

    /**
    * Process Current.
    */
    public static final String PROCESS_CURRENT = "process.current";

    /**
    * Process Current Max.
    */
    public static final String PROCESS_CURRENT_MAX = "process.current.max";

    /**
    * Process Pid Kd.
    */
    public static final String PROCESS_PID_KD = "process.pid.kd";

    /**
    * Process Pid Ki.
    */
    public static final String PROCESS_PID_KI = "process.pid.ki";

    /**
    * Process Pid Kp.
    */
    public static final String PROCESS_PID_KP = "process.pid.kp";

    /**
    * Process Pid Offset.
    */
    public static final String PROCESS_PID_OFFSET = "process.pid.offset";

    /**
    * Process Startup Mode.
    */
    public static final String PROCESS_STARTUP_MODE = "process.startup.mode";

    /**
    * Process State.
    */
    public static final String PROCESS_STATE = "process.state";

    /**
    * Process Voltage.
    */
    public static final String PROCESS_VOLTAGE = "process.voltage";

    /**
    * Process Voltage On.
    */
    public static final String PROCESS_VOLTAGE_ON = "process.voltage.on";

    /**
    * Process Voltage Start.
    */
    public static final String PROCESS_VOLTAGE_START = "process.voltage.start";

    /**
    * Process Voltage Start Duration.
    */
    public static final String PROCESS_VOLTAGE_START_DURATION = "process.voltage.start.duration";

    /**
    * Process Voltage Tf.
    */
    public static final String PROCESS_VOLTAGE_TF = "process.voltage.tf";

    /**
    * Pvt Numseqs.
    */
    public static final String PVT_NUMSEQS = "pvt.numseqs";

    /**
    * Resolution.
    */
    public static final String RESOLUTION = "resolution";

    /**
    * Scope Channel Size.
    */
    public static final String SCOPE_CHANNEL_SIZE = "scope.channel.size";

    /**
    * Scope Channel Size Max.
    */
    public static final String SCOPE_CHANNEL_SIZE_MAX = "scope.channel.size.max";

    /**
    * Scope Delay.
    */
    public static final String SCOPE_DELAY = "scope.delay";

    /**
    * Scope Numchannels.
    */
    public static final String SCOPE_NUMCHANNELS = "scope.numchannels";

    /**
    * Scope Timebase.
    */
    public static final String SCOPE_TIMEBASE = "scope.timebase";

    /**
    * Sensor Temperature 1.
    */
    public static final String SENSOR_TEMPERATURE_1 = "sensor.temperature.1";

    /**
    * Sensor Temperature 2.
    */
    public static final String SENSOR_TEMPERATURE_2 = "sensor.temperature.2";

    /**
    * Stream Numbufs.
    */
    public static final String STREAM_NUMBUFS = "stream.numbufs";

    /**
    * Stream Numstreams.
    */
    public static final String STREAM_NUMSTREAMS = "stream.numstreams";

    /**
    * System Access.
    */
    public static final String SYSTEM_ACCESS = "system.access";

    /**
    * System Axiscount.
    */
    public static final String SYSTEM_AXISCOUNT = "system.axiscount";

    /**
    * System Current.
    */
    public static final String SYSTEM_CURRENT = "system.current";

    /**
    * System Current Max.
    */
    public static final String SYSTEM_CURRENT_MAX = "system.current.max";

    /**
    * System Led Enable.
    */
    public static final String SYSTEM_LED_ENABLE = "system.led.enable";

    /**
    * System Serial.
    */
    public static final String SYSTEM_SERIAL = "system.serial";

    /**
    * System Temperature.
    */
    public static final String SYSTEM_TEMPERATURE = "system.temperature";

    /**
    * System Uptime.
    */
    public static final String SYSTEM_UPTIME = "system.uptime";

    /**
    * System Voltage.
    */
    public static final String SYSTEM_VOLTAGE = "system.voltage";

    /**
    * Trigger Numactions.
    */
    public static final String TRIGGER_NUMACTIONS = "trigger.numactions";

    /**
    * Trigger Numtriggers.
    */
    public static final String TRIGGER_NUMTRIGGERS = "trigger.numtriggers";

    /**
    * User Data 0.
    */
    public static final String USER_DATA_0 = "user.data.0";

    /**
    * User Data 1.
    */
    public static final String USER_DATA_1 = "user.data.1";

    /**
    * User Data 10.
    */
    public static final String USER_DATA_10 = "user.data.10";

    /**
    * User Data 11.
    */
    public static final String USER_DATA_11 = "user.data.11";

    /**
    * User Data 12.
    */
    public static final String USER_DATA_12 = "user.data.12";

    /**
    * User Data 13.
    */
    public static final String USER_DATA_13 = "user.data.13";

    /**
    * User Data 14.
    */
    public static final String USER_DATA_14 = "user.data.14";

    /**
    * User Data 15.
    */
    public static final String USER_DATA_15 = "user.data.15";

    /**
    * User Data 2.
    */
    public static final String USER_DATA_2 = "user.data.2";

    /**
    * User Data 3.
    */
    public static final String USER_DATA_3 = "user.data.3";

    /**
    * User Data 4.
    */
    public static final String USER_DATA_4 = "user.data.4";

    /**
    * User Data 5.
    */
    public static final String USER_DATA_5 = "user.data.5";

    /**
    * User Data 6.
    */
    public static final String USER_DATA_6 = "user.data.6";

    /**
    * User Data 7.
    */
    public static final String USER_DATA_7 = "user.data.7";

    /**
    * User Data 8.
    */
    public static final String USER_DATA_8 = "user.data.8";

    /**
    * User Data 9.
    */
    public static final String USER_DATA_9 = "user.data.9";

    /**
    * User Vdata 0.
    */
    public static final String USER_VDATA_0 = "user.vdata.0";

    /**
    * User Vdata 1.
    */
    public static final String USER_VDATA_1 = "user.vdata.1";

    /**
    * User Vdata 2.
    */
    public static final String USER_VDATA_2 = "user.vdata.2";

    /**
    * User Vdata 3.
    */
    public static final String USER_VDATA_3 = "user.vdata.3";

    /**
    * Vel.
    */
    public static final String VEL = "vel";

    /**
    * Version.
    */
    public static final String VERSION = "version";

    /**
    * Version Build.
    */
    public static final String VERSION_BUILD = "version.build";

    /**
    * Virtual Numvirtual.
    */
    public static final String VIRTUAL_NUMVIRTUAL = "virtual.numvirtual";

}
