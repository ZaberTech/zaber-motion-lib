/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The tuning of this axis represented by PID parameters.
 */
public final class PidTuning implements zaber.motion.dto.Message {

    private String type;

    /**
     * The tuning algorithm used to tune this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The tuning algorithm used to tune this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public String getType() {
        return this.type;
    }

    /**
     * The tuning algorithm used to tune this axis.
     */
    public PidTuning withType(String aType) {
        this.setType(aType);
        return this;
    }

    private int version;

    /**
     * The version of the tuning algorithm used to tune this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * The version of the tuning algorithm used to tune this axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("version")
    public int getVersion() {
        return this.version;
    }

    /**
     * The version of the tuning algorithm used to tune this axis.
     */
    public PidTuning withVersion(int aVersion) {
        this.setVersion(aVersion);
        return this;
    }

    private double p;

    /**
     * The positional tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("p")
    public void setP(double p) {
        this.p = p;
    }

    /**
     * The positional tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("p")
    public double getP() {
        return this.p;
    }

    /**
     * The positional tuning argument.
     */
    public PidTuning withP(double aP) {
        this.setP(aP);
        return this;
    }

    private double i;

    /**
     * The integral tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("i")
    public void setI(double i) {
        this.i = i;
    }

    /**
     * The integral tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("i")
    public double getI() {
        return this.i;
    }

    /**
     * The integral tuning argument.
     */
    public PidTuning withI(double aI) {
        this.setI(aI);
        return this;
    }

    private double d;

    /**
     * The derivative tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("d")
    public void setD(double d) {
        this.d = d;
    }

    /**
     * The derivative tuning argument.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("d")
    public double getD() {
        return this.d;
    }

    /**
     * The derivative tuning argument.
     */
    public PidTuning withD(double aD) {
        this.setD(aD);
        return this;
    }

    private double fc;

    /**
     * The frequency cutoff for the tuning.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fc")
    public void setFc(double fc) {
        this.fc = fc;
    }

    /**
     * The frequency cutoff for the tuning.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("fc")
    public double getFc() {
        return this.fc;
    }

    /**
     * The frequency cutoff for the tuning.
     */
    public PidTuning withFc(double aFc) {
        this.setFc(aFc);
        return this;
    }

    /**
     * Empty constructor.
     */
    public PidTuning() {
    }

    /**
     * Constructor with all properties.
     */
    public PidTuning(
        String type,
        int version,
        double p,
        double i,
        double d,
        double fc
    ) {
        this.type = type;
        this.version = version;
        this.p = p;
        this.i = i;
        this.d = d;
        this.fc = fc;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        PidTuning other = (PidTuning) obj;

        return (
            EqualityUtility.equals(type, other.type)
            && EqualityUtility.equals(version, other.version)
            && EqualityUtility.equals(p, other.p)
            && EqualityUtility.equals(i, other.i)
            && EqualityUtility.equals(d, other.d)
            && EqualityUtility.equals(fc, other.fc)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(type),
            EqualityUtility.generateHashCode(version),
            EqualityUtility.generateHashCode(p),
            EqualityUtility.generateHashCode(i),
            EqualityUtility.generateHashCode(d),
            EqualityUtility.generateHashCode(fc)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PidTuning { ");
        sb.append("type: ");
        sb.append(this.type);
        sb.append(", ");
        sb.append("version: ");
        sb.append(this.version);
        sb.append(", ");
        sb.append("p: ");
        sb.append(this.p);
        sb.append(", ");
        sb.append("i: ");
        sb.append(this.i);
        sb.append(", ");
        sb.append("d: ");
        sb.append(this.d);
        sb.append(", ");
        sb.append("fc: ");
        sb.append(this.fc);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static PidTuning fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, PidTuning.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<PidTuning> PARSER =
        new zaber.motion.dto.Parser<PidTuning>() {
            @Override
            public PidTuning fromByteArray(byte[] data) {
                return PidTuning.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<PidTuning> parser() {
        return PARSER;
    }

}
