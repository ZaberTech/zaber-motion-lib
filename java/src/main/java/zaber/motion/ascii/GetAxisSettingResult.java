/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.Units;
import zaber.motion.EqualityUtility;


/**
 * The response from a multi-get axis command.
 */
public final class GetAxisSettingResult implements zaber.motion.dto.Message {

    private String setting;

    /**
     * The setting read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public void setSetting(String setting) {
        this.setting = setting;
    }

    /**
     * The setting read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("setting")
    public String getSetting() {
        return this.setting;
    }

    /**
     * The setting read.
     */
    public GetAxisSettingResult withSetting(String aSetting) {
        this.setSetting(aSetting);
        return this;
    }

    private double value;

    /**
     * The value read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * The value read.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    /**
     * The value read.
     */
    public GetAxisSettingResult withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    private Units unit;

    /**
     * The unit of the values.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public void setUnit(Units unit) {
        this.unit = unit;
    }

    /**
     * The unit of the values.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("unit")
    public Units getUnit() {
        return this.unit;
    }

    /**
     * The unit of the values.
     */
    public GetAxisSettingResult withUnit(Units aUnit) {
        this.setUnit(aUnit);
        return this;
    }

    /**
     * Empty constructor.
     */
    public GetAxisSettingResult() {
    }

    /**
     * Constructor with all properties.
     */
    public GetAxisSettingResult(
        String setting,
        double value,
        Units unit
    ) {
        this.setting = setting;
        this.value = value;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        GetAxisSettingResult other = (GetAxisSettingResult) obj;

        return (
            EqualityUtility.equals(setting, other.setting)
            && EqualityUtility.equals(value, other.value)
            && EqualityUtility.equals(unit, other.unit)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(setting),
            EqualityUtility.generateHashCode(value),
            EqualityUtility.generateHashCode(unit)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GetAxisSettingResult { ");
        sb.append("setting: ");
        sb.append(this.setting);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(", ");
        sb.append("unit: ");
        sb.append(this.unit);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static GetAxisSettingResult fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, GetAxisSettingResult.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<GetAxisSettingResult> PARSER =
        new zaber.motion.dto.Parser<GetAxisSettingResult>() {
            @Override
            public GetAxisSettingResult fromByteArray(byte[] data) {
                return GetAxisSettingResult.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<GetAxisSettingResult> parser() {
        return PARSER;
    }

}
