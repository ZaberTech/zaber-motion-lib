/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Denotes type of an axis and units it accepts.
 */
public enum AxisType {

    UNKNOWN(0),

    LINEAR(1),

    ROTARY(2),

    PROCESS(3),

    LAMP(4);

    private int value;

    AxisType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static AxisType valueOf(int argValue) {
        for (AxisType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
