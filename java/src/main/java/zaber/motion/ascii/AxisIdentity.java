/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Representation of data gathered during axis identification.
 */
public final class AxisIdentity implements zaber.motion.dto.Message {

    private int peripheralId;

    /**
     * Unique ID of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public void setPeripheralId(int peripheralId) {
        this.peripheralId = peripheralId;
    }

    /**
     * Unique ID of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public int getPeripheralId() {
        return this.peripheralId;
    }

    /**
     * Unique ID of the peripheral hardware.
     */
    public AxisIdentity withPeripheralId(int aPeripheralId) {
        this.setPeripheralId(aPeripheralId);
        return this;
    }

    private String peripheralName;

    /**
     * Name of the peripheral.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralName")
    public void setPeripheralName(String peripheralName) {
        this.peripheralName = peripheralName;
    }

    /**
     * Name of the peripheral.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralName")
    public String getPeripheralName() {
        return this.peripheralName;
    }

    /**
     * Name of the peripheral.
     */
    public AxisIdentity withPeripheralName(String aPeripheralName) {
        this.setPeripheralName(aPeripheralName);
        return this;
    }

    private long peripheralSerialNumber;

    /**
     * Serial number of the peripheral, or 0 when not applicable.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralSerialNumber")
    public void setPeripheralSerialNumber(long peripheralSerialNumber) {
        this.peripheralSerialNumber = peripheralSerialNumber;
    }

    /**
     * Serial number of the peripheral, or 0 when not applicable.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralSerialNumber")
    public long getPeripheralSerialNumber() {
        return this.peripheralSerialNumber;
    }

    /**
     * Serial number of the peripheral, or 0 when not applicable.
     */
    public AxisIdentity withPeripheralSerialNumber(long aPeripheralSerialNumber) {
        this.setPeripheralSerialNumber(aPeripheralSerialNumber);
        return this;
    }

    private boolean isPeripheral;

    /**
     * Indicates whether the axis is a peripheral or part of an integrated device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isPeripheral")
    public void setIsPeripheral(boolean isPeripheral) {
        this.isPeripheral = isPeripheral;
    }

    /**
     * Indicates whether the axis is a peripheral or part of an integrated device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isPeripheral")
    public boolean getIsPeripheral() {
        return this.isPeripheral;
    }

    /**
     * Indicates whether the axis is a peripheral or part of an integrated device.
     */
    public AxisIdentity withIsPeripheral(boolean aIsPeripheral) {
        this.setIsPeripheral(aIsPeripheral);
        return this;
    }

    private AxisType axisType;

    /**
     * Determines the type of an axis and units it accepts.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public void setAxisType(AxisType axisType) {
        this.axisType = axisType;
    }

    /**
     * Determines the type of an axis and units it accepts.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axisType")
    public AxisType getAxisType() {
        return this.axisType;
    }

    /**
     * Determines the type of an axis and units it accepts.
     */
    public AxisIdentity withAxisType(AxisType aAxisType) {
        this.setAxisType(aAxisType);
        return this;
    }

    private boolean isModified;

    /**
     * The peripheral has hardware modifications.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isModified")
    public void setIsModified(boolean isModified) {
        this.isModified = isModified;
    }

    /**
     * The peripheral has hardware modifications.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isModified")
    public boolean getIsModified() {
        return this.isModified;
    }

    /**
     * The peripheral has hardware modifications.
     */
    public AxisIdentity withIsModified(boolean aIsModified) {
        this.setIsModified(aIsModified);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AxisIdentity() {
    }

    /**
     * Constructor with all properties.
     */
    public AxisIdentity(
        int peripheralId,
        String peripheralName,
        long peripheralSerialNumber,
        boolean isPeripheral,
        AxisType axisType,
        boolean isModified
    ) {
        this.peripheralId = peripheralId;
        this.peripheralName = peripheralName;
        this.peripheralSerialNumber = peripheralSerialNumber;
        this.isPeripheral = isPeripheral;
        this.axisType = axisType;
        this.isModified = isModified;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AxisIdentity other = (AxisIdentity) obj;

        return (
            EqualityUtility.equals(peripheralId, other.peripheralId)
            && EqualityUtility.equals(peripheralName, other.peripheralName)
            && EqualityUtility.equals(peripheralSerialNumber, other.peripheralSerialNumber)
            && EqualityUtility.equals(isPeripheral, other.isPeripheral)
            && EqualityUtility.equals(axisType, other.axisType)
            && EqualityUtility.equals(isModified, other.isModified)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(peripheralId),
            EqualityUtility.generateHashCode(peripheralName),
            EqualityUtility.generateHashCode(peripheralSerialNumber),
            EqualityUtility.generateHashCode(isPeripheral),
            EqualityUtility.generateHashCode(axisType),
            EqualityUtility.generateHashCode(isModified)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AxisIdentity { ");
        sb.append("peripheralId: ");
        sb.append(this.peripheralId);
        sb.append(", ");
        sb.append("peripheralName: ");
        sb.append(this.peripheralName);
        sb.append(", ");
        sb.append("peripheralSerialNumber: ");
        sb.append(this.peripheralSerialNumber);
        sb.append(", ");
        sb.append("isPeripheral: ");
        sb.append(this.isPeripheral);
        sb.append(", ");
        sb.append("axisType: ");
        sb.append(this.axisType);
        sb.append(", ");
        sb.append("isModified: ");
        sb.append(this.isModified);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AxisIdentity fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AxisIdentity.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AxisIdentity> PARSER =
        new zaber.motion.dto.Parser<AxisIdentity>() {
            @Override
            public AxisIdentity fromByteArray(byte[] data) {
                return AxisIdentity.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AxisIdentity> parser() {
        return PARSER;
    }

}
