// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents a PVT buffer with this number on a device.
 * A PVT buffer is a place to store a queue of PVT actions.
 */
public class PvtBuffer {
    private Device device;

    /**
     * @return The Device this buffer exists on.
     */
    public Device getDevice() {
        return this.device;
    }

    private int bufferId;

    /**
     * @return The number identifying the buffer on the device.
     */
    public int getBufferId() {
        return this.bufferId;
    }

    public PvtBuffer(
        Device device, int bufferId) {
        this.device = device;
        this.bufferId = bufferId;
    }

    /**
     * Gets the buffer contents as an array of strings.
     * @return A CompletableFuture that can be completed to get the result:
     * A string array containing all the PVT commands stored in the buffer.
     */
    public CompletableFuture<String[]> getContentAsync() {
        zaber.motion.requests.StreamBufferGetContentRequest request =
            new zaber.motion.requests.StreamBufferGetContentRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setBufferId(getBufferId());
        request.setPvt(true);
        CompletableFuture<zaber.motion.requests.StreamBufferGetContentResponse> response = Call.callAsync(
            "device/stream_buffer_get_content",
            request,
            zaber.motion.requests.StreamBufferGetContentResponse.parser());
        return response
            .thenApply(r -> r.getBufferLines());
    }

    /**
     * Gets the buffer contents as an array of strings.
     * @return A string array containing all the PVT commands stored in the buffer.
     */
    public String[] getContent() {
        try {
            return getContentAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Erases the contents of the buffer.
     * This method fails if there is a PVT sequence writing to the buffer.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> eraseAsync() {
        zaber.motion.requests.StreamBufferEraseRequest request =
            new zaber.motion.requests.StreamBufferEraseRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setBufferId(getBufferId());
        request.setPvt(true);
        return Call.callAsync("device/stream_buffer_erase", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Erases the contents of the buffer.
     * This method fails if there is a PVT sequence writing to the buffer.
     */
    public void erase() {
        try {
            eraseAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
