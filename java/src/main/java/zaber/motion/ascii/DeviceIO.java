// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import zaber.motion.Units;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing access to the I/O channels of the device.
 */
public class DeviceIO {
    private Device device;

    public DeviceIO(
        Device device) {
        this.device = device;
    }

    /**
     * Returns the number of I/O channels the device has.
     * @return A CompletableFuture that can be completed to get the result:
     * An object containing the number of I/O channels the device has.
     */
    public CompletableFuture<DeviceIOInfo> getChannelsInfoAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        CompletableFuture<DeviceIOInfo> response = Call.callAsync(
            "device/get_io_info",
            request,
            DeviceIOInfo.parser());
        return response;
    }

    /**
     * Returns the number of I/O channels the device has.
     * @return An object containing the number of I/O channels the device has.
     */
    public DeviceIOInfo getChannelsInfo() {
        try {
            return getChannelsInfoAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the label of the specified channel.
     * @param portType The type of channel to set the label of.
     * @param channelNumber Channel number starting at 1.
     * @param label The label to set for the specified channel.
     * If no value or an empty string is provided, this label is deleted.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLabelAsync(
        IoPortType portType,
        int channelNumber,
        String label) {
        zaber.motion.requests.SetIoPortLabelRequest request =
            new zaber.motion.requests.SetIoPortLabelRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setPortType(portType);
        request.setChannelNumber(channelNumber);
        request.setLabel(label);
        return Call.callAsync("device/set_io_label", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the label of the specified channel.
     * @param portType The type of channel to set the label of.
     * @param channelNumber Channel number starting at 1.
     * @param label The label to set for the specified channel.
     * If no value or an empty string is provided, this label is deleted.
     */
    public void setLabel(
        IoPortType portType,
        int channelNumber,
        String label) {
        try {
            setLabelAsync(portType, channelNumber, label).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the label of the specified channel.
     * @param portType The type of channel to get the label of.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to get the result:
     * The label of the specified channel.
     */
    public CompletableFuture<String> getLabelAsync(
        IoPortType portType,
        int channelNumber) {
        zaber.motion.requests.GetIoPortLabelRequest request =
            new zaber.motion.requests.GetIoPortLabelRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setPortType(portType);
        request.setChannelNumber(channelNumber);
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_io_label",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the label of the specified channel.
     * @param portType The type of channel to get the label of.
     * @param channelNumber Channel number starting at 1.
     * @return The label of the specified channel.
     */
    public String getLabel(
        IoPortType portType,
        int channelNumber) {
        try {
            return getLabelAsync(portType, channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns every label assigned to an IO port on this device.
     * @return A CompletableFuture that can be completed to get the result:
     * The labels set for this device's IO.
     */
    public CompletableFuture<IoPortLabel[]> getAllLabelsAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        CompletableFuture<zaber.motion.requests.GetAllIoPortLabelsResponse> response = Call.callAsync(
            "device/get_all_io_labels",
            request,
            zaber.motion.requests.GetAllIoPortLabelsResponse.parser());
        return response
            .thenApply(r -> r.getLabels());
    }

    /**
     * Returns every label assigned to an IO port on this device.
     * @return The labels set for this device's IO.
     */
    public IoPortLabel[] getAllLabels() {
        try {
            return getAllLabelsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current value of the specified digital input channel.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to get the result:
     * True if voltage is present on the input channel and false otherwise.
     */
    public CompletableFuture<Boolean> getDigitalInputAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceGetDigitalIORequest request =
            new zaber.motion.requests.DeviceGetDigitalIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("di");
        request.setChannelNumber(channelNumber);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/get_digital_io",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the current value of the specified digital input channel.
     * @param channelNumber Channel number starting at 1.
     * @return True if voltage is present on the input channel and false otherwise.
     */
    public boolean getDigitalInput(
        int channelNumber) {
        try {
            return getDigitalInputAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current value of the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the output channel is conducting and false otherwise.
     */
    public CompletableFuture<Boolean> getDigitalOutputAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceGetDigitalIORequest request =
            new zaber.motion.requests.DeviceGetDigitalIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("do");
        request.setChannelNumber(channelNumber);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/get_digital_io",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the current value of the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @return True if the output channel is conducting and false otherwise.
     */
    public boolean getDigitalOutput(
        int channelNumber) {
        try {
            return getDigitalOutputAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current value of the specified analog input channel.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to get the result:
     *  A measurement of the voltage present on the input channel.
     */
    public CompletableFuture<Double> getAnalogInputAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceGetAnalogIORequest request =
            new zaber.motion.requests.DeviceGetAnalogIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("ai");
        request.setChannelNumber(channelNumber);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_analog_io",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the current value of the specified analog input channel.
     * @param channelNumber Channel number starting at 1.
     * @return  A measurement of the voltage present on the input channel.
     */
    public double getAnalogInput(
        int channelNumber) {
        try {
            return getAnalogInputAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current values of the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to get the result:
     * A measurement of voltage that the output channel is conducting.
     */
    public CompletableFuture<Double> getAnalogOutputAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceGetAnalogIORequest request =
            new zaber.motion.requests.DeviceGetAnalogIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("ao");
        request.setChannelNumber(channelNumber);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_analog_io",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the current values of the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @return A measurement of voltage that the output channel is conducting.
     */
    public double getAnalogOutput(
        int channelNumber) {
        try {
            return getAnalogOutputAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current values of all digital input channels.
     * @return A CompletableFuture that can be completed to get the result:
     * True if voltage is present on the input channel and false otherwise.
     */
    public CompletableFuture<boolean[]> getAllDigitalInputsAsync() {
        zaber.motion.requests.DeviceGetAllDigitalIORequest request =
            new zaber.motion.requests.DeviceGetAllDigitalIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("di");
        CompletableFuture<zaber.motion.requests.DeviceGetAllDigitalIOResponse> response = Call.callAsync(
            "device/get_all_digital_io",
            request,
            zaber.motion.requests.DeviceGetAllDigitalIOResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Returns the current values of all digital input channels.
     * @return True if voltage is present on the input channel and false otherwise.
     */
    public boolean[] getAllDigitalInputs() {
        try {
            return getAllDigitalInputsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current values of all digital output channels.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the output channel is conducting and false otherwise.
     */
    public CompletableFuture<boolean[]> getAllDigitalOutputsAsync() {
        zaber.motion.requests.DeviceGetAllDigitalIORequest request =
            new zaber.motion.requests.DeviceGetAllDigitalIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("do");
        CompletableFuture<zaber.motion.requests.DeviceGetAllDigitalIOResponse> response = Call.callAsync(
            "device/get_all_digital_io",
            request,
            zaber.motion.requests.DeviceGetAllDigitalIOResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Returns the current values of all digital output channels.
     * @return True if the output channel is conducting and false otherwise.
     */
    public boolean[] getAllDigitalOutputs() {
        try {
            return getAllDigitalOutputsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current values of all analog input channels.
     * @return A CompletableFuture that can be completed to get the result:
     * Measurements of the voltages present on the input channels.
     */
    public CompletableFuture<double[]> getAllAnalogInputsAsync() {
        zaber.motion.requests.DeviceGetAllAnalogIORequest request =
            new zaber.motion.requests.DeviceGetAllAnalogIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("ai");
        CompletableFuture<zaber.motion.requests.DeviceGetAllAnalogIOResponse> response = Call.callAsync(
            "device/get_all_analog_io",
            request,
            zaber.motion.requests.DeviceGetAllAnalogIOResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Returns the current values of all analog input channels.
     * @return Measurements of the voltages present on the input channels.
     */
    public double[] getAllAnalogInputs() {
        try {
            return getAllAnalogInputsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current values of all analog output channels.
     * @return A CompletableFuture that can be completed to get the result:
     * Measurements of voltage that the output channels are conducting.
     */
    public CompletableFuture<double[]> getAllAnalogOutputsAsync() {
        zaber.motion.requests.DeviceGetAllAnalogIORequest request =
            new zaber.motion.requests.DeviceGetAllAnalogIORequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelType("ao");
        CompletableFuture<zaber.motion.requests.DeviceGetAllAnalogIOResponse> response = Call.callAsync(
            "device/get_all_analog_io",
            request,
            zaber.motion.requests.DeviceGetAllAnalogIOResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Returns the current values of all analog output channels.
     * @return Measurements of voltage that the output channels are conducting.
     */
    public double[] getAllAnalogOutputs() {
        try {
            return getAllAnalogOutputsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputAsync(
        int channelNumber,
        DigitalOutputAction value) {
        zaber.motion.requests.DeviceSetDigitalOutputRequest request =
            new zaber.motion.requests.DeviceSetDigitalOutputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/set_digital_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     */
    public void setDigitalOutput(
        int channelNumber,
        DigitalOutputAction value) {
        try {
            setDigitalOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsAsync(
        DigitalOutputAction[] values) {
        zaber.motion.requests.DeviceSetAllDigitalOutputsRequest request =
            new zaber.motion.requests.DeviceSetAllDigitalOutputsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setValues(values);
        return Call.callAsync("device/set_all_digital_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     */
    public void setAllDigitalOutputs(
        DigitalOutputAction[] values) {
        try {
            setAllDigitalOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputScheduleAsync(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.DeviceSetDigitalOutputScheduleRequest request =
            new zaber.motion.requests.DeviceSetDigitalOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        request.setFutureValue(futureValue);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/set_digital_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputScheduleAsync(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay) {
        return setDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     */
    public void setDigitalOutputSchedule(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay,
        Units unit) {
        try {
            setDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     */
    public void setDigitalOutputSchedule(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay) {
        setDigitalOutputSchedule(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsScheduleAsync(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.DeviceSetAllDigitalOutputsScheduleRequest request =
            new zaber.motion.requests.DeviceSetAllDigitalOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setValues(values);
        request.setFutureValues(futureValues);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/set_all_digital_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsScheduleAsync(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay) {
        return setAllDigitalOutputsScheduleAsync(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     */
    public void setAllDigitalOutputsSchedule(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay,
        Units unit) {
        try {
            setAllDigitalOutputsScheduleAsync(values, futureValues, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     */
    public void setAllDigitalOutputsSchedule(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay) {
        setAllDigitalOutputsSchedule(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputAsync(
        int channelNumber,
        double value) {
        zaber.motion.requests.DeviceSetAnalogOutputRequest request =
            new zaber.motion.requests.DeviceSetAnalogOutputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/set_analog_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     */
    public void setAnalogOutput(
        int channelNumber,
        double value) {
        try {
            setAnalogOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsAsync(
        double[] values) {
        zaber.motion.requests.DeviceSetAllAnalogOutputsRequest request =
            new zaber.motion.requests.DeviceSetAllAnalogOutputsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setValues(values);
        return Call.callAsync("device/set_all_analog_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     */
    public void setAllAnalogOutputs(
        double[] values) {
        try {
            setAllAnalogOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputScheduleAsync(
        int channelNumber,
        double value,
        double futureValue,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.DeviceSetAnalogOutputScheduleRequest request =
            new zaber.motion.requests.DeviceSetAnalogOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        request.setFutureValue(futureValue);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/set_analog_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputScheduleAsync(
        int channelNumber,
        double value,
        double futureValue,
        double delay) {
        return setAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     */
    public void setAnalogOutputSchedule(
        int channelNumber,
        double value,
        double futureValue,
        double delay,
        Units unit) {
        try {
            setAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     */
    public void setAnalogOutputSchedule(
        int channelNumber,
        double value,
        double futureValue,
        double delay) {
        setAnalogOutputSchedule(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsScheduleAsync(
        double[] values,
        double[] futureValues,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.DeviceSetAllAnalogOutputsScheduleRequest request =
            new zaber.motion.requests.DeviceSetAllAnalogOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setValues(values);
        request.setFutureValues(futureValues);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/set_all_analog_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsScheduleAsync(
        double[] values,
        double[] futureValues,
        double delay) {
        return setAllAnalogOutputsScheduleAsync(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     */
    public void setAllAnalogOutputsSchedule(
        double[] values,
        double[] futureValues,
        double delay,
        Units unit) {
        try {
            setAllAnalogOutputsScheduleAsync(values, futureValues, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     */
    public void setAllAnalogOutputsSchedule(
        double[] values,
        double[] futureValues,
        double delay) {
        setAllAnalogOutputsSchedule(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
     * Set the frequency to 0 to disable the filter.
     * @param channelNumber Channel number starting at 1.
     * @param cutoffFrequency Cutoff frequency of the low-pass filter.
     * @param unit Units of frequency.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogInputLowpassFilterAsync(
        int channelNumber,
        double cutoffFrequency,
        Units unit) {
        zaber.motion.requests.DeviceSetLowpassFilterRequest request =
            new zaber.motion.requests.DeviceSetLowpassFilterRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setChannelNumber(channelNumber);
        request.setCutoffFrequency(cutoffFrequency);
        request.setUnit(unit);
        return Call.callAsync("device/set_lowpass_filter", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
     * Set the frequency to 0 to disable the filter.
     * @param channelNumber Channel number starting at 1.
     * @param cutoffFrequency Cutoff frequency of the low-pass filter.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogInputLowpassFilterAsync(
        int channelNumber,
        double cutoffFrequency) {
        return setAnalogInputLowpassFilterAsync(channelNumber, cutoffFrequency, Units.NATIVE);
    }

    /**
     * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
     * Set the frequency to 0 to disable the filter.
     * @param channelNumber Channel number starting at 1.
     * @param cutoffFrequency Cutoff frequency of the low-pass filter.
     * @param unit Units of frequency.
     */
    public void setAnalogInputLowpassFilter(
        int channelNumber,
        double cutoffFrequency,
        Units unit) {
        try {
            setAnalogInputLowpassFilterAsync(channelNumber, cutoffFrequency, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the cutoff frequency of the low-pass filter for the specified analog input channel.
     * Set the frequency to 0 to disable the filter.
     * @param channelNumber Channel number starting at 1.
     * @param cutoffFrequency Cutoff frequency of the low-pass filter.
     */
    public void setAnalogInputLowpassFilter(
        int channelNumber,
        double cutoffFrequency) {
        setAnalogInputLowpassFilter(channelNumber, cutoffFrequency, Units.NATIVE);
    }

    /**
     * Cancels a scheduled digital output action.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelDigitalOutputScheduleAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceCancelOutputScheduleRequest request =
            new zaber.motion.requests.DeviceCancelOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(false);
        request.setChannelNumber(channelNumber);
        return Call.callAsync("device/cancel_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancels a scheduled digital output action.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     */
    public void cancelDigitalOutputSchedule(
        int channelNumber) {
        try {
            cancelDigitalOutputScheduleAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllDigitalOutputsScheduleAsync(
        boolean[] channels) {
        zaber.motion.requests.DeviceCancelAllOutputsScheduleRequest request =
            new zaber.motion.requests.DeviceCancelAllOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(false);
        request.setChannels(channels);
        return Call.callAsync("device/cancel_all_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllDigitalOutputsScheduleAsync() {
        return cancelAllDigitalOutputsScheduleAsync(new boolean[0]);
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
     */
    public void cancelAllDigitalOutputsSchedule(
        boolean[] channels) {
        try {
            cancelAllDigitalOutputsScheduleAsync(channels).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     */
    public void cancelAllDigitalOutputsSchedule() {
        cancelAllDigitalOutputsSchedule(new boolean[0]);
    }

    /**
     * Cancels a scheduled analog output value.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAnalogOutputScheduleAsync(
        int channelNumber) {
        zaber.motion.requests.DeviceCancelOutputScheduleRequest request =
            new zaber.motion.requests.DeviceCancelOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(true);
        request.setChannelNumber(channelNumber);
        return Call.callAsync("device/cancel_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancels a scheduled analog output value.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     */
    public void cancelAnalogOutputSchedule(
        int channelNumber) {
        try {
            cancelAnalogOutputScheduleAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllAnalogOutputsScheduleAsync(
        boolean[] channels) {
        zaber.motion.requests.DeviceCancelAllOutputsScheduleRequest request =
            new zaber.motion.requests.DeviceCancelAllOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(true);
        request.setChannels(channels);
        return Call.callAsync("device/cancel_all_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllAnalogOutputsScheduleAsync() {
        return cancelAllAnalogOutputsScheduleAsync(new boolean[0]);
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
     */
    public void cancelAllAnalogOutputsSchedule(
        boolean[] channels) {
        try {
            cancelAllAnalogOutputsScheduleAsync(channels).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     */
    public void cancelAllAnalogOutputsSchedule() {
        cancelAllAnalogOutputsSchedule(new boolean[0]);
    }

}
