// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.AxisMoveType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents a lockstep group with this ID on a device.
 * A lockstep group is a movement synchronized pair of axes on a device.
 * Requires at least Firmware 6.15 or 7.11.
 */
public class Lockstep {
    private Device device;

    /**
     * @return Device that controls this lockstep group.
     */
    public Device getDevice() {
        return this.device;
    }

    private int lockstepGroupId;

    /**
     * @return The number that identifies the lockstep group on the device.
     */
    public int getLockstepGroupId() {
        return this.lockstepGroupId;
    }

    public Lockstep(
        Device device, int lockstepGroupId) {
        this.device = device;
        this.lockstepGroupId = lockstepGroupId;
    }

    /**
     * Activate the lockstep group on the axes specified.
     * @param axes The numbers of axes in the lockstep group.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableAsync(
        int... axes) {
        zaber.motion.requests.LockstepEnableRequest request =
            new zaber.motion.requests.LockstepEnableRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setAxes(axes);
        return Call.callAsync("device/lockstep_enable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Activate the lockstep group on the axes specified.
     * @param axes The numbers of axes in the lockstep group.
     */
    public void enable(
        int... axes) {
        try {
            enableAsync(axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Disable the lockstep group.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableAsync() {
        zaber.motion.requests.LockstepDisableRequest request =
            new zaber.motion.requests.LockstepDisableRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        return Call.callAsync("device/lockstep_disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disable the lockstep group.
     */
    public void disable() {
        try {
            disableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops ongoing lockstep group movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.LockstepStopRequest request =
            new zaber.motion.requests.LockstepStopRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/lockstep_stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops ongoing lockstep group movement. Decelerates until zero speed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync() {
        return stopAsync(true);
    }

    /**
     * Stops ongoing lockstep group movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void stop(
        boolean waitUntilIdle) {
        try {
            stopAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops ongoing lockstep group movement. Decelerates until zero speed.
     */
    public void stop() {
        stop(true);
    }

    /**
     * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.LockstepHomeRequest request =
            new zaber.motion.requests.LockstepHomeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/lockstep_home", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync() {
        return homeAsync(true);
    }

    /**
     * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void home(
        boolean waitUntilIdle) {
        try {
            homeAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Retracts the axes of the lockstep group until a home associated with an individual axis is detected.
     */
    public void home() {
        home(true);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.LockstepMoveRequest request =
            new zaber.motion.requests.LockstepMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setType(AxisMoveType.ABS);
        request.setArg(position);
        request.setUnit(unit);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/lockstep_move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit) {
        return moveAbsoluteAsync(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position) {
        return moveAbsoluteAsync(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        moveAbsolute(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     * @param unit Units of position.
     */
    public void moveAbsolute(
        double position,
        Units unit) {
        moveAbsolute(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to an absolute position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Absolute position.
     */
    public void moveAbsolute(
        double position) {
        moveAbsolute(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.LockstepMoveRequest request =
            new zaber.motion.requests.LockstepMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setType(AxisMoveType.REL);
        request.setArg(position);
        request.setUnit(unit);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/lockstep_move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        return moveRelativeAsync(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit) {
        return moveRelativeAsync(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position) {
        return moveRelativeAsync(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveRelative(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveRelative(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        moveRelative(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        moveRelative(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     * @param unit Units of position.
     */
    public void moveRelative(
        double position,
        Units unit) {
        moveRelative(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move the first axis of the lockstep group to a position relative to its current position.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param position Relative position.
     */
    public void moveRelative(
        double position) {
        moveRelative(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.LockstepMoveRequest request =
            new zaber.motion.requests.LockstepMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setType(AxisMoveType.VEL);
        request.setArg(velocity);
        request.setUnit(unit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/lockstep_move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit,
        double acceleration) {
        return moveVelocityAsync(velocity, unit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit) {
        return moveVelocityAsync(velocity, unit, 0, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity) {
        return moveVelocityAsync(velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveVelocity(
        double velocity,
        Units unit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveVelocityAsync(velocity, unit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveVelocity(
        double velocity,
        Units unit,
        double acceleration) {
        moveVelocity(velocity, unit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     */
    public void moveVelocity(
        double velocity,
        Units unit) {
        moveVelocity(velocity, unit, 0, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group at the specified speed.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param velocity Movement velocity.
     */
    public void moveVelocity(
        double velocity) {
        moveVelocity(velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count,
        boolean waitUntilIdle) {
        zaber.motion.requests.LockstepMoveSinRequest request =
            new zaber.motion.requests.LockstepMoveSinRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setAmplitude(amplitude);
        request.setAmplitudeUnits(amplitudeUnits);
        request.setPeriod(period);
        request.setPeriodUnits(periodUnits);
        request.setCount(count);
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/lockstep_move_sin", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count) {
        return moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, true);
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits) {
        return moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, 0, true);
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count,
        boolean waitUntilIdle) {
        try {
            moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count) {
        moveSin(amplitude, amplitudeUnits, period, periodUnits, count, true);
    }

    /**
     * Moves the first axis of the lockstep group in a sinusoidal trajectory.
     * The other axes in the lockstep group maintain their offsets throughout movement.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits) {
        moveSin(amplitude, amplitudeUnits, period, periodUnits, 0, true);
    }

    /**
     * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @param waitUntilIdle Determines whether function should return after the movement is finished.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinStopAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.LockstepStopRequest request =
            new zaber.motion.requests.LockstepStopRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/lockstep_move_sin_stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinStopAsync() {
        return moveSinStopAsync(true);
    }

    /**
     * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @param waitUntilIdle Determines whether function should return after the movement is finished.
     */
    public void moveSinStop(
        boolean waitUntilIdle) {
        try {
            moveSinStopAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops the lockstep group at the end of the sinusoidal trajectory for the first axis.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     */
    public void moveSinStop() {
        moveSinStop(true);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.LockstepMoveRequest request =
            new zaber.motion.requests.LockstepMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setType(AxisMoveType.MAX);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/lockstep_move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveMaxAsync(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity) {
        return moveMaxAsync(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle) {
        return moveMaxAsync(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync() {
        return moveMaxAsync(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveMax(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveMax(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity) {
        moveMax(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveMax(
        boolean waitUntilIdle) {
        moveMax(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the maximum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.max for all axes in the group.
     */
    public void moveMax() {
        moveMax(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.LockstepMoveRequest request =
            new zaber.motion.requests.LockstepMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setType(AxisMoveType.MIN);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/lockstep_move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveMinAsync(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity) {
        return moveMinAsync(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle) {
        return moveMinAsync(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync() {
        return moveMinAsync(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveMin(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveMin(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity) {
        moveMin(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveMin(
        boolean waitUntilIdle) {
        moveMin(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axes to the minimum valid position.
     * The axes in the lockstep group maintain their offsets throughout movement.
     * Respects lim.min for all axes in the group.
     */
    public void moveMin() {
        moveMin(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Waits until the lockstep group stops moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync(
        boolean throwErrorOnFault) {
        zaber.motion.requests.LockstepWaitUntilIdleRequest request =
            new zaber.motion.requests.LockstepWaitUntilIdleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setThrowErrorOnFault(throwErrorOnFault);
        return Call.callAsync("device/lockstep_wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until the lockstep group stops moving.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        return waitUntilIdleAsync(true);
    }

    /**
     * Waits until the lockstep group stops moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     */
    public void waitUntilIdle(
        boolean throwErrorOnFault) {
        try {
            waitUntilIdleAsync(throwErrorOnFault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until the lockstep group stops moving.
     */
    public void waitUntilIdle() {
        waitUntilIdle(true);
    }

    /**
     * Returns bool indicating whether the lockstep group is executing a motion command.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the axes are currently executing a motion command.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.LockstepEmptyRequest request =
            new zaber.motion.requests.LockstepEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/lockstep_is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether the lockstep group is executing a motion command.
     * @return True if the axes are currently executing a motion command.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use GetAxisNumbers instead.
     *
     * Gets the axes of the lockstep group.
     * @return A CompletableFuture that can be completed to get the result:
     * LockstepAxes instance which contains the axes numbers of the lockstep group.
     */
    @Deprecated
    public CompletableFuture<LockstepAxes> getAxesAsync() {
        zaber.motion.requests.LockstepEmptyRequest request =
            new zaber.motion.requests.LockstepEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        CompletableFuture<LockstepAxes> response = Call.callAsync(
            "device/lockstep_get_axes",
            request,
            LockstepAxes.parser());
        return response;
    }

    /**
     * @deprecated Use GetAxisNumbers instead.
     *
     * Gets the axes of the lockstep group.
     * @return LockstepAxes instance which contains the axes numbers of the lockstep group.
     */
    public LockstepAxes getAxes() {
        try {
            return getAxesAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the axis numbers of the lockstep group.
     * @return A CompletableFuture that can be completed to get the result:
     * Axis numbers in order specified when enabling lockstep.
     */
    public CompletableFuture<int[]> getAxisNumbersAsync() {
        zaber.motion.requests.LockstepEmptyRequest request =
            new zaber.motion.requests.LockstepEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        CompletableFuture<zaber.motion.requests.LockstepGetAxisNumbersResponse> response = Call.callAsync(
            "device/lockstep_get_axis_numbers",
            request,
            zaber.motion.requests.LockstepGetAxisNumbersResponse.parser());
        return response
            .thenApply(r -> r.getAxes());
    }

    /**
     * Gets the axis numbers of the lockstep group.
     * @return Axis numbers in order specified when enabling lockstep.
     */
    public int[] getAxisNumbers() {
        try {
            return getAxisNumbersAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the initial offsets of secondary axes of an enabled lockstep group.
     * @param unit Units of position.
     * Uses primary axis unit conversion.
     * @return A CompletableFuture that can be completed to get the result:
     * Initial offset for each axis of the lockstep group.
     */
    public CompletableFuture<double[]> getOffsetsAsync(
        Units unit) {
        zaber.motion.requests.LockstepGetRequest request =
            new zaber.motion.requests.LockstepGetRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleArrayResponse> response = Call.callAsync(
            "device/lockstep_get_offsets",
            request,
            zaber.motion.requests.DoubleArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Gets the initial offsets of secondary axes of an enabled lockstep group.
     * @return A CompletableFuture that can be completed to get the result:
     * Initial offset for each axis of the lockstep group.
     */
    public CompletableFuture<double[]> getOffsetsAsync() {
        return getOffsetsAsync(Units.NATIVE);
    }

    /**
     * Gets the initial offsets of secondary axes of an enabled lockstep group.
     * @param unit Units of position.
     * Uses primary axis unit conversion.
     * @return Initial offset for each axis of the lockstep group.
     */
    public double[] getOffsets(
        Units unit) {
        try {
            return getOffsetsAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the initial offsets of secondary axes of an enabled lockstep group.
     * @return Initial offset for each axis of the lockstep group.
     */
    public double[] getOffsets() {
        return getOffsets(Units.NATIVE);
    }

    /**
     * Gets the twists of secondary axes of an enabled lockstep group.
     * @param unit Units of position.
     * Uses primary axis unit conversion.
     * @return A CompletableFuture that can be completed to get the result:
     * Difference between the initial offset and the actual offset for each axis of the lockstep group.
     */
    public CompletableFuture<double[]> getTwistsAsync(
        Units unit) {
        zaber.motion.requests.LockstepGetRequest request =
            new zaber.motion.requests.LockstepGetRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleArrayResponse> response = Call.callAsync(
            "device/lockstep_get_twists",
            request,
            zaber.motion.requests.DoubleArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Gets the twists of secondary axes of an enabled lockstep group.
     * @return A CompletableFuture that can be completed to get the result:
     * Difference between the initial offset and the actual offset for each axis of the lockstep group.
     */
    public CompletableFuture<double[]> getTwistsAsync() {
        return getTwistsAsync(Units.NATIVE);
    }

    /**
     * Gets the twists of secondary axes of an enabled lockstep group.
     * @param unit Units of position.
     * Uses primary axis unit conversion.
     * @return Difference between the initial offset and the actual offset for each axis of the lockstep group.
     */
    public double[] getTwists(
        Units unit) {
        try {
            return getTwistsAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the twists of secondary axes of an enabled lockstep group.
     * @return Difference between the initial offset and the actual offset for each axis of the lockstep group.
     */
    public double[] getTwists() {
        return getTwists(Units.NATIVE);
    }

    /**
     * Returns current position of the primary axis.
     * @param unit Units of the position.
     * @return A CompletableFuture that can be completed to get the result:
     * Primary axis position.
     */
    public CompletableFuture<Double> getPositionAsync(
        Units unit) {
        zaber.motion.requests.LockstepGetRequest request =
            new zaber.motion.requests.LockstepGetRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/lockstep_get_pos",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns current position of the primary axis.
     * @return A CompletableFuture that can be completed to get the result:
     * Primary axis position.
     */
    public CompletableFuture<Double> getPositionAsync() {
        return getPositionAsync(Units.NATIVE);
    }

    /**
     * Returns current position of the primary axis.
     * @param unit Units of the position.
     * @return Primary axis position.
     */
    public double getPosition(
        Units unit) {
        try {
            return getPositionAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current position of the primary axis.
     * @return Primary axis position.
     */
    public double getPosition() {
        return getPosition(Units.NATIVE);
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     * @param unit Units of the tolerance.
     * Uses primary axis unit conversion when setting to all axes,
     * otherwise uses specified secondary axis unit conversion.
     * @param axisIndex Optional index of a secondary axis to set the tolerance for.
     * If left empty or set to 0, the tolerance is set to all the secondary axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setToleranceAsync(
        double tolerance,
        Units unit,
        int axisIndex) {
        zaber.motion.requests.LockstepSetRequest request =
            new zaber.motion.requests.LockstepSetRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        request.setValue(tolerance);
        request.setUnit(unit);
        request.setAxisIndex(axisIndex);
        return Call.callAsync("device/lockstep_set_tolerance", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     * @param unit Units of the tolerance.
     * Uses primary axis unit conversion when setting to all axes,
     * otherwise uses specified secondary axis unit conversion.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setToleranceAsync(
        double tolerance,
        Units unit) {
        return setToleranceAsync(tolerance, unit, 0);
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setToleranceAsync(
        double tolerance) {
        return setToleranceAsync(tolerance, Units.NATIVE, 0);
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     * @param unit Units of the tolerance.
     * Uses primary axis unit conversion when setting to all axes,
     * otherwise uses specified secondary axis unit conversion.
     * @param axisIndex Optional index of a secondary axis to set the tolerance for.
     * If left empty or set to 0, the tolerance is set to all the secondary axes.
     */
    public void setTolerance(
        double tolerance,
        Units unit,
        int axisIndex) {
        try {
            setToleranceAsync(tolerance, unit, axisIndex).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     * @param unit Units of the tolerance.
     * Uses primary axis unit conversion when setting to all axes,
     * otherwise uses specified secondary axis unit conversion.
     */
    public void setTolerance(
        double tolerance,
        Units unit) {
        setTolerance(tolerance, unit, 0);
    }

    /**
     * Sets lockstep twist tolerance.
     * Twist tolerances that do not match the system configuration can reduce performance or damage the system.
     * @param tolerance Twist tolerance.
     */
    public void setTolerance(
        double tolerance) {
        setTolerance(tolerance, Units.NATIVE, 0);
    }

    /**
     * Checks if the lockstep group is currently enabled on the device.
     * @return A CompletableFuture that can be completed to get the result:
     * True if a lockstep group with this ID is enabled on the device.
     */
    public CompletableFuture<Boolean> isEnabledAsync() {
        zaber.motion.requests.LockstepEmptyRequest request =
            new zaber.motion.requests.LockstepEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/lockstep_is_enabled",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Checks if the lockstep group is currently enabled on the device.
     * @return True if a lockstep group with this ID is enabled on the device.
     */
    public boolean isEnabled() {
        try {
            return isEnabledAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string which represents the enabled lockstep group.
     * @return String which represents the enabled lockstep group.
     */
    public String toString() {
        zaber.motion.requests.LockstepEmptyRequest request =
            new zaber.motion.requests.LockstepEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setLockstepGroupId(getLockstepGroupId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/lockstep_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
