/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * The axis numbers of a lockstep group.
 */
public final class LockstepAxes implements zaber.motion.dto.Message {

    private int axis1;

    /**
     * The axis number used to set the first axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis1")
    public void setAxis1(int axis1) {
        this.axis1 = axis1;
    }

    /**
     * The axis number used to set the first axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis1")
    public int getAxis1() {
        return this.axis1;
    }

    /**
     * The axis number used to set the first axis.
     */
    public LockstepAxes withAxis1(int aAxis1) {
        this.setAxis1(aAxis1);
        return this;
    }

    private int axis2;

    /**
     * The axis number used to set the second axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis2")
    public void setAxis2(int axis2) {
        this.axis2 = axis2;
    }

    /**
     * The axis number used to set the second axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis2")
    public int getAxis2() {
        return this.axis2;
    }

    /**
     * The axis number used to set the second axis.
     */
    public LockstepAxes withAxis2(int aAxis2) {
        this.setAxis2(aAxis2);
        return this;
    }

    private int axis3;

    /**
     * The axis number used to set the third axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis3")
    public void setAxis3(int axis3) {
        this.axis3 = axis3;
    }

    /**
     * The axis number used to set the third axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis3")
    public int getAxis3() {
        return this.axis3;
    }

    /**
     * The axis number used to set the third axis.
     */
    public LockstepAxes withAxis3(int aAxis3) {
        this.setAxis3(aAxis3);
        return this;
    }

    private int axis4;

    /**
     * The axis number used to set the fourth axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis4")
    public void setAxis4(int axis4) {
        this.axis4 = axis4;
    }

    /**
     * The axis number used to set the fourth axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("axis4")
    public int getAxis4() {
        return this.axis4;
    }

    /**
     * The axis number used to set the fourth axis.
     */
    public LockstepAxes withAxis4(int aAxis4) {
        this.setAxis4(aAxis4);
        return this;
    }

    /**
     * Empty constructor.
     */
    public LockstepAxes() {
    }

    /**
     * Constructor with all properties.
     */
    public LockstepAxes(
        int axis1,
        int axis2,
        int axis3,
        int axis4
    ) {
        this.axis1 = axis1;
        this.axis2 = axis2;
        this.axis3 = axis3;
        this.axis4 = axis4;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        LockstepAxes other = (LockstepAxes) obj;

        return (
            EqualityUtility.equals(axis1, other.axis1)
            && EqualityUtility.equals(axis2, other.axis2)
            && EqualityUtility.equals(axis3, other.axis3)
            && EqualityUtility.equals(axis4, other.axis4)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(axis1),
            EqualityUtility.generateHashCode(axis2),
            EqualityUtility.generateHashCode(axis3),
            EqualityUtility.generateHashCode(axis4)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LockstepAxes { ");
        sb.append("axis1: ");
        sb.append(this.axis1);
        sb.append(", ");
        sb.append("axis2: ");
        sb.append(this.axis2);
        sb.append(", ");
        sb.append("axis3: ");
        sb.append(this.axis3);
        sb.append(", ");
        sb.append("axis4: ");
        sb.append(this.axis4);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static LockstepAxes fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, LockstepAxes.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<LockstepAxes> PARSER =
        new zaber.motion.dto.Parser<LockstepAxes>() {
            @Override
            public LockstepAxes fromByteArray(byte[] data) {
                return LockstepAxes.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<LockstepAxes> parser() {
        return PARSER;
    }

}
