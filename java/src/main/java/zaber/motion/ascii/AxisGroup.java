// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.ArrayUtility;
import zaber.motion.Units;
import zaber.motion.Measurement;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Groups multiple axes across devices into a single group to allow for simultaneous movement.
 * Note that the movement is not coordinated and trajectory is inconsistent and not repeatable between calls.
 * Make sure that any possible trajectory is clear of potential obstacles.
 * The movement methods return after all the axes finish the movement successfully
 * or throw an error as soon as possible.
 */
public class AxisGroup {
    private Axis[] axes;

    /**
     * @return Axes of the group.
     */
    public Axis[] getAxes() {
        return this.axes;
    }

    /**
     * Initializes the group with the axes to be controlled.
     */
    public AxisGroup(
        Axis[] axes) {
        this.axes = axes.clone();
    }

    /**
     * Homes the axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        return Call.callAsync("axes/home", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Homes the axes.
     */
    public void home() {
        try {
            homeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops the axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        return Call.callAsync("axes/stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops the axes.
     */
    public void stop() {
        try {
            stopAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axes to absolute position.
     * @param position Position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        Measurement... position) {
        zaber.motion.requests.AxesMoveRequest request =
            new zaber.motion.requests.AxesMoveRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        request.setPosition(position);
        return Call.callAsync("axes/move_absolute", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axes to absolute position.
     * @param position Position.
     */
    public void moveAbsolute(
        Measurement... position) {
        try {
            moveAbsoluteAsync(position).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move axes to position relative to the current position.
     * @param position Position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        Measurement... position) {
        zaber.motion.requests.AxesMoveRequest request =
            new zaber.motion.requests.AxesMoveRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        request.setPosition(position);
        return Call.callAsync("axes/move_relative", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Move axes to position relative to the current position.
     * @param position Position.
     */
    public void moveRelative(
        Measurement... position) {
        try {
            moveRelativeAsync(position).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves axes to the minimum position as specified by limit.min.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        return Call.callAsync("axes/move_min", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves axes to the minimum position as specified by limit.min.
     */
    public void moveMin() {
        try {
            moveMinAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves axes to the maximum position as specified by limit.max.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        return Call.callAsync("axes/move_max", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves axes to the maximum position as specified by limit.max.
     */
    public void moveMax() {
        try {
            moveMaxAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until all the axes stop moving.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        return Call.callAsync("axes/wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until all the axes stop moving.
     */
    public void waitUntilIdle() {
        try {
            waitUntilIdleAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether any of the axes is executing a motion command.
     * @return A CompletableFuture that can be completed to get the result:
     * True if any of the axes is currently executing a motion command. False otherwise.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "axes/is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether any of the axes is executing a motion command.
     * @return True if any of the axes is currently executing a motion command. False otherwise.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether all the axes are homed.
     * @return A CompletableFuture that can be completed to get the result:
     * True if all the axes are homed. False otherwise.
     */
    public CompletableFuture<Boolean> isHomedAsync() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "axes/is_homed",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether all the axes are homed.
     * @return True if all the axes are homed. False otherwise.
     */
    public boolean isHomed() {
        try {
            return isHomedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current axes position.
     * The positions are requested sequentially.
     * The result position may not be accurate if the axes are moving.
     * @param unit Units of position. You can specify units once or for each axis separately.
     * @return A CompletableFuture that can be completed to get the result:
     * Axes position.
     */
    public CompletableFuture<double[]> getPositionAsync(
        Units... unit) {
        zaber.motion.requests.AxesGetSettingRequest request =
            new zaber.motion.requests.AxesGetSettingRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        request.setSetting("pos");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleArrayResponse> response = Call.callAsync(
            "axes/get_setting",
            request,
            zaber.motion.requests.DoubleArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Returns current axes position.
     * The positions are requested sequentially.
     * The result position may not be accurate if the axes are moving.
     * @param unit Units of position. You can specify units once or for each axis separately.
     * @return Axes position.
     */
    public double[] getPosition(
        Units... unit) {
        try {
            return getPositionAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string that represents the axes.
     * @return A string that represents the axes.
     */
    public String toString() {
        zaber.motion.requests.AxesEmptyRequest request =
            new zaber.motion.requests.AxesEmptyRequest();
        request.setInterfaces(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getConnection().getInterfaceId()));
        request.setDevices(ArrayUtility.arrayToInt(this.axes, axis -> axis.getDevice().getDeviceAddress()));
        request.setAxes(ArrayUtility.arrayToInt(this.axes, axis -> axis.getAxisNumber()));
        zaber.motion.requests.StringResponse response = Call.callSync(
            "axes/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
