// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.Measurement;
import zaber.motion.gateway.Call;
import zaber.motion.RotationDirection;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.StreamSegmentType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * A handle for a stream with this number on the device.
 * Streams provide a way to execute or store a sequence of actions.
 * Stream methods append actions to a queue which executes or stores actions in a first in, first out order.
 */
public class Stream {
    private Device device;

    /**
     * @return Device that controls this stream.
     */
    public Device getDevice() {
        return this.device;
    }

    private int streamId;

    /**
     * @return The number that identifies the stream on the device.
     */
    public int getStreamId() {
        return this.streamId;
    }


    /**
     * @return Current mode of the stream.
     */
    public StreamMode getMode() {
        return this.retrieveMode();
    }


    /**
     * @return An array of axes definitions the stream is set up to control.
     */
    public StreamAxisDefinition[] getAxes() {
        return this.retrieveAxes();
    }

    private StreamIo io;

    /**
     * @return Gets an object that provides access to I/O for this stream.
     */
    public StreamIo getIo() {
        return this.io;
    }

    public Stream(
        Device device, int streamId) {
        this.device = device;
        this.streamId = streamId;
        this.io = new StreamIo(device, streamId);
    }

    /**
     * Setup the stream to control the specified axes and to queue actions on the device.
     * Allows use of lockstep axes in a stream.
     * @param axes Definition of the stream axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupLiveCompositeAsync(
        StreamAxisDefinition... axes) {
        zaber.motion.requests.StreamSetupLiveCompositeRequest request =
            new zaber.motion.requests.StreamSetupLiveCompositeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_live_composite", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the stream to control the specified axes and to queue actions on the device.
     * Allows use of lockstep axes in a stream.
     * @param axes Definition of the stream axes.
     */
    public void setupLiveComposite(
        StreamAxisDefinition... axes) {
        try {
            setupLiveCompositeAsync(axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the stream to control the specified axes and to queue actions on the device.
     * @param axes Numbers of physical axes to setup the stream on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupLiveAsync(
        int... axes) {
        zaber.motion.requests.StreamSetupLiveRequest request =
            new zaber.motion.requests.StreamSetupLiveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_live", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the stream to control the specified axes and to queue actions on the device.
     * @param axes Numbers of physical axes to setup the stream on.
     */
    public void setupLive(
        int... axes) {
        try {
            setupLiveAsync(axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the stream to control the specified axes and queue actions into a stream buffer.
     * Allows use of lockstep axes in a stream.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axes Definition of the stream axes.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupStoreCompositeAsync(
        StreamBuffer streamBuffer,
        StreamAxisDefinition... axes) {
        zaber.motion.requests.StreamSetupStoreCompositeRequest request =
            new zaber.motion.requests.StreamSetupStoreCompositeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setStreamBuffer(streamBuffer.getBufferId());
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_store_composite", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the stream to control the specified axes and queue actions into a stream buffer.
     * Allows use of lockstep axes in a stream.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axes Definition of the stream axes.
     */
    public void setupStoreComposite(
        StreamBuffer streamBuffer,
        StreamAxisDefinition... axes) {
        try {
            setupStoreCompositeAsync(streamBuffer, axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the stream to control the specified axes and queue actions into a stream buffer.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axes Numbers of physical axes to setup the stream on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupStoreAsync(
        StreamBuffer streamBuffer,
        int... axes) {
        zaber.motion.requests.StreamSetupStoreRequest request =
            new zaber.motion.requests.StreamSetupStoreRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setStreamBuffer(streamBuffer.getBufferId());
        request.setAxes(axes);
        return Call.callAsync("device/stream_setup_store", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the stream to control the specified axes and queue actions into a stream buffer.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axes Numbers of physical axes to setup the stream on.
     */
    public void setupStore(
        StreamBuffer streamBuffer,
        int... axes) {
        try {
            setupStoreAsync(streamBuffer, axes).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
     * Afterwards, you may call the resulting stream buffer on arbitrary axes.
     * This mode does not allow for unit conversions.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axesCount The number of axes in the stream.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setupStoreArbitraryAxesAsync(
        StreamBuffer streamBuffer,
        int axesCount) {
        zaber.motion.requests.StreamSetupStoreArbitraryAxesRequest request =
            new zaber.motion.requests.StreamSetupStoreArbitraryAxesRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setStreamBuffer(streamBuffer.getBufferId());
        request.setAxesCount(axesCount);
        return Call.callAsync("device/stream_setup_store_arbitrary_axes", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Setup the stream to use a specified number of axes, and to queue actions in a stream buffer.
     * Afterwards, you may call the resulting stream buffer on arbitrary axes.
     * This mode does not allow for unit conversions.
     * @param streamBuffer The stream buffer to queue actions in.
     * @param axesCount The number of axes in the stream.
     */
    public void setupStoreArbitraryAxes(
        StreamBuffer streamBuffer,
        int axesCount) {
        try {
            setupStoreArbitraryAxesAsync(streamBuffer, axesCount).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Append the actions in a stream buffer to the queue.
     * @param streamBuffer The stream buffer to call.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> callAsync(
        StreamBuffer streamBuffer) {
        zaber.motion.requests.StreamCallRequest request =
            new zaber.motion.requests.StreamCallRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setStreamBuffer(streamBuffer.getBufferId());
        return Call.callAsync("device/stream_call", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Append the actions in a stream buffer to the queue.
     * @param streamBuffer The stream buffer to call.
     */
    public void call(
        StreamBuffer streamBuffer) {
        try {
            callAsync(streamBuffer).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute line movement in the stream.
     * @param endpoint Positions for the axes to move to, relative to their home positions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> lineAbsoluteAsync(
        Measurement... endpoint) {
        zaber.motion.requests.StreamLineRequest request =
            new zaber.motion.requests.StreamLineRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_line", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute line movement in the stream.
     * @param endpoint Positions for the axes to move to, relative to their home positions.
     */
    public void lineAbsolute(
        Measurement... endpoint) {
        try {
            lineAbsoluteAsync(endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative line movement in the stream.
     * @param endpoint Positions for the axes to move to, relative to their positions before movement.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> lineRelativeAsync(
        Measurement... endpoint) {
        zaber.motion.requests.StreamLineRequest request =
            new zaber.motion.requests.StreamLineRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_line", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative line movement in the stream.
     * @param endpoint Positions for the axes to move to, relative to their positions before movement.
     */
    public void lineRelative(
        Measurement... endpoint) {
        try {
            lineRelativeAsync(endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute line movement in the stream, targeting a subset of the stream axes.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param endpoint Positions for the axes to move to, relative to their home positions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> lineAbsoluteOnAsync(
        int[] targetAxesIndices,
        Measurement[] endpoint) {
        zaber.motion.requests.StreamLineRequest request =
            new zaber.motion.requests.StreamLineRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_line", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute line movement in the stream, targeting a subset of the stream axes.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param endpoint Positions for the axes to move to, relative to their home positions.
     */
    public void lineAbsoluteOn(
        int[] targetAxesIndices,
        Measurement[] endpoint) {
        try {
            lineAbsoluteOnAsync(targetAxesIndices, endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative line movement in the stream, targeting a subset of the stream axes.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param endpoint Positions for the axes to move to, relative to their positions before movement.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> lineRelativeOnAsync(
        int[] targetAxesIndices,
        Measurement[] endpoint) {
        zaber.motion.requests.StreamLineRequest request =
            new zaber.motion.requests.StreamLineRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_line", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative line movement in the stream, targeting a subset of the stream axes.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param endpoint Positions for the axes to move to, relative to their positions before movement.
     */
    public void lineRelativeOn(
        int[] targetAxesIndices,
        Measurement[] endpoint) {
        try {
            lineRelativeOnAsync(targetAxesIndices, endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute arc movement on the first two axes of the stream.
     * Absolute meaning that the home positions of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> arcAbsoluteAsync(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        return Call.callAsync("device/stream_arc", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute arc movement on the first two axes of the stream.
     * Absolute meaning that the home positions of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     */
    public void arcAbsolute(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        try {
            arcAbsoluteAsync(rotationDirection, centerX, centerY, endX, endY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative arc movement on the first two axes of the stream.
     * Relative meaning that the current position of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> arcRelativeAsync(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        return Call.callAsync("device/stream_arc", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative arc movement on the first two axes of the stream.
     * Relative meaning that the current position of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     */
    public void arcRelative(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        try {
            arcRelativeAsync(rotationDirection, centerX, centerY, endX, endY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute arc movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> arcAbsoluteOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        return Call.callAsync("device/stream_arc", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute arc movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     */
    public void arcAbsoluteOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        try {
            arcAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative arc movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> arcRelativeOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        return Call.callAsync("device/stream_arc", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative arc movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the arc exists.
     * @param centerY The second dimension of the position of the center of the circle on which the arc exists.
     * @param endX The first dimension of the end position of the arc.
     * @param endY The second dimension of the end position of the arc.
     */
    public void arcRelativeOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY) {
        try {
            arcRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute helix movement in the stream.
     * Requires at least Firmware 7.28.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * The first two axes refer to the helix's arc component,
     * while the rest refers to the helix's line component.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
     * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
     * @param endX The first dimension of the end position of the helix's arc component.
     * @param endY The second dimension of the end position of the helix's arc component.
     * @param endpoint Positions for the helix's line component axes, relative to their home positions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> helixAbsoluteOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY,
        Measurement... endpoint) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_helix", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute helix movement in the stream.
     * Requires at least Firmware 7.28.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * The first two axes refer to the helix's arc component,
     * while the rest refers to the helix's line component.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
     * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
     * @param endX The first dimension of the end position of the helix's arc component.
     * @param endY The second dimension of the end position of the helix's arc component.
     * @param endpoint Positions for the helix's line component axes, relative to their home positions.
     */
    public void helixAbsoluteOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY,
        Measurement... endpoint) {
        try {
            helixAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY, endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative helix movement in the stream.
     * Requires at least Firmware 7.28.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * The first two axes refer to the helix's arc component,
     * while the rest refers to the helix's line component.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
     * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
     * @param endX The first dimension of the end position of the helix's arc component.
     * @param endY The second dimension of the end position of the helix's arc component.
     * @param endpoint Positions for the helix's line component axes, relative to their positions before movement.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> helixRelativeOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY,
        Measurement... endpoint) {
        zaber.motion.requests.StreamArcRequest request =
            new zaber.motion.requests.StreamArcRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        request.setEndX(endX);
        request.setEndY(endY);
        request.setEndpoint(endpoint);
        return Call.callAsync("device/stream_helix", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative helix movement in the stream.
     * Requires at least Firmware 7.28.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * The first two axes refer to the helix's arc component,
     * while the rest refers to the helix's line component.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle on which the helix projects.
     * @param centerY The second dimension of the position of the center of the circle on which the helix projects.
     * @param endX The first dimension of the end position of the helix's arc component.
     * @param endY The second dimension of the end position of the helix's arc component.
     * @param endpoint Positions for the helix's line component axes, relative to their positions before movement.
     */
    public void helixRelativeOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY,
        Measurement endX,
        Measurement endY,
        Measurement... endpoint) {
        try {
            helixRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY, endX, endY, endpoint).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute circle movement on the first two axes of the stream.
     * Absolute meaning that the home positions of the axes are treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> circleAbsoluteAsync(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        zaber.motion.requests.StreamCircleRequest request =
            new zaber.motion.requests.StreamCircleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        return Call.callAsync("device/stream_circle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute circle movement on the first two axes of the stream.
     * Absolute meaning that the home positions of the axes are treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     */
    public void circleAbsolute(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        try {
            circleAbsoluteAsync(rotationDirection, centerX, centerY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative circle movement on the first two axes of the stream.
     * Relative meaning that the current position of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> circleRelativeAsync(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        zaber.motion.requests.StreamCircleRequest request =
            new zaber.motion.requests.StreamCircleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        return Call.callAsync("device/stream_circle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative circle movement on the first two axes of the stream.
     * Relative meaning that the current position of the axes is treated as the origin.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     */
    public void circleRelative(
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        try {
            circleRelativeAsync(rotationDirection, centerX, centerY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue an absolute circle movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> circleAbsoluteOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        zaber.motion.requests.StreamCircleRequest request =
            new zaber.motion.requests.StreamCircleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.ABS);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        return Call.callAsync("device/stream_circle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue an absolute circle movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     */
    public void circleAbsoluteOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        try {
            circleAbsoluteOnAsync(targetAxesIndices, rotationDirection, centerX, centerY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queue a relative circle movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> circleRelativeOnAsync(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        zaber.motion.requests.StreamCircleRequest request =
            new zaber.motion.requests.StreamCircleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setType(StreamSegmentType.REL);
        request.setTargetAxesIndices(targetAxesIndices);
        request.setRotationDirection(rotationDirection);
        request.setCenterX(centerX);
        request.setCenterY(centerY);
        return Call.callAsync("device/stream_circle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Queue a relative circle movement in the stream.
     * The movement will only target the specified subset of axes in the stream.
     * Requires at least Firmware 7.11.
     * @param targetAxesIndices Indices of the axes in the stream the movement targets.
     * Refers to the axes provided during the stream setup or further execution.
     * Indices are zero-based.
     * @param rotationDirection The direction of the rotation.
     * @param centerX The first dimension of the position of the center of the circle.
     * @param centerY The second dimension of the position of the center of the circle.
     */
    public void circleRelativeOn(
        int[] targetAxesIndices,
        RotationDirection rotationDirection,
        Measurement centerX,
        Measurement centerY) {
        try {
            circleRelativeOnAsync(targetAxesIndices, rotationDirection, centerX, centerY).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Wait a specified time.
     * @param time Amount of time to wait.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitAsync(
        double time,
        Units unit) {
        zaber.motion.requests.StreamWaitRequest request =
            new zaber.motion.requests.StreamWaitRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setTime(time);
        request.setUnit(unit);
        return Call.callAsync("device/stream_wait", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Wait a specified time.
     * @param time Amount of time to wait.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitAsync(
        double time) {
        return waitAsync(time, Units.NATIVE);
    }

    /**
     * Wait a specified time.
     * @param time Amount of time to wait.
     * @param unit Units of time.
     */
    public void wait(
        double time,
        Units unit) {
        try {
            waitAsync(time, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Wait a specified time.
     * @param time Amount of time to wait.
     */
    public void wait(
        double time) {
        wait(time, Units.NATIVE);
    }

    /**
     * Waits until the live stream executes all queued actions.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync(
        boolean throwErrorOnFault) {
        zaber.motion.requests.StreamWaitUntilIdleRequest request =
            new zaber.motion.requests.StreamWaitUntilIdleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setThrowErrorOnFault(throwErrorOnFault);
        return Call.callAsync("device/stream_wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until the live stream executes all queued actions.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        return waitUntilIdleAsync(true);
    }

    /**
     * Waits until the live stream executes all queued actions.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     */
    public void waitUntilIdle(
        boolean throwErrorOnFault) {
        try {
            waitUntilIdleAsync(throwErrorOnFault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until the live stream executes all queued actions.
     */
    public void waitUntilIdle() {
        waitUntilIdle(true);
    }

    /**
     * Cork the front of the stream's action queue, blocking execution.
     * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
     * Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
     * You can only cork an idle live stream.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> corkAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        return Call.callAsync("device/stream_cork", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cork the front of the stream's action queue, blocking execution.
     * Execution resumes upon uncorking the queue, or when the number of queued actions reaches its limit.
     * Corking eliminates discontinuities in motion due to subsequent stream commands reaching the device late.
     * You can only cork an idle live stream.
     */
    public void cork() {
        try {
            corkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Uncork the front of the queue, unblocking command execution.
     * You can only uncork an idle live stream that is corked.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> uncorkAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        return Call.callAsync("device/stream_uncork", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Uncork the front of the queue, unblocking command execution.
     * You can only uncork an idle live stream that is corked.
     */
    public void uncork() {
        try {
            uncorkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Pauses or resumes execution of the stream in live mode.
     * The hold only takes effect during execution of motion segments.
     * @param hold True to pause execution, false to resume.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setHoldAsync(
        boolean hold) {
        zaber.motion.requests.StreamSetHoldRequest request =
            new zaber.motion.requests.StreamSetHoldRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setHold(hold);
        return Call.callAsync("device/stream_set_hold", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Pauses or resumes execution of the stream in live mode.
     * The hold only takes effect during execution of motion segments.
     * @param hold True to pause execution, false to resume.
     */
    public void setHold(
        boolean hold) {
        try {
            setHoldAsync(hold).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a boolean value indicating whether the live stream is executing a queued action.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the stream is executing a queued action.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/stream_is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a boolean value indicating whether the live stream is executing a queued action.
     * @return True if the stream is executing a queued action.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of velocity.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum speed of the stream.
     */
    public CompletableFuture<Double> getMaxSpeedAsync(
        Units unit) {
        zaber.motion.requests.StreamGetMaxSpeedRequest request =
            new zaber.motion.requests.StreamGetMaxSpeedRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/stream_get_max_speed",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum speed of the stream.
     */
    public CompletableFuture<Double> getMaxSpeedAsync() {
        return getMaxSpeedAsync(Units.NATIVE);
    }

    /**
     * Gets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of velocity.
     * @return The maximum speed of the stream.
     */
    public double getMaxSpeed(
        Units unit) {
        try {
            return getMaxSpeedAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @return The maximum speed of the stream.
     */
    public double getMaxSpeed() {
        return getMaxSpeed(Units.NATIVE);
    }

    /**
     * Sets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxSpeed Maximum speed at which any stream action is executed.
     * @param unit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxSpeedAsync(
        double maxSpeed,
        Units unit) {
        zaber.motion.requests.StreamSetMaxSpeedRequest request =
            new zaber.motion.requests.StreamSetMaxSpeedRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setMaxSpeed(maxSpeed);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_max_speed", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxSpeed Maximum speed at which any stream action is executed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxSpeedAsync(
        double maxSpeed) {
        return setMaxSpeedAsync(maxSpeed, Units.NATIVE);
    }

    /**
     * Sets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxSpeed Maximum speed at which any stream action is executed.
     * @param unit Units of velocity.
     */
    public void setMaxSpeed(
        double maxSpeed,
        Units unit) {
        try {
            setMaxSpeedAsync(maxSpeed, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the maximum speed of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxSpeed Maximum speed at which any stream action is executed.
     */
    public void setMaxSpeed(
        double maxSpeed) {
        setMaxSpeed(maxSpeed, Units.NATIVE);
    }

    /**
     * Gets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of acceleration.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum tangential acceleration of the live stream.
     */
    public CompletableFuture<Double> getMaxTangentialAccelerationAsync(
        Units unit) {
        zaber.motion.requests.StreamGetMaxTangentialAccelerationRequest request =
            new zaber.motion.requests.StreamGetMaxTangentialAccelerationRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/stream_get_max_tangential_acceleration",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum tangential acceleration of the live stream.
     */
    public CompletableFuture<Double> getMaxTangentialAccelerationAsync() {
        return getMaxTangentialAccelerationAsync(Units.NATIVE);
    }

    /**
     * Gets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of acceleration.
     * @return The maximum tangential acceleration of the live stream.
     */
    public double getMaxTangentialAcceleration(
        Units unit) {
        try {
            return getMaxTangentialAccelerationAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @return The maximum tangential acceleration of the live stream.
     */
    public double getMaxTangentialAcceleration() {
        return getMaxTangentialAcceleration(Units.NATIVE);
    }

    /**
     * Sets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
     * @param unit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxTangentialAccelerationAsync(
        double maxTangentialAcceleration,
        Units unit) {
        zaber.motion.requests.StreamSetMaxTangentialAccelerationRequest request =
            new zaber.motion.requests.StreamSetMaxTangentialAccelerationRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setMaxTangentialAcceleration(maxTangentialAcceleration);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_max_tangential_acceleration", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxTangentialAccelerationAsync(
        double maxTangentialAcceleration) {
        return setMaxTangentialAccelerationAsync(maxTangentialAcceleration, Units.NATIVE);
    }

    /**
     * Sets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
     * @param unit Units of acceleration.
     */
    public void setMaxTangentialAcceleration(
        double maxTangentialAcceleration,
        Units unit) {
        try {
            setMaxTangentialAccelerationAsync(maxTangentialAcceleration, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the maximum tangential acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxTangentialAcceleration Maximum tangential acceleration at which any stream action is executed.
     */
    public void setMaxTangentialAcceleration(
        double maxTangentialAcceleration) {
        setMaxTangentialAcceleration(maxTangentialAcceleration, Units.NATIVE);
    }

    /**
     * Gets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of acceleration.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum centripetal acceleration of the live stream.
     */
    public CompletableFuture<Double> getMaxCentripetalAccelerationAsync(
        Units unit) {
        zaber.motion.requests.StreamGetMaxCentripetalAccelerationRequest request =
            new zaber.motion.requests.StreamGetMaxCentripetalAccelerationRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/stream_get_max_centripetal_acceleration",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum centripetal acceleration of the live stream.
     */
    public CompletableFuture<Double> getMaxCentripetalAccelerationAsync() {
        return getMaxCentripetalAccelerationAsync(Units.NATIVE);
    }

    /**
     * Gets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param unit Units of acceleration.
     * @return The maximum centripetal acceleration of the live stream.
     */
    public double getMaxCentripetalAcceleration(
        Units unit) {
        try {
            return getMaxCentripetalAccelerationAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @return The maximum centripetal acceleration of the live stream.
     */
    public double getMaxCentripetalAcceleration() {
        return getMaxCentripetalAcceleration(Units.NATIVE);
    }

    /**
     * Sets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
     * @param unit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxCentripetalAccelerationAsync(
        double maxCentripetalAcceleration,
        Units unit) {
        zaber.motion.requests.StreamSetMaxCentripetalAccelerationRequest request =
            new zaber.motion.requests.StreamSetMaxCentripetalAccelerationRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setMaxCentripetalAcceleration(maxCentripetalAcceleration);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_max_centripetal_acceleration", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setMaxCentripetalAccelerationAsync(
        double maxCentripetalAcceleration) {
        return setMaxCentripetalAccelerationAsync(maxCentripetalAcceleration, Units.NATIVE);
    }

    /**
     * Sets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
     * @param unit Units of acceleration.
     */
    public void setMaxCentripetalAcceleration(
        double maxCentripetalAcceleration,
        Units unit) {
        try {
            setMaxCentripetalAccelerationAsync(maxCentripetalAcceleration, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the maximum centripetal acceleration of the live stream.
     * Converts the units using the first axis of the stream.
     * @param maxCentripetalAcceleration Maximum centripetal acceleration at which any stream action is executed.
     */
    public void setMaxCentripetalAcceleration(
        double maxCentripetalAcceleration) {
        setMaxCentripetalAcceleration(maxCentripetalAcceleration, Units.NATIVE);
    }

    /**
     * Returns a string which represents the stream.
     * @return String which represents the stream.
     */
    public String toString() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/stream_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Disables the stream.
     * If the stream is not setup, this command does nothing.
     * Once disabled, the stream will no longer accept stream commands.
     * The stream will process the rest of the commands in the queue until it is empty.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        return Call.callAsync("device/stream_disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables the stream.
     * If the stream is not setup, this command does nothing.
     * Once disabled, the stream will no longer accept stream commands.
     * The stream will process the rest of the commands in the queue until it is empty.
     */
    public void disable() {
        try {
            disableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to the stream.
     * Keeps resending the command while the device rejects with AGAIN reason.
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandAsync(
        String command) {
        zaber.motion.requests.StreamGenericCommandRequest request =
            new zaber.motion.requests.StreamGenericCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setCommand(command);
        return Call.callAsync("device/stream_generic_command", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to the stream.
     * Keeps resending the command while the device rejects with AGAIN reason.
     * @param command Command and its parameters.
     */
    public void genericCommand(
        String command) {
        try {
            genericCommandAsync(command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a batch of generic ASCII commands to the stream.
     * Keeps resending command while the device rejects with AGAIN reason.
     * The batch is atomic in terms of thread safety.
     * @param batch Array of commands.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandBatchAsync(
        String[] batch) {
        zaber.motion.requests.StreamGenericCommandBatchRequest request =
            new zaber.motion.requests.StreamGenericCommandBatchRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setBatch(batch);
        return Call.callAsync("device/stream_generic_command_batch", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a batch of generic ASCII commands to the stream.
     * Keeps resending command while the device rejects with AGAIN reason.
     * The batch is atomic in terms of thread safety.
     * @param batch Array of commands.
     */
    public void genericCommandBatch(
        String[] batch) {
        try {
            genericCommandBatchAsync(batch).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queries the stream status from the device
     * and returns boolean indicating whether the stream is disabled.
     * Useful to determine if streaming was interrupted by other movements.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the stream is disabled.
     */
    public CompletableFuture<Boolean> checkDisabledAsync() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/stream_check_disabled",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Queries the stream status from the device
     * and returns boolean indicating whether the stream is disabled.
     * Useful to determine if streaming was interrupted by other movements.
     * @return True if the stream is disabled.
     */
    public boolean checkDisabled() {
        try {
            return checkDisabledAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Makes the stream throw StreamDiscontinuityException when it encounters discontinuities (ND warning flag).
     */
    public void treatDiscontinuitiesAsError() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        Call.callSync("device/stream_treat_discontinuities", request, null);
    }


    /**
     * Prevents StreamDiscontinuityException as a result of expected discontinuity when resuming streaming.
     */
    public void ignoreCurrentDiscontinuity() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        Call.callSync("device/stream_ignore_discontinuity", request, null);
    }


    /**
     * Gets the axes of the stream.
     * @return An array of axis numbers of the axes the stream is set up to control.
     */
    private StreamAxisDefinition[] retrieveAxes() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        zaber.motion.requests.StreamGetAxesResponse response = Call.callSync(
            "device/stream_get_axes",
            request,
            zaber.motion.requests.StreamGetAxesResponse.parser());
        return response.getAxes();
    }


    /**
     * Get the mode of the stream.
     * @return Mode of the stream.
     */
    private StreamMode retrieveMode() {
        zaber.motion.requests.StreamEmptyRequest request =
            new zaber.motion.requests.StreamEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        zaber.motion.requests.StreamModeResponse response = Call.callSync(
            "device/stream_get_mode",
            request,
            zaber.motion.requests.StreamModeResponse.parser());
        return response.getStreamMode();
    }


    /**
     * @deprecated Use Stream.Io.WaitDigitalInput instead.
     *
     * Wait for a digital input channel to reach a given value.
     * @param channelNumber The number of the digital input channel.
     * Channel numbers are numbered from one.
     * @param value The value that the stream should wait for.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> waitDigitalInputAsync(
        int channelNumber,
        boolean value) {
        zaber.motion.requests.StreamWaitDigitalInputRequest request =
            new zaber.motion.requests.StreamWaitDigitalInputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_wait_digital_input", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.WaitDigitalInput instead.
     *
     * Wait for a digital input channel to reach a given value.
     * @param channelNumber The number of the digital input channel.
     * Channel numbers are numbered from one.
     * @param value The value that the stream should wait for.
     */
    public void waitDigitalInput(
        int channelNumber,
        boolean value) {
        try {
            waitDigitalInputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use Stream.Io.WaitAnalogInput instead.
     *
     * Wait for the value of a analog input channel to reach a condition concerning a given value.
     * @param channelNumber The number of the analog input channel.
     * Channel numbers are numbered from one.
     * @param condition A condition (e.g. {@literal <,} {@literal <=,} ==, !=).
     * @param value The value that the condition concerns, in Volts.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> waitAnalogInputAsync(
        int channelNumber,
        String condition,
        double value) {
        zaber.motion.requests.StreamWaitAnalogInputRequest request =
            new zaber.motion.requests.StreamWaitAnalogInputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setChannelNumber(channelNumber);
        request.setCondition(condition);
        request.setValue(value);
        return Call.callAsync("device/stream_wait_analog_input", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.WaitAnalogInput instead.
     *
     * Wait for the value of a analog input channel to reach a condition concerning a given value.
     * @param channelNumber The number of the analog input channel.
     * Channel numbers are numbered from one.
     * @param condition A condition (e.g. {@literal <,} {@literal <=,} ==, !=).
     * @param value The value that the condition concerns, in Volts.
     */
    public void waitAnalogInput(
        int channelNumber,
        String condition,
        double value) {
        try {
            waitAnalogInputAsync(channelNumber, condition, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use Stream.Io.SetDigitalOutput instead.
     *
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setDigitalOutputAsync(
        int channelNumber,
        DigitalOutputAction value) {
        zaber.motion.requests.StreamSetDigitalOutputRequest request =
            new zaber.motion.requests.StreamSetDigitalOutputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_digital_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.SetDigitalOutput instead.
     *
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     */
    public void setDigitalOutput(
        int channelNumber,
        DigitalOutputAction value) {
        try {
            setDigitalOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use Stream.Io.SetAllDigitalOutputs instead.
     *
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAllDigitalOutputsAsync(
        DigitalOutputAction[] values) {
        zaber.motion.requests.StreamSetAllDigitalOutputsRequest request =
            new zaber.motion.requests.StreamSetAllDigitalOutputsRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_digital_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.SetAllDigitalOutputs instead.
     *
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     */
    public void setAllDigitalOutputs(
        DigitalOutputAction[] values) {
        try {
            setAllDigitalOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use Stream.Io.setAnalogOutput instead.
     *
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAnalogOutputAsync(
        int channelNumber,
        double value) {
        zaber.motion.requests.StreamSetAnalogOutputRequest request =
            new zaber.motion.requests.StreamSetAnalogOutputRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_analog_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.setAnalogOutput instead.
     *
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     */
    public void setAnalogOutput(
        int channelNumber,
        double value) {
        try {
            setAnalogOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use Stream.Io.setAllAnalogOutputs instead.
     *
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    @Deprecated
    public CompletableFuture<Void> setAllAnalogOutputsAsync(
        double[] values) {
        zaber.motion.requests.StreamSetAllAnalogOutputsRequest request =
            new zaber.motion.requests.StreamSetAllAnalogOutputsRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setStreamId(getStreamId());
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_analog_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * @deprecated Use Stream.Io.setAllAnalogOutputs instead.
     *
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     */
    public void setAllAnalogOutputs(
        double[] values) {
        try {
            setAllAnalogOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
