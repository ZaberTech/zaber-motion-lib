/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * A parameter used to establish the servo tuning of an axis.
 */
public final class ServoTuningParam implements zaber.motion.dto.Message {

    private String name;

    /**
     * The name of the parameter to set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The name of the parameter to set.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * The name of the parameter to set.
     */
    public ServoTuningParam withName(String aName) {
        this.setName(aName);
        return this;
    }

    private double value;

    /**
     * The value to use for this parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * The value to use for this parameter.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("value")
    public double getValue() {
        return this.value;
    }

    /**
     * The value to use for this parameter.
     */
    public ServoTuningParam withValue(double aValue) {
        this.setValue(aValue);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ServoTuningParam() {
    }

    /**
     * Constructor with all properties.
     */
    public ServoTuningParam(
        String name,
        double value
    ) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ServoTuningParam other = (ServoTuningParam) obj;

        return (
            EqualityUtility.equals(name, other.name)
            && EqualityUtility.equals(value, other.value)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(name),
            EqualityUtility.generateHashCode(value)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ServoTuningParam { ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("value: ");
        sb.append(this.value);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ServoTuningParam fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ServoTuningParam.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ServoTuningParam> PARSER =
        new zaber.motion.dto.Parser<ServoTuningParam>() {
            @Override
            public ServoTuningParam fromByteArray(byte[] data) {
                return ServoTuningParam.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ServoTuningParam> parser() {
        return PARSER;
    }

}
