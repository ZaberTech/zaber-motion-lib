// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing access to I/O for a stream.
 */
public class StreamIo {
    private Device device;

    private int streamId;

    public StreamIo(
        Device device, int streamId) {
        this.device = device;
        this.streamId = streamId;
    }

    /**
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputAsync(
        int channelNumber,
        DigitalOutputAction value) {
        zaber.motion.requests.StreamSetDigitalOutputRequest request =
            new zaber.motion.requests.StreamSetDigitalOutputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_digital_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets value for the specified digital output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform on the channel.
     */
    public void setDigitalOutput(
        int channelNumber,
        DigitalOutputAction value) {
        try {
            setDigitalOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsAsync(
        DigitalOutputAction[] values) {
        zaber.motion.requests.StreamSetAllDigitalOutputsRequest request =
            new zaber.motion.requests.StreamSetAllDigitalOutputsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_digital_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets values for all digital output channels.
     * @param values The type of action to perform on the channel.
     */
    public void setAllDigitalOutputs(
        DigitalOutputAction[] values) {
        try {
            setAllDigitalOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputScheduleAsync(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.StreamSetDigitalOutputScheduleRequest request =
            new zaber.motion.requests.StreamSetDigitalOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        request.setFutureValue(futureValue);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_digital_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDigitalOutputScheduleAsync(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay) {
        return setDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     */
    public void setDigitalOutputSchedule(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay,
        Units unit) {
        try {
            setDigitalOutputScheduleAsync(channelNumber, value, futureValue, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified digital output channel.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @param value The type of action to perform immediately on the channel.
     * @param futureValue The type of action to perform in the future on the channel.
     * @param delay Delay between setting current value and setting future value.
     */
    public void setDigitalOutputSchedule(
        int channelNumber,
        DigitalOutputAction value,
        DigitalOutputAction futureValue,
        double delay) {
        setDigitalOutputSchedule(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsScheduleAsync(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.StreamSetAllDigitalOutputsScheduleRequest request =
            new zaber.motion.requests.StreamSetAllDigitalOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setValues(values);
        request.setFutureValues(futureValues);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_all_digital_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllDigitalOutputsScheduleAsync(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay) {
        return setAllDigitalOutputsScheduleAsync(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     */
    public void setAllDigitalOutputsSchedule(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay,
        Units unit) {
        try {
            setAllDigitalOutputsScheduleAsync(values, futureValues, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future values for all digital output channels.
     * Requires at least Firmware 7.37.
     * @param values The type of actions to perform immediately on output channels.
     * @param futureValues The type of actions to perform in the future on output channels.
     * @param delay Delay between setting current values and setting future values.
     */
    public void setAllDigitalOutputsSchedule(
        DigitalOutputAction[] values,
        DigitalOutputAction[] futureValues,
        double delay) {
        setAllDigitalOutputsSchedule(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputAsync(
        int channelNumber,
        double value) {
        zaber.motion.requests.StreamSetAnalogOutputRequest request =
            new zaber.motion.requests.StreamSetAnalogOutputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_set_analog_output", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets value for the specified analog output channel.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to.
     */
    public void setAnalogOutput(
        int channelNumber,
        double value) {
        try {
            setAnalogOutputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsAsync(
        double[] values) {
        zaber.motion.requests.StreamSetAllAnalogOutputsRequest request =
            new zaber.motion.requests.StreamSetAllAnalogOutputsRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setValues(values);
        return Call.callAsync("device/stream_set_all_analog_outputs", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets values for all analog output channels.
     * @param values Voltage values to set the output channels to.
     */
    public void setAllAnalogOutputs(
        double[] values) {
        try {
            setAllAnalogOutputsAsync(values).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputScheduleAsync(
        int channelNumber,
        double value,
        double futureValue,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.StreamSetAnalogOutputScheduleRequest request =
            new zaber.motion.requests.StreamSetAnalogOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        request.setFutureValue(futureValue);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_analog_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAnalogOutputScheduleAsync(
        int channelNumber,
        double value,
        double futureValue,
        double delay) {
        return setAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     * @param unit Units of time.
     */
    public void setAnalogOutputSchedule(
        int channelNumber,
        double value,
        double futureValue,
        double delay,
        Units unit) {
        try {
            setAnalogOutputScheduleAsync(channelNumber, value, futureValue, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future value for the specified analog output channel.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @param value Value to set the output channel voltage to immediately.
     * @param futureValue Value to set the output channel voltage to in the future.
     * @param delay Delay between setting current value and setting future value.
     */
    public void setAnalogOutputSchedule(
        int channelNumber,
        double value,
        double futureValue,
        double delay) {
        setAnalogOutputSchedule(channelNumber, value, futureValue, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsScheduleAsync(
        double[] values,
        double[] futureValues,
        double delay,
        Units unit) {
        if (delay <= 0) {
            throw new IllegalArgumentException("Delay must be a positive value.");
        }
        zaber.motion.requests.StreamSetAllAnalogOutputsScheduleRequest request =
            new zaber.motion.requests.StreamSetAllAnalogOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setValues(values);
        request.setFutureValues(futureValues);
        request.setDelay(delay);
        request.setUnit(unit);
        return Call.callAsync("device/stream_set_all_analog_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAllAnalogOutputsScheduleAsync(
        double[] values,
        double[] futureValues,
        double delay) {
        return setAllAnalogOutputsScheduleAsync(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     * @param unit Units of time.
     */
    public void setAllAnalogOutputsSchedule(
        double[] values,
        double[] futureValues,
        double delay,
        Units unit) {
        try {
            setAllAnalogOutputsScheduleAsync(values, futureValues, delay, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets current and future values for all analog output channels.
     * Requires at least Firmware 7.38.
     * @param values Voltage values to set the output channels to immediately.
     * @param futureValues Voltage values to set the output channels to in the future.
     * @param delay Delay between setting current values and setting future values.
     */
    public void setAllAnalogOutputsSchedule(
        double[] values,
        double[] futureValues,
        double delay) {
        setAllAnalogOutputsSchedule(values, futureValues, delay, Units.NATIVE);
    }

    /**
     * Cancels a scheduled digital output action.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelDigitalOutputScheduleAsync(
        int channelNumber) {
        zaber.motion.requests.StreamCancelOutputScheduleRequest request =
            new zaber.motion.requests.StreamCancelOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(false);
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        return Call.callAsync("device/stream_cancel_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancels a scheduled digital output action.
     * Requires at least Firmware 7.37.
     * @param channelNumber Channel number starting at 1.
     */
    public void cancelDigitalOutputSchedule(
        int channelNumber) {
        try {
            cancelDigitalOutputScheduleAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllDigitalOutputsScheduleAsync(
        boolean[] channels) {
        zaber.motion.requests.StreamCancelAllOutputsScheduleRequest request =
            new zaber.motion.requests.StreamCancelAllOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(false);
        request.setStreamId(this.streamId);
        request.setChannels(channels);
        return Call.callAsync("device/stream_cancel_all_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllDigitalOutputsScheduleAsync() {
        return cancelAllDigitalOutputsScheduleAsync(new boolean[0]);
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled digital output action for that channel.
     */
    public void cancelAllDigitalOutputsSchedule(
        boolean[] channels) {
        try {
            cancelAllDigitalOutputsScheduleAsync(channels).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled digital output actions.
     * Requires at least Firmware 7.37.
     */
    public void cancelAllDigitalOutputsSchedule() {
        cancelAllDigitalOutputsSchedule(new boolean[0]);
    }

    /**
     * Cancels a scheduled analog output value.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAnalogOutputScheduleAsync(
        int channelNumber) {
        zaber.motion.requests.StreamCancelOutputScheduleRequest request =
            new zaber.motion.requests.StreamCancelOutputScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(true);
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        return Call.callAsync("device/stream_cancel_output_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancels a scheduled analog output value.
     * Requires at least Firmware 7.38.
     * @param channelNumber Channel number starting at 1.
     */
    public void cancelAnalogOutputSchedule(
        int channelNumber) {
        try {
            cancelAnalogOutputScheduleAsync(channelNumber).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllAnalogOutputsScheduleAsync(
        boolean[] channels) {
        zaber.motion.requests.StreamCancelAllOutputsScheduleRequest request =
            new zaber.motion.requests.StreamCancelAllOutputsScheduleRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setAnalog(true);
        request.setStreamId(this.streamId);
        request.setChannels(channels);
        return Call.callAsync("device/stream_cancel_all_outputs_schedule", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> cancelAllAnalogOutputsScheduleAsync() {
        return cancelAllAnalogOutputsScheduleAsync(new boolean[0]);
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     * @param channels Optionally specify which channels to cancel.
     * Array length must be empty or equal to the number of channels on device.
     * Specifying "True" for a channel will cancel the scheduled analog output value for that channel.
     */
    public void cancelAllAnalogOutputsSchedule(
        boolean[] channels) {
        try {
            cancelAllAnalogOutputsScheduleAsync(channels).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Cancel all scheduled analog output actions.
     * Requires at least Firmware 7.38.
     */
    public void cancelAllAnalogOutputsSchedule() {
        cancelAllAnalogOutputsSchedule(new boolean[0]);
    }

    /**
     * Wait for a digital input channel to reach a given value.
     * @param channelNumber The number of the digital input channel.
     * Channel numbers are numbered from one.
     * @param value The value that the stream should wait for.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitDigitalInputAsync(
        int channelNumber,
        boolean value) {
        zaber.motion.requests.StreamWaitDigitalInputRequest request =
            new zaber.motion.requests.StreamWaitDigitalInputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setValue(value);
        return Call.callAsync("device/stream_wait_digital_input", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Wait for a digital input channel to reach a given value.
     * @param channelNumber The number of the digital input channel.
     * Channel numbers are numbered from one.
     * @param value The value that the stream should wait for.
     */
    public void waitDigitalInput(
        int channelNumber,
        boolean value) {
        try {
            waitDigitalInputAsync(channelNumber, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Wait for the value of a analog input channel to reach a condition concerning a given value.
     * @param channelNumber The number of the analog input channel.
     * Channel numbers are numbered from one.
     * @param condition A condition (e.g. {@literal <,} {@literal <=,} ==, !=).
     * @param value The value that the condition concerns, in Volts.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitAnalogInputAsync(
        int channelNumber,
        String condition,
        double value) {
        zaber.motion.requests.StreamWaitAnalogInputRequest request =
            new zaber.motion.requests.StreamWaitAnalogInputRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setStreamId(this.streamId);
        request.setChannelNumber(channelNumber);
        request.setCondition(condition);
        request.setValue(value);
        return Call.callAsync("device/stream_wait_analog_input", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Wait for the value of a analog input channel to reach a condition concerning a given value.
     * @param channelNumber The number of the analog input channel.
     * Channel numbers are numbered from one.
     * @param condition A condition (e.g. {@literal <,} {@literal <=,} ==, !=).
     * @param value The value that the condition concerns, in Volts.
     */
    public void waitAnalogInput(
        int channelNumber,
        String condition,
        double value) {
        try {
            waitAnalogInputAsync(channelNumber, condition, value).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
