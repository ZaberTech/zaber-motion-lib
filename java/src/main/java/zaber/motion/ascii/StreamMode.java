/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Mode of a stream.
 */
public enum StreamMode {

    DISABLED(0),

    STORE(1),

    STORE_ARBITRARY_AXES(2),

    LIVE(3);

    private int value;

    StreamMode(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static StreamMode valueOf(int argValue) {
        for (StreamMode value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
