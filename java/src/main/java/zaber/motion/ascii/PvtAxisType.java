/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Denotes type of the PVT sequence axis.
 */
public enum PvtAxisType {

    PHYSICAL(0),

    LOCKSTEP(1);

    private int value;

    PvtAxisType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static PvtAxisType valueOf(int argValue) {
        for (PvtAxisType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
