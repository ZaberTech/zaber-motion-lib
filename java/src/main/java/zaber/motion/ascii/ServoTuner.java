// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Exposes the capabilities to inspect and edit an axis' servo tuning.
 * Requires at least Firmware 6.25 or 7.00.
 */
public class ServoTuner {
    private Axis axis;

    /**
     * @return The axis that will be tuned.
     */
    public Axis getAxis() {
        return this.axis;
    }

    /**
     * Creates instance of ServoTuner for the given axis.
     */
    public ServoTuner(
        Axis axis) {
        this.axis = axis;
    }

    /**
     * Get the paramset that this device uses by default when it starts up.
     * @return A CompletableFuture that can be completed to get the result:
     * The paramset used when the device restarts.
     */
    public CompletableFuture<ServoTuningParamset> getStartupParamsetAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        CompletableFuture<zaber.motion.requests.ServoTuningParamsetResponse> response = Call.callAsync(
            "servotuning/get_startup_set",
            request,
            zaber.motion.requests.ServoTuningParamsetResponse.parser());
        return response
            .thenApply(r -> r.getParamset());
    }

    /**
     * Get the paramset that this device uses by default when it starts up.
     * @return The paramset used when the device restarts.
     */
    public ServoTuningParamset getStartupParamset() {
        try {
            return getStartupParamsetAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the paramset that this device uses by default when it starts up.
     * @param paramset The paramset to use at startup.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setStartupParamsetAsync(
        ServoTuningParamset paramset) {
        zaber.motion.requests.ServoTuningRequest request =
            new zaber.motion.requests.ServoTuningRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        return Call.callAsync("servotuning/set_startup_set", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set the paramset that this device uses by default when it starts up.
     * @param paramset The paramset to use at startup.
     */
    public void setStartupParamset(
        ServoTuningParamset paramset) {
        try {
            setStartupParamsetAsync(paramset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Load the values from one paramset into another.
     * @param toParamset The paramset to load into.
     * @param fromParamset The paramset to load from.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> loadParamsetAsync(
        ServoTuningParamset toParamset,
        ServoTuningParamset fromParamset) {
        zaber.motion.requests.LoadParamset request =
            new zaber.motion.requests.LoadParamset();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setToParamset(toParamset);
        request.setFromParamset(fromParamset);
        return Call.callAsync("servotuning/load_paramset", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Load the values from one paramset into another.
     * @param toParamset The paramset to load into.
     * @param fromParamset The paramset to load from.
     */
    public void loadParamset(
        ServoTuningParamset toParamset,
        ServoTuningParamset fromParamset) {
        try {
            loadParamsetAsync(toParamset, fromParamset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the full set of tuning parameters used by the firmware driving this axis.
     * @param paramset The paramset to get tuning for.
     * @return A CompletableFuture that can be completed to get the result:
     * The raw representation of the current tuning.
     */
    public CompletableFuture<ParamsetInfo> getTuningAsync(
        ServoTuningParamset paramset) {
        zaber.motion.requests.ServoTuningRequest request =
            new zaber.motion.requests.ServoTuningRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        CompletableFuture<ParamsetInfo> response = Call.callAsync(
            "servotuning/get_raw",
            request,
            ParamsetInfo.parser());
        return response;
    }

    /**
     * Get the full set of tuning parameters used by the firmware driving this axis.
     * @param paramset The paramset to get tuning for.
     * @return The raw representation of the current tuning.
     */
    public ParamsetInfo getTuning(
        ServoTuningParamset paramset) {
        try {
            return getTuningAsync(paramset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set individual tuning parameters.
     * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
     * @param paramset The paramset to set tuning of.
     * @param tuningParams The params to set.
     * @param setUnspecifiedToDefault If true, any tuning parameters not included in TuningParams
     * are reset to their default values.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        boolean setUnspecifiedToDefault) {
        zaber.motion.requests.SetServoTuningRequest request =
            new zaber.motion.requests.SetServoTuningRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        request.setTuningParams(tuningParams);
        request.setSetUnspecifiedToDefault(setUnspecifiedToDefault);
        return Call.callAsync("servotuning/set_raw", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set individual tuning parameters.
     * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
     * @param paramset The paramset to set tuning of.
     * @param tuningParams The params to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams) {
        return setTuningAsync(paramset, tuningParams, false);
    }

    /**
     * Set individual tuning parameters.
     * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
     * @param paramset The paramset to set tuning of.
     * @param tuningParams The params to set.
     * @param setUnspecifiedToDefault If true, any tuning parameters not included in TuningParams
     * are reset to their default values.
     */
    public void setTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        boolean setUnspecifiedToDefault) {
        try {
            setTuningAsync(paramset, tuningParams, setUnspecifiedToDefault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set individual tuning parameters.
     * Only use this method if you have a strong understanding of Zaber specific tuning parameters.
     * @param paramset The paramset to set tuning of.
     * @param tuningParams The params to set.
     */
    public void setTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams) {
        setTuning(paramset, tuningParams, false);
    }

    /**
     * Sets the tuning of a paramset using the PID method.
     * @param paramset The paramset to get tuning for.
     * @param p The proportional gain. Must be in units of N/m.
     * @param i The integral gain. Must be in units of N/m⋅s.
     * @param d The derivative gain. Must be in units of N⋅s/m.
     * @param fc The cutoff frequency. Must be in units of Hz.
     * @return A CompletableFuture that can be completed to get the result:
     * The PID representation of the current tuning after your changes have been applied.
     */
    public CompletableFuture<PidTuning> setPidTuningAsync(
        ServoTuningParamset paramset,
        double p,
        double i,
        double d,
        double fc) {
        zaber.motion.requests.SetServoTuningPIDRequest request =
            new zaber.motion.requests.SetServoTuningPIDRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        request.setP(p);
        request.setI(i);
        request.setD(d);
        request.setFc(fc);
        CompletableFuture<PidTuning> response = Call.callAsync(
            "servotuning/set_pid",
            request,
            PidTuning.parser());
        return response;
    }

    /**
     * Sets the tuning of a paramset using the PID method.
     * @param paramset The paramset to get tuning for.
     * @param p The proportional gain. Must be in units of N/m.
     * @param i The integral gain. Must be in units of N/m⋅s.
     * @param d The derivative gain. Must be in units of N⋅s/m.
     * @param fc The cutoff frequency. Must be in units of Hz.
     * @return The PID representation of the current tuning after your changes have been applied.
     */
    public PidTuning setPidTuning(
        ServoTuningParamset paramset,
        double p,
        double i,
        double d,
        double fc) {
        try {
            return setPidTuningAsync(paramset, p, i, d, fc).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the PID representation of this paramset's servo tuning.
     * @param paramset The paramset to get tuning for.
     * @return A CompletableFuture that can be completed to get the result:
     * The PID representation of the current tuning.
     */
    public CompletableFuture<PidTuning> getPidTuningAsync(
        ServoTuningParamset paramset) {
        zaber.motion.requests.ServoTuningRequest request =
            new zaber.motion.requests.ServoTuningRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        CompletableFuture<PidTuning> response = Call.callAsync(
            "servotuning/get_pid",
            request,
            PidTuning.parser());
        return response;
    }

    /**
     * Gets the PID representation of this paramset's servo tuning.
     * @param paramset The paramset to get tuning for.
     * @return The PID representation of the current tuning.
     */
    public PidTuning getPidTuning(
        ServoTuningParamset paramset) {
        try {
            return getPidTuningAsync(paramset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the parameters that are required to tune this device.
     * @return A CompletableFuture that can be completed to get the result:
     * The tuning parameters.
     */
    public CompletableFuture<SimpleTuningParamDefinition[]> getSimpleTuningParamDefinitionsAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        CompletableFuture<zaber.motion.requests.GetSimpleTuningParamDefinitionResponse> response = Call.callAsync(
            "servotuning/get_simple_params_definition",
            request,
            zaber.motion.requests.GetSimpleTuningParamDefinitionResponse.parser());
        return response
            .thenApply(r -> r.getParams());
    }

    /**
     * Gets the parameters that are required to tune this device.
     * @return The tuning parameters.
     */
    public SimpleTuningParamDefinition[] getSimpleTuningParamDefinitions() {
        try {
            return getSimpleTuningParamDefinitionsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the tuning of this device using the simple input method.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setSimpleTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass,
        Double carriageMass) {
        zaber.motion.requests.SetSimpleTuning request =
            new zaber.motion.requests.SetSimpleTuning();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        request.setTuningParams(tuningParams);
        request.setLoadMass(loadMass);
        request.setCarriageMass(carriageMass);
        return Call.callAsync("servotuning/set_simple_tuning", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set the tuning of this device using the simple input method.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setSimpleTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass) {
        return setSimpleTuningAsync(paramset, tuningParams, loadMass, null);
    }

    /**
     * Set the tuning of this device using the simple input method.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
     */
    public void setSimpleTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass,
        Double carriageMass) {
        try {
            setSimpleTuningAsync(paramset, tuningParams, loadMass, carriageMass).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the tuning of this device using the simple input method.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     */
    public void setSimpleTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass) {
        setSimpleTuning(paramset, tuningParams, loadMass, null);
    }

    /**
     * Get the simple tuning parameters for this device.
     * @param paramset The paramset to get tuning for.
     * @return A CompletableFuture that can be completed to get the result:
     * The simple tuning parameters.
     */
    public CompletableFuture<SimpleTuning> getSimpleTuningAsync(
        ServoTuningParamset paramset) {
        zaber.motion.requests.ServoTuningRequest request =
            new zaber.motion.requests.ServoTuningRequest();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        CompletableFuture<SimpleTuning> response = Call.callAsync(
            "servotuning/get_simple_tuning",
            request,
            SimpleTuning.parser());
        return response;
    }

    /**
     * Get the simple tuning parameters for this device.
     * @param paramset The paramset to get tuning for.
     * @return The simple tuning parameters.
     */
    public SimpleTuning getSimpleTuning(
        ServoTuningParamset paramset) {
        try {
            return getSimpleTuningAsync(paramset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use GetSimpleTuning instead.
     *
     * Checks if the provided simple tuning is being stored by this paramset.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the provided simple tuning is currently stored in this paramset.
     */
    @Deprecated
    public CompletableFuture<Boolean> isUsingSimpleTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass,
        Double carriageMass) {
        zaber.motion.requests.SetSimpleTuning request =
            new zaber.motion.requests.SetSimpleTuning();
        request.setInterfaceId(getAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getAxis().getDevice().getDeviceAddress());
        request.setAxis(getAxis().getAxisNumber());
        request.setParamset(paramset);
        request.setTuningParams(tuningParams);
        request.setLoadMass(loadMass);
        request.setCarriageMass(carriageMass);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "servotuning/is_using_simple_tuning",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * @deprecated Use GetSimpleTuning instead.
     *
     * Checks if the provided simple tuning is being stored by this paramset.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the provided simple tuning is currently stored in this paramset.
     */
    @Deprecated
    public CompletableFuture<Boolean> isUsingSimpleTuningAsync(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass) {
        return isUsingSimpleTuningAsync(paramset, tuningParams, loadMass, null);
    }

    /**
     * @deprecated Use GetSimpleTuning instead.
     *
     * Checks if the provided simple tuning is being stored by this paramset.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @param carriageMass The mass of the carriage in kg. If this value is not set the default carriage mass is used.
     * @return True if the provided simple tuning is currently stored in this paramset.
     */
    public boolean isUsingSimpleTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass,
        Double carriageMass) {
        try {
            return isUsingSimpleTuningAsync(paramset, tuningParams, loadMass, carriageMass).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use GetSimpleTuning instead.
     *
     * Checks if the provided simple tuning is being stored by this paramset.
     * @param paramset The paramset to set tuning for.
     * @param tuningParams The params used to tune this device.
     * To get what parameters are expected, call GetSimpleTuningParamList.
     * All values must be between 0 and 1.
     * @param loadMass The mass loaded on the stage (excluding the mass of the carriage itself) in kg.
     * @return True if the provided simple tuning is currently stored in this paramset.
     */
    @Deprecated
    public boolean isUsingSimpleTuning(
        ServoTuningParamset paramset,
        ServoTuningParam[] tuningParams,
        double loadMass) {
        return isUsingSimpleTuning(paramset, tuningParams, loadMass, null);
    }

}
