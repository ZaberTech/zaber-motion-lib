/* This file is generated. Do not modify by hand. */

package zaber.motion.ascii;

/**
 * Denotes type of the stream axis.
 */
public enum StreamAxisType {

    PHYSICAL(0),

    LOCKSTEP(1);

    private int value;

    StreamAxisType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static StreamAxisType valueOf(int argValue) {
        for (StreamAxisType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
