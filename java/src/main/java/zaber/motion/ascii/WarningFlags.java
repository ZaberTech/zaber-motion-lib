// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

/**
 * Warning flag constants that indicate whether any device fault or warning is active.
 * For more information please refer to the
 * [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_message_format_warning_flags).
 */
public final class WarningFlags {
    private WarningFlags() {
    }

    /**
    * Critical System Error.
    */
    public static final String CRITICAL_SYSTEM_ERROR = "FF";

    /**
    * Peripheral Not Supported.
    */
    public static final String PERIPHERAL_NOT_SUPPORTED = "FN";

    /**
    * Peripheral Inactive.
    */
    public static final String PERIPHERAL_INACTIVE = "FZ";

    /**
    * Hardware Emergency Stop Driver Disabled.
    */
    public static final String HARDWARE_EMERGENCY_STOP = "FH";

    /**
    * Overvoltage or Undervoltage Driver Disabled.
    */
    public static final String OVERVOLTAGE_OR_UNDERVOLTAGE = "FV";

    /**
    * Driver Disabled on Startup or by Command.
    * Devices with Firmware 7.11 and above.
    */
    public static final String DRIVER_DISABLED_NO_FAULT = "FO";

    /**
    * Current Inrush Error.
    */
    public static final String CURRENT_INRUSH_ERROR = "FC";

    /**
    * Motor Temperature Error.
    */
    public static final String MOTOR_TEMPERATURE_ERROR = "FM";

    /**
    * Driver Disabled.
    * Devices with Firmware 7.10 and lower.
    */
    public static final String DRIVER_DISABLED = "FD";

    /**
    * Encoder Error.
    */
    public static final String ENCODER_ERROR = "FQ";

    /**
    * Index Error.
    */
    public static final String INDEX_ERROR = "FI";

    /**
    * Analog Encoder Sync Error.
    */
    public static final String ANALOG_ENCODER_SYNC_ERROR = "FA";

    /**
    * Overdrive Limit Exceeded.
    */
    public static final String OVERDRIVE_LIMIT_EXCEEDED = "FR";

    /**
    * Stalled and Stopped.
    */
    public static final String STALLED_AND_STOPPED = "FS";

    /**
    * Stream Bounds Error.
    */
    public static final String STREAM_BOUNDS_ERROR = "FB";

    /**
    * Interpolated Path Deviation.
    */
    public static final String INTERPOLATED_PATH_DEVIATION = "FP";

    /**
    * Limit Error.
    */
    public static final String LIMIT_ERROR = "FE";

    /**
    * Excessive Twist.
    */
    public static final String EXCESSIVE_TWIST = "FT";

    /**
    * Unexpected Limit Trigger.
    */
    public static final String UNEXPECTED_LIMIT_TRIGGER = "WL";

    /**
    * Voltage Out of Range.
    */
    public static final String VOLTAGE_OUT_OF_RANGE = "WV";

    /**
    * Controller Temperature High.
    */
    public static final String CONTROLLER_TEMPERATURE_HIGH = "WT";

    /**
    * Stalled with Recovery.
    */
    public static final String STALLED_WITH_RECOVERY = "WS";

    /**
    * Displaced When Stationary.
    */
    public static final String DISPLACED_WHEN_STATIONARY = "WM";

    /**
    * Invalid Calibration Type.
    */
    public static final String INVALID_CALIBRATION_TYPE = "WP";

    /**
    * No Reference Position.
    */
    public static final String NO_REFERENCE_POSITION = "WR";

    /**
    * Device Not Homed.
    */
    public static final String DEVICE_NOT_HOMED = "WH";

    /**
    * Manual Control.
    */
    public static final String MANUAL_CONTROL = "NC";

    /**
    * Movement Interrupted.
    */
    public static final String MOVEMENT_INTERRUPTED = "NI";

    /**
    * Stream Discontinuity.
    */
    public static final String STREAM_DISCONTINUITY = "ND";

    /**
    * Value Rounded.
    */
    public static final String VALUE_ROUNDED = "NR";

    /**
    * Value Truncated.
    */
    public static final String VALUE_TRUNCATED = "NT";

    /**
    * Setting Update Pending.
    */
    public static final String SETTING_UPDATE_PENDING = "NU";

    /**
    * Joystick Calibrating.
    */
    public static final String JOYSTICK_CALIBRATING = "NJ";

    /**
    * Device in Firmware Update Mode.
    * Firmware 6.xx only.
    */
    public static final String FIRMWARE_UPDATE_MODE = "NB";

}
