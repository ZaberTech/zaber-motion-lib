// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.Units;
import zaber.motion.Measurement;
import zaber.motion.FirmwareVersion;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.AxisMoveType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents an axis of motion associated with a device.
 */
public class Axis {
    private Device device;

    /**
     * @return Device that controls this axis.
     */
    public Device getDevice() {
        return this.device;
    }

    private int axisNumber;

    /**
     * @return The axis number identifies the axis on the device.
     * The first axis has the number one.
     */
    public int getAxisNumber() {
        return this.axisNumber;
    }

    private AxisSettings settings;

    /**
     * @return Settings and properties of this axis.
     */
    public AxisSettings getSettings() {
        return this.settings;
    }

    private AxisStorage storage;

    /**
     * @return Key-value storage of this axis.
     * Requires at least Firmware 7.30.
     */
    public AxisStorage getStorage() {
        return this.storage;
    }

    private Warnings warnings;

    /**
     * @return Warnings and faults of this axis.
     */
    public Warnings getWarnings() {
        return this.warnings;
    }


    /**
     * @return Identity of the axis.
     */
    public AxisIdentity getIdentity() {
        return this.retrieveIdentity();
    }


    /**
     * @return Unique ID of the peripheral hardware.
     */
    public int getPeripheralId() {
        return this.getIdentity().getPeripheralId();
    }


    /**
     * @return Name of the peripheral.
     */
    public String getPeripheralName() {
        return this.getIdentity().getPeripheralName();
    }


    /**
     * @return Serial number of the peripheral, or 0 when not applicable.
     */
    public long getPeripheralSerialNumber() {
        return this.getIdentity().getPeripheralSerialNumber();
    }


    /**
     * @return Indicates whether the axis is a peripheral or part of an integrated device.
     */
    public boolean getIsPeripheral() {
        return this.getIdentity().getIsPeripheral();
    }


    /**
     * @return Determines the type of an axis and units it accepts.
     */
    public AxisType getAxisType() {
        return this.getIdentity().getAxisType();
    }


    /**
     * @return User-assigned label of the peripheral.
     */
    public String getLabel() {
        return this.retrieveLabel();
    }

    public Axis(
        Device device, int axisNumber) {
        this.device = device;
        this.axisNumber = axisNumber;
        this.settings = new AxisSettings(this);
        this.storage = new AxisStorage(this);
        this.warnings = new Warnings(device, axisNumber);
    }

    /**
     * Homes axis. Axis returns to its homing position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceHomeRequest request =
            new zaber.motion.requests.DeviceHomeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/home", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Homes axis. Axis returns to its homing position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> homeAsync() {
        return homeAsync(true);
    }

    /**
     * Homes axis. Axis returns to its homing position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void home(
        boolean waitUntilIdle) {
        try {
            homeAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Homes axis. Axis returns to its homing position.
     */
    public void home() {
        home(true);
    }

    /**
     * Stops ongoing axis movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceStopRequest request =
            new zaber.motion.requests.DeviceStopRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops ongoing axis movement. Decelerates until zero speed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync() {
        return stopAsync(true);
    }

    /**
     * Stops ongoing axis movement. Decelerates until zero speed.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void stop(
        boolean waitUntilIdle) {
        try {
            stopAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops ongoing axis movement. Decelerates until zero speed.
     */
    public void stop() {
        stop(true);
    }

    /**
     * Parks the axis in anticipation of turning the power off.
     * It can later be powered on, unparked, and moved without first having to home it.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> parkAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        return Call.callAsync("device/park", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Parks the axis in anticipation of turning the power off.
     * It can later be powered on, unparked, and moved without first having to home it.
     */
    public void park() {
        try {
            parkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Unparks axis. Axis will now be able to move.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> unparkAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        return Call.callAsync("device/unpark", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Unparks axis. Axis will now be able to move.
     */
    public void unpark() {
        try {
            unparkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether the axis is parked or not.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the axis is currently parked. False otherwise.
     */
    public CompletableFuture<Boolean> isParkedAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/is_parked",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether the axis is parked or not.
     * @return True if the axis is currently parked. False otherwise.
     */
    public boolean isParked() {
        try {
            return isParkedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until axis stops moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync(
        boolean throwErrorOnFault) {
        zaber.motion.requests.DeviceWaitUntilIdleRequest request =
            new zaber.motion.requests.DeviceWaitUntilIdleRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setThrowErrorOnFault(throwErrorOnFault);
        return Call.callAsync("device/wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until axis stops moving.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        return waitUntilIdleAsync(true);
    }

    /**
     * Waits until axis stops moving.
     * @param throwErrorOnFault Determines whether to throw error when fault is observed.
     */
    public void waitUntilIdle(
        boolean throwErrorOnFault) {
        try {
            waitUntilIdleAsync(throwErrorOnFault).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Waits until axis stops moving.
     */
    public void waitUntilIdle() {
        waitUntilIdle(true);
    }

    /**
     * Returns bool indicating whether the axis is executing a motion command.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the axis is currently executing a motion command.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether the axis is executing a motion command.
     * @return True if the axis is currently executing a motion command.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether the axis has position reference and was homed.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the axis has position reference and was homed.
     */
    public CompletableFuture<Boolean> isHomedAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "device/is_homed",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether the axis has position reference and was homed.
     * @return True if the axis has position reference and was homed.
     */
    public boolean isHomed() {
        try {
            return isHomedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.ABS);
        request.setArg(position);
        request.setUnit(unit);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        return moveAbsoluteAsync(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position,
        Units unit) {
        return moveAbsoluteAsync(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveAbsoluteAsync(
        double position) {
        return moveAbsoluteAsync(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveAbsoluteAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        moveAbsolute(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveAbsolute(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        moveAbsolute(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     * @param unit Units of position.
     */
    public void moveAbsolute(
        double position,
        Units unit) {
        moveAbsolute(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to absolute position.
     * @param position Absolute position.
     */
    public void moveAbsolute(
        double position) {
        moveAbsolute(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.MAX);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveMaxAsync(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle,
        double velocity) {
        return moveMaxAsync(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync(
        boolean waitUntilIdle) {
        return moveMaxAsync(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMaxAsync() {
        return moveMaxAsync(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveMaxAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveMax(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveMax(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMax(
        boolean waitUntilIdle,
        double velocity) {
        moveMax(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveMax(
        boolean waitUntilIdle) {
        moveMax(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the maximum position as specified by limit.max.
     */
    public void moveMax() {
        moveMax(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.MIN);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveMinAsync(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle,
        double velocity) {
        return moveMinAsync(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync(
        boolean waitUntilIdle) {
        return moveMinAsync(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveMinAsync() {
        return moveMinAsync(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveMinAsync(waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveMin(waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveMin(waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveMin(
        boolean waitUntilIdle,
        double velocity) {
        moveMin(waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveMin(
        boolean waitUntilIdle) {
        moveMin(waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to the minimum position as specified by limit.min.
     */
    public void moveMin() {
        moveMin(true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.REL);
        request.setArg(position);
        request.setUnit(unit);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        return moveRelativeAsync(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        return moveRelativeAsync(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position,
        Units unit) {
        return moveRelativeAsync(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveRelativeAsync(
        double position) {
        return moveRelativeAsync(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveRelativeAsync(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveRelative(position, unit, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveRelative(position, unit, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle,
        double velocity) {
        moveRelative(position, unit, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveRelative(
        double position,
        Units unit,
        boolean waitUntilIdle) {
        moveRelative(position, unit, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     * @param unit Units of position.
     */
    public void moveRelative(
        double position,
        Units unit) {
        moveRelative(position, unit, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Move axis to position relative to current position.
     * @param position Relative position.
     */
    public void moveRelative(
        double position) {
        moveRelative(position, Units.NATIVE, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.VEL);
        request.setArg(velocity);
        request.setUnit(unit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit,
        double acceleration) {
        return moveVelocityAsync(velocity, unit, acceleration, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity,
        Units unit) {
        return moveVelocityAsync(velocity, unit, 0, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveVelocityAsync(
        double velocity) {
        return moveVelocityAsync(velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveVelocity(
        double velocity,
        Units unit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveVelocityAsync(velocity, unit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveVelocity(
        double velocity,
        Units unit,
        double acceleration) {
        moveVelocity(velocity, unit, acceleration, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     * @param unit Units of velocity.
     */
    public void moveVelocity(
        double velocity,
        Units unit) {
        moveVelocity(velocity, unit, 0, Units.NATIVE);
    }

    /**
     * Begins to move axis at specified speed.
     * @param velocity Movement velocity.
     */
    public void moveVelocity(
        double velocity) {
        moveVelocity(velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Returns current axis position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to get the result:
     * Axis position.
     */
    public CompletableFuture<Double> getPositionAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setSetting("pos");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns current axis position.
     * @return A CompletableFuture that can be completed to get the result:
     * Axis position.
     */
    public CompletableFuture<Double> getPositionAsync() {
        return getPositionAsync(Units.NATIVE);
    }

    /**
     * Returns current axis position.
     * @param unit Units of position.
     * @return Axis position.
     */
    public double getPosition(
        Units unit) {
        try {
            return getPositionAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current axis position.
     * @return Axis position.
     */
    public double getPosition() {
        return getPosition(Units.NATIVE);
    }

    /**
     * Gets number of index positions of the axis.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of index positions.
     */
    public CompletableFuture<Integer> getNumberOfIndexPositionsAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_count",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets number of index positions of the axis.
     * @return Number of index positions.
     */
    public int getNumberOfIndexPositions() {
        try {
            return getNumberOfIndexPositionsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current axis index position.
     * @return A CompletableFuture that can be completed to get the result:
     * Index position starting from 1 or 0 if the position is not an index position.
     */
    public CompletableFuture<Integer> getIndexPositionAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_position",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns current axis index position.
     * @return Index position starting from 1 or 0 if the position is not an index position.
     */
    public int getIndexPosition() {
        try {
            return getIndexPositionAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setType(AxisMoveType.INDEX);
        request.setArgInt(index);
        request.setWaitUntilIdle(waitUntilIdle);
        request.setVelocity(velocity);
        request.setVelocityUnit(velocityUnit);
        request.setAcceleration(acceleration);
        request.setAccelerationUnit(accelerationUnit);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        return moveIndexAsync(index, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        return moveIndexAsync(index, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index,
        boolean waitUntilIdle,
        double velocity) {
        return moveIndexAsync(index, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index,
        boolean waitUntilIdle) {
        return moveIndexAsync(index, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveIndexAsync(
        int index) {
        return moveIndexAsync(index, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     * @param accelerationUnit Units of acceleration.
     */
    public void moveIndex(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration,
        Units accelerationUnit) {
        try {
            moveIndexAsync(index, waitUntilIdle, velocity, velocityUnit, acceleration, accelerationUnit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     * @param acceleration Movement acceleration.
     * Default value of 0 indicates that the accel setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveIndex(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit,
        double acceleration) {
        moveIndex(index, waitUntilIdle, velocity, velocityUnit, acceleration, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     * @param velocityUnit Units of velocity.
     */
    public void moveIndex(
        int index,
        boolean waitUntilIdle,
        double velocity,
        Units velocityUnit) {
        moveIndex(index, waitUntilIdle, velocity, velocityUnit, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @param velocity Movement velocity.
     * Default value of 0 indicates that the maxspeed setting is used instead.
     * Requires at least Firmware 7.25.
     */
    public void moveIndex(
        int index,
        boolean waitUntilIdle,
        double velocity) {
        moveIndex(index, waitUntilIdle, velocity, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveIndex(
        int index,
        boolean waitUntilIdle) {
        moveIndex(index, waitUntilIdle, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Moves the axis to index position.
     * @param index Index position. Index positions are numbered from 1.
     */
    public void moveIndex(
        int index) {
        moveIndex(index, true, 0, Units.NATIVE, 0, Units.NATIVE);
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<Response> response = Call.callAsync(
            "interface/generic_command",
            request,
            Response.parser());
        return response;
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors) {
        return genericCommandAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command) {
        return genericCommandAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors) {
        return genericCommand(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this axis.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command) {
        return genericCommand(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.GenericCommandResponseCollection> response = Call.callAsync(
            "interface/generic_command_multi_response",
            request,
            zaber.motion.requests.GenericCommandResponseCollection.parser());
        return response
            .thenApply(r -> r.getResponses());
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponseAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command) {
        return genericCommandMultiResponseAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandMultiResponseAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponse(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this axis and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command) {
        return genericCommandMultiResponse(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setCommand(command);
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to this axis without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     */
    public void genericCommandNoResponse(
        String command) {
        try {
            genericCommandNoResponseAsync(command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Formats parameters into a command and performs unit conversions.
     * Parameters in the command template are denoted by a question mark.
     * Command returned is only valid for this axis and this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
     * @param parameters Variable number of command parameters.
     * @return Command with converted parameters.
     */
    public String prepareCommand(
        String commandTemplate,
        Measurement... parameters) {
        zaber.motion.requests.PrepareCommandRequest request =
            new zaber.motion.requests.PrepareCommandRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setCommandTemplate(commandTemplate);
        request.setParameters(parameters);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/prepare_command",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Sets the user-assigned peripheral label.
     * The label is stored on the controller and recognized by other software.
     * @param label Label to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLabelAsync(
        String label) {
        zaber.motion.requests.DeviceSetStorageRequest request =
            new zaber.motion.requests.DeviceSetStorageRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setValue(label);
        return Call.callAsync("device/set_label", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the user-assigned peripheral label.
     * The label is stored on the controller and recognized by other software.
     * @param label Label to set.
     */
    public void setLabel(
        String label) {
        try {
            setLabelAsync(label).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the peripheral name.
     * @return The label.
     */
    private String retrieveLabel() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/get_label",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns a string that represents the axis.
     * @return A string that represents the axis.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/axis_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns a serialization of the current axis state that can be saved and reapplied.
     * @return A CompletableFuture that can be completed to get the result:
     * A serialization of the current state of the axis.
     */
    public CompletableFuture<String> getStateAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_state",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a serialization of the current axis state that can be saved and reapplied.
     * @return A serialization of the current state of the axis.
     */
    public String getState() {
        try {
            return getStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Applies a saved state to this axis.
     * @param state The state object to apply to this axis.
     * @return A CompletableFuture that can be completed to get the result:
     * Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public CompletableFuture<SetStateAxisResponse> setStateAsync(
        String state) {
        zaber.motion.requests.SetStateRequest request =
            new zaber.motion.requests.SetStateRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setState(state);
        CompletableFuture<SetStateAxisResponse> response = Call.callAsync(
            "device/set_axis_state",
            request,
            SetStateAxisResponse.parser());
        return response;
    }

    /**
     * Applies a saved state to this axis.
     * @param state The state object to apply to this axis.
     * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public SetStateAxisResponse setState(
        String state) {
        try {
            return setStateAsync(state).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this axis.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this axis.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state,
        FirmwareVersion firmwareVersion) {
        zaber.motion.requests.CanSetStateRequest request =
            new zaber.motion.requests.CanSetStateRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setState(state);
        request.setFirmwareVersion(firmwareVersion);
        CompletableFuture<zaber.motion.requests.CanSetStateAxisResponse> response = Call.callAsync(
            "device/can_set_axis_state",
            request,
            zaber.motion.requests.CanSetStateAxisResponse.parser());
        return response
            .thenApply(r -> r.getError());
    }

    /**
     * Checks if a state can be applied to this axis.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this axis.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state) {
        return canSetStateAsync(state, null);
    }

    /**
     * Checks if a state can be applied to this axis.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return An explanation of why this state cannot be set to this axis.
     */
    public String canSetState(
        String state,
        FirmwareVersion firmwareVersion) {
        try {
            return canSetStateAsync(state, firmwareVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this axis.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return An explanation of why this state cannot be set to this axis.
     */
    public String canSetState(
        String state) {
        return canSetState(state, null);
    }

    /**
     * Returns identity.
     * @return Axis identity.
     */
    private AxisIdentity retrieveIdentity() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        AxisIdentity response = Call.callSync(
            "device/get_axis_identity",
            request,
            AxisIdentity.parser());
        return response;
    }


    /**
     * Disables the driver, which prevents current from being sent to the motor or load.
     * If the driver is already disabled, the driver remains disabled.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverDisableAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        return Call.callAsync("device/driver_disable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables the driver, which prevents current from being sent to the motor or load.
     * If the driver is already disabled, the driver remains disabled.
     */
    public void driverDisable() {
        try {
            driverDisableAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to enable the driver repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverEnableAsync(
        double timeout) {
        zaber.motion.requests.DriverEnableRequest request =
            new zaber.motion.requests.DriverEnableRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setTimeout(timeout);
        return Call.callAsync("device/driver_enable", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Attempts to enable the driver repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> driverEnableAsync() {
        return driverEnableAsync(10);
    }

    /**
     * Attempts to enable the driver repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     * @param timeout Timeout in seconds. Specify 0 to attempt to enable the driver once.
     */
    public void driverEnable(
        double timeout) {
        try {
            driverEnableAsync(timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to enable the driver repeatedly for the specified timeout.
     * If the driver is already enabled, the driver remains enabled.
     */
    public void driverEnable() {
        driverEnable(10);
    }

    /**
     * Activates a peripheral on this axis.
     * Removes all identity information for the device.
     * Run the identify method on the device after activating to refresh the information.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> activateAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        return Call.callAsync("device/activate", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Activates a peripheral on this axis.
     * Removes all identity information for the device.
     * Run the identify method on the device after activating to refresh the information.
     */
    public void activate() {
        try {
            activateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Restores all axis settings to their default values.
     * Deletes all zaber axis storage keys.
     * Disables lockstep if the axis is part of one. Unparks the axis.
     * Preserves storage.
     * The device needs to be identified again after the restore.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> restoreAsync() {
        zaber.motion.requests.DeviceRestoreRequest request =
            new zaber.motion.requests.DeviceRestoreRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        return Call.callAsync("device/restore", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Restores all axis settings to their default values.
     * Deletes all zaber axis storage keys.
     * Disables lockstep if the axis is part of one. Unparks the axis.
     * Preserves storage.
     * The device needs to be identified again after the restore.
     */
    public void restore() {
        try {
            restoreAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count,
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceMoveSinRequest request =
            new zaber.motion.requests.DeviceMoveSinRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setAmplitude(amplitude);
        request.setAmplitudeUnits(amplitudeUnits);
        request.setPeriod(period);
        request.setPeriodUnits(periodUnits);
        request.setCount(count);
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/move_sin", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count) {
        return moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, true);
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinAsync(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits) {
        return moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, 0, true);
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     * @param waitUntilIdle Determines whether function should return after the movement is finished or just started.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count,
        boolean waitUntilIdle) {
        try {
            moveSinAsync(amplitude, amplitudeUnits, period, periodUnits, count, waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     * @param count Number of sinusoidal cycles to complete.
     * Must be a multiple of 0.5
     * If count is not specified or set to 0, the axis will move indefinitely.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits,
        double count) {
        moveSin(amplitude, amplitudeUnits, period, periodUnits, count, true);
    }

    /**
     * Moves the axis in a sinusoidal trajectory.
     * @param amplitude Amplitude of the sinusoidal motion (half of the motion's peak-to-peak range).
     * @param amplitudeUnits Units of position.
     * @param period Period of the sinusoidal motion in milliseconds.
     * @param periodUnits Units of time.
     */
    public void moveSin(
        double amplitude,
        Units amplitudeUnits,
        double period,
        Units periodUnits) {
        moveSin(amplitude, amplitudeUnits, period, periodUnits, 0, true);
    }

    /**
     * Stops the axis at the end of the sinusoidal trajectory.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @param waitUntilIdle Determines whether function should return after the movement is finished.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinStopAsync(
        boolean waitUntilIdle) {
        zaber.motion.requests.DeviceStopRequest request =
            new zaber.motion.requests.DeviceStopRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(getAxisNumber());
        request.setWaitUntilIdle(waitUntilIdle);
        return Call.callAsync("device/move_sin_stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops the axis at the end of the sinusoidal trajectory.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> moveSinStopAsync() {
        return moveSinStopAsync(true);
    }

    /**
     * Stops the axis at the end of the sinusoidal trajectory.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     * @param waitUntilIdle Determines whether function should return after the movement is finished.
     */
    public void moveSinStop(
        boolean waitUntilIdle) {
        try {
            moveSinStopAsync(waitUntilIdle).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops the axis at the end of the sinusoidal trajectory.
     * If the sinusoidal motion was started with an integer-plus-half cycle count,
     * the motion ends at the half-way point of the sinusoidal trajectory.
     */
    public void moveSinStop() {
        moveSinStop(true);
    }

}
