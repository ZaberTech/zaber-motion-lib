// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.FirmwareVersion;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.Measurement;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents the controller part of one device - may be either a standalone controller or an integrated controller.
 */
public class Device {
    private Connection connection;

    /**
     * @return Connection of this device.
     */
    public Connection getConnection() {
        return this.connection;
    }

    private int deviceAddress;

    /**
     * @return The device address uniquely identifies the device on the connection.
     * It can be configured or automatically assigned by the renumber command.
     */
    public int getDeviceAddress() {
        return this.deviceAddress;
    }

    private DeviceSettings settings;

    /**
     * @return Settings and properties of this device.
     */
    public DeviceSettings getSettings() {
        return this.settings;
    }

    private DeviceStorage storage;

    /**
     * @return Key-value storage of this device.
     */
    public DeviceStorage getStorage() {
        return this.storage;
    }

    private DeviceIO io;

    /**
     * @return I/O channels of this device.
     */
    public DeviceIO getIO() {
        return this.io;
    }

    private AllAxes allAxes;

    /**
     * @return Virtual axis which allows you to target all axes of this device.
     */
    public AllAxes getAllAxes() {
        return this.allAxes;
    }

    private Warnings warnings;

    /**
     * @return Warnings and faults of this device and all its axes.
     */
    public Warnings getWarnings() {
        return this.warnings;
    }


    /**
     * @return Identity of the device.
     */
    public DeviceIdentity getIdentity() {
        return this.retrieveIdentity();
    }


    /**
     * @return Indicates whether or not the device has been identified.
     */
    public boolean getIsIdentified() {
        return this.retrieveIsIdentified();
    }

    private Oscilloscope oscilloscope;

    /**
     * @return Oscilloscope recording helper for this device.
     * Requires at least Firmware 7.00.
     */
    public Oscilloscope getOscilloscope() {
        return this.oscilloscope;
    }


    /**
     * @return Unique ID of the device hardware.
     */
    public int getDeviceId() {
        return this.getIdentity().getDeviceId();
    }


    /**
     * @return Serial number of the device.
     */
    public long getSerialNumber() {
        return this.getIdentity().getSerialNumber();
    }


    /**
     * @return Name of the product.
     */
    public String getName() {
        return this.getIdentity().getName();
    }


    /**
     * @return Number of axes this device has.
     */
    public int getAxisCount() {
        return this.getIdentity().getAxisCount();
    }


    /**
     * @return Version of the firmware.
     */
    public FirmwareVersion getFirmwareVersion() {
        return this.getIdentity().getFirmwareVersion();
    }


    /**
     * @return The device is an integrated product.
     */
    public boolean getIsIntegrated() {
        return this.getIdentity().getIsIntegrated();
    }


    /**
     * @return User-assigned label of the device.
     */
    public String getLabel() {
        return this.retrieveLabel();
    }

    private Triggers triggers;

    /**
     * @return Triggers for this device.
     * Requires at least Firmware 7.06.
     */
    public Triggers getTriggers() {
        return this.triggers;
    }

    private Streams streams;

    /**
     * @return Gets an object that provides access to Streams on this device.
     * Requires at least Firmware 7.05.
     */
    public Streams getStreams() {
        return this.streams;
    }

    private Pvt pvt;

    /**
     * @return Gets an object that provides access to PVT functions of this device.
     * Note that as of ZML v5.0.0, this returns a Pvt object and NOT a PvtSequence object.
     * The PvtSequence can now be obtained from the Pvt object.
     * Requires at least Firmware 7.33.
     */
    public Pvt getPvt() {
        return this.pvt;
    }

    public Device(
        Connection connection, int deviceAddress) {
        this.connection = connection;
        this.deviceAddress = deviceAddress;
        this.settings = new DeviceSettings(this);
        this.storage = new DeviceStorage(this);
        this.io = new DeviceIO(this);
        this.allAxes = new AllAxes(this);
        this.warnings = new Warnings(this, 0);
        this.oscilloscope = new Oscilloscope(this);
        this.triggers = new Triggers(this);
        this.streams = new Streams(this);
        this.pvt = new Pvt(this);
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @param assumeVersion The identification assumes the specified firmware version
     * instead of the version queried from the device.
     * Providing this argument can lead to unexpected compatibility issues.
     * @return A CompletableFuture that can be completed to get the result:
     * Device identification data.
     */
    public CompletableFuture<DeviceIdentity> identifyAsync(
        FirmwareVersion assumeVersion) {
        zaber.motion.requests.DeviceIdentifyRequest request =
            new zaber.motion.requests.DeviceIdentifyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setAssumeVersion(assumeVersion);
        CompletableFuture<DeviceIdentity> response = Call.callAsync(
            "device/identify",
            request,
            DeviceIdentity.parser());
        return response;
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @return A CompletableFuture that can be completed to get the result:
     * Device identification data.
     */
    public CompletableFuture<DeviceIdentity> identifyAsync() {
        return identifyAsync(null);
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @param assumeVersion The identification assumes the specified firmware version
     * instead of the version queried from the device.
     * Providing this argument can lead to unexpected compatibility issues.
     * @return Device identification data.
     */
    public DeviceIdentity identify(
        FirmwareVersion assumeVersion) {
        try {
            return identifyAsync(assumeVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @return Device identification data.
     */
    public DeviceIdentity identify() {
        return identify(null);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int axis,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setAxis(axis);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<Response> response = Call.callAsync(
            "interface/generic_command",
            request,
            Response.parser());
        return response;
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int axis,
        boolean checkErrors) {
        return genericCommandAsync(command, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        int axis) {
        return genericCommandAsync(command, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command) {
        return genericCommandAsync(command, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int axis,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandAsync(command, axis, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int axis,
        boolean checkErrors) {
        return genericCommand(command, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        int axis) {
        return genericCommand(command, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command) {
        return genericCommand(command, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int axis,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setAxis(axis);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.GenericCommandResponseCollection> response = Call.callAsync(
            "interface/generic_command_multi_response",
            request,
            zaber.motion.requests.GenericCommandResponseCollection.parser());
        return response
            .thenApply(r -> r.getResponses());
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int axis,
        boolean checkErrors) {
        return genericCommandMultiResponseAsync(command, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        int axis) {
        return genericCommandMultiResponseAsync(command, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command) {
        return genericCommandMultiResponseAsync(command, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int axis,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandMultiResponseAsync(command, axis, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int axis,
        boolean checkErrors) {
        return genericCommandMultiResponse(command, axis, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        int axis) {
        return genericCommandMultiResponse(command, axis, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device and expect multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command) {
        return genericCommandMultiResponse(command, 0, true, 0);
    }

    /**
     * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * Specifying -1 omits the number completely.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command,
        int axis) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setAxis(axis);
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command) {
        return genericCommandNoResponseAsync(command, 0);
    }

    /**
     * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param axis Optional axis number to send the command to.
     * Specifying -1 omits the number completely.
     */
    public void genericCommandNoResponse(
        String command,
        int axis) {
        try {
            genericCommandNoResponseAsync(command, axis).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this device without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     */
    public void genericCommandNoResponse(
        String command) {
        genericCommandNoResponse(command, 0);
    }

    /**
     * Gets an Axis class instance which allows you to control a particular axis on this device.
     * Axes are numbered from 1.
     * @param axisNumber Number of axis intended to control.
     * @return Axis instance.
     */
    public Axis getAxis(
        int axisNumber) {
        if (axisNumber <= 0) {
            throw new IllegalArgumentException("Invalid value; physical axes are numbered from 1.");
        }
        return new Axis(this, axisNumber);
    }


    /**
     * Gets a Lockstep class instance which allows you to control a particular lockstep group on the device.
     * Requires at least Firmware 6.15 or 7.11.
     * @param lockstepGroupId The ID of the lockstep group to control. Lockstep group IDs start at one.
     * @return Lockstep instance.
     */
    public Lockstep getLockstep(
        int lockstepGroupId) {
        if (lockstepGroupId <= 0) {
            throw new IllegalArgumentException("Invalid value; lockstep groups are numbered from 1.");
        }
        return new Lockstep(this, lockstepGroupId);
    }


    /**
     * Formats parameters into a command and performs unit conversions.
     * Parameters in the command template are denoted by a question mark.
     * Command returned is only valid for this device.
     * Unit conversion is not supported for commands where axes can be remapped, such as stream and PVT commands.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param commandTemplate Template of a command to prepare. Parameters are denoted by question marks.
     * @param parameters Variable number of command parameters.
     * @return Command with converted parameters.
     */
    public String prepareCommand(
        String commandTemplate,
        Measurement... parameters) {
        zaber.motion.requests.PrepareCommandRequest request =
            new zaber.motion.requests.PrepareCommandRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommandTemplate(commandTemplate);
        request.setParameters(parameters);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/prepare_command",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Sets the user-assigned device label.
     * The label is stored on the controller and recognized by other software.
     * @param label Label to set.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLabelAsync(
        String label) {
        zaber.motion.requests.DeviceSetStorageRequest request =
            new zaber.motion.requests.DeviceSetStorageRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setValue(label);
        return Call.callAsync("device/set_label", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the user-assigned device label.
     * The label is stored on the controller and recognized by other software.
     * @param label Label to set.
     */
    public void setLabel(
        String label) {
        try {
            setLabelAsync(label).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the device name.
     * @return The label.
     */
    private String retrieveLabel() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/get_label",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns a serialization of the current device state that can be saved and reapplied.
     * @return A CompletableFuture that can be completed to get the result:
     * A serialization of the current state of the device.
     */
    public CompletableFuture<String> getStateAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_state",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a serialization of the current device state that can be saved and reapplied.
     * @return A serialization of the current state of the device.
     */
    public String getState() {
        try {
            return getStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Applies a saved state to this device.
     * @param state The state object to apply to this device.
     * @param deviceOnly If true, only device scope settings and features will be set.
     * @return A CompletableFuture that can be completed to get the result:
     * Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public CompletableFuture<SetStateDeviceResponse> setStateAsync(
        String state,
        boolean deviceOnly) {
        zaber.motion.requests.SetStateRequest request =
            new zaber.motion.requests.SetStateRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setState(state);
        request.setDeviceOnly(deviceOnly);
        CompletableFuture<SetStateDeviceResponse> response = Call.callAsync(
            "device/set_device_state",
            request,
            SetStateDeviceResponse.parser());
        return response;
    }

    /**
     * Applies a saved state to this device.
     * @param state The state object to apply to this device.
     * @return A CompletableFuture that can be completed to get the result:
     * Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public CompletableFuture<SetStateDeviceResponse> setStateAsync(
        String state) {
        return setStateAsync(state, false);
    }

    /**
     * Applies a saved state to this device.
     * @param state The state object to apply to this device.
     * @param deviceOnly If true, only device scope settings and features will be set.
     * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public SetStateDeviceResponse setState(
        String state,
        boolean deviceOnly) {
        try {
            return setStateAsync(state, deviceOnly).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Applies a saved state to this device.
     * @param state The state object to apply to this device.
     * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public SetStateDeviceResponse setState(
        String state) {
        return setState(state, false);
    }

    /**
     * Checks if a state can be applied to this device.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return A CompletableFuture that can be completed to get the result:
     * An object listing errors that come up when trying to set the state.
     */
    public CompletableFuture<CanSetStateDeviceResponse> canSetStateAsync(
        String state,
        FirmwareVersion firmwareVersion) {
        zaber.motion.requests.CanSetStateRequest request =
            new zaber.motion.requests.CanSetStateRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setState(state);
        request.setFirmwareVersion(firmwareVersion);
        CompletableFuture<CanSetStateDeviceResponse> response = Call.callAsync(
            "device/can_set_state",
            request,
            CanSetStateDeviceResponse.parser());
        return response;
    }

    /**
     * Checks if a state can be applied to this device.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return A CompletableFuture that can be completed to get the result:
     * An object listing errors that come up when trying to set the state.
     */
    public CompletableFuture<CanSetStateDeviceResponse> canSetStateAsync(
        String state) {
        return canSetStateAsync(state, null);
    }

    /**
     * Checks if a state can be applied to this device.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return An object listing errors that come up when trying to set the state.
     */
    public CanSetStateDeviceResponse canSetState(
        String state,
        FirmwareVersion firmwareVersion) {
        try {
            return canSetStateAsync(state, firmwareVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this device.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return An object listing errors that come up when trying to set the state.
     */
    public CanSetStateDeviceResponse canSetState(
        String state) {
        return canSetState(state, null);
    }

    /**
     * Waits for the device to start responding to messages.
     * Useful to call after resetting the device.
     * Throws RequestTimeoutException upon timeout.
     * @param timeout For how long to wait in milliseconds for the device to start responding.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitToRespondAsync(
        double timeout) {
        zaber.motion.requests.WaitToRespondRequest request =
            new zaber.motion.requests.WaitToRespondRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setTimeout(timeout);
        return Call.callAsync("device/wait_to_respond", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits for the device to start responding to messages.
     * Useful to call after resetting the device.
     * Throws RequestTimeoutException upon timeout.
     * @param timeout For how long to wait in milliseconds for the device to start responding.
     */
    public void waitToRespond(
        double timeout) {
        try {
            waitToRespondAsync(timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Changes the address of this device.
     * After the address is successfully changed, the existing device class instance no longer represents the device.
     * Instead, use the new device instance returned by this method.
     * @param address The new address to assign to the device.
     * @return A CompletableFuture that can be completed to get the result:
     * New device instance with the new address.
     */
    public CompletableFuture<Device> renumberAsync(
        int address) {
        if (address < 1 || address > 99) {
            throw new IllegalArgumentException("Invalid value; device addresses are numbered from 1 to 99.");
        }
        zaber.motion.requests.RenumberRequest request =
            new zaber.motion.requests.RenumberRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setAddress(address);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/renumber",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> new Device(this.connection, r.getValue()));
    }

    /**
     * Changes the address of this device.
     * After the address is successfully changed, the existing device class instance no longer represents the device.
     * Instead, use the new device instance returned by this method.
     * @param address The new address to assign to the device.
     * @return New device instance with the new address.
     */
    public Device renumber(
        int address) {
        try {
            return renumberAsync(address).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Restores most of the settings to their default values.
     * Deletes all triggers, stream and PVT buffers, servo tunings.
     * Deletes all zaber storage keys.
     * Disables locksteps, unparks axes.
     * Preserves storage, communication settings, peripherals (unless hard is specified).
     * The device needs to be identified again after the restore.
     * @param hard If true, completely erases device's memory. The device also resets.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> restoreAsync(
        boolean hard) {
        zaber.motion.requests.DeviceRestoreRequest request =
            new zaber.motion.requests.DeviceRestoreRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setHard(hard);
        return Call.callAsync("device/restore", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Restores most of the settings to their default values.
     * Deletes all triggers, stream and PVT buffers, servo tunings.
     * Deletes all zaber storage keys.
     * Disables locksteps, unparks axes.
     * Preserves storage, communication settings, peripherals (unless hard is specified).
     * The device needs to be identified again after the restore.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> restoreAsync() {
        return restoreAsync(false);
    }

    /**
     * Restores most of the settings to their default values.
     * Deletes all triggers, stream and PVT buffers, servo tunings.
     * Deletes all zaber storage keys.
     * Disables locksteps, unparks axes.
     * Preserves storage, communication settings, peripherals (unless hard is specified).
     * The device needs to be identified again after the restore.
     * @param hard If true, completely erases device's memory. The device also resets.
     */
    public void restore(
        boolean hard) {
        try {
            restoreAsync(hard).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Restores most of the settings to their default values.
     * Deletes all triggers, stream and PVT buffers, servo tunings.
     * Deletes all zaber storage keys.
     * Disables locksteps, unparks axes.
     * Preserves storage, communication settings, peripherals (unless hard is specified).
     * The device needs to be identified again after the restore.
     */
    public void restore() {
        restore(false);
    }

    /**
     * Returns identity.
     * @return Device identity.
     */
    private DeviceIdentity retrieveIdentity() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        DeviceIdentity response = Call.callSync(
            "device/get_identity",
            request,
            DeviceIdentity.parser());
        return response;
    }


    /**
     * Returns whether or not the device have been identified.
     * @return True if the device has already been identified. False otherwise.
     */
    private boolean retrieveIsIdentified() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        zaber.motion.requests.BoolResponse response = Call.callSync(
            "device/get_is_identified",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response.getValue();
    }


}
