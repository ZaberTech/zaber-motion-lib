// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.ascii;

import zaber.motion.ArrayUtility;
import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Provides a convenient way to control the oscilloscope data recording feature of some devices.
 * The oscilloscope can record the values of some settings over time at high resolution.
 * Requires at least Firmware 7.00.
 */
public class Oscilloscope {
    private Device device;

    /**
     * @return Device that this Oscilloscope measures.
     */
    public Device getDevice() {
        return this.device;
    }

    public Oscilloscope(
        Device device) {
        this.device = device;
    }

    /**
     * Select a setting to be recorded.
     * @param axis The 1-based index of the axis to record the value from.
     * @param setting The name of a setting to record.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> addChannelAsync(
        int axis,
        String setting) {
        zaber.motion.requests.OscilloscopeAddSettingChannelRequest request =
            new zaber.motion.requests.OscilloscopeAddSettingChannelRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(axis);
        request.setSetting(setting);
        return Call.callAsync("oscilloscope/add_setting_channel", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Select a setting to be recorded.
     * @param axis The 1-based index of the axis to record the value from.
     * @param setting The name of a setting to record.
     */
    public void addChannel(
        int axis,
        String setting) {
        try {
            addChannelAsync(axis, setting).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Select an I/O pin to be recorded.
     * Requires at least Firmware 7.33.
     * @param ioType The I/O port type to read data from.
     * @param ioChannel The 1-based index of the I/O pin to read from.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> addIoChannelAsync(
        IoPortType ioType,
        int ioChannel) {
        zaber.motion.requests.OscilloscopeAddIoChannelRequest request =
            new zaber.motion.requests.OscilloscopeAddIoChannelRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setIoType(ioType);
        request.setIoChannel(ioChannel);
        return Call.callAsync("oscilloscope/add_io_channel", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Select an I/O pin to be recorded.
     * Requires at least Firmware 7.33.
     * @param ioType The I/O port type to read data from.
     * @param ioChannel The 1-based index of the I/O pin to read from.
     */
    public void addIoChannel(
        IoPortType ioType,
        int ioChannel) {
        try {
            addIoChannelAsync(ioType, ioChannel).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Clear the list of channels to record.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> clearAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        return Call.callAsync("oscilloscope/clear_channels", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Clear the list of channels to record.
     */
    public void clear() {
        try {
            clearAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the current sampling interval.
     * @param unit Unit of measure to represent the timebase in.
     * @return A CompletableFuture that can be completed to get the result:
     * The current sampling interval in the selected time units.
     */
    public CompletableFuture<Double> getTimebaseAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.timebase");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the current sampling interval.
     * @return A CompletableFuture that can be completed to get the result:
     * The current sampling interval in the selected time units.
     */
    public CompletableFuture<Double> getTimebaseAsync() {
        return getTimebaseAsync(Units.NATIVE);
    }

    /**
     * Get the current sampling interval.
     * @param unit Unit of measure to represent the timebase in.
     * @return The current sampling interval in the selected time units.
     */
    public double getTimebase(
        Units unit) {
        try {
            return getTimebaseAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the current sampling interval.
     * @return The current sampling interval in the selected time units.
     */
    public double getTimebase() {
        return getTimebase(Units.NATIVE);
    }

    /**
     * Set the sampling interval.
     * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
     * @param unit Unit of measure the timebase is represented in.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setTimebaseAsync(
        double interval,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.timebase");
        request.setValue(interval);
        request.setUnit(unit);
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set the sampling interval.
     * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setTimebaseAsync(
        double interval) {
        return setTimebaseAsync(interval, Units.NATIVE);
    }

    /**
     * Set the sampling interval.
     * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
     * @param unit Unit of measure the timebase is represented in.
     */
    public void setTimebase(
        double interval,
        Units unit) {
        try {
            setTimebaseAsync(interval, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the sampling interval.
     * @param interval Sample interval for the next oscilloscope recording. Minimum value is 100µs.
     */
    public void setTimebase(
        double interval) {
        setTimebase(interval, Units.NATIVE);
    }

    /**
     * Get the current sampling frequency.
     * The values is calculated as the inverse of the current sampling interval.
     * @param unit Unit of measure to represent the frequency in.
     * @return A CompletableFuture that can be completed to get the result:
     * The inverse of current sampling interval in the selected units.
     */
    public CompletableFuture<Double> getFrequencyAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.timebase");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "oscilloscope/get_frequency",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the current sampling frequency.
     * The values is calculated as the inverse of the current sampling interval.
     * @return A CompletableFuture that can be completed to get the result:
     * The inverse of current sampling interval in the selected units.
     */
    public CompletableFuture<Double> getFrequencyAsync() {
        return getFrequencyAsync(Units.NATIVE);
    }

    /**
     * Get the current sampling frequency.
     * The values is calculated as the inverse of the current sampling interval.
     * @param unit Unit of measure to represent the frequency in.
     * @return The inverse of current sampling interval in the selected units.
     */
    public double getFrequency(
        Units unit) {
        try {
            return getFrequencyAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the current sampling frequency.
     * The values is calculated as the inverse of the current sampling interval.
     * @return The inverse of current sampling interval in the selected units.
     */
    public double getFrequency() {
        return getFrequency(Units.NATIVE);
    }

    /**
     * Set the sampling frequency (inverse of the sampling interval).
     * The value is quantized to the next closest value supported by the firmware.
     * @param frequency Sample frequency for the next oscilloscope recording.
     * @param unit Unit of measure the frequency is represented in.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setFrequencyAsync(
        double frequency,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.timebase");
        request.setValue(frequency);
        request.setUnit(unit);
        return Call.callAsync("oscilloscope/set_frequency", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set the sampling frequency (inverse of the sampling interval).
     * The value is quantized to the next closest value supported by the firmware.
     * @param frequency Sample frequency for the next oscilloscope recording.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setFrequencyAsync(
        double frequency) {
        return setFrequencyAsync(frequency, Units.NATIVE);
    }

    /**
     * Set the sampling frequency (inverse of the sampling interval).
     * The value is quantized to the next closest value supported by the firmware.
     * @param frequency Sample frequency for the next oscilloscope recording.
     * @param unit Unit of measure the frequency is represented in.
     */
    public void setFrequency(
        double frequency,
        Units unit) {
        try {
            setFrequencyAsync(frequency, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the sampling frequency (inverse of the sampling interval).
     * The value is quantized to the next closest value supported by the firmware.
     * @param frequency Sample frequency for the next oscilloscope recording.
     */
    public void setFrequency(
        double frequency) {
        setFrequency(frequency, Units.NATIVE);
    }

    /**
     * Get the delay before oscilloscope recording starts.
     * @param unit Unit of measure to represent the delay in.
     * @return A CompletableFuture that can be completed to get the result:
     * The current start delay in the selected time units.
     */
    public CompletableFuture<Double> getDelayAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.delay");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the delay before oscilloscope recording starts.
     * @return A CompletableFuture that can be completed to get the result:
     * The current start delay in the selected time units.
     */
    public CompletableFuture<Double> getDelayAsync() {
        return getDelayAsync(Units.NATIVE);
    }

    /**
     * Get the delay before oscilloscope recording starts.
     * @param unit Unit of measure to represent the delay in.
     * @return The current start delay in the selected time units.
     */
    public double getDelay(
        Units unit) {
        try {
            return getDelayAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the delay before oscilloscope recording starts.
     * @return The current start delay in the selected time units.
     */
    public double getDelay() {
        return getDelay(Units.NATIVE);
    }

    /**
     * Set the sampling start delay.
     * @param interval Delay time between triggering a recording and the first data point being recorded.
     * @param unit Unit of measure the delay is represented in.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDelayAsync(
        double interval,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.delay");
        request.setValue(interval);
        request.setUnit(unit);
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Set the sampling start delay.
     * @param interval Delay time between triggering a recording and the first data point being recorded.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setDelayAsync(
        double interval) {
        return setDelayAsync(interval, Units.NATIVE);
    }

    /**
     * Set the sampling start delay.
     * @param interval Delay time between triggering a recording and the first data point being recorded.
     * @param unit Unit of measure the delay is represented in.
     */
    public void setDelay(
        double interval,
        Units unit) {
        try {
            setDelayAsync(interval, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Set the sampling start delay.
     * @param interval Delay time between triggering a recording and the first data point being recorded.
     */
    public void setDelay(
        double interval) {
        setDelay(interval, Units.NATIVE);
    }

    /**
     * Get the maximum number of channels that can be recorded.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum number of channels that can be added to an Oscilloscope recording.
     */
    public CompletableFuture<Integer> getMaxChannelsAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.numchannels");
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "oscilloscope/get_setting",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the maximum number of channels that can be recorded.
     * @return The maximum number of channels that can be added to an Oscilloscope recording.
     */
    public int getMaxChannels() {
        try {
            return getMaxChannelsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the maximum number of samples that can be recorded per Oscilloscope channel.
     * @return A CompletableFuture that can be completed to get the result:
     * The maximum number of samples that can be recorded per Oscilloscope channel.
     */
    public CompletableFuture<Integer> getMaxBufferSizeAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.channel.size.max");
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "oscilloscope/get_setting",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the maximum number of samples that can be recorded per Oscilloscope channel.
     * @return The maximum number of samples that can be recorded per Oscilloscope channel.
     */
    public int getMaxBufferSize() {
        try {
            return getMaxBufferSizeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Get the number of samples that can be recorded per channel given the current number of channels added.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of samples that will be recorded per channel with the current channels. Zero if none have been added.
     */
    public CompletableFuture<Integer> getBufferSizeAsync() {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setSetting("scope.channel.size");
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "oscilloscope/get_setting",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Get the number of samples that can be recorded per channel given the current number of channels added.
     * @return Number of samples that will be recorded per channel with the current channels. Zero if none have been added.
     */
    public int getBufferSize() {
        try {
            return getBufferSizeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Trigger data recording.
     * @param captureLength Optional number of samples to record per channel.
     * If left empty, the device records samples until the buffer fills.
     * Requires at least Firmware 7.29.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> startAsync(
        int captureLength) {
        zaber.motion.requests.OscilloscopeStartRequest request =
            new zaber.motion.requests.OscilloscopeStartRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setCaptureLength(captureLength);
        return Call.callAsync("oscilloscope/start", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Trigger data recording.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> startAsync() {
        return startAsync(0);
    }

    /**
     * Trigger data recording.
     * @param captureLength Optional number of samples to record per channel.
     * If left empty, the device records samples until the buffer fills.
     * Requires at least Firmware 7.29.
     */
    public void start(
        int captureLength) {
        try {
            startAsync(captureLength).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Trigger data recording.
     */
    public void start() {
        start(0);
    }

    /**
     * End data recording if currently in progress.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopAsync() {
        zaber.motion.requests.OscilloscopeRequest request =
            new zaber.motion.requests.OscilloscopeRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        return Call.callAsync("oscilloscope/stop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * End data recording if currently in progress.
     */
    public void stop() {
        try {
            stopAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of recorded channel data arrays, in the order added.
     */
    public CompletableFuture<OscilloscopeData[]> readAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        CompletableFuture<zaber.motion.requests.OscilloscopeReadResponse> response = Call.callAsync(
            "oscilloscope/read",
            request,
            zaber.motion.requests.OscilloscopeReadResponse.parser());
        return response
            .thenApply(r -> ArrayUtility.arrayFromInt(OscilloscopeData[]::new, r.getDataIds(), id -> new OscilloscopeData(id)));
    }

    /**
     * Reads the last-recorded data from the oscilloscope. Will block until any in-progress recording completes.
     * @return Array of recorded channel data arrays, in the order added.
     */
    public OscilloscopeData[] read() {
        try {
            return readAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

}
