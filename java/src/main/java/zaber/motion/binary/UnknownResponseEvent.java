/* This file is generated. Do not modify by hand. */

package zaber.motion.binary;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Reply that could not be matched to a request.
 */
public final class UnknownResponseEvent implements zaber.motion.dto.Message {

    private int deviceAddress;

    /**
     * Number of the device that sent or should receive the message.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public void setDeviceAddress(int deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * Number of the device that sent or should receive the message.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceAddress")
    public int getDeviceAddress() {
        return this.deviceAddress;
    }

    /**
     * Number of the device that sent or should receive the message.
     */
    public UnknownResponseEvent withDeviceAddress(int aDeviceAddress) {
        this.setDeviceAddress(aDeviceAddress);
        return this;
    }

    private int command;

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public void setCommand(int command) {
        this.command = command;
    }

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("command")
    public int getCommand() {
        return this.command;
    }

    /**
     * The warning flag contains the highest priority warning currently active for the device or axis.
     */
    public UnknownResponseEvent withCommand(int aCommand) {
        this.setCommand(aCommand);
        return this;
    }

    private int data;

    /**
     * Data payload of the message, if applicable, or zero otherwise.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public void setData(int data) {
        this.data = data;
    }

    /**
     * Data payload of the message, if applicable, or zero otherwise.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("data")
    public int getData() {
        return this.data;
    }

    /**
     * Data payload of the message, if applicable, or zero otherwise.
     */
    public UnknownResponseEvent withData(int aData) {
        this.setData(aData);
        return this;
    }

    /**
     * Empty constructor.
     */
    public UnknownResponseEvent() {
    }

    /**
     * Constructor with all properties.
     */
    public UnknownResponseEvent(
        int deviceAddress,
        int command,
        int data
    ) {
        this.deviceAddress = deviceAddress;
        this.command = command;
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        UnknownResponseEvent other = (UnknownResponseEvent) obj;

        return (
            EqualityUtility.equals(deviceAddress, other.deviceAddress)
            && EqualityUtility.equals(command, other.command)
            && EqualityUtility.equals(data, other.data)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceAddress),
            EqualityUtility.generateHashCode(command),
            EqualityUtility.generateHashCode(data)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnknownResponseEvent { ");
        sb.append("deviceAddress: ");
        sb.append(this.deviceAddress);
        sb.append(", ");
        sb.append("command: ");
        sb.append(this.command);
        sb.append(", ");
        sb.append("data: ");
        sb.append(this.data);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static UnknownResponseEvent fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, UnknownResponseEvent.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<UnknownResponseEvent> PARSER =
        new zaber.motion.dto.Parser<UnknownResponseEvent>() {
            @Override
            public UnknownResponseEvent fromByteArray(byte[] data) {
                return UnknownResponseEvent.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<UnknownResponseEvent> parser() {
        return PARSER;
    }

}
