// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.binary;

import zaber.motion.ArrayUtility;
import zaber.motion.gateway.Call;
import zaber.motion.gateway.Events;
import zaber.motion.exceptions.ExceptionConverter;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.UnknownBinaryResponseEventWrapper;
import zaber.motion.requests.BinaryReplyOnlyEventWrapper;
import zaber.motion.requests.DisconnectedEvent;
import zaber.motion.requests.InterfaceType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * Class representing access to particular connection (serial port, TCP connection) using the legacy Binary protocol.
 */
public class Connection implements AutoCloseable {

    private Observable<UnknownResponseEvent> unknownResponse;

    /**
     * Event invoked when a response from a device cannot be matched to any known request.
     */
    public Observable<UnknownResponseEvent> getUnknownResponse() {
        return this.unknownResponse;
    }

    private Observable<ReplyOnlyEvent> replyOnly;

    /**
     * Event invoked when a reply-only command such as a move tracking message is received from a device.
     */
    public Observable<ReplyOnlyEvent> getReplyOnly() {
        return this.replyOnly;
    }

    private Subject<MotionLibException> disconnected = ReplaySubject.create();

    /**
     * Event invoked when connection is interrupted or closed.
     */
    public Observable<MotionLibException> getDisconnected() {
        return this.disconnected;
    }

    /**
     * Default baud rate for serial connections.
     */
    public static final int DEFAULT_BAUD_RATE = 9600;


    private int interfaceId;

    /**
     * @return The interface ID identifies thisConnection instance with the underlying library.
     */
    public int getInterfaceId() {
        return this.interfaceId;
    }

    public Connection(
        int interfaceId) {
        this.interfaceId = interfaceId;
        this.subscribe();
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 9600).
     * @param useMessageIds Enable use of message IDs (defaults to disabled).
     * All your devices must be pre-configured to match.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName,
        int baudRate,
        boolean useMessageIds) {
        zaber.motion.requests.OpenBinaryInterfaceRequest request =
            new zaber.motion.requests.OpenBinaryInterfaceRequest();
        request.setInterfaceType(InterfaceType.SERIAL_PORT);
        request.setPortName(portName);
        request.setBaudRate(baudRate);
        request.setUseMessageIds(useMessageIds);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "binary/interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 9600).
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName,
        int baudRate) {
        return openSerialPortAsync(portName, baudRate, false);
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the port.
     */
    public static CompletableFuture<Connection> openSerialPortAsync(
        String portName) {
        return openSerialPortAsync(portName, DEFAULT_BAUD_RATE, false);
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 9600).
     * @param useMessageIds Enable use of message IDs (defaults to disabled).
     * All your devices must be pre-configured to match.
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName,
        int baudRate,
        boolean useMessageIds) {
        try {
            return openSerialPortAsync(portName, baudRate, useMessageIds).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @param baudRate Optional baud rate (defaults to 9600).
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName,
        int baudRate) {
        return openSerialPort(portName, baudRate, false);
    }

    /**
     * Opens a serial port.
     * @param portName Name of the port to open.
     * @return An object representing the port.
     */
    public static Connection openSerialPort(
        String portName) {
        return openSerialPort(portName, DEFAULT_BAUD_RATE, false);
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @param useMessageIds Enable use of message IDs (defaults to disabled).
     * All your devices must be pre-configured to match.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openTcpAsync(
        String hostName,
        int port,
        boolean useMessageIds) {
        zaber.motion.requests.OpenBinaryInterfaceRequest request =
            new zaber.motion.requests.OpenBinaryInterfaceRequest();
        request.setInterfaceType(InterfaceType.TCP);
        request.setHostName(hostName);
        request.setPort(port);
        request.setUseMessageIds(useMessageIds);
        CompletableFuture<zaber.motion.requests.OpenInterfaceResponse> response = Call.callAsync(
            "binary/interface/open",
            request,
            zaber.motion.requests.OpenInterfaceResponse.parser());
        return response
            .thenApply(r -> new Connection(r.getInterfaceId()));
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the connection.
     */
    public static CompletableFuture<Connection> openTcpAsync(
        String hostName,
        int port) {
        return openTcpAsync(hostName, port, false);
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @param useMessageIds Enable use of message IDs (defaults to disabled).
     * All your devices must be pre-configured to match.
     * @return An object representing the connection.
     */
    public static Connection openTcp(
        String hostName,
        int port,
        boolean useMessageIds) {
        try {
            return openTcpAsync(hostName, port, useMessageIds).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a TCP connection.
     * @param hostName Hostname or IP address.
     * @param port Port number.
     * @return An object representing the connection.
     */
    public static Connection openTcp(
        String hostName,
        int port) {
        return openTcp(hostName, port, false);
    }

    /**
     * Close the connection.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> closeAsync() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        return Call.callAsync("interface/close", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Close the connection.
     */
    public void close() {
        try {
            closeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        int device,
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        zaber.motion.requests.GenericBinaryRequest request =
            new zaber.motion.requests.GenericBinaryRequest();
        request.setInterfaceId(getInterfaceId());
        request.setDevice(device);
        request.setCommand(command);
        request.setData(data);
        request.setTimeout(timeout);
        request.setCheckErrors(checkErrors);
        CompletableFuture<Message> response = Call.callAsync(
            "binary/interface/generic_command",
            request,
            Message.parser());
        return response;
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        int device,
        CommandCode command,
        int data,
        double timeout) {
        return genericCommandAsync(device, command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        int device,
        CommandCode command,
        int data) {
        return genericCommandAsync(device, command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        int device,
        CommandCode command) {
        return genericCommandAsync(device, command, 0, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Message genericCommand(
        int device,
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        try {
            return genericCommandAsync(device, command, data, timeout, checkErrors).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return A response to the command.
     */
    public Message genericCommand(
        int device,
        CommandCode command,
        int data,
        double timeout) {
        return genericCommand(device, command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A response to the command.
     */
    public Message genericCommand(
        int device,
        CommandCode command,
        int data) {
        return genericCommand(device, command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @return A response to the command.
     */
    public Message genericCommand(
        int device,
        CommandCode command) {
        return genericCommand(device, command, 0, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        int device,
        CommandCode command,
        int data) {
        zaber.motion.requests.GenericBinaryRequest request =
            new zaber.motion.requests.GenericBinaryRequest();
        request.setInterfaceId(getInterfaceId());
        request.setDevice(device);
        request.setCommand(command);
        request.setData(data);
        return Call.callAsync("binary/interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic Binary command to this connection without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        int device,
        CommandCode command) {
        return genericCommandNoResponseAsync(device, command, 0);
    }

    /**
     * Sends a generic Binary command to this connection without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     */
    public void genericCommandNoResponse(
        int device,
        CommandCode command,
        int data) {
        try {
            genericCommandNoResponseAsync(device, command, data).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this connection without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param device Device address to send the command to. Use zero for broadcast.
     * @param command Command to send.
     */
    public void genericCommandNoResponse(
        int device,
        CommandCode command) {
        genericCommandNoResponse(device, command, 0);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when any device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Message[]> genericCommandMultiResponseAsync(
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        zaber.motion.requests.GenericBinaryRequest request =
            new zaber.motion.requests.GenericBinaryRequest();
        request.setInterfaceId(getInterfaceId());
        request.setCommand(command);
        request.setData(data);
        request.setTimeout(timeout);
        request.setCheckErrors(checkErrors);
        CompletableFuture<zaber.motion.requests.BinaryMessageCollection> response = Call.callAsync(
            "binary/interface/generic_command_multi_response",
            request,
            zaber.motion.requests.BinaryMessageCollection.parser());
        return response
            .thenApply(r -> r.getMessages());
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Message[]> genericCommandMultiResponseAsync(
        CommandCode command,
        int data,
        double timeout) {
        return genericCommandMultiResponseAsync(command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Message[]> genericCommandMultiResponseAsync(
        CommandCode command,
        int data) {
        return genericCommandMultiResponseAsync(command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Message[]> genericCommandMultiResponseAsync(
        CommandCode command) {
        return genericCommandMultiResponseAsync(command, 0, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when any device rejects the command.
     * @return All responses to the command.
     */
    public Message[] genericCommandMultiResponse(
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        try {
            return genericCommandMultiResponseAsync(command, data, timeout, checkErrors).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for all responses from the device chain. 0 or negative defaults to 0.5s.
     * @return All responses to the command.
     */
    public Message[] genericCommandMultiResponse(
        CommandCode command,
        int data,
        double timeout) {
        return genericCommandMultiResponse(command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return All responses to the command.
     */
    public Message[] genericCommandMultiResponse(
        CommandCode command,
        int data) {
        return genericCommandMultiResponse(command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this connection and expects responses from one or more devices.
     * Responses are returned in order of arrival.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @return All responses to the command.
     */
    public Message[] genericCommandMultiResponse(
        CommandCode command) {
        return genericCommandMultiResponse(command, 0, 0.0, true);
    }

    /**
     * Renumbers devices present on this connection. After renumbering, you must identify devices again.
     * @return A CompletableFuture that can be completed to get the result:
     * Total number of devices that responded to the renumber.
     */
    public CompletableFuture<Integer> renumberDevicesAsync() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "binary/device/renumber",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Renumbers devices present on this connection. After renumbering, you must identify devices again.
     * @return Total number of devices that responded to the renumber.
     */
    public int renumberDevices() {
        try {
            return renumberDevicesAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @param identifyDevices Determines whether device identification should be performed as well.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of detected devices.
     */
    public CompletableFuture<Device[]> detectDevicesAsync(
        boolean identifyDevices) {
        zaber.motion.requests.BinaryDeviceDetectRequest request =
            new zaber.motion.requests.BinaryDeviceDetectRequest();
        request.setInterfaceId(getInterfaceId());
        request.setIdentifyDevices(identifyDevices);
        CompletableFuture<zaber.motion.requests.BinaryDeviceDetectResponse> response = Call.callAsync(
            "binary/device/detect",
            request,
            zaber.motion.requests.BinaryDeviceDetectResponse.parser());
        return response
            .thenApply(r -> ArrayUtility.arrayFromInt(Device[]::new, r.getDevices(), address -> this.getDevice(address)));
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of detected devices.
     */
    public CompletableFuture<Device[]> detectDevicesAsync() {
        return detectDevicesAsync(true);
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @param identifyDevices Determines whether device identification should be performed as well.
     * @return Array of detected devices.
     */
    public Device[] detectDevices(
        boolean identifyDevices) {
        try {
            return detectDevicesAsync(identifyDevices).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Attempts to detect any devices present on this connection.
     * @return Array of detected devices.
     */
    public Device[] detectDevices() {
        return detectDevices(true);
    }

    /**
     * Gets a Device class instance which allows you to control a particular device on this connection.
     * Devices are numbered from 1.
     * @param deviceAddress Address of device intended to control. Address is configured for each device.
     * @return Device instance.
     */
    public Device getDevice(
        int deviceAddress) {
        if (deviceAddress <= 0) {
            throw new IllegalArgumentException("Invalid value; physical devices are numbered from 1.");
        }
        return new Device(this, deviceAddress);
    }


    /**
     * Returns a string that represents the connection.
     * @return A string that represents the connection.
     */
    public String toString() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getInterfaceId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "interface/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    private void subscribe() {
        this.unknownResponse = Events.getEventObservable()
            .takeUntil(this.disconnected)
            .filter(event -> event.getEventName().equals("binary/interface/unknown_response"))
            .map(event -> (UnknownBinaryResponseEventWrapper) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .map(event -> event.getUnknownResponse());

        this.replyOnly = Events.getEventObservable()
            .takeUntil(this.disconnected)
            .filter(event -> event.getEventName().equals("binary/interface/reply_only"))
            .map(event -> (BinaryReplyOnlyEventWrapper) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .map(event -> event.getReply());

        Events.getEventObservable()
            .filter(event -> event.getEventName().equals("interface/disconnected"))
            .map(event -> (DisconnectedEvent) event.getEventData())
            .filter(event -> (event.getInterfaceId() == this.interfaceId))
            .take(1)
            .map(event -> ExceptionConverter.convert(event.getErrorType(), event.getErrorMessage()))
            .subscribe(this.disconnected);
    }
}
