/* This file is generated. Do not modify by hand. */

package zaber.motion.binary;

/**
 * Named constants for all Zaber Binary protocol reply-only command codes.
 */
public enum ReplyCode {

    MOVE_TRACKING(8),

    LIMIT_ACTIVE(9),

    MANUAL_MOVE_TRACKING(10),

    MANUAL_MOVE(11),

    SLIP_TRACKING(12),

    UNEXPECTED_POSITION(13),

    ERROR(255);

    private int value;

    ReplyCode(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static ReplyCode valueOf(int argValue) {
        for (ReplyCode value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
