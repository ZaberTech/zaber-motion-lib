/* This file is generated. Do not modify by hand. */

package zaber.motion.binary;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.FirmwareVersion;
import zaber.motion.EqualityUtility;


/**
 * Representation of data gathered during device identification.
 */
public final class DeviceIdentity implements zaber.motion.dto.Message {

    private int deviceId;

    /**
     * Unique ID of the device hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Unique ID of the device hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceId")
    public int getDeviceId() {
        return this.deviceId;
    }

    /**
     * Unique ID of the device hardware.
     */
    public DeviceIdentity withDeviceId(int aDeviceId) {
        this.setDeviceId(aDeviceId);
        return this;
    }

    private long serialNumber;

    /**
     * Serial number of the device.
     * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("serialNumber")
    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Serial number of the device.
     * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("serialNumber")
    public long getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * Serial number of the device.
     * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
     */
    public DeviceIdentity withSerialNumber(long aSerialNumber) {
        this.setSerialNumber(aSerialNumber);
        return this;
    }

    private String name;

    /**
     * Name of the product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name of the product.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public String getName() {
        return this.name;
    }

    /**
     * Name of the product.
     */
    public DeviceIdentity withName(String aName) {
        this.setName(aName);
        return this;
    }

    private FirmwareVersion firmwareVersion;

    /**
     * Version of the firmware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public void setFirmwareVersion(FirmwareVersion firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    /**
     * Version of the firmware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("firmwareVersion")
    public FirmwareVersion getFirmwareVersion() {
        return this.firmwareVersion;
    }

    /**
     * Version of the firmware.
     */
    public DeviceIdentity withFirmwareVersion(FirmwareVersion aFirmwareVersion) {
        this.setFirmwareVersion(aFirmwareVersion);
        return this;
    }

    private boolean isPeripheral;

    /**
     * Indicates whether the device is a peripheral or part of an integrated device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isPeripheral")
    public void setIsPeripheral(boolean isPeripheral) {
        this.isPeripheral = isPeripheral;
    }

    /**
     * Indicates whether the device is a peripheral or part of an integrated device.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("isPeripheral")
    public boolean getIsPeripheral() {
        return this.isPeripheral;
    }

    /**
     * Indicates whether the device is a peripheral or part of an integrated device.
     */
    public DeviceIdentity withIsPeripheral(boolean aIsPeripheral) {
        this.setIsPeripheral(aIsPeripheral);
        return this;
    }

    private int peripheralId;

    /**
     * Unique ID of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public void setPeripheralId(int peripheralId) {
        this.peripheralId = peripheralId;
    }

    /**
     * Unique ID of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralId")
    public int getPeripheralId() {
        return this.peripheralId;
    }

    /**
     * Unique ID of the peripheral hardware.
     */
    public DeviceIdentity withPeripheralId(int aPeripheralId) {
        this.setPeripheralId(aPeripheralId);
        return this;
    }

    private String peripheralName;

    /**
     * Name of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralName")
    public void setPeripheralName(String peripheralName) {
        this.peripheralName = peripheralName;
    }

    /**
     * Name of the peripheral hardware.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("peripheralName")
    public String getPeripheralName() {
        return this.peripheralName;
    }

    /**
     * Name of the peripheral hardware.
     */
    public DeviceIdentity withPeripheralName(String aPeripheralName) {
        this.setPeripheralName(aPeripheralName);
        return this;
    }

    private DeviceType deviceType;

    /**
     * Determines the type of an device and units it accepts.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceType")
    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * Determines the type of an device and units it accepts.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("deviceType")
    public DeviceType getDeviceType() {
        return this.deviceType;
    }

    /**
     * Determines the type of an device and units it accepts.
     */
    public DeviceIdentity withDeviceType(DeviceType aDeviceType) {
        this.setDeviceType(aDeviceType);
        return this;
    }

    /**
     * Empty constructor.
     */
    public DeviceIdentity() {
    }

    /**
     * Constructor with all properties.
     */
    public DeviceIdentity(
        int deviceId,
        long serialNumber,
        String name,
        FirmwareVersion firmwareVersion,
        boolean isPeripheral,
        int peripheralId,
        String peripheralName,
        DeviceType deviceType
    ) {
        this.deviceId = deviceId;
        this.serialNumber = serialNumber;
        this.name = name;
        this.firmwareVersion = firmwareVersion;
        this.isPeripheral = isPeripheral;
        this.peripheralId = peripheralId;
        this.peripheralName = peripheralName;
        this.deviceType = deviceType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DeviceIdentity other = (DeviceIdentity) obj;

        return (
            EqualityUtility.equals(deviceId, other.deviceId)
            && EqualityUtility.equals(serialNumber, other.serialNumber)
            && EqualityUtility.equals(name, other.name)
            && EqualityUtility.equals(firmwareVersion, other.firmwareVersion)
            && EqualityUtility.equals(isPeripheral, other.isPeripheral)
            && EqualityUtility.equals(peripheralId, other.peripheralId)
            && EqualityUtility.equals(peripheralName, other.peripheralName)
            && EqualityUtility.equals(deviceType, other.deviceType)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(deviceId),
            EqualityUtility.generateHashCode(serialNumber),
            EqualityUtility.generateHashCode(name),
            EqualityUtility.generateHashCode(firmwareVersion),
            EqualityUtility.generateHashCode(isPeripheral),
            EqualityUtility.generateHashCode(peripheralId),
            EqualityUtility.generateHashCode(peripheralName),
            EqualityUtility.generateHashCode(deviceType)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceIdentity { ");
        sb.append("deviceId: ");
        sb.append(this.deviceId);
        sb.append(", ");
        sb.append("serialNumber: ");
        sb.append(this.serialNumber);
        sb.append(", ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("firmwareVersion: ");
        sb.append(this.firmwareVersion);
        sb.append(", ");
        sb.append("isPeripheral: ");
        sb.append(this.isPeripheral);
        sb.append(", ");
        sb.append("peripheralId: ");
        sb.append(this.peripheralId);
        sb.append(", ");
        sb.append("peripheralName: ");
        sb.append(this.peripheralName);
        sb.append(", ");
        sb.append("deviceType: ");
        sb.append(this.deviceType);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static DeviceIdentity fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, DeviceIdentity.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<DeviceIdentity> PARSER =
        new zaber.motion.dto.Parser<DeviceIdentity>() {
            @Override
            public DeviceIdentity fromByteArray(byte[] data) {
                return DeviceIdentity.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<DeviceIdentity> parser() {
        return PARSER;
    }

}
