// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.binary;

import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing access to various device settings and properties.
 */
public class DeviceSettings {
    private Device device;

    public DeviceSettings(
        Device device) {
        this.device = device;
    }

    /**
     * Returns any device setting or property.
     * @param setting Setting to get.
     * @param unit Units of setting.
     * @return A CompletableFuture that can be completed to get the result:
     * Setting value.
     */
    public CompletableFuture<Double> getAsync(
        BinarySettings setting,
        Units unit) {
        zaber.motion.requests.BinaryDeviceGetSettingRequest request =
            new zaber.motion.requests.BinaryDeviceGetSettingRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setSetting(setting);
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns any device setting or property.
     * @param setting Setting to get.
     * @return A CompletableFuture that can be completed to get the result:
     * Setting value.
     */
    public CompletableFuture<Double> getAsync(
        BinarySettings setting) {
        return getAsync(setting, Units.NATIVE);
    }

    /**
     * Returns any device setting or property.
     * @param setting Setting to get.
     * @param unit Units of setting.
     * @return Setting value.
     */
    public double get(
        BinarySettings setting,
        Units unit) {
        try {
            return getAsync(setting, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns any device setting or property.
     * @param setting Setting to get.
     * @return Setting value.
     */
    public double get(
        BinarySettings setting) {
        return get(setting, Units.NATIVE);
    }

    /**
     * Sets any device setting.
     * @param setting Setting to set.
     * @param value Value of the setting.
     * @param unit Units of setting.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAsync(
        BinarySettings setting,
        double value,
        Units unit) {
        zaber.motion.requests.BinaryDeviceSetSettingRequest request =
            new zaber.motion.requests.BinaryDeviceSetSettingRequest();
        request.setInterfaceId(this.device.getConnection().getInterfaceId());
        request.setDevice(this.device.getDeviceAddress());
        request.setSetting(setting);
        request.setValue(value);
        request.setUnit(unit);
        return Call.callAsync("binary/device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets any device setting.
     * @param setting Setting to set.
     * @param value Value of the setting.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setAsync(
        BinarySettings setting,
        double value) {
        return setAsync(setting, value, Units.NATIVE);
    }

    /**
     * Sets any device setting.
     * @param setting Setting to set.
     * @param value Value of the setting.
     * @param unit Units of setting.
     */
    public void set(
        BinarySettings setting,
        double value,
        Units unit) {
        try {
            setAsync(setting, value, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets any device setting.
     * @param setting Setting to set.
     * @param value Value of the setting.
     */
    public void set(
        BinarySettings setting,
        double value) {
        set(setting, value, Units.NATIVE);
    }

}
