// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.binary;

import zaber.motion.Units;
import zaber.motion.FirmwareVersion;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.AxisMoveType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents a device using the binary protocol.
 */
public class Device {
    /**
     * Default timeout for move commands in seconds.
     */
    public static final double DEFAULT_MOVEMENT_TIMEOUT = 60;


    private Connection connection;

    /**
     * @return Connection of this device.
     */
    public Connection getConnection() {
        return this.connection;
    }

    private DeviceSettings settings;

    /**
     * @return Settings and properties of this axis.
     */
    public DeviceSettings getSettings() {
        return this.settings;
    }

    private int deviceAddress;

    /**
     * @return The device address uniquely identifies the device on the connection.
     * It can be configured or automatically assigned by the renumber command.
     */
    public int getDeviceAddress() {
        return this.deviceAddress;
    }


    /**
     * @return Identity of the device.
     */
    public DeviceIdentity getIdentity() {
        return this.retrieveIdentity();
    }


    /**
     * @return Indicates whether or not the device has been identified.
     */
    public boolean getIsIdentified() {
        return this.retrieveIsIdentified();
    }


    /**
     * @return Unique ID of the device hardware.
     */
    public int getDeviceId() {
        return this.getIdentity().getDeviceId();
    }


    /**
     * @return Serial number of the device.
     * Requires at least Firmware 6.15 for devices or 6.24 for peripherals.
     */
    public long getSerialNumber() {
        return this.getIdentity().getSerialNumber();
    }


    /**
     * @return Name of the product.
     */
    public String getName() {
        return this.getIdentity().getName();
    }


    /**
     * @return Version of the firmware.
     */
    public FirmwareVersion getFirmwareVersion() {
        return this.getIdentity().getFirmwareVersion();
    }


    /**
     * @return Indicates whether the device is a peripheral or part of an integrated device.
     */
    public boolean getIsPeripheral() {
        return this.getIdentity().getIsPeripheral();
    }


    /**
     * @return Unique ID of the peripheral hardware.
     */
    public int getPeripheralId() {
        return this.getIdentity().getPeripheralId();
    }


    /**
     * @return Name of the peripheral hardware.
     */
    public String getPeripheralName() {
        return this.getIdentity().getPeripheralName();
    }


    /**
     * @return Determines the type of an device and units it accepts.
     */
    public DeviceType getDeviceType() {
        return this.getIdentity().getDeviceType();
    }

    public Device(
        Connection connection, int deviceAddress) {
        this.connection = connection;
        this.settings = new DeviceSettings(this);
        this.deviceAddress = deviceAddress;
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        zaber.motion.requests.GenericBinaryRequest request =
            new zaber.motion.requests.GenericBinaryRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setData(data);
        request.setTimeout(timeout);
        request.setCheckErrors(checkErrors);
        CompletableFuture<Message> response = Call.callAsync(
            "binary/interface/generic_command",
            request,
            Message.parser());
        return response;
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        CommandCode command,
        int data,
        double timeout) {
        return genericCommandAsync(command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        CommandCode command,
        int data) {
        return genericCommandAsync(command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Message> genericCommandAsync(
        CommandCode command) {
        return genericCommandAsync(command, 0, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Message genericCommand(
        CommandCode command,
        int data,
        double timeout,
        boolean checkErrors) {
        try {
            return genericCommandAsync(command, data, timeout, checkErrors).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return A response to the command.
     */
    public Message genericCommand(
        CommandCode command,
        int data,
        double timeout) {
        return genericCommand(command, data, timeout, true);
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A response to the command.
     */
    public Message genericCommand(
        CommandCode command,
        int data) {
        return genericCommand(command, data, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this device.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @return A response to the command.
     */
    public Message genericCommand(
        CommandCode command) {
        return genericCommand(command, 0, 0.0, true);
    }

    /**
     * Sends a generic Binary command to this device without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        CommandCode command,
        int data) {
        zaber.motion.requests.GenericBinaryRequest request =
            new zaber.motion.requests.GenericBinaryRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setData(data);
        return Call.callAsync("binary/interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic Binary command to this device without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        CommandCode command) {
        return genericCommandNoResponseAsync(command, 0);
    }

    /**
     * Sends a generic Binary command to this device without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     * @param data Optional data argument to the command. Defaults to zero.
     */
    public void genericCommandNoResponse(
        CommandCode command,
        int data) {
        try {
            genericCommandNoResponseAsync(command, data).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this device without expecting a response.
     * For more information please refer to the
     * [Binary Protocol Manual](https://www.zaber.com/protocol-manual?protocol=Binary#topic_quick_command_reference).
     * @param command Command to send.
     */
    public void genericCommandNoResponse(
        CommandCode command) {
        genericCommandNoResponse(command, 0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @param toUnit Unit to convert retrieved data to.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return A CompletableFuture that can be completed to get the result:
     * Data that has been converted to the provided unit.
     */
    public CompletableFuture<Double> genericCommandWithUnitsAsync(
        CommandCode command,
        double data,
        Units fromUnit,
        Units toUnit,
        double timeout) {
        zaber.motion.requests.BinaryGenericWithUnitsRequest request =
            new zaber.motion.requests.BinaryGenericWithUnitsRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setCommand(command);
        request.setData(data);
        request.setFromUnit(fromUnit);
        request.setToUnit(toUnit);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/generic_command_with_units",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @param toUnit Unit to convert retrieved data to.
     * @return A CompletableFuture that can be completed to get the result:
     * Data that has been converted to the provided unit.
     */
    public CompletableFuture<Double> genericCommandWithUnitsAsync(
        CommandCode command,
        double data,
        Units fromUnit,
        Units toUnit) {
        return genericCommandWithUnitsAsync(command, data, fromUnit, toUnit, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @return A CompletableFuture that can be completed to get the result:
     * Data that has been converted to the provided unit.
     */
    public CompletableFuture<Double> genericCommandWithUnitsAsync(
        CommandCode command,
        double data,
        Units fromUnit) {
        return genericCommandWithUnitsAsync(command, data, fromUnit, Units.NATIVE, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @return A CompletableFuture that can be completed to get the result:
     * Data that has been converted to the provided unit.
     */
    public CompletableFuture<Double> genericCommandWithUnitsAsync(
        CommandCode command,
        double data) {
        return genericCommandWithUnitsAsync(command, data, Units.NATIVE, Units.NATIVE, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @return A CompletableFuture that can be completed to get the result:
     * Data that has been converted to the provided unit.
     */
    public CompletableFuture<Double> genericCommandWithUnitsAsync(
        CommandCode command) {
        return genericCommandWithUnitsAsync(command, 0, Units.NATIVE, Units.NATIVE, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @param toUnit Unit to convert retrieved data to.
     * @param timeout Number of seconds to wait for a response from the device. 0 or negative defaults to 0.5s.
     * @return Data that has been converted to the provided unit.
     */
    public double genericCommandWithUnits(
        CommandCode command,
        double data,
        Units fromUnit,
        Units toUnit,
        double timeout) {
        try {
            return genericCommandWithUnitsAsync(command, data, fromUnit, toUnit, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @param toUnit Unit to convert retrieved data to.
     * @return Data that has been converted to the provided unit.
     */
    public double genericCommandWithUnits(
        CommandCode command,
        double data,
        Units fromUnit,
        Units toUnit) {
        return genericCommandWithUnits(command, data, fromUnit, toUnit, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @param fromUnit Unit to convert sent data from.
     * @return Data that has been converted to the provided unit.
     */
    public double genericCommandWithUnits(
        CommandCode command,
        double data,
        Units fromUnit) {
        return genericCommandWithUnits(command, data, fromUnit, Units.NATIVE, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @param data Data argument to the command. Defaults to zero.
     * @return Data that has been converted to the provided unit.
     */
    public double genericCommandWithUnits(
        CommandCode command,
        double data) {
        return genericCommandWithUnits(command, data, Units.NATIVE, Units.NATIVE, 0.0);
    }

    /**
     * Sends a generic Binary command to this device with unit conversions for both sent data and retrieved data.
     * @param command Command to send.
     * @return Data that has been converted to the provided unit.
     */
    public double genericCommandWithUnits(
        CommandCode command) {
        return genericCommandWithUnits(command, 0, Units.NATIVE, Units.NATIVE, 0.0);
    }

    /**
     * Homes device. Device returns to its homing position.
     * @param unit Unit to convert returned position to.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> homeAsync(
        Units unit,
        double timeout) {
        zaber.motion.requests.BinaryDeviceHomeRequest request =
            new zaber.motion.requests.BinaryDeviceHomeRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setUnit(unit);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/home",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Homes device. Device returns to its homing position.
     * @param unit Unit to convert returned position to.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> homeAsync(
        Units unit) {
        return homeAsync(unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Homes device. Device returns to its homing position.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> homeAsync() {
        return homeAsync(Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Homes device. Device returns to its homing position.
     * @param unit Unit to convert returned position to.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return Current position that has been converted to the provided unit.
     */
    public double home(
        Units unit,
        double timeout) {
        try {
            return homeAsync(unit, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Homes device. Device returns to its homing position.
     * @param unit Unit to convert returned position to.
     * @return Current position that has been converted to the provided unit.
     */
    public double home(
        Units unit) {
        return home(unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Homes device. Device returns to its homing position.
     * @return Current position that has been converted to the provided unit.
     */
    public double home() {
        return home(Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @param unit Unit to convert returned position to.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> stopAsync(
        Units unit,
        double timeout) {
        zaber.motion.requests.BinaryDeviceStopRequest request =
            new zaber.motion.requests.BinaryDeviceStopRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setUnit(unit);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/stop",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @param unit Unit to convert returned position to.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> stopAsync(
        Units unit) {
        return stopAsync(unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> stopAsync() {
        return stopAsync(Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @param unit Unit to convert returned position to.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return Current position that has been converted to the provided unit.
     */
    public double stop(
        Units unit,
        double timeout) {
        try {
            return stopAsync(unit, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @param unit Unit to convert returned position to.
     * @return Current position that has been converted to the provided unit.
     */
    public double stop(
        Units unit) {
        return stop(unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Stops ongoing device movement. Decelerates until zero speed.
     * @return Current position that has been converted to the provided unit.
     */
    public double stop() {
        return stop(Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveAbsoluteAsync(
        double position,
        Units unit,
        double timeout) {
        zaber.motion.requests.BinaryDeviceMoveRequest request =
            new zaber.motion.requests.BinaryDeviceMoveRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setType(AxisMoveType.ABS);
        request.setArg(position);
        request.setUnit(unit);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/move",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveAbsoluteAsync(
        double position,
        Units unit) {
        return moveAbsoluteAsync(position, unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveAbsoluteAsync(
        double position) {
        return moveAbsoluteAsync(position, Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return Current position that has been converted to the provided unit.
     */
    public double moveAbsolute(
        double position,
        Units unit,
        double timeout) {
        try {
            return moveAbsoluteAsync(position, unit, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @return Current position that has been converted to the provided unit.
     */
    public double moveAbsolute(
        double position,
        Units unit) {
        return moveAbsolute(position, unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to absolute position.
     * @param position Absolute position.
     * @return Current position that has been converted to the provided unit.
     */
    public double moveAbsolute(
        double position) {
        return moveAbsolute(position, Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveRelativeAsync(
        double position,
        Units unit,
        double timeout) {
        zaber.motion.requests.BinaryDeviceMoveRequest request =
            new zaber.motion.requests.BinaryDeviceMoveRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setType(AxisMoveType.REL);
        request.setArg(position);
        request.setUnit(unit);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/move",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveRelativeAsync(
        double position,
        Units unit) {
        return moveRelativeAsync(position, unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @return A CompletableFuture that can be completed to get the result:
     * Current position that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveRelativeAsync(
        double position) {
        return moveRelativeAsync(position, Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @param timeout Number of seconds to wait for response from the device chain (defaults to 60s).
     * @return Current position that has been converted to the provided unit.
     */
    public double moveRelative(
        double position,
        Units unit,
        double timeout) {
        try {
            return moveRelativeAsync(position, unit, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @param unit Unit for the provided position as well as position returned by the device.
     * @return Current position that has been converted to the provided unit.
     */
    public double moveRelative(
        double position,
        Units unit) {
        return moveRelative(position, unit, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Move device to position relative to current position.
     * @param position Relative position.
     * @return Current position that has been converted to the provided unit.
     */
    public double moveRelative(
        double position) {
        return moveRelative(position, Units.NATIVE, DEFAULT_MOVEMENT_TIMEOUT);
    }

    /**
     * Begins to move device at specified speed.
     * @param velocity Movement velocity.
     * @param unit Unit to convert returned velocity to.
     * @return A CompletableFuture that can be completed to get the result:
     * Device velocity that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveVelocityAsync(
        double velocity,
        Units unit) {
        zaber.motion.requests.BinaryDeviceMoveRequest request =
            new zaber.motion.requests.BinaryDeviceMoveRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setType(AxisMoveType.VEL);
        request.setArg(velocity);
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/move",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Begins to move device at specified speed.
     * @param velocity Movement velocity.
     * @return A CompletableFuture that can be completed to get the result:
     * Device velocity that has been converted to the provided unit.
     */
    public CompletableFuture<Double> moveVelocityAsync(
        double velocity) {
        return moveVelocityAsync(velocity, Units.NATIVE);
    }

    /**
     * Begins to move device at specified speed.
     * @param velocity Movement velocity.
     * @param unit Unit to convert returned velocity to.
     * @return Device velocity that has been converted to the provided unit.
     */
    public double moveVelocity(
        double velocity,
        Units unit) {
        try {
            return moveVelocityAsync(velocity, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Begins to move device at specified speed.
     * @param velocity Movement velocity.
     * @return Device velocity that has been converted to the provided unit.
     */
    public double moveVelocity(
        double velocity) {
        return moveVelocity(velocity, Units.NATIVE);
    }

    /**
     * Waits until device stops moving.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> waitUntilIdleAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        return Call.callAsync("binary/device/wait_until_idle", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Waits until device stops moving.
     */
    public void waitUntilIdle() {
        try {
            waitUntilIdleAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Check whether the device is moving.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the device is currently executing a motion command.
     */
    public CompletableFuture<Boolean> isBusyAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "binary/device/is_busy",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Check whether the device is moving.
     * @return True if the device is currently executing a motion command.
     */
    public boolean isBusy() {
        try {
            return isBusyAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @param assumeVersion The identification assumes the specified firmware version
     * instead of the version queried from the device.
     * Providing this argument can lead to unexpected compatibility issues.
     * @return A CompletableFuture that can be completed to get the result:
     * Device identification data.
     */
    public CompletableFuture<DeviceIdentity> identifyAsync(
        FirmwareVersion assumeVersion) {
        zaber.motion.requests.DeviceIdentifyRequest request =
            new zaber.motion.requests.DeviceIdentifyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setAssumeVersion(assumeVersion);
        CompletableFuture<DeviceIdentity> response = Call.callAsync(
            "binary/device/identify",
            request,
            DeviceIdentity.parser());
        return response;
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @return A CompletableFuture that can be completed to get the result:
     * Device identification data.
     */
    public CompletableFuture<DeviceIdentity> identifyAsync() {
        return identifyAsync(null);
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @param assumeVersion The identification assumes the specified firmware version
     * instead of the version queried from the device.
     * Providing this argument can lead to unexpected compatibility issues.
     * @return Device identification data.
     */
    public DeviceIdentity identify(
        FirmwareVersion assumeVersion) {
        try {
            return identifyAsync(assumeVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Queries the device and the database, gathering information about the product.
     * Without this information features such as unit conversions will not work.
     * Usually, called automatically by detect devices method.
     * @return Device identification data.
     */
    public DeviceIdentity identify() {
        return identify(null);
    }

    /**
     * Parks the axis.
     * Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
     * It can later be unparked and moved without first having to home it.
     * Requires at least Firmware 6.06.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> parkAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        return Call.callAsync("binary/device/park", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Parks the axis.
     * Motor drivers remain enabled and hold current continues to be applied until the device is powered off.
     * It can later be unparked and moved without first having to home it.
     * Requires at least Firmware 6.06.
     */
    public void park() {
        try {
            parkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Unparks axis. Axis will now be able to move.
     * Requires at least Firmware 6.06.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> unparkAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        return Call.callAsync("binary/device/unpark", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Unparks axis. Axis will now be able to move.
     * Requires at least Firmware 6.06.
     */
    public void unpark() {
        try {
            unparkAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns bool indicating whether the axis is parked or not.
     * Requires at least Firmware 6.06.
     * @return A CompletableFuture that can be completed to get the result:
     * True if the axis is currently parked. False otherwise.
     */
    public CompletableFuture<Boolean> isParkedAsync() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "binary/device/is_parked",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns bool indicating whether the axis is parked or not.
     * Requires at least Firmware 6.06.
     * @return True if the axis is currently parked. False otherwise.
     */
    public boolean isParked() {
        try {
            return isParkedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current device position.
     * @param unit Units of position.
     * @return A CompletableFuture that can be completed to get the result:
     * Axis position.
     */
    public CompletableFuture<Double> getPositionAsync(
        Units unit) {
        zaber.motion.requests.BinaryDeviceGetSettingRequest request =
            new zaber.motion.requests.BinaryDeviceGetSettingRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        request.setSetting(BinarySettings.CURRENT_POSITION);
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "binary/device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns current device position.
     * @return A CompletableFuture that can be completed to get the result:
     * Axis position.
     */
    public CompletableFuture<Double> getPositionAsync() {
        return getPositionAsync(Units.NATIVE);
    }

    /**
     * Returns current device position.
     * @param unit Units of position.
     * @return Axis position.
     */
    public double getPosition(
        Units unit) {
        try {
            return getPositionAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current device position.
     * @return Axis position.
     */
    public double getPosition() {
        return getPosition(Units.NATIVE);
    }

    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "binary/device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns identity.
     * @return Device identity.
     */
    private DeviceIdentity retrieveIdentity() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        DeviceIdentity response = Call.callSync(
            "binary/device/get_identity",
            request,
            DeviceIdentity.parser());
        return response;
    }


    /**
     * Returns whether or not the device have been identified.
     * @return True if the device has already been identified. False otherwise.
     */
    private boolean retrieveIsIdentified() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setDevice(getDeviceAddress());
        zaber.motion.requests.BoolResponse response = Call.callSync(
            "binary/device/get_is_identified",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response.getValue();
    }


}
