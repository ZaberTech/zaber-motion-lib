/* This file is generated. Do not modify by hand. */

package zaber.motion.microscopy;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Status of the autofocus.
 */
public final class AutofocusStatus implements zaber.motion.dto.Message {

    private boolean inFocus;

    /**
     * Indicates whether the autofocus is in focus.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inFocus")
    public void setInFocus(boolean inFocus) {
        this.inFocus = inFocus;
    }

    /**
     * Indicates whether the autofocus is in focus.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inFocus")
    public boolean getInFocus() {
        return this.inFocus;
    }

    /**
     * Indicates whether the autofocus is in focus.
     */
    public AutofocusStatus withInFocus(boolean aInFocus) {
        this.setInFocus(aInFocus);
        return this;
    }

    private boolean inRange;

    /**
     * Indicates whether the autofocus is in range.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inRange")
    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    /**
     * Indicates whether the autofocus is in range.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inRange")
    public boolean getInRange() {
        return this.inRange;
    }

    /**
     * Indicates whether the autofocus is in range.
     */
    public AutofocusStatus withInRange(boolean aInRange) {
        this.setInRange(aInRange);
        return this;
    }

    /**
     * Empty constructor.
     */
    public AutofocusStatus() {
    }

    /**
     * Constructor with all properties.
     */
    public AutofocusStatus(
        boolean inFocus,
        boolean inRange
    ) {
        this.inFocus = inFocus;
        this.inRange = inRange;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AutofocusStatus other = (AutofocusStatus) obj;

        return (
            EqualityUtility.equals(inFocus, other.inFocus)
            && EqualityUtility.equals(inRange, other.inRange)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(inFocus),
            EqualityUtility.generateHashCode(inRange)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AutofocusStatus { ");
        sb.append("inFocus: ");
        sb.append(this.inFocus);
        sb.append(", ");
        sb.append("inRange: ");
        sb.append(this.inRange);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static AutofocusStatus fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, AutofocusStatus.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<AutofocusStatus> PARSER =
        new zaber.motion.dto.Parser<AutofocusStatus>() {
            @Override
            public AutofocusStatus fromByteArray(byte[] data) {
                return AutofocusStatus.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<AutofocusStatus> parser() {
        return PARSER;
    }

}
