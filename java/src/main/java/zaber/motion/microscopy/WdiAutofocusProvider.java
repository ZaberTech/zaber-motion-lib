// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.InterfaceType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


/**
 * Class representing access to WDI Autofocus connection.
 */
public class WdiAutofocusProvider implements AutoCloseable {
    /**
     * Default port number for TCP connections to WDI autofocus.
     */
    public static final int DEFAULT_TCP_PORT = 27;


    private int providerId;

    /**
     * @return The ID identifies the autofocus with the underlying library.
     */
    public int getProviderId() {
        return this.providerId;
    }

    public WdiAutofocusProvider(
        int providerId) {
        this.providerId = providerId;
    }

    /**
     * Opens a TCP connection to WDI autofocus.
     * @param hostName Hostname or IP address.
     * @param port Optional port number (defaults to 27).
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the autofocus connection.
     */
    public static CompletableFuture<WdiAutofocusProvider> openTcpAsync(
        String hostName,
        int port) {
        zaber.motion.requests.OpenInterfaceRequest request =
            new zaber.motion.requests.OpenInterfaceRequest();
        request.setInterfaceType(InterfaceType.TCP);
        request.setHostName(hostName);
        request.setPort(port);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "wdi/open",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> new WdiAutofocusProvider(r.getValue()));
    }

    /**
     * Opens a TCP connection to WDI autofocus.
     * @param hostName Hostname or IP address.
     * @return A CompletableFuture that can be completed to get the result:
     * An object representing the autofocus connection.
     */
    public static CompletableFuture<WdiAutofocusProvider> openTcpAsync(
        String hostName) {
        return openTcpAsync(hostName, DEFAULT_TCP_PORT);
    }

    /**
     * Opens a TCP connection to WDI autofocus.
     * @param hostName Hostname or IP address.
     * @param port Optional port number (defaults to 27).
     * @return An object representing the autofocus connection.
     */
    public static WdiAutofocusProvider openTcp(
        String hostName,
        int port) {
        try {
            return openTcpAsync(hostName, port).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Opens a TCP connection to WDI autofocus.
     * @param hostName Hostname or IP address.
     * @return An object representing the autofocus connection.
     */
    public static WdiAutofocusProvider openTcp(
        String hostName) {
        return openTcp(hostName, DEFAULT_TCP_PORT);
    }

    /**
     * Close the connection.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> closeAsync() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getProviderId());
        return Call.callAsync("wdi/close", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Close the connection.
     */
    public void close() {
        try {
            closeAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @param offset Offset within the register (defaults to 0).
     * @param registerBank Register bank letter (defaults to U for user bank).
     * @return A CompletableFuture that can be completed to get the result:
     * Array of integers read from the device.
     */
    public CompletableFuture<int[]> genericReadAsync(
        int registerId,
        int size,
        int count,
        int offset,
        String registerBank) {
        zaber.motion.requests.WdiGenericRequest request =
            new zaber.motion.requests.WdiGenericRequest();
        request.setInterfaceId(getProviderId());
        request.setRegisterId(registerId);
        request.setSize(size);
        request.setCount(count);
        request.setOffset(offset);
        request.setRegisterBank(registerBank);
        CompletableFuture<zaber.motion.requests.IntArrayResponse> response = Call.callAsync(
            "wdi/read",
            request,
            zaber.motion.requests.IntArrayResponse.parser());
        return response
            .thenApply(r -> r.getValues());
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @param offset Offset within the register (defaults to 0).
     * @return A CompletableFuture that can be completed to get the result:
     * Array of integers read from the device.
     */
    public CompletableFuture<int[]> genericReadAsync(
        int registerId,
        int size,
        int count,
        int offset) {
        return genericReadAsync(registerId, size, count, offset, "U");
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @return A CompletableFuture that can be completed to get the result:
     * Array of integers read from the device.
     */
    public CompletableFuture<int[]> genericReadAsync(
        int registerId,
        int size,
        int count) {
        return genericReadAsync(registerId, size, count, 0, "U");
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @return A CompletableFuture that can be completed to get the result:
     * Array of integers read from the device.
     */
    public CompletableFuture<int[]> genericReadAsync(
        int registerId,
        int size) {
        return genericReadAsync(registerId, size, 1, 0, "U");
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @param offset Offset within the register (defaults to 0).
     * @param registerBank Register bank letter (defaults to U for user bank).
     * @return Array of integers read from the device.
     */
    public int[] genericRead(
        int registerId,
        int size,
        int count,
        int offset,
        String registerBank) {
        try {
            return genericReadAsync(registerId, size, count, offset, registerBank).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @param offset Offset within the register (defaults to 0).
     * @return Array of integers read from the device.
     */
    public int[] genericRead(
        int registerId,
        int size,
        int count,
        int offset) {
        return genericRead(registerId, size, count, offset, "U");
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @param count Number of values to read (defaults to 1).
     * @return Array of integers read from the device.
     */
    public int[] genericRead(
        int registerId,
        int size,
        int count) {
        return genericRead(registerId, size, count, 0, "U");
    }

    /**
     * Generic read operation.
     * @param registerId Register address to read from.
     * @param size Data size to read. Valid values are (1,2,4). Determines the data type (Byte, Word, DWord).
     * @return Array of integers read from the device.
     */
    public int[] genericRead(
        int registerId,
        int size) {
        return genericRead(registerId, size, 1, 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     * @param offset Offset within the register (defaults to 0).
     * @param registerBank Register bank letter (defaults to U for user bank).
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericWriteAsync(
        int registerId,
        int size,
        int[] data,
        int offset,
        String registerBank) {
        zaber.motion.requests.WdiGenericRequest request =
            new zaber.motion.requests.WdiGenericRequest();
        request.setInterfaceId(getProviderId());
        request.setRegisterId(registerId);
        request.setSize(size);
        request.setData(data);
        request.setOffset(offset);
        request.setRegisterBank(registerBank);
        return Call.callAsync("wdi/write", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     * @param offset Offset within the register (defaults to 0).
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericWriteAsync(
        int registerId,
        int size,
        int[] data,
        int offset) {
        return genericWriteAsync(registerId, size, data, offset, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericWriteAsync(
        int registerId,
        int size,
        int[] data) {
        return genericWriteAsync(registerId, size, data, 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericWriteAsync(
        int registerId,
        int size) {
        return genericWriteAsync(registerId, size, new int[0], 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericWriteAsync(
        int registerId) {
        return genericWriteAsync(registerId, 0, new int[0], 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     * @param offset Offset within the register (defaults to 0).
     * @param registerBank Register bank letter (defaults to U for user bank).
     */
    public void genericWrite(
        int registerId,
        int size,
        int[] data,
        int offset,
        String registerBank) {
        try {
            genericWriteAsync(registerId, size, data, offset, registerBank).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     * @param offset Offset within the register (defaults to 0).
     */
    public void genericWrite(
        int registerId,
        int size,
        int[] data,
        int offset) {
        genericWrite(registerId, size, data, offset, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     * @param data Array of values to write to the register. Empty array is allowed.
     */
    public void genericWrite(
        int registerId,
        int size,
        int[] data) {
        genericWrite(registerId, size, data, 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     * @param size Data size to write. Valid values are (0,1,2,4). Determines the data type (Nil, Byte, Word, DWord).
     */
    public void genericWrite(
        int registerId,
        int size) {
        genericWrite(registerId, size, new int[0], 0, "U");
    }

    /**
     * Generic write operation.
     * @param registerId Register address to read from.
     */
    public void genericWrite(
        int registerId) {
        genericWrite(registerId, 0, new int[0], 0, "U");
    }

    /**
     * Enables the laser.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> enableLaserAsync() {
        zaber.motion.requests.WdiGenericRequest request =
            new zaber.motion.requests.WdiGenericRequest();
        request.setInterfaceId(getProviderId());
        request.setRegisterId(1);
        return Call.callAsync("wdi/write", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Enables the laser.
     */
    public void enableLaser() {
        try {
            enableLaserAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Disables the laser.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> disableLaserAsync() {
        zaber.motion.requests.WdiGenericRequest request =
            new zaber.motion.requests.WdiGenericRequest();
        request.setInterfaceId(getProviderId());
        request.setRegisterId(2);
        return Call.callAsync("wdi/write", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Disables the laser.
     */
    public void disableLaser() {
        try {
            disableLaserAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the status of the autofocus.
     * @return A CompletableFuture that can be completed to get the result:
     * The status of the autofocus.
     */
    public CompletableFuture<WdiAutofocusProviderStatus> getStatusAsync() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getProviderId());
        CompletableFuture<zaber.motion.requests.WdiGetStatusResponse> response = Call.callAsync(
            "wdi/get_status",
            request,
            zaber.motion.requests.WdiGetStatusResponse.parser());
        return response
            .thenApply(r -> r.getStatus());
    }

    /**
     * Gets the status of the autofocus.
     * @return The status of the autofocus.
     */
    public WdiAutofocusProviderStatus getStatus() {
        try {
            return getStatusAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string that represents the autofocus connection.
     * @return A string that represents the connection.
     */
    public String toString() {
        zaber.motion.requests.InterfaceEmptyRequest request =
            new zaber.motion.requests.InterfaceEmptyRequest();
        request.setInterfaceId(getProviderId());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "wdi/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
