// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Device;
import zaber.motion.ascii.DeviceIO;
import zaber.motion.ascii.Connection;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Use to manage an LED controller.
 * Requires at least Firmware 7.09.
 */
public class Illuminator {
    private Device device;

    /**
     * @return The base device of this illuminator.
     */
    public Device getDevice() {
        return this.device;
    }

    private DeviceIO io;

    /**
     * @return I/O channels of this device.
     */
    public DeviceIO getIO() {
        return this.io;
    }

    /**
     * Creates instance of `Illuminator` based on the given device.
     * If the device is identified, this constructor will ensure it is an illuminator.
     */
    public Illuminator(
        Device device) {
        this.device = device;
        this.io = new DeviceIO(device);
        this.verifyIsIlluminator();
    }

    /**
     * Gets an IlluminatorChannel class instance that allows control of a particular channel.
     * Channels are numbered from 1.
     * @param channelNumber Number of channel to control.
     * @return Illuminator channel instance.
     */
    public IlluminatorChannel getChannel(
        int channelNumber) {
        if (channelNumber <= 0) {
            throw new IllegalArgumentException("Invalid value; channels are numbered from 1.");
        }
        return new IlluminatorChannel(this, channelNumber);
    }


    /**
     * Checks if this is an illuminator or some other type of device and throws an error if it is not.
     */
    private void verifyIsIlluminator() {
        zaber.motion.requests.DeviceEmptyRequest request =
            new zaber.motion.requests.DeviceEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        Call.callSync("illuminator/verify", request, null);
    }


    /**
     * Finds an illuminator on a connection.
     * In case of conflict, specify the optional device address.
     * @param connection Connection on which to detect the illuminator.
     * @param deviceAddress Optional device address of the illuminator.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of illuminator.
     */
    public static CompletableFuture<Illuminator> findAsync(
        Connection connection,
        int deviceAddress) {
        zaber.motion.requests.FindDeviceRequest request =
            new zaber.motion.requests.FindDeviceRequest();
        request.setInterfaceId(connection.getInterfaceId());
        request.setDeviceAddress(deviceAddress);
        CompletableFuture<zaber.motion.requests.FindDeviceResponse> response = Call.callAsync(
            "illuminator/detect",
            request,
            zaber.motion.requests.FindDeviceResponse.parser());
        return response
            .thenApply(r -> new Illuminator(new Device(connection, r.getAddress())));
    }

    /**
     * Finds an illuminator on a connection.
     * In case of conflict, specify the optional device address.
     * @param connection Connection on which to detect the illuminator.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of illuminator.
     */
    public static CompletableFuture<Illuminator> findAsync(
        Connection connection) {
        return findAsync(connection, 0);
    }

    /**
     * Finds an illuminator on a connection.
     * In case of conflict, specify the optional device address.
     * @param connection Connection on which to detect the illuminator.
     * @param deviceAddress Optional device address of the illuminator.
     * @return New instance of illuminator.
     */
    public static Illuminator find(
        Connection connection,
        int deviceAddress) {
        try {
            return findAsync(connection, deviceAddress).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Finds an illuminator on a connection.
     * In case of conflict, specify the optional device address.
     * @param connection Connection on which to detect the illuminator.
     * @return New instance of illuminator.
     */
    public static Illuminator find(
        Connection connection) {
        return find(connection, 0);
    }

    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
