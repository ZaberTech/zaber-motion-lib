// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Device;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.requests.AxisMoveType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * A generic turret device.
 */
public class FilterChanger {
    private Device device;

    /**
     * @return The base device of this turret.
     */
    public Device getDevice() {
        return this.device;
    }

    /**
     * Creates instance of `FilterChanger` based on the given device.
     */
    public FilterChanger(
        Device device) {
        this.device = device;
    }

    /**
     * Gets number of filters of the changer.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of positions.
     */
    public CompletableFuture<Integer> getNumberOfFiltersAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(1);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_count",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets number of filters of the changer.
     * @return Number of positions.
     */
    public int getNumberOfFilters() {
        try {
            return getNumberOfFiltersAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the current filter number starting from 1.
     * The value of 0 indicates that the position is either unknown or between two filters.
     * @return A CompletableFuture that can be completed to get the result:
     * Filter number starting from 1 or 0 if the position cannot be determined.
     */
    public CompletableFuture<Integer> getCurrentFilterAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(1);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_position",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns the current filter number starting from 1.
     * The value of 0 indicates that the position is either unknown or between two filters.
     * @return Filter number starting from 1 or 0 if the position cannot be determined.
     */
    public int getCurrentFilter() {
        try {
            return getCurrentFilterAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Changes to the specified filter.
     * @param filter Filter number starting from 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> changeAsync(
        int filter) {
        zaber.motion.requests.DeviceMoveRequest request =
            new zaber.motion.requests.DeviceMoveRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setAxis(1);
        request.setType(AxisMoveType.INDEX);
        request.setWaitUntilIdle(true);
        request.setArgInt(filter);
        return Call.callAsync("device/move", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Changes to the specified filter.
     * @param filter Filter number starting from 1.
     */
    public void change(
        int filter) {
        try {
            changeAsync(filter).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
