/* This file is generated. Do not modify by hand. */

package zaber.motion.microscopy;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Status of the WDI autofocus.
 */
public final class WdiAutofocusProviderStatus implements zaber.motion.dto.Message {

    private boolean inRange;

    /**
     * Indicates whether the autofocus is in range.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inRange")
    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    /**
     * Indicates whether the autofocus is in range.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("inRange")
    public boolean getInRange() {
        return this.inRange;
    }

    /**
     * Indicates whether the autofocus is in range.
     */
    public WdiAutofocusProviderStatus withInRange(boolean aInRange) {
        this.setInRange(aInRange);
        return this;
    }

    private boolean laserOn;

    /**
     * Indicates whether the laser is turned on.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("laserOn")
    public void setLaserOn(boolean laserOn) {
        this.laserOn = laserOn;
    }

    /**
     * Indicates whether the laser is turned on.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("laserOn")
    public boolean getLaserOn() {
        return this.laserOn;
    }

    /**
     * Indicates whether the laser is turned on.
     */
    public WdiAutofocusProviderStatus withLaserOn(boolean aLaserOn) {
        this.setLaserOn(aLaserOn);
        return this;
    }

    /**
     * Empty constructor.
     */
    public WdiAutofocusProviderStatus() {
    }

    /**
     * Constructor with all properties.
     */
    public WdiAutofocusProviderStatus(
        boolean inRange,
        boolean laserOn
    ) {
        this.inRange = inRange;
        this.laserOn = laserOn;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        WdiAutofocusProviderStatus other = (WdiAutofocusProviderStatus) obj;

        return (
            EqualityUtility.equals(inRange, other.inRange)
            && EqualityUtility.equals(laserOn, other.laserOn)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(inRange),
            EqualityUtility.generateHashCode(laserOn)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WdiAutofocusProviderStatus { ");
        sb.append("inRange: ");
        sb.append(this.inRange);
        sb.append(", ");
        sb.append("laserOn: ");
        sb.append(this.laserOn);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static WdiAutofocusProviderStatus fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, WdiAutofocusProviderStatus.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<WdiAutofocusProviderStatus> PARSER =
        new zaber.motion.dto.Parser<WdiAutofocusProviderStatus>() {
            @Override
            public WdiAutofocusProviderStatus fromByteArray(byte[] data) {
                return WdiAutofocusProviderStatus.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<WdiAutofocusProviderStatus> parser() {
        return PARSER;
    }

}
