// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Device;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.Units;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * An abstraction over a device and it's digital output channel.
 */
public class CameraTrigger {
    private Device device;

    /**
     * @return The device whose digital output triggers the camera.
     */
    public Device getDevice() {
        return this.device;
    }

    private int channel;

    /**
     * @return The digital output channel that triggers the camera.
     */
    public int getChannel() {
        return this.channel;
    }

    /**
     * Creates instance of `CameraTrigger` based on the given device and digital output channel.
     */
    public CameraTrigger(
        Device device, int channel) {
        this.device = device;
        this.channel = channel;
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     * @param unit Units of time.
     * @param wait If false, the method does not wait until the trigger pulse is finished.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> triggerAsync(
        double pulseWidth,
        Units unit,
        boolean wait) {
        zaber.motion.requests.MicroscopeTriggerCameraRequest request =
            new zaber.motion.requests.MicroscopeTriggerCameraRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        request.setChannelNumber(getChannel());
        request.setDelay(pulseWidth);
        request.setUnit(unit);
        request.setWait(wait);
        return Call.callAsync("microscope/trigger_camera", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     * @param unit Units of time.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> triggerAsync(
        double pulseWidth,
        Units unit) {
        return triggerAsync(pulseWidth, unit, true);
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> triggerAsync(
        double pulseWidth) {
        return triggerAsync(pulseWidth, Units.NATIVE, true);
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     * @param unit Units of time.
     * @param wait If false, the method does not wait until the trigger pulse is finished.
     */
    public void trigger(
        double pulseWidth,
        Units unit,
        boolean wait) {
        try {
            triggerAsync(pulseWidth, unit, wait).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     * @param unit Units of time.
     */
    public void trigger(
        double pulseWidth,
        Units unit) {
        trigger(pulseWidth, unit, true);
    }

    /**
     * Triggers the camera.
     * Schedules trigger pulse on the digital output channel.
     * By default, the method waits until the trigger pulse is finished.
     * @param pulseWidth The time duration of the trigger pulse.
     * Depending on the camera setting, the argument can be use to specify exposure.
     */
    public void trigger(
        double pulseWidth) {
        trigger(pulseWidth, Units.NATIVE, true);
    }

    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getDevice().getConnection().getInterfaceId());
        request.setDevice(getDevice().getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
