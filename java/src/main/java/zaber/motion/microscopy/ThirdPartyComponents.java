/* This file is generated. Do not modify by hand. */

package zaber.motion.microscopy;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.EqualityUtility;


/**
 * Third party components of the microscope.
 */
public final class ThirdPartyComponents implements zaber.motion.dto.Message {

    private Integer autofocus;

    /**
     * Autofocus provider identifier.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("autofocus")
    public void setAutofocus(Integer autofocus) {
        this.autofocus = autofocus;
    }

    /**
     * Autofocus provider identifier.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("autofocus")
    public Integer getAutofocus() {
        return this.autofocus;
    }

    /**
     * Autofocus provider identifier.
     */
    public ThirdPartyComponents withAutofocus(Integer aAutofocus) {
        this.setAutofocus(aAutofocus);
        return this;
    }

    /**
     * Empty constructor.
     */
    public ThirdPartyComponents() {
    }

    /**
     * Constructor with all properties.
     */
    public ThirdPartyComponents(
        Integer autofocus
    ) {
        this.autofocus = autofocus;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ThirdPartyComponents other = (ThirdPartyComponents) obj;

        return (
            EqualityUtility.equals(autofocus, other.autofocus)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(autofocus)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ThirdPartyComponents { ");
        sb.append("autofocus: ");
        sb.append(this.autofocus);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static ThirdPartyComponents fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, ThirdPartyComponents.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<ThirdPartyComponents> PARSER =
        new zaber.motion.dto.Parser<ThirdPartyComponents>() {
            @Override
            public ThirdPartyComponents fromByteArray(byte[] data) {
                return ThirdPartyComponents.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<ThirdPartyComponents> parser() {
        return PARSER;
    }

}
