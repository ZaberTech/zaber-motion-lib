// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.AxisGroup;
import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.gateway.Call;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represent a microscope.
 * Parts of the microscope may or may not be instantiated depending on the configuration.
 * Requires at least Firmware 7.34.
 */
public class Microscope {
    private Connection connection;

    /**
     * @return Connection of the microscope.
     */
    public Connection getConnection() {
        return this.connection;
    }

    private MicroscopeConfig config;

    private Illuminator illuminator;

    /**
     * @return The illuminator.
     */
    public Illuminator getIlluminator() {
        return this.illuminator;
    }

    private Axis focusAxis;

    /**
     * @return The focus axis.
     */
    public Axis getFocusAxis() {
        return this.focusAxis;
    }

    private Axis xAxis;

    /**
     * @return The X axis.
     */
    public Axis getXAxis() {
        return this.xAxis;
    }

    private Axis yAxis;

    /**
     * @return The Y axis.
     */
    public Axis getYAxis() {
        return this.yAxis;
    }

    private AxisGroup plate;

    /**
     * @return Axis group consisting of X and Y axes representing the plate of the microscope.
     */
    public AxisGroup getPlate() {
        return this.plate;
    }

    private ObjectiveChanger objectiveChanger;

    /**
     * @return The objective changer.
     */
    public ObjectiveChanger getObjectiveChanger() {
        return this.objectiveChanger;
    }

    private FilterChanger filterChanger;

    /**
     * @return The filter changer.
     */
    public FilterChanger getFilterChanger() {
        return this.filterChanger;
    }

    private Autofocus autofocus;

    /**
     * @return The autofocus feature.
     */
    public Autofocus getAutofocus() {
        return this.autofocus;
    }

    private CameraTrigger cameraTrigger;

    /**
     * @return The camera trigger.
     */
    public CameraTrigger getCameraTrigger() {
        return this.cameraTrigger;
    }

    /**
     * Creates instance of `Microscope` from the given config.
     * Parts are instantiated depending on device addresses in the config.
     */
    public Microscope(
        Connection connection, MicroscopeConfig config) {
        this.connection = connection;
        this.config = MicroscopeConfig.fromByteArray(config.toByteArray());
        this.illuminator = config.getIlluminator() != null && config.getIlluminator() > 0
            ? new Illuminator(new Device(connection, config.getIlluminator())) : null;
        this.focusAxis = config.getFocusAxis() != null && config.getFocusAxis().getDevice() > 0
            ? new Axis(new Device(connection, config.getFocusAxis().getDevice()), config.getFocusAxis().getAxis()) : null;
        this.xAxis = config.getXAxis() != null && config.getXAxis().getDevice() > 0
            ? new Axis(new Device(connection, config.getXAxis().getDevice()), config.getXAxis().getAxis()) : null;
        this.yAxis = config.getYAxis() != null && config.getYAxis().getDevice() > 0
            ? new Axis(new Device(connection, config.getYAxis().getDevice()), config.getYAxis().getAxis()) : null;
        this.plate = this.xAxis != null && this.yAxis != null
            ? new AxisGroup(new Axis[] {this.xAxis, this.yAxis}) : null;
        this.objectiveChanger = config.getObjectiveChanger() != null && config.getObjectiveChanger() > 0 && this.focusAxis != null
            ? new ObjectiveChanger(new Device(connection, config.getObjectiveChanger()), this.focusAxis) : null;
        this.filterChanger = config.getFilterChanger() != null && config.getFilterChanger() > 0
            ? new FilterChanger(new Device(connection, config.getFilterChanger())) : null;
        this.autofocus = config.getAutofocus() != null && config.getAutofocus() > 0 && getFocusAxis() != null
            ? new Autofocus(config.getAutofocus(), getFocusAxis(), this.objectiveChanger != null ? this.objectiveChanger.getTurret() : null) : null;
        this.cameraTrigger = config.getCameraTrigger() != null && config.getCameraTrigger().getDevice() > 0
            ? new CameraTrigger(new Device(connection, config.getCameraTrigger().getDevice()), config.getCameraTrigger().getChannel()) : null;
    }

    /**
     * Finds a microscope on a connection.
     * @param connection Connection on which to detect the microscope.
     * @param thirdPartyComponents Third party components of the microscope that cannot be found on the connection.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of microscope.
     */
    public static CompletableFuture<Microscope> findAsync(
        Connection connection,
        ThirdPartyComponents thirdPartyComponents) {
        zaber.motion.requests.MicroscopeFindRequest request =
            new zaber.motion.requests.MicroscopeFindRequest();
        request.setInterfaceId(connection.getInterfaceId());
        request.setThirdParty(thirdPartyComponents);
        CompletableFuture<zaber.motion.requests.MicroscopeConfigResponse> response = Call.callAsync(
            "microscope/detect",
            request,
            zaber.motion.requests.MicroscopeConfigResponse.parser());
        return response
            .thenApply(r -> new Microscope(connection, r.getConfig()));
    }

    /**
     * Finds a microscope on a connection.
     * @param connection Connection on which to detect the microscope.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of microscope.
     */
    public static CompletableFuture<Microscope> findAsync(
        Connection connection) {
        return findAsync(connection, null);
    }

    /**
     * Finds a microscope on a connection.
     * @param connection Connection on which to detect the microscope.
     * @param thirdPartyComponents Third party components of the microscope that cannot be found on the connection.
     * @return New instance of microscope.
     */
    public static Microscope find(
        Connection connection,
        ThirdPartyComponents thirdPartyComponents) {
        try {
            return findAsync(connection, thirdPartyComponents).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Finds a microscope on a connection.
     * @param connection Connection on which to detect the microscope.
     * @return New instance of microscope.
     */
    public static Microscope find(
        Connection connection) {
        return find(connection, null);
    }

    /**
     * Initializes the microscope.
     * Homes all axes, filter changer, and objective changer if they require it.
     * @param force Forces all devices to home even when not required.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> initializeAsync(
        boolean force) {
        zaber.motion.requests.MicroscopeInitRequest request =
            new zaber.motion.requests.MicroscopeInitRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setConfig(this.config);
        request.setForce(force);
        return Call.callAsync("microscope/initialize", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Initializes the microscope.
     * Homes all axes, filter changer, and objective changer if they require it.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> initializeAsync() {
        return initializeAsync(false);
    }

    /**
     * Initializes the microscope.
     * Homes all axes, filter changer, and objective changer if they require it.
     * @param force Forces all devices to home even when not required.
     */
    public void initialize(
        boolean force) {
        try {
            initializeAsync(force).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Initializes the microscope.
     * Homes all axes, filter changer, and objective changer if they require it.
     */
    public void initialize() {
        initialize(false);
    }

    /**
     * Checks whether the microscope is initialized.
     * @return A CompletableFuture that can be completed to get the result:
     * True, when the microscope is initialized. False, otherwise.
     */
    public CompletableFuture<Boolean> isInitializedAsync() {
        zaber.motion.requests.MicroscopeEmptyRequest request =
            new zaber.motion.requests.MicroscopeEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setConfig(this.config);
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "microscope/is_initialized",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Checks whether the microscope is initialized.
     * @return True, when the microscope is initialized. False, otherwise.
     */
    public boolean isInitialized() {
        try {
            return isInitializedAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string that represents the microscope.
     * @return A string that represents the microscope.
     */
    public String toString() {
        zaber.motion.requests.MicroscopeEmptyRequest request =
            new zaber.motion.requests.MicroscopeEmptyRequest();
        request.setInterfaceId(getConnection().getInterfaceId());
        request.setConfig(this.config);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "microscope/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
