/* This file is generated. Do not modify by hand. */

package zaber.motion.microscopy;

import java.util.Objects;

import zaber.motion.dto.Mapper;
import zaber.motion.AxisAddress;
import zaber.motion.ChannelAddress;
import zaber.motion.EqualityUtility;


/**
 * Configuration representing a microscope setup.
 * Device address of value 0 means that the part is not present.
 */
public final class MicroscopeConfig implements zaber.motion.dto.Message {

    private AxisAddress focusAxis;

    /**
     * Focus axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public void setFocusAxis(AxisAddress focusAxis) {
        this.focusAxis = focusAxis;
    }

    /**
     * Focus axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("focusAxis")
    public AxisAddress getFocusAxis() {
        return this.focusAxis;
    }

    /**
     * Focus axis of the microscope.
     */
    public MicroscopeConfig withFocusAxis(AxisAddress aFocusAxis) {
        this.setFocusAxis(aFocusAxis);
        return this;
    }

    private AxisAddress xAxis;

    /**
     * X axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("xAxis")
    public void setXAxis(AxisAddress xAxis) {
        this.xAxis = xAxis;
    }

    /**
     * X axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("xAxis")
    public AxisAddress getXAxis() {
        return this.xAxis;
    }

    /**
     * X axis of the microscope.
     */
    public MicroscopeConfig withXAxis(AxisAddress aXAxis) {
        this.setXAxis(aXAxis);
        return this;
    }

    private AxisAddress yAxis;

    /**
     * Y axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("yAxis")
    public void setYAxis(AxisAddress yAxis) {
        this.yAxis = yAxis;
    }

    /**
     * Y axis of the microscope.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("yAxis")
    public AxisAddress getYAxis() {
        return this.yAxis;
    }

    /**
     * Y axis of the microscope.
     */
    public MicroscopeConfig withYAxis(AxisAddress aYAxis) {
        this.setYAxis(aYAxis);
        return this;
    }

    private Integer illuminator;

    /**
     * Illuminator device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("illuminator")
    public void setIlluminator(Integer illuminator) {
        this.illuminator = illuminator;
    }

    /**
     * Illuminator device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("illuminator")
    public Integer getIlluminator() {
        return this.illuminator;
    }

    /**
     * Illuminator device address.
     */
    public MicroscopeConfig withIlluminator(Integer aIlluminator) {
        this.setIlluminator(aIlluminator);
        return this;
    }

    private Integer filterChanger;

    /**
     * Filter changer device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("filterChanger")
    public void setFilterChanger(Integer filterChanger) {
        this.filterChanger = filterChanger;
    }

    /**
     * Filter changer device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("filterChanger")
    public Integer getFilterChanger() {
        return this.filterChanger;
    }

    /**
     * Filter changer device address.
     */
    public MicroscopeConfig withFilterChanger(Integer aFilterChanger) {
        this.setFilterChanger(aFilterChanger);
        return this;
    }

    private Integer objectiveChanger;

    /**
     * Objective changer device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("objectiveChanger")
    public void setObjectiveChanger(Integer objectiveChanger) {
        this.objectiveChanger = objectiveChanger;
    }

    /**
     * Objective changer device address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("objectiveChanger")
    public Integer getObjectiveChanger() {
        return this.objectiveChanger;
    }

    /**
     * Objective changer device address.
     */
    public MicroscopeConfig withObjectiveChanger(Integer aObjectiveChanger) {
        this.setObjectiveChanger(aObjectiveChanger);
        return this;
    }

    private Integer autofocus;

    /**
     * Autofocus identifier.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("autofocus")
    public void setAutofocus(Integer autofocus) {
        this.autofocus = autofocus;
    }

    /**
     * Autofocus identifier.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("autofocus")
    public Integer getAutofocus() {
        return this.autofocus;
    }

    /**
     * Autofocus identifier.
     */
    public MicroscopeConfig withAutofocus(Integer aAutofocus) {
        this.setAutofocus(aAutofocus);
        return this;
    }

    private ChannelAddress cameraTrigger;

    /**
     * Camera trigger digital output address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("cameraTrigger")
    public void setCameraTrigger(ChannelAddress cameraTrigger) {
        this.cameraTrigger = cameraTrigger;
    }

    /**
     * Camera trigger digital output address.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("cameraTrigger")
    public ChannelAddress getCameraTrigger() {
        return this.cameraTrigger;
    }

    /**
     * Camera trigger digital output address.
     */
    public MicroscopeConfig withCameraTrigger(ChannelAddress aCameraTrigger) {
        this.setCameraTrigger(aCameraTrigger);
        return this;
    }

    /**
     * Empty constructor.
     */
    public MicroscopeConfig() {
    }

    /**
     * Constructor with all properties.
     */
    public MicroscopeConfig(
        AxisAddress focusAxis,
        AxisAddress xAxis,
        AxisAddress yAxis,
        Integer illuminator,
        Integer filterChanger,
        Integer objectiveChanger,
        Integer autofocus,
        ChannelAddress cameraTrigger
    ) {
        this.focusAxis = focusAxis;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.illuminator = illuminator;
        this.filterChanger = filterChanger;
        this.objectiveChanger = objectiveChanger;
        this.autofocus = autofocus;
        this.cameraTrigger = cameraTrigger;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MicroscopeConfig other = (MicroscopeConfig) obj;

        return (
            EqualityUtility.equals(focusAxis, other.focusAxis)
            && EqualityUtility.equals(xAxis, other.xAxis)
            && EqualityUtility.equals(yAxis, other.yAxis)
            && EqualityUtility.equals(illuminator, other.illuminator)
            && EqualityUtility.equals(filterChanger, other.filterChanger)
            && EqualityUtility.equals(objectiveChanger, other.objectiveChanger)
            && EqualityUtility.equals(autofocus, other.autofocus)
            && EqualityUtility.equals(cameraTrigger, other.cameraTrigger)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(focusAxis),
            EqualityUtility.generateHashCode(xAxis),
            EqualityUtility.generateHashCode(yAxis),
            EqualityUtility.generateHashCode(illuminator),
            EqualityUtility.generateHashCode(filterChanger),
            EqualityUtility.generateHashCode(objectiveChanger),
            EqualityUtility.generateHashCode(autofocus),
            EqualityUtility.generateHashCode(cameraTrigger)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MicroscopeConfig { ");
        sb.append("focusAxis: ");
        sb.append(this.focusAxis);
        sb.append(", ");
        sb.append("xAxis: ");
        sb.append(this.xAxis);
        sb.append(", ");
        sb.append("yAxis: ");
        sb.append(this.yAxis);
        sb.append(", ");
        sb.append("illuminator: ");
        sb.append(this.illuminator);
        sb.append(", ");
        sb.append("filterChanger: ");
        sb.append(this.filterChanger);
        sb.append(", ");
        sb.append("objectiveChanger: ");
        sb.append(this.objectiveChanger);
        sb.append(", ");
        sb.append("autofocus: ");
        sb.append(this.autofocus);
        sb.append(", ");
        sb.append("cameraTrigger: ");
        sb.append(this.cameraTrigger);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static MicroscopeConfig fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, MicroscopeConfig.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<MicroscopeConfig> PARSER =
        new zaber.motion.dto.Parser<MicroscopeConfig>() {
            @Override
            public MicroscopeConfig fromByteArray(byte[] data) {
                return MicroscopeConfig.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<MicroscopeConfig> parser() {
        return PARSER;
    }

}
