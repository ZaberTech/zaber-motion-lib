// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.Device;
import zaber.motion.ascii.Connection;
import zaber.motion.Measurement;
import zaber.motion.Units;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Represents an objective changer of a microscope.
 * Unstable. Expect breaking changes in future releases.
 * Requires at least Firmware 7.32.
 */
public class ObjectiveChanger {
    private Device turret;

    /**
     * @return Device address of the turret.
     */
    public Device getTurret() {
        return this.turret;
    }

    private Axis focusAxis;

    /**
     * @return The focus axis.
     */
    public Axis getFocusAxis() {
        return this.focusAxis;
    }

    /**
     * Creates instance of `ObjectiveChanger` based on the given device.
     * If the device is identified, this constructor will ensure it is an objective changer.
     */
    public ObjectiveChanger(
        Device turret, Axis focusAxis) {
        this.turret = turret;
        this.focusAxis = focusAxis;
        this.verifyIsChanger();
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @param turretAddress Optional device address of the turret device (X-MOR).
     * @param focusAddress Optional device address of the focus device (X-LDA).
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of objective changer.
     */
    @Deprecated
    public static CompletableFuture<ObjectiveChanger> findAsync(
        Connection connection,
        int turretAddress,
        int focusAddress) {
        zaber.motion.requests.ObjectiveChangerRequest request =
            new zaber.motion.requests.ObjectiveChangerRequest();
        request.setInterfaceId(connection.getInterfaceId());
        request.setTurretAddress(turretAddress);
        request.setFocusAddress(focusAddress);
        CompletableFuture<zaber.motion.requests.ObjectiveChangerCreateResponse> response = Call.callAsync(
            "objective_changer/detect",
            request,
            zaber.motion.requests.ObjectiveChangerCreateResponse.parser());
        return response
            .thenApply(r -> new ObjectiveChanger(
                new Device(connection, r.getTurret()),
                new Axis(new Device(connection, r.getFocusAddress()), r.getFocusAxis())));
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @param turretAddress Optional device address of the turret device (X-MOR).
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of objective changer.
     */
    @Deprecated
    public static CompletableFuture<ObjectiveChanger> findAsync(
        Connection connection,
        int turretAddress) {
        return findAsync(connection, turretAddress, 0);
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @return A CompletableFuture that can be completed to get the result:
     * New instance of objective changer.
     */
    @Deprecated
    public static CompletableFuture<ObjectiveChanger> findAsync(
        Connection connection) {
        return findAsync(connection, 0, 0);
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @param turretAddress Optional device address of the turret device (X-MOR).
     * @param focusAddress Optional device address of the focus device (X-LDA).
     * @return New instance of objective changer.
     */
    public static ObjectiveChanger find(
        Connection connection,
        int turretAddress,
        int focusAddress) {
        try {
            return findAsync(connection, turretAddress, focusAddress).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @param turretAddress Optional device address of the turret device (X-MOR).
     * @return New instance of objective changer.
     */
    @Deprecated
    public static ObjectiveChanger find(
        Connection connection,
        int turretAddress) {
        return find(connection, turretAddress, 0);
    }

    /**
     * @deprecated Use microscope's `Find` method instead or instantiate manually.
     *
     * Finds an objective changer on a connection.
     * In case of conflict, specify the optional device addresses.
     * Devices on the connection must be identified.
     * @param connection Connection on which to detect the objective changer.
     * @return New instance of objective changer.
     */
    @Deprecated
    public static ObjectiveChanger find(
        Connection connection) {
        return find(connection, 0, 0);
    }

    /**
     * Changes the objective.
     * Runs a sequence of movements switching from the current objective to the new one.
     * The focus stage moves to the focus datum after the objective change.
     * @param objective Objective number starting from 1.
     * @param focusOffset Optional offset from the focus datum.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> changeAsync(
        int objective,
        Measurement focusOffset) {
        zaber.motion.requests.ObjectiveChangerChangeRequest request =
            new zaber.motion.requests.ObjectiveChangerChangeRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setTurretAddress(getTurret().getDeviceAddress());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setObjective(objective);
        request.setFocusOffset(focusOffset);
        return Call.callAsync("objective_changer/change", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Changes the objective.
     * Runs a sequence of movements switching from the current objective to the new one.
     * The focus stage moves to the focus datum after the objective change.
     * @param objective Objective number starting from 1.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> changeAsync(
        int objective) {
        return changeAsync(objective, null);
    }

    /**
     * Changes the objective.
     * Runs a sequence of movements switching from the current objective to the new one.
     * The focus stage moves to the focus datum after the objective change.
     * @param objective Objective number starting from 1.
     * @param focusOffset Optional offset from the focus datum.
     */
    public void change(
        int objective,
        Measurement focusOffset) {
        try {
            changeAsync(objective, focusOffset).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Changes the objective.
     * Runs a sequence of movements switching from the current objective to the new one.
     * The focus stage moves to the focus datum after the objective change.
     * @param objective Objective number starting from 1.
     */
    public void change(
        int objective) {
        change(objective, null);
    }

    /**
     * Moves the focus stage out of the turret releasing the current objective.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> releaseAsync() {
        zaber.motion.requests.ObjectiveChangerRequest request =
            new zaber.motion.requests.ObjectiveChangerRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setTurretAddress(getTurret().getDeviceAddress());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        return Call.callAsync("objective_changer/release", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the focus stage out of the turret releasing the current objective.
     */
    public void release() {
        try {
            releaseAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns current objective number starting from 1.
     * The value of 0 indicates that the position is either unknown or between two objectives.
     * @return A CompletableFuture that can be completed to get the result:
     * Current objective number starting from 1 or 0 if not applicable.
     */
    public CompletableFuture<Integer> getCurrentObjectiveAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setDevice(getTurret().getDeviceAddress());
        request.setAxis(1);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_position",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns current objective number starting from 1.
     * The value of 0 indicates that the position is either unknown or between two objectives.
     * @return Current objective number starting from 1 or 0 if not applicable.
     */
    public int getCurrentObjective() {
        try {
            return getCurrentObjectiveAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets number of objectives that the turret can accommodate.
     * @return A CompletableFuture that can be completed to get the result:
     * Number of positions.
     */
    public CompletableFuture<Integer> getNumberOfObjectivesAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setDevice(getTurret().getDeviceAddress());
        request.setAxis(1);
        CompletableFuture<zaber.motion.requests.IntResponse> response = Call.callAsync(
            "device/get_index_count",
            request,
            zaber.motion.requests.IntResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets number of objectives that the turret can accommodate.
     * @return Number of positions.
     */
    public int getNumberOfObjectives() {
        try {
            return getNumberOfObjectivesAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param unit Units of datum.
     * @return A CompletableFuture that can be completed to get the result:
     * The datum.
     */
    public CompletableFuture<Double> getFocusDatumAsync(
        Units unit) {
        zaber.motion.requests.ObjectiveChangerSetRequest request =
            new zaber.motion.requests.ObjectiveChangerSetRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setTurretAddress(getTurret().getDeviceAddress());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "objective_changer/get_datum",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @return A CompletableFuture that can be completed to get the result:
     * The datum.
     */
    public CompletableFuture<Double> getFocusDatumAsync() {
        return getFocusDatumAsync(Units.NATIVE);
    }

    /**
     * Gets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param unit Units of datum.
     * @return The datum.
     */
    public double getFocusDatum(
        Units unit) {
        try {
            return getFocusDatumAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @return The datum.
     */
    public double getFocusDatum() {
        return getFocusDatum(Units.NATIVE);
    }

    /**
     * Sets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param datum Value of datum.
     * @param unit Units of datum.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setFocusDatumAsync(
        double datum,
        Units unit) {
        zaber.motion.requests.ObjectiveChangerSetRequest request =
            new zaber.motion.requests.ObjectiveChangerSetRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setTurretAddress(getTurret().getDeviceAddress());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setValue(datum);
        request.setUnit(unit);
        return Call.callAsync("objective_changer/set_datum", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param datum Value of datum.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setFocusDatumAsync(
        double datum) {
        return setFocusDatumAsync(datum, Units.NATIVE);
    }

    /**
     * Sets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param datum Value of datum.
     * @param unit Units of datum.
     */
    public void setFocusDatum(
        double datum,
        Units unit) {
        try {
            setFocusDatumAsync(datum, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the focus datum.
     * The focus datum is the position that the focus stage moves to after an objective change.
     * It is backed by the limit.home.offset setting.
     * @param datum Value of datum.
     */
    public void setFocusDatum(
        double datum) {
        setFocusDatum(datum, Units.NATIVE);
    }

    /**
     * Checks if this is a objective changer and throws an error if it is not.
     */
    private void verifyIsChanger() {
        zaber.motion.requests.ObjectiveChangerRequest request =
            new zaber.motion.requests.ObjectiveChangerRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setTurretAddress(getTurret().getDeviceAddress());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        Call.callSync("objective_changer/verify", request, null);
    }


    /**
     * Returns a string that represents the device.
     * @return A string that represents the device.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getTurret().getConnection().getInterfaceId());
        request.setDevice(getTurret().getDeviceAddress());
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/device_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
