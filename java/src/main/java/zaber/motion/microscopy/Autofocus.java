// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.Device;
import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.Units;
import zaber.motion.NamedParameter;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * A generic autofocus device.
 */
public class Autofocus {
    private int providerId;

    /**
     * @return The identification of external device providing the capability.
     */
    public int getProviderId() {
        return this.providerId;
    }

    private Axis focusAxis;

    /**
     * @return The focus axis.
     */
    public Axis getFocusAxis() {
        return this.focusAxis;
    }

    private Device objectiveTurret;

    /**
     * @return The objective turret device if the microscope has one.
     */
    public Device getObjectiveTurret() {
        return this.objectiveTurret;
    }

    /**
     * Creates instance of `Autofocus` based on the given provider id.
     */
    public Autofocus(
        int providerId, Axis focusAxis, Device objectiveTurret) {
        this.providerId = providerId;
        this.focusAxis = focusAxis;
        this.objectiveTurret = objectiveTurret;
    }

    /**
     * Sets the current focus to be target for the autofocus control loop.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setFocusZeroAsync() {
        zaber.motion.requests.EmptyAutofocusRequest request =
            new zaber.motion.requests.EmptyAutofocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        return Call.callAsync("autofocus/set_zero", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the current focus to be target for the autofocus control loop.
     */
    public void setFocusZero() {
        try {
            setFocusZeroAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the status of the autofocus.
     * @return A CompletableFuture that can be completed to get the result:
     * The status of the autofocus.
     */
    public CompletableFuture<AutofocusStatus> getStatusAsync() {
        zaber.motion.requests.EmptyAutofocusRequest request =
            new zaber.motion.requests.EmptyAutofocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        CompletableFuture<zaber.motion.requests.AutofocusGetStatusResponse> response = Call.callAsync(
            "autofocus/get_status",
            request,
            zaber.motion.requests.AutofocusGetStatusResponse.parser());
        return response
            .thenApply(r -> r.getStatus());
    }

    /**
     * Returns the status of the autofocus.
     * @return The status of the autofocus.
     */
    public AutofocusStatus getStatus() {
        try {
            return getStatusAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the device until it's in focus.
     * @param scan If true, the autofocus will approach from the limit moving until it's in range.
     * @param timeout Sets autofocus timeout duration in milliseconds.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> focusOnceAsync(
        boolean scan,
        int timeout) {
        zaber.motion.requests.AutofocusFocusRequest request =
            new zaber.motion.requests.AutofocusFocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        request.setOnce(true);
        request.setScan(scan);
        request.setTimeout(timeout);
        return Call.callAsync("autofocus/focus_once", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the device until it's in focus.
     * @param scan If true, the autofocus will approach from the limit moving until it's in range.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> focusOnceAsync(
        boolean scan) {
        return focusOnceAsync(scan, -1);
    }

    /**
     * Moves the device until it's in focus.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> focusOnceAsync() {
        return focusOnceAsync(false, -1);
    }

    /**
     * Moves the device until it's in focus.
     * @param scan If true, the autofocus will approach from the limit moving until it's in range.
     * @param timeout Sets autofocus timeout duration in milliseconds.
     */
    public void focusOnce(
        boolean scan,
        int timeout) {
        try {
            focusOnceAsync(scan, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Moves the device until it's in focus.
     * @param scan If true, the autofocus will approach from the limit moving until it's in range.
     */
    public void focusOnce(
        boolean scan) {
        focusOnce(scan, -1);
    }

    /**
     * Moves the device until it's in focus.
     */
    public void focusOnce() {
        focusOnce(false, -1);
    }

    /**
     * Moves the focus axis continuously maintaining focus.
     * Starts the autofocus control loop.
     * Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> startFocusLoopAsync() {
        zaber.motion.requests.AutofocusFocusRequest request =
            new zaber.motion.requests.AutofocusFocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        return Call.callAsync("autofocus/start_focus_loop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Moves the focus axis continuously maintaining focus.
     * Starts the autofocus control loop.
     * Note that the control loop may stop if the autofocus comes out of range or a movement error occurs.
     */
    public void startFocusLoop() {
        try {
            startFocusLoopAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Stops autofocus control loop.
     * If the focus axis already stopped moving because of an error, an exception will be thrown.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> stopFocusLoopAsync() {
        zaber.motion.requests.EmptyAutofocusRequest request =
            new zaber.motion.requests.EmptyAutofocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        return Call.callAsync("autofocus/stop_focus_loop", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Stops autofocus control loop.
     * If the focus axis already stopped moving because of an error, an exception will be thrown.
     */
    public void stopFocusLoop() {
        try {
            stopFocusLoopAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the lower motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.min setting of the focus axis.
     * @param unit The units of the limit.
     * @return A CompletableFuture that can be completed to get the result:
     * Limit value.
     */
    public CompletableFuture<Double> getLimitMinAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getFocusAxis().getDevice().getDeviceAddress());
        request.setAxis(getFocusAxis().getAxisNumber());
        request.setSetting("motion.tracking.limit.min");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the lower motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.min setting of the focus axis.
     * @return A CompletableFuture that can be completed to get the result:
     * Limit value.
     */
    public CompletableFuture<Double> getLimitMinAsync() {
        return getLimitMinAsync(Units.NATIVE);
    }

    /**
     * Gets the lower motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.min setting of the focus axis.
     * @param unit The units of the limit.
     * @return Limit value.
     */
    public double getLimitMin(
        Units unit) {
        try {
            return getLimitMinAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the lower motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.min setting of the focus axis.
     * @return Limit value.
     */
    public double getLimitMin() {
        return getLimitMin(Units.NATIVE);
    }

    /**
     * Gets the upper motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.max setting of the focus axis.
     * @param unit The units of the limit.
     * @return A CompletableFuture that can be completed to get the result:
     * Limit value.
     */
    public CompletableFuture<Double> getLimitMaxAsync(
        Units unit) {
        zaber.motion.requests.DeviceGetSettingRequest request =
            new zaber.motion.requests.DeviceGetSettingRequest();
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getFocusAxis().getDevice().getDeviceAddress());
        request.setAxis(getFocusAxis().getAxisNumber());
        request.setSetting("motion.tracking.limit.max");
        request.setUnit(unit);
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "device/get_setting",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the upper motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.max setting of the focus axis.
     * @return A CompletableFuture that can be completed to get the result:
     * Limit value.
     */
    public CompletableFuture<Double> getLimitMaxAsync() {
        return getLimitMaxAsync(Units.NATIVE);
    }

    /**
     * Gets the upper motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.max setting of the focus axis.
     * @param unit The units of the limit.
     * @return Limit value.
     */
    public double getLimitMax(
        Units unit) {
        try {
            return getLimitMaxAsync(unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the upper motion limit for the autofocus control loop.
     * Gets motion.tracking.limit.max setting of the focus axis.
     * @return Limit value.
     */
    public double getLimitMax() {
        return getLimitMax(Units.NATIVE);
    }

    /**
     * Sets the lower motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.min setting of the focus axis.
     * @param limit The lower limit of the focus axis.
     * @param unit The units of the limit.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLimitMinAsync(
        double limit,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getFocusAxis().getDevice().getDeviceAddress());
        request.setAxis(getFocusAxis().getAxisNumber());
        request.setSetting("motion.tracking.limit.min");
        request.setValue(limit);
        request.setUnit(unit);
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the lower motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.min setting of the focus axis.
     * @param limit The lower limit of the focus axis.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLimitMinAsync(
        double limit) {
        return setLimitMinAsync(limit, Units.NATIVE);
    }

    /**
     * Sets the lower motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.min setting of the focus axis.
     * @param limit The lower limit of the focus axis.
     * @param unit The units of the limit.
     */
    public void setLimitMin(
        double limit,
        Units unit) {
        try {
            setLimitMinAsync(limit, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the lower motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.min setting of the focus axis.
     * @param limit The lower limit of the focus axis.
     */
    public void setLimitMin(
        double limit) {
        setLimitMin(limit, Units.NATIVE);
    }

    /**
     * Sets the upper motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.max setting of the focus axis.
     * @param limit The upper limit of the focus axis.
     * @param unit The units of the limit.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLimitMaxAsync(
        double limit,
        Units unit) {
        zaber.motion.requests.DeviceSetSettingRequest request =
            new zaber.motion.requests.DeviceSetSettingRequest();
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setDevice(getFocusAxis().getDevice().getDeviceAddress());
        request.setAxis(getFocusAxis().getAxisNumber());
        request.setSetting("motion.tracking.limit.max");
        request.setValue(limit);
        request.setUnit(unit);
        return Call.callAsync("device/set_setting", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the upper motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.max setting of the focus axis.
     * @param limit The upper limit of the focus axis.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setLimitMaxAsync(
        double limit) {
        return setLimitMaxAsync(limit, Units.NATIVE);
    }

    /**
     * Sets the upper motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.max setting of the focus axis.
     * @param limit The upper limit of the focus axis.
     * @param unit The units of the limit.
     */
    public void setLimitMax(
        double limit,
        Units unit) {
        try {
            setLimitMaxAsync(limit, unit).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the upper motion limit for the autofocus control loop.
     * Use the limits to prevent the focus axis from crashing into the sample.
     * Changes motion.tracking.limit.max setting of the focus axis.
     * @param limit The upper limit of the focus axis.
     */
    public void setLimitMax(
        double limit) {
        setLimitMax(limit, Units.NATIVE);
    }

    /**
     * Typically, the control loop parameters and objective are kept synchronized by the library.
     * If the parameters or current objective changes outside of the library, call this method to synchronize them.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> synchronizeParametersAsync() {
        zaber.motion.requests.EmptyAutofocusRequest request =
            new zaber.motion.requests.EmptyAutofocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        return Call.callAsync("autofocus/sync_params", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Typically, the control loop parameters and objective are kept synchronized by the library.
     * If the parameters or current objective changes outside of the library, call this method to synchronize them.
     */
    public void synchronizeParameters() {
        try {
            synchronizeParametersAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets the parameters for the autofocus objective.
     * Note that the method temporarily switches current objective to set the parameters.
     * @param objective The objective (numbered from 1) to set the parameters for.
     * If your microscope has only one objective, use value of 1.
     * @param parameters The parameters for the autofocus objective.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setObjectiveParametersAsync(
        int objective,
        NamedParameter[] parameters) {
        zaber.motion.requests.AutofocusSetObjectiveParamsRequest request =
            new zaber.motion.requests.AutofocusSetObjectiveParamsRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        request.setObjective(objective);
        request.setParameters(parameters);
        return Call.callAsync("autofocus/set_objective_params", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets the parameters for the autofocus objective.
     * Note that the method temporarily switches current objective to set the parameters.
     * @param objective The objective (numbered from 1) to set the parameters for.
     * If your microscope has only one objective, use value of 1.
     * @param parameters The parameters for the autofocus objective.
     */
    public void setObjectiveParameters(
        int objective,
        NamedParameter[] parameters) {
        try {
            setObjectiveParametersAsync(objective, parameters).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns the parameters for the autofocus objective.
     * @param objective The objective (numbered from 1) to get the parameters for.
     * If your microscope has only one objective, use value of 1.
     * Note that the method temporarily switches current objective to get the parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * The parameters for the autofocus objective.
     */
    public CompletableFuture<NamedParameter[]> getObjectiveParametersAsync(
        int objective) {
        zaber.motion.requests.AutofocusGetObjectiveParamsRequest request =
            new zaber.motion.requests.AutofocusGetObjectiveParamsRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        request.setObjective(objective);
        CompletableFuture<zaber.motion.requests.AutofocusGetObjectiveParamsResponse> response = Call.callAsync(
            "autofocus/get_objective_params",
            request,
            zaber.motion.requests.AutofocusGetObjectiveParamsResponse.parser());
        return response
            .thenApply(r -> r.getParameters());
    }

    /**
     * Returns the parameters for the autofocus objective.
     * @param objective The objective (numbered from 1) to get the parameters for.
     * If your microscope has only one objective, use value of 1.
     * Note that the method temporarily switches current objective to get the parameters.
     * @return The parameters for the autofocus objective.
     */
    public NamedParameter[] getObjectiveParameters(
        int objective) {
        try {
            return getObjectiveParametersAsync(objective).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a string that represents the autofocus.
     * @return A string that represents the autofocus.
     */
    public String toString() {
        zaber.motion.requests.EmptyAutofocusRequest request =
            new zaber.motion.requests.EmptyAutofocusRequest();
        request.setProviderId(getProviderId());
        request.setInterfaceId(getFocusAxis().getDevice().getConnection().getInterfaceId());
        request.setFocusAddress(getFocusAxis().getDevice().getDeviceAddress());
        request.setFocusAxis(getFocusAxis().getAxisNumber());
        request.setTurretAddress(objectiveTurret != null ? objectiveTurret.getDeviceAddress() : 0);
        zaber.motion.requests.StringResponse response = Call.callSync(
            "autofocus/to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
