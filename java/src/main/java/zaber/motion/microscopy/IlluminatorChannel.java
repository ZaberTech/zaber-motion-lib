// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion.microscopy;

import zaber.motion.ascii.Axis;
import zaber.motion.ascii.AxisSettings;
import zaber.motion.ascii.AxisStorage;
import zaber.motion.ascii.SetStateAxisResponse;
import zaber.motion.ascii.Warnings;
import zaber.motion.ascii.Response;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.FirmwareVersion;
import zaber.motion.gateway.Call;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Use to control a channel (LED lamp) on an illuminator.
 * Requires at least Firmware 7.09.
 */
public class IlluminatorChannel {
    private Illuminator illuminator;

    /**
     * @return Illuminator of this channel.
     */
    public Illuminator getIlluminator() {
        return this.illuminator;
    }

    private int channelNumber;

    /**
     * @return The channel number identifies the channel on the illuminator.
     */
    public int getChannelNumber() {
        return this.channelNumber;
    }

    private Axis axis;

    private AxisSettings settings;

    /**
     * @return Settings and properties of this channel.
     */
    public AxisSettings getSettings() {
        return this.settings;
    }

    private AxisStorage storage;

    /**
     * @return Key-value storage of this channel.
     */
    public AxisStorage getStorage() {
        return this.storage;
    }

    private Warnings warnings;

    /**
     * @return Warnings and faults of this channel.
     */
    public Warnings getWarnings() {
        return this.warnings;
    }

    public IlluminatorChannel(
        Illuminator illuminator, int channelNumber) {
        this.illuminator = illuminator;
        this.channelNumber = channelNumber;
        this.axis = new Axis(illuminator.getDevice(), channelNumber);
        this.settings = new AxisSettings(this.axis);
        this.storage = new AxisStorage(this.axis);
        this.warnings = new Warnings(illuminator.getDevice(), channelNumber);
    }

    /**
     * Turns this channel on.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> onAsync() {
        zaber.motion.requests.ChannelOn request =
            new zaber.motion.requests.ChannelOn();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setOn(true);
        return Call.callAsync("illuminator/on", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Turns this channel on.
     */
    public void on() {
        try {
            onAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Turns this channel off.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> offAsync() {
        zaber.motion.requests.ChannelOn request =
            new zaber.motion.requests.ChannelOn();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setOn(false);
        return Call.callAsync("illuminator/on", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Turns this channel off.
     */
    public void off() {
        try {
            offAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Turns this channel on or off.
     * @param on True to turn channel on, false to turn it off.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setOnAsync(
        boolean on) {
        zaber.motion.requests.ChannelOn request =
            new zaber.motion.requests.ChannelOn();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setOn(on);
        return Call.callAsync("illuminator/on", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Turns this channel on or off.
     * @param on True to turn channel on, false to turn it off.
     */
    public void setOn(
        boolean on) {
        try {
            setOnAsync(on).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if this channel is on.
     * @return A CompletableFuture that can be completed to get the result:
     * True if channel is on, false otherwise.
     */
    public CompletableFuture<Boolean> isOnAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        CompletableFuture<zaber.motion.requests.BoolResponse> response = Call.callAsync(
            "illuminator/is_on",
            request,
            zaber.motion.requests.BoolResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Checks if this channel is on.
     * @return True if channel is on, false otherwise.
     */
    public boolean isOn() {
        try {
            return isOnAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sets channel intensity as a fraction of the maximum flux.
     * @param intensity Fraction of intensity to set (between 0 and 1).
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> setIntensityAsync(
        double intensity) {
        zaber.motion.requests.ChannelSetIntensity request =
            new zaber.motion.requests.ChannelSetIntensity();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setIntensity(intensity);
        return Call.callAsync("illuminator/set_intensity", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sets channel intensity as a fraction of the maximum flux.
     * @param intensity Fraction of intensity to set (between 0 and 1).
     */
    public void setIntensity(
        double intensity) {
        try {
            setIntensityAsync(intensity).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Gets the current intensity of this channel.
     * @return A CompletableFuture that can be completed to get the result:
     * Current intensity as fraction of maximum flux.
     */
    public CompletableFuture<Double> getIntensityAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        CompletableFuture<zaber.motion.requests.DoubleResponse> response = Call.callAsync(
            "illuminator/get_intensity",
            request,
            zaber.motion.requests.DoubleResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Gets the current intensity of this channel.
     * @return Current intensity as fraction of maximum flux.
     */
    public double getIntensity() {
        try {
            return getIntensityAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<Response> response = Call.callAsync(
            "interface/generic_command",
            request,
            Response.parser());
        return response;
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command,
        boolean checkErrors) {
        return genericCommandAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * A response to the command.
     */
    public CompletableFuture<Response> genericCommandAsync(
        String command) {
        return genericCommandAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when the device rejects the command.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command,
        boolean checkErrors) {
        return genericCommand(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this channel.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A response to the command.
     */
    public Response genericCommand(
        String command) {
        return genericCommand(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors,
        int timeout) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setCommand(command);
        request.setCheckErrors(checkErrors);
        request.setTimeout(timeout);
        CompletableFuture<zaber.motion.requests.GenericCommandResponseCollection> response = Call.callAsync(
            "interface/generic_command_multi_response",
            request,
            zaber.motion.requests.GenericCommandResponseCollection.parser());
        return response
            .thenApply(r -> r.getResponses());
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponseAsync(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to get the result:
     * All responses to the command.
     */
    public CompletableFuture<Response[]> genericCommandMultiResponseAsync(
        String command) {
        return genericCommandMultiResponseAsync(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @param timeout The timeout, in milliseconds, for a device to respond to the command.
     * Overrides the connection default request timeout.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors,
        int timeout) {
        try {
            return genericCommandMultiResponseAsync(command, checkErrors, timeout).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @param checkErrors Controls whether to throw an exception when a device rejects the command.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command,
        boolean checkErrors) {
        return genericCommandMultiResponse(command, checkErrors, 0);
    }

    /**
     * Sends a generic ASCII command to this channel and expects multiple responses.
     * Responses are returned in order of arrival.
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return All responses to the command.
     */
    public Response[] genericCommandMultiResponse(
        String command) {
        return genericCommandMultiResponse(command, true, 0);
    }

    /**
     * Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     * @return A CompletableFuture that can be completed to know when the work is complete.
     */
    public CompletableFuture<Void> genericCommandNoResponseAsync(
        String command) {
        zaber.motion.requests.GenericCommandRequest request =
            new zaber.motion.requests.GenericCommandRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setCommand(command);
        return Call.callAsync("interface/generic_command_no_response", request, null)
            .thenApply(r -> (Void) null);
    }

    /**
     * Sends a generic ASCII command to this channel without expecting a response and without adding a message ID
     * For more information refer to: [ASCII Protocol Manual](https://www.zaber.com/protocol-manual#topic_commands).
     * @param command Command and its parameters.
     */
    public void genericCommandNoResponse(
        String command) {
        try {
            genericCommandNoResponseAsync(command).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns a serialization of the current channel state that can be saved and reapplied.
     * @return A CompletableFuture that can be completed to get the result:
     * A serialization of the current state of the channel.
     */
    public CompletableFuture<String> getStateAsync() {
        zaber.motion.requests.AxisEmptyRequest request =
            new zaber.motion.requests.AxisEmptyRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        CompletableFuture<zaber.motion.requests.StringResponse> response = Call.callAsync(
            "device/get_state",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response
            .thenApply(r -> r.getValue());
    }

    /**
     * Returns a serialization of the current channel state that can be saved and reapplied.
     * @return A serialization of the current state of the channel.
     */
    public String getState() {
        try {
            return getStateAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Applies a saved state to this channel.
     * @param state The state object to apply to this channel.
     * @return A CompletableFuture that can be completed to get the result:
     * Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public CompletableFuture<SetStateAxisResponse> setStateAsync(
        String state) {
        zaber.motion.requests.SetStateRequest request =
            new zaber.motion.requests.SetStateRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setState(state);
        CompletableFuture<SetStateAxisResponse> response = Call.callAsync(
            "device/set_axis_state",
            request,
            SetStateAxisResponse.parser());
        return response;
    }

    /**
     * Applies a saved state to this channel.
     * @param state The state object to apply to this channel.
     * @return Reports of any issues that were handled, but caused the state to not be exactly restored.
     */
    public SetStateAxisResponse setState(
        String state) {
        try {
            return setStateAsync(state).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this channel.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this channel.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state,
        FirmwareVersion firmwareVersion) {
        zaber.motion.requests.CanSetStateRequest request =
            new zaber.motion.requests.CanSetStateRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setState(state);
        request.setFirmwareVersion(firmwareVersion);
        CompletableFuture<zaber.motion.requests.CanSetStateAxisResponse> response = Call.callAsync(
            "device/can_set_axis_state",
            request,
            zaber.motion.requests.CanSetStateAxisResponse.parser());
        return response
            .thenApply(r -> r.getError());
    }

    /**
     * Checks if a state can be applied to this channel.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return A CompletableFuture that can be completed to get the result:
     * An explanation of why this state cannot be set to this channel.
     */
    public CompletableFuture<String> canSetStateAsync(
        String state) {
        return canSetStateAsync(state, null);
    }

    /**
     * Checks if a state can be applied to this channel.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @param firmwareVersion The firmware version of the device to apply the state to.
     * Use this to ensure the state will still be compatible after an update.
     * @return An explanation of why this state cannot be set to this channel.
     */
    public String canSetState(
        String state,
        FirmwareVersion firmwareVersion) {
        try {
            return canSetStateAsync(state, firmwareVersion).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Checks if a state can be applied to this channel.
     * This only covers exceptions that can be determined statically such as mismatches of ID or version,
     * the process of applying the state can still fail when running.
     * @param state The state object to check against.
     * @return An explanation of why this state cannot be set to this channel.
     */
    public String canSetState(
        String state) {
        return canSetState(state, null);
    }

    /**
     * Returns a string that represents the channel.
     * @return A string that represents the channel.
     */
    public String toString() {
        zaber.motion.requests.AxisToStringRequest request =
            new zaber.motion.requests.AxisToStringRequest();
        request.setInterfaceId(getIlluminator().getDevice().getConnection().getInterfaceId());
        request.setDevice(getIlluminator().getDevice().getDeviceAddress());
        request.setAxis(getChannelNumber());
        request.setTypeOverride("Channel");
        zaber.motion.requests.StringResponse response = Call.callSync(
            "device/axis_to_string",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
