package zaber.motion.gateway;

import zaber.motion.dto.Message;

public final class SdkEvent {

    private String event;

    private Message eventData;

    public SdkEvent(String event, Message eventData) {
        this.event = event;
        this.eventData = eventData;
    }

    public String getEventName() {
        return this.event;
    }

    public Message getEventData() {
        return this.eventData;
    }
}
