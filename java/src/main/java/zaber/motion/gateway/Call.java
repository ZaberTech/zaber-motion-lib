package zaber.motion.gateway;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import com.sun.jna.Pointer;
import zaber.motion.exceptions.MotionLibException;
import zaber.motion.gateway.GoLibraryLoader.NativeLibrary;
import zaber.motion.exceptions.LibraryIntegrationException;
import zaber.motion.requests.GatewayRequest;
import zaber.motion.requests.GatewayResponse;
import zaber.motion.requests.ResponseType;
import zaber.motion.exceptions.ExceptionConverter;
import zaber.motion.dto.SerializationException;
import zaber.motion.dto.Message;
import zaber.motion.dto.Parser;

public final class Call {

    private Call() {
    }

    private static Set<NativeLibrary.Func> garbageCollectorProtector = Collections.synchronizedSet(new HashSet<>());

    public static <T extends Message> T callSync(String request, Message requestData,
                                                 Parser<T> parser) throws MotionLibException {
        GatewayRequest genRequest = new GatewayRequest(request);

        List<byte[]> messages = new ArrayList<byte[]>();
        try {
            messages.add(genRequest.toByteArray());
            if (requestData != null) {
                messages.add(requestData.toByteArray());
            }
        } catch (SerializationException e) {
            throw new LibraryIntegrationException("Cannot serialize request", e);
        }

        byte[] data = Serialization.serialize(messages);

        List<byte[]> rawResponse = new ArrayList<>(1);

        NativeLibrary.Func callbackSync = new NativeLibrary.Func() {
            @Override
            @SuppressWarnings("checkstyle:magicnumber")
            public void invoke(Pointer response) {
                byte[] sizeArray = response.getByteArray(0, Serialization.SIZE_TYPE_SIZE);
                int size = Serialization.getSizeFromByteArrayLE(sizeArray, 0);
                rawResponse.add(response.getByteArray(0, size));
            }
        };

        int result = NativeLibrary.INSTANCE.zml_call(data, (long) 0, callbackSync, (char) 0);

        if (result != 0) {
            throw new LibraryIntegrationException("Invalid result code: " + result);
        }

        List<byte[]> responses = Serialization.deserialize(rawResponse.get(0));

        return parseResponse(responses, parser);
    }

    public static <T extends Message> CompletableFuture<T> callAsync(String request, Message requestData,
                                                                     Parser<T> parser) throws MotionLibException {
        GatewayRequest genRequest = new GatewayRequest(request);

        List<byte[]> messages = new ArrayList<byte[]>();
        try {
            messages.add(genRequest.toByteArray());
            if (requestData != null) {
                messages.add(requestData.toByteArray());
            }
        } catch (SerializationException e) {
            throw new LibraryIntegrationException("Cannot serialize request", e);
        }

        byte[] data = Serialization.serialize(messages);

        CompletableFuture<byte[]> future = new CompletableFuture<>();

        NativeLibrary.Func callbackAsync = new NativeLibrary.Func() {
            @Override
            @SuppressWarnings("checkstyle:magicnumber")
            public void invoke(Pointer response) {
                byte[] sizeArray = response.getByteArray(0, Serialization.SIZE_TYPE_SIZE);
                int size = Serialization.getSizeFromByteArrayLE(sizeArray, 0);
                future.complete(response.getByteArray(0, size));
            }
        };

        garbageCollectorProtector.add(callbackAsync);

        int result = NativeLibrary.INSTANCE.zml_call(data, (long) 0, callbackAsync, (char) 1);

        if (result != 0) {
            throw new LibraryIntegrationException("Invalid result code: " + result);
        }

        return future.thenApply(rawResponse -> {
            garbageCollectorProtector.remove(callbackAsync);

            List<byte[]> rawResponses = Serialization.deserialize(rawResponse);
            try {
                return parseResponse(rawResponses, parser);
            } catch (MotionLibException e) {
                throw new CompletionException(e);
            }
        });
    }

    private static <T extends Message> T parseResponse(List<byte[]> messages, Parser<T> parser)
                                                       throws MotionLibException {
        try {
            GatewayResponse response = GatewayResponse.fromByteArray(messages.get(0));

            if (response.getResponse() != ResponseType.OK) {
                if (messages.size() > 1) {
                    throw ExceptionConverter.convert(response.getErrorType(), response.getErrorMessage(), messages.get(1));
                }
                throw ExceptionConverter.convert(response.getErrorType(), response.getErrorMessage());
            }
            if (parser != null && messages.size() == 1) {
                throw new LibraryIntegrationException("No response from library");
            }
            if (parser == null && messages.size() > 1) {
                throw new LibraryIntegrationException("Response from library ignored");
            }
            if (parser != null) {
                return parser.fromByteArray(messages.get(1));
            }
            return null;
        } catch (SerializationException e) {
            throw new LibraryIntegrationException("Cannot parse response from library", e);
        }
    }

}
