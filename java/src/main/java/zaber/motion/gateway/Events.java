package zaber.motion.gateway;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.HashMap;

import com.sun.jna.Pointer;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import zaber.motion.exceptions.LibraryIntegrationException;
import zaber.motion.gateway.GoLibraryLoader.NativeLibrary;
import zaber.motion.requests.AlertEventWrapper;
import zaber.motion.requests.BinaryReplyOnlyEventWrapper;
import zaber.motion.requests.DisconnectedEvent;
import zaber.motion.requests.GatewayEvent;
import zaber.motion.requests.TestEvent;
import zaber.motion.requests.UnknownBinaryResponseEventWrapper;
import zaber.motion.requests.UnknownResponseEventWrapper;
import zaber.motion.dto.AnyParser;
import zaber.motion.dto.Message;
import zaber.motion.dto.SerializationException;

class DaemonThreadFactory implements ThreadFactory {
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.setName("ZaberMotionEventExecutorThread");
        return thread;
    }
}

public final class Events {

    private Events() {
    }

    private static Map<String, AnyParser> eventParsers = initializeParserMap();

    private static Subject<SdkEvent> eventObservable = PublishSubject.create();

    private static Executor executor = Executors.newSingleThreadExecutor(new DaemonThreadFactory());

    private static NativeLibrary.Func handleEvent = new NativeLibrary.Func() {

        @Override
        @SuppressWarnings("checkstyle:magicnumber")
        public void invoke(Pointer response) {
            int size = Serialization.getSizeFromByteArrayLE(response.getByteArray(0, Serialization.SIZE_TYPE_SIZE), 0);
            byte[] responseArray = response.getByteArray(0, size);

            executor.execute(() -> processEvent(responseArray));
        }
    };

    public static void setEventHandler(NativeLibrary library) {
        library.zml_setEventHandler((long) 0, handleEvent);
    }

    private static void processEvent(byte[] rawResponse) {
        List<byte[]> messages = Serialization.deserialize(rawResponse);

        GatewayEvent event;
        try {
            event = GatewayEvent.fromByteArray(messages.get(0));
        } catch (SerializationException e) {
            throw new LibraryIntegrationException("Cannot parse event from library", e);
        }

        String eventName = event.getEvent();
        AnyParser eventParser = eventParsers.get(eventName);

        Message eventDataProto;
        try {
            eventDataProto = eventParser.fromByteArray(messages.get(1));
        } catch (SerializationException e) {
            throw new LibraryIntegrationException("Cannot parse event from library", e);
        }

        SdkEvent eventObject = new SdkEvent(event.getEvent(), eventDataProto);
        eventObservable.onNext(eventObject);
    }

    private static Map<String, AnyParser> initializeParserMap() {
        Map<String, AnyParser> parsers = new HashMap<>();
        parsers.put("test/event", TestEvent.parser());
        parsers.put("interface/unknown_response", UnknownResponseEventWrapper.parser());
        parsers.put("binary/interface/unknown_response", UnknownBinaryResponseEventWrapper.parser());
        parsers.put("interface/alert", AlertEventWrapper.parser());
        parsers.put("binary/interface/reply_only", BinaryReplyOnlyEventWrapper.parser());
        parsers.put("interface/disconnected", DisconnectedEvent.parser());
        return parsers;
    }

    public static Observable<SdkEvent> getEventObservable() {
        return eventObservable;
    }

}
