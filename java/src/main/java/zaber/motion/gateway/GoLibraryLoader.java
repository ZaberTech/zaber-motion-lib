package zaber.motion.gateway;

import zaber.jne.JNE;
import zaber.jne.OperatingSystem;
import zaber.jne.HardwareArchitecture;

import java.io.File;
import java.io.IOException;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import zaber.motion.exceptions.LibraryIntegrationException;

public final class GoLibraryLoader {

    private GoLibraryLoader() {
    }

    private enum OS {
        WINDOWS, LINUX, DARWIN
    }

    private enum Platform {
        X86, AMD64, ARM, ARM64
    }

    private static OS os = findOS();

    private static Platform platform = findPlatform();

    private static String nativeLibName = getLibraryName();

    public interface NativeLibrary extends Library {
        interface Func extends Callback {
            void invoke(Pointer response);
        }

        //CHECKSTYLE:OFF

        int zml_call(byte[] message, long tag, Func callback, char async);

        void zml_setEventHandler(long tag, Func callback);

        //CHECKSTYLE:ON

        NativeLibrary INSTANCE = GoLibraryLoader.loadLibrary();
    }

    public static NativeLibrary loadLibrary() {
        try {
            File nativeLib = JNE.find(
                nativeLibName, nativeLibName,
                null, OperatingSystem.ANY, HardwareArchitecture.ANY
            );
            if (nativeLib == null) {
                throw new LibraryIntegrationException("Cannot find dynamic library file: " + nativeLibName);
            }
            NativeLibrary lib = (NativeLibrary) Native.loadLibrary(nativeLib.getAbsolutePath(), NativeLibrary.class);

            Events.setEventHandler(lib);

            return lib;
        } catch (IOException e) {
            throw new LibraryIntegrationException("Cannot find or extract dynamic library file: " + nativeLibName, e);
        }
    }

    private static String getLibraryName() {
        String name = "zaber-motion-core";

        if (os == OS.WINDOWS && platform == Platform.X86) {
            return name.concat("-windows-386.dll");
        } else if (os == OS.WINDOWS && platform == Platform.AMD64) {
            return name.concat("-windows-amd64.dll");
        } else if (os == OS.LINUX && platform == Platform.X86) {
            return name.concat("-linux-386.so");
        } else if (os == OS.LINUX && platform == Platform.AMD64) {
            return name.concat("-linux-amd64.so");
        } else if (os == OS.LINUX && platform == Platform.ARM) {
            return name.concat("-linux-arm.so");
        } else if (os == OS.LINUX && platform == Platform.ARM64) {
            return name.concat("-linux-arm64.so");
        } else if (os == OS.DARWIN) {
            return name.concat("-darwin-uni.dylib");
        }
        throw new LibraryIntegrationException(String.format("System is not supported: %s,%s", platform, os));
    }

    private static OS findOS() {
        String osName = System.getProperty("os.name").toLowerCase();

        if (osName.contains("mac")) {
            return OS.DARWIN;
        } else if (osName.contains("win")) {
            return OS.WINDOWS;
        } else if (osName.contains("nix") || osName.contains("nux") || osName.contains("aix")) {
            return OS.LINUX;
        }
        throw new LibraryIntegrationException("OS is not supported: " + osName);
    }

    private static Platform findPlatform() {
        String arch = System.getProperty("os.arch").toLowerCase();

        if (arch.contains("amd64") || arch.contains("x86_64")) {
            return Platform.AMD64;
        } else if (arch.contains("i386") || arch.contains("x86")) {
            return Platform.X86;
        } else if (arch.contains("aarch64") || arch.contains("arm64")) {
            return Platform.ARM64;
        } else if (arch.contains("arm")) {
            return Platform.ARM;
        }
        throw new LibraryIntegrationException("Architecture is not supported: " + arch);
    }
}
