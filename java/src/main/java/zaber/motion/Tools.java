// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion;

import zaber.motion.gateway.Call;
import zaber.motion.exceptions.MotionLibException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Class providing various utility functions.
 */
public final class Tools {
    private Tools() {
    }

    /**
     * Lists all serial ports on the computer.
     * @return A CompletableFuture that can be completed to get the result:
     * Array of serial port names.
     */
    public static CompletableFuture<String[]> listSerialPortsAsync() {
        zaber.motion.requests.EmptyRequest request =
            new zaber.motion.requests.EmptyRequest();
        CompletableFuture<zaber.motion.requests.ToolsListSerialPortsResponse> response = Call.callAsync(
            "tools/list_serial_ports",
            request,
            zaber.motion.requests.ToolsListSerialPortsResponse.parser());
        return response
            .thenApply(r -> r.getPorts());
    }

    /**
     * Lists all serial ports on the computer.
     * @return Array of serial port names.
     */
    public static String[] listSerialPorts() {
        try {
            return listSerialPortsAsync().get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MotionLibException) {
                throw (MotionLibException) e.getCause();
            } else {
                throw new MotionLibException(e.getCause());
            }
        } catch (InterruptedException e) {
            throw new MotionLibException(e);
        }
    }

    /**
     * Returns path of message router named pipe on Windows
     * or file path of unix domain socket on UNIX.
     * @return Path of message router's named pipe or unix domain socket.
     */
    public static String getMessageRouterPipePath() {
        zaber.motion.requests.EmptyRequest request =
            new zaber.motion.requests.EmptyRequest();
        zaber.motion.requests.StringResponse response = Call.callSync(
            "tools/get_message_router_pipe",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


    /**
     * Returns the path for communicating with a local device database service.
     * This will be a named pipe on Windows and the file path of a unix domain socket on UNIX.
     * @return Path of database service's named pipe or unix domain socket.
     */
    public static String getDbServicePipePath() {
        zaber.motion.requests.EmptyRequest request =
            new zaber.motion.requests.EmptyRequest();
        zaber.motion.requests.StringResponse response = Call.callSync(
            "tools/get_db_service_pipe",
            request,
            zaber.motion.requests.StringResponse.parser());
        return response.getValue();
    }


}
