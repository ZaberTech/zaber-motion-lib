package zaber.motion.dto;

public interface Parser<TMessage extends Message> extends AnyParser {
    TMessage fromByteArray(byte[] data);
}
