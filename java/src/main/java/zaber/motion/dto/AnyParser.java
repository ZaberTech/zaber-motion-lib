package zaber.motion.dto;

public interface AnyParser {
    Message fromByteArray(byte[] data);
}
