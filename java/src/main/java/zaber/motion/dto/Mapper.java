package zaber.motion.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.undercouch.bson4jackson.BsonFactory;

public final class Mapper {
    static final ObjectMapper MAPPER = new ObjectMapper(new BsonFactory());

    public static ObjectMapper getDefault() {
        return MAPPER;
    }

    private Mapper() {
    }
}
