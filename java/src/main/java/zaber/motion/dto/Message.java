package zaber.motion.dto;

public interface Message {
    byte[] toByteArray();
}
