/* This file is generated. Do not modify by hand. */

package zaber.motion;

import java.util.Objects;

import zaber.motion.dto.Mapper;

/**
 * Class representing version of firmware in the controller.
 */
public final class FirmwareVersion implements zaber.motion.dto.Message {

    private int major;

    /**
     * Major version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("major")
    public void setMajor(int major) {
        this.major = major;
    }

    /**
     * Major version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("major")
    public int getMajor() {
        return this.major;
    }

    /**
     * Major version number.
     */
    public FirmwareVersion withMajor(int aMajor) {
        this.setMajor(aMajor);
        return this;
    }

    private int minor;

    /**
     * Minor version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("minor")
    public void setMinor(int minor) {
        this.minor = minor;
    }

    /**
     * Minor version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("minor")
    public int getMinor() {
        return this.minor;
    }

    /**
     * Minor version number.
     */
    public FirmwareVersion withMinor(int aMinor) {
        this.setMinor(aMinor);
        return this;
    }

    private int build;

    /**
     * Build version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("build")
    public void setBuild(int build) {
        this.build = build;
    }

    /**
     * Build version number.
     */
    @com.fasterxml.jackson.annotation.JsonProperty("build")
    public int getBuild() {
        return this.build;
    }

    /**
     * Build version number.
     */
    public FirmwareVersion withBuild(int aBuild) {
        this.setBuild(aBuild);
        return this;
    }

    /**
     * Empty constructor.
     */
    public FirmwareVersion() {
    }

    /**
     * Constructor with all properties.
     */
    public FirmwareVersion(
        int major,
        int minor,
        int build
    ) {
        this.major = major;
        this.minor = minor;
        this.build = build;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        FirmwareVersion other = (FirmwareVersion) obj;

        return (
            EqualityUtility.equals(major, other.major)
            && EqualityUtility.equals(minor, other.minor)
            && EqualityUtility.equals(build, other.build)
            );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            EqualityUtility.generateHashCode(major),
            EqualityUtility.generateHashCode(minor),
            EqualityUtility.generateHashCode(build)
        );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FirmwareVersion { ");
        sb.append("major: ");
        sb.append(this.major);
        sb.append(", ");
        sb.append("minor: ");
        sb.append(this.minor);
        sb.append(", ");
        sb.append("build: ");
        sb.append(this.build);
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public byte[] toByteArray() {
        try {
            return Mapper.getDefault().writeValueAsBytes(this);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    public static FirmwareVersion fromByteArray(byte[] data) {
        try {
            return Mapper.getDefault().readValue(data, FirmwareVersion.class);
        } catch (Exception e) {
            throw new zaber.motion.dto.SerializationException(e);
        }
    }

    static final zaber.motion.dto.Parser<FirmwareVersion> PARSER =
        new zaber.motion.dto.Parser<FirmwareVersion>() {
            @Override
            public FirmwareVersion fromByteArray(byte[] data) {
                return FirmwareVersion.fromByteArray(data);
            }
        };

    public static zaber.motion.dto.Parser<FirmwareVersion> parser() {
        return PARSER;
    }

}
