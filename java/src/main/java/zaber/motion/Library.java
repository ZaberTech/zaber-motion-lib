// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion;

import zaber.motion.gateway.Call;

/**
 * Access class to general library information and configuration.
 */
public final class Library {
    static {
        Library.checkVersion();
    }

    private Library() {
    }

    /**
     * Sets library logging output.
     * @param mode Logging output mode.
     * @param filePath Path of the file to open.
     */
    public static void setLogOutput(
        LogOutputMode mode,
        String filePath) {
        zaber.motion.requests.SetLogOutputRequest request =
            new zaber.motion.requests.SetLogOutputRequest();
        request.setMode(mode);
        request.setFilePath(filePath);
        Call.callSync("logging/set_output", request, null);
    }

    /**
     * Sets library logging output.
     * @param mode Logging output mode.
     */
    public static void setLogOutput(
        LogOutputMode mode) {
        setLogOutput(mode, null);
    }


    /**
     * Sets source of Device DB data. Allows selection of a web service or a local file.
     * @param sourceType Source type.
     * @param urlOrFilePath URL of the web service or path to the local file.
     * Leave empty for the default URL of Zaber web service.
     */
    public static void setDeviceDbSource(
        DeviceDbSourceType sourceType,
        String urlOrFilePath) {
        zaber.motion.requests.SetDeviceDbSourceRequest request =
            new zaber.motion.requests.SetDeviceDbSourceRequest();
        request.setSourceType(sourceType);
        request.setUrlOrFilePath(urlOrFilePath);
        Call.callSync("device_db/set_source", request, null);
    }

    /**
     * Sets source of Device DB data. Allows selection of a web service or a local file.
     * @param sourceType Source type.
     */
    public static void setDeviceDbSource(
        DeviceDbSourceType sourceType) {
        setDeviceDbSource(sourceType, null);
    }


    /**
     * Enables Device DB store.
     * The store uses filesystem to save information obtained from the Device DB.
     * The stored data are later used instead of the Device DB.
     * @param storeLocation Specifies relative or absolute path of the folder used by the store.
     * If left empty defaults to a folder in user home directory.
     * Must be accessible by the process.
     */
    public static void enableDeviceDbStore(
        String storeLocation) {
        zaber.motion.requests.ToggleDeviceDbStoreRequest request =
            new zaber.motion.requests.ToggleDeviceDbStoreRequest();
        request.setToggleOn(true);
        request.setStoreLocation(storeLocation);
        Call.callSync("device_db/toggle_store", request, null);
    }

    /**
     * Enables Device DB store.
     * The store uses filesystem to save information obtained from the Device DB.
     * The stored data are later used instead of the Device DB.
     */
    public static void enableDeviceDbStore() {
        enableDeviceDbStore(null);
    }


    /**
     * Disables Device DB store.
     */
    public static void disableDeviceDbStore() {
        zaber.motion.requests.ToggleDeviceDbStoreRequest request =
            new zaber.motion.requests.ToggleDeviceDbStoreRequest();
        Call.callSync("device_db/toggle_store", request, null);
    }


    /**
     * Disables certain customer checks (like FF flag).
     * @param mode Whether to turn internal mode on or off.
     */
    public static void setInternalMode(
        boolean mode) {
        zaber.motion.requests.SetInternalModeRequest request =
            new zaber.motion.requests.SetInternalModeRequest();
        request.setMode(mode);
        Call.callSync("library/set_internal_mode", request, null);
    }


    /**
     * Sets the period between polling for IDLE during movements.
     * Caution: Setting the period too low may cause performance issues.
     * @param period Period in milliseconds.
     * Negative value restores the default period.
     */
    public static void setIdlePollingPeriod(
        int period) {
        zaber.motion.requests.IntRequest request =
            new zaber.motion.requests.IntRequest();
        request.setValue(period);
        Call.callSync("library/set_idle_polling_period", request, null);
    }


    /**
     * Throws an error if the version of the loaded shared library does not match the caller's version.
     */
    public static void checkVersion() {
        zaber.motion.requests.CheckVersionRequest request =
            new zaber.motion.requests.CheckVersionRequest();
        request.setHost("java");
        request.setVersion("7.5.0");
        Call.callSync("library/check_version", request, null);
    }


}
