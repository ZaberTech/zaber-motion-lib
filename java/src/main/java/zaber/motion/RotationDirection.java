/* This file is generated. Do not modify by hand. */

package zaber.motion;

/**
 * Direction of rotation.
 */
public enum RotationDirection {

    CLOCKWISE(0),

    COUNTERCLOCKWISE(1),

    CW(0),

    CCW(1);

    private int value;

    RotationDirection(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static RotationDirection valueOf(int argValue) {
        for (RotationDirection value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
