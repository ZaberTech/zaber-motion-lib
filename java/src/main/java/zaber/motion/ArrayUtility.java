package zaber.motion;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public final class ArrayUtility {
    private ArrayUtility() {
    }

    public static boolean[] toPrimitiveArray(Boolean[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new boolean[0];
        }
        boolean[] result = new boolean[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static double[] toPrimitiveArray(Double[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new double[0];
        }
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static int[] toPrimitiveArray(Integer[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new int[0];
        }
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static Boolean[] toObjectArray(boolean[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new Boolean[0];
        }
        Boolean[] result = new Boolean[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static Integer[] toObjectArray(int[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new Integer[0];
        }
        Integer[] result = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static Double[] toObjectArray(double[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new Double[0];
        }
        Double[] result = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static <T> List<T> asList(T[] array) {
        if (array == null) {
            return new ArrayList<T>();
        }
        return Arrays.asList(array);
    }

    public static List<Integer> asList(int[] array) {
        if (array == null) {
            return new ArrayList<Integer>();
        }
        List<Integer> result = new ArrayList<Integer>(array.length);
        for (int i = 0; i < array.length; i++) {
            result.add(array[i]);
        }
        return result;
    }

    public static <T> int[] arrayToInt(T[] array, Function<T, Integer> callback) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = callback.apply(array[i]);
        }
        return result;
    }

    public static <T extends Object> T[] arrayFromInt(IntFunction<T[]> newArray, int[] array, Function<Integer, T> callback) {
        T[] result = newArray.apply(array.length);
        for (int i = 0; i < array.length; i++) {
            result[i] = callback.apply(array[i]);
        }
        return result;
    }
}
