// ===== THIS FILE IS GENERATED FROM A TEMPLATE ===== //
// ============== DO NOT EDIT DIRECTLY ============== //

package zaber.motion;

import zaber.motion.gateway.Call;

/**
 * Helper for working with units of measure.
 */
public final class UnitTable {
    private UnitTable() {
    }

    /**
     * Gets the standard symbol associated with a given unit.
     * @param unit Unit of measure.
     * @return Symbols corresponding to the given unit. Throws NoValueForKey if no symbol is defined.
     */
    public static String getSymbol(
        Units unit) {
        zaber.motion.requests.UnitGetSymbolRequest request =
            new zaber.motion.requests.UnitGetSymbolRequest();
        request.setUnit(unit);
        zaber.motion.requests.UnitGetSymbolResponse response = Call.callSync(
            "units/get_symbol",
            request,
            zaber.motion.requests.UnitGetSymbolResponse.parser());
        return response.getSymbol();
    }


    /**
     * Gets the unit enum value associated with a standard symbol.
     * Note not all units can be retrieved this way.
     * @param symbol Symbol to look up.
     * @return The unit enum value with the given symbols. Throws NoValueForKey if the symbol is not supported for lookup.
     */
    public static Units getUnit(
        String symbol) {
        zaber.motion.requests.UnitGetEnumRequest request =
            new zaber.motion.requests.UnitGetEnumRequest();
        request.setSymbol(symbol);
        zaber.motion.requests.UnitGetEnumResponse response = Call.callSync(
            "units/get_enum",
            request,
            zaber.motion.requests.UnitGetEnumResponse.parser());
        return response.getUnit();
    }


}
