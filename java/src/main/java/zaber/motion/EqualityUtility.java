package zaber.motion;

import java.util.Arrays;
import java.util.Objects;

public final class EqualityUtility {
    private EqualityUtility() {
    }

    public static boolean equals(Object objA, Object objB) {
        if (objA == null || objB == null) {
            return objA == objB;
        }

        if (objA.getClass().isArray() && objB.getClass().isArray()) {
            return Arrays.deepEquals((Object[]) objA, (Object[]) objB);
        } else {
            return objA.equals(objB);
        }
    }

    public static int generateHashCode(Object obj) {
        if (obj == null) {
            return 0;
        }

        if (obj.getClass().isArray()) {
            return Arrays.hashCode((Object[]) obj);
        }

        return Objects.hash(obj);
    }
}
