/* This file is generated. Do not modify by hand. */

package zaber.motion;

/**
 * Type of source of Device DB data.
 */
public enum DeviceDbSourceType {

    WEB_SERVICE(0),

    FILE(1);

    private int value;

    DeviceDbSourceType(int value) {
        this.value = value;
    }

    @com.fasterxml.jackson.annotation.JsonValue
    public int getValue() {
        return value;
    }

    public static DeviceDbSourceType valueOf(int argValue) {
        for (DeviceDbSourceType value : values()) {
            if (value.value == argValue) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid value: %d", argValue));
    }
}
