package zaber;

import static org.junit.Assert.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import zaber.motion.exceptions.InvalidPacketException;
import zaber.motion.exceptions.RequestTimeoutException;
import zaber.motion.gateway.Call;
import zaber.motion.gateway.Events;
import zaber.motion.gateway.SdkEvent;
import zaber.motion.requests.TestEvent;
import zaber.motion.requests.TestRequest;
import zaber.motion.requests.TestResponse;
import zaber.motion.requests.TestResponseLong;

public class GatewayTest {
    @Test
    public void testAsyncRequestWithResponse() throws Exception {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");

        TestResponse result = Call.callAsync("test/request", testRequest, TestResponse.parser()).get();
        assertEquals("Hello...", result.getDataPong());
    }

    @Test
    public void testSyncRequestWithResponse() throws Exception {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");

        TestResponse result = (TestResponse) Call.callSync("test/request", testRequest, TestResponse.parser());
        assertEquals("Hello...", result.getDataPong());
    }

    @Test
    public void testAsyncRequestWithError() throws Exception {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");
        testRequest.setReturnError(true);

        try {
            Call.callAsync("test/request", testRequest, TestResponse.parser()).get();
            fail("Should have thrown request timeout exception.");
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof RequestTimeoutException);
            assertEquals(e.getCause().getMessage(), "Device has not responded in given timeout");
        }
    }

    @Test
    public void testAsyncRequestWithLongResponse() throws Exception {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");

        TestResponseLong result = (TestResponseLong) Call.callAsync("test/request_long", testRequest, TestResponseLong.parser()).get();
        String[] dataPong = result.getDataPong();
        for (int i = 0; i < dataPong.length; i++) {
            assertEquals("Hello..." + i, dataPong[i]);
        }
    }

    @Test
    public void testSyncRequestWithError() {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");
        testRequest.setReturnError(true);

        try {
            Call.callSync("test/request", testRequest, TestResponse.parser());
            fail("Should have thrown request timeout exception.");
        } catch (RequestTimeoutException e) {
            assertEquals(e.getMessage(), "Device has not responded in given timeout");
        }
    }

    @Test
    public void testSyncRequestWithErrorAndErrorData() {
        TestRequest testRequest = new TestRequest();
        testRequest.setDataPing("Hello");
        testRequest.setReturnErrorWithData(true);

        try {
            Call.callSync("test/request", testRequest, TestResponse.parser());
            fail("Should have trown invalid packet exception.");
        } catch (InvalidPacketException e) {
            String expectedPacket = "123";
            String expectedReason = "For test";
            String expectedMessage = String.format("Cannot parse incoming packet: \"%s\" (%s)", expectedPacket, expectedReason);
            assertEquals(expectedMessage, e.getMessage());
            assertEquals(expectedPacket, e.getDetails().getPacket());
            assertEquals(expectedReason, e.getDetails().getReason());
        }
    }

    @Test
    public void testRequestWithoutResponse() throws Exception {
        Call.callAsync("test/request_no_response", null, null).get();
    }

    @Test
    public void testEventEmission() throws Exception {
        CompletableFuture<SdkEvent> future = new CompletableFuture<>();

        Events.getEventObservable().take(1).subscribe((SdkEvent e) -> future.complete(e));

        Call.callAsync("test/emit_event", null, null).get();

        SdkEvent testEvent = future.get();
        assertEquals(testEvent.getEventName(), "test/event");
        assertEquals(((TestEvent) testEvent.getEventData()).getData(), "testing event data");
    }

}
