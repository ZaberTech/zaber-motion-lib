package zaber;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import zaber.motion.FirmwareVersion;
import zaber.motion.ascii.CanSetStateAxisResponse;
import zaber.motion.ascii.CanSetStateDeviceResponse;
import zaber.motion.ascii.DeviceIdentity;

public class EqualityTest {
    @Test
    public void testEquals() throws Exception {
        FirmwareVersion versionA = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionB = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);

        assertEquals(versionA, versionB);

        versionB.setBuild(4);

        assertNotEquals(versionA, versionB);
    }

    @Test
    public void testEqualsMemberClass() throws Exception {
        DeviceIdentity identityA = new DeviceIdentity().withAxisCount(3);
        DeviceIdentity identityB = new DeviceIdentity().withAxisCount(3);
        FirmwareVersion versionA = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionB = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        identityA.setFirmwareVersion(versionA);
        identityB.setFirmwareVersion(versionB);

        assertEquals(identityA, identityB);

        versionB.setBuild(4);

        assertNotEquals(identityA, identityB);
    }

    @Test
    public void testEqualsMemberArray() throws Exception {
        CanSetStateAxisResponse axisResponseA = new CanSetStateAxisResponse().withError("Test Axis");
        CanSetStateAxisResponse axisResponseB = new CanSetStateAxisResponse().withError("Test Axis");

        CanSetStateDeviceResponse deviceResponseA = new CanSetStateDeviceResponse()
                .withError("Test")
                .withAxisErrors(new CanSetStateAxisResponse[] { axisResponseA });

        CanSetStateDeviceResponse deviceResponseB = new CanSetStateDeviceResponse()
                .withError("Test")
                .withAxisErrors(new CanSetStateAxisResponse[] { axisResponseB });

        assertEquals(deviceResponseA, deviceResponseB);

        deviceResponseA.setAxisErrors(new CanSetStateAxisResponse[] { axisResponseA, axisResponseB });

        assertNotEquals(deviceResponseA, deviceResponseB);
    }

    @Test
    public void testHash() throws Exception {
        FirmwareVersion versionA = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionB = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionC = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(3);

        HashSet<FirmwareVersion> set = new HashSet<>();
        set.add(versionA);
        set.add(versionB);
        set.add(versionC);

        assertEquals(2, set.size());
    }

    @Test
    public void testHashMemberClass() throws Exception {
        FirmwareVersion versionA = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionB = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(0);
        FirmwareVersion versionC = new FirmwareVersion().withMajor(1).withMinor(2).withBuild(3);
        DeviceIdentity identityA = new DeviceIdentity().withAxisCount(3).withFirmwareVersion(versionA);
        DeviceIdentity identityB = new DeviceIdentity().withAxisCount(3).withFirmwareVersion(versionB);
        DeviceIdentity identityC = new DeviceIdentity().withAxisCount(3).withFirmwareVersion(versionC);


        HashSet<DeviceIdentity> set = new HashSet<>();
        set.add(identityA);
        set.add(identityB);
        set.add(identityC);

        assertEquals(2, set.size());
    }

    @Test
    public void testHashMemberArray() throws Exception {
        CanSetStateAxisResponse axisResponseA = new CanSetStateAxisResponse().withError("Test Axis");
        CanSetStateAxisResponse axisResponseB = new CanSetStateAxisResponse().withError("Test Axis");
        CanSetStateAxisResponse axisResponseC = new CanSetStateAxisResponse().withError("Test Axis 2");

        CanSetStateDeviceResponse deviceResponseA = new CanSetStateDeviceResponse()
                .withError("Test")
                .withAxisErrors(new CanSetStateAxisResponse[] { axisResponseA });

        CanSetStateDeviceResponse deviceResponseB = new CanSetStateDeviceResponse()
                .withError("Test")
                .withAxisErrors(new CanSetStateAxisResponse[] { axisResponseB });

        CanSetStateDeviceResponse deviceResponseC = new CanSetStateDeviceResponse()
                .withError("Test")
                .withAxisErrors(new CanSetStateAxisResponse[] { axisResponseC });

        HashSet<CanSetStateDeviceResponse> set = new HashSet<>();
        set.add(deviceResponseA);
        set.add(deviceResponseB);
        set.add(deviceResponseC);

        assertEquals(2, set.size());
    }
}
