# Zaber Motion Library Changelog

## 2025-02-28 7.5.0
* **Bug fixes and Improvements**
    * Added an optional `FirmwareVersion` parameter to `CanSetState` that allows checking if the state can be set to the device after a version change.

## 2025-02-26 7.4.2
* **Bug fixes and Improvements**
    * Updated the offline device database interface to support a recent update to the database schema.
    * Added missing `ToString()` implementations to C++ exceptions.
    * Added missing `__repr__()` implementation to Python exceptions.
    * Added constants for recently added device settings.

## 2025-02-13 7.4.1
* **Bug fixes and Improvements**
    * Fixing G64 G-code: In cases when axes are specified out of order or not at all, the produced arc had a wrong direction.
    * A pre-release version of Zaber Motion Library for the Swift programming language has been added. The API for Swift may change without notice until an official release, and is not yet recommended for production projects.

## 2025-02-07 7.4.0
* **Additions**
    * Driver enable has a timeout argument that defaults to 10 seconds.
      The method will keep trying to enable the driver repeatedly until the timeout expires.
    * `AllAxes` class also gets `driver_enable` and `driver_disable` methods.
    * `Device` and `Axis` class has a new `restore` method that deletes all zaber storage keys
       and sends `system/axis restore` command.

## 2025-01-24 7.3.0
* **Additions**
    * The `SetState` methods of axis and device now return a list of warnings if expected issues occur
* **Bug fixes and Improvements**
    * The `SetState` function disables the driver while setting state.

## 2024-12-06 7.2.3
* **Improvements**
    * New inline documentation added for fields of Option types
    * Sending `get` and `system` commands to a device showing FF will no longer trigger `DeviceFailedException`

## 2024-12-06 7.2.2
* **Bug fixes and Improvements**
    * Servo Tuner parameters can apply default parameters in addition the provided ones
    * Fixing pyinstaller compatibility on Unix
    * Allowing binary devices to assume a different firmware version

## 2024-11-27 7.2.1
* **Bug fixes and Improvements**
    * Fixing precision when setting Servo Tuner parameters

## 2024-11-07 7.2.0
* **Breaking Changes**
    * The C++ import library on Windows (zaber-motion.lib) now has separate Debug and Release files.
* **Additions**
    * `CameraTrigger` class in microscopy
* **Bug fixes and Improvements**
    * Fixing error when setting parameters to disabled objectives in WDI Autofocus.
    * Improving error messages of Trigger API
    * Fixing C++ examples

## 2024-10-21 7.1.2
* **Additions**
    * In C++: Explicitly calls `std::terminate` on unhandled exception thrown from user-registered event callbacks
    * Adds a method to the javascript gateway to bind to core binaries in custom locations

## 2024-10-15 7.1.1
* **Additions**
    * Added a new RemoteModeException type for movement-related commands rejected due to devices being remote mode,
      which forbids local motion control. This is for future EtherCAT support.
    * Made new subclasses of CommandFailedException for the common rejection reasons of BADCOMMAND, BADDATA and
      DRIVERDISABLED. You can continue catching CommandFailedException and checking the data for these conditions
      if you want, or you can catch the more specific exception types instead.
    * Added a stream hold function.

## 2024-09-20 7.0.0
* **Breaking Changes**
    * .NET Framework is no longer supported in C#. In C# the library is compatible with the NetStandard and .NET platforms.
    * The C++ Connection event callback registration API has been modified with the removal of RxCpp:
        * All connection member functions `getXEvent` have been changed to `setXCallback`, which takes an `std::function` object
        * Users are now limited to a single callback per event instead of being able to register arbitrary callbacks via `Connection::getXEvent().subscribe()`
    * Renames C++ shared library from `libzml` to `libzaber-motion` (`zaber-motion.dll` on Windows)
        * C++ API users must update their build configs to link to `libzaber-motion.(so/dylib)` on macOS and linux (`zaber-motion.dll` on Windows)
        * Windows C++ installer will now install Debug and Release builds in the Debug and Release folders
    * Renames Go shared library from `libzaber-motion-lib` to `libzaber-motion-core` (`zaber-motion-core.dll` on Windows)
        * This may result into update of some bundling tools configuration (e.g. pyinstaller)
* **Bug fixes and Improvements**
    * Adds strict null checks for C#
    * Replaces RxCpp dependency with custom callback system in C++
    * Updates C++ DTO setter member funcs to accept arguments by value
    * Updates C++ DTO getter member funcs to return const refs for non-primitive data types
    * Windows C++ installer will now prompt users to uninstall previous bundle if it exists

## 2024-09-27 6.7.1
* **Bug fixes and Improvements**
    * Fixing microscope initialization request in C#

## 2024-09-17 6.7.0
* **New features**
    * `move_sin` method for `Axis` and `Lockstep` classes
    * `renumber` method for `Device` class
    * Timeout argument for `focus_once` autofocus method
* **Bug fixes and Improvements**
    * New advanced parameters for autofocus

## 2024-08-28 6.6.1
* **Bug fixes and Improvements**
    * Fix segfault in Warnings::getFlags and Warnings::clearFlags

## 2024-08-23 Version 6.6.0
* **Bug fixes and Improvements**
    * Low-pass filter for analog inputs.
    * Fixed exception that does not allow to create some microscope classes with empty constructor
    * Fixed argument type for limit setter in Autofocus class
    * Support for new X-LDA in ObjectiveChanger `find` method.
    * ToString methods no longer throw exceptions when called on closed connections.

## 2024-08-15 Version 6.5.0
* **New features**
    * Microscope Autofocus API
* **Bug fixes and Improvements**
    * The Oscilloscope feature now supports unit conversion for analog I/O channels and throws a useful
      exception message if unit conversion is attempted on digital I/O data.

## 2024-08-07 Version 6.4.2
* **Bug fixes and Improvements**
    * Adds support for more units when using the `UnitTable.getUnit()` function

## 2024-07-30 Version 6.4.1
* **Bug fixes and Improvements**
    * Support for servo tuning of rotary stages
    * Improved support for MinGW on Windows

## 2024-07-22 Version 6.4.0
* **New features**
    * Adds new `StreamIo` and `PvtIo` classes to handle IO functionality for `Stream` and `PvtSequence` classes
    * Adds scheduled IO functions to `StreamIo` and `PvtIo`
* **Bug fixes and Improvements**
    * Fixed a dangling pointer in CPP events handling.
    * Fixed an issue with default device database store path on Windows.
* **Deprecated features**
    * The following functions in the `Stream` class have been deprecated in favour of their equivalents in the `StreamIo` class, and will be removed in a future major release:
        * `waitDigitalInput`
        * `waitAnalogInput`
        * `setDigitalOutput`
        * `setAllDigitalOutputs`
        * `setAnalogOutput`
        * `setAllAnalogOutputs`
    * The following functions in the `PvtSequence` class have been deprecated in favour of their equivalents in the `PvtIo` class, and will be removed in a future major release:
        * `setDigitalOutput`
        * `setAllDigitalOutputs`
        * `setAnalogOutput`
        * `setAllAnalogOutputs`

## 2024-07-05 Version 6.3.0
* **New features**
    * Adds `setLabel`, `getLabel`, and `getAllLabels` to Triggers
* **Bug fixes and Improvements**
    * Fixes an incorrect exception in `Storage` when trying to set an empty string value
    * Makes an internal dependency optional
    * Fixes conflicting C++ macros

## 2024-06-24 Version 6.2.1
* **Bug fixes and Improvements**
    * Adding `except` argument to `forget_devices` method of `Connection` class

## 2024-06-20 Version 6.2.0
* **New features**
    * Adding `forget_devices` method to `Connection` class

## 2024-06-10 Version 6.1.0
* **New features**
    * Trigger firing based on absolute value of setting
    * Adding version check between languages and the shared library
* **Bug fixes and Improvements**
    * Renaming share library entry point from `call`,`setEventHandler` to `zml_call`,`zml_setEventHandler`
    * Cleanup of C++ includes to hide shared library gateway
    * Switching bson Python dependency to a forked modernized version
    * Python tolerates Iterable in place of List
    * Nicer error messages when validating Python types

## 2024-05-28 Version 6.0.1
* **Bug fixes and Improvements**
    * Allowing to erase IO label.
    * Typing for events in Python.
    * Ensuring proper types for in Python and JavaScript. Fixes edge cases like `None` being accepted for numbers.

## 2024-05-21 Version 6.0.0
* **Breaking Changes**
    * The API is nearly fully compatible with previous versions
        * Introducing support for nullable primitive types; the following allows nil
            * CarriageMass in servo tuning
            * Scaling, Transformation, MicrostepResolution in gcode
            * Device addresses in MicroscopeConfig
    * The library no longer depends on Google protobufs
    * Specific front-end languages introduced dependency to JSON/BSON serialization libraries
    * Typescript library no longer exports namespaces
    * Python library uses reactivex instead of rx (basically 3 to 4 major version change)
* **Bug fixes and Improvements**
    * Process controller support in C++
* **New Support**
    * Windows on Arm64

## 2024-05-16 Version 5.2.2
* **Bug fixes and Improvements**
    * Improved trigger `onFireSet` and `onFireSetToSetting` functions to allow setting axis-scope settings on all axes simultaneously

## 2024-04-23 Version 5.2.1
* **Bug fixes and Improvements**
    * Fixed a bug that prevented trigger functions without units from working on certain devices

## 2024-04-11 Version 5.2.0
* **New features**
    * Adding labels to IO pins
    * Adding peripheral serial number to axis identity
    * Schedule for analog output pins
* **Bug fixes and Improvements**
    * Sped up serial port opening on Windows
    * Refreshing cached information of microscope classes when `find` is called.
    * Fixed segfault when `call` on called on a disabled stream.
    * Python source distribution now includes downloads the golang binaries.

## 2024-03-13 Version 5.1.5
* **Bug fixes and Improvements**
    * Reduced the size of the Go runtime DLLs by stripping the symbol and DWARF tables.
    * Force initialization for microscope.

## 2024-03-08 Version 5.1.4
* **Bug fixes and Improvements**
    * Storing simple servo tuning paramters in the device storage to ensure compatibility with UI
    * Fixing missing record in RECORD file in Python wheels

## 2024-02-29 Version 5.1.3
* **Bug fixes and Improvements**
    * Returns `null` instead of `0` for missing `defaultValue` in the `SimpleTuningParamDefinition` struct.
    * Introduction of optional values in `gcode.AxisDefinition`, `gcode.AxisTransformation`, `ascii.SimpleTuningParamDefinition` and microscope namespace.
    * Upgrade to protobuf 21.12
    * Renaming of Python binding directory from `bindings` to `zaber_motion_bindings` to avoid conflicts with other packages.
    * Buildng using new dockerized system.

## 2024-02-21 Version 5.1.2
* **Bug fixes and Improvements**
    * Adds `defaultValue` to the `SimpleTuningParamDefinition` struct.
    * Python distribution upgrade

## 2024-02-15 Version 5.1.1
* **New features**
    * `is_homed` and `is_busy` methods for the `AxisGroup` class.
* **Bug fixes and Improvements**
    * Fixing a `NullReferenceException` when working with `Microscope` class in C#.

## 2024-02-08 Version 5.1.0
* **New features**
    * Added functionality to schedule future digital output actions to the `DeviceIO` class.
* **Bug fixes and Improvements**
    * Fixed a bug in the `axis.activate()` function that could lead to a program crash

## 2024-01-30 Version 5.0.1
* **Breaking changes**
    * The Stream and PVT functions of the `Device` class have been moved out into new classes accessible
      via the new `Streams` and `Pvt` members.
    * (C#) The `.NET Standard` framework dependency has been updated from 2.0 to 2.1, meaning this build
      is no longer compatible with any `.NET Framework` versions.
    * (C#) There is now a separate build provided for `.NET Framework 4.6.2`.
    * (C#) The `.NET Core 3.1` framework dependency has been replaced with `.NET 6.0`.
    * (C#) The Visual Studio version used to compile the library has been updated from 2019 to 2022. This should only
      affect people trying to compile the library from source.
    * (C#) Enum value names for Triggers, Process Controller, and Servo Tuner corrected to pascal case.
    * (C++) All exception types have been moved into the `zaber::motion::exceptions` namespace to reduce clutter
      in the main namespace.
    * Trigger distance condition function names changed for clarity
    * Trigger fires remaining and total fires is now -1 when trigger is enabled indefinitely
    * Digital output setter functions now support toggle and keep actions using the new `DigitalOutputAction`
      type as a parameter. Booleans are no longer supported as a parameter.
    * The `streams.toggleDigitalOutput` function has been removed as it duplicates functionality
* **Bug fixes and Improvements**
    * (C#) Fixed a potential deadlock when disposing the Connection or Transport classes in an async context.
    * (C#) Made disposable classes also implement `IAsyncDisposable` in .NET and .NET Standard projects. Connection
      and Transport instances can now be used with the `await using` statement. This is not available in .NET Framework
      projects.
    * Adds a new `activate` method for `Axis` class to enable new peripheral activation on standalone controllers
    * Fixed a bug where setting a trigger condition based on distance or encoder distance for a peripheral did not
      support unit conversions.
    * Fixed a bug where passing an invalid axis as an argument to several functions in the Trigger class
      could crash ZML.

## 2024-01-03 Version 4.8.0
* **New features**
    * A new API for working with device trigger conditions has been added. This API should be considered
      experimental and subject to change for now.
    * Added `driver_disable` and `driver_enable` methods to the `Axis` class.
* **Bug fixes and Improvements**
    * Fixed a bug in `settings.get_many` that would sometimes incorrectly produce NaNs for device-level settings
      mixed in with axis-level settings.

## 2023-11-15 Version 4.7.0
* **New features**
    * Focus datum manipulation for objective changer
    * `is_initialized` method for microscope
* **Bug fixes and Improvements**
    * Illuminator detection fix

## 2023-10-30 Version 4.6.0
* **New features**
    * Added the AxisGroup helper class, which can be used to issue simple move commands and wait for idle conditions
      across any number of axes.
    * New API to get multiple settings in a single synchronized read.

## 2023-09-25 Version 4.5.1
* **New features**
    * Process Controller API.
    * Methods to get and move to index position.
    * Added a UnitTable helper to convert between standard unit symbols like `cm` and the Units enumeration.
        * Note not all symbols are supported for lookup; complex application-specific units and symbols that
          are duplicated between dimensions are omitted.
    * Adds `LAMP` and `PROCESS` as axis types

## 2023-09-06 Version 4.4.0
* **New features**
    * Oscilloscope get/set methods for frequency
    * Oscilloscope get times of all samples

## 2023-08-17 Version 4.3.0
* **New features**
    * Labeling of devices and axes
* **Bug fixes and Improvements**
    * All relevant warnings are included in MovementFailedException's message
    * C++, MSVC: warnings suppressed from external headers and protobuf generated code

## 2023-08-08 Version 4.2.5
* **Bug fixes and Improvements**
    * Switching to /MD(d) flag for the Windows C++ prebuilt binaries
    * Fixing race condition between alert and reply to a move command.
        * Manifests as a movement returning prematurely after previous movement with `wait_until_idle=False` just finishes.

## 2023-07-25 Version 4.2.4
* **Bug fixes and Improvements**
    * Improvements to save/restore
    * Python support for `async with`
    * Fixing universal binary for C++ on macOS

## 2023-07-13 Version 4.2.3
* **Improvements**
    * Add Temperature Units
    * Add string settings constants for several new settings

## 2023-07-06 Version 4.2.2
* **Bug fixes and Improvements**
    * C++: On UNIX the binaries now use soname to attach version information.
    * C++: Disconnect observable is now backed by a replay subject that will replay the event if subscribed after closing.

## 2023-06-29 Version 4.2.1
* **Bug fixes and Improvements**
    * Enable detecting unidentified X-MORs
    * Ensure python library is using the correct backend at runtime.

## 2023-06-16 Version 4.2.0
* **New features**
    * Helix stream movement.

## 2023-06-13 Version 4.1.0
* **New features**
    * `get_from_all_axes` method to get one setting from all axes.
* **Bug fixes and Improvements**
    * `ToString` method call on closed connection does no throw exception.

## 2023-06-05 Version 4.0.0
* **Breaking changes**
    * C++ library requires C++17 standard to compile.
    * C++ wraps some types with `std::optional`.
    * C# moves exceptions into `Zaber.Motion.Exceptions` namespace.
* **New features**
    * PVT Sequences can calculate velocity of points using finite difference.
* **Bug fixes and Improvements**
    * Unused stream and PVT buffers return empty array from `get_content` call (instead of raising exception)

## 2023-07-07 Version 3.4.3
* **Bug fixes and Improvements**
    * C++: On UNIX the binaries now use soname to attach version information.
    * C++: Disconnect observable is now backed by a replay subject that will replay the event if subscribed after closing.
    * Enable detecting unidentified X-MORs

## 2023-06-26 Version 3.4.2
* **New features**
    * Python adds support for unit literals for certain dimensions
    * The Oscilloscope can now record I/O pins on controllers using Firmware 7.33 or later.
* **Bug fixes and Improvements**
    * Setting device database to a non-existing file no longer creates an empty file and throws exception instead.

## 2023-05-24 Version 3.4.1
* **Bug fixes and Improvements**
    * `CommandFailedException` now contains the failed command in message as well as in details.
    * `OscilloscopeData.GetSampleTime()` now correctly reports times for Firmware versions prior to 7.33, which
    recorded the first sample after one timebase interval rather than immediately at the start of the recording window.
    This means the returned times will be one timebase interval higher than with previous ZML versions, unless you are
    using current Firmware.
    * Support for saving/restoring of PVT buffers

## 2023-05-11 Version 3.4.0
* **New features**
    * Support for PVT, upcoming firmware feature
    * Stream discontinuity handling
* **Bug fixes and Improvements**
    * Fixing backwards-compatibility with exports in JavaScript

## 2023-04-28 Version 3.3.0
* **New features**
    * `set_tolerance` method for `Lockstep` class.
    * `get_position` method for `Lockstep` class.
    * `wait_to_respond` method for `Device` class (useful after resetting).
    * `identify` method on `Device` gets `assume_version` argument allowing to override firmware version queried from the device.
    * New documentation at [https://software.zaber.com/motion-library/](https://software.zaber.com/motion-library/).
* **Bug fixes and Improvements**
    * Library is less likely to crash when setting invalid state to a device.

## 2023-04-05 Version 3.2.1
* **Bug fixes and Improvements**
    * Fixed a syntax error in the MATLAB lockstep example code.
    * `MovementFailedException` and `MovementInterruptedException` details now contain device and axis that caused the exception.

## 2023-03-21 Version 3.2.0
* **New features**
    * Experimental microscope API
* **Bug fixes and Improvements**
    * Fixing Python requirements


## 2023-03-03 Version 3.1.1
* **Bug fixes and Improvements**
    * C++ - removing global using clauses
    * Js - initialization improvements
    * Js - exception module export
    * C# - ServoTuner constructor is now public
    * Python - explicit exports that satisfies mypy
    * `CanSetState(Device|Axis)Response` classes moved to `ascii` namespace.
    * Documentation improvements

## 2023-02-14 Version 3.1.0
* **New features**
    * `enable_alerts` (and disable counterpart) methods added to `Connection` class.
        * Documentation is updated with recommendations of using alerts.
* **Bug fixes and Improvements**
    * Alerts are used to improve consistency of movements of all axes.
    * Some C++ arguments changed from passing by const references to passing by value and vice versa.
    * Adding logging of opening of connection through message router.

## 2023-02-07 Version 3.0.1
* **Bug fixes and Improvements**
    * Fixing compatibility with Ubuntu 18 and 22.
    * C++ data-classes have `noexcept` in their constructors.

## 2023-02-01 Version 3.0.0
* **Breaking changes**
     * Major C++ improvements
        * Classes no longer contain hidden pointers to their parents in hierarchy.
            * Instances can be easily copied around without any pitfalls.
        * Connection class is split to a copyable parent BaseConnection and move-only Connection (RAII).
     * Option objects for C++ and Typescript (as alternative to named arguments).
        * In C++, option objects are an additional overloads of existing methods.
        * In Typescript, the positional arguments are removed resulting in many breaking changes.
     * Binary Connection method `open_tcp` no longer has a default value for the TCP port.
     * ASCII Connection method `open_tcp` now has a default value for the TCP port (chain port of X-MCC).
     * The C# type of the `SerialNumber` device property has changed from `uint` to `long` for CLS compliance.
     * The setting name constant for the FW6 `deviceid` setting has been renamed from `Deviceid` to `DeviceIdLegacy`.
     * Device Database Store functionality is enabled by default.
     * `open_serial_port_directly` removed and replaced by argument of `open_serial_port`.
     * `open_iot_unauthenticated` removed and replaced by argument of `open_iot`.
* **Bug fixes and Improvements**
     * The C# library has been marked CLS compliant to enable interoperability with more .NET languages.
     * In-code documentation now contains minimal required firmware version.
     * Acceleration argument of movement methods now accepts Infinity making the device ignore acceleration limits.
     * Movement methods query device for `maxspeed` when acceleration is specified without velocity.
     * Lockstep achieves the same functionality with less messages (communication pattern change)

## 2022-01-20 Version 2.15.4
* **Bug fixes and Improvements**
    * Fixing bug preventing G-Code translator working with stream buffers
    * Improving error for stream setup
    * Improving error for unit conversions
    * Improving documentation of compatibility
    * Fixing C++ freezing if the library was called from an event handler

## 2022-12-12 Version 2.15.3
* **New features**
    * New hidden `GetDbServicePipePath` method to get the location of the pipe serving a local device database
* **Bug fixes and Improvements**
    * Downloads device DB from appropriate source (not "current")
    * Prevents interface IDs from wrapping

## 2022-11-25 Version 2.15.2
* **Bug fixes and Improvements**
    * Upgrading protobuf libraries to 21.9
    * Allowing range for Python protobuf library `protobuf>=3.20.0,<4.22.0`

## 2022-11-14 Version 2.15.1
* **Bug fixes and Improvements**
    * Fixing copy assignment operator in C++ (Credit: Dr. -Ing. Franz-Georg Ulmer)
    * Extending support for Binary Protocol
    * Save/Load state - support for axis storage on integrated devices

## 2022-11-08 Version 2.15.0
* **New features**
    * The Oscilloscope class now exposes the FW 7.29 `numchannels`, `scope.channel.size` and `scope.channel.size.max` settings, with default values for older Firmware versions.
    * The Oscilloscope `Start()` method now has an optional parameter for the FW 7.29 capture length parameter.
    * Adding Storage API methods `listKeys` and `keyExists`.
* **Bug fixes and Improvements**
    * Save/Load state support stored positions.
    * Support for Message Router 1.6.0
    * Documentation fixes
    * Device DB upgrade

## 2022-10-07 Version 2.14.7
* **Bug fixes and Improvements**
    * Save/Load state bug fixes and support of FW 7.29
    * Support for the latest Device DB

## 2022-08-12 Version 2.14.6
* **Bug fixes and Improvements**
    * Fixed error when using the Oscilloscope to record device-scope settings.
    * Corrected some dead links and typos in the documentation.

## 2022-07-27 Version 2.14.5
* **Bug fixes and Improvements**
    * Fixing concurrency issues with Binary Protocol
    * Fixing ASCII message ID leakage during protocol errors
    * Redoing and updating Getting Started guide
    * Support for multiple v8 context for JS library
    * Fixing package size limit exceeding when sending to unknown device
    * Fixing parking not supported error
    * Updating warning flags enum

## 2022-07-08 Version 2.14.4
* **Bug fixes and Improvements**
    * Fixing G-Code exact stop (G9, G61.1)
    * Fixing occasional stalling at sharp corners

## 2022-07-06 Version 2.14.3
* **Bug fixes and Improvements**
    * G-Code optimization introduced in FW 7.28

## 2022-06-20 Version 2.14.2
* **New features**
    * Adds field `version` to paramset info
    * A new method `check_disabled` of the `Stream` class

## 2022-06-09 Version 2.14.1
* **New features**
    * New method to create an offline G-Code translator from a device
* **Bug fixes and Improvements**
    * `erase_key` method of `Storage` class returns false instead of throwing exception when the key does not exist.
    * Concurrency improvements of G-Code translator


## 2022-06-03 Version 2.14.0
* **New features**
    * Custom unit conversions of hardware modified devices
    * Method to erase storage with a particular key
    * `is_homed` method to indicate if device was homed and has reference
* **Bug fixes and Improvements**
    * Enhancing device database failed exception.
    * Reserving maximum space for message IDs in case they get remapped.
    * Fixing an issue with servo-tuner error handling
    * Renaming PeripheralErrors to AxisErrors in CanSetStateDeviceResponse class
    * Documenting matlab array behavior in streams documentation
    * Improving order of setting of settings when setting a state
    * Cleanup of Python package classifiers

## 2022-05-18 Version 2.13.2
* **Bug fixes and Improvements**
    * Fixing Python license field
    * Protobuf 3.20.1

## 2022-05-16 Version 2.13.1
* **Bug fixes and Improvements**
    * Compatibility with the new database

## 2022-04-28 Version 2.13.0
* **New features**
    * Additional arguments (speed, velocity) for movement methods.
    * New dimensions for current tuning settings.
    * A method for verification of simple tuning.

## 2022-04-18 Version 2.12.0
* **New features**
    * Storage API allows adding arbitrary key-value data to a device
    * Line continuations allow sending commands that are too long to fit on a single line
    * Add virtual device connection howto.

## 2022-04-05 Version 2.11.2
* **Bug fixes and Improvements**
    * Fixing G-Code compatibility with older versions of firmware.

## 2022-03-31 Version 2.11.1
* **Bug fixes and Improvements**
    * Fixing exception when setting multiple G-Code translators on one controller
    * Sets storage associated with peripherals on a state restore

## 2022-03-21 Version 2.11.0
* **New features**
    * Servo Tuner API
    * Save/Load supports values stored with the key value store
    * API to get the expected location of message router pipe
* **Bug fixes and Improvements**
    * Fixing port sharing with Zaber Launcher for multiple OS users

## 2022-03-03 Version 2.10.1
* **Bug fixes and Improvements**
    * Adding G-Codes (G30, G30.1, M64, M65, M66, M68, G92, M700)
    * Improvements to TCP/IP documentation.

## 2022-02-16 Version 2.10.0
* **New features**
    * Scope API
    * GCode API
* **Bug fixes and Improvements**
    * Adding compatibility with Mono
    * Improvements of save/restore behavior.

## 2022-01-26 Version 2.9.1
* **New features**
    * Add isIntegrated field to device identity
    * set_all_digital/analog_outputs method for streams
* **Bug fixes and Improvements**
    * Ensure unique titles between ASCII and Binary protocol
    * Better unit conversion errors
    * Fix bug where device restore could fail to set certain settings

## 2022-01-05 Version 2.9.0
* **New features**
    * Experimental G-Code translator (unstable)

## 2021-12-31 Version 2.8.1
* **Bug fixes and Improvements**
    * Fixes an error that occurs when setting the state of a device with an unset stream buffer
    * Amends the changelog with correct date for the 2.8.0 release

## 2021-12-22 Version 2.8.0
* **New features**
    * `GetState` returns a string representing a device or axis' current state. This may include:
        * Settings
        * Servo Tuning
        * Stream Buffers
        * Triggers
    * `SetState` takes a state returned by `GetState` and applies it to a device, setting its state to match the save
    * `CanSetState` will check if a supplied state is compatible with a device or axis, and return an error explaining why not if it isn't.

## 2021-11-30 Version 2.7.2
* **Bug fixes and Improvements**
    * Removing protobuf header files from C++

## 2021-11-15 Version 2.7.1
* **Bug fixes and Improvements**
    * JS library does not work in Windows when path contains unicode characters

## 2021-10-22 Version 2.7.0
* **New features**
    * `GetString` and `SetString` methods for accessing string-based device/axis settings
* **Bug fixes and Improvements**
    * Fixing digital output method on streams
    * Fixing rare python warning message when script ends
    * Documentation improvements

## 2021-08-23 Version 2.6.1
* **Bug fixes and Improvements**
    * Identification resets all stored device information
    * N-API upgrade to version 4 (Node.js library)
    * Unifying web and Node.js library

## 2021-07-30 Version 2.6.0
* **Bug fixes and Improvements**
    * Adding support for communicating with Zaber Launcher remotely (Network Sharing)

## 2021-07-27 Version 2.5.1
* **Bug fixes and Improvements**
    * Fixing Device DB local file compatibility

## 2021-07-26 Version 2.5.0
* **New features**
    * Support for hardware modified devices

## 2021-07-16 Version 2.4.2
* **Bug fixes and Improvements**
    * Upgraded all external dependencies
    * Allowing setting of unknown setting without unit conversion

## 2021-06-28 Version 2.4.1
* **Bug fixes and Improvements**
    * Improved compatibility with Zaber Launcher

## 2021-06-17 Version 2.4.0
* **New features**
    * Support for port sharing with Zaber Launcher
    * Support for upcoming IoT services
* **Bug fixes and Improvements**
    * Logging to file will append to the file instead of overwriting the content.
    * `GetChannelsInfo` will return zeros when device does not support io at all (instead of error).

## 2021-06-01 Version 2.3.2
* **Bug fixes and Improvements**
    * Fixing compatibility with Firmware 6.04

## 2021-04-07 Version 2.3.1
* **New features**
    * Custom transport feature allowing to use the library with different means of communication.
    * WebAssembly support for JS library.
* **Bug fixes and Improvements**
    * Fixing description of park/unpark to be consistent across FW versions.

## 2021-03-10 Version 2.2.0
* **Bug fixes and Improvements**
    * Adding `GetAxisNumbers` method to `Lockstep` class that returns array of axis numbers (`GexAxes` marked obsolete).

## 2021-03-01 Version 2.1.3
* **Bug fixes and Improvements**
    * Ensuring offline Device Database file compatibility. (New database file download required)

## 2021-02-09 Version 2.1.2
* **Bug fixes and Improvements**
    * Adding empty constructor for some C++ classes (Device, Axis, Stream)

## 2021-02-01 Version 2.1.1
* **Bug fixes and Improvements**
    * Support for FW 7.17

## 2021-01-15 Version 2.1.0
* **New features**
    * StopAll and HomeAll methods on Connection class to simplify homing and stopping across the entire device chain
* **Bug fixes and Improvements**
    * Introduction of `peripheral.id` constant (FW 7) and distinction from `peripheralid` constant (FW 6)
    * Fixing naming in DeviceAddressConflictExceptionData renaming `DeviceIds` property to `DeviceAddresses`
    * Adding py.typed to the python library to allow mypy to type check code against the library types

## 2020-11-09 Version 2.0.1
* **Bug fixes and Improvements**
    * Support for multi-axes integrated products (requires refreshing data from api.zaber.io)
    * Error handling tutorial improvements
    * Improvements of JavaDoc
    * Removing mistaken constant `encoder.counth`

## 2020-10-15 Version 2.0.0
* **New features**
    * New exception system providing more information on cause of exceptions
        * e.g. CommandFailedException contains parsed reply from the device
        * new exception SerialPortBusyException
* **Breaking changes**
    * Removal of obsolete methods
        * Lockstep.getOffset
        * Lockstep.getTwist
        * Library.toggleDeviceDbStore
    * Change of library default caching directory for Linux, MacOS
        * On Linux `$XDG_CACHE_HOME/zaber-motion-lib` (or `~/.cache/zaber-motion-lib` if not set)
        * On MacOS `~/Library/Caches/zaber-motion-lib`
    * C++ MotionLibException's method `message()` renamed to `getMessage()` to conform to library conventions

## 2020-09-23 Version 1.6.1
* **Bug fixes and Improvements**
    * Binary device with FW 5 is not asked for FW build

## 2020-09-18 Version 1.6.0
* **Bug fixes and Improvements**
    * Strong-named assemblies for .NET as well as digital signature
    * Unit conversions for controllers
    * Protobufs 3.13.0
    * Go 1.15

## 2020-09-15 Version 1.5.1
* **Bug fixes and Improvements**
    * Fixing that waitForIdle throws CommandPreemptedException upon receiving unexpected response (Binary protocol)
    * Adding isIdentified property on Device class
    * Improvements to the documentation

## 2020-08-19 Version 1.5.0
* **New features**
    * Allowing to disable checksums for a connection
* **Bug fixes and Improvements**
    * Improvements to C++ and Python documentation
    * Js library is compiled to ES2018 standard and contain inline source maps

## 2020-07-24 Version 1.4.4
* **Bug fixes and Improvements**
    * Making asyncio Python methods for opening port cancel-proof in sense that the port is closed when canceled
    * Improving update instructions for Matlab library
    * Adding newly introduced ASCII protocol warning flags

## 2020-07-08 Version 1.4.3
* **Bug fixes and Improvements**
    * Fixing segmentation fault when Python asyncio task is cancelled
    * JavaScript library no longer uses eval in generated protobuf code
    * Dropping support for Node.js 8

## 2020-06-17 Version 1.4.2
* **Bug fixes and Improvements**
    * Critical: Invalidation of Device DB store after temporary caching issue on api.zaber.io

## 2020-06-05, Version 1.4.1
* **Bug fixes and Improvements**
    * Protobufs 3.12.3
    * Documentation improvements

## 2020-06-02, Version 1.4.0
* **New features**
    * move_min and move_max methods
* **Bug fixes and Improvements**
    * Fixing missing `DeviceSettings` class in API documentation
    * Fixing compatibility with Matlab 2020
    * Fixing JS library being broken when packed in asar archive by electron

## 2020-05-20, Version 1.3.1
* **Bug fixes and Improvements**
    * Fixing ArgumentNullException when C# Connection gets disposed by finalizer

## 2020-05-14, Version 1.3.0
* **New features**
    * Parking API
* **Bug fixes and Improvements**
    * Fixing handling of signals on Windows (https://gitlab.com/ZaberTech/zaber-motion-lib/-/issues/5)
    * Empty constructor for C++ Connection class
    * Making toggleDeviceDBStore method obsolete in favor of enableDeviceDBStore
    * Checking axis number in reply in genericCommand

## 2020-04-29, Version 1.2.2
* **New features**
    * Win32 support
    * On UNIX platforms library uses flock in addition to TIOCEXCL
* **Bug fixes and Improvements**
    * `toString` methods for Java and C++ classes
    * Specific exception is thrown when used with incompatible devices
    * C++ documentation for Qt and MinGW

## 2020-04-21, Version 1.2.1
* **New features**
    * Octave support (installation and tutorials)
* **Bug fixes and Improvements**
    *  Adding mathematical rounding to unit conversions
        * Previously the number was cut at needed precision
    * Fixing that stream commands get stuck on a stream FB error
    * Error is thrown when rounding and unit conversion of non-zero acceleration results into a zero value
    * Adding missing namespaces to C++ enums
    * Improved compatibility with old versions of firmware
    * Updating the go protobuf library
    * Little documentation improvements

## 2020-04-07, Version 1.2.0
* **New features**
    * Adding custom timeouts for ASCII generic command methods
    * Adding get/set of default request timeout of ASCII connection
* **Bug fixes and Improvements**
    * Adding documentation of events
    * Adding description of exceptions
    * Hiding internal classes of C# implementation
    * Adding numbering information to API doc

## 2020-04-02, Version 1.1.1
* **Bug fixes and Improvements**
    * Fixing name of `reply_only` event in Python Binary Connection (mistakenly called `alert`)

## 2020-03-30, Version 1.1.0
* **New features**
    * Stream API
        * Provides streaming of movement commands and synchronized multi-axis movement
    * Analog output support
* **Bug fixes and Improvements**
    * Improvement of lockstep documentation.

## 2020-03-23, Version 1.0.13
* **Bug fixes and Improvements**
    * Fixing that copying of Device and Axis classes in C++ makes the properties (e.g. setting, all axes) of instances unusable.
    * Adding code documentation to C++ properties
    * Improving C++ installation documentation
    * Making getters of C++ classes constant (rest of the classes)
    * Improving compatibility with X-JOY3 product
    * Adding lockstep support for more that two axes

## 2020-03-04, Version 1.0.12

* **Bug fixes and Improvements**
    * Fixing Binary Protocol and TCP (Python)
    * Adding const to getters (C++)

## 2020-03-03, Version 1.0.11

* **Bug fixes and Improvements**
    * Providing a default path for Device DB store
* **Documentation changes**
	* Turning on Device DB store in tutorials
    * Making library's need for internet connection more apparent

## 2020-02-26, Version 1.0.10

* **Bug fixes and Improvements**
    * Updating dependencies across the library
        * Protobuf 3.11.2
        * RxPy 3.0.1 (Python)
    * Adding ARM64 (aarch64) platform for Linux

## 2019-12-13, Version 1.0.8

* **Bug fixes and Improvements**
    * Making NuGet package work with MSBuild older than 15.3


## 2019-11-21, Version 1.0.7

* **Bug fixes and Improvements**
    * Improving prepareCommand capabilities allowing numbers to be place directly to template.


## 2019-11-04, Version 1.0.6

* **Bug fixes and Improvements**
    * Fixing compatibility issues with products predating Firmware 5.34 (Auto-Reply setting missing)


## 2019-11-04, Version 1.0.5

* **Bug fixes and Improvements**
    * Improved the error message when the Device Database service is not reachable
    * Ensured compatibility with upcoming Firmware 7 products


## 2019-10-23, Version 1.0.4

* **Bug fixes and Improvements**
    * Added support for Binary Protocol with Firmware version lower than 6.06


## 2019-10-21, Version 1.0.3

* **Bug fixes and Improvements**
    * Improved Firmware 7 support (commands and settings formatting).
    * Added unit conversions and command formatting to `Device` class.
    * Fixed failure of GenericCommandMultiResponse for more than 1k replies.
    * Fixed JavaSript initialization when importing directly from `dist` folder

* **Documentation changes**
	* Adding enum values to the API reference


## 2019-10-04, Version 1.0.2

* **Bug fixes and Improvements**
    * Improved performance of GenericCommandMultiResponse (no waiting when targeting particular device).
    * Removing unused dependencies of the JS library.
    * Fixing protobuf dependency version of the Python library.

* **Documentation changes**
	* General improvements of the texts.


## 2019-09-23, Version 1.0.1

* **Bug fixes**
    * Fixing typescript compilation errors due to empty namespaces.
    * Fixed behavior: Binary connection was getting closed due to unmatched error. Now it emits unknownResponse.
    * Fixed behavior: Commands are now preempted even without matching requests.

* **Documentation changes**
	* Adding C++ examples and installation instructions.

* **API changes**
    * C++ API
    * Reordering genericCommand arguments in binary Device and Communication class (Unreleased)


## 2019-08-30, Version 1.0.0

* **Documentation changes**
	* Fixing couple of mistakes in example codes
	* Improving texts and instructions

## 2019-08-27, Version 0.0.27

* **Documentation changes**
	* Fixing couple of mistakes in example codes

* **Bug fixes**
    * Fixing missing types in Typescript namespaces
    * Fixing harmless error that occasionally pops up in Python on Mac OS
    * Fixing prepareCommand in Python


## 2019-08-19, Version 0.0.26

* **API changes**
    * Breaking change: Unit of measure names in JavaScript and Python have been changed to match common standards
	* Breaking change: Some method names have changed to improve clarity and consistency
	  * `Connection.Device()` and `Lockstep()` are now `GetDevice()` and `GetLockstep()`
	  * `Device.Axis()` and `Lockstep()` are now `GetAxis()` and `GetLockstep()`
	  * `Warnings.Get()` and `Clear()` are now `GetFlags()` and `ClearFlags()`

* **Documentation changes**
	* Many updates to reflect reviewer contributions
    * Docs about finding the right serial port amended to cover direct USB connections
	* Some site navigation controls have been altered
	* Binary protocol support is now fully documented
	* A temporary banner has been added stating this library is in early development and subject to change

* **Bug fixes**
    * Fixed errors when using Binary controller + peripheral combinations


## 2019-08-12, Version 0.0.25

* **Adding settings constants for ASCII Protocol**

* **Finalizing Binary Support**

* **Fixing various issues with C++**

## 2019-07-24, Version 0.0.24

* **Extending settings API to provide arbitrary unit conversions**

* **Providing properties on Axis and Device that expose device identity information**

## 2019-07-12, Version 0.0.23

* **Python bugfix**
    * Fixed a circular dependency problem that prevented using the ASCII Device class in Python.

## 2019-07-12, Version 0.0.22

* **I/O support**
    * I/O pins are now supported on controllers and integrated devices that have them.
    * There is a device I/O how-to guide in the documentation.

* **Lockstep support**
    * Lockstep mode is now supported on two-axis ASCII devices.
    * There is a lockstep how-to guide in the documentation.

* **Binary generic commands**
    * There is now a second Connection class that supports the Binary protocol.
    * Binary generic commands (without unit conversions) can be sent or broadcast.
    * Spontaneous binary messages such as move tracking become events you can subscribe to.
    * Documentation for the Binary protocol is completely separate from ASCII but has the same structure.

* **ASCII-specific namespace (BREAKING change)**
    * Object types specific to the ASCII protocol have been moved into an ASCII namespace or subdirectory.
    * Import statements in existing code will need modification - please refer to the updated documentation.
    * The API reference now indicates what import statements are needed for each type.

* **Example projects**
    * The documentation includes downloadable, self-contained example projects for ASCII in several programming languages.

* **Database usage documentation**
    * There is a how-to guide explaining how to use the device database offline, how to cache data.
    * For Zaberians, the how-to also explains how to use the master database.

* **Exception types (BREAKING change)**
    * The numeric MotionLibErrorType code no longer exists
    * Instead there are different subclasses of MotionLibException to report different error conditions.
    * The list of exception types can be seen at the bottom of the API reference section for MotionLibException.


## 2019-06-12, Version 0.0.21

* **Java Support**
    * Full support of Java programming language with the API matching the APIs of other languages.
    * Available through Maven.

* **Changed type of the serial number in device identity to unsigned integer**
    * Ensuring that the library covers the full range of Zaber serial numbers.

* **Documentation improvements**
    * Improved navigation in Getting Started guide.
    * Improved responsive design.
    * External links are marked with an icon.
    * Language specific paragraphs.


## 2019-06-05, Version 0.0.20

* **Fixing parsing of serial number**
    * Only affects Zaber internal development.


## 2019-05-28, Version 0.0.19

* **Renaming Communication class to Connection**

* **Adding renumber method to Connection class**
    * The method allows to automatically assign addresses to a device chain.

* **Python asyncio**
    * All blocking methods in Python got new asyncio counterparts with `_async` suffix.

* **Improving the error message when the setting is not supported by the device**


## 2019-05-15, Version 0.0.18

* **Device Database store**
    * It is a feature allowing to store Device Database information to hard-drive to minimize the usage of web-service API.

* **Adding serial number to device identity**
    * Queries device for system.serial and exposes the returned information in a field.

* **Renaming method wait_till_idle to wait_until_idle**

* **Device settings API**
    * Allows read and write device settings using API similar to axis settings.

* **Improving error message when device rejects the read or write of settings**

* **Device Database documentation**
    * Adding more information about Device Database and its API.

* **Sorting classes in the reference**

* **Fixing letter case of enum values in the reference (Python, Js)**

* **Improving grammar in documentation**
    * Improving texts on the portal.

* **Adding an appropriate license to source code and packaging**
    * Additionally, collecting licenses of all used libraries and tools.

* **Adding Changelog**
