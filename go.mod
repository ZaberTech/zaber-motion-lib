module zaber-motion-lib

go 1.23

require (
	github.com/Microsoft/go-winio v0.6.2
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/elliotchance/pie/v2 v2.5.1
	github.com/ethereum/go-ethereum v1.10.16
	github.com/go-gl/mathgl v1.0.0
	github.com/iancoleman/strcase v0.3.0
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/zabertech/go-serial v0.0.0-20210201195853-2428148c5139
	gitlab.com/ZaberTech/zaber-device-db-service v0.0.0-20250220200722-1bb521485637
	gitlab.com/ZaberTech/zaber-go-lib v0.0.0-20240813205049-f6bcd1951361
	go.mongodb.org/mongo-driver v1.14.0
	golang.org/x/exp v0.0.0-20220321173239-a90fa8a75705
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/deckarep/golang-set v0.0.0-20180603214616-504e848d77ea // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
)

replace github.com/ethereum/go-ethereum => github.com/zabertech/go-ethereum v1.10.4-rpc-1
