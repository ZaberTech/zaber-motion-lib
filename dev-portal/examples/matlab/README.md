Zaber MATLAB example
====================

How to Run
----------

1. Open MATLAB, search for `Zaber Motion Library` in Add-On Explorer (Home > Add-Ons > Get Add-Ons).
2. Click on the `Zaber Motion Library` result and click `Add` to install the Add-On.
3. Open `Main.m` file inside this folder.
4. Change `openSerialPort` argument on line 6 to your serial port with Zaber devices.
5. Run the program by selecting Editor -> Run.

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
