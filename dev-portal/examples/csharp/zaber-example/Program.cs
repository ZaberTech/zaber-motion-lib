using System;
using Zaber.Motion;
using Zaber.Motion.Ascii;

namespace csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var connection = Connection.OpenSerialPort("COM3")) {
                connection.EnableAlerts();

                var deviceList = connection.DetectDevices();
                Console.WriteLine($"Found {deviceList.Length} devices.");

                var device = deviceList[0];

                var axis = device.GetAxis(1);
                if (!axis.IsHomed())
                {
                    axis.Home();
                }

                // Move to 10mm
                axis.MoveAbsolute(10, Units.Length_Millimetres);

                // Move by an additional 5mm
                axis.MoveRelative(5, Units.Length_Millimetres);
            }
        }
    }
}
