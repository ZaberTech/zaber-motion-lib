Zaber C\# Example
=================

How to Run
----------
1. Open Visual Studio and open `zaber-example.sln` solution inside the `zaber-example` folder.
2. Open `Program.cs` and change `OpenSerialPort` argument on the line 10 to your serial port with Zaber devices.
3. Build the project by selecting Build -> Build Solution.
4. Once the build is successful, run it by selecting Debug -> Start Debugging.

Further Reading
---------------

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
