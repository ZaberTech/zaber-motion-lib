javaaddpath("motion-library-jar-with-dependencies.jar");

CONNECTION_CLASS = "zaber.motion.ascii.Connection";
LENGTH_MILLIMETRES = java_get("zaber.motion.Units", "LENGTH_MILLIMETRES");

connection = javaMethod("openSerialPort", CONNECTION_CLASS, "COM3");
try
    connection.enableAlerts();

    deviceList = connection.detectDevices();
    fprintf("Found %d devices.\n", length(deviceList));

    device = deviceList(1);

    axis = device.getAxis(1);
    if (!axis.isHomed())
        axis.home();
    endif

    % Move to 10mm
    axis.moveAbsolute(10, LENGTH_MILLIMETRES);
    % Move by an additional 5mm
    axis.moveRelative(5, LENGTH_MILLIMETRES);

    connection.close();
catch exception
    connection.close();
    rethrow(exception);
end
