Zaber Octave example
====================

How to Run
----------

1. Download the library's jar file from [Zaber Developer Portal](https://software.zaber.com/motion-library/docs/tutorials/install/octave).
2. Move the jar file into this folder.
3. Launch Octave and open the current folder.
4. Open `example.m` file in the editor.
5. Change `openSerialPort` argument on line 9 to your serial port with Zaber devices.
6. Run the program by selecting Run -> Save File and Run.

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
