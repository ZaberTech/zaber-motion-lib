Zaber Java Example
==================

There are several IDEs, that can be used to run this example. These instructions
use IntelliJ.

How to Run
-----

1.  Open IntelliJ IDE
2.  Select \'Open Project\' or \'File > Open\', navigate to this folder and select
    `zaber_example`.
3.  Open `App.java` below the `src/main/java/zaber_example/` folder.
    and change the `OpenSerialPort` argument on the line 12 to your serial port with Zaber devices.
4.  Run the project by right clicking `App.java` and selecting Run.

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
