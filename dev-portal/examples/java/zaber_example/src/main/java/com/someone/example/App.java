package com.someone.example;

import zaber.motion.Units;
import zaber.motion.ascii.Axis;
import zaber.motion.ascii.Connection;
import zaber.motion.ascii.Device;

/**
 * Example Java Zaber application
 */
public class App
{
    public static void main(String[] args)
    {
        try (Connection connection = Connection.openSerialPort("COM3")) {
            connection.enableAlerts();

            Device[] deviceList = connection.detectDevices();
            System.out.println(String.format("Found %d devices.", deviceList.length));

            Device device = deviceList[0];

            Axis axis = device.getAxis(1);
            if (!axis.isHomed()) {
                axis.home();
            }

            // Move to 10mm
            axis.moveAbsolute(10, Units.LENGTH_MILLIMETRES);
            // Move by an additional 5mm
            axis.moveRelative(5, Units.LENGTH_MILLIMETRES);
        }
    }
}
