Zaber Nodejs example
====================

How to Run
----------

In order to run the project you need to install [Node.js](https://nodejs.org/).
Afterwards, open `main.js` file in your favorite code editor
and change `openSerialPort` argument on the line 4 to your serial port with Zaber devices.
Open a terminal in this folder and run the following commands:

    npm install
    npm start

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
