Zaber Python Example
====================

How to Run
----------
Open `main.py` file in your favorite code editor and change `open_serial_port`
argument on the line 3 to your serial port with Zaber devices.

Then open a command prompt in this folder and enter the following commands,
depending on your operating system.

### Linux & MacOS

``` {.bash}
# install zaber-motion library
pip3 install --user zaber-motion

# run example program
python3 main.py
```

### Windows

``` {.bash}
# install the library
py -3 -m pip install --user zaber-motion

# run example program
py -3 main.py

```

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
