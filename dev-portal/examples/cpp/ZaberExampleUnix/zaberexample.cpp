#include <iostream>
#include <vector>
#include <exception>
#include <zaber/motion/ascii.h>

using namespace zaber::motion;
using namespace zaber::motion::ascii;

int main() {
    try {
        Library::checkVersion();

        Connection connection = Connection::openSerialPort("/dev/ttyUSB0");
        connection.enableAlerts();

        std::vector<Device> deviceList = connection.detectDevices();
        std::cout << "Found " << deviceList.size() << " devices" << std::endl;

        Device device = deviceList[0];

        Axis axis = device.getAxis(1);
        if (!axis.isHomed()) {
            axis.home();
        }

        // Move to 10mm
        axis.moveAbsolute(10, Units::LENGTH_MILLIMETRES);

        // Move by an additional 5mm
        axis.moveRelative(5, Units::LENGTH_MILLIMETRES);

        return 0;
    } catch (const std::exception& e) {
        // Print the error before crashing the program.
        std::cerr << e.what() << std::endl;
        throw;
    }
}
