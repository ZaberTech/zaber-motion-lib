Zaber C++ Example
==================

This example uses CMAKE to generate build files for Unix based OSes, and Visual Studio for Windows.

How to Run
----------
Open `main.cpp` file in your favorite code editor and change the `openSerialPort`
argument on line 13 to the serial port with your Zaber device.

Then follow the instructions below.

### Install the Zaber Motion C++ Library

Follow the instructions on the online documentation [here](https://software.zaber.com/motion-library/docs/tutorials/install/cpp).


### Linux

``` {.bash}
# After installing the Zaber Motion Library

# Build and run the example
cd ZaberExampleUnix
g++ -std=c++17 zaberexample.cpp -o zaberexample -lzaber-motion
./zaberexample
```

### MacOS

``` {.bash}
# After installing the Zaber Motion Library

# Build and run the example
cd ZaberExampleUnix
clang++ -std=c++17 zaberexample.cpp -o zaberexample -lzaber-motion -Wl,-rpath,/usr/local/lib
./zaberexample
```

### Windows

1.  Open the `ZaberExampleWindows/ZaberExample/ZaberExample.sln` file with Visual Studio 2017
2.  In the menu, select `build` -> `Build Solution`
3.  In the unzipped example folder, go to the `Release` directory
4.  Double click on `zaberexample.exe`

### Further Reading

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
