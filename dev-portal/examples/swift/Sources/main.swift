import ZaberMotion
import ZaberMotionAscii
import ZaberMotionExceptions

do {
    let connection = try await Connection.openSerialPort(portName: "COM3")
    try await connection.enableAlerts()

    let deviceList = try await connection.detectDevices()
    print("Found \(deviceList.count) devices.")

    let device = deviceList[0]
    let axis = try device.getAxis(axisNumber: 1)

    if (!(try await axis.isHomed())) {
        try await axis.home()
    }

    // Move to 10mm
    try await axis.moveAbsolute(position: 10, unit: Units.Length.mm)
    // Move by additional 5mm
    try await axis.moveRelative(position: 10, unit: Units.Length.mm)

    try await connection.close()
} catch let e as MotionLibException {
    print(e.toString())
} catch {
    print("Error: \(error)")
}
