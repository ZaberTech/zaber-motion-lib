// swift-tools-version: 6.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HelloZaber",
    platforms: [
        .macOS(.v10_15)
    ],
    dependencies: [
        .package(
            url: "https://github.com/zabertech/zaber-motion-lib-swift",
            from: "7.5.0"
        )
    ],
    targets: [
        .executableTarget(
            name: "hello-zaber",
            dependencies: [
                .product(name: "ZaberMotionLib", package: "zaber-motion-lib-swift")
            ]
        ),
    ]
)
