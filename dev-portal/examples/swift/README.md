Zaber Swift Example
=================

How to Run
----------
1. Open project folder in XCode.
2. Open `HelloZaber.swift` and change the `openSerialPort` `portName` argument on the line 7 to your serial port with Zaber devices.
3. Start the program.

You may also run the program from the terminal by navigating to the project folder and entering `swift run`. Note that you will still have to complete step 2 above in the text editor of your choice.

Further Reading
---------------

For more information, troubleshooting, and guides, check out the [Zaber
Developer Portal](https://software.zaber.com/motion-library).
