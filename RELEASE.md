# Production Release

In the zaber-motion-lib directory:
* Create a new branch with name starting with "release"
* Prepare the release notes:
  * You will need an environment variable called GITLAB_TOKEN to be set to a private api access token generated from the
    [Access Tokens page of your GitLab user settings](https://gitlab.izaber.com/-/profile/personal_access_tokens)
  * `cd scripts` and `node release_notes.js`
  * The output contains the merge request titles since the last version tag. Review and edit the output; look at
    completed tickets or individual commits if you need more information
  * Add the new release notes at the top of CHANGELOG.md in the repo root directory
* Run `npx gulp set_version --new_version X.X.X` from the main directory to set version for the library
* Commit, push
* Optional manual testing:
  * Execute the manual jobs called `publish_rc` in the CI pipeline to publish release candidates
  * Test the release candidates, for example by walking through the new project setup instructions in
    the documentation, or running previously created projects for each language
* Execute the manual jobs called `publish_prod` in the CI to finish the publishing
  * Some of these jobs require the corresponding `publish_rc` to be run first - particularly JS
  * The publish_dev_portal_zshared job might fail if someone has manually edited files at the destination.
    If this happens, delete the affected destination files and retry
* In [developer portal](https://gitlab.izaber.com/software-public/developer-portal): update git submodule commit ref and do release
* Create a tag named `vX.X.X` (substitute actual version) at the commit you started the pipeline from and push the tag:
```
git tag vX.X.X
git push origin vX.X.X
```
* Dependencies that might need upgrading:
  * Consider adding a release on MATLAB file exchange if there is a new feature or MATLAB specific bug.
  * Consider updating app components to the newest ZML version if the features/bugfixes in this release affect launcher or the web front end.

# Dependencies upgrade
* Once in a while we should upgrade our dependencies.
* You can do that by running `npx gulp upgrade_all_dependencies`.


# Internal Releases

## Testing JS/TS

Sometimes it's useful to test JS version of the library with our software before publishing it for our customers.
* Create a new branch with name starting with "release"
* Edit `js/package.json` and change version to e.g. `2.7.9-rc1` and run `npm i`
* Commit, push
* Run manual CI job `publish_js_rc` and nothing else
* Install wherever needed by specifying the exact version e.g. `2.7.9-rc1`
* After this has completed, you should also consider releasing this to app-components if there is any new functionality you wish to use with launcher. Otherwise, it will continue using the old version of motion library in many places throughout the app, leading to errors when using devices injected by the services. To release with `app-components`:
  * bring the `test` or `staging` branch of `app-components` up to date, and then push it to origin
  * Run the pipelines on the branch. It should automatically trigger a `publish_gitlab` job if the build is successful
  * Add the newly published version of `app-components` to the dependencies of launcher, and everything should work as expected.
  * Afterwards, be sure to kick off a new `build`/`publish_gitlab` job from `app-components` master so that no one else ends up using your code under test.


  ## Testing Python

  * Python [requires](https://packaging.python.org/en/latest/specifications/core-metadata/#version) that versions are numbered with a limited set of characters, thus all versions you hope to use to test with python should use `rc`+ candidate version. so `npx gulp set_version --new_version 1.2.3-rc1` is OK, but `npx gulp set_version --new_version 1.2.3-test-release-1` will not build.
  * Install with a command like: `pip install -i https://test.pypi.org/simple/ zaber-motion==1.2.3rc1` (note that the `-` between the patch version and `rc` is removed here)
