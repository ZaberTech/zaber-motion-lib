//go:build !wasm

package main

import "C"
import (
	"log"
	"unsafe"
	"zaber-motion-lib/internal/gateway"
	_ "zaber-motion-lib/internal/init"
)

func init() {
	log.Print("Zaber Motion Library init...")
}

//export zml_call
func zml_call(request unsafe.Pointer, tag int64, callback unsafe.Pointer, async bool) int32 {
	return gateway.CallInternal(request, (gateway.Tag)(tag), callback, async)
}

func main() {

}
